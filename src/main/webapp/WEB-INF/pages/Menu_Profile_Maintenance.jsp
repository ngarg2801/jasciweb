<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html lang="en">

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

<link href="<c:url value="/resourcesValidate/css/MenuProfileMaintenance.css"/>" rel="stylesheet" type="text/css">
<style>
.k-virtual-scrollable-wrap {
height: 100%;
overflow-x: hidden;

}

.k-grid th.k-header, .k-grid-header {
white-space: normal !important;
padding-right: 13px !important;
}


@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape){
.k-grid th.k-header, .k-grid-header {
white-space: normal !important;
padding-right: 0px !important;
}
}

@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait)
{

.k-grid th.k-header, .k-grid-header {
white-space: normal !important;
padding-right: 0px !important;
}

}
</style>
		<body>

			<div class="page-container">
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<div class="container">
						<!-- BEGIN PAGE TITLE -->
						<div class="page-title">
							<h1>${objMenuOptionLabel.getLblMenuProfilesMaintenance()}</h1>
						</div>
						<!-- END PAGE TITLE -->
						<!-- BEGIN PAGE TOOLBAR -->

						<!-- END PAGE TOOLBAR -->
					</div>
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">

					<div class="container">
						<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
						<div class="modal fade" id="portlet-config" tabindex="-1"
							role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true"></button>
										<h4 class="modal-title">Modal title</h4>
									</div>
									<div class="modal-body">Widget settings form goes here</div>
									<div class="modal-footer">
										<button type="button" class="btn blue">Save changes</button>
										<button type="button" class="btn default" data-dismiss="modal">Close</button>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
						<!-- /.modal -->
						<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
						<!-- BEGIN PAGE BREADCRUMB -->

						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->

						<div class="portlet light" id="portletlightwhite">

							<div class="portlet-body">
								<div id="ErrorMessage" class="note note-danger"
									style="display: none; margin-left: -14px;">
									<p class="error-MPA" id="Perror"
										style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
								</div>
								<form:form class="form-horizontal form-row-seperated"
									name="myForm" action="#" onsubmit="return isformSubmit();"
									modelAttribute="MenuProfile_Update_Object" method="post">


									<div class="row">
										<div class="col-md-12">
											<div class="portlet">

												<div class="portlet-body">
													<div class="tabbable">

														<div class="tab-content no-space">
															<div class="tab-pane active" id="tab_general">
																<div class="form-body">
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenuProfile()}:
																		<span class="required"> * </span></label>
																		<div class="col-md-10">
																		
																		
																			<input type="text" class="form-controlwidth"
																				id="MenuProfileID" name="MenuProfile"
																				 maxlength="100"
																				value="${objHeader.getMenuProfile()}"/>
																				
																				<input type="hidden" class="form-controlwidth"
																				id="MenuProfileIDH" name="MenuProfile"
																				 maxlength="100"
																				value="${objHeader.getMenuProfile()}"/>
																				
																				 <span
																				id="ErrorMessageMenuProfile" class="error"> </span>
																		</div>

																	</div>
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLblLastActivityBy()}:
																		</label>
																		<div class="col-md-10">
																			<input type="text" class="form-controlwidth"
																				name="LastActivityTeamMember" id="lastActivityTeamMemberID"
																				value="${objHeader.getLastActivityTeamMember()}"
																				disabled="disabled"/>
																				
																				<input type="hidden" class="form-controlwidth"
																				name="LastActivityTeamMember" id="lastActivityTeamMemberID"
																				value="${objHeader.getLastActivityTeamMember()}"
																				>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLblLastActivityDate()}:
																		</label>
																		<div class="col-md-10">
																			<input type="text" class="form-controlwidth"
																				name="LastActivityDate" id="LastDateID"
																				
																				disabled="disabled"/>
																				<input type="hidden" class="form-controlwidth"
																				name="LastActivityDateH" id="LastDate"
																				
																				>
																		</div>
																	</div>
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenuType()}:
																			<span class="required"> * </span>
																		</label>
																		<div class="col-md-10">
																			<select class="selectmenumaintenance form-control" name="MenuType"
																				id="MenuTypeID" onchange="DownSelectedChange();">
																				<option value="">${objMenuOptionLabel.getLblSelect()}...
																				</option>
																				<c:if test="${not empty objGeneralCodeMenuType}">
																					<c:forEach var="MenuTypeList"
																						items="${objGeneralCodeMenuType}">

																						<option
																							value="${MenuTypeList.getGeneralCode()}">${MenuTypeList.getDescription20()}</option>
																					</c:forEach>
																				</c:if>




																			</select> <span id="ErrorMessageMenuType" class="error-select">
																			</span>
																		</div>

																	</div>
																	
																	
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLblSelectApplication()}:
																		</label>
																		<div class="col-md-10">
																			<select class="selectmenumaintenance form-control"
																				name="Application" id="Application"
																				onchange="DownSelectedChange();">
																				<option value="">${objMenuOptionLabel.getLblSelect()}...</option>

																				<c:if test="${not empty objGeneralCodeApplication}">
																					<c:forEach var="ApplicationList"
																						items="${objGeneralCodeApplication}">

																						<option
																							value="${ApplicationList.getGeneralCode()}">${ApplicationList.getDescription20()}</option>
																					</c:forEach>
																					<option
																						value="Display All">Display All</option>
																				</c:if>

																			</select> <span id="ErrorMessageApplication" class="error">
																			</span>
																		</div>

																		<%-- 	<span class="displayallprofile">${objMenuOptionLabel.getLblOR()}</span>
											<span style="margin-left:9px; position:relative; top: -30px;"><button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>${objMenuOptionLabel.getLbldisplayall()}</button></span> --%>
																	</div>
																	
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenu_App_Icon()}:
																		<span class="required"> * </span></label>
																		<div class="col-md-10">
																			<select class="selectmenumaintenance form-control" name="AppIcon"
																				id="AppIconID" onchange="ChangeImage()">
																				<option value="">${objMenuOptionLabel.getLblSelect()}...</option>

																				<c:if test="${not empty objMenuAppIcon}">
																					<c:forEach var="AppIconList"
																						items="${objMenuAppIcon}">

																						<option value="${AppIconList.getAppIcon()}">${AppIconList.getAppIcon()}</option>
																					</c:forEach>

																				</c:if>




																			</select><img
																				src=""
																				alt="" id="receiveicon"></img> <span
																				id="#ErrorMessageAppIcon1" class="error-select"
																				>
																			</span>
																		</div>

																	</div>
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLbldescriptionshrot()}:
																			<span class="required"> * </span>
																		</label>
																		<div class="col-md-10">
																			<input type="text" class="form-controlwidth"
																				name="Description20" id="DescriptionShortID"
																				 maxlength="20"
																				 value="${objHeader.getDescription20()}"> <span
																				id="ErrorMessageDescriptionShort" class="error">
																			</span>
																		</div>

																	</div>
																	<div class="form-group">
																		<label class="col-md-2 control-label">${objMenuOptionLabel.getLbldescriptionlong()}:
																			<span class="required"> * </span>
																		</label>
																		<div class="col-md-10">
																			<input type="text" class="form-controlwidth"
																				name="Description50" id="DescriptionLongID"
																				value="${objHeader.getDescription50()}" maxlength="50"> <span
																				id="ErrorMessageDescriptionLong" class="error">
																			</span>
																		</div>

																	</div>

																	<div class="repeatingSection" id="TextBoxesGroup">
																		<div class="form-group" id="TextBoxDiv1">
																			<label class="col-md-2 control-label">${objMenuOptionLabel.getLblHelpLine()}:
																				<span class="required"> * </span>
																			</label>
																			<div class="col-md-10">
																				<textarea class="form-controlwidth" id="HelpLineID" style="resize:none"
																					name="HelpLine" maxlength="500">${objHeader.getHelpLine()}</textarea>
																				<span id="ErrorMessageHelp" class="error error-TextArea"
																					> </span>
																			</div>

																		</div>

																	</div>






																	<input type="hidden" class="form-controlwidth"
																		name="AssignList" id="AssignList" />











																</div>
															</div>




														</div>
													</div>
												</div>
											</div>

										</div>

									</div>

			<div class="portlet box blue" style="border: 1px solid #EFF3F8;">
						<div class="portlet-title" id="MenuOptionlblID">
							<div class="caption" id="lblMenuOptiontitleDiv" >
							${objMenuOptionLabel.getLblAvailableMenuOptions()}
							</div>
							
						</div>
						<div class="portlet-title" id="MenuProfileOptionlblID">
							<div class="caption" id="lblMenuPRofileOptionDiv">
							${objMenuOptionLabel.getLblMenuProfilesOptions()}
							</div>
							
						</div>
						</div>
									<div id="panels">

										<div id="grid1"></div>
										<div id="commands">
											<div>
												<a href="" id="copySelectedToGrid2" type="button"
													class="k-button">&gt;</a>
											</div>
											<div>
												<a href="" id="copySelectedToGrid1" type="button"
													class="k-button">&lt;</a>
											</div>
											<div>
												<a href="" id="copySelectedToGridAll2" type="button"
													class="k-button">&gt;&gt;</a>
											</div>
											<div>
												<a href="" id="copySelectedToGridAll1" type="button"
													class="k-button">&lt;&lt;</a>
											</div>
										</div>
										<div id="grid2"></div>
									</div>

									<!--end tabbable-->


									<!-- END PAGE CONTENT INNER -->
									<div class="margin-bottom-5-right-allign_menu_add_profile" id="MenuProfileSaveAndCancel">
										<button class="btn btn-sm yellow filter-submit margin-bottom">
											<i class="fa fa-check"></i>&nbsp;${objMenuOptionLabel.getLblUpdate_Save()}</button>
										<button type="button" onclick="resetPage()" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> ${objMenuOptionLabel.getLblReset()}</button>
										<button type="button" onclick="cancelButton()"
											class="btn btn-sm red filter-cancel">
											<i class="fa fa-times"></i>
											${objMenuOptionLabel.getLblCancel()}
										</button>
									</div>


								</form:form>
							</div>



						</div>
					</div>

				</div>
				<!-- END PAGE CONTENT -->
				<!-- BEGIN QUICK SIDEBAR -->
				<!--Cooming Soon...-->
				<!-- END QUICK SIDEBAR -->
			</div>
			<!-- END PAGE CONTAINER -->
			<!-- BEGIN PRE-FOOTER -->
	</tiles:putAttribute>

</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/moment.min.js"/>"
	type="text/javascript"></script>
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />

<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
	type="text/javascript"></script>
<script type="text/javascript">

function resetPage(){
    
    window.parent.location = window.parent.location.href;
    
   }

	//for set image on chenge drop down
	function ChangeImage() {
		var valuaddress = $('#AppIconID  option:selected').text();
		var valappIconSelect = $('#AppIconID  option:selected').val();
		if(valappIconSelect==""){
			$("#receiveicon").hide();
			//$("##ErrorMessageAppIcon1").show();
		}else{
		 $.ajax({
		     url: '${pageContext.request.contextPath}/MenuAppIconUrl',
		     cache: false,
		     error: function() {
		        //$('#info').html('<p>An error has occurred</p>');
		     },
		     data:{ MenuOptionApplication: valuaddress },
		     success: function(data) {
		    	 $("#receiveicon").show();
		 		jQuery("#receiveicon").attr('src', data[0]);
		 		//$("##ErrorMessageAppIcon1").hide();
		 		isBlankStateCode('ZipCode', '#ErrorMessageAppIcon1',
		 	           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		 	             + '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');
		     },
		     type: 'POST'
		  });
		//  $('#container').html('<img alt="" src="'+$(this).val()+'">');
		}
	}
	
	
	function onchangeMenuType(){
		var valMenuType = $('#MenuTypeID  option:selected').val();
		if(valMenuType == ""){
			$("#Application").attr('disabled', 'disabled');
		}else{
			$("#Application").removeAttr("disabled");
		}
	}

	
	
	
	//for checking which screen previous
	function checkEditORSave() {

		var editScreen = "${objIsEdit}";
		if (editScreen == "EditScreen") {
			$('#MenuProfileID').attr('disabled', 'disabled');
			DropDownSelected();
			$('#MenuTypeID').attr('disabled', 'disabled');
			ChangeImage();
			loadEditGrid();
		} else {
			$("#Application").attr('disabled', 'disabled');
			var dataVal = "";

			var grid2 = $("#grid2").kendoGrid({
				dataSource : dataVal,
				editable : "popup",
				selectable : "multiple",
				pageable : false,
				columns : [ {
					field : "application",
					width : 90,
					title : "${objMenuOptionLabel.getLblApplication()}"
				}, {
					field : "applicationSub",
					width : 90,
					title : "${objMenuOptionLabel.getLblSubApplication()}"
				}, {
					field : "menuOption",
					width : 90,
					title : "${objMenuOptionLabel.getLblMenuOption()}"
				}

				]
			}).data("kendoGrid");

			var grid1 = $("#grid1").kendoGrid({
				dataSource : dataVal,
				editable : "popup",
				selectable : "multiple",
				pageable : false,
				columns : [ {
					field : "application",
					width : 90,
					title : "${objMenuOptionLabel.getLblApplication()}"
				}, {
					field : "applicationSub",
					width : 90,
					title : "${objMenuOptionLabel.getLblSubApplication()}"
				}, {
					field : "menuOption",
					width : 90,
					title : "${objMenuOptionLabel.getLblMenuOption()}"
				} ]
			}).data("kendoGrid");

			
			$("#copySelectedToGrid2").on("click", function(idx, elem) {

				moveTo(grid1, grid2);

				var jsonVal = JSON.stringify(grid2.dataSource.data())
				$("#AssignList").val(jsonVal);

				//return false;
			},false);
			$("#copySelectedToGrid1").on("click", function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				//return false;
			},false);

			$("#copySelectedToGridAll2").on("click", function(idx, elem) {
				moveToAll(grid1, grid2);
				var jsonVal = JSON.stringify(grid2.dataSource.data())
				$("#AssignList").val(jsonVal);

				//return false;
			},false);

			$("#copySelectedToGridAll1").on("click", function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				//return false;
			},false);
		}
	}

	function isformSubmit() {

		var isSubmit = false;
		var testNumber = false;

		var isBlankMenuProfile = isBlankField(
				'MenuProfileID',
				'ErrorMessageMenuProfile',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankMenuType = isBlankField(
				'MenuTypeID',
				'ErrorMessageMenuType',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankAppIcon = isBlankField(
				'AppIconID',
				'#ErrorMessageAppIcon1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankShortDescription = isBlankField(
				'DescriptionShortID',
				'ErrorMessageDescriptionShort',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankLongDescription = isBlankField(
				'DescriptionLongID',
				'ErrorMessageDescriptionLong',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankHelpLine = isBlankField(
				'HelpLineID',
				'ErrorMessageHelp',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
		/* var isBlankApplication = isBlankField(
				'Application',
				'ErrorMessageApplication',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}'); */

		if (isBlankHelpLine && isBlankLongDescription
				&& isBlankShortDescription && isBlankAppIcon && isBlankMenuType
				&& isBlankMenuProfile) {

			var editScreen = "${objIsEdit}";
			if (editScreen == "EditScreen") {

				
				var jsonData=$('#AssignList').val();
				var hiddenInput,para,MenuTypeInput;
				// para = document.getElementById('hidden');
				 /*  hiddenInput = document.createElement('input');
		            hiddenInput.type = 'hidden';
		            hiddenInput.name = 'MenuProfile';
		            hiddenInput.value = '${objHeader.getMenuProfile()}'; */
		            MenuTypeInput = document.createElement('input');
		            MenuTypeInput.type = 'hidden';
		            MenuTypeInput.name = 'MenuType';
		            MenuTypeInput.value = '${objHeader.getMenuType()}';
		           // para.appendChild(hiddenInput);   
				if(jsonData.length>0 && jsonData.length!=2){
					
					
					 bootbox.alert('${objMenuOptionLabel.getLblON_UPDATE_SUCCESSFULLY()}',function(){
						 document.myForm.action = '${pageContext.request.contextPath}/MenuProfile_Maintenance_UpdateTable';
							//document.myForm.appendChild(hiddenInput);
							document.myForm.appendChild(MenuTypeInput);
							document.myForm.submit();
							isSubmit = true;
						});
					
					
				}else{
					
					 bootbox
                     .confirm(
                       '${objMenuOptionLabel.getLblNo_menu_option_has_been_assigned_to_the_profile()}',
                       function(okOrCancel) {

                        if(okOrCancel == true)
                        {
					/* if(confirm('${objMenuOptionLabel.getLblNo_menu_option_has_been_assigned_to_the_profile()}')==true){
					 */
					 bootbox.alert('${objMenuOptionLabel.getLblON_UPDATE_SUCCESSFULLY()}',function(){
							
					 //alert('${objMenuOptionLabel.getLblON_UPDATE_SUCCESSFULLY()}');
					document.myForm.action = '${pageContext.request.contextPath}/MenuProfile_Maintenance_UpdateTable';
					//document.myForm.appendChild(hiddenInput);
					document.myForm.appendChild(MenuTypeInput);
					document.myForm.submit();
					isSubmit = true;
					
					 });
                       
						
					}else{
						isSubmit=false;
						
						return isSubmit;
					}
                       });
				}
                       
				

			} else {
				var menuOption = $('#MenuProfileID').val();
				
				var errMsgId = document.getElementById("Perror");
				$
						.post(
								"${pageContext.request.contextPath}/RestFull_Check_MenuProfile",
								{

									MenuOptionValue : menuOption
								},
								function(data, status) {

									if (data) {

										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblERR_MENU_PROFILE_ALREADY_USED()}';
										$('#ErrorMessage').show();
										isSubmit = false;

										return isSubmit;
									} else {
										$('#ErrorMessage').hide();
										var jsonData=$('#AssignList').val();
										if(jsonData.length>0 && jsonData.length!=2){
											 bootbox.alert('${objMenuOptionLabel.getLblON_SAVE_SUCCESSFULLY()}',function(){
													
											//alert('${objMenuOptionLabel.getLblON_SAVE_SUCCESSFULLY()}');
											document.myForm.action = '${pageContext.request.contextPath}/MenuProfile_Maintenance_UpdateTable';
											//alert('${objMenuOptionLabel.getLblON_SAVE_SUCCESSFULLY()}');
											document.myForm.submit();
											isSubmit = true;
											 });
										}else{
											
											
											 bootbox
						                     .confirm(
						                       '${objMenuOptionLabel.getLblNo_menu_option_has_been_assigned_to_the_profile()}',
						                       function(okOrCancel) {

						                        if(okOrCancel == true)
						                        {
											/* if(confirm('${objMenuOptionLabel.getLblNo_menu_option_has_been_assigned_to_the_profile()}')==true){
											 */	
												 bootbox.alert('${objMenuOptionLabel.getLblON_SAVE_SUCCESSFULLY()}',function(){
														
												//alert('${objMenuOptionLabel.getLblON_SAVE_SUCCESSFULLY()}');
												document.myForm.action = '${pageContext.request.contextPath}/MenuProfile_Maintenance_UpdateTable';
												//alert('${objMenuOptionLabel.getLblON_SAVE_SUCCESSFULLY()}');
												document.myForm.submit();
											isSubmit = true;
												 });
											
						                        }else{
												isSubmit=false;
											}
						                       });
										}
										//isSubmit = true;

										
										return isSubmit;
									}

								});
			}

		} else {

		}

		//document.location.href = '#top';
		window.scrollTo(0,0);
		return isSubmit;

	}

	
function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  
	  	
	  $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  }
	
	

	function isFieldBlank(fieldID){

		var fieldLength = document.getElementById(fieldId).value.trim().length;
		//var errMsgId=document.getElementById(errMsgId);


		if (fieldLength>0) {
			errMsgId.innerHTML = "";
			/*errMsgId.style.display = "none";*/
			return true;

		}else{
			
			return false;
		}
	}
	
	
	function dropDownMenuType() {

		var valMenuType = $('#MenuTypeID  option:selected').val();
		if(valMenuType == ""){
			$("#Application").attr('disabled', 'disabled');
		}else{
			$("#Application").removeAttr("disabled");
		}
		
		
		 var valMenuType = $('#MenuTypeID  option:selected').text();
			var selectedValue = $('#Application option:selected').val();
			var displayAll = "Display All";
			var screen = '${Menu_LookUp}';
		var MenuProfile=$('#MenuProfileID').val();
		var lastActivityTeamMember=$('#lastActivityTeamMemberID').val();
		var lastDate=$('#LastDateID').val();
		
		var descShort=$('#DescriptionShortID').val();
		var descLong=$('#DescriptionLongID').val();
		
		var helpLine=$('#HelpLineID').val();
		var AppIcon=$('#AppIconID').val();
		var editScreen = "${objIsEdit}";
		if (editScreen == "EditScreen") {
			if(screen=='true'){

				 var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
			 		+"&lblMenuProfile="+MenuProfile+"&lblPartOfMneuProfileDescription=test&Menu_LookUp=${Menu_LookUp}"
			 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
					+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
			 window.location=url;
			}else{
			var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
	 		+"&lblMenuProfile="+MenuProfile+"&Menu_LookUp=${Menu_LookUp}"
	 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
			+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
		 window.location=url;
			}
		}else{
		
			if(screen=='true'){

				 var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
			 		+"&lblMenuProfile="+MenuProfile+"&lblPartOfMneuProfileDescription=test&Menu_LookUp=${Menu_LookUp}"
			 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
					+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
			 window.location=url;
			}else{
			var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
	 		+"&lblMenuProfile="+MenuProfile+"&Menu_LookUp=${Menu_LookUp}"
	 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
			+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
		 window.location=url;
			}
		}
	
	}
	
	
	
	function dropDown() {

		
		 var valMenuType = $('#MenuTypeID  option:selected').text();
			var selectedValue = $('#Application option:selected').val();
			var displayAll = "Display All";
			var screen = '${Menu_LookUp}';
		var MenuProfile=$('#MenuProfileID').val();
		var lastActivityTeamMember=$('#lastActivityTeamMemberID').val();
		var lastDate=$('#LastDateID').val();
		
		var descShort=$('#DescriptionShortID').val();
		var descLong=$('#DescriptionLongID').val();
		
		var helpLine=$('#HelpLineID').val();
		var AppIcon=$('#AppIconID').val();
		var editScreen = "${objIsEdit}";
		if (editScreen == "EditScreen") {
			if(screen=='true'){

				 var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
			 		+"&lblMenuProfile="+MenuProfile+"&lblPartOfMneuProfileDescription=test&Menu_LookUp=${Menu_LookUp}"
			 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
					+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
			 window.location=url;
			}else{
			var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
	 		+"&lblMenuProfile="+MenuProfile+"&Menu_LookUp=${Menu_LookUp}"
	 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
			+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
		 window.location=url;
			}
		}else{
		
			if(screen=='true'){

				 var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
			 		+"&lblMenuProfile="+MenuProfile+"&lblPartOfMneuProfileDescription=test&Menu_LookUp=${Menu_LookUp}"
			 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
					+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
			 window.location=url;
			}else{
			var url="${pageContext.request.contextPath}/MenuProfileMaintenance?MenuType="+valMenuType+"&lblApplication="+selectedValue
	 		+"&lblMenuProfile="+MenuProfile+"&Menu_LookUp=${Menu_LookUp}"
	 		+"&lbllastActivityTeamMember="+lastActivityTeamMember+"&lbllastDate="+lastDate
			+"&lbldescShort="+descShort+"&lbldescLong="+descLong+"&lblhelpLine="+helpLine+"&lblAppIcon="+AppIcon+"&lblEdit=${objIsEdit}";
		 window.location=url;
			}
		}
	
	}

	
	function onSelectDropDownList(displayAll){
		
		var grid2 = "";
		var grid1 = "";
		var menuType=displayAll.toLowerCase();
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');
		var selectedValue = $('#MenuProfileID').val();
		 $.ajax({
		     url: '${pageContext.request.contextPath}/RestLoadEditGrid',
		     cache: false,
		     error: function() {
		        //$('#info').html('<p>An error has occurred</p>');
		     },
		     data:{ MenuOptionApplication : selectedValue
					 },
					 type: 'POST',
		     success: function(data) {
		

								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													
													selectable : "multiple",
													scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid2").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid2").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
													columns : [
															{
																field : "application",
																width : 90,
																title : "${objMenuOptionLabel.getLblApplication()}"
															},
															{
																field : "applicationSub",
																width : 90,
																title : "${objMenuOptionLabel.getLblSubApplication()}"
															},
															{
																field : "menuOption",
																width : 90,
																title : "${objMenuOptionLabel.getLblMenuOption()}"
															}

													]
												}).data("kendoGrid");
								var jsonVal = JSON
								.stringify(grid2.dataSource
										.data());
							$("#AssignList").val(
								jsonVal);

								if (data != "") {

									//var application = "Display All";

									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									
									



								}else if(menuType.length>0){
									
									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				 
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									 
									 
																			 
									
								}
								
								
								else{
									
									var data="[{}]";
									grid1 = $("#grid1")
									.kendoGrid(
											{
												dataSource : data,
												editable : "popup",
												selectable : "multiple",
												pageable : false,
												scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height:300,
												columns : [
														{
															field : "application",
															width : 90,
															title : "${objMenuOptionLabel.getLblApplication()}"
														},
														{
															field : "applicationSub",
															width : 90,
															title : "${objMenuOptionLabel.getLblSubApplication()}"
														},
														{
															field : "menuOption",
															width : 90,
															title : "${objMenuOptionLabel.getLblMenuOption()}"
														} ]
											})
									.data(
											"kendoGrid");
								}
							}
						});
		 
		 $("#copySelectedToGrid2")
			.on(
					"click",
					function(idx, elem) {

						moveTo(grid1, grid2);

						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});
	$("#copySelectedToGrid1").on("click",
			function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});

	$("#copySelectedToGridAll2")
			.on(
					"click",
					function(idx, elem) {
						moveToAll(grid1, grid2);
						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});

	$("#copySelectedToGridAll1").on("click",
			function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});
	

	}
		
		
	
	
	
	
	//set last date in last activity date according to formate
	function setDateLast() {

		var LastDatevalue = "${objHeader.getLastActivityDate()}";

		var res = LastDatevalue.split(" ");

		document.getElementById("LastDateID").value = res[0];

	}

	function moveTo(from, to) {
		var selected = from.select();
		if (selected.length > 0) {
			var items = [];
			$.each(selected, function(idx, elem) {
				items.push(from.dataItem(elem));
			});
			var fromDS = from.dataSource;
			var toDS = to.dataSource;
			$.each(items, function(idx, elem) {
				toDS.add($.extend({}, elem));
				fromDS.remove(elem);
			});
			toDS.sync();
			fromDS.sync();

		}
	}

	function moveToAll(from, to) {
		var varselected = from.dataSource;
		var allvalGrid = varselected.data();

		if (allvalGrid.length > 0) {
			var items = [];
			$.each(allvalGrid, function(idx, elem) {
				items.push(elem);
			});

			var fromDS = from.dataSource;
			var toDS = to.dataSource;
			$.each(items, function(idx, elem) {
				toDS.add($.extend({}, elem));
				fromDS.remove(elem);
			});
			toDS.sync();
			fromDS.sync();

		}
	}

	function DropDownSelected() {
		var menuTypeselect = "${objHeader.getMenuType()}".toUpperCase();

		var appIcon = "${objHeader.getAppIcon()}";

		if (menuTypeselect.trim() != '') {
			document.getElementById('MenuTypeID').value = menuTypeselect;
			

		}
		if (appIcon.trim() != '') {
			$("#receiveicon").show();
			document.getElementById('AppIconID').value = appIcon;
			jQuery("#receiveicon").attr('src', appIcon);
			//$("##ErrorMessageAppIcon1").hide();
			

		}
	  

		

	}

	//for edit to populate data in grid of tab
	function loadEditGridSelectionMobile(displayAll) {
		var grid2 = "";
		var grid1 = "";
		var menuType=displayAll.toLowerCase();
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');
		//var selectedValue = "${objHeader.getMenuProfile()}";
		 $.ajax({
		     url: '${pageContext.request.contextPath}/RestLoadEditGrid',
		     cache: false,
		     error: function() {
		        //$('#info').html('<p>An error has occurred</p>');
		     },
		     data:{ MenuOptionApplication : selectedValue
					 },
					 type: 'POST',
		     success: function(data) {
		

								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													
													selectable : "multiple",
													scrollable: true,
											    height: "496px",
											    
											    dataBound: function(e) {
											    	  $("#grid2").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid2").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
													columns : [
															{
																field : "application",
																width : 90,
																title : "${objMenuOptionLabel.getLblApplication()}"
															},
															{
																field : "applicationSub",
																width : 90,
																title : "${objMenuOptionLabel.getLblSubApplication()}"
															},
															{
																field : "menuOption",
																width : 90,
																title : "${objMenuOptionLabel.getLblMenuOption()}"
															}

													]
												}).data("kendoGrid");
								var jsonVal = JSON
								.stringify(grid2.dataSource
										.data());
							$("#AssignList").val(
								jsonVal);

								if (data != "") {

									var application = "Display All";

									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: true,
											    height: "496px",
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									
									



								}else if((menuType.length)>0){
									
									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: true,
											    height: "496px",
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				 
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									 
									 
										/*
									 */
									
								}
								
								
								else{
									
									var data="[{}]";
									grid1 = $("#grid1")
									.kendoGrid(
											{
												dataSource : data,
												editable : "popup",
												selectable : "multiple",
												pageable : false,
												scrollable: true,
											    height:"496px",
												columns : [
														{
															field : "application",
															width : 90,
															title : "${objMenuOptionLabel.getLblApplication()}"
														},
														{
															field : "applicationSub",
															width : 90,
															title : "${objMenuOptionLabel.getLblSubApplication()}"
														},
														{
															field : "menuOption",
															width : 90,
															title : "${objMenuOptionLabel.getLblMenuOption()}"
														} ]
											})
									.data(
											"kendoGrid");
								}
							}
						});
		 
		 $("#copySelectedToGrid2")
			.on(
					"click",
					function(idx, elem) {

						moveTo(grid1, grid2);

						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});
	$("#copySelectedToGrid1").on("click",
			function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});

	$("#copySelectedToGridAll2")
			.on(
					"click",
					function(idx, elem) {
						moveToAll(grid1, grid2);
						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});

	$("#copySelectedToGridAll1").on("click",
			function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});
	
	var jsonVal = JSON
	.stringify(grid2.dataSource
			.data());
$("#AssignList").val(
	jsonVal);
	}

	
	//for edit to populate data in grid
	function loadEditGrid(displayAll) {
		var grid2 = "";
		var grid1 = "";
		var menuType=displayAll.toLowerCase();
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');
		var selectedValue = $('#MenuProfileID').val();
		 $.ajax({
		     url: '${pageContext.request.contextPath}/RestLoadEditGrid',
		     cache: false,
		     error: function() {
		        //$('#info').html('<p>An error has occurred</p>');
		     },
		     data:{ MenuOptionApplication : selectedValue
					 },
					 type: 'POST',
		     success: function(data) {
		

								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													
													selectable : "multiple",
													scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid2").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid2").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
													columns : [
															{
																field : "application",
																width : 90,
																title : "${objMenuOptionLabel.getLblApplication()}"
															},
															{
																field : "applicationSub",
																width : 90,
																title : "${objMenuOptionLabel.getLblSubApplication()}"
															},
															{
																field : "menuOption",
																width : 90,
																title : "${objMenuOptionLabel.getLblMenuOption()}"
															}

													]
												}).data("kendoGrid");
								var jsonVal = JSON
								.stringify(grid2.dataSource
										.data());
							$("#AssignList").val(
								jsonVal);

								if (data != "") {

									var application = "Display All";

									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
											var selectedValue = $('#MenuProfileID').val();
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									
									



								}else if(menuType!="display all"){
									
									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
											var selectedValue = $('#MenuProfileID').val();
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				 
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									 
									 
																			 
									
								}
								
								
								else{
									
									var data="[{}]";
									grid1 = $("#grid1")
									.kendoGrid(
											{
												dataSource : data,
												editable : "popup",
												selectable : "multiple",
												pageable : false,
												scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height:300,
												columns : [
														{
															field : "application",
															width : 90,
															title : "${objMenuOptionLabel.getLblApplication()}"
														},
														{
															field : "applicationSub",
															width : 90,
															title : "${objMenuOptionLabel.getLblSubApplication()}"
														},
														{
															field : "menuOption",
															width : 90,
															title : "${objMenuOptionLabel.getLblMenuOption()}"
														} ]
											})
									.data(
											"kendoGrid");
								}
							}
						});
		 
		 $("#copySelectedToGrid2")
			.on(
					"click",
					function(idx, elem) {

						moveTo(grid1, grid2);

						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});
	$("#copySelectedToGrid1").on("click",
			function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});

	$("#copySelectedToGridAll2")
			.on(
					"click",
					function(idx, elem) {
						moveToAll(grid1, grid2);
						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});

	$("#copySelectedToGridAll1").on("click",
			function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});
	
	
	}

	
	function loadEditGridMobile(displayAll) {
		var grid2 = "";
		var grid1 = "";
		var menuType=displayAll.toLowerCase();
		//var selectedValue = "${objHeader.getMenuProfile()}";
		var selectedValue = "${objHeader.getMenuProfile()}";
		MenuProfileID
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');

		 $.ajax({
		     url: '${pageContext.request.contextPath}/RestLoadEditGrid',
		     cache: false,
		     error: function() {
		        //$('#info').html('<p>An error has occurred</p>');
		     },
		     data:{ MenuOptionApplication : selectedValue
					 },
					 type: 'POST',
		     success: function(data) {
		

								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													
													selectable : "multiple",
													scrollable: true,
											    height: 496,
											    
											    dataBound: function(e) {
											    	  $("#grid2").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid2").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
													columns : [
															{
																field : "application",
																width : 90,
																title : "${objMenuOptionLabel.getLblApplication()}"
															},
															{
																field : "applicationSub",
																width : 90,
																title : "${objMenuOptionLabel.getLblSubApplication()}"
															},
															{
																field : "menuOption",
																width : 90,
																title : "${objMenuOptionLabel.getLblMenuOption()}"
															}

													]
												}).data("kendoGrid");
								var jsonVal = JSON
								.stringify(grid2.dataSource
										.data());
							$("#AssignList").val(
								jsonVal);

								if (data != "") {

									var application = "Display All";

									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable:true,
											    height: "496px",
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									
									



								}else if(menuType!="display all"){
									
									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: true,
											    height: "496px",
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				 
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									 
									 
																			 
									
								}
								
								
								else{
									
									var data="[{}]";
									grid1 = $("#grid1")
									.kendoGrid(
											{
												dataSource : data,
												editable : "popup",
												selectable : "multiple",
												pageable : false,
												scrollable: true,
											    height:"496px",
												columns : [
														{
															field : "application",
															width : 90,
															title : "${objMenuOptionLabel.getLblApplication()}"
														},
														{
															field : "applicationSub",
															width : 90,
															title : "${objMenuOptionLabel.getLblSubApplication()}"
														},
														{
															field : "menuOption",
															width : 90,
															title : "${objMenuOptionLabel.getLblMenuOption()}"
														} ]
											})
									.data(
											"kendoGrid");
								}
							}
						});
		 
		 $("#copySelectedToGrid2")
			.on(
					"click",
					function(idx, elem) {

						moveTo(grid1, grid2);

						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});
	$("#copySelectedToGrid1").on("click",
			function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});

	$("#copySelectedToGridAll2")
			.on(
					"click",
					function(idx, elem) {
						moveToAll(grid1, grid2);
						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});

	$("#copySelectedToGridAll1").on("click",
			function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});
	
	
	}
	
	function loadGridMobile(displayAll) {
		var grid2 = "";
		var grid1 = "";
		var menuType=displayAll.toLowerCase();
		//var selectedValue = "${objHeader.getMenuProfile()}";
		var selectedValue = $('#MenuProfileID').val();
		
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');

		 $.ajax({
		     url: '${pageContext.request.contextPath}/RestLoadEditGrid',
		     cache: false,
		     error: function() {
		        //$('#info').html('<p>An error has occurred</p>');
		     },
		     
		     
		     
		     data:{ MenuOptionApplication : selectedValue
					 },
					 type: 'POST',
		     success: function(data) {
		

								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													
													selectable : "multiple",
													scrollable: true,
											    height: 496,
											    
											    dataBound: function(e) {
											    	  $("#grid2").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid2").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
													columns : [
															{
																field : "application",
																width : 90,
																title : "${objMenuOptionLabel.getLblApplication()}"
															},
															{
																field : "applicationSub",
																width : 90,
																title : "${objMenuOptionLabel.getLblSubApplication()}"
															},
															{
																field : "menuOption",
																width : 90,
																title : "${objMenuOptionLabel.getLblMenuOption()}"
															}

													]
												}).data("kendoGrid");
								var jsonVal = JSON
								.stringify(grid2.dataSource
										.data());
							$("#AssignList").val(
								jsonVal);

								if (data != "") {

									var application = "Display All";

									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									//var selectedValue = "${objHeader.getMenuProfile()}";
									var selectedValue = $('#MenuProfileID').val();
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable:true,
											    height: "496px",
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									
									



								}else if(menuType.lenght>0){
									
									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: true,
											    height: "496px",
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				 
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									 
									 
																			 
									
								}
								
								
								else{
									
									var data="[{}]";
									grid1 = $("#grid1")
									.kendoGrid(
											{
												dataSource : data,
												editable : "popup",
												selectable : "multiple",
												pageable : false,
												scrollable: true,
											    height:"496px",
												columns : [
														{
															field : "application",
															width : 90,
															title : "${objMenuOptionLabel.getLblApplication()}"
														},
														{
															field : "applicationSub",
															width : 90,
															title : "${objMenuOptionLabel.getLblSubApplication()}"
														},
														{
															field : "menuOption",
															width : 90,
															title : "${objMenuOptionLabel.getLblMenuOption()}"
														} ]
											})
									.data(
											"kendoGrid");
								}
							}
						});
		 
		 $("#copySelectedToGrid2")
			.on(
					"click",
					function(idx, elem) {

						moveTo(grid1, grid2);

						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});
	$("#copySelectedToGrid1").on("click",
			function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});

	$("#copySelectedToGridAll2")
			.on(
					"click",
					function(idx, elem) {
						moveToAll(grid1, grid2);
						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});

	$("#copySelectedToGridAll1").on("click",
			function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});
	
	
	}
	
	
	//for edit to populate data in grid
	function loadEditGridSelection(displayAll) {
		var grid2 = "";
		var grid1 = "";
		var menuType=displayAll.toLowerCase();
		//var selectedValue = "${objHeader.getMenuProfile()}";
		var selectedValue = $('#MenuProfileID').val();
		 $.ajax({
		     url: '${pageContext.request.contextPath}/RestLoadEditGrid',
		     cache: false,
		     error: function() {
		        //$('#info').html('<p>An error has occurred</p>');
		     },
		     data:{ MenuOptionApplication : selectedValue
					 },
					 type: 'POST',
		     success: function(data) {
		

								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													
													selectable : "multiple",
													scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid2").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid2").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
													columns : [
															{
																field : "application",
																width : 90,
																title : "${objMenuOptionLabel.getLblApplication()}"
															},
															{
																field : "applicationSub",
																width : 90,
																title : "${objMenuOptionLabel.getLblSubApplication()}"
															},
															{
																field : "menuOption",
																width : 90,
																title : "${objMenuOptionLabel.getLblMenuOption()}"
															}

													]
												}).data("kendoGrid");
								var jsonVal = JSON
								.stringify(grid2.dataSource
										.data());
							$("#AssignList").val(
								jsonVal);

								if (data != "") {

									var application = "Display All";

									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objHeader.getMenuProfile()}";
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									
									



								}else if((menuType.length)>0){
									
									if (displayAll.trim() != '') {

										document.getElementById('Application').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
											var selectedValue = $('#MenuProfileID').val();
									
									 var valMenuType = $('#MenuTypeID  option:selected').text();
									 $.ajax({
									     url: '${pageContext.request.contextPath}/MenuOptionList_Edit',
									     cache: false,
									     type: 'POST',
									     error: function() {
									        //$('#info').html('<p>An error has occurred</p>');
									     },
									     data:{ MenuOptionApplication : selectedValue,
												MenuOptionDisplayAll : displayAll,
												MenuType : valMenuType },
									     success: function(data) {
									
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																					
																			
																				
																				editable : "popup",
																				selectable : "multiple",
																					scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height: 300,
											    
											    dataBound: function(e) {
											    	  $("#grid1").find("tr").kendoTooltip({
											    		  filter: "td:nth-child(3)",
											    	   content:  function(e){
											    	        var dataItem = $("#grid1").data("kendoGrid").dataItem(e.target.closest("tr"));
											    	        var content =dataItem.menuOption;
											    	        return content;
											    	       },
											    	  
											    	   position: "bottom",
											    	  });
											    	 },
																				 
																				columns : [
																						{
																							field : "application",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblApplication()}"
																						},
																						{
																							field : "applicationSub",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblSubApplication()}"
																						},
																						{
																							field : "menuOption",
																							width : 90,
																							title : "${objMenuOptionLabel.getLblMenuOption()}"
																						} ]
																			})
																	.data(
																			"kendoGrid");
														}

													});
									 
									 
										/*
									 */
									
								}
								
								
								else{
									
									var data="[{}]";
									grid1 = $("#grid1")
									.kendoGrid(
											{
												dataSource : data,
												editable : "popup",
												selectable : "multiple",
												pageable : false,
												scrollable: {
													virtual: true,
													horizontal:false
											    },
											    height:300,
												columns : [
														{
															field : "application",
															width : 90,
															title : "${objMenuOptionLabel.getLblApplication()}"
														},
														{
															field : "applicationSub",
															width : 90,
															title : "${objMenuOptionLabel.getLblSubApplication()}"
														},
														{
															field : "menuOption",
															width : 90,
															title : "${objMenuOptionLabel.getLblMenuOption()}"
														} ]
											})
									.data(
											"kendoGrid");
								}
							}
						});
		 
		 $("#copySelectedToGrid2")
			.on(
					"click",
					function(idx, elem) {

						moveTo(grid1, grid2);

						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});
	$("#copySelectedToGrid1").on("click",
			function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});

	$("#copySelectedToGridAll2")
			.on(
					"click",
					function(idx, elem) {
						moveToAll(grid1, grid2);
						var jsonVal = JSON
								.stringify(grid2.dataSource
										.data())
						$("#AssignList").val(
								jsonVal);

						return false;
					});

	$("#copySelectedToGridAll1").on("click",
			function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				return false;
			});
	
	var jsonVal = JSON
	.stringify(grid2.dataSource
			.data());
$("#AssignList").val(
	jsonVal);
	}

	
	
	//for reset and back screen
	function cancelButton() {

		//window.history.back();
		var screen = '${Menu_LookUp}';
		var editScreen = "${objIsEdit}";
		//if (editScreen == "EditScreen") {
			if (screen == 'true') {

				
				window.location.href = 'MenuProfileMaintenanceLookUp';

				//document.getElementById("myForm").reset();

			} else {

				window.location.href = 'Menu_Profile_searchLookup';				

			}
		// } 
		//else {
			 /*
			window.location.reload();

		} */
	}
	
	
	function DownSelectedChange(){
		
		var valMenuType = $('#MenuTypeID  option:selected').val();
		if(valMenuType == ""){
			$("#Application").attr('disabled', 'disabled');
		}else{
			$("#Application").removeAttr("disabled");
		}
		var editScreen = "${objIsEdit}";
		var displayAll = $('#Application option:selected').val();
	//	var selectedValue = "${objApplication}";
	if (editScreen == "EditScreen") {
		//var displayAll = "Display All";
		if("${objDeviceType}"!="MOBILE"){
			loadEditGrid(displayAll);
			}else{
			loadEditGridMobile(displayAll);
			}
	}else{
		if("${objDeviceType}"!="MOBILE"){
			onSelectDropDownList(displayAll);
			}else{
			loadEditMobile(displayAll);
			}
		
	}
		
	}

	$(document).ready(function() {
		$("#receiveicon").hide();
		setDateLast();
		//checkEditORSave();
		var editScreen = "${objIsEdit}";
		var onChangeSelectedDropDown='${objChangeDropDown}';
		//if(onChangeSelectedDropDown=='true'){
		
		if (editScreen == "EditScreen") {
			$('#LastDateID').val(ConvertUTCToLocalTimeZone('${objHeader.getLastActivityDate()}'));
				$('#LastDate').val(getUTCDateTime());
		//	$('#MenuProfileID').attr('readonly', 'true');
		$('#MenuProfileID').attr('disabled', 'disabled');
			DropDownSelected();
			$('#MenuTypeID').attr('disabled', 'disabled');
			ChangeImage();
			var displayAll = "Display All";
			if("${objDeviceType}"!="MOBILE"){
			loadEditGrid(displayAll);
			}else{
			loadEditGridMobile(displayAll);
			}
		} else {
			
			$('#LastDateID').val(getLocalDate());
			$('#LastDate').val(getUTCDateTime());
			
			$("#Application").attr('disabled', 'disabled');
			$('#MenuProfileIDH').attr('disabled', 'disabled');
			var dataVal = "";
			
			var dataVal = "";

			var grid2 = $("#grid2").kendoGrid({
				dataSource : dataVal,
				editable : "popup",
				selectable : "multiple",
				pageable : false,
				columns : [ {
					field : "application",
					width : 90,
					title : "${objMenuOptionLabel.getLblApplication()}"
				}, {
					field : "applicationSub",
					width : 90,
					title : "${objMenuOptionLabel.getLblSubApplication()}"
				}, {
					field : "menuOption",
					width : 90,
					title : "${objMenuOptionLabel.getLblMenuOption()}"
				}

				]
			}).data("kendoGrid");

			var grid1 = $("#grid1").kendoGrid({
				dataSource : dataVal,
				editable : "popup",
				selectable : "multiple",
				pageable : false,
				columns : [ {
					field : "application",
					width : 90,
					title : "${objMenuOptionLabel.getLblApplication()}"
				}, {
					field : "applicationSub",
					width : 90,
					title : "${objMenuOptionLabel.getLblSubApplication()}"
				}, {
					field : "menuOption",
					width : 90,
					title : "${objMenuOptionLabel.getLblMenuOption()}"
				} ]
			}).data("kendoGrid");

			
			$("#copySelectedToGrid2").on("click", function(idx, elem) {

				moveTo(grid1, grid2);

				var jsonVal = JSON.stringify(grid2.dataSource.data())
				$("#AssignList").val(jsonVal);

				//return false;
			},false);
			$("#copySelectedToGrid1").on("click", function(idx, elem) {
				moveTo(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				//return false;
			},false);

			$("#copySelectedToGridAll2").on("click", function(idx, elem) {
				moveToAll(grid1, grid2);
				var jsonVal = JSON.stringify(grid2.dataSource.data())
				$("#AssignList").val(jsonVal);

				//return false;
			},false);

			$("#copySelectedToGridAll1").on("click", function(idx, elem) {
				moveToAll(grid2, grid1);
				var jsonVal = JSON
				.stringify(grid2.dataSource
						.data())
				$("#AssignList").val(jsonVal);
				//return false;
			},false);
		 }
			
		
		/*}else{ */
			
			
			/* if (editScreen == "EditScreen") {
				$('#LastDateID').val(ConvertUTCToLocalTimeZone('${objHeader.getLastActivityDate()}'));
				$('#LastDate').val(getUTCDateTime());
				$('#MenuProfileID').attr('disabled', 'disabled');
				DropDownSelected();
				$('#MenuTypeID').attr('disabled', 'disabled');
				ChangeImage();
				var selectedValue = "${objApplication}";
				if("${objDeviceType}"!="MOBILE"){
				loadEditGridSelection(selectedValue);
				}else{
				loadEditGridSelectionMobile(selectedValue);
				}
			}else{
				$('#LastDateID').val(getLocalDate());
				$('#LastDate').val(getUTCDateTime());
				
				DropDownSelected();
				ChangeImage();
				onSelectDropDownList();
			} */
		//}
		
		setInterval(function() {

			var h = window.innerHeight;
			if (window.innerHeight == 928) {
				h = h - 187;

			} else {
				h = h - 239;
			}
			document.getElementById("page-content").style.minHeight = h + "px";

		}, 30);

		// initiate layout and plugins
		//Metronic.init(); // init metronic core components
		//Layout.init(); // init current layout
		//Demo.init(); // init demo features
		//UINestable.init();
	});
	
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'MenuProfileMaintenance',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }	
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>