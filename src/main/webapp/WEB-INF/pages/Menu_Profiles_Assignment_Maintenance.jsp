<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html lang="en" class="no-js">

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<link
			href="<c:url value="/resourcesValidate/css/MenuProfileAssigment.css"/>"
			rel="stylesheet" type="text/css">
		<style>
.k-virtual-scrollable-wrap {
	height: 100%;
	overflow-x: hidden;
}

.k-grid th.k-header, .k-grid-header {
	white-space: normal !important;
	padding-right: 13px !important;
}


@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape){
.k-grid th.k-header, .k-grid-header {
white-space: normal !important;
padding-right: 0px !important;
}
}

@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait)
{

.k-grid th.k-header, .k-grid-header {
white-space: normal !important;
padding-right: 0px !important;
}

}

</style>
		<body>

			<!-- BEGIN PAGE CONTAINER -->
			<div class="page-container">
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<div class="container">
						<!-- BEGIN PAGE TITLE -->
						<div class="page-title">
							<h1>${objMenuAssignmentLBL.getLbl_Menu_Profiles_Assignment_Maintenance()}</h1>
						</div>
						<!-- END PAGE TITLE -->

					</div>
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">
						<form:form name="myForm" action="#"
							onsubmit="return isformSubmit();"
							modelAttribute="objMenuProfileAssignment" method="post">
							<!-- BEGIN PAGE CONTENT INNER -->
							<div class="row margin-top-10">
								<div class="col-md-12">
									<div class="portlet_new">


										<div class="form-body">

											<div class="form-group" style="margin-left: -15px;">
												<label class="col-md-2 control-label"
													style="font-size: 16px; width: 100%;"><b><span>${objMenuAssignmentLBL.getLbl_Team_Member()}:</span>&nbsp;&nbsp;${objMenuProfileAssignmentPojo.getName()}</b>
												</label>

											</div>
											<br>
											<br>
											<br>

										</div>

										<input type="hidden" class="form-controlwidth"
											id="MenuAssignmentJson" name="MenuAssignmentJson" />
										<input type="hidden" class="form-controlwidth"
											id="lblTeamMemberID" name="lblTeamMemberID"
											value="${objTeamMemberID}" />
										<div class="portlet-body">
											<div class="tabbable">
												<input type="hidden" class="form-controlwidth"
													name="LastActivityDateH" id="LastDate">
												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">


															<div class="form-group" style="width: auto;">
																<label class="col-md-2 control-label" id="SelectLabelID">${objMenuAssignmentLBL.getLbl_Select_Menu_Type()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="lblMenuTypeID" id="lblMenuTypeID"
																		onchange="loadDataOnChange();">
																		<option value="">${objMenuAssignmentLBL.getLbl_Select()}...</option>
																		<c:if test="${not empty objMenuTypes}">
																			<c:forEach var="ApplicationList"
																				items="${objMenuTypes}">

																				<option
																					value="${ApplicationList.getGeneralCode()}">${ApplicationList.getDescription20()}</option>
																			</c:forEach>
																			<option value="Display All">Display All</option>
																		</c:if>



																	</select>
																	<!-- 				
												<span style="font-size: 14px;
font-weight: 600;
margin-left: 248px;
position: absolute;
top: 8px;">OR</span>
											<span style="margin-left: 280px;
position: absolute;
top: 4px;"><button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>Display All</button></span>
												 -->
																</div>



															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="portlet box blue" style="border: 1px solid #EFF2F7;">
									<div class="portlet-title" id="MenuOptionlblID">
										<div class="caption" id="lblMenuPRofileOptionDiv">
											${objMenuAssignmentLBL.getLbl_Available_Menu_Profiles()}</div>

									</div>
									<div class="portlet-title" id="MenuProfileOptionlblID">
										<div class="caption" id="lblMenuPRofileOptionDiv">
											${objMenuAssignmentLBL.getLbl_Menu_Profiles_Assigned()}</div>

									</div>
								</div>
								<!-- for processing box  -->
								<div id="panels">

									<div id="grid1"></div>
									<div id="commands">
										<div>
											<a href="" id="copySelectedToGrid2" type="button"
												class="k-button">&gt;</a>
										</div>
										<div>
											<a href="" id="copySelectedToGrid1" type="button"
												class="k-button">&lt;</a>
										</div>
										<div>
											<a href="" id="copySelectedToGridAll2" type="button"
												class="k-button">&gt;&gt;</a>
										</div>
										<div>
											<a href="" id="copySelectedToGridAll1" type="button"
												class="k-button">&lt;&lt;</a>
										</div>
									</div>
									<div id="grid2"></div>
								</div>



								<!-- END SAMPLE TABLE PORTLET-->
								<div
									class="margin-bottom-5-right-allign_menu_profile_assignment_display">
									<button class="btn btn-sm yellow filter-submit margin-bottom">
										<i class="fa fa-check"></i>
										${objMenuAssignmentLBL.getLbl_Save_Update()}
									</button>
									<button type="button" onclick="return resetPage();" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> ${objMenuAssignmentLBL.getLblReset()}</button>
									<button type="button" onclick="cancelButton();"
										class="btn btn-sm red filter-cancel">
										<i class="fa fa-times"></i>
										${objMenuAssignmentLBL.getLbl_Cancel()}
									</button>
								</div>

							</div>

							<!--<div class="margin-bottom-5-right-allign_security_team">
											<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i> Reselect</button>
										    <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>Sort First Name</button></div>-->
							<!-- END PAGE CONTENT INNER -->
						</form:form>
					</div>


				</div>

				<!-- END PAGE CONTENT -->
			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />

<script>

function resetPage(){
    
    window.parent.location = window.parent.location.href;
   }

	//for reset and back screen
	function cancelButton() {

		//window.history.back();
		var screen = '${Menu_LookUp}';

		if (screen == 'true') {

			window.location.href = 'MenuProfileAssignment_SearchLookup';

			//document.getElementById("myForm").reset();

		} else {

			window.location.href = 'MenuProfileAssigmentLookUp';

		}

	}

	function isformSubmit() {

		var isSubmit = false;

		var isAssigned = $("#MenuAssignmentJson").val();

		if (isAssigned.length > 0 && isAssigned.length != 2) {
			
			 bootbox.alert('${objMenuAssignmentLBL.getLbl_Your_record_has_been_updated_successfully()}',function(){
				 document.myForm.action = '${pageContext.request.contextPath}/MenuProfileAssignment_SaveAndUpdate';
					document.myForm.submit();
					isSubmit = true;
				});
			//alert('${objMenuAssignmentLBL.getLbl_Your_record_has_been_updated_successfully()}');

			
		} else {
			/* if (confirm('${objMenuAssignmentLBL.getLbl_No_menu_profile_has_been_assigned_to_the_TeamMember()}') == true) {
			 */	
			 bootbox
             .confirm(
               '${objMenuAssignmentLBL.getLbl_No_menu_profile_has_been_assigned_to_the_TeamMember()}',
               function(okOrCancel) {

                if(okOrCancel == true)
                {
				 bootbox.alert('${objMenuAssignmentLBL.getLbl_Your_record_has_been_updated_successfully()}',function(){
						
				document.myForm.action = '${pageContext.request.contextPath}/MenuProfileAssignment_SaveAndUpdate';
				document.myForm.submit();
				//alert('${objMenuAssignmentLBL.getLbl_Your_record_has_been_updated_successfully()}');

				isSubmit = true;
				 });
			} else {
				isSubmit = false;
			}
               });

		}

		//document.location.href = '#top';
		window.scrollTo(0,0);
		return isSubmit;
	}

	function onSelectedChangedValueData() {

		var grid2 = "";
		var grid1 = "";
		var selectedValue = $('#lblMenuTypeID option:selected').val();
		//var displayAll = "${objMenuOptionLabel.getLbldisplayall()}";

		if (selectedValue == "") {

		} else {
			$('#lblMenuTypeID').val(selectedValue);
			$
					.ajax({
						url : '${pageContext.request.contextPath}/RestFull_MenuProfile_fromMenuHeader',
						cache : false,
						data : {
							MenuType : selectedValue
						},
						error : function() {
							//$('#info').html('<p>An error has occurred</p>');
						},

						success : function(data) {
							isRefresh = true;
							// $("#grid1").data("kendoGrid").dataSource.empty();
							grid1 = $("#grid1")
									.kendoGrid(
											{
												dataSource : data,

												selectable : "multiple",
												pageable : false,
												async : true,
												cache : false,
												columns : [
														{
															field : "profile",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
														},
														{
															field : "menuType",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
														},
														{
															field : "description50",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_Description()}"
														}

												]
											}).data("kendoGrid");

							var toDataSourse = "[{}]";
							//$("#grid2").data("kendoGrid").dataSource.empty();
							grid2 = $("#grid2")
									.kendoGrid(
											{
												dataSource : toDataSourse,

												selectable : "multiple",
												pageable : false,
												cache : false,
												async : true,
												columns : [
														{
															field : "profile",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
														},
														{
															field : "menuType",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
														},
														{
															field : "description50",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_Description()}"
														}

												]
											}).data("kendoGrid");

							$("#copySelectedToGrid2").on(
									"click",
									function(idx, elem) {

										moveTo(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										isRefresh = false;
										return false;
									});
							$("#copySelectedToGrid1").on(
									"click",
									function(idx, elem) {
										moveTo(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										isRefresh = false;
										return false;
									});

							$("#copySelectedToGridAll2").on(
									"click",
									function(idx, elem) {
										moveToAll(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});

							$("#copySelectedToGridAll1").on(
									"click",
									function(idx, elem) {
										moveToAll(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});
						},
						type : 'POST'
					});
		}
	}

	//on change drop down
	function dropDown() {
		var selectedValue = $('#lblMenuTypeID option:selected').val();

		var changeStatus = '${Menu_LookUp}';
		var blank = "test";
		if (changeStatus == 'true') {
			var url = "${pageContext.request.contextPath}/MenuProfileAssigmentMaintenance?MenuType="
					+ selectedValue
					+ "&lblTeamMemberID=${objLblTeamId}&Menu_LookUp=${Menu_LookUp}";
			window.location = url;
		} else {

			var url = "${pageContext.request.contextPath}/MenuProfileAssigmentMaintenance?MenuType="
					+ selectedValue
					+ "&lblTeamMemberID=${objLblTeamId}&lblPartOfTeamMemberNameID="
					+ blank + "&Menu_LookUp=${Menu_LookUp}";
			window.location = url;
		}

	}

	//for edit to populate data in grid
	function loadEditGrid(displayAll) {
		var grid2 = null;
		var grid1 = null;
		var menutype = "";
		try {
			menutype = displayAll.toLowerCase();
		} catch (err) {
			menutype = "";
		}
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');
		var selectedValue = "${objTeamMemberID}";
		$
				.post(
						"${pageContext.request.contextPath}/RestFull_AssignedMenuProfile",
						{
							TeamMemberID : selectedValue

						},
						function(data, status) {

							if (data) {
								isRefresh = true;
								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													selectable : "multiple",
													pageable : false,
													cache : false,
													scrollable : {
														virtual : true,
														horizontal : false
													},
													height : 300,

													dataBound : function(e) {
														$("#grid2")
																.find("tr")
																.kendoTooltip(
																		{
																			filter : "td:nth-child(3)",
																			content : function(
																					e) {
																				var dataItem = $(
																						"#grid2")
																						.data(
																								"kendoGrid")
																						.dataItem(
																								e.target
																										.closest("tr"));
																				var content = dataItem.help_LINE
																						+ ' '
																						+ dataItem.description50;
																				return content;
																			},

																			position : "bottom",
																		});
													},
													columns : [
															{
																field : "profile",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
															},
															{
																field : "menuType",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
															},
															{
																field : "description50",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Description()}"
															}

													]
												}).data("kendoGrid");

								if (data != "") {

									//var application = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									if (displayAll.trim() != '') {

										document
												.getElementById('lblMenuTypeID').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objTeamMemberID}";
									//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									$
											.post(
													"${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader",
													{
														MenuType : displayAll,
														SelectedTeamMember : selectedValue
													},
													function(data, status) {

														if (data) {
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																				editable : "popup",
																				selectable : "multiple",
																				pageable : false,
																				cache : false,
																				scrollable : {
																					virtual : true,
																					horizontal : false
																				},
																				height : 300,
																				dataBound : function(
																						e) {

																					$(
																							"#grid1")
																							.find(
																									"tr")
																							.kendoTooltip(
																									{
																										filter : "td:nth-child(3)",
																										content : function(
																												e) {
																											var dataItem = $(
																													"#grid1")
																													.data(
																															"kendoGrid")
																													.dataItem(
																															e.target
																																	.closest("tr"));
																											var content = dataItem.help_LINE
																													+ ' '
																													+ dataItem.description50;
																											return content;
																										},

																										position : "bottom",
																									});
																				},
																				columns : [
																						{
																							field : "profile",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																						},
																						{
																							field : "menuType",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																						},
																						{
																							field : "description50",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Description()}"
																						}

																				]
																			})
																	.data(
																			"kendoGrid");
														}

													});

								} else if (menutype != "display all") {

									if (displayAll.trim() != '') {

										document
												.getElementById('lblMenuTypeID').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objTeamMemberID}";
									//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									$
											.post(
													"${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader",
													{
														MenuType : displayAll,
														SelectedTeamMember : selectedValue
													},
													function(data, status) {

														if (data) {
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																				editable : "popup",
																				selectable : "multiple",
																				pageable : false,
																				cache : false,
																				scrollable : {
																					virtual : true,
																					horizontal : false
																				},
																				height : 300,
																				dataBound : function(
																						e) {
																					$(
																							"#grid1")
																							.find(
																									"tr")
																							.kendoTooltip(
																									{
																										filter : "td:nth-child(3)",
																										content : function(
																												e) {
																											var dataItem = $(
																													"#grid1")
																													.data(
																															"kendoGrid")
																													.dataItem(
																															e.target
																																	.closest("tr"));
																											var content = dataItem.help_LINE
																													+ ' '
																													+ dataItem.description50;
																											return content;
																										},

																										position : "bottom",
																									});
																				},
																				columns : [
																						{
																							field : "profile",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																						},
																						{
																							field : "menuType",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																						},
																						{
																							field : "description50",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Description()}"
																						}

																				]
																			})
																	.data(
																			"kendoGrid");
														}

													});

								} else {

									var data = "[{}]";
									grid1 = $("#grid1")
											.kendoGrid(
													{
														dataSource : data,
														editable : "popup",
														selectable : "multiple",
														pageable : false,
														cache : false,
														scrollable : {
															virtual : true,
															horizontal : false
														},
														columns : [
																{
																	field : "profile",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																},
																{
																	field : "menuType",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																},
																{
																	field : "description50",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_Description()}"
																}

														]
													}).data("kendoGrid");
								}

							}
							var jsonVal = JSON.stringify(grid2.dataSource
									.data())
							$("#MenuAssignmentJson").val(jsonVal);
							$("#copySelectedToGrid2").on(
									"click",
									function(idx, elem) {

										moveTo(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);

										return false;
									});
							$("#copySelectedToGrid1").on(
									"click",
									function(idx, elem) {
										moveTo(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});

							$("#copySelectedToGridAll2").on(
									"click",
									function(idx, elem) {
										moveToAll(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);

										return false;
									});

							$("#copySelectedToGridAll1").on(
									"click",
									function(idx, elem) {
										moveToAll(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});

						});
	}

	//for edit to populate data in grid
	function loadEditGridMobile(displayAll) {
		var grid2 = null;
		var grid1 = null;
		var menutype = displayAll.toLowerCase();
		var isRefresh = false;
		var selectedValue = "${objTeamMemberID}";
		$
				.post(
						"${pageContext.request.contextPath}/RestFull_AssignedMenuProfile",
						{
							TeamMemberID : selectedValue

						},
						function(data, status) {

							if (data) {
								isRefresh = true;
								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													selectable : "multiple",
													pageable : false,
													cache : false,
													scrollable : true,
													height : 300,

													dataBound : function(e) {
														$("#grid2")
																.find("tr")
																.kendoTooltip(
																		{
																			filter : "td:nth-child(3)",
																			content : function(
																					e) {
																				var dataItem = $(
																						"#grid2")
																						.data(
																								"kendoGrid")
																						.dataItem(
																								e.target
																										.closest("tr"));
																				var content = dataItem.help_LINE
																						+ ' '
																						+ dataItem.description50;
																				return content;
																			},

																			position : "bottom",
																		});
													},
													columns : [
															{
																field : "profile",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
															},
															{
																field : "menuType",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
															},
															{
																field : "description50",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Description()}"
															}

													]
												}).data("kendoGrid");

								if (data != "") {

									//var application = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									if (displayAll.trim() != '') {

										document
												.getElementById('lblMenuTypeID').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objTeamMemberID}";
									//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									$
											.post(
													"${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader",
													{
														MenuType : displayAll,
														SelectedTeamMember : selectedValue
													},
													function(data, status) {

														if (data) {
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																				editable : "popup",
																				selectable : "multiple",
																				pageable : false,
																				cache : false,
																				scrollable : true,
																				height : 300,
																				dataBound : function(
																						e) {

																					$(
																							"#grid1")
																							.find(
																									"tr")
																							.kendoTooltip(
																									{
																										filter : "td:nth-child(3)",
																										content : function(
																												e) {
																											var dataItem = $(
																													"#grid1")
																													.data(
																															"kendoGrid")
																													.dataItem(
																															e.target
																																	.closest("tr"));
																											var content = dataItem.help_LINE
																													+ ' '
																													+ dataItem.description50;
																											return content;
																										},

																										position : "bottom",
																									});
																				},
																				columns : [
																						{
																							field : "profile",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																						},
																						{
																							field : "menuType",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																						},
																						{
																							field : "description50",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Description()}"
																						}

																				]
																			})
																	.data(
																			"kendoGrid");
														}

													});

								} else if (menutype != "display all") {

									if (displayAll.trim() != '') {

										document
												.getElementById('lblMenuTypeID').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objTeamMemberID}";
									//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									$
											.post(
													"${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader",
													{
														MenuType : displayAll,
														SelectedTeamMember : selectedValue
													},
													function(data, status) {

														if (data) {
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																				editable : "popup",
																				selectable : "multiple",
																				pageable : false,
																				cache : false,
																				scrollable : true,
																				height : 300,
																				dataBound : function(
																						e) {
																					$(
																							"#grid1")
																							.find(
																									"tr")
																							.kendoTooltip(
																									{
																										filter : "td:nth-child(3)",
																										content : function(
																												e) {
																											var dataItem = $(
																													"#grid1")
																													.data(
																															"kendoGrid")
																													.dataItem(
																															e.target
																																	.closest("tr"));
																											var content = dataItem.help_LINE
																													+ ' '
																													+ dataItem.description50;
																											return content;
																										},

																										position : "bottom",
																									});
																				},
																				columns : [
																						{
																							field : "profile",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																						},
																						{
																							field : "menuType",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																						},
																						{
																							field : "description50",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Description()}"
																						}

																				]
																			})
																	.data(
																			"kendoGrid");
														}

													});

								} else {

									var data = "[{}]";
									grid1 = $("#grid1")
											.kendoGrid(
													{
														dataSource : data,
														editable : "popup",
														selectable : "multiple",
														pageable : false,
														cache : false,
														scrollable : true,
														columns : [
																{
																	field : "profile",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																},
																{
																	field : "menuType",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																},
																{
																	field : "description50",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_Description()}"
																}

														]
													}).data("kendoGrid");
								}

							}
							var jsonVal = JSON.stringify(grid2.dataSource
									.data())
							$("#MenuAssignmentJson").val(jsonVal);
							$("#copySelectedToGrid2").on(
									"click",
									function(idx, elem) {

										moveTo(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);

										return false;
									});
							$("#copySelectedToGrid1").on(
									"click",
									function(idx, elem) {
										moveTo(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});

							$("#copySelectedToGridAll2").on(
									"click",
									function(idx, elem) {
										moveToAll(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);

										return false;
									});

							$("#copySelectedToGridAll1").on(
									"click",
									function(idx, elem) {
										moveToAll(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});

						});
	}

	//for edit to populate data in grid
	function loadGridonSelection(displayAll) {
		var grid2 = null;
		var grid1 = null;
		var menutype = "";
		try {
			menutype = displayAll.toLowerCase();
		} catch (err) {
			menutype = "";
		}
		var isRefresh = false;
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');
		var selectedValue = "${objTeamMemberID}";
		$
				.ajax({
					url : '${pageContext.request.contextPath}/RestFull_AssignedMenuProfile',
					cache : false,
					error : function() {
						//$('#info').html('<p>An error has occurred</p>');
					},
					data : {
						TeamMemberID : selectedValue

					},
					type : 'POST',
					success : function(data) {

						if (data) {

							grid2 = $("#grid2")
									.kendoGrid(
											{
												dataSource : data,
												editable : "popup",
												selectable : "multiple",
												pageable : false,
												cache : false,
												scrollable : {
													virtual : true,
													horizontal : false
												},
												height : 300,
												dataBound : function(e) {
													$("#grid2")
															.find("tr")
															.kendoTooltip(
																	{
																		filter : "td:nth-child(3)",
																		content : function(
																				e) {

																			var dataItem = $(
																					"#grid2")
																					.data(
																							"kendoGrid")
																					.dataItem(
																							e.target
																									.closest("tr"));
																			var content = dataItem.help_LINE
																					+ ' '
																					+ dataItem.description50;
																			return content;
																		},

																		position : "bottom",
																	});
												},
												columns : [
														{
															field : "profile",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
														},
														{
															field : "menuType",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
														},
														{
															field : "description50",
															width : 90,
															title : "${objMenuAssignmentLBL.getLbl_Description()}"
														}

												]
											}).data("kendoGrid");

							if (data != "") {

								//var application = "${objMenuAssignmentLBL.getLbl_Display_All()}";

								if (displayAll.trim() != '') {

									document.getElementById('lblMenuTypeID').value = displayAll;

								}
								/* var selectedValue = $(
										'#Application option:selected')
										.val(); */
								var selectedValue = "${objTeamMemberID}";
								//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

								$
										.ajax({
											url : '${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader',
											cache : false,
											error : function() {
												//$('#info').html('<p>An error has occurred</p>');
											},
											data : {
												MenuType : displayAll,
												SelectedTeamMember : selectedValue
											},
											type : 'POST',
											success : function(data) {

												if (data) {
													grid1 = $("#grid1")
															.kendoGrid(
																	{
																		dataSource : data,
																		editable : "popup",
																		selectable : "multiple",
																		pageable : false,
																		cache : false,
																		scrollable : {
																			virtual : true,
																			horizontal : false
																		},
																		height : 300,
																		dataBound : function(
																				e) {
																			$(
																					"#grid1")
																					.find(
																							"tr")
																					.kendoTooltip(
																							{
																								filter : "td:nth-child(3)",
																								content : function(
																										e) {
																									var dataItem = $(
																											"#grid1")
																											.data(
																													"kendoGrid")
																											.dataItem(
																													e.target
																															.closest("tr"));
																									var content = dataItem.help_LINE
																											+ ' '
																											+ dataItem.description50;
																									return content;
																								},

																								position : "bottom",
																							});
																		},
																		columns : [
																				{
																					field : "profile",
																					width : 90,
																					title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																				},
																				{
																					field : "menuType",
																					width : 90,
																					title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																				},
																				{
																					field : "description50",
																					width : 90,
																					title : "${objMenuAssignmentLBL.getLbl_Description()}"
																				}

																		]
																	})
															.data("kendoGrid");
												}
											}
										});

							} else if (menutype.length > 0) {

								if (displayAll.trim() != '') {

									document.getElementById('lblMenuTypeID').value = displayAll;

								}
								/* var selectedValue = $(
										'#Application option:selected')
										.val(); */
								var selectedValue = "${objTeamMemberID}";
								//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

								$
										.ajax({
											url : '${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader',
											cache : false,
											error : function() {
												//$('#info').html('<p>An error has occurred</p>');
											},
											type : 'POST',
											data : {
												MenuType : displayAll,
												SelectedTeamMember : selectedValue
											},
											success : function(data) {

												if (data) {
													grid1 = $("#grid1")
															.kendoGrid(
																	{
																		dataSource : data,
																		editable : "popup",
																		selectable : "multiple",
																		pageable : false,
																		cache : false,
																		scrollable : {
																			virtual : true,
																			horizontal : false
																		},
																		height : 300,
																		dataBound : function(
																				e) {
																			$(
																					"#grid1")
																					.find(
																							"tr")
																					.kendoTooltip(
																							{
																								filter : "td:nth-child(3)",
																								content : function(
																										e) {
																									var dataItem = $(
																											"#grid1")
																											.data(
																													"kendoGrid")
																											.dataItem(
																													e.target
																															.closest("tr"));

																									var content = dataItem.help_LINE
																											+ ' '
																											+ dataItem.description50;
																									return content;
																								},

																								position : "bottom",
																							});
																		},
																		columns : [
																				{
																					field : "profile",
																					width : 90,
																					title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																				},
																				{
																					field : "menuType",
																					width : 90,
																					title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																				},
																				{
																					field : "description50",
																					width : 90,
																					title : "${objMenuAssignmentLBL.getLbl_Description()}"
																				}

																		]
																	})
															.data("kendoGrid");
												}
											}
										});

							} else {

								var data = "[{}]";
								grid1 = $("#grid1")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													selectable : "multiple",
													pageable : false,
													cache : false,
													scrollable : {
														virtual : true,
														horizontal : false
													},
													columns : [
															{
																field : "profile",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
															},
															{
																field : "menuType",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
															},
															{
																field : "description50",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Description()}"
															}

													]
												}).data("kendoGrid");
							}

						}
						var jsonVal = JSON.stringify(grid2.dataSource.data())
						$("#MenuAssignmentJson").val(jsonVal);
						$("#copySelectedToGrid2").on(
								"click",
								function(idx, elem) {

									moveTo(grid1, grid2);
									var jsonVal = JSON
											.stringify(grid2.dataSource.data())
									$("#MenuAssignmentJson").val(jsonVal);

									return false;
								});
						$("#copySelectedToGrid1").on(
								"click",
								function(idx, elem) {
									moveTo(grid2, grid1);
									var jsonVal = JSON
											.stringify(grid2.dataSource.data())
									$("#MenuAssignmentJson").val(jsonVal);
									return false;
								});

						$("#copySelectedToGridAll2").on(
								"click",
								function(idx, elem) {
									moveToAll(grid1, grid2);
									var jsonVal = JSON
											.stringify(grid2.dataSource.data())
									$("#MenuAssignmentJson").val(jsonVal);

									return false;
								});

						$("#copySelectedToGridAll1").on(
								"click",
								function(idx, elem) {
									moveToAll(grid2, grid1);
									var jsonVal = JSON
											.stringify(grid2.dataSource.data())
									$("#MenuAssignmentJson").val(jsonVal);
									return false;
								});
					}

				});
	}

	//for edit to populate data in grid
	function loadGridonSelectionMobile(displayAll) {
		var grid2 = null;
		var grid1 = null;
		var menutype = displayAll.toLowerCase();
		var isRefresh = false;
		$('#grid2').val('');
		$('#grid2').html('');
		$('#grid1').val('');
		$('#grid1').html('');
		var selectedValue = "${objTeamMemberID}";
		$
				.post(
						"${pageContext.request.contextPath}/RestFull_AssignedMenuProfile",
						{
							TeamMemberID : selectedValue

						},
						function(data, status) {

							if (data) {
								isRefresh = true;
								grid2 = $("#grid2")
										.kendoGrid(
												{
													dataSource : data,
													editable : "popup",
													selectable : "multiple",
													pageable : false,
													cache : false,
													scrollable : true,
													height : 300,
													dataBound : function(e) {
														$("#grid2")
																.find("tr")
																.kendoTooltip(
																		{
																			filter : "td:nth-child(3)",
																			content : function(
																					e) {

																				var dataItem = $(
																						"#grid2")
																						.data(
																								"kendoGrid")
																						.dataItem(
																								e.target
																										.closest("tr"));
																				var content = dataItem.help_LINE
																						+ ' '
																						+ dataItem.description50;
																				return content;
																			},

																			position : "bottom",
																		});
													},
													columns : [
															{
																field : "profile",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
															},
															{
																field : "menuType",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
															},
															{
																field : "description50",
																width : 90,
																title : "${objMenuAssignmentLBL.getLbl_Description()}"
															}

													]
												}).data("kendoGrid");

								if (data != "") {

									//var application = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									if (displayAll.trim() != '') {

										document
												.getElementById('lblMenuTypeID').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objTeamMemberID}";
									//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									$
											.post(
													"${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader",
													{
														MenuType : displayAll,
														SelectedTeamMember : selectedValue
													},
													function(data, status) {

														if (data) {
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																				editable : "popup",
																				selectable : "multiple",
																				pageable : false,
																				cache : false,
																				scrollable : true,
																				height : 300,
																				dataBound : function(
																						e) {
																					$(
																							"#grid1")
																							.find(
																									"tr")
																							.kendoTooltip(
																									{
																										filter : "td:nth-child(3)",
																										content : function(
																												e) {
																											var dataItem = $(
																													"#grid1")
																													.data(
																															"kendoGrid")
																													.dataItem(
																															e.target
																																	.closest("tr"));
																											var content = dataItem.help_LINE
																													+ ' '
																													+ dataItem.description50;
																											return content;
																										},

																										position : "bottom",
																									});
																				},
																				columns : [
																						{
																							field : "profile",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																						},
																						{
																							field : "menuType",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																						},
																						{
																							field : "description50",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Description()}"
																						}

																				]
																			})
																	.data(
																			"kendoGrid");
														}

													});

								} else if (menutype.length > 0) {

									if (displayAll.trim() != '') {

										document
												.getElementById('lblMenuTypeID').value = displayAll;

									}
									/* var selectedValue = $(
											'#Application option:selected')
											.val(); */
									var selectedValue = "${objTeamMemberID}";
									//var displayAll = "${objMenuAssignmentLBL.getLbl_Display_All()}";

									$
											.post(
													"${pageContext.request.contextPath}/RestFull_MenuProfileAssignment_fromMenuHeader",
													{
														MenuType : displayAll,
														SelectedTeamMember : selectedValue
													},
													function(data, status) {

														if (data) {
															grid1 = $("#grid1")
																	.kendoGrid(
																			{
																				dataSource : data,
																				editable : "popup",
																				selectable : "multiple",
																				pageable : false,
																				cache : false,
																				scrollable : true,
																				height : 300,
																				dataBound : function(
																						e) {
																					$(
																							"#grid1")
																							.find(
																									"tr")
																							.kendoTooltip(
																									{
																										filter : "td:nth-child(3)",
																										content : function(
																												e) {
																											var dataItem = $(
																													"#grid1")
																													.data(
																															"kendoGrid")
																													.dataItem(
																															e.target
																																	.closest("tr"));

																											var content = dataItem.help_LINE
																													+ ' '
																													+ dataItem.description50;
																											return content;
																										},

																										position : "bottom",
																									});
																				},
																				columns : [
																						{
																							field : "profile",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																						},
																						{
																							field : "menuType",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																						},
																						{
																							field : "description50",
																							width : 90,
																							title : "${objMenuAssignmentLBL.getLbl_Description()}"
																						}

																				]
																			})
																	.data(
																			"kendoGrid");
														}

													});

								} else {

									var data = "[{}]";
									grid1 = $("#grid1")
											.kendoGrid(
													{
														dataSource : data,
														editable : "popup",
														selectable : "multiple",
														pageable : false,
														cache : false,
														scrollable : true,
														columns : [
																{
																	field : "profile",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
																},
																{
																	field : "menuType",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
																},
																{
																	field : "description50",
																	width : 90,
																	title : "${objMenuAssignmentLBL.getLbl_Description()}"
																}

														]
													}).data("kendoGrid");
								}

							}
							var jsonVal = JSON.stringify(grid2.dataSource
									.data())
							$("#MenuAssignmentJson").val(jsonVal);
							$("#copySelectedToGrid2").on(
									"click",
									function(idx, elem) {

										moveTo(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);

										return false;
									});
							$("#copySelectedToGrid1").on(
									"click",
									function(idx, elem) {
										moveTo(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});

							$("#copySelectedToGridAll2").on(
									"click",
									function(idx, elem) {
										moveToAll(grid1, grid2);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);

										return false;
									});

							$("#copySelectedToGridAll1").on(
									"click",
									function(idx, elem) {
										moveToAll(grid2, grid1);
										var jsonVal = JSON
												.stringify(grid2.dataSource
														.data())
										$("#MenuAssignmentJson").val(jsonVal);
										return false;
									});

						});
	}

	function moveTo(from, to) {

		var selected = from.select();
		if (selected.length > 0) {
			var items = [];
			$.each(selected, function(idx, elem) {
				items.push(from.dataItem(elem));
			});
			fromDS = from.dataSource;
			toDS = to.dataSource;
			$.each(items, function(idx, elem) {
				toDS.add($.extend({}, elem));
				fromDS.remove(elem);
			});
			/* toDS.sync();
			fromDS.sync(); */
		}

	}

	function moveToAll(from, to) {
		var varselected = from.dataSource;
		var allvalGrid = varselected.data();

		if (allvalGrid.length > 0) {
			var items = [];
			$.each(allvalGrid, function(idx, elem) {
				items.push(elem);
			});

			var fromDS = from.dataSource;
			var toDS = to.dataSource;
			$.each(items, function(idx, elem) {
				toDS.add($.extend({}, elem));
				fromDS.remove(elem);
			});
			//toDS.sync();
			//fromDS.sync();

		}
	}

	function loadDataOnChange() {
		var selectedValue = $('#lblMenuTypeID option:selected').val();
		if ("${objDeviceType}" != "MOBILE") {
			loadGridonSelection(selectedValue);
		} else {
			loadGridonSelectionMobile(selectedValue);
		}
	}

	jQuery(document).ready(function() {

		$('#LastDate').val(getUTCDateTime());
		var changeStatus = '${objchangedDropDown}';

		if (changeStatus == 'true') {
			//onSelectedChangedValueData();

			var selectedValue = "${objSelectedMenuType}";
			if ("${objDeviceType}" != "MOBILE") {
				loadGridonSelection(selectedValue);
			} else {
				loadGridonSelectionMobile(selectedValue);
			}
		} else {
			var selectedValue = "Display All";
			if ("${objDeviceType}" != "MOBILE") {
				loadEditGrid(selectedValue);
			} else {
				loadEditGridMobile(selectedValue);
			}
		}
		/* 
		var dataVal = "";

		var grid2 = $("#grid2").kendoGrid({
			dataSource : dataVal,
			editable : "popup",
			selectable : "multiple",
			pageable : false,
			columns : [
						{
							field : "profile",
							width : 90,
							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
						},
						{
							field : "menuType",
							width : 90,
							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
						},
						{
							field : "description50",
							width : 90,
							title : "${objMenuAssignmentLBL.getLbl_Description()}"
						}

				]
		}).data("kendoGrid");
		
		var grid1 = $("#grid1").kendoGrid({
			dataSource : dataVal,
			editable : "popup",
			selectable : "multiple",
			pageable : false,
			columns : [
						{
							field : "profile",
							width : 90,
							title : "${objMenuAssignmentLBL.getLbl_Menu_profile()}"
						},
						{
							field : "menuType",
							width : 90,
							title : "${objMenuAssignmentLBL.getLbl_MenuType()}"
						},
						{
							field : "description50",
							width : 90,
							title : "${objMenuAssignmentLBL.getLbl_Description()}"
						}

				]
		}).data("kendoGrid");

		
		

		$("#copySelectedToGrid2").on("click", function(idx, elem) {

			moveTo(grid1, grid2);

		
			return false;
		});
		$("#copySelectedToGrid1").on("click", function(idx, elem) {
			moveTo(grid2, grid1);
		
			return false;
		});

		$("#copySelectedToGridAll2").on("click", function(idx, elem) {
			moveToAll(grid1, grid2);
			

			return false;
		});

		$("#copySelectedToGridAll1").on("click", function(idx, elem) {
			moveToAll(grid2, grid1);
		
			return false;
		});
		 */

		//onloadGrid();
		//for footer adjust
		setInterval(function() {

			var h = window.innerHeight;
			if (window.innerHeight >= 900) {
				h = h - 187;

			} else {
				h = h - 239;
			}
			document.getElementById("page-content").style.minHeight = h + "px";

		}, 30);

		/*  Metronic.init(); // init metronic core componets
		 Layout.init(); // init layout
		 Demo.init(); // init demo(theme settings page)
		 Index.init(); // init index page
		 Tasks.initDashboardWidget();  */// init tash dashboard widget
	});

	function headerChangeLanguage() {

		/* alert('in security look up'); */

		$.ajax({
			url : '${pageContext.request.contextPath}/HeaderLanguageChange',
			type : 'GET',
			cache : false,
			success : function(data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});

	}
	
	 function headerInfoHelp(){
		  	
		  	//alert('in security look up');
		  	
		   	/* $
		  	.post(
		  			"${pageContext.request.contextPath}/HeaderInfoHelp",
		  			function(
		  					data1) {

		  				if (data1.boolStatus) {

		  					location.reload();
		  				}
		  			});
		  	 */
		  	$.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
		  			  {InfoHelp:'MenuProfileAssignment',
		   		InfoHelpType:'PROGRAM'
		  		 
		  			  }, function( data1,status ) {
		  				  if (data1.boolStatus) {
		  					  window.open(data1.strMessage); 
		  					  					  
		  					}
		  				  else
		  					  {
		  					//  alert('No help found yet');
		  					  }
		  				  
		  				  
		  			  });
	   }
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>