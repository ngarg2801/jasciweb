<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream" %>
<!DOCTYPE html>

<html>
<head>
<style type="text/css">
.error-Top-Forgot {
  margin-top: -2.2% !important;
}
</style>
<meta charset="utf-8" />
<title>JASCI | Login Options - Login Form 1</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/uniform/css/uniform.default.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link
	href="<c:url value="/resources/assets/global/plugins/select2/select2.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/admin/pages/css/login.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link
	href="<c:url value="/resources/assets/global/css/components.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/assets/global/css/plugins.css"/>"
	rel="stylesheet" type="text/css" />
<link href="/resources/assets/admin/layout/css/layout.css"
	rel="stylesheet" type="text/css" />
<link id="style_color"
	href="<c:url value="/resources/assets/admin/layout/css/themes/default.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/admin/layout/css/custom.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<link href="<c:url value="/resourcesValidate/css/error-style.css"/>"  rel="stylesheet" type="text/css">
	<script>
		window.location.hash = "no-back-button";
		window.location.hash = "Again-No-back-button";//again because google chrome don't insert first hash into history
		window.onhashchange = function() {
			window.location.hash = "no-back-button";
		}
	</script>
<script
	src="<c:url value="/resources/assets/global/plugins/jquery-1.11.0.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/jquery-migrate-1.2.1.min.js"/>"
	type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script
	src="<c:url value="/resources/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/jquery.blockui.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/jquery.cokie.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/uniform/jquery.uniform.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>"
	type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script
	src="<c:url value="/resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"/>"
	type="text/javascript"></script>
<script type="text/javascript"
	src="<c:url value="/resources/assets/global/plugins/select2/select2.min.js"/>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script
	src="<c:url value="/resources/assets/global/scripts/metronic.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/admin/layout/scripts/layout.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/admin/layout/scripts/quick-sidebar.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/admin/layout/scripts/demo.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resources/assets/admin/pages/scripts/login.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script src="<c:url value="/resourcesValidate/js/moment.js"/>" type="text/javascript"></script>
<link href="<c:url value="/resourcesValidate/css/glass-style.css"/>"  rel="stylesheet" type="text/css">

<script>
	function body_load(url) {
		
		
	}
	function previousPage() {

		$("#ErrorMessage1").hide();
		$("#ErrorMessage2").hide();
		$("#ErrorMessage3").hide();
		$("#ErrorMessage4").hide();
		window.location.href = 'loginForm';
	}

	function isValidEmail() {
		$('#LastActivityDateStringH').val(getUTCDateTime());
		
		var x = document.forms["forgetPass"]["email"].value;

		if (x == '') {

			$("#ErrorMessage1").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").show();

			return false;

		} else {
			$("#ErrorMessage4").hide();
		}

		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if (!re.test(x)) {
			$("#ErrorMessage1").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").show();
			// document.getElementById("ErrorMessage3").style.display="block";
			return false;
		} else {
			$("#ErrorMessage3").hide();

			return true;
		}

	}
</script>

</head>
<style>
input {
	border: solid 1px blue !important;
	box-shadow: none !important;
}

input:-moz-placeholder {
	box-shadow: none !important;
}

input:invalid {
	box-shadow: 0 0 3px lime !important;
}

input:focus, textarea:focus {
	border-color: initial;
}

input.error, textarea.error {
	border: solid 1px gray !important;
	box-shadow: none !important;
}


.error-note{

height: 52px;
}


</style>

<body class="login" onload="body_load();">

	<!-- BEGIN LOGO -->
	<div class="logo">
		<!-- <a href="index.html"> -->
		<img class="logojasci"
			src="<c:url value="/resources/assets/admin/layout3/img/Jasci_02.png"/>"
			alt="" />
		<!-- </a> -->
	</div>
	<!-- END LOGO -->
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="menu-toggler sidebar-toggler"></div>
	<!-- END SIDEBAR TOGGLER BUTTON -->

	<div class="content_pass">
		<form class="login-form" name="forgetPass" method="post"
			action="forgetpassword" modelAttribute="forgotpassword"
			onsubmit="return isValidEmail();">

			<c:if test="${Result eq 'true'}">
				<div id="ErrorMessage1" class="note error-note note-danger">
					<p class="error error-Top-Forgot margin-left-7pix  error-msg-width-forget margin-top-8px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${Message}</p>
				</div>
			</c:if>
			<c:if test="${Result eq 'false'}">
				<div id="ErrorMessage2" class="note note-danger">
					<p class="error error-Top-Forgot margin-left-7pix error-msg-width-forget margin-top-8px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${Message}</p>
				</div>
			</c:if>

			<div id="ErrorMessage3" class="note note-danger"
				style="display: none;">
				<p class="error error-Top-Forgot margin-left-7pix error-msg-width-forget margin-top-8px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter a valid email address.</p>
			</div>
			<div id="ErrorMessage4" class="note note-danger"
				style="display: none;">
				<p class="error error-Top-Forgot margin-left-7pix error-msg-width-forget margin-top-8px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter email address.</p>
			</div>

		<input id="LastActivityDateStringH" type="hidden" class="form-controlwidth" name="LastActivityDateStringH" />
			<h3 class="form-title">${ObjLoginBe.getStrForgetPasswordOnNewPassword()}
			</h3>
			<p>${ObjLoginBe.getStrEnterEmailMessage()}</p>


			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<div class="input-icon">
					<i class="fa fa-envelope"></i> <input
						class="form-control placeholder-no-fix" type="text"
						autocomplete="off" placeholder="Email"
						onkeypress="isValidEmail();" onkeydown="isValidEmail();"
						style="border: none" name="email" id="email">
				</div>
			</div>

			<div class="margin-bottom-5-right-allign_forgetpassword">

				<button type="submit"
					class="btn btn-sm green filter-submit margin-bottom">
					<i class="fa fa-check"></i>&nbsp;${ObjLoginBe.getStrSubmitButton()}</button>


				<button type="button" class="btn btn-sm red filter-cancel"
					onclick="previousPage();">
					<i class="fa fa-times"></i>&nbsp;${ObjLoginBe.getStrBackButton()}</button>

			</div>

		</form>

	</div>


<div class="copyright">
 <div class="textnotice"><div style="float:left"><img style="height: auto; width: 38px; position: relative; left: -5px; top: 7px;" src="<c:url value="/resources/assets/admin/layout3/img/used img.png"/>" alt=""/></div><p class="textemail">${ObjCommonSession.getForValidEmaildID()}</p>
 </div>
</div>

	<!-- <div class="copyright">Copyright &copy;2014 - JASCI, LLC. All
		Rights Reserved.</div> -->
		<div class="copyright">${ObjCommonSession.getFooterText()}</div>
</body>

</html>