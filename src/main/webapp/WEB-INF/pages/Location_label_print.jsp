<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	
	

		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->




		<!-- BEGIN PAGE CONTAINER -->
		<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_LabelSelect()}</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->
					<div class="page-toolbar">
						<!-- BEGIN THEME PANEL -->
						<div class="btn-group btn-theme-panel">
							<a href="javascript:;" class="btn dropdown-toggle"
								data-toggle="dropdown"> <!--<i class="icon-settings"></i>-->
							</a>
							<div
								class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<h3>THEME COLORS</h3>
										<div class="row">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<ul class="theme-colors">
													<li class="theme-color theme-color-default"
														data-theme="default"><span class="theme-color-view"></span>
														<span class="theme-color-name">Default</span></li>
													<li class="theme-color theme-color-blue-hoki"
														data-theme="blue-hoki"><span class="theme-color-view"></span>
														<span class="theme-color-name">Blue Hoki</span></li>
													<li class="theme-color theme-color-blue-steel"
														data-theme="blue-steel"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Blue Steel</span></li>
													<li class="theme-color theme-color-yellow-orange"
														data-theme="yellow-orange"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Orange</span></li>
													<li class="theme-color theme-color-yellow-crusta"
														data-theme="yellow-crusta"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Yellow Crusta</span></li>
												</ul>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<ul class="theme-colors">
													<li class="theme-color theme-color-green-haze"
														data-theme="green-haze"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Green Haze</span></li>
													<li class="theme-color theme-color-red-sunglo"
														data-theme="red-sunglo"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Red Sunglo</span></li>
													<li class="theme-color theme-color-red-intense"
														data-theme="red-intense"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Red Intense</span></li>
													<li class="theme-color theme-color-purple-plum"
														data-theme="purple-plum"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Purple Plum</span></li>
													<li class="theme-color theme-color-purple-studio"
														data-theme="purple-studio"><span
														class="theme-color-view"></span> <span
														class="theme-color-name">Purple Studio</span></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12 seperator">
										<h3>LAYOUT</h3>
										<ul class="theme-settings">
											<li>Layout <select
												class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips"
												data-original-title="Change layout type"
												data-container="body" data-placement="left">
													<option value="boxed" selected="selected">Boxed</option>
													<option value="fluid">Fluid</option>
											</select>
											</li>
											<li>Top Menu Style <select
												class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips"
												data-original-title="Change top menu dropdowns style"
												data-container="body" data-placement="left">
													<option value="dark" selected="selected">Dark</option>
													<option value="light">Light</option>
											</select>
											</li>
											<li>Top Menu Mode <select
												class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips"
												data-original-title="Enable fixed(sticky) top menu"
												data-container="body" data-placement="left">
													<option value="fixed">Fixed</option>
													<option value="not-fixed" selected="selected">Not
														Fixed</option>
											</select>
											</li>
											<li>Mega Menu Style <select
												class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips"
												data-original-title="Change mega menu dropdowns style"
												data-container="body" data-placement="left">
													<option value="dark" selected="selected">Dark</option>
													<option value="light">Light</option>
											</select>
											</li>
											<li>Mega Menu Mode <select
												class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips"
												data-original-title="Enable fixed(sticky) mega menu"
												data-container="body" data-placement="left">
													<option value="fixed" selected="selected">Fixed</option>
													<option value="not-fixed">Not Fixed</option>
											</select>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- END THEME PANEL -->
					</div>
					<!-- END PAGE TOOLBAR -->
				</div>
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content min-height-llp-page" id="page-content" >
				<div class="container">
					<!-- BEGIN PAGE BREADCRUMB -->
					<ul class="page-breadcrumb breadcrumb hide">
						<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
						<li class="active"></li>
					</ul>
					<!-- END PAGE BREADCRUMB -->
					<!-- BEGIN PAGE CONTENT INNER -->
					<div class="row margin-top-10">
						<div class="col-md-12">
							<form class="form-horizontal form-row-seperated" action="#">
								<div class="portlet">
									<input type="hidden" name="wizarControlNumberFromGrid" id="wizarControlNumberFromGrid">
									

									<div class="portlet-body">
									<div id="ErrorMessage" class="note note-danger display-none margin-left-14pix">
												<p id="Perror" class="error error-Top margin-left-7pix">error</p>
											</div>
										<div class="tabbable">

											<div class="tab-content no-space">
												<div class="tab-pane active margen-left-for-fit" id="tab_general">
													<div class="form-body">

														<div class="form-group margin-bototm-4prcnt">
															<label class="col-md-2  bold-text label-width-location-label" >${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_LabelSelect()}: </label>
															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium location-width-45px width-58prcnt"
																	name="LabelSelection" id="LabelSelection">
																	<option value="0">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Select()}...</option>
																	<option value="1">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_LocationLabelSelection()}</option>
																	<option value="2">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_ReprintFlaggedLocations()}</option>
																</select>
															</div>
														</div>
														
														<div class="form-group" id="fulfillmentCenterID" style="display: none;">
															<label class="col-md-2  label-width-location-label" >${LocationLabelPrintSceernLabelObj.getFullfillment_center()}: </label>
															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium location-width-45px width-58prcnt"
																	name="fullfillmentCenter" id="fullfillmentCenter">
																	<option value="">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Select()}...</option>
																	<c:forEach items="${LocationLabelPrintFullfillmentCenterListObj}" var="FulfilmentCenter">
																	<option value="${FulfilmentCenter.getId().getFulfillmentCenter()}">${FulfilmentCenter.getName20()}</option>
																	</c:forEach>
																</select>
															</div>
														</div>

														<div id="Section1" style="display: none;">
															<!-- Begin section 1-->

															<div id="rd">
																<input type="radio" name="RadioSelect" class="check1"
																	value="Radiopart1" id="Radiopart1">
															</div>
															<div id="Section1Part1">
																<!-- Begin Section1Part1 -->
																<div class="form-group">
																	<label class="col-md-2  label-width-location-label" >${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Area()}:<!-- <span class="required" id="requiredArea1">*</span> --> </label>
																	<div class="col-md-10" id="location-lbl-text">
																	<select
																		class="table-group-action-input form-control input-medium location-width-45px width-58prcnt"
																		id="Area" name="Area">
																		<option value="">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Select()}...</option>																		
																		<c:forEach items="${LocationLabelPrintAreasListObj}"
																			var="AreasList">
																		<option
																				value='${AreasList.getGeneralCode()}'>${AreasList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																		<label class="label-width-location-label-2 location-label-portrait-set ">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_AndLocation()}:<!-- <span class="required" id="requiredAndLocation">*</span> --></label>
																		<input type="text" class="textbox2 form-controlwidth location-width-45px location-margin-right-25px" id="Location" name="Location" maxlength="20">
																	</div>
																			
																</div>
																

															</div>
															<!-- End Section1Part2 -->

															<div id="rd" >
																
																<input type="radio" name="RadioSelect"
																	value="Radiopart2" id="Radiopart2">
															</div>
															<div id="Section1Part2">
																<!-- Begin Section1Part1 -->
																<div class="form-group">
																	<label class="col-md-2  label-width-location-label ">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_WizardControlNumber()}:<!-- <span class="required" id="requiredWCN">*</span> --></label>
																	<div class="col-md-10">
																		<input type="text" class="form-controlwidth location-width-45px width-58prcnt"
																			name="WizardControlNumber" id="WizardControlNumber" maxlength="11"><label class="label-width-location-label-2 location-label-portrait-set">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Or()}</label>
																			<div
																		class="lookup-all-with-selection-btn">
																		<button class="btn btn-sm yellow margin-bottom"
																			id="btnLookupAllWithSelection" onclick="div_show();return false;">
																			<i class="fa fa-search"></i> ${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_LookupAllwithSelection()}
																		</button>
																	</div>
																			
																	</div>
																</div>
																

															</div>
															<!-- End Section1Part2 -->




															<div id="rd">
																<input type="radio" name="RadioSelect"
																	value="Radiopart3" id="Radiopart3">
															</div>
															<div id="Section1Part2">
																<!-- Begin Section1Part3 -->
																<div class="form-group">
																	<label class="col-md-2  label-width-location-label " >${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Area()}:<!-- <span class="required" id="requiredArea2">*</span> --></label>
																	<div class="col-md-10" id="location-lbl-text">
																	<select
																		class="table-group-action-input form-control input-medium width-18prcnt padding-0px "
																		id="FromArea" name="FromArea" >
																	
																		<option value="">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Select()}...</option>																		
																		<c:forEach items="${LocationLabelPrintAreasListObj}"
																			var="AreasList">
																		<option
																				value='${AreasList.getGeneralCode()}'>${AreasList.getDescription20()}</option>
																		</c:forEach>
																	</select><label class="label-width-location-label-3 location-label-portrait-set margin-left-1-5px width-14px ">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_FromLocation()}:<!-- <span class="required" id="requiredFromLocation">*</span> --></label>
																			<input type="text" class="textbox2 form-controlwidth margin-right-42px width-23px " id="FromAndLocation" name="FromAndLocation" maxlength="20" >
																			<label class="label-width-location-label-2 location-label-portrait-set margin-left-45-5px ">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_ToLocation()}:<!-- <span class="required" id="requiredToLocation">*</span> --></label>
																		<input type="text" class="textbox2 form-controlwidth location-margin-right-25px margin-right-minus-93px" id="ToLocation" name="ToLocation" maxlength="20">
																	</div>
																			
																</div>

															</div>
															<!-- End Section1Part3 -->
															</div>
														<!-- end section 1-->

															
															

														


														<div id="Section2" style="display: none;">
															<!-- Begin section 2-->

															<%-- <div class="form-group">
																<label class="col-md-2 label-width-location-label">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_ReprintFlaggedLocations()}: </label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth location-width-45px"
																		name="ReprintFlaggedLocations"
																		id="ReprintFlaggedLocations">
																</div>
															</div> --%>
															
															
															
															<div class="form-group">
																<label class="col-md-2 label-width-location-label ">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Area()}:<!-- <span class="required" id="requiredState">*</span> --></label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium location-width-45px width-58prcnt"
																		id="AndArea" name="AndArea">
																		<option value="">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Select()}...</option>																		
																		<c:forEach items="${LocationLabelPrintAreasListObj}"
																			var="AreasList">
																		<option
																				value='${AreasList.getGeneralCode()}'>${AreasList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															
															
															

														</div>
														<!-- end section 2-->
														<div id="BarCode-Section" style="display: none;">
														<div class="form-group">
																<label class="col-md-2  label-width-location-label">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_LabelType()}:<!-- <span class="required" id="requiredState">*</span> --> </label>
																<div class="col-md-10">
																
																		
																	<select
																		class="table-group-action-input form-control input-medium location-width-45px width-58prcnt "
																		name="LabelType" id="LabelType">
																		<option value="">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Select()}...</option>
																		
																		<c:forEach items="${LocationLabelPrintLocLabelListObj}"
																			var="LocLabelList">
																		<option
																				value='${LocLabelList.getGeneralCode().toLowerCase()}'>${LocLabelList.getDescription20()}</option>
																		</c:forEach>
																		
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2  label-width-location-label ">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Barcode_Type()}:<!-- <span class="required" id="requiredState">*</span> --></label>
																<div class="col-md-10">
																
																		
																	<select
																		class="table-group-action-input form-control input-medium location-width-45px width-58prcnt"
																		name="BarcodeType" id="BarcodeType">
																		<option value="">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Select()}...</option>
																		<option value="1">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_BarcodeType3of9()}</option>
																		<option value="2">${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_BarcodeType128()}</option>
																		<%-- <c:forEach items="${LocationLabelPrintLocLabelListObj}"
																			var="LocLabelList">
																		<option
																				value='${LocLabelList.getId().getGeneralCode()}'>${LocLabelList.getId().getGeneralCode()}</option>
																		</c:forEach> --%>
																		
																	</select>
																</div>
															</div>
														</div>
														<!-- end BarCode-Section -->
														<div id="printLabelbtn" style="display: none;">
															<div
																class="label-print-btn">
																<button class="btn btn-sm yellow m" onclick="isValidSelection();return false;">
																	<i class="fa fa-check"></i> ${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_PrintLabels()}
																</button>
															</div>
														</div>

													</div>
												</div>

							</div>
										</div>
									</div>


								</div>
							</form>
						</div>

					</div>

					<!-- END PAGE CONTENT INNER -->
				</div>
			</div>
			<!-- END PAGE CONTENT -->
		</div>
		<!-- END PAGE CONTAINER -->
		
		
		
		
		
	<!-- pop up code -->	
		
<div class="cover back-cover" ></div>

<div id="show" class="Show_main show-main-div-popup" >
<div class="form-body">
            <div id="DivLabelCompany" class="form-group">
             <label class="col-md-2  wizard-location-width" id="LabelCompany" ><b>&nbsp;&nbsp;&nbsp;${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Wizard_Location()}</b>&nbsp;<span id="Label"></span></label>
          <hr id="borderline"></hr>
            </div>
				</div><br /><br /><br />
			<div class="form-body" id="grid-form">
				<div class="form-group">
					<div id="ErrorMessageGrid" class="note note-danger" style="display:none;">
     					<p id="MessageRestFull" class="error err-modified" ></p>	
    				</div>
					<div class="grid-setting">
					
					<div class="row">

									<kendo:grid name="WizardLocation" columnResize="true"
										sortable="true" resizable="true" reorderable="true" selectable="row" change="Check" autoSync="true">
										<kendo:grid-pageable refresh="true" pageSizes="true" 
											buttonCount="5">
										</kendo:grid-pageable>
										<kendo:grid-editable mode="inline" confirmation="Message" />

										<kendo:grid-columns>

											<kendo:grid-column
												title="${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_wizard_ID()}"
												field="wizardId" width="30%;" />
											
											<kendo:grid-column
												
												title="${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_WizardControlNumber()}"
												field="wizardControlNumber" width="30%;" />
												
											<kendo:grid-column
												title="${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Description()}"
												field="description50" width="40%;">
												
											</kendo:grid-column>
										</kendo:grid-columns>

										<kendo:dataSource pageSize="10">
											<kendo:dataSource-transport>
												 <kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/RestGetLocationLabelPrintGetWizardLocationList"></kendo:dataSource-transport-read>
											 	<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/Customers"></kendo:dataSource-transport-read> --%>
												<kendo:dataSource-transport-update
													url="GeneralCodeList/Grid/update" dataType="json"
													type="POST" contentType="application/json" />
												<kendo:dataSource-transport-destroy
													url="GeneralCodeList/Grid/delete" dataType="json"
													type="POST" contentType="application/json">
													<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
												</kendo:dataSource-transport-destroy>


												<kendo:dataSource-transport-parameterMap>
													<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>
											<kendo:dataSource-schema>
												<kendo:dataSource-schema-model id="intKendoID">
													<kendo:dataSource-schema-model-fields>
														<kendo:dataSource-schema-model-field name="intKendoID">

														</kendo:dataSource-schema-model-field>


													</kendo:dataSource-schema-model-fields>
												</kendo:dataSource-schema-model>
											</kendo:dataSource-schema>



										</kendo:dataSource>
									</kendo:grid>

									<div class="popupbutton popup-bottom-button" >
						<button class="btn btn-sm green  margin-bottom" onclick="reassign();" id="popup"><i class="fa fa-check"></i> ${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Submit()}</button>
     					<button class="btn btn-sm red filter-cancel" onclick="div_hide()" id="popup"><i class="fa fa-times"></i>${LocationLabelPrintSceernLabelObj.getLocation_Label_Print_Cancel()}</button>
						</div>

								</div>
					
						
					</div>
					
				</div>
		</div>
	
</div>
		

	</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(action) {

	}
</script>
<script>
	jQuery(document).ready(function() {

		onLoadValue();
		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		setInterval(function() {

			var h = window.innerHeight;

			if (window.innerHeight >= 900 || window.innerHeight == 1004) {
				h = h - 187;

			} else {
				h = h - 239;
			}
			document.getElementById("page-content").style.minHeight = h + "px";
			
			
			
			

		}, 30);

		/* Metronic.init(); // init metronic core componets
		Layout.init(); // init layout
		Demo.init(); // init demo(theme settings page)
		Index.init(); // init index page
		Tasks.initDashboardWidget(); // init tash dashboard widget */
	});
</script>

<script>

var selection1=false;
var selection2=false;
	function onLoadValue() {

		
		$('#fulfillmentCenterID').hide();
	            	$('#Section1').hide();
					$('#Section2').hide();
					$('#printLabelbtn').hide();
					$('#BarCode-Section').hide();
					
					$('#Area').change(function() {
						var varAreaValue=($(this).val());
						var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
							$.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintType",{Area : varAreaValue,
								fullfillmentCenter:fullfillmentCenterID },function(data, status) {
							if (data.length==0) {
								document.getElementById('LabelType').value = '';
							} else {
																
									/* var vl=data[0].trim();
									document.getElementById('LabelType').value = vl; */
									
							}
						});
					});
					
					$('#AndArea').change(function() {
						var varAreaValue=($(this).val());
						var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
							$.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintType",{Area : varAreaValue,
								fullfillmentCenter:fullfillmentCenterID},function(data, status) {
							if (data.length==0) {
							} else {
													
									/* var vl=data[0].trim();
									document.getElementById('LabelType').value = vl; */
									
							}
						});
					});
					
					$('#FromArea').change(function() {
						var varAreaValue=($(this).val());
						var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val().trim();
						
						
							$.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintType",{Area : varAreaValue,
								fullfillmentCenter:fullfillmentCenterID },function(data, status) {
							if (data.length==0) {
							} else {
													
									/* var vl=data[0].trim();
									document.getElementById('LabelType').value = vl; */
									
							}
						});
							
						
					});

					
					 $("#WizardControlNumber").keypress(function(){
						 var varWizardControlNumber = $('#WizardControlNumber').val();
						 var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
						 $.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintGetGetWizardControlNumber",{WizardControlNumber : varWizardControlNumber,
							 fullfillmentCenter:fullfillmentCenterID},function(data, status) {
								if (data.length==0) {
									//document.getElementById('LabelType').value = '';
								} else {
									/* var vl=data[0].trim();
									document.getElementById('LabelType').value = vl; */		
								}
							});//	
						    
						  });
					 
					 $("#WizardControlNumber").keypress(function(){
						 
					 });
					
				$('#LabelSelection').change(function() {
					if ($(this).val() == '1') {
					  selection1=true;
					  selection2=false;
		     		  $('#Section1').show();
		     		 $('#fulfillmentCenterID').show();
		     		 
		     		  $('#ErrorMessage').hide();
		     		  $('#BarCode-Section').show();	
					  $('#Section2').hide();
					  $('input:radio[name=RadioSelect]')[0].checked = true;
					 
					  $('#Area').removeAttr('disabled');	
				      $('#Location').removeAttr('disabled');
					 
					  $('#WizardControlNumber').attr('disabled','disabled');
					  $('#btnLookupAllWithSelection').attr('disabled','disabled');
					  $('#FromArea').attr('disabled','disabled');
					  $('#FromAndLocation').attr('disabled','disabled');
					  $('#ToLocation').attr('disabled','disabled');
					  
					  $('#FromArea').val('');
					  $('#WizardControlNumber').val('');
					  $('#FromAndLocation').val('');
					  $('#ToLocation').val('');				
				      $('#printLabelbtn').show();	
				      $('#BarcodeType').val('');
					  $('#LabelType').val('');
				      $('#AndArea').val('');
				      $('#Area').val('');
				      $('#fullfillmentCenter').val('');
				} 
					
					if ($(this).val() == '2') {
						selection1=false;
						selection2=true;
						$('#BarCode-Section').show();
						$('#ErrorMessage').hide();
						$('#Section2').show();
						 $('#fulfillmentCenterID').show();
						$('#printLabelbtn').show();
						$('#Section1').hide();
						$('#BarcodeType').val('');
						$('#LabelType').val('');
				        $('#AndArea').val('');
				        $('#ReprintFlaggedLocations').val('');
				        $('#printLabelbtn').show();
				        $('#Area').val('');
				        $('#fullfillmentCenter').val('');
					} 
					
					if ($(this).val() == '0') {	
						$('#Section2').hide();	
						$('#Section1').hide();
						$('#ErrorMessage').hide();
						$('#printLabelbtn').hide();
						$('#BarCode-Section').hide();
						$('#Area').val('');
						 $('#fullfillmentCenter').val('');
						
					}
					
					

				});

				
				$('#Radiopart1').change(function() {
				  if ($(this).is(":checked")){
				  
			      $('#Area').removeAttr('disabled');	
			      $('#Location').removeAttr('disabled');
				 
				  $('#WizardControlNumber').attr('disabled','disabled');
				  $('#btnLookupAllWithSelection').attr('disabled','disabled');
				  		  
					
				  $('#FromArea').attr('disabled','disabled');
				  $('#FromAndLocation').attr('disabled','disabled');
				  $('#ToLocation').attr('disabled','disabled');
				  
				 
				  $('#FromArea').val('');
				  $('#WizardControlNumber').val('');
				  $('#FromAndLocation').val('');
				  $('#ToLocation').val('');
				  
				}		
				});

				$('#Radiopart2').change(function() {
				  if ($(this).is(":checked")){
				  
				 
				  
				  $('#Area').attr('disabled','disabled');	
			      $('#Location').attr('disabled','disabled');
				 
				  $('#WizardControlNumber').removeAttr('disabled');
				  $('#btnLookupAllWithSelection').removeAttr('disabled');
				  		  
					
				  $('#FromArea').attr('disabled','disabled');
				  $('#FromAndLocation').attr('disabled','disabled');
				  $('#ToLocation').attr('disabled','disabled');
				  
				 		  
				  $('#Area').val('');
				  $('#FromArea').val('');
				  $('#Location').val('');
				  $('#FromAndLocation').val('');
				  $('#ToLocation').val('');
				}		
				});
				
				$('#Radiopart3').change(function() {
				  if ($(this).is(":checked")){
					  
					  $('#Area').attr('disabled','disabled');	
				      $('#Location').attr('disabled','disabled');
					 
					  $('#WizardControlNumber').attr('disabled','disabled');	
					  $('#btnLookupAllWithSelection').attr('disabled','disabled');	
					  		  
						
					  $('#FromArea').removeAttr('disabled');
					  $('#FromAndLocation').removeAttr('disabled');
					  $('#ToLocation').removeAttr('disabled');
					  
					 		  
					  $('#Area').val('');
					  $('#Location').val('');
					  $('#WizardControlNumber').val('');
				
				}		
				});


	}

	function isValidSelection() {

		/**selection part 1*/
		if(selection1) {
			
		var isTest=false;
		var checkedRadioPart1 = $('input[value=Radiopart1]').is(":checked");
		var checkedRadioPart2 = $('input[value=Radiopart2]').is(":checked");
		var checkedRadioPart3 = $('input[value=Radiopart3]').is(":checked");
		var isblankFullfillmentcenter=checkFullfillmentCenter('Perror','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ '${LocationLabelPrintSceernLabelObj.getKP_Please_Select_Fullfillment_Center()}');
		
		if(isblankFullfillmentcenter){
		if (checkedRadioPart1) {
			
			var isBlankLocation =false;	
			var isBlankArea = isBlankField(
					'Area',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Area()}');
			
			if(isBlankArea){
			 isBlankLocation = isBlankField(
					'Location',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_enter_And_Location()}');
			}
			
			
			
			if(isBlankArea && isBlankLocation){
				$('#ErrorMessage').hide();
			   isTest=true;
				
				
			}else{
				$('#ErrorMessage').show();
				/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = ""; */
				isTest=false;
				isSubmit = false;
			}
			
			
			
			
			
		}

		if (checkedRadioPart2) {
				
			var isValueNumeric=false;
			var isBlankWizardControlNumber = isBlankField(
					'WizardControlNumber',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_enter_Wizard_Control_Number()}');
			
			
			if(isBlankWizardControlNumber){
				
				isValueNumeric = isNumeric(
						'WizardControlNumber',
						'Perror',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${LocationLabelPrintSceernLabelObj.getERR_Please_enter_numeric_Wizard_Control_Number()}');
				
			}
			if(isBlankWizardControlNumber && isValueNumeric){
				$('#ErrorMessage').hide();
				isTest=true;
				
			}else{
				$('#ErrorMessage').show();
				/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = ""; */
				isSubmit = false;
				isTest=false;
			}
			
			
		}

		if (checkedRadioPart3) {
			
			
			var isBlankToLocation =false;	
			var isBlankFromAndLocation=false;
			
		
			var isBlankFromArea = isBlankField(
					'FromArea',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Area()}');
			
			
			if(isBlankFromArea){
				isBlankFromAndLocation = isBlankField(
					'FromAndLocation',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_enter_From_Location()}');
			}
			
			
			if(isBlankFromAndLocation){
				isBlankToLocation = isBlankField(
					'ToLocation',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_enter_To_Location()}');
			}
			
				
			if(isBlankFromArea && isBlankToLocation && isBlankFromAndLocation && isblankFullfillmentcenter){
				$('#ErrorMessage').hide();
				isTest=true;
				
			}else{
				$('#ErrorMessage').show();
				/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = ""; */
				isSubmit = false;
				isTest=false;
			}
			
			
			
			}//end part three
		
		}	else{
			$('#ErrorMessage').show();
			/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
			elem1.value = ""; */
			isSubmit = false;
			isTest=false;
		}
		
		if(isTest){
			
			var isBlankBarcodeType=false;
			var isBlankLabelType = isBlankField(
					'LabelType',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Label_Type()}');
			
			
			
			if(isBlankLabelType){
				
				isBlankBarcodeType = isBlankField(
						'BarcodeType',
						'Perror',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Barcode_Type()}');
				
			}
			
				if(isBlankLabelType &&  isBlankBarcodeType){
					
					$('#ErrorMessage').hide();
					 
					if(checkedRadioPart1 && isTest){
						
						var varArea=$('#Area').val();//$("#Area option:selected").text();
						var varLocation = $('#Location').val();
						var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
						var errMsgId = document.getElementById("Perror");
						$.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintGetLocation",{Area : varArea,Location:varLocation
							,fullfillmentCenter:fullfillmentCenterID},function(data, status) {
											if (data.length==0) {
												errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${LocationLabelPrintSceernLabelObj.getERR_Invalid_And_Location()}';
												$('#ErrorMessage').show();
										
											} else {
												$('#ErrorMessage').hide();	
											bootbox.alert('in progress...!',function() {	} );										
												 //alert('in progress...!');					
											}
										});//end check location
						
						
					}	//end part 1
					
					else if(checkedRadioPart2 && isTest){
						
						
						var varWizardControlNumber = $('#WizardControlNumber').val();
						var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
						var errMsgId = document.getElementById("Perror");
						$.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintGetGetWizardControlNumber",{WizardControlNumber : varWizardControlNumber,
							fullfillmentCenter:fullfillmentCenterID},function(data, status) {
											if (data.length==0) {
												errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${LocationLabelPrintSceernLabelObj.getERR_Invalid_Wizard_Control_Number()}';
												$('#ErrorMessage').show();
												
												
										
											} else {
												$('#ErrorMessage').hide();	
									bootbox.alert('in progress...!',function() {	} );													
												// alert('in progress...!');						
											}
										});//	
						
						
						
					}//end pasrt two
					else if(checkedRadioPart3 && isTest){
						
						
						var varFromArea = $('#FromArea').val();
						var varFromLocation = $('#FromAndLocation').val();
						var varToLocation = $('#ToLocation').val();
						var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
						
						$.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintGetLocation",{Area : varFromArea,Location: varFromLocation
							,fullfillmentCenter:fullfillmentCenterID},function(data, status) {
							if (data.length==0) {
								var errMsgId = document.getElementById("Perror");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${LocationLabelPrintSceernLabelObj.getERR_Invalid_From_Location()}';
								$('#ErrorMessage').show();
						
							} else {
								$('#ErrorMessage').hide();	
								
								
								$.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintGetLocation",{Area : varFromArea,Location: varToLocation,
									fullfillmentCenter:fullfillmentCenterID},function(data, status) {
									if (data.length==0) {
										var errMsgId = document.getElementById("Perror");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${LocationLabelPrintSceernLabelObj.getERR_Invalid_To_Location()}';
										$('#ErrorMessage').show();
								
									} else {
										$('#ErrorMessage').hide();	
										bootbox.alert('in progress...!',function() {	} );
									//	 alert('in progress...!');	
								
									}
								});
												
							}
						});//end check location
						
						
						
					}//end pasrt three
					else{
						$('#ErrorMessage').show();
						
					}
				}
				else{
					$('#ErrorMessage').show();
				}		
			}else{
				$('#ErrorMessage').show();
				/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = ""; */
				isSubmit = false;
			}
		
		
		}
		
		/**end selection part 1*/
		
		/** selection part 2*/
		
		if(selection2){
			
			
			var isblankFullfillmentcenter=checkFullfillmentCenter('Perror','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${LocationLabelPrintSceernLabelObj.getKP_Please_Select_Fullfillment_Center()}');
			if(isblankFullfillmentcenter){
				$('#ErrorMessage').hide();
			var isBlankAndArea = isBlankField(
					'AndArea',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Area()}');
			
			
			
			if(isBlankAndArea){
				$('#ErrorMessage').hide();
				
				
				
				var isBlankLabelType = isBlankField(
						'LabelType',
						'Perror',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Label_Type()}');
				
				
				
				if(isBlankLabelType){
					
					isBlankBarcodeType = isBlankField(
							'BarcodeType',
							'Perror',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Barcode_Type()}');
					
				}
				
					if(isBlankLabelType &&  isBlankBarcodeType && isblankFullfillmentcenter){
						var varFromArea = $('#AndArea').val();
						var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
						$.post("${pageContext.request.contextPath}/RestGetLocationLabelRePrintGetLocation",{Area : varFromArea,
							fullfillmentCenter:fullfillmentCenterID},function(data, status) {
							if (data.length==0) {
								var errMsgId = document.getElementById("Perror");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${LocationLabelPrintSceernLabelObj.getERR_LocationLabel_Reprint_Area()}';
								$('#ErrorMessage').show();
						
							} else {
								$('#ErrorMessage').hide();	
						bootbox.alert('in progress...!',function() {	} );
						 	//alert('in progress...!');	
							}
						});
						
					}	else{
						$('#ErrorMessage').show();
						
						isSubmit = false;
						
					}			
				
				
			}else{
				$('#ErrorMessage').show();
				/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = ""; */
				isSubmit = false;
			}
			}else{
				$('#ErrorMessage').show();
				/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = ""; */
				isSubmit = false;
			}
	
		}
		/** end selection part 2*/		
	}
	
	
	
	function div_show() {
	    
		var isblankFullfillmentcenter=checkFullfillmentCenter('Perror','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ '${LocationLabelPrintSceernLabelObj.getKP_Please_Select_Fullfillment_Center()}');
		
		if(isblankFullfillmentcenter){
			$('#ErrorMessage').hide();
		var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
		var errMsgId = document.getElementById("Perror");
		$.ajax({
			type : "POST",
			url : "${pageContext.request.contextPath}/setFulfillmentId",
			data :  {
		 		ffid:fullfillmentCenterID
				 
			  },
			cache: false,
						 
			success : function(response, textStatus, xhr) {
				
				if(response){
					$('#ErrorMessage').hide();
					$('#ErrorMessageGrid').hide();
					/* $('#WizardControlNumber').val(''); */
					$('#wizarControlNumberFromGrid').val('');		
					//jQuery("#WizardLocation").data("kendoGrid").dataSource.read();
			        //jQuery("#WizardLocation").data("kendoGrid").refresh();
				    var grid = $("#WizardLocation").data("kendoGrid");
				    grid.dataSource.page(1);
				    grid.dataSource.read();	     
					$('.Show_main').show();		
					$('.Show_main').css({"display":"inline-table"});
					$('.cover').show();
				}else{
					
					errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${LocationLabelPrintSceernLabelObj.getWizard_Control_Number_not_found_for_selected_Fulfillment_Center()}';
					$('#ErrorMessage').show();
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				
			}
		});
				

		}
		else{
			$('#ErrorMessage').show();
		}
	}
	
	function div_hide() {
		$('.Show_main').hide();
		$('.cover').hide();		
	}
	function Check(e)
	{
		    e.preventDefault();
	        var grid = $("#WizardLocation").data("kendoGrid");
	        var selectedItem = grid.dataItem(grid.select());
	        var varwizarControlNumber=selectedItem.wizardControlNumber;
	        $("#wizarControlNumberFromGrid").val(varwizarControlNumber);
	        
	}
	
	function reassign()
	{
		$('#ErrorMessageGrid').hide();
		var wizarControlNumbere=document.getElementById("wizarControlNumberFromGrid").value;
		
		var isBlankwizarControlNumbere=isBlankField('wizarControlNumberFromGrid', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ '${LocationLabelPrintSceernLabelObj.getERR_Please_select_Wizard_Control_Number()}');
		
		if(isBlankwizarControlNumbere){
			$('#ErrorMessageGrid').hide();
			$('#WizardControlNumber').val(wizarControlNumbere);
			var fullfillmentCenterID=$('#fullfillmentCenter  option:selected').val();
				 var varWizardControlNumber = $('#WizardControlNumber').val();
				 $.post("${pageContext.request.contextPath}/RestGetLocationLabelPrintGetGetWizardControlNumber",{WizardControlNumber : varWizardControlNumber
					 ,fullfillmentCenter:fullfillmentCenterID},function(data, status) {
						if (data.length==0) {
							//document.getElementById('LabelType').value = '';
						} else {
							/* var vl=data[0].trim();
							document.getElementById('LabelType').value = vl; */		
						}
					});//	
				
			div_hide();
		}
		else {
					
	    	$('#ErrorMessageGrid').show();
				
		}
	}
			  function headerChangeLanguage(){
		  	
		  	/* alert('in security look up'); */
		  	
		   $.ajax({
				url: '${pageContext.request.contextPath}/HeaderLanguageChange',
				type: 'GET',
		        cache: false,
				success: function(
						data1) {

					if (data1.boolStatus) {

						 location.reload(); 
						
					}
				}
			});
		  	
		  	
		  }

			  //to check fufillment center is blank or not //sarvendra tyagi
			  function checkFullfillmentCenter(errMsgId,errMsg){
				  var vartext=$('#fullfillmentCenter option:selected').val();
				  var varlength=vartext.trim().length
				  var errMsgId=document.getElementById(errMsgId);
				  if(varlength>0){
					  errMsgId.innerHTML = "";
					  return true;
				  }else{
					  errMsgId.innerHTML = errMsg;
					  return false;
				  }
				  
			  }
			  
		  function headerInfoHelp(){
		  	
		  	//alert('in security look up');
		  	
		   	/* $
		  	.post(
		  			"${pageContext.request.contextPath}/HeaderInfoHelp",
		  			function(
		  					data1) {

		  				if (data1.boolStatus) {

		  					location.reload();
		  				}
		  			});
		  	 */
		   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
		  			  {InfoHelp:'LocationLabelPrint',
		   		InfoHelpType:'PROGRAM'
		  		 
		  			  }, function( data1,status ) {
		  				  if (data1.boolStatus) {
		  					  window.open(data1.strMessage); 
		  					  					  
		  					}
		  				  else
		  					  {
		  					//  alert('No help found yet');
		  					  }
		  				  
		  				  
		  			  });
		  	
		  } 	
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>