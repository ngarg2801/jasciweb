<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date" %>
<!DOCTYPE html>
<style>
#AddBtn{
margin-left: 92.1%;
margin-bottom: 1%;
}
.k-grid th.k-header,
.k-grid-header
{
  /*  text-align:center !important; */
}
.k-grid-content>table>tbody>tr
{
    
  
}

.fa-remove:before, .fa-close:before, .fa-times:before,.fa-check:before,.fa-search:before ,.fa-caret-up{
	margin-right: 5px !important;
}

.fa-plus:before{
margin-right: 5px !important;
}

</style>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<!-- <div id="container" style="position: relative" class="loader_div"> -->
	<div class="body">
	
	<!-- END PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${ViewLabels.getTeamMemberMessages_TMLookUp()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			<div class="page-toolbar">
				<!-- BEGIN THEME PANEL -->
				<div class="btn-group btn-theme-panel">
					<a data-toggle="dropdown" class="btn dropdown-toggle" href="javascript:;">
					<!--<i class="icon-settings"></i>-->
					</a>
					<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<h3>THEME COLORS</h3>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li data-theme="default" class="theme-color theme-color-default">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Default</span>
											</li>
											<li data-theme="blue-hoki" class="theme-color theme-color-blue-hoki">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Hoki</span>
											</li>
											<li data-theme="blue-steel" class="theme-color theme-color-blue-steel">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Steel</span>
											</li>
											<li data-theme="yellow-orange" class="theme-color theme-color-yellow-orange">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Orange</span>
											</li>
											<li data-theme="yellow-crusta" class="theme-color theme-color-yellow-crusta">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Yellow Crusta</span>
											</li>
										</ul>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li data-theme="green-haze" class="theme-color theme-color-green-haze">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Green Haze</span>
											</li>
											<li data-theme="red-sunglo" class="theme-color theme-color-red-sunglo">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Sunglo</span>
											</li>
											<li data-theme="red-intense" class="theme-color theme-color-red-intense">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Intense</span>
											</li>
											<li data-theme="purple-plum" class="theme-color theme-color-purple-plum">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Plum</span>
											</li>
											<li data-theme="purple-studio" class="theme-color theme-color-purple-studio">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Studio</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 seperator">
								<h3>LAYOUT</h3>
								<ul class="theme-settings">
									<li>
										 Layout
										<select data-placement="left" data-container="body" data-original-title="Change layout type" class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="boxed">Boxed</option>
											<option value="fluid">Fluid</option>
										</select>
									</li>
									<li>
										 Top Menu Style
										<select data-placement="left" data-container="body" data-original-title="Change top menu dropdowns style" class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="dark">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Top Menu Mode
										<select data-placement="left" data-container="body" data-original-title="Enable fixed(sticky) top menu" class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips">
											<option value="fixed">Fixed</option>
											<option selected="selected" value="not-fixed">Not Fixed</option>
										</select>
									</li>
									<li>
										 Mega Menu Style
										<select data-placement="left" data-container="body" data-original-title="Change mega menu dropdowns style" class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="dark">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Mega Menu Mode
										<select data-placement="left" data-container="body" data-original-title="Enable fixed(sticky) mega menu" class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="fixed">Fixed</option>
											<option value="not-fixed">Not Fixed</option>
										</select>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END THEME PANEL -->
			</div>
			<!-- END PAGE TOOLBAR -->
		</div>
	</div>

	
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					${ViewLabels.getTeamMemberMessages_SearchLookup()}
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				
							<div class="form-body">
												
							<div id="ErrorMessage" class="note note-danger" style="display:none ; margin-left:15px;">
     <p id="Perror" class="error error-Top" style="margin-left: -7px !important;"></p>	
    </div>					
													
			</div><br/><br/><br/>
				<div class="col-md-6">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue" style="margin-right: -16px; margin-left: -1px;">
						<div class="portlet-title">
							<div class="caption" style="color:#000000;font-weight: 600;">
							${ViewLabels.getTeamMemberMessages_TeamMemberMaintenance()}
							</div>
							
						</div>
					</div>
					<script>
					function Check(e)
					{
						
						
                            e.preventDefault();
					        var grid = $("#TeamMemberList").data("kendoGrid");
					        var selectedItem = grid.dataItem(grid.select());
                            var teamMember=selectedItem.teammember;
                         
                           
                            window.location="${pageContext.request.contextPath}/TeamMemberMessages_Get/"+teamMember;
                           // window.location="${pageContext.request.contextPath}/TeamMemberMessagesMaintenanceEdit"; });
						  
					
					}
					</script>
						<div class="row" style="margin-top: 40px;">
						
						<kendo:grid name="TeamMemberList" id="TeamMemberList" selectable="multiple,row" 
						change="Check" resizable="true"  sortable="true" autoSync="true">
   <%--   <kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5" >
    	</kendo:grid-pageable>
 --%>
                   <kendo:grid-columns>
                   <kendo:grid-column template="<input type='checkbox'  class='checkboxId'/>" width="11%" >
                   
                   </kendo:grid-column>
					<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_Name()}" field="sortname" />
					<kendo:grid-column title="FirstName"
      field="firstname" hidden="true"/>
      <kendo:grid-column title="LastName"
      field="lastname" hidden="true"/>
					<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_Department()}" field="department" />
					<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_Shift()}" field="shiftcode" />


				</kendo:grid-columns>


      <kendo:dataSource autoSync="true">
       <kendo:dataSource-transport>
        <kendo:dataSource-transport-read cache="false"
         url="${pageContext.request.contextPath}/Teammember_search/Grid/read"></kendo:dataSource-transport-read>



        <kendo:dataSource-transport-parameterMap>

         <script>
                  function parameterMap(options,type) { 
                   
                    return JSON.stringify(options);
                   
                  }
                  
                
                  </script>
        </kendo:dataSource-transport-parameterMap>

       </kendo:dataSource-transport>
       
       <kendo:dataSource-schema>
                <kendo:dataSource-schema-model id="intKendoID">
                    <kendo:dataSource-schema-model-fields>
                        <kendo:dataSource-schema-model-field name="intKendoID" >
                        	
                        </kendo:dataSource-schema-model-field>
                        <kendo:dataSource-schema-model-field name="firstname" type="string" >
                        	
                        </kendo:dataSource-schema-model-field> 
                        <kendo:dataSource-schema-model-field name="lastname" type="string" >
                        	
                        </kendo:dataSource-schema-model-field>    
                    </kendo:dataSource-schema-model-fields>
                </kendo:dataSource-schema-model>
            </kendo:dataSource-schema>
       
       
       
      </kendo:dataSource>
     </kendo:grid>
									
				</div>	
					
				
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
				<!--end tabbable-->
				
				<!--end tabbable-->
				
			</div>
			<div class="margin-bottom-5-right-allign_teammessage_lookup" >
										
										    <button class="btn btn-sm yellow  margin-bottom" onclick="sortData();" id="sortBtn"><i class="fa fa-caret-up"></i>${ViewLabels.getTeamMemberMessages_SortFirstName()}</button>
											<button class="btn btn-sm yellow   margin-bottom" id="validate" ><i class="fa fa-check"></i>${ViewLabels.getTeamMemberMessages_MessageAllSelected()}</button></div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	
						
	<!-- END PAGE CONTENT -->
	</div>
	
<!-- </div> --></tiles:putAttribute>


</tiles:insertDefinition>

<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->

<!-- END PRE-FOOTER -->
<!-- BEGIN FOOTER -->

<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>
<script>
function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);               


 var itemss =[];
 checkedIds={};
   $('#TeamMemberList').on("click", ".checkboxId", function (e) 

                  {
                	   
                	   	var checked = $(this).is(':checked');
                	  //  row = $(this).closest("tr");
                	   	var rowIndex = $(this).parent().index();
                	     var cellIndex = $(this).parent().parent().index();
                 	    var grid = $("#TeamMemberList").data("kendoGrid");
                        var dataItem = grid.dataItem(grid.tbody.find('tr:eq(' + cellIndex + ')'));
                     
                       

                       // checkedIds[dataItem1.id] = checked;
            
                        if (checked) {
                            itemss.push(dataItem.teammember);
                          //  row.addClass("k-state-selected");
                          } 
                        else
                        {
                            //itemss.pop(dataItem.teammember);
                            Array.prototype.remove = function(envy) { this.splice(this.indexOf(envy) == -1 ? this.length : this.indexOf(envy), 1); }
                            itemss.remove(dataItem.teammember);
                         //   row.removeClass("k-state-selected");
                        } 
                       
                  }
 
 ); 
 
 
 
 function onDataBound(e) {
     var view = this.dataSource.view();
     for(var i = 0; i < view.length;i++){
         if(checkedIds[view[i].id]){
             this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                 .addClass("k-state-selected")
                 .find(".checkbox")
                 .attr("checked","checked");
         }
     }           
 }


 
 $('#validate').click(function() {

		var myJsonString = JSON.stringify(itemss);
		if(itemss.length!=0)
			{
			$('#ErrorMessage').hide();			
		 $.post( "${pageContext.request.contextPath}/controllerMethod",
				  {TeamMemberList:myJsonString}, function( data,status ) 
				  {
				 
					  window.location.href='TeamMemberMessagesMaintenanceNew';
				 
				  });
				  
			}
		else
			{
			var errMsgId = document.getElementById("Perror");
			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${ViewLabels.getTeamMemberMessages_MessageALLError()}';
			$('#ErrorMessage').show();
       	    document.location.href = '#top';

			
			}
		
		
 });
 
                   </script>
<script>
  
    	

function sortData()
{
	
	
 
var Button=document.getElementById("sortBtn");
 var text=Button.innerHTML;
 if(text==='<i class=\"fa fa-caret-up\"></i>${ViewLabels.getTeamMemberMessages_SortFirstName()}')
  {
  
  Button.innerHTML="<i class=\"fa fa-caret-up\"></i>${ViewLabels.getTeamMemberMessages_SortLastName()}";
  var kendoGridData=$("#TeamMemberList").data('kendoGrid');
  var dsSort = [];
  dsSort.push({ field: "firstname", dir: "asc" });
  kendoGridData.dataSource.sort(dsSort);  
  }
 else if(text==='<i class=\"fa fa-caret-up\"></i>${ViewLabels.getTeamMemberMessages_SortLastName()}')
  
  {
	//  Button.innerHTML="<i class=\"fa fa-check\"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SortFirstName()}";

	 
	 Button.innerHTML="<i class=\"fa fa-caret-up\"></i>${ViewLabels.getTeamMemberMessages_SortFirstName()}";
  var kendoGridData=$("#TeamMemberList").data('kendoGrid');
  var dsSort = [];
  dsSort.push({ field: "lastname", dir: "asc" });
  kendoGridData.dataSource.sort(dsSort); 
  
  } 
 
}
   
</script>
<script>
jQuery(document).ready(function() { 
	

	var kendoGridData = $("#TeamMemberList").data('kendoGrid');
    var dsSort = [];
    dsSort.push({
     field : "lastname",
     dir : "asc"
    });
    
    kendoGridData.dataSource.sort(dsSort);
    
	$(window).keydown(function(event){
	       if(event.keyCode == 13) {
	         event.preventDefault();
	         return false;
	       }
	     });
  // Metronic.init(); // init metronic core componets
   //Layout.init(); // init layout

	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);

	
	
	});
	
	
	
function headerChangeLanguage(){
	   
	   /* alert('in security look up'); */
	   
	   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	   
	   
	  }

	  function headerInfoHelp(){
	   
	   //alert('in security look up');
	   
	    /* $
	   .post(
	     "${pageContext.request.contextPath}/HeaderInfoHelp",
	     function(
	       data1) {

	      if (data1.boolStatus) {

	       location.reload();
	      }
	     });
	    */
	     $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	       {InfoHelp:'TeamMemberMessage',
	     InfoHelpType:'PROGRAM'
	     
	       }, function( data1,status ) {
	        if (data1.boolStatus) {
	         window.open(data1.strMessage); 
	                
	       }
	        else
	         {
	       //  alert('No help found yet');
	         }
	        
	        
	       });
	   
	  }
 
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>