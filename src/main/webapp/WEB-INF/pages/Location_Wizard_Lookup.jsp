   <!-- 
Date Developed  Dec 30 2014
Description It is used to Search record on behalf of wizard control no,Area,LocationType and Any Part of the Wizard Description 
Created By Pradeep Kumar -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${Location_Wizard_Lookup_Level.getKP_Location_Wizard_Lookup()}</h1>
					</div>
				</div>
				<%-- <!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014É World Wide 
*/

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

	
	
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT --> --%>
				<div class="page-content" id="page-content">
					<div class="container">


						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">



						<%-- 	<div id="ErrorMessage" class="note note-danger"	style="display: none;">
								<p class="error"
									style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p>
							</div>
							 --%>
	<div id="ErrorMessage" class="note note-danger CustomHide" style="display:none">
     <p id="MessageRestFull" class="error error-Top"></p>	
    </div>
							<div class="col-md-12">
								<form:form name="myForm"
									class="form-horizontal form-row-seperated" action="Wizard_location"
									onsubmit="return isformSubmit();" method="get">
									<div class="portlet">

										<div class="portlet-body">
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">
															<div class="form-group">
																<label class="col-md-2 control-label">${Location_Wizard_Lookup_Level.getKP_Wizard_Control_Number()}:</label>
																<div class="col-md-10" id="wizardDiv">
																	<input type="text" class="form-controlwidth"
																		name="WIZARD_CONTROL_NUMBER" id="WizardControlNumberID"
																		><i
																		class="fa fa-search SearchIconWizardControlCustomCss" 
																		id="wizardconSearch"
																		
																		onclick="actionForm('Wizard_Location_Search_Lookup','WizardControlNumber');"></i>
																</div>
															</div>
															
															<div class="form-group hidden">
																<label class="col-md-2 control-label">${Location_Wizard_Lookup_Level.getKP_Wizard_Control_Number()}:</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="search" id="searchid"
																		><i
																		class=" SearchIconWizardControlCustomCss"
																		
																		onclick="actionForm('Location_Wizard_Maintenance_Edit','WizardControlNumber');"></i>
																</div>
															</div>
															
															<div class="form-group hidden">
																<label class="col-md-2 control-label">${Location_Wizard_Lookup_Level.getKP_Wizard_ID()}</label>
																<div class="col-md-10">
																	<c:set var="StrSelectArea"
																		value="${SelectArea}" />
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="WIZARD_ID" id="WizardID">
																		 <option value="Locations">${Location_Wizard_Lookup_Level.getKP_Select()}...</option> 
																	<%-- <c:forEach items="${Drop_Down_Wizard_ID}"
																			var="AreaList">
																			
																		<option
																				value='${AreaList.getDescription20()}'>${AreaList.getDescription20()}</option>
																		</c:forEach> --%>
																				</select> <i class="fa fa-search SearchIconCustomCss"
																		
																		onclick="actionForm('Location_Wizard_Maintenance_Edit','Wizard_ID');"></i>
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${Location_Wizard_Lookup_Level.getKP_Area()}:</label>
																<div class="col-md-10" id="areaDiv">
																	<c:set var="StrSelectArea"
																		value="${SelectArea}" />
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="AREA" id="AreaID">
																		<option value="">${Location_Wizard_Lookup_Level.getKP_Select()}...</option>
																		<c:forEach items="${Location_Wizard_Maintenance_Drop_Down}"
																			var="AreaList">
																		<option
																				value='${AreaList.getGeneralCode()}'>${AreaList.getDescription20()}</option>
																		</c:forEach>
																		<%-- <c:forEach items="${StrSelectArea}"
																			var="SelectArea">

																			<option>${SelectArea}</option>
																		</c:forEach>
															 --%>		</select> <i class="fa fa-search SearchIconCustomCss" id="areaSearch"
																		
																		onclick="actionForm('Wizard_Location_Search_Lookup','Area');"></i>
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${Location_Wizard_Lookup_Level.getKP_Location_Type()}:</label>
																<div class="col-md-10" id="locationDiv">
																	<c:set var="StrLocationType"
																		value="${SelectLocationType}" />
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="LocationType" id="LocationTypeID">
																		<option value="">${Location_Wizard_Lookup_Level.getKP_Select()}...</option>
																		<c:forEach items="${Drop_Down_Location_Type}"
																			var="AreaList">
																		<option
																				value='${AreaList.getGeneralCode()}'>${AreaList.getDescription20()}</option>
																		</c:forEach>
																		<%-- <c:forEach items="${StrLocationType}"
																			var="SelectLocationType">

																			<option>${SelectLocationType}</option>
																		</c:forEach> --%>
																	</select> <i class="fa fa-search SearchIconCustomCss" id="locationserach"
																	
																		onclick="actionForm('Wizard_Location_Search_Lookup','LocationType');"></i>
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${Location_Wizard_Lookup_Level.getKP_Any_Part_of_Wizard_Description()}</label>
																<div class="col-md-10" id="anypartDiv">
																	<input type="text" class="form-controlwidth"
																		name="AnyPartofWizardDescription" id="AnyPartofWizardDescriptionID"
																		> <i
																		class="fa fa-search SearchIconCustomCss" id="anypartdescription"
																	   
																		onclick="actionForm('Wizard_Location_Search_Lookup','AnyPartofWizardDescription');"></i>
																</div>
															</div>
															
															


															<div
																class="margin-bottom-5-right-allign_icon_maintenanace Custom_NewBtn">
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom"
																	onclick="actionForm('new','new');">
																	<i class="fa fa-plus"></i>&nbsp;${Location_Wizard_Lookup_Level.getKP_New()}
																</button>
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom"
																	id="BtnDisplayAllID"
																	onclick="actionForm('displayAll','displayAll');">
																	<i class="fa fa-check"></i>&nbsp;${Location_Wizard_Lookup_Level.getKP_Display_All()}
																</button>
																
																
																	
																	<div class="form-group hidden">
																<label class="col-md-2 control-label">${Location_Wizard_Lookup_Level.getKP_Wizard_Control_Number()}</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="searchValue" id="searchValue"
																		>
																		<input type="text" class="form-controlwidth"
																		name="FromLookup" id="FromLookup" value="FromLookup"
																		><i
																		class=" SearchIconWizardControlCustomCss"
																		
																		onclick="actionForm('Location_Wizard_Maintenance_Edit','WizardControlNumber');"></i>
																</div>
															</div>													
															</div>
															

														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form:form>

							</div>
							<!--end tabbable-->

							<!--end tabbable-->

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->





			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<!-- This is used to Set Custom Css that created by Pradeep Kumar for wizard Location's all Screens -->
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/WizardLocation.css"/>"
	rel="stylesheet" />

<script>
function checkBlank(fieldID){
	var fieldLength = document.getElementById(fieldID).value.trim().length;
	if (fieldLength>0) {
		
		return true;

	}
	else {
		
		
		return false;
	}
} 

jQuery(document).ready(function() {  
	
	var wizardControlNoEnter="";
	var AnyWizardDescriptionEnter="";
	
	
	
	/* $('#WizardControlNumberID').keydown(

	          function() {
	        	  document.getElementById('wizardconSearch').focus();
	        	  alert("hi");
	           
	            
	          });
	
	$('#AreaID').keydown(

	          function() {
	        	  document.getElementById('areaSearch').focus();
	        	  alert("hi");
	            
	          });
	
	$('#LocationTypeID').keydown(

	          function() {
	        	  document.getElementById('locationserach').focus();
	        	  alert("hi");
	            
	          });
	
	$('#AnyPartofWizardDescriptionID').keydown(

	          function() {
	        	  document.getElementById('anypartdescription').focus();
	           
	        	  alert("hi");
	          }); */
	
 	/* $('#WizardControlNumberID').keypress(

	          function() {
	            wizardControlNoEnter = document.getElementById('WizardControlNumberID').value;
	           
	            
	          }); */
	        /*   
	          $('#wizardDiv').children().blur(function(){
	        	  $('#wizardconSearch').focus();
	        	  });
	
	$('#WizardControlNumberID').keyup(

	          function() {
	            wizardControlNoEnter = document.getElementById('WizardControlNumberID').value;
	            
	          }); */
	   
	/*  $('#AnyPartofWizardDescriptionID').keypress(

	          function() {
	            AnyWizardDescriptionEnter = document.getElementById('AnyPartofWizardDescriptionID').value;
	            
	          }); */
	 
	 $('#AnyPartofWizardDescriptionID').keyup(

	          function() {
	            AnyWizardDescriptionEnter = document.getElementById('AnyPartofWizardDescriptionID').value;
	            
	          });
	 
	          $(window).keydown(function(event){
	        	    if(event.keyCode == 13) {
	        	     
	        	    	var BoolAnyPartofWizardDescriptionID = checkBlank('AnyPartofWizardDescriptionID');
	        	    	var BoolLocationTypeID = checkBlank('LocationTypeID');
	        	    	var BoolAreaID = checkBlank('AreaID');
						
							var BoolWizardControlNumberID= checkBlank('WizardControlNumberID');
							
							if(BoolWizardControlNumberID){
							
							$('#wizardconSearch').click();
							}else if(BoolAreaID){
							
							
							$('#areaSearch').click();
							
							}else if(BoolLocationTypeID){
							
							$('#locationserach').click();
							}else if(BoolAnyPartofWizardDescriptionID){
							
							$('#anypartdescription').click();
							}/* else{
							$('#BtnDisplayAllID').click();
							} */
						 event.preventDefault();
	        	 return false;
						}
						});

	
	  
	setInterval(function () {
		
    var h = window.innerHeight;
    if(window.innerHeight>=900 || window.innerHeight==1004){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
 
});
		  function headerChangeLanguage(){
		  	
		  	/* alert('in security look up'); */
		  	
		   $.ajax({
				url: '${pageContext.request.contextPath}/HeaderLanguageChange',
				type: 'GET',
		        cache: false,
				success: function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				}
			});
		  	
		  	
		  }

		  function headerInfoHelp(){
		  	
		  	//alert('in security look up');
		  	
		   	/* $
		  	.post(
		  			"${pageContext.request.contextPath}/HeaderInfoHelp",
		  			function(
		  					data1) {

		  				if (data1.boolStatus) {

		  					location.reload();
		  				}
		  			});
		  	 */
		   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
		  			  {InfoHelp:'LocationWizard',
		   		InfoHelpType:'PROGRAM'
		  		 
		  			  }, function( data1,status ) {
		  				  if (data1.boolStatus) {
		  					  window.open(data1.strMessage); 
		  					  					  
		  					}
		  				  else
		  					  {
		  					//  alert('No help found yet');
		  					  }
		  				  
		  				  
		  			  });
		  	
		  } 
</script>
<script>
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(url, action) {
		$('#ErrorMessage').hide();

		if (action == 'WizardControlNumber') {
			$('#searchid').val('WIZARD_CONTROL_NUMBER');
			var validWizardControlNumber = isBlankField('WizardControlNumberID', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Lookup_Level.getKP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK()}');

			var BlankArea = document
					.getElementById("AreaID"); // Get text field
			var BlankLocationtype = document.getElementById("LocationTypeID");
			var BlankAnyPartofWizardDescription = document.getElementById("AnyPartofWizardDescriptionID");
			var WizardID =$('#WizardID').val();
			BlankArea.value = "";
			BlankLocationtype.value = "";
			BlankAnyPartofWizardDescription.value = "";

			if (validWizardControlNumber) {

				var WizardControlNumberValue = $('#WizardControlNumberID').val();
				
				$
				.post(
						"${pageContext.request.contextPath}/RestGetLocationWizardData",
						{
							WIZARD_ID : WizardID,
							WIZARD_CONTROL_NUMBER : WizardControlNumberValue

						},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Location_Wizard_Lookup_Level.getKP_ERR_INVALID_WIZARD_CONTROL_NUMBER()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										//isSubmit = true;
										$('#searchValue').val(WizardControlNumberValue);
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post info help

			}
			else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		}//end infohelp if 

		else if (action == 'Area') {
			$('#searchid').val('AREA');
			var validdescription = isBlankField('AreaID',
					'MessageRestFull', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Lookup_Level.getKP_ERR_AREA_LEFT_BLANK()}');

			var BlankWizardControlNumber = document.getElementById("WizardControlNumberID"); // Get text field
			var BlankLocationtype = document.getElementById("LocationTypeID");
			var BlankAnyPartofWizardDescription = document.getElementById("AnyPartofWizardDescriptionID");
			var WizardID =$('#WizardID').val();
			BlankWizardControlNumber.value = "";
			BlankLocationtype.value = "";
			BlankAnyPartofWizardDescription = "";
			if (validdescription) {
	
				var AreaValue = $('#AreaID option:selected').val();
				
				$.post(
								"${pageContext.request.contextPath}/RestGetLocationWizardAreaSearch",
								{
									WIZARD_ID : WizardID,
									Area : AreaValue
									
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Location_Wizard_Lookup_Level.getKP_ERR_INVALID_AREA()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										//isSubmit = true;
										$('#searchValue').val(AreaValue);
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} else if (action == 'LocationType') {
			$('#searchid').val('Location_Type');
			var validWizardControlNumber = isBlankField('LocationTypeID', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Lookup_Level.getKP_ERR_LOCATION_TYPE_LEFT_BLANK()}');

			var BlankArea = document.getElementById("AreaID"); // Get text field
			var BlankWizardControlNumber = document
					.getElementById("WizardControlNumberID");
			var BlankAnyPartofWizardDescription = document
			.getElementById("AnyPartofWizardDescriptionID");
			var WizardID =$('#WizardID').val();
			BlankArea.value = "";
			BlankWizardControlNumber.value = "";
			BlankAnyPartofWizardDescription.value ="";
			if (validWizardControlNumber) {

				
				
				
				
				var LocationTypeValue = $("#LocationTypeID option:selected").val();
				
				
				$.post(
								"${pageContext.request.contextPath}/RestGetLocationWizardLocationTypeSearch",
								{
									WIZARD_ID : WizardID,
									LOCATION_TYPE : LocationTypeValue
									
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Location_Wizard_Lookup_Level.getKP_ERR_INVALID_LOCATION_TYPE()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										//isSubmit = true;
										$('#searchValue').val(LocationTypeValue);
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							
				
				
				
			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} else if (action == 'AnyPartofWizardDescription') {
			$('#searchid').val('AnyPartofWizardDiscription');
			var validAnyPartofWizardDescription = isBlankField('AnyPartofWizardDescriptionID', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Lookup_Level.getKP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK()}');

			var BlankArea = document.getElementById("AreaID"); // Get text field
			var BlankWizardControlNumber = document
					.getElementById("WizardControlNumberID");
			var BlankLocationType = document
			.getElementById("LocationTypeID");
			var WizardID =$('#WizardID').val();

			BlankArea.value = "";
			BlankWizardControlNumber.value = "";
			BlankLocationType.value ="";
			if (validAnyPartofWizardDescription) {

				
				
				
				
				var AnyPartofWizardDescriptionValue = $("#AnyPartofWizardDescriptionID").val();
				
				
				$.post(
								"${pageContext.request.contextPath}/RestGetAnyPartofWizardDiscriptionSearch",
								{
									WIZARD_ID : WizardID,
									AnyPartofWizardDiscription : AnyPartofWizardDescriptionValue
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Location_Wizard_Lookup_Level.getKP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										//isSubmit = true;
										$('#searchValue').val(AnyPartofWizardDescriptionValue);
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							
				
				
				
			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} 
		else if (action == 'displayAll') {

			var url="${pageContext.request.contextPath}/Wizard_Location_Search_Lookup";
			document.myForm.action = url;
			document.myForm.submit();

		}
		else if(action == 'new'){
			var url="${pageContext.request.contextPath}/Location_Wizard_Maintenance";
			document.myForm.action = url;
			document.myForm.submit();
		
			
		}

	}
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>