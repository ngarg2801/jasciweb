
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date" %>
<!DOCTYPE html>
<style>
.fa-remove:before, .fa-close:before, .fa-times:before,.fa-check:before,.fa-search:before {
	margin-right: 5px !important;
}

.fa-plus:before{
margin-right: 5px !important;
}

</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div class="body">
	
		
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${ViewLabels.getTeamMemberMessages_TMMLookUp()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			<div class="page-toolbar">
				<!-- BEGIN THEME PANEL -->
				<div class="btn-group btn-theme-panel">
					<a data-toggle="dropdown" class="btn dropdown-toggle" href="javascript:;">
					<!--<i class="icon-settings"></i>-->
					</a>
					<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<h3>THEME COLORS</h3>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li data-theme="default" class="theme-color theme-color-default">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Default</span>
											</li>
											<li data-theme="blue-hoki" class="theme-color theme-color-blue-hoki">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Hoki</span>
											</li>
											<li data-theme="blue-steel" class="theme-color theme-color-blue-steel">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Steel</span>
											</li>
											<li data-theme="yellow-orange" class="theme-color theme-color-yellow-orange">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Orange</span>
											</li>
											<li data-theme="yellow-crusta" class="theme-color theme-color-yellow-crusta">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Yellow Crusta</span>
											</li>
										</ul>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li data-theme="green-haze" class="theme-color theme-color-green-haze">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Green Haze</span>
											</li>
											<li data-theme="red-sunglo" class="theme-color theme-color-red-sunglo">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Sunglo</span>
											</li>
											<li data-theme="red-intense" class="theme-color theme-color-red-intense">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Intense</span>
											</li>
											<li data-theme="purple-plum" class="theme-color theme-color-purple-plum">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Plum</span>
											</li>
											<li data-theme="purple-studio" class="theme-color theme-color-purple-studio">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Studio</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 seperator">
								<h3>LAYOUT</h3>
								<ul class="theme-settings">
									<li>
										 Layout
										<select data-placement="left" data-container="body" data-original-title="Change layout type" class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="boxed">Boxed</option>
											<option value="fluid">Fluid</option>
										</select>
									</li>
									<li>
										 Top Menu Style
										<select data-placement="left" data-container="body" data-original-title="Change top menu dropdowns style" class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="dark">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Top Menu Mode
										<select data-placement="left" data-container="body" data-original-title="Enable fixed(sticky) top menu" class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips">
											<option value="fixed">Fixed</option>
											<option selected="selected" value="not-fixed">Not Fixed</option>
										</select>
									</li>
									<li>
										 Mega Menu Style
										<select data-placement="left" data-container="body" data-original-title="Change mega menu dropdowns style" class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="dark">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Mega Menu Mode
										<select data-placement="left" data-container="body" data-original-title="Enable fixed(sticky) mega menu" class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips">
											<option selected="selected" value="fixed">Fixed</option>
											<option value="not-fixed">Not Fixed</option>
										</select>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END THEME PANEL -->
			</div>
			<!-- END PAGE TOOLBAR -->
		</div>
	</div>

	
	<div class="page-content" id="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					${ViewLabels.getTeamMemberMessages_TeamMemberMaintenance()}
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				
				
							<div class="col-md-12">
					<form class="form-horizontal form-row-seperated" name="myForm" action="#" onsubmit="return isformSubmit();">
						
						<div id="ErrorMessage" class="note note-danger" style="display:none;">
						 <p id="MessageRestFull" class="error error-Top" style="margin-left:-7px;"></p>	
						
						</div>
					
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getTeamMemberMessages_EnterTeamMember()}:
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" id="TeamMember" name="TeamMember" >
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="searchTeamMember" onclick="actionForm('TeamMemberMessagesMaintenancelookup','TeamMember');"></i>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getTeamMemberMessages_PartOfTeamMember()}:
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" id="PartTeamMember" name="PartTeamMember" >
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="searchPartTeamMember" onclick="actionForm('TeamMemberMessagesMaintenancelookup','PartTeamMember');"></i>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getTeamMemberMessages_Department()}:
													</label>
													<div class="col-md-10">
														<!-- <input type="text" class="form-controlwidth"  id="Department" name="Department" > -->
														
														<select class="table-group-action-input form-control input-medium" name="Department" id="Department">
														<option value="">${ViewLabels.getTeamMemberMessages_Select()}</option>
														
														
														           <c:forEach items="${DepartmentList}" var="Department">
                                                                       <option value="${Department.getGeneralCode()}">${Department.getDescription20()}</option>
																	</c:forEach>
														</select>
														
														
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="searchDepartment" onclick="actionForm('TeamMemberMessagesMaintenancelookup','Department');"></i>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getTeamMemberMessages_Shift()}:
													</label>
													<div class="col-md-10">
														<!-- <input type="text" class="form-controlwidth" id="Shift" name="Shift" > -->
														
														
														<select class="table-group-action-input form-control input-medium" name="Shift" id="Shift">
														<option value="">${ViewLabels.getTeamMemberMessages_Select()}</option>
														
														
														           <c:forEach items="${ShiftCodeList}" var="Shiftcode">
                                                                       <option value="${Shiftcode.getGeneralCode()}">${Shiftcode.getDescription20()}</option>
																	</c:forEach>
														</select>
														
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="serachShift" onclick="actionForm('TeamMemberMessagesMaintenancelookup','Shift');"></i>
													</div>
												</div>
												
												
												<div class="margin-bottom-5-right-allign_team_memeber_lookup">
											<!--<button class="btn btn-sm yellow filter-submit margin-bottom" onclick="actionForm('TeamMemberMessagesMaintenancelookup','search');"><i class="fa fa-search"></i> Search</button>-->
										    <button class="btn btn-sm yellow filter-submit margin-bottom" id="btnDispID" onclick="actionForm('TeamMemberMessagesMaintenancelookup','displayall');"><i class="fa fa-check"></i>${ViewLabels.getTeamMemberMessages_DisplayAll()}</button></div>
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				
				<!--end tabbable-->
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	</div>
	
	</tiles:putAttribute>
	</tiles:insertDefinition>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
		type="text/javascript"></script>
<script>
jQuery(document).ready(function() 
		{
	
	      $(window).keydown(function(event){
	          if(event.keyCode == 13) {
		      var BoolTeamMember = checkBlank('TeamMember');
               var BoolPartTeamMember = checkBlank('PartTeamMember');
               var BoolDepartment = checkBlank('Department');
               var BoolShift= checkBlank('Shift');
      
     if(BoolTeamMember){
    BoolPartTeamMember=false;
    BoolDepartment=false;
    BoolShift=false;
    $('#searchTeamMember').click();
          }
    else if(BoolPartTeamMember){
    BoolTeamMember=false;
    BoolDepartment=false;
    BoolShift=false;
    $('#searchPartTeamMember').click();
          }
    else if(BoolDepartment){
    BoolTeamMember=false;
    BoolPartTeamMember=false;
    BoolShift=false;
    $('#searchDepartment').click();
          }
    else if(BoolShift){
    BoolTeamMember=false;
    BoolPartTeamMember=false;
    BoolDepartment=false;
    $('#serachShift').click();
          }
   /*  else{
    $('#btnDispID').click();
    }	 */		  
			  
	            event.preventDefault();
	            return false;
	          }
	        });
	
	
	  	setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
   //Layout.init(); // init layout
 
});
</script>
<script>
  var isSubmit = false;

  function isformSubmit() {

   return isSubmit;
  }

  function actionForm(url, action) 
  {
	  $('#ErrorMessage').hide();
	  var TeamMemberNameValue = document.getElementById("TeamMember");
	  var PartTeamMemberValue = document.getElementById("PartTeamMember");
	  var DepartmentValue = document.getElementById("Department");
	  var ShiftValue = document.getElementById("Shift");
	  
	  if (action == 'TeamMember') 
   {
       var TeamMember = isBlankField('TeamMember', 'MessageRestFull','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
       '${ViewLabels.getTeamMemberMessages_BlankTeamMember()}');

   	if (TeamMember)
	{
		PartTeamMemberValue.value = "";
		DepartmentValue.value = "";
		ShiftValue.value = "";
		ShiftValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
		DepartmentValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
		
		var TeamMember = $('#TeamMember').val();
		$.post(
						"${pageContext.request.contextPath}/RestTeamMemberMessage_CheckMemberId",
					
						{
							teamMember : TeamMember
							
						},
						function(data, status) {

							if (data) 
							{
							
								isSubmit = true;
								$('#ErrorMessage').hide();
								document.myForm.action = url;
								document.myForm.submit();
								
								// alert ("User Already Exists");
							} else {
								var errMsgId = document.getElementById("MessageRestFull");
						errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ViewLabels.getTeamMemberMessages_InvalidTM()}';
						$('#ErrorMessage').show();
						isSubmit = false;	
							}
						});//end post TeammemberId

	} 
   	
   	
   	
	else 
	{
		PartTeamMemberValue.value = "";
		DepartmentValue.value = "";
		ShiftValue.value = "";
		ShiftValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
		DepartmentValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
		$('#ErrorMessage').show();
		document.myForm;
		isSubmit = false;
   }
   }
   else if(action=='PartTeamMember')
   {
	   var PartTeamMember=isBlankField('PartTeamMember', 'MessageRestFull','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			   '${ViewLabels.getTeamMemberMessages_BlankPartTeamMember()}');
	   if (PartTeamMember) 
		{
		    TeamMemberNameValue.value = "";
			DepartmentValue.value = "";
			ShiftValue.value = "";
			ShiftValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
			DepartmentValue.text='${ViewLabels.getTeamMemberMessages_Select()}';

			
			var PartTeamMember = $('#PartTeamMember').val();
			$.post(
							"${pageContext.request.contextPath}/RestTeamMemberMessage_CheckMemberName",
							
							{
								partTeamMember : PartTeamMember
								
							},
							function(data, status) {

								if (data) 
								{
								
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
									
									// alert ("User Already Exists");
								} else {
									var errMsgId = document.getElementById("MessageRestFull");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${ViewLabels.getTeamMemberMessages_InvalidPartOfTm()}';
							$('#ErrorMessage').show();
							isSubmit = false;	
								}
							});//end post Part of Teammember Name


		} 
		else 
		{
			 TeamMemberNameValue.value = "";
				DepartmentValue.value = "";
				ShiftValue.value = "";
				ShiftValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
				DepartmentValue.text='${ViewLabels.getTeamMemberMessages_Select()}';

			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
	   }
	 
   }
   else if(action=='Department')
   {
	   var Department=isBlankField('Department', 'MessageRestFull','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
			   '${ViewLabels.getTeamMemberMessages_BlankDepartment()}');
	   if (Department) 
		{
		    TeamMemberNameValue.value = "";
		    PartTeamMemberValue.value = "";
			ShiftValue.value = "";
			ShiftValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
			
			var Department = $('#Department').val();
			$.post(
							"${pageContext.request.contextPath}/RestTeamMemberMessage_CheckDepartment",
							
							{
								department : Department
								
							},
							function(data, status) {

								if (data) 
								{
								
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
									
									// alert ("User Already Exists");
								} else {
									var errMsgId = document.getElementById("MessageRestFull");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${ViewLabels.getTeamMemberMessages_InvalidDepartment()}';
							$('#ErrorMessage').show();
							isSubmit = false;	
								}
							});//end post TeammemberId

		} 
		else 
		{
			 TeamMemberNameValue.value = "";
			    PartTeamMemberValue.value = "";
				ShiftValue.value = "";
				DepartmentValue.value="";
				ShiftValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
				DepartmentValue.text='${ViewLabels.getTeamMemberMessages_Select()}';

			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
	   }
	  
	    
   }
   else if(action=='Shift')
   {
	   var Shift=isBlankField('Shift', 'MessageRestFull','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
	    '${ViewLabels.getTeamMemberMessages_BlankShift()}');
	   if (Shift) 
		{
		    TeamMemberNameValue.value = "";
		    PartTeamMemberValue.value = "";
		    DepartmentValue.value = "";
			DepartmentValue.text='${ViewLabels.getTeamMemberMessages_Select()}';

			var Shift = $('#Shift').val();
			$.post(
							"${pageContext.request.contextPath}/RestTeamMemberMessage_CheckShift",
							
							{
								shift : Shift
								
							},
							function(data, status) {

								if (data) 
								{
								
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
									
									// alert ("User Already Exists");
								} else {
									var errMsgId = document.getElementById("MessageRestFull");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${ViewLabels.getTeamMemberMessages_InvalidShift()}';
							$('#ErrorMessage').show();
							isSubmit = false;	
								}
							});//end post TeammemberId

		} 
		else 
		{
			TeamMemberNameValue.value = "";
		    PartTeamMemberValue.value = "";
		    DepartmentValue.value = "";
		    ShiftValue.value="";
		    ShiftValue.text='${ViewLabels.getTeamMemberMessages_Select()}';
			DepartmentValue.text='${ViewLabels.getTeamMemberMessages_Select()}';

			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
	   }
	  
   }
   
   else if (action == 'displayall') {

	   var elem = document.getElementById("TeamMember"); // Get text field
	    elem.value = "";
	    var elem1 = document.getElementById("PartTeamMember"); // Get text field
	    elem1.value = "";
	    var elem2 = document.getElementById("Department"); // Get text field
	    elem2.value = "";
	    var elem3 = document.getElementById("Shift"); // Get text field
	    elem3.value = "";
	    elem2.text='${ViewLabels.getTeamMemberMessages_Select()}';
	    elem3.text='${ViewLabels.getTeamMemberMessages_Select()}';

    
    document.myForm.action = url;
    document.myForm.submit();
    $('#ErrorMessage').hide();
    isSubmit = true;
   }

  }
  
  
  function headerChangeLanguage(){
	   
	   /* alert('in security look up'); */
	   
$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	   
	   
	  }

	  function headerInfoHelp(){
	   
	   //alert('in security look up');
	   
	    /* $
	   .post(
	     "${pageContext.request.contextPath}/HeaderInfoHelp",
	     function(
	       data1) {

	      if (data1.boolStatus) {

	       location.reload();
	      }
	     });
	    */
	     $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	       {InfoHelp:'TeamMemberMessage',
	     InfoHelpType:'PROGRAM'
	     
	       }, function( data1,status ) {
	        if (data1.boolStatus) {
	         window.open(data1.strMessage); 
	                
	       }
	        else
	         {
	       //  alert('No help found yet');
	         }
	        
	        
	       });
	   
	  }
 </script>
<!-- END JAVASCRIPTS -->

<!-- END BODY -->
</html>