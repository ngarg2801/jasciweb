<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<!DOCTYPE html>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014É World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->


		<!-- BEGIN PAGE CONTAINER -->

		<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${objMenuOptionLabel.getLblMenuOptionMaintenanceLookUp()}</h1>
					</div>
					<!-- END PAGE TITLE -->
					
				</div>
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content" id="page-content">
				<div class="container">
					<!-- BEGIN PAGE BREADCRUMB -->
					<ul class="page-breadcrumb breadcrumb hide">
						<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
						<li class="active">message maintenance</li>
					</ul>
					<!-- END PAGE BREADCRUMB -->
					<!-- BEGIN PAGE CONTENT INNER -->

					<div id="ErrorMessage1" class="note note-danger"
						style="display: none">
						<span class="error error-Top" id="error1"
							style="margin-left: -7px !important;"></span>
					</div>
					<div class="row margin-top-10">
						<div class="col-md-12">
							<form:form name="myForm"
								class="form-horizontal form-row-seperated" action="#"
								onsubmit="return isformSubmit();" method="get">
								<div class="portlet">

									<div class="portlet-body">
										<div class="tabbable">

											<div class="tab-content no-space">
												<div class="tab-pane active" id="tab_general">
													<div class="form-body">
														<div class="form-group">
															<label class="col-md-2 control-label">${objMenuOptionLabel.getLblApplication()}: </label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="lblApplication" id="lblApplication"
																	> <i
																	class="fa fa-search"
																	style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																	id="BtnApplicationID"
																	onclick="actionForm('MenuMaintenanceSearchLookUp','Applictionsearch');"></i>
															</div>
														</div>
													<!-- <div class="form-group">
															<label class="col-md-2 control-label">${objMenuOptionLabel.getLblpartOfApplication()}: </label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="lblPartApplication" id="lblPartApplication"
																	>
																<i class="fa fa-search"
																	style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																	onclick="actionForm('MenuMaintenanceSearchLookUp','PartApplictionsearch');"></i>
															</div>
														</div> -->	
														<div class="form-group">
															<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenuOption()}: </label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="lblMenuOption" id="lblMenuOption"
																	> <i
																	class="fa fa-search"
																	style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																	id="BtnMenuOptionID"
																	onclick="actionForm('Menu_Maintenance_Screen','MenuOptionsearch');"></i>

															</div>
														</div>
														<!-- <div class="form-group">
															<label class="col-md-2 control-label">${objMenuOptionLabel.getLblPartOfMenuOption()}: </label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="lblPartMenuOption" id="lblPartMenuOption"
																	> <i
																	class="fa fa-search"
																	style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																	onclick="actionForm('MenuMaintenanceSearchLookUp','PartMenuOptionsearch');"></i>
															</div>
														</div> -->
														<div class="form-group">
															<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenuType()}:
															</label>
															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="lblMenuType" id="lblMenuType">
																	<option value="">${objMenuOptionLabel.getLblSelect()}...</option>
																	<c:if test="${not empty ObjMenuTypesOption}">
											
																	<c:forEach var="MenuAssignedList" items="${ObjMenuTypesOption}">
											
																	<option value="${MenuAssignedList.getGeneralCode()}">${MenuAssignedList.getDescription20()}</option>
																	</c:forEach>
																	</c:if>
																</select> <i class="fa fa-search"
																	id="SelectSearchMenuTypeIcon"
																	style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																	onclick="actionForm('MenuMaintenanceSearchLookUp','MenuTypesearch');"></i></a>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${objMenuOptionLabel.getLblPartOFtheMenuOptionDescription()}: </label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="lblPartDescription20" id="lblPartDescription20"
																	>
																<i class="fa fa-search"
																	style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																	id="BtnPartOfDescpID"
																	onclick="actionForm('MenuMaintenanceSearchLookUp','MenuDescriptionsearch');"></i>
															</div>
														</div>
														<div class="form-group" id="TenantID">
															<label class="col-md-2 control-label">${objMenuOptionLabel.getLblTenantID()}: </label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="txtTenant" id="txtTenant"
																	> <i
																	class="fa fa-search"
																	style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																	id="BtnTenantID"
																	onclick="actionForm('MenuMaintenanceSearchLookUp','Tenantsearch');"></i></a>
															</div>
														</div>

														<div
															class="margin-bottom-5-right-allign_menu_optionmaintenance">
															<button
																class="btn btn-sm yellow filter-submit margin-bottom"
																onclick="actionForm('Menu_Maintenance_Screen','newSearch');">
																<i class="fa fa-plus"></i>&nbsp;${objMenuOptionLabel.getLblNew()}
															</button>
															<button
																class="btn btn-sm yellow filter-submit margin-bottom"
																id="BtnDisplayAllID"
																onclick="actionForm('MenuMaintenanceSearchLookUp','displayAll');">
																<i class="fa fa-check"></i>&nbsp;${objMenuOptionLabel.getLbldisplayall()}
															</button>
															</a>
														</div>



													</div>
												</div>




											</div>
										</div>
									</div>
								</div>
								
							</form:form>
							
						</div>

					</div>

					<!-- END PAGE CONTENT INNER -->
				</div>
			</div>
			<!-- END PAGE CONTENT -->
		</div>
		<!-- END PAGE CONTAINER -->
	</tiles:putAttribute>

</tiles:insertDefinition>


<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>


	
	
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}
	
	
	function checkBlank(fieldID){
		var fieldLength = document.getElementById(fieldID).value.trim().length;
		if (fieldLength>0) {
			
			return true;

		}
		else {
			
			
			return false;
		}
	} 

	$(document).ready(function() {
		var tenant='${Tenant_Id}';
		 if(tenant.toUpperCase()=="${ObjCommonSession.getJasci_Tenant()}" ){
			
		}else{
		document.getElementById('TenantID').style.display='none';
		}
		
		
		
		
		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
		     
		    	var BoolMenuTypeID = checkBlank('lblMenuType');
		    	var BoolMenuOption = checkBlank('lblMenuOption');
		    	var BoolPartDescp = checkBlank('lblPartDescription20');
		    	var BoolApplication= checkBlank('lblApplication');
		    	var BoolTenantID= checkBlank('txtTenant');
		    		
		    	 if(tenant.toUpperCase()=="${ObjCommonSession.getJasci_Tenant()}" ){
		    		
					if(BoolApplication){
					$('#BtnApplicationID').click();
					}else if(BoolMenuOption){
					
						$('#BtnMenuOptionID').click();
					}else if(BoolMenuTypeID){
					$('#SelectSearchMenuTypeIcon').click();
					}else if(BoolPartDescp){
					$('#BtnPartOfDescpID').click();
					}else if(BoolTenantID){
					$('#BtnTenantID').click();
					}/* else{
			    		
			    		$('#BtnDisplayAllID').click();
			    	} */
					
		    				    		
		    		
		    	}else{
		    	if(BoolApplication){
					$('#BtnApplicationID').click();
					}else if(BoolMenuOption){
					
						$('#BtnMenuOptionID').click();
					}else if(BoolMenuTypeID){
					$('#SelectSearchMenuTypeIcon').click();
					}else if(BoolPartDescp){
					$('#BtnPartOfDescpID').click();
					}/* else{
			    		
			    		$('#BtnDisplayAllID').click();
			    	} */
		    	}
		    	 event.preventDefault();
		         return false;
		    }
		});
		
		setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30); 
			
		
		 Metronic.init(); // init metronic core componets
		   Layout.init(); // init layout
		   Demo.init(); // init demo(theme settings page)
		 //  Index.init(); // init index page
		   Tasks.initDashboardWidget(); 
	
		 
		 
		 
		
	});
	
	
	
	function actionForm(url, action) {
		$('#ErrorMessage').hide();
		if (action == 'Applictionsearch') {

			var valid = isBlankField('lblApplication', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${objMenuOptionLabel.getLblERR_APPLICATION_LEFT_BLANK()}');

			if (valid) {

				/* var PartApplication = document
						.getElementById("lblPartApplication"); // Get text field
				PartApplication.value = ""; */

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				var Tenant = document.getElementById("txtTenant");
				Tenant.value = "";

				var valApplication = $('#lblApplication').val();
				$('#ErrorMessage1').hide();
				var FieldValue = valApplication;
				var Field = 'lblApplication';

				$.post(
								"${pageContext.request.contextPath}/MenuOption_searchlookup",
								{
									MenuOptionSearchFieldValue : FieldValue,
									MenuOptionFieldSearch : Field
								},
								function(data, status) {

									if (!data) {

										error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblERR_INVALID_APPLICATION()}';
										$('#ErrorMessage1').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage1').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/'
												+ url;
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member

			} else {
				$('#ErrorMessage1').show();
				document.myForm;
				/* var PartApplication = document
						.getElementById("lblPartApplication"); // Get text field
				PartApplication.value = ""; */

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				var Tenant = document.getElementById("txtTenant");
				Tenant.value = "";
				isSubmit = false;

			}

		}

		else if (action == 'PartApplictionsearch') {

			var validPartAppl = isBlankField('lblPartApplication', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${objMenuOptionLabel.getLblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK()}');

			if (validPartAppl) {
				var Application = document.getElementById("lblApplication"); // Get text field
				Application.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = "";
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";

				$('#ErrorMessage1').hide();

				var partoftammbername = $('#lblPartApplication').val();

				var FieldValue = partoftammbername;
				var Field = 'lblPartApplication';

				$
						.post(
								"${pageContext.request.contextPath}/MenuOption_searchlookup",
								{
									MenuOptionSearchFieldValue : FieldValue,
									MenuOptionFieldSearch : Field
								},
								function(data, status) {

									if (!data) {

										error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblERR_INVALID_APPLICATION()}';
										$('#ErrorMessage1').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage1').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/'
												+ url;
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member				

			} else {
				$('#ErrorMessage1').show();
				document.myForm;
				var PartApplication = document.getElementById("lblApplication"); // Get text field
				PartApplication.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = "";
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				var Tenant = document.getElementById("txtTenant");
				Tenant.value = "";
				isSubmit = false;

			}

		} else if (action == 'MenuOptionsearch') {

			var validPartAppl = isBlankField('lblMenuOption', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${objMenuOptionLabel.getLblERR_MENU_OPTION_LEFT_BLANK()}');

			if (validPartAppl) {
				var Application = document.getElementById("lblApplication"); // Get text field
				Application.value = "";

				/* var MenuOption = document.getElementById("lblPartApplication");
				MenuOption.value = ""; */
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";

				$('#ErrorMessage1').hide();

				var partoftammbername = $('#lblMenuOption').val();

				var FieldValue = partoftammbername;
				var Field = 'lblMenuOption';

				$
						.post(
								"${pageContext.request.contextPath}/RestFull_Check_MenuOption",
								{
									MenuOptionValue : FieldValue

								},
								function(data, status) {

									if (!data) {

										error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblERR_INVALID_MENU_OPTION()}';
										$('#ErrorMessage1').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage1').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/'
												+ url;
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member				

			} else {

				$('#ErrorMessage1').show();
				document.myForm;
				var PartApplication = document.getElementById("lblApplication"); // Get text field
				PartApplication.value = "";

				/* var MenuOption = document.getElementById("lblPartApplication");
				MenuOption.value = "";
				var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				var Tenant = document.getElementById("txtTenant");
				Tenant.value = "";
				isSubmit = false;

			}

		} else if (action == 'PartMenuOptionsearch') {

			var validPartAppl = isBlankField('lblPartMenuOption', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${objMenuOptionLabel.getLblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK()}');

			if (validPartAppl) {
				var Application = document.getElementById("lblApplication"); // Get text field
				Application.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				var PartMenuOption = document
						.getElementById("lblPartApplication");
				PartMenuOption.value = "";
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";

				$('#ErrorMessage1').hide();

				var partoftammbername = $('#lblPartMenuOption').val();

				var FieldValue = partoftammbername;
				var Field = 'lblPartMenuOption';

				$
						.post(
								"${pageContext.request.contextPath}/MenuOption_searchlookup",
								{
									MenuOptionSearchFieldValue : FieldValue,
									MenuOptionFieldSearch : Field
								},
								function(data, status) {

									if (!data) {

										error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblERR_INVALID_MENU_OPTION()}';
										$('#ErrorMessage1').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage1').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/'
												+ url;
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member				

			} else {
				$('#ErrorMessage1').show();
				document.myForm;
				var PartApplication = document.getElementById("lblApplication"); // Get text field
				PartApplication.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				var PartMenuOption = document
						.getElementById("lblPartApplication");
				PartMenuOption.value = "";
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				var Tenant = document.getElementById("txtTenant");
				Tenant.value = "";
				isSubmit = false;

			}

		} else if (action == 'MenuTypesearch') {

			var validPartAppl = isBlankField('lblMenuType', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${objMenuOptionLabel.getLblERR_MENU_TYPE_LEFT_BLANK()}');

			if (validPartAppl) {
				var Application = document.getElementById("lblApplication"); // Get text field
				Application.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = "";
				var MenuType = document.getElementById("lblPartApplication");
				MenuType.value = ""; */
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";

				$('#ErrorMessage1').hide();

				var partoftammbername = $('#lblMenuType').val();

				var FieldValue = partoftammbername;
				var Field = 'lblMenuType';

				$
						.post(
								"${pageContext.request.contextPath}/MenuOption_searchlookup",
								{
									MenuOptionSearchFieldValue : FieldValue,
									MenuOptionFieldSearch : Field
								},
								function(data, status) {

									if (!data) {

										error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblERR_INVALID_MENU_TYPE_LEFT_BLANK()}';
										$('#ErrorMessage1').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage1').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/'
												+ url;
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member				

			} else {
				$('#ErrorMessage1').show();
				document.myForm;
				var PartApplication = document.getElementById("lblApplication"); // Get text field
				PartApplication.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				/* MenuOption.value = "";
				var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = "";
				var MenuType = document.getElementById("lblPartApplication");
				MenuType.value = ""; */
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				var Tenant = document.getElementById("txtTenant");
				Tenant.value = "";
				isSubmit = false;

			}

		} else if (action == 'MenuDescriptionsearch') {

			var validPartAppl = isBlankField('lblPartDescription20', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${objMenuOptionLabel.getLblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK()}');

			if (validPartAppl) {
				var Application = document.getElementById("lblApplication"); // Get text field
				Application.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				/* var PartDescription = document
						.getElementById("lblPartApplication");
				PartDescription.value = ""; */

				$('#ErrorMessage1').hide();

				var partoftammbername = $('#lblPartDescription20').val();

				var FieldValue = partoftammbername;
				var Field = 'lblPartDescription20';

				$
						.post(
								"${pageContext.request.contextPath}/MenuOption_searchlookup",
								{
									MenuOptionSearchFieldValue : FieldValue,
									MenuOptionFieldSearch : Field
								},
								function(data, status) {

									if (!data) {

										error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION()}';
										$('#ErrorMessage1').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage1').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/'
												+ url;
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member				

			} else {
				$('#ErrorMessage1').show();
				document.myForm;
				var PartApplication = document.getElementById("lblApplication"); // Get text field
				PartApplication.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				/* var PartDescription = document
						.getElementById("lblPartApplication");
				PartDescription.value = ""; */
				var Tenant = document.getElementById("txtTenant");
				Tenant.value = "";
				isSubmit = false;

			}

		} else if (action == 'Tenantsearch') {

			var validPartAppl = isBlankField('txtTenant', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${objMenuOptionLabel.getLblErr_INVALID_TENANT_ID_LEFT_BLANK()}');

			if (validPartAppl) {
				var Application = document.getElementById("lblApplication"); // Get text field
				Application.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				/* var Tenant = document.getElementById("lblPartApplication");
				Tenant.value = ""; */

				$('#ErrorMessage1').hide();

				var partoftammbername = $('#txtTenant').val();

				var FieldValue = partoftammbername;
				var Field = 'txtTenant';

				$
						.post(
								"${pageContext.request.contextPath}/MenuOption_searchlookup",
								{
									MenuOptionSearchFieldValue : FieldValue,
									MenuOptionFieldSearch : Field
								},
								function(data, status) {

									if (!data) {

										error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${objMenuOptionLabel.getLblErr_INVALID_TENANTID()}';
										$('#ErrorMessage1').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage1').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/'
												+ url;
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member				

			} else {
				$('#ErrorMessage1').show();
				document.myForm;
				var PartApplication = document.getElementById("lblApplication"); // Get text field
				PartApplication.value = "";

				var MenuOption = document.getElementById("lblMenuOption");
				MenuOption.value = "";
				/* var PartMenuOption = document
						.getElementById("lblPartMenuOption");
				PartMenuOption.value = ""; */
				var MenuType = document.getElementById("lblMenuType");
				MenuType.value = "";
				var PartDescription = document
						.getElementById("lblPartDescription20");
				PartDescription.value = "";
				/* var Tenant = document.getElementById("txtTenant");
				Tenant.value = ""; */
				isSubmit = false;

			}

		}else if(action == 'newSearch'){
			
			document.myForm.action = '${pageContext.request.contextPath}/'
				+ url;
			isSubmit = true;
		}

		else if (action == 'displayAll') {

			
			var PartApplication = document.getElementById("lblApplication"); // Get text field
			PartApplication.value = "";

			var MenuOption = document.getElementById("lblMenuOption");
			MenuOption.value = "";
			/* var PartMenuOption = document
					.getElementById("lblPartMenuOption");
			PartMenuOption.value = ""; */
			var MenuType = document.getElementById("lblMenuType");
			MenuType.value = "";
			var PartDescription = document
					.getElementById("lblPartDescription20");
			PartDescription.value = "";
			/* var Tenant = document.getElementById("txtTenant");
			Tenant.value = ""; */
			document.myForm.action = '${pageContext.request.contextPath}/'
					+ url;
			document.myForm.submit();
			$('#ErrorMessage').hide();
			isSubmit = true;
		}

	}

	/* function actionForm(url, action) {
		
		var FieldValue='sdsd';
		var Field='lblApplication'
		 
		 $.post( "${pageContext.request.contextPath}/MenuOption_searchlookup",
			     {val1:FieldValue,
			 val2:Field}, function( data,status ) 
			     {
			    
				 if(data)
	        	 {
				
					 alert('Hello');
					 
	        	 }
			    
			     });
			     
			 
			 
			 
			
		
	} */
	
		function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'MenuOption',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>



<!-- END JAVASCRIPTS -->
</body>

<!-- END BODY -->
</html>