﻿<!-- 
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014© World Wide 
Date Developed  Dec 24 2014
Description It is used to search for Location profiles
Created By Diksha Gupta-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>
						${LocationsProfileLabels.getLocationProfiles_LookupLabel()}</h1>
					</div>
				</div>
				
	
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT --> 
				<div class="page-content" id="page-content">
					<div class="container">

<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active"></li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">



						<%-- 	<div id="ErrorMessage" class="note note-danger"	style="display: none;">
								<p class="error"
									style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p>
							</div>
							 --%>
	<div id="ErrorMessage" class="note note-danger" style="display:none;">
     <p id="MessageRestFull" class="error error-Top" style="margin-left: -7px !important;"></p>	
    </div>
							<div class="col-md-12">
								<form:form name="myForm"
									class="form-horizontal form-row-seperated" action="#"
									onsubmit="return isformSubmit();" method="get">
									<div class="portlet">

										<div class="portlet-body">
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">
															<div class="form-group">
																<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationProfile()}:</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="LocationProfile" id="LocationProfile"
																		> <i class="fa fa-search" id="BtnIdLocationProfile"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('Location_Profile_Maintenance_SearchLookup','LocationProfile');"></i>
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_ProfileGroup()}: </label>
																<div class="col-md-10">
																	<c:set var="StrSelectArea"
																		value="${SelectProfileGroup}" />
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="ProfileGroup" id="ProfileGroup">
																		<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
														
														
														           <c:forEach items="${GeneralCodeProfileGroup}" var="Profilegroup">
                                                                       <option value="${Profilegroup.getGeneralCode()}">${Profilegroup.getDescription20()}</option>
																	</c:forEach>
																	</select> <i class="fa fa-search" id="BtnIdProfileGroup"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('Location_Profile_Maintenance_SearchLookup','ProfileGroup');"></i>
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_PartOfLocation()}:</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="PartofLocationProfileDescription" id="PartofLocationProfileDescription"
																		> <i
																		class="fa fa-search"  id="BtnIdPartofLocationProfileDescription"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('Location_Profile_Maintenance_SearchLookup','PartofLocationProfileDescription');"></i>
																</div>
															</div>
															
															
															<div class="margin-bottom-5-right-allign_LocationProfile_LookUp_NewDisplayall">

																<button
																	class="btn btn-sm yellow filter-submit margin-bottom "
																	onclick="actionForm('LocationProfileMaintenance','new');">
																	<i class="fa fa-plus"></i>${LocationsProfileLabels.getLocationProfiles_NewBtn()}
																</button>
																

																 <button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdDisplayAll"
																	onclick="actionForm('Location_Profile_Maintenance_SearchLookup','displayall');">
																	<i class="fa fa-check"></i>${LocationsProfileLabels.getLocationProfiles_DisplayAllBtn()}
																</button> 
															</div>

														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form:form>

							</div>
							<!--end tabbable-->

							<!--end tabbable-->

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->





			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationProfileMaintenance.css"/>"
	rel="stylesheet" />

<script>
function checkBlank(fieldID){
 var fieldLength = document.getElementById(fieldID).value.trim().length;
 if (fieldLength>0) {
  
  return true;

 }
 else {
  
  
  return false;
 }
}


jQuery(document).ready(function() {    

	$(window).keydown(function(event){
              if(event.keyCode == 13) {
               		   
               var BoolLocationProfile = checkBlank('LocationProfile');
               var BoolProfileGroup = checkBlank('ProfileGroup');
               var BoolPartofLocationProfileDescription = checkBlank('PartofLocationProfileDescription');
            
			   
          if(BoolLocationProfile){
			
				$('#BtnIdLocationProfile').click();
          }
		   else if(BoolProfileGroup){
			
				$('#BtnIdProfileGroup').click();
          }
		  else if(BoolPartofLocationProfileDescription){
			 
				$('#BtnIdPartofLocationProfileDescription').click();
          }
		   /* else{
		  $('#BtnIdDisplayAll').click();
		  } */
			event.preventDefault();
            return false;
              }
          });
	
	  
	setInterval(function () {
		
    var h = window.innerHeight;
    if(window.innerHeight>=900){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
 
});
</script>
<script>
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(url, action) {
		$('#ErrorMessage').hide();

		if (action == 'LocationProfile') {

			var validLocationProfile = isBlankField('LocationProfile', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${LocationsProfileLabels.getLocationProfiles_BlankLocationProfile()}');
			

			var BlankProfileGroup = document.getElementById("ProfileGroup"); // Get text field
			var BlankDescription = document.getElementById("PartofLocationProfileDescription");
			BlankDescription.value = "";
			BlankProfileGroup.value = "";
			BlankProfileGroup.text="Select...";


			if (validLocationProfile) {

				var locationProfile = $('#LocationProfile').val();
				$.post(
								"${pageContext.request.contextPath}/RestCheckLocationProfile",
								{
									LocationProfile : locationProfile
									
								},
								function(data, status) {

									if (data) 
									{
									
										isSubmit = true;
										$('#ErrorMessage').hide();
										document.myForm.action = url;
										document.myForm.submit();
										
										// alert ("User Already Exists");
									} else {
										var errMsgId = document.getElementById("MessageRestFull");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${LocationsProfileLabels.getLocationProfiles_InvalidLocationProfile()}';   
								$('#ErrorMessage').show();
								isSubmit = false;	
									}
								});//end post info help

			}
			else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		}//end infohelp if 

		else if (action == 'ProfileGroup') {

			var validProfileGroup = isBlankField('ProfileGroup',
					'MessageRestFull', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${LocationsProfileLabels.getLocationProfiles_BlankProfileGroup()}');
			

			var BlankLocationProfile = document.getElementById("LocationProfile"); // Get text field
			var BlankPartofLocationProfileDescription = document.getElementById("PartofLocationProfileDescription");
			BlankLocationProfile.value = "";
			BlankPartofLocationProfileDescription.value = "";
			if (validProfileGroup) {
	
				var ProfileGroup = $('#ProfileGroup').val();
				$.post(
								"${pageContext.request.contextPath}/RestCheckProfileGroup",
								{
									ProfileGroup :ProfileGroup
								},
								function(data, status) {

									if (data) {
										
										isSubmit = true;
										$('#ErrorMessage').hide();
										document.myForm.action = url;
										document.myForm.submit();
										
										
										// alert ("User Already Exists");
									} else {
										var errMsgId = document.getElementById("MessageRestFull");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${LocationsProfileLabels.getLocationProfiles_InvalidProfileGroup()}';  
								$('#ErrorMessage').show();
								isSubmit = false;
									}
								});//end post infoPartoftheDescription
							

			} else {
				$('#ErrorMessage').show();
				//document.myForm;
				isSubmit = false;
			}

		} else if (action == 'PartofLocationProfileDescription') {

			var validdescription = isBlankField('PartofLocationProfileDescription', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${LocationsProfileLabels.getLocationProfiles_BlankPartOfLocation()}');
		

			var BlankProfileGroup = document.getElementById("ProfileGroup"); // Get text field
			var BlankLocationProfile = document.getElementById("LocationProfile");
			BlankLocationProfile.value = "";
			BlankProfileGroup.value = "";
			BlankProfileGroup.text="Select...";

			if (validdescription) {

            	var Description = $("#PartofLocationProfileDescription").val();
				$.post(
								"${pageContext.request.contextPath}/RestCheckLocationProfileDescription",
								{
									Description :Description
								},
								function(data, status) {

									if (data) {
										isSubmit = true;
										$('#ErrorMessage').hide();
										document.myForm.action = url;
										document.myForm.submit();
										
										// alert ("User Already Exists");
									} else {
										var errMsgId = document.getElementById("MessageRestFull");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${LocationsProfileLabels.getLocationProfiles_InvalidPart()}';   
								$('#ErrorMessage').show();
								isSubmit = false;
									}
								});//end post infoPartoftheDescription
							
				
				
				
			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} else if (action == 'displayall') {

			var BlankLocationProfile = document.getElementById("LocationProfile");
			var BlankProfileGroup = document.getElementById("ProfileGroup"); // Get text field
			var BlankDescription = document.getElementById("PartofLocationProfileDescription");
			BlankDescription.value = "";
			BlankProfileGroup.value = "";
			BlankLocationProfile.value="";
			isSubmit = true;
			document.myForm.action = url;
			document.myForm.submit();

		}
		else if(action == 'new'){
			var url="${pageContext.request.contextPath}/Location_Profile_Maintenance";
			document.myForm.action = url;
			document.myForm.submit();
		
			
		}

	}
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'LocationProfileMaintenance',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>