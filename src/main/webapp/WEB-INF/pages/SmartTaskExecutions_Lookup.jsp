<!-- 
------------------------------------------------------------------------------------------------------
	Program Name : SmartTaskExecutions_Lookup.jsp 
------------------------------------------------------------------------------------------------------	
Date Developed  May 14 2015
Description It is used to Search record on behalf of inputs.
Created By Aakash Bishnoi -->
<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">

	<div class="page-head">
			<div class="container">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
				<h1>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Executions_Lookup()}</h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
	</div>
	
        <!-- BEGIN PAGE CONTENT -->
      <div class="page-content" id="page-content">
            <div class="container">
                
                
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row margin-top-10">
                
                <div id="ErrorMessage" class="note note-danger" style="display:none">
		     		<p id="MessageRestFull" class="error error-Top"></p>	
		    	</div>
                
                
                    <div class="col-md-12">
                        
                        <form:form name="myForm"
									class="form-horizontal form-row-seperated" action="#"
									onsubmit="return isformSubmit();" method="get" modelAttribute="ObjSmartTaskConfigurator">
                        <div class="portlet">
                            <div class="portlet-body">
                                <div class="tabbable">
                                    <div class="tab-content no-space">
                                        <div class="tab-pane active" id="tab_general">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                       ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Application()}:
                                                    </label>
                                                    <div class="col-md-10">
														<form:select path="APPLICATION_ID"  style="display:inline"  class="table-group-action-input form-control input-medium" id="APPLICATION_ID" name="APPLICATION_ID">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectApplication}" var="DropDownApplication">
													    		<option value="${DropDownApplication.getGeneralCode()}">${DropDownApplication.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
													</div>
													
                                                  
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Group()}:
                                                    </label>
                                                    
                                                    <div class="col-md-10">
														
														<form:select path="EXECUTION_SEQUENCE_GROUP"  style="display:inline"  class="table-group-action-input form-control input-medium" id="EXECUTION_SEQUENCE_GROUP" name="EXECUTION_SEQUENCE_GROUP">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectExecutionGroup}" var="DropDownExecutionGroup">
													    		<option value="${DropDownExecutionGroup.getGeneralCode()}">${DropDownExecutionGroup.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                          ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Type()}:
                                                    </label>
                                                   <div class="col-md-10">
														
														<form:select path="EXECUTION_TYPE"  style="display:inline"  class="table-group-action-input form-control input-medium" id="EXECUTION_TYPE" name="EXECUTION_TYPE">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectExecutionType}" var="DropDownExecutionType">
													    		<option value="${DropDownExecutionType.getGeneralCode()}">${DropDownExecutionType.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                          ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Device()}:
                                                    </label>
                                                    <div class="col-md-10">
														
														<form:select path="EXECUTION_DEVICE"  style="display:inline"  class="table-group-action-input form-control input-medium" id="EXECUTION_DEVICE" name="EXECUTION_DEVICE">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectExecutionDevice}" var="DropDownExecutionDevice">
													    		<option value="${DropDownExecutionDevice.getGeneralCode()}">${DropDownExecutionDevice.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Name()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                       <input type="text" class="form-controlwidth" id="EXECUTION_NAME" name="EXECUTION_NAME">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                       ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Any_Part_of_the_Execution_Description()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                      <input type="text" class="form-controlwidth" id="DESCRIPTION20" name="DESCRIPTION20">
                                                    </div>
                                                </div>
                                                	
                                                <div id="HideTenant" class="form-group" style="display:block;">
                                                    <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Tenant()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-controlwidth" id="TENANT_ID" name="TENANT_ID">
                                                    </div>
                                                </div>
                                                
                                               <div id="Buttons" class="form-group" >
   													 <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Buttons()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-controlwidth" id="BUTTONS_ID" name="BUTTONS_ID" maxlength="1">  
                                                        <span>&nbsp;&nbsp;(${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Y()} / ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_N()})</span>
                                                    </div>
                                                </div>
                                                
                                                <div class="margin-bottom-5-right-allign_Smart_Task_Configurator">
                                                   				 <button
																	class="btn btn-sm yellow filter-submit margin-bottom"id="BtnIdNew"
																	onclick="actionForm('SmartTaskExecutions_Maintenance','new');">
																	<i class="fa fa-plus"></i>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_New()}
																</button>
																
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdDisplayAll"
																	onclick="actionForm('SmartTaskExecutions_SearchLookup','displayall');">
																	<i class="fa fa-check"></i>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Display_All()}
																</button>
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdSearch"
																	onclick="actionForm('SmartTaskExecutions_SearchLookup','Search');">
																	<i class="fa fa-check"></i>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Search()}
																</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form:form>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->
  
</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
		<link type="text/css"	href="<c:url value="/resourcesValidate/css/SmartTaskConfigurator.css"/>"
	rel="stylesheet" /> 
	
<script>

jQuery(document).ready(function() {   
	
	if("${JASCI}" == "${ObjCommonSession.getJasci_Tenant()}"){
		$('#HideTenant').show();
	}
	
//	$('#HideTenant').show();
	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
});


function checkBlank(fieldID) {
	 var fieldLength = document.getElementById(fieldID).value.trim().length;
	 if (fieldLength>0) {
	  return true;
	 }
	 else {
	  return false;
	 }
}

$('#ErrorMessage').hide();


$(window).keydown(function(event){
          if(event.keyCode == 13) {
           var BoolAPPLICATION_ID = checkBlank('APPLICATION_ID');
           var BoolEXECUTION_SEQUENCE_GROUP = checkBlank('EXECUTION_SEQUENCE_GROUP');
           var BoolEXECUTION_TYPE = checkBlank('EXECUTION_TYPE');
           var BoolEXECUTION_DEVICE= checkBlank('EXECUTION_DEVICE');		   
		   var BoolDESCRIPTION20 = checkBlank('DESCRIPTION20');
           var BoolEXECUTION_NAME = checkBlank('EXECUTION_NAME');
           var BoolTENANT_ID = checkBlank('TENANT_ID');
          }
      });
      
      

var isSubmit = false;

function isformSubmit() {
	
		return isSubmit;
}

function actionForm(url, action) {
	
	$('#ErrorMessage').hide();
	var BlankAPPLICATION_ID = document.getElementById("APPLICATION_ID");
	var BlankEXECUTION_SEQUENCE_GROUP = document.getElementById("EXECUTION_SEQUENCE_GROUP");
	var BlankEXECUTION_TYPE = document.getElementById("EXECUTION_TYPE");
	var BlankEXECUTION_DEVICE = document.getElementById("EXECUTION_DEVICE");
	var BlankDESCRIPTION20 = document.getElementById("DESCRIPTION20");
	var BlankEXECUTION_NAME = document.getElementById("EXECUTION_NAME");
	var BlankTENANT_ID = document.getElementById("TENANT_ID");
	
 if (action == 'Search') {
		var APPLICATION_ID = $('#APPLICATION_ID').val();
		var EXECUTION_SEQUENCE_GROUP = $('#EXECUTION_SEQUENCE_GROUP').val();
		var EXECUTION_TYPE = $('#EXECUTION_TYPE').val();
		var EXECUTION_DEVICE = $('#EXECUTION_DEVICE').val();
		var EXECUTION_NAME = $('#EXECUTION_NAME').val();
		var DESCRIPTION20 = $('#DESCRIPTION20').val();
		var TENANT_ID = $('#TENANT_ID').val();
		var BUTTONS_ID = $('#BUTTONS_ID').val();
	
		if  (APPLICATION_ID == '' &&  EXECUTION_SEQUENCE_GROUP == '' &&  EXECUTION_TYPE == '' && 
			EXECUTION_DEVICE == '' && EXECUTION_NAME == '' && DESCRIPTION20 == ''  &&  TENANT_ID == '' && BUTTONS_ID == '')  {
			var errMsgId = document.getElementById("MessageRestFull");
			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 	+ 'Please Select at least one of them.';
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
	    }		
	   else
	   {

		   $.post("${pageContext.request.contextPath}/SmartTaskExecutions_ParametersSearch_Lookup",
					{
			   			Application : APPLICATION_ID,
						ExecutionGroup : EXECUTION_SEQUENCE_GROUP,
						ExecutionType : EXECUTION_TYPE,
						ExecutionDevice : EXECUTION_DEVICE,
						Execution : EXECUTION_NAME,
						Description : DESCRIPTION20,
						Tenant : TENANT_ID,
						Buttons : BUTTONS_ID
					},
					function(data, status) {

						if (data.length < 1) {
							var errMsgId = document
									.getElementById("MessageRestFull");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Invalid_Execution_Device()}';
							$('#ErrorMessage').show();
							isSubmit = false;
							// alert ("User Already Exists");
						} else {
							isSubmit = true;
							$('#ErrorMessage').hide();
							document.myForm.action = url;
							document.myForm.submit();
						}
					});//end post info help

	   }	   
	} //if (action == 'SearchAll')
	else if (action == 'displayall' || action == 'new') {
		
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_NAME.value = "";
		BlankTENANT_ID.value = "";
        isSubmit = true;
        $('#ErrorMessage').hide();
        document.myForm.action = url;
        document.myForm.submit();
	  } 

}

function headerChangeLanguage(){
	  
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp() {
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'SmartTaskConfigurator',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>