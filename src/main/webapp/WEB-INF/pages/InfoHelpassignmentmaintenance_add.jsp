<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL" %>
<!DOCTYPE html>

<head>
<style>
.errormsgline{
display:inline !important;
}
.readonlyBackground{
background-color:#EBEBE4 !important;}

.paddingcombobox{
padding-left: 8px !important;
}
/*  .info_helpUpdate_margin-bottom-5_info_update{
		margin-bottom: 5px;
		margin-left: 41.0%;
		margin-top:54px;
		} */
		#ExecutionPath{background-color:white !important}
@media all and (-ms-high-contrast:none)
     {
    
		#ExecutionPath{background-color:white !important}
     .Executionbackground { background-color: transparent!important; margin-left: -48.9%!important;border: none!important;width: 240px!important;} / IE10 /
     ::-ms-backdrop, .Executionbackground { background-color: transparent!important; margin-left: -48.9%!important;border: none!important;width: 240px!important; } / IE11 */
     .paddingOnIe{padding-top:3px!important;padding-bottom:3px!important;padding-left:4px!important;padding-right:4px!important;}
     ::-ms-backdrop, .paddingOnIe {padding-top:3px!important;padding-bottom:3px!important;padding-left:4px!important;padding-right:4px!important;}
     }

 .Executionbackground
 {
  margin-left: -35.6%; 
  border: none; 
 width: 240px;
 }   

 
 fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.fa-check:before {
	margin-right: 5px !important;
}

</style>
</head>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<!DOCTYPE html>
<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014ɠWorld Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8">

</head>

<body>

<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance()}</h1>
			</div>
	
		
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
		
			<div class="row margin-top-10">
				<div class="col-md-12">
				<%-- <div id="ErrorMessage" class="note note-danger"
								style="display: none;">
								<p  class="error" style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p>
							</div> --%>
		<div id="ErrorMessage" class="note note-danger" style="display:none">
	     <p id="MessageRestFull" class="error error-Top" style="margin-left: -7px !important;"></p>	
	    </div>
					<form:form class="form-horizontal form-row-seperated"   onsubmit="return isFormSubmit();" action="#" >
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
												
												<div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Last_Activity_Date()}:
													</label>
													<div class="col-md-10">
														<input type="text" disabled="true" class="form-control2 errormsgline readonlyBackground" id="Last_Activity_Date" name="Last_Activity_Date" >
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Last_Activity_By()}:
													</label>
													<div class="col-md-10">
														<input type="text" disabled="true" class="form-control2 errormsgline readonlyBackground" value="${TeamMember}" name="product[name]" >
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Info_Help_Type()}:
													<span class="required">
													* </span></label>
													<div class="col-md-10">
														<!-- <select class="table-group-action-input form-control input-medium" id="infoHelpType" name="product[tax_class]">
															<option value="">Select...</option>
															<option value="Info-help1">Info-help1</option>
															<option value="Info-help2">Info-help2</option>
															<option value="Info-help3">Info-help3</option>
															
														</select> -->
														
														
														
														<select class="table-group-action-input form-control input-medium paddingcombobox" id="infoHelpType" name="product[tax_class]">
															<option value="">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Select()}..</option>
															<c:forEach items="${SelectInfoHelpType}" var="InfoHelpTypeDropDown">
													    		<option value="${InfoHelpTypeDropDown.getGeneralCode()}">${InfoHelpTypeDropDown.getDescription20()}</option>														   
															 </c:forEach>																																							
														</select>
														
														
														<span id="ErrorMessageinfoHelpType" class="error-select"></span>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Info_Help()}:
													<span class="required">
													* </span></label>
													<div class="col-md-10">
														<input type="text" class="form-control2 errormsgline" maxlength="100" name="product[name]" id="infoHelp" ></select>
														<span id="ErrorMessageinfoHelp" class="error"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Language()}:
													<span class="required">
													* </span></label>
													<div class="col-md-10">
														<select class="table-group-action-input form-control input-medium paddingcombobox" id="Language"  name="product[tax_class]">
															<!-- <option value="">Select...</option>
															<option value="EN">English</option>
															<option value="HI">Hindi</option> -->	
																<option value="">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Select()}..</option>													
															 <c:forEach items="${SelectLanguage}" var="LanguageList">
																			<%-- <c:if test="${LanguageList.getId().getGeneralCode()==TenantLanguage}"> --%>
																			<option value='${LanguageList.getGeneralCode()}'>${LanguageList.getDescription20()}</option>
																			<%-- </c:if> --%>
															</c:forEach>
															 
														</select><span id="ErrorMessageLanguage" class="error-select"></span>
													</div>
												</div>

												
												<div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Description_Short()}:
													<span class="required">
													* </span></label>
													<div class="col-md-10">
														<input type="text" class="form-control2 errormsgline" name="product[name]"  maxlength="20" id="Description20" >
														<span id="ErrorMessageDescription20" class="error"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Description_Long()}:
													<span class="required">
													* </span></label>
													<div class="col-md-10">													
														<input type="text" class="form-control2 errormsgline"  id="Description50" maxlength="50" >
														<span id="ErrorMessageDescription50" class="error"></span>
													</div>
												</div>
												</form:form>
												<%-- <div class="form-group">
													<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Execution_Path()}:
													<span class="required">
													* </span></label>
													<div class="col-md-10">													
														<input type="file"  class="form-control2 errormsgline paddingOnIe"  id="ExecutionPath"  ><span id="ErrorMessageExecutionPath" name="Browse" class="error"></span>
													</div>
													
												</div>	 --%>
												
															<div class="form-group">
														<form name="fileForm" id="fileForm" action="uploadpdf" target="uploadTrg"  method="post" enctype="multipart/form-data" >
																<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Execution_Path()}:<span class="required"> *</span> </label>
																<div class="col-md-10">
																
																<input type="file" id="browse"  style="display: none;" name="fileupload" onChange="Handlechange();" />
																	<input type="text" class="form-controlwidth" 
																		name="ExecutionPath" readonly="true" id="ExecutionPath"  maxlength="500" >
																		<input type="button" value="${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Choose_File()}" id="fakeBrowse" onclick="HandleBrowseClick();"/>
																		<div id="fit">
																		<span id="ErrorMessageExecutionPath"  class="error-choose"></span>
																		<!-- <span id="ErrorMessageAppIconAddress" class="error err"></span></div> -->
																		<input type="submit" id="btnSubmit" class="disp-none" style="display: none;" value="Upload file" />
																		
																	</div>
																</form>
															</div>
										   <div class="info_helpUpdate_margin-bottom-5_info_update">
											<button class="btn btn-sm yellow margin-bottom" onclick="testForm();"><i class="fa fa-check"></i>${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_SaveUpdate()}</button>
											 <button class="btn btn-sm red filter-cancel" onclick="return resetButton();"><i class="fa fa-times"></i>${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Reset()}</button>
											  <button class="btn btn-sm red filter-cancel" onclick="return cancelButton();"><i class="fa fa-times"></i>${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Cancel()}</button>
										   </div>		
							
												
											</div>
										</div>
										
										<script>
									       function resetButton(){
									           
									          /*  location.reload();  */
									    	   window.parent.location = window.parent.location.href;
									           
									          }
									          function cancelButton(){
									           
									           var backState = '${backStatus}';
									               
									           
									              if (backState == 'lookup') {

									               window.location="${pageContext.request.contextPath}/InfoHelpAssignmentSearch";
									              
									             }
									             else{
									              
									              window.location="${pageContext.request.contextPath}/InfoHelpSearch_lookup";
									             } 
									               
									           
									           
									          }
										</script>
										
										
									</div>
								</div>
							</div>
						</div>
					
				</div>
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>

<!-- END PAGE LEVEL SCRIPTS -->
<script>




jQuery(document).ready(function() {    

	$('#Last_Activity_Date').val(getLocalDate());

	
	
	var valueBrowse='${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Choose_File()}';
	 
	 var pddingPixel=2;
	 setInterval(function () {
	  
	     var w = window.innerWidth;      
	     var h = window.innerHeight;  
	     
	    
	     if(window.innerHeight>=900 || window.innerHeight==1004){
	      pddingPixel=4;
	     }
	     else if(w==1024){
	      pddingPixel=7;
	     }
	    else{
	     pddingPixel=2;
	     }
	     var lengthPadding=valueBrowse.length+pddingPixel;
	  $('#ExecutionPath').css({"padding-left":lengthPadding+"%"});
	     
	  }, 30);
	 
	$("#btnSubmit").hide();
	$("#browse").hide();
	$(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
		 $('#fileForm').ajaxForm({
			 
			complete:function(data, textStatus, jqXHR) {
			 
			
			},
			success: function(response) { branchDetailsSuccess(response); },
			
		}); 
		 
		 function branchDetailsSuccess(result) {
			
			    var textinput = document.getElementById("ExecutionPath");
				// var textaddress = document.getElementById("iconAddress");				
				   textinput.value = result;
				  //  textaddress.value=result;
				    isParent=false;
				   // jQuery("#AppIconImage").attr('src',result);				   
				   // $("#AppIconImage").show();
					$("#ErrorMessageExecutionPath").hide();
			}
	
	
	$(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	  
	setInterval(function () {
		
/* var fieldLength = document.getElementById('ExecutionPath').value.trim().length;
		
		if (fieldLength==0) {
			
		$("#AppIconImage").hide();
		}
		else{
			$("#AppIconImage").show();
			$("#ErrorMessageExecutionPath").hide();
		 }*/
		
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
 
});

$(function(){
	
	
	/* $('#ExecutionPath').keyup(function()
			  {  
		 jQuery("#AppIconImage").attr('src',$(this).val());
			  }) ;
	 $('#ExecutionPath').bind('paste', function(e)
			{ 
		 jQuery("#AppIconImage").attr('src',$(this).val());
		});
	
    $('#ExecutionPath').blur(function(){
    	
      //  $('#container').html('<img alt="" src="'+$(this).val()+'">');
        jQuery("#AppIconImage").attr('src',$(this).val());
    });
  */
    
});

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
	</tiles:putAttribute>

</tiles:insertDefinition>

	<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/jquery-1.8.2.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/jquery.ajaxfileupload.js"/>"
	type="text/javascript"></script>	
<script src="http://malsup.github.com/jquery.form.js"></script>
<script>
       
var isParent=false;




        var isSubmit=false;
        function isFormSubmit(){
        	return isSubmit;
        }
        
        function testForm(){
        	        	
        	if(isParent){
        		return false;
        	}else{
        		
        	}
        	
        	var validInfoHelpType = isBlankField('infoHelpType', 'ErrorMessageinfoHelpType',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank()}');
        	
        	var validInfoHelp = isBlankField('infoHelp', 'ErrorMessageinfoHelp',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank()}');
        	
        	var validLanguage = isBlankField('Language', 'ErrorMessageLanguage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+'${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank()}');
        	
        	var validDescription20 = isBlankField('Description20', 'ErrorMessageDescription20',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank()}');
        	
        	var validDescription50 = isBlankField('Description50', 'ErrorMessageDescription50',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank()}');
        	        	
        	var validExecutionPath = isBlankField('ExecutionPath', 'ErrorMessageExecutionPath',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank()}');
							
        	
        	if(validInfoHelpType && validInfoHelp && validLanguage && validDescription20 && validDescription50 && validExecutionPath){
        		
        		
        		var varinfoHelp=$('#infoHelp').val();        	
        		var varLanguage=$('#Language').val();
        		
				$.post("${pageContext.request.contextPath}/RestInfohelpCheck",
								{
									InfoHelp : varinfoHelp,
									Language :varLanguage
									
								},
								function(data, status) {

									if (data) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Infohelps_already_used()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										
										
										
										//save recodrs
										var varLastActivityBy='${TeamMember}';
										var varInfoHelpType=$('#infoHelpType').val(); 
										var varInfoHelp=$('#infoHelp').val();    
										var varLanguage=$('#Language').val();
										var varDescription20=$('#Description20').val();
										var varDescription50=$('#Description50').val();
										var varExecutionPath=$('#ExecutionPath').val();
										var varLastActivityDate=getUTCDateTime();
										
										$.post("${pageContext.request.contextPath}/RestInsertInfoHelp",
												{
											LastActivityBy:varLastActivityBy,
											InfoHelpType:varInfoHelpType,
											InfoHelp:varInfoHelp,
											Language:varLanguage,
											Description20:varDescription20,
											Description50:varDescription50,
											ExecutionPath:varExecutionPath,
											LastActivityDate:varLastActivityDate		
												},
												function(data, status) {

													if (data) {
														var errMsgId = document
																.getElementById("ErrorMessage");
														errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
																+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Internal_Error()}';
														$('#ErrorMessage').show();
														isSubmit = false;
														// alert ("User Already Exists");
													} else {
														$('#ErrorMessage').hide();
														bootbox.alert('${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Confirm_Success()}',function(){
															isSubmit = true;;
															window.location.href = '${pageContext.request.contextPath}/InfoHelpSearchlookup';
														});
														
														
													}
												});
											
										
										
															//isSubmit = true;
									//	document.myForm.action = url;
										//document.myForm.submit();
									}
								});//end post infoPartoftheDescription
        		
        		
        		
        		
        		
        		
        	}else{
        		
        		
        		
        	}
        	
        	
        	
        }
     
        function HandleBrowseClick()
    	{
    	    var fileinput = document.getElementById("browse");
    	   
    	    fileinput.click();
    	  
    	}

        
    	function Handlechange()
    	{
    	    var fileinput = document.getElementById("browse");
    	    
    	    var status=load_image('browse',fileinput.value);
    	   
    	    if(status){
    	    var textinput = document.getElementById("ExecutionPath");
    	   // var data = fileinput.value;
    	   
    	    //var arr = data.split('/');
    	    //textinput.value = arr[1];
    	      isParent=true;
    	      textinput.value='${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Loading()}...';
    	    $("#btnSubmit").trigger('click');
    	    
    	  
    	    }
    	}

    	
    	function load_image(id,ext)
    	{
    		 var errMsgId = document.getElementById("ErrorMessageExecutionPath");
    	   if(validateExtension(ext) == false)
    	   {
    	     // alert("Upload only JPEG or JPG format ");
    	     // document.getElementById("imagePreview").innerHTML = "";
    	      document.getElementById("browse").focus();
    	      var errMsgId = document.getElementById("ErrorMessageExecutionPath");														
    			errMsgId.innerHTML ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${Infohelp_ScreenLabels.getInfoHelpassignmentmaintenance_Supported_formats_is_pdf()}';
    			/*  $('#fakeBrowse').css({"margin-left":"-85.5%"}); */
    	      var textinput = document.getElementById("ExecutionPath");        
		       textinput.value = '';
		       		     $("#ErrorMessageExecutionPath").show();
    			return false;
    	      
    	     
    	   }
    	   else{
    		   errMsgId.innerHTML='';
    		  /*  $('#fakeBrowse').css({"margin-left":"-50.0%"}); */
    		   return true;
    	   }
    	}

    	function validateExtension(v)
    	{
    		  var allowedExtensions = new Array("pdf","PDF");
    	      for(var ct=0;ct<allowedExtensions.length;ct++)
    	      {
    	          sample = v.lastIndexOf(allowedExtensions[ct]);
    	          if(sample != -1){return true;}
    	      }
    	      return false;
    	}
    	
        function headerChangeLanguage(){
        	
        	/* alert('in security look up'); */
        	
         	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
        	
        	
        }

        function headerInfoHelp(){
        	
        	//alert('in security look up');
        	
         	/* $
        	.post(
        			"${pageContext.request.contextPath}/HeaderInfoHelp",
        			function(
        					data1) {

        				if (data1.boolStatus) {

        					location.reload();
        				}
        			});
        	 */
         	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
        			  {InfoHelp:'InfoHelp',
         		InfoHelpType:'PROGRAM'
        		 
        			  }, function( data1,status ) {
        				  if (data1.boolStatus) {
        					  window.open(data1.strMessage); 
        					  					  
        					}
        				  else
        					  {
        					//  alert('No help found yet');
        					  }
        				  
        				  
        			  });
        	
        }
    </script>
    
    
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>