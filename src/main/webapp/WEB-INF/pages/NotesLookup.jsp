<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>



<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

<style>
.k-icon, .k-state-disabled .k-icon, .k-column-menu .k-sprite {
opacity: .8 !important;
margin-top: -4px !important;
}

</style>
		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->

		<!-- BEGIN PAGE CONTENT -->
		 <div id="container" style="position: relative" class="loader_div">
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${viewLabels.getLbl_SearchLookup_title()}</h1>
					</div>
				</div>

				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<!--<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Dashboard
				</li>
			</ul>-->
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<div class="col-md-12">
								<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
								<!-- Begin: life time stats -->
								<!-- not in used -->
							<!--	<div class="form-body margin-left-15pix" id="partgroup">
									<div class="form-group"">
										<label class="col-md-2 control-label" id="part-name1"><b>${NotesbeLabelObj.getNotes_DateGridLabel()}: ${Date}</b></label>
										
									</div>
									<br/>
									<br/>
									<div class="form-group">
										<label class="col-md-2 control-label" id="part-name2"><b>${NotesbeLabelObj.getNotes_ByGridLabel()}: ${By}</b> </label>
										
									</div>
									<br/>
									<br/>
									<div class="form-group margin-top-12pix" >
										<label class="col-md-2 control-label" id="part-name3"><b>${NotesbeLabelObj.getNotes_GeneralCodeId_Application()}: ${AppIconName} </b></label>
										
									</div>
									<br/>
									
								</div> -->
								<div class="portlet">
									
								<a  id="AddBtnAppIcon" href="#" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480" onclick="addButton();" >${viewLabels.getLbl_Add_New_noteslookup()}</span>
								</a>								
								<script type="text/javascript">
								
								function addButton(){
									
									var url="${pageContext.request.contextPath}/Notes_Maintenace_Screen_addnew";
	                                //var wnd = $("#details").data("kendoWindow");
	                               
	    							 var myform = document.createElement("form");	                                 
	                                 var varFlagForReturn = document.createElement("input");
	                                 varFlagForReturn.setAttribute("type", "hidden");
	                                 varFlagForReturn.value = "SearchLookup";
	                                 varFlagForReturn.name = "FlagForReturn";
	                                 
	                                 
	                                 myform.action = url;
	                                 myform.method = "get";
	                                 document.body.appendChild(myform);
	                                 myform.submit();
								}
								
								</script>
								
									<div class="portlet-body">
										<div class="table-container">
													<kendo:grid name="NotesListGrid" columnResize="true" dataBound="gridDataBound"
										sortable="true" resizable="true" reorderable="true">
										<kendo:grid-pageable refresh="true" pageSizes="true"
											buttonCount="5">
										</kendo:grid-pageable>
										<kendo:grid-editable mode="inline" confirmation="Message" />

										<kendo:grid-columns>

											<kendo:grid-column
												title="${viewLabels.getLbl_Date_noteslookup()}"
												field="last_Activity_Date" width="20%;" />
											
											<kendo:grid-column
												
												title="${viewLabels.getLbl_By_noteslookup()}"
												field="last_Activity_Team_Member" width="22%;" />
												
												
											<kendo:grid-column
												
												title="${viewLabels.getLbl_Note_noteslookup()}"
												field="note" width="40%;" />	
												
												
											<kendo:grid-column
												title="${viewLabels.getLbl_Select_noteslookup()}"
												width="18%;">
												<kendo:grid-column-command>

													<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
													<kendo:grid-column-commandItem
														className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
														name="editDetails"
														text="${viewLabels.getLbl_Edit_noteslookup()}">
														<kendo:grid-column-commandItem-click>
															<script>
                           
                            function editNotes(e) {
                            	
                                e.preventDefault();
				               var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                               var varLastActivityDate=dataItem.last_Activity_Date;
                               var varid = dataItem.auto_ID;
                               //alert(varLastActivityDate+ ' '+varid);
                               // debugger;
                                //var varNotelink=dataItem.Notelink;
                              /*   var varTeamMember=dataItem.TeamMember;
                                var varLastActivityDate=dataItem.varLastActivityDate;
                             var varNote=dataItem.Note; */

                               <%--  var contextPath='<%=request.getContextPath()%>'; --%>
                               
                             
                                 var url="${pageContext.request.contextPath}/Notes_Maintenance_Screen_Action";
                                //var wnd = $("#details").data("kendoWindow");
                               
    							 var myform = document.createElement("form");

                                 var varLastActivityDateFiled = document.createElement("input");
                                 varLastActivityDateFiled.value = varLastActivityDate;
                                 varLastActivityDateFiled.name = "LAST_ACTIVITY_DATE";
                                 varLastActivityDateFiled.setAttribute("type", "hidden");

                                 
                       /*           var varNotelinkField = document.createElement("input");
                                 varNotelinkField.setAttribute("type", "hidden");
                                 varNotelinkField.value = varNotelink;
                                 varNotelinkField.name = "Notelink";
                                 varNotelinkField.setAttribute("type", "hidden"); */
                                 
/*                                  var varTeamMemberField = document.createElement("input");
                                 varTeamMemberField.setAttribute("type", "hidden");
                                 varTeamMemberField.value = varTeamMember;
                                 varTeamMemberField.name = "TeamMember"; */
                                 
                                 
                                 var varFlagForReturn = document.createElement("input");
                                 varFlagForReturn.setAttribute("type", "hidden");
                                 varFlagForReturn.value = "SearchLookup";
                                 varFlagForReturn.name = "FlagForReturn";
                                 
                                 
                                 var varidFiled = document.createElement("input");
                                 varidFiled.value = varid;
                                 varidFiled.name = "ID";
                                 varidFiled.setAttribute("type", "hidden");
                                 
                                 
                                 myform.action = url;
                                 myform.method = "get";
                                 myform.appendChild(varFlagForReturn);
                                 myform.appendChild(varLastActivityDateFiled);
                                 myform.appendChild(varidFiled);
                                 document.body.appendChild(myform);
                                 myform.submit();
    							
                            }
                            
                          
                            </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

													<kendo:grid-column-commandItem
														className="btn btn-sm red filter-cancel fa fa-times"
														name="btnDelete" text="${viewLabels.getLbl_Delete_noteslookup()}">

														<kendo:grid-column-commandItem-click>

															<script>	
															
															
															
															
															
								                            function deleteRecords(e) {
								                            	
								                                e.preventDefault();
												               var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
								                                var varLastActivitydate=dataItem.last_Activity_Date;
								                                var varid = dataItem.auto_ID;
								                               // debugger;
								                                
								                              /*   var varTeamMember=dataItem.TeamMember;
								                                var varLastActivityDate=dataItem.varLastActivityDate;
								                                var varNote=dataItem.Note; */
								                                
								                               
								                                
								                               
								                               <%--  var contextPath='<%=request.getContextPath()%>'; --%>
								                               
								                              
								                            	   bootbox.confirm('${viewLabels.getLbl_note_delete_msg()}',
																	function(okOrCancel) {

																	if(okOrCancel == true)
																	{
								                               var url="${pageContext.request.contextPath}/Notes_Maintenance_delete_Action";
								                                // var url="${pageContext.request.contextPath}/Notes_Maintenance_delete_Action";
								                                //var wnd = $("#details").data("kendoWindow");
								                               
								    							 var myform = document.createElement("form");

								                                 var varLastActivitydateFiled = document.createElement("input");
								                                 varLastActivitydateFiled.value = varLastActivitydate;
								                                 varLastActivitydateFiled.name = "LAST_ACTIVITY_DATE";
								                                 varLastActivitydateFiled.setAttribute("type", "hidden");
								                                 
								                                 /* var varNotelinkField = document.createElement("input");
								                                 varNotelinkField.setAttribute("type", "hidden");
								                                 varNotelinkField.value = varNotelink;
								                                 varNotelinkField.name = "Notelink";
								                                 varNotelinkField.setAttribute("type", "hidden"); */
								                                 
								/*                                  var varTeamMemberField = document.createElement("input");
								                                 varTeamMemberField.setAttribute("type", "hidden");
								                                 varTeamMemberField.value = varTeamMember;
								                                 varTeamMemberField.name = "TeamMember"; */
								                                 
								                                 
								                                 var varFlagForReturn = document.createElement("input");
								                                 varFlagForReturn.setAttribute("type", "hidden");
								                                 varFlagForReturn.value = "SearchLookup";
								                                 varFlagForReturn.name = "FlagForReturn";
								                                 var varidFiled = document.createElement("input");
								                                 varidFiled.value = varid;
								                                 varidFiled.name = "ID";
								                                 varidFiled.setAttribute("type", "hidden");
								                                 
								                                 myform.action = url;
								                                 myform.method = "get";
								                                 myform.appendChild(varFlagForReturn);
								                                 myform.appendChild(varLastActivitydateFiled);
								                                 myform.appendChild(varidFiled);
								                                 document.body.appendChild(myform);
								                                 myform.submit();
								                                 
								                               
								                            }
								                            });
								                               
								                               
								    							
								                            }
								                            						
															
															
															
											</script>				
															
															
															
															
<script type="text/javascript">
/* 															
function deleteRecords(e) { 

	   e.preventDefault();
	   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
       var notes_id=dataItem.Notes_Id;
       var notes_link=dataItem.Notes_link;
       
       
       if (confirm("do You want to delete") == true){
         var url="${pageContext.request.contextPath}/Notes_Maintenance_delete_screen";
        //var wnd = $("#details").data("kendoWindow");
       
		 var myform = document.createElement("form");

         var varApplicationFiled = document.createElement("input");
         varNotesidFiled.value = notes_id;
         varNotesidFiled.name = "Notes_Id";
         varNotesidFiled.setAttribute("type", "hidden");
         
         var varAppIconField = document.createElement("input");
         varNoteslinkField.setAttribute("type", "hidden");
         varNoteslinkField.value = notes_link;
         varNoteslinkField.name = "Notes_link";
         
         var varKendoUrlField = document.createElement("input");
         varKendoUrlField.setAttribute("type", "hidden");
         varKendoUrlField.value = '${KendoReadUrl}';
         varKendoUrlField.name = "KendoUrl";
         
         myform.action = url;
         myform.method = "post";
         myform.appendChild(varNotesidFiled);
         myform.appendChild(varNoteslinkField);
         myform.appendChild(varKendoUrlField);
         document.body.appendChild(myform);
         myform.submit();
		


//var wnd = $("#details").data("kendoWindow");
  // window.location=url;

}else{
  
   
  
   
}
		} */
</script>															

														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

												</kendo:grid-column-command>
											</kendo:grid-column>
										</kendo:grid-columns>

										<kendo:dataSource pageSize="10">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/NotesLookup/Grid/read"></kendo:dataSource-transport-read>
												<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8087//NotesLookup/Grid/read"></kendo:dataSource-transport-read> --%>
												<kendo:dataSource-transport-update
													url="GeneralCodeList/Grid/update" dataType="json"
													type="POST" contentType="application/json" />
												<kendo:dataSource-transport-destroy
													url="GeneralCodeList/Grid/delete" dataType="json"
													type="POST" contentType="application/json">
													<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
												</kendo:dataSource-transport-destroy>


												<kendo:dataSource-transport-parameterMap>
													<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>
											<kendo:dataSource-schema>
												<kendo:dataSource-schema-model id="intKendoID">
													<kendo:dataSource-schema-model-fields>
														<kendo:dataSource-schema-model-field name="intKendoID">

														</kendo:dataSource-schema-model-field>


													</kendo:dataSource-schema-model-fields>
												</kendo:dataSource-schema-model>
											</kendo:dataSource-schema>



										</kendo:dataSource>
									</kendo:grid>
											
											
										</div>
									</div>
								</div>
								<!-- End: life time stats -->
							</div>

						</div>
						<div class="row"></div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
				<!-- END PAGE CONTENT -->
			</div>
		</div>
		 </div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<link href="<c:url value="/resourcesValidate/css/menu-app-icon-style.css"/>"  rel="stylesheet" type="text/css">
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
	type="text/javascript"></script>
	
<script>
	jQuery(document).ready(function() {
		
		var value='${KendoReadUrl}';
		if(value=='/NotesLookup/Grid/read'){
				$('#partgroup').hide();
			
		}else{
			$('#partgroup').show();
			
		}
		
		/*  $("#NotesListGrid").kendoGrid({ 
	          dataSource: dataSource, 
	          scrollable: true, 
	          filterable: true, 
	          toolbar: ["create"],
	          columns: [
	            { field: "ID", width: "50px" }, 
	            { field: "Text", width: "200px", attributes: {
	              style: 'white-space: nowrap '
	            }  }], 
	          editable: "incell"
	        }).data("kendoGrid");  */ 
		
		$("#NotesListGrid").kendoTooltip({
		     filter: "td:nth-child(3)", //this filter selects the first column cells
		     iframe: false,
		     width: 500,
		     position: "bottom", 
		     content: function(e){
		      var dataItem = $("#NotesListGrid").data("kendoGrid").dataItem(e.target.closest("tr"));
		      var content = dataItem.note;
		      return content;
		     }
		   }).data("kendoTooltip");
		
setInterval(function () {
			
		    var h = window.innerHeight;		    
		    if(window.innerHeight>=900){
		    	h=h-187;
		    	
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";		     
		    
			}, 30);
		/* Metronic.init(); // init metronic core componets
		Layout.init(); // init layout
		Demo.init(); // init demo(theme settings page)
		Index.init(); // init index page
		Tasks.initDashboardWidget(); // init tash dashboard widget */
	});
	
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'Notes',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  }
	 
	  
	  function gridDataBound(e) {
			var grid = e.sender;
			if (grid.dataSource.total() > 0) {
				var colCount = grid.columns.length;
				kendo.ui.progress(ajaxContainer, false);

			}
			else
				{
					kendo.ui.progress(ajaxContainer, false);
				}
		};
		
		var ajaxContainer = $("#container");
		kendo.ui.progress(ajaxContainer, true); 
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>