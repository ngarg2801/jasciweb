<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>

<tiles:insertDefinition name="subMenuAssigmentTemplate">

	<tiles:putAttribute name="body">
		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_MenuAppIconMaintenance()}</h1>
					</div>
				</div>
				
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#"></a><i class="fa fa-circle"></i></li>
							<li class="active"></li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<form:form class="form-horizontal form-row-seperated" name="myForm"
									action="#"
									onsubmit="return isformSubmit();"
									modelAttribute="MENUAPPICON_REGISTER">
								
									<div class="portlet">

										<div class="portlet-body">
										<div id="ErrorMessage" style="display: none; margin-left: 0px!important;" class="note note-danger display-none"												>
												<p   id="Perror" class="error error-Top margin-left-7pix"></p>
											</div>
											<input type="hidden" name="iconAddress" value="${MenuAppIconListObj.getAppIconAddress()}" id="iconAddress" >
											<input type="hidden" name="ApplicationId" value="${MenuAppIconListObj.getApplication()}" id="ApplicationId" >
											<input type="hidden" name="AppIconOld" value="${MenuAppIconListObj.getAppIcon()}" id="AppIconOld" >
											<input type="hidden" name="getLastActivitydate" id="getLastActivitydate">
								 		
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">
															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_AppIconGrid()}:
																	<span class="required">*</span>
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="AppIcon" id="AppIcon"  maxlength="100" value="${MenuAppIconListObj.getAppIcon()}" >
																		<span id="ErrorMessageAppIcon" class="error"> </span>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_LastActivityDate()}: </label>

																<div class="col-md-10">
																	<input type="text"  readonly="true"  class="form-controlwidth"
																		name="LastActivityDate" id="LastActivityDate"  >
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_LastActivityBy()}: </label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth" readonly="true"
																		name="LastActivityTeamMember"  value="${TeamMemberName}" >
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_PreferredApplicationLabel()}:<span class="required">*</span>
																</label>
																<div class="col-md-10">
																	<select
																		class="selectmenuappicon"
																		name="Application" id="PreferredApplication"  disabled="disabled" style="background-color: #eee;">
																		<option value="">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_Select()}...</option>
																		<c:forEach items="${GeneralCodeApplicationListObject}"
																			var="AppList">
																		<option
																				value='${AppList.getGeneralCode()}'>${AppList.getGeneralCode()}</option>
																		</c:forEach>
																	</select><span id="ErrorMessagePreferredApplication" class="error-select"> </span>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_DescriptionShortLabel()}:<span class="required">*</span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="DescriptionShort" id="DescriptionShort" maxlength="20" value="${MenuAppIconListObj.getDescriptionShort()}">
																			<span id="ErrorMessageDescriptionShort" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_DescriptionLongLabel()}:<span class="required">*</span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="DescriptionLong" id="DescriptionLong" maxlength="50" value="${MenuAppIconListObj.getDescriptionLong()}">
																		<span id="ErrorMessageDescriptionLong" class="error"> </span>
																</div>
															</div>
															</form:form>
															<div class="form-group">
														<form name="fileForm" id="fileForm" action="uploadimage" target="uploadTrg"  method="post" enctype="multipart/form-data" onSubmit="return submitPostUsingAjax();">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_AppIconAddressLabel()}:<span class="required">*</span> </label>
																<div class="col-md-10">
																
																<input type="file" id="browse" name="fileupload" style="display: none;" onChange="Handlechange();" />
																	<input type="text" class="form-controlwidth" 
																		name="AppIconAddress" readonly="true"  id="AppIconAddress" maxlength="500"  value="${MenuAppIconListObj.getAppIconAddress()}">
																		<input type="button" value="${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ButtonBrowseText()}" id="fakeBrowse" onclick="HandleBrowseClick();"/>
																		<div id="fit"><span id="ErrorMessageAppIconAddress" class="error err"></span></div>
																		<input type="submit" id="btnSubmit" class="disp-none" value="Upload file" />
																		
																	<img src=""
																		alt="" onclick="SetId();"  id="AppIconImage"   class="receivingbutton img-layout" >			
																</div>
																</form>
															</div>



															<div class="margin-bottom-5-right-allign_app_icon_update  margin-right-menu-app-btn">
																<button
																	class="btn btn-sm yellow  margin-bottom">
																	<i class="fa fa-check"></i> ${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ButtonSaveUpdateText()}
																</button>
																<button class="btn btn-sm red" type="button"
																onclick="reloadPage();">
																	<i class="fa fa-times"></i>${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ButtonResetText()}
																</button>
																<button class="btn btn-sm red" type="button"
																onclick="cancelButton();">
																	<i class="fa fa-times"></i>${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ButtonCancelText()}
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								
							</div>
						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		
		<!-- END PAGE CONTAINER -->
		<!-- BEGIN PRE-FOOTER -->

	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/jquery-1.8.2.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/jquery.ajaxfileupload.js"/>"
	type="text/javascript"></script>	
<script src="http://malsup.github.com/jquery.form.js"></script>

<script>
jQuery(document).ready(function() { 
	
 $('#LastActivityDate').val(ConvertUTCToLocalTimeZone('${LastActivityDate}'));
	
var valueBrowse='${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ButtonBrowseText()}';
	
	var pddingPixel=2;
	
	setInterval(function () {
		
	    var w = window.innerWidth;		    
	    var h = window.innerHeight;		
	    
		  
	    if(window.innerHeight>=900 || window.innerHeight==1004){
	    	pddingPixel=4;
	    }
	    else if(w==1024){
	    	pddingPixel=7;
	    }
	   else{
		   pddingPixel=2;
	    }
	    var lengthPadding=valueBrowse.length+pddingPixel;
		$('#AppIconAddress').css({"padding-left":lengthPadding+"%"});
	    
		}, 5);
	
	$(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	
	
	$('#browse').hide();
	$('#btnSubmit').hide();
	 $('#fileForm').ajaxForm({
			complete:function(data, textStatus, jqXHR) {
			 
			
			},
			success: function(response) { branchDetailsSuccess(response); }
		 
		}); 
		 
		 function branchDetailsSuccess(result) {
			 
			  var textinput = document.getElementById("AppIconAddress");
				 var textaddress = document.getElementById("iconAddress");				
				   textinput.value = result;
				    textaddress.value=result;
				    isParent=false;
				    jQuery("#AppIconImage").attr('src',result);				   
				    $("#AppIconImage").show();
					$("#ErrorMessageAppIconAddress").hide();
			}
		 
	setCheckbox();
	  var valuaddress=$('#AppIconAddress').val();
	    	
	      //  $('#container').html('<img alt="" src="'+$(this).val()+'">');
	        jQuery("#AppIconImage").attr('src',valuaddress);
	  
	setInterval(function () {
var fieldLength = document.getElementById('AppIconAddress').value.trim().length;
		
		if (fieldLength==0) {
			
		$("#AppIconImage").hide();
		}
		else{
		//	$("#AppIconImage").show();
			$("#ErrorMessageAppIconAddress").hide();
		}
	    var h = window.innerHeight;		    
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    	
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";		     
	    
		}, 30);
	
	
  /*  Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo(theme settings page)
   Index.init(); // init index page
   Tasks.initDashboardWidget(); // init tash dashboard widget */
});1

$(function(){
	
	

	/* $('#AppIconAddress').keyup(function()
			  {  
		 jQuery("#AppIconImage").attr('src',$(this).val());
			  }) ;
	 $('#AppIconAddress').bind('paste', function(e)
			{ 
		 jQuery("#AppIconImage").attr('src',$(this).val());
		});
	
    $('#AppIconAddress').blur(function(){
    	
      //  $('#container').html('<img alt="" src="'+$(this).val()+'">');
        jQuery("#AppIconImage").attr('src',$(this).val());
    }); */
});
</script>
<script>
var isParent=false;
	function isformSubmit() {
		if(isParent){
			return false;
		}else{
			
		}
		var isSubmit = false;
	
		var isBlankAppIcon = isBlankField('AppIcon',
				'ErrorMessageAppIcon', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		
		var isBlankPreferredApplication= isBlankField('PreferredApplication',
				'ErrorMessagePreferredApplication', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		
		var isBlankDescriptionShort= isBlankField('DescriptionShort',
				'ErrorMessageDescriptionShort', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		
		var isBlankDescriptionLong= isBlankField('DescriptionLong',
				'ErrorMessageDescriptionLong', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		
		
		var isBlankAppIconAddress= isBlankField('AppIconAddress',
				'ErrorMessageAppIconAddress', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		/* if(isBlankAppIconAddress){
			$('#fakeBrowse').css({"margin-left":"-50%"});
			}else{
				$('#fakeBrowse').css({"margin-left":"-85.5%"});
			} */
		
		
		if(isBlankAppIcon && isBlankPreferredApplication && isBlankDescriptionShort && isBlankDescriptionLong && isBlankAppIconAddress){
			
			
			 var errMsgId=document.getElementById("Perror");
			 var valueAppIcon=$('#AppIcon').val();
			 var oldvalueAppIcon=$('#AppIconOld').val();
				
			if(valueAppIcon!=oldvalueAppIcon){
			$.post("${pageContext.request.contextPath}/RestGetMenuAppIconsListByAppIcon",
					{
						StrAppIcon :valueAppIcon					

					},
					function(data, status) {
					
						
						if(data){
							
							 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED()}';
				        	 $('#ErrorMessage').show();
				        	 isSubmit=false;
							
						}
						else{
							 $('#ErrorMessage').hide();
							 bootbox.alert('${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY()}',function(){
									
							 isSubmit=true;
							 $('#getLastActivitydate').val(getUTCDateTime());
				        	 //alert('${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY()}');
							 document.myForm.action = '${pageContext.request.contextPath}/Menuappicon_addorupdate';							 
							 document.myForm.submit();
							 });
						}
						
					});//end check app icon
			}else{
				$('#ErrorMessage').hide();
				 bootbox.alert('${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY()}',function(){
						
				 isSubmit=true;
				 $('#getLastActivitydate').val(getUTCDateTime());
	        	// alert('${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY()}');
				 document.myForm.action = '${pageContext.request.contextPath}/Menuappicon_addorupdate';							 
				 document.myForm.submit();
				 });
				
			}
        	 
		}
		else{
			
			
		}
		
		
		
		return isSubmit;
	
	}
		
	function setCheckbox(){
		
		$("#PreferredApplication").attr("readonly", "true");
		$("#PreferredApplication").css('pointer-events', 'none');
		
		
		var currentApplicationValue = '${MenuAppIconListObj.getApplication()}';
		if (currentApplicationValue.trim() != '') {
			document.getElementById('PreferredApplication').value = currentApplicationValue;
			//document.getElementById('Shiftcode').remove(0);
		} else {

		}
	}	
	
	function reloadPage(){
	window.parent.location = window.parent.location.href;
	}
	
	function cancelButton() {

		
			var flagBack='${FlagForReturnObj}';
			var varBackScreen='${BackScreen}';
			
			if(flagBack=='SearchLookup'){
			window.location.href = '${pageContext.request.contextPath}/'+varBackScreen;
			}
			else{
				window.location.href = '${pageContext.request.contextPath}/'+'Menu_app_icon_lookup';
				
			}
		
 
	}
	
	function HandleBrowseClick()
	{
	    var fileinput = document.getElementById("browse");
	    fileinput.click();
	  
	}

	function Handlechange()
	{
	    var fileinput = document.getElementById("browse");
	    
	    var status=load_image('browse',fileinput.value);
	    
	    if(status){
	    var textinput = document.getElementById("AppIconAddress");
	   // var data = fileinput.value;
	    //var arr = data.split('/');
	    //textinput.value = arr[1];
	     isParent=true;
	     textinput.value='${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_Text_Loading()}...';
	    $("#btnSubmit").trigger('click');
	  
	    }
	}

	
	function load_image(id,ext)
	{
		 var errMsgId = document.getElementById("ErrorMessageAppIconAddress");
	   if(validateExtension(ext) == false)
	   {
	     // alert("Upload only JPEG or JPG format ");
	     // document.getElementById("imagePreview").innerHTML = "";
	      document.getElementById("browse").focus();
	      var errMsgId = document.getElementById("ErrorMessageAppIconAddress");														
			errMsgId.innerHTML ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_SUPPRTED_FORMAT()}';
			/*  $('#fakeBrowse').css({"margin-left":"-85.5%"}); */
			 var textinput = document.getElementById("AppIconAddress");			  			
				   textinput.value = '';
				    $("#AppIconImage").hide();
					$("#ErrorMessageAppIconAddress").show();
	      return false;
	      
	     
	   }
	   else{
		   errMsgId.innerHTML='';
		  /*  $('#fakeBrowse').css({"margin-left":"-50.0%"}); */
		   return true;
	   }
	}


	function validateExtension(v)
	{
	      var allowedExtensions = new Array("jpg","JPG","jpeg","JPEG","png","PNG","GIF","gif");
	      for(var ct=0;ct<allowedExtensions.length;ct++)
	      {
	          sample = v.lastIndexOf(allowedExtensions[ct]);
	          if(sample != -1){return true;}
	      }
	      return false;
	}
function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}

	function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'MenuApp',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						//  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
	
</script>		
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>