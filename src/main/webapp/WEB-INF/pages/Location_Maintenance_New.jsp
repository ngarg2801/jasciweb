<!-- 
Date Developed  Sep 18 2014
Description It is used to Add value of Location Maintenance Screen
Created By Aakash Bishnoi -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL" %>
<!DOCTYPE html>

  <%-- 
    <%!
    	COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
    	
	%>
<c:set var="StrTenant" value="<%=objCommonsessionbe.getTenant()%>"/>	
<c:set var="StrCompany" value="<%=objCommonsessionbe.getCompany()%>"/>	

 --%>
<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014ɠWorld Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->



	<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	
	<div class="page-head">
   <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
   <h1>${ScreenLabels.getLocationMaintenace_Location_Maintenance()}</h1>
    </div>
    </div>
	<div class="page-content" id="page-content">
		<div class="container">
			
    <div id="ErrorMessage" class="note note-danger" style="display:none">
     <p id="MessageRestFull" class="error error-Top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Messages</p>	
    </div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			
			<div class="row margin-top-10">
				<div class="col-md-12">
				
					<form:form  name="myform" method="get" action="#" onsubmit="return SubmitRecord();" class="form-horizontal form-row-seperated" modelAttribute="ObjectLocation">
					
										<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
	

     											<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Last_Activity_Date">${ScreenLabels.getLocationMaintenace_Last_Activity_Date_YYYY_MM_DD()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text" disabled="true" id="Last_Activity_Date" name="Last_Activity_Date" class="form-controlwidth"/>
														
															<input  type="hidden"  id="Last_Activity_DateH" name="Last_Activity_DateH" class="form-controlwidth"/>
											
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Last_Activity_Team_Member">${ScreenLabels.getLocationMaintenace_Last_Activity_By()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text" disabled="true" id="Last_Activity_Team_Member" name="Last_Activity_Team_Member" class="form-controlwidth" value="${TeamMemberValue}" maxlength="36" />
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Last_Activity_Task">${ScreenLabels.getLocationMaintenace_Last_Activity_Task()}:</form:label> <span class="required">
													 </span>
													</label>
													<%-- <div class="col-md-10">
														<input path="Last_Activity_Task" type="text" readonly="true" id="Last_Activity_Task"  name="Last_Activity_Task" class="form-controlwidth" maxlength="20" />
													</div> --%>
													<div class="col-md-10">
													<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="Last_Activity_Task" name="Last_Activity_Task">
															<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															
														<c:forEach items="${SelectLastActivityTask}" var="DropDownLastActivityTask">
													    		<option value="${DropDownLastActivityTask.getGeneralCode()}">${DropDownLastActivityTask.getDescription20()}</option>														   
															 </c:forEach>	
														</select>
														</div>
												</div>
												
												
    											<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Fullfillment_Center_ID">${ScreenLabels.getLocationMaintenace_FulFillment_Center()}:</form:label> <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<select  style="display:inline"  class="table-group-action-input form-control input-medium" id="Fullfillment_Center_ID" name="Fullfillment_Center_ID">												
																	 <option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															            <c:forEach items="${SelectFulFillment}" var="FulfilmentCenter">
                                                                       <option value="${FulfilmentCenter.getId().getFulfillmentCenter()}">${FulfilmentCenter.getName20()}</option>
																	</c:forEach>	
														</select>
 														<span id="ErrorMessageFullfillment_Center_ID" class="error">	</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"> <form:label path="Location">${ScreenLabels.getLocationMaintenace_Location()}:</form:label> <span class="required">
													* </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth" maxlength="20" name="Location"  id="Location"  />																						
														<span id="ErrorMessageLocation" class="error">	</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"> <form:label path="Area">${ScreenLabels.getLocationMaintenace_Area()}:</form:label> <span class="required">
													* </span>
													</label>
													
													<div class="col-md-10">
													<%-- 	<input  path="Area" type="text" class="form-controlwidth"  maxlength="100" name="Area"  id="Area"  /> --%>																						
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="Area" name="Area">
															<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															
														<c:forEach items="${SelectArea}" var="DropDownArea">
													    		<option value="${DropDownArea.getGeneralCode()}">${DropDownArea.getDescription20()}</option>														   
															 </c:forEach>	
														</select>
														
														<span id="ErrorMessageArea" class="error">	</span>
													</div>
												</div>
												
												
													
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Location_Type">${ScreenLabels.getLocationMaintenace_Location_Type()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="Location_Type" name="Location_Type">
														<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<c:forEach items="${SelectLocationType}" var="DropDownLocationtype">
														    		<option value="${DropDownLocationtype.getGeneralCode()}">${DropDownLocationtype.getDescription20()}</option>														   
														    </c:forEach>	
														</select>
														
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label"> <form:label path="Description20">${ScreenLabels.getLocationMaintenace_Location_Short()}:</form:label>
													 <span class="required">
													* </span>
													</label>
													
													<div class="col-md-10">
														<input   type="text" class="form-controlwidth" maxlength="20" name="Description20"  id="Description20"  />																						
														<span id="ErrorMessageDescription20" class="error">	</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Description50">${ScreenLabels.getLocationMaintenace_Location_Long()}:</form:label>
													 <span class="required">
													* </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="50" name="Description50"  id="Description50"  />																						
														<span id="ErrorMessageDescription50" class="error"></span>
													</div>
												</div>
												
												
												
											
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Location_Profile">${ScreenLabels.getLocationMaintenace_Location_Profile()}:</form:label>  <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<select  style="display:inline"  class="table-group-action-input form-control input-medium" id="Location_Profile" name="Location_Profile">
														<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<%--<c:forEach items="${SelectLocationProfilevalue}" var="DropDownLocationProfile">
														    		<option value="${DropDownLocationProfile.getLocationProfileId().getProfile()}">${DropDownLocationProfile.getDescription20()}</option>														   
														    </c:forEach>	 --%>
														</select>
														<span id="ErrorMessageLocation_Profile" class="error"></span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Wizard_ID">${ScreenLabels.getLocationMaintenace_Wizard_ID()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select  style="display:inline"  class="table-group-action-input form-control input-medium" id="Wizard_ID" name="Wizard_ID">
															<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<c:forEach items="${SelectWizardID}" var="DropDownWizardID">
														    		<option value="${DropDownWizardID.getGeneralCode()}">${DropDownWizardID.getDescription20()}</option>														   
														    </c:forEach>	

														</select>
														
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Wizard_Control_Number">${ScreenLabels.getLocationMaintenace_Wizard_Control_Number()}:</form:label>
													</label>
													
													<div class="col-md-10">
														<input id="Wizard_Control_Number"   name="Wizard_Control_Number"   maxlength="11" class="form-controlwidth" />
														<span id="ErrorMessageWizard_Control_Number" class="error"></span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Work_Group_Zone">${ScreenLabels.getLocationMaintenace_Work_Group_Zone()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select  style="display:inline"  class="table-group-action-input form-control input-medium" id="Work_Group_Zone" name="Work_Group_Zone">
															<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<c:forEach items="${SelectWorkGroupZone}" var="DropDownWorkGroupZone">
														    		<option value="${DropDownWorkGroupZone.getGeneralCode()}">${DropDownWorkGroupZone.getDescription20()}</option>														   
														    </c:forEach>	
														</select>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Work_Zone">${ScreenLabels.getLocationMaintenace_Work_Zone()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="Work_Zone" name="Work_Zone">
															<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<c:forEach items="${SelectWorkZone}" var="DropDownWorkZone">
														    		<option value="${DropDownWorkZone.getGeneralCode()}">${DropDownWorkZone.getDescription20()}</option>														   
														    </c:forEach>	
														</select>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Check_Digit">${ScreenLabels.getLocationMaintenace_Check_Digit()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text"   id="Check_Digit" name="Check_Digit" class="form-controlwidth"  maxlength="20" />
													</div>
												</div>
												
												
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Alternate_Location">${ScreenLabels.getLocationMaintenace_Alternate_Location()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text"  id="Alternate_Location" name="Alternate_Location" class="form-controlwidth"  maxlength="20" />
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Location_Height">${ScreenLabels.getLocationMaintenace_Location_Height()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text"  id="Location_Height" name="Location_Height" class="form-controlwidth" maxlength="12" />
														<span id="ErrorMessageLocation_Height" class="error long_errormsg"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Location_Width">${ScreenLabels.getLocationMaintenace_Location_Width()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text" id="Location_Width" name="Location_Width"  class="form-controlwidth"  maxlength="12" />
														<span id="ErrorMessageLocation_Width" class="error long_errormsg"></span>
													</div>
												</div> 
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Location_Depth">${ScreenLabels.getLocationMaintenace_Location_Depth()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text"  id="Location_Depth" name="Location_Depth" class="form-controlwidth" maxlength="12" />
														<span id="ErrorMessageLocation_Depth" class="error long_errormsg"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Location_Weight_Capacity">${ScreenLabels.getLocationMaintenace_Location_Weight_Capacity()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text" id="Location_Weight_Capacity" name="Location_Weight_Capacity" class="form-controlwidth"  maxlength="12" />
														<span id="ErrorMessageLocation_Weight_Capacity" class="error long_errormsg"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Location_Height_Capacity">${ScreenLabels.getLocationMaintenace_Location_Height_Capacity()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text" id="Location_Height_Capacity" name="Location_Height_Capacity" class="form-controlwidth"  maxlength="12" />
														<span id="ErrorMessageLocation_Height_Capacity" class="error long_errormsg"></span>
													</div>
												</div>
												
												
											
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Number_Pallets_Fit">${ScreenLabels.getLocationMaintenace_Number_of_Pallets()}:</form:label><span class="required">
													 </span> 
													</label>
													<div class="col-md-10">
														<input  type="text" name="Number_Pallets_Fit" id="Number_Pallets_Fit" class="form-controlwidth" maxlength="3" />
													    <span id="ErrorMessageNumber_Pallets_Fit" class="error"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Number_Floor_Locations">${ScreenLabels.getLocationMaintenace_Number_of_Floor_Pallet_Location()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<input  type="text" id="Number_Floor_Locations" name="Number_Floor_Locations" class="form-controlwidth"  maxlength="3" />
														 <span id="ErrorMessageNumber_Floor_Locations" class="error long_errormsg"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Allocation_Allowable">${ScreenLabels.getLocationMaintenace_Allocation_Allowable()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select  style="display:inline"  class="table-group-action-input form-control input-medium" id="Allocation_Allowable" name="Allocation_Allowable">
															<option  value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<option  value="Y">${ScreenLabels.getLocationMaintenace_Yes()}</option>
															<option  value="N">${ScreenLabels.getLocationMaintenace_No()}</option>
														</select>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Free_Location_When_Zero">${ScreenLabels.getLocationMaintenace_Free_Location_When_Zero()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="Free_Location_When_Zero" name="Free_Location_When_Zero">
															<option  value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<option value="Y">${ScreenLabels.getLocationMaintenace_Yes()}</option>
															<option  value="N">${ScreenLabels.getLocationMaintenace_No()}</option>
														</select>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label reducespace"><form:label path="Free_Prime_Days">${ScreenLabels.getLocationMaintenace_Free_Prime_Location_in_99999_Days()}:</form:label><span class="required">
													 </span>
													</label>
													<div class="col-md-10 reducespac2">
													<input type="text" id="Free_Prime_Days" name="Free_Prime_Days" class="form-controlwidth"  maxlength="5" />
													 <span id="ErrorMessageFree_Prime_Days" class="error long_errormsg"></span>
													</div>
												</div>
												
												 <div class="form-group">
													<label class="col-md-2 control-label reducespac1"><form:label path="Multiple_Items">${ScreenLabels.getLocationMaintenace_Multiple_Products_In_The_Same_Location()}:</form:label><span class="required">
													 </span>
													</label>
													<div class="col-md-10 reducespac1 reducespac4">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="Multiple_Items" name="Multiple_Items">
															<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<option value="Y">${ScreenLabels.getLocationMaintenace_Yes()}</option>
															<option  value="N">${ScreenLabels.getLocationMaintenace_No()}</option>
														</select>
														
													</div>
												</div>
 												
												<div class="form-group">
													<label class="col-md-2 control-label reducespace3"><form:label path="Storage_Type">${ScreenLabels.getLocationMaintenace_Storage_Type()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10 reducespace3">
														<select  style="display:inline"  class="table-group-action-input form-control input-medium" id="Storage_Type" name="Storage_Type">
														<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															<c:forEach items="${SelectStorageType}" var="DropDownStorageType">
														    		<option value="${DropDownStorageType.getGeneralCode()}">${DropDownStorageType.getDescription20()}</option>														   
														    </c:forEach>															</select>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Slotting">${ScreenLabels.getLocationMaintenace_Slotting()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:select  path="Slotting" style="display:inline"  class="table-group-action-input form-control input-medium" id="Slotting" name="Slotting">
															<form:option  path="Slotting" value="">${ScreenLabels.getLocationMaintenace_Select()}..</form:option>
															<form:option  path="Slotting"  value="M">${ScreenLabels.getLocationMaintenace_Manual_Assigned_Location()}</form:option>
															<form:option  path="Slotting" value="S">${ScreenLabels.getLocationMaintenace_Used_In_Slotting()}</form:option>
														</form:select>
														
													</div>
												</div>
												
												
										<div class="margin-bottom-5-right-allign Custom_AddButtons">
												  
												<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>${ScreenLabels.getLocationMaintenace_Save_Update()}</button>
												<button type="button" onclick="return resetPage();" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i>${ScreenLabels.getLocationMaintenace_Reset()}</button>
												<button type="button" onclick="return reloadPage();" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i>${ScreenLabels.getLocationMaintenace_Cancel()}</button></div>																						
											</div>
										</div>
										
										<script>
										function resetPage(){													
											
											/* location.reload(); */
											window.parent.location = window.parent.location.href;
										}
												function reloadPage(){													
													
													var backState = '${backStatus}';
												     
													
													   if (backState == 'lookup') {

														   window.location="${pageContext.request.contextPath}/Location_Maintenance_Search";
													   
													  }
													  else{
													   
														  window.location="${pageContext.request.contextPath}/Location_Maintenance_Search_Lookup";
													  } 
													   
													
													 
													 
												}
												</script>
										
										
									</div>
								</div>
							</div>
						</div>
						
						
						</form:form>
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>


<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<!-- This is used to Set Custom Css that created by Aakash Bishnoi for Location's all Screens -->
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />
<script>

function locationProfileDropdown(){
	 
	  var firstDropVal = $("#Fullfillment_Center_ID").val().replace(/&quot/g,"\"");
	
	  if(firstDropVal != ""){
	$.get("${pageContext.request.contextPath}/RestSelectFulfillmentCenterValue",
			{
    			Fullfilment_Center : firstDropVal
				
			},
			function(data, status) {
				if (data) {
					 $('#Location_Profile').empty();
					  $('#Location_Profile').each(function() {
			           // Create option
			           var option = $("<option />");
			           option.attr("value", '').text('Select..');
			           $('#Location_Profile').append(option);
			           }); 
					 if(data==''){
								 var errMsgId = document
									.getElementById("MessageRestFull");
							 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ScreenLabels.getLocationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center()}';
								//document.location.href = '#top';
								window.scrollTo(0,0);
							$('#ErrorMessage').show();
							IsSubmit = false;
							return IsSubmit;		 
					 }else
						 {
						$('#ErrorMessage').hide();
					 
			      	
			          //$('#ApplicationSub').append( new Option("Select...",""));
			          for (var subApplication in data){
			        	
			           $('#Location_Profile').append( new Option(data[subApplication].description20,data[subApplication].locationProfileId.profile));
			          }
					}
				} else {			
														
				}
			});
	  }
	  else{
		  $('#ErrorMessage').hide();
	  }
}

$("#Fullfillment_Center_ID").change(function () {
  
    
    locationProfileDropdown();
   
});



function SubmitRecord(){

	var IsSubmit = false;
	 
	var testNumber = false;

	var isBlankFullfillment_Center_ID = isBlankField('Fullfillment_Center_ID',
			'ErrorMessageFullfillment_Center_ID',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Maindatory_field_can_not_be_left_blank()}");
	
	var isBlankArea = isBlankField('Area',
			'ErrorMessageArea',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Maindatory_field_can_not_be_left_blank()}");
	
	var isBlankLocation = isBlankField('Location',
			'ErrorMessageLocation',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Maindatory_field_can_not_be_left_blank()}");
	
	
	var isBlankLocationLong = isBlankField('Description50',
			'ErrorMessageDescription50',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Maindatory_field_can_not_be_left_blank()}");
	
	var isBlankLocationShort = isBlankField('Description20', 'ErrorMessageDescription20',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Maindatory_field_can_not_be_left_blank()}");
	
	var isBlankLocationProfile = isBlankField('Location_Profile',
			'ErrorMessageLocation_Profile',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Maindatory_field_can_not_be_left_blank()}");
	
	var isNumericNumberofPalet = isNumeric('Number_Pallets_Fit',
			'ErrorMessageNumber_Pallets_Fit',
	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Number_of_Pallets_must_be_numeric_only()}");
	
	var isNumericFree_Prime_Days = isNumeric('Free_Prime_Days',
			'ErrorMessageFree_Prime_Days',
	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only()}");
	
	var isNumericNumberofFlore = isNumeric('Number_Floor_Locations',
			'ErrorMessageNumber_Floor_Locations',
	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only()}");
	
	var isNumericWizard_Control_Number = isNumeric('Wizard_Control_Number', 'ErrorMessageWizard_Control_Number',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Wizard_Control_Number_must_be_numeric_only()}");
	
		
	var isNumericLocation_Height = isDecimal('Location_Height', 'ErrorMessageLocation_Height',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only()}");
	
	var isNumericLocation_Width = isDecimal('Location_Width', 'ErrorMessageLocation_Width',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only()}");
	
	var isNumericLocation_Depth = isDecimal('Location_Depth', 'ErrorMessageLocation_Depth',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only()}");
	
	var isNumericLocation_Weight_Capacity = isDecimal('Location_Weight_Capacity', 'ErrorMessageLocation_Weight_Capacity',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only()}");

	var isNumericLocation_Height_Capacity = isDecimal('Location_Height_Capacity', 'ErrorMessageLocation_Height_Capacity',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabels.getLocationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only()}");
	
	
	if(!($('#Location_Weight_Capacity').val() <= $('#Location_Width').val()))
	{
		isShowErrorMessage('ErrorMessageLocation_Weight_Capacity','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${ScreenLabels.getLocationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width()}');
		isNumericLocation_Weight_Capacity=false;
	}
	
	if(!($('#Location_Height_Capacity').val() <= $('#Location_Height').val()))
		{
		
		isShowErrorMessage('ErrorMessageLocation_Height_Capacity','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${ScreenLabels.getLocationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height()}');
		isNumericLocation_Height_Capacity=false;
		}
		
	var VarFulfillmentCenter=$('#Fullfillment_Center_ID').val();   
	var VarArea=$('#Area').val();   
	var VarLocation=$('#Location').val();  
	var VarAlternateLocation=$('#Alternate_Location').val();

	

	if(VarAlternateLocation){
		if(VarLocation.toUpperCase() == VarAlternateLocation.toUpperCase()){
			var errMsgId = document
			.getElementById("MessageRestFull");
			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ '${ScreenLabels.getLocationMaintenace_Loaction_and_Alternate_Location_cannot_be_same()}';
				//document.location.href = '#top';
				window.scrollTo(0,0);
			$('#ErrorMessage').show();
			IsSubmit = false;
			return IsSubmit;
		}
	}
	
	if (isBlankFullfillment_Center_ID && isBlankArea && isBlankLocation && isBlankLocationLong && isBlankLocationShort && isBlankLocationProfile
			&& isNumericNumberofPalet && isNumericNumberofFlore && isNumericLocation_Height && isNumericLocation_Width && isNumericLocation_Depth && isNumericLocation_Weight_Capacity && isNumericLocation_Height_Capacity && isNumericWizard_Control_Number && isNumericFree_Prime_Days) {
		$('#ErrorMessage').hide();
		$.post("${pageContext.request.contextPath}/RestCheckUniqueLocation",
				{
					FulfillmentCenter : VarFulfillmentCenter,
					Area : VarArea,
					Location : VarLocation				
				},
				function(data, status) {

					if (data) {
						var errMsgId = document
								.getElementById("MessageRestFull");
						errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ScreenLabels.getLocationMaintenace_Locaton_already_Used()}';
						$('#ErrorMessage').show();
						IsSubmit = false;
						// alert ("User Already Exists");
					} else {
																
						$.post("${pageContext.request.contextPath}/RestCheckUniqueAlternateLocation",
								{
									FulfillmentCenter : VarFulfillmentCenter,
									//Area : VarArea,
									//Location : VarLocation,	
									Alternate_Location : VarAlternateLocation
								},
								function(data, status) {
									if (data) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${ScreenLabels.getLocationMaintenace_Alternate_Locaton_already_Used()}';
										$('#ErrorMessage').show();
										//document.location.href = '#top';
										window.scrollTo(0,0);
										IsSubmit = false;
										// alert ("User Already Exists");
									} else {			
										$('#ErrorMessage').hide();
										
										 
										 
										 bootbox.alert('${ScreenLabels.getLocationMaintenace_Your_record_has_been_saved_successfully()}',function(){
										IsSubmit = true;
										
										myform.action="Location_Maintenance_Add";
										myform.submit(); 	
										});
										
									}
								});

						
					}
				});																						
	} else {
		 
		IsSubmit = false;
	} 

	/* document.location.href = '#top' */
	window.scrollTo(0,0);
	return IsSubmit;
}

$( document ).ready(function() {
	
	$('#Last_Activity_Date').val(getLocalDate());
	$('#Last_Activity_DateH').val(getUTCDateTime());
	
	$('#ErrorMessage').hide();	
		  
	  $(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	
	  
	 
	setInterval(function () {
		
    var h = window.innerHeight;
    if(window.innerHeight==928){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 100);
	});
	
function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'LocationMaintenance',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>