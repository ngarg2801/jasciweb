<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${Infohelp_ScreenLabels.getInfohelp_Info_Help_Assignment_Search()}</h1>
					</div>
				</div>
				<%-- <!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014É World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

	
	
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT --> --%>
				<div class="page-content" id="page-content">
					<div class="container">


						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">



						<%-- 	<div id="ErrorMessage" class="note note-danger"	style="display: none;">
								<p class="error"
									style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p>
							</div>
							 --%>
	<div id="ErrorMessage" class="note note-danger" style="display:none">
     <p id="MessageRestFull" class="error error-Top" style="margin-left: -7px !important;"></p>	
    </div>
							<div class="col-md-12">
								<form:form name="myForm"
									class="form-horizontal form-row-seperated" action="#"
									onsubmit="return isformSubmit();" method="get">
									<div class="portlet">
<input type="hidden" name="backStatus" id="backStatus" value="lookup">
										<div class="portlet-body">
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">
															<div class="form-group">
																<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfohelps_Enter_Info_Help()}: </label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="EnterInfoHelp" id="EnterInfoHelp"
																		><i
																		class="fa fa-search" id="BtnIdinfohelp"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('InfoHelpSearchlookup','infohelp');"></i>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfohelp_Part_of_the_Description()}: </label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="PartoftheDescription" id="PartoftheDescription"
																		> <i
																		class="fa fa-search" id="BtnIddescription"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('InfoHelpSearchlookup','description');"></i>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${Infohelp_ScreenLabels.getInfohelp_Info_Help_Type()}: </label>
																<div class="col-md-10">
																	<c:set var="StrInfoHelpType"
																		value="${SelectInfoHelpType}" />
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="InfoHelpType" id="InfoHelpType">
																		<option value="">${Infohelp_ScreenLabels.getInfohelp_Select()}..</option>
																		<c:forEach items="${StrInfoHelpType}"
																			var="SelectInfoHelpType">

																			<option value="${SelectInfoHelpType.getGeneralCode()}">${SelectInfoHelpType.getDescription20()}</option>
																		</c:forEach>
																	</select> <i class="fa fa-search"  id="BtnIdInfoHelpType"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('InfoHelpSearchlookup','InfoHelpType');"></i>
																</div>
															</div>


															<div
																class="margin-bottom-5-right-allign_icon_maintenanace">
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom"
																	onclick="actionForm('new','new');">
																	<i class="fa fa-plus"></i>&nbsp;${Infohelp_ScreenLabels.getInfohelp_New()}
																</button>

																
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdDisplayAll"
																	onclick="actionForm('InfoHelpSearchlookup','displayall');">
																	<i class="fa fa-check"></i>&nbsp;${Infohelp_ScreenLabels.getInfohelp_DisplayAll()}
																</button>
															</div>

														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form:form>

							</div>
							<!--end tabbable-->

							<!--end tabbable-->

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->





			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>

<script>

function checkBlank(fieldID){
 var fieldLength = document.getElementById(fieldID).value.trim().length;
 if (fieldLength>0) {
  
  return true;

 }
 else {
  
  
  return false;
 }
}


jQuery(document).ready(function() {    

	$(window).keydown(function(event){
              if(event.keyCode == 13) {
               		   
               var BoolEnterInfoHelp = checkBlank('EnterInfoHelp');
               var BoolPartoftheDescription = checkBlank('PartoftheDescription');
               var BoolInfoHelpType = checkBlank('InfoHelpType');
            
			   
          if(BoolEnterInfoHelp){
			
				$('#BtnIdinfohelp').click();
          }
		   else if(BoolPartoftheDescription){
			
				$('#BtnIddescription').click();
          }
		  else if(BoolInfoHelpType){
			 
				$('#BtnIdInfoHelpType').click();
          }
		  /*  else{
		  $('#BtnIdDisplayAll').click();
		  } */
			event.preventDefault();
            return false;
              }
          });
	
	  
	
 
});
setInterval(function () {
	
    var h = window.innerHeight;
    if(window.innerHeight>=900 || window.innerHeight==1004 ){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
</script>
<script>
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(url, action) {
		$('#ErrorMessage').hide();

		if (action == 'infohelp') {

			var validinfohelp = isBlankField('EnterInfoHelp', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelps_Please_enter_Info_Help()}');

			var BlankDescription = document
					.getElementById("PartoftheDescription"); // Get text field
			var BlankInfohelptype = document.getElementById("InfoHelpType");
			BlankDescription.value = "";
			BlankInfohelptype.value = "";

			if (validinfohelp) {

				var infohelp1 = $('#EnterInfoHelp').val();
				$.post(
								"${pageContext.request.contextPath}/RestInfohelpListAllCheck",
								{
									InfoHelp : infohelp1,
									Description :"",
									InfoHelpType :""
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Infohelp_ScreenLabels.getInfoHelps_Invalid_Info_Help()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										//isSubmit = true;
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post info help

			}
			else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		}//end infohelp if 

		else if (action == 'description') {

			var validdescription = isBlankField('PartoftheDescription',
					'MessageRestFull', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelps_Please_enter_Part_of_the_Description()}');

			var BlankEnterInfoHelp = document.getElementById("EnterInfoHelp"); // Get text field
			var BlankInfohelptype = document.getElementById("InfoHelpType");
			BlankEnterInfoHelp.value = "";
			BlankInfohelptype.value = "";
			if (validdescription) {
	
				var infoPartoftheDescription = $('#PartoftheDescription').val();
				$.post(
								"${pageContext.request.contextPath}/RestInfohelpListAllCheck",
								{
									InfoHelp : "",
									Description :infoPartoftheDescription,
									InfoHelpType :""
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Infohelp_ScreenLabels.getInfoHelps_Invalid_Description()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										//isSubmit = true;
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} else if (action == 'InfoHelpType') {

			var validdescription = isBlankField('InfoHelpType', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Infohelp_ScreenLabels.getInfoHelps_Please_select_Info_Help_Type()}');

			var BlankEnterInfoHelp = document.getElementById("EnterInfoHelp"); // Get text field
			var BlankDescription = document
					.getElementById("PartoftheDescription");

			BlankEnterInfoHelp.value = "";
			BlankDescription.value = "";
			if (validdescription) {

				
				
				
				
				var InfoHelpType1 = $("#InfoHelpType option:selected").val()
				$.post(
								"${pageContext.request.contextPath}/RestInfohelpListAllCheck",
								{
									InfoHelp : "",
									Description :"",
									InfoHelpType :InfoHelpType1
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${Infohelp_ScreenLabels.getInfoHelps_Invalid_Info_Help_Type()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										//isSubmit = true;
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							
				
				
				
			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} else if (action == 'displayall') {

			var BlankEnterInfoHelp = document.getElementById("EnterInfoHelp"); // Get text field
			var BlankDescription = document
					.getElementById("PartoftheDescription");
			var BlankInfohelptype = document.getElementById("InfoHelpType");
			BlankEnterInfoHelp.value = "";
			BlankDescription.value = "";
			BlankInfohelptype.value = "";
			
			isSubmit = true;
			document.myForm.action = url;
			document.myForm.submit();

		}
		else if(action == 'new'){
			var url="${pageContext.request.contextPath}/InfoHelpassignmentmaintenance_add";
			document.myForm.action = url;
			document.myForm.submit();
		
			
		}
return isSubmit;
	}
	  function headerChangeLanguage(){
      	
      	/* alert('in security look up'); */
      	
       $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
      	
      	
      }

      function headerInfoHelp(){
      	
      	//alert('in security look up');
      	
       	/* $
      	.post(
      			"${pageContext.request.contextPath}/HeaderInfoHelp",
      			function(
      					data1) {

      				if (data1.boolStatus) {

      					location.reload();
      				}
      			});
      	 */
       	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
      			  {InfoHelp:'InfoHelp',
       		InfoHelpType:'PROGRAM'
      		 
      			  }, function( data1,status ) {
      				  if (data1.boolStatus) {
      					  window.open(data1.strMessage); 
      					  					  
      					}
      				  else
      					  {
      					//  alert('No help found yet');
      					  }
      				  
      				  
      			  });
      	
      }
</script>

<!-- END JAVASCRIPTS -->

<!-- END BODY -->
</html>