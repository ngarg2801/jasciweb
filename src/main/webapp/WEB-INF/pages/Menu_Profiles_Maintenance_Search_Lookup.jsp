<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">

<link href="<c:url value="/resources/assets/global/css/MenuProfileMaintenance.css"/>" rel="stylesheet" type="text/css">

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">



<body>
	<div id="container" style="position: relative" class="loader_div">
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${objMenuOptionLabel.getLblMenuProfilesMaintenanceSearchLookup()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
	
			<div class="container">
			<!-- BEGIN PAGE CONTENT INNER -->
			<div>
								<a id="AddBtn2" href="${pageContext.request.contextPath}/MenuProfileMaintenance" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${objMenuOptionLabel.getLblAddnew()} </span>
								</a>
								
									</div>
			<div class="row margin-top-10">
			<div class="col-md-12">
					<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
					<!-- Begin: life time stats -->
					
					<div class="portlet">
						
							
													
													
													
											
				<%-- 			<div class="actions" style="margin-bottom:-2px;">
								<a id="AddBtn" href="${pageContext.request.contextPath}/MenuProfileMaintenance" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${objMenuOptionLabel.getLblAddnew()} </span>
								</a>
								
									
									
							</div> --%>
						
			<div class="row">
			
					<kendo:grid name="SearchLookupGridTenant" id="MenuProfileGrid"
									resizable="true" reorderable="true" sortable="true" dataBound="gridDataBound">

									<kendo:grid-pageable refresh="true" pageSizes="false"
										buttonCount="5">
									</kendo:grid-pageable>
									<kendo:grid-editable mode="inline" confirmation="Message" />
									<kendo:grid-columns>


										<kendo:grid-column title="${objMenuOptionLabel.getLblMenuProfile()}" field="id.menuProfile" />
										<kendo:grid-column title="${objMenuOptionLabel.getLblMenuType()}" field="menuType" />
										<kendo:grid-column title="${objMenuOptionLabel.getLblDescription()}" field="description50" />
										
										
										<kendo:grid-column title="${objMenuOptionLabel.getLblAction()}" width="18%;">
											<kendo:grid-column-command>

												<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
												<kendo:grid-column-commandItem
													className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
													name="editDetails" text="${objMenuOptionLabel.getLblEdit()}">
													<kendo:grid-column-commandItem-click>
														<script>
                           
                            function editTeammember(e) {
                               

                                e.preventDefault();
	
                                
                                var dataItemEdit = this.dataItem($(e.currentTarget).closest("tr"));
                                var menuOptionVal=dataItemEdit.id.menuProfile;
                                //var tenantval=dataItemEdit.tenant;
                          
                               
                               
                               
                             
                                 var url="${pageContext.request.contextPath}/MenuProfileMaintenance";
                                //var wnd = $("#details").data("kendoWindow");
                               
    							 var myform = document.createElement("form");


                                 var tenantFiled = document.createElement("input");
                                 tenantFiled.value = "tenant";
                                 tenantFiled.name = "tenant";
                                 tenantFiled.setAttribute("type", "hidden");
                                 
                                 var menuOptionFiled = document.createElement("input");
                                 menuOptionFiled.value = menuOptionVal;
                                 menuOptionFiled.name = "lblMenuProfile";
                                 menuOptionFiled.setAttribute("type", "hidden");
                                 
                                 
                                 myform.action = url;
                                 myform.method = "get";
                                 myform.appendChild(tenantFiled);
                                 myform.appendChild(menuOptionFiled);
                                 document.body.appendChild(myform);
                                 myform.submit();
    							//alert(res);
                              // window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                            }
                            
                          
                            </script>



													</kendo:grid-column-commandItem-click>
												</kendo:grid-column-commandItem>

												<kendo:grid-column-commandItem
													className="btn btn-sm red filter-cancel fa fa-times"
													name="btnDelete" text="${objMenuOptionLabel.getLblDelete()}">

													<kendo:grid-column-commandItem-click>

														<script>						   
							    function deleteRecords(e) { 
                               

                                e.preventDefault();
                                var dataItemGrid = this.dataItem($(e.currentTarget).closest("tr"));
                                
                                
                               var currentStatus=dataItemGrid.id.menuProfile;
                               var valTenant=dataItemGrid.id.tenant;
                               $.post("${pageContext.request.contextPath}/Check_MenuProfile",
              						 {
                            	   MenuOptionValue:currentStatus,
                            	   MenuProfileTenant:valTenant
              					
              					 }, function( data,status) {
              					 
              					
              					 if(!data)
              		        	 {
              						 
              						 bootbox
                                     .confirm(
                                       '${objMenuOptionLabel.getLbl_ERR_WHILE_DELETING_MENU_OPTION()}',
                                       function(okOrCancel) {

                                        if(okOrCancel == true)
                                        {
              					
              						/* if(confirm('${objMenuOptionLabel.getLbl_ERR_WHILE_DELETING_MENU_OPTION()}')==true){
              						 */	
              							/* var datassfsd=dataItemSss($(e.currentTarget).closest("tr")); */
                                        
              							
              							//var tenantval=dataItemGrid.tenant;
              							
                                        var menuOptionVal=dataItemGrid.id.menuProfile;
                                        var valTenant=dataItemGrid.id.tenant;
                                        var url= "${pageContext.request.contextPath}/MenuProfile_searchlookup_delete";
        	                           
        						    	
        						    	 var myform1 = document.createElement("form");

                                         var tenantFiled = document.createElement("input");
                                         tenantFiled.value = valTenant;
                                         tenantFiled.name = "tenant";
                                         tenantFiled.setAttribute("type", "hidden");
                                         
                                         var menuOptionFiled = document.createElement("input");
                                         menuOptionFiled.value = menuOptionVal;
                                         menuOptionFiled.name = "MenuProfile";
                                         menuOptionFiled.setAttribute("type", "hidden");
                                         
                                         myform1.action = url;
                                         myform1.method = "post";
                                        
                                         
                                         myform1.appendChild(tenantFiled);
                                         myform1.appendChild(menuOptionFiled);
                                         
                                         
                                         document.body.appendChild(myform1);
                                         myform1.submit();
        						    	
              							
              						}else{
              							
              							
              							
              							
              						}
                                        });
              		        	 }
              					 else{
              						 
              						 
              						 bootbox
                                     .confirm(
                                       '${objMenuOptionLabel.getLbl_ERR_WHILE_DELETING_MENU_PROFILE()}',
                                       function(okOrCancel) {

                                        if(okOrCancel == true)
                                        {
              						
              						/*  if(confirm('${objMenuOptionLabel.getLbl_ERR_WHILE_DELETING_MENU_PROFILE()}')==true){
              						 */	 
              							var valTenant=dataItemGrid.id.tenant;
              							
										var menuOptionVal=dataItemGrid.id.menuProfile;
                                        var url= "${pageContext.request.contextPath}/MenuProfile_searchlookup_delete";
        	                           
        						    	 
        						    	 var myform1 = document.createElement("form");

                                         var tenantFiled = document.createElement("input");
                                         tenantFiled.value = valTenant;
                                         tenantFiled.name = "tenant";
                                         tenantFiled.setAttribute("type", "hidden");
                                         
                                         var menuOptionFiled = document.createElement("input");
                                         menuOptionFiled.value = menuOptionVal;
                                         menuOptionFiled.name = "MenuProfile";
                                         menuOptionFiled.setAttribute("type", "hidden");
                                         
                                         myform1.action = url;
                                         myform1.method = "post";
                                        
                                         
                                         myform1.appendChild(tenantFiled);
                                         myform1.appendChild(menuOptionFiled);
                                         
                                         
                                         document.body.appendChild(myform1);
                                         myform1.submit();
        						    	
        						    	
              							 
              						 }else{
              							 
              						 }
                                        });
              						//return isSubmit;
              					 }
              				 });
                                
                          
                               
										} 
							    </script>
													</kendo:grid-column-commandItem-click>
												</kendo:grid-column-commandItem>

											</kendo:grid-column-command>
										</kendo:grid-column>
									</kendo:grid-columns>



									<kendo:dataSource autoSync="true" pageSize="10">
										<kendo:dataSource-transport>
											<kendo:dataSource-transport-read cache="false"
												url="${pageContext.request.contextPath}/RestFull_MenuProfile_SearchLookUp">
											</kendo:dataSource-transport-read>



											<kendo:dataSource-transport-parameterMap>

												<script>
                  function parameterMap(options,type) { 
                   
                    return JSON.stringify(options);
                   
                  }
                  
                  
                
                
                  </script>
											</kendo:dataSource-transport-parameterMap>

										</kendo:dataSource-transport>
									</kendo:dataSource>
								</kendo:grid>

			
				</div>
				
				</div>
				</div>
			
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
</div>
</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>
<script>
function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
    jQuery(document).ready(function() {    
    	
    	function headerChangeLanguage(){
    	  	
    	  	/* alert('in security look up'); */
    	  	
    	  $.ajax({
    			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
    			type: 'GET',
    	        cache: false,
    			success: function(
    					data1) {

    				if (data1.boolStatus) {

    					location.reload();
    				}
    			}
    		});
    	  	
    	  }
    	
    	setInterval(function () {
    		
    	    var h = window.innerHeight;		    
    	    if(window.innerHeight>=900){
    	    	h=h-187;
    	    	
    	    }
    	   else{
    	    	h=h-239;
    	    }
    	      document.getElementById("page-content").style.minHeight = h+"px";		     
    	    
    		}, 30);
           //Metronic.init(); // init metronic core components
//Layout.init(); // init current layout
//Demo.init(); // init demo features
         //  EcommerceOrders.init();
        });
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'MenuProfileMaintenance',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }	
    </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>