<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->

		<!-- BEGIN PAGE CONTENT -->
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_MenuAppIconLookUpLabel()}</h1>
					</div>
				</div>

				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active"></li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<div class="col-md-12">
								<form class="form-horizontal form-row-seperated" action="#"
									name="myForm" onsubmit="return isformSubmit();">
									<input type="hidden" name="backStatus" id="backStatus" value="lookup">
									
									<div class="portlet">

										<div class="portlet-body">
											<div id="ErrorMessage" class="note note-danger display-none " style="display: none;">
												<p id="Perror" class="error error-Top margin-left-7pix">error</p>
											</div>
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">

															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_EnterAppIconLabel()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth" name="AppIcon"
																		id="AppIcon" >
																	<i class="fa fa-search" id="searchAppIcon"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																		onclick="actionForm('appicon');"></i>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_PartOfTheAppIconNameLabel()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		id="AppIconName" name="AppIconName" >
																	<i class="fa fa-search" id="searchAppIconName"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																		onclick="actionForm('appiconname');"></i>
																</div>
															</div>
															<div class="form-group" id="particon" style="display: none;">
																<label class="col-md-2 control-label">${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_PartOfTheAppIconLabel()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		id="AppIconPart" name="AppIconPart" >
																	<i class="fa fa-search"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer;"
																		onclick="actionForm('appiconpart');"></i>
																</div>
															</div>

															<div
																class="margin-bottom-5-right-allign_info_app_icon_main margin-right-lookup-btn">
																<button class="btn btn-sm yellow margin-bottom"
																	onclick="actionForm('new');">
																	<i class="fa fa-plus"></i> ${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ButtonNewText()}
																</button>
																<button class="btn btn-sm yellow  margin-bottom" id="btnDispID"
																	onclick="actionForm('displayAll');">
																	<i class="fa fa-check"></i> ${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ButtonDisplayAllText()}
																</button>
															</div>


														</div>
													</div>
			</div>
											</div>
										</div>
									</div>
								</form>
							</div>

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(action) {
		$('#ErrorMessage').hide();
		if (action == 'appicon') {
			$('#AppIconName').val('');
			$('#AppIconPart').val('');
			var validAppIcon = isBlankField(
					'AppIcon',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON()}');

			if (validAppIcon) {

				var varAppIcon = $('#AppIcon').val();
				var errMsgId = document.getElementById("Perror");
				$
						.post(
								"${pageContext.request.contextPath}/RestCheckIcon",
								{
									StrAppIcon : varAppIcon,
									StrAppIconName : "",
									StrAppIconPart : "",
									StrSearchBy:"StrAppIcon"
	
								},
								function(data, status) {

									if (data.length==0) {

										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_INVALID_APP_ICON()}';
										$('#ErrorMessage').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/Menuappicon_addorupdate_request';
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check team member			

			} else {

				$('#ErrorMessage').show();
				/* var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = ""; */
				isSubmit = false;

			}

		}//end if appicon
		else if (action == 'appiconname') {
			 $('#AppIcon').val('');
			 $('#AppIconPart').val('');
			var validAppIconName = isBlankField(
					'AppIconName',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME()}');

			if (validAppIconName) {

				var varAppIconName = $('#AppIconName').val();
				var errMsgId = document.getElementById("Perror");
				$
						.post(
								"${pageContext.request.contextPath}/RestCheckIcon",
								{
									StrAppIcon : "",
									StrAppIconName : varAppIconName,
									StrAppIconPart : "",
									StrSearchBy:"StrAppIconName"

								},
								function(data, status) {

									if (data.length==0) {

										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME()}';
										$('#ErrorMessage').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/Menu_app_icon_search_by_part_lookup';
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check icon part name		

			} else {
				$('#ErrorMessage').show();

				isSubmit = false;
			}

		}

		else if (action == 'appiconpart') {

			$('#AppIconName').val('');
			$('#AppIcon').val('');
			var validAppIconPart = isBlankField(
					'AppIconPart',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON()}');

			if (validAppIconPart) {

				
				var varAppIconPart = $('#AppIconPart').val();
				var errMsgId = document.getElementById("Perror");
				$
						.post(
								"${pageContext.request.contextPath}/RestCheckIcon",
								{
									StrAppIcon :"",
									StrAppIconName :"",
									StrAppIconPart :varAppIconPart,
									StrSearchBy:"StrAppIconPart"

								},
								function(data, status) {

									if (data.length==0) {

										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${MenuAppIconMaintenanceSceernLabelObj.getMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON()}';
										$('#ErrorMessage').show();
										isSubmit = false;
									} else {
										$('#ErrorMessage').hide();
										isSubmit = true;
										document.myForm.action = '${pageContext.request.contextPath}/Menu_app_icon_search_by_part_lookup';
										document.myForm.submit();
										//return isSubmit;
									}
								});//end check icon part name	
				
				
				
				
			} else {
				$('#ErrorMessage').show();

				isSubmit = false;
			}

		}

		else if (action == 'new') {

			document.myForm.action = '${pageContext.request.contextPath}/'
					+ 'Menu_app_icon_maintenance_new';
			document.myForm.submit();

		} else if (action == 'displayAll') {

			document.myForm.action = '${pageContext.request.contextPath}/'
					+ 'Menu_app_icon_search_lookup';
			document.myForm.submit();

		}

	}
	
	function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}

	function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'MenuApp',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						//  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
</script>
<script>
	jQuery(document).ready(function() {
		$('#particon').hide();
		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
			
			var BoolAppIconID = checkBlank('AppIcon');
            var BoolAppIconName= checkBlank('AppIconName');
			 if(BoolAppIconID && BoolAppIconName){
                  $('#searchAppIcon').click();
              }
			else if(BoolAppIconID && !BoolAppIconName){
			$('#searchAppIcon').click();
			
			}
			else if(!BoolAppIconID && BoolAppIconName){
			$('#searchAppIconName').click();
			}
			/* else{
			$('#btnDispID').click();
			} */
			
		      event.preventDefault();
		      return false;
		    }
		  });
	setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);

		/* Metronic.init(); // init metronic core componets
		Layout.init(); // init layout
		Demo.init(); // init demo(theme settings page)
		Index.init(); // init index page
		Tasks.initDashboardWidget(); // init tash dashboard widget */
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>