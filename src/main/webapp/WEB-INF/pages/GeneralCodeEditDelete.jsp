<!-- 
Date Developed  Sep 18 2014
Description It is used to Add value of General Code Identification/General Code
Created By Aakash Bishnoi -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL" %>
<!DOCTYPE html>

  
    <%!
    	COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
    	
	%>
<c:set var="StrTenant" value="<%=objCommonsessionbe.getTenant()%>"/>	
<c:set var="StrCompany" value="<%=objCommonsessionbe.getCompany()%>"/>	


<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014ɠWorld Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<style>
.error {
	color: #ff0000;
}
 
.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
#AddButtonCss
{
padding: 0;
margin-bottom: 9px;
min-height: 41px;
width: 100%;
float: left;
}
</style>

	<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

	<div class="page-head">
   <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
   <h1>${ScreenLabel.getGeneralCodes_GeneralCode_NewEdit()}</h1>
    </div>
    </div>
	<div class="page-content" id="page-content">
		<div class="container">
			
    <div id="ConstraintMessage" class="note note-danger" style="display:none" >
     <p  class="error error-Top" id="MessageRestFull" style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ConstraintViolation}</p>	
    </div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			
			<div class="row margin-top-10">
				<div class="col-md-12">
				
					<form:form  name="myform" method="post" action="GeneralCodeEditDelete" onsubmit="return body_load();" class="form-horizontal form-row-seperated" modelAttribute="GENERAL_CODES">
					
										<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
	
	 
<%-- <div>${ConstraintViolation}</div> --%>
	<input type="hidden" name="getLastActivitydate" id="getLastActivitydate">
   	<div>
     	<div><form:input path="Tenant" value="${StrTenant}" type="hidden"/>
     </div>
    
    
    <div>
     <div><form:input path="Company" value="${StrCompany}"  type="hidden"/>
     </div>
    
												<div class="form-group">
													<label class="col-md-2 control-label" ><form:label path="Application">${ScreenLabel.getGeneralCodes_Application()}:</form:label><span class="required">
													* </span>
													</label>
													<div  class="col-md-10">													
													<c:set var="StrApplication" value="${StrApplication}"/>		
																										
													<select name="SelectApplication" id="SelectApplication"   style="display:inline" class="table-group-action-input form-control input-medium">													
													<c:choose>													
													<c:when test="${fn:containsIgnoreCase(StrGeneralCode, \"APPLICATIONS\") || StrGeneralCode == null}">
													<option>${StrApplication}</option>
													<script>
													$("#SelectApplication").attr("readonly", "true");
													$("#SelectApplication").css('pointer-events', 'none');
													</script>
													</c:when>
													<c:when test="${StrGeneralCode != \"APPLICATIONS\" || StrGeneralCode != null}">
													 <option value="">${ScreenLabel.getGeneralCodes_Select()}..</option>
													 <c:forEach items="${ApplicationValue}" var="Applicationvalues">
													    
													     <option>${Applicationvalues.getGeneralCode()}</option>
													 </c:forEach>
													</c:when>
													</c:choose>		
													<%-- <option>${StrApplication}</option>	 --%>															
													</select>
													<span id="ErrorMessageSelectApplication" class="error-select"></span>												
													</div>
												</div>
												
												<div style="pointer-events: none" class="form-group">
													<label class="col-md-2 control-label"><form:label path="GeneralCodeID">${ScreenLabel.getGeneralCodes_GeneralCodeID()}:</form:label> <span class="required">
													* </span>
													</label>
													
													<div id="disablegeneralcode" name="disablegeneralcode" class="col-md-10">
														<c:set var="StrGeneralCode" value="${StrGeneralCode}"/>
														<select readonly="true" name="SelectGeneralCode" id="SelectGeneralCode"   class="table-group-action-input form-control input-medium">
															 
															 <c:choose>
															 
														        <c:when test="${StrGeneralCode != null}">
														       
														            <option id="menuvaluecheck"><c:out  value="${StrGeneralCode}"/></option>
														        </c:when>
														        <c:otherwise>
														            <option value="GENERALCODES">${ScreenLabel.getGeneralCodes_GENERALCODES()}</option>
														            <script>
														            
														            </script>
														         
														        </c:otherwise>
														    </c:choose>													
														</select>
														<span id="ErrorMessageSelectGeneralCode" class="error-select">	</span>	
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"> <form:label path="GeneralCode">${ScreenLabel.getGeneralCodes_GeneralCode()}:</form:label><span class="required">
													* </span>
													</label>
													
													<div class="col-md-10">
														<input  path="GeneralCode" type="text" class="form-controlwidth"    maxlength="100" name="TextBoxGeneralCode"  id="TextBoxGeneralCode"  value="${GeneralCodesObject.getId().getGeneralCode()}"/>																						
														<span id="ErrorMessageTextBoxGeneralCode" class="error"></span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="SystemUse">${ScreenLabel.getGeneralCodes_SystemUse()}:</form:label><span class="required">
													* </span>
													</label>
													
													<div class="col-md-10">
														<form:select path="SystemUse"  style="display:inline"  class="table-group-action-input form-control input-medium" id="SystemUse" name="SystemUse">
															<form:option path="SystemUse" value="">${ScreenLabel.getGeneralCodes_Select()}..</form:option>
															<form:option path="SystemUse" value="Y">${ScreenLabel.getGeneralCodes_YES()}</form:option>
															<form:option path="SystemUse" value="N">${ScreenLabel.getGeneralCodes_NO()}</form:option>
														</form:select>
														<span id="ErrorMessageSystemUse" class="error"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Description20">${ScreenLabel.getGeneralCodes_Description20()}:</form:label> <span class="required">
													* </span>
													</label>
													
													<div class="col-md-10">
														<form:textarea id="Description20"   name="Description20" path="Description20"  style="resize:none" maxlength="20" class="form-controlwidth" ></form:textarea>
														<span id="ErrorMessageDescription20" class="error error-TextArea"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Description50">${ScreenLabel.getGeneralCodes_Description50()}:</form:label> <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<form:textarea  path="Description50" id="Description50"   name="Description50"  style="resize:none" class="form-controlwidth"   maxlength="50"></form:textarea>
														<span id="ErrorMessageDescription50" class="error error-TextArea"></span>
													</div>
												</div>
													<div class="form-group" id="MenuoptionHide" style="display:none;">
													<label class="col-md-2 control-label"><form:label path="MenuOptionName">${ScreenLabel.getGeneralCodes_MenuOptionName()}:</form:label>  <span class="required">
													* </span>
													</label>
													<div class="col-md-10">														
														<form:input path="MenuOptionName" type="text" id="MenuOptionName"   name="MenuOptionName"  class="form-controlwidth"   maxlength="100"  />														
														<span id="ErrorMessageMenuOptionName" class="error"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Helpline">${ScreenLabel.getGeneralCodes_Helpline()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:textarea path="Helpline" type="text"   class="form-controlwidth" maxlength="500"  />
													</div>
												</div>
												<%-- <div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Helpline2">${ScreenLabel.getGeneralCodes_Helpline2()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Helpline2" type="text"   class="form-controlwidth"  maxlength="100" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Helpline3">${ScreenLabel.getGeneralCodes_Helpline3()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Helpline3" type="text"   class="form-controlwidth"  maxlength="100" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Helpline4">${ScreenLabel.getGeneralCodes_Helpline4()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Helpline4" type="text"   class="form-controlwidth" maxlength="100" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Helpline5">${ScreenLabel.getGeneralCodes_Helpline5()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Helpline5" type="text"   class="form-controlwidth"  maxlength="100" />
													</div>
												</div> --%>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control01Description">${ScreenLabel.getGeneralCodes_Control01Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control01Description" type="text"   class="form-controlwidth" maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control01Value">${ScreenLabel.getGeneralCodes_Control01Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control01Value" type="text"   class="form-controlwidth"  maxlength="20" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control02Description">${ScreenLabel.getGeneralCodes_Control02Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control02Description" type="text"   class="form-controlwidth"  maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control02Value">${ScreenLabel.getGeneralCodes_Control02Value()}:</form:label><span class="required">
													 </span> 
													</label>
													<div class="col-md-10">
														<form:input path="Control02Value" type="text"   class="form-controlwidth" maxlength="20" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control03Description">${ScreenLabel.getGeneralCodes_Control03Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control03Description" type="text"   class="form-controlwidth"  maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control03Value">${ScreenLabel.getGeneralCodes_Control03Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control03Value" class="form-controlwidth"   maxlength="20" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control04Description">${ScreenLabel.getGeneralCodes_Control04Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control04Description" type="text"   class="form-controlwidth"  maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control04Value">${ScreenLabel.getGeneralCodes_Control04Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control04Value" type="text"   class="form-controlwidth"  maxlength="20" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control05Description">${ScreenLabel.getGeneralCodes_Control05Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control05Description" type="text"   class="form-controlwidth"  maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control05Value">${ScreenLabel.getGeneralCodes_Control05Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control05Value" type="text"   class="form-controlwidth"  maxlength="20" />
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control06Description">${ScreenLabel.getGeneralCodes_Control06Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control06Description" type="text"   class="form-controlwidth" maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control06Value">${ScreenLabel.getGeneralCodes_Control06Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control06Value" type="text"   class="form-controlwidth"  maxlength="20" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control07Description">${ScreenLabel.getGeneralCodes_Control07Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control07Description" type="text"   class="form-controlwidth"  maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control07Value">${ScreenLabel.getGeneralCodes_Control07Value()}:</form:label><span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control07Value" type="text"   class="form-controlwidth"  maxlength="20" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control08Description">${ScreenLabel.getGeneralCodes_Control08Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control08Description" type="text"   class="form-controlwidth"   maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control08Value">${ScreenLabel.getGeneralCodes_Control08Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control08Value" class="form-controlwidth"   maxlength="20"  />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control09Description">${ScreenLabel.getGeneralCodes_Control09Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control09Description" type="text"   class="form-controlwidth"   maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control09Value">${ScreenLabel.getGeneralCodes_Control09Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control09Value" type="text"   class="form-controlwidth"   maxlength="20" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control10Description">${ScreenLabel.getGeneralCodes_Control10Description()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control10Description" type="text"   class="form-controlwidth"   maxlength="50" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"><form:label path="Control10Value">${ScreenLabel.getGeneralCodes_Control10Value()}:</form:label> <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<form:input path="Control10Value" type="text"  class="form-controlwidth"   maxlength="20" />
																								

														
													
													</div>
												</div>
												<div class="margin-bottom-5-right-allign">
												<button type="submit" class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>&nbsp;${ScreenLabel.getGeneralCodes_GeneralCode_SaveUpdate()} </button>
												<button type="button" onclick="return resetPage();" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> ${ScreenLabel.getGeneralCodes_Reset()}</button>
												<button type="button" onclick="return reloadPage();" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> ${ScreenLabel.getGeneralCodes_Cancels()}</button></div>																						
											</div>
										</div>
										
										<script>
										function resetPage(){
									           
									          /*  location.reload(); */
											window.parent.location = window.parent.location.href;
									          }
										function reloadPage(){             
						                       //window.location.href = 'GeneralCodeList';
						                      //window.location.href='GeneralCodeEditDelete';
						                      var StrTenant='${StrTenant}';
						                      var StrCompany='${StrCompany}';
						                      var StrAppliation='${StrApplication}';
						                      var StrGeneralCode='${StrGeneralCode}';
						                     
						                       if(StrGeneralCode == ""){
						                  
						                       window.location.href='GeneralCodeList';
						                      
						                      }else{ 
						                                         
						                       var url = '${pageContext.request.contextPath}/General_code_identificationdata?MenuValue='+StrGeneralCode;
						                       //var url = StrTenant+"/"+StrCompany+"/"+StrAppliation+"/"+StrGeneralCode;
						                       //window.location.href='${pageContext.request.contextPath}/GeneralCodeEditDelete/'+url;
						                       window.location.href=url;
						                       //window.history.back();
						                       } 
						                       
						                     }
												</script>
										
										
									</div>
								</div>
							</div>
						</div>
						
						</div>
						</div>
						</form:form>
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>

<script>

</script>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>

function body_load(){

	var isSubmit = false;
	var testNumber = false;

	var isBlankSelectApplication = isBlankField('SelectApplication',
			'ErrorMessageSelectApplication',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabel.getGeneralCodes_ErrorMsg_MandatoryFields()}");
	
	var isBlankSelectGeneralCode = isBlankField('SelectGeneralCode', 'ErrorMessageSelectGeneralCode',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabel.getGeneralCodes_ErrorMsg_MandatoryFields()}");
	
	var isBlankTextBoxGeneralCode = isBlankField('TextBoxGeneralCode',
			'ErrorMessageTextBoxGeneralCode',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabel.getGeneralCodes_ErrorMsg_MandatoryFields()}");
	
	var isBlankSystemUse = isBlankField('SystemUse',
			'ErrorMessageSystemUse',
	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabel.getGeneralCodes_ErrorMsg_MandatoryFields()}");
	
	var isBlankDescription20 = isBlankField('Description20', 'ErrorMessageDescription20',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabel.getGeneralCodes_ErrorMsg_MandatoryFields()}");
	
	var isBlankDescription50 = isBlankField('Description50',
			'ErrorMessageDescription50',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabel.getGeneralCodes_ErrorMsg_MandatoryFields()}");
	
	
	 var isMenuValueCheck=true;
	var menuvaluecheck=$('#menuvaluecheck').val();
	if((! ($('#menuvaluecheck').val()))){
	
		var isBlankMenuOptionName=isBlankField('MenuOptionName',
				'ErrorMessageMenuOptionName',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+"${ScreenLabel.getGeneralCodes_ErrorMsg_MandatoryFields()}");
		if(isBlankMenuOptionName){
			isMenuValueCheck=true;
		}
		else{
			isMenuValueCheck=false;
		}
		
		
	}
	else{
		
		document.getElementById('MenuoptionHide').style.display='none';
		isMenuValueCheck=true;
		isBlankMenuOptionName=true;
	} 
	
	
		
	/* var isBlankEmergencycontacthomephone = isBlankField(
			'Emergencyhomephone', 'ErrorMessageEmergencyhomephone',
			'Mandatory field cannot be left blank.'); */

	

	if (isBlankSelectApplication && isBlankSelectGeneralCode && isBlankTextBoxGeneralCode
			&& isBlankSystemUse && isBlankDescription20 && isBlankDescription50 && isBlankMenuOptionName
			) {
				ValueSelectApplication= $('#SelectApplication').val();
				ValueSelectGeneralCodeID= $('#SelectGeneralCode').val();
				ValueSelectGeneralCode= $('#TextBoxGeneralCode').val();
				ValueDescription20= $('#Description20').val();
				ValueDescription50= $('#Description50').val();
				ValueMenuName= $('#MenuOptionName').val();
				var PageName="Add";
				/*alert("ValueSelectApplication "+ValueSelectApplication);
				alert("ValueSelectGeneralCodeID "+ValueSelectGeneralCodeID);
				alert("ValueDescription20 "+ValueDescription20);
				alert("ValueDescription50 "+ValueDescription50);*/
				
				
				
				
				
		
		
					//alert("${ScreenLabel.getGeneralCodes_Confirm_Save()}");
					$.post("${pageContext.request.contextPath}/RestCheckUniqueGeneralCode",
								{
								Application_ID:ValueSelectApplication,
								General_Code_ID : ValueSelectGeneralCodeID,
								General_Code:ValueSelectGeneralCode
								
								},
								function(data, status) {
									if (data) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+"${ScreenLabel.getGeneralCodes_ErrorMsg_GeneralCode()}";
										$('#ConstraintMessage').show();
									//document.location.href = '#top';
									window.scrollTo(0,0);
									isSubmit = false;
										
									} else {			
										 $.post("${pageContext.request.contextPath}/RestCheckUniqueMenuName",
								{
									Menu_Option_Name:ValueMenuName
								},
								function(data, status) {
									if (data) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ "${ScreenLabel.getGeneralCodes_Menu_Option_already_used()}";
										$('#ConstraintMessage').show();
										//document.location.href = '#top';		
										window.scrollTo(0,0);
										isSubmit = false;
									} else {			
									$.post("${pageContext.request.contextPath}/RestCheckUniqueDescriptionShort",
							{
								Application_ID:ValueSelectApplication,
								General_Code_ID : ValueSelectGeneralCodeID,
								General_Code:ValueSelectGeneralCode,
								Description20 : ValueDescription20,
								ScreenName : PageName
							},
							function(data, status) {
								if (data) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ "${ScreenLabel.getGeneralCodes_Description_Short_is_already_Used()}";
									$('#ConstraintMessage').show();
									//document.location.href = '#top';
									window.scrollTo(0,0);
									isSubmit = false;
									// alert ("User Already Exists");
								} else {			
									
									$.post("${pageContext.request.contextPath}/RestCheckUniqueDescriptionLong",
								{
									Application_ID:ValueSelectApplication,
									General_Code_ID : ValueSelectGeneralCodeID,
									General_Code:ValueSelectGeneralCode,	
									Description50 : ValueDescription50,
									ScreenName : PageName
								},
								function(data, status) {
									if (data) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ "${ScreenLabel.getGeneralCodes_Description_Long_is_already_Used()}";
										$('#ConstraintMessage').show();
										//document.location.href = '#top';
										window.scrollTo(0,0);
										isSubmit = false;
										// alert ("User Already Exists");
									} else {			
										$('#ConstraintMessage').hide();
										 bootbox.alert('${ScreenLabel.getGeneralCodes_Confirm_Save()}',function(){
											 //alert("${ScreenLabel.getGeneralCodes_Confirm_Save()}");
												isSubmit = true;
												myform.action="GeneralCodeEditDelete";
												myform.submit(); 
										 });
																														
									}
								});
																						
								}
							});																		
									}
								});  																
									}
								});
					  

					
					
		
	} else {
		 document.getElementById('ConstraintMessage').style.display='none';
		isSubmit = false;
	}
	//document.location.href = '#top';
	window.scrollTo(0,0);
	return isSubmit;
	
}
function headerChangeLanguage(){
	
	/* alert('in security look up'); */
	
 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	
	
}

function headerInfoHelp(){
	
	//alert('in security look up');
	
 	/* $
	.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
	 */
 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
			  {InfoHelp:'General Code',
 		InfoHelpType:'PROGRAM'
		 
			  }, function( data1,status ) {
				  if (data1.boolStatus) {
					  window.open(data1.strMessage); 
					  					  
					}
				  else
					  {
					//  alert('No help found yet');
					  }
				  
				  
			  });
	
}

$( document ).ready(function() {
		
	 $('#getLastActivitydate').val(getUTCDateTime());
	 
	 if(! ($('#menuvaluecheck').val())){			
				  $("#MenuoptionHide").show();				  				
	}
	
	  var value='${ConstraintViolation}';
	  if(value==""){
		  document.getElementById('ConstraintMessage').style.display='none';
		 /*  $("#UniqueMessage").hide(); */
	  }
	  else{
		  document.getElementById('ConstraintMessage').style.display='block'; 
		  document.getElementById('TextBoxGeneralCode').value = "";
		  document.getElementById('MenuOptionName').value = ""; 
		 /*  $("#ConstraintMessage").show();  */
	 }
	  

	});
	
setInterval(function () {
	
    var h = window.innerHeight;
    if(window.innerHeight>=900 || window.innerHeight==1004 ){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>