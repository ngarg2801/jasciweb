<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<style>
fa-remove:before, .fa-close:before, .fa-times:before,.fa-check:before,.fa-search:before {
	margin-right: 5px !important;
}
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->

		<!-- BEGIN PAGE CONTENT -->
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${ViewLabels.getLanguageTranslation_LTLookup()}</h1>
					</div>
				</div>

				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active"></li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
						
						
						
							<div id="ErrorMessage" class="note note-danger" style="display:none ; margin-left:15px;">
     <p id="Perror" class="error error-Top" style="margin-left: -7px !important;"></p>	
    </div>
								<div class="col-md-12">
					<form:form class="form-horizontal form-row-seperated" name="myForm" action="#" method ="get" onsubmit="return isformSubmit();">
						<div class="portlet">
								<input type="hidden" name="backStatus" id="backStatus" value="lookup">
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
											<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getLanguageTranslation_Language()}:<span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<select class="table-group-action-input form-control input-medium" name="Language" id="Language">
														<option value="">${ViewLabels.getLanguageTranslation_Select()}...</option>
											
														 <c:forEach items="${LanguageList}" var="LanguageItem">
														        <%-- <c:choose>
    																	<c:when test="${LanguageItem.getLanguage().equalsIgnoreCase(DefaultLanguageValue)}">
     															     <option value="${LanguageItem.getLanguage()}" selected="selected">${LanguageItem.getDescription20()}</option>
       															
       																	</c:when>
   
    																	<c:otherwise> --%>
                                                                       <option value="${LanguageItem.getGeneralCode()}">${LanguageItem.getDescription20()}</option>
        																	
    																<%-- </c:otherwise>
																			</c:choose> --%>

																	</c:forEach>
														
														
														
														          <%--  <c:forEach items="${LanguageList}" var="LanguageItem">
                                                                       <option value="${LanguageItem.getLanguage()}">${LanguageItem.getDescription20()}</option>
																	</c:forEach> --%>
														</select>

<!-- 													<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer; ;left: 27.9%; top: 10px;" onclick="actionForm('Language');"></i>
 -->													</div>
												</div>
												
								               <div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getLanguageTranslation_EnglishKeyPhrase()}:<span class="required">
													* </span>
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" id="KeyPhrase" name="KeyPhrase">
														<i class="fa fa-search" id="BtnIdKeyPhrase" style="color:rgba(68, 77, 88, 1); cursor:pointer; " onclick="actionForm('keyphrase');"></i>
													</div>
												</div>
																		<div class="margin-bottom-5-right-allign_info_language_search">
											<button class="btn btn-sm yellow  margin-bottom" onclick="actionForm('new');"><i class="fa fa-plus"></i> ${ViewLabels.getLanguageTranslation_New()}</button>
										    <button class="btn btn-sm yellow  margin-bottom" id="BtnIdDisplayAll" onclick="actionForm('DisplayAll');"><i class="fa fa-check"></i>${ViewLabels.getLanguageTranslation_DisplayAll()}</button>
											</div>

								</div>
										</div>
							</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>

	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(action) {

		
		 if (action == 'keyphrase') {

			var KeyPhrase = isBlankField(
					'KeyPhrase',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLanguageTranslation_BlankKeyphraseMessage()}');

			if (KeyPhrase) {

				
				var KeyPhraseVal = $('#KeyPhrase').val().trim();
			    document.getElementById('KeyPhrase').value=KeyPhraseVal;
				//var languageText=$('#Language').text();
				$.post(
								"${pageContext.request.contextPath}/RestCheckValidKeyPhrase",
								{KeyPhrase:KeyPhraseVal },
								function(data, status) {

									if (!data) {
										var errMsgId = document.getElementById("Perror");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${ViewLabels.getLanguageTranslation_InValidKeyPhraseMessage()}';
										document.getElementById('Language').value="";
										 document.getElementById('Language').text="Select";
										$('#ErrorMessage').show();
										isSubmit = false;
									}else {
										
										//alert("Valid language");
										document.getElementById('Language').value="";
										 document.getElementById('Language').text="Select";

										$('#ErrorMessage').hide();
										
										 isSubmit=true;
									     document.myForm.action = "${pageContext.request.contextPath}/Language_Translations_Search_Lookup_action";
										 document.myForm.submit(); 
									}
								});//end check i 

			} else {
				document.getElementById('Language').value="";
				 document.getElementById('Language').text="Select";

				$('#ErrorMessage').show();
				 /* var elem1 = document.getElementById("Language"); // Get text field
					elem1.value = ""; 
					elem1.text="Select"; */
				isSubmit = false;
			}

		}
	
	
      else if(action=='new')
			{
			var url="${pageContext.request.contextPath}/Language_Translations_Maintenance";
			document.myForm.action = url;
			document.myForm.submit();
		
			}
	

		 else if (action == 'DisplayAll') 
		 {
			  
			 var languagetext = isBlankField(
						'Language',
						'Perror',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ViewLabels.getLanguageTranslation_BlankLanguageMessage()}');
			 
			 if(languagetext){
			 document.getElementById('KeyPhrase').value="";
			    var languageVal = $('#Language').val();
				var languageText=$('#Language').text();
				$.post(
								"${pageContext.request.contextPath}/RestCheckValidLanguage",
								{Language:languageVal },
								function(data, status) {

									if (!data) {
										var errMsgId = document.getElementById("Perror");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${ViewLabels.getLanguageTranslation_InValidLanguageMessage()}';
										$('#ErrorMessage').show();
										isSubmit = false;
									}else {
																				
										$('#ErrorMessage').hide();
										isSubmit=true;
									     document.myForm.action = "${pageContext.request.contextPath}/Language_Translations_Search_Lookup_action";
										 document.myForm.submit(); 
									}
								});//end post infoPartoftheDescription
			 }else{
				 $('#ErrorMessage').show();
				 /* var elem1 = document.getElementById("Language"); // Get text field
					elem1.value = ""; 
					elem1.text="Select"; */
					 document.getElementById('KeyPhrase').value="";

				isSubmit = false;
			 }		

			} 
			
		 }
		 
		function getNew()
		{

			
			
			isSubmit=true;
					$('ErrorMessage').hide();
			var url="${pageContext.request.contextPath}/Language_Translations_Maintenance";
			document.myForm.action = url;
			document.myForm.submit();
		
			

		} 

	
</script>
<script>

function checkBlank(fieldID){
 var fieldLength = document.getElementById(fieldID).value.trim().length;
 if (fieldLength>0) {
  
  return true;

 }
 else {
  
  
  return false;
 }
}



	jQuery(document).ready(function() {
		
	setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
	
	$(window).keydown(function(event){
              if(event.keyCode == 13) {
               		   
            
               var BoolKeyPhrase = checkBlank('KeyPhrase');
              
            
			   
          if(BoolKeyPhrase){
			
				$('#BtnIdKeyPhrase').click();
          }
		   
		   /* else{
		  $('#BtnIdDisplayAll').click();
		  } */
			event.preventDefault();
            return false;
              }
          });
	
		/* Metronic.init(); // init metronic core componets
		Layout.init(); // init layout
		Demo.init(); // init demo(theme settings page)
		Index.init(); // init index page
		Tasks.initDashboardWidget(); // init tash dashboard widget */
	});
	
	 function headerChangeLanguage(){
	       
	       /* alert('in security look up'); */
	       
	      $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	       
	      }

	      function headerInfoHelp(){
	       
	       //alert('in security look up');
	       
	        /* $
	       .post(
	         "${pageContext.request.contextPath}/HeaderInfoHelp",
	         function(
	           data1) {

	          if (data1.boolStatus) {

	           location.reload();
	          }
	         });
	        */
	         $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	           {InfoHelp:'LanguageTranslation',
	         InfoHelpType:'PROGRAM'
	         
	           }, function( data1,status ) {
	            if (data1.boolStatus) {
	             window.open(data1.strMessage); 
	                    
	           }
	            else
	             {
	           //  alert('No help found yet');
	             }
	            
	            
	           });
	       
	      }
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>