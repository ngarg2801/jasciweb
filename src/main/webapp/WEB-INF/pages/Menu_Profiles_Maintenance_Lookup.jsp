<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<link href="<c:url value="/resources/assets/global/css/MenuProfileMaintenance.css"/>" rel="stylesheet" type="text/css">
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">




<body>

<!-- BEGIN PAGE CONTAINER -->
<div class="page-container" >
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${objMenuOptionLabel.getLblMenuProfilesMaintenanceLookup()}</h1>
			</div>
			
		
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div id="ErrorMessage1" class="note note-danger"
						style="display: none">
						<span class="error error-Top" id="error1"
							style="margin-left: -7px !important;"></span>
					</div>
			<div class="row margin-top-10">
				
							<div class="col-md-12">
					<form:form class="form-horizontal form-row-seperated" name="myForm" action="#"
					onsubmit="return isformSubmit();" method="get">
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
												<div class="form-group">
													<label class="col-md-2 control-label"> ${objMenuOptionLabel.getLblMenuProfile()}: 
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="lblMenuProfile" id="lblMenuProfile" >
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;"
														id="BtnMenuProfileID"
														onclick="actionForm('MenuProfileMaintenance','MenuProfilesearch');"></i>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblPartoftheMenuProfileDescription()}: 
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="lblPartOfMneuProfileDescription"
														 id="lblPartOfMneuProfileDescription">
													<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" 
													id="BtnMenuProfileDescpID"
													onclick="actionForm('MenuProfileMaintenanceSearchLookUp','PartMenuProfileDescriptionSearch');"></i></a>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenuType()}: 
													</label>
													<div class="col-md-10">
															<select class="table-group-action-input form-control input-medium" 
															name="lblMenuType" id="lblMenuType">
															<option value="">${objMenuOptionLabel.getLblSelect()}...</option>
															<c:if test="${not empty objGeneralCodeMenuType}">
															<c:forEach var="MenuTypeList" items="${objGeneralCodeMenuType}">
											
																	<option value="${MenuTypeList.getGeneralCode()}">${MenuTypeList.getDescription20()}</option>
																	</c:forEach>
															</c:if>
															
															
																														
														</select>
														<i class="fa fa-search" id="searchiconallselect"
														 style="color:rgba(68, 77, 88, 1); cursor:pointer;"
														onclick="actionForm('MenuProfileMaintenanceSearchLookUp','MenuTypeSearch');"></i>
													</div>
													
												</div>
												
												
												
												<div class="margin-bottom-5-right-allign_Menu_Profile_LookUp">
											<button class="btn btn-sm yellow filter-submit margin-bottom"
											onclick="actionForm('MenuProfileMaintenance','NewSearch');">
											<i class="fa fa-plus"></i>&nbsp;${objMenuOptionLabel.getLblNew()}
											</button>
										    
											<button class="btn btn-sm yellow filter-submit margin-bottom" id="BtnDisplayAllID"
											onclick="actionForm('MenuProfileMaintenanceSearchLookUp','DisplayAllSearch');">
											<i class="fa fa-check"></i> ${objMenuOptionLabel.getLbldisplayall()}
											</button></div>
												
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>
				
				<!--end tabbable-->
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>

</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->


<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>

<script type="text/javascript">


function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  }

function actionForm(url, action) {
	$('#ErrorMessage').hide();
	if (action == 'MenuProfilesearch') {

		var valid = isBlankField('lblMenuProfile', 'error1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MENU_PROFILE_LEFT_BLANK()}');

		if (valid) {

			var PartMenuProfileDescription = document
					.getElementById("lblPartOfMneuProfileDescription"); // Get text field
					PartMenuProfileDescription.value = "";

			
			var MenuType = document.getElementById("lblMenuType");
			MenuType.value = "";
			

			var valMenuProfile = $('#lblMenuProfile').val();
			$('#ErrorMessage1').hide();
			var FieldValue = valMenuProfile;
			var Field = 'lblMenuProfile';

			$.post("${pageContext.request.contextPath}/RestFull_Check_MenuProfile",
							{
								MenuOptionValue : FieldValue
								
							},
							function(data, status) {

								if (!data) {

									error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${objMenuOptionLabel.getLblERR_INVALID_MENU_PROFILE()}';
									$('#ErrorMessage1').show();
									isSubmit = false;
								} else {
									$('#ErrorMessage1').hide();
									isSubmit = true;
									document.myForm.action = '${pageContext.request.contextPath}/'
											+ url;
									document.myForm.submit();
									//return isSubmit;
								}
							});//end check team member

		} else {
			$('#ErrorMessage1').show();
			document.myForm;
			var PartMenuProfileDescription = document
			.getElementById("lblPartOfMneuProfileDescription"); // Get text field
			PartMenuProfileDescription.value = "";

	
	var MenuType = document.getElementById("lblMenuType");
	MenuType.value = "";
			isSubmit = false;

		}

	}

	else if (action == 'PartMenuProfileDescriptionSearch') {

		var validPartAppl = isBlankField('lblPartOfMneuProfileDescription', 'error1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK()}');

		if (validPartAppl) {
			var PartMenuProfileDescription = document
			.getElementById("lblMenuProfile"); // Get text field
			PartMenuProfileDescription.value = "";

	
	var MenuType = document.getElementById("lblMenuType");
	MenuType.value = "";
			$('#ErrorMessage1').hide();

			var partofMenuProfileDesc = $('#lblPartOfMneuProfileDescription').val();

			var FieldValue = partofMenuProfileDesc;
			var Field = 'lblPartOfMneuProfileDescription';

			$
					.post(
							"${pageContext.request.contextPath}/MenuProfile_check_lookup",
							{
								MenuProfileSearchFieldValue : FieldValue,
								MenuOptionFieldSearch : Field
							},
							function(data, status) {

								if (!data) {

									error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${objMenuOptionLabel.getLblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION()}';
									$('#ErrorMessage1').show();
									isSubmit = false;
								} else {
									$('#ErrorMessage1').hide();
									isSubmit = true;
									document.myForm.action = '${pageContext.request.contextPath}/'
											+ url;
									document.myForm.submit();
									//return isSubmit;
								}
							});//end check team member				

		} else {
			$('#ErrorMessage1').show();
			document.myForm;
			var PartMenuProfileDescription = document
			.getElementById("lblMenuProfile"); // Get text field
			PartMenuProfileDescription.value = "";

	
	var MenuType = document.getElementById("lblMenuType");
	MenuType.value = "";
			isSubmit = false;

		}

	} else if (action == 'MenuTypeSearch') {

		var validPartAppl = isBlankField('lblMenuType', 'error1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuOptionLabel.getLblERR_MENU_TYPE_LEFT_BLANK()}');

		if (validPartAppl) {
			var PartMenuProfileDescription = document
			.getElementById("lblPartOfMneuProfileDescription"); // Get text field
			PartMenuProfileDescription.value = "";

	
	var MenuType = document.getElementById("lblMenuProfile");
	MenuType.value = "";
			$('#ErrorMessage1').hide();

			var partoftammbername = $('#lblMenuType').val();

			var FieldValue = partoftammbername;
			var Field = 'lblMenuType';

			$
					.post(
							"${pageContext.request.contextPath}/MenuProfile_check_lookup",
							{
								MenuProfileSearchFieldValue : FieldValue,
								MenuOptionFieldSearch : Field
							},
							function(data, status) {

								if (!data) {

									error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${objMenuOptionLabel.getLblERR_INVALID_MENU_TYPE_LEFT_BLANK()}';
									$('#ErrorMessage1').show();
									isSubmit = false;
								} else {
									$('#ErrorMessage1').hide();
									isSubmit = true;
									document.myForm.action = '${pageContext.request.contextPath}/'
											+ url;
									document.myForm.submit();
									//return isSubmit;
								}
							});//end check team member				

		} else {
			$('#ErrorMessage1').show();
			document.myForm;
			var PartMenuProfileDescription = document
			.getElementById("lblPartOfMneuProfileDescription"); // Get text field
			PartMenuProfileDescription.value = "";

	
	var MenuType = document.getElementById("lblMenuProfile");
	MenuType.value = "";
			isSubmit = false;

		}

	} else if(action == 'NewSearch'){
		
		var PartMenuProfileDescription = document
		.getElementById("lblPartOfMneuProfileDescription"); // Get text field
		PartMenuProfileDescription.value = "";
		var PartMenuProfile = document
		.getElementById("lblMenuProfile"); // Get text field
		PartMenuProfile.value = "";


var MenuType = document.getElementById("lblMenuType");
MenuType.value = "";
		
		document.myForm.action = '${pageContext.request.contextPath}/'
			+ url;
		isSubmit = true;
	}

	else if (action == 'DisplayAllSearch') {
		
		var PartMenuProfileDescription = document
		.getElementById("lblPartOfMneuProfileDescription"); // Get text field
		PartMenuProfileDescription.value = "";
		var PartMenuProfile = document
		.getElementById("lblMenuProfile"); // Get text field
		PartMenuProfileDescription.value = "";


var MenuType = document.getElementById("lblMenuType");
MenuType.value = "";


		document.myForm.action = '${pageContext.request.contextPath}/'
				+ url;
		document.myForm.submit();
		$('#ErrorMessage').hide();
		isSubmit = true;
	}

}

function checkBlank(fieldID){
	var fieldLength = document.getElementById(fieldID).value.trim().length;
	if (fieldLength>0) {
		
		return true;

	}
	else {
		
		
		return false;
	}
} 


jQuery(document).ready(function() { 
	
	
	
	$(window).keydown(function(event){
	    if(event.keyCode == 13) {
	     
	    	var BoolMenuTypeID = checkBlank('lblMenuType');
	    	var BoolMenuProfile = checkBlank('lblMenuProfile');
	    	var BoolMenuProfileDescp = checkBlank('lblPartOfMneuProfileDescription');
	    	
	    	if(BoolMenuProfile){
			$('#BtnMenuProfileID').click();
			}else if(BoolMenuProfileDescp){
				$('#BtnMenuProfileDescpID').click();
			}else if(BoolMenuTypeID){
			$('#searchiconallselect').click();
			}/* else{
	    		
	    		$('#BtnDisplayAllID').click();
	    	} */
			
			
	    	
	    	 event.preventDefault();
	         return false;
	    }
	});
	
	
	setInterval(function () {
		
	    var h = window.innerHeight;		    
	    if(window.innerHeight>=900){
	    	h=h-187;
	    	
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";		     
	    
		}, 30);
  // Metronic.init(); // init metronic core componets
   
  // Demo.init(); // init demo(theme settings page)
  // Index.init(); // init index page
  // Tasks.initDashboardWidget(); // init tash dashboard widget
  
});

function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'MenuProfileMaintenance',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }	
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>