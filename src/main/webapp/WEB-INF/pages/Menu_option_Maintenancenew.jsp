<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014É World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico">

<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${objMenuOptionLabel.getLblMenuOptionMantenance()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			<div class="page-toolbar">
				<!-- BEGIN THEME PANEL -->
				<div class="btn-group btn-theme-panel">
					<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
					<!--<i class="icon-settings"></i>-->
					</a>
					<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<h3>THEME COLORS</h3>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li class="theme-color theme-color-default" data-theme="default">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Default</span>
											</li>
											<li class="theme-color theme-color-blue-hoki" data-theme="blue-hoki">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Hoki</span>
											</li>
											<li class="theme-color theme-color-blue-steel" data-theme="blue-steel">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Steel</span>
											</li>
											<li class="theme-color theme-color-yellow-orange" data-theme="yellow-orange">

												<span class="theme-color-view"></span>
												<span class="theme-color-name">Orange</span>
											</li>
											<li class="theme-color theme-color-yellow-crusta" data-theme="yellow-crusta">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Yellow Crusta</span>
											</li>
										</ul>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li class="theme-color theme-color-green-haze" data-theme="green-haze">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Green Haze</span>
											</li>
											<li class="theme-color theme-color-red-sunglo" data-theme="red-sunglo">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Sunglo</span>
											</li>
											<li class="theme-color theme-color-red-intense" data-theme="red-intense">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Intense</span>
											</li>
											<li class="theme-color theme-color-purple-plum" data-theme="purple-plum">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Plum</span>
											</li>
											<li class="theme-color theme-color-purple-studio" data-theme="purple-studio">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Studio</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 seperator">
								<h3>LAYOUT</h3>
								<ul class="theme-settings">
									<li>
										 Layout
										<select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips" data-original-title="Change layout type" data-container="body" data-placement="left">
											<option value="boxed" selected="selected">Boxed</option>
											<option value="fluid">Fluid</option>
										</select>
									</li>
									<li>
										 Top Menu Style
										<select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change top menu dropdowns style" data-container="body" data-placement="left">
											<option value="dark" selected="selected">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Top Menu Mode
										<select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) top menu" data-container="body" data-placement="left">
											<option value="fixed">Fixed</option>
											<option value="not-fixed" selected="selected">Not Fixed</option>
										</select>
									</li>
									<li>
										 Mega Menu Style
										<select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change mega menu dropdowns style" data-container="body" data-placement="left">
											<option value="dark" selected="selected">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Mega Menu Mode
										<select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) mega menu" data-container="body" data-placement="left">
											<option value="fixed" selected="selected">Fixed</option>
											<option value="not-fixed">Not Fixed</option>
										</select>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END THEME PANEL -->
			</div>
			<!-- END PAGE TOOLBAR -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					message maintenance
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
		
			<div class="row margin-top-10">
			<div id="ErrorMessage" class="note note-danger" style="display:none; margin-left:-14px;" >
     <p  class="error error-Top" id="Perror" style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p> 
    </div>
			<div id="ErrorMessage1" class="note note-danger"
												style="display: none">
												<p class="error error-Top" style="margin-left: -7px !important;"></p>
											</div>
				<div class="col-md-12">
					<form:form class="form-horizontal form-row-seperated" 
					action="#"
					name="myForm" onsubmit="return isformSubmit();"
					   modelAttribute="MenuOption_Update_Object" method="post" >
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
											
											<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblLastActivityDate()}
													
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="LastActivityDate"
														id="LastDateID"
														 placeholder=" " disabled="disabled">
														 <input type="hidden" class="form-controlwidth" name="LastActivityDateH"
														id="LastDate">
													</div>
												</div>
													<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblLastActivityBy()}
													
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="LastActivityTeamMember" 
														value="${TEAMMEMBER}"
														placeholder="" disabled="disabled">
													</div>
												</div>
														
												 <div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenuType()}:<span class="required">
													* </span> 
													</label>
													<div class="col-md-10">
														<select class="table-group-action-input form-control input-medium" 
														name="MenuType" id="dropMenuTypeID">
															<option value="">${objMenuOptionLabel.getLblSelect()}...</option>
															<c:if test="${not empty menuTypeOption}">
																<c:forEach var="MenuTypeList" items="${menuTypeOption}">
											
																	<option value="${MenuTypeList.getGeneralCode()}">${MenuTypeList.getDescription20()}</option>
																	</c:forEach>
																	</c:if>
														</select><span id="ErrorMessageMenuType" style="margin-left: 8px;" class="error">
																	</span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblMenuOption()}: <span class="required">
													* </span> 
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="MenuOption" 
														value="${MenuOption_Update_Object.getMenuOption()}"
														id="MenuOptionID" maxlength="100">
													<span id="ErrorMessageMenuOption" class="error">
																	</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLbldescriptionshrot()}: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="Description20"
														id="DescriptionShortID" 
														value="${MenuOption_Update_Object.getDescription20()}"
														maxlength="20">
													<span id="ErrorMessageDescriptionShort" class="error">
																	</span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLbldescriptionlong()}: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="Description50" 
														value="${MenuOption_Update_Object.getDescription50()}"
														id="LongDescriptionID" maxlength="50">
													<span id="ErrorMessageDescriptionLong" class="error">
																	</span>
													</div>
												</div><div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblApplication()}: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
													
														<select class="table-group-action-input form-control input-medium" 
														id="Application" name="Application">
														<c:if test="${not empty MapApplication}">
															<option value="">${objMenuOptionLabel.getLblSelect()}...</option>
															<c:forEach var="ObjList" items="${MapApplication}">
															<option value="${ObjList.getGeneralCode()}">${ObjList.getDescription20()}</option>
															
															</c:forEach>
											</c:if>
														</select>
														
														<span id="ErrorMessageApplication" class="error">
																	</span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblApplicationSub()}: <span class="required">
													* </span>
													</label>
												<div class="col-md-10">
														<select class="table-group-action-input form-control input-medium" 
														id="ApplicationSub" name="ApplicationSub">
															<option value="">${objMenuOptionLabel.getLblSelect()}...</option>
																<c:forEach var="ObjList" items="${MapSubApplication}">
															<option value="${ObjList.getGeneralCode()}">${ObjList.getDescription20()}</option>
															
															</c:forEach>
											
														</select>
														<span id="ErrorMessageSubApplication" class="error-select">
																	</span>
													</div>
												</div>
												<div class="repeatingSection" id="TextBoxesGroup">
												<div class="form-group" id="TextBoxDiv1">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblHelpLine()}: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
																	<textarea class="form-controlwidth" style="resize:none"
																		
																		id="Help1ID" name="Helpline" id="textbox1"
																		maxlength="500">${MenuOption_Update_Object.getHelpline()}</textarea> <span
																		id="ErrorMessageHelp1"  class="error error-TextArea"> </span>
																</div>
													
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuOptionLabel.getLblExecutionPath()}: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<input type="Text" class="form-controlwidth" 
														value="${MenuOption_Update_Object.getExecution()}"
														id="ExecutionID" name="Execution" >
													<span id="ErrorMessageMenuExecution" class="error">
																	</span>
													</div>
													
												</div>
												
												</div>
												
												<div class="margin-bottom-5-right-allign_maintenancenewedit_options">
												<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>&nbsp;${objMenuOptionLabel.getLblUpdate_Save()}</button>
												<button type="button" onclick="return resetPage();" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> ${objMenuOptionLabel.getLblReset()}</button>
												<button type="button" class="btn btn-sm red filter-cancel"
												onclick="cancelButton();"><i class="fa fa-times"></i>${objMenuOptionLabel.getLblCancel()}</button></div>
										 
												
												
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
						</div>
						</div>
					
					</form:form>
					
				</div>
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />



 

<!-- END JAVASCRIPTS -->
</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
	type="text/javascript"></script>

<script type="text/javascript">

function resetPage(){
    
    window.parent.location = window.parent.location.href;
   }

jQuery(document).ready(function() {

	 $('#LastDateID').val(getLocalDate());
	 $('#LastDate').val(getUTCDateTime());
	 
/* 	 $(window).keydown(function(event){
		    if(event.keyCode == 13) {
		    	 event.preventDefault();
		         return false;
		    }
	 }); */
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
	
	//Metronic.init(); // init metronic core components
	//Layout.init(); // init current layout
	//Demo.init();
});



function isformSubmit() {

	var isSubmit = false;
	var testNumber = false;

	var isBlankMenuType = isBlankField('dropMenuTypeID',
			'ErrorMessageMenuType', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	var isBlankMenuOption = isBlankField('MenuOptionID', 'ErrorMessageMenuOption',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	var isBlankShortDesciption = isBlankField('DescriptionShortID',
			'ErrorMessageDescriptionShort', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	var isBlanklongDesciption = isBlankField(
			'LongDescriptionID',
			'ErrorMessageDescriptionLong',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	var isBlankHelp1 = isBlankField('Help1ID',
			'ErrorMessageHelp1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	var isBlankApplicationSub = isBlankField('ApplicationSub',
			'ErrorMessageSubApplication', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	var isBlankApplication = isBlankField('Application',
			'ErrorMessageApplication', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	
	var isBlankApplication = isBlankField('ExecutionID',
			'ErrorMessageMenuExecution', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${objMenuOptionLabel.getLblERR_MANDATORY_FIELD_LEFT_BLANK()}');
	$('#ErrorMessage').hide();
	if(isBlankMenuType && isBlankMenuOption && isBlankApplication
			&& isBlankApplicationSub && isBlankHelp1 && isBlanklongDesciption && isBlankShortDesciption ){
		var menuOption=$('#MenuOptionID').val();
		var errMsgId=document.getElementById("Perror");
		
		$.ajax({
		     url: '${pageContext.request.contextPath}/RestFull_Check_MenuOption',
		     cache: false,
		     data:{ MenuOptionValue:menuOption},
		     error: function() {
		    	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${objMenuOptionLabel.getLblMenu_Execution_Path_Not_Valid()}';
	        	 $('#ErrorMessage').show();
				
		     },
		     
		     success: function(data) {
		    	 
		    	 if(data){
		    	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${objMenuOptionLabel.getLblERR_MENU_OPTION_ALREADY_USED()}';
	        	 $('#ErrorMessage').show();
	        	 isSubmit=false;
		    	 }else{
					 $('#ErrorMessage').hide();
					 var ExecutionPath=$('#ExecutionID').val();
					 if(Contains("#",ExecutionPath)){
						 
						 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${objMenuOptionLabel.getLblMenu_Execution_Path_Not_Valid()}';
			        	 $('#ErrorMessage').show(); 
					 }
					 else if(Contains(";",ExecutionPath)){
				         errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${objMenuOptionLabel.getLblMenu_Execution_Path_Not_Valid()}';
				               $('#ErrorMessage').show(); 
				        }else if(ExecutionPath.charAt(0)=="?"){
				         errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${objMenuOptionLabel.getLblMenu_Execution_Path_Not_Valid()}';
				               $('#ErrorMessage').show();
				         
				         
				        }
					 else{
					 $.ajax({
					     url: '${pageContext.request.contextPath}/'+ExecutionPath,
					     cache: false,
					     error: function() {
					    	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${objMenuOptionLabel.getLblMenu_Execution_Path_Not_Valid()}';
				        	 $('#ErrorMessage').show();
							
					     },
					     
					     success: function(data) {
					    	 
					    	 $('#ErrorMessage').hide();
					    	 bootbox.alert('${objMenuOptionLabel.getLblON_SAVE_SUCCESSFULLY()}',function(){
								 isSubmit=true;
								 
								 document.myForm.action = '${pageContext.request.contextPath}/Menu_Maintenance_Screen_Update';
								 document.myForm.submit();
								});
					     },
					     type: 'GET'
					  });

					 
					 }
		     }
		     },
		     type: 'POST'
		  });
		
		
	}else{
		
		isSubmit = false;

	}
	
	//document.location.href = '#top';
	window.scrollTo(0,0);
	return isSubmit;

}

function Contains(serchvalue,StringValue){
	 var str=false;
	for (i = 0; i < StringValue.length; i++) { 
	    if(serchvalue==StringValue.charAt(i)){

	str=true;
	}
	}
	return str;



	}
function cancelButton() {

	//window.history.back();
	var screen = '${Screen_lookup}';
				
	if (screen=='LookUpScreen') {

		window.location.href = 'MenuMaintenance';
		
	} else {

		window.location.href = 'Menu_Maintenance_searchScreenAfter';
		
	}
	//window.location.reload();

}

/* function dropDown(){
	
	
	var selectedValue=$('#Application option:selected').val();
	
	$.post("${pageContext.request.contextPath}/RestFull_AddValueDropDown",
			{
				ValApplication:selectedValue
				
			},
			function(data, status) {
				
				if(data){
					$('#ApplicationSub').empty();
					$('#ApplicationSub').each(function() {
						// Create option
						var option = $("<option />");
						option.attr("value", '').text('${objMenuOptionLabel.getKeyPharseSelect()}');
						$('#ApplicationSub').append(option);
						});
					//$('#ApplicationSub').append( new Option("Select...",""));
					for (var subApplication in data){
						
						$('#ApplicationSub').append( new Option(data[subApplication].id.GeneralCodeID,data[subApplication].description20) );
					}
			}
				//$('#ApplicationSub').append( new Option("sub","sub") );
				
			});
	

			
	
} */
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'MenuOption',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>
</body>
<!-- END BODY -->
</html>