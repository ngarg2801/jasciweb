<!--  
Date Developed  Sep 18 2014
Description It is used to show the list of Location Table and Edit record on behalf of Primary Composite Keys
Created By Aakash Bishnoi -->



<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>
<style>

 



</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
<div id="container" style="position: relative" class="loader_div">
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
			<h1>${ScreenLabels.getLocationMaintenace_Location_Search_Lookup()}</h1>
				
			</div>
		
		</div>

	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			
				<div>
		<a  id="AddBtn2" href="${pageContext.request.contextPath}/Location_Maintenance_New" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${ScreenLabels.getLocationMaintenace_Add_New()}</span>
						</a>	
		</div>					
									
			<div class="row margin-top-46">
			<div class="col-md-12">
				
					<div class="portlet">
						
							
	<div class="row" >							
	<kendo:grid name="LocationMaintenance"  resizable="true" reorderable="true" sortable="true" dataBound="gridDataBound">
				<kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5">
    	</kendo:grid-pageable>
				<kendo:grid-editable mode="inline" confirmation="" />
				
				<kendo:grid-columns>
				
					<kendo:grid-column  field="area_Description" title="${ScreenLabels.getLocationMaintenace_Area()}" width="12%" />
					<kendo:grid-column  field="fullfillment_Center_ID_Description" title="${ScreenLabels.getLocationMaintenace_FulFillment_Center()}" width="12%" />
					<kendo:grid-column field="location"	title="${ScreenLabels.getLocationMaintenace_Location()}" width="12%"/>
					<kendo:grid-column field="description50" title="${ScreenLabels.getLocationMaintenace_Description()}" width="12%" />
					<kendo:grid-column field="location_Type_Description" title="${ScreenLabels.getLocationMaintenace_Location_Type()}" width="12%" />
					<kendo:grid-column field="last_Activity_Date" title="${ScreenLabels.getLocationMaintenace_Last_ActivityDate()}" width="12%" />
					<kendo:grid-column field="last_Activity_Team_Member" title="${ScreenLabels.getLocationMaintenace_Last_Activity_By()}" width="12%" />
					<kendo:grid-column field="last_Activity_Task_Description" title="${ScreenLabels.getLocationMaintenace_Last_Activity_Task()}" width="11%" />
					<kendo:grid-column title="${ScreenLabels.getLocationMaintenace_Actions()}" width="17%">
						<kendo:grid-column-command>
						<kendo:grid-column-commandItem className="fa fa-copy btn btn-sm yellow filter-submit margin-bottom" name="CopyDetails" text="${ScreenLabels.getLocationMaintenace_Copy()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function CopyDetails(e) {
                               

                            	e.preventDefault();

                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                               
                                var Strfullfillment_Center_ID=dataItem.fullfillment_Center_ID;
                             	var Strarea=dataItem.area;
                           		var Strlocation=dataItem.location;
            
                                 var url= "${pageContext.request.contextPath}/Location_Maintenance_Copy";
                                 var myform = document.createElement("form");

                                
                                
                                var FulfillmentCenterField = document.createElement("input");
                                FulfillmentCenterField.type = 'hidden';
                                FulfillmentCenterField.value = Strfullfillment_Center_ID;
                                FulfillmentCenterField.name = "Fulfillment_Center";
                                
                                var AreaField = document.createElement("input");
                                AreaField.type = 'hidden';
                                AreaField.value = Strarea;
                                AreaField.name = "Area";
                                
                                var LocationField = document.createElement("input");
                                LocationField.type = 'hidden';
                                LocationField.value = Strlocation;
                                LocationField.name = "Location";
                                
                                
                                myform.action = url;
                                myform.method = "get"
                                myform.appendChild(FulfillmentCenterField);
                                myform.appendChild(AreaField);
                                myform.appendChild(LocationField);
                                document.body.appendChild(myform);
                                myform.submit();
                                
                                
                                
                                
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
                        <kendo:grid-column-commandItem className="fa fa-note btn btn-sm yellow margin-bottom"  name="ShowNotes" text="${ScreenLabels.getLocationMaintenace_Notes()}">
								<kendo:grid-column-commandItem-click>
							<script>
						        function ShowNotes(e)
						        {
						          
                                   e.preventDefault();
									
									
                                   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                  
                                 var StrLocation=dataItem.location;
                                     
                                    var url= "${pageContext.request.contextPath}/Notes_Lookup";
                                 
                                   var myform = document.createElement("form");

                                                                      
                                   var NotesLinkField = document.createElement("input");
                                   NotesLinkField.type = 'hidden';
                                   NotesLinkField.value = StrLocation;
                                   NotesLinkField.name = "NOTE_LINK";
                                   
                                   var NotesIdField = document.createElement("input");
                                   NotesIdField.type = 'hidden';
                                   NotesIdField.value = "LOCATION";
                                   NotesIdField.name = "NOTE_ID";
                                   
                                   
                                   myform.action = url;
                                  
                                   myform.appendChild(NotesLinkField);
                                   myform.appendChild(NotesIdField);
                                   document.body.appendChild(myform);
                                   myform.submit();
                                  
                
        }
        </script>
			                </kendo:grid-column-commandItem-click>
							</kendo:grid-column-commandItem>
							<%-- <kendo:grid-column-commandItem name="edit" text="${ScreenLabels.getMenuMessage_Edit()}" /> --%>
						<kendo:grid-column-commandItem className="icon-pencil btn btn-sm yellow filter-submit margin-bottom" name="editDetails" text="${ScreenLabels.getLocationMaintenace_Edit()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function showDetails(e) {
                               

                            	e.preventDefault();

                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                               
                                var Strfullfillment_Center_ID=dataItem.fullfillment_Center_ID;
                             	var Strarea=dataItem.area;
                           		var Strlocation=dataItem.location;
            
                                 var url= "${pageContext.request.contextPath}/Location_Maintenance_Update";
                                //var wnd = $("#details").data("kendoWindow");
                                //window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                                var myform = document.createElement("form");

                                
                                
                                var FulfillmentCenterField = document.createElement("input");
                                FulfillmentCenterField.type = 'hidden';
                                FulfillmentCenterField.value = Strfullfillment_Center_ID;
                                FulfillmentCenterField.name = "Fulfillment_Center";
                                
                                var AreaField = document.createElement("input");
                                AreaField.type = 'hidden';
                                AreaField.value = Strarea;
                                AreaField.name = "Area";
                                
                                var LocationField = document.createElement("input");
                                LocationField.type = 'hidden';
                                LocationField.value = Strlocation;
                                LocationField.name = "Location";
                                
                                
                                myform.action = url;
                                myform.method = "get"
                                myform.appendChild(FulfillmentCenterField);
                                myform.appendChild(AreaField);
                                myform.appendChild(LocationField);
                                document.body.appendChild(myform);
                                myform.submit();
                                
                                
                                
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
                        	
                        
							<kendo:grid-column-commandItem className="fa fa-times btn btn-sm red filter-cancel" name="DeleteDetails" text="${ScreenLabels.getLocationMaintenace_Delete()}">
								<kendo:grid-column-commandItem-click>
							<script>
							function deleteMenuMessages(e) {
				                
			                     e.preventDefault();

                               var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                              
                               var Strfullfillment_Center_ID=dataItem.fullfillment_Center_ID;
                            	var Strarea=dataItem.area;
                          		var Strlocation=dataItem.location;
           
	                           		
	                                var dataSource = $("#LocationMaintenance").data("kendoGrid").dataSource;
	                              /*   var url= "${pageContext.request.contextPath}/MenuMessageDelete"; */
	                               /*  if (confirm("${ScreenLabels.getLocationMaintenace_Are_you_sure_you_want_to_delete_this_record()}") == true) {
	                                */
	                                bootbox
                                    .confirm(
                                      '${ScreenLabels.getLocationMaintenace_Are_you_sure_you_want_to_delete_this_record()}',
                                      function(okOrCancel) {

                                       if(okOrCancel == true)
                                       {
	                                	
	                                	$.get("${pageContext.request.contextPath}/RestLocationExistForDelete",
	                    						{
	                    							FulfillmentCenter : Strfullfillment_Center_ID,
	                    							Area : Strarea,
	                    							Location : Strlocation,	
	                    							
	                    						},
	                    						function(data, status) {
	                    							
	                    							for (key in data) {
	                    								
	                    								
	                    							if (data[key] == "Zero") {
	                    								bootbox.alert("${ScreenLabels.getLocationMaintenace_Location_is_still_assigned_and_can_not_be_deleted()}",function(){});
	                    								
	                    							}
	                    							else if (data[key] == "One") {
	                    								bootbox.alert("${ScreenLabels.getLocationMaintenace_Location_has_Inventory_and_can_not_be_deleted()}",function(){});
	                    								
	                    								// alert ("User Already Exists");
	                    							}
	                    							else if(data[key] == "Delete") {			
	                    								
	                    						 			  var url= "${pageContext.request.contextPath}/Location_Meaintenance_Delete";
	                    	                          		  var myform = document.createElement("form");

	                    	                                
	                    	                                
	                    	                                var FulfillmentCenterField = document.createElement("input");
	                    	                                FulfillmentCenterField.type = 'hidden';
	                    	                                FulfillmentCenterField.value = Strfullfillment_Center_ID;
	                    	                                FulfillmentCenterField.name = "Fulfillment_Center";
	                    	                                
	                    	                                var AreaField = document.createElement("input");
	                    	                                AreaField.type = 'hidden';
	                    	                                AreaField.value = Strarea;
	                    	                                AreaField.name = "Area";
	                    	                                
	                    	                                var LocationField = document.createElement("input");
	                    	                                LocationField.type = 'hidden';
	                    	                                LocationField.value = Strlocation;
	                    	                                LocationField.name = "Location";
	                    	                                
	                    	                                
	                    	                                myform.action = url;
	                    	                                myform.method = "post"
	                    	                                myform.appendChild(FulfillmentCenterField);
	                    	                                myform.appendChild(AreaField);
	                    	                                myform.appendChild(LocationField);
	                    	                                document.body.appendChild(myform);
	                    	                                myform.submit();																	
	                    							}
	                    						}
	                    						});
	                                }
                                      });
	                                	
	                                
						}

							</script>
			                </kendo:grid-column-commandItem-click>
							</kendo:grid-column-commandItem>
												</kendo:grid-column-command>
					</kendo:grid-column>
				</kendo:grid-columns>
		
				<kendo:dataSource pageSize="10">
					<kendo:dataSource-transport>
						<kendo:dataSource-transport-read cache="false" url="${pageContext.request.contextPath}/LocationDisplayAll"></kendo:dataSource-transport-read>
						<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/GeneralCode"></kendo:dataSource-transport-read> --%>
						<kendo:dataSource-transport-update url="GeneralCodeList/Grid/update" dataType="json" type="POST" contentType="application/json" />
						<kendo:dataSource-transport-destroy	url="${pageContext.request.contextPath}/MenuMessageDelete" dataType="json" type="POST" contentType="application/json">
						<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
						</kendo:dataSource-transport-destroy>


						<kendo:dataSource-transport-parameterMap>
							<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
						</kendo:dataSource-transport-parameterMap>

					</kendo:dataSource-transport>
				



				</kendo:dataSource>
			</kendo:grid>
							
					</div>	
					<!--  <div class="margin-bottom-5-right-allign_MenuMessage_Notes" >
          
			             
			              <button class="btn btn-sm yellow   margin-bottom" id="validate" ><i class="fa fa-check"></i>Notes</button>
          			 </div> -->
					</div>
					<!-- End: life time stats -->
				</div>	
				
				
			</div>
			</div>
			
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	
	<!-- END PAGE CONTENT -->
	
</div>

</div>
</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>
 
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<!-- This is used to Set Custom Css that created by Aakash Bishnoi for Location's all Screens -->
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />

<script>

function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
  

  jQuery(document).ready(function() {    

	  $(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	  /* $("#LocationMaintenance").kendoTooltip({
	      filter: "td:nth-child(3)", //this filter selects the first column cells
	      position: "bottom", 
	      content: function(e){
	       var dataItem = $("#LocationMaintenance").data("kendoGrid").dataItem(e.target.closest("tr"));
	       var content =dataItem.notes;
	       return content;
	      }
	    }).data("kendoTooltip");
	  */
	 
	  $("#LocationMaintenance").kendoTooltip({
	       filter: "td:nth-child(3)", //this filter selects the first column cells
	       iframe:false,
		   width:250,
	       position: "bottom",
	       content: function(e){
	    	   
	    	
	        var dataItem = $("#LocationMaintenance").data("kendoGrid").dataItem(e.target.closest("tr"));
	        var content = dataItem.notes;
	       if(content){
	         return content;
	        }
	        else{
	         e.preventDefault();
	   }
	       }
	     }).data("kendoTooltip");
	  
	  
	setInterval(function () {
		
    var h = window.innerHeight;
    if(window.innerHeight>=900){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
 
});
  
  function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'LocationMaintenance',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }

    </script>


