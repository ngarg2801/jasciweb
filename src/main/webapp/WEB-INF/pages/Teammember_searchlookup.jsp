<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>


<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE"%>

<style>
#AddBtn1 {
margin-right: -2.7%;
margin-bottom: 1%;
float: right;
}

fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}

.k-grid-content {
	position: relative;
	width: 100%;
	overflow: auto;
	overflow-x: auto;
	overflow-y: hidden !important;
	zoom: 0;
}

.k-grid th.k-header, .k-grid-header {
	/*text-align: center !important;*/
	white-space: normal !important;
	padding-right: 0px !important;
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
	white-space: normal !important;
}
.k-grid .k-button {
margin: 2px .16em !important;
}
/*

.k-grid-content>table>tbody>tr {
	text-align: center !important;
} */
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div id="container" style="position: relative" class="loader_div">
		<div class="page-container">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SearchLooup()}</h1>
					</div>
				</div>


				<div class="page-content" id="page-content" >
					<div class="container">
					<!-- <div class="box">
                <a href="#" class="k-button" id="save">Save State</a>
                <a href="#" class="k-button" id="load">Load State</a>
            </div> -->
					<div>
					<a id="AddBtn1"
									href="${pageContext.request.contextPath}/Team_member_maintenancenew"
									class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
									<span class="hidden-480">
										${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddNew()}</span>
								</a>
					
					</div>
						
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active">Dashboard</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-46">
							<!-- <div class="col-md-12">


								<c:if test="${!PartOfTeamMemberObj.equalsIgnoreCase('')}">
									<div class="form-group"
										style="margin-left: -15px; margin-bottom: -30px;">
										<label class="col-md-2 control-label"
											style="width: 800px; font-size: 16; margin-left: -15px;"><b>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_PartOfTheTeamMemberName()}
												: ${PartOfTeamMemberObj} <b></label>

									</div>
									</br>
									</br>
									</br>
								</c:if>
							</div> -->

							<!-- Begin: life time stats -->
							<div class="portlet">

								

								<div class="row">

									<kendo:grid name="TeammembersList" columnResize="true" dataBound="gridDataBound"
										sortable="true" resizable="true" reorderable="true" >
										<kendo:grid-pageable refresh="true" pageSizes="true"
											buttonCount="5">
										</kendo:grid-pageable>
										<kendo:grid-editable mode="inline" confirmation="Message" />

										<kendo:grid-columns>

											<kendo:grid-column
												title="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Name()}"
												field="sortname" width="33%;" />
											<kendo:grid-column title="FirstName" field="firstname"
												hidden="true" />
											<kendo:grid-column title="LastName" field="lastname"
												hidden="true" />

											<kendo:grid-column
												
												title="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMember()}"
												field="teammember" width="30%;" />
												
											<kendo:grid-column
												title="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Status()}"
												field="currentstatus" width="15%;" />	

											<kendo:grid-column
												title="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Action()}"
												width="22%;">
												<kendo:grid-column-command>
												
												<!-- note button -->
												
													<kendo:grid-column-commandItem
														className="btn btn-sm yellow filter-submit margin-bottom fa fa-note"
														name="notebtn"
														text=" ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Notes()}">
														<kendo:grid-column-commandItem-click>
															<script>
                           
                            function showNoteScreen(e) {
                               

                                e.preventDefault();
	
                                
                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                
                                var StrTeamMember=dataItem.teammember;
                                            
                                 var url= "${pageContext.request.contextPath}/Notes_Lookup";
                              
                                var myform = document.createElement("form");

                                                                   
                                var NotesLinkField = document.createElement("input");
                                NotesLinkField.type = 'hidden';
                                NotesLinkField.value = StrTeamMember;
                                NotesLinkField.name = "NOTE_LINK";
                                
                                var NotesIdField = document.createElement("input");
                                NotesIdField.type = 'hidden';
                                NotesIdField.value = "TEAMMEM";
                                NotesIdField.name = "NOTE_ID";
                                myform.method = "get";
                                
                                myform.action = url;
                               
                                myform.appendChild(NotesLinkField);
                                myform.appendChild(NotesIdField);
                                document.body.appendChild(myform);
                                myform.submit();
                               
                            }
                            
                          
                            </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

													<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
													<kendo:grid-column-commandItem
														className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
														name="editDetails"
														text="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Edit()}">
														<kendo:grid-column-commandItem-click>
															<script>
                           
                            function editTeammember(e) {
                               

                                e.preventDefault();
	
                                
                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                var teamMember=dataItem.teammember;
                               
                                
                               
                               <%--  var contextPath='<%=request.getContextPath()%>'; --%>
                               
                             
                                 var url="${pageContext.request.contextPath}/Teammember_searchlookup_update";
                                //var wnd = $("#details").data("kendoWindow");
                               
    							 var myform = document.createElement("form");

                                 var teamMemberFiled = document.createElement("input");
                                 teamMemberFiled.value = teamMember;
                                 teamMemberFiled.name = "teamMember";
                                 teamMemberFiled.setAttribute("type", "hidden");
                                 
                                 var LastNameField = document.createElement("input");
                                 LastNameField.setAttribute("type", "hidden");
                                 LastNameField.value = "LastName";
                                 LastNameField.name = "LastName";
                                 
                                 myform.action = url;
                                 myform.method = "get";
                                 myform.appendChild(teamMemberFiled);
                                 myform.appendChild(LastNameField);
                                 document.body.appendChild(myform);
                                 myform.submit();
    							//alert(res);
                              // window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                            }
                            
                          
                            </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

													<kendo:grid-column-commandItem
														className="btn btn-sm red filter-cancel fa fa-times"
														name="btnDelete" text="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Delete()}">

														<kendo:grid-column-commandItem-click>

															<script>						   
							    function deleteRecords(e) { 
                               

                                e.preventDefault();
                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                
                                
                               var currentStatus=dataItem.currentstatus;
                                
                               if(currentStatus =='Leave of Absence' || currentStatus=='Active'){
                                
                            	   bootbox.confirm('${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD()}',
                            			   function(okOrCancel) {
                            			                            if(okOrCancel == true)
                            			                                {
                            			                            	
                            			                            	 
                            			                                   var tenant=dataItem.tenant;
                            			                                   var sortValue='LastName'
                            			                                   var teamMember=dataItem.teammember;
                            			                                  
                            			   						    	var url= "${pageContext.request.contextPath}/Teammember_searchlookup_delete";
                            			   	                           
                            			   						    	
                            			   						    	 var myform1 = document.createElement("form");

                            			                                    var tenantFiled = document.createElement("input");
                            			                                    tenantFiled.value = tenant;
                            			                                    tenantFiled.name = "tenant";
                            			                                    tenantFiled.setAttribute("type", "hidden");
                            			                                    
                            			                                    var teamMemberFiled = document.createElement("input");
                            			                                    teamMemberFiled.value = teamMember;
                            			                                    teamMemberFiled.name = "teamMember";
                            			                                    teamMemberFiled.setAttribute("type", "hidden");
                            			                                    
                            			                                                
                            			                                   
                            			                                    
                            			                                    var sortValueFiled = document.createElement("input");
                            			                                    sortValueFiled.value = sortValue;
                            			                                    sortValueFiled.name = "sortValue";
                            			                                    sortValueFiled.setAttribute("type", "hidden");
                            			                                    
                            			                                    
                            			                                    myform1.action = url;
                            			                                    myform1.method = "post";
                            			                                   
                            			                                    
                            			                                    myform1.appendChild(tenantFiled);
                            			                                    myform1.appendChild(teamMemberFiled);
                            			                                    myform1.appendChild(sortValueFiled);
                            			                                    
                            			                                    document.body.appendChild(myform1);
                            			                                    myform1.submit();
                            			   						    	
                            			   						    	
                            			   						    	//var wnd = $("#details").data("kendoWindow");
                            			   	                              // window.location=url;
                            			                            	
                            			   }
                            			   });
                            	   
                               }else{
                            	  
                            	   
                            	   alert('${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE()}');
                            	   
                               }
										}
							    </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

												</kendo:grid-column-command>
											</kendo:grid-column>
										</kendo:grid-columns>

										<kendo:dataSource pageSize="10">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/Teammember_searchlookup/Grid/read"></kendo:dataSource-transport-read>
												<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/Customers"></kendo:dataSource-transport-read> --%>
												<kendo:dataSource-transport-update
													url="GeneralCodeList/Grid/update" dataType="json"
													type="POST" contentType="application/json" />
												<%-- <kendo:dataSource-transport-destroy
													url="GeneralCodeList/Grid/delete" dataType="json"
													type="POST" contentType="application/json"> --%>
													<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
												<%-- </kendo:dataSource-transport-destroy> --%>


												<%-- <kendo:dataSource-transport-parameterMap>
													<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
												</kendo:dataSource-transport-parameterMap> --%>

											</kendo:dataSource-transport>
											<kendo:dataSource-schema>
												<kendo:dataSource-schema-model id="intKendoID">
													<kendo:dataSource-schema-model-fields>
														<kendo:dataSource-schema-model-field name="intKendoID">

														</kendo:dataSource-schema-model-field>


													</kendo:dataSource-schema-model-fields>
												</kendo:dataSource-schema-model>
											</kendo:dataSource-schema>



										</kendo:dataSource>
									</kendo:grid>

									<div
										style="float: right; position: relative; margin-top: 10px;">
									<%-- 	<button class="btn btn-sm yellow margin-bottom">
											<i class="fa fa-note"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Notes()}
										</button> --%>
										<button class="btn btn-sm yellow  margin-bottom" onclick="sortData();" id="sortBtn" style="min-width: 130px!important;"><i class="fa fa-caret-up"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SortFirstName()}</button>

										<%-- <button class="btn btn-sm yellow  margin-bottom" onclick="sortData();" id="sortBtn"><i class="fa fa-check"></i>${viewLabels.getLblSelectionSortFirstName()}</button> --%>


									</div>

								</div>
								<!-- End: life time stats -->
							</div>

						</div>
						<div class="row"></div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>
	</div>
	</tiles:putAttribute>


</tiles:insertDefinition>
    
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
 <script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>
<script>
  
    	

function sortData()
{
	
	//sortFirstName display:none;
	 
	/* value='${SortName}';
	
	window.location.href = '${pageContext.request.contextPath}/Teammember_Sort_searchlookup/'+value;
  */
  
//sortFirstName display:none;
 
 /* value='${SortName}';
 var kendoGridData=$("#CompanyList").data('kendoGrid');
 var dsSort = [];
 dsSort.push({ field: "firstName", dir: "asc" });
 kendoGridData.dataSource.sort(dsSort);  */

 //window.location.href = '${pageContext.request.contextPath}/Teammember_Sort_searchlookup/'+value;
 
var Button=document.getElementById("sortBtn");
 var text=Button.innerHTML;
 if(text==='<i class=\"fa fa-caret-up\"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SortFirstName()}')
  {
  
  Button.innerHTML="<i class=\"fa fa-caret-up\"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SortLastName()}";
  var kendoGridData=$("#TeammembersList").data('kendoGrid');
  var dsSort = [];
  dsSort.push({ field: "firstname", dir: "asc" });
  kendoGridData.dataSource.sort(dsSort);  
  }
 else if(text==='<i class=\"fa fa-caret-up\"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SortLastName()}')
  
  {
  Button.innerHTML="<i class=\"fa fa-caret-up\"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SortFirstName()}";
  var kendoGridData=$("#TeammembersList").data('kendoGrid');
  var dsSort = [];
  dsSort.push({ field: "lastname", dir: "asc" });
  kendoGridData.dataSource.sort(dsSort); 
  
  } 
 
}
  

function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'TeamMember',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }
  $(function () {
		/*  var grid = $("#TeammembersList").data("kendoGrid");

     $("#save").click(function (e) {
         e.preventDefault();
         localStorage["kendo-grid-options"] = kendo.stringify(grid.getOptions());
     });

     $("#load").click(function (e) {
         e.preventDefault();
         var options = localStorage["kendo-grid-options"];
         if (options) {
             grid.setOptions(JSON.parse(options));
         }
     }); */
	});
</script>
<script>
function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
   
   
	jQuery(document).ready(function() {
		
		
		
		setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
		
		 var kendoGridData = $("#TeammembersList").data('kendoGrid');
	      var dsSort = [];
	      dsSort.push({
	       field : "lastname",
	       dir : "asc"
	      });
	      
	      kendoGridData.dataSource.sort(dsSort);
		
		var grid = $("#TeammembersList").data("kendoGrid");
		grid.bind("dataBound", function(e) {
			$("#TeammembersList tbody tr .k-grid-btnDelete").each(function () {
		        var currentDataItem = $("#TeammembersList").data("kendoGrid").dataItem($(this).closest("tr"));
		 
		        //Check in the current dataItem if the row is deletable
		        var currentStatus=currentDataItem.currentstatus;
		         if (currentStatus =='Deleted') {
		            $(this).remove();
		        } 
		    });
		});
		
		/* var dataSource= grid.dataSource;
		var state = {
		        columns: grid.columns,
		        page: dataSource.page(),
		        pageSize: dataSource.pageSize(),
		        sort: dataSource.sort(),
		        filter: dataSource.filter(),
		        group: dataSource.group()
		    };
		 */
		
		/* $("#save").click(function (e) {
	         e.preventDefault();
	        
	         localStorage["kendo-grid-options"] = JSON.stringify((state);
	     });

	     $("#load").click(function (e) {
	         e.preventDefault();
	         var grid = $("#TeammembersList").data("kendoGrid");
	         var state1 = localStorage["kendo-grid-options"];
	         var state = JSON.parse(state);
	         var options = grid.options;
	         options.columns = state.columns;

	            options.dataSource.page = state.page;
	            options.dataSource.pageSize = state.pageSize;
	            options.dataSource.sort = state.sort;
	            options.dataSource.filter = state.filter;
	            options.dataSource.group = state.group;
	            grid.setOptions(options);
	            
	        /*  if (optionsold) {
	             grid.setOptions(JSON.parse(optionsold));
	         } */
	     //}); 
	     
		 $("#save").click(function (e) {
	         e.preventDefault();
	        
	         localStorage["kendo-grid-options"] = kendo.stringify(grid.getOptions());
	     });

	     $("#load").click(function (e) {
	         e.preventDefault();
	         var options = localStorage["kendo-grid-options"];
	         if (options) {
	             grid.setOptions(JSON.parse(options));
	         }
	     }); 
		
		
		

	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>