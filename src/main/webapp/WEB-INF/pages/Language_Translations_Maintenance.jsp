<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<style>
fa-remove:before, .fa-close:before, .fa-times:before,.fa-check:before {
	margin-right: 5px !important;
}
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">

	<tiles:putAttribute name="body">
		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1> ${ViewLabels.getLanguageTranslation_LTMaintenance()}</h1>
					</div>
				</div>
				
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#"></a><i class="fa fa-circle"></i></li>
							<li class="active"></li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
						<div id="ErrorMessage" class="note note-danger" style="display:none; width:657px ; ">
     <p id="Perror" class="error error-Top" style="margin-left: -7px !important;"></p>	
    </div>
							<form:form class="form-horizontal form-row-seperated"  onsubmit="return isformSubmit();" action="#" commandName="InsertRow" name="myForm">
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									<input type="hidden" name="getLastActivitydate" id="getLastActivitydate">
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
											<div class="form-group">
													<label class="col-md-2 control-label"> ${ViewLabels.getLanguageTranslation_LastActivityDate()} :
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" id="LastActivityDate" name="LastActivityDate" disabled="true">
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getLanguageTranslation_LastActivityBy()} :
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" name="LastActivityBy" value="${LanguageBe.getLastActivityBy()}" disabled="true">
													</div>
												</div>

												
												<div class="form-group">
												
												
													<label class="col-md-2 control-label">${ViewLabels.getLanguageTranslation_Language()} :<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
														<select class="selectmenuappicon" id="Language" name="Language">
															<option value="">${ViewLabels.getLanguageTranslation_Select()}...</option>
															 <c:forEach items="${LanguageList}" var="LanguageItem">
                                                                       <option value="${LanguageItem.getGeneralCode()}">${LanguageItem.getDescription20()}</option>
																	</c:forEach>
															
														</select><span id="ErrorMessageLanguage" class="error-select" ></span>
											       </div>														
																							
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"> ${ViewLabels.getLanguageTranslation_KeyPhrase()} :<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" id="KeyPhrase" name="KeyPhrase" maxlength="500" >
														<span id="ErrorMessageKeyPhrase" class="error" ></span>
														</div>
																							
												</div>

												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getLanguageTranslation_Translation()} :<span class="required" maxlength="500">
													* </span>
													
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" id="Translation" name="Translation" maxlength="500">
														<span id="ErrorMessageTranslation" class="error" ></span>
													</div>														
													
												</div>	
											<div class="info_helpUpdate_margin-bottom-5_language_update">
											<button class="btn btn-sm yellow  margin-bottom" id="SaveBtn"><i class="fa fa-check"></i>${ViewLabels.getLanguageTranslation_SaveUpdate()}</button>
											<button class="btn btn-sm red " type="button" onclick=" reloadPage();"><i class="fa fa-times"></i> ${ViewLabels.getLanguageTranslation_Reset()}</button>
										    <button class="btn btn-sm red " type="button" onclick=" PageReset();"><i class="fa fa-times"></i> ${ViewLabels.getLanguageTranslation_Cancel()}</button>
										    </div>		
												
												
												
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
					</form:form>
							</div>
						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		
		<!-- END PAGE CONTAINER -->
		<!-- BEGIN PRE-FOOTER -->

	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>



/* function PageReset()
{

		
		window.location.href='Language_Translations_Maintenance';
	
} */
function reloadPage(){													
	
	/* location.reload(); */
	window.parent.location = window.parent.location.href;
}
		function PageReset(){													
			
			var backState = '${backStatus}';
		     
			
			   if (backState == 'lookup') {

				   window.location="${pageContext.request.contextPath}/Language_Translations_Lookup";
			   
			  }
			  else{
			   
				  window.location="${pageContext.request.contextPath}/Language_Translations_Search_Lookup";
			  } 
			   
			
			 
			 
		}
jQuery(document).ready(function() { 
	
	
	$('#LastActivityDate').val(getLocalDate());
	var message='${Saved}';
	if(message=='Saved')
		{
		 bootbox.alert('${ViewLabels.getLanguageTranslation_SaveMessage()}',function(){
		window.location.href="Language_Translations_Lookup";
		 });
		}
	
	
	
	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
	
	
  /*  Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo(theme settings page)
   Index.init(); // init index page
   Tasks.initDashboardWidget(); // init tash dashboard widget */
});


function headerChangeLanguage(){
    
    /* alert('in security look up'); */
    
    $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
    
   }

   function headerInfoHelp(){
    
    //alert('in security look up');
    
     /* $
    .post(
      "${pageContext.request.contextPath}/HeaderInfoHelp",
      function(
        data1) {

       if (data1.boolStatus) {

        location.reload();
       }
      });
     */
      $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
        {InfoHelp:'LanguageTranslation',
      InfoHelpType:'PROGRAM'
      
        }, function( data1,status ) {
         if (data1.boolStatus) {
          window.open(data1.strMessage); 
                 
        }
         else
          {
        //  alert('No help found yet');
          }
         
         
        });
    
   }

</script>
<script>
	function isformSubmit() {

		var isSubmit = false;
	
		var isBlankLanguage = isBlankField('Language',
				'ErrorMessageLanguage', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getLanguageTranslation_MandatoryFieldMessage()}');
		
		var isBlankKeyPhrase= isBlankField('KeyPhrase',
				'ErrorMessageKeyPhrase', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getLanguageTranslation_MandatoryFieldMessage()}');
		
		var isBlankTranslation= isBlankField('Translation',
				'ErrorMessageTranslation', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getLanguageTranslation_MandatoryFieldMessage()}');
		
				
		
		if(isBlankLanguage && isBlankKeyPhrase && isBlankTranslation)
		{
			
			    var languageVal = $('#Language').val();
			    var keyPhraseVal=$('#KeyPhrase').val();
				var languageText=$('#Language').text();
				$.post(
								"${pageContext.request.contextPath}/RestAlreadyExist",
								{Language:languageVal, KeyPhrase:keyPhraseVal},
								function(data, status) {

									if (data) {
										var errMsgId = document.getElementById("Perror");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${ViewLabels.getLanguageTranslation_KeyPhraseAlreadyExist()}';
										$('#ErrorMessage').show();
										isSubmit = false;
									}else {
										
										//alert("Not exist");
										
										 isSubmit=true;
										 //var languageVal = $('#Language').val();
										 $('#getLastActivitydate').val(getUTCDateTime());
										    var keyPhraseVal=$('#KeyPhrase').val().trim();
										    document.getElementById('KeyPhrase').value=keyPhraseVal;
										 	var TranslationVal = $('#Translation').val().trim();
										    document.getElementById('Translation').value=TranslationVal;
										    
							        	 document.myForm.action = '${pageContext.request.contextPath}/Language_Translation_New';
										 document.myForm.submit();
									}
								});//end post 

        	 
		}
		return isSubmit;
	
	}
		
		
</script>		
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>