
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date" %>
<!DOCTYPE html>
<style>

.Grid-centre
{
margin-top: 37px;
margin-left: -17px;
padding-right: 15px;
}

.k-grid th.k-header,
.k-grid-header
{
  /*  text-align:center !important; */
}
.k-grid-content>table>tbody>tr
{
    
    /* text-align:center !important; */
}

.fa-remove:before, .fa-close:before, .fa-times:before,.fa-check:before,.fa-search:before {
	margin-right: 5px !important;
}

.fa-plus:before{
margin-right: 5px !important;
}


.icon-pencil:before {
 margin-right: 5px !important;
}
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div id="container" style="position: relative" class="loader_div">
	<div class="page-container">
	
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${ViewLabels.getTeamMemberMessages_ActiveTMM()} </h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
					
		</div>
	
	<div class="page-content" id="page-content">
		<div class="container">
			
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
			<div class="col-md-12">
					<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
					<!-- Begin: life time stats -->
					<div class="portlet">
					<div class="form-body">
            <div class="form-group" style="margin-left: -15px;">
             <label class="col-md-2 control-label" style="font-size:16px; width:500px;"><b>${ViewLabels.getTeamMemberMessages_Teammember()}: &nbsp;${TeamMember}</b>
             </label>
             
            </div><br>
            
            </div>
							
								<a id="AddBtnActiveMessages" href="${pageContext.request.contextPath}/TeamMemberMessagesMaintenanceNew" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${ViewLabels.getTeamMemberMessages_Addnew()} </span>
								</a>
							
						
						<div class="portlet-body">
				
			<div class="table-container"  >
					<kendo:grid name="TeammembersMessagesList" id="TeammembersMessagesList" resizable="true"
					 reorderable="true" databound="hoverOnGrid" sortable="true" autoSync="true" dataBound="gridDataBound" >
						
				<kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5">
    	        </kendo:grid-pageable>
				<kendo:grid-editable mode="inline" confirmation="Message" />
					<kendo:grid-columns>
					
					<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_StartDate()}" field="startDate" />
					<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_EndDate()}" field="endDate" />
					<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_Type()}" field="type" />
					<span id="MessageId">
					<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_Message()}" field="message"  />
					</span>
     				<kendo:grid-column title="${ViewLabels.getTeamMemberMessages_Actions()}">
						<kendo:grid-column-command>
							<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
						<kendo:grid-column-commandItem className="icon-pencil btn btn-sm yellow filter-submit margin-bottom" name="editDetails" text="${ViewLabels.getTeamMemberMessages_Edit()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function showDetails(e) 
                            {
                                  e.preventDefault();
	                              var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                  var tenant=dataItem.tenant;
						    	  var company=dataItem.company;
						    	  var teamMember=dataItem.teamMember;
                                  var type=dataItem.type;
                                  var messagenumber=dataItem.messageNumber;
                            //  ${pageContext.request.contextPath}/Teammember_Message_Edit/${TeamMemberMessages.getTeamMemberMessagesId().getTenant()}
                            ///${TeamMemberMessages.getTeamMemberMessagesId().getTeamMember()} /${TeamMemberMessages.getTeamMemberMessagesId().getCompany()}'
                               
                                var url= "${pageContext.request.contextPath}/Teammember_Message_Edit";
                                //var wnd = $("#details").data("kendoWindow");
                                var myform1 = document.createElement("form");

                                var teamMemberField = document.createElement("input");
                                teamMemberField.value = teamMember;
                                teamMemberField.name = "TeamMember";
                                teamMemberField.setAttribute("type", "hidden");
                                
                                var typeField = document.createElement("input");
                                typeField.value = type;
                                typeField.name = "Type";
                                typeField.setAttribute("type", "hidden");
                                
                                var messagenumberField = document.createElement("input");
                                messagenumberField.value = messagenumber;
                                messagenumberField.name = "MessageNumber";
                                messagenumberField.setAttribute("type", "hidden");
                                          
                                myform1.action = url;
                                myform1.method = "post";
                               
                                
                                myform1.appendChild(teamMemberField);
                                myform1.appendChild(messagenumberField);
                                myform1.appendChild(typeField);

                                
                                document.body.appendChild(myform1);
                                myform1.submit();
                                
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
							
						<kendo:grid-column-commandItem className="fa fa-times btn btn-sm red filter-cancel"  name="deleteDetails" text="${ViewLabels.getTeamMemberMessages_Delete()}">
							   <kendo:grid-column-commandItem-click>
                            <script>
                            function deleteConfirmation(e) {
                            	  e.preventDefault();
                            	
                            	  var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
						    	  var tenant=dataItem.tenant;
						    	  var company=dataItem.company;
						    	  var teamMember=dataItem.teamMember;
						    	  var message=dataItem.message;
						    	  var messagenumber=dataItem.messageNumber;
						    	  
                            	  bootbox
                                  .confirm(
                                    '${ViewLabels.getTeamMemberMessages_DeleteMessage()}',
                                    function(okOrCancel) {

                                     if(okOrCancel == true)
                                     {
                            	  
							   /*  if (confirm('${ViewLabels.getTeamMemberMessages_DeleteMessage()}') == true) {
							     */	
							    	
							    	 
							    	  
							    	  
							    	  // '${pageContext.request.contextPath}/Teammember_Message_delete/${TeamMemberMessages.getTeamMemberMessagesId().getTenant()}
							    	  // /${TeamMemberMessages.getTeamMemberMessagesId().getTeamMember()}/${TeamMemberMessages.getTeamMemberMessagesId().getCompany()}
							    	  // /${TeamMemberMessages.getTicketTapeMessge()}'
		                              var url= "${pageContext.request.contextPath}/Teammember_Message_delete/";
		                                //var wnd = $("#details").data("kendoWindow");
		                              //var url= "${pageContext.request.contextPath}/Language_Delete/";
		                                
		                                
		                                var myform1 = document.createElement("form");

		                                var teamMemberField = document.createElement("input");
		                                teamMemberField.value = teamMember;
		                                teamMemberField.name = "TeamMember";
		                                teamMemberField.setAttribute("type", "hidden");
		                                
		                                var messagenumberField = document.createElement("input");
		                                messagenumberField.value = messagenumber;
		                                messagenumberField.name = "MessageNumber";
		                                messagenumberField.setAttribute("type", "hidden");
		                                          
		                                myform1.action = url;
		                                myform1.method = "post";
		                               
		                                
		                                myform1.appendChild(teamMemberField);
		                                myform1.appendChild(messagenumberField);
		                               
		                                document.body.appendChild(myform1);
		                                myform1.submit();
		                                
							    	
							    
							    
							
                                    }
                                    });
                            }
                            
                            </script>
                        </kendo:grid-column-commandItem-click>
                         </kendo:grid-column-commandItem>
                        
						</kendo:grid-column-command>
					</kendo:grid-column>
				</kendo:grid-columns>
		                    
				<kendo:dataSource pageSize="10" autoSync="true">
					<kendo:dataSource-transport>
						<kendo:dataSource-transport-read cache="false" url="${pageContext.request.contextPath}/Teammember_Messages_search/Grid/read"></kendo:dataSource-transport-read>
						<kendo:dataSource-transport-parameterMap>
							<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
						</kendo:dataSource-transport-parameterMap>

					</kendo:dataSource-transport>
				<kendo:dataSource-schema>
                <kendo:dataSource-schema-model id="intKendoID">
                    <kendo:dataSource-schema-model-fields>
                        <kendo:dataSource-schema-model-field name="intKendoID" >
                        	
                        </kendo:dataSource-schema-model-field>
                         
                        
                    </kendo:dataSource-schema-model-fields>
                </kendo:dataSource-schema-model>
            </kendo:dataSource-schema>



				</kendo:dataSource>
			</kendo:grid>
		
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
</div>
</div></div></div></div></div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>
<script>

function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   }
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);


function hoverOnGrid(e)
{


$("tr", "#TeammembersMessagesList").on("mouseover", function (ev) {
    // Invoke display tooltip / edit row
    var rowData = grid.dataItem(this);
    if (rowData) {
        showTooltip(rowData);
    }
});



/* $("#TeammembersMessagesList").on("mouseover", function (ev)
{
/* //alert("hello");  
//var dataItem = this.dataItem($(ev.currentTarget).closest("tr"));

// Invoke display tooltip / edit row
var grid = $("#TeammembersMessagesList").data("kendoGrid");
//  var rowData = grid.dataItem(this);
// var selectedItem = grid.dataItem(grid.select());



     var cellIndex = $(this).parent().parent().index();
         	    var grid = $("#TeammembersMessagesList").data("kendoGrid");
                var dataItem = grid.dataItem(grid.tbody.find('tr:eq(' + cellIndex + ')'));
// var teamMember=selectedItem.teammember;
//showTooltip("hello");

}); */




}



    jQuery(document).ready(function() {    
    	
    	
    	var tooltip=$("#MessageId").kendoTooltip({
            content: "Hello!",
            
            position: "top"
            	
        });
    	
    	
    	
    	$(window).keydown(function(event){
    	       if(event.keyCode == 13) {
    	         event.preventDefault();
    	         return false;
    	       }
    	     });
         //  Metronic.init(); // init metronic core components
//Layout.init(); // init current layout
//Demo.init(); // init demo features
    // EcommerceOrders.init();
    
setInterval(function () {
	
    var h = window.innerHeight;
    if(window.innerHeight>=900 || window.innerHeight==1004 ){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
 
});
      
    function headerChangeLanguage(){
    	   
    	   /* alert('in security look up'); */
    	   
    	   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
    	   
    	   
    	  }

    	  function headerInfoHelp(){
    	   
    	     $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
    	       {InfoHelp:'TeamMemberMessage',
    	     InfoHelpType:'PROGRAM'
    	     
    	       }, function( data1,status ) {
    	        if (data1.boolStatus) {
    	         window.open(data1.strMessage); 
    	                
    	       }
    	        else
    	         {
    	       //  alert('No help found yet');
    	         }
    	        
    	        
    	       });
    	   
    	  }
                                    
    
    </script>
    <script>
    
    /* 	function deleteConfirmation(url) {
    	    var x;
    	  
    	    if (confirm("Are you sure you want to delete this message?") == true) 
    	    {
    	     
    	    	window.location.href = url;
    	     
    	    } else{}
    	}
    	    
    	    function edit(url)
    	    {
    	    
    	    	 window.location.href = url;
    	    } */
    	
    
    </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>