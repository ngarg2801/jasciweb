<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="com.jasci.common.constant.CONFIGURATIONEFILE,java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,com.jasci.common.util.ZIPVALIDATION,java.text.SimpleDateFormat,java.util.Date"%>
	
<!DOCTYPE html>


<%
	/* Properties ObjectProperty = new Properties();
	String propFileName = "config.properties";
	try {
		InputStream ObjectInputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		ObjectProperty.load(ObjectInputStream);
	} catch (IOException ObjectIOException) {
	} */
	CONFIGURATIONEFILE ObjConfiguration=new CONFIGURATIONEFILE();
	String CountryCode = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.CountryCode);
%>

<c:set var="cd" value="<%=CountryCode%>" />


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMemberMaintenanceNew()}</h1>
					</div>
				</div>

				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content hideHorizontalScrollbar" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMemberMaintenanceNew()}</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">


							<!-- BEGIN PAGE CONTENT-->
							<div class="col-md-12">
								<form:form class="form-horizontal form-row-seperated" name="myForm"
									action="#"
									onsubmit="return isformSubmit();"
									commnadName="TEAM_MEMBERS_REGISTER">
									<div class="portlet">
										<input type="hidden" name="getSetupdate" id="getSetupdate">
										<input type="hidden" name="getLastActivitydate" id="getLastActivitydate">
								 			
										<div class="portlet-body">

											<%-- <div id="ErrorMessage" class="note note-danger"
												style="display: none">
												<p class="error" style="margin-left: -7px !important;">${TeamMemberInvalidTeamMemberIdMsg}</p>
											</div>
 --%>
<div id="ErrorMessage"  style="display: none;" class="note note-danger"  >
     <p  class="error error-Top margin-left-7pix" id="Perror">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidTeamMemberIdMsg}</p> 
    </div>

											<div id="ErrorMessage1" style="display: none;" class="note note-danger"
												>
												<p class="error error-Top margin-left-7pix" id="Perror1" ></p>
											</div>




											<!-- <div id="ErrorMessage" class="note note-danger"
												style="display: none; width: 400px;">
												<p></p>
											</div>
											<div id="ErrorMessage1" class="note note-danger"
												style="display: none; width: 400px;">
												<p></p>
											</div> -->



											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">


															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMemberId()}:<span
																	class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Teammember" id="Teammember" maxlength="20"
																		><span
																		id="ErrorMessageTeammember" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SetUpDate()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Setupdate" id="Setupdate"
																		disabled="disabled">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SetupBy()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Setupby" value="${TeamMemberName}"
																		disabled="disabled">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastActivityDate()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Lastactivitydate"  id="Lastactivitydate"
																		disabled="disabled">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastActivityBy()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Lastactivityteammember" value="${TeamMemberName}"
																		disabled="disabled">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_CurrentStatus()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Currentstatus" id="Currentstatus">

																		<option value="A">Active</option>
																		<option value="L">Leave of Absence</option>
																		<!-- <option value="I">Not Active</option> -->

																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_FirstName()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		id="Firstname" name="Firstname" maxlength="50" >
																	<span id="ErrorMessageFirstname" class="error">
																	</span>
																</div>

															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastName()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Lastname" id="Lastname" maxlength="50" >
																	<span id="ErrorMessageLastname" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_MiddleName()}:<!-- span
															class="required"> * </span> -->
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Middlename" id="Middlename" maxlength="50">
																		
																	<span id="ErrorMessageMiddlename" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine1()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline1" maxlength="100"
																		id="Addressline1"> <span
																		id="ErrorMessageAddressline1" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine2()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline2" maxlength="100"
																		id="Addressline2">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine3()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline3" maxlength="100"
																		id="Addressline3">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine4()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline4" maxlength="100"
																		id="Addressline4">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_City()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="City"
																		maxlength="50" id="City"> <span
																		id="ErrorMessageCity" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_CountryCode()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		 name="Countrycode"
																		id="Countrycode">
																		<option value="none">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach
																			items="${GeneralCodeLanguageCountryCodeListObject}"
																			var="ContyCodeList">
																			<option
																				value='${ContyCodeList.getGeneralCode()}'>${ContyCodeList.getDescription20()}</option>
																		</c:forEach>
																	</select> <span id="ErrorMessageCountrycode" class="error-select"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_State()}:<span
																class="required" id="requiredState"> * </span>
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Statecode" id="Statecode">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach items="${GeneralCodeStatesListObject}"
																			var="StatesList">
																			<option
																				value='${StatesList.getGeneralCode()}'>${StatesList.getDescription20()}</option>
																		</c:forEach>
																	</select><span id="ErrorMessageState" class="error-select"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ZipCode()}:
																	<span class="required"
																	> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		maxlength="10" name="Zipcode"
																		id="ZipCode"> <span id="ErrorMessageZipCode"
																		class="error long_errormsg"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkPhone()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workphone"
																		maxlength="10" id="Workphone"> <span
																		id="ErrorMessageWorkphone" class="error"></span>

																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkExtension()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workextension" id="Workextension"
																		maxlength="10"> <span
																		id="ErrorMessageWorkextension" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkCell()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workcell"
																		maxlength="10" id="Workcell"> <span
																		id="ErrorMessageWorkcell" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkFax()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workfax"
																		maxlength="10" id="Workfax"> <span
																		id="ErrorMessageWorkfax" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkEmail()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workemailaddress"
																		maxlength="100" id="WorkEmail"> <span
																		id="ErrorMessageWorkEmail" class="error long_errormsg"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_HomePhone()}:<span
																	class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Homephone"
																		maxlength="10" id="Homephone"> <span
																		id="ErrorMessageHomephone" class="error long_errormsg"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Cell()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Cell"
																		maxlength="10" id="Cell"> <span
																		id="ErrorMessageCell" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Email()}:<span
																	class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Personalemailaddress"
																		maxlength="100" id="Email"> <span
																		id="ErrorMessageEmail" class="error long_errormsg"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyContactName()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencycontactname" maxlength="50">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label"
																	style="padding-left: -1px;">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyHomePhone()}:<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencycontacthomephone"
																		maxlength="10" id="Emergencyhomephone"> <span
																		id="ErrorMessageEmergencyhomephone" class="error long_errormsg"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyCell()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencycontactcell"
																	    maxlength="10" id="Emergencycell"> <span
																		id="ErrorMessageEmergencycell" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyEmial()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencyemailaddress"
																		maxlength="100" id="EmergencyEmail"> <span
																		id="ErrorMessageEmergencyEmail" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_DepartMent()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Department">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach items="${GeneralCodeDepartmentListObject}"
																			var="DepartmentList">
																			<option
																				value='${DepartmentList.getGeneralCode()}'>${DepartmentList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ShiftCode()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Shiftcode">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach items="${GeneralCodeShiftListObject}"
																			var="ShiftList">
																			<option value='${ShiftList.getGeneralCode()}'>${ShiftList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															<div class="form-group">																
																
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Systemuse">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<option	value='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_Y().charAt(0)}'>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_Y()}</option>
																		<option	value='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_N().charAt(0)}'>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_N()}</option>
																		
																	</select>
																</div>
															</div>
																
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Language()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Language" id="Language">
																		<%-- <option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option> --%>
																		<c:forEach items="${GeneralCodeLanguageListObject}"
																			var="LanguageList">
																			<c:if test="${LanguageList.getDescription20()==TenantLanguage}">
																			<c:set var="lnagCode" value="${LanguageList.getGeneralCode()}" />
																			
																			</c:if>
																			<option
																				value='${LanguageList.getGeneralCode()}'>${LanguageList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_StartDate()}:<span class="required" id="rqidStartDate"  style="margin-left: 0px; position: absolute;">* </span> </label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Startdate" id="Startdate" readonly="true" style="background-color:white;" >
																	<span id="ErrorMessageStartdate" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label" >${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastDate()}:
																	<!-- <span class="required" id="rqidLastDate"  style="margin-left: 0px; position: absolute;"> * </span> -->
																</label>
																<div class="col-md-10">
																	<input type="text" style="background-color:white;"
																		class="form-controlwidth" readonly="true"
																		name="Lastdate" id="Lastdate">
																	<span id="ErrorMessageLastdate" class="error"> </span>
																</div>
															</div>

														

														<div class="col-md-12" style="margin-left: -16px;">
															<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
															<!-- Begin: life time stats -->
															<div class="portlet">
																<div class="portlet-title">
																	<!--<div class="caption">
								Company: JASCI
							</div>-->
																	<div class="actions"></div>
																</div>
																<div class="portlet-body">
																	<div class="table-container">
																		<div class="table-actions-wrapper">
																			<span> </span>


																		</div>
																		<span id="ErrorMessageCompany" class="error error-Company" > </span>
																		<table
																			class=""
																			id="grid">
																			<colgroup>                  
                 													   		<col style="width:1%" />
                  													  		<col style="width:5%" />
                  														  	<col style="width:6%" />
               																</colgroup>
																			
																			<thead>
																				<tr>
																					<th width="1%">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}</th>
																					<th width="5%">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Company()}</th>
																					<th width="6%">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_CompanyName()}</th>

																				</tr>
																				</thead>
																				<tbody>
																				<c:set var="lengthList" value="${CompaniesListObject.size()}" />
																				<c:forEach items="${CompaniesListObject}"
																					var="CompaniesList">
																					
																					
																					
																					<tr >
																					<c:if test="${lengthList==1}">
																					<td><input type="checkbox"
																							name="checkBoxCompany" class="group-checkable" checked
																							value='${CompaniesList.getId().getCompany()}'>
																						</td>
																					</c:if>
																					<c:if test="${lengthList>1}">
																						<td><input type="checkbox"
																							name="checkBoxCompany" class="group-checkable"
																							value='${CompaniesList.getId().getCompany()}'>
																						</td>
																						</c:if>
																						<td>${CompaniesList.getId().getCompany()}</td>

																						<td>${CompaniesList.getName50()}</td>
																					</tr>
																				</c:forEach>

																			
																			</tbody>

																		</table>

																	</div>

																</div>

															</div>
															<!-- End: life time stats -->

														</div>



														<div class="margin-right-team-member-maintenence-btn">
															<button
																class="btn btn-sm yellow filter-submit margin-bottom">
																<i class="fa fa-check" style="margin-right: 3px;"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Save()}
															</button>
															<%-- <button class="btn btn-sm yellow  margin-bottom"
																onclick="return false;">
																<i class="fa fa-note"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Notes()}
															</button> --%>
															<button class="btn btn-sm red " type="reset" onclick="reloadPage();">
																<i class="fa fa-times"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ButtonResetText()}
															</button> 
															<button class="btn btn-sm red " type="reset" onclick="clearBtn();">
																<i class="fa fa-times"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Cancel()}
															</button> 
														</div>

													</div>
												</div>

											</div>
										</div>
									</div>
							</div>
							</form:form>
						</div>
						<!-- END PAGE CONTENT-->

						<!--end tabbable-->

						<!--end tabbable-->

					</div>

					<!-- END PAGE CONTENT INNER -->
				</div>
			</div>
			<!-- END PAGE CONTENT -->
		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>



<script src="<c:url value="/resourcesValidate/js/moment.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>" type="text/javascript"></script>




<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />
	
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />

<%-- <script src="<c:url value="/resourcesValidate/js/jquery-1.7.2.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resourcesValidate/js/jquery-ui-1.8.19.custom.min.js"/>"
	type="text/javascript"></script> --%>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	$(function() {
		 var dates = $("#Startdate, #Lastdate").datepicker(
				{
					minDate : '0',
					dateFormat : 'yy-mm-dd',
					//showOn : "button",
					//	buttonImage : "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
					//buttonImage:'/resourcesValidate/css/icon_date_picker.png',
					//buttonImage:'<c:url value="/resources/assets/admin/layout3/img/Language_Icon.png"/>',
					//buttonImageOnly : true,

					onSelect : function(selectedDate) {
						var option = this.id == "Startdate" ? "minDate"
								: "maxDate", instance = $(this).data(
								"datepicker"), date = $.datepicker.parseDate(
								instance.settings.dateFormat
										|| $.datepicker._defaults.dateFormat,
								selectedDate, instance.settings);
						date.setDate(date.getDate());
						dates.not(this).datepicker("option", option, date);
					}
				}); 
		 
		 
		 
		 

/* var dates = $("#Startdate").datepicker({
			minDate: '0',
			dateFormat: 'y-mm-dd'});				
		
var dates1 = $("#Lastdate").datepicker({
minDate: '1',
dateFormat: 'y-mm-dd'});
 */
		
		
		$('#Countrycode').change(function() {

			if ($(this).val() == '${cd}') {
				$("#Statecode").removeAttr("disabled");
				$("#requiredState").show();
				$("#idRequired").show();
				
				$("#Statecode").val($("#Statecode option:first").val());

			} else {
				$('#Statecode').attr('disabled', 'disabled');
				$("#requiredState").hide();
				$("#idRequired").hide();
				$("#Statecode").val($("#Statecode option:first").val());
			}

		});
	});

	jQuery(document)
			.ready(
					function() {
						
						
						
						 $('#Setupdate').val(getLocalDate());
						 $('#Lastactivitydate').val(getLocalDate());
						 
						 $("#grid").kendoGrid({
		                       
		                        sortable: true,
		                        resizable:true,
		                        reorderable: true,
		                                          
		                     
		                    });

						 
						 $('#ErrorMessage').hide();
						 $('#ErrorMessage1').hide();
						 
						 
						if ($('#Countrycode').val() == '${cd}') {
							$("#Statecode").removeAttr("disabled");
							$("#requiredState").show();
							$("#idRequired").show();

						} else {
						$("#requiredState").hide();
						$("#idRequired").hide();
							$('#Statecode').attr('disabled', 'disabled');
						}
						
						
						/* $("#Language option:selected").text();*/
						
						var TenantLanguageValue = '${lnagCode}';
						if (TenantLanguageValue.trim() != '') {
							  document.getElementById('Language').value = TenantLanguageValue;						
							
						} else {

						} 
						
						//Metronic.init(); // init metronic core componets
						//Layout.init(); // init layout
						//Demo.init(); // init demo(theme settings page)
						//Index.init(); // init index page
						//Tasks.initDashboardWidget(); // init tash dashboard widget
					/*	setTimeout(function() {
							
							/* var value = '${TeamMemberInvalidTeamMemberIdMsg}';

							if (value == '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_ADDRESS()}' || value == '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_ZIP_CODE()}' || value == '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST()}' || value == '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS()}') {

								$('#ErrorMessage').show();
								/* $('#ErrorMessage')
										.text(
												'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST()}');
							 	document.location.href = '#top';

							} else if (value == '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_SAVE_SUCCESSFULLY()}') {
								$('#ErrorMessage').hide();
								alert('${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_SAVE_SUCCESSFULLY()}');
								//window.location.href = 'Team_member_maintenance';
								window.location.href = 'Team_member_maintenance_lookup';
							} else {
								$('#ErrorMessage').hide();
							}
							 
							
							
     }, 500);*/
						
						

					});
</script>
<script>
	function isformSubmit() {

		var isSubmit = false;
		var testNumber = false;
				

		var isBlankFirstname = isBlankField('Firstname',
				'ErrorMessageFirstname', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankLastname = isBlankField('Lastname', 'ErrorMessageLastname',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankTeamMemberID = isBlankField('Teammember',
				'ErrorMessageTeammember', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankAddressline1 = isBlankField('Addressline1',
				'ErrorMessageAddressline1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankCity = isBlankField('City', 'ErrorMessageCity',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankHomephone = isBlankField('Homephone',
				'ErrorMessageHomephone', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		var isBlankEmergencycontacthomephone = isBlankField(
				'Emergencyhomephone', 'ErrorMessageEmergencyhomephone',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		var isNumHomephone = false;
		var isNumEmergencycontacthomephone = false;
		
		
		
		var isBlankEmail=isBlankField('Email',
				'ErrorMessageEmail', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS()}');
		var isValidEmail=false;	
		if(isBlankEmail){
			isValidEmail = isValidEmailText('Email', 'ErrorMessageEmail',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS()}');
		}
		
		
		if (isBlankHomephone) {
			isNumHomephone = isNumeric('Homephone', 'ErrorMessageHomephone',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		}

		if (isBlankEmergencycontacthomephone) {
			var isNumEmergencycontacthomephone = isNumeric(
					'Emergencyhomephone', 'ErrorMessageEmergencyhomephone',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		}
		
		var isNumWorkphone = isNumeric('Workphone', 'ErrorMessageWorkphone',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_PHONE()}');
		var isNumWorkcell = isNumeric('Workcell', 'ErrorMessageWorkcell',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_CELL()}');
		var isNumWorkfax = isNumeric('Workfax', 'ErrorMessageWorkfax',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_FAX()}');
		var isNumCell = isNumeric('Cell', 'ErrorMessageCell',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_CELL()}');		
		var isNumEmergencycontactcell = isNumeric('Emergencycell',
				'ErrorMessageEmergencycell', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL()}');		
		var isNumext = isNumeric('Workextension', 'ErrorMessageWorkextension',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION()}');

		if (isNumWorkphone && isNumWorkcell && isNumWorkfax && isNumHomephone
				&& isNumCell && isNumEmergencycontacthomephone
				&& isNumEmergencycontactcell && isNumext) {
			testNumber = true;
		}

		var isValidWorkEmail = isValidEmailText('WorkEmail',
				'ErrorMessageWorkEmail', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT()}');
		
		
		
		
		
		var isValidEmergencyEmail = isValidEmailText('EmergencyEmail',
				'ErrorMessageEmergencyEmail', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT()}');

		var isCompanyChecked = isCompanySelected('checkBoxCompany',
				'ErrorMessageCompany', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_NO_COMPANY_SELECTED()}');
		
		var isCmpnySelected = isCountrySelected();

		var status = $("#Currentstatus :selected").val();

		var isStartDateLessThenEnd = false;
		var isLastDateLessThenEnd = false;
		var isValideStartDate = true;
		var isValideLastDate = true;

		if (status != 'X') {
			$('#rqidLastDate').show();
			/* isValideStartDate = compareDate('Startdate',
					'ErrorMessageStartdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ 'Invalid start date.');*/
							
			var isBalnkStartDate= isBlankField('Startdate', 'ErrorMessageStartdate',
									'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		
			var isBalnkEndsDate=true; /* isBlankField('Lastdate', 'ErrorMessageLastdate',
									'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
			 */
			if(isBalnkStartDate && isBalnkEndsDate){
			
				isStartDateLessThenEnd = isBlankField('Startdate',
						'ErrorMessageStartdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE()}');

				var lastDateValue=$('#Lastdate').val();
				
				if(lastDateValue) { 
				
				isLastDateLessThenEnd = testDate('Startdate','Lastdate',
						'ErrorMessageLastdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE()}');
				}
				else{
				isLastDateLessThenEnd=true;
				
				}
								
			}
			
			
			

			/* isStartDateLessThenEnd = isStartDateGreaterThenEndDate('Startdate',
					'Lastdate', 'ErrorMessageStartdate',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ 'Start date must be before End date.'); 
			isLastDateLessThenEnd = isStartDateGreaterThenEndDate('Startdate',
					'Lastdate', 'ErrorMessageLastdate',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ 'Last date must be after Start date.'); */

		} else {
			isStartDateLessThenEnd = true;
			isStartDateLessThenEnd = true
			isLastDateLessThenEnd = true;
			var errMsgId1 = document.getElementById('ErrorMessageStartdate');
			var errMsgId2 = document.getElementById('ErrorMessageLastdate');

			errMsgId1.innerHTML = '';
			errMsgId2.innerHTML = '';
			$('#rqidLastDate').hide();
			//$('#rqidStartDate').hide();
			/* 	isValideLastDate = compareLastDate('Lastdate',
						'ErrorMessageLastdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ 'Invalid last date.');
				isValideStartDate = compareDate('Startdate',
						'ErrorMessageStartdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ 'Invalid start date.'); */

		}

		var validZip = isZipCodeValid();
		var validState=isStateValid();

		if (isBlankFirstname && isBlankLastname && isBlankTeamMemberID
				&& isBlankAddressline1 && isBlankCity && isBlankHomephone
				&& isBlankEmergencycontacthomephone && isBlankEmail) {

			if (isValidEmail && isValidEmergencyEmail && isValidWorkEmail) {

				if (isCompanyChecked && isCmpnySelected && testNumber) {

					if (isStartDateLessThenEnd && isLastDateLessThenEnd
							&& isValideStartDate && validZip && validState) {
						var errMsgId=document.getElementById("Perror");
						var isTeammember=true;
						var isEmail=false;
						var teammember=$('#Teammember').val();
						var tenat='${TeamMemberTenant}';
						var fulfilmentcenter='${TeamMemberFulfilmentCenter}';
						//http://localhost:8080/JASCI/RestCheckTemmmember?TeamMember=tm1212&Tenant=43310asp-d6d4-4715-8762-6dcba01010a&FulfillmentCenter=4
						 $.post( "${pageContext.request.contextPath}/RestCheckTemmmember",{
							 
						 TeamMember:teammember,
						 Tenant:tenat
						 },
						 function( data,status ) {
						 
							 if(data.length>=1 )
				        	 {
												        	
				        	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST()}';
				        	 $('#ErrorMessage').show();
				        	 isSubmit=false;
				        	// alert ("User Already Exists");
				        	 }
							 else{
								    var email=$('#Email').val();
									 $.post( "${pageContext.request.contextPath}/RestCheckEmail",
											 {
										     TeamMemberEmail:email
										     },
											 function( data,status) {
										 
										
												 if(data)
							        	 		{
										
							        	 		errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS()}';
							        	 		$('#ErrorMessage').show();
							        	 		isSubmit=false;
							        			// alert ("User Already Exists");
							        	 		}
										 	else{
											
											 
										      var zipcode1=$('#ZipCode').val();
											  //http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
											  $.post( "${pageContext.request.contextPath}/RestCheckZipCode",
													  {
												     TeamMemberZipCode:zipcode1
												      },
												      function( data,status ) {
												
												 		if(data)
									        				 {
												
									        				 	errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_ZIP_CODE()}';
									        					 $('#ErrorMessage').show();
									        				 isSubmit=false;
									        				 }
											 
												 else{
													
													 var zipcode=$('#ZipCode').val();
													 var address1=$('#Addressline1').val();
													 var address2=$('#Addressline2').val();
													 var address3=$('#Addressline3').val();
													 var address4=$('#Addressline4').val();
													 var city=$('#City').val();
													 var Countrycode=$("#Countrycode option:selected").text();
													 var Statecode=$("#Statecode option:selected").text();
													 if(Statecode=='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...'){
														 Statecode="";
													 }
													 var ZipCode=$('#ZipCode').val();
													 
													
													 //http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
													 $.post( "${pageContext.request.contextPath}/RestCheckAddress",
															 {
														 TeamMemberAddress1:address1,
														 TeamMemberAddress2:address2,
														 TeamMemberAddress3:address3,
														 TeamMemberAddress4:address4,
														 TeamMemberCity:city,
														 TeamMemberStateCode:Statecode,
														 TeamMemberCountryCode:Countrycode,
														 TeamMemberZipCode:ZipCode
														 }, function( data,status ) {
														 
														
														 if(data)
											        	 {
														
											        	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_ADDRESS()}';
											        	 $('#ErrorMessage').show();
											        	 isSubmit=false;
											        	 }
														 else{
															 $('#ErrorMessage').hide();
															 isSubmit=true;
															// alert('${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_SAVE_SUCCESSFULLY()}');
															 bootbox.alert('${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_SAVE_SUCCESSFULLY()}',
															            function() {
																 $('#getSetupdate').val(getUTCDateTime());
																 $('#getLastActivitydate').val(getUTCDateTime());
																 $('#Startdate').val(ConvertLocalToUTCCurrentTimeZone($('#Startdate').val()));
																 $('#Lastdate').val(ConvertLocalToUTCCurrentTimeZone($('#Lastdate').val()));															
																 
																 document.myForm.action = '${pageContext.request.contextPath}/Team_member_maintenancenew';
																 document.myForm.submit();
																		});
															 
															
															//return isSubmit;
														 }
													 });//end address validation
												 }
											 });//end zip validate										 
											 
											// $('#ErrorMessage').hide();
										 }
									 });//end email validate	 
								 
							 }
						 });//end team member
						
						
					}

				}
			}
		} else {
			isSubmit = false;
		}
		//document.location.href = '#top';
		window.scrollTo(0,0);
		return isSubmit;
	}

	function isCompanySelected(checkBoxName, errMsgid, errMsg) {

		var errMsgId = document.getElementById(errMsgid);

		var i, chks = document.getElementsByName(checkBoxName), valid = false;
		for (i = 0; i < chks.length; i++) {
			if (chks[i].checked) {
				valid = true;
				break;
			}
		}

		if (valid) {
			errMsgId.innerHTML = '';
			return true;
		} else {
			errMsgId.innerHTML = errMsg;
			return false;
		}
	}

	function isCountrySelected() {

		var errMsgId = document.getElementById('ErrorMessageCountrycode');
		var ContryCode = $("#Countrycode :selected").val();

		if (ContryCode == 'none') {

			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}';
			return false;
		}

		else {
			errMsgId.innerHTML = '';
			return true;
		}

	}

	function isZipCodeValid() {
		var ContryCode = $("#Countrycode :selected").val();
		if (ContryCode == '${cd}') {

			var isBlankZip = isBlankField('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if(isBlankZip){
			var isNumericZip = isNumeric('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if (isNumericZip) {
				//$('#ErrorMessageZipCode').hide();
				isBlankStateCode('ZipCode', 'ErrorMessageZipCode',
		 	           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		 	             + '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				$('#idRequired').hide();
				return true;
			} else {
				$('#ErrorMessageZipCode').show();
				$('#idRequired').show();
				return false;
			}
				}else {
					$('#ErrorMessageZipCode').show();
					$('#idRequired').show();
					return false;
				}
		} else {
			
			var isBlankZip = isBlankField('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if(isBlankZip){
			var isNumericZip = isNumeric('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if (isNumericZip) {
				//$('#ErrorMessageZipCode').hide();
				isBlankStateCode('ZipCode', 'ErrorMessageZipCode',
		 	           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		 	             + '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				$('#idRequired').hide();
				return true;
			} else {
				$('#ErrorMessageZipCode').show();
				$('#idRequired').show();
				return false;
			}
				}else {
					$('#ErrorMessageZipCode').show();
					$('#idRequired').show();
					return false;
				}

	}
	}
	/* check validation for State*/
	function isStateValid() {
		var ContryCode = $("#Countrycode :selected").val();
		if (ContryCode == '${cd}') {

			var isBlankState = isBlankField(
					'Statecode',
					'ErrorMessageState',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
				

			if (isBlankState) {
			//	$('#ErrorMessageState').hide();
				$('#requiredState').hide();
				return true;
			} else {
			//	$('#ErrorMessageState').show();
				$('#requiredState').show();
				return false;
			}

		} else {
		//	$('#ErrorMessageState').hide();
		 isBlankStateCode('State', 'ErrorMessageState',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
			$('#requiredState').hide();
			return true;
		}

	}

	
	function reloadPage(){
	window.parent.location = window.parent.location.href;
	}
	
		
	function clearBtn(){
	//	url="${pageContext.request.contextPath}/GeneralCodeSubListID/Grid/readSublists"
		//window.location.href='Team_member_maintenancenew';
	//	window.location.href = 'Team_member_maintenance_lookup';
		window.history.back();
		var backState = '${backStatus}';
					
		
		/*  if (backState == 'lookup') {

			 window.location.href = 'Team_member_maintenance';
			
		}
		else{
			
			window.location.href = 'Team_member_maintenance_lookup';
		}  */
	}
function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}

	function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'TeamMember',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						//  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
	
	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);	
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>