<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html lang="en" class="no-js">

<style>

.k-grid th.k-header, .k-grid-header {
	white-space: normal !important;
	padding-right: 0px !important;
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
	white-space: normal !important;
}

.k-grid-content {
	position: relative;
	width: 100%;
	overflow: auto;
	overflow-x: auto;
	overflow-y: hidden !important;
	zoom: 1;
}

fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}


.k-icon, .k-state-disabled .k-icon, .k-column-menu .k-sprite {
opacity: .8;
margin-top: 5px;
}



</style>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<body>
<div id="container" style="position: relative" class="loader_div">
			<!-- BEGIN PAGE CONTAINER -->
			<div class="page-container">
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<div class="container">
						<!-- BEGIN PAGE TITLE -->
						<div class="page-title">
							<h1>${objMenuOptionLabel.getLblMenuOptionSearchLookUp()}</h1>
						</div>
						<!-- END PAGE TITLE -->

					</div>
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">

				<div>
				<a id="AddBtn2"	href="${pageContext.request.contextPath}/Menu_Maintenance_Screen"
								class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
								<span class="hidden-480">
								${objMenuOptionLabel.getLblAddnew()}</span>
								</a>
				</div>

						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-46">
							<div class="col-md-12">


								<div class="portlet">
									



<%-- 
										<div class="actions">
											<a id="AddBtn"
												href="${pageContext.request.contextPath}/Menu_Maintenance_Screen"
												class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
												<span class="hidden-480">
													${objMenuOptionLabel.getLblAddnew()}</span>
											</a>



										</div> --%>

										<div class="row">

											<kendo:grid name="SearchLookupGridTenant" id="grid1"
												resizable="true" reorderable="true" sortable="true" dataBound="gridDataBound">

												<kendo:grid-pageable refresh="true" pageSizes="false"
													buttonCount="5">
												</kendo:grid-pageable>
												<kendo:grid-editable mode="inline" confirmation="Message" />
												<kendo:grid-columns>


													<kendo:grid-column
														title="${objMenuOptionLabel.getLblTenantID()}"
														field="tenant" width="12%;" />
													<kendo:grid-column
														title="${objMenuOptionLabel.getLblMenuType()}"
														field="menuType" width="12%;" />
													<kendo:grid-column
														title="${objMenuOptionLabel.getLblMenuOption()}"
														field="menuOption" width="19%;"/>
													<kendo:grid-column
														title="${objMenuOptionLabel.getLblDescription()}"
														field="description50" width="15%;" />

													<kendo:grid-column
														title="${objMenuOptionLabel.getLblApplication()}"
														field="application" width="12%;" />
													<kendo:grid-column
														title="${objMenuOptionLabel.getLblApplicationSub()}"
														field="applicationSub" width="12%;"/>

													<kendo:grid-column title="${objMenuOptionLabel.getLblAction()}" width="18%;">
														<kendo:grid-column-command>

															<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
															<kendo:grid-column-commandItem
																className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
																name="editDetails"
																text="${objMenuOptionLabel.getLblEdit()}">
																<kendo:grid-column-commandItem-click>
																	<script>
                          
                            function editTeammember(e) {
                              

                                e.preventDefault();
       
                               
                                var dataItemEdit = this.dataItem($(e.currentTarget).closest("tr"));
                                var menuOptionVal=dataItemEdit.menuOption;
                                var tenantval=dataItemEdit.tenant;
                               
                              
                              
                              
                            
                                 var url="${pageContext.request.contextPath}/Menu_Maintenance_Screen";
                                //var wnd = $("#details").data("kendoWindow");
                              
                                                 var myform = document.createElement("form");


                                 var tenantFiled = document.createElement("input");
                                 tenantFiled.value = "tenant";
                                 tenantFiled.name = "tenant";
                                 tenantFiled.setAttribute("type", "hidden");
                                
                                 var menuOptionFiled = document.createElement("input");
                                 menuOptionFiled.value = menuOptionVal;
                                 menuOptionFiled.name = "lblMenuOption";
                                 menuOptionFiled.setAttribute("type", "hidden");
                                
                                
                                 myform.action = url;
                                 myform.method = "get";
                                 myform.appendChild(tenantFiled);
                                 myform.appendChild(menuOptionFiled);
                                 document.body.appendChild(myform);
                                 myform.submit();
                                                //alert(res);
                              // window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                            }
                           
                         
                            </script>



																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

															<kendo:grid-column-commandItem
																className="btn btn-sm red filter-cancel fa fa-times"
																name="btnDelete"
																text="${objMenuOptionLabel.getLblDelete()}">

																<kendo:grid-column-commandItem-click>

																	<script>                                          
                                                    function deleteRecords(e) {
                              

                                e.preventDefault();
                                var dataItemGrid = this.dataItem($(e.currentTarget).closest("tr"));
                               
                               
                               var currentStatus=dataItemGrid.menuOption;
                              
                               $.post("${pageContext.request.contextPath}/Check_MenuOption",
                                                        {
                                     MenuOptionValue:currentStatus,
                                                
                                                 }, function( data,status) {
                                                
                                                
                                                 if(!data)
                                          {
                                           
                                                	 bootbox
                                                     .confirm(
                                                       '${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_OPTION()}',
                                                       function(okOrCancel) {

                                                        if(okOrCancel == true)
                                                        {
                                                	 /* 
                                                       if(confirm('${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_OPTION()}')==true){
                                                     */          
                                                              /* var datassfsd=dataItemSss($(e.currentTarget).closest("tr")); */
                                       
                                                              
                                                              var tenantval=dataItemGrid.tenant;
                                                              
                                        var menuOptionVal=dataItemGrid.menuOption;
                                        var url= "${pageContext.request.contextPath}/MenuOption_searchlookup_delete";
                                        
                                                       
                                                        var myform1 = document.createElement("form");

                                         var tenantFiled = document.createElement("input");
                                         tenantFiled.value = tenantval;
                                         tenantFiled.name = "tenant";
                                         tenantFiled.setAttribute("type", "hidden");
                                        
                                         var menuOptionFiled = document.createElement("input");
                                         menuOptionFiled.value = menuOptionVal;
                                         menuOptionFiled.name = "Menu_Option";
                                         menuOptionFiled.setAttribute("type", "hidden");
                                        
                                         myform1.action = url;
                                         myform1.method = "post";
                                       
                                        
                                         myform1.appendChild(tenantFiled);
                                         myform1.appendChild(menuOptionFiled);
                                        
                                        
                                         document.body.appendChild(myform1);
                                         myform1.submit();
                                                       
                                                              
                                                       }else{
                                                              
                                                              
                                                              
                                                              
                                                       }
                                                        });
                                          }
                                                 else{
                                                       
                                                	 bootbox
                                                     .confirm(
                                                       '${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_PROFILE()}',
                                                       function(okOrCancel) {

                                                        if(okOrCancel == true)
                                                        {
                           
                                                        /* if(confirm('${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_PROFILE()}')==true){
                                                         */      
                                                                     var tenantval=dataItemGrid.tenant;
                                                              
                                        var menuOptionVal=dataItemGrid.menuOption;
                                        var url= "${pageContext.request.contextPath}/MenuOption_searchlookup_delete";
                                        
                                                       
                                                        var myform1 = document.createElement("form");

                                         var tenantFiled = document.createElement("input");
                                         tenantFiled.value = tenantval;
                                         tenantFiled.name = "tenant";
                                         tenantFiled.setAttribute("type", "hidden");
                                        
                                         var menuOptionFiled = document.createElement("input");
                                         menuOptionFiled.value = menuOptionVal;
                                         menuOptionFiled.name = "Menu_Option";
                                         menuOptionFiled.setAttribute("type", "hidden");
                                        
                                         myform1.action = url;
                                         myform1.method = "post";
                                       
                                        
                                         myform1.appendChild(tenantFiled);
                                         myform1.appendChild(menuOptionFiled);
                                        
                                        
                                         document.body.appendChild(myform1);
                                         myform1.submit();
                                                       
                                                       
                                                        }      
                                                        });
                                                       
                                                       //return isSubmit;
                                                 }
                                          });
                               
                         
                              
                                                                     }
                                                    </script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

														</kendo:grid-column-command>
													</kendo:grid-column>
												</kendo:grid-columns>



												<kendo:dataSource autoSync="true" pageSize="10">
													<kendo:dataSource-transport>
														<kendo:dataSource-transport-read cache="false"
															url="${pageContext.request.contextPath}/RestFull_SearchLookUp">
														</kendo:dataSource-transport-read>



														<kendo:dataSource-transport-parameterMap>

															<script>
                  function parameterMap(options,type) {
                  
                    return JSON.stringify(options);
                  
                  }
                 
                 
               
               
                  </script>
														</kendo:dataSource-transport-parameterMap>

													</kendo:dataSource-transport>
												</kendo:dataSource>
											</kendo:grid>





											<!--second grid  -->

											<kendo:grid name="SearchLookupGrid" id="grid2"
												resizable="true" reorderable="true" sortable="true" dataBound="gridDataBound">

												<kendo:grid-pageable refresh="true" pageSizes="false"
													buttonCount="5">
												</kendo:grid-pageable>
												<kendo:grid-editable mode="inline" confirmation="Message" />
												<kendo:grid-columns>



													<kendo:grid-column
														title="${objMenuOptionLabel.getLblMenuType()}"
														field="menuType" width="15%;" />
													<kendo:grid-column
														title="${objMenuOptionLabel.getLblMenuOption()}"
														field="menuOption" width="20%;" />
													<kendo:grid-column
														title="${objMenuOptionLabel.getLblDescription()}"
														field="description50" width="17%;" />

													<kendo:grid-column
														title="${objMenuOptionLabel.getLblApplication()}"
														field="application" width="15%;" />
													<kendo:grid-column
														title="${objMenuOptionLabel.getLblApplicationSub()}"
														field="applicationSub" width="15%;" />

													<kendo:grid-column title="${objMenuOptionLabel.getLblAction()}" width="18%;">
														<kendo:grid-column-command>

															<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
															<kendo:grid-column-commandItem
																className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
																name="editDetails"
																text="${objMenuOptionLabel.getLblEdit()}">
																<kendo:grid-column-commandItem-click>
																	<script>
                          
                            function editTeammember(e) {
                              

                                e.preventDefault();
       
                               
                                var dataItemEdit = this.dataItem($(e.currentTarget).closest("tr"));
                                var menuOptionVal=dataItemEdit.menuOption;
                                var tenantval=dataItemEdit.tenant;
                               
                              
                              
                              
                            
                                 var url="${pageContext.request.contextPath}/Menu_Maintenance_Screen";
                                //var wnd = $("#details").data("kendoWindow");
                              
                                                 var myform = document.createElement("form");


                                 var tenantFiled = document.createElement("input");
                                 tenantFiled.value = "tenant";
                                 tenantFiled.name = "tenant";
                                 tenantFiled.setAttribute("type", "hidden");
                                
                                 var menuOptionFiled = document.createElement("input");
                                 menuOptionFiled.value = menuOptionVal;
                                 menuOptionFiled.name = "lblMenuOption";
                                 menuOptionFiled.setAttribute("type", "hidden");
                                
                                
                                 myform.action = url;
                                 myform.method = "get";
                                 myform.appendChild(tenantFiled);
                                 myform.appendChild(menuOptionFiled);
                                 document.body.appendChild(myform);
                                 myform.submit();
                                                //alert(res);
                              // window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                            }
                           
                         
                            </script>



																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

															<kendo:grid-column-commandItem
																className="btn btn-sm red filter-cancel fa fa-times"
																name="btnDelete"
																text="${objMenuOptionLabel.getLblDelete()}">

																<kendo:grid-column-commandItem-click>

																	<script>                                          
                                                    function deleteRecords(e) {
                              

                                e.preventDefault();
                                var dataItemGrid = this.dataItem($(e.currentTarget).closest("tr"));
                               
                               
                               var currentStatus=dataItemGrid.menuOption;
                              
                               $.post("${pageContext.request.contextPath}/Check_MenuOption",
                                                        {
                                     MenuOptionValue:currentStatus,
                                                
                                                 }, function( data,status) {
                                                
                                                
                                                 if(!data)
                                          {
                                                
                                                	 bootbox
                                                     .confirm(
                                                       '${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_OPTION()}',
                                                       function(okOrCancel) {

                                                        if(okOrCancel == true)
                                                        {
                                                      /*  if(confirm('${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_OPTION()}')==true){
                                                        */       
                                                              /* var datassfsd=dataItemSss($(e.currentTarget).closest("tr")); */
                                       
                                                              
                                                              var tenantval=dataItemGrid.tenant;
                                                              
                                        var menuOptionVal=dataItemGrid.menuOption;
                                        var url= "${pageContext.request.contextPath}/MenuOption_searchlookup_delete";
                                        
                                                       
                                                        var myform1 = document.createElement("form");

                                         var tenantFiled = document.createElement("input");
                                         tenantFiled.value = tenantval;
                                         tenantFiled.name = "tenant";
                                         tenantFiled.setAttribute("type", "hidden");
                                        
                                         var menuOptionFiled = document.createElement("input");
                                         menuOptionFiled.value = menuOptionVal;
                                         menuOptionFiled.name = "Menu_Option";
                                         menuOptionFiled.setAttribute("type", "hidden");
                                        
                                         myform1.action = url;
                                         myform1.method = "post";
                                       
                                        
                                         myform1.appendChild(tenantFiled);
                                         myform1.appendChild(menuOptionFiled);
                                        
                                        
                                         document.body.appendChild(myform1);
                                         myform1.submit();
                                                       
                                                              
                                                       }else{
                                                              
                                                              
                                                              
                                                              
                                                       }
                                         
                                          }
                                                       );
                                                       
                                          }
                                                 else{
                                                       
                                                        /* if(confirm('${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_PROFILE()}')==true){
                                                         */     
                                                         bootbox
                                                         .confirm(
                                                           '${objMenuOptionLabel.getLblERR_WHILE_DELETING_MENU_PROFILE()}',
                                                           function(okOrCancel) {

                                                            if(okOrCancel == true)
                                                            {
                                                                     var tenantval=dataItemGrid.tenant;
                                                              
                                        var menuOptionVal=dataItemGrid.menuOption;
                                        var url= "${pageContext.request.contextPath}/MenuOption_searchlookup_delete";
                                        
                                                       
                                                        var myform1 = document.createElement("form");

                                         var tenantFiled = document.createElement("input");
                                         tenantFiled.value = tenantval;
                                         tenantFiled.name = "tenant";
                                         tenantFiled.setAttribute("type", "hidden");
                                        
                                         var menuOptionFiled = document.createElement("input");
                                         menuOptionFiled.value = menuOptionVal;
                                         menuOptionFiled.name = "Menu_Option";
                                         menuOptionFiled.setAttribute("type", "hidden");
                                        
                                         myform1.action = url;
                                         myform1.method = "post";
                                       
                                        
                                         myform1.appendChild(tenantFiled);
                                         myform1.appendChild(menuOptionFiled);
                                        
                                        
                                         document.body.appendChild(myform1);
                                         myform1.submit();
                                                       
                                                       
                                                              
                                                        }else{
                                                              
                                                        }
                                                           });
                                                       
                                                       //return isSubmit;
                                                 }
                                          });
                               
                         
                              
                                                                     }
                                                    </script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

														</kendo:grid-column-command>
													</kendo:grid-column>
												</kendo:grid-columns>



												<kendo:dataSource autoSync="true" pageSize="10">
													<kendo:dataSource-transport>
														<kendo:dataSource-transport-read cache="false"
															url="${pageContext.request.contextPath}/RestFull_SearchLookUp">
														</kendo:dataSource-transport-read>



														<kendo:dataSource-transport-parameterMap>

															<script>
                  function parameterMap(options,type) {
                  
                    return JSON.stringify(options);
                  
                  }
                 
                 
               
               
                  </script>
														</kendo:dataSource-transport-parameterMap>

													</kendo:dataSource-transport>
												</kendo:dataSource>
											</kendo:grid>

									
										

									
</div>
								</div>


							</div>

						</div>


						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
			<!-- END PAGE CONTAINER -->
			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>

<script>
                                                       jQuery(document).ready(function() {

                                                              var tenant='${Tenant_Id}';
                                                              
                                                              if(tenant.toUpperCase() !== "${ObjCommonSession.getJasci_Tenant()}"){
                                                              var grid = $("#grid1").data("kendoGrid");
                                                      		grid.bind("dataBound", function(e) {
                                                      			$("#grid1 tbody tr .k-grid-btnDelete").each(function () {
                                                      		        var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                                                      		 
                                                      		        //Check in the current dataItem if the row is deletable
                                                      		        var currentStatus=currentDataItem.tenant;
                                                      		         if (currentStatus.toUpperCase() =="${ObjCommonSession.getJasci_Tenant()}") {
                                                      		            $(this).remove();
                                                      		        } 
                                                      		    });
                                                      			
                                                      			
                                                      			$("#grid1 tbody tr .k-grid-editDetails").each(
                                            							function() {
                                            								var currentDataItem = $("#grid1").data("kendoGrid").dataItem($(this).closest("tr"));
                                            								//Check in the current dataItem if the row is deletable
                                            								 var currentStatus=currentDataItem.tenant;
                                            								if (currentStatus.toUpperCase() =="${ObjCommonSession.getJasci_Tenant()}") {
                                                              		            $(this).remove();
                                                              		        } 
                                            							});
                                                      		});
                                                      		
                                                      		
                                                              }
                                                              if(tenant.toUpperCase()=="${ObjCommonSession.getJasci_Tenant()}" ){
                                                                     
                                                                     document.getElementById('grid2').style.display='none';
                                                                     
                                                              }else{
                                                              
                                                                document.getElementById('grid1').style.display='none'; 
                                                              }
                                                              
                                                              var status='${objDescOrder}';
                                                              if(status=='UpdateSearch'){
                                                                     var kendoGridData = $("#grid2").data('kendoGrid');
                                                                    var dsSort = [];
                                                                    dsSort.push({
                                                                     field : "lastActivityDate",
                                                                     dir : "desc"
                                                                    });
                                                                    kendoGridData.dataSource.sort(dsSort);
                                                                   
                                                                    var kendoGridData = $("#grid1").data('kendoGrid');
                                                                    var dsSort = [];
                                                                    dsSort.push({
                                                                     field : "lastActivityDate",
                                                                     dir : "desc"
                                                                    });
                                                                   
                                                                    kendoGridData.dataSource.sort(dsSort);
                                                              }
                                                              
                                                              /* var IsInserted=
                                                                */
                                                            
                                                           
                                                               
                                                              setInterval(function () {
                                                                     
                                                                  var h = window.innerHeight;
                                                                  if(window.innerHeight>=900 || window.innerHeight==1004 ){
                                                                     h=h-187;
                                                                  }
                                                                 else{
                                                                     h=h-239;
                                                                  }
                                                                    document.getElementById("page-content").style.minHeight = h+"px";
                                                                 
                                                                     }, 30);
                                                              
                                                             // Metronic.init(); // init metronic core components
                                                            //  Layout.init(); // init current layout
                                                            //  Demo.init(); // init demo features
                                                                //         EcommerceOrders.init();
                                                       });
                                                       
													   	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'MenuOption',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
	  
	  function gridDataBound(e) {
		    var grid = e.sender;
		    if (grid.dataSource.total() > 0) {
		     var colCount = grid.columns.length;
		     kendo.ui.progress(ajaxContainer, false);

		    }
		    else
		     {
		      kendo.ui.progress(ajaxContainer, false);
		     }
		   };
		   
		   var ajaxContainer = $("#container");
		   kendo.ui.progress(ajaxContainer, true);
                                                       </script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>