<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE"%>
<script>
var contextPath='<%=request.getContextPath()%>';
</script>
<style>
#AddBtn {
	margin-left: 92.1%;
	margin-bottom: 1%;
}

fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}

/* .k-grid-content {
	position: relative;
	width: 100%;
	overflow: auto;
	overflow-x: auto;
	overflow-y: hidden !important;
	zoom: 0;
}

.k-grid th.k-header, .k-grid-header {
	
	white-space: normal !important;
	padding-right: 0px !important;
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
	white-space: normal !important;
}
.k-grid .k-button {
margin: 2px .16em !important;
} */
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div id="container" style="position: relative" class="loader_div">
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${viewLabels.getLblTeamMemberSelectionTitle()}</h1>
					</div>
				</div>


				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active">
								${viewLabels.getLblTeamMemberSelectionTitle()}</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">

							<div class="form-body">
								<div class="form-group">
									<label class="col-md-2 control-label"
										style="color:#000000; display:${lblHidden}; width: 100%; font-weight:600">${viewLabels.getLblSelectionPartOfTeamMember()}:
										${TeamMemberNamePart} </label>
										
										<!-- <input type="hidden" id="selec_TeamMember" >
										<input type="hidden" id="selec_Tenant" > -->
										
								</div>
							</div>
							<br />
							<br />
							<br />
							<div class="col-md-6" style="width: 100%;">
								<!-- BEGIN SAMPLE TABLE PORTLET-->


								<div>



									<kendo:grid name="CompanyList" resizable="true" dataBound="gridDataBound"
										reorderable="true" sortable="true">
										<kendo:grid-pageable refresh="true" pageSizes="true"
											buttonCount="5">
										</kendo:grid-pageable>
										<kendo:grid-editable mode="inline" confirmation="Message" />

										<kendo:grid-columns>

											<kendo:grid-column
												title='${viewLabels.getLblSelectionGridName()}'
												field="fullName" width="40%" />
											<kendo:grid-column title="FirstName" field="firstName"
												hidden="true" />
											<kendo:grid-column title="LastName" field="lastName"
												hidden="true" />
											<kendo:grid-column
												title='${viewLabels.getLblSelectionGridTeamMember()}'
												field="teamMember" width="32%" />
											<kendo:grid-column
												title='${viewLabels.getLblSelectionGridStatus()}'
												field="teamMemberStatus" width="10%" />
											<kendo:grid-column
												title='${viewLabels.getLblSelectionGridActions()}'
												width="18%">
												<kendo:grid-column-command>
													<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
													<kendo:grid-column-commandItem
														className="icon-pencil btn btn-sm yellow filter-submit margin-bottom"
														name="editDetails"
														text=' ${viewLabels.getLblSelectionGridEdit()}'>
														<kendo:grid-column-commandItem-click>
															<script>
																function showDetails(
																		e) {

																	e
																			.preventDefault();

																	var dataItem = this
																			.dataItem($(
																					e.currentTarget)
																					.closest(
																							"tr"));

																	var TeamMemberStatus = dataItem.teamMemberStatus;
																	if (TeamMemberStatus === 'Active') {

																		var teamMember = dataItem.teamMember;
																		var tenant = '${viewTenant}';
																		
																		
																		//var res = encodeURI(teamMember);
																		var inputJson='{"TeamMember":"'+teamMember+'","Tenant":"'+tenant+'"}';
																		/* var url = contextPath
																				+ "/AddUpdateTeamMember?TeamMember="
																				+ teamMember
																				+ "&Tenant="
																				+ tenant;
																		 */
																		var url = contextPath
																		+ "/AddUpdateTeamMember"
																		
																		
																		 var myform = document.createElement("form");

										                                 var teamMemberFiled = document.createElement("input");
										                                 teamMemberFiled.value = teamMember;
										                                 teamMemberFiled.name = "TeamMember";
										                                 teamMemberFiled.setAttribute("type", "hidden");
										                                 
										                                 var LastNameField = document.createElement("input");
										                                 LastNameField.setAttribute("type", "hidden");
										                                 LastNameField.value = tenant;
										                                 LastNameField.name = "Tenant";
										                                 
										                                 var LastActivityDateStringH = document.createElement("input");
										                                 LastActivityDateStringH.setAttribute("type", "hidden");
										                                 LastActivityDateStringH.value = getUTCDateTime();
										                                 LastActivityDateStringH.name = "LastActivityDateStringH";
										                                 
										                                 var FullNameString = document.createElement("input");
										                                 FullNameString.setAttribute("type", "hidden");
										                                 FullNameString.value = dataItem.fullName;
										                                 FullNameString.name = "FullName";
										                                 
										                               
										                                 myform.action = url;
										                                 myform.method = "get";
										                                 myform.appendChild(teamMemberFiled);
										                                 myform.appendChild(LastNameField);
										                                 myform.appendChild(LastActivityDateStringH);
										                                 myform.appendChild(FullNameString);
										                                 document.body.appendChild(myform);
										                                 myform.submit();
																		
																		
																	/* 	var elem1 = document.getElementById("selec_TeamMember");
																		elem1.value = teamMember;
																		var elem2 = document.getElementById("selec_Tenant");
																		elem2.value = tenant;
																		 */
																		
																		//var wnd = $("#details").data("kendoWindow");
																		//window.location = url;

																	}

																	else {
																		//alert('${viewLabels.getErrSelectionNotAbleToEdit()}');
																		
																		 bootbox.alert(
												                            '${viewLabels.getErrSelectionNotAbleToEdit()}',
												                            function() {            	
												                            });
																	}
																	// wnd.content(detailsTemplate(dataItem));
																	//wnd.center().open();
																}
															</script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

													<kendo:grid-column-commandItem
														className="fa fa-times btn btn-sm red filter-cancel"
														name="deleteDetails"
														text=' ${viewLabels.getLblSelectionGridDelete()}'>
														<kendo:grid-column-commandItem-click>
															<script>
															function deleteConfirmation(e) {
																e.preventDefault();
																 var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

						      bootbox.confirm('${viewLabels.getErrSelectionConfirmDelete()}',
								function(okOrCancel) {
						    	            			              if(okOrCancel == true) {
						    	            			            	  
																                            			                            	
																								

																								dataItem.seqStatus = 'X';
																								var tenant = '${viewTenant}';
																								var teamMember = dataItem.teamMember;
																								var Company='Abc';
																								var url = contextPath
																										+ "/deleteFromSEQ?Tenant="
																										+ tenant
																										+ "&TeamMember="
																										+ teamMember
																										+ "&Company=Abc";
																								
																								 $.post( "${pageContext.request.contextPath}/deleteFromSEQ",
																										  {Tenant:tenant,
																									 TeamMember:teamMember,
																									 Company:Company
																									 
																										  }, function( data1,status ) {
																											  if (!data1.boolStatus) {

																													if (data1.strStatus === "UserIsInactive") {
																														//alert('${viewLabels.getErrSelectionUserIsAlreadyInActive()}');
																														
																														 bootbox.alert('${viewLabels.getErrSelectionUserIsAlreadyInActive()}',
																																 function() {
																																 
																																 });
																													} else {
																														//alert('${viewLabels.getErrSelectionUserNotExists()}');
																														 bootbox.alert('${viewLabels.getErrSelectionUserNotExists()}',
																																 function() {
																																 
																																 });
																													}
																												} else {

																													//alert('${viewLabels.getErrSelectionStatusUpdated()}');
																													 bootbox.alert('${viewLabels.getErrSelectionStatusUpdated()}',
																															 function() {
																															 
																															 });
																													
																													$("#CompanyList").data("kendoGrid").dataSource.read();
																												}
																											});	
																		}		  
																											  
													 });
																								

										}
															</script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

												</kendo:grid-column-command>
											</kendo:grid-column>



										</kendo:grid-columns>

										<kendo:dataSource autoSync="true" pageSize="10">

											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false" url="${pageContext.request.contextPath}${KendoReadURL}"></kendo:dataSource-transport-read>
												<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/Customers"></kendo:dataSource-transport-read> --%>
												<kendo:dataSource-transport-update
													url="GeneralCodeList/Grid/update" dataType="json"
													type="POST" contentType="application/json" />
												<kendo:dataSource-transport-destroy
													url="GeneralCodeList/Grid/delete" dataType="json"
													type="POST" contentType="application/json">
													<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
												</kendo:dataSource-transport-destroy>


												<kendo:dataSource-transport-parameterMap>
													<script>
														function parameterMap(
																options, type) {

															return JSON
																	.stringify(options);

														}
													</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>
											<kendo:dataSource-schema>
												<kendo:dataSource-schema-model id="intKendoID">
													<kendo:dataSource-schema-model-fields>
														<kendo:dataSource-schema-model-field name="intKendoID">

														</kendo:dataSource-schema-model-field>
														<kendo:dataSource-schema-model-field name="firstName"
															type="string">

														</kendo:dataSource-schema-model-field>
														<kendo:dataSource-schema-model-field name="lastName"
															type="string">

														</kendo:dataSource-schema-model-field>
													</kendo:dataSource-schema-model-fields>
												</kendo:dataSource-schema-model>
											</kendo:dataSource-schema>

										</kendo:dataSource>
									</kendo:grid>
									<div
										style="float: right; position: relative; margin-top: 10px;">

										<button class="btn btn-sm yellow  margin-bottom"
											onclick="sortData();" id="sortBtn"><i class="fa fa-caret-up"></i> ${viewLabels.getLblSelectionSortFirstName()}</button>

									</div>
								</div>

								<!-- END SAMPLE TABLE PORTLET-->
							</div>
							<!--end tabbable-->

							<!--end tabbable-->

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>
		<!-- END PAGE CONTENT -->
</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
<script>
function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
	function sortData() {
		//sortFirstName display:none;

		/* value='${SortName}';
		var kendoGridData=$("#CompanyList").data('kendoGrid');
		var dsSort = [];
		dsSort.push({ field: "firstName", dir: "asc" });
		kendoGridData.dataSource.sort(dsSort);	 */

		//window.location.href = '${pageContext.request.contextPath}/Teammember_Sort_searchlookup/'+value;
		var Button = document.getElementById("sortBtn");
		var text = Button.innerHTML;
		if (text === '<i class=\"fa fa-caret-up\"></i> ${viewLabels.getLblSelectionSortFirstName()}') {

			Button.innerHTML = "<i class=\"fa fa-caret-up\"></i> ${viewLabels.getLblSelectionSortLastName()}";
			var kendoGridData = $("#CompanyList").data('kendoGrid');
			var dsSort = [];
			dsSort.push({
				field : "firstName",
				dir : "asc"
			});
			kendoGridData.dataSource.sort(dsSort);
		} else if (text === '<i class=\"fa fa-caret-up\"></i> ${viewLabels.getLblSelectionSortLastName()}')

		{
			Button.innerHTML = "<i class=\"fa fa-caret-up\"></i> ${viewLabels.getLblSelectionSortFirstName()}";
			var kendoGridData = $("#CompanyList").data('kendoGrid');
			var dsSort = [];
			dsSort.push({
				field : "lastName",
				dir : "asc"
			});
			kendoGridData.dataSource.sort(dsSort);

		}

	}
</script>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>
function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'SecuritySetUp',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }


	jQuery(document).ready(
		
			function() {
				
				setInterval(function () {
					
				    var h = window.innerHeight;
				    if(window.innerHeight>=900 || window.innerHeight==1004 ){
				    	h=h-187;
				    }
				   else{
				    	h=h-239;
				    }
				      document.getElementById("page-content").style.minHeight = h+"px";
				    
					}, 30);

				var grid = $("#CompanyList").data("kendoGrid");
				
				grid.bind("dataBound", function(e) {
					  $("#CompanyList tbody tr .k-grid-deleteDetails").each(
							function() {
								var currentDataItem = $("#CompanyList").data(
										"kendoGrid").dataItem(
										$(this).closest("tr"));

								//Check in the current dataItem if the row is deletable
								var currentStatus = currentDataItem.seqStatus;
								if (currentStatus == 'X'
										|| currentStatus == 'x') {
									$(this).remove();
								}
							});
			
						$("#CompanyList tbody tr .k-grid-editDetails").each(
							function() {
								var currentDataItem = $("#CompanyList").data("kendoGrid").dataItem($(this).closest("tr"));
								//Check in the current dataItem if the row is deletable
								var currentStatus = currentDataItem.teamMemberStatus;
								if (currentStatus != 'Active') {
									$(this).remove();
								}
							});
							
							
				});
				
				
				
				var kendoGridData = $("#CompanyList").data('kendoGrid');
				var dsSort = [];
				dsSort.push({
					field : "lastName",
					dir : "asc"
				});
				kendoGridData.dataSource.sort(dsSort);

				//Layout.init(); // init layout
				//Demo.init(); // init demo(theme settings page)

			});
</script>
</body>
<!-- END BODY -->
</html>