<!-- 
Date Developed  Sep 18 2014
Description It is used to show the list of General Code Identification
Created By Aakash Bishnoi -->




<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">


<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<head>
<style>

.margin-bottom-5-right-allign_menu_main {
margin-bottom: 5px !important;
margin-right: 0.1%;
float: right;
}
.TextAreaErrorMessage{
position: relative !important;
top: -23px !important;
}


.errorMessagesFormat{
position: absolute;
width: 122%;
top:5px;

}

.CustomDatePickettextBox{
margin-left:22.5px !important;
}

@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait)
{
.CustomDatePickettextBox {
    margin-left: 0px !important;
}
.margin-bottom-5-right-allign_menu_main {
margin-bottom: 5px !important;
margin-right: 0.1% !important;
float: right;
} 
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape)
{
.CustomDatePickettextBox {
    margin-left: 22.5px !important;
}
.margin-bottom-5-right-allign_menu_main {
margin-bottom: 5px !important;
margin-right: 0% !important;
float: right;
} 
}
</style>



</head>
<body>

<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${ScreenLabels.getMenuMessage_Menu_Messages_Maintenance()}</h1>
			</div>
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			  <div id="ConstraintMessage" class="note note-danger" style="display:none" >
				 <p  class="error error-Top" id="CombinedErrorMessage" style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;message</p>	
			</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<form:form class="form-horizontal form-row-seperated" action="#" method="post" onsubmit="return isFormSubmit();" name="myform" modelAttribute="ObjectMenuMessages">
						<div class="portlet">
							<input type="hidden" name="Last_Activity_DateH" id="Last_Activity_DateH">
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
											<div class="form-group">
													<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_Start_Date_YYMMDD()}:
													<span class="required">
         										    * </span>
													</label>
													<div class="col-md-3">
													<div class="input-group input-medium date date-picker CustomDatePickettextBox">
														<form:input type="text" path="Start_Date" class="form-control"  
														name="startdate" id="startdate" readonly="true" style="width:241px; background-color:white;" />
														<span id="ErrorMessagestartdate" class="error errorMessagesFormat"></span>
												</div>
													</div>
											  </div>
												<div class="form-group">
													<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_End_Date_YYMMDD()}:
													<span class="required">
         										    * </span>
													</label>
													<div class="col-md-3">
													<div class="input-group input-medium date date-picker CustomDatePickettextBox">
														<form:input type="text" class="form-control" 
														path="End_Date"  name="enddate" id="enddate" readonly="true" style="width:241px; background-color:white;" />
														<span id="ErrorMessageenddate" class="error errorMessagesFormat"></span>
												</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_Ticket_Tape_Message()}: 
													</label>
													
													<div class="col-md-10">
													<form:textarea type="text" class="form-controlwidth" maxlength="500" path="Ticket_Tape_Message" name="TicketTapeMessage" id="TicketTapeMessage" />
													<span id="ErrorMessageTicketTapeMessage" class="error TextAreaErrorMessage"></span>
													</div>
												</div>
													<div class="form-group">
													<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_Message()}:
													</label>
													
													<div class="col-md-10">
															<form:textarea type="text" class="form-controlwidth" maxlength="500" path="Message1" name="Message" id="Message"/>
															<span id="ErrorMessageMessage" class="error TextAreaErrorMessage"></span>
													</div>
												</div>
													
												
												
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
						<div id="container" style="position: relative" class="loader_div">
						<div class="col-md-12 semi-grid">
					<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<!--<div class="caption">
								Company: JASCI
							</div>-->
							<div class="actions">
								
								
									
									
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-container">
								<div class="table-actions-wrapper">
									<span>
									</span>
									
									
								</div>
									
									<kendo:grid name="menumessagecompanies"  id="menumessagecompanies" selectable="multiple,row" resizable="true"
										reorderable="true" sortable="true" dataBound="gridDataBound">
										<kendo:grid-editable mode="inline" confirmation="true" />

										<kendo:grid-columns>
										 <kendo:grid-column template="<input type='checkbox'  class='checkboxId' />" width="11%">
                   							</kendo:grid-column>
                  							<kendo:grid-column
												title="${ScreenLabels.getMenuMessage_Company_ID()}"
												field="id.company"/>
											<kendo:grid-column
												title="${ScreenLabels.getMenuMessage_Company_Name()}"
												field="name50"/>
										</kendo:grid-columns>

										<kendo:dataSource autoSync="true">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/TeamMemberCompanyList"></kendo:dataSource-transport-read>

												<kendo:dataSource-transport-parameterMap>
											<script>
												function parameterMap(options,type) { 
													
														return JSON.stringify(options);
													
												}
											</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>




										</kendo:dataSource>
									</kendo:grid>
									
							</div>
							
						</div>
						
					</div>
					<!-- End: life time stats -->
					
												<div class="margin-bottom-5-right-allign_menu_main">
		
											<button class="btn btn-sm yellow filter-submit margin-bottom" id="saveBtn"><i class="fa fa-check"></i>${ScreenLabels.getMenuMessage_Save_Update()}</button>
										          <button class="btn btn-sm red filter-cancel" type="button" onclick="return resetButton();"><i class="fa fa-times"></i> ${ScreenLabels.getMenuMessage_Reset()}</button>
												<button class="btn btn-sm red filter-cancel" type="button" onclick="return cancelButton();"><i class="fa fa-times"></i> ${ScreenLabels.getMenuMessage_Cancel()}</button></div>
												
												
				</div>
				</div>
					</form:form>
				</div>
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	
</div>
</body>
<!-- END BODY -->
</html>
</tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />


<!-- END PAGE LEVEL SCRIPTS -->
<script>
     
function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
   
 jQuery(document).ready(function() {    
	
	 $(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	  
	 setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
   });   
    </script>
    <script>
   
    $(function() {
   	 var dates = $("#startdate, #enddate").datepicker(
   			{
   				minDate : '0',
   				dateFormat : 'yy-mm-dd',
   				onSelect : function(selectedDate) {
   					var option = this.id == "startdate" ? "minDate"
   							: "maxDate", instance = $(this).data(
   							"datepicker"), date = $.datepicker.parseDate(
   							instance.settings.dateFormat
   									|| $.datepicker._defaults.dateFormat,
   							selectedDate, instance.settings);
   					date.setDate(date.getDate());
   					dates.not(this).datepicker("option", option, date);
   				}
   			});
    });
    
    
    </script>
 <script type="text/javascript">

  /*   $(function () {
    $('#enddate').datepicker({ 
	minDate: 1,
	dateFormat: 'yy-mm-dd' 
	});
	 $('#startdate').datepicker({ 
	minDate:0,
	dateFormat: 'yy-mm-dd' 
	});
    });
 */
 
 
 function cancelButton(){
	  window.location="${pageContext.request.contextPath}/Menu_messages_search_lookup";
	  }
	    function resetButton(){
	     window.parent.location = window.parent.location.href; 
	     }
	    
    var itemss =[];
  
      $('#menumessagecompanies').on("click", ".checkboxId", function (e) 

                     {
                   	   
                   	   	var checked = $(this).is(':checked');
                        var rowIndex = $(this).parent().index();
                   	    var cellIndex = $(this).parent().parent().index();
                    	var grid = $("#menumessagecompanies").data("kendoGrid");
                        var dataItem = grid.dataItem(grid.tbody.find('tr:eq(' + cellIndex + ')'));
								
                           if (checked) {
								
                               itemss.push(dataItem.id.company);
												
                             } 
                           else if(!checked)
                           {
							
								Array.prototype.remove = function(v) { this.splice(this.indexOf(v) == -1 ? this.length : this.indexOf(v), 1); }
                               itemss.remove(dataItem.id.company);
								
                           } 
                          
                           
                     }
    
    ); 
      var IsSubmit=false;  
			
  function isFormSubmit(){
	 
		 var ValidStartDate=isBlankField('startdate', 'ErrorMessagestartdate',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ '${ScreenLabels.getMenuMessage_Mandatory_Fields()}');
		
		var ValidEndDate=isBlankField('enddate', 'ErrorMessageenddate',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ '${ScreenLabels.getMenuMessage_Mandatory_Fields()}');
		
		var isStartDateLessThenEnd = false;
		var isLastDateLessThenEnd = false;	
		
		
		if(ValidStartDate && ValidEndDate){
			
				isStartDateLessThenEnd = isBlankField('startdate',
						'ErrorMessagestartdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ScreenLabels.getMenuMessage_ERR_Start_Date()}');

				isLastDateLessThenEnd = testDate('startdate','enddate',
						'ErrorMessageenddate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ScreenLabels.getMenuMessage_ERR_End_Date()}');
				
			}
			
			
			
			var ValueTicketTapeMessage=$("#TicketTapeMessage").val().trim();
			var ValueMessage=$("#Message").val().trim();
			var ValidTicketTapeMessage=false;
			var ValidMessage=false;
			var ValidCompany=false;
			
			if(ValueTicketTapeMessage!=''){
			ValidTicketTapeMessage = true;
			
			}
			
			if(ValueMessage!=''){
			ValidMessage = true;
			
			}
			
			if(itemss!=''){
			var ValidCompany=true;
			}
			
			
			/* This is used to check all field are filled */
			
			if(ValidStartDate && ValidEndDate && ValidCompany && isLastDateLessThenEnd && ( ValidTicketTapeMessage || ValidMessage)){
				 $('#Last_Activity_DateH').val(getUTCDateTime());
				 $('#startdate').val(ConvertLocalToUTCCurrentTimeZone($('#startdate').val()));
				 $('#enddate').val(ConvertLocalToUTCCurrentTimeZone($('#enddate').val()));		
					var myJsonString = JSON.stringify(itemss);			
				
					  $.post( "${pageContext.request.contextPath}/CompanyListCheckController",
							  {
						 
						  CompanyList:myJsonString}, function( data,status ) 
							  {
								  
							  document.getElementById('ConstraintMessage').style.display='none'; 
								
								 bootbox.alert('${ScreenLabels.getMenuMessage_Confirm_Save()}',function(){
									 document.myform.action="Menu_messages_maintenance_insert";								 
										
										
										IsSubmit=true;
										
										document.myform.submit();
										$('#startdate').val(ConvertUTCToLocalTimeZone($('#startdate').val()));
										 $('#enddate').val(ConvertUTCToLocalTimeZone($('#enddate').val()));	 
								 });		
										
 							  });
			}else{
				
					/* it is check TicketTypeMessage and Message field are blank */
					if(!ValidTicketTapeMessage && !ValidMessage){
					
						 document.getElementById('ConstraintMessage').style.display='block'; 
						 document.getElementById("CombinedErrorMessage").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getMenuMessage_ERR_Atleast_One()}';
						 IsSubmit = false;
					}
					else 
					{
					/* It is used to check companies checkbox are selected or not */
						if(!ValidCompany){
						
							document.getElementById('ConstraintMessage').style.display='block'; 
							 document.getElementById("CombinedErrorMessage").innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ScreenLabels.getMenuMessage_ERR_SELECT_ONE_COMPANY()}';
							 IsSubmit = false;
						}
						else{
							document.getElementById('ConstraintMessage').style.display='none'; 
						}
						
					}
			}
	
			
		
		return IsSubmit;
		
	}
  function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'MenuMessages',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>

<!-- END JAVASCRIPTS -->

