<!-- 
Date Developed  Sep 18 2014
Description It is used to show the list of General Code Identification
Created By Aakash Bishnoi -->




<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">


<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<body>

<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Menu Messages Maintenance</h1>
			</div>
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					message maintenance
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<form:form class="form-horizontal form-row-seperated" action="#" method="post" onsubmit="return isFormSubmit();" name="myform" modelAttribute="ObjectMenuMessages">
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
											<div class="form-group">
													<label class="col-md-2 control-label">Start Date:
													</label>
													<div class="col-md-3">
													<div class="input-group input-medium date date-picker">
														<form:input type="text" path="Start_Date" class="form-control"  name="startdate" id="startdate"/>
														<!--<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>-->
												</div>
													</div>
											  </div>
												<div class="form-group">
													<label class="col-md-2 control-label">End Date:
													</label>
													<div class="col-md-3">
													<div class="input-group input-medium date date-picker">
														<form:input type="text" class="form-control" path="End_Date"  name="enddate" id="enddate"/>
													<!--	<span class="input-group-btn">
												<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
												</span>-->
												</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">Ticket Tape Message: 
													</label>
													
													<div class="col-md-10">
													<form:input type="text" class="form-controlwidth" path="Ticket_Tape_Message" name="TicketTapeMessage" id="TicketTapeMessage" />
													</div>
												</div>
													<div class="form-group">
													<label class="col-md-2 control-label">Message1: 
													</label>
													
													<div class="col-md-10">
															<form:input type="text" class="form-controlwidth" path="Message1" name="Message1" id="Message1"/>
													</div>
												</div>
													
												
												
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
					
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<!--<div class="caption">
								Company: JASCI
							</div>-->
							<div class="actions">
								
								
									
									
							</div>
						</div>
						<div class="col-md-12" style="margin-left: -19px; width:62%">
						<div class="portlet-body">
							<div class="table-container">
								<div class="table-actions-wrapper">
									<span>
									</span>
									
									
								</div>
									<div class="row" style="width:102.8%">
									<kendo:grid name="menumessagecompanies" resizable="true"
										reorderable="true" sortable="true">
										<kendo:grid-editable mode="inline" confirmation="true" />

										<kendo:grid-columns>
										 <kendo:grid-column template="<input type='checkbox'  class='checkboxId' />" width="25">
                   							</kendo:grid-column>
                  
											<kendo:grid-column
												title="Company Name"
												field="CompanyID"/>
										</kendo:grid-columns>

										<kendo:dataSource autoSync="true">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/TeamMemberCompanyList"></kendo:dataSource-transport-read>

												<kendo:dataSource-transport-parameterMap>
											<script>
												function parameterMap(options,type) { 
													
														return JSON.stringify(options);
													
												}
											</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>




										</kendo:dataSource>
									</kendo:grid>
									</div>
								<div class="margin-bottom-5-right-allign_menu_main">

											<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>SAVE/UPDATE</button>
										   
												<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Cancel</button></div>
												
							</div>
							
						</div>
						</div>
					</div>
					<!-- End: life time stats -->
					
				
					</form:form>
				</div>
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	
</div>
</body>
<!-- END BODY -->
</html>
</tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />


<!-- END PAGE LEVEL SCRIPTS -->
 <script type="text/javascript">
    $(function () {
    $('#enddate').datepicker({ 
	minDate: 1,
	dateFormat: 'yy-mm-dd' 
	});
	 $('#startdate').datepicker({ 
	minDate:0,
	dateFormat: 'yy-mm-dd' 
	});
    });

</script>
<script>
function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'MenuMessages',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }

        jQuery(document).ready(function() {       
           
Layout.init(); // init current layout

        });   
    </script>
<!-- END JAVASCRIPTS -->

