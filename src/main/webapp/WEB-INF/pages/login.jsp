<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<!--Company : NGI VENTURES PVT. LTD. 
//Created By: Suraj jena
//Created on:  10/10/2014
//Modify By: Suraj jena     
//Modify on:10/10/2014
//Purpuse : Code Behind of Login Screen.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<link href="<c:url value="/resourcesValidate/css/error-style.css"/>"  rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/glass-style.css"/>"  rel="stylesheet" type="text/css">
<style>
 .checkboxset_tab3{
margin-top:0px !important;

}
.checkboxset_tab4{
margin-top:1px !important;
} 
.error-note{

height: 49px;
}
.error-note1{

height: 68px;
}
.loginerroruppermessage{
  margin-top: -2.8% !important;
  position: absolute !important;
  width: 91% !important;
}

@media (max-width: 1280px) {
.loginerroruppermessage{
  margin-top: -2.8% !important;
  position: absolute !important;
  width: 85% !important;
}

}

</style>
<meta charset="utf-8" />
<title>JASCI | Login Options - Login Form 1</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/uniform/css/uniform.default.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link
	href="<c:url value="/resources/assets/global/plugins/select2/select2.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/admin/pages/css/login.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link
	href="<c:url value="/resources/assets/global/css/components.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/assets/global/css/plugins.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/admin/layout/css/layout.css"/>"
	rel="stylesheet" type="text/css" />
<link id="style_color"
	href="<c:url value="/resources/assets/admin/layout/css/themes/default.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/admin/layout/css/custom.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<link href="<c:url value="/resourcesValidate/css/error-style.css"/>"  rel="stylesheet" type="text/css">
<%
	Cookie[] cookies=request.getCookies();
String UserId="",password="";
if (cookies != null) {
     for (Cookie cookie : cookies) {
       if (cookie.getName().equals("cookieLoginUserId")) {
    	   UserId=cookie.getValue();
       }
     
    }
}
%>
<script>
	window.onload = function() {
		/*  var password = $("#password").val; */
		$("#password").onpaste = function(e) {
			e.preventDefault();
		}
		$("#password").oncopy = function(e) {
			e.preventDefault();
		}
	}

	function hideMSG() {
		$("#ErrorMessage").hide();
		$("#ErrorMessage2").hide();
		$("#ErrorMessage3").hide();
		$("#ErrorMessage4").hide();
		$("#ErrorMessage").remove();
	}

	function isBlankUserId() {
		var x = document.forms["loginForm"]["UserId"].value;
		if (x == '') {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").show();
			$("#ErrorMessage4").hide();
			return false;
		} else {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").hide();
			return true;
		}
	}
	function isBlankPassword() {
		var x = document.forms["loginForm"]["password"].value;
		if (x == '') {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").show();

			return false;
		} else {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").hide();
			return true;
		}
	}

	function isBlank() {

		var UserResult = isBlankUserId();
		var PasswordResult = isBlankPassword();

		if (UserResult && PasswordResult) {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").hide();
			return true;

		} else if (!UserResult && !PasswordResult) {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").show();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").hide();
			return false;
		}

		else if (!UserResult) {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").show();
			$("#ErrorMessage4").hide();
			return false;

		} else if (!PasswordResult) {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").hide();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").show();
			return false;
		}

		else {
			$("#ErrorMessage").hide();
			$("#ErrorMessage2").show();
			$("#ErrorMessage3").hide();
			$("#ErrorMessage4").hide();
			return false;
		}

	}
</script>

</head>
<body class="login"
	style="display: flex; flex-direction: column; min-height: 100%;">
	<!-- BEGIN LOGO -->
	<div class="logo">
		<!-- <a href="index.html"> -->
		<img class="logojasci" style="margin-left: 390px;"
			src="<c:url value="/resources/assets/admin/layout3/img/Jasci_02.png"/>"
			alt="" />
		<!-- 	</a> -->
	<%-- 	<div style="margin-top: -77px; margin-left: 390px;">
			<img style="height: 68px;" src="<c:url value="${CompanyLogo}"/>"
				alt="" />
		</div> --%>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="menu-toggler sidebar-toggler"></div>
	<!-- END SIDEBAR TOGGLER BUTTON -->
	<!-- BEGIN LOGIN -->


	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<div class="messasgebox">
			<!-- <div class="messageboard">MessageBoard </div> -->

			<div class="messageboard">${ObjLoginBe.getStrMessageBoard()}</div>


			<div class="dividermessagebox"></div>
			<div class="messagetext">
				<marquee direction="up" height="275px" loop="-1" scrollamount="3"
					style="width: 300px; text-align: justify; !important">
					<!-- <font face="Arial, Helvetica, sans-serif" size="2" color="#ffffff">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</font><div class="horizontalrow"> -->
			</div>
			<br />
			<c:forEach var="Value" items="${ObjMenuMessages}">
				<div>
					<font face="Arial, Helvetica, sans-serif" size="2" color="#ffffff">

						${Value.getMessage()}</font>
				</div>
				<hr>
			</c:forEach>
		</div>

		</marquee>
	</div>

	</div>
	<div class="dividerloginmessage"></div>

	<form:form name="loginForm" class="login-form" action="loginForm"
		onsubmit="return isBlank();" commandName="validate">

			<input type="hidden" name="ClientDeviceTimeZone" id="ClientDeviceTimeZone">
		<h3 class="form-title">${ObjLoginBe.getStrLoginToAccount()}</h3>
		<!-- <div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<div id="errorDiv" style="visbility:false;">Enter correct username and password</div>
			<span>
			Please enter Username and Password. </span>
			 
		</div>
		 -->


		<c:if test="${message}">
		<c:if test="${fn:length(ErrorMessage) gt 45}">
		<div id="ErrorMessage" class="note error-note1 note-danger ">
				<p class="error margin-left-7pix loginerroruppermessage">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ErrorMessage}</p>
			</div>
		</c:if>
		<c:if test="${fn:length(ErrorMessage) lt 45}">
		<div id="ErrorMessage" class="note error-note note-danger ">
				<p class="error margin-left-7pix loginerroruppermessage">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ErrorMessage}</p>
			</div>
		</c:if>


		</c:if>




		<div id="ErrorMessage2" class="note note-danger"
			style="display: none;">
			<p class="error margin-left-7pix loginerroruppermessage">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter UserId and Password.</p>
		</div>
		<div id="ErrorMessage3" class="note note-danger"
			style="display: none;">
			<p class="error margin-left-7pix loginerroruppermessage">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter UserId.</p>
		</div>
		<div id="ErrorMessage4" class="note note-danger"
			style="display: none;">
			<p class="error margin-left-7pix loginerroruppermessage">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter Password.</p>
		</div>



		<%--  ${ObjLoginBe.getStrErrorMessageInvalidUser()} --%>

<input type="hidden" name="LastActivityDateStringH" id="LastActivityDateStringH"/>


		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">${ObjLoginBe.getStrUserId()}</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input
					class="form-control placeholder-no-fix" type="text"
					reautocomplete="off" placeholder="${ObjLoginBe.getStrUserId()}"
					name="UserId" id="UserId" value="<%= UserId %>" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">${ObjLoginBe.getStrPassword()}</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i> <input
					class="form-control placeholder-no-fix" type="password"
					autocomplete="off" placeholder="${ObjLoginBe.getStrPassword()}"
					name="password" id="password" value="<%= password %>"
					oncopy="return false;" onpaste="return false;" />
			</div>
		</div>
		<div class="form-actions">
			<label class="checkbox"> <input
				type="checkbox" id="chechboxid" name="remember" class="margin-0-px" /> ${ObjLoginBe.getStrRememberMe()}
			</label>
			<button type="submit" onclick="hideMSG();HideErrorDiv();"
				class="btn green pull-right">
				${ObjLoginBe.getStrLoginButton()} <i
					class="m-icon-swapright m-icon-white"></i>
			</button>


		</div>
		<div class="forget-password">
			<h4>${ObjLoginBe.getStrForgotPassword()}</h4>
			<p>
				${ObjLoginBe.getStrNoWorriesClick()}<a href="forgetpassword">
					${ObjLoginBe.getStrHereLink()} </a> ${ObjLoginBe.getStrResetPassword()}
			</p>
		</div>
		<!-- <div class="create-account">
			<p>
				 Don't have an account yet ?&nbsp; 
				 <a href="login.html">
				Create an account </a>
			</p>
		</div>  -->

	</form:form>

	</div>
	<!-- END LOGIN -->
	<!-- BEGIN TICKET TAP BAR -->

	<div class="copyright" style="position: fixed; bottom: 0;">
		<div class="tickettape">
			<marquee class="textslide" loop="-1" scrollamount="4">
				<c:forEach var="Value" items="${ObjMenuMessages}">
			   ${Value.getTicket_Tape_Message()} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			  
			</c:forEach>
			</marquee>
		</div>
	</div>

	<%-- <div class="tickettape">
		<marquee class="textslide" loop="-1" scrollamount="4">
			<c:forEach var="Value" items="${ObjMenuMessages}">
			   ${Value.getTicketTapeMessage()}
			  
			</c:forEach>
		</marquee>
	</div> --%>
	<!-- END TICKET TAP BAR -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-1.11.0.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-migrate-1.2.1.min.js"/>"
		type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery.blockui.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery.cokie.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/uniform/jquery.uniform.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"/>"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/assets/global/plugins/select2/select2.min.js"/>"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script
		src="<c:url value="/resources/assets/global/scripts/metronic.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/layout/scripts/layout.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/layout/scripts/quick-sidebar.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/layout/scripts/demo.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/pages/scripts/login.js"/>"
		type="text/javascript"></script>
		<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	
	<script src="<c:url value="/resourcesValidate/js/jstz.min.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/moment.js"/>" type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
	/* function setError(){
		if("${ErrorMessage}".lenght>100){
		document.getElementById('ErrorMessage').style.Hieght ="68px";
		}
	} */
	
		function HideErrorDiv() {
	<%request.setAttribute("message",false);%>
		}

		
		
		var boolval = false;

		jQuery(document).ready(function() {
			//getClientTimeZone();
			$('#LastActivityDateStringH').val(getUTCDateTime());
			
			var offset = new Date().getTimezoneOffset();
			var tz = jstz.determine(); // Determines the time zone of the browser client
            $('#ClientDeviceTimeZone').val(tz.name());
            var isTrue = '${message}';
			if (isTrue) {
				$("#ErrorMessage").show();
				//alert("if");
			} else {
				///alert("else");
				$("#ErrorMessage").hide();
			}

			var ScreenType='FULLSCREEN';
			var agent = navigator.userAgent;      
		    var isWebkit = (agent.indexOf("AppleWebKit") > 0);      
		    var isIPad = (agent.indexOf("iPad") > 0);      
		    var isIOS = (agent.indexOf("iPhone") > 0 || agent.indexOf("iPod") > 0);     
		    var isAndroid = (agent.indexOf("Android")  > 0);     
		    var isNewBlackBerry = (agent.indexOf("AppleWebKit") > 0 && agent.indexOf("BlackBerry") > 0);     
		    var isWebOS = (agent.indexOf("webOS") > 0);      
		    var isWindowsMobile = (agent.indexOf("IEMobile") > 0);     
		    var isSmallScreen = (screen.width < 767 || (isAndroid && screen.width < 1000));     
		    var isUnknownMobile = (isWebkit && isSmallScreen);     
		    var isMobile = (isIOS || isAndroid || isNewBlackBerry || isWebOS || isWindowsMobile || isUnknownMobile);     
		    var isTablet = (isIPad || (isMobile && !isSmallScreen));  
		    var isGLASS = (agent.indexOf("X6") > 0 || agent.indexOf("X7") > 0);
		    if(isGLASS){
		        ScreenType="GLASS";
		        
		       } 
		    else if( (isMobile && isSmallScreen)||isTablet)
		        {
		        ScreenType="MOBILE";
		        
		       
			        if(screen.width<700)
			    	{	   
			    		 $("#chechboxid").addClass("checkboxset_tab3"); 
			    	}
			    	//this is tab4 css and condition
			    	else if(screen.width>750)
			    	{	    
			    		$("#chechboxid").addClass("checkboxset_tab4"); 
			    		
			    	}
		       }
		       
		       else{
		        ScreenType="FULLSCREEN";
		        //alert("FULLSCREEN");
		        
		       }
		 
		    
		    $.post( "${pageContext.request.contextPath}/SetDeviceType",
					  {DeviceType:ScreenType
				 
					  }, function( data1,status ) {
						  						  
					  });
			
			//Metronic.init(); // init metronic core components
			Layout.init(); // init current layout
			QuickSidebar.init(); // init quick sidebar
			//Demo.init(); // init demo features
			//Login.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>