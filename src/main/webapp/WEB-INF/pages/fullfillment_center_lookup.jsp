<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
<head>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>" type="text/javascript"></script>
</head>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<div class="page-container">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${ViewLabels.getLbl_Fullfillment_Centers_Lookup()}</h1>
			</div>
			</div>
			</div>
		
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id = "page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<div class="row margin-top-10">
							<div id="ErrorMessage" class="note note-danger"
								style="display: none;">
								<p class="error error-Top"   id="Perror"  style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${PageRedirectLoadMessage}</p>
							</div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<form name="myForm" class="form-horizontal form-row-seperated" action="#" onsubmit="return isformSubmit();" method="GET">
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
												
												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getLbl_Fulfillment_Center()}:
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="FufffillmentID" id="FufffillmentID" placeholder="">
													<i class="fa fa-search" id="searchFufffillmentID" style="color:rgba(68, 77, 88, 1); cursor:pointer;" onclick="actionForm('FullfillmentCenterLookupFormSubmit','searchFullfillmentID');"></i>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getLbl_Part_of_the_Fulfillment_Center_Name()}:
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" name="PartOfFullfillMentCenterName" id="PartOfFullfillMentCenterName"  placeholder="">
													<i class="fa fa-search"  id="searchPartOfFullfillMentCenterName"style="color:rgba(68, 77, 88, 1); cursor:pointer;" onclick="actionForm('FullfillmentCenterLookupFormSubmit','searchFullfillmentCenterName');"></i>
													</div>
												</div>
																									
												<div class="margin-bottom-5-right-allign_info_app_icon_main">
											<button class="btn btn-sm yellow filter-submit margin-bottom" onclick="actionForm('FullfillmentCenterLookupNewRecordFormSubmit','newRecord');"><i class="fa fa-plus"></i> ${ViewLabels.getLbl_New()}</button>
										    <button class="btn btn-sm yellow filter-submit margin-bottom" id="btnDispID" onclick="actionForm('FullfillmentCenterLookupDisplayAllFormSubmit','displayAll');"><i class="fa fa-check"></i> ${ViewLabels.getLbl_Display_All()}</button>
										   <!--   <button class="btn btn-sm yellow filter-submit margin-bottom" onclick="actionForm('test','test');"><i class="fa fa-times"></i> test</button> -->
											</div>
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		
	
	
	
	
				<!-- END PAGE CONTENT -->
				

</div>

			</div>
			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>



<!-- END PAGE LEVEL SCRIPTS -->


<!-- END BODY -->


<script>
	 jQuery(document).ready(function() {

		var value = '${PageRedirectLoadMessage}';

		/* var value1=$('#Perror').text().trim();
		
		if(value1===value)
			{
			$('#ErrorMessage').hide();
			}
		else{ */
		if (value.length > 5) {
			$('#ErrorMessage').show();
		} else {
			$('#ErrorMessage').hide();
		}
		//}

		//Metronic.init(); // init metronic core componets
		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)
		//Index.init(); // init index page
		//Tasks.initDashboardWidget(); // init tash dashboard widget

	}); 

	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}
	

	function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}
	
function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'fulfillment',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						 // alert('No help found yet');
						  }
					  
					  
				  });
		
	}

	function actionForm(url, action) {
		
			if(action=='searchFullfillmentID')
			{
				/* var elem1 = document.getElementById("PartOfFullfillment");
				elem1.value = ""; */
				var tenantvalue='${ObjCommonSession.getTenant()}' ;
				var elem1Value = document.getElementById("FufffillmentID").value;
				var elem2 = document.getElementById("PartOfFullfillMentCenterName");
				elem2.value = "";
				var varFufffillmentID = isBlankField('FufffillmentID', 'Perror',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ViewLabels.getERR_Please_enter_Fulfillment_center_ID()}');
				
				if(varFufffillmentID)
					{
					//alert("All is well");
					 $.post( "${pageContext.request.contextPath}/SearchFullfillment",
					  {Fullfillment:elem1Value,
						 IsSearchByFullfillmentID:1,
						 IsSearchByPartOfFullfillMentCenterName:0,
						 IsSearchByPartOfFullfillment:0,
						 Tenant:tenantvalue
					}, function( data1,status ) {
						 // alert(data1);
						  
						  if(data1.length>0)
							  {
							  document.myForm.action = url;
								document.myForm.submit();
								$('#ErrorMessage').hide();
								isSubmit = true;
							  }
						  else
							  {
							  $('#ErrorMessage').show();
							  setErrorMessage('Perror','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ViewLabels.getERR_Invalid_Fulfillment_Center()}');
							  }
			});
					
					
					
					
					}
				else
					{
					
					$('#ErrorMessage').show();
					isSubmit=false;
					}
			}
			else if(action=='searchFullfillmentCenterName')
			{
				
				var tenantvalue='${ObjCommonSession.getTenant()}' ;
				var elem1 = document.getElementById("FufffillmentID");//FufffillmentID
				elem1.value = "";
				/* var elem2 = document.getElementById("PartOfFullfillment");//PartOfFullfillment
				elem2.value = ""; */
				
				 var elem2Value = document.getElementById("PartOfFullfillMentCenterName").value;//PartOfFullfillment
				var varFufffillmentID = isBlankField('PartOfFullfillMentCenterName', 'Perror',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ViewLabels.getERR_Please_enter_Part_of_Fulfillment_Center_Name()}');
				
				if(varFufffillmentID)
					{
					
					
					 $.post( "${pageContext.request.contextPath}/SearchFullfillment",
							  {Fullfillment:elem2Value,
								 IsSearchByFullfillmentID:0,
								 IsSearchByPartOfFullfillMentCenterName:1,
								 IsSearchByPartOfFullfillment:0,
								 Tenant:tenantvalue
							}, function( data1,status ) {
								//  alert(data1);
								  
								  if(data1.length>0)
									  {
									  document.myForm.action = url;
										document.myForm.submit();
										$('#ErrorMessage').hide();
										isSubmit = true;
									  }
								  else{
									  $('#ErrorMessage').show();
									  setErrorMessage('Perror','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ViewLabels.getERR_Invalid_Part_of_the_fulfillment()}');
									  
								  }
					});
					
					
					}
				else
					{
					
					$('#ErrorMessage').show();
					isSubmit=false;
					}
			}
		
			else if(action=='searchPartOfFullfillment')
			{
				var elem1 = document.getElementById("FufffillmentID");//FufffillmentID
				elem1.value = "";
				var elem2 = document.getElementById("PartOfFullfillMentCenterName");//searchPartOfFullfillment
				elem2.value = "";
				var varFufffillmentID = isBlankField('PartOfFullfillment', 'Perror',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ViewLabels.getERR_Please_enter_Part_of_Fulfillment_Center_Name()}');
				
				if(varFufffillmentID)
					{
					document.myForm.action = url;
					document.myForm.submit();
					$('#ErrorMessage').hide();
					isSubmit = true;
					
					}
				else
					{
					
					$('#ErrorMessage').show();
					isSubmit=false;
					}
			}
			else if(action=='displayAll')
			{
				   document.myForm.action = url;
					document.myForm.submit();
					$('#ErrorMessage').hide();
					isSubmit = true;
					
				
			}
			else if(action=='newRecord')
			{
				
					document.myForm.action = url;
					//url='${pageContext.request.contextPath}/'+url;
					
					//window.location = url;
					//FullfillmentCenterMaintenance
					document.myForm.submit();
					$('#ErrorMessage').hide();
					isSubmit = true;
					
					
			}
			else if(action==='test')
				{
				
				
				var sample='{"results":[{"States":["CT", "DE", "MA", "NJ", "NY", "NC", "PA", "WA", "WV", "CA", "DC", "VA"],"max_heartdisease_percnt":["4.41", "5.95", "5.8", "6.95", "14.44", "15.7", "13.34", "7.6", "13.98", "25.7", "3.16", "16.23"]}]}';
				
				var JSONARRAY=$.parseJSON(sample);
				alert(JSONARRAY);
				
				
				}
		
		/* if (action == 'search') {
			var TeamMemberName = isBlankField('TeamMemberName', 'Perror',
					'At least one field is required for search.');

			var PartOfTeamMemberName = isBlankField('PartOfTeamMemberName', 'Perror',
					'At least one field is required for search.');

			if (TeamMemberName || PartOfTeamMemberName) {
				document.myForm.action = url;
				document.myForm.submit();
				$('#ErrorMessage').hide();
				isSubmit = true;

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		}
		else if (action=='searchTeamMember')
			{
			var elem1 = document.getElementById("PartOfTeamMemberName");
			elem1.value = "";
			var TeamMemberName = isBlankField('TeamMemberName', 'Perror',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${viewLabels.getErrTeamMemberNotLeftBlank()}');
			if (TeamMemberName) {
				document.myForm.action = url;
				document.myForm.submit();
				$('#ErrorMessage').hide();
				isSubmit = true;

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}
			
			
			}
		else if (action=='searchPartOfTeamMember')
		{
			var elem = document.getElementById("TeamMemberName");
			elem.value = "";
			var PartOfTeamMemberName = isBlankField('PartOfTeamMemberName', 'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${viewLabels.getErrPartOfTeamMemberNotLeftBlank()}');

	if (PartOfTeamMemberName) {
		document.myForm.action = url;
		document.myForm.submit();
		$('#ErrorMessage').hide();
		isSubmit = true;

	} else {
		$('#ErrorMessage').show();
		document.myForm;
		isSubmit = false;
	}
		}
		else if (action == 'displayall') {

			var elem = document.getElementById("TeamMemberName");
			elem.value = "";
			var elem1 = document.getElementById("PartOfTeamMemberName");
			elem1.value = "";

			document.myForm.action = url;
			document.myForm.submit();
			$('#ErrorMessage').hide();
			isSubmit = true;
		} */

	}

	jQuery(document).ready(function() {

		  
		setInterval(function () {
			//alert("hello");
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
		    
	    
		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)
		
		window.onunload = unloadPage;


		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
			
			
			var BoolFFID = checkBlank('FufffillmentID');
            var BoolFFName= checkBlank('PartOfFullfillMentCenterName');
			 if(BoolFFID && BoolFFName){
                  $('#searchFufffillmentID').click();
              }
			else if(BoolFFID && !BoolFFName){
			$('#searchFufffillmentID').click();
			
			}
			else if(!BoolFFID && BoolFFName){
			$('#searchPartOfFullfillMentCenterName').click();
			}
			/* else{
			$('#btnDispID').click();
			}
			 */
			
			
		      event.preventDefault();
		      return false;
		    }
		  });

	});
	
	function unloadPage()
	{
	   // alert("unload event detected!");
	   
	    
	}
</script>
</html>