<!-- 
Date Developed  Dec 30 2014
Description It is used to show the list of Location Table and Edit record on behalf of Primary Composite Keys
Created By Pradeep Kumar -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>

<style>
<!--
[
if
lt
IE
9
]
>
<
link
 
rel
="stylesheet"
 
type
="text/css"
 
href
="/css/WizardLocationStyle
.css
"/
>
<!
[
endif
]
-->
</style>



<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<div id="container" style="position: relative" class="loader_div">
			<div class="page-container">
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<div class="container">
						<!-- BEGIN PAGE TITLE -->
						<div class="page-title">
							<h1>${Location_Wizard_Search_Lookup_Level.getKP_Location_Wizard_Search_Lookup()}</h1>

						</div>

					</div>

					<!-- END PAGE HEAD -->
					<!-- BEGIN PAGE CONTENT -->
					<div class="page-content" id="page-content">
						<div class="container">

							<div>
								<a id="AddBtn"
									href="${pageContext.request.contextPath}/Location_Wizard_Maintenance"
									class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
									<span class="hidden-480">${Location_Wizard_Search_Lookup_Level.getKP_Add_New()}</span>
								</a>
								<form action="#">
									<input type="hidden" name="checkPage" value="displayAll" />
								</form>
							</div>

							<div class="row margin-top-46">
								<div class="col-md-12">

									<div class="portlet">
										<!-- <div class="form-body">
										<div class="form-group divMarginFullfilment">
											<label
												class="col-md-2 control-label labelSettingFullfillment"><b>
													Company:Rock Audio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</b> </label>

										</div>
										<br>

									</div> -->

										<%-- <a id="AddBtn"
										href="${pageContext.request.contextPath}/Location_Wizard_Maintenance"
										class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
										<span class="hidden-480">${Location_Wizard_Search_Lookup_Level.getKP_Add_New()}</span>
									</a> --%>
										<div class="row" >
										
											<kendo:grid name="wizardGrid" resizable="true" mobile="true" 
												dataBound="gridDataBound" reorderable="true" sortable="true"
												autoBind="true">
												<kendo:grid-pageable refresh="true" pageSizes="true"
													buttonCount="5">
												</kendo:grid-pageable>
												<kendo:grid-editable mode="inline" confirmation="" />

												<kendo:grid-columns>
													<kendo:grid-column field="wizardId"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Wizard_ID()}"
														width="100" />
													<kendo:grid-column field="fulfillmentDesc50"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Fulfillment_Name()}"
														width="120" />
													<kendo:grid-column field="wizardControlNumber"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Wizard_Control_Number()}"
														width="120" />
													<kendo:grid-column field="description50"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Description()}"
														width="120" />
													<kendo:grid-column field="noOfLocations"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Number_of_Locations()}"
														width="110" />
													<kendo:grid-column field="locationType"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Location_Type()}"
														width="120" />
													<kendo:grid-column field="locationProfile"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Location_Profile()}"
														width="120" />
													<kendo:grid-column field="dateCreated"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Created_Date()}"
														width="110" />
													<kendo:grid-column field="createdByTeamMember"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Created_By()}"
														width="110" />
													<kendo:grid-column field="lastActivityDate"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Applied_Date()}"
														width="110" />
													<kendo:grid-column field="lastActivityTeamMember"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Applied_By()}"
														width="110" />
													<kendo:grid-column field="status"
														title="${Location_Wizard_Search_Lookup_Level.getKP_Status()}"
														width="110" />

													<kendo:grid-column
														title="${Location_Wizard_Search_Lookup_Level.getKP_Actions()}"
														width="200">
														<kendo:grid-column-command>
															<kendo:grid-column-commandItem
																className="fa fa-copy btn btn-sm yellow filter-submit margin-bottom"
																name="CopyDetails"
																text=" ${Location_Wizard_Search_Lookup_Level.getKP_Copy()}">
																<kendo:grid-column-commandItem-click>
																	<script>
																		function CopyDetails(
																				e) {

																			e
																					.preventDefault();

																			var dataItem = this
																					.dataItem($(
																							e.currentTarget)
																							.closest(
																									"tr"));
																			var StrWizard_ID = dataItem.wizardId;
																			var Strfulfillment_ID = dataItem.fulfillmentCenterId;
																			var StrWizardControlNumber = dataItem.wizardControlNumber;

																			var url = "${pageContext.request.contextPath}/Location_Wizard_Maintenance_Edit_And_Copy_OnSearch_Lookup";

																			var myform = document
																					.createElement("form");

																			var WizardIDFiled = document
																					.createElement("input");
																			WizardIDFiled.type = 'hidden';
																			WizardIDFiled.value = StrWizard_ID;
																			WizardIDFiled.name = "WIZARD_ID";

																			var CopyFiled = document
																					.createElement("input");
																			CopyFiled.type = 'hidden';
																			CopyFiled.value = 'Copy';
																			CopyFiled.name = "Copy";

																			var fulfillmentField = document
																					.createElement("input");
																			fulfillmentField.type = 'hidden';
																			fulfillmentField.value = Strfulfillment_ID;
																			fulfillmentField.name = "FULFILLMENT_CENTER_ID";

																			var WizardControlNumberField = document
																					.createElement("input");
																			WizardControlNumberField.type = 'hidden';
																			WizardControlNumberField.value = StrWizardControlNumber;
																			WizardControlNumberField.name = "WIZARD_CONTROL_NUMBER";

																			myform.action = url;
																			myform.method = "get"
																			myform
																					.appendChild(WizardIDFiled);
																			myform
																					.appendChild(fulfillmentField);
																			myform
																					.appendChild(WizardControlNumberField);

																			myform
																					.appendChild(CopyFiled);
																			document.body
																					.appendChild(myform);
																			myform
																					.submit();

																		}
																	</script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

															<%-- 		<kendo:grid-column-commandItem
															className="icon-pencil btn btn-sm yellow filter-submit margin-bottom"
															text="${Location_Wizard_Search_Lookup_Level.getKP_Edit()}">
															<kendo:grid-column-commandItem-click>
																<script>
																	function EditDetails(
																			e) {

																		 e
																			.preventDefault();

																	var dataItem = this
																			.dataItem($(
																					e.currentTarget)
																					.closest(
																							"tr"));
																	var StrWizard_ID = dataItem.id.wizard_ID;
																	var Strfulfillment_ID = dataItem.id.fulfillment_CENTER_ID;
																	var StrWizardControlNumber = dataItem.id.wizard_CONTROL_NUMBER;
																

																	var url = "${pageContext.request.contextPath}/Location_Wizard_Maintenance_Edit_And_Copy_OnSearch_Lookup";

																	var myform = document
																			.createElement("form");

																	var WizardIDFiled = document
																			.createElement("input");
																	WizardIDFiled.type = 'hidden';
																	WizardIDFiled.value = StrWizard_ID;
																	WizardIDFiled.name = "WIZARD_ID";
																	
																	var EditFiled = document
																	.createElement("input");
																	EditFiled.type = 'hidden';
																	EditFiled.value = 'Edit';
																	EditFiled.name = "Edit";

																	var fulfillmentField = document
																			.createElement("input");
																	fulfillmentField.type = 'hidden';
																	fulfillmentField.value = Strfulfillment_ID;
																	fulfillmentField.name = "FULFILLMENT_CENTER_ID";

																	var WizardControlNumberField = document
																			.createElement("input");
																	WizardControlNumberField.type = 'hidden';
																	WizardControlNumberField.value = StrWizardControlNumber;
																	WizardControlNumberField.name = "WIZARD_CONTROL_NUMBER";

																

																	myform.action = url;
																	myform.method = "get"
																	myform
																			.appendChild(WizardIDFiled);
																	myform
																			.appendChild(fulfillmentField);
																	myform
																			.appendChild(WizardControlNumberField);
																	myform
																	.appendChild(EditFiled);
																
																	document.body
																			.appendChild(myform);
																	myform
																			.submit();
								
																	}
																</script>
															</kendo:grid-column-commandItem-click>
														</kendo:grid-column-commandItem>
														
											 			
														 <kendo:grid-column-commandItem name="edit" text="${ScreenLabels.getMenuMessage_Edit()}" /> --%>
															<kendo:grid-column-commandItem
																className="icon-pencil btn btn-sm yellow filter-submit margin-bottom"
																name="editDetails"
																text="${Location_Wizard_Search_Lookup_Level.getKP_Notes()}">
																<kendo:grid-column-commandItem-click>
																	<script>
																		function showNoteScreen(
																				e) {

																			e
																					.preventDefault();

																			var dataItem = this
																					.dataItem($(
																							e.currentTarget)
																							.closest(
																									"tr"));

																			var StrwizardControlNumber = dataItem.wizardControlNumber;

																			var url = "${pageContext.request.contextPath}/Notes_Lookup";

																			var myform = document
																					.createElement("form");

																			var NotesLinkField = document
																					.createElement("input");
																			NotesLinkField.type = 'hidden';
																			NotesLinkField.value = StrwizardControlNumber;
																			NotesLinkField.name = "NOTE_LINK";

																			var NotesIdField = document
																					.createElement("input");
																			NotesIdField.type = 'hidden';
																			NotesIdField.value = "WIZARDLOC";
																			NotesIdField.name = "NOTE_ID";
																			myform.method = "get";

																			myform.action = url;

																			myform
																					.appendChild(NotesLinkField);
																			myform
																					.appendChild(NotesIdField);
																			document.body
																					.appendChild(myform);
																			myform
																					.submit();

																		}
																	</script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

															<kendo:grid-column-commandItem
																className="fa fa-times btn btn-sm red filter-cancel"
																name="deleteWizardOnly"
																text="${Location_Wizard_Search_Lookup_Level.getKP_Delete_Wizard_Only()}">
																<kendo:grid-column-commandItem-click>
																	<script>
																		function deleteWizardOnly(
																				e) {
																			
																			e
																			.preventDefault();

																	var dataItem = this
																			.dataItem($(
																					e.currentTarget)
																					.closest(
																							"tr"));
																	changeCss('${Location_Wizard_Search_Lookup_Level.getKP_Are_you_sure_you_want_to_delete_this_record()}');
																			bootbox
																			.confirm(
																					'${Location_Wizard_Search_Lookup_Level.getKP_Are_you_sure_you_want_to_delete_this_record()}',
																					function(okOrCancel) {

																						if(okOrCancel == true)
																						{
																				/* if (confirm('${Location_Wizard_Search_Lookup_Level.getKP_Location_WIzard_is_about_to_be_deleted()}') == true)
																					{ */

																				
																				var StrWizard_ID = dataItem.wizardId;
																				var Strfulfillment_ID = dataItem.fulfillmentCenterId;
																				var StrWizardControlNumber = dataItem.wizardControlNumber;

																				$
																						.post(
																								"${pageContext.request.contextPath}/RestCheckLocationList",
																								{
																									WIZARD_ID : StrWizard_ID,
																									WIZARD_CONTROL_NUMBER : StrWizardControlNumber,
																									FULFILLMENT_CENTER_ID : Strfulfillment_ID,
																									TENANT_ID : '',
																									COMPANY_ID : ''

																								},
																								function(
																										data,
																										status) {

																									if (data.length < 1) {

																										var myform = document
																												.createElement("form");

																										var WizardIDFiled = document
																												.createElement("input");
																										WizardIDFiled.type = 'hidden';
																										WizardIDFiled.value = StrWizard_ID;
																										WizardIDFiled.name = "WIZARD_ID";

																										var EditFiled = document
																												.createElement("input");
																										EditFiled.type = 'hidden';
																										EditFiled.value = 'Edit';
																										EditFiled.name = "Edit";

																										var fulfillmentField = document
																												.createElement("input");
																										fulfillmentField.type = 'hidden';
																										fulfillmentField.value = Strfulfillment_ID;
																										fulfillmentField.name = "FULFILLMENT_CENTER_ID";

																										var WizardControlNumberField = document
																												.createElement("input");
																										WizardControlNumberField.type = 'hidden';
																										WizardControlNumberField.value = StrWizardControlNumber;
																										WizardControlNumberField.name = "WIZARD_CONTROL_NUMBER";

																										myform
																												.appendChild(WizardIDFiled);
																										myform
																												.appendChild(fulfillmentField);
																										myform
																												.appendChild(WizardControlNumberField);
																										document.body
																												.appendChild(myform);
																										myform.action = "${pageContext.request.contextPath}/Delete_WizardOnly";
																										myform.method = "GET"
																										myform
																												.submit();

																										// alert ("User Already Exists");
																									} else {
																										//isSubmit = true;
																										changeCss('${Location_Wizard_Search_Lookup_Level.getKP_Deletion_will_remove_the_Wizard_from_the_locations()}');
																										bootbox
																										.confirm(
																												'${Location_Wizard_Search_Lookup_Level.getKP_Deletion_will_remove_the_Wizard_from_the_locations()}',
																												function(okOrCancel) {

																													if(okOrCancel == true)
																													{ 
																											var myform = document
																													.createElement("form");

																											var WizardIDFiled = document
																													.createElement("input");
																											WizardIDFiled.type = 'hidden';
																											WizardIDFiled.value = StrWizard_ID;
																											WizardIDFiled.name = "WIZARD_ID";

																											var EditFiled = document
																													.createElement("input");
																											EditFiled.type = 'hidden';
																											EditFiled.value = 'Edit';
																											EditFiled.name = "Edit";

																											var fulfillmentField = document
																													.createElement("input");
																											fulfillmentField.type = 'hidden';
																											fulfillmentField.value = Strfulfillment_ID;
																											fulfillmentField.name = "FULFILLMENT_CENTER_ID";

																											var WizardControlNumberField = document
																													.createElement("input");
																											WizardControlNumberField.type = 'hidden';
																											WizardControlNumberField.value = StrWizardControlNumber;
																											WizardControlNumberField.name = "WIZARD_CONTROL_NUMBER";

																											myform
																													.appendChild(WizardIDFiled);
																											myform
																													.appendChild(fulfillmentField);
																											myform
																													.appendChild(WizardControlNumberField);
																											document.body
																													.appendChild(myform);
																											myform.action = "${pageContext.request.contextPath}/Delete_WizardOnly";
																											myform.method = "GET"
																											myform
																													.submit();
																										}
																									});
																									}
																								});//end post

																				/* } */
																			}
																		});
																		}
																	</script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

															<kendo:grid-column-commandItem
																className="fa fa-times btn btn-sm red filter-cancel"
																name="deleteWizardAndLocation"
																text="${Location_Wizard_Search_Lookup_Level.getKP_Delete_Wizard_And_Location()}">
																<kendo:grid-column-commandItem-click>
																	<script>
																		function deleteWizardAndLocation(
																				e) {

																			e.preventDefault();

																	var dataItem = this
																			.dataItem($(
																					e.currentTarget)
																					.closest(
																							"tr"));
																			/* if (confirm('${Location_Wizard_Search_Lookup_Level.getKP_Are_you_sure_you_want_to_delete_this_record()}') == true)
																			{ */
																				changeCss('${Location_Wizard_Search_Lookup_Level.getKP_Are_you_sure_you_want_to_delete_this_record()}');
																			bootbox
																					.confirm(
																							'${Location_Wizard_Search_Lookup_Level.getKP_Are_you_sure_you_want_to_delete_this_record()}',
																							function(okOrCancel) {
																								if(okOrCancel == true)
																									{
																								var StrWizard_ID = dataItem.wizardId;
																								var Strfulfillment_ID = dataItem.fulfillmentCenterId;
																								var StrWizardControlNumber = dataItem.wizardControlNumber;
																								
																								changeCss('${Location_Wizard_Search_Lookup_Level.getKP_Location_WIzard_and_its_Locations_are_about_to_be_deleted()}');
																								bootbox
																										.confirm(
																												'${Location_Wizard_Search_Lookup_Level.getKP_Location_WIzard_and_its_Locations_are_about_to_be_deleted()}',
																												function(okOrCancel) {

																													if(okOrCancel == true)
																													{
																													

																													$
																															.post(
																																	"${pageContext.request.contextPath}/RestCheckLocationListInventory_LevelList",
																																	{
																																		WIZARD_ID : StrWizard_ID,
																																		WIZARD_CONTROL_NUMBER : StrWizardControlNumber,
																																		FULFILLMENT_CENTER_ID : Strfulfillment_ID,
																																		TENANT_ID : '',
																																		COMPANY_ID : ''

																																	},
																																	function(
																																			data,
																																			status) {

																																		if (data.length < 1) {

																																			var myform = document
																																					.createElement("form");

																																			var WizardIDFiled = document
																																					.createElement("input");
																																			WizardIDFiled.type = 'hidden';
																																			WizardIDFiled.value = StrWizard_ID;
																																			WizardIDFiled.name = "WIZARD_ID";

																																			var EditFiled = document
																																					.createElement("input");
																																			EditFiled.type = 'hidden';
																																			EditFiled.value = 'NoData';
																																			EditFiled.name = "DATA";

																																			var fulfillmentField = document
																																					.createElement("input");
																																			fulfillmentField.type = 'hidden';
																																			fulfillmentField.value = Strfulfillment_ID;
																																			fulfillmentField.name = "FULFILLMENT_CENTER_ID";

																																			var WizardControlNumberField = document
																																					.createElement("input");
																																			WizardControlNumberField.type = 'hidden';
																																			WizardControlNumberField.value = StrWizardControlNumber;
																																			WizardControlNumberField.name = "WIZARD_CONTROL_NUMBER";

																																			myform
																																					.appendChild(WizardIDFiled);
																																			myform
																																					.appendChild(fulfillmentField);
																																			myform
																																					.appendChild(WizardControlNumberField);
																																			myform
																																					.appendChild(EditFiled);
																																			document.body
																																					.appendChild(myform);
																																			myform.action = "${pageContext.request.contextPath}/Delete_WizardAndLocation";
																																			myform.method = "GET"
																																			myform
																																					.submit();

																																			// alert ("User Already Exists");
																																		} else {
																																			//isSubmit = true;
																																			var text = "";
																																			for (i = 0; i < data.length; i++) {
																																				if(data.length==1)
																											                                     {
																											                                     text += data[i][4];
																											                                     }
																											                                   else	if(i==0)
																																					{
																																				text += "\n"+data[i][4]+" ,"
																																						+ "\r\n";
																																					}else if(data.length-1== i)
																												                                    {
																													                                    text += data[i][4];
																													                                    }
																																				else
																																					{
																																					text += data[i][4]+" ,";
																																					}
																																			}

																																			/* if (confirm('${Location_Wizard_Search_Lookup_Level.getKP_Deletion_will_not_remove_these()}'
																																					+ text) == true) { */
																																				changeCss('${Location_Wizard_Search_Lookup_Level.getKP_Deletion_will_not_remove_these()}');
																																				bootbox
																																				.confirm(
																																						'${Location_Wizard_Search_Lookup_Level.getKP_Deletion_will_not_remove_these()}'+"\n"
																																						+ text,
																																					function(okOrCancel) {

																																				if(okOrCancel == true)
																																				{
																																				
																																				var myform = document
																																						.createElement("form");

																																				var WizardIDFiled = document
																																						.createElement("input");
																																				WizardIDFiled.type = 'hidden';
																																				WizardIDFiled.value = StrWizard_ID;
																																				WizardIDFiled.name = "WIZARD_ID";

																																				var EditFiled = document
																																						.createElement("input");
																																				EditFiled.type = 'hidden';
																																				EditFiled.value = 'YesData';
																																				EditFiled.name = "DATA";

																																				var fulfillmentField = document
																																						.createElement("input");
																																				fulfillmentField.type = 'hidden';
																																				fulfillmentField.value = Strfulfillment_ID;
																																				fulfillmentField.name = "FULFILLMENT_CENTER_ID";

																																				var WizardControlNumberField = document
																																						.createElement("input");
																																				WizardControlNumberField.type = 'hidden';
																																				WizardControlNumberField.value = StrWizardControlNumber;
																																				WizardControlNumberField.name = "WIZARD_CONTROL_NUMBER";

																																				myform
																																						.appendChild(WizardIDFiled);
																																				myform
																																						.appendChild(fulfillmentField);
																																				myform
																																						.appendChild(WizardControlNumberField);
																																				myform
																																						.appendChild(EditFiled);
																																				document.body
																																						.appendChild(myform);
																																				myform.action = "${pageContext.request.contextPath}/Delete_WizardAndLocation";
																																				myform.method = "GET"
																																				myform
																																						.submit();
																																			}
																																						});
																																		}
																																	});//end post
																													}

																												});
																									}
																							});

																			//	}
																			//	}
																		}
																	</script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>

														</kendo:grid-column-command>
													</kendo:grid-column>
												</kendo:grid-columns>

												<kendo:dataSource pageSize="10">
													<kendo:dataSource-transport>
														<kendo:dataSource-transport-read cache="false"
															url="${pageContext.request.contextPath}/Grid_Data"></kendo:dataSource-transport-read>
														<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/GeneralCode"></kendo:dataSource-transport-read> --%>
														<kendo:dataSource-transport-update
															url="GeneralCodeList/Grid/update" dataType="json"
															type="POST" contentType="application/json" />
														<kendo:dataSource-transport-destroy
															url="${pageContext.request.contextPath}/MenuMessageDelete"
															dataType="json" type="POST"
															contentType="application/json">
														</kendo:dataSource-transport-destroy>


														<kendo:dataSource-transport-parameterMap>
															<script>
																function parameterMap(
																		options,
																		type) {

																	return JSON
																			.stringify(options);
																	kendo.ui
																			.progress(
																					$("#description50"),
																					true);

																}
															</script>
														</kendo:dataSource-transport-parameterMap>

													</kendo:dataSource-transport>




												</kendo:dataSource>
											</kendo:grid>

										</div>




										<!-- 	<div class="margin-bottom-5-right-allign_MenuMessage_Notes">

										<button class="btn btn-sm yellow   margin-bottom"
											id="validate">
											<i class="fa fa-check"></i>Delete Wizard & Location
										</button>

										
										<button class="btn btn-sm yellow   margin-bottom"
											id="validate">
											<i class="fa fa-check"></i>Delete Wizard Only
										</button>
				
										<button class="btn btn-sm yellow   margin-bottom"
											id="validate">
											<i class="fa fa-check"></i>Notes
										</button>
										
										<button class="btn btn-sm yellow   margin-bottom"
											id="validate">
											<i class="fa fa-check"></i>Copy
										</button>						
										
										
									</div> -->



									</div>

									<!-- End: life time stats -->
								</div>


							</div>
						</div>


						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>

				<!-- END PAGE CONTENT -->
			</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
	type="text/javascript"></script>
	
    
  <%--   <link type="text/css" rel="stylesheet" href="<c:url value="/KendoUI/styles/kendo.common.min.css"/>"/>
    <link type="text/css" rel="stylesheet" href="<c:url value="/KendoUI/styles/kendo.default.min.css"/>"/>
    <link type="text/css" rel="stylesheet" href="<c:url value="/KendoUI/styles/kendo.dataviz.min.css"/>"/>
    <link type="text/css" rel="stylesheet" href="<c:url value="/KendoUI/styles/kendo.dataviz.default.min.css"/>"/>
    <link type="text/css" rel="stylesheet" href="<c:url value="/KendoUI/styles/kendo.mobile.all.min.css"/>"/>

    <script src="<c:url value="/KendoUI/js/jquery-1.9.1.min.js"/>" ></script>
    <script src="<c:url value="/KendoUI/js/kendo.all.min.js"/>"></script>  --%>
	
<!-- This is used to Set Custom Css that created by Pradeep Kumar for Wizard Location's all Screens -->
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/WizardLocation.css"/>"
	rel="stylesheet" />

<script>
	jQuery(document).ready(function() {

		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});

		setInterval(function() {

			var h = window.innerHeight;
			if (window.innerHeight >= 900 || window.innerHeight == 1004) {
				h = h - 187;
			} else {
				h = h - 239;
			}
			document.getElementById("page-content").style.minHeight = h + "px";

		}, 30);

	});
	
	  $("#wizardGrid").kendoTooltip({
	       filter: "td:nth-child(3)", //this filter selects the first column cells
	       iframe:false,
		   width:350,
	       position: "bottom",
	       content: function(e){
	    	   
	    	
	        var dataItem = $("#wizardGrid").data("kendoGrid").dataItem(e.target.closest("tr"));
	        var content = dataItem.notes;
	       if(content){
	         return content;
	        }
	        else{
	         e.preventDefault();
	   }
	       }
	     }).data("kendoTooltip");
	  
	
	function headerChangeLanguage() {

		/* alert('in security look up'); */

		$.ajax({
			url : '${pageContext.request.contextPath}/HeaderLanguageChange',
			type : 'GET',
			cache : false,
			success : function(data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});

	}

	function headerInfoHelp() {

		//alert('in security look up');

		/* $
		.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
		 */
		$.post("${pageContext.request.contextPath}/HeaderInfoHelp", {
			InfoHelp : 'LocationWizard',
			InfoHelpType : 'PROGRAM'

		}, function(data1, status) {
			if (data1.boolStatus) {
				window.open(data1.strMessage);

			} else {
				//  alert('No help found yet');
			}

		});

	}

	var gridElement = $("#wizardGrid");

	function restoreSelection(e) {

		kendo.ui.progress(gridElement, false);
	}

	function showLoading(e) {
		kendo.ui.progress(gridElement, true);
	}

	function gridDataBound(e) {
		var grid = e.sender;
		if (grid.dataSource.total() > 0) {
			var colCount = grid.columns.length;
			kendo.ui.progress(ajaxContainer, false);

		}
		else
			{
				kendo.ui.progress(ajaxContainer, false);
			}
	};
	
	function changeCss(Msg) {
		  var setwidthpopup =     document.getElementsByClassName('modal-dialog');
		 // setwidthpopup.style.width = Msg.length;
		  $(".modal-dialog").css('width',Msg.length)
		  $('.modal-dialog').css({"width":"500px"});
		}
	
</script>
<script>
    var app = new kendo.mobile.Application(document.body);
</script>
<script>
	var ajaxContainer = $("#container");
	kendo.ui.progress(ajaxContainer, true);
</script>

