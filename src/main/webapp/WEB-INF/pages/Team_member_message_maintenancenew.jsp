
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date" %>
<!DOCTYPE html>
<style>
.TextAreaErrorMessage{
position: relative !important;
top: -23px !important;
}

.fa-remove:before, .fa-close:before, .fa-times:before,.fa-check:before,.fa-search:before {
	margin-right: 5px !important;
}

.fa-plus:before{
margin-right: 5px !important;
}

</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div class="body">
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${ViewLabels.getTeamMemberMessages_TMMMaintenance()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			<div class="page-toolbar">
				<!-- BEGIN THEME PANEL -->
				<div class="btn-group btn-theme-panel">
					<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
					<!--<i class="icon-settings"></i>-->
					</a>
					<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<h3>THEME COLORS</h3>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li class="theme-color theme-color-default" data-theme="default">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Default</span>
											</li>
											<li class="theme-color theme-color-blue-hoki" data-theme="blue-hoki">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Hoki</span>
											</li>
											<li class="theme-color theme-color-blue-steel" data-theme="blue-steel">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Blue Steel</span>
											</li>
											<li class="theme-color theme-color-yellow-orange" data-theme="yellow-orange">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Orange</span>
											</li>
											<li class="theme-color theme-color-yellow-crusta" data-theme="yellow-crusta">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Yellow Crusta</span>
											</li>
										</ul>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<ul class="theme-colors">
											<li class="theme-color theme-color-green-haze" data-theme="green-haze">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Green Haze</span>
											</li>
											<li class="theme-color theme-color-red-sunglo" data-theme="red-sunglo">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Sunglo</span>
											</li>
											<li class="theme-color theme-color-red-intense" data-theme="red-intense">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Red Intense</span>
											</li>
											<li class="theme-color theme-color-purple-plum" data-theme="purple-plum">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Plum</span>
											</li>
											<li class="theme-color theme-color-purple-studio" data-theme="purple-studio">
												<span class="theme-color-view"></span>
												<span class="theme-color-name">Purple Studio</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 seperator">
								<h3>LAYOUT</h3>
								<ul class="theme-settings">
									<li>
										 Layout
										<select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips" data-original-title="Change layout type" data-container="body" data-placement="left">
											<option value="boxed" selected="selected">Boxed</option>
											<option value="fluid">Fluid</option>
										</select>
									</li>
									<li>
										 Top Menu Style
										<select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change top menu dropdowns style" data-container="body" data-placement="left">
											<option value="dark" selected="selected">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Top Menu Mode
										<select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) top menu" data-container="body" data-placement="left">
											<option value="fixed">Fixed</option>
											<option value="not-fixed" selected="selected">Not Fixed</option>
										</select>
									</li>
									<li>
										 Mega Menu Style
										<select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips" data-original-title="Change mega menu dropdowns style" data-container="body" data-placement="left">
											<option value="dark" selected="selected">Dark</option>
											<option value="light">Light</option>
										</select>
									</li>
									<li>
										 Mega Menu Mode
										<select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips" data-original-title="Enable fixed(sticky) mega menu" data-container="body" data-placement="left">
											<option value="fixed" selected="selected">Fixed</option>
											<option value="not-fixed">Not Fixed</option>
										</select>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- END THEME PANEL -->
			</div>
			<!-- END PAGE TOOLBAR -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Dashboard 
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				
							
				<!-- BEGIN PAGE CONTENT-->
			<div class="col-md-12">
					<form:form class="form-horizontal form-row-seperated" name="myForm"  onsubmit="return isformSubmit();" action="TeamMemberMessagesMaintenanceNew" commandName="InsertRow">
						<div class="portlet">
							<input type="hidden" name="getLastActivitydate" id="getLastActivitydate">
								 		
							<div class="portlet-body">
							                <div id="ErrorMessage" class="note note-danger"
												style="display: none; width: 400px;">
												<p></p>
											</div>
											<div id="ErrorMessage1" class="note note-danger"
												style="display: none; width: 400px;">
												<p></p>
											</div>
								<div class="tabbable">
										<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
											 
											 
													<input type="hidden" class="form-controlwidth" name="TeamMember" value="${TeamMember}">
												
													
											<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getTeamMemberMessages_StartDate_YY()}:<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" id="StartDate" name="StartDate" readonly="true" style="width:241px; background-color:white;" >
														<span id="ErrorMessageStartDate" class="error"></span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label" >${ViewLabels.getTeamMemberMessages_EndDate_YY()}:<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" id="EndDate" readonly="true" style="width:241px; background-color:white;" name="EndDate" >
														<span id="ErrorMessageEndDate" class="error"></span>
													</div>
												</div>
													<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getTeamMemberMessages_TicketTape()}:
													
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" id="TicketTapeMessage" maxlength="500" name="TicketTapeMessage" >
														<span id="ErrorMessageTicketTapeMessage" class="error"></span>
													</div>
												</div>
													<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getTeamMemberMessages_Message()}:
													
													</label>
													<div class="col-md-10">
														<!-- <input type="text" class="form-controlwidth" id="Message1" maxlength="500" name="Message1" > -->
														<textarea class="form-controlwidth" id="Message1"  maxlength="500"  style="resize:none" name="Message1"></textarea>
														<span id="ErrorMessageMessage1" class="error"></span>
													</div>
												</div>
													
												
												<div class="margin-bottom-5-right-allign_maintenancenew_message">
												<button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>${ViewLabels.getTeamMemberMessages_SaveUpdate()}</button>
												<button class="btn btn-sm red " type="reset" onclick="reloadPage();"><i class="fa fa-times"></i>${ViewLabels.getTeamMemberMessages_Reset()}</button>
												<button class="btn btn-sm red " type="button" onclick="PageReset();"><i class="fa fa-times"></i>${ViewLabels.getTeamMemberMessages_Cancel()}</button>
										   </div>
										  
										</div>
										
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>
			<!-- END PAGE CONTENT-->
			
				<!--end tabbable-->
				
				<!--end tabbable-->
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
 type="text/javascript"></script>
<link type="text/css"
 href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
 rel="stylesheet" />
<script>

function reloadPage(){													
	
	window.parent.location = window.parent.location.href;
	 
}
function PageReset()
{

	  window.history.back();
		//window.location.href='TeamMemberMessagesMaintenanceNew';
	
}

	$(function() {
		 var dates = $("#StartDate, #EndDate").datepicker(
				{
					minDate : '0',
					dateFormat : 'yy-mm-dd',
					//showOn : "button",
					//	buttonImage : "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
					//buttonImage:'/resourcesValidate/css/icon_date_picker.png',
					//buttonImage:'<c:url value="/resources/assets/admin/layout3/img/Language_Icon.png"/>',
					//buttonImageOnly : true,

					onSelect : function(selectedDate) {
						var option = this.id == "StartDate" ? "minDate"
								: "maxDate", instance = $(this).data(
								"datepicker"), date = $.datepicker.parseDate(
								instance.settings.dateFormat
										|| $.datepicker._defaults.dateFormat,
								selectedDate, instance.settings);
						date.setDate(date.getDate());
						dates.not(this).datepicker("option", option, date);
					}
				}); 
		 
		 
		 
	});
</script>

<script>
jQuery(document).ready(function() { 
	
	var value = '${SavedSuccessfully}';
	var message = '${ViewLabels.getTeamMemberMessages_SaveMessage()}';
	if (value == 'Record has been saved successfully.') {
		 bootbox.alert(message,function(){
				
		//alert(message);
		//window.location.href = 'Team_member_maintenance';
		window.location.href = 'TeamMemberMessagesMaintenancelookup';
		 });
	} else {
		
	}

	$(window).keydown(function(event){
	       if(event.keyCode == 13) {
	         event.preventDefault();
	         return false;
	       }
	     });
	
	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
	 
	});
	
	
function headerChangeLanguage(){
	   
	   /* alert('in security look up'); */
	   
	  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	   
	   
	  }

	  function headerInfoHelp(){
	   
	   //alert('in security look up');
	   
	    /* $
	   .post(
	     "${pageContext.request.contextPath}/HeaderInfoHelp",
	     function(
	       data1) {

	      if (data1.boolStatus) {

	       location.reload();
	      }
	     });
	    */
	     $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	       {InfoHelp:'TeamMemberMessage',
	     InfoHelpType:'PROGRAM'
	     
	       }, function( data1,status ) {
	        if (data1.boolStatus) {
	         window.open(data1.strMessage); 
	                
	       }
	        else
	         {
	       //  alert('No help found yet');
	         }
	        
	        
	       });
	   
	  }
	
   //Metronic.init(); // init metronic core componets
  // Layout.init(); // init layout
  // Demo.init(); // init demo(theme settings page)
  // Index.init(); // init index page
   //Tasks.initDashboardWidget(); // init tash dashboard widget

</script>
<script>


function isformSubmit() {
	var isSubmit = false;
	var mandatoryMessage='${ViewLabels.getTeamMemberMessages_MandatoryFieldMessage()}';
	var ticketTapeMessage='${ViewLabels.getTeamMemberMessages_BlankTicketTapeMessage()}';
	var DateMessage='${ViewLabels.getTeamMemberMessages_InvalidDateMessage()}'
	
  
  var BlankStartDate = isBlankField('StartDate', 'ErrorMessageStartDate','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			+mandatoryMessage);
  
  var BlankEndDate = isBlankField('EndDate', 'ErrorMessageEndDate','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			+ mandatoryMessage);
    
  
  
  var IsValidStartDate;
  var IsValidEndDate;
  var BlankTicket;
  var BlankMessage1;
  
  if(BlankStartDate && BlankEndDate)
  {
	  isLastDateLessThenEnd = testDate('StartDate','EndDate',
				'ErrorMessageEndDate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ DateMessage);
	  if(isLastDateLessThenEnd)
	  {
		  BlankTicket= isBlankField('TicketTapeMessage', 'ErrorMessageTicketTapeMessage','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ticketTapeMessage);
		  
		  
		  BlankMessage1=isBlankField('Message1', 'ErrorMessageMessage1','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		  +ticketTapeMessage);
		  
		  if((BlankTicket || BlankMessage1))
			  {
			  var Tickettape = $('#TicketTapeMessage').val().trim();
			    document.getElementById('TicketTapeMessage').value=Tickettape;
			    var Message = $('#Message1').val().trim();
			    document.getElementById('Message1').value=Message;
			    $('#getLastActivitydate').val(getUTCDateTime());
				 $('#StartDate').val(ConvertLocalToUTCCurrentTimeZone($('#StartDate').val()));
				 $('#EndDate').val(ConvertLocalToUTCCurrentTimeZone($('#EndDate').val()));															
				 
			    isSubmit = true; 
			  }
			
	  }
  }
  if(BlankTicket || BlankMessage1)
  {
	  ErrorMessageTicketTapeMessage.innerHTML = "";
	  ErrorMessageMessage1.innerHTML = "";
	  
  }
  
	return isSubmit;
	  
	 
   

  }
 
  

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>