<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.2.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8" />
<title>JASCI | Login Options - Login Form 1</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/uniform/css/uniform.default.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link
	href="<c:url value="/resources/assets/global/plugins/select2/select2.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/admin/pages/css/login.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link
	href="<c:url value="/resources/assets/global/css/components.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/assets/global/css/plugins.css"/>"
	rel="stylesheet" type="text/css" />
<link href="/resources/assets/admin/layout/css/layout.css"
	rel="stylesheet" type="text/css" />
<link id="style_color"
	href="<c:url value="/resources/assets/admin/layout/css/themes/default.css"/>"
	rel="stylesheet" type="text/css" />
<link
	href="<c:url value="/resources/assets/admin/layout/css/custom.css"/>"
	rel="stylesheet" type="text/css" />
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<link href="<c:url value="/resourcesValidate/css/error-style.css"/>"  rel="stylesheet" type="text/css">
<style type="text/css">
.error-note{

height: 75px;
}
.error-Msg{
top:23px;

}
</style>
</head>
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="login">
	<!-- BEGIN LOGO -->
	<div class="logo">
		<!-- <a href="index.html"> -->
		<img class="logojasci"
			src="<c:url value="/resources/assets/admin/layout3/img/Jasci_02.png"/>"
			alt="" />
		<!-- </a> -->
	</div>
	<!-- END LOGO -->
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="menu-toggler sidebar-toggler"></div>
	<!-- END SIDEBAR TOGGLER BUTTON -->
	<!-- BEGIN LOGIN -->
	<div class="content_pass">
		<!-- BEGIN LOGIN FORM -->
		<form class="login-form" action="setPassword" method="post"
			onsubmit="return checkPasswordMatch();" commandName="getNewPassword">
			<!-- <div id="ErrorMessage" class="note note-danger" style="display:none;"></div> -->
			<font color="red"> <!-- private String StrPasswordExpiredMessage;
	private String StrPasswordNotMatchedMessage;
	private String StrPasswordUsedMessage;
	private String StrPasswordPatternMessage;
	 -->

<input id="LastActivityDateStringH" type="hidden" class="form-controlwidth" name="LastActivityDateStringH" />
				<div id="ErrorMessage11" class="note note-danger"
					style="display: none;">

					<p class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter all passwords first.</p>

				</div>


				<div id="ErrorMessage12" class="note note-danger"
					style="display: none;">

					<p   class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter new password.</p>

				</div>


				<div id="ErrorMessage13" class="note note-danger" style="display: none;">

					<p  class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter confirm password.</p>

				</div> <c:if test="${ValidationResult eq 'PasswordNotMatchedMessage'}">
					<div id="ErrorMessage1" class="note error-note note-danger">
						<p  class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ObjPasswordBe.getStrPasswordNotMatchedMessage()}</p>
					</div>
				</c:if> <c:if test="${ValidationResult eq 'PasswordPatternMessage'}">
					<div id="ErrorMessage2" class="note error-note note-danger">
						<p  class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ObjPasswordBe.getStrPasswordPatternMessage()}</p>
					</div>
				</c:if> <c:if test="${ValidationResult eq 'PasswordUsedMessage'}">
					<div id="ErrorMessage3" class="note error-note note-danger">
						<p  class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ObjPasswordBe.getStrPasswordUsedMessage()}</p>
					</div>
				</c:if> <c:if test="${ ValidationResult eq 'PasswordExpiredMessage'}">
					<div id="ErrorMessage4" class="note error-note note-danger">
						<p style="width: 270px; top:32px !important" class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							${ObjPasswordBe.getStrPasswordExpiredMessage()}</p>
					</div>
				</c:if> <c:if test="${ValidationResult eq 'TemporaryPasswordMessage'}">
					<div id="ErrorMessage5" class="note error-note note-danger">
						<p  class="error error-Msg margin-left-7pix">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ObjPasswordBe.getStrTemporaryPasswordMessage()}</p>
					</div>
				</c:if>



			</font> <br>
			<h3 class="form-title">
				${ObjPasswordBe.getStrCreateNewPassword()}
				<!-- Create New Password -->
			</h3>

			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">${ObjPasswordBe.getStrNewPassword()}</label>
				<div class="input-icon">
					<i class="fa fa-lock"></i> <input type="hidden"
						value="${StrUserId}" name="struserId"> <input
						class="form-control placeholder-no-fix" autofocus
						placeholder="${ObjPasswordBe.getStrNewPassword()}" type="password"
						oncopy="return false;" onpaste="return false;" autocomplete="off"
						name="password" id="password" accept="password" value="" />
					<%-- 				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="${ObjPasswordBe.getStrNewPassword()}New Password" name="password"/>
 --%>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">${ObjPasswordBe.getStrConfirmPassword()}</label>
				<div class="input-icon">
					<i class="fa fa-lock"></i> <input
						class="form-control placeholder-no-fix"
						placeholder="${ObjPasswordBe.getStrConfirmPassword()}"
						type="password" oncopy="return false;" onpaste="return false;"
						autocomplete="off" name="UserId" id="UserId" accept="repassword"
						value="" />
					<%-- <input class="form-control placeholder-no-fix"  autocomplete="off" type="password" placeholder=" ${ObjPasswordBe.getStrConfirmPassword()}" oncopy="return false;" onpaste="return false;" name="confirmPassword" id="repassword"  accept="repassword"  value=""  /> --%>
					<%-- 				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="${ObjPasswordBe.getStrConfirmPassword()} Confirm Password" name="ConfirmPassword"/>
 --%>
				</div>
			</div>
			<div class="margin-bottom-5-right-allign_changepassword">
				<button type="submit"
					class="btn btn-sm yellow filter-submit margin-bottom">
					<!-- onclick="checkPasswordMatch();" -->
					<i class="fa fa-check"></i>&nbsp;${ObjPasswordBe.getStrSubmitButton()}
				</button>
				<button type="button" class="btn btn-sm red filter-cancel"
					onclick="HideErrorMessage();previousPage();">
					<i class="fa fa-times"></i>&nbsp;${ObjPasswordBe.getStrCancelButton()}
				</button>
			</div>

		</form>
		<!-- END LOGIN FORM -->
		<!-- BEGIN FORGOT PASSWORD FORM -->


		<!-- 	 <button id="backonpage" type="button" onclick="previousPage()" class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Cancel</button></div>
 -->
		<script>
			function previousPage() {
				$("#ErrorMessage").html("");
				window.location.href = 'loginForm';
			}

			function HideErrorMessage() {
				$("#ErrorMessage1").hide();
				$("#ErrorMessage2").hide();
				$("#ErrorMessage3").hide();
				$("#ErrorMessage4").hide();
				$("#ErrorMessage5").hide();
			}
		</script>

	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->

	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
	<script>
		window.location.hash = "no-back-button";
		window.location.hash = "Again-No-back-button";//again because google chrome don't insert first hash into history
		window.onhashchange = function() {
		window.location.hash = "no-back-button";
		}
	</script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-1.11.0.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-migrate-1.2.1.min.js"/>"
		type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery.blockui.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery.cokie.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/uniform/jquery.uniform.min.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script
		src="<c:url value="/resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"/>"
		type="text/javascript"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/assets/global/plugins/select2/select2.min.js"/>"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script
		src="<c:url value="/resources/assets/global/scripts/metronic.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/layout/scripts/layout.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/layout/scripts/quick-sidebar.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/layout/scripts/demo.js"/>"
		type="text/javascript"></script>
	<script
		src="<c:url value="/resources/assets/admin/pages/scripts/login.js"/>"
		type="text/javascript"></script>
		
		<script src="<c:url value="/resourcesValidate/form-validation.js" />"
	type="text/javascript"></script>
	<!-- END PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/resourcesValidate/js/moment.js"/>" type="text/javascript"></script>

	<script type="text/javascript">
		function checkPasswordMatch() {

			var password = $("#password").val();
			var confirmPassword = $("#UserId").val();

			if (password == '' && confirmPassword == '') {

				$("#ErrorMessage1").hide();
				$("#ErrorMessage2").hide();
				$("#ErrorMessage3").hide();
				$("#ErrorMessage4").hide();
				$("#ErrorMessage5").hide();
				$("#ErrorMessage11").show();
				$("#ErrorMessage12").hide();
				$("#ErrorMessage13").hide();
				return false;
			}

			else if (password == '') {
				$("#ErrorMessage1").hide();
				$("#ErrorMessage2").hide();
				$("#ErrorMessage3").hide();
				$("#ErrorMessage4").hide();
				$("#ErrorMessage5").hide();
				$("#ErrorMessage11").hide();
				$("#ErrorMessage12").show();
				$("#ErrorMessage13").hide();

				return false;

			}

			else if (confirmPassword == '') {
				$("#ErrorMessage1").hide();
				$("#ErrorMessage2").hide();
				$("#ErrorMessage3").hide();
				$("#ErrorMessage4").hide();
				$("#ErrorMessage5").hide();
				$("#ErrorMessage11").hide();
				$("#ErrorMessage12").hide();
				$("#ErrorMessage13").show();
				return false;

			}

			else {

				$("#ErrorMessage1").hide();
				$("#ErrorMessage2").hide();
				$("#ErrorMessage3").hide();
				$("#ErrorMessage4").hide();
				$("#ErrorMessage5").hide();
				$("#ErrorMessage11").hide();
				$("#ErrorMessage12").hide();
				$("#ErrorMessage13").hide();
				return true;
			}

			//var boolIsPass=isValidePassword();
			//var boolIsConf=false;

			/*
			if(boolIsPass){
				boolIsConf=isValidConfirmPass();
				if(boolIsConf){
					return true;
				}
			}
			else{
				return false;
			}*/

		}
	</script>
	<script>
		jQuery(document).ready(function() {
			$('#LastActivityDateStringH').val(getUTCDateTime());
		
			//Metronic.init(); // init metronic core components
			//Layout.init(); // init current layout
			//QuickSidebar.init(); // init quick sidebar
			//Demo.init(); // init demo features
			//  Login.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>