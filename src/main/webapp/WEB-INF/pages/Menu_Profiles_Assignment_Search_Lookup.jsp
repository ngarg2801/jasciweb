<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html lang="en" class="no-js">

<link href="<c:url value="/resourcesValidate/css/MenuProfileAssigment.css"/>" rel="stylesheet" type="text/css">

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
<body>
<div id="container" style="position: relative" class="loader_div">
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${objMenuAssignmentLBL.getLbl_Menu_Profiles_Assignment_SEARCHLOOKUP()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
			<div class="col-md-12">
					<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
					<!-- Begin: life time stats -->
					
					<div class="portlet">

							<!-- <div class="actions">
								<a id="AddBtn" href="" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								Add New </span>
								</a>
								</div> -->
												
			<div class="row">
			
					<kendo:grid name="SearchLookupGridTenant" id="MenuAssigmentSearchGrid"
									autoBind="true" scrollable="false" 
									pageable="true" sortable="true" resizable="false"
									reorderable="true" >

									<kendo:grid-pageable refresh="true" pageSizes="false"
										buttonCount="5">
									</kendo:grid-pageable>
									<kendo:grid-editable mode="inline" confirmation="Message" />
									<kendo:grid-columns>


										
										<kendo:grid-column title="${objMenuAssignmentLBL.getLbl_Name()}" field="name" />
										<kendo:grid-column title="${objMenuAssignmentLBL.getLbl_Team_Member_ID()}" field="teamMemberID" />
										<%-- <kendo:grid-column title="" field="profile" /> --%>
										
										
										<kendo:grid-column title="${objMenuAssignmentLBL.getLbl_Actions()}" width="18%;">
											<kendo:grid-column-command>

												<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
												<kendo:grid-column-commandItem
													className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
													name="editDetails" text="${objMenuAssignmentLBL.getLbl_Edit()}">
													<kendo:grid-column-commandItem-click>
														<script>
                           
                            function editTeammember(e) {
                               

                                e.preventDefault();
	
                                
                                var dataItemEdit = this.dataItem($(e.currentTarget).closest("tr"));
                                var menuOptionVal=dataItemEdit.teamMemberID;
                                //var tenantval=dataItemEdit.tenant;
                                
                               
                               
                               
                             
                                 var url="${pageContext.request.contextPath}/MenuProfileAssigmentMaintenance";
                                //var wnd = $("#details").data("kendoWindow");
                               
    							 var myform = document.createElement("form");


                                 var tenantFiled = document.createElement("input");
                                 tenantFiled.value = "tenant";
                                 tenantFiled.name = "tenant";
                                 tenantFiled.setAttribute("type", "hidden");
                                 
                                 var menuOptionFiled = document.createElement("input");
                                 menuOptionFiled.value = menuOptionVal;
                                 menuOptionFiled.name = "lblTeamMemberID";
                                 menuOptionFiled.setAttribute("type", "hidden");
                                 
                                 
                                 myform.action = url;
                                 myform.method = "get";
                                 myform.appendChild(tenantFiled);
                                 myform.appendChild(menuOptionFiled);
                                 document.body.appendChild(myform);
                                 myform.submit();
    							//alert(res);
                              // window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                            }
                            
                          
                            </script>



													</kendo:grid-column-commandItem-click>
												</kendo:grid-column-commandItem>

												<kendo:grid-column-commandItem
													className="btn btn-sm red filter-cancel fa fa-times"
													name="btnDelete" text="${objMenuAssignmentLBL.getLbl_Delete()}">

													<kendo:grid-column-commandItem-click>

														<script>						   
							    function deleteRecords(e) { 
                               

                                e.preventDefault();
                                var dataItemGrid = this.dataItem($(e.currentTarget).closest("tr"));
                                
                                
                               var currentStatus=dataItemGrid.teamMemberID;
                               var valTenant=dataItemGrid.tenant;
                               
              		        	 
              		        	

                          	 bootbox
                               .confirm(
                                 '${objMenuAssignmentLBL.getLbl_Are_you_sure_you_want_to_delete_row()}',
                                 function(okOrCancel) {

                                  if(okOrCancel == true)
                                  {
              						
              					/* 	 if(confirm('${objMenuAssignmentLBL.getLbl_Are_you_sure_you_want_to_delete_row()}')==true){
              							 
              						 */	
                                        var url= "${pageContext.request.contextPath}/MenuProfileAssignment_Delete";
        	                           
        						    	 
        						    	 var myform1 = document.createElement("form");

                                         var tenantFiled = document.createElement("input");
                                         tenantFiled.value = valTenant;
                                         tenantFiled.name = "tenant";
                                         tenantFiled.setAttribute("type", "hidden");
                                         
                                         var menuOptionFiled = document.createElement("input");
                                         menuOptionFiled.value = currentStatus;
                                         menuOptionFiled.name = "lblTeamMemberID";
                                         menuOptionFiled.setAttribute("type", "hidden");
                                         
                                         myform1.action = url;
                                         myform1.method = "post";
                                        
                                         
                                         myform1.appendChild(tenantFiled);
                                         myform1.appendChild(menuOptionFiled);
                                         
                                         
                                         document.body.appendChild(myform1);
                                         myform1.submit();
        						    	
        						    	
              							 
              						 }else{
              							 
              						 }
                                  });
              						//return isSubmit;
              					 }
              				     
                          
                               
										
							    </script>
													</kendo:grid-column-commandItem-click>
												</kendo:grid-column-commandItem>

											</kendo:grid-column-command>
										</kendo:grid-column>
									</kendo:grid-columns>



									<kendo:dataSource autoSync="true" pageSize="10">
										<kendo:dataSource-transport>
											<kendo:dataSource-transport-read cache="false"
												url="${pageContext.request.contextPath}/Kendo_MenuProfileAssignment_List">
											</kendo:dataSource-transport-read>



											<kendo:dataSource-transport-parameterMap>

												<script>
                  function parameterMap(options,type) { 
                   
                    return JSON.stringify(options);
                   
                  }
                  
                  
                
                
                  </script>
											</kendo:dataSource-transport-parameterMap>

										</kendo:dataSource-transport>
									</kendo:dataSource>
								</kendo:grid>

			
				
				</div>
									
							
						</div>
						
					</div>
					<!-- End: life time stats -->
				</div>	
						<div id="Menu_Assigment_ShortButton">
					
												
								
										    <button class="btn btn-sm yellow filter-submit margin-bottom" 
										    onclick="sortData();" id="sortBtn" ><i class="fa fa-caret-up"></i> ${objMenuAssignmentLBL.getLblSort_First_Name()}</button></div>
				
				</div>	
			</div>
			
		
			<!-- END PAGE CONTENT INNER -->
		</div>

	</div>
	<!-- END PAGE CONTENT -->

<!-- END PAGE CONTAINER -->

</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>
<script>

function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  }
function sortData()
{
	
	
 
var Button=document.getElementById("sortBtn");
 var text=Button.innerHTML;
 if(text==='<i class=\"fa fa-caret-up\"></i> ${objMenuAssignmentLBL.getLblSort_First_Name()}')
  {
  
  Button.innerHTML="<i class=\"fa fa-caret-up\"></i> ${objMenuAssignmentLBL.getLblSort_Last_Name()}";
  var kendoGridData=$("#MenuAssigmentSearchGrid").data('kendoGrid');
  var dsSort = [];
  dsSort.push({ field: "firstName", dir: "asc" });
  kendoGridData.dataSource.sort(dsSort);  
  }
 else if(text==='<i class=\"fa fa-caret-up\"></i> ${objMenuAssignmentLBL.getLblSort_Last_Name()}')
  
  {
  Button.innerHTML="<i class=\"fa fa-caret-up\"></i> ${objMenuAssignmentLBL.getLblSort_First_Name()}";
  var kendoGridData=$("#MenuAssigmentSearchGrid").data('kendoGrid');
  var dsSort = [];
  dsSort.push({ field: "lastName", dir: "asc" });
  kendoGridData.dataSource.sort(dsSort); 
  
  } 
 
}
  


    jQuery(document).ready(function() {   
    	
    	var grid = $("#MenuAssigmentSearchGrid").data("kendoGrid");
		grid.bind("dataBound", function(e) {
			$("#MenuAssigmentSearchGrid tbody tr .k-grid-btnDelete").each(function () {
		        var currentDataItem = $("#MenuAssigmentSearchGrid").data("kendoGrid").dataItem($(this).closest("tr"));
		 
		        //Check in the current dataItem if the row is deletable
		        var currentStatus=currentDataItem.profile;
		         if (currentStatus =='X') {
		            $(this).remove();
		        } 
		    });
		});
    	setInterval(function () {
    		
    	    var h = window.innerHeight;		    
    	    if(window.innerHeight>=900){
    	    	h=h-187;
    	    	
    	    }
    	   else{
    	    	h=h-239;
    	    }
    	      document.getElementById("page-content").style.minHeight = h+"px";		     
    	    
    		}, 30);
    	
      //     Metronic.init(); // init metronic core components
//Layout.init(); // init current layout
//Demo.init(); // init demo features
        //   EcommerceOrders.init();
        });
    
    /* function gridDataBound(e) {
	    var grid = e.sender;
	    if (grid.dataSource.total() > 0) {
	     var colCount = grid.columns.length;
	     kendo.ui.progress(ajaxContainer, false);

	    }
	    else
	     {
	      kendo.ui.progress(ajaxContainer, false);
	     }
	   };
	   
	   var ajaxContainer = $("#container");
	   kendo.ui.progress(ajaxContainer, true); */
	   
	   function headerInfoHelp(){
		  	
		  	//alert('in security look up');
		  	
		   	/* $
		  	.post(
		  			"${pageContext.request.contextPath}/HeaderInfoHelp",
		  			function(
		  					data1) {

		  				if (data1.boolStatus) {

		  					location.reload();
		  				}
		  			});
		  	 */
		  	$.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
		  			  {InfoHelp:'MenuProfileAssignment',
		   		InfoHelpType:'PROGRAM'
		  		 
		  			  }, function( data1,status ) {
		  				  if (data1.boolStatus) {
		  					  window.open(data1.strMessage); 
		  					  					  
		  					}
		  				  else
		  					  {
		  					//  alert('No help found yet');
		  					  }
		  				  
		  				  
		  			  });
	   }
		  	
    </script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>