
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div class="page-container">
			
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMemberMaintenance()}</h1>
					</div>
				</div>
				
				<%-- <!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014ɠWorld Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

	
	
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT --> --%>
				<div class="page-content"  id="page-content" >

					<div class="container">


						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<%-- <div id="ErrorMessage" class="note note-danger"
								style="display: none;">
								<p>${TeamMemberInvalidMsg}</p>
							</div> --%>
						
						<div id="ErrorMessage" class="note note-danger"   style="display: none;">
     <p  class="error  error-Top margin-left-7pix" id="Perror"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p> 
    </div>
    
    
    
    <div id="ErrorMessage1" class="note note-danger" style="display: none;" >
     <span  class="error error-Top margin-left-7pix"  id= "error1" ></span>
    </div>

							<div class="col-md-12">
								<form:form name="myForm"
									class="form-horizontal form-row-seperated"  action="#"
									onsubmit="return isformSubmit();"  method="GET">
									<div class="portlet">

										<div class="portlet-body">
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EnterTeamMember()}:</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="TeamMemberName" id="TeamMemberName"
																		placeholder=""><i
																		class="fa fa-search" id="searchTeamMemberName"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('Teammember_searchlookup','search');"></i>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_PartOfTheTeamMemberName()}: </label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="PartOfTeamMemberName" id="PartOfTeamMemberName"
																		placeholder=""><i  id="searchPartOfTeamMemberName"
																		class="fa fa-search"
																		style="color: rgba(68, 77, 88, 1); cursor: pointer; margin-left: 5px;"
																		onclick="actionForm('Teammember_searchlookup','searchPart');"></i>
																</div>
															</div>



															<div
																class="margin-bottom-5-right-allign_icon_maintenanace">
																 <button
															class="btn btn-sm yellow filter-submit margin-bottom"
															onclick="actionForm('Team_member_maintenancenew','new');">
															<i class="fa fa-plus" style="margin-right: 4px;"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_New()}
														</button>
														<!-- <button
															class="btn btn-sm yellow filter-submit margin-bottom"
															onclick="actionForm('Teammember_searchlookup','search');">
															<i class="fa fa-search"></i> Search
														</button>  -->
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="btnDispID"
																	
																	onclick="actionForm('Teammember_searchlookup','displayall');">
																	<i class="fa fa-check"></i>&nbsp;${TeamMemberSceernLabelObj.getTeamMemberMaintenance_DisplayAll()}
																</button>
															</div>

														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form:form>

							</div>
							<!--end tabbable-->

							<!--end tabbable-->

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->





			</div>
	</div>
	
	</tiles:putAttribute>

</tiles:insertDefinition>


<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
	jQuery(document).ready(function() {
		
		$('#ErrorMessage').hide();
		$('#ErrorMessage1').hide();
		
		/* var value = '${TeamMemberInvalidMsg}';

		if (value=='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER()}' || value=='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME()}') {
			$('#ErrorMessage').show();
		} else {
			$('#ErrorMessage').hide();
		} */
		
		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
			
			var BoolTeamMemberID = checkBlank('TeamMemberName');
            var BoolTeamPartName= checkBlank('PartOfTeamMemberName');
			 if(BoolTeamMemberID && BoolTeamPartName){
                  $('#searchTeamMemberName').click();
              }
			else if(BoolTeamMemberID && !BoolTeamPartName){
			$('#searchTeamMemberName').click();
			
			}
			else if(!BoolTeamMemberID && BoolTeamPartName){
			$('#searchPartOfTeamMemberName').click();
			}
			/* else{
			$('#btnDispID').click();
			} */
			
		      event.preventDefault();
		      return false;
		    }
		  });

		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)
		//Index.init(); // init index page

		//Tasks.initDashboardWidget(); // init tash dashboard widget

	});
</script>

<script>
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(url, action) {
		$('#ErrorMessage').hide();
		if (action == 'search') {
			
			var valid = isBlankField('TeamMemberName', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR()}');

			if (valid) {
			
				var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = "";
				var tammbername = $('#TeamMemberName').val();				
				$('#ErrorMessage1').hide();				
				var fulfilmentCenter='${TeamMemberFulfilmentCenter}';
				var tenant='${TeamMemberTenant}';
				var errMsgId=document.getElementById("Perror");
				 $.post( "${pageContext.request.contextPath}/RestCheckTeamMember",
						 {
					 TeamMemberName:tammbername,
					 PartOfTeamMember:"",
					 Tenant:tenant
					 }, function( data,status ) {
					 
					
					 if(data)
		        	 {
					
		        	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER()}';
		        	 $('#ErrorMessage').show();
		        	 isSubmit=false;
		        	 }
					 else{
						 $('#ErrorMessage').hide();
						 isSubmit=true;
						 document.myForm.action = '${pageContext.request.contextPath}/'+url;
						 document.myForm.submit();
						//return isSubmit;
					 }
				 });//end check team member
				
				
			} else {
				$('#ErrorMessage1').show();
				document.myForm;
				var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
				elem1.value = "";
				isSubmit = false;
				
			}

		}
		
				
		else if (action == 'searchPart') {

			var valid1 = isBlankField('PartOfTeamMemberName', 'error1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER()}');

			if (valid1) {
				var elem = document.getElementById("TeamMemberName"); // Get text field
				elem.value = "";
				$('#ErrorMessage1').hide();
				var partoftammbername = $('#PartOfTeamMemberName').val();				
							
				var fulfilmentCenter='${TeamMemberFulfilmentCenter}';
				var tenant='${TeamMemberTenant}';
				var errMsgId=document.getElementById("Perror");
				 $.post( "${pageContext.request.contextPath}/RestCheckTeamMember",
						 {
					 TeamMemberName:"",
					 PartOfTeamMember:partoftammbername,
					 Tenant:tenant
					 }, function( data,status ) {
					 
					
					 if(data)
		        	 {
					
		        	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME()}';
		        	 $('#ErrorMessage').show();
		        	 isSubmit=false;
		        	 }
					 else{
						 $('#ErrorMessage').hide();
						 isSubmit=true;
						 document.myForm.action = '${pageContext.request.contextPath}/'+url;
						 document.myForm.submit();
						//return isSubmit;
					 }
				 });//end check team member				
				
				
			} else {
				$('#ErrorMessage1').show();
				var elem = document.getElementById("TeamMemberName"); // Get text field
				elem.value = "";
				document.myForm;
				isSubmit = false;
				
			}

		}
		
		else if (action == 'displayall' || action == 'new') {

			var elem = document.getElementById("TeamMemberName"); // Get text field
			elem.value = "";
			var elem1 = document.getElementById("PartOfTeamMemberName"); // Get text field
			elem1.value = "";

			document.myForm.action = '${pageContext.request.contextPath}/'+url;
			document.myForm.submit();
			$('#ErrorMessage').hide();
			isSubmit = true;
		}

	}
function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 	/* $
		.get(
				"${pageContext.request.contextPath}/HeaderLanguageChange",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				}); */
				
	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}

	function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'TeamMember',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						//  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
</script>

<script>
	jQuery(document).ready(function() {

		  
		setInterval(function () {
			
	    var h = window.innerHeight;
	    
	    //alert(h);
	    
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
	    
	});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>