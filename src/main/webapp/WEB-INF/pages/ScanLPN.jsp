<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL"%>
<!DOCTYPE html>
<html lang="en">
<head>


<link href="<c:url value="/resourcesValidate/css/executions.css"/>"
	rel="stylesheet" type="text/css">

<link href="<c:url value="/resourcesValidate/css/execution-style.css"/>"
	rel="stylesheet" type="text/css">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>


<script
	src="<c:url value="/resourcesValidate/js/generateDynamicLayout.js"/>"
	type="text/javascript"></script>

</head>

<tiles:insertDefinition name="executionTemplate">
	<tiles:putAttribute name="body">
		<div class="container" id="page-content">
			<!-- <div class="divspace">

<div class="divhistory">
</div>
<div class="divspacecurrent"> -->

			<!-- <label class="label_scanLPN">SCAN LPN</label>
<br>
<input class="form-control input-sm scan_text_box" id="scanLPNId" type="text"> -->
			<div class="col-xs-4 current_action_div"  id="historyAction">
				

			</div>
			<div class="col-xs-4 current_action_div"  id="currentAction">
				<label for="textboxscanlpn" class="label_scanLPN">${ExecutionLabels.getSCAN_LPN()}</label>
				<input class="form-control textbox_widthadd " id="textboxscanlpn"
					type="text">

			</div>
		</div>
		<!-- 
</div>
</div> -->
	</tiles:putAttribute>
</tiles:insertDefinition>
<script>
	function checkWorkType() {

	}
	jQuery(document)
			.ready(
					function() {
					$('#buttonContinue').hide();
					$('#buttonExit').hide();
											
						var ButtonName='${ExecutionBe.getStrBUTTON_NAME()}';
						var ButtonFlag='${ExecutionBe.getStrBUTTON()}';
						if(ButtonFlag=='Y'){
							$('#buttonContinue').show();							
							$("#buttonContinue").html(ButtonName);
							$('#currentAction').hide();
							$('#buttonExit').hide();
						}
						else{
							$('#currentAction').show();
						     $('#buttonExit').show();
						}

						$("#buttonContinue")
								.click(
										function() {
											//window.location = "${pageContext.request.contextPath}/menu_profile_assignment";
									   $('#currentAction').show();
									   $('#buttonExit').show();
									   
										});

						$("#buttonExit")
								.click(
										function() {
											window.location = "${pageContext.request.contextPath}/ScanLPN?strJSON="
													+ sharedObj
													+ "&executionName="
													+ "ScanLPN"
													+ "&IsCompleted=" + true;
										});
						$("#textboxscanlpn").keyup(function(event) {
							if (event.keyCode == 13) {
								doneTyping();
							}
						});
						
						
						
						var topBartextList = '${TOP_BAR_TEXT_LIST}';
						var layoutId = document.getElementById("topbarLayout");
						generateTopBar("#32cd32", topBartextList, layoutId);

						var sharedObj = '${SharedObjJson}';

						//user is "finished typing," do something
						function doneTyping() {
							//do something
							var scanlpn = $('#textboxscanlpn').val();
							$
									.post(
											"${pageContext.request.contextPath}/checkValidLpn",

											{
												TENANT_ID : '${SharedObject.getStrTenantID()}',
												COMPANY_ID : '${SharedObject.getStrCompanyID()}',
												FulfillmentCenter : '${ObjCommonSession.getFulfillmentCenter()}',
												LPN : scanlpn,
												Task : '${SharedObject.getStrTASK()}',
												WorkType : '${SharedObject.getStrWorkType()}'

											},
											function(data, status) {

												var result;
												for (key in data) {

													result = data[key];
												}

												if (result == 'Success') {
												
												var jsonStr = '${executionHistory}';
     var obj = JSON.parse(jsonStr);
     
    // var person = {ExcecutionName:"SCAN_LPN", lastName:"Doe", age:50, eyeColor:"blue"};
    
     // var jsonObject = {'ExcecutionName':, 'b':2}
     
     var positionIndex=obj.EXCECUTIONHISTORY.length;
     positionIndex=positionIndex+1;
      
     obj['EXCECUTIONHISTORY'].push({"ExcecutionName":"SCANLPN_BROWSER","EXCECUTIONHISTORYVALUES":[{"Name":"SCAN LPN","Value":scanlpn}],"Position":positionIndex});
     jsonStr = JSON.stringify(obj);
     jsonStr=encodeURI(jsonStr);
													window.location = "${pageContext.request.contextPath}/ScanLPN?strJSON="
															+ sharedObj
															+ "&executionName="
															+ jsonStr
															+ "&IsCompleted="
															+ true;
												}

												else {
													$('#DisplayMessageAreaid')
															.addClass(
																	"ErrorDisplayMessage");
													document
															.getElementById("displayMessage").innerHTML = '&nbsp;&nbsp;'
															+ result;

												}

											});

						}

					});
</script>