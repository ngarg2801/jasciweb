<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body onload="textboxhide()">
	<label id="lblsel">Select Common Class:- </label>
	<select id="dropdown" onchange="dropdownChange()">
		<option>SELECT</option>
		<option value="1">ADDRESSVALIDATION</option>
		<option value="2">FULFILLMENTCENTERLOOKUP</option>
		<option value="3">GETINFOHELP</option>
		<option value="4">GETLANGUAGE</option>
		<option value="5">LOOKUPGENERALCODE</option>
		<option value="6">TEAMMEMBERLOOKUP</option>
		<option value="7">UPDATEINVENTORY</option>
		<option value="8">ZIPVALIDATION</option>
	</select>
	<div id="demo1"></div>
	<div id="demo"></div>

	<div id="my_div"></div>

	<SCRIPT>
		function dropdownChange() {
			var day;
			var value = document.getElementById("dropdown").value;
			// document.getElementById("demo1").innerHTML = "dropdown value is " + value;
			textboxhide();
			switch (value) {
			case "1":
				textboxhide();
				document.getElementById("AddressLine1").style.visibility = 'visible';
				document.getElementById("lblAddressLine1").style.visibility = 'visible';
				document.getElementById("AddressLine2").style.visibility = 'visible';
				document.getElementById("lblAddressLine2").style.visibility = 'visible';
				document.getElementById("AddressLine3").style.visibility = 'visible';
				document.getElementById("lblAddressLine3").style.visibility = 'visible';
				document.getElementById("AddressLine4").style.visibility = 'visible';
				document.getElementById("lblAddressLine4").style.visibility = 'visible';
				document.getElementById("City").style.visibility = 'visible';
				document.getElementById("lblCity").style.visibility = 'visible';
				document.getElementById("StateCode").style.visibility = 'visible';
				document.getElementById("lblStateCode").style.visibility = 'visible';
				document.getElementById("CountryCode").style.visibility = 'visible';
				document.getElementById("lblCountryCode").style.visibility = 'visible';
				document.getElementById("ZipCode").style.visibility = 'visible';
				document.getElementById("lblZipCode").style.visibility = 'visible';
				day = "Sunday";
				break;
			case "2":
				textboxhide();
				document.getElementById("Tenant").style.visibility = 'visible';
				document.getElementById("lblTenant").style.visibility = 'visible';
				document.getElementById("FulfillmentCenter").style.visibility = 'visible';
				document.getElementById("lblFulfillmentCenter").style.visibility = 'visible';
				day = "Monday";
				break;
			case "3":
				textboxhide();
				document.getElementById("InfoHelp").style.visibility = 'visible';
				document.getElementById("lblInfoHelp").style.visibility = 'visible';
				document.getElementById("Language").style.visibility = 'visible';
				document.getElementById("lblLanguage").style.visibility = 'visible';
				document.getElementById("InfoHelpType").style.visibility = 'visible';
				document.getElementById("lblInfoHelpType").style.visibility = 'visible';
				day = "Tuesday";
				break;
			case "4":
				textboxhide();
				document.getElementById("KeyPhrase").style.visibility = 'visible';
				document.getElementById("lblKeyPhrase").style.visibility = 'visible';
				document.getElementById("Language").style.visibility = 'visible';
				document.getElementById("lblLanguage").style.visibility = 'visible';
				day = "Wednesday";
				break;
			case "5":
				textboxhide();
				document.getElementById("Attribute_Request").style.visibility = 'visible';
				document.getElementById("lblAttribute_Request").style.visibility = 'visible';
				document.getElementById("Drop_Down_Selection").style.visibility = 'visible';
				document.getElementById("lblDrop_Down_Selection").style.visibility = 'visible';
				document.getElementById("Tenant").style.visibility = 'visible';
				document.getElementById("lblTenant").style.visibility = 'visible';
				document.getElementById("Company").style.visibility = 'visible';
				document.getElementById("lblCompany").style.visibility = 'visible';
				document.getElementById("GeneralCodeID").style.visibility = 'visible';
				document.getElementById("lblGeneralCodeID").style.visibility = 'visible';
				document.getElementById("GeneralCode").style.visibility = 'visible';
				document.getElementById("lblGeneralCode").style.visibility = 'visible';
				day = "Thursday";
				break;
			case "6":
				textboxhide();
				document.getElementById("Team_Member").style.visibility = 'visible';
				document.getElementById("lblTeam_Member").style.visibility = 'visible';
				day = "Friday";
				break;
			case "7":
				textboxhide();
				document.getElementById("Tenant").style.visibility = 'visible';
				document.getElementById("lblTenant").style.visibility = 'visible';
				document.getElementById("FulfillmentCenter").style.visibility = 'visible';
				document.getElementById("lblFulfillmentCenter").style.visibility = 'visible';
				document.getElementById("Company").style.visibility = 'visible';
				document.getElementById("lblCompany").style.visibility = 'visible';
				document.getElementById("Area").style.visibility = 'visible';
				document.getElementById("lblArea").style.visibility = 'visible';
				document.getElementById("Location").style.visibility = 'visible';
				document.getElementById("lblLocation").style.visibility = 'visible';
				document.getElementById("LocationFlag").style.visibility = 'visible';
				document.getElementById("lblLocationFlag").style.visibility = 'visible';
				document.getElementById("Product").style.visibility = 'visible';
				document.getElementById("lblProduct").style.visibility = 'visible';
				document.getElementById("Quality").style.visibility = 'visible';
				document.getElementById("lblQuality").style.visibility = 'visible';
				document.getElementById("Lot").style.visibility = 'visible';
				document.getElementById("lblLot").style.visibility = 'visible';
				document.getElementById("Serial01").style.visibility = 'visible';
				document.getElementById("lblSerial01").style.visibility = 'visible';
				document.getElementById("Serial02").style.visibility = 'visible';
				document.getElementById("lblSerial02").style.visibility = 'visible';
				document.getElementById("Serial03").style.visibility = 'visible';
				document.getElementById("lblSerial03").style.visibility = 'visible';
				document.getElementById("Serial04").style.visibility = 'visible';
				document.getElementById("lblSerial04").style.visibility = 'visible';
				document.getElementById("Serial05").style.visibility = 'visible';
				document.getElementById("lblSerial05").style.visibility = 'visible';
				document.getElementById("Serial06").style.visibility = 'visible';
				document.getElementById("lblSerial06").style.visibility = 'visible';
				document.getElementById("Serial07").style.visibility = 'visible';
				document.getElementById("lblSerial07").style.visibility = 'visible';
				document.getElementById("Serial08").style.visibility = 'visible';
				document.getElementById("lblSerial08").style.visibility = 'visible';
				document.getElementById("Serial09").style.visibility = 'visible';
				document.getElementById("lblSerial09").style.visibility = 'visible';
				document.getElementById("Serial10").style.visibility = 'visible';
				document.getElementById("lblSerial10").style.visibility = 'visible';
				document.getElementById("Serial11").style.visibility = 'visible';
				document.getElementById("lblSerial11").style.visibility = 'visible';
				document.getElementById("Serial12").style.visibility = 'visible';
				document.getElementById("lblSerial12").style.visibility = 'visible';
				document.getElementById("LPN").style.visibility = 'visible';
				document.getElementById("lblLPN").style.visibility = 'visible';
				document.getElementById("InventroryControl01").style.visibility = 'visible';
				document.getElementById("lblInventroryControl01").style.visibility = 'visible';
				document.getElementById("InventroryControl02").style.visibility = 'visible';
				document.getElementById("lblInventroryControl02").style.visibility = 'visible';
				document.getElementById("InventroryControl03").style.visibility = 'visible';
				document.getElementById("lblInventroryControl03").style.visibility = 'visible';
				document.getElementById("InventroryControl04").style.visibility = 'visible';
				document.getElementById("lblInventroryControl04").style.visibility = 'visible';
				document.getElementById("InventroryControl05").style.visibility = 'visible';
				document.getElementById("lblInventroryControl05").style.visibility = 'visible';
				document.getElementById("InventroryControl06").style.visibility = 'visible';
				document.getElementById("lblInventroryControl06").style.visibility = 'visible';
				document.getElementById("InventroryControl07").style.visibility = 'visible';
				document.getElementById("lblInventroryControl07").style.visibility = 'visible';
				document.getElementById("InventroryControl08").style.visibility = 'visible';
				document.getElementById("lblInventroryControl08").style.visibility = 'visible';
				document.getElementById("InventroryControl09").style.visibility = 'visible';
				document.getElementById("lblInventroryControl09").style.visibility = 'visible';
				document.getElementById("InventroryControl10").style.visibility = 'visible';
				document.getElementById("lblInventroryControl10").style.visibility = 'visible';
				document.getElementById("Adjusted_Quantity").style.visibility = 'visible';
				document.getElementById("lblAdjusted_Quantity").style.visibility = 'visible';
				document.getElementById("UnitofMeasureCode").style.visibility = 'visible';
				document.getElementById("lblUnitofMeasureCode").style.visibility = 'visible';
				document.getElementById("UnitofMeasureQty").style.visibility = 'visible';
				document.getElementById("lblUnitofMeasureQty").style.visibility = 'visible';
				document.getElementById("ProductValue").style.visibility = 'visible';
				document.getElementById("lblProductValue").style.visibility = 'visible';
				document.getElementById("Team_Member").style.visibility = 'visible';
				document.getElementById("lblTeam_Member").style.visibility = 'visible';
				document.getElementById("Task").style.visibility = 'visible';
				document.getElementById("lblTask").style.visibility = 'visible';
				day = "Saturday";
				break;
			case "8":
				textboxhide();
				document.getElementById("ZipCode").style.visibility = 'visible';
				document.getElementById("lblZipCode").style.visibility = 'visible';
				day = "last";
				break;
			}
			// document.getElementById("demo").innerHTML = "Today is " + day; 
		}

		function textboxhide() {
			document.getElementById("Tenant").style.visibility = 'hidden';
			document.getElementById("Company").style.visibility = 'hidden';
			document.getElementById("Company_Name_20").style.visibility = 'hidden';
			document.getElementById("Company_Name_50").style.visibility = 'hidden';
			document.getElementById("Fulfillment_Center_Name_20").style.visibility = 'hidden';
			document.getElementById("Fulfillment_Center_Name_50").style.visibility = 'hidden';
			document.getElementById("PurchaseOrdersRequireApproval").style.visibility = 'hidden';
			document.getElementById("Team_Member").style.visibility = 'hidden';
			document.getElementById("Tenant_Language").style.visibility = 'hidden';
			document.getElementById("Team_Member_Language").style.visibility = 'hidden';
			document.getElementById("Team_Member_Name").style.visibility = 'hidden';
			document.getElementById("Authority_Profile").style.visibility = 'hidden';
			document.getElementById("Menu_Profile_Tablet").style.visibility = 'hidden';
			document.getElementById("Menu_Profile_Station").style.visibility = 'hidden';
			document.getElementById("Menu_Profile_RF").style.visibility = 'hidden';
			document.getElementById("Menu_Profile_Glass").style.visibility = 'hidden';
			document.getElementById("Equipment_Certification").style.visibility = 'hidden';
			document.getElementById("Theme_Mobile").style.visibility = 'hidden';
			document.getElementById("Theme_RF").style.visibility = 'hidden';
			document.getElementById("Theme_Full_Display").style.visibility = 'hidden';
			document.getElementById("FulfillmentCenter").style.visibility = 'hidden';
			document.getElementById("Area").style.visibility = 'hidden';
			document.getElementById("Location").style.visibility = 'hidden';
			document.getElementById("LocationFlag").style.visibility = 'hidden';
			document.getElementById("Product").style.visibility = 'hidden';
			document.getElementById("Quality").style.visibility = 'hidden';
			document.getElementById("Lot").style.visibility = 'hidden';
			document.getElementById("Serial").style.visibility = 'hidden';
			document.getElementById("Serial01").style.visibility = 'hidden';
			document.getElementById("Serial02").style.visibility = 'hidden';
			document.getElementById("Serial03").style.visibility = 'hidden';
			document.getElementById("Serial04").style.visibility = 'hidden';
			document.getElementById("Serial05").style.visibility = 'hidden';
			document.getElementById("Serial06").style.visibility = 'hidden';
			document.getElementById("Serial07").style.visibility = 'hidden';
			document.getElementById("Serial08").style.visibility = 'hidden';
			document.getElementById("Serial09").style.visibility = 'hidden';
			document.getElementById("Serial10").style.visibility = 'hidden';
			document.getElementById("Serial11").style.visibility = 'hidden';
			document.getElementById("Serial12").style.visibility = 'hidden';
			document.getElementById("LPN").style.visibility = 'hidden';
			document.getElementById("InventroryControl01").style.visibility = 'hidden';
			document.getElementById("InventroryControl02").style.visibility = 'hidden';
			document.getElementById("InventroryControl03").style.visibility = 'hidden';
			document.getElementById("InventroryControl04").style.visibility = 'hidden';
			document.getElementById("InventroryControl05").style.visibility = 'hidden';
			document.getElementById("InventroryControl06").style.visibility = 'hidden';
			document.getElementById("InventroryControl07").style.visibility = 'hidden';
			document.getElementById("InventroryControl08").style.visibility = 'hidden';
			document.getElementById("InventroryControl09").style.visibility = 'hidden';
			document.getElementById("InventroryControl10").style.visibility = 'hidden';
			document.getElementById("Quantity_being_adjusted").style.visibility = 'hidden';
			document.getElementById("Adjustment_Quantity").style.visibility = 'hidden';
			document.getElementById("Adjusted_Quantity").style.visibility = 'hidden';
			document.getElementById("Task").style.visibility = 'hidden';
			document.getElementById("WorkType").style.visibility = 'hidden';
			document.getElementById("WorkControlNumber").style.visibility = 'hidden';
			document.getElementById("UnitofMeasureCode").style.visibility = 'hidden';
			document.getElementById("UnitofMeasureQty").style.visibility = 'hidden';
			document.getElementById("ProductValue").style.visibility = 'hidden';
			document.getElementById("PurchaseOrderNumber").style.visibility = 'hidden';
			document.getElementById("Reference").style.visibility = 'hidden';
			document.getElementById("InventoryValue").style.visibility = 'hidden';
			document.getElementById("InventoryPickSequence").style.visibility = 'hidden';
			document.getElementById("CasePackQty").style.visibility = 'hidden';
			document.getElementById("Language").style.visibility = 'hidden';
			document.getElementById("InfoHelp").style.visibility = 'hidden';
			document.getElementById("InfoHelpType").style.visibility = 'hidden';
			document.getElementById("Attribute_Request").style.visibility = 'hidden';
			document.getElementById("Drop_Down_Selection").style.visibility = 'hidden';
			document.getElementById("GeneralCode").style.visibility = 'hidden';
			document.getElementById("GeneralCodeID").style.visibility = 'hidden';
			document.getElementById("KeyPhrase").style.visibility = 'hidden';
			document.getElementById("AddressLine1").style.visibility = 'hidden';
			document.getElementById("AddressLine2").style.visibility = 'hidden';
			document.getElementById("AddressLine3").style.visibility = 'hidden';
			document.getElementById("AddressLine4").style.visibility = 'hidden';
			document.getElementById("City").style.visibility = 'hidden';
			document.getElementById("StateCode").style.visibility = 'hidden';
			document.getElementById("CountryCode").style.visibility = 'hidden';
			document.getElementById("ZipCode").style.visibility = 'hidden';

			document.getElementById("lblTenant").style.visibility = 'hidden';
			document.getElementById("lblCompany").style.visibility = 'hidden';
			document.getElementById("lblCompany_Name_20").style.visibility = 'hidden';
			document.getElementById("lblCompany_Name_50").style.visibility = 'hidden';
			document.getElementById("lblFulfillment_Center_Name_20").style.visibility = 'hidden';
			document.getElementById("lblFulfillment_Center_Name_50").style.visibility = 'hidden';
			document.getElementById("lblPurchaseOrdersRequireApproval").style.visibility = 'hidden';
			document.getElementById("lblTeam_Member").style.visibility = 'hidden';
			document.getElementById("lblTenant_Language").style.visibility = 'hidden';
			document.getElementById("lblTeam_Member_Language").style.visibility = 'hidden';
			document.getElementById("lblTeam_Member_Name").style.visibility = 'hidden';
			document.getElementById("lblAuthority_Profile").style.visibility = 'hidden';
			document.getElementById("lblMenu_Profile_Tablet").style.visibility = 'hidden';
			document.getElementById("lblMenu_Profile_Station").style.visibility = 'hidden';
			document.getElementById("lblMenu_Profile_RF").style.visibility = 'hidden';
			document.getElementById("lblMenu_Profile_Glass").style.visibility = 'hidden';
			document.getElementById("lblEquipment_Certification").style.visibility = 'hidden';
			document.getElementById("lblTheme_Mobile").style.visibility = 'hidden';
			document.getElementById("lblTheme_RF").style.visibility = 'hidden';
			document.getElementById("lblTheme_Full_Display").style.visibility = 'hidden';
			document.getElementById("lblFulfillmentCenter").style.visibility = 'hidden';
			document.getElementById("lblArea").style.visibility = 'hidden';
			document.getElementById("lblLocation").style.visibility = 'hidden';
			document.getElementById("lblLocationFlag").style.visibility = 'hidden';
			document.getElementById("lblProduct").style.visibility = 'hidden';
			document.getElementById("lblQuality").style.visibility = 'hidden';
			document.getElementById("lblLot").style.visibility = 'hidden';
			document.getElementById("lblSerial").style.visibility = 'hidden';
			document.getElementById("lblSerial01").style.visibility = 'hidden';
			document.getElementById("lblSerial02").style.visibility = 'hidden';
			document.getElementById("lblSerial03").style.visibility = 'hidden';
			document.getElementById("lblSerial04").style.visibility = 'hidden';
			document.getElementById("lblSerial05").style.visibility = 'hidden';
			document.getElementById("lblSerial06").style.visibility = 'hidden';
			document.getElementById("lblSerial07").style.visibility = 'hidden';
			document.getElementById("lblSerial08").style.visibility = 'hidden';
			document.getElementById("lblSerial09").style.visibility = 'hidden';
			document.getElementById("lblSerial10").style.visibility = 'hidden';
			document.getElementById("lblSerial11").style.visibility = 'hidden';
			document.getElementById("lblSerial12").style.visibility = 'hidden';
			document.getElementById("lblLPN").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl01").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl02").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl03").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl04").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl05").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl06").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl07").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl08").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl09").style.visibility = 'hidden';
			document.getElementById("lblInventroryControl10").style.visibility = 'hidden';
			document.getElementById("lblQuantity_being_adjusted").style.visibility = 'hidden';
			document.getElementById("lblAdjustment_Quantity").style.visibility = 'hidden';
			document.getElementById("lblAdjusted_Quantity").style.visibility = 'hidden';
			document.getElementById("lblTask").style.visibility = 'hidden';
			document.getElementById("lblWorkType").style.visibility = 'hidden';
			document.getElementById("lblWorkControlNumber").style.visibility = 'hidden';
			document.getElementById("lblUnitofMeasureCode").style.visibility = 'hidden';
			document.getElementById("lblUnitofMeasureQty").style.visibility = 'hidden';
			document.getElementById("lblProductValue").style.visibility = 'hidden';
			document.getElementById("lblPurchaseOrderNumber").style.visibility = 'hidden';
			document.getElementById("lblReference").style.visibility = 'hidden';
			document.getElementById("lblInventoryValue").style.visibility = 'hidden';
			document.getElementById("lblInventoryPickSequence").style.visibility = 'hidden';
			document.getElementById("lblCasePackQty").style.visibility = 'hidden';
			document.getElementById("lblLanguage").style.visibility = 'hidden';
			document.getElementById("lblInfoHelp").style.visibility = 'hidden';
			document.getElementById("lblInfoHelpType").style.visibility = 'hidden';
			document.getElementById("lblAttribute_Request").style.visibility = 'hidden';
			document.getElementById("lblDrop_Down_Selection").style.visibility = 'hidden';
			document.getElementById("lblGeneralCode").style.visibility = 'hidden';
			document.getElementById("lblGeneralCodeID").style.visibility = 'hidden';
			document.getElementById("lblKeyPhrase").style.visibility = 'hidden';
			document.getElementById("lblAddressLine1").style.visibility = 'hidden';
			document.getElementById("lblAddressLine2").style.visibility = 'hidden';
			document.getElementById("lblAddressLine3").style.visibility = 'hidden';
			document.getElementById("lblAddressLine4").style.visibility = 'hidden';
			document.getElementById("lblCity").style.visibility = 'hidden';
			document.getElementById("lblStateCode").style.visibility = 'hidden';
			document.getElementById("lblCountryCode").style.visibility = 'hidden';
			document.getElementById("lblZipCode").style.visibility = 'hidden';

		}
	</SCRIPT>

	<form:form action="testcommonclasses" method="post"
		commandName="testcommonclassesvalidate">
		<br>
		<br>
		<label id="lblTenant">Tenant:- </label>
		<input type="text" id="Tenant" name="Tenant" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
<label id="lblCompany">Company:- </label>
		<input type="text" id="Company" name="Company" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblCompany_Name_20">Company_Name_20:- </label>
		<input type="text" id="Company_Name_20" name="Company_Name_20" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblCompany_Name_50">Company_Name_50:- </label>
		<input type="text" id="Company_Name_50" name="Company_Name_50" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblFulfillment_Center_Name_20">Fulfillment_Center_Name_20:-
		</label>
		<input type="text" id="Fulfillment_Center_Name_20"
			name="Fulfillment_Center_Name_20" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblFulfillment_Center_Name_50">Fulfillment_Center_Name_50:-
		</label>
		<input type="text" id="Fulfillment_Center_Name_50"
			name="Fulfillment_Center_Name_50" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblPurchaseOrdersRequireApproval">PurchaseOrdersRequireApproval:-
		</label>
		<input type="text" id="PurchaseOrdersRequireApproval"
			name="PurchaseOrdersRequireApproval" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTeam_Member">Team_Member:- </label>
		<input type="text" id="Team_Member" name="Team_Member" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTenant_Language">Tenant_Language:- </label>
		<input type="text" id="Tenant_Language" name="Tenant_Language" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTeam_Member_Language">Team_Member_Language:- </label>
		<input type="text" id="Team_Member_Language"
			name="Team_Member_Language" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTeam_Member_Name">Team_Member_Name:- </label>
		<input type="text" id="Team_Member_Name" name="Team_Member_Name" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAuthority_Profile">Authority_Profile:- </label>
		<input type="text" id="Authority_Profile" name="Authority_Profile" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblMenu_Profile_Tablet">Menu_Profile_Tablet:- </label>
		<input type="text" id="Menu_Profile_Tablet" name="Menu_Profile_Tablet" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblMenu_Profile_Station">Menu_Profile_Station:- </label>
		<input type="text" id="Menu_Profile_Station"
			name="Menu_Profile_Station" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblMenu_Profile_RF">Menu_Profile_RF:- </label>
		<input type="text" id="Menu_Profile_RF" name="Menu_Profile_RF" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblMenu_Profile_Glass">Menu_Profile_Glass:- </label>
		<input type="text" id="Menu_Profile_Glass" name="Menu_Profile_Glass" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblEquipment_Certification">Equipment_Certification:-
		</label>
		<input type="text" id="Equipment_Certification"
			name="Equipment_Certification" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTheme_Mobile">Theme_Mobile:- </label>
		<input type="text" id="Theme_Mobile" name="Theme_Mobile" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTheme_RF">Theme_RF:- </label>
		<input type="text" id="Theme_RF" name="Theme_RF" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTheme_Full_Display">Theme_Full_Display:- </label>
		<input type="text" id="Theme_Full_Display" name="Theme_Full_Display" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblFulfillmentCenter">FulfillmentCenter:- </label>
		<input type="text" id="FulfillmentCenter" name="FulfillmentCenter" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblArea">Area:- </label>
		<input type="text" id="Area" name="Area" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblLocation">Location:- </label>
		<input type="text" id="Location" name="Location" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblLocationFlag">LocationFlag:- </label>
		<input type="text" id="LocationFlag" name="LocationFlag" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblProduct">Product:- </label>
		<input type="text" id="Product" name="Product" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblQuality">Quality:- </label>
		<input type="text" id="Quality" name="Quality" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblLot">Lot:- </label>
		<input type="text" id="Lot" name="Lot" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial">Serial:- </label>
		<input type="text" id="Serial" name="Serial" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial01">Serial01:- </label>
		<input type="text" id="Serial01" name="Serial01" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial02">Serial02:- </label>
		<input type="text" id="Serial02" name="Serial02" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial03">Serial03:- </label>
		<input type="text" id="Serial03" name="Serial03" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial04">Serial04:- </label>
		<input type="text" id="Serial04" name="Serial04" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial05">Serial05:- </label>
		<input type="text" id="Serial05" name="Serial05" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial06">Serial06:- </label>
		<input type="text" id="Serial06" name="Serial06" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial07">Serial07:- </label>
		<input type="text" id="Serial07" name="Serial07" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial08">Serial08:- </label>
		<input type="text" id="Serial08" name="Serial08" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial09">Serial09:- </label>
		<input type="text" id="Serial09" name="Serial09" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial10">Serial10:- </label>
		<input type="text" id="Serial10" name="Serial10" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial11">Serial11:- </label>
		<input type="text" id="Serial11" name="Serial11" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblSerial12">Serial12:- </label>
		<input type="text" id="Serial12" name="Serial12" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblLPN">LPN:- </label>
		<input type="text" id="LPN" name="LPN" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl01">InventroryControl01:- </label>
		<input type="text" id="InventroryControl01" name="InventroryControl01" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl02">InventroryControl02:- </label>
		<input type="text" id="InventroryControl02" name="InventroryControl02" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl03">InventroryControl03:- </label>
		<input type="text" id="InventroryControl03" name="InventroryControl03" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl04">InventroryControl04:- </label>
		<input type="text" id="InventroryControl04" name="InventroryControl04" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl05">InventroryControl05:- </label>
		<input type="text" id="InventroryControl05" name="InventroryControl05" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl06">InventroryControl06:- </label>
		<input type="text" id="InventroryControl06" name="InventroryControl06" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl07">InventroryControl07:- </label>
		<input type="text" id="InventroryControl07" name="InventroryControl07" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl08">InventroryControl08:- </label>
		<input type="text" id="InventroryControl08" name="InventroryControl08" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl09">InventroryControl09:- </label>
		<input type="text" id="InventroryControl09" name="InventroryControl09" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventroryControl10">InventroryControl10:- </label>
		<input type="text" id="InventroryControl10" name="InventroryControl10" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblQuantity_being_adjusted">Quantity_being_adjusted:-
		</label>
		<input type="text" id="Quantity_being_adjusted"
			name="Quantity_being_adjusted" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAdjustment_Quantity">Adjustment_Quantity:- </label>
		<input type="text" id="Adjustment_Quantity" name="Adjustment_Quantity" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAdjusted_Quantity">Adjusted_Quantity:- </label>
		<input type="text" id="Adjusted_Quantity" name="Adjusted_Quantity" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblTask">Task:- </label>
		<input type="text" id="Task" name="Task" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblWorkType">WorkType:- </label>
		<input type="text" id="WorkType" name="WorkType" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblWorkControlNumber">WorkControlNumber:- </label>
		<input type="text" id="WorkControlNumber" name="WorkControlNumber" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblUnitofMeasureCode">UnitofMeasureCode:- </label>
		<input type="text" id="UnitofMeasureCode" name="UnitofMeasureCode" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblUnitofMeasureQty">UnitofMeasureQty:- </label>
		<input type="text" id="UnitofMeasureQty" name="UnitofMeasureQty" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblProductValue">ProductValue:- </label>
		<input type="text" id="ProductValue" name="ProductValue" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblPurchaseOrderNumber">PurchaseOrderNumber:- </label>
		<input type="text" id="PurchaseOrderNumber" name="PurchaseOrderNumber" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblReference">Reference:- </label>
		<input type="text" id="Reference" name="Reference" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventoryValue">InventoryValue:- </label>
		<input type="text" id="InventoryValue" name="InventoryValue" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInventoryPickSequence">InventoryPickSequence:- </label>
		<input type="text" id="InventoryPickSequence"
			name="InventoryPickSequence" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblCasePackQty">CasePackQty:- </label>
		<input type="text" id="CasePackQty" name="CasePackQty" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblLanguage">Language:- </label>
		<input type="text" id="Language" name="Language" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInfoHelp">InfoHelp:- </label>
		<input type="text" id="InfoHelp" name="InfoHelp" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblInfoHelpType">InfoHelpType:- </label>
		<input type="text" id="InfoHelpType" name="InfoHelpType" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAttribute_Request">Attribute_Request:- </label>
		<input type="text" id="Attribute_Request" name="Attribute_Request" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblDrop_Down_Selection">Drop_Down_Selection:- </label>
		<input type="text" id="Drop_Down_Selection" name="Drop_Down_Selection" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblGeneralCode">GeneralCode:- </label>
		<input type="text" id="GeneralCode" name="GeneralCode" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblGeneralCodeID">GeneralCodeID:- </label>
		<input type="text" id="GeneralCodeID" name="GeneralCodeID" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblKeyPhrase">KeyPhrase:- </label>
		<input type="text" id="KeyPhrase" name="KeyPhrase" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAddressLine1">AddressLine1* :- </label>
		<input type="text" id="AddressLine1" name="AddressLine1"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAddressLine2">AddressLine2:- </label>
		<input type="text" id="AddressLine2" name="AddressLine2" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAddressLine3">AddressLine3:- </label>
		<input type="text" id="AddressLine3" name="AddressLine3" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblAddressLine4">AddressLine4:- </label>
		<input type="text" id="AddressLine4" name="AddressLine4" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblCity">City:- </label>
		<input type="text" id="City" name="City" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblStateCode">StateCode:- </label>
		<input type="text" id="StateCode" name="StateCode" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblCountryCode">CountryCode:- </label>
		<input type="text" id="CountryCode" name="CountryCode" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<label id="lblZipCode">ZipCode:- </label>
		<input type="text" id="ZipCode" name="ZipCode" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<br>
		<br>
		<button type="submit" value="Submit">Submit</button>
	</form:form>



	<c:choose>
		<c:when test="${FulfillmentCenter != null}">

			<TABLE cellpadding="10" border="1" style="background-color: #ffffcc;">
				<TR>
					<TH>Tenant</TH>
					<TH>FulfillmentCenter</TH>
					<TH>Name20</TH>
					<TH>Name50</TH>
					<TH>AddressLine1</TH>
					<TH>AddressLine2</TH>
					<TH>AddressLine3</TH>
					<TH>AddressLine4</TH>
					<TH>City</TH>
					<TH>StateCode</TH>
					<TH>CountryCode</TH>
					<TH>ZipCode</TH>
					<TH>ContactName1</TH>
					<TH>ContactPhone1</TH>
					<TH>ContactExtension1</TH>
					<TH>ContactCell1</TH>
					<TH>ContactFax1</TH>
					<TH>ContactEmail1</TH>
					<TH>ContactName2</TH>
					<TH>ContactPhone2</TH>
					<TH>ContactExtension2</TH>
					<TH>ContactCell2</TH>
					<TH>ContactFax2</TH>
					<TH>ContactEmail2</TH>
					<TH>ContactName3</TH>
					<TH>ContactPhone3</TH>
					<TH>ContactExtension3</TH>
					<TH>ContactCell3</TH>
					<TH>ContactFax3</TH>
					<TH>ContactEmail3</TH>
					<TH>ContactName4</TH>
					<TH>ContactPhone4</TH>
					<TH>ContactExtension4</TH>
					<TH>ContactCell4</TH>
					<TH>ContactFax4</TH>
					<TH>ContactEmail4</TH>
					<TH>MainFax</TH>
					<TH>MainEmail</TH>
					<TH>SetupSelected</TH>
					<TH>SetupDate</TH>
					<TH>SetupBy</TH>
					<TH>LastActivityDate</TH>
					<TH>LastActivityTeamMember</TH>
					<TH>LastActivityTask</TH>
				</TR>
				<TR>
					<TD>${FulfillmentCenter.getTenant()}</TD>
					<TD>${FulfillmentCenter.getFulfillmentCenter()}</TD>
					<TD>${FulfillmentCenter.getName20()}</TD>
					<TD>${FulfillmentCenter.getName50()}</TD>
					<TD>${FulfillmentCenter.getAddressLine1()}</TD>
					<TD>${FulfillmentCenter.getAddressLine2()}</TD>
					<TD>${FulfillmentCenter.getAddressLine3()}</TD>
					<TD>${FulfillmentCenter.getAddressLine4()}</TD>
					<TD>${FulfillmentCenter.getCity()}</TD>
					<TD>${FulfillmentCenter.getStateCode()}</TD>
					<TD>${FulfillmentCenter.getCountryCode()}</TD>
					<TD>${FulfillmentCenter.getZipCode()}</TD>
					<TD>${FulfillmentCenter.getContactName1()}</TD>
					<TD>${FulfillmentCenter.getContactPhone1()}</TD>
					<TD>${FulfillmentCenter.getContactExtension1()}</TD>
					<TD>${FulfillmentCenter.getContactCell1()}</TD>
					<TD>${FulfillmentCenter.getContactFax1()}</TD>
					<TD>${FulfillmentCenter.getContactEmail1()}</TD>
					<TD>${FulfillmentCenter.getContactName2()}</TD>
					<TD>${FulfillmentCenter.getContactPhone2()}</TD>
					<TD>${FulfillmentCenter.getContactExtension2()}</TD>
					<TD>${FulfillmentCenter.getContactCell2()}</TD>
					<TD>${FulfillmentCenter.getContactFax2()}</TD>
					<TD>${FulfillmentCenter.getContactEmail2()}</TD>
					<TD>${FulfillmentCenter.getContactName3()}</TD>
					<TD>${FulfillmentCenter.getContactPhone3()}</TD>
					<TD>${FulfillmentCenter.getContactExtension3()}</TD>
					<TD>${FulfillmentCenter.getContactCell3()}</TD>
					<TD>${FulfillmentCenter.getContactFax3()}</TD>
					<TD>${FulfillmentCenter.getContactEmail3()}</TD>
					<TD>${FulfillmentCenter.getContactName4()}</TD>
					<TD>${FulfillmentCenter.getContactPhone4()}</TD>
					<TD>${FulfillmentCenter.getContactExtension4()}</TD>
					<TD>${FulfillmentCenter.getContactCell4()}</TD>
					<TD>${FulfillmentCenter.getContactFax4()}</TD>
					<TD>${FulfillmentCenter.getContactEmail4()}</TD>
					<TD>${FulfillmentCenter.getMainFax()}</TD>
					<TD>${FulfillmentCenter.getMainEmail()}</TD>
					<TD>${FulfillmentCenter.getSetUpSelected()}</TD>
					<TD>${FulfillmentCenter.getSetUpDate()}</TD>
					<TD>${FulfillmentCenter.getSetUpBy()}</TD>
					<TD>${FulfillmentCenter.getLastActivityDate()}</TD>
					<TD>${FulfillmentCenter.getLastActivityTeamMember()}</TD>
					<TD>${FulfillmentCenter.getLastActivityTask()}</TD>

				</TR>

			</TABLE>

		</c:when>
		<c:when test="${TeammemberLookup != null}">

			<TABLE cellpadding="10" border="1" style="background-color: #ffffcc;">
				<TR>
					<TH>Tenant</TH>
					<TH>TeamMember</TH>
					<TH>FulfillmentCenter</TH>
					<TH>Department</TH>
					<TH>LastName</TH>
					<TH>FirstName</TH>
					<TH>MiddleName</TH>
					<TH>AddressLine1</TH>
					<TH>AddressLine2</TH>
					<TH>AddressLine3</TH>
					<TH>AddressLine4</TH>
					<TH>City</TH>
					<TH>StateCode</TH>
					<TH>CountryCode</TH>
					<TH>ZipCode</TH>
					<TH>WorkPhone</TH>
					<TH>WorkExtension</TH>
					<TH>WorkCell</TH>
					<TH>WorkFax</TH>
					<TH>HomePhone</TH>
					<TH>Cell</TH>
					<TH>EmergencyContactName</TH>
					<TH>EmergencyContactHomePhone</TH>
					<TH>EmergencyContactCell</TH>
					<TH>ShiftCode</TH>
					<TH>SystemUse</TH>
					<TH>AuthorityProfile</TH>
					<TH>TaskProfile</TH>
					<TH>Menu_Profile_Glass</TH>
					<TH>Menu_Profile_Tablet</TH>
					<TH>Menu_Profile_Mobile</TH>
					<TH>Menu_Profile_Station</TH>
					<TH>Menu_Profile_RF</TH>
					<TH>EquipmentCertification</TH>
					<TH>Langage</TH>
					<TH>StartDate</TH>
					<TH>LastDate</TH>
					<TH>SetupDate</TH>
					<TH>SetupBy</TH>
					<TH>LastActivityDate</TH>
					<TH>LastActivityTeamMember</TH>
					<TH>CurrentStatus</TH>

				</TR>
				<TR>
					<TD>${TeammemberLookup.getTenant()}</TD>
					<TD>${TeammemberLookup.getTeamMember ()}</TD>
					<TD>${TeammemberLookup.getFulfillmentCenter()}</TD>
					<TD>${TeammemberLookup.getDepartment()}</TD>
					<TD>${TeammemberLookup.getLastName()}</TD>
					<TD>${TeammemberLookup.getFirstName()}</TD>
					<TD>${TeammemberLookup.getMiddleName ()}</TD>
					<TD>${TeammemberLookup.getAddressLine1()}</TD>
					<TD>${TeammemberLookup.getAddressLine2()}</TD>
					<TD>${TeammemberLookup.getAddressLine3()}</TD>
					<TD>${TeammemberLookup.getAddressLine4()}</TD>
					<TD>${TeammemberLookup.getCity()}</TD>
					<TD>${TeammemberLookup.getStateCode()}</TD>
					<TD>${TeammemberLookup.getCountryCode()}</TD>
					<TD>${TeammemberLookup.getZipCode()}</TD>
					<TD>${TeammemberLookup.getWorkPhone()}</TD>
					<TD>${TeammemberLookup.getWorkExtension()}</TD>
					<TD>${TeammemberLookup.getWorkCell()}</TD>
					<TD>${TeammemberLookup.getWorkFax()}</TD>
					<TD>${TeammemberLookup.getHomePhone()}</TD>
					<TD>${TeammemberLookup.getCell()}</TD>
					<TD>${TeammemberLookup.getEmergencyContactName()}</TD>
					<TD>${TeammemberLookup.getEmergencyContactHomePhone()}</TD>
					<TD>${TeammemberLookup.getEmergencyContactCell()}</TD>
					<TD>${TeammemberLookup.getShiftCode()}</TD>
					<TD>${TeammemberLookup.getSystemUse()}</TD>
					<TD>${TeammemberLookup.getAuthorityProfile()}</TD>
					<TD>${TeammemberLookup.getTaskProfile()}</TD>
					<TD>${TeammemberLookup.getMenu_Profile_Glass()}</TD>
					<TD>${TeammemberLookup.getMenu_Profile_Tablet()}</TD>
					<TD>${TeammemberLookup.getMenu_Profile_Mobile()}</TD>
					<TD>${TeammemberLookup.getMenu_Profile_Station()}</TD>
					<TD>${TeammemberLookup.getMenu_Profile_RF()}</TD>
					<TD>${TeammemberLookup.getEquipmentCertification()}</TD>
					<TD>${TeammemberLookup.getLANGUAGE()}</TD>
					<TD>${TeammemberLookup.getStartDate()}</TD>
					<TD>${TeammemberLookup.getLastDate()}</TD>
					<TD>${TeammemberLookup.getSetupDate()}</TD>
					<TD>${TeammemberLookup.getSetupBy()}</TD>
					<TD>${TeammemberLookup.getLastActivityDate()}</TD>
					<TD>${TeammemberLookup.getLastActivityTeamMember()}</TD>
					<TD>${TeammemberLookup.getCurrentStatus()}</TD>

				</TR>
			</TABLE>
		</c:when>
		<c:when test="${Addressvalidation != null}">
          Addressvalidation is ${Addressvalidation}
           </c:when>
		<c:when test="${InfoHelp != null}">

			<TABLE cellpadding="10" border="1" style="background-color: #ffffcc;">
				<TR>
					<TH>InfoHelp</TH>
					<TH>Language</TH>
					<TH>InfoHelpType</TH>
					<TH>Description20</TH>
					<TH>Description50</TH>
					<TH>Execution</TH>
					<TH>LastActivityDate</TH>
					<TH>LastActivityTeamMember</TH>
				</TR>
				<TR>
					<TD>${InfoHelp.getInfoHelp()}</TD>
					<TD>${InfoHelp.getLanguage()}</TD>
					<TD>${InfoHelp.getInfoHelpType()}</TD>
					<TD>${InfoHelp.getDescription20()}</TD>
					<TD>${InfoHelp.getDescription50()}</TD>
					<TD>${InfoHelp.getExecution()}</TD>
					<TD>${InfoHelp.getLastActivityDate()}</TD>
					<TD>${InfoHelp.getLastActivityTeamMember()}</TD>

				</TR>
			</TABLE>
		</c:when>
		<c:when test="${Language != null}">
			<TABLE cellpadding="10" border="1" style="background-color: #ffffcc;">
				<TR>
					<TH>Langage</TH>
					<TH>KeyPhrase</TH>
					<TH>Translation</TH>
					<TH>LastActivityDate</TH>
					<TH>LastActivityTeamMember</TH>
				</TR>
				<TR>
					<TD>${Language.getLanguage()}</TD>
					<TD>${Language.getKeyPhrase()}</TD>
					<TD>${Language.getTranslation()}</TD>
					<TD>${Language.getLastActivityDate()}</TD>
					<TD>${Language.getLastActivityTeamMember()}</TD>
				</TR>
			</TABLE>
		</c:when>
		<c:when
			test="${!empty LookupGeneralCodeList && Attrebutes_Request == \"Y\"}">
			<TABLE cellpadding="10" border="1" style="background-color: #ffffcc;">
				<TR>
					<TH>Tenant</TH>
					<TH>Company</TH>
					<TH>Application</TH>
					<TH>GeneralCodeID</TH>
					<TH>GeneralCode</TH>
					<TH>SystemUse</TH>
					<TH>Description20</TH>
					<TH>Description50</TH>
					<TH>MenuOptionName</TH>
					<TH>ControlNumber01</TH>
					<TH>ControlNumber02</TH>
					<TH>ControlNumber03</TH>
					<TH>ControlNumber04</TH>
					<TH>ControlNumber05</TH>
					<TH>ControlNumber06</TH>
					<TH>ControlNumber07</TH>
					<TH>ControlNumber08</TH>
					<TH>ControlNumber09</TH>
					<TH>ControlNumber10</TH>
					<TH>Control01Description</TH>
					<TH>Control01Value</TH>
					<TH>Control02Description</TH>
					<TH>Control02Value</TH>
					<TH>Control03Description</TH>
					<TH>Control03Value</TH>
					<TH>Control04Description</TH>
					<TH>Control04Value</TH>
					<TH>Control05Description</TH>
					<TH>Control05Value</TH>
					<TH>Control06Description</TH>
					<TH>Control06Value</TH>
					<TH>Control07Description</TH>
					<TH>Control07Value</TH>
					<TH>Control08Description</TH>
					<TH>Control08Value</TH>
					<TH>Control09Description</TH>
					<TH>Control09Value</TH>
					<TH>Control10Description</TH>
					<TH>Control10Value</TH>
					<TH>Helpline1</TH>
					<TH>Helpline2</TH>
					<TH>Helpline3</TH>
					<TH>Helpline4</TH>
					<TH>Helpline5</TH>
					<TH>LastActivityDate</TH>
					<TH>LastActivityTeamMember</TH>
				</TR>
				<c:forEach items="${LookupGeneralCodeList}" var="LookupGeneralCode">
					<TR>
						<TD>${LookupGeneralCode.getTenant()}</TD>
						<TD>${LookupGeneralCode.getCompany()}</TD>
						<TD>${LookupGeneralCode.getApplication()}</TD>
						<TD>${LookupGeneralCode.getGeneralCodeID()}</TD>
						<TD>${LookupGeneralCode.getGeneralCode()}</TD>
						<TD>${LookupGeneralCode.getSystemUse()}</TD>
						<TD>${LookupGeneralCode.getDescription20()}</TD>
						<TD>${LookupGeneralCode.getDescription50()}</TD>
						<TD>${LookupGeneralCode.getMenuOptionName()}</TD>
						<TD>${LookupGeneralCode.getControlNumber01()}</TD>
						<TD>${LookupGeneralCode.getControlNumber02()}</TD>
						<TD>${LookupGeneralCode.getControlNumber03()}</TD>
						<TD>${LookupGeneralCode.getControlNumber04()}</TD>
						<TD>${LookupGeneralCode.getControlNumber05()}</TD>
						<TD>${LookupGeneralCode.getControlNumber06()}</TD>
						<TD>${LookupGeneralCode.getControlNumber07()}</TD>
						<TD>${LookupGeneralCode.getControlNumber08()}</TD>
						<TD>${LookupGeneralCode.getControlNumber09()}</TD>
						<TD>${LookupGeneralCode.getControlNumber10()}</TD>
						<TD>${LookupGeneralCode.getControl01Description()}</TD>
						<TD>${LookupGeneralCode.getControl01Value()}</TD>
						<TD>${LookupGeneralCode.getControl02Description()}</TD>
						<TD>${LookupGeneralCode.getControl02Value()}</TD>
						<TD>${LookupGeneralCode.getControl03Description()}</TD>
						<TD>${LookupGeneralCode.getControl03Value()}</TD>
						<TD>${LookupGeneralCode.getControl04Description()}</TD>
						<TD>${LookupGeneralCode.getControl04Value()}</TD>
						<TD>${LookupGeneralCode.getControl05Description()}</TD>
						<TD>${LookupGeneralCode.getControl05Value()}</TD>
						<TD>${LookupGeneralCode.getControl06Description()}</TD>
						<TD>${LookupGeneralCode.getControl06Value()}</TD>
						<TD>${LookupGeneralCode.getControl07Description()}</TD>
						<TD>${LookupGeneralCode.getControl07Value()}</TD>
						<TD>${LookupGeneralCode.getControl08Description()}</TD>
						<TD>${LookupGeneralCode.getControl08Value()}</TD>
						<TD>${LookupGeneralCode.getControl09Description()}</TD>
						<TD>${LookupGeneralCode.getControl09Value()}</TD>
						<TD>${LookupGeneralCode.getControl10Description()}</TD>
						<TD>${LookupGeneralCode.getControl10Value()}</TD>
						<TD>${LookupGeneralCode.getHelpline1()}</TD>
						<TD>${LookupGeneralCode.getHelpline2()}</TD>
						<TD>${LookupGeneralCode.getHelpline3()}</TD>
						<TD>${LookupGeneralCode.getHelpline4()}</TD>
						<TD>${LookupGeneralCode.getHelpline5()}</TD>
						<TD>${LookupGeneralCode.getLastActivityDate()}</TD>
						<TD>${LookupGeneralCode.getLastActivityTeamMember()}</TD>

					</TR>
				</c:forEach>
			</TABLE>
		</c:when>
		<c:when
			test="${Attrebutes_Request == \"N\"}">
			GENERAL CODES : <select name="GENERAL CODES" style="width: 150px">
				<option>Select</option>
				<c:forEach items="${LookupGeneralCodeList}" var="LookupGeneralCode">
					<option>${LookupGeneralCode.getGeneralCode()}</option>
				</c:forEach>
			</select>
		</c:when>
		<c:when test="${zipvalidation != null}">
           Zipvalidation is  ${zipvalidation}
           </c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>


	${message}

</body>
</html>
