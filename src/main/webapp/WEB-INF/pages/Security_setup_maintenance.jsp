<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<%@ page
	import="java.util.Properties,java.io.IOException,com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIES,com.jasci.biz.AdminModule.model.COMPANIES,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.*"%>
<!DOCTYPE html>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${viewLabels.getLblSecurityTitle()}</h1>
					</div>
				</div>


				
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content hideHorizontalScrollbar" id="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					${viewLabels.getLblSecurityTitle()}
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div id="ErrorMessage" class="note note-danger"
								style="display: none;">
								
							</div>
							<div class="form-group">
									<label class="col-md-2 control-label" style="color:#000000; display:inline; width: 100%; font-weight:600; font-size: 15px;margin-bottom: 17px;">${viewLabels.getSecurity_labels_TeamMemberName()}:&nbsp;${FullName} </label>
								</div>
					
					<div class="col-md-12">
					<form:form name="myForm" class="form-horizontal form-row-seperated" onsubmit="return body_load();" action="#"  modelAttribute="Security" method="POST">
						<div class="portlet">
							<input type="hidden" path="PasswordDate" name="PasswordDate"  value="${Security.getPasswordDate()}">
							<input type="hidden" name="TeamMember" path="TeamMember" value="${Security.getTeamMember()}">
							<input type="hidden" name="Status" path="Status" value="${Security.getStatus()}">
								<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body" style="width:105%">
											<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLAstActivityDate()}:
													
													</label>
													<div class="col-md-10">
														<form:input path="LastActivityDate" type="text" class="form-controlwidth"  id="LastActivityDate" name="LastActivityDate" readonly="true"/>
														<input id="LastActivityDateStringH" type="hidden" class="form-controlwidth" name="LastActivityDateStringH" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLastActivityBy()}:
													
													</label>
													<div class="col-md-10">
													<form:input path="LastActivityTeamMember" type="text" class="form-controlwidth"  value="${Security.getLastActivityTeamMember()}" name="LastActivityTeamMember"  readonly="true"/>
													</div>
												</div>
													
											
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityUserID()}:<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
													<form:input path="UserId" type="text" class="form-controlwidth"  maxlength="50"  value="${Security.getUserId()}" name="UserId" id="UserId"  readonly="${viewIsUserExists}"/>	<span id="ErrorUserID" class="error long_errormsg_both"> </span>													
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityTenant()}:
													
													</label>
													<div class="col-md-10">
														<form:input path="Tenant" type="text" class="form-controlwidth"  value="${Security.getTenant()}" name="Tenant"  readonly="true"/>															
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityCompany()}:
													
													</label>
													<div class="col-md-10">
														<form:input path="Company" type="text" class="form-controlwidth"  value="${viewCompanyID}" name="Company" readonly="true"/>														
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityCurrentPwd()}:<span   class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
														<form:input path="Password" type="password" class="form-controlwidth" maxlength="20" value="${Security.getPassword()}" name="Password" id="Password" />		<span id="ErrorPwd" class="error"> </span>												
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecuritySeqQues1()}:<span class="required">
													* </span>
													</label>												
													<div class="col-md-10">													
														<select class="selectmenumaintenance" name="SecurityQuestion1" id="SecurityQuestion1"  >
															    <option value="">${viewLabels.getSecurity_labels_Select()}...</option>
                 												 <c:forEach items="${GeneralCodeValue}" var="SequrityQuestionsList">
                 												           <c:choose>
    																					<c:when test="${Security.getSecurityQuestion1().equalsIgnoreCase(SequrityQuestionsList.getDescription50())}">
       																							<option value='${SequrityQuestionsList.getDescription50()}' selected="selected">${SequrityQuestionsList.getDescription50()} </option>
   																						</c:when>
   
    																					<c:otherwise>
        																	<option value='${SequrityQuestionsList.getDescription50()}'>${SequrityQuestionsList.getDescription50()} </option>
    																					</c:otherwise>
																			</c:choose>
                 												           
                 														  
                  												 </c:forEach>
														</select> <span id="ErrorSQ1" class="error-select"> </span>
													</div>											
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecuritySeqAns1()}:<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
														<form:input path="SecurityQuestionAnswer1" type="text" class="form-controlwidth"  value="${Security.getSecurityQuestionAnswer1()}" name="SecurityQuestionAnswer1" id="SecurityAnswer1" />	<span id="ErrorSA1" class="error"> </span>													
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecuritySeqQues2()}: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<select class="selectmenumaintenance" name="SecurityQuestion2" id="SecurityQuestion2">
															  <option value="">${viewLabels.getSecurity_labels_Select()}...</option>
                 												 <c:forEach items="${GeneralCodeValue}" var="SequrityQuestionsList2">
                 														   <c:choose>
    																					<c:when test="${Security.getSecurityQuestion2().equalsIgnoreCase(SequrityQuestionsList2.getDescription50())}">
       																							<option value='${SequrityQuestionsList2.getDescription50()}' selected="selected">${SequrityQuestionsList2.getDescription50()} </option>
   																						</c:when>
   
    																					<c:otherwise>
        																	<option value='${SequrityQuestionsList2.getDescription50()}'>${SequrityQuestionsList2.getDescription50()} </option>
    																					</c:otherwise>
																			</c:choose>
                  												 </c:forEach>  
														</select><span id="ErrorSQ2" class="error-select"> </span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecuritySeqAns2()}:<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
														<form:input path="SecurityQuestionAnswer2" type="text" class="form-controlwidth"  value="${Security.getSecurityQuestionAnswer2()}" name="SecurityQuestionAnswer2" id="SecurityAnswer2" maxlength="100"/><span id="ErrorSA2" class="error"> </span>														
													</div>
												</div>	
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecuritySeqQues3()}: <span class="required">
													* </span>
													</label>
													<div class="col-md-10">
														<select class="selectmenumaintenance" name="SecurityQuestion3" id="SecurityQuestion3">
															<option value="">${viewLabels.getSecurity_labels_Select()}...</option>
                 												  <c:forEach items="${GeneralCodeValue}" var="SequrityQuestionsList3">
                 														   <c:choose>
    																					<c:when test="${Security.getSecurityQuestion3().equalsIgnoreCase(SequrityQuestionsList3.getDescription50())}">
       																							<option value='${SequrityQuestionsList3.getDescription50()}' selected="selected">${SequrityQuestionsList3.getDescription50()} </option>
   																						</c:when>
   
    																					<c:otherwise>
        																	<option value='${SequrityQuestionsList3.getDescription50()}'>${SequrityQuestionsList3.getDescription50()} </option>
    																					</c:otherwise>
																			</c:choose>
                  												 </c:forEach> 
														</select><span id="ErrorSQ3" class="error-select"> </span>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecuritySeqAns3()}:<span class="required" >
													* </span>
													
													</label>
													
													<div class="col-md-10">
														<form:input path="SecurityQuestionAnswer3" type="text" maxlength="100" class="form-controlwidth"  value="${Security.getSecurityQuestionAnswer3()}" name="SecurityQuestionAnswer3" id="SecurityAnswer3"  />	<span id="ErrorSA3" class="error"> </span>													
													</div>
												</div>																							
													<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLstPwd1()}:
													
													</label>
													<div class="col-md-10">
													    <form:input path="LastPassword1" type="text" class="form-controlwidth"  value="${Security.getLastPassword1()}" name="LastPassword1"  readonly="true"/>														
													</div>
												</div>
													<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLstPwd2()}:
													
													</label>
													<div class="col-md-10">
														<form:input path="LastPassword2" type="text" class="form-controlwidth"  value="${Security.getLastPassword2()}" name="LastPassword2"  readonly="true"/>
													</div>
												</div>
													<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLstPwd3()}:
													
													</label>
													<div class="col-md-10">
														<form:input path="LastPassword3" type="text" class="form-controlwidth"  value="${Security.getLastPassword3()}" name="LastPassword3"   readonly="true"/>
													</div>
												</div>
												
												 
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLstPwd4()}: 
													</label>
													<div class="col-md-10">
														<form:input path="LastPassword4" type="text" class="form-controlwidth"  value="${Security.getLastPassword4()}" name="LastPassword4"  readonly="true"/>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLstPwd5()}:
													</label>
													<div class="col-md-10">
														<form:input path="LastPassword5" type="text" class="form-controlwidth"  value="${Security.getLastPassword5()}" name="LastPassword5"   readonly="true"/>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityNoOfAtmp()}:
													</label>
													<div class="col-md-10">
														<form:input path="NumberAttempts" type="text" class="form-controlwidth"  value="0" name="NumberAttempts"  readonly="true"/>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLblSecurityLstInvalidDate()}: 
													</label>
													<div class="col-md-10">
														<form:input  path="InvalidAttemptDate" type="text" class="form-controlwidth" id="InvalidAttemptDate" name="InvalidAttemptDate"  readonly="true"/><span class="required">
														<input  id="InvalidAttemptDateH" type="hidden" class="form-controlwidth" name="InvalidAttemptDateH"/>
													</div>
												</div>
												
												
												</div>
												
												<div class="margin-bottom-5-right-allign_maintenancenew_maintenanace">
												
												<button class="btn btn-sm yellow filter-submit margin-bottom" onclick="actionForm('AddUser','update');"><i class="fa fa-check"></i> ${viewLabels.getLblSequrityUpdate()}</button>
												<button class="btn btn-sm red filter-cancel"  onclick="reloadPage();"><i class="fa fa-times"></i>&nbsp;${viewLabels.getLblSecurity_ButtonResetText()}</button>
												<button class="btn btn-sm red filter-cancel"  onclick="return previousPage();"><i class="fa fa-times"></i>&nbsp;${viewLabels.getLblSequrityDelete()}</button></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>
				<!--end tabbable-->
				
				<!--end tabbable-->
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->

<script>
jQuery(document).ready(function() {    

var nameofscreen="${viewIsUserExists}";
	/* alert("screen name"+nameofscreen); */
	
	if(nameofscreen=="false"){
/* 	alert("add"); */
	$('#LastActivityDate').val(getLocalDate());
	$('#LastActivityDateStringH').val(getUTCDateTime());
	$('#InvalidAttemptDate').val('');
	$('#InvalidAttemptDateH').val('');
	}
	else{
		/* alert("edit"); */
		$('#LastActivityDate').val(ConvertUTCToLocalTimeZone('${Security.getLastActivityDate()}'));
		$('#LastActivityDateStringH').val(getUTCDateTime());
		var dateValue='${Security.getInvalidAttemptDate()}';
		if(dateValue.length>2){
		$('#InvalidAttemptDate').val(ConvertUTCToLocalTimeZone('${Security.getInvalidAttemptDate()}'));
		$('#InvalidAttemptDateH').val(ConvertUTCToLocalTimeZone('${Security.getInvalidAttemptDate()}'));
		}
		
	}
 //  Metronic.init(); // init metronic core componets
   //Layout.init(); // init layout
  // Demo.init(); // init demo(theme settings page)
  // Index.init(); // init index page
  // Tasks.initDashboardWidget(); // init tash dashboard widget
});
</script>
<script type="text/javascript">
var isSubmit = false;
function body_load(){
	
	//var isblank=true;
	/* var re = /^[\w ]+$/;
	
	
		if($('#UserId').val() === ""){
			
		  alert("UserId can not be left blank");
		  myForm.UserId.focus(); 
		 document.location.href = '#top';
		 $.get( "${pageContext.request.contextPath}/checkUserAvailability?UserName="+userid, function( data ) {
			 
            alert(data);
            
            });
   
		 
		  isblank=false;
		}else if($('#SecurityQuestion1').val() === ""){		
			  alert("SecurityQuestion1 can not be left blank");
			  myForm.SecurityQuestion1.focus(); 
			 document.location.href = '#top';
			  isblank=false;
			}else if($('#SecurityAnswer1').val() === ""){		
				  alert("SecurityAnswer1 can not be left blank");
				  myForm.SecurityAnswer1.focus(); 
				 document.location.href = '#top';
				  isblank=false;
				}else if($('#SecurityQuestion2').val() === ""){		
					  alert("SecurityQuestion2 can not be left blank");
					  myForm.SecurityQuestion2.focus(); 
					 document.location.href = '#top';
					  isblank=false;
					}else if($('#SecurityAnswer2').val() === ""){		
						  alert("SecurityAnswer2 can not be left blank");
						  myForm.SecurityAnswer2.focus(); 
						 document.location.href = '#top';
						  isblank=false;
						}else if($('#SecurityQuestion3').val() === ""){		
							  alert("SecurityQuestion3 can not be left blank");
							  myForm.SecurityQuestion3.focus(); 
							 document.location.href = '#top';
							  isblank=false;
							}else if($('#SecurityAnswer3').val() === ""){		
								  alert("SecurityAnswer3 can not be left blank");
								  myForm.SecurityAnswer3.focus(); 
								 document.location.href = '#top';
								  isblank=false;
								} */
							
								return isSubmit;
	
	
}

</script>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />
<script type="text/javascript">
function reloadPage(){
	window.parent.location = window.parent.location.href;
	}
	
function previousPage(){													
	//location.reload(); 
	       // window.location.href='${pageContext.request.contextPath}/Menu_app_icon_maintenance_new';
			var backState = '${backStatus}';
					
		
		 if (backState == 'lookup') {

			 window.location.href = 'Team_member_lookup';
			
		}
		else{
			
			 window.history.back();
		} 
}

/* $(function(){
    $('#SecurityQuestion1').change(function(){
        var val = $(this).val();
        var sel1 = $(this);
		if(val==='')
			{
			
			}
		else
			{
      
        if($("#SecurityQuestion2 option[value='"+val+"']").length>0)
        	{
        	$("#SecurityQuestion2 option[value='"+val+"']").remove();
        	}
        else
        	{
        	$("#SecurityQuestion2").append('<option value="'+val+'">'+val+'</option>');
        	}
        if($("#SecurityQuestion3 option[value='"+val+"']").length>0)
    		{
        	$("#SecurityQuestion3 option[value='"+val+"']").remove();
    		}
        else
        	{
        	$("#SecurityQuestion3").append('<option value="'+val+'">'+val+'</option>');
        	
        	}
   
			}        
    });
});
$(function(){
    $('#SecurityQuestion2').change(function(){
        var val = $(this).val();
        var sel1 = $(this);
        var sel2 = document.getElementById('SecurityQuestion1');
        var sel3 = document.getElementById('SecurityQuestion3');
        var opt = document.createElement('option'); // create new option element
    	if(val==='')
		{
		
		}
	else
		{
        
        if($("#SecurityQuestion1 option[value='"+val+"']").length>0)
    	{
    	$("#SecurityQuestion1 option[value='"+val+"']").remove();
    	}
    else
    	{
    	$("#SecurityQuestion1").append('<option value="'+val+'">'+val+'</option>');
    	}
    if($("#SecurityQuestion3 option[value='"+val+"']").length>0)
		{
    	$("#SecurityQuestion3 option[value='"+val+"']").remove();
		}
    else
    	{
    	$("#SecurityQuestion3").append('<option value="'+val+'">'+val+'</option>');
    	
    	}
		}
                  
    });
});



$(function(){
    $('#SecurityQuestion3').change(function(){
        var val = $(this).val();
        var sel1 = $(this);
        var sel2 = document.getElementById('SecurityQuestion2');
        var sel3 = document.getElementById('SecurityQuestion3');
        var opt = document.createElement('option'); // create new option element
    	if(val==='')
		{
		
		}
	else
		{
        
        if($("#SecurityQuestion2 option[value='"+val+"']").length>0)
    	{
    	$("#SecurityQuestion2 option[value='"+val+"']").remove();
    	}
    else
    	{
    	$("#SecurityQuestion2").append('<option value="'+val+'">'+val+'</option>');
    	}
    if($("#SecurityQuestion1 option[value='"+val+"']").length>0)
		{
    	$("#SecurityQuestion1 option[value='"+val+"']").remove();
		}
    else
    	{
    	$("#SecurityQuestion1").append('<option value="'+val+'">'+val+'</option>');
    	
    	}
   
		}          
    });
});
 */

function regularExpressionValidator(Text,Regix) { 
	
	
	
	
    return Regix.test(Text);
} 

function CheckForError()
{
		var testResult=false;
	   var tempUser=isBlankField('UserId','ErrorUserID','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
	   if(!tempUser)
		   {
		   $('#ErrorUserID').show();
		 //  return 0;
		   }
	   else
		   {
		   var fieldValue = document.getElementById('UserId').value.trim();
		   var Rex= /^(?=.*[a-zA-Z].*)(?=.*\d)([a-zA-Z0-9@*#])(?!.*\s).{5,}$/;
		  
		   
		   var validUSer=regularExpressionValidator(fieldValue,Rex);
		   
		   if(!validUSer)
			   {
			   setErrorMessage('ErrorUserID','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserValid()}');
			   $('#ErrorUserID').show();
			//   return 0;
			   }
		   }
	   
	   var tempPassword=isBlankField('Password','ErrorPwd','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
	   if(!tempPassword)
	   {
	   $('#ErrorPwd').show();
	   //return 0;
	   }
	   else
		   {
		   /* var fieldValue = document.getElementById('Password').value.trim();
		   var Rex=/^(?=.*\d)(?=.*\d)(?=.*\d)(?=.*[A-Z])(?!.*\s).{8,}$/;
		   
		   var validUSer=regularExpressionValidator(fieldValue,Rex);
		   
		   if(!validUSer)
			   {
			   setErrorMessage('ErrorPwd','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityPasswordValid()}');
			   $('#ErrorPwd').show();
			   return 0;
			   } */
		   }
	   
	   var tempSequrityQtn3=isBlankField('SecurityQuestion3','ErrorSQ3','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
	   if(!tempSequrityQtn3)
	   {
	   //$('#ErrorSQ3').show();
	   //return 0;
	   }
	    var tempSequrityQtn2=isBlankField('SecurityQuestion2','ErrorSQ2','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
	   if(!tempSequrityQtn2)
	   {
	   //$('#ErrorSQ2').show();
	   //return 0;
	   }
	    var tempSequrityQtn1=isBlankField('SecurityQuestion1','ErrorSQ1','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
	   if(!tempSequrityQtn1)
	   {
	   //$('#ErrorSQ1').show();
	   //return 0;
	   }
	   if(tempSequrityQtn1 && tempSequrityQtn2 && tempSequrityQtn3 ){
	   
	   
	   
	   var e1 = document.getElementById("SecurityQuestion1");
       var strOption1 = e1.options[e1.selectedIndex].value;   
       var e2 = document.getElementById("SecurityQuestion2");
       var strOption2 = e2.options[e2.selectedIndex].value;
       var e3 = document.getElementById("SecurityQuestion3");
       var strOption3 = e3.options[e3.selectedIndex].value;
	   var tempSequrityQuestion1=compareThreeTextEquality(strOption1,strOption2,strOption3);
	    	if(!tempSequrityQuestion1)
	    	{
	    		setErrorMessage('ErrorSQ1','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecuritySeqQuesNotValid()}');
	    		setErrorMessage('ErrorSQ2','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecuritySeqQuesNotValid()}');
	    		setErrorMessage('ErrorSQ3','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecuritySeqQuesNotValid()}');
	    		
	    //	 $('#ErrorSQ1').show();
	    //	 $('#ErrorSQ2').show();
	    //	 $('#ErrorSQ3').show();
			 testResult=false;
	  	 //  return 0;
	    	} 
	    	else
	    		{
	    	//	 $('#ErrorSQ1').hide();
		    //	 $('#ErrorSQ2').hide();
		    //	 $('#ErrorSQ3').hide();
				 testResult=true;
	    		}
	   }
	 //  var tempSequrityQuestion1=isBlankField('SecurityQuestion1','ErrorMessage','please select sequrity question 1');
	   
	   
	   var tempSequrityQAns1=isBlankField('SecurityAnswer1','ErrorSA1','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
	   if(!tempSequrityQAns1)
	   {
	   $('#ErrorSA1').show();
	   //return 0;
	   }
		  // var tempSequrityQuestion2=isBlankField('SecurityQuestion2','ErrorMessage','please select sequrity question 2');
	   
	   var tempSequrityQAns2=isBlankField('SecurityAnswer2','ErrorSA2','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
//		var tempSequrityQuestion3=isBlankField('SecurityQuestion1','ErrorMessage','please select sequrity question 3');
	    if(!tempSequrityQAns2)
	   {
	   $('#ErrorSA2').show();
	   //return 0;
	   }


	   var tempSequrityQAns3=isBlankField('SecurityAnswer3','ErrorSA3','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNotLeftBlank()}');
	   if(!tempSequrityQAns3)
	   {
	   $('#ErrorSA3').show();
	   //return 0;
	   }

	   
	 if(testResult && tempUser && tempPassword  && tempSequrityQAns1 && tempSequrityQAns2 && tempSequrityQAns3 && tempSequrityQtn3 && tempSequrityQtn2 && tempSequrityQtn1 && validUSer)
		{
		 
		 return 1;
		}
	 else{
		 return 0;
	 }
	}

function actionForm(url, action) {													
	   if (action == 'update')
		   {
		   var viewUserExists=${viewIsUserExists};
		   var userid=$('#UserId').val();
		var ErrorStatus=CheckForError();
		if(ErrorStatus==1)
			{
		
		   
				
		   
			  $.post( "${pageContext.request.contextPath}/checkUserAvailability",
					  {UserName:userid}, function( data,status ) {
				 
		         //alert(data);
		         if(!viewUserExists)
		        	 {
		         if(data.length>=1 )
		        	 {
		        	 var errMsgId=document.getElementById("ErrorUserID");
		        	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${viewLabels.getErrSecurityUserNameNotAvailable()}';
		        	 $('#ErrorUserID').show();
		        	 isSubmit=false;
		        	 
		        	// isSubmit=false;
					// document.location.href = '#top';
					 window.scrollTo(0,0);
		        	// alert ("User Already Exists");
		        	 }
		         else
		        	 {
		        	 $('#ErrorUserID').hide();
		        	// alert ("User Available");
		        	
		        	//alert('${viewLabels.getInfoStatusAddedSuccessFully()}');
		        	
		        	bootbox.alert('${viewLabels.getInfoStatusAddedSuccessFully()}',
		        			 function() {
		        		 isSubmit=true;
			        	  document.myForm.action = url;
			  			document.myForm.submit();    
		        			 }
		        	   	 );
      	 
		        	 }
		        	 }
		         else
		        	 {
		        	 $('#ErrorUserID').hide();
			        	// alert ("User Available");
			        	
			        	if(viewUserExists)
			        		{
			        		//alert('${viewLabels.getInfoStatusUpdatedSuccessFully()}');
			        		
			        		
			        		bootbox.alert('${viewLabels.getInfoStatusUpdatedSuccessFully()}',
				        			 function() {
			        			 $('#InvalidAttemptDateH').val('${Security.getInvalidAttemptDate()}');
			        			 isSubmit=true;
						        	
					        	  document.myForm.action = url;
					  			document.myForm.submit(); 
				        			 }
				        	   	 );
			        		
			        		 
			        		}
			        	else
			        		{
			        		//alert('${viewLabels.getInfoStatusAddedSuccessFully()}');

			        		bootbox.alert('${viewLabels.getInfoStatusAddedSuccessFully()}',
				        			 function() {
			        			isSubmit=true;
					        	
					        	  document.myForm.action = url;
					  			document.myForm.submit(); 
				        			 }
				        	   	 );
			        		
			        		}
			        	 
		        	 }
		         });
		   }
		 else
			 {
			 isSubmit=false;
			// document.location.href = '#top';
			 window.scrollTo(0,0);
			 
			 }
		   }
	   
	 
		   
}

function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'SecuritySetUp',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }
  setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);	
</script>



<!-- END JAVASCRIPTS -->

<!-- END BODY -->
</html>