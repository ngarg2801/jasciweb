<!-- 
Date Developed  Sep 18 2014
Description It is used to show the list of General Code 
Created By Aakash Bishnoi -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE" %>
<script>
var contextPath='<%=request.getContextPath()%>';

</script>
<style>
/* .centercolumn{

text-align:center !important;
} */

#AddBtn2{
margin-right: -1.35%;
margin-bottom: 1%;
float: right;
}

 .k-grid th.k-header, .k-grid-header {

    white-space: normal !important;
    padding-right: 0px !important;
    overflow: visible !important;
    
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
    white-space: normal !important;
    
}

.k-grid-content {
position: relative;
width: 100%;
overflow: auto;
overflow-x: auto; 
overflow-y:hidden !important;
zoom: 1;
} 

fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}

</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div id="container" style="position: relative" class="loader_div">
	<div class="body">
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${Infohelp_ScreenLabels.getInfoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->

			<!-- END PAGE TOOLBAR -->
		</div>
	</div>
	<div class="page-content" id="page-content" >
		<div class="container">
		<div>
		<a  id="AddBtn2" href="${pageContext.request.contextPath}/InfoHelpassignmentmaintenance_add" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Add_New()}</span>
								</a>
		</div>
<%-- 		<script>
var contextPath='<%=request.getContextPath()%>';
<%

%>
</script> --%>
		
		
		
		
		
		
			<!-- BEGIN PAGE BREADCRUMB -->
			<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Dashboard
				</li>
			</ul>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-46">
			<div class="col-md-12">
					<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
					<!-- Begin: life time stats -->
					<div class="portlet">
								
					<%-- <a  id="AddBtn" href="${pageContext.request.contextPath}/InfoHelpassignmentmaintenance_add" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Add_New()}</span>
								</a>	 --%>		
							
		<div class="row" >
	
			<kendo:grid name="infohelpsearchlookup" resizable="true" reorderable="true" sortable="true" dataBound="gridDataBound">
				<kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5">
    	</kendo:grid-pageable>
				<kendo:grid-editable mode="inline" confirmation="" />
				<kendo:grid-columns>
					
					<kendo:grid-column  title="${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Info_Help_Type()}" field="infoHelpType" width="20%"  />
					
					<kendo:grid-column title="${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Info_Help()}"	field="infoHelp" width="20%"/>
					<kendo:grid-column title="${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Language()}" field="language" width="20%"/>
					<kendo:grid-column title="${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Discription()}" field="description50" width="22%" />

					<kendo:grid-column title="${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Action()}" width="18%">
						<kendo:grid-column-command>
							<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
						<kendo:grid-column-commandItem className="icon-pencil btn btn-sm yellow filter-submit margin-bottom" name="editDetails" text="${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Edit()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function showDetails(e) {
                               

                                e.preventDefault();

                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                var Strinfohelp=dataItem.infoHelp;
                                var StrLanguage=dataItem.languageCode;
                                
                               
            
                                var url= "${pageContext.request.contextPath}/InfoHelpassignmentEdit";
                                //var wnd = $("#details").data("kendoWindow");
                                window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                                var myform = document.createElement("form");

                                var InfoTypeField = document.createElement("input");
                                InfoTypeField.type = 'hidden';
                                InfoTypeField.value = Strinfohelp;
                                InfoTypeField.name = "InfoHelp";
                                
                                var LanguageField = document.createElement("input");
                                LanguageField.type = 'hidden';
                                LanguageField.value = StrLanguage;
                                LanguageField.name = "Language";
                                
                                myform.action = url;
                                myform.method = "get"
                                myform.appendChild(InfoTypeField);
                                myform.appendChild(LanguageField);
                               
                                document.body.appendChild(myform);
                                myform.submit();
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
							<kendo:grid-column-commandItem className="fa fa-times btn btn-sm red filter-cancel" name="DeleteDetails" text="${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Delete()}">
								<kendo:grid-column-commandItem-click>
							<script>
							 /* function deleteInfoHelps(e) {
			                    e.preventDefault();
			                    var dataItem = this.dataItem($(e.target).closest("tr"));			                  
			                    if (confirm('Are you sure you want to delete this record.')) {
			                        var dataSource = $("#infohelpsearchlookup").data("kendoGrid").dataSource;
			                        dataSource.remove(dataItem);
			                        dataSource.sync();
			                    }
			                } */ 
			                function deleteInfoHelp(e) {
			                    e.preventDefault();
			                   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
			                   var Strinfohelp=dataItem.infoHelp;
                               var StrLanguage=dataItem.languageCode;
                                var dataSource = $("#infohelpsearchlookup").data("kendoGrid").dataSource;
                                var url= "${pageContext.request.contextPath}/KindoInfoHelpsdelete";
                              /*   if (confirm("${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Confirm_Delete()}")) {
			                    */
			                    bootbox
                                .confirm(
                                  '${Infohelp_ScreenLabels.getInfoHelpSearchlookup_Confirm_Delete()}',
                                  function(okOrCancel) {

                                   if(okOrCancel == true)
                                   {
			                     
                                	 var myform = document.createElement("form");

                                     var InfoTypeField = document.createElement("input");
                                     InfoTypeField.type = 'hidden';
                                     InfoTypeField.value = Strinfohelp;
                                     InfoTypeField.name = "InfoHelp";
                                     
                                     var LanguageField = document.createElement("input");
                                     LanguageField.type = 'hidden';
                                     LanguageField.value = StrLanguage;
                                     LanguageField.name = "Language";
                                     
                                     myform.action = url;
                                     myform.method = "get"
                                     myform.appendChild(InfoTypeField);
                                     myform.appendChild(LanguageField);
                                    
                                     document.body.appendChild(myform);
                                     myform.submit();                    				                        			                      			                         
			                    } 
                                  });
			                }
							</script>
			                </kendo:grid-column-commandItem-click>
							</kendo:grid-column-commandItem>
							
						</kendo:grid-column-command>
					</kendo:grid-column>
				</kendo:grid-columns>
		
				<kendo:dataSource pageSize="10" >
					<kendo:dataSource-transport>
						<kendo:dataSource-transport-read cache="false" url="${pageContext.request.contextPath}/InfoHelps_Kendo_DisplayAll"></kendo:dataSource-transport-read>
												
						<kendo:dataSource-transport-destroy	url="${pageContext.request.contextPath}/KindoInfoHelpsdelete" dataType="json" type="POST" contentType="application/json">
						
						</kendo:dataSource-transport-destroy>


						<kendo:dataSource-transport-parameterMap>
							<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
						</kendo:dataSource-transport-parameterMap>

					</kendo:dataSource-transport>




				</kendo:dataSource>
			</kendo:grid>
					</div>
					<!-- End: life time stats -->
				</div>	
				
			</div>
			<div class="row">
				
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
</div>
</div>
	</tiles:putAttribute>
	
	
</tiles:insertDefinition>
<script>

function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);

function headerChangeLanguage(){
	
	/* alert('in security look up'); */
	
 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	
	
}

function headerInfoHelp(){
	
	//alert('in security look up');
	
 	/* $
	.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
	 */
 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
			  {InfoHelp:'InfoHelp',
 		InfoHelpType:'PROGRAM'
		 
			  }, function( data1,status ) {
				  if (data1.boolStatus) {
					  window.open(data1.strMessage); 
					  					  
					}
				  else
					  {
					//  alert('No help found yet');
					  }
				  
				  
			  });
	
}
jQuery(document).ready(function() {    
	$(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	  
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
 
});
</script>
</html>
