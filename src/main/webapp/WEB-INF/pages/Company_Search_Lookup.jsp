<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE"%>

<style>
#AddBtn1 {
margin-right: -2.7%;
margin-bottom: 1%;
float: right;
}


</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div id="container" style="position: relative" class="loader_div">
		<div class="page-container">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_SearchLookupLabel()}</h1>
					</div>
				</div>


				<div class="page-content" id="page-content" >
					<div class="container">
					<div>
					<a id="AddBtn1"
									href="${pageContext.request.contextPath}/Company_maintenance_new"
									class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
									<span class="hidden-480">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ButtonAddNewText()}</span>
								</a>
					</div>
					
						<%-- 		<script>
var contextPath='<%=request.getContextPath()%>';
<%

%>
</script> --%>






						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active">Dashboard</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-46">
							<div class="col-md-12">


								
							</div>

							<!-- Begin: life time stats -->
							<div class="portlet">

								


								<div class="row">

									<kendo:grid name="CompanyMaintenance" columnResize="true" dataBound="gridDataBound"
										sortable="true" resizable="true" reorderable="true">
										<kendo:grid-pageable refresh="true" pageSizes="true"
											buttonCount="5">
										</kendo:grid-pageable>
										<kendo:grid-editable mode="inline" confirmation="Message" />

										<kendo:grid-columns>

											<kendo:grid-column
												title="${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_CompanyidLabel()}"  field="companyId" width="35%;" />
											
											<kendo:grid-column
												
												title="${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_CompanyNameLabel()}" field="name50" width="47%;" />
												
											<kendo:grid-column
												title="${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ButtonActionText()}"
												width="18%;">
												<kendo:grid-column-command>
												

													<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
													<kendo:grid-column-commandItem
														className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
														name="editDetails"
														text="${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ButtonEditText()}">
														<kendo:grid-column-commandItem-click>
															<script>
                           
															 function editTeammember(e) {
									                               

									                                e.preventDefault();
										
									                                
									                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
									                                var varcompanyId=dataItem.companyId;
									                               
									                                
									                               
									                               <%--  var contextPath='<%=request.getContextPath()%>'; --%>
									                               
									                             
									                                 var url="${pageContext.request.contextPath}/Comapany_addorupdate_request";
									                                //var wnd = $("#details").data("kendoWindow");
									                               
									    							 var myform = document.createElement("form");

									                                 var varCompanyIDFiled = document.createElement("input");
									                                 varCompanyIDFiled.value = varcompanyId;
									                                 varCompanyIDFiled.name = "CompanyID";
									                                 varCompanyIDFiled.setAttribute("type", "hidden");
									                                 
									                                          
									                                 var varFlagForReturn = document.createElement("input");
									                                 varFlagForReturn.setAttribute("type", "hidden");
									                                 varFlagForReturn.value = "SearchLookup";
									                                 varFlagForReturn.name = "FlagForReturn";
									                                 
									                                 
									                                 myform.action = url;
									                                 myform.method = "get";
									                                 myform.appendChild(varFlagForReturn);
									                                 myform.appendChild(varCompanyIDFiled);
									                                 document.body.appendChild(myform);
									                                 myform.submit();
									    							
									                            }
									                            
									                          
                          
                            </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

													<kendo:grid-column-commandItem
														className="btn btn-sm red filter-cancel fa fa-times"
														name="btnDelete" text="${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ButtonDeleteText()}">

														<kendo:grid-column-commandItem-click>

															<script>						   
															 function deleteRecords(e) { 
									                               
														    	   e.preventDefault();
														    	   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
									                                var varcompanyId=dataItem.companyId;
									                               
									                               
									                               
									                             //  if (confirm('${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_WHILE_DELETING_COMPANY()}') == true){
									                                
									                            	   bootbox.confirm('${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_WHILE_DELETING_COMPANY()}',
									                            			   function(okOrCancel) {
									                            			                            if(okOrCancel == true)
									                            			                                {
									                            	   var url="${pageContext.request.contextPath}/Company_maintenance_delete";
									                                //var wnd = $("#details").data("kendoWindow");
									                               
									    							 var myform = document.createElement("form");

									    							 var varCompanyIDFiled = document.createElement("input");
									                                 varCompanyIDFiled.value = varcompanyId;
									                                 varCompanyIDFiled.name = "CompanyID";
									                                 varCompanyIDFiled.setAttribute("type", "hidden");
									                                 
									                               									                                 
									                                 var varKendoUrlField = document.createElement("input");
									                                 varKendoUrlField.setAttribute("type", "hidden");
									                                 varKendoUrlField.value = '${KendoReadUrl}';
									                                 varKendoUrlField.name = "KendoUrl";
									                                 
									                                 myform.action = url;
									                                 myform.method = "post";
									                                 myform.appendChild(varCompanyIDFiled);
									                                 myform.appendChild(varKendoUrlField);
									                                 document.body.appendChild(myform);
									                                 myform.submit();
									    							
													    	
													    	
													    	//var wnd = $("#details").data("kendoWindow");
								                              // window.location=url;
							                              
							                               }});
																	}
							    </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

												</kendo:grid-column-command>
											</kendo:grid-column>
										</kendo:grid-columns>

										<kendo:dataSource pageSize="10">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/${KendoReadUrl}"></kendo:dataSource-transport-read>
												<kendo:dataSource-transport-update
													url="GeneralCodeList/Grid/update" dataType="json"
													type="POST" contentType="application/json" />
												<kendo:dataSource-transport-destroy
													url="GeneralCodeList/Grid/delete" dataType="json"
													type="POST" contentType="application/json">
													<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
												</kendo:dataSource-transport-destroy>


												<kendo:dataSource-transport-parameterMap>
													<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>
											<kendo:dataSource-schema>
												<kendo:dataSource-schema-model id="intKendoID">
													<kendo:dataSource-schema-model-fields>
														<kendo:dataSource-schema-model-field name="intKendoID">

														</kendo:dataSource-schema-model-field>


													</kendo:dataSource-schema-model-fields>
												</kendo:dataSource-schema-model>
											</kendo:dataSource-schema>



										</kendo:dataSource>
									</kendo:grid>

									

								</div>
								<!-- End: life time stats -->
							</div>

						</div>
						<div class="row"></div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>
		</div>
	</tiles:putAttribute>


</tiles:insertDefinition>
<script>
  
function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
   

function headerChangeLanguage(){
	
	/* alert('in security look up'); */
	
 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	
	
}

function headerInfoHelp(){
	
	//alert('in security look up');
	
 	/* $
	.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
	 */
 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
			  {InfoHelp:'Company',
 		InfoHelpType:'PROGRAM'
		 
			  }, function( data1,status ) {
				  if (data1.boolStatus) {
					  window.open(data1.strMessage); 
					  					  
					}
				  else
					  {
					//  alert('No help found yet');
					  }
				  
				  
			  });
	
}

   
</script>
<script>
	jQuery(document).ready(function() {
		
		
		
		setInterval(function () {
			
		    var h = window.innerHeight;		    
		    var h = window.innerHeight;		
			   
		    if(window.innerHeight>=900 || window.innerHeight==1004){
		    	h=h-187;
		    	
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";		     
		    
			}, 30);
		
		
		
		
	

	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>