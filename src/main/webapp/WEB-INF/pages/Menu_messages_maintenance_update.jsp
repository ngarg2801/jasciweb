<!-- 
Date Developed  Sep 18 2014
Description It is used to show the list of General Code Identification
Created By Aakash Bishnoi -->


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">


		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->
		<html>
<head>
<style>
.TextAreaErrorMessage {
	position: relative !important;
	top: -23px !important;
}

.errorMessagesFormat {
	position: absolute;
	width: 122%;
	top:5px;
}

.paddingAsrik {
	padding-right: 2px !important;
}

.CustomDatePickettextBox {
	margin-left: 22.5px !important;
}

@media only screen and (min-device-width : 768px) and (max-device-width
	: 1024px) and (orientation : portrait) {
	.CustomDatePickettextBox {
		margin-left: 0px !important;
	}
}
</style>



</head>
<body>

	<div class="page-container">
		<!-- BEGIN PAGE HEAD -->
		<div class="page-head">
			<div class="container">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>${ScreenLabels.getMenuMessage_Menu_Messages_Maintenance()}</h1>
				</div>

			</div>
		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content" id="page-content">
			<div class="container paddingAsrik">
				<!-- BEGIN PAGE BREADCRUMB -->
				<ul class="page-breadcrumb breadcrumb hide">
					<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
					<li class="active">message maintenance</li>
				</ul>
				<!-- END PAGE BREADCRUMB -->
				<!-- BEGIN PAGE CONTENT INNER -->
				<div class="row margin-top-10">
					<div class="col-md-12">
						<form:form class="form-horizontal form-row-seperated" action="#"
							method="post" onsubmit="return isFormSubmit();" name="myform"
							modelAttribute="ObjectMenuMessages">
							<div class="portlet">
								<c:set var="StrType" value="${ObjectMenuMessages.getType()}" />
								<form:input type="hidden" path="Message_ID" class="form-control"
									value="${ObjectMenuMessages.getMessage_ID()}" name="Message_ID"
									id="Message_ID" />
								<form:input type="hidden" path="Tenant_ID" class="form-control"
									value="${ObjectMenuMessages.getTenant_ID()}" name="Tenant_ID"
									id="Tenant_ID" />
								<form:input type="hidden" path="Company_ID" class="form-control"
									value="${ObjectMenuMessages.getCompany_ID()}" name="Company_ID"
									id="Company_ID" />
								<form:input type="hidden" path="Status" class="form-control"
									value="${ObjectMenuMessages.getStatus()}" name="Status"
									id="Status" />
								<input type="hidden" name="Last_Activity_DateH"
									id="Last_Activity_DateH">

								<div class="portlet-body">
									<div class="tabbable">

										<div class="tab-content no-space">
											<div class="tab-pane active" id="tab_general">
												<div class="form-body">

													<div class="form-group">
														<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_Start_Date_YYMMDD()}:
															<span class="required"> * </span>
														</label>
														<div class="col-md-3">
															<div
																class="input-group input-medium date date-picker CustomDatePickettextBox">
																<form:input type="text" path="Start_Date"
																	class="form-control" name="startdate" id="startdate"
																	readonly="true"
																	style="width:241px; background-color:white;" />
																<span id="ErrorMessagestartdate"
																	class="error errorMessagesFormat"></span>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_End_Date_YYMMDD()}:
															<span class="required"> * </span>
														</label>
														<div class="col-md-3">
															<div
																class="input-group input-medium date date-picker CustomDatePickettextBox">
																<form:input type="text" class="form-control"
																	path="End_Date" name="enddate" id="enddate"
																	readonly="true"
																	style="width:241px; background-color:white;" />
																<span id="ErrorMessageenddate"
																	class="error errorMessagesFormat"></span>
															</div>
														</div>
													</div>
													<div class="form-group" id="HideTicket">
														<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_Ticket_Tape_Message()}:
															<span class="required"> * </span>
														</label>

														<div class="col-md-10">
															<textarea class="form-controlwidth" maxlength="500" style="resize:none"
																name="Ticket_Tape_Message" id="TicketTapeMessage">${ObjectMenuMessages.getTicket_Tape_Message()}</textarea>
															<span id="ErrorMessageTicketTapeMessage"
																class="error"></span>
														</div>
													</div>
													<div class="form-group" id="HideMessage">
														<label class="col-md-2 control-label">${ScreenLabels.getMenuMessage_Message()}:
															<span class="required"> * </span>
														</label>

														<div class="col-md-10">
															<textarea class="form-controlwidth" maxlength="500"
																name="Message" id="Message">${ObjectMenuMessages.getMessage1()}</textarea>
															<span id="ErrorMessageMessage"
																class="error"></span>
														</div>
													</div>


													<div class="margin-bottom-5-right-allign_menu_main">
														<button
															class="btn btn-sm yellow filter-submit margin-bottom">
															<i class="fa fa-check"></i>&nbsp;${ScreenLabels.getMenuMessage_Save_Update()}
														</button>
														<button class="btn btn-sm red filter-cancel" type="button"
															onclick="return resetButton();">
															<i class="fa fa-times"></i>
															${ScreenLabels.getMenuMessage_Reset()}
														</button>
														<button class="btn btn-sm red filter-cancel" type="button"
															onclick="return cancelButton();">
															<i class="fa fa-times"></i>
															${ScreenLabels.getMenuMessage_Cancel()}
														</button>
													</div>


												</div>
											</div>




										</div>
									</div>
								</div>
							</div>

						</form:form>
					</div>

				</div>

				<!-- END PAGE CONTENT INNER -->
			</div>
		</div>

	</div>
</body>
<!-- END BODY -->
		</html>
	</tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />
<script>

	jQuery(document)
			.ready(
					function() {
						$('#Last_Activity_DateH').val(getUTCDateTime());
						$('#startdate')
								.val(
										ConvertUTCToLocalTimeZone("${ObjectMenuMessages.getStart_Date()}"));
						$('#enddate')
								.val(
										ConvertUTCToLocalTimeZone("${ObjectMenuMessages.getEnd_Date()}"));

						var StrTypeValue = "${StrType}";

						//prevent enter key
						$(window).keydown(function(event) {
							if (event.keyCode == 13) {
								event.preventDefault();
								return false;
							}
						});

						setInterval(
								function() {

									var h = window.innerHeight;
									if (window.innerHeight >= 900
											|| window.innerHeight == 1004) {
										h = h - 187;
									} else {
										h = h - 239;
									}
									document.getElementById("page-content").style.minHeight = h
											+ "px";

								}, 30);

						if (StrTypeValue == "Ticket") {

							$('#Message').attr('readonly', true);
							//$('#AstrikMessage').hide();
							$('#HideMessage').hide();

						}
						if (StrTypeValue == 'Message') {

							$('#TicketTapeMessage').attr('readonly', true);
							//$('#AstrikTicket').hide();
							$('#HideTicket').hide()
						}

					});
</script>
<script>
	var IsSubmit = false;

	$(function() {
		var dates = $("#startdate, #enddate").datepicker(
				{
					minDate : '0',
					dateFormat : 'yy-mm-dd',
					onSelect : function(selectedDate) {
						var option = this.id == "startdate" ? "minDate"
								: "maxDate", instance = $(this).data(
								"datepicker"), date = $.datepicker.parseDate(
								instance.settings.dateFormat
										|| $.datepicker._defaults.dateFormat,
								selectedDate, instance.settings);
						//date.setDate(date.getDate());
						//dates.not(this).datepicker("option", option, date);
					}
				});
	});

	function cancelButton() {
		window.location = "${pageContext.request.contextPath}/Menu_messages_search_lookup";
	}
	function resetButton() {
		window.parent.location = window.parent.location.href;
	}
	function isFormSubmit() {

		var StrTypeValue = "${StrType}";

		var ValidStartDate = isBlankField('startdate', 'ErrorMessagestartdate',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getMenuMessage_Mandatory_Fields()}');

		var ValidEndDate = isBlankField('enddate', 'ErrorMessageenddate',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getMenuMessage_Mandatory_Fields()}');

		var isStartDateLessThenEnd = false;
		var isLastDateLessThenEnd = false;

		if (ValidStartDate && ValidEndDate) {

			isStartDateLessThenEnd = isBlankField('startdate',
					'ErrorMessagestartdate',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ScreenLabels.getMenuMessage_ERR_Start_Date()}');

			isLastDateLessThenEnd = testDate('startdate', 'enddate',
					'ErrorMessageenddate',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ScreenLabels.getMenuMessage_ERR_End_Date()}');

		}

		 /* $('#startdate').val(
				ConvertLocalToUTCCurrentTimeZone($('#startdate').val()));
		$('#enddate')
				.val(ConvertLocalToUTCCurrentTimeZone($('#enddate').val()));  */

		if (StrTypeValue == "Ticket") {

			var ValidTicketTapeMessage = isBlankField(
					'TicketTapeMessage',
					'ErrorMessageTicketTapeMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ScreenLabels.getMenuMessage_Mandatory_Fields()}');

			if (ValidStartDate && ValidEndDate && ValidTicketTapeMessage
					&& isLastDateLessThenEnd) {
				/* myform.action="MenuMessageUpdate";
				myform.submit(); 
					
				alert("${ScreenLabels.getMenuMessage_Confirm_Update()}");
				IsSubmit=true; */

				bootbox.alert(
						'${ScreenLabels.getMenuMessage_Confirm_Update()}',
						function() {
							$('#startdate').val(
									ConvertLocalToUTCCurrentTimeZone($('#startdate').val()));
							$('#enddate')
									.val(ConvertLocalToUTCCurrentTimeZone($('#enddate').val())); 
							document.myform.action = "MenuMessageUpdate";

							IsSubmit = true;

							document.myform.submit();
						});

			}

		}
		if (StrTypeValue == 'Message') {

			var ValidMessage1 = isBlankField(
					'Message',
					'ErrorMessageMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ScreenLabels.getMenuMessage_Mandatory_Fields()}');

			if (ValidStartDate && ValidEndDate && ValidMessage1
					&& isLastDateLessThenEnd) {
				/*myform.action="MenuMessageUpdate";
				myform.submit(); 
				
				alert("${ScreenLabels.getMenuMessage_Confirm_Update()}");
				
				IsSubmit=true;*/
				bootbox.alert(
						'${ScreenLabels.getMenuMessage_Confirm_Update()}',
						function() {
							$('#startdate').val(
									ConvertLocalToUTCCurrentTimeZone($('#startdate').val()));
							$('#enddate')
									.val(ConvertLocalToUTCCurrentTimeZone($('#enddate').val())); 
							document.myform.action = "MenuMessageUpdate";

							IsSubmit = true;

							document.myform.submit();
						});

			}
		}
	/* 	$('#startdate').val(ConvertUTCToLocalTimeZone("${ObjectMenuMessages.getStart_Date()}"));
		$('#enddate').val(ConvertUTCToLocalTimeZone("${ObjectMenuMessages.getEnd_Date()}")); */
		return IsSubmit;

	}
	function headerChangeLanguage() {

		/* alert('in security look up'); */

		$.ajax({
			url : '${pageContext.request.contextPath}/HeaderLanguageChange',
			type : 'GET',
			cache : false,
			success : function(data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});

	}

	function headerInfoHelp() {

		//alert('in security look up');

		/* $
		.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
		 */
		$.post("${pageContext.request.contextPath}/HeaderInfoHelp", {
			InfoHelp : 'MenuMessages',
			InfoHelpType : 'PROGRAM'

		}, function(data1, status) {
			if (data1.boolStatus) {
				window.open(data1.strMessage);

			} else {
				//  alert('No help found yet');
			}

		});

	}
</script>
<script type="text/javascript">
   /*  $(function () {
    $('#enddate').datepicker({ 
	minDate: 0,
	dateFormat: 'yy-mm-dd' 
	});
	 $('#startdate').datepicker({ 
	minDate:0,
	dateFormat: 'yy-mm-dd' 
	});
    }); */
	
	
</script>

<!-- END PAGE LEVEL SCRIPTS -->


<!-- END JAVASCRIPTS -->

