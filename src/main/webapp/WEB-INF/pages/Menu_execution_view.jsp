<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.2.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project. -->


<!-- KENDO UI CSS AND JS -->
<link href="<c:url value="/KendoUI/styles/kendo.common.min.css" />"
	rel="stylesheet" />
<link href="<c:url value="/KendoUI/styles/kendo.default.min.css" />"
	rel="stylesheet" type="text/css" />
<script src="<c:url value="/KendoUI/js/jquery.min.js"/>"></script>
<script src="<c:url value="/KendoUI/js/kendo.all.min.js"/>"></script>


<html lang="en">

<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>JASCI</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"> 
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" />" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/assets/global/plugins/uniform/css/uniform.default.css"/>" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<c:url value="/resources/assets/admin/pages/css/lock.css"/>" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<c:url value="/resources/assets/global/css/components.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/css/plugins.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/layout.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/themes/default.css"/>" rel="stylesheet" type="text/css" id="style_color">
<link href="<c:url value="/resources/assets/admin/layout3/css/custom.css"/>" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<link href="<c:url value="/resourcesValidate/css/glass-style.css"/>"  rel="stylesheet" type="text/css">
<Style>
.k-icon, .k-state-disabled .k-icon, .k-column-menu .k-sprite {
opacity: .8;
margin-top: -3px;
}
</Style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->

<script type="text/javascript">
function Execute() {
	  // run the first time; all subsequent calls will take care of themselves 
	 $.get( "${pageContext.request.contextPath}/MenuExecutionLabel?languageName=ENG&ScreenName=MenuExecutionScreen", function( data ) {
		 /* debugger */
		
		 
		 var span = document.getElementById("TeamMemberLabel");
		    span.textContent = data[1];
		    var span = document.getElementById("SelectCompanyToWorkWithLabel");
		    span.textContent = data[0];
		   
		 
		 
		 
	 });
	  
	  
	  
	}
$(document).ready(function() {
	  // run the first time; all subsequent calls will take care of themselves 
	//  Execute();
	  var ScreenType='FULLSCREEN';
			var agent = navigator.userAgent;      
		    var isWebkit = (agent.indexOf("AppleWebKit") > 0);      
		    var isIPad = (agent.indexOf("iPad") > 0);      
		    var isIOS = (agent.indexOf("iPhone") > 0 || agent.indexOf("iPod") > 0);     
		    var isAndroid = (agent.indexOf("Android")  > 0);     
		    var isNewBlackBerry = (agent.indexOf("AppleWebKit") > 0 && agent.indexOf("BlackBerry") > 0);     
		    var isWebOS = (agent.indexOf("webOS") > 0);      
		    var isWindowsMobile = (agent.indexOf("IEMobile") > 0);     
		    var isSmallScreen = (screen.width < 767 || (isAndroid && screen.width < 1000));     
		    var isUnknownMobile = (isWebkit && isSmallScreen);     
		    var isMobile = (isIOS || isAndroid || isNewBlackBerry || isWebOS || isWindowsMobile || isUnknownMobile);     
		    var isTablet = (isIPad || (isMobile && !isSmallScreen));  
		    var isGLASS = (agent.indexOf("X6") > 0 || agent.indexOf("X7") > 0);
  		
	 
	 
	    if(isGLASS){
	    	
	        ScreenType="GLASS";
	        $('body').css('background-color', 'black !important');
	       }
	    else if((isMobile && isSmallScreen)||isTablet)
        {
	    	
	    	$("#extendwidthfortablet").css("width", "80%");
        }
	    else{
	    	
	    	}
			/* var isGLASSX7 = (agent.indexOf("X7") > 0);
	    if(isGLASSX7){
	        ScreenType="GLASS";
	        $('body').css('background-color', 'black !important');
	       }
	    else{
	    	
	    	} */
		  //setTimeout(executeQuery,1000);
	});

</script>

<body  style="background-color:rgba(144, 154, 164, 1) ;" >
<div class="page-lock" id="extendwidthfortablet" style="width:66%;">
	<div class="page-logo">
		<a class="brand" href="#">
		<img src="<c:url value="/resources/assets/admin/layout3/img/Logo2.png"/>" alt="logo" class="logo-company"/>
		</a>
	</div>
	<div class="row margin-top-10">
				<div class="form-body">
												<div class="form-group team-member-name-align">
													<label class="col-md-2 control-label" id="MenuExecutionLabelTeamMember"  >${ObjCommonSession.getLblTeam_Member()}<span id="TeamMemberLabel">${objMenuOptionLabel.getLblTeam_Member()}:&nbsp;</span>${TeamMemberName}
													</label>
												</div>
			</div><br/><br/><br/>
				<div class="col-md-6_company" style="width:67%">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue kendo-table-width">
						<div class="portlet-title" style="float: none;">
							<div class="caption" id="textformatClassID"><span id="SelectCompanyToWorkWithLabel">${objMenuOptionLabel.getLblCompaySelectWorkWith()}
							
							</span>
							</div>
							
						</div>  				
						<script type="text/javascript">
						function DoubleClickLi(e){
							
							
							
						$("#grid").on("dblclick", function (e) 
						        {
							
							
						                            e.preventDefault();
						                            
						                          
						             var grid = $("#grid").data("kendoGrid");
						             var selectedItem = grid.dataItem(grid.select());
						                            var company=selectedItem.Company;
						                            var name=selectedItem.Name20;
						                            var purchaseOrderApprove=selectedItem.PurchaseOrderApproval;
						                            var companyLogo=selectedItem.ComapnyLogo;
						                            var TMobile=selectedItem.TMobile;
						                            var TRF=selectedItem.TRF;
						                            var TRF=selectedItem.TFullDisplay;
						                            
						                           /*  var url="${pageContext.request.contextPath}/MenuAssignment?Company="+company+"&CompanyName="+name
						                            		+"&CompanyLogoURL="+companyLogo+"&purchaseOrderApprove="+purchaseOrderApprove;
						                           // window.location="${pageContext.request.contextPath}/MenuAssignment/"+company+"/"+Name;
						                           window.location=url;  */
						                          
						                            
						                            
						                           
						                            
						        
						      });
						
						
						
						
					 	$('#grid').on('click touchend', function(e) {
							  e.preventDefault();
	                            
							  
							 
	                          
					             var grid = $("#grid").data("kendoGrid");
					             var selectedItem = grid.dataItem(grid.select());
					                            var company=selectedItem.Company;
					                            var name=selectedItem.Name20;
					                            var purchaseOrderApprove=selectedItem.PurchaseOrderApproval;
					                            var companyLogo=selectedItem.ComapnyLogo;
					                            var TMobile=selectedItem.TMobile;
					                            var TRF=selectedItem.TRF;
					                            var TFullDisplay=selectedItem.TFullDisplay;
					                            
					                          /*   var http = new XMLHttpRequest();
					                            var url = "${pageContext.request.contextPath}/MenuAssignment";
					                            var parameters = "Company="+company+"&CompanyName="+name+"&CompanyLogoURL="+companyLogo+"&purchaseOrderApprove="+purchaseOrderApprove;
					                            http.open("POST", url, true);
					                             
					                            //Send the proper header information along with the request
					                            http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					                            http.setRequestHeader("Content-length", parameters .length);
					                            http.setRequestHeader("Connection", "close");
					                             
					                            http.onreadystatechange = function() {//Handler function for call back on state change.
					                                if(http.readyState == 4) {
					                                    alert(http.responseText);
					                                }
					                            }
					                            http.send(parameters); */
					                            
					                           /*  xmlhttp.open("POST","MenuAssignment",true);
												  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
												  xmlhttp.send("Company="+company+"&CompanyName="+name+"&CompanyLogoURL="+companyLogo+"&purchaseOrderApprove="+purchaseOrderApprove);
					                             */
					                          var url="${pageContext.request.contextPath}/MenuAssignment?Company="+company+"&CompanyName="+name
					                            		+"&CompanyLogoURL="+companyLogo+"&purchaseOrderApprove="+purchaseOrderApprove
					                            		+"&TMobile="+TMobile+"&TRF="+TRF
					                            		+"&TFullDisplay="+TFullDisplay; 
					                           // window.location="${pageContext.request.contextPath}/MenuAssignment/"+company+"/"+Name;
					                            window.location=url;
					                          
					                        
						});  
						
						}
						
						
						
				 						
						</script>
					<kendo:grid name="CompanyList" id="grid"  selectable="multiple"
						autoBind="true"  scrollable="false" navigatable="true" pageable="true" change="DoubleClickLi" >
						

						
						<kendo:grid-columns >

							<kendo:grid-column title="${objMenuOptionLabel.getLblCompany_ID()}" field="Company" />
							<kendo:grid-column title="${objMenuOptionLabel.getLblName()}" field="Name20" />

						</kendo:grid-columns>
						

			
						<kendo:dataSource autoSync="true" pageSize="10">
							<kendo:dataSource-transport>
								<kendo:dataSource-transport-read cache="false"
									url="${pageContext.request.contextPath}/MenuKendoList"></kendo:dataSource-transport-read>



								<kendo:dataSource-transport-parameterMap>

									<script>
                  function parameterMap(options,type) { 
                   
                    return JSON.stringify(options);
                   
                  }
                  
                  
                
                
                  </script>
								</kendo:dataSource-transport-parameterMap>

							</kendo:dataSource-transport>
						</kendo:dataSource>
					</kendo:grid>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				</div>
				
				
				
			</div>
	<div class="page-footer-custom" style="margin-left: 13px; width:100%;">
		Copyright &copy;2014 - JASCI, LLC. All Rights Reserved.
	</div>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->

<!-- <script src="<c:url value="/resources/assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script>-->
<script src="<c:url value="/resources/assets/global/plugins/jquery-migrate.min.js"/>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<c:url value="/resources/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.cokie.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/uniform/jquery.uniform.min.js"/>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<c:url value="/resources/assets/global/plugins/backstretch/jquery.backstretch.min.js"/>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script src="<c:url value="/resources/assets/global/scripts/metronic.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/layout.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/demo.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/pages/scripts/lock.js"/>" type="text/javascript"></script>
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
   Lock.init();
   //Execute();
});
</script>
<script>
function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'MenuExecution',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }
	jQuery(document).ready(function() {

		  
		setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
	    
		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)

	

	});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>