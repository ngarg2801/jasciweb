<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">


		<!-- BEGIN PAGE TITLE -->
		<div class="page-container">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${ViewLabels.getLbl_Fulfillment_Center_Maintenance()}</h1>
					</div>
				</div>
			</div>

		</div>
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content hideHorizontalScrollbar" id ="page-content">
			<div class="container">
				<!-- BEGIN PAGE BREADCRUMB -->

				<!-- END PAGE BREADCRUMB -->
				<!-- BEGIN PAGE CONTENT INNER -->
				<div class="row margin-top-10">
					<div id="ErrorMessage" class="note note-danger"
						style="display: none;">
						<p class="error-ful" id="Perror" style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${PageRedirectLoadMessage}</p>
					</div>

					<div class="col-md-12">
						<form:form name="myForm"
							class="form-horizontal form-row-seperated" action="#"
							onsubmit="return isformSubmit();"
							modelAttribute="ViewFulfillment" method="POST">
							<div class="portlet">
								<form:input type="hidden" path="IsNew" name="IsNew"  value="${viewIsFulfillmentExists}"/>
								<input type="hidden" name="SetUpBy" path="SetUpBy"   value="${ViewFulfillment.getSetUpBy()}">
								<div class="portlet-body">
									<div class="tabbable">

										<div class="tab-content no-space">
											<div class="tab-pane active" id="tab_general">
												<div class="form-body">


													<%--  <div class="form-group">
													<label class="col-md-2 control-label">Fullfillment Center :<span class="required">
													* </span> 
													</label>
													
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="Fullfillment" readonly="${viewIsFulfillmentExists}" value = "${ViewFulfillment.getFullfillment()}"  />
													</div>
												</div> --%>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Fulfillment_Center()}:<span class="required"> * </span>

														</label>
														<div class="col-md-10">
															<form:input path="Fullfillment" type="text"
																class="form-controlwidth" maxlength="36"
																value="${ViewFulfillment.getFullfillment()}"
																name="Fullfillment" id="Fullfillment"
																readonly="${viewIsFulfillmentExists}" />
															<span id="ErrorFullfillment" class="error"> </span>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Set_up_Selected()}:</label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="SetUpSelected" path="SetUpSelected"
																readonly="true"
																value="${ViewFulfillment.getSetUpSelected()}" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Set_up_Date()}:</label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="SetUpDate" path="SetUpDate" readonly="true"
																 />
																
																	<input type="hidden" class="form-controlwidth"
																name="SetUpDateH" id="SetUpDateH"
																 />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Setup_By()}: </label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="SetUpByName" path="SetUpByName"
																 readonly="true"
																value="${ViewFulfillment.getSetUpByName()}" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Last_Activity_Date()}: </label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="LastActivityDateString" path="LastActivityDateString"
																readonly="true"
																 />
																
																<input type="hidden" class="form-controlwidth"
																name="LastActivityDateStringH" id="LastActivityDateStringH"
																 />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Last_Activity_By()}: </label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="LastActivityTeamMember" path="LastActivityTeamMember"
																readonly="true"
																value="${ViewFulfillment.getLastActivityTeamMember()}" />
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Name_Short()}:
															<span class="required"> * </span>
														</label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="Name20" id="Name20" path="Name20" maxlength="20"
																value="${ViewFulfillment.getName20()}" />
															<span id="ErrorName20" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Name_Long()}:
															<span class="required"> * </span>
														</label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="Name50" path="Name50" id="Name50" maxlength="50"
																value="${ViewFulfillment.getName50()}" />
															<span id="ErrorName50" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Address_Line_1()}: <span class="required"> * </span>
														</label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="AddressLine1" path="AddressLine1"
																id="AddressLine1" maxlength="100"
																value="${ViewFulfillment.getAddressLine1()}" />
															<span id="ErrorAddressLine1" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Address_Line_2()}: </label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="AddressLine2" path="AddressLine2"
																id="AddressLine2" maxlength="100"
																value="${ViewFulfillment.getAddressLine2()}" />
															<span id="ErrorAddressLine2" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Address_Line_3()}: </label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="AddressLine3" path="AddressLine3"
																id="AddressLine3" maxlength="100"
																value="${ViewFulfillment.getAddressLine3()}" />
															<span id="ErrorAddressLine3" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Address_Line_4()}: </label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="AddressLine4" path="AddressLine4"
																id="AddressLine4" maxlength="100"
																value="${ViewFulfillment.getAddressLine4()}" />
															<span id="ErrorAddressLine4" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_City()}: <span
															class="required"> * </span>
														</label>

														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="City" path="City" id="City" maxlength="100"
																value="${ViewFulfillment.getCity()}" />
															<span id="ErrorCity" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Country_Code()}: <span class="required"> * </span>
														</label>
														<div class="col-md-10">
															<select
																class="table-group-action-input form-control input-medium"
																name="CountryCode" id="CountryCode">
																<option value="none">${ViewLabels.getLbl_Select()}...</option>
																<c:forEach items="${ViewCountryCodelst}"
																	var="ContyCodeList">
																	<%-- <option
																				value='${ContyCodeList.getId().getGeneralCode()}'>${ContyCodeList.getDescription20()}</option>
																	 --%>
																	<c:choose>
																		<c:when
																			test="${ViewFulfillment.getCountryCode().equalsIgnoreCase(ContyCodeList.getGeneralCode())}">
																			<option
																				value='${ContyCodeList.getGeneralCode()}'
																				selected="selected">${ContyCodeList.getDescription20()}
																			</option>
																		</c:when>

																		<c:otherwise>
																			<option
																				value='${ContyCodeList.getGeneralCode()}'>${ContyCodeList.getDescription20()}
																			</option>
																		</c:otherwise>
																	</c:choose>

																</c:forEach>

															</select><span id="ErrorCountryCode" class="error-select"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_State()}: <span
															id="StateRequiredID" class="required"> * </span>
														</label>
														<div class="col-md-10">
															<select
																class="table-group-action-input form-control input-medium"
																name="StateCode" id="StateCode">
																<option value="none" selected="selected">${ViewLabels.getLbl_Select()}...</option>
																 <%-- <c:choose>
																<c:when
																			test="${ViewFulfillment.getStateCode()==''}">
																			<option value="none" selected="selected">Select...</option>
																			
																		</c:when>
																</c:choose> --%>
																
																<c:forEach items="${ViewStatesCodelst}"
																	var="ContyCodeList">
																	<%-- <option
																				value='${ContyCodeList.getId().getGeneralCode()}'>${ContyCodeList.getDescription20()}</option>
																		  --%>

																	 <c:choose>
																		<c:when
																			test="${ViewFulfillment.getStateCode().equalsIgnoreCase(ContyCodeList.getGeneralCode())}">
																			<option
																				value='${ContyCodeList.getGeneralCode()}'
																				selected="selected">${ContyCodeList.getDescription20()}
																			</option>
																		</c:when>

																		<c:otherwise>
																			<option
																				value='${ContyCodeList.getGeneralCode()}'>${ContyCodeList.getDescription20()}
																			</option>
																		</c:otherwise>
																	</c:choose> 
																</c:forEach>
															</select><span id="ErrorStateCode" class="error-select"> </span>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Zip_Code()}: <span
															id="ZipRequiredID" class="required"> * </span>
														</label>
														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="ZipCode" path="ZipCode" id="ZipCode"
																maxlength="10" value="${ViewFulfillment.getZipCode()}" />
															<span id="ErrorZipCode" class="error long_errormsg"> </span>
														</div>
													</div>
													<div class="repeatingSection" id="TextBoxesGroup">
														<div class="form-group" id="TextBoxDiv1">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Name()}: <!-- <span class="required"> * </span> -->
															</label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactName1" id="ContactName1" maxlength="100"
																	path="ContactName1"
																	value="${ViewFulfillment.getContactName1()}" />
																<span id="ErrorContactName1" class="error "> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Phone()}: <!-- <span class="required"> * </span> -->
															</label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactPhone1" id="ContactPhone1" maxlength="10"
																	path="ContactPhone1"
																	value="${ViewFulfillment.getContactPhone1()}" />
																<span id="ErrorContactPhone1" class="error long_errormsg_both"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Extension()}: <!-- <span class="required"> * </span> -->
															</label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactExtension1" id="ContactExtension1"
																	maxlength="10" path="ContactExtension1"
																	value="${ViewFulfillment.getContactExtension1()}" />
																<span id="ErrorContactExtension1" class="error long_errormsg_both">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Cell()}: <!-- <span class="required"> * </span> -->
															</label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactCell1" id="ContactCell1" maxlength="10"
																	path="ContactCell1"
																	value="${ViewFulfillment.getContactCell1()}" />
																<span id="ErrorContactCell1" class="error long_errormsg_both"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Fax()}:<!--  <span class="required"> * </span> -->
															</label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactFax1" path="ContactFax1" id="ContactFax1"
																	maxlength="10"
																	value="${ViewFulfillment.getContactFax1()}" />
																<span id="ErrorContactFax1" class="error long_errormsg_both"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Email()}: <!-- <span class="required"> * </span> -->
															</label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactEmail1" path="ContactEmail1"
																	id="ContactEmail1" maxlength="100"
																	value="${ViewFulfillment.getContactEmail1()}" />
																<span id="ErrorContactEmail1" class="error long_errormsg_both"> </span>
															</div>
															<div class="formRowRepeatingSectionForOne" id="add1">
																<%-- <img
																	src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																	alt="logo" id='addButton1' style="cursor: pointer"
																	class="addFight" onClick="addFight(1);" /> --%>
																	
																	<button class="btn btn-sm green filter-cancel"
															onClick="addFight(1);">
															<i class="fa fa-plus"></i> ${ViewLabels.getLbl_Contact()}
														</button>

																<!-- <a href="#" class= "addFight" onClick="addFight();" style="color:#FF0000" >Add Contact Details</a>-->
															</div>
														</div>

													</div>
													
													<div class="repeatingSection" id="TextBoxesGroup2"
														hidden=true>
														<hr class="hrfulfillmentmaintenanceDivision" />
														<div class="form-group" id="TextBoxDiv1">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Name2()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactName2" path="ContactName2" maxlength="100"
																	id="ContactName2"
																	value="${ViewFulfillment.getContactName2()}" />
																<span id="ErrorContactName2" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Phone2()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactPhone2" path="ContactPhone2"
																	maxlength="10" id="ContactPhone2"
																	value="${ViewFulfillment.getContactPhone2()}" />
																<span id="ErrorContactPhone2" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Extension2()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactExtension2" path="ContactExtension2"
																	maxlength="10" id="ContactExtension2"
																	value="${ViewFulfillment.getContactExtension2()}" />
																<span id="ErrorContactExtension2" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Cell2()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactCell2" path="ContactCell2"
																	id="ContactCell2" maxlength="10"
																	value="${ViewFulfillment.getContactCell2()}" />
																<span id="ErrorContactCell2" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Fax2()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactFax2" path="ContactFax2" id="ContactFax2"
																	maxlength="10"
																	value="${ViewFulfillment.getContactFax2()}" />
																<span id="ErrorContactFax2" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Email2()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactEmail2" path="ContactEmail2"
																	maxlength="100"
																	value="${ViewFulfillment.getContactEmail2()}" />
																	
																	<button class="btn btn-sm red filter-cancel fulfillmentMinusContact"
															onClick="deleteGroup(2);">
															<i class="fa fa-minus"></i> ${ViewLabels.getLbl_Contact()}
														</button>
																<%-- <img
																	src="<c:url value="/resources/assets/admin/layout3/img/Delete_button.png"/>"
																	alt="logo" style="cursor: pointer" id='removeButton2'
																	class="deleteFight" onClick="deleteGroup(2);"> --%><span
																	id="ErrorContactEmail2" class="error"> </span>
															</div>





															<div class="formRowRepeatingSectionForOne" id="add2">
																<%-- <img
																	src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																	alt="logo" id='addButton2' style="cursor: pointer"
																	class="addFight" onClick="addFight(2);" /> --%>
																<button class="btn btn-sm green filter-cancel"
															onClick="addFight(2);">
															<i class="fa fa-plus"></i> ${ViewLabels.getLbl_Contact()}
														</button>
														

															</div>
															
														</div>

													</div>

													<div class="repeatingSection" id="TextBoxesGroup3"
														hidden=true>
														<hr class="hrfulfillmentmaintenanceDivision" />
														<div class="form-group" id="TextBoxDiv1">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Name3()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactName3" path="ContactName3" maxlength="100"
																	id="ContactName3"
																	value="${ViewFulfillment.getContactName3()}" />
																<span id="ErrorContactName3" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Phone3()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactPhone3" path="ContactPhone3"
																	maxlength="10" id="ContactPhone3"
																	value="${ViewFulfillment.getContactPhone3()}" />
																<span id="ErrorContactPhone3" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Extension3()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactExtension3" path="ContactExtension3"
																	maxlength="10" id="ContactExtension3"
																	value="${ViewFulfillment.getContactExtension3()}" />
																<span id="ErrorContactExtension3" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Cell3()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactCell3" path="ContactCell3"
																	id="ContactCell3" maxlength="10"
																	value="${ViewFulfillment.getContactCell3()}" />
																<span id="ErrorContactCell3" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Fax3()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactFax3" path="ContactFax3" id="ContactFax3"
																	maxlength="10"
																	value="${ViewFulfillment.getContactFax3()}" />
																<span id="ErrorContactFax3" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Email3()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactEmail3" path="ContactEmail3"
																	id="ContactEmail3" maxlength="100"
																	value="${ViewFulfillment.getContactEmail3()}" />
																	
																<button class="btn btn-sm red filter-cancel fulfillmentMinusContact"
															onClick="deleteGroup(3);">
															<i class="fa fa-minus"></i> ${ViewLabels.getLbl_Contact()}
														</button>
																<%-- <img
																	src="<c:url value="/resources/assets/admin/layout3/img/Delete_button.png"/>"
																	alt="logo" style="cursor: pointer" id='removeButton3'
																	class="deleteFight" onClick="deleteGroup(3);"> --%>
																	<span
																	id="ErrorContactEmail3" class="error"> </span>
															</div>
															<div class="formRowRepeatingSectionForOne" id="add3">
															<button class="btn btn-sm green filter-cancel"
															onClick="addFight(3);">
															<i class="fa fa-plus"></i> ${ViewLabels.getLbl_Contact()}
														</button>
															
																<%-- <img
																	src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																	alt="logo" id='addButton3' style="cursor: pointer"
																	class="addFight" onClick="addFight(3);" /> --%>

																<!-- <a href="#" class= "addFight" onClick="addFight();" style="color:#FF0000" >Add Contact Details</a>-->
															</div>
															
														</div>

													</div>
													<div class="repeatingSection" id="TextBoxesGroup4"
														hidden=true>
														<hr class="hrfulfillmentmaintenanceDivision" />
														<div class="form-group" id="TextBoxDiv1">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Name4()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactName4" path="ContactName4"
																	id="ContactName4" maxlength="100"
																	value="${ViewFulfillment.getContactName4()}" />
																<span id="ErrorContactName4" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Phone4()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactPhone4" path="ContactPhone4"
																	id="ContactPhone4" maxlength="10"
																	value="${ViewFulfillment.getContactPhone4()}" />
																<span id="ErrorContactPhone4" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Extension4()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactExtension4" path="ContactExtension4"
																	id="ContactExtension4" maxlength="10"
																	value="${ViewFulfillment.getContactExtension4()}" />
																<span id="ErrorContactExtension4" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Cell4()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactCell4" path="ContactCell4"
																	id="ContactCell4" maxlength="10"
																	value="${ViewFulfillment.getContactCell4()}" />
																<span id="ErrorContactCell4" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Fax4()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactFax4" path="ContactFax4" id="ContactFax4"
																	maxlength="10"
																	value="${ViewFulfillment.getContactFax4()}" />
																<span id="ErrorContactFax4" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${ViewLabels.getLbl_Contact_Email4()}: </label>
															<div class="col-md-10">
																<form:input type="text" class="form-controlwidth"
																	name="ContactEmail4" path="ContactEmail4"
																	id="ContactEmail4" maxlength="100"
																	value="${ViewFulfillment.getContactEmail4()}" />
																	<!-- <button class="btn btn-sm red filter-cancel removeContactfulfillment"
															onClick="deleteGroup(4);">
															<i class="fa fa-minus"></i> Contact
														</button> -->
														
														<button class="btn btn-sm red filter-cancel fulfillmentMinusContact"
															onClick="deleteGroup(4);">
															<i class="fa fa-minus"></i> ${ViewLabels.getLbl_Contact()}
														</button>
																<%-- <img
																	src="<c:url value="/resources/assets/admin/layout3/img/Delete_button.png"/>"
																	alt="logo" style="cursor: pointer" id='removeButton4'
																	class="deleteFight" onClick="deleteGroup(4);"> --%><span
																	id="ErrorContactEmail4" class="error"> </span>
															</div>

														
														
														<%-- <div class="formRowRepeatingSectionForOne" id="add3">
															
															
															
																<img
																	src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																	alt="logo" id='addButton3' style="cursor: pointer"
																	class="addFight" onClick="addFight(3);" />

																<!-- <a href="#" class= "addFight" onClick="addFight();" style="color:#FF0000" >Add Contact Details</a>-->
															</div> --%>
															
															</div>

													</div>


													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Main_Fax()}: </label>
														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="MainFax" id="MainFax" maxlength="10" path="MainFax"
																value="${ViewFulfillment.getMainFax()}" />
															<span id="ErrorMainFax" class="error"> </span>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-2 control-label">${ViewLabels.getLbl_Main_Email()}:
														</label>
														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="MainEmail" id="MainEmail" maxlength="100" path="MainEmail"
																value="${ViewFulfillment.getMainEmail()}" />
															<span id="ErrorMainEmail" class="error"> </span>
														</div>
													</div>


													<div class="margin-bottom-5-right-allign_add_fullfillment">
														<button
															class="btn btn-sm yellow filter-submit margin-bottom"
															onclick="onSaveClick();">
															<i class="fa fa-check"></i> ${ViewLabels.getLbl_Save_Update()}
														</button>
														<button class="btn btn-sm red filter-cancel"
															onclick="reloadPage();">
															<i class="fa fa-times"></i> ${ViewLabels.getLbl_Button_ResetText()}
														</button>
														<button class="btn btn-sm red filter-cancel"
															onclick="onCancelClick();">
															<i class="fa fa-times"></i> ${ViewLabels.getLbl_Cancel()}
														</button>
													</div>



												</div>
											</div>




										</div>
									</div>
								</div>
							</div>

						</form:form>
					</div>

				</div>

				<!-- END PAGE CONTENT INNER -->
			</div>
		</div>
		<!-- END PAGE CONTENT -->









		<!-- END PAGE CONTENT -->



	</tiles:putAttribute>

</tiles:insertDefinition>




<!-- END PAGE LEVEL SCRIPTS -->
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script type="text/javascript">

	/* jQuery(document).ready(function() {

		var value = '${TeamMemberInvalidMsg}';

		if (value.length > 5) {
			$('#ErrorMessage').show();
		} else {
			$('#ErrorMessage').hide();
		}

		//Metronic.init(); // init metronic core componets
		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)
		//Index.init(); // init index page
		//Tasks.initDashboardWidget(); // init tash dashboard widget

	}); */
</script>

<script type="text/javascript">
	var isSubmit = false;

	
	$('#CountryCode').change(function() {

		if ($(this).val() == '840') {

			$("#StateRequiredID").show();
			//$("#ZipRequiredID").show();
			//	$('#ZipCode').removeAttr("disabled");
			$("#StateCode").removeAttr("disabled");
			$("#StateCode").val($("#Statecode option:first").val());

		} else {
			$("#StateRequiredID").hide();
			//$("#ZipRequiredID").hide();
			//	$('#ZipCode').attr('disabled', 'disabled');
			//$('#ZipCode').val('');

			$('#StateCode').attr('disabled', 'disabled');
			//$("#StateCode").val($("#Statecode option:first").val());
			$("#StateCode").val('none');
		}

	});

	function deleteGroup(ButtonClick) {

		if (ButtonClick == 4) {
			$('#add3').show();
			$('#TextBoxesGroup4').hide();
			var elem1 = document.getElementById("ContactName4");
			elem1.value = "";
			var elem2 = document.getElementById("ContactPhone4");
			elem2.value = "";
			var elem3 = document.getElementById("ContactExtension4");
			elem3.value = "";
			var elem4 = document.getElementById("ContactCell4");
			elem4.value = "";
			var elem5 = document.getElementById("ContactFax4");
			elem5.value = "";
			var elem6 = document.getElementById("ContactEmail4");
			elem6.value = "";

		} else if (ButtonClick == 3) {
			$('#add2').show();
			$('#TextBoxesGroup3').hide();

			var elem1 = document.getElementById("ContactName3");
			elem1.value = "";
			var elem2 = document.getElementById("ContactPhone3");
			elem2.value = "";
			var elem3 = document.getElementById("ContactExtension3");
			elem3.value = "";
			var elem4 = document.getElementById("ContactCell3");
			elem4.value = "";
			var elem5 = document.getElementById("ContactFax3");
			elem5.value = "";
			var elem6 = document.getElementById("ContactEmail3");
			elem6.value = "";

		} else if (ButtonClick == 2) {
			$('#add1').show();
			$('#TextBoxesGroup2').hide();
			var elem1 = document.getElementById("ContactName2");
			elem1.value = "";
			var elem2 = document.getElementById("ContactPhone2");
			elem2.value = "";
			var elem3 = document.getElementById("ContactExtension2");
			elem3.value = "";
			var elem4 = document.getElementById("ContactCell2");
			elem4.value = "";
			var elem5 = document.getElementById("ContactFax2");
			elem5.value = "";
			var elem6 = document.getElementById("ContactEmail2");
			elem6.value = "";

		}

		//return isSubmit;
	}

	function addFight(ButtonClick) {

		if (ButtonClick == 1) {
			$('#add1').hide();
			//$('#add2').show();
			$('#TextBoxesGroup2').show();

		} else if (ButtonClick == 2) {
			
			/* if(CheckValueExistsForGroup(2))
				{
			$('#add2').hide();
			//$('#add3').show();
			$('#TextBoxesGroup3').show();
				}
			else{
				alert("Please enter Any value for Contact 2 ");
			}
			 */
			$('#add2').hide();
			//$('#add3').show();
			$('#TextBoxesGroup3').show();
			
		} else if (ButtonClick == 3) {
			/* if(CheckValueExistsForGroup(2)&&CheckValueExistsForGroup(3))
			{
			$('#add3').hide();
			$('#TextBoxesGroup4').show();
			}
			else
				{
				alert("Please enter Any value for Contact 2 And Contact 3");
				}
			 */
			$('#add3').hide();
			$('#TextBoxesGroup4').show();
		}

		//return isSubmit;
	}

	function CheckValueExistsForGroup(GroupIndex)
	{
		
		if(GroupIndex==2)
			{
			var elem1 = document.getElementById("ContactName2").value.trim().length;
			var elem2 = document.getElementById("ContactPhone2").value.trim().length;
			var elem3 = document.getElementById("ContactExtension2").value.trim().length;
			var elem4 = document.getElementById("ContactCell2").value.trim().length;
			var elem5 = document.getElementById("ContactFax2").value.trim().length;
			var elem6 = document.getElementById("ContactEmail2").value.trim().length;
			
			if((elem1>0)||(elem2>0)||(elem3>0)||(elem4>0)||(elem5>0)){
				
				return true;
				}
			
			}
		else if(GroupIndex==3)
			{
			
			var elem1 = document.getElementById("ContactName3").value.trim().length;
			var elem2 = document.getElementById("ContactPhone3").value.trim().length;
			var elem3 = document.getElementById("ContactExtension3").value.trim().length;
			var elem4 = document.getElementById("ContactCell3").value.trim().length;
			var elem5 = document.getElementById("ContactFax3").value.trim().length;
			var elem6 = document.getElementById("ContactEmail3").value.trim().length;
			
			if((elem1>0)||(elem2>0)||(elem3>0)||(elem4>0)||(elem5>0)){
				
				return true;
				}
			} 
		else if(GroupIndex==4)
		{
		
			var elem1 = document.getElementById("ContactName4").value.trim().length;
			var elem2 = document.getElementById("ContactPhone4").value.trim().length;
			var elem3 = document.getElementById("ContactExtension4").value.trim().length;
			var elem4 = document.getElementById("ContactCell4").value.trim().length;
			var elem5 = document.getElementById("ContactFax4").value.trim().length;
			var elem6 = document.getElementById("ContactEmail4").value.trim().length;
			
			if((elem1>0)||(elem2>0)||(elem3>0)||(elem4>0)||(elem5>0)){
				
				return true;
				}
		} 
	
	}
	
	
	function isformSubmit() {

		return isSubmit;
	}

	function onSaveClick() {
		var isBlankFulfillmentId = isBlankFieldWithTrim('Fullfillment',
				'ErrorFullfillment', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');

		var isBlankName20 = isBlankFieldWithTrim('Name20', 'ErrorName20',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');

		var isBlankName50 = isBlankFieldWithTrim('Name50', 'ErrorName50',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');

		var isBlankAddressLine1 = isBlankFieldWithTrim('AddressLine1',
				'ErrorAddressLine1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');

		var isBlankCity = isBlankFieldWithTrim('City', 'ErrorCity',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');

		var isBlankContactName1 =true; /* isBlankFieldWithTrim('ContactName1',
				'ErrorContactName1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}'); */

		var isBlankContactPhone1 =true;/* isBlankFieldWithTrim('ContactPhone1',
				'ErrorContactPhone1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}'); */

		var isBlankContactCell1 =true;/*  isBlankFieldWithTrim('ContactCell1',
				'ErrorContactCell1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}'); */

		var isBlankContactExtension1 = true;/* isBlankFieldWithTrim('ContactExtension1',
				'ErrorContactExtension1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}'); */
		var isBlankContactFax1 =true;/*  isBlankFieldWithTrim('ContactFax1',
				'ErrorContactFax1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}'); */
		var isBlankContactEmail1 =true; /* isBlankFieldWithTrim('ContactEmail1',
				'ErrorContactEmail1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS()}');
 */
		/* var isValidContactEmail1=null;
		var isNumContactPhone1=null;
		var isNumContactExtension1=null;
		var isNumContactFax1=null;
		var isNumContactCell1=null; */

		var isValidContactEmail1 = false;
		var isNumContactPhone1 = false;
		var isNumContactExtension1 = false;
		var isNumContactFax1 = false;
		var isNumContactCell1 = false;

		var isValidContactEmail2 = true;
		var isNumContactPhone2 = true;
		var isNumContactExtension2 = true;
		var isNumContactFax2 = true;
		var isNumContactCell2 = true;

		var isValidContactEmail3 = true;
		var isNumContactPhone3 = true;
		var isNumContactExtension3 = true;
		var isNumContactFax3 = true;
		var isNumContactCell3 = true;

		var isValidContactEmail4 = true;
		var isNumContactPhone4 = true;
		var isNumContactExtension4 = true;
		var isNumContactFax4 = true;
		var isNumContactCell4 = true;

		//var isBlankZipCode = true;
		var isBlankZipCode = isBlankFieldWithTrim('ZipCode', 'ErrorZipCode',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ViewLabels.getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		
		if(isBlankZipCode)
		{
	isValidZipCode=isNumeric('ZipCode', 'ErrorZipCode',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			+ '${ViewLabels.getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		} 
		var isValidZipCode = true;
		var isValidStateCode = true;
		
		
		var IsAllowForSubmitAccToContactGroup=true;
		
		
		var eleCountryCode = document.getElementById("CountryCode");

		var isValidCountrySelected = false;
		var intCountrySelectedIndex = eleCountryCode.selectedIndex;
		//  var strOption1 = e1.options[e1.selectedIndex].value;

		if (intCountrySelectedIndex == 0) {
			isValidCountrySelected = false;
			setErrorMessage('ErrorCountryCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');

			//$('#ErrorCountryCode').show();
		} else {
			isValidCountrySelected = true;
			//$('#ErrorCountryCode').hide();
			isBlankStateCode('ZipCode', 'ErrorCountryCode',
			      '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			        + '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');

			if (eleCountryCode.options[intCountrySelectedIndex].value === '840') {
				var eleStateCode = document.getElementById("StateCode");
				var intStateSelectedIndex = eleStateCode.selectedIndex;

				if (intStateSelectedIndex == 0) {
					setErrorMessage('ErrorStateCode',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');
					isValidStateCode=false;
				//	$('#ErrorStateCode').show();

				} else {
					isValidStateCode=true;
				//	$('#ErrorStateCode').hide();
				}

				/* isBlankZipCode = isBlankFieldWithTrim('ZipCode', 'ErrorZipCode',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');
				
				if(isBlankZipCode)
					{
				isValidZipCode=isNumeric('ZipCode', 'ErrorZipCode',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ViewLabels.getLbl_Not_a_valid_number()}');
					} */

			} else {
				
			    isBlankStateCode('ZipCode', 'ErrorStateCode',
			      '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			        + '${ViewLabels.getERR_Mandatory_field_cannot_be_left_blank()}');
			//	$('#ErrorStateCode').hide();
				/* $('#ErrorZipCode').hide(); */

			}

		}

		if (isBlankContactEmail1) {
			isValidContactEmail1 = isValidEmailText('ContactEmail1',
					'ErrorContactEmail1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_email_id()}');
		}

		if (isBlankContactPhone1) {
			isNumContactPhone1 = isNumeric('ContactPhone1',
					'ErrorContactPhone1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');
		}
		if (isBlankContactExtension1) {
			isNumContactExtension1 = isNumeric('ContactExtension1',
					'ErrorContactExtension1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');
		}
		if (isBlankContactFax1) {
			isNumContactFax1 = isNumeric('ContactFax1', 'ErrorContactFax1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');
		}

		if (isBlankContactCell1) {
			isNumContactCell1 = isNumeric('ContactCell1', 'ErrorContactCell1',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');
		}

		if (!$("#TextBoxesGroup2").is(':hidden')) {

	/* var errMsgId = document.getElementById("Perror");
			
			if(CheckValueExistsForGroup(2)){
				IsAllowForSubmitAccToContactGroup=true;
			$('#ErrorMessage').hide();
			}
		else{
			IsAllowForSubmitAccToContactGroup=false;
			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ 'Please Provide All Contacts Details.';
		$('#ErrorMessage').show();
		document.location.href = '#top';
			} */
			
			isValidContactEmail2 = isValidEmailText('ContactEmail2',
					'ErrorContactEmail2',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_email_id()}');

			isNumContactPhone2 = isNumeric('ContactPhone2',
					'ErrorContactPhone2',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactExtension2 = isNumeric('ContactExtension2',
					'ErrorContactExtension2',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactFax2 = isNumeric('ContactFax2', 'ErrorContactFax2',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactCell2 = isNumeric('ContactCell2', 'ErrorContactCell2',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

		}

		if (!$("#TextBoxesGroup3").is(':hidden')) {

		
			
			/*	var errMsgId = document.getElementById("Perror"); 
			if(CheckValueExistsForGroup(2) && CheckValueExistsForGroup(3)){
				IsAllowForSubmitAccToContactGroup=true;
			$('#ErrorMessage').hide();
			}
		else{
			IsAllowForSubmitAccToContactGroup=false;
			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				+ 'Please Provide All Contacts Details.';
		$('#ErrorMessage').show();
		document.location.href = '#top';
			} */
			
			isValidContactEmail3 = isValidEmailText('ContactEmail3',
					'ErrorContactEmail3',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_email_id()}');

			isNumContactPhone3 = isNumeric('ContactPhone3',
					'ErrorContactPhone3',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactExtension3 = isNumeric('ContactExtension3',
					'ErrorContactExtension3',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactFax3 = isNumeric('ContactFax3', 'ErrorContactFax3',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactCell3 = isNumeric('ContactCell3', 'ErrorContactCell3',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

		}

		if (!$("#TextBoxesGroup4").is(':hidden')) {
			
		/* 	var errMsgId = document.getElementById("Perror");
			
			if(CheckValueExistsForGroup(2) && CheckValueExistsForGroup(3) && CheckValueExistsForGroup(4)){
				IsAllowForSubmitAccToContactGroup=true;
				$('#ErrorMessage').hide();
				}
			else{
				IsAllowForSubmitAccToContactGroup=false;
				errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ 'Please Provide All Contacts Details.';
			$('#ErrorMessage').show();
			document.location.href = '#top';
				} */
			

			isValidContactEmail4 = isValidEmailText('ContactEmail4',
					'ErrorContactEmail4',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_email_id()}');

			isNumContactPhone4 = isNumeric('ContactPhone4',
					'ErrorContactPhone4',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactExtension4 = isNumeric('ContactExtension4',
					'ErrorContactExtension4',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactFax4 = isNumeric('ContactFax4', 'ErrorContactFax4',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

			isNumContactCell4 = isNumeric('ContactCell4', 'ErrorContactCell4',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');

		}
		
		var IsValidMainFax=true;
		var intMainFaxLength = $('#MainFax').val().trim().length;
		if(intMainFaxLength>0)
			{
			IsValidMainFax=isNumeric('MainFax', 'ErrorMainFax',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_number()}');
			}
		else{
			
			var errMsgId=document.getElementById('ErrorMainFax');
			   errMsgId.innerHTML = "";
		//	$('#ErrorMainFax').hide();
		}
		
		var IsValidMainEmail=true;
		var intMainEmailLength = $('#MainEmail').val().trim().length;
		if(intMainEmailLength>0)
			{
			IsValidMainEmail=isValidEmailText('MainEmail', 'ErrorMainEmail',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ViewLabels.getLbl_Not_a_valid_email_id()}');
			}
		else
			{
			var errMsgId=document.getElementById('ErrorMainEmail');
			   errMsgId.innerHTML = "";
			//$('#ErrorMainEmail').hide();
			}
		
	
		
		
		
		if (isBlankFulfillmentId && isBlankName20 && isBlankName50
				&& isBlankAddressLine1 && isBlankCity && isBlankContactName1
				&& isBlankContactPhone1 && isBlankContactCell1
				&& isBlankContactExtension1 && isBlankContactFax1
				&& isBlankContactEmail1 && isValidContactEmail1
				&& isNumContactPhone1 && isNumContactExtension1
				&& isNumContactFax1 && isNumContactCell1
				&& isValidContactEmail2 && isNumContactPhone2
				&& isNumContactExtension2 && isNumContactFax2
				&& isNumContactCell2 && isValidContactEmail3
				&& isNumContactPhone3 && isNumContactExtension3
				&& isNumContactFax3 && isNumContactCell3
				&& isValidContactEmail4 && isNumContactPhone4
				&& isNumContactExtension4 && isNumContactFax4
				&&IsAllowForSubmitAccToContactGroup
				&& isNumContactCell4 && IsValidMainEmail 
				&& IsValidMainFax && isBlankZipCode 
				&& isValidZipCode && isValidStateCode
		) {
			
			
		

			var IsCheckNext = true;
			var ValueName20=$("#Name20").val();
			var ValueName50=$("#Name50").val();
			var PageName="Add";
			var PageNameEdit="Edit";
			
			if (isBlankAddressLine1 && isValidCountrySelected) {
				var zipcode = $('#ZipCode').val();
				var address1 = $('#AddressLine1').val();
				var address2 = $('#AddressLine2').val();
				var address3 = $('#AddressLine3').val();
				var address4 = $('#AddressLine4').val();
				var city = $('#City').val();
				var Countrycode = $("#CountryCode option:selected").text();
				var Statecode = $("#StateCode option:selected").text();
				if (Statecode == '${ViewLabels.getLbl_Select()}...') {
					Statecode = "";
				}
				var ZipCode = $('#ZipCode').val();

				var errMsgId = document.getElementById("Perror");

				$
						.post(
								"${pageContext.request.contextPath}/RestCheckAddress",
								{
									TeamMemberAddress1 : address1,
									TeamMemberAddress2 : address2,
									TeamMemberAddress3 : address3,
									TeamMemberAddress4 : address4,
									TeamMemberCity : city,
									TeamMemberStateCode : Statecode,
									TeamMemberCountryCode : Countrycode,
									TeamMemberZipCode : ZipCode
								},
								function(data, status) {

									if (data) {
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${ViewLabels.getERR_Invalid_Address()}';
										$('#ErrorMessage').show();
										//document.location.href = '#top';
										window.scrollTo(0,0);

									} else {

										$('#ErrorMessage').hide();

										var intZipLength = $('#ZipCode').val()
												.trim().length;

										if (intZipLength > 0) {
											var zipcode1 = $('#ZipCode').val();
											//http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
											$
													.post(
															"${pageContext.request.contextPath}/RestCheckZipCode",
															{
																TeamMemberZipCode : zipcode1
															},
															function(data,
																	status) {

																if (data) {
																	errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
																			+ '${ViewLabels.getERR_Invalid_ZipCode()}';
																	$(
																			'#ErrorMessage')
																			.show();
																	//document.location.href = '#top';
																	window.scrollTo(0,0);

																} else {

																	$(
																			'#ErrorMessage')
																			.hide();
																	var fulfillmentVal = document
																			.getElementById('Fullfillment').value
																			.trim();
																	var viewIsFulfillmentExists = ${viewIsFulfillmentExists};
																	
																	var tenant = '${ViewTenant}';
																	

																	if (!viewIsFulfillmentExists) {
																		$
																				.post(
																						"${pageContext.request.contextPath}/IsFulfillmentAvailable",
																						{
																							Tenant : tenant,
																							Fullfillment : fulfillmentVal
																						},
																						function(
																								data,
																								status) {

																							if (data.boolStatus) {
																								
																								if(data.strMessage==='A')
                         {
                        errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                          + '${ViewLabels.getERR_Fulfillment_center_already_used()}';
                         }
                        else if(data.strMessage==='X')
                         {
                         errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                          + '${ViewLabels.getERR_Fulfillment_Center_aready_exist_with_inactive_status()}';
                         }
                          $(
                          '#ErrorMessage')
                          .show();
                        isSubmit=false;
                      //  document.location.href = '#top';
						window.scrollTo(0,0);
																								
																							} else {
																							
			$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionShort",
				{
					FulFillmentCenter:fulfillmentVal,
					Description20 : ValueName20,
					ScreenName : PageName
					
				},
				function(data, status) {
					if (data) {
							isSubmit = false;
							var errMsgId = document.getElementById("Perror");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+'${ViewLabels.getLbl_Name_Short_already_used()}';
							$('#ErrorMessage').show();
							window.scrollTo(0,0);
						// alert ("User Already Exists");
					} else {			
								
								$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionLong",
							{
								FulFillmentCenter:fulfillmentVal,
								Description50 : ValueName50,
								ScreenName : PageName
								
							},
							function(data, status) {
								if (data) {
								isSubmit = false;
									var errMsgId = document.getElementById("Perror");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+'${ViewLabels.getLbl_Name_Long_already_used()}';
									$('#ErrorMessage').show();
									window.scrollTo(0,0);
									// alert ("User Already Exists");
								} else {
										
										isSubmit=true;
										$('#ErrorMessage').hide();
										document.myForm.action = '${pageContext.request.contextPath}/AddUpdateFullfillmentCenter';
										document.myForm.submit();
								}
							});													
					}
				});
																								
																							}

																						});

																	}
																	else
																	{
																	
			$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionShort",
				{
					FulFillmentCenter:fulfillmentVal,
					Description20 : ValueName20,
					ScreenName : PageNameEdit
					
				},
				function(data, status) {
					if (data) {
							isSubmit = false;
							var errMsgId = document.getElementById("Perror");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+'${ViewLabels.getLbl_Name_Short_already_used()}';
							$('#ErrorMessage').show();
							window.scrollTo(0,0);
						// alert ("User Already Exists");
					} else {			
								
								$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionLong",
							{
								FulFillmentCenter:fulfillmentVal,
								Description50 : ValueName50,
								ScreenName : PageNameEdit
								
							},
							function(data, status) {
								if (data) {
								isSubmit = false;
									var errMsgId = document.getElementById("Perror");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+'${ViewLabels.getLbl_Name_Long_already_used()}';
									$('#ErrorMessage').show();
	
									window.scrollTo(0,0);
	
									// alert ("User Already Exists");
								} else {

										isSubmit=true;
										$('#ErrorMessage').hide();
										document.myForm.action = '${pageContext.request.contextPath}/AddUpdateFullfillmentCenter';
										document.myForm.submit();
								}
							});													
					}
				});
																	
																	}
																}
															});

										} else {
											var fulfillmentVal = document
													.getElementById('Fullfillment').value
													.trim();
											var viewIsFulfillmentExists = ${viewIsFulfillmentExists};

											var tenant ='${ViewTenant}';
											if (!viewIsFulfillmentExists) {
												$
														.post(
																"${pageContext.request.contextPath}/IsFulfillmentAvailable",
																{
																	Tenant : tenant,
																	Fullfillment : fulfillmentVal
																},
																function(data,
																		status) {

																	if (data.boolStatus) {
																		if(data.strMessage==='A')
                         {
                        errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                          + '${ViewLabels.getERR_Fulfillment_center_already_used()}';
                         }
                        else if(data.strMessage==='X')
                         {
                         errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                          + '${ViewLabels.getERR_Fulfillment_Center_aready_exist_with_inactive_status()}';
                         }
                          $(
                          '#ErrorMessage')
                          .show();
                        isSubmit=false;
                      //  document.location.href = '#top';
						window.scrollTo(0,0);
																	} else {
			$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionShort",
				{
					FulFillmentCenter:fulfillmentVal,
					Description20 : ValueName20,
					ScreenName : PageName
					
				},
				function(data, status) {
					if (data) {
							isSubmit = false;
							var errMsgId = document.getElementById("Perror");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+'${ViewLabels.getLbl_Name_Short_already_used()}';
							$('#ErrorMessage').show();
							window.scrollTo(0,0);
						// alert ("User Already Exists");
					} else {			
								
								$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionLong",
							{
								FulFillmentCenter:fulfillmentVal,
								Description50 : ValueName50,
								ScreenName : PageName
								
							},
							function(data, status) {
								if (data) {
								isSubmit = false;
									var errMsgId = document.getElementById("Perror");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+'${ViewLabels.getLbl_Name_Long_already_used()}';
									$('#ErrorMessage').show();
									window.scrollTo(0,0);
									// alert ("User Already Exists");
								} else {
									
										isSubmit=true;
										$('#ErrorMessage').hide();
										document.myForm.action = '${pageContext.request.contextPath}/AddUpdateFullfillmentCenter';
										document.myForm.submit();
								}
							});													
					}
				});
																								/*alert("2");
																		isSubmit=true
																		$(
																				'#ErrorMessage')
																				.hide();
																		
																		document.myForm.action = '${pageContext.request.contextPath}/AddUpdateFullfillmentCenter';
																		 document.myForm.submit();*/
																	}

																});

											}
											else
												{
												$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionShort",
				{
					FulFillmentCenter:fulfillmentVal,
					Description20 : ValueName20,
					ScreenName : PageNameEdit
					
				},
				function(data, status) {
					if (data) {
							isSubmit = false;
							var errMsgId = document.getElementById("Perror");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+'${ViewLabels.getLbl_Name_Short_already_used()}';
							$('#ErrorMessage').show();
							window.scrollTo(0,0);
						// alert ("User Already Exists");
					} else {			
								
								$.post("${pageContext.request.contextPath}/RestCheckUniqueFulfillmentDescriptionLong",
							{
								FulFillmentCenter:fulfillmentVal,
								Description50 : ValueName50,
								ScreenName : PageNameEdit
								
							},
							function(data, status) {
								if (data) {
								isSubmit = false;
									var errMsgId = document.getElementById("Perror");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+'${ViewLabels.getLbl_Name_Long_already_used()}';
									$('#ErrorMessage').show();
									window.scrollTo(0,0);
									// alert ("User Already Exists");
								} else {
										
										isSubmit=true;
										$('#ErrorMessage').hide();
										document.myForm.action = '${pageContext.request.contextPath}/AddUpdateFullfillmentCenter';
										document.myForm.submit();
								}
							});													
					}
				});
												/*alert("21");
												isSubmit=true;
												document.myForm.action = '${pageContext.request.contextPath}/AddUpdateFullfillmentCenter';
												 document.myForm.submit();*/
												}
										}

									}

									//	alert(data);
								});

			}

			/*  var intZipLength=$('#ZipCode').val().trim().length;
			
			if(intZipLength>0)
				{

				
				 if(IsCheckNext)
				 {
			      var zipcode1=$('#ZipCode').val();
				  //http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
				  $.post( "${pageContext.request.contextPath}/RestCheckZipCode",
						  {
					     TeamMemberZipCode:zipcode1
					      },
					      function( data,status ) {
					    	
					    	  if(data)
			        	 		{
							 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'Not a valid zip';
				        	 $('#ErrorMessage').show();
				        	 IsCheckNext=false;
				        	 
			        	 		}
							 else
								 {
								 IsCheckNext=true;	 
								 $('#ErrorMessage').hide();
								 }
					      }
					      );
				}
				} */

			/* var fulfillmentVal=document.getElementById('Fullfillment').value.trim();
			var viewIsFulfillmentExists=	${viewIsFulfillmentExists};
			
			var tenant='43310asp-d6d4-4715-8762-6dcba01010a';
			
			 if(IsCheckNext)
			 {
			if(!viewIsFulfillmentExists)
				{
			  $.post( "${pageContext.request.contextPath}/IsFulfillmentAvailable",
					  {
				  Tenant:tenant,
				  Fullfillment:fulfillmentVal
				  }, function( data,status ) {
						  
						if(data.boolStatus)
							{
							 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'not available';
				        	 $('#ErrorMessage').show();
				        	 IsCheckNext=false;
							}
						else
							{
							
							 IsCheckNext=true;
							 $('#ErrorMessage').hide();
							}
						  
					  });
			
				}
			
			
			 } */
		}
	else{
	window.scrollTo(0,0);
	}

	}
	
	

	
	
	function reloadPage(){
	/* location.reload(); */
		window.parent.location = window.parent.location.href;
	}
	function onCancelClick()
	{
	var IsFullfillmentExists='${viewIsFulfillmentExists}';
	
	if(IsFullfillmentExists==='true'){
		
		var CURRENTDOCUMENTURL=window.location.href.substring(window.location.href.lastIndexOf("/"),window.location.href.length);
		
		if(CURRENTDOCUMENTURL.indexOf("FullfillmentCenterMaintenanceUpdate") > -1)
			{
			window.history.back();
			}
		else if(CURRENTDOCUMENTURL==="/FullfillmentCenterMaintenance")
		{
			window.location.href ='${pageContext.request.contextPath}/fullfillmentCenterLookup';
		}
		else{
		window.location.href ='${pageContext.request.contextPath}/FullfillmentCenterDisplayAllLookUp';
		}
		}
	else{
		
		//window.location.href ='${pageContext.request.contextPath}/FullfillmentCenterMaintenance_new';
		window.history.back();
	}
		
		
	}
	
function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 	$
		.get(
				"${pageContext.request.contextPath}/HeaderLanguageChange",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		
		
	}
	
function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'fulfillment',
	 		InfoHelpType:'fullscreen'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
	
	function actionForm(url, action) {

		if (action == 'search') {
			var TeamMemberName = isBlankFieldWithTrim('TeamMemberName', 'Perror',
					'At least one field is required for search.');

			var PartOfTeamMemberName = isBlankFieldWithTrim('PartOfTeamMemberName',
					'Perror', 'At least one field is required for search.');

			if (TeamMemberName || PartOfTeamMemberName) {
				document.myForm.action = url;
				document.myForm.submit();
				$('#ErrorMessage').hide();
				isSubmit = true;

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
				window.scrollTo(0,0);
			}

		} else if (action == 'searchTeamMember') {
			var elem1 = document.getElementById("PartOfTeamMemberName");
			elem1.value = "";
			var TeamMemberName = isBlankFieldWithTrim(
					'TeamMemberName',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${viewLabels.getErrTeamMemberNotLeftBlank()}');
			if (TeamMemberName) {
				document.myForm.action = url;
				document.myForm.submit();
				$('#ErrorMessage').hide();
				isSubmit = true;

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				window.scrollTo(0,0);
				isSubmit = false;
			}

		} else if (action == 'searchPartOfTeamMember') {
			var elem = document.getElementById("TeamMemberName");
			elem.value = "";
			var PartOfTeamMemberName = isBlankFieldWithTrim(
					'PartOfTeamMemberName',
					'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${viewLabels.getErrPartOfTeamMemberNotLeftBlank()}');

			if (PartOfTeamMemberName) {
				document.myForm.action = url;
				document.myForm.submit();
				$('#ErrorMessage').hide();
				isSubmit = true;

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				window.scrollTo(0,0);
				isSubmit = false;
			}
		} else if (action == 'displayall') {

			var elem = document.getElementById("TeamMemberName");
			elem.value = "";
			var elem1 = document.getElementById("PartOfTeamMemberName");
			elem1.value = "";

			document.myForm.action = url;
			document.myForm.submit();
			$('#ErrorMessage').hide();
			isSubmit = true;
		}

	}
	
function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}

	function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'Company',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						//  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
	
</script>

<script>
	jQuery(document).ready(function() {
	var nameofscreen=$('#IsNew').val();
	
	if(nameofscreen=="false"){
	$('#SetUpDate').val(getLocalDate());
	$('#SetUpDateH').val(getUTCDateTime());
	$('#LastActivityDateString').val(getLocalDate());
	$('#LastActivityDateStringH').val(getUTCDateTime());
	}
	else{
		$('#SetUpDate').val(ConvertUTCToLocalTimeZone('${ViewFulfillment.getSetUpDate()}'));
		$('#LastActivityDateString').val(ConvertUTCToLocalTimeZone('${ViewFulfillment.getLastActivityDateString()}'));
		$('#LastActivityDateStringH').val(getUTCDateTime());
	}
		var value2 = '${ViewShowSecondContactDiv}';
		var value3 = '${ViewShowThirdContactDiv}';
		var value4 = '${ViewShowFourthContactDiv}';

		if (value2 === 'true') {
			addFight(1);

		}
		if (value3 === 'true') {
			addFight(2);

		}
		if (value4 === 'true') {
			addFight(3);

		}

		var value = '${PageRedirectLoadMessage}';

		if (value.length > 5) {
			$('#ErrorMessage').show();
		} else {
			$('#ErrorMessage').hide();
		}
		
		
		
		
		
		var ViewIsSuccessMessage='${ViewIsSuccessMessage}';
	

		
		 if (ViewIsSuccessMessage.length > 5) {
			 bootbox.alert(ViewIsSuccessMessage,function(){
				 window.location.href ='${pageContext.request.contextPath}/FullfillmentCenterDisplayAllLookUp';
			 });
			
			
			
		} else {
		//	alert(ViewIsSuccessMessage);
		} 
		
		/* var ISStateValue='${ViewFulfillment.getStateCode()}';
		if(ISStateValue.length==0)
			{
			$("#StateCode").val('none');
			} */
		
		
		var varCountryCodeValue='${ViewFulfillment.getCountryCode()}';
		
		if(varCountryCodeValue==='840'){
			
			$("#StateRequiredID").show();
			//$("#ZipRequiredID").show();
			//	$('#ZipCode').removeAttr("disabled");
			$("#StateCode").removeAttr("disabled");
			//$("#StateCode").val($("#Statecode option:first").val());

		} else {
			$("#StateRequiredID").hide();
			//$("#ZipRequiredID").hide();
			//	$('#ZipCode').attr('disabled', 'disabled');
			//$("#StateCode").val($("#Statecode option:first").val());
		//	$('#ZipCode').val('');

			$('#StateCode').attr('disabled', 'disabled');
			$("#StateCode").val('none');
		}
		

		setInterval(function () {
				
		    var h = window.innerHeight;
		    if(window.innerHeight>=900){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);

		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)
		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});

	});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>