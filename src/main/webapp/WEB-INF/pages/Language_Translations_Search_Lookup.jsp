<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<style>
fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}

.k-grid-content {
 position: relative;
	width: 100%;
	overflow: auto;
	overflow-x: auto;
	overflow-y: hidden !important;
	zoom: 0;
}

.k-grid th.k-header, .k-grid-header {
	/*text-align: center !important;*/
	/* white-space: normal !important;
	padding-right: 0px !important; */
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
	white-space: normal !important;
}
.k-grid .k-button {
margin: 2px .16em !important;
}
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

<div id="container" style="position: relative" class="loader_div">
		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->

		<!-- BEGIN PAGE CONTENT -->
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${ViewLabels.getLanguageTranslation_LTSearchLookup()}</h1>
					</div>
				</div>

				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content" >
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<!--<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Dashboard
				</li>
			</ul>-->
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<div class="col-md-12">
									<div class="portlet">
									
						<a id="AddBtnAppIcon" href="${pageContext.request.contextPath}/Language_Translations_Maintenance" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${ViewLabels.getLanguageTranslation_AddNew()} </span>
								</a>
				<div class="portlet-body">
				<div class="table-container">
						
						<kendo:grid name="LanguagesList" id="LanguagesList" resizable="true" 
						reorderable="true"  sortable="true" columnResize="true" dataBound="gridDataBound">
						
				<kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5">
    	        </kendo:grid-pageable>
				<kendo:grid-editable mode="inline" confirmation="Message" />
					<kendo:grid-columns>
					
					<kendo:grid-column title="${ViewLabels.getLanguageTranslation_Language()}" field="description20" />
					<kendo:grid-column title="${ViewLabels.getLanguageTranslation_KeyPhrase()}" field="keyPhrase" />
					<kendo:grid-column title="${ViewLabels.getLanguageTranslation_Translation()}" field="translation" />
					
     				<kendo:grid-column title="${ViewLabels.getLanguageTranslation_Action()}">
						<kendo:grid-column-command>
							<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
						<kendo:grid-column-commandItem className="icon-pencil btn btn-sm yellow filter-submit margin-bottom" name="editDetails" text="${ViewLabels.getLanguageTranslation_Edit()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function showDetails(e) 
                            {
                                  e.preventDefault();
	                              var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                  var language=dataItem.language;
                                  var Keyphrase=dataItem.keyPhrase;
                                  var lastActivityDate=dataItem.lastActivityDate;
                                  var LastActivityBy=dataItem.lastActivityBy;
                                  var Description=dataItem.description20;
                                  var translation=dataItem.translation;
                                  
                                  var url= "${pageContext.request.contextPath}/Language_Edit";

                                var myform2 = document.createElement("form");

                                var LanguageField = document.createElement("input");
                                LanguageField.value = language;
                                LanguageField.name = "Language";
                                LanguageField.setAttribute("type", "hidden");
                                
                                var KeyPhraseField = document.createElement("input");
                                KeyPhraseField.value = Keyphrase;
                                KeyPhraseField.name = "KeyPhrase";
                                KeyPhraseField.setAttribute("type", "hidden");
                                
                                
                                Description
                                var DescriptionField = document.createElement("input");
                                DescriptionField.value = Description;
                                DescriptionField.name = "Description";
                                DescriptionField.setAttribute("type", "hidden");
                                
                               /*  var lastActivityDateField = document.createElement("input");
                                lastActivityDateField.value = lastActivityDate;
                                lastActivityDateField.name = "lastActivityDate";
                                lastActivityDateField.setAttribute("type", "hidden");
                                
                                var LastActivityByField = document.createElement("input");
                                LastActivityByField.value = lastActivityBy;
                                LastActivityByField.name = "LastActivityBy";
                                LastActivityByField.setAttribute("type", "hidden");
                                           */
                                myform2.action = url;
                                myform2.method = "post";
                               
                                
                                myform2.appendChild(LanguageField);
                                myform2.appendChild(KeyPhraseField);
                                myform2.appendChild( DescriptionField);
                               // myform2.appendChild(lastActivityDateField);
                                // myform2.appendChild(LastActivityByField);
                                
                                
                               
                                document.body.appendChild(myform2);
                                myform2.submit();
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
							
						<kendo:grid-column-commandItem className="fa fa-times btn btn-sm red filter-cancel"  name="deleteDetails" text="${ViewLabels.getLanguageTranslation_Delete()}">
							   <kendo:grid-column-commandItem-click>
                            <script>
                            function deleteConfirmation(e) {
                            	  e.preventDefault();
                            	  var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
						    	  var language=dataItem.language;
                                  var Keyphrase=dataItem.keyPhrase;
                            	  bootbox.confirm(
                                    '${ViewLabels.getLanguageTranslation_DeleteMessage()}',
                                    function(okOrCancel) {

                                     if(okOrCancel == true)
                                     {
           					
                            	/* 
							    if (confirm('${ViewLabels.getLanguageTranslation_DeleteMessage()}') == true) {
							     */	
							    	
							    	
							    	  
							    	  // '${pageContext.request.contextPath}/Teammember_Message_delete/${TeamMemberMessages.getTeamMemberMessagesId().getTenant()}
							    	  // /${TeamMemberMessages.getTeamMemberMessagesId().getTeamMember()}/${TeamMemberMessages.getTeamMemberMessagesId().getCompany()}
							    	  // /${TeamMemberMessages.getTicketTapeMessge()}'
                                var url= "${pageContext.request.contextPath}/Language_Delete/";
                                
                                
                                var myform1 = document.createElement("form");

                                var LanguageField = document.createElement("input");
                                LanguageField.value = language;
                                LanguageField.name = "Language";
                                LanguageField.setAttribute("type", "hidden");
                                
                                var KeyPhraseField = document.createElement("input");
                                KeyPhraseField.value = Keyphrase;
                                KeyPhraseField.name = "KeyPhrase";
                                KeyPhraseField.setAttribute("type", "hidden");
                                          
                                myform1.action = url;
                                myform1.method = "post";
                               
                                
                                myform1.appendChild(LanguageField);
                                myform1.appendChild(KeyPhraseField);
                               
                                document.body.appendChild(myform1);
                                myform1.submit();
						 
							    	
							    }
                                     });
							    
							}
                                     
                            </script>
                        </kendo:grid-column-commandItem-click>
                         </kendo:grid-column-commandItem>
                        
						</kendo:grid-column-command>
					</kendo:grid-column>
				</kendo:grid-columns>
		                    
				<kendo:dataSource pageSize="10" autoSync="true">
					<kendo:dataSource-transport>
						<kendo:dataSource-transport-read cache="false" url="${pageContext.request.contextPath}/Language_Translation_LanguageData/Grid/read"></kendo:dataSource-transport-read>
						<kendo:dataSource-transport-parameterMap>
							<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
						</kendo:dataSource-transport-parameterMap>

					</kendo:dataSource-transport>
				<kendo:dataSource-schema>
                <kendo:dataSource-schema-model id="intKendoID">
                    <kendo:dataSource-schema-model-fields>
                        <kendo:dataSource-schema-model-field name="intKendoID" >
                        	
                        </kendo:dataSource-schema-model-field>
                         
                        
                    </kendo:dataSource-schema-model-fields>
                </kendo:dataSource-schema-model>
            </kendo:dataSource-schema>



				</kendo:dataSource>
			</kendo:grid>
		
			</div>
			</div>
			</div>
			</div>
		
		</div>	
										<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
				<!-- END PAGE CONTENT -->
			</div>
		</div>
		
		
	</div>	
	
		
	</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
 type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
 type="text/javascript"></script>
 <script>
 
 
 function gridDataBound(e) {
     var grid = e.sender;
     if (grid.dataSource.total() > 0) {
      var colCount = grid.columns.length;
      kendo.ui.progress(ajaxContainer, false);

     }
     else
      {
       kendo.ui.progress(ajaxContainer, false);
      }
    };
    
    var ajaxContainer = $("#container");
    kendo.ui.progress(ajaxContainer, true);
 
 
 function headerChangeLanguage(){
 	
 	/* alert('in security look up'); */
 	
  	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
 	
 }

 function headerInfoHelp(){
 	
 	//alert('in security look up');
 	
  	/* $
 	.post(
 			"${pageContext.request.contextPath}/HeaderInfoHelp",
 			function(
 					data1) {

 				if (data1.boolStatus) {

 					location.reload();
 				}
 			});
 	 */
  	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
 			  {InfoHelp:'LanguageTranslation',
  		InfoHelpType:'PROGRAM'
 		 
 			  }, function( data1,status ) {
 				  if (data1.boolStatus) {
 					  window.open(data1.strMessage); 
 					  					  
 					}
 				  else
 					  {
 					//  alert('No help found yet');
 					  }
 				  
 				  
 			  });
 	
 }
	jQuery(document).ready(function() {
	setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30); 
		// Metronic.init(); // init metronic core componets
		//Layout.init(); // init layout
		/*Demo.init(); // init demo(theme settings page)
		Index.init(); // init index page
		Tasks.initDashboardWidget(); // init tash dashboard widget */
	});
	 
	
</script> 
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>