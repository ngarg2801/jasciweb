<!--
----------------------------------------------------------------------------------------------------------
 		Program Name : SmartTaskExecutions_SearchLookup.jsp
---------------------------------------------------------------------------------------------------------- 		
 		
Date Developed  Apr 15 2015
Description It is used to show the list of SMART_TASK_EXECUTIONS Table and Edit record on behalf of Primary Composite Keys
Created By Aakash Bishnoi -->



<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<!-- BEGIN PAGE CONTAINER -->
		<div id="container" style="position: relative" class="loader_div">
			<div class="page-container">
				<!-- BEGIN PAGE HEAD -->
				<div class="page-head">
					<div class="container">
						<!-- BEGIN PAGE TITLE -->
						<div class="page-title">
							<h1>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Search_Lookup()}</h1>
						</div>


					</div>
				</div>
				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<!--<ul class="page-breadcrumb breadcrumb hide">
				<li>
					<a href="#">Home</a><i class="fa fa-circle"></i>
				</li>
				<li class="active">
					Dashboard
				</li>
			</ul>-->
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<div class="col-md-12">
								<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
								<!-- Begin: life time stats -->
								<div class="portlet">
										<div>
											<a  id="AddBtnSmartTaskExecutions" href="${pageContext.request.contextPath}/SmartTaskExecutions_Maintenance" class="btn default yellow-stripe">
													<i class="fa fa-plus"></i>
													<span class="hidden-480">
													 		${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Add_New()}
													</span>
											</a>
										</div>
									
									<div class="portlet-body">
										<div class="table-container">
											<div class="table-actions-wrapper">
												<span></span>
											</div>
											
											<div>
												<kendo:grid name="SmartTaskConfigurator" resizable="true"  reorderable="true" sortable="true" dataBound="gridDataBound">
												<kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5"></kendo:grid-pageable>
												<kendo:grid-editable mode="inline" confirmation="" />
												
												<kendo:grid-columns>
													<kendo:grid-column field="tenant_Id" 	title=" ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Tenant()}"	width="6.5%" />
													<kendo:grid-column field="company_Id"  title=" ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Company()}"	width="6.5%" />
             										<kendo:grid-column field="execution_Type_Description"  title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Type()}"  width="6.5%" />
             										<kendo:grid-column field="execution_Device_Description"  title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Device()}"  width="6%" />
             										<kendo:grid-column field="execution_Group_Description"  title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Group()}"  width="8%" />
            										<kendo:grid-column field="execution_Name"  title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution()}"  width="14.5%" />
             										<kendo:grid-column field="description50"  title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Description()}"  width="13.5%" />
             										<kendo:grid-column field="button"  title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Buttons()}"  width="6%" />
             										<kendo:grid-column field="application_Id_Description"  title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Application()}"  width="8%"  />
             										<kendo:grid-column field="action" title="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Actions()}"   width="17%">
             										
              										<kendo:grid-column-command>
              										
              											<kendo:grid-column-commandItem
																className="icon-pencil btn btn-sm yellow filter-submit margin-bottom"
																name="editDetails"
																text="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Edit()}">
																<kendo:grid-column-commandItem-click>
																	<script>
						        										function showDetails(e)  {
                                   												e.preventDefault();
                                   												var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                   												
                                   												var StrTenant_Id=dataItem.tenant_Id;
                                 												var StrCompany_Id=dataItem.company_Id;
                                 												var StrExecution_Name=dataItem.execution_Name;
                                 												
                                 								//				bootbox.alert('DeleteExecutionDetails Execution Name = ' + StrExecution_Name);
                                 								//				bootbox.alert('DeleteExecutionDetails Company Id = ' + StrCompany_Id);
                                 								//				bootbox.alert('DeleteExecutionDetails Tenant Id = ' + StrTenant_Id);
                                 												
                                 												var url= "${pageContext.request.contextPath}/SmartTaskExecutions_GetExecutions";
																				var myform = document.createElement("form");
											
																				var TENANT_IDField = document.createElement("input");
              						  		  										  TENANT_IDField.type = 'hidden';
             							         									  TENANT_IDField.value = StrTenant_Id;
             							          									  TENANT_IDField.name = "TENANT_ID";

             							     									var COMPANY_IDField = document.createElement("input");
              							 		  									  COMPANY_IDField.type = 'hidden';
               						          								          COMPANY_IDField.value = StrCompany_Id;
               							       										  COMPANY_IDField.name = "COMPANY_ID";
				
               							 										var EXECUTION_NAMEField = document.createElement("input");
                   							   										  EXECUTION_NAMEField.type = 'hidden';
                    						                                          EXECUTION_NAMEField.value = StrExecution_Name;
                    						                                          EXECUTION_NAMEField.name = "EXECUTION_NAME";     

																					myform.action = url;
																					myform.method = "post"
																					myform.appendChild(TENANT_IDField);
																					myform.appendChild(COMPANY_IDField);
																					myform.appendChild(EXECUTION_NAMEField);
																					document.body.appendChild(myform);
																					myform.submit();
       																 	} 
        															</script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>
															
															<kendo:grid-column-commandItem
																className="icon-pencil btn btn-sm yellow filter-submit margin-bottom"
																name="deleteDetails"
																text="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Delete()}">
																<kendo:grid-column-commandItem-click>
																	<script>
																	
						        										function deleteExecutionDetails(e)  {
                                   												e.preventDefault();
                                   												var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                   												
                                 												var StrTenant_Id=dataItem.tenant_Id;
                                 												var StrCompany_Id=dataItem.company_Id;
                                 												var StrExecution_Name=dataItem.execution_Name;
                                 												
                                 										//		bootbox.alert('DeleteExecutionDetails Tenant Id = ' + StrTenant_Id);
                                 										//		bootbox.alert('DeleteExecutionDetails Company Id = ' + StrCompany_Id);
                                 										//		bootbox.alert('DeleteExecutionDetails Execution Name = ' + StrExecution_Name);
                                 												
                                 												bootbox.confirm(
                                          												'${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Are_you_sure_you_want_to_delete_this_record()}',
                                          												function(okOrCancel)  {
                                            													if (okOrCancel == true)  {
                                            														$.post("${pageContext.request.contextPath}/SmartTaskExecutions_IsSeqInstructionsExist",
	                    																				 {
                                            															    Tenant_Id : StrTenant_Id,
                                            															    Company_Id : StrCompany_Id,
                                            															    Execution_Name : StrExecution_Name
	                    																				 },
	                    																				function(data, status) {
	                    																					if (data) {
	                    																						bootbox.alert("${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted()}",
	                    																								function() {
	                    																								});
	                    																					}
 	                    																   				   else {
 	                    																   								var url= "${pageContext.request.contextPath}/SmartTaskExecutions_Delete";
                                  																						var myform = document.createElement("form");
                                  												
                                  																						var TENANT_IDField = document.createElement("input");
                  	                                						  		  										  TENANT_IDField.type = 'hidden';
                  	                               							         										  TENANT_IDField.value = StrTenant_Id;
                  	                               							          										  TENANT_IDField.name = "TENANT_ID";
                                   
                  	                               							     										var COMPANY_IDField = document.createElement("input");
                   	                               							 		  										  COMPANY_IDField.type = 'hidden';
                   	                                						          								          COMPANY_IDField.value = StrCompany_Id;
                   	                                							       										  COMPANY_IDField.name = "COMPANY_ID";
														
                   	                                							 										var EXECUTION_NAMEField = document.createElement("input");
	                    	                               							   										  EXECUTION_NAMEField.type = 'hidden';
	                    	                                						                                          EXECUTION_NAMEField.value = StrExecution_Name;
	                    	                                						                                          EXECUTION_NAMEField.name = "EXECUTION_NAME";     
                                   
                                   																						myform.action = url;
                                   																						myform.method = "post"
                                   																						myform.appendChild(TENANT_IDField);
                                   																						myform.appendChild(COMPANY_IDField);
                                   																						myform.appendChild(EXECUTION_NAMEField);
                                   																						document.body.appendChild(myform);
                                   																						myform.submit();
 	                    											   									   }
	                 	                    														    }
	                 																			);
	                 	                    												}		
	                 																	}
                 	                                								);
	                                                   							}
	                 	
        															</script>
																</kendo:grid-column-commandItem-click>
															</kendo:grid-column-commandItem>
              										
														<kendo:grid-column-commandItem
																className="fa fa-note btn btn-sm yellow margin-bottom"
																name="ShowNotes"
																text="${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Notes()}">
																<kendo:grid-column-commandItem-click>
																	<script>
						        										function ShowNotes(e)  {
                                   												e.preventDefault();
                                   												var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                 												var StrExecution=dataItem.execution_Name;
                                    											var url= "${pageContext.request.contextPath}/Notes_Lookup";
                                  												var myform = document.createElement("form");
                                   												var NotesLinkField = document.createElement("input");
                                   													  NotesLinkField.type = 'hidden';
                                   													  NotesLinkField.value = StrExecution;
                                   													  NotesLinkField.name = "NOTE_LINK";
                                   
                                   												var NotesIdField = document.createElement("input");
                                  													  NotesIdField.type = 'hidden';
                                   													  NotesIdField.value = 'TASKEXECUTION';
                                   													  NotesIdField.name = "NOTE_ID";
                                   
                                   												myform.action = url;
                                   												myform.appendChild(NotesLinkField);
                                   												myform.appendChild(NotesIdField);
                                   												document.body.appendChild(myform);
                                   												myform.submit();
       																 	} 
        															</script>
																</kendo:grid-column-commandItem-click>
														</kendo:grid-column-commandItem>
														
														
														</kendo:grid-column-command>
														
														
													</kendo:grid-column>
												</kendo:grid-columns>

												<kendo:dataSource pageSize="10">
													<kendo:dataSource-transport>
														<kendo:dataSource-transport-read cache="false"
															url="${pageContext.request.contextPath}/SmartTaskExecutions_Kendo_DisplayAll">
														</kendo:dataSource-transport-read>

														<kendo:dataSource-transport-parameterMap>
															<script>
	                											function parameterMap(options,type) {
	                													return JSON.stringify(options);
	                											}
                											</script>
														</kendo:dataSource-transport-parameterMap>

														</kendo:dataSource-transport>
												</kendo:dataSource>
											</kendo:grid>
											</div>
										</div>
									</div>
								</div>
								<!-- End: life time stats -->
							</div>
						</div>
						<div class="row"></div>
						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
			<!-- END PAGE CONTAINER -->
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>" 	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"	type="text/javascript"></script>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/SmartTaskConfigurator.css"/>"
	rel="stylesheet" />
<script>



function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);
    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
  

  jQuery(document).ready(function() {    
	  if("${JASCI}" != "${ObjCommonSession.getJasci_Tenant()}"){
		  var grid = $("#SmartTaskConfigurator").data("kendoGrid");
		  grid.hideColumn(0);
		  grid.hideColumn(1);
		}

	  $(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	 });
	
	 
	 
	 /*$("#SmartTaskConfigurator").kendoTooltip({
	       filter: "td:nth-child(5)", //this filter selects the first column cells
	       iframe:false,
		   width:250,
	       position: "bottom",
	       content: function(e){
	    	   
	    	
	        var dataItem = $("#SmartTaskConfigurator").data("kendoGrid").dataItem(e.target.closest("tr"));
	        var content = dataItem.notes;
	       if(content){
	         return content;
	        }
	        else{
	         e.preventDefault();
	   }
	       }
	     }).data("kendoTooltip");
	  */
	  
	setInterval(function () {
		
    var h = window.innerHeight;
    if(window.innerHeight>=900){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
 
});
  
function headerChangeLanguage(){
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {
				if (data1.boolStatus) {
					location.reload();
				}
			}
		});
 }

function headerInfoHelp(){
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'SmartTaskConfigurator',
	   		InfoHelpType:'PROGRAM'
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  			  });
	  	
}

    </script>
 <style>
   .portlet {
    margin-top: 0px;
    margin-bottom: 25px;
    padding: 0px;
        width: 101.5%;
	}
	
	.centercolumn {
		text-align:center;
	}
	
	
</style>