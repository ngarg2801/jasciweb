<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>

<tiles:insertDefinition name="subMenuAssigmentTemplate">

	<tiles:putAttribute name="body">
		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->

		<!-- BEGIN PAGE CONTAINER -->
		<div class="page-container">
			<!-- BEGIN PAGE HEAD -->
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_MaintenanceLabel()}</h1>
					</div>
					<!-- END PAGE TITLE -->
					<!-- BEGIN PAGE TOOLBAR -->

				</div>
			</div>
			<!-- END PAGE HEAD -->
			<!-- BEGIN PAGE CONTENT -->

			<div class="page-content hideHorizontalScrollbar">
				<div class="container">

					<!-- END PAGE BREADCRUMB -->
					<!-- BEGIN PAGE CONTENT INNER -->
					<div class="row margin-top-10">
						<div class="col-md-12">
							<form:form class="form-horizontal form-row-seperated"
								name="myForm" id="myForm" action="#"
								modelAttribute="COMPANY_REGISTER">

								<div class="portlet">
								
								<input type="hidden" name="getSetupdate" id="getSetupdate">
										<input type="hidden" name="getLastActivitydate" id="getLastActivitydate">
								 			
										
									<input type="hidden" name="iconAddress" value=""
										id="iconAddress">
										
									<input type="hidden" name="BillingControlNumber" value="${CompanyMaintenanceBillingControlNumberObj}"
										id="iconAddress">

									<div class="portlet-body">
										<div id="ErrorMessage"
											class="note note-danger display-none margin-left-14pix" style="display: none;">
											<p id="Perror" class="error error-Top margin-left-7pix">error</p>
										</div>
										<div class="tabbable">

											<div class="tab-content no-space">
												<div class="tab-pane active" id="tab_general">
													<div class="form-body">

														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_CompanyidLabel()}:<span
																class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="CompanyId" id="Company" maxlength="36"> <span
																	id="ErrorMessageCompany1" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_SetupSelectedWizardLabel()}:<!-- <span
																class="required">* </span> -->
															</label>

															 <div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="SetUpSelected" id="SetupSelectedWizard"
																	maxlength="10" readonly="true"> <span
																	id="ErrorMessageSetupSelectedWizard" class="error">
																</span>
															</div> 
															
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_SetupdateLabel()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="SetUpDate" id="SetupDate" readonly="true" >

															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_SetupbyLabel()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="SetupBy" id="SetupBy" readonly="true" value="${TeamMemberName}"> 
																	
																		

															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_LastactivitydateLabel()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="LastActivityDate" id="LastActivityDate" disabled="disabled" >
																	
																	<input type="hidden" class="form-controlwidth"
																	name="LastActivityDate" id="LastActivityDateH" >

															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_LastactivityteammemberLabel()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="LastActivityBy" id="LastActivityBy" readonly="true" value="${TeamMemberName}">

															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Name20Label()}:
																<span class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="Name20" id="NameShort" maxlength="20"> <span
																	id="ErrorMessageNameShort" class="error"> </span>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Name50Label()}:
																<span class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="Name50" id="NameLong" maxlength="50"> <span
																	id="ErrorMessageNameLong" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Addressline1Label()}:
																<span class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="AddressLine1" id="AddressLine1" maxlength="100">
																<span id="ErrorMessageAddressLine1" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Addressline2Label()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="AddressLine2" id="AddressLine2" maxlength="100">
																<span id="ErrorMessageAddressLine2" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Addressline3Label()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="AddressLine3" id="AddressLine3" maxlength="100">
																<span id="ErrorMessageAddressLine3" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Addressline4Label()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="AddressLine4" id="AddressLine4" maxlength="100">
																<span id="ErrorMessageAddressLine4" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_CityLabel()}:
																<span class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth" name="City"
																	id="City" maxlength="100"> <span
																	id="ErrorMessageCity" class="error"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_CountrycodeLabel()}:
																<span class="required"> * </span>
															</label>

															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="CountryCode" id="CountryCode">
																	<option value="none">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>
																	<c:forEach
																		items="${GeneralCodeLanguageCountryCodeListObject}"
																		var="ContyCodeList">
																		<option
																			value='${ContyCodeList.getGeneralCode()}'>${ContyCodeList.getDescription20()}</option>
																	</c:forEach>
																</select> <span id="ErrorMessageCountryCode" class="error-select">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_StatecodeLabel()}:<span
																class="required" id="requiredState"> * </span>
															</label>

															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="StateCode" id="State">
																	<option value="">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>
																	<c:forEach items="${GeneralCodeStatesListObject}"
																		var="StatesList">
																		<option value='${StatesList.getGeneralCode()}'>${StatesList.getDescription20()}</option>
																	</c:forEach>
																</select> <span id="ErrorMessageState" class="error-select"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ZipcodeLabel()}:<span
																class="required" id="requiredZipCode"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="ZipCode" id="ZipCode" maxlength="10"> <span
																	id="ErrorMessageZipCode" class="error long_errormsg"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Billtoname20Label()}:<span
																class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToName20" id="BillToName20" maxlength="20">
																<span id="ErrorMessageBillToName20" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Billtoname50Label()}:<span
																class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToName50" id="BillToName50" maxlength="50">
																<span id="ErrorMessageBillToName50" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Billtoaddressline1Label()}:<span
																class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToAddressLine1" id="BillToAddressLine1"
																	maxlength="100"> <span
																	id="ErrorMessageBillToAddressLine1" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Billtoaddressline2Label()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToAddressLine2" id="BillToAddressLine2"
																	maxlength="100"> <span
																	id="ErrorMessageBillToAddressLine2" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Billtoaddressline3Label()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToAddressLine3" id="BillToAddressLine3"
																	maxlength="100"> <span
																	id="ErrorMessageBillToAddressLine3" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Billtoaddressline4Label()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToAddressLine4" id="BillToAddressLine4"
																	maxlength="100"> <span
																	id="ErrorMessageBillToAddressLine4" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_BilltofromcityLabel()}:<span
																class="required"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToFromCity" id="BillToCity" maxlength="100">
																<span id="ErrorMessageBillToCity" class="error">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_BilltocountrycodeLabel()}:<span
																class="required"> * </span>
															</label>

															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="BillToCountryCode" id="BillToCountryCode">
																	<option value="none">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>
																	<c:forEach
																		items="${GeneralCodeLanguageCountryCodeListObject}"
																		var="ContyCodeList">
																		<option
																			value='${ContyCodeList.getGeneralCode()}'>${ContyCodeList.getDescription20()}</option>
																	</c:forEach>
																</select> <span id="ErrorMessageBillToCountryCode" class="error-select">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_BilltostatecodeLabel()}:<span
																class="required" id="requiredBillToState"> * </span>
															</label>

															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="BillToStateCode" id="BillToState">
																	<option value="">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>
																	<c:forEach items="${GeneralCodeStatesListObject}"
																		var="StatesList">
																		<option value='${StatesList.getGeneralCode()}'>${StatesList.getDescription20()}</option>
																	</c:forEach>
																</select> <span id="ErrorMessageBillToState" class="error-select">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_BilltozipcodeLabel()}:<span
																class="required" id="requiredBillToZipCode"> * </span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillToZipCode" id="BillToZipCode" maxlength="10">
																<span id="ErrorMessageBillToZipCode" class="error long_errormsg">
																</span>
															</div>
														</div>








														<!-- group1 -->

														<div class="repeatingSection" id="TextBoxesGroup">
															<div class="form-group" id="TextBoxDiv1">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactname1Label()}:<!-- <span
																	class="required"> * </span> -->
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactName1" id="ContactName" maxlength="100">
																	<span id="ErrorMessageContactName" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactphone1Label()}:<!-- <span
																	class="required"> * </span> -->
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactPhone1" id="ContactPhone" maxlength="10">
																	<span id="ErrorMessageContactPhone" class="error long_errormsg_both">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactextension1Label()}:<!-- <span
																	class="required"> * </span> -->
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactExtension1" id="ContactExtension"
																		maxlength="10"> <span
																		id="ErrorMessageContactExtension" class="error long_errormsg_both">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactcell1Label()}:<!-- <span
																	class="required"> * </span> -->
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactCell1" id="ContactCell" maxlength="10">
																	<span id="ErrorMessageContactCell" class="error long_errormsg_both">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactfax1Label()}:<!-- <span
																	class="required"> * </span> -->
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactFax1" id="ContactFax" maxlength="10">
																	<span id="ErrorMessageContactFax" class="error long_errormsg_both">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactemail1Label()}:<!-- <span
																	class="required"> * </span> -->
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactEmail1" id="ContactEmail" maxlength="100">
																	<span id="ErrorMessageContactEmail" class="error long_errormsg_both">
																	</span>
																</div>

																<div class="formRowRepeatingSectionForOne" id="add1">
																	<%-- <img
																		src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																		alt="logo" id='addButton1' style="cursor: pointer"
																		class="addFight" onClick="addFight(1);" /> --%>
																		
																		<button class="btn btn-sm green "	onClick="addFight(1); return false;"><i class="fa fa-plus"></i> ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contact()}</button>

																</div>
															</div>
														</div>




														<!-- group 2 -->





														<div class="repeatingSection" id="TextBoxesGroup2"
															hidden=true>
															<hr class="hrfulfillmentmaintenanceDivision" />
															<div class="form-group" id="TextBoxDiv1">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactname2Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactName2" id="ContactName2" maxlength="100">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactphone2Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactPhone2" id="ContactPhone2" maxlength="10"><span
																		id="ErrorMessageContactPhone2" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactextension2Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactExtension2" maxlength="10"
																		id="ContactExtension2"><span
																		id="ErrorMessageContactExtension2" class="error">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactcell2Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactCell2" id="ContactCell2" maxlength="10"><span
																		id="ErrorMessageContactCell2" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactfax2Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactFax2" id="ContactFax2" maxlength="10">
																				
																		<span	id="ErrorMessageContactFax2" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactemail2Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactEmail2" id="ContactEmail2"
																		maxlength="100"> <button class="btn btn-sm red filter-cancel fulfillmentMinusContact"
															onClick="deleteGroup(2);return false;">
															<i class="fa fa-minus"></i> ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contact()}
														</button>
																	<span
																		id="ErrorMessageContactEmail2" class="error"> </span>
																</div>

																<div class="formRowRepeatingSectionForOne" id="add2">
																	<%-- <img
																		src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																		alt="logo" id='addButton2' style="cursor: pointer"
																		class="addFight" onClick="addFight(2);" />
 --%>
 													    <button class="btn btn-sm green " onClick="addFight(2);return false;"><i class="fa fa-plus"></i>  ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contact()}</button>
														
																</div>
															</div>
														</div>







														<!-- group 3 -->





														<div class="repeatingSection" id="TextBoxesGroup3"
															hidden=true>
															<hr class="hrfulfillmentmaintenanceDivision" />
															<div class="form-group" id="TextBoxDiv1">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactname3Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactName3" id="ContactName3" maxlength="100">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactphone3Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactPhone3" id="ContactPhone3" maxlength="10"><span
																		id="ErrorMessageContactPhone3" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactextension3Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactExtension3" id="ContactExtension3"
																		maxlength="10"><span
																		id="ErrorMessageContactExtension3" class="error">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactcell3Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactCell3" id="ContactCell3" maxlength="10"><span
																		id="ErrorMessageContactCell3" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactfax3Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactFax3" id="ContactFax3" maxlength="10"><span
																		id="ErrorMessageContactFax3" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactemail3Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactEmail3" id="ContactEmail3"
																		maxlength="100"> 
																	<button class="btn btn-sm red filter-cancel fulfillmentMinusContact"
															onClick="deleteGroup(3);return false;">
															<i class="fa fa-minus"></i> ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contact()}
														</button><span
																		id="ErrorMessageContactEmail3" class="error"> </span>
																</div>

																<div class="formRowRepeatingSectionForOne" id="add3">
																	<%-- <img
																		src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																		alt="logo" id='addButton3' style="cursor: pointer"
																		class="addFight" onClick="addFight(3);" /> --%>
																		<button class="btn btn-sm green" onClick="addFight(3); return false;"><i class="fa fa-plus"></i> ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contact()}</button>
															          

																</div>
															</div>
														</div>





														<!-- group 4 -->





														<div class="repeatingSection" id="TextBoxesGroup4"
															hidden=true>
															<hr class="hrfulfillmentmaintenanceDivision" />
															<div class="form-group" id="TextBoxDiv1">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactname4Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactName4" id="ContactName4" maxlength="100">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactphone4Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactPhone4" id="ContactPhone4" maxlength="10"><span
																		id="ErrorMessageContactPhone4" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactextension4Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactExtension4" id="ContactExtension4"
																		maxlength="10"><span
																		id="ErrorMessageContactExtension4" class="error">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactcell4Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactCell4" id="ContactCell4" maxlength="10"><span
																		id="ErrorMessageContactCell4" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactfax4Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactFax4" id="ContactFax4" maxlength="10"><span
																		id="ErrorMessageContactFax4" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contactemail4Label()}:
																</label>

																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="ContactEmail4" id="ContactEmail4"
																		maxlength="100">
																		<button class="btn btn-sm red filter-cancel fulfillmentMinusContact"
															onClick="deleteGroup(4);return false;">
															<i class="fa fa-minus"></i> ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contact()}
														</button>
																		 <span
																		id="ErrorMessageContactEmail4" class="error"> </span>
																</div>

																<%-- <div class="formRowRepeatingSectionForOne" id="add4">
																	<img
																		src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																		alt="logo" id='addButton4' style="cursor: pointer"
																		class="addFight" onClick="addFight(4);" />
																		
																		<button class="btn btn-sm red "	onClick="deleteGroup(4);return false;">	<i class="fa fa-minus"></i> ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Contact()}	</button>


																</div> --%>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_MainfaxLabel()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="MainFax" id="MainFax" maxlength="10"><span id="ErrorMessageMainFax" class="error">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_MainemailLabel()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="MainEmail" id="MainEmail" maxlength="100"><span id="ErrorMessageMainEmail" class="error">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_BillingcontrolnumberLabel()}:
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="BillingControlNumber" id="BillingControlNumber" value="${NextBillingControlNumber}"
																	maxlength="10"><span id="ErrorMessageBillingControlNumber" class="error">
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ThemefulldisplayLabel()}:
																<span class="required"> * </span>
															</label>
															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="ThemeFullDisplay" id="ThemeFullDisplay">
																	<option value="">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>

																	<c:forEach
																		items="${GeneralCodeThemeFullDisplayListObject}"
																		var="ThemeFullDisplayList">
																		<option
																			value='${ThemeFullDisplayList.getGeneralCode()}'>${ThemeFullDisplayList.getDescription20()}</option>
																	</c:forEach>
																</select> <span id="ErrorMessageThemeFullDisplay" class="error-select">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ThememobileLabel()}:<span
																class="required"> * </span>
															</label>
															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="ThemeMobile" id="ThemeMobile">
																	<option value="">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>

																	<c:forEach items="${GeneralCodeThemeMobileListObject}"
																		var="ThemeMobileList">
																		<option
																			value='${ThemeMobileList.getGeneralCode()}'>${ThemeMobileList.getDescription20()}</option>
																	</c:forEach>
																</select><span id="ErrorMessageThemeMobile" class="error-select">
																</span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ThemerfLabel()}:
																<span class="required"> * </span>
															</label>
															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="ThemeRF" id="ThemeRF">
																	<option value="">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>

																	<c:forEach items="${GeneralCodeThemeRFListObject}"
																		var="ThemeRFList">
																		<option
																			value='${ThemeRFList.getGeneralCode()}'>${ThemeRFList.getDescription20()}</option>
																	</c:forEach>
																</select><span id="ErrorMessageThemeRF" class="error-select"> </span>
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_PurchaseorderreqapprovalLabel()}:
															</label>
															<div class="col-md-10">
																<select
																	class="table-group-action-input form-control input-medium"
																	name="PurchageOrdersRequireApproval"
																	id="PurchaseOrderRequireApproval">
																	<option value="">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...</option>
																	<option value="Y">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Yes()}</option>
																	<option value="N">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_No()}</option>



																</select>
															</div>
														</div>
														<div class="form-group" id="tm" >
															<label class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_TimezoneLabel()}:<span
										class="required">*</span>
															</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="TimeZone" id="TimeZone" maxlength="100">
																<span id="ErrorMessageTimeZone" class="error"> </span>
															</div>
														</div>
							</form:form>
							<div class="form-group">
								<form name="fileForm" id="fileForm" action="uploadimageCompany"
									target="uploadTrg" method="post" enctype="multipart/form-data">
									<input type="hidden" name="loadType"
										value="companyMaintenanceIconBucketName" /> <label
										class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_LogoLabel()}:
									</label>
									<%-- <span id="CompanyLogoFormatText">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_SupportedFormatsLabel()}</span> --%>
									<%-- <label
										class="col-md-2 control-label">${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_SupportedFormatsLabel()}:
									</label> --%>
									<div class="col-md-10">

										<input type="file" id="browse" style="display: none;"
											name="fileupload" onChange="Handlechange();" /> <input
											type="text" class="form-controlwidth"
											name="AppIconAddress" readonly="true" id="CompanyIconAddress"
											maxlength="500"> <input type="button"
											value="${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_BrowseText()}" id="fakeBrowse1"
											onclick="HandleBrowseClick();" /> <div id="fit"><span id="ErrorMessageAppIconAddress" class="error err"></span></div><input type="submit"
											id="btnSubmit" class="disp-none" value="Upload file" /> <img
											src="" alt="" onclick="SetId();" id="AppIconImage"
											style="display: none;" class="logocompany_screen">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="margin-bottom-5-right-allign_company_main_new margin-right-company-maintenence-btn">
			<button type="button" onclick="sendMyForm();"
				class="btn btn-sm yellow  margin-bottom">
				<i class="fa fa-check"></i> ${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_SaveUpdateText()}</button>
				<button class="btn btn-sm red " onclick="reloadPage();">
				<i class="fa fa-times"></i>${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ButtonResetText()}</button>
			<button class="btn btn-sm red " onclick="clearBtn();">
				<i class="fa fa-times"></i>${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ButtonCancelText()}</button>
		</div>

		</div>

		</div>

		<!-- END PAGE CONTENT INNER -->

		</div>


		</div>

		<!-- END PAGE CONTAINER -->
		<!-- BEGIN PRE-FOOTER -->
	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/jquery-1.8.2.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resourcesValidate/jquery.ajaxfileupload.js"/>"
	type="text/javascript"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<link
	href="<c:url value="/resourcesValidate/css/menu-app-icon-style.css"/>"
	rel="stylesheet" type="text/css">
	
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />
<script>
	$(function() {

		$('#CountryCode').change(function() {

			if ($(this).val() == '${CountryCodeObject}') {
				$("#State").removeAttr("disabled");
				$("#State").val($("#State option:first").val());
				$("#requiredState").show();
				$('#requiredZipCode').show();

			} else {
				$('#State').attr('disabled', 'disabled');
				$("#State").val($("#State option:first").val());
				$("#requiredState").hide();
				//$('#requiredZipCode').hide();
			}

		});

		$('#BillToCountryCode').change(function() {

			if ($(this).val() == '${CountryCodeObject}') {
				$("#BillToState").removeAttr("disabled");
				$("#BillToState").val($("#BillToState option:first").val());
				$("#requiredBillToState").show();
				$('#requiredBillToZipCode').show();

			} else {
				$('#BillToState').attr('disabled', 'disabled');
				$("#BillToState").val($("#BillToState option:first").val());
				$("#requiredBillToState").hide();
				$('#requiredBillToZipCode').show();
			}

		});

	});

	jQuery(document).ready(function() {
		 $('#SetupDate').val(getLocalDate());
		 $('#LastActivityDate').val(getLocalDate());
		 $('#LastActivityDateH').val(getUTCDateTime());
		 
		var valueBrowse='${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_BrowseText()}';
		$('#BillingControlNumber').attr('disabled', 'disabled');
		var iconimage=-36;		
		var pddingPixel=2;
		
		disableState();
		
		setInterval(function () {
			
		    var w = window.innerWidth;		    
		    var h = window.innerHeight;		
		    
			  
		    if(window.innerHeight>=900 || window.innerHeight==1004){
		    	pddingPixel=4;
		    	iconimage=-20;
		    }
		    else if(w==1024){
		    	pddingPixel=7;
		    	iconimage=-20;
		    }
		   else{
			   pddingPixel=2;
			   iconimage=-20;
		    }
		    var lengthPadding=valueBrowse.length+pddingPixel;
		    $('#CompanyIconAddress').css({"padding-left":lengthPadding+"%"});
		    
			}, 30);
		
			
		$("#tm").hide();
		$("#requiredState").hide();
		$('#requiredZipCode').show();
		$("#requiredBillToState").hide();
		$('#requiredBillToZipCode').show();
		$("#btnSubmit").hide();
		$("#browse").hide();
		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		$('#fileForm').ajaxForm({

			complete : function(data, textStatus, jqXHR) {

			},
			success : function(response) {
				branchDetailsSuccess(response);
			},

		});

		function branchDetailsSuccess(result) {

			var textinput = document.getElementById("CompanyIconAddress");
			var textaddress = document.getElementById("iconAddress");
			textinput.value = result;
			textaddress.value = result;
			isParent = false;
			 $('#AppIconImage').css({"margin-top":iconimage+"px"});
			jQuery("#AppIconImage").attr('src', result);
			
			$("#AppIconImage").show();
			$("#ErrorMessageAppIconAddress").hide();
		}

		/*  var form = document.getElementById("myForm");
		 form.onsubmit = function() {
		   return false;
		 } */

		/*  Metronic.init(); // init metronic core componets
		 Layout.init(); // init layout
		 Demo.init(); // init demo(theme settings page)
		 Index.init(); // init index page
		 Tasks.initDashboardWidget(); // init tash dashboard widget */
	});
</script>
<script>
	var isParent = false;

	function isformSubmit() {
		
		$('#ErrorMessage').hide();	

		if (isParent) {
			return false;
		} else {

		}

		var isSubmit = false;

		/* validation variable */
		var isNumberContactPhone = false;
		var isNumberContactExtension = false;
		var isNumberContactCell = false;
		var isNumberContactFax = false;
		var isValidContactEmail = false;

		var isNumberContactPhone2 = true;
		var isNumberContactExtension2 = true;
		var isNumberContactCell2 = true;
		var isNumberContactFax2 = true;
		var isValidContactEmail2 = true;

		var isNumberContactPhone3 = true;
		var isNumberContactExtension3 = true;
		var isNumberContactCell3 = true;
		var isNumberContactFax3 = true;
		var isValidContactEmail3 = true;

		var isNumberContactPhone4 = true;
		var isNumberContactExtension4 = true;
		var isNumberContactCell4 = true;
		var isNumberContactFax4 = true;
		var isValidContactEmail4 = true;
		
		var isValidMainEmail=true;
		var isNumericMainFax=true;
		var isNumericBillingCounterNumber=true;

		var isBlankCompany = isBlankField(
				'Company',
				'ErrorMessageCompany1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankSetupSelectedWizard =true; /* isBlankField(
				'SetupSelectedWizard',
				'ErrorMessageSetupSelectedWizard',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
 */
		var isBlankNameShort = isBlankField(
				'NameShort',
				'ErrorMessageNameShort',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankNameLong = isBlankField(
				'NameLong',
				'ErrorMessageNameLong',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankAddressLine1 = isBlankField(
				'AddressLine1',
				'ErrorMessageAddressLine1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankCity = isBlankField(
				'City',
				'ErrorMessageCity',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankBillToName20 = isBlankField(
				'BillToName20',
				'ErrorMessageBillToName20',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankBillToName50 = isBlankField(
				'BillToName50',
				'ErrorMessageBillToName50',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankBillToAddressLine1 = isBlankField(
				'BillToAddressLine1',
				'ErrorMessageBillToAddressLine1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankBillToCity = isBlankField(
				'BillToCity',
				'ErrorMessageBillToCity',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankContactName =true;/*  isBlankField(
				'ContactName',
				'ErrorMessageContactName',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}'); */

		var isBlankContactPhone = true;/* isBlankField(
				'ContactPhone',
				'ErrorMessageContactPhone',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
 */
		if (isBlankContactPhone) {

			isNumberContactPhone = isNumeric(
					'ContactPhone',
					'ErrorMessageContactPhone',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');
		}

		var isBlankContactExtension =true; /* isBlankField(
				'ContactExtension',
				'ErrorMessageContactExtension',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
 */
		if (isBlankContactExtension) {

			isNumberContactExtension = isNumeric(
					'ContactExtension',
					'ErrorMessageContactExtension',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');
		}

		var isBlankContactCell =true; /* isBlankField(
				'ContactCell',
				'ErrorMessageContactCell',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
 */
		if (isBlankContactCell) {

			isNumberContactCell = isNumeric(
					'ContactCell',
					'ErrorMessageContactCell',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');
		}

		var isBlankContactFax =true; /* isBlankField(
				'ContactFax',
				'ErrorMessageContactFax',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
 */
		if (isBlankContactFax) {

			isNumberContactFax = isNumeric(
					'ContactFax',
					'ErrorMessageContactFax',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');
		}

		var isBlankContactEmail =true; /* isBlankField(
				'ContactEmail',
				'ErrorMessageContactEmail',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS()}');
 */
		if (isBlankContactEmail) {
			isValidContactEmail = isValidEmailText(
					'ContactEmail',
					'ErrorMessageContactEmail',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS()}');
		}

		var isBlankThemeFullDisplay = isBlankField(
				'ThemeFullDisplay',
				'ErrorMessageThemeFullDisplay',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankThemeMobile = isBlankField(
				'ThemeMobile',
				'ErrorMessageThemeMobile',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankThemeRF = isBlankField(
				'ThemeRF',
				'ErrorMessageThemeRF',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

		var isBlankTimeZone =true; /* isBlankField(
				'TimeZone',
				'ErrorMessageTimeZone',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
 */
		var isBlankAppIconAddress=true; /* = isBlankField(
				'CompanyIconAddress',
				'ErrorMessageAppIconAddress',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		
		if(isBlankAppIconAddress){
		$("#ErrorMessageAppIconAddress").hide();
		}
		else{
			$("#ErrorMessageAppIconAddress").show();
			} */
		

		/* Group 2 validation */
		isNumberContactPhone2 = isNumeric(
				'ContactPhone2',
				'ErrorMessageContactPhone2',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactExtension2 = isNumeric(
				'ContactExtension2',
				'ErrorMessageContactExtension2',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactCell2 = isNumeric(
				'ContactCell2',
				'ErrorMessageContactCell2',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactFax2 = isNumeric(
				'ContactFax2',
				'ErrorMessageContactFax2',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isValidContactEmail2 = isValidEmailText(
				'ContactEmail2',
				'ErrorMessageContactEmail2',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS()}');

		/* Group 3 validation */

		isNumberContactPhone3 = isNumeric(
				'ContactPhone3',
				'ErrorMessageContactPhone3',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactExtension3 = isNumeric(
				'ContactExtension3',
				'ErrorMessageContactExtension3',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactCell3 = isNumeric(
				'ContactCell3',
				'ErrorMessageContactCell3',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactFax3 = isNumeric(
				'ContactFax3',
				'ErrorMessageContactFax3',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isValidContactEmail3 = isValidEmailText(
				'ContactEmail3',
				'ErrorMessageContactEmail3',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS()}');

		/* Group 4 validation */
		isNumberContactPhone4 = isNumeric(
				'ContactPhone4',
				'ErrorMessageContactPhone4',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactExtension4 = isNumeric(
				'ContactExtension4',
				'ErrorMessageContactExtension4',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactCell4 = isNumeric(
				'ContactCell4',
				'ErrorMessageContactCell4',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isNumberContactFax4 = isNumeric(
				'ContactFax4',
				'ErrorMessageContactFax4',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		isValidContactEmail4 = isValidEmailText(
				'ContactEmail4',
				'ErrorMessageContactEmail4',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS()}');
		
		
		
		 isNumericMainFax = isNumeric(
				'MainFax',
				'ErrorMessageMainFax',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');

		 isNumericBillingCounterNumber = isNumeric(
				'BillingControlNumber',
				'ErrorMessageBillingControlNumber',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER()}');
		
		
		isValidMainEmail = isValidEmailText(
				'MainEmail',
				'ErrorMessageMainEmail',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS()}');
		
		

		var isBlankBillToZipCode = isBillToZipCodeValid();
		var isBlankZipCode = isZipCodeValid();
		var isBlankCountryCode = isCountrySelected();
		var isBlankBillToCountryCode = isBillToCountrySelected();
		var isBlankBillToState = isBillToStateValid();
		var isBlankState = isStateValid();

		/*comman class validation*/

		/* start condition 1*/
		   if (isBlankCompany && isBlankSetupSelectedWizard && isBlankNameShort
				&& isBlankNameLong && isBlankAddressLine1 && isBlankCity
				&& isBlankBillToName20 && isBlankBillToName50
				&& isBlankBillToAddressLine1 && isBlankBillToCity
				&& isBlankContactName && isBlankContactPhone
				&& isBlankContactExtension && isBlankContactCell
				&& isBlankContactFax && isBlankContactEmail
				&& isBlankThemeFullDisplay && isBlankThemeMobile
				&& isBlankThemeRF && isBlankTimeZone && isBlankAppIconAddress
				&& isNumberContactPhone && isNumberContactExtension
				&& isNumberContactCell && isNumberContactFax
				&& isValidContactEmail && isNumberContactPhone2
				&& isNumberContactExtension2 && isNumberContactCell2
				&& isNumberContactFax2 && isValidContactEmail2
				&& isNumberContactPhone3 && isNumberContactExtension3
				&& isNumberContactCell3 && isNumberContactFax3
				&& isValidContactEmail3 && isNumberContactPhone4
				&& isNumberContactExtension4 && isNumberContactCell4
				&& isNumberContactFax4 && isValidContactEmail4
				&& isBlankBillToZipCode && isBlankZipCode && isBlankCountryCode
				&& isBlankBillToCountryCode && isBlankBillToState
				&& isBlankState && isValidMainEmail && isNumericMainFax && isNumericBillingCounterNumber) { 
			
			/*start condition 2  block 1*/

			var varCompanyid = $('#Company').val();
			var status = false;
			
			$
					.post(
							"${pageContext.request.contextPath}/RestGetCompanyByID",
							{
								CompanyID : varCompanyid

							},
							function(data, status) {

								if (data.length >= 1) {
							         
							         var valueofstatus=data[0];
							         var errMsgId = document.getElementById("Perror");
							         if(valueofstatus=='A'){
							         errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_COMPANY_ALREADY_USED()}';
							         }else{
							          errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status()}';
							         }
							         $('#ErrorMessage').show();
							          isSubmit=false; 
							        } else {
									
									//block 2 check valid zip code  

									var zipcode1 = $('#ZipCode').val();
								
									//http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
									$
											.post(
													"${pageContext.request.contextPath}/RestCheckZipCode",
													{
														TeamMemberZipCode : zipcode1
													},
													function(data, status) {

														if (data) {
															var errMsgId = document.getElementById("Perror");
															errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_ZIP_CODE()}';
															$('#ErrorMessage').show();
															 isSubmit=false;	

														}

														else {
															
															
															//block 3 check bill to zip code
															
															$('#ErrorMessage').hide();	
															var zipcode1 = $('#BillToZipCode').val();
															
															//http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
															$
																	.post(
																			"${pageContext.request.contextPath}/RestCheckZipCode",
																			{
																				TeamMemberZipCode : zipcode1
																			},
																			function(data, status) {

																				if (data) {
																					var errMsgId = document.getElementById("Perror");
																					errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE()}';
																					$('#ErrorMessage').show();
																					 isSubmit=false;	

																				}

																				else {
																					$('#ErrorMessage').hide();	
																					
																					//block 4 check address validation
																					
																				
																					var zipcode = $('#ZipCode').val();
																					var address1 = $('#AddressLine1').val();
																					var address2 = $('#AddressLine2').val();
																					var address3 = $('#AddressLine3').val();
																					var address4 = $('#AddressLine4').val();
																					var city = $('#City').val();
																					var Countrycode = $("#CountryCode option:selected").text();
																					var Statecode = $("#State option:selected").text();
																					if (Statecode == '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...') {
																						Statecode = "";
																					}
																					var ZipCode = $('#ZipCode').val();

																					//http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
																					$
																							.post(
																									"${pageContext.request.contextPath}/RestCheckAddress",
																									{
																										TeamMemberAddress1 : address1,
																										TeamMemberAddress2 : address2,
																										TeamMemberAddress3 : address3,
																										TeamMemberAddress4 : address4,
																										TeamMemberCity : city,
																										TeamMemberStateCode : Statecode,
																										TeamMemberCountryCode : Countrycode,
																										TeamMemberZipCode : ZipCode
																									},
																									function(data, status) {

																										if (data) {
																											var errMsgId = document.getElementById("Perror");
																											errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_ADDRESS()}';
																											$('#ErrorMessage').show();
																											 isSubmit=false;	
																											/*  alert('In valid addres'); */
																										} else {
																											
																											
																											//block 5 bill to address validation
																											
																										$('#ErrorMessage').hide();	
																											
																											var zipcode = $('#BillToZipCode').val();
																											var address1 = $('#BillToAddressLine1').val();
																											var address2 = $('#BillToAddressLine2').val();
																											var address3 = $('#BillToAddressLine3').val();
																											var address4 = $('#BillToAddressLine4').val();
																											var city = $('#BillToCity').val();
																											var Countrycode = $("#BillToCountryCode option:selected").text();
																											var Statecode = $("#BillToState option:selected").text();
																											if (Statecode == '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Select()}...') {
																												Statecode = "";
																											}
																											var ZipCode = $('#BillToZipCode').val();
																									
																											//http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
																											$
																													.post(
																															"${pageContext.request.contextPath}/RestCheckAddress",
																															{
																																TeamMemberAddress1 : address1,
																																TeamMemberAddress2 : address2,
																																TeamMemberAddress3 : address3,
																																TeamMemberAddress4 : address4,
																																TeamMemberCity : city,
																																TeamMemberStateCode : Statecode,
																																TeamMemberCountryCode : Countrycode,
																																TeamMemberZipCode : ZipCode
																															},
																															function(data, status) {
																									
																																if (data) {
																																	var errMsgId = document.getElementById("Perror");
																																	errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'	+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_BILL_TO_ADDRESS()}';
																																	$('#ErrorMessage').show();
																																	isSubmit = false;
																																	/* alert('in valid bill to addres'); */
																																} else {
																																	
																																	$('#ErrorMessage').hide();	
																																	var errMsgId = document.getElementById("Perror");
																																	 var email=$('#ContactEmail').val();
																																	 $.post( "${pageContext.request.contextPath}/RestCheckCompanyUniqueEmail", {Email:email },
																																			 function( data,status) {
																																		 		 if(data)	 		{
																																		     	errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED()}';
																															        	 		$('#ErrorMessage').show();
																															        	 		isSubmit=false;
																															        	}
																																		 	else{
																																		 		 $('#getSetupdate').val(getUTCDateTime());
																																				 $('#getLastActivitydate').val(getUTCDateTime());
																																		 		$('#ErrorMessage').hide();	
																																				//alert('${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ON_SAVE_SUCCESSFULLY()}'); 
																																				 bootbox.alert('${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ON_SAVE_SUCCESSFULLY()}',
																																			function() {
																																			isSubmit = true;
																																			//document.location.href = '#top';
																																			window.scrollTo(0,0);
																																	        $('#myForm').attr('action','${pageContext.request.contextPath}/Comapany_addorupdate');
																																	        $('#myForm').submit();  
																																		 	});}
																																	
																																}); 
																																}
																															});//end block 5 bill to address validation
																																																				
																													
																											
																										}
																									});//end block 4 address validation
									
																				}

																			});//end block 3 check valid bill to zip code	
															

														}

													});//end block 2 check valid zip code								

								}
							});//end check company id block1		

			/*end  condition 2*/

		}/*end condition 1*/
		else {
			isSubmit = true;
		}/*end else condition 1*/

		//document.location.href = '#top';
		window.scrollTo(0,0);
		return isSubmit;

	}

	function clearBtn() {
		//	url="${pageContext.request.contextPath}/GeneralCodeSubListID/Grid/readSublists"
		window.location.href = '${pageContext.request.contextPath}/Company_maintenance_new';
	}

	function deleteGroup(ButtonClick) {

		if (ButtonClick == 4) {
			$('#add3').show();
			$('#TextBoxesGroup4').hide();
			var elem1 = document.getElementById("ContactName4");
			elem1.value = "";
			var elem2 = document.getElementById("ContactPhone4");
			elem2.value = "";
			var elem3 = document.getElementById("ContactExtension4");
			elem3.value = "";
			var elem4 = document.getElementById("ContactCell4");
			elem4.value = "";
			var elem5 = document.getElementById("ContactFax4");
			elem5.value = "";
			var elem6 = document.getElementById("ContactEmail4");
			elem6.value = "";

		} else if (ButtonClick == 3) {
			$('#add2').show();
			$('#TextBoxesGroup3').hide();

			var elem1 = document.getElementById("ContactName3");
			elem1.value = "";
			var elem2 = document.getElementById("ContactPhone3");
			elem2.value = "";
			var elem3 = document.getElementById("ContactExtension3");
			elem3.value = "";
			var elem4 = document.getElementById("ContactCell3");
			elem4.value = "";
			var elem5 = document.getElementById("ContactFax3");
			elem5.value = "";
			var elem6 = document.getElementById("ContactEmail3");
			elem6.value = "";

		} else if (ButtonClick == 2) {
			$('#add1').show();
			$('#TextBoxesGroup2').hide();
			var elem1 = document.getElementById("ContactName2");
			elem1.value = "";
			var elem2 = document.getElementById("ContactPhone2");
			elem2.value = "";
			var elem3 = document.getElementById("ContactExtension2");
			elem3.value = "";
			var elem4 = document.getElementById("ContactCell2");
			elem4.value = "";
			var elem5 = document.getElementById("ContactFax2");
			elem5.value = "";
			var elem6 = document.getElementById("ContactEmail2");
			elem6.value = "";

		}
	}

	function addFight(ButtonClick) {

		if (ButtonClick == 1) {
			$('#add1').hide();
			//$('#add2').show();
			$('#TextBoxesGroup2').show();

		} else if (ButtonClick == 2) {
			$('#add2').hide();
			//$('#add3').show();
			$('#TextBoxesGroup3').show();

		} else if (ButtonClick == 3) {
			$('#add3').hide();
			$('#TextBoxesGroup4').show();

		}

		//return isSubmit;
	}

	function HandleBrowseClick() {
		var fileinput = document.getElementById("browse");

		fileinput.click();

	}

	/* browse for image file*/
	function Handlechange() {
		var fileinput = document.getElementById("browse");

		var status = load_image('browse', fileinput.value);

		if (status) {
			var textinput = document.getElementById("CompanyIconAddress");
			// var data = fileinput.value;
			//var arr = data.split('/');
			//textinput.value = arr[1];
			isParent = true;
			textinput.value = '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_Text_Loading()}';
			$("#btnSubmit").trigger('click');

		}
	}

	function load_image(id, ext) {
		var errMsgId = document.getElementById("ErrorMessageAppIconAddress");
		if (validateExtension(ext) == false) {
			// alert("Upload only JPEG or JPG format ");
			// document.getElementById("imagePreview").innerHTML = "";
			var errMsgId = document.getElementById("ErrorMessageAppIconAddress");														
			errMsgId.innerHTML ='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_INVALID_SUPPORTED_FORMATS()}';
			
			/* $('#fakeBrowse1').css({"margin-left":"-92%"}); */
			document.getElementById("browse").focus();
			
			var textinput = document.getElementById("CompanyIconAddress");
			textinput.value = '';
		
			$("#AppIconImage").hide();
			$("#ErrorMessageAppIconAddress").show();
			return false;
		} else {
			errMsgId.innerHTML='';
			/* $('#fakeBrowse1').css({"margin-left":"-50%"}); */
			return true;
		}
	}

	/* function check only images  file allowed*/
	function validateExtension(v) {
		var allowedExtensions = new Array("jpg", "JPG", "jpeg", "JPEG", "png",
				"PNG", "GIF", "gif");
		for (var ct = 0; ct < allowedExtensions.length; ct++) {
			sample = v.lastIndexOf(allowedExtensions[ct]);
			if (sample != -1) {
				return true;
			}
		}
		return false;
	}

	/* check validation for zip code*/
	function isZipCodeValid() {
		var ContryCode = $("#CountryCode :selected").val();
		if (ContryCode == '${CountryCodeObject}') {

			
			var isBlankZip = isBlankField(
					'ZipCode',
					'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if(isBlankZip){
			var isNumberZip = isNumeric(
					'ZipCode',
					'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if (isNumberZip) {
				//$('#ErrorMessageZipCode').hide();
				
				isBlankStateCode('ZipCode', 'ErrorMessageZipCode',
				           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				             + '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
			
				//$('#requiredZipCode').hide();
				return true;
			} else {
				$('#ErrorMessageZipCode').show();
				$('#requiredZipCode').show();
				return false;
			}
			
			}else {
				$('#ErrorMessageZipCode').show();
				$('#requiredZipCode').show();
				return false;
			}

		} else {
			/* $('#ErrorMessageZipCode').hide();
			$('#requiredZipCode').hide();
			return true; */
			
			var isBlankZip = isBlankField(
					'ZipCode',
					'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if(isBlankZip){
			var isNumberZip = isNumeric(
					'ZipCode',
					'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if (isNumberZip) {
				//$('#ErrorMessageZipCode').hide();
				
				isBlankStateCode('ZipCode', 'ErrorMessageZipCode',
				           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				             + '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
			
				//$('#requiredZipCode').hide();
				return true;
			} else {
				$('#ErrorMessageZipCode').show();
				$('#requiredZipCode').show();
				return false;
			}
			
			}else {
				$('#ErrorMessageZipCode').show();
				$('#requiredZipCode').show();
				return false;
			}
		}

	}

	/* check validation for Bill to zip code*/
	/* function isBillToZipCodeValid() {
		var ContryCode = $("#BillToCountryCode :selected").val();
		if (ContryCode == '${CountryCodeObject}') {

			var isBlankZip = isBlankField(
					'ZipCode',
					'ErrorMessageBillToZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if(isBlankZip){
			
			
			var isNumericZip = isNumeric(
					'BillToZipCode',
					'ErrorMessageBillToZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if (isNumericZip) {
				isBlankStateCode('ZipCode', 'ErrorMessageBillToZipCode',
				           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				             + '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				//$('#ErrorMessageBillToZipCode').hide();
				//$('#requiredBillToZipCode').hide();
				return true;
			} else {
				$('#ErrorMessageBillToZipCode').show();
				$('#requiredBillToZipCode').show();
				return false;
			}
	}else {
		$('#ErrorMessageBillToZipCode').show();
		$('#requiredBillToZipCode').show();
		return false;
	}

		} else {
			/* $('#ErrorMessageBillToZipCode').hide();
			$('#idRequiredBillToZipCode').hide();
			return true; */
			
		/* 	var isBlankZip = isBlankField(
					'ZipCode',
					'ErrorMessageBillToZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if(isBlankZip){
			
			
			var isNumericZip = isNumeric(
					'BillToZipCode',
					'ErrorMessageBillToZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if (isNumericZip) {
				isBlankStateCode('ZipCode', 'ErrorMessageBillToZipCode',
				           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
				             + '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				//$('#ErrorMessageBillToZipCode').hide();
				//$('#requiredBillToZipCode').hide();
				return true;
			} else {
				$('#ErrorMessageBillToZipCode').show();
				$('#requiredBillToZipCode').show();
				return false;
			}
	}else {
		$('#ErrorMessageBillToZipCode').show();
		$('#requiredBillToZipCode').show();
		return false;
	}
	}

		

	} */
	/* check validation for Bill to zip code*/
	function isBillToZipCodeValid() {
		var ContryCode = $("#BillToCountryCode :selected").val();
		if (ContryCode == '${CountryCodeObject}') {

			var isBlankZip = isBlankField(
					'BillToZipCode',
					'ErrorMessageBillToZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if (isBlankZip) {
				var isNumberZip = isNumeric(
						'BillToZipCode',
						'ErrorMessageBillToZipCode',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

				if (isNumberZip) {
					//$('#ErrorMessageBillToZipCode').hide();
					isBlankStateCode(
							'ZipCode',
							'ErrorMessageBillToZipCode',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

					//$('#requiredBillToZipCode').hide();
					return true;
				} else {
					$('#ErrorMessageBillToZipCode').show();
					$('#requiredBillToZipCode').show();
					return false;
				}

			} else {
				$('#ErrorMessageBillToZipCode').show();
				$('#requiredBillToZipCode').show();
				return false;
			}

		} else {
			/* $('#ErrorMessageBillToZipCode').hide();
			$('#idRequiredBillToZipCode').hide();
			return true; */
			var isBlankZip = isBlankField(
					'BillToZipCode',
					'ErrorMessageBillToZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

			if (isBlankZip) {
				var isNumberZip = isNumeric(
						'BillToZipCode',
						'ErrorMessageBillToZipCode',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

				if (isNumberZip) {
					//$('#ErrorMessageBillToZipCode').hide();
					isBlankStateCode(
							'ZipCode',
							'ErrorMessageBillToZipCode',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

					//$('#requiredBillToZipCode').hide();
					return true;
				} else {
					$('#ErrorMessageBillToZipCode').show();
					$('#requiredBillToZipCode').show();
					return false;
				}

			} else {
				$('#ErrorMessageBillToZipCode').show();
				$('#requiredBillToZipCode').show();
				return false;
			}
		}

	}

	/*check country seletion */
	function isCountrySelected() {

		var errMsgId = document.getElementById('ErrorMessageCountryCode');
		var ContryCode = $("#CountryCode :selected").val();

		if (ContryCode == 'none') {

			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}';
			return false;
		}

		else {
			errMsgId.innerHTML = '';
			return true;
		}

	}

	/*check bill to country code  seletion */
	function isBillToCountrySelected() {

		var errMsgId = document.getElementById('ErrorMessageBillToCountryCode');
		var ContryCode = $("#BillToCountryCode :selected").val();

		if (ContryCode == 'none') {

			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}';
			return false;
		}

		else {
			errMsgId.innerHTML = '';
			return true;
		}

	}

	/* check validation for Bill to State*/
	function isBillToStateValid() {
		var ContryCode = $("#BillToCountryCode :selected").val();
		if (ContryCode == '${CountryCodeObject}') {

			var isBlankZip = isBlankField(
					'BillToState',
					'ErrorMessageBillToState',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

			if (isBlankZip) {
				isBlankStateCode(
						'ZipCode',
						'ErrorMessageBillToState',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

				//$('#ErrorMessageBillToState').hide();
				$('#requiredBillToState').hide();
				return true;
			} else {
				$('#ErrorMessageBillToState').show();
				$('#requiredBillToState').show();
				return false;
			}

		} else {
			isBlankStateCode(
					'ZipCode',
					'ErrorMessageBillToState',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

			//$('#ErrorMessageBillToState').hide();
			$('#requiredBillToState').hide();
			return true;
		}

	}

	/* check validation for State*/
	function isStateValid() {
		var ContryCode = $("#CountryCode :selected").val();
		if (ContryCode == '${CountryCodeObject}') {

			var isBlankZip = isBlankField(
					'State',
					'ErrorMessageState',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

			if (isBlankZip) {
				//$('#ErrorMessageState').hide();
				isBlankStateCode(
						'ZipCode',
						'ErrorMessageState',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

				$('#requiredState').hide();
				return true;
			} else {
				$('#ErrorMessageState').show();
				$('#requiredState').show();
				return false;
			}

		} else {
			isBlankStateCode(
					'ZipCode',
					'ErrorMessageState',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${CompanyMaintenanceSceernLabelObj.getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');

			//$('#ErrorMessageState').hide();
			$('#requiredState').hide();
			return true;
		}

	}

	function sendMyForm() {
		isformSubmit();
	}
	
	
	function reloadPage(){
	/* location.reload(); */
		window.parent.location = window.parent.location.href;
	}
	function clearBtn(){
		 // url="${pageContext.request.contextPath}/GeneralCodeSubListID/Grid/readSublists"
		  //window.location.href='Team_member_maintenancenew';
		 // window.location.href = 'Team_member_maintenance_lookup';
		  //window.history.back();
		 // window.location.href='${pageContext.request.contextPath}/Company_maintenance_new';
		  var backState = '${backStatus}';
		     
		  
		   if (backState == 'lookup') {

		    
		    window.location.href = '${pageContext.request.contextPath}/'+'Company_lookup';
		   
		  }
		  else{
		   
			  window.location.href = '${pageContext.request.contextPath}/'+'Company_search_lookup';
		  } 
		 }
	
	function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}

	function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'Company',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						//  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
	function disableState(){
		$('#State').attr('disabled', 'disabled');
		$("#State").val($("#State option:first").val());
		$("#requiredState").hide();
		$('#requiredZipCode').show()
		
		$('#BillToState').attr('disabled', 'disabled');
		$("#BillToState").val($("#BillToState option:first").val());
		$("#requiredBillToState").hide();
		$('#requiredBillToZipCode').show();
	}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>