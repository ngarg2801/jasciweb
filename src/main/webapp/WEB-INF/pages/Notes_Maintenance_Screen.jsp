<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>

<tiles:insertDefinition name="subMenuAssigmentTemplate">

	<tiles:putAttribute name="body">
	
	
	<style>
	
	textarea.form-controlwidth {
  height: auto;
  padding-left: 7px;
  height: 204px;
  max-width: 368px;
}
	
	</style>
	
	<link href="<c:url value="/resourcesValidate/css/MenuProfileAssigment.css"/>" rel="stylesheet" type="text/css">
		<!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014?World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
		<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
		<!--[if !IE]><!-->
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${viewLabels.getLbl_NotesMaintenance_title()}</h1>
					</div>
				</div>
				
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#"></a><i class="fa fa-circle"></i></li>
							<li class="active"></li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
						<div id="ErrorMessage" class="note note-danger" style="display:none; width:657px ; ">
     <p id="Perror" class="error" style="margin-left: -7px !important;"></p>	
    </div>
							<form:form class="form-horizontal form-row-seperated"
							onsubmit="return isformSubmit();"
							action="#"
							commandName="InsertRow"
							name="myForm"
							id="myForm"
							method="get"
							modelAttribute="SAVEORUPDATE_NOTE">
										
										 <input type="hidden" name="NOTE_ID" value="${objnoteFields.getNote_Id()}" id="NOTE_ID" >						
										<input type="hidden" name="NOTE_LINK" value="${objnoteFields.getNote_Link()}" id="NOTE_LINK" >
								        <input type="hidden" name="LastActivitydate" id="getLastActivitydate">
										<input type="hidden" name="LastActivityBy" value="${objnoteFields.getLast_Activity_Team_Member()}" id="LastActivityby">
										<input type="hidden" name="ID" value="${objnoteFields.getAUTO_ID()}" id="ID">
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
											<div class="form-group">
													<label class="col-md-2 control-label">${viewLabels.getLbl_NoteID_NotesMaintenance()}:
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="Note_Id" value="${objnoteFields.getNote_Id()}" disabled="disabled"/>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label"> ${viewLabels.getLbl_NoteLink_NotesMaintenance()}:
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" name="Note_Link" value="${objnoteFields.getNote_Link()}" disabled="disabled"/>
													</div>
												</div>
												
												
												
											<div class="form-group">
													<label class="col-md-2 control-label"> ${viewLabels.getLbl_Notes_Date_YY_MM_DD_NotesMaintenance()}:
													</label>
													<div class="col-md-10">													
														<%-- <input type="text" class="form-controlwidth" name="Last_Activity_Date" value="${objnoteFields.getNotes_Date()}" readonly="true"/> --%>
														<input type="text" class="form-controlwidth" name="Last_Activity_Date" value="${objnoteFields.getLast_Activity_Date()}" disabled="disabled"/>
													</div>
												</div>
												
												
												
												<div class="form-group">
													<label class="col-md-2 control-label"> ${viewLabels.getLbl_Last_Activity_By_NotesMaintenance()}:
													</label>
													<div class="col-md-10">													
														<%-- <input type="text" class="form-controlwidth" name="Last_Activity_Team_Member" value="${objnoteFields.getNotes_Team_Member()}" readonly="true"/> --%>
														<input type="text" class="form-controlwidth" name="Last_Activity_Team_Member" value="${objnoteFields.getLast_Activity_Team_Member()}" disabled="disabled"/>
													</div>
												</div>			
												
												
													
												<div class="form-group">
													<label class="col-md-2 control-label"> ${viewLabels.getLbl_Note_NotesMaintenance()}:
													<span class="required"> * </span>
													</label>
													<div class="col-md-10">													
														
														<textarea maxlength="500" class="form-controlwidth" id="Note" name="Note">${objnoteFields.getNote()}</textarea>
														<span id="NotesErrorMessage" class="error-Note"></span>
													</div>
												</div>											
												
												<div class="repeatingSection" id="TextBoxesGroup2"
															hidden=true>
															
															<div class="form-group" id="TextBoxDiv1">
																<label class="col-md-2 control-label">${viewLabels.getLbl_CONTINUATIONS_SEQUENCE()}:
																</label>

																<div class="col-md-10">
																	<textarea  class="form-controlwidth" name="NotesExtra" id="NotesExtra" >${objnoteFields.getNotesExtra()}</textarea>
																	<button
																		id="deleteID" class="btn btn-sm red filter-cancel fulfillmentMinusContact" type="button"
															onClick="deleteGroup();return false;">	<i class="fa fa-minus"></i></button>
																</div>
															</div>
															
															
															
															
										
														</div>

												
<%-- 												<div class="form-group">
												
												
													<label class="col-md-2 control-label">${ViewLabels.getLanguageTranslation_Language()} :<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">
														<select class="table-group-action-input form-control input-medium" id="Language" name="Language">
															<option value="">Select...</option>
															 <c:forEach items="${LanguageList}" var="LanguageItem">
                                                                       <option value="${LanguageItem.getLanguage()}">${LanguageItem.getDescription20()}</option>
																	</c:forEach>
															
														</select><span id="ErrorMessageLanguage" class="error" ></span>
											       </div>														
																							
												</div> --%>
<%-- 												<div class="form-group">
													<label class="col-md-2 control-label"> ${ViewLabels.getLanguageTranslation_KeyPhrase()} :<span class="required">
													* </span>
													
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" id="KeyPhrase" name="KeyPhrase" >
														<span id="ErrorMessageKeyPhrase" class="error" ></span>
														</div>
																							
												</div> --%>

<%-- 												<div class="form-group">
													<label class="col-md-2 control-label">${ViewLabels.getLanguageTranslation_Translation()} :<span class="required" maxlength="500">
													* </span>
													
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" id="Translation" name="Translation" maxlength="500">
														<span id="ErrorMessageTranslation" class="error" ></span>
													</div>														
													
												</div> --%>	
												
												<div class="formAddTextArea" id="add1">
																	<%-- <img
																		src="<c:url value="/resources/assets/admin/layout3/img/Add_button.png"/>"
																		alt="logo" id='addButton1' style="cursor: pointer"
																		class="addFight" onClick="addFight(1);" /> --%>
																		
																		<button class="btn btn-sm green "	onClick="addFight(); return false;"><i class="fa fa-plus"></i></button>

																</div>
												
																	<div class="notes_update_margin-bottom-5_language_update">
											<button class="btn btn-sm yellow  margin-bottom" id="SaveBtn"><i class="fa fa-check"></i>&nbsp;${viewLabels.getLbl_Save_Update_NotesMaintenance()}</button>
										    <button class="btn btn-sm red " type="reset" onclick=" PageReload();"><i class="fa fa-times"></i>${viewLabels.getLbl_Reset_NotesMaintenance()}</button>
										    <button class="btn btn-sm red " type="Button" onclick=" PageReset();"><i class="fa fa-times"></i>${viewLabels.getLbl_Cancel_NotesMaintenance()}</button>
										    </div>		
												
												
												
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
					</form:form>
							</div>
						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		
		<!-- END PAGE CONTAINER -->
		<!-- BEGIN PRE-FOOTER -->

	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
	type="text/javascript"></script>
<script>

function deleteGroup(){
	//document.getElementById('TextBoxesGroup2').value = ''
	$('#NotesExtra').val("");
	$('#TextBoxesGroup2').hide();
	
	$('#add1').show();
	$('#deleteID').hide();
}

function ContinuesSequence(){
	
	var continuesSequence=$('#NotesExtra').val().trim().length;
	
	if(continuesSequence>0){
		$('#TextBoxesGroup2').show();
		
		$('#deleteID').show();
		$('#add1').hide();
	}
}

function addFight() {

	//$('#NotesErrorMessage').hide();
		
		$('#TextBoxesGroup2').show();
		$('#NotesExtra').val("");
		$('#add1').hide();
		$('#deleteID').show();
	
}

function PageReload()
{
		window.parent.location = window.parent.location.href;
}
function PageReset()
{
	 window.history.back();
		//window.location.href='Notes_Lookup';	
}
jQuery(document).ready(function() { 
	
	
	
	var message='${Saved}';
	if(message=='Saved')
		{
		bootbox.alert('${ViewLabels.getLanguageTranslation_SaveMessage()}',function(){
			window.location.href="Language_Translations_Lookup";
		});
		
		}
	
	ContinuesSequence();
	
	
	setInterval(function () {
		
	    var h = window.innerHeight;		    
	    if(window.innerHeight>=900){
	    	h=h-187;
	    	
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";		     
	    
		}, 30);
	
	
  /*  Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo(theme settings page)
   Index.init(); // init index page
   Tasks.initDashboardWidget(); // init tash dashboard widget */
});


</script>
<script>
	function isformSubmit() {

		
		 var isSubmit = false;
			
			var isBlankNote = isBlankField('Note',
					'NotesErrorMessage', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${viewLabels.getLbl_note_blank_field_error_msg()}');
		
		if(isBlankNote)
			{
			
			$('#getLastActivitydate').val(getUTCDateTime());
		//	alert('${viewLabels.getLbl_note_update_msg()}');
			bootbox.alert('${viewLabels.getLbl_note_update_msg()}',function(){
				isSubmit=true;
			document.myForm.action = '${pageContext.request.contextPath}/Notes_Maintenance_Screen_saveOrupdate';
			document.myForm.submit();
			});
			}
		else
			{
			isSubmit=false;
			}
		
		return isSubmit;
		
	}
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'Notes',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }	
		
</script>		
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>