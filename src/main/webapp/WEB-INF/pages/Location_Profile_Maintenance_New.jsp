<!--Company : NGI VENTURES PVT. LTD.
Created By: Diksha  Gupta
Created on:  Dec 24 2014
Purpuse : using for save new record for Location Profile.-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date;" %>
<!DOCTYPE html>




<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<style>
.error {
	color: #ff0000;
}
 
.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
#AddButtonCss
{
padding: 0;
margin-bottom: 9px;
min-height: 41px;
width: 100%;
float: left;
}
</style>

	<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div class="body">
	<div class="page-head">
   <div class="container">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
   <h1>${LocationsProfileLabels.getLocationProfiles_MaintenanceLabel()}</h1>
    </div>
    </div>
	<div class="page-content" id="page-content">
		<div class="container">
			<!-- BEGIN PAGE BREADCRUMB -->
			
			<!-- <div id="ErrorMessage" style="display: none;">
			<i style="color:#C26542"class="fa-lg fa fa-warning"></i>
     <p style="margin-left: 24px;margin-top: -17px;color:#C26542;">Mandatory fields cannot be left blank.</p>
    </div> -->
   <%--  <span id="ConstraintMessage" style="display:none"  class="error">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ConstraintViolation}</span> --%>
   <%--  <div id="ConstraintMessage" class="note note-danger" style="display:none" >
			<i style="color:#D14D80"class="fa-lg fa fa-warning"></i>
     <p style="margin-left: 24px;margin-top: -17px;color:#D14D80;">${ConstraintViolation}</p>
    </div> --%>
    <div id="ConstraintMessage" class="note note-danger" style="display:none" >
     <p  id="Perror" class="error error-Top" style="margin-left: -7px !important;margin-top: -8px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>	
    </div>
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			
			<div class="row margin-top-10">
				<div class="col-md-12">
				
					<form:form method="post" action="#" onsubmit="return IsSubmit();" class="form-horizontal form-row-seperated" modelAttribute="ObjectLocationProfiles" name="myForm">
					
								
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
	
	<!--   <div id="DivLabelCompany" class="form-group">
             <label class="col-md-2 control-label" id="LabelCompany"><b>Company:Rock Audio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fulfillment Center: Value1</b>
             </label>
             
            </div><br>
             -->
      
<%-- <div>${ConstraintViolation}</div> --%>

   <%-- 	<div>
     	<div><inputpath="Tenant" value="${StrTenant}" type="hidden"/>
     </div>
    
    
    <div>
     <div><inputpath="Company" value="${StrCompany}"  type="hidden"/>
     </div> --%>
    											<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LastActivityBy()}: <span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth" value="${LastActivityTeamMember}" maxlength="100" name="LastActivityBy" disabled="true" id="LastActivityBy"  />																						
														<span id="ErrorMessageLastActivityBy" class="error">	</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LastActivityDate()}: <span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"  maxlength="100" name="LastActivityDate" disabled="true" id="LastActivityDate"  />																						
														<input  type="hidden"  id="Last_Activity_DateH" name="Last_Activity_DateH" class="form-controlwidth"/>
														<span id="ErrorMessageLastActivityDate" class="error">	</span>
													</div>
												</div>
												
												<!-- <div class="form-group">
													<label class="col-md-2 control-label">Last Assigned Date: <span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"  maxlength="100" name="LastAssignedDate"  id="LastAssignedDate"  />																						
														<span id="ErrorMessageLastAssignedDate" class="error">	</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">Last Assigned TeamMember: <span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"  maxlength="100" name="LastAssignedTeamMember"  id="LastAssignedTeamMember"  />																						
														<span id="ErrorMessageLastAssignedTeamMember" class="error">	</span>
													</div>
												</div>
												 -->
    											<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_FulfillmentcenterLabel()}: <span class="required">*
													 </span>
													</label>
													
													<div class="col-md-10">
														<!-- <input  type="text" class="form-controlwidth"  maxlength="100" name="FulFillmentCenter"  id="FulFillmentCenter"  /> -->																						
														
															
															 <select   style="display:inline"  class="table-group-action-input form-control input-medium" id="FulFillmentCenter" name="FulFillmentCenter">
															           <option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															            <c:forEach items="${FulfilmentCenterList}" var="FulfilmentCenter">
                                                                       <option value="${FulfilmentCenter.getId().getFulfillmentCenter()}">${FulfilmentCenter.getName20()}</option>
																	</c:forEach>
                                                                      
                                                     	
														</select>
													
														<span id="ErrorMessageFulFillmentCenter" class="error">	</span>
													</div>
												</div>
												
												<!-- <div class="form-group">
													<label class="col-md-2 control-label">Number Of Locations: <span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"  maxlength="100" name="NumberOfLocations"  id="NumberOfLocations"  />																						
														<span id="ErrorMessageNumberOfLocations" class="error">	</span>
													</div>
												</div> -->
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_ProfileGroup()}: <span class="required">*
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="ProfileGroup" name="ProfileGroup">
															<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
														
														
														           <c:forEach items="${GeneralCodeProfileGroup}" var="Profilegroup">
                                                                       <option value="${Profilegroup.getGeneralCode()}">${Profilegroup.getDescription20()}</option>
																	</c:forEach>
															
															
														</select><span id="ErrorMessageProfileGroup" class="error" >  </span>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label"> ${LocationsProfileLabels.getLocationProfiles_LocationProfile()}: <span class="required">*
													 </span>
													</label>
													
													<div class="col-md-10">
														<input   type="text" class="form-controlwidth"  maxlength="10" name="LocationProfile"  id="LocationProfile"  />																						
														<span id="ErrorMessageLocationProfile" class="error"> </span>
													</div>
												</div>
												
			
												
												<div class="form-group">
													<label class="col-md-2 control-label"> ${LocationsProfileLabels.getLocationProfiles_DescriptionShort()}: <span class="required">*
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth" maxlength="20" name="Description20"  id="Description20"  />																						
														<span id="ErrorMessageDescription20" class="error">	</span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_DescriptionLong()}: <span class="required">*
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="50" name="Description50"  id="Description50"  />																						
														<span id="ErrorMessageDescription50" class="error"> </span>
													</div>
												</div>
												
												
												
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationType()}: <span class="required">*
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="LocationType" name="LocationType">
															<option  value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															
														           <c:forEach items="${GeneralCodeLocationType}" var="LocationType">
                                                                       <option value="${LocationType.getGeneralCode()}">${LocationType.getDescription20()}</option>
																	</c:forEach>
															
															
														</select><span id="ErrorMessageLocationType" class="error"> </span>
														
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationHeight()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="12" name="LocationHeight"  id="LocationHeight"  />																						
														<span id="ErrorMessageLocationHeight" class="error long_errormsg"> </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationWidth()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="12" name="LocationWidth"  id="LocationWidth"  />																						
														<span id="ErrorMessageLocationWidth" class="error long_errormsg"> </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationDepth()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="12" name="LocationDepth"  id="LocationDepth"  />																						
														<span id="ErrorMessageLocationDepth" class="error long_errormsg"> </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationWeightCapacity()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="12" name="LocationWeightCapacity"  id="LocationWeightCapacity"  />																						
														<span id="ErrorMessageLocationWeightCapacity" class="error long_errormsg"> </span>
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationHeightCapacity()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="12" name="LocationHeightCapacity"  id="LocationHeightCapacity"  />																						
														<span id="ErrorMessageLocationHeightCapacity" class="error long_errormsg"> </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_NumberOfPallets()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="3" name="NumberOfPallets"  id="NumberOfPallets"  />																						
														<span id="ErrorMessageNumberOfPallets" class="error long_errormsg"> </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_NumberOfFloorPallet()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"    maxlength="3" name="NumberOfFloorPallet"  id="NumberOfFloorPallet"  />																						
														<span id="ErrorMessageNumberOfFloorPallet" class="error long_errormsg"> </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_AllocationAllowable()}: <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="AllocationAllowable" name="AllocationAllowable">
															<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															<option  value="Y">Yes</option>
															<option  value="N">No</option>
														</select>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_FreeLocationWhenZero()}: <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="FreeLocation" name="FreeLocation">
															<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															<option  value="Y">Yes</option>
															<option  value="N">No</option>
														</select>
														
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_FreePrimeDays()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"   maxlength="5" name="PrimeDays"  id="PrimeDays"  />																						
														<span id="ErrorMessagePrimeDays" class="error long_errormsg"> </span>
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_MultipleProducts()}: <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="MultipleProducts" name="MultipleProducts">
															<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															<option  value="Y">Yes</option>
															<option  value="N">No</option>
														</select>
														
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_StorageTypes()}: <span class="required">*
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="StorageType" name="StorageType">
															<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															
														           <c:forEach items="${GeneralCodeStorageType}" var="StorageType">
                                                                       <option value="${StorageType.getGeneralCode()}">${StorageType.getDescription20()}</option>
																	</c:forEach>
															
													
														</select><span id="ErrorMessageStorageType" class="error" > </span>
														
													</div>
												</div>
												
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_Slotting()}: <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="Slotting" name="Slotting">
															<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															<option  value="M">Manual Assigned Location</option>
															<option  value="S">Used In Slotting</option>
														</select>
														
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_LocationCheck()}: <span class="required">
													 </span>
													</label>
													<div class="col-md-10">
														<select   style="display:inline"  class="table-group-action-input form-control input-medium" id="LocationCheck" name="LocationCheck">
															<option value="">${LocationsProfileLabels.getLocationProfiles_SelectDropDown()}...</option>
															<option  value="Y">Yes</option>
															<option  value="N">No</option>
														</select>
														
													</div>
												</div>
												
													
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_CCActivityPoints()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"   maxlength="3" name="CCActivityPoints"  id="CCActivityPoints"  />																						
														<span id="ErrorMessageCCActivityPoints" class="error long_errormsg"> </span>
													</div>
												</div>
												
													
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_CCActivityHighAmount()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"   maxlength="3" name="CCAmount"  id="CCAmount"  />																						
														<span id="ErrorMessageCCAmount" class="error long_errormsg"> </span>
													</div>
												</div>
												
													
												<div class="form-group">
													<label class="col-md-2 control-label">${LocationsProfileLabels.getLocationProfiles_CCActivityHighFactor()}:<span class="required">
													 </span>
													</label>
													
													<div class="col-md-10">
														<input  type="text" class="form-controlwidth"   maxlength="3" name="CCFactor"  id="CCFactor"  />																						
														<span id="ErrorMessageCCFactor" class="error long_errormsg"> </span>
													</div>
												</div>
												
												
												
												
										      <div class="margin-bottom-5-right-allign_Location_Profile_Maintenance">
										      	<!-- 	<button type="button" class="btn btn-sm yellow   margin-bottom" id="validate" onclick="getNotes();" ><i class="fa fa-check"></i>Notes</button> -->
												<button type="submit" class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-check"></i>${LocationsProfileLabels.getLocationProfiles_SaveUpdate()}</button>
													<button type="button" class="btn btn-sm red filter-cancel" onclick="resetPage();"><i class="fa fa-times" ></i>${LocationsProfileLabels.getLocationProfiles_Reset()}</button>
												<button type="button" class="btn btn-sm red filter-cancel" onclick="page_Load();"><i class="fa fa-times" ></i>${LocationsProfileLabels.getLocationProfiles_Cancel()}</button>
										
																																		
											</div>
										</div>
																			
									</div>
								</div>
							</div>
						</div>
						
						</div>
						</div>
					
						</form:form>
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>

</tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationProfileMaintenance.css"/>"
	rel="stylesheet" />

<script>
jQuery(document).ready(function() {    
	$(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	  
	setInterval(function () {
		
  var h = window.innerHeight;
  if(window.innerHeight==928){
  	h=h-187;
  }
 else{
  	h=h-239;
  }
    document.getElementById("page-content").style.minHeight = h+"px";
  
	}, 100);
 //  Metronic.init(); // init metronic core componets
   //Layout.init(); // init layout
   //Demo.init(); // init demo(theme settings page)
   //Index.init(); // init index page
   //Tasks.initDashboardWidget(); // init tash dashboard widget
});
</script>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script>

//^\d{0,8}(\.\d{0,3})$ for decimal

function IsSubmit(){

	var isSubmit = false;
	var testNumber = false;
	
	var isBlankSelectFulFillmentCenter = isBlankField('FulFillmentCenter',
			'ErrorMessageFulFillmentCenter',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_MandatoryFieldErrorMessage()}');
	
	var isBlankSelectProfileGrp = isBlankField('ProfileGroup',
			'ErrorMessageProfileGroup',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_MandatoryFieldErrorMessage()}');
	
	var isBlankLocationProfile = isBlankField('LocationProfile', 'ErrorMessageLocationProfile',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_MandatoryFieldErrorMessage()}');
	
	var isBlankDescriptionShort = isBlankField('Description20',
			'ErrorMessageDescription20',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_MandatoryFieldErrorMessage()}');
	
	var isBlankDescriptionLong = isBlankField('Description50',
			'ErrorMessageDescription50',
	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_MandatoryFieldErrorMessage()}');
	
	var isBlankType = isBlankField('LocationType', 'ErrorMessageLocationType',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_MandatoryFieldErrorMessage()}');
	
	var isBlankStorageType= isBlankField('StorageType',
			'ErrorMessageStorageType',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_MandatoryFieldErrorMessage()}');
	
	var isNumericCCActivityPoints=isNumeric('CCActivityPoints','ErrorMessageCCActivityPoints','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_CCActivityPointsErrorMessage()}');
	var isNumericCCAmount=isNumeric('CCAmount','ErrorMessageCCAmount','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_CCAmountErrorMessage()}');
	var isNumericCCFactor=isNumeric('CCFactor','ErrorMessageCCFactor','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_CCFactorErrorMessage()}');
	var isNumericNumberOfPallets=isNumeric('NumberOfPallets','ErrorMessageNumberOfPallets','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_NumberOfPalletErrorMessage()}');
	var isNumericNumberOfFloorPallet=isNumeric('NumberOfFloorPallet','ErrorMessageNumberOfFloorPallet','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_NumberOfFloorPalletErrorMessage()}');
	var isNumericPrimeDays=isNumeric('PrimeDays','ErrorMessagePrimeDays','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_FreePrimeLocationErrorMessage()}');
	var isFloatLocationHeight=isDecimal('LocationHeight','ErrorMessageLocationHeight','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_LocationHeightErrorMessage()}');
	var isFloatLocationWidth=isDecimal('LocationWidth','ErrorMessageLocationWidth','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_LocationWidthErrorMessage()}');
	var isFloatLocationDepth=isDecimal('LocationDepth','ErrorMessageLocationDepth','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_LocationDepthErrorMessage()}');
	var isFloatLocationWeightCapacity=isDecimal('LocationWeightCapacity','ErrorMessageLocationWeightCapacity','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_LocationWeightCapacityErrorMessage()}');
	var isFloatLocationHeightCapacity=isDecimal('LocationHeightCapacity','ErrorMessageLocationHeightCapacity','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_LocationHeightCapacityErrorMessage()}');
	
	if(!($('#LocationWeightCapacity').val() <= $('#LocationWidth').val()))
	{
		isShowErrorMessage('ErrorMessageLocationWeightCapacity','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth()}');
		isFloatLocationWeightCapacity=false;
	}
	
	if(!($('#LocationHeightCapacity').val() <= $('#LocationHeight').val()))
		{
		
		isShowErrorMessage('ErrorMessageLocationHeightCapacity','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight()}');
		isFloatLocationHeightCapacity=false;
		}
	
	
	if (isBlankSelectFulFillmentCenter && isBlankSelectProfileGrp && isBlankLocationProfile && isBlankDescriptionShort
			&& isBlankDescriptionLong && isBlankType && isBlankStorageType && 
               isNumericCCActivityPoints && isNumericCCAmount && isNumericCCFactor && 
               isNumericNumberOfPallets && isNumericNumberOfFloorPallet && isNumericPrimeDays &&
           isFloatLocationHeight && isFloatLocationWidth && isFloatLocationDepth && 
           isFloatLocationWeightCapacity && isFloatLocationHeightCapacity)
       {

		

	     var LocationProfile = $('#LocationProfile').val();
	    var ProfileGroup=$('#ProfileGroup').val();
	    var FulFillmentCenter=$('#FulFillmentCenter').val();
		 var ValueDescription20=$('#Description20').val();
		  var ValueDescription50=$('#Description50').val();
		var PageName="Add";
		
		$.post(
						"${pageContext.request.contextPath}/RestCheck_AlreadyExists_LocationProfile",
						{LocationProfile:LocationProfile, ProfileGroup:ProfileGroup,FulFillmentCenter:FulFillmentCenter},
						function(data, status) {

							if (data) 
							{
							isSubmit = false;
							var errMsgId = document.getElementById("Perror");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_AlreadyExistsErrorMessage()}';
							$('#ConstraintMessage').show();
								
								
							}else 
							{
								
								
							$.post("${pageContext.request.contextPath}/RestCheckUniqueLocationProfileDescriptionShort",
							{
								FulFillmentCenter:FulFillmentCenter,
								Description20 : ValueDescription20,
								ScreenName : PageName,
								LocationProfile:LocationProfile
							},
							function(data, status) {
								if (data) {
										isSubmit = false;
										var errMsgId = document.getElementById("Perror");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_Description_Short_is_already_Used()}';
										$('#ConstraintMessage').show();
									// alert ("User Already Exists");
								} else {			
									
									$.post("${pageContext.request.contextPath}/RestCheckUniqueLocationProfileDescriptionLong",
								{
									FulFillmentCenter:FulFillmentCenter,
									Description50 : ValueDescription50,
									ScreenName : PageName,
									LocationProfile:LocationProfile
								},
								function(data, status) {
									if (data) {
									isSubmit = false;
										var errMsgId = document.getElementById("Perror");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_Description_Long_is_already_Used()}';
										$('#ConstraintMessage').show();
										// alert ("User Already Exists");
									} else {			
										$('#ConstraintMessage').hide();
										isSubmit=true;
										document.myForm.action = '${pageContext.request.contextPath}/Location_Profile_Add';
										document.myForm.submit();																				
									}
								});
																						
								}
							});
								 
							}
						}); //end post 			
       }
	else {
		 document.getElementById('ConstraintMessage').style.display='none';
		 isSubmit = false;
	}
	//document.location.href = '#top';
	window.scrollTo(0,0);
	return isSubmit;
	
}

$( document ).ready(function() {
		$('#LastActivityDate').val(getLocalDate());
	$('#Last_Activity_DateH').val(getUTCDateTime());
	
	var message='${Saved}';
	if(message=='Saved')
		{
		bootbox.alert('${LocationsProfileLabels.getLocationProfiles_SavedMessage()}',function(){
		window.location.href="Location_Profile_Maintenance_SearchLookup";
		});
		}
	else
	{}
	
	});
	
	function resetPage()
	{
	window.parent.location = window.parent.location.href;
	}
	
function page_Load()
{
		//window.location.href="Location_Profile_Maintenance";
 window.history.back();
	}
	
	function getNotes()
	{
		
	}
	
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'LocationProfileMaintenance',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>