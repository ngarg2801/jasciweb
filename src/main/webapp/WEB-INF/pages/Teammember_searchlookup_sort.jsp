<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE"%>
<script>
var contextPath='<%=request.getContextPath()%>';

</script>
<style>
#AddBtn {
	margin-left: 92.1%;
	margin-bottom: 1%;
}

fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}


.k-grid-content
{
   overflow-y: hidden !important;
}
/* .k-grid th.k-header, .k-grid-header {
	text-align: center !important;
}

.k-grid-content>table>tbody>tr {
	text-align: center !important;
} */
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<div class="body">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SearchLooup()}</h1>
					</div>
				</div>


				<div class="page-content" style="min-height: 428px;">
					<div class="container">
						<%-- 		<script>
var contextPath='<%=request.getContextPath()%>';
<%

%>
</script> --%>






						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active">Dashboard</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<div class="col-md-12">


								<c:if test="${!PartOfTeamMemberObj.equalsIgnoreCase('')}">
									<div class="form-group"
										style="margin-left: -15px; margin-bottom: -22px; margin-bottom: -30px;" >
										<label class="col-md-2 control-label" style="width: 800px; font-size: 16px; margin-left: -15px;"><b>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_PartOfTheTeamMemberName()}: ${PartOfTeamMemberObj} <b></label>
										
									</div>
									</br>
									</br>
									</br>
								</c:if>
							</div>

							<!-- Begin: life time stats -->
							<div class="portlet">

								<a id="AddBtn"
									href="${pageContext.request.contextPath}/Team_member_maintenancenew"
									class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
									<span class="hidden-480"> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddNew()}</span>
								</a>


								<div class="row">

									<kendo:grid name="customers" id="grid" columnResize="true"
										sortable="true" resizable="true" reorderable="true">
										<kendo:grid-pageable refresh="true" pageSizes="true"
											buttonCount="5">
										</kendo:grid-pageable>
										<kendo:grid-editable mode="inline" confirmation="Message" />
										
										<kendo:grid-columns>

											<kendo:grid-column title="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Name()}" field="sortname" />
											
											<kendo:grid-column title="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMember()}" field="teammember" />

											<kendo:grid-column title="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Action()}" width="200px;">
												<kendo:grid-column-command>
													<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
													<kendo:grid-column-commandItem
														className="btn btn-sm yellow filter-submit margin-bottom icon-pencil"
														name="editDetails" text="${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Edit()}">
														<kendo:grid-column-commandItem-click>
															<script>
                           
                            function editTeammember(e) {
                               

                                e.preventDefault();
	
                                
                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                var teamMember=dataItem.teammember;
                               
                               <%--  var contextPath='<%=request.getContextPath()%>'; --%>
                               
                                var url= contextPath+"/Teammember_searchlookup/"+teamMember+"/"+"LastName";
                                //var wnd = $("#details").data("kendoWindow");
                                window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                            }
                            
                          
                            </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

													<kendo:grid-column-commandItem
														className="btn btn-sm red filter-cancel fa fa-times"
														text="Delete">

														<kendo:grid-column-commandItem-click>

															<script>						   
							    function deleteRecords(e) { 
                               

                                e.preventDefault();
                                
                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                
                                
                                var currentStatus=dataItem.currentstatus;
                                 
                                if(currentStatus =='A' || currentStatus =='a' ||  currentStatus =='L' || currentStatus =='l'){
                                if (confirm("Selected team member cannot be deleted as it is used in other table.If you want to  delete then record will be deleted from table SECURITY_AUTHORIZATIONS.") == true) {
  								  
                              
                                var tenant=dataItem.tenant;
                                var sortValue='LastName'
                                var teamMember=dataItem.teammember;
                                var fullFillmentCenter=dataItem.fulfillmentcenter;
						    	var url= contextPath+"/Teammember_searchlookup_delete/"+tenant+"/"+teamMember+"/"+fullFillmentCenter+"/"+sortValue;
	                                //var wnd = $("#details").data("kendoWindow");
	                               window.location=url;
                                }
                                else{
                                	
                                }
                                }else{
                                	
                                }
										}
							    </script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>

												</kendo:grid-column-command>
											</kendo:grid-column>
										</kendo:grid-columns>

										<kendo:dataSource pageSize="10">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/Teammember_searchlookup_sort/Grid/read"></kendo:dataSource-transport-read>
												<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/Customers"></kendo:dataSource-transport-read> --%>
												<kendo:dataSource-transport-update
													url="GeneralCodeList/Grid/update" dataType="json"
													type="POST" contentType="application/json" />
												<kendo:dataSource-transport-destroy
													url="GeneralCodeList/Grid/delete" dataType="json"
													type="POST" contentType="application/json">
													<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
												</kendo:dataSource-transport-destroy>


												<kendo:dataSource-transport-parameterMap>
													<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>
											<kendo:dataSource-schema>
												<kendo:dataSource-schema-model id="intKendoID">
													<kendo:dataSource-schema-model-fields>
														<kendo:dataSource-schema-model-field name="intKendoID">

														</kendo:dataSource-schema-model-field>


													</kendo:dataSource-schema-model-fields>
												</kendo:dataSource-schema-model>
											</kendo:dataSource-schema>



										</kendo:dataSource>
									</kendo:grid>

									<div
										style="float: right; position: relative; margin-top: 10px;">
										<button class="btn btn-sm yellow margin-bottom">
											<i class="fa fa-search"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Notes()}
										</button>
										<button class="btn btn-sm yellow  margin-bottom"
											onclick="sortData();" id="sortBtn">
											<i class="fa fa-check"></i>  ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SortLastName()}
										</button>

									</div>

								</div>
								<!-- End: life time stats -->
							</div>

						</div>
						<div class="row"></div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>

	</tiles:putAttribute>


</tiles:insertDefinition>
<script>
  
    	
function sortData()
{
//sortFirstName display:none;
 
	value='${SortName}';
	
	window.location.href = '${pageContext.request.contextPath}/Teammember_Sort_searchlookup/'+value;
 
}




function deleteWorker(e) {
	
	var tr = $(e.currentTarget).closest("tr");
	var item = $("#grid").data("kendoGrid").dataItem(tr);
	var tenant=item.tenant;
   	  var fullFillmentCenter=item.fulfillmentcenter;
   	  var teamMember=item.teammember;
      var url= contextPath+"/Teammember_searchlookup_delete/"+tenant+"/"+teamMember+"/"+fullFillmentCenter;
      window.location=url;
	
	
	/*  if (confirm("Selected team member cannot be deleted as it is used in other table.If you want to  delete then record will be deleted from table SECURITY_AUTHORIZATIONS.") == true) {
	  
	var tr = $(e.currentTarget).closest("tr");
	var item = $("#grid").data("kendoGrid").dataItem(tr);
	var tenant=item.tenant;
   	  var fullFillmentCenter=item.fulfillmentcenter;
   	  var teamMember=item.teammember;
      var url= contextPath+"/Teammember_searchlookup_delete/"+tenant+"/"+teamMember+"/"+fullFillmentCenter;
      window.location=url;
           
   	
   } else {
       
  
   } */
   
	
    
    
    // Do whatever else you need
}
 
function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'TeamMember',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }
</script>
</html>
