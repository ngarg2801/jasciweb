<!-- 
Date Developed  Sep 18 2014
Description It is used to Search record on behalf of Area,Location,LocationType and Descriptions 
Created By Aakash Bishnoi -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${ScreenLabels.getLocationMaintenace_Location_Lookup()}</h1>
					</div>
				</div>
				<%-- <!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014É World Wide 
*/
//Created By: Suraj jena
//Created on:  10/16/2014
//Modify By: Suraj jena     
//Modify on:10/16/2014
//Purpuse : using for save new record.--!>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

	
	
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT --> --%>
				<div class="page-content" id="page-content">
					<div class="container">


						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">



						<%-- 	<div id="ErrorMessage" class="note note-danger"	style="display: none;">
								<p class="error"
									style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p>
							</div>
							 --%>
	<div id="ErrorMessage" class="note note-danger" style="display:none">
     <p id="MessageRestFull" class="error error-Top"></p>	
    </div>
							<div class="col-md-12">
								<form:form name="myForm"
									class="form-horizontal form-row-seperated" action="#"
									onsubmit="return isformSubmit();" method="get" modelAttribute="ObjectLocation">
									<div class="portlet">
										<input type="hidden" name="backStatus" id="backStatus" value="lookup">
										<div class="portlet-body">
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">
															<div class="form-group">
																<label class="col-md-2 control-label">${ScreenLabels.getLocationMaintenace_Location()}: </label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth" name="Location" id="Location">
																	<i id="BtnIDLocation" class="fa fa-search SearchIconCustomCss"	onclick="actionForm('Location_Maintenance_SearchLookup','Location');"></i>
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${ScreenLabels.getLocationMaintenace_Area()}: </label>
																<div class="col-md-10">
																	<form:select path="Area"  style="display:inline"  class="table-group-action-input form-control input-medium" id="Area" name="Area">
															<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
															
														<c:forEach items="${SelectArea}" var="DropDownArea">
													    		<option value="${DropDownArea.getGeneralCode()}">${DropDownArea.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
														
														
														<i  class="fa fa-search SearchIconCustomCss"
																	id="BtnIDArea"	
																		onclick="actionForm('Location_Maintenance_SearchLookup','Area');"></i>
																		
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${ScreenLabels.getLocationMaintenace_Any_Part_of_Location_Description()}: </label>
																<div class="col-md-10">
																	<!-- <input type="text" class="form-controlwidth"
																		name="PartofLocationDescription" id="PartofLocationDescription"
																		> <i
																		class="fa fa-search SearchIconCustomCss"
																	
																		onclick="actionForm('LocationMaintenanceSearchlookup','PartofLocationDescription');"></i> -->
																		
																	<input type="text" class="form-controlwidth" name="PartofLocationDescription" id="PartofLocationDescription">
																	<i class="fa fa-search SearchIconCustomCss" id="BtnIDPartofLocationDescription"	onclick="actionForm('Location_Maintenance_SearchLookup','PartofLocationDescription');"></i>
															
																</div>
															</div>
															
															<div class="form-group">
																<label class="col-md-2 control-label">${ScreenLabels.getLocationMaintenace_Location_Type()}: </label>
																<div class="col-md-10">
																		<form:select path="Location_Type"  style="display:inline"  class="table-group-action-input form-control input-medium" id="Location_Type" name="Location_Type">
																		<option value="">${ScreenLabels.getLocationMaintenace_Select()}..</option>
																			<c:forEach items="${SelectLocationType}" var="DropDownLocationtype">
																		    		<option value="${DropDownLocationtype.getGeneralCode()}">${DropDownLocationtype.getDescription20()}</option>														   
																		    </c:forEach>	
																		</form:select>
																	
																	<i class="fa fa-search SearchIconCustomCss" id="BtnIDLocation_Type"	onclick="actionForm('Location_Maintenance_SearchLookup','Location_Type');"></i>
																</div>
															</div>


															<div
																class="margin-bottom-5-right-allign_Location_maintenanace">
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom"
																	onclick="actionForm('Location_Maintenance_New','new');">
																	<i class="fa fa-plus"></i>${ScreenLabels.getLocationMaintenace_New()}
																</button>
																	<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdDisplayAll"
																	onclick="actionForm('Location_Maintenance_SearchLookup','displayall');">
																	<i class="fa fa-check"></i>${ScreenLabels.getLocationMaintenace_Display_All()}
																</button>
																<%-- <a href="${pageContext.request.contextPath}/InfoHelpSearchlookup"><button type=
														"button"
															class="btn btn-sm yellow filter-submit margin-bottom">
															
															<i class="fa fa-check"></i> Display All
														</button></a> --%>
																 <!-- <button
																	class="btn btn-sm yellow filter-submit margin-bottom"
																	onclick="actionForm('InfoHelpSearchlookup','displayall');">
																	<i class="fa fa-check"></i>Display All</button>	 -->														</button> 
															</div>

														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form:form>

							</div>
							<!--end tabbable-->

							<!--end tabbable-->

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->





			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<!-- This is used to Set Custom Css that created by Aakash Bishnoi for Location's all Screens -->
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />

<script>

function checkBlank(fieldID){
 var fieldLength = document.getElementById(fieldID).value.trim().length;
 if (fieldLength>0) {
  
  return true;

 }
 else {
  
  
  return false;
 }
}


		  
jQuery(document).ready(function() {    
	
	$('#ErrorMessage').hide();
	
	
	$(window).keydown(function(event){
              if(event.keyCode == 13) {
               		   
               var BoolLocation = checkBlank('Location');
               var BoolArea = checkBlank('Area');
               var BoolPartofLocationDescription = checkBlank('PartofLocationDescription');
               var BoolLocation_Type= checkBlank('Location_Type');
			   
          if(BoolLocation){
				BoolArea=false;
				BoolPartofLocationDescription=false;
				BoolLocation_Type=false;
				$('#BtnIDLocation').click();
          }
		   else if(BoolArea){
				BoolLocation=false;
				BoolPartofLocationDescription=false;
				BoolLocation_Type=false;
				$('#BtnIDArea').click();
          }
		  else if(BoolPartofLocationDescription){
				BoolLocation=false;
				BoolArea=false;
				BoolLocation_Type=false;
				$('#BtnIDPartofLocationDescription').click();
          }
		  else if(BoolLocation_Type){
				BoolLocation=false;
				BoolArea=false;
				BoolPartofLocationDescription=false;
				$('#BtnIDLocation_Type').click();
          }
		/*   else{
		  $('#BtnIdDisplayAll').click();
		  } */
			event.preventDefault();
            return false;
              }
          });
		 
	  
	setInterval(function () {
		
    var h = window.innerHeight;
    if(window.innerHeight>=900){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 30);
 
});
</script>
<script>
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(url, action) {
		$('#ErrorMessage').hide();

		if (action == 'Area') {

			var validArea = isBlankField('Area', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${ScreenLabels.getLocationMaintenace_Please_select_a_Area_first()}');
			var BlankLocation = document.getElementById("Location");
			var BlankDescription = document.getElementById("PartofLocationDescription");
			var BlankLocationtype = document.getElementById("Location_Type");
			BlankDescription.value = "";
			BlankLocation.value = "";
			BlankLocationtype.value = "";

			if (validArea) {

				var AreaValue = $('#Area').val();
				$.get(
								"${pageContext.request.contextPath}/LocationSearch",
								{
									Location : "",
									FulfillmentCenter : "",
									Area : AreaValue,
									Description : "",
									LocationType : ""
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${ScreenLabels.getLocationMaintenace_Invalid_Area()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										isSubmit = true;
										$('#ErrorMessage').hide();
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post info help

			}
			else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		}//end infohelp if 

		else if (action == 'PartofLocationDescription') {

			var validDescription = isBlankField('PartofLocationDescription',
					'MessageRestFull', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${ScreenLabels.getLocationMaintenace_Please_enter_Part_Of_Loctaion_Description()}');

			var BlankLocation = document.getElementById("Location");
			var BlankArea = document.getElementById("Area");
			var BlankLocationtype = document.getElementById("Location_Type");
			BlankArea.value = "";
			BlankLocation.value = "";
			BlankLocationtype.value = "";
			if (validDescription) {
	
				var VarPartofLocationDescription = $('#PartofLocationDescription').val();
				$.get(
								"${pageContext.request.contextPath}/LocationSearch",
								{
									Location : "",
									FulfillmentCenter : "",
									Area : "",
									Description : VarPartofLocationDescription ,
									LocationType : ""
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${ScreenLabels.getLocationMaintenace_Invalid_Location_Description()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										isSubmit = true;
										$('#ErrorMessage').hide();
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} else if (action == 'Location_Type') {

			var validLocation_Type = isBlankField('Location_Type', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getLocationMaintenace_Please_Select_Location_Type()}');

			var BlankLocation = document.getElementById("Location");
			var BlankArea = document.getElementById("Area");
			var BlankPartofLocationDescription = document.getElementById("PartofLocationDescription");
			BlankArea.value = "";
			BlankLocation.value = "";
			BlankPartofLocationDescription.value = "";
			if (validLocation_Type) {

				
				
				
				
				var Location_Type = $("#Location_Type").val()
				$.get(
								"${pageContext.request.contextPath}/LocationSearch",
								{
									Location : "",
									FulfillmentCenter : "",
									Area : "",
									Description : "" ,
									LocationType : Location_Type
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${ScreenLabels.getLocationMaintenace_Invalid_Location_Type()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										isSubmit = true;
										$('#ErrorMessage').hide();
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							
				
				
				
			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		} else if (action == 'Location') {

			var validLocation = isBlankField('Location', 'MessageRestFull',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getLocationMaintenace_Please_enter_Location()}');

			var BlankLocationtype = document.getElementById("Location_Type");
			var BlankArea = document.getElementById("Area");
			var BlankPartofLocationDescription = document.getElementById("PartofLocationDescription");
			BlankArea.value = "";
			BlankLocationtype.value = "";
			BlankPartofLocationDescription.value = "";
			if (validLocation) {

				
				
				
				
				var VarLocation = $("#Location").val()
				$.get(
								"${pageContext.request.contextPath}/LocationSearch",
								{
									Location : VarLocation,
									FulfillmentCenter : "",
									Area : "",
									Description : "" ,
									LocationType : ""
								},
								function(data, status) {

									if (data.length < 1) {
										var errMsgId = document
												.getElementById("MessageRestFull");
										errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${ScreenLabels.getLocationMaintenace_Invalid_Location()}';
										$('#ErrorMessage').show();
										isSubmit = false;
										// alert ("User Already Exists");
									} else {
										isSubmit = true;
										$('#ErrorMessage').hide();
										document.myForm.action = url;
										document.myForm.submit();
									}
								});//end post infoPartoftheDescription
							
				
				
				
			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}
		}
		else if (action == 'displayall') {
		    var BlankLocation = document.getElementById("Location");
		    var BlankDescription = document.getElementById("PartofLocationDescription");
		    var BlankLocationtype = document.getElementById("Location_Type");
		    var BlankArea = document.getElementById("Area");
		    
		    BlankArea.value = "";
		    BlankDescription.value = "";
		    BlankLocation.value = "";
		    BlankLocationtype.value = "";

		        isSubmit = true;
		        $('#ErrorMessage').hide();
		        document.myForm.action = url;
		        document.myForm.submit();
		      
		  
		  
		  
		  } 
		else if(action == 'new'){
			$('#ErrorMessage').hide();
			var url="${pageContext.request.contextPath}/Location_Maintenance_New";
			document.myForm.action = url;
			document.myForm.submit();
		
			
		} 

	}
	
	function headerChangeLanguage(){
	  	
	  	/* alert('in security look up'); */
	  	
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	//alert('in security look up');
	  	
	   	/* $
	  	.post(
	  			"${pageContext.request.contextPath}/HeaderInfoHelp",
	  			function(
	  					data1) {

	  				if (data1.boolStatus) {

	  					location.reload();
	  				}
	  			});
	  	 */
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'LocationMaintenance',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>