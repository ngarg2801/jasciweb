﻿<!--  
Date Developed  Dec 24 2014
Description It is used to show the list of Location profiles.
Created By Diksha Gupta-->

<script>

/* 
$("#selectBox option[value='option1']").remove();
$("#selectBox").append('<option value="option5">option5</option>');
 */

var locationProfileValue=" ";
var numberOfLocations;
var RelocationProfileValue=" ";
var ReFulfillmentCenterValue;
var readurl;
</script>


<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>
<style>
	
	.k-grid tbody .k-button, .k-ie8 .k-grid tbody button.k-button {
min-width: 68px !important;
}
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
<div id="container" style="position: relative" class="loader_div">
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
			<h1>${LocationsProfileLabels.getLocationProfiles_SearchLookupLabel()}</h1>
				
			</div>
		
		</div>

	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			
									
									
			<div class="row margin-top-10">
			<div class="col-md-12">
				
					<div class="portlet">
					 <div class="form-body">
            <!--<div class="form-group divMarginFullfilment">
             <label class="col-md-2 control-label labelSettingFullfillment"><b>Company:Rock Audio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fulfillment Center:value</b>
             </label>
             
            </div><br> -->
            </div>
           	<div id="ConstraintMessage" class="note note-danger" style="display:none" >
     <p  id="Perror" class="error error-Top" style="margin-left: -7px !important;margin-top: -8px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>	
    </div>
						
						<a  id="AddBtnLocationProfile" href="${pageContext.request.contextPath}/Location_Profile_Maintenance" class="btn default yellow-stripe">
							<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${LocationsProfileLabels.getLocationProfiles_AddnewBtn()}</span>
						</a>	 
					<div class="portlet-body">	
						
						<script>
						
					function Check(e)
					{
						
						  $('#ConstraintMessage').hide();	

                            e.preventDefault();
					        var grid = $("#locationProfileGrid").data("kendoGrid");
					        var selectedItem = grid.dataItem(grid.select());
					         ProfileGroupValue=selectedItem.profileGroup;
					         FulfillmentCenterValue=selectedItem.fulFillmentCenter;
                             locationProfileValue=selectedItem.locationProfile;
                             numberOfLocations=selectedItem.numberOfLocations;
                             
                          
                           
					}
					
					function reassignCheck(e)
					{
						
						  $('#ConstraintMessage').hide();	

                            e.preventDefault();
					        var grid = $("#WizardLocation").data("kendoGrid");
					        var selectedItem = grid.dataItem(grid.select());
					         
					          ReFulfillmentCenterValue=selectedItem.fulFillmentCenter;
                            	RelocationProfileValue=selectedItem.locationProfile;
                           
                            	
                           
					}
					</script>
<div class="table-container"  >
							
	<kendo:grid name="locationProfileGrid"  resizable="true" reorderable="true" sortable="true" selectable="row" change="Check" autoSync="true"  dataBound="gridDataBound">
				<kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5">
    	</kendo:grid-pageable>
				<kendo:grid-editable mode="inline" confirmation="" />
				
				<kendo:grid-columns>
				
					<kendo:grid-column field="profileGroup" title="${LocationsProfileLabels.getLocationProfiles_ProfileGroup()}" width="12%" />
					<kendo:grid-column  field="fullfillment_Center_ID_Description" title="${LocationsProfileLabels.getLocationProfiles_FulfillmentcenterLabel()}" width="12%" />
					<kendo:grid-column field="locationProfile"	title="${LocationsProfileLabels.getLocationProfiles_LocationProfile()}" width="12%"/>
					<kendo:grid-column field="description50" title="${LocationsProfileLabels.getLocationProfiles_Description()}" width="12%" />
					<kendo:grid-column field="numberOfLocations" title="${LocationsProfileLabels.getLocationProfiles_NumberOfLocations()}" width="12%" />
					<kendo:grid-column field="lastUsedDate" title="${LocationsProfileLabels.getLocationProfiles_LastUsedDate()}" width="12%" />
					<kendo:grid-column field="lastUsedTeamMember" title="${LocationsProfileLabels.getLocationProfiles_LastUsedBy()}" width="12%" />
					<kendo:grid-column field="lastActivityDate" title="${LocationsProfileLabels.getLocationProfiles_LastChangeDate()}" width="13%" />
				    <kendo:grid-column field="lastActivityTeamMember" title="${LocationsProfileLabels.getLocationProfiles_LastChangeBy()}" width="13%" />
					
					<kendo:grid-column title="${LocationsProfileLabels.getLocationProfiles_Actions()}" width="21%">
						<kendo:grid-column-command>
						<kendo:grid-column-commandItem className="fa fa-copy btn btn-sm yellow filter-submit margin-bottom" name="CopyDetails" text="${LocationsProfileLabels.getLocationProfiles_CopyBtn()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function CopyDetails(e) {
                               

                                e.preventDefault();

                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                var StrTenant=dataItem.tenant;
                                var StrCompany=dataItem.company;
                                var StrFulFillmentCenter=dataItem.fulFillmentCenter;
                             	var StrLocationProfile=dataItem.locationProfile;
                           		var StrProfileGroup=dataItem.profileGroupCode;
            
                                 var url= "${pageContext.request.contextPath}/LocationProfile_maintenance_copy";
                              
                                var myform = document.createElement("form");

                                var TenantField = document.createElement("input");
                                TenantField.type = 'hidden';
                                TenantField.value = StrTenant;
                                TenantField.name = "Tenant";
                                
                                var CompanyField = document.createElement("input");
                                CompanyField.type = 'hidden';
                                CompanyField.value = StrCompany;
                                CompanyField.name = "Company";
                                
                                var FulfillmentCenterField = document.createElement("input");
                                FulfillmentCenterField.type = 'hidden';
                                FulfillmentCenterField.value = StrFulFillmentCenter;
                                FulfillmentCenterField.name = "FulFillmentCenter";
                                
                                var LocationProfileField = document.createElement("input");
                                LocationProfileField.type = 'hidden';
                                LocationProfileField.value = StrLocationProfile;
                                LocationProfileField.name = "LocationProfile";
                                
                                var ProfileGroupField = document.createElement("input");
                                ProfileGroupField.type = 'hidden';
                                ProfileGroupField.value = StrProfileGroup;
                                ProfileGroupField.name = "ProfileGroup";
                                
                                
                                myform.action = url;
                                myform.appendChild(TenantField);
                                myform.appendChild(CompanyField);
                                myform.appendChild(FulfillmentCenterField);
                                myform.appendChild(LocationProfileField);
                                myform.appendChild(ProfileGroupField);
                                document.body.appendChild(myform);
                                myform.submit();
                     
                                
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
                        <kendo:grid-column-commandItem className="fa fa-note btn btn-sm yellow filter-cancel" name="ShowNotes" text="${LocationsProfileLabels.getLocationProfiles_NotesBtn()}">
								<kendo:grid-column-commandItem-click>
								<script>
								function ShowNotes(e)
								{
									 
			                                e.preventDefault();

			                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
			                               
			                             	var StrLocationProfile=dataItem.locationProfile;
			                           					            
			                                 var url= "${pageContext.request.contextPath}/Notes_Lookup";
			                              
			                                var myform = document.createElement("form");

			                                			                                
			                                var NotesLinkField = document.createElement("input");
			                                NotesLinkField.type = 'hidden';
			                                NotesLinkField.value = StrLocationProfile;
			                                NotesLinkField.name = "NOTE_LINK";
			                                
			                                var NotesIdField = document.createElement("input");
			                                NotesIdField.type = 'hidden';
			                                NotesIdField.value = "LOCPROFILE";
			                                NotesIdField.name = "NOTE_ID";
			                                myform.action = url;
			                               
			                                myform.appendChild(NotesLinkField);
			                                myform.appendChild(NotesIdField);
			                                document.body.appendChild(myform);
			                                myform.submit();
		                                
			             
								}
								</script>
								</kendo:grid-column-commandItem-click>
								</kendo:grid-column-commandItem>
							<%-- <kendo:grid-column-commandItem name="edit" text="${ScreenLabels.getMenuMessage_Edit()}" /> --%>
						<kendo:grid-column-commandItem className="icon-pencil btn btn-sm yellow filter-submit margin-bottom" name="editDetails" text="${LocationsProfileLabels.getLocationProfiles_EditBtn()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function editDetails(e) {
                               

                            	var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                var StrTenant=dataItem.tenant;
                                var StrCompany=dataItem.company;
                                var StrFulFillmentCenter=dataItem.fulFillmentCenter;
                             	var StrLocationProfile=dataItem.locationProfile;
                           		var StrProfileGroup=dataItem.profileGroupCode;
                           		var StrNumberOfLocations=dataItem.numberOfLocations;
            
                                 var url= "${pageContext.request.contextPath}/LocationProfile_maintenance_edit";
                              
                                var myform = document.createElement("form");

                                var TenantField = document.createElement("input");
                                TenantField.type = 'hidden';
                                TenantField.value = StrTenant;
                                TenantField.name = "Tenant";
                                
                                var NumberOfLocationsField = document.createElement("input");
                                NumberOfLocationsField.type = 'hidden';
                                NumberOfLocationsField.value = StrNumberOfLocations;
                                NumberOfLocationsField.name = "NumberOfLocations";
                                
                                
                                var CompanyField = document.createElement("input");
                                CompanyField.type = 'hidden';
                                CompanyField.value = StrCompany;
                                CompanyField.name = "Company";
                                
                                var FulfillmentCenterField = document.createElement("input");
                                FulfillmentCenterField.type = 'hidden';
                                FulfillmentCenterField.value = StrFulFillmentCenter;
                                FulfillmentCenterField.name = "FulFillmentCenter";
                                
                                var LocationProfileField = document.createElement("input");
                                LocationProfileField.type = 'hidden';
                                LocationProfileField.value = StrLocationProfile;
                                LocationProfileField.name = "LocationProfile";
                                
                                var ProfileGroupField = document.createElement("input");
                                ProfileGroupField.type = 'hidden';
                                ProfileGroupField.value = StrProfileGroup;
                                ProfileGroupField.name = "ProfileGroup";
                                
                                
                                myform.action = url;
                                myform.appendChild(TenantField);
                                myform.appendChild(CompanyField);
                                myform.appendChild(FulfillmentCenterField);
                                myform.appendChild(LocationProfileField);
                                myform.appendChild(ProfileGroupField);
                                myform.appendChild(NumberOfLocationsField);
                                document.body.appendChild(myform);
                                myform.submit();
                                
                                
                                          
                                
                                
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
							<kendo:grid-column-commandItem className="fa fa-times btn btn-sm red filter-cancel"  name="deleteLocationProfile" text="${LocationsProfileLabels.getLocationProfiles_DeleteBtn()}">
								<kendo:grid-column-commandItem-click>
							<script>
							function deleteLocationProfile(e) {
			               
			                    e.preventDefault();
				                   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                   var StrTenant=dataItem.tenant;
                                   var StrCompany=dataItem.company;
                                   var StrFulFillmentCenter=dataItem.fulFillmentCenter;
                                	var StrLocationProfile=dataItem.locationProfile;
                              		var StrProfileGroup=dataItem.profileGroupCode;
                              		var StrNumberofLocations=dataItem.numberOfLocations;
               
	                                var url= "${pageContext.request.contextPath}/Location_Profile_Delete";
	                                
	                                
	                            	$.post("${pageContext.request.contextPath}/RestGetNumberOfLocation",
	        								{
	        									FulFillmentCenter:StrFulFillmentCenter,
	        									LocationProfile:StrLocationProfile
	        								},
	        								function(data, status) {
	        									if (data) {
	        										bootbox.alert('${LocationsProfileLabels.getLocationProfiles_ReassignBeforeDeletion()}',function(){
											//document.getElementById("Perror").innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_ReassignBeforeDeletion()}';
											//$('#ConstraintMessage').show();	     
											//document.location.href = '#top';
											location.reload();
											//isSubmit = false;
	        									});
	        									}
												else {			
											
	        										//$('#ConstraintMessage').hide();
	        									
                                    	   bootbox
                                           .confirm("${LocationsProfileLabels.getLocationProfiles_DeleteMessage()}",function(okOrCancel) {

                                               if(okOrCancel == true)
                                               {
	                                  
	                                    var myform = document.createElement("form");

	                                    var TenantField = document.createElement("input");
	                                    TenantField.type = 'hidden';
	                                    TenantField.value = StrTenant;
	                                    TenantField.name = "Tenant";
	                                    
	                                    var CompanyField = document.createElement("input");
	                                    CompanyField.type = 'hidden';
	                                    CompanyField.value = StrCompany;
	                                    CompanyField.name = "Company";
	                                    
	                                    var FulfillmentCenterField = document.createElement("input");
	                                    FulfillmentCenterField.type = 'hidden';
	                                    FulfillmentCenterField.value = StrFulFillmentCenter;
	                                    FulfillmentCenterField.name = "FulFillmentCenter";
	                                    
	                                    var LocationProfileField = document.createElement("input");
	                                    LocationProfileField.type = 'hidden';
	                                    LocationProfileField.value = StrLocationProfile;
	                                    LocationProfileField.name = "LocationProfile";
	                                    
	                                    var ProfileGroupField = document.createElement("input");
	                                    ProfileGroupField.type = 'hidden';
	                                    ProfileGroupField.value = StrProfileGroup;
	                                    ProfileGroupField.name = "ProfileGroup";
	                                    
	                                    
	                                    myform.action = url;
	                                    myform.appendChild(TenantField);
	                                    myform.appendChild(CompanyField);
	                                    myform.appendChild(FulfillmentCenterField);
	                                    myform.appendChild(LocationProfileField);
	                                    myform.appendChild(ProfileGroupField);
	                                    document.body.appendChild(myform);
	                                    myform.submit();
	                                    isSubmit = true;
	                                    }
                                           });
	                               }
	      	                                	
	        									        								});
	                                
						}
							</script>
			                </kendo:grid-column-commandItem-click>
							</kendo:grid-column-commandItem>
							
						</kendo:grid-column-command>
					</kendo:grid-column>
				</kendo:grid-columns>
		
				<kendo:dataSource pageSize="10"  autoSync="true">
					<kendo:dataSource-transport>
						<kendo:dataSource-transport-read cache="false" url="${pageContext.request.contextPath}/LocationProfileData/grid/read"></kendo:dataSource-transport-read>
						<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/GeneralCode"></kendo:dataSource-transport-read> --%>
						<kendo:dataSource-transport-update url="GeneralCodeList/Grid/update" dataType="json" type="POST" contentType="application/json" />
						<kendo:dataSource-transport-destroy	url="${pageContext.request.contextPath}/LocationProfileDelete" dataType="json" type="POST" contentType="application/json">
						<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
						</kendo:dataSource-transport-destroy>

				
						<kendo:dataSource-transport-parameterMap>
							<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
						</kendo:dataSource-transport-parameterMap>

					</kendo:dataSource-transport>
				



				</kendo:dataSource>
			</kendo:grid>
							
				</div>
				</div>
				</div>
					<div class="margin-bottom-5-right-allign_LocationProfile_Searchlookup" >
										
										    <button class="btn btn-sm yellow  margin-bottom" onclick="div_show()" id="popup"><i class="fa fa-check"></i>${LocationsProfileLabels.getLocationProfiles_ReassignLocationBtn()}</button>
										<!-- 	<button class="btn btn-sm yellow   margin-bottom" id="validate" ><i class="fa fa-check"></i>Notes</button> -->
											</div>
					</div>
					<!-- End: life time stats -->
			
			</div>
			</div>
			</div>
			
			
			<!-- END PAGE CONTENT INNER -->
		
	</div>
	
	<!-- END PAGE CONTENT -->
	
</div>
</div>
<div class="cover back-cover" ></div>
<div id="container" style="position: relative" class="loader_div">
<div id="show" class="Show_main show-main-div-popup-locationprofilemaintenance" >
<div class="form-body">
            <div id="DivLabelCompany" class="form-group">
             <label class="col-md-2 control-label" id="LabelCompany" style="font-size: 18px;"><b>&nbsp;&nbsp;&nbsp;${LocationsProfileLabels.getLocationProfiles_FromProfile()}:</b>&nbsp;<span id="Label"></span></label>
          
            </div>
				</div><br /><br /><br />
				
			<div class="form-body" id="grid-form">
				<div class="form-group">
					<div id="ErrorMessageGrid" class="note note-danger" style="display:none;">
     					<p id="MessageRestFull" class="error err-modified" ></p>	
    				</div>
					<div class="grid-setting">
					
					<div class="row">

									<kendo:grid name="WizardLocation" columnResize="true"
										sortable="true" resizable="true" reorderable="true" selectable="row" change="reassignCheck" autoSync="true"  dataBound="gridDataBound">
										<kendo:grid-pageable refresh="true" pageSizes="true" 
											buttonCount="5">
										</kendo:grid-pageable>
										<kendo:grid-editable mode="inline" confirmation="Message" />

										<kendo:grid-columns>
										
										<kendo:grid-column field="profileGroup" title="${LocationsProfileLabels.getLocationProfiles_ProfileGroup()}" width="50%" />
										<%-- <kendo:grid-column  field="fullfillment_Center_ID_Description" title="${LocationsProfileLabels.getLocationProfiles_FulfillmentcenterLabel()}" width="30%" /> --%>
										<kendo:grid-column field="locationProfile"	title="${LocationsProfileLabels.getLocationProfiles_LocationProfile()}" width="50%"/>
					
										
										</kendo:grid-columns>

                            
										<kendo:dataSource pageSize="10">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url='${pageContext.request.contextPath}/LocationProfileData/grid/read/reassign'></kendo:dataSource-transport-read>
												
												<kendo:dataSource-transport-update
													url="GeneralCodeList/Grid/update" dataType="json"
													type="POST" contentType="application/json" />
												<kendo:dataSource-transport-destroy
													url="GeneralCodeList/Grid/delete" dataType="json"
													type="POST" contentType="application/json">
													<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
												</kendo:dataSource-transport-destroy>


												<kendo:dataSource-transport-parameterMap>
													<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>
											<kendo:dataSource-schema>
												<kendo:dataSource-schema-model id="intKendoID">
													<kendo:dataSource-schema-model-fields>
														<kendo:dataSource-schema-model-field name="intKendoID">

														</kendo:dataSource-schema-model-field>


													</kendo:dataSource-schema-model-fields>
												</kendo:dataSource-schema-model>
											</kendo:dataSource-schema>



										</kendo:dataSource>
									</kendo:grid>

									<div class="popupbutton popup-bottom-button_LocationProfile" >
						<button class="btn btn-sm green  margin-bottom" onclick="reassign();" id="popup"><i class="fa fa-check"></i> ${LocationsProfileLabels.getLocationProfiles_Apply()}</button>
     					<button class="btn btn-sm red filter-cancel" onclick="div_hide()" id="popup"><i class="fa fa-times"></i>${LocationsProfileLabels.getLocationProfiles_Cancel()}</button>
						</div>

								</div>
					
						
					</div>
					
				</div>
		</div>
	
</div>
	</div>	


</tiles:putAttribute>
</tiles:insertDefinition>
<link type="text/css"
 href="<c:url value="/resourcesValidate/css/LocationProfileMaintenance.css"/>"
 rel="stylesheet" />
 <script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>

<script>

function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
  
   
   
  jQuery(document).ready(function() {  
	  
	  $('#ConstraintMessage').hide();	
	  $('#ErrorMessage').hide();
	$(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	  
	
	/* $("#locationProfileGrid").kendoTooltip({
	     filter: "td:nth-child(2)", //this filter selects the first column cells
	     position: "bottom", 
	     content: function(e){
	      var dataItem = $("#locationProfileGrid").data("kendoGrid").dataItem(e.target.closest("tr"));
	      var content =dataItem.notes;
	      return content;
	     }
	   }).data("kendoTooltip"); */
	
	   $("#locationProfileGrid").kendoTooltip({
		      filter: "td:nth-child(3)", //this filter selects the first column cells
		      iframe:false,
		      width:250,
		      position: "bottom", 
		      content: function(e){
		       var dataItem = $("#locationProfileGrid").data("kendoGrid").dataItem(e.target.closest("tr"));
		       var content =dataItem.notes;
		      if(content){
		         return content;
		        }
		        else{
		         e.preventDefault();
		   }
		      }
		    }).data("kendoTooltip");
	   
	
	setInterval(function () {
		
    var h = window.innerHeight;
    if(window.innerHeight==928){
    	h=h-187;
    }
   else{
    	h=h-239;
    }
      document.getElementById("page-content").style.minHeight = h+"px";
    
	}, 100);
 
});
  
  
//Function To Display Popup
	
	
	function div_show() {
			           
	if(locationProfileValue==" ")
	{

		document.getElementById("Perror").innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_SelectToProfile()}';
	    $('#ConstraintMessage').show();
	    document.location.href = '#top'
	}
	else
	{
		$('#ConstraintMessage').hide();

	if(numberOfLocations=="0")
	{
		document.getElementById("Perror").innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_NoLocationToReassign()}';
	    $('#ConstraintMessage').show();
	    document.location.href = '#top'
	}
	else
		{
		$('#ConstraintMessage').hide();
		$.post(
				"${pageContext.request.contextPath}/Location_Profile_Reassign_JspValue",
				{ReassignprofileGroup:ProfileGroupValue, ReassignfulFillmentCenter:FulfillmentCenterValue,ReassignlocationProfile:locationProfileValue},
				function(data, status) {
						
					if (data) 
					{var grid = $("#WizardLocation").data("kendoGrid");
				    grid.dataSource.page(1);
				    grid.dataSource.read();	     
					$('.Show_main').show();		
					$('.Show_main').css({"display":"inline-table"});
					$('.cover').show();
					document.getElementById("Label").innerHTML=locationProfileValue;			 
					}
					else
					{
						document.getElementById("Perror").innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center()}';
						 $('#ConstraintMessage').show();
						    document.location.href = '#top'
					}
				});

}
}
}
function div_hide() {
$('.Show_main').hide();
$('.cover').hide();
$('#ErrorMessageGrid').hide();
}

function reassign()
{
	$('#ErrorMessageGrid').hide();
	var ToSelectLocation=RelocationProfileValue;
	var FromlocationProfile=locationProfileValue;
	var VarFulFillmentCenterID=ReFulfillmentCenterValue;
	
	if(RelocationProfileValue==" ")
	{
		
	
		document.getElementById("MessageRestFull").innerHTML='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${LocationsProfileLabels.getLocationProfiles_SelectFromProfile()}';
		$('#ErrorMessageGrid').show();
		
	}
			else {
				
				$('#ErrorMessageGrid').hide();
				$.post(
					"${pageContext.request.contextPath}/ReassignLocationProfile",
					{ToSelectLocation:ToSelectLocation, FromlocationProfile:FromlocationProfile,FulFillmentCenter:VarFulFillmentCenterID},
					function(data, status) {

						if (data) 
						{
						window.location.href="LocationProfileMaintenanceSearchLookup";					 
						}
						else
						{
							//alert("error");
						}
					}); //end post 		 */	
				
			}
}
function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'LocationProfileMaintenance',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }

    </script>
   
<script>
 

</script>


