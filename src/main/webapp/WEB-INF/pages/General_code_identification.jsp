<!-- 
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014© World Wide 
Date Developed  Sep 18 2014
Description It is used to show the list of General Code Identification
Created By Aakash Bishnoi -->



<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE"%>
<script>
var contextPath='<%=request.getContextPath()%>';
</script>
<style>
#AddBtn2 {
    margin-right: -1.4%;
    float: right;
    margin-top: -4% !important;
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape)
{
#AddBtn2 {
    margin-right: -1.4%;
    float: right;
    margin-top: -5% !important;
}
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait)
{
#AddBtn2 {
    margin-right: -1.4%;
    float: right;
    margin-top: -6.5%!important;
}
}
 .k-grid th.k-header, .k-grid-header {

    white-space: normal !important;
    /* overflow-y:hidden !important; */
    padding-right: 0px !important;
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
    white-space: normal !important;
    
}
 
 .k-grid-content {
position: relative;
width: 100%;
overflow: auto;
overflow-x: auto; 
overflow-y:hidden !important;
zoom: 1;
} 
 
.fa-remove:before, .fa-close:before, .fa-times:before {
 margin-right: 5px !important;
}

.icon-pencil:before {
 margin-right: 5px !important;
}

.discriptionformat {
	top: -10px;
	font-size: 14px;
	font-weight: 600;
	position: relative;
	text-align: center;
}
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div id="container" style="position: relative" class="loader_div">
		<div class="page-head">
			<div class="container">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1 id="Application">GeneralCode Identification</h1>
				</div>
			</div>
			<div class="body">

				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE CONTENT -->
				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active">Dashboard</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->

						<div class="row margin-top-10">
							<div class="col-md-12">
								<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
								<!-- Begin: life time stats Main List -->
								<div class="portlet">


									<div id="DescriptionLong" class="discriptionformat">DESCRIPTION</div>
									<div class="row" style="width:102.8%">
									<kendo:grid name="generalcodeid" resizable="true"
										reorderable="true" sortable="true">
										<kendo:grid-editable mode="inline" confirmation="true" />

										<kendo:grid-columns>
											<kendo:grid-column
												title="${ScreenLabel.getGeneralCodes_Application()}"
												field="Application" width="16%"/>
											<kendo:grid-column
												title="${ScreenLabel.getGeneralCodes_Code_Identification()}"
												field="GeneralCodeID" width="42%"/>
											<kendo:grid-column
												title="${ScreenLabel.getGeneralCodes_Description()}"
												field="description50" width="42%"/>


										</kendo:grid-columns>

										<kendo:dataSource autoSync="true">
											<kendo:dataSource-transport>
												<kendo:dataSource-transport-read cache="false"
													url="${pageContext.request.contextPath}/GeneralCodeListID/Grid/readlists"></kendo:dataSource-transport-read>

												<kendo:dataSource-transport-parameterMap>
													<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
												</kendo:dataSource-transport-parameterMap>

											</kendo:dataSource-transport>
										</kendo:dataSource>
									</kendo:grid>
									</div>
								</div>
								<!-- End: life time stats MAIN LIST-->
							</div>

						</div>

						<div class="row margin-top-46" >
							<div class="col-md-12" style="margin-left: -14px;">

								<!-- Begin: life time stats SUB LIST -->
								<div class="container">
									<!-- <button onclick="callAddGeneralCodeID()" >Add New</button> -->

									<a class="btn default yellow-stripe" id="AddBtn2"
										onClick="document.location.href=callAddGeneralCodeID();">
										<i class="fa fa-plus"></i> <span class="hidden-480">
											${ScreenLabel.getGeneralCodes_AddNewBtn()}</span>
									</a>


									<script type="text/javascript">
						function callAddGeneralCodeID()
						{
							
						 var data=$("#generalcodeid").data("kendoGrid").dataSource.data();
						 
						 if(data!=null)
						  {
						 if(data[0]!=null)
						  {
							
						   var url = data[0].Tenant+"/"+data[0].Company+"/"+data[0].Application+"/"+data[0].GeneralCode;
						   
							
						   // return url;
						 return contextPath+"/GeneralCodeEditDelete/"+url;
						  }
						 else{
						  
						 }
						  }
						}
						
						
					</script>



									<div class="row">
										<!-- description20 -->
										<kendo:grid name="subgeneralcodelist" resizable="true"
											reorderable="true" sortable="true" dataBound="gridDataBound">
											<kendo:grid-pageable refresh="true" pageSizes="true"
												buttonCount="5">
											</kendo:grid-pageable>
											<kendo:grid-editable mode="inline" confirmation="true" />

											<kendo:grid-columns>
												<kendo:grid-column
													title="${ScreenLabel.getGeneralCodes_General_Code()}"
													field="GeneralCode" width="41%" />

												<kendo:grid-column
													title="${ScreenLabel.getGeneralCodes_Description()}"
													field="description50" width="41%"/>


												<kendo:grid-column title="${ScreenLabel.getGeneralCodes_Actions()}" width="18%">
													<kendo:grid-column-command>
														<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
														<kendo:grid-column-commandItem
															className="icon-pencil btn btn-sm yellow filter-submit margin-bottom"
															name="editDetails" text="${ScreenLabel.getGeneralCodes_Edit()}">
															<kendo:grid-column-commandItem-click>
																<script>
                            function showDetails(e) {
                               

                                e.preventDefault();

                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                var tenant=dataItem.tenant;
                                var company=dataItem.company;
                                var application=dataItem.application;
                                var GeneralCodeID=dataItem.generalCodeID;
                                var GeneralCode=dataItem.generalCode;
                               <%--  var contextPath='<%=request.getContextPath()%>'; --%>
                                
                                var url= contextPath+"/GeneralCodeEdit";
                               
                                //window.location=url;
                                var myform = document.createElement("form");

                                var TenantFiled = document.createElement("input");
                                TenantFiled.type = 'hidden';
                                TenantFiled.value = tenant;
                                TenantFiled.name = "Tenant";
                                
                                var CompanyField = document.createElement("input");
                                CompanyField.type = 'hidden';
                                CompanyField.value = company;
                                CompanyField.name = "Company";
                                
                                var ApplicationField = document.createElement("input");
                                ApplicationField.type = 'hidden';
                                ApplicationField.value = application;
                                ApplicationField.name = "Application";
                                
                                var GeneralCodeIDField = document.createElement("input");
                                GeneralCodeIDField.type = 'hidden';
                                GeneralCodeIDField.value = GeneralCodeID;
                                GeneralCodeIDField.name = "GeneralCodeID";
                                
                                var GeneralCodeField = document.createElement("input");
                                GeneralCodeField.type = 'hidden';
                                GeneralCodeField.value = GeneralCode;
                                GeneralCodeField.name = "GeneralCode";
                                
                                var PageStatusField = document.createElement("input");
                                PageStatusField.type = 'hidden';
                                PageStatusField.value = "1";
                                PageStatusField.name = "PageStatus";
                                
                                myform.action = url;
                                myform.method = "get"
                                myform.appendChild(TenantFiled);
                                myform.appendChild(CompanyField);
                                myform.appendChild(ApplicationField);
                                myform.appendChild(GeneralCodeIDField);
                                myform.appendChild(GeneralCodeField);
                                myform.appendChild(PageStatusField);
                                document.body.appendChild(myform);
                                myform.submit();
                                
                                
                                
                            }
                            </script>
															</kendo:grid-column-commandItem-click>
														</kendo:grid-column-commandItem>
														<kendo:grid-column-commandItem	className="fa fa-times btn btn-sm red filter-cancel" name="DeleteDetails" text="${ScreenLabel.getGeneralCodes_Delete()}">
														<kendo:grid-column-commandItem-click>
															<script>
															function deleteGeneralCode(e) {
											                    e.preventDefault();
											                    var dataItem = this.dataItem($(e.target).closest("tr"));
											                   // if (confirm("${ScreenLabel.getGeneralCodes_Confirm_Delete()}")) {
											                    bootbox
                                .confirm(
                                  '${ScreenLabel.getGeneralCodes_Confirm_Delete()}',
                                  function(okOrCancel) {

                                   if(okOrCancel == true)
                                   {	
											                        var dataSource = $("#subgeneralcodelist").data("kendoGrid").dataSource;
											                        dataSource.remove(dataItem);
											                        dataSource.sync();
											                    }
                                  });
											                }
															</script>
			               								 </kendo:grid-column-commandItem-click>
			               								 </kendo:grid-column-commandItem>
													</kendo:grid-column-command>
												</kendo:grid-column>
											</kendo:grid-columns>

											<kendo:dataSource pageSize="10" autoSync="true">
												<kendo:dataSource-transport>
													<kendo:dataSource-transport-read cache="false"
														url="${pageContext.request.contextPath}/GeneralCodeSubListID/Grid/readSublists"></kendo:dataSource-transport-read>
													<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/Customers"></kendo:dataSource-transport-read> --%>
													<%-- <kendo:dataSource-transport-update
														url="${pageContext.request.contextPath}/GeneralCodeList/Grid/update"
														dataType="json" type="POST" contentType="application/json" /> --%>
													<kendo:dataSource-transport-destroy
														url="${pageContext.request.contextPath}/GeneralCodeID/Grid/delete"
														dataType="json" type="POST" contentType="application/json" />


													<kendo:dataSource-transport-parameterMap>
														<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
													</kendo:dataSource-transport-parameterMap>

												</kendo:dataSource-transport>
												<kendo:dataSource-schema>
													<kendo:dataSource-schema-model id="intKendoID">
														<kendo:dataSource-schema-model-fields>
															<kendo:dataSource-schema-model-field name="intKendoID">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="Tenant"
																type="string">

															</kendo:dataSource-schema-model-field>

															<kendo:dataSource-schema-model-field name="Company"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="Application"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="GeneralCodeID"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="GeneralCode"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="tenant"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="helpline1"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="helpline2"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="helpline3"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="helpline4"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="helpline5"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="company"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="systemUse"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="application"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="generalCode"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="generalCodeID"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="menuOptionName" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="description20"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field name="description50"
																type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="lastActivityDate" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="lastActivityTeamMember" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control01Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control02Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control03Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control04Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control05Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control06Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control07Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control08Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control09Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control10Description" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control01Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control02Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control03Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control04Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control05Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control06Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control07Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control08Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control09Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="control10Value" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber01" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber02" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber03" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber04" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber05" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber06" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber07" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber08" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber09" type="string">

															</kendo:dataSource-schema-model-field>
															<kendo:dataSource-schema-model-field
																name="controlNumber10" type="string">


															</kendo:dataSource-schema-model-field>



														</kendo:dataSource-schema-model-fields>
													</kendo:dataSource-schema-model>
												</kendo:dataSource-schema>



											</kendo:dataSource>
										</kendo:grid>
									</div>



								</div>



								<!-- End: life time stats SUB  LIST-->
							</div>

						</div>

						<div class="row">

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>
	
	</div>
	</tiles:putAttribute>


</tiles:insertDefinition>

<script type="text/javascript">

function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);

function headerChangeLanguage(){
	
	/* alert('in security look up'); */
	
 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	
	
}

function headerInfoHelp(){
	
	//alert('in security look up');
	
 	/* $
	.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
	 */
 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
			  {InfoHelp:'General Code',
 		InfoHelpType:'PROGRAM'
		 
			  }, function( data1,status ) {
				  if (data1.boolStatus) {
					  window.open(data1.strMessage); 
					  					  
					}
				  else
					  {
					//  alert('No help found yet');
					  }
				  
				  
			  });
	
}

    $(document).ready(function () {
    	$.ajax({
            url: '${pageContext.request.contextPath}/GeneralCodeListID/Grid/readlists',
            type: 'GET',
            cache: false,
            success: function(data) {
              var StrApplicationUrl = data[0].GeneralCode;
                 var DescriptionLongUrl= data[0].description50;
                 document.getElementById("Application").innerHTML = StrApplicationUrl.toUpperCase();
                 document.getElementById("DescriptionLong").innerHTML = DescriptionLongUrl.toUpperCase();
            }
        });
    	
	setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
    });

</script>
