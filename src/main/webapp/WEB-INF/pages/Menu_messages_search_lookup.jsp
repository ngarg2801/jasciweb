<!-- 
Date Developed  Sep 18 2014
Description It is used to show the list of General Code Identification
Created By Aakash Bishnoi -->



<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT"%>
<style>
#AddBtn2{
margin-right: -1.35%;
margin-bottom: 1%;
float: right;
}


 .k-grid th.k-header, .k-grid-header {

    white-space: normal !important;
    padding-right: 0px !important;
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
    white-space: normal !important;
    
}

.k-grid-content {
position: relative;
width: 100%;
overflow: auto;
overflow-x: auto; 
overflow-y:hidden !important;
zoom: 1;
} 

fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}

</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
<div id="container" style="position: relative" class="loader_div">
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
			<h1>${ScreenLabels.getMenuMessage_Menu_Messages_Search_Lookup()}</h1>
				
			</div>
			
		
		</div>

	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			
			<div>
			
					<a  id="AddBtn2" href="${pageContext.request.contextPath}/Menu_messages_maintenance_insert" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${ScreenLabels.getMenuMessage_Add_New()}</span>
								</a>	
		</div>
			<div class="row margin-top-46">
			<div class="col-md-12">
				
					
					<div class="portlet">
						
					<%-- 	
					<a  id="AddBtn" href="${pageContext.request.contextPath}/Menu_messages_maintenance_insert" class="btn default yellow-stripe">
								<i class="fa fa-plus"></i>
								<span class="hidden-480">
								${ScreenLabels.getMenuMessage_Add_New()}</span>
								</a>	 --%>	
	<div class="row" >							
	<kendo:grid name="MenuMessage"  resizable="true" reorderable="true" sortable="true" dataBound="gridDataBound">
				<kendo:grid-pageable refresh="true" pageSizes="true" buttonCount="5">
    	</kendo:grid-pageable>
				<kendo:grid-editable mode="inline" confirmation="" />
				
				<kendo:grid-columns>
				
					<kendo:grid-column  field="start_Date" title="${ScreenLabels.getMenuMessage_Start_Date()}" width="14%" />
					<kendo:grid-column field="end_Date"	title="${ScreenLabels.getMenuMessage_End_Date()}" width="14%"/>
					<kendo:grid-column field="type" title="${ScreenLabels.getMenuMessage_Type()}" width="18%" />
					<kendo:grid-column field="message" title="${ScreenLabels.getMenuMessage_Message()}" width="18%" />
					<kendo:grid-column field="company_Name" title="${ScreenLabels.getMenuMessage_Company_Name()}" width="18%" />
					<kendo:grid-column title="${ScreenLabels.getMenuMessage_Actions()}" width="18%">
						<kendo:grid-column-command>
							<%-- <kendo:grid-column-commandItem name="edit" text="${ScreenLabels.getMenuMessage_Edit()}" /> --%>
						<kendo:grid-column-commandItem className="icon-pencil btn btn-sm yellow filter-submit margin-bottom" name="editDetails" text="${ScreenLabels.getMenuMessage_Edit()}">
                        <kendo:grid-column-commandItem-click>
                            <script>
                            function showDetails(e) {
                               

                                e.preventDefault();

                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                                var StrTtenant_ID=dataItem.tenant_ID;
                                var StrCompany_ID=dataItem.company_ID;
                                var StrStatus=dataItem.status;
          	            	var StrType=dataItem.type;
                           		var StrMessage_ID=dataItem.message_ID;
            
                                 var url= "${pageContext.request.contextPath}/Menu_messages_maintenance_update";
                                //var wnd = $("#details").data("kendoWindow");
                                //window.location=url;
                               // wnd.content(detailsTemplate(dataItem));
                                //wnd.center().open();
                                var myform = document.createElement("form");

                                var TenantFiled = document.createElement("input");
                                TenantFiled.type = 'hidden';
                                TenantFiled.value = StrTtenant_ID;
                                TenantFiled.name = "Tenant_ID";
                                
                                var CompanyField = document.createElement("input");
                                CompanyField.type = 'hidden';
                                CompanyField.value = StrCompany_ID;
                                CompanyField.name = "Company_ID";
                                
                                var StatusField = document.createElement("input");
                                StatusField.type = 'hidden';
                                StatusField.value = StrStatus;
                                StatusField.name = "Status";
                                
                                var TypeField = document.createElement("input");
                                TypeField.type = 'hidden';
                                TypeField.value = StrType;
                                TypeField.name = "type";
                                
                                var MessageField = document.createElement("input");
                                MessageField.type = 'hidden';
                                MessageField.value = StrMessage_ID;
                                MessageField.name = "Message_ID";
                                
                                
                                myform.action = url;
                                myform.method = "get"
                                myform.appendChild(TenantFiled);
                                myform.appendChild(CompanyField);
                                myform.appendChild(StatusField);
                                myform.appendChild(TypeField);
                                myform.appendChild(MessageField);
                                document.body.appendChild(myform);
                                myform.submit();
                                
                                
                                
                                
                                
                            }
                            </script>
                        </kendo:grid-column-commandItem-click>
                        </kendo:grid-column-commandItem>
							<kendo:grid-column-commandItem className="fa fa-times btn btn-sm red filter-cancel"  name="DeleteDetails" text="${ScreenLabels.getMenuMessage_Delete()}">
								<kendo:grid-column-commandItem-click>
							<script>
							function deleteMenuMessages(e) {
			                
			                    e.preventDefault();
				                   var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
				                   var StrTtenant_ID=dataItem.tenant_ID;
	                                var StrCompany_ID=dataItem.company_ID;
	                                var StrStatus=dataItem.status;
	                             	var StrType=dataItem.type;
	                           		var StrMessage_ID=dataItem.message_ID;
	                                var dataSource = $("#MenuMessage").data("kendoGrid").dataSource;
	                                var url= "${pageContext.request.contextPath}/MenuMessageDelete";

	                                bootbox
                                    .confirm(
                                      '${ScreenLabels.getMenuMessage_ERR_Confirm_Delete()}',
                                      function(okOrCancel) {

                                       if(okOrCancel == true)
                                       {
										//if (confirm("${ScreenLabels.getMenuMessage_ERR_Confirm_Delete()}")) {
	                                	var myform = document.createElement("form");

	                                    var TenantFiled = document.createElement("input");
	                                    TenantFiled.type = 'hidden';
	                                    TenantFiled.value = StrTtenant_ID;
	                                    TenantFiled.name = "Tenant_ID";
	                                    
	                                    var CompanyField = document.createElement("input");
	                                    CompanyField.type = 'hidden';
	                                    CompanyField.value = StrCompany_ID;
	                                    CompanyField.name = "Company_ID";
	                                    
	                                    var StatusField = document.createElement("input");
	                                    StatusField.type = 'hidden';
	                                    StatusField.value = StrStatus;
	                                    StatusField.name = "Status";
	                                    
	                                    var TypeField = document.createElement("input");
	                                    TypeField.type = 'hidden';
	                                    TypeField.value = StrType;
	                                    TypeField.name = "type";
	                                    
	                                    var MessageField = document.createElement("input");
	                                    MessageField.type = 'hidden';
	                                    MessageField.value = StrMessage_ID;
	                                    MessageField.name = "Message_ID";
	                                    
	                                    
	                                    myform.action = url;
	                                    myform.method = "post"
	                                    myform.appendChild(TenantFiled);
	                                    myform.appendChild(CompanyField);
	                                    myform.appendChild(StatusField);
	                                    myform.appendChild(TypeField);
	                                    myform.appendChild(MessageField);
	                                    document.body.appendChild(myform);
	                                    myform.submit();   
			                }
                                       });
						}
							</script>
			                </kendo:grid-column-commandItem-click>
							</kendo:grid-column-commandItem>
						</kendo:grid-column-command>
					</kendo:grid-column>
				</kendo:grid-columns>
		
				<kendo:dataSource pageSize="10">
					<kendo:dataSource-transport>
						<kendo:dataSource-transport-read cache="false" url="${pageContext.request.contextPath}/MenuMessagesreadAllMessages"></kendo:dataSource-transport-read>
						<%-- <kendo:dataSource-transport-read dataType="json" cache="false" url="http://localhost:8084/GridTest/api/GeneralCode"></kendo:dataSource-transport-read> --%>
						<kendo:dataSource-transport-update url="GeneralCodeList/Grid/update" dataType="json" type="POST" contentType="application/json" />
						<kendo:dataSource-transport-destroy	url="${pageContext.request.contextPath}/MenuMessageDelete" dataType="json" type="POST" contentType="application/json">
						<%-- <kendo:grid-column-commandItem name="junk">
						</kendo:grid-column-commandItem> --%>
						</kendo:dataSource-transport-destroy>


						<kendo:dataSource-transport-parameterMap>
							<script>
	                	function parameterMap(options,type) { 
	                		
	                			return JSON.stringify(options);
	                		
	                	}
                	</script>
						</kendo:dataSource-transport-parameterMap>

					</kendo:dataSource-transport>
				



				</kendo:dataSource>
			</kendo:grid>
							
					</div>	
					</div>
					<!-- End: life time stats -->
				</div>	
				
				
			</div>
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	
	<!-- END PAGE CONTENT -->
	
</div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>

<script>


function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);
   
function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'MenuMessages',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }
  jQuery(document).ready(function() {    
	$(window).keydown(function(event){
	      if(event.keyCode == 13) {
	        event.preventDefault();
	        return false;
	      }
	    });
	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
 
});
    </script>


