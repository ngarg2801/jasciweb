<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ page
	import="com.jasci.common.constant.CONFIGURATIONEFILE,java.util.Properties,java.io.IOException, org.apache.commons.lang.StringEscapeUtils,com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIES,com.jasci.biz.AdminModule.model.COMPANIES,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.*"%>
<!DOCTYPE html>

<%
	String checked = "";
%>
<%
	/* Properties ObjectProperty = new Properties();
	String propFileName = "config.properties";
	try {
		InputStream ObjectInputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
		ObjectProperty.load(ObjectInputStream);
	} catch (IOException ObjectIOException) {
	} */
	CONFIGURATIONEFILE ObjConfiguration=new CONFIGURATIONEFILE();
	String CountryCode = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.CountryCode);
%>

<c:set var="cd" value="<%=CountryCode%>" />


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<div class="body">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMemberMaintenanceNew()}</h1>
					</div>
				</div>


				<!-- BEGIN PAGE CONTENT -->


				<div class="page-content hideHorizontalScrollbar" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						<ul class="page-breadcrumb breadcrumb hide">
							<li><a href="#">Home</a><i class="fa fa-circle"></i></li>
							<li class="active">Team Member maintenance new</li>
						</ul>
						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">


							<!-- BEGIN PAGE CONTENT-->
							<div class="col-md-12">
								<form:form class="form-horizontal form-row-seperated" name="myForm"
									action="#"
									onsubmit="return isformSubmit();"
									modelAttribute="TeamMember_Update_Object" method="post">
									<input type="hidden" name="teamMember" id="checkyear"
										value="${TeamMemberIdValue}">
									<input type="hidden" name="SortName" id="checkyear"
										value="${SortValue}">
										
										<input type="hidden" name="NOTE_LINK" value="${TeamMemberIdValue}" id="iconAddress" >
								        <input type="hidden" name="NOTE_ID" value="TEAMMEM" id="ApplicationId" >
								        <input type="hidden" name="getSetupdate" id="getSetupdate">
										<input type="hidden" name="getLastActivitydate" id="getLastActivitydate">
								 		
										
									<div class="portlet">
										<input type="hidden" id="TeamMemberIdValue" name="TeamMemberIdValue"
											value="${TeamMemberIdValue}">
										<div class="portlet-body">

											<div id="ErrorMessage"  style="display: none;" class="note note-danger"
												>
												<p class="error error-Top margin-left-7pix" id="Perror" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidTeamMemberIdMsg}</p>
											</div>


											<div id="ErrorMessage1" style="display: none;" class="note note-danger"
												>
												<p class="error error-Top margin-left-7pix" id="Perror1" ></p>
											</div>
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">


															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_TeamMemberId()}:
																</label>
																<div class="col-md-10">
																	<input type="text" disabled="disabled"
																		class="form-controlwidth" name="Teammember"
																		id="Teammember"
																		value="${TeamMemberIdValue}"><span
																		id="ErrorMessageTeammember" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SetUpDate()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Setupdate" id="Setupdate"
																		disabled="disabled" >
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SetupBy()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Setupby"
																		value="${SetupTeamMemberName}"
																		disabled="disabled">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastActivityDate()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Lastactivitydate" id="Lastactivitydate"
																		disabled="disabled">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastActivityBy()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Lastactivityteammember"
																		value="${TeamMemberName}"
																		disabled="disabled">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_CurrentStatus()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Currentstatus" id="Currentstatus">
																		<option value="A">Active</option>
																		<option value="L">Leave of Absence</option>
																		<!-- <option value="I">Not Active</option> -->
																		<option value="X">Deleted</option>
																	</select>
																</div>
															</div>

															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_FirstName()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		id="Firstname" name="Firstname" maxlength="50"
																		value="${TeamMember_Update_Object.getFirstName()}">
																	<span id="ErrorMessageFirstname" class="error">
																	</span>
																</div>

															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastName()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																
																	<input type="text" class="form-controlwidth"
																		name="Lastname" id="Lastname" maxlength="50"
																		value="${TeamMember_Update_Object.getLastName()}">
																	<span id="ErrorMessageLastname" class="error"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_MiddleName()}:<!-- span
															class="required"> * </span> -->
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Middlename" id="Middlename" maxlength="50"
																		value="${TeamMember_Update_Object.getMiddleName()}">
																	<span id="ErrorMessageMiddlename" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine1()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline1" maxlength="100"
																		id="Addressline1"
																		value="${TeamMember_Update_Object.getAddressLine1()}">
																	<span id="ErrorMessageAddressline1" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine2()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline2" maxlength="100"
																		id="Addressline2"
																		value="${TeamMember_Update_Object.getAddressLine2()}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine3()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline3" maxlength="100"
																		id="Addressline3"
																		value="${TeamMember_Update_Object.getAddressLine3()}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_AddressLine4()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Addressline4" maxlength="100"
																		id="Addressline4"
																		value="${TeamMember_Update_Object.getAddressLine4()}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_City()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="City"
																		maxlength="50" id="City"
																		value="${TeamMember_Update_Object.getCity()}">
																	<span id="ErrorMessageCity" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_CountryCode()}:
																	<span class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		style="display: inline;" name="Countrycode"
																		id="Countrycode">
																		<option value="none">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach
																			items="${GeneralCodeLanguageCountryCodeListObject}"
																			var="ContyCodeList">
																			<option
																				value='${ContyCodeList.getGeneralCode()}'>${ContyCodeList.getDescription20()}</option>
																		</c:forEach>
																	</select> <span id="ErrorMessageCountrycode" class="error-select"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_State()}:<span
																class="required" id="requiredState"> * </span>
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Statecode" id="Statecode">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach items="${GeneralCodeStatesListObject}"
																			var="StatesList">
																			<option
																				value='${StatesList.getGeneralCode()}'>${StatesList.getDescription20()}</option>
																		</c:forEach>
																	</select> <span id="ErrorMessageState" class="error-select"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ZipCode()}:
																	<span class="required"
																	> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Zipcode" maxlength="10"
																		value="${TeamMember_Update_Object.getZipCode()}"
																		id="ZipCode"> <span id="ErrorMessageZipCode"
																		class="error long_errormsg"></span>

																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkPhone()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workphone"
																		maxlength="10" id="Workphone"
																		value="${TeamMember_Update_Object.getWorkPhone()}">
																	<span id="ErrorMessageWorkphone" class="error long_errormsg"></span>

																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkExtension()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workextension" id="Workextension"
																		maxlength="10"
																		value="${TeamMember_Update_Object.getWorkExtension()}">
																	<span id="ErrorMessageWorkextension" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkCell()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workcell"
																		maxlength="10" id="Workcell"
																		value="${TeamMember_Update_Object.getWorkCell()}">
																	<span id="ErrorMessageWorkcell" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkFax()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workfax"
																		maxlength="10" id="Workfax"
																		value="${TeamMember_Update_Object.getWorkFax()}">
																	<span id="ErrorMessageWorkfax" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_WorkEmail()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Workemailaddress"
																		maxlength="100" id="WorkEmail" value="${TeamMember_Update_Object.getWorkEmailAddress()}">
																	<span id="ErrorMessageWorkEmail" class="error long_errormsg">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_HomePhone()}:<span
																	class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Homephone"
																		maxlength="10" id="Homephone"
																		value="${TeamMember_Update_Object.getHomePhone()}">
																	<span id="ErrorMessageHomephone" class="error long_errormsg"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Cell()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Cell"
																		maxlength="10" id="Cell" value="${TeamMember_Update_Object.getCell()}"> <span id="ErrorMessageCell"	class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Email()}:<span
																	class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Personalemailaddress"
																		maxlength="100" id="Email"
																		value="${TeamMember_Update_Object.getPersonalEmailAddress()}">
																	<span id="ErrorMessageEmail" class="error long_errormsg"> </span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyContactName()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencycontactname" maxlength="50"
																		value="${TeamMember_Update_Object.getEmergencyContactName()}">
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label"
																	style="padding-left: -1px;">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyHomePhone()}:<span	class="required"> * </span>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencycontacthomephone"
																		maxlength="10" id="Emergencyhomephone"
																		value="${TeamMember_Update_Object.getEmergencyContactHomePhone()}">
																	<span id="ErrorMessageEmergencyhomephone" class="error long_errormsg"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyCell()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencycontactcell"
																		maxlength="10" id="Emergencycell"
																		value="${TeamMember_Update_Object.getEmergencyContactCell()}">
																	<span id="ErrorMessageEmergencycell" class="error"></span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_EmergencyEmial()}:
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Emergencyemailaddress"
																		maxlength="100" id="EmergencyEmail"  value="${TeamMember_Update_Object.getEmergencyEmailAddress()}">
																	<span id="ErrorMessageEmergencyEmail" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_DepartMent()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Department" id="Department">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach items="${GeneralCodeDepartmentListObject}"
																			var="DepartmentList">
																			<option
																				value='${DepartmentList.getGeneralCode()}'>${DepartmentList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ShiftCode()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Shiftcode" id="Shiftcode">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<c:forEach items="${GeneralCodeShiftListObject}"
																			var="ShiftList">
																			<option value='${ShiftList.getGeneralCode()}'>${ShiftList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Systemuse" id="Systemuse">
																		<option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option>
																		<option	value='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_Y().charAt(0)}'>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_Y()}</option>
																		<option	value='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_N().charAt(0)}'>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_SystemUse_N()}</option>
																		
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Language()}:
																</label>
																<div class="col-md-10">
																	<select
																		class="table-group-action-input form-control input-medium"
																		name="Language" id="Language">
																		<%-- <option value="">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...</option> --%>
																		<c:forEach items="${GeneralCodeLanguageListObject}"
																			var="LanguageList">
																			<option
																				value='${LanguageList.getGeneralCode()}'>${LanguageList.getDescription20()}</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_StartDate()}:<span class="required" id="rqidStartDate"  style="margin-left: 0px; position: absolute;">* </span> </label>
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Startdate" id="Startdate" readonly="true"
																		style="background-color:white;"
																		value="${StartDateObj}">
																	<span id="ErrorMessageStartdate" class="error">
																	</span>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label" >${TeamMemberSceernLabelObj.getTeamMemberMaintenance_LastDate()}:
																	<!-- <span class="required" id="rqidLastDate"  style="margin-left: 0px; position: absolute;"> * </span> -->
																</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="Lastdate" id="Lastdate" readonly="true"
																		style="background-color:white;"
																		value="${LastDateObj}">
																	<span id="ErrorMessageLastdate" class="error"> </span>
																</div>
															</div>

														</div>

													<div class="col-md-12" style="margin-left: -16px;">
															<!--<div class="note note-danger">
						<p>
							 NOTE: The below datatable is not connected to a real database so the filter and sorting is just simulated for demo purposes only.
						</p>
					</div>-->
															<!-- Begin: life time stats -->
															<div class="portlet">
																<div class="portlet-title">
																	<!--<div class="caption">
								Company: JASCI
							</div>-->
																	<div class="actions"></div>
																</div>
																<div class="portlet-body">
																	<div class="table-container">
																		<div class="table-actions-wrapper">
																			<span> </span>


																		</div>
																		<span id="ErrorMessageCompany" class="error"> </span>
																		<table
																			class=""
																			id="grid">
																			<colgroup>                  
                 													   		<col style="width:1%" />
                  													  		<col style="width:5%" />
                  														  	<col style="width:6%" />
               																</colgroup>
																			
																			<thead>
																				<tr role="row" class="heading">
																					<th width="1%">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}</th>
																					<th width="5%">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Company()}</th>
																					<th width="6%">${TeamMemberSceernLabelObj.getTeamMemberMaintenance_CompanyName()}</th>


																				</tr>
																				</thead>
																				<tbody>
																				<c:forEach items="${CompaniesListObject}"
																					var="CompaniesList">

																					<c:set var="ceckedValue" value="" />

																					<c:forEach items="${CompaniesTeamMemberListObject}"
																						var="CompaniesTeamMemberList">

																						<c:set var="ObjTeamMemberCompany"
																							value="${CompaniesTeamMemberList}" />

																						<c:set var="ObjCompanies" value="${CompaniesList}" />

																						<c:set var="StrTeamMemberCompany"
																							value="${ObjTeamMemberCompany.getId().getCompany()}" />

																						<c:set var="StrCompanies"
																							value="${ObjCompanies.getId().getCompany()}" />

																						<c:if
																							test="${StrCompanies.equalsIgnoreCase(StrTeamMemberCompany)}">

																							<c:set var="ceckedValue" value="checked" />

																						</c:if>

																					</c:forEach>

																					<tr>


																						<td><c:if
																								test="${ceckedValue.equalsIgnoreCase('checked') }">
																								<input type="checkbox" name="checkBoxCompany"
																									class="group-checkable" checked
																									value='${CompaniesList.getId().getCompany()}'>

																							</c:if> <c:if
																								test="${ceckedValue.equalsIgnoreCase('') }">
																								<input type="checkbox" name="checkBoxCompany"
																									class="group-checkable"
																									value='${CompaniesList.getId().getCompany()}'>

																							</c:if></td>
																						<td>${CompaniesList.getId().getCompany()}</td>
																						<td>${CompaniesList.getName50()}</td>

																					</tr>

																				</c:forEach>
																			
																			</tbody>

																		</table>

																	</div>

																</div>

															</div>
															<!-- End: life time stats -->

														</div>



														<div class="margin-right-team-member-maintenence-btn">
															<button class="btn btn-sm yellow  margin-bottom">
																<i class="fa fa-check" style="margin-right: 3px;"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Save()}
															</button>
															<button class="btn btn-sm yellow  margin-bottom"
																onclick="sendToNoteScreen(); return false;">
																<i class="fa fa-note"></i> ${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Notes()}
															</button>
															<button type="button" class="btn btn-sm red "
																onclick="reloadPage();">
																<i class="fa fa-times"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ButtonResetText()}
															</button>
															<button type="button" class="btn btn-sm red "
																onclick="cancelButton();">
																<i class="fa fa-times"></i>${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Cancel()}
															</button>
														</div>

													</div>
												</div>

											</div>
										</div>
									</div>
							</div>
							</form:form>
						</div>
						<!-- END PAGE CONTENT-->

						<!--end tabbable-->

						<!--end tabbable-->

					</div>

					<!-- END PAGE CONTENT INNER -->
				</div>
			</div>
			<!-- END PAGE CONTENT -->
		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/js/moment.js"/>" type="text/javascript"></script>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>


<link type="text/css"
	href="<c:url value="/resourcesValidate/css/ui-lightness/jquery-ui-1.8.19.custom.css"/>"
	rel="stylesheet" />
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/LocationMaintenance.css"/>"
	rel="stylesheet" />
<%-- <script src="<c:url value="/resourcesValidate/js/jquery-1.7.2.min.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resourcesValidate/js/jquery-ui-1.8.19.custom.min.js"/>"
	type="text/javascript"></script> --%>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
/* var varStartTime='${StartDateObj}';
var varLastTime='${LastDateObj}';
var flagStartDate=false;
var flagLadtDate=false; */

	$(function() {
		  var dates = $("#Startdate, #Lastdate").datepicker(
					{
						minDate : '0',
						dateFormat : 'yy-mm-dd',
						//showOn : "button",
						//	buttonImage : "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
						//buttonImage:'/resourcesValidate/css/icon_date_picker.png',
						//buttonImage:'<c:url value="/resources/assets/admin/layout3/img/Language_Icon.png"/>',
						//buttonImageOnly : true,

						onSelect : function(selectedDate) {
							var option = this.id == "Startdate" ? "minDate"
									: "maxDate", instance = $(this).data(
									"datepicker"), date = $.datepicker.parseDate(
									instance.settings.dateFormat
											|| $.datepicker._defaults.dateFormat,
									selectedDate, instance.settings);
							//date.setDate(date.getDate());
							//dates.not(this).datepicker("option", option, date);
						}
					});  

		/*  var dates = $("#Startdate").datepicker({
			minDate : '0',
			dateFormat : 'y-mm-dd'
		});

		var dates = $("#Lastdate").datepicker({
			minDate : '1',
			dateFormat : 'y-mm-dd'
		}); */
 
		$('#Countrycode').change(function() {

			if ($(this).val() == '${cd}') {
				$("#Statecode").removeAttr("disabled");
					$("#requiredState").show();
					$('#idRequired').show();
				$("#Statecode").val($("#Statecode option:first").val());

			} else {
				$("#requiredState").hide();
					$('#idRequired').hide();
				$('#Statecode').attr('disabled', 'disabled');
				$("#Statecode").val($("#Statecode option:first").val());
			}

		});
		
		
		
		
	});

	jQuery(document)
			.ready(
					function() {
						 $('#Setupdate').val(ConvertUTCToLocalTimeZone('${SetupDateObj}'));
						 $('#Lastactivitydate').val(ConvertUTCToLocalTimeZone('${LastActivityDateObj}'));
						 $('#Startdate').val(ConvertUTCToLocalTimeZone($('#Startdate').val()));
						 $('#Lastdate').val(ConvertUTCToLocalTimeZone($('#Lastdate').val()));
						
						 $('#ErrorMessage').hide();
						 $('#ErrorMessage1').hide();
						setCheckBoxValues();

						//Metronic.init(); // init metronic core componets
						//Layout.init(); // init layout
						//Demo.init(); // init demo(theme settings page)
						//Index.init(); // init index page
						//Tasks.initDashboardWidget(); // init tash dashboard widget

						 $("#grid").kendoGrid({
		                       
		                        sortable: true,
		                        resizable:true,
		                        reorderable: true,
		                        
		                        
		                     
		                    });
						setCheckBoxValues();

					});
</script>
<script>

var formSubmit=true;
	function isformSubmit() {

		var isSubmit = false;
		var testNumber = false;

		var isBlankFirstname = isBlankField('Firstname',
				'ErrorMessageFirstname', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankLastname = isBlankField('Lastname', 'ErrorMessageLastname',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankTeamMemberID = isBlankField('Teammember',
				'ErrorMessageTeammember', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankAddressline1 = isBlankField('Addressline1',
				'ErrorMessageAddressline1', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankCity = isBlankField('City', 'ErrorMessageCity',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		var isBlankHomephone = isBlankField('Homephone',
				'ErrorMessageHomephone', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		var isBlankEmergencycontacthomephone = isBlankField(
				'Emergencyhomephone', 'ErrorMessageEmergencyhomephone',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');

		var isNumHomephone = false;
		var isNumEmergencycontacthomephone = false;

		if (isBlankHomephone) {
			isNumHomephone = isNumeric('Homephone', 'ErrorMessageHomephone',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		}

		if (isBlankEmergencycontacthomephone) {
			var isNumEmergencycontacthomephone = isNumeric(
					'Emergencyhomephone', 'ErrorMessageEmergencyhomephone',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
		}

		var isBlankEmail = isBlankField('Email', 'ErrorMessageEmail',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS()}');
		var isValidEmail = false;
		
		if (isBlankEmail) {
			isValidEmail = isValidEmailText('Email', 'ErrorMessageEmail',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS()}');
		}
		
		var isNumWorkphone = isNumeric('Workphone', 'ErrorMessageWorkphone',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_PHONE()}');
		var isNumWorkcell = isNumeric('Workcell', 'ErrorMessageWorkcell',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_CELL()}');
		var isNumWorkfax = isNumeric('Workfax', 'ErrorMessageWorkfax',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_FAX()}');
		var isNumCell = isNumeric('Cell', 'ErrorMessageCell',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_CELL()}');
		var isNumEmergencycontactcell = isNumeric('Emergencycell',
				'ErrorMessageEmergencycell', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL()}');

		var isNumext = isNumeric('Workextension', 'ErrorMessageWorkextension',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION()}');

		if (isNumWorkphone && isNumWorkcell && isNumWorkfax && isNumHomephone
				&& isNumCell && isNumEmergencycontacthomephone
				&& isNumEmergencycontactcell && isNumext) {
			testNumber = true;
		}

		var isValidWorkEmail = isValidEmailText('WorkEmail',
				'ErrorMessageWorkEmail', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT()}');
		/* var isValidEmail = isValidEmailText('Email', 'ErrorMessageEmail',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'Please enter a valid email address.'); */
		var isValidEmergencyEmail = isValidEmailText('EmergencyEmail',
				'ErrorMessageEmergencyEmail', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT()}');

		var isCompanyChecked = isCompanySelected('checkBoxCompany',
				'ErrorMessageCompany', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_NO_COMPANY_SELECTED()}');

		var isCmpnySelected = isCountrySelected();

		var status = $("#Currentstatus :selected").val();
		var isStartDateLessThenEnd = true;
		var isLastDateLessThenEnd = true;
		var isValideStartDate = true;
		var isValideLastDate = true;

		if (status != 'X') {
			$('#rqidLastDate').show();
			/* isValideStartDate = compareDate('Startdate',
					'ErrorMessageStartdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ 'Invalid start date.');*/
							
			var isBalnkStartDate= isBlankField('Startdate', 'ErrorMessageStartdate',
									'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
		
			var isBalnkEndsDate=true;/*  isBlankField('Lastdate', 'ErrorMessageLastdate',
									'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
			 */
			if(isBalnkStartDate && isBalnkEndsDate){
			
				/* isStartDateLessThenEnd = testDateLast('Startdate','Lastdate',
						'ErrorMessageStartdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE()}');
 */
 
				var lastDateValue=$('#Lastdate').val();
				
				if(lastDateValue) { 
				isLastDateLessThenEnd = testDate('Startdate','Lastdate',
						'ErrorMessageLastdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE()}');
								
				}else{
					isLastDateLessThenEnd=true;
				}
								
			}
		
			
			

			/* isStartDateLessThenEnd = isStartDateGreaterThenEndDate('Startdate',
					'Lastdate', 'ErrorMessageStartdate',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ 'Start date must be before End date.'); 
			isLastDateLessThenEnd = isStartDateGreaterThenEndDate('Startdate',
					'Lastdate', 'ErrorMessageLastdate',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ 'Last date must be after Start date.'); */

		} else {
			isStartDateLessThenEnd = true;
			isStartDateLessThenEnd = true
			isLastDateLessThenEnd = true;
			var errMsgId1 = document.getElementById('ErrorMessageStartdate');
			var errMsgId2 = document.getElementById('ErrorMessageLastdate');

			errMsgId1.innerHTML = '';
			errMsgId2.innerHTML = '';
			$('#rqidLastDate').hide();
			//$('#rqidStartDate').hide();
			/* 	isValideLastDate = compareLastDate('Lastdate',
						'ErrorMessageLastdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ 'Invalid last date.');
				isValideStartDate = compareDate('Startdate',
						'ErrorMessageStartdate', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ 'Invalid start date.'); */

		}
		var validZip = isZipCodeValid();
		var validState=isStateValid();

		if (isBlankFirstname && isBlankLastname && isBlankTeamMemberID
				&& isBlankAddressline1 && isBlankCity && isBlankHomephone
				&& isBlankEmergencycontacthomephone && isBlankEmail ) {

			if (isValidEmail && isValidEmergencyEmail && isValidWorkEmail) {

				if (isCompanyChecked && isCmpnySelected && testNumber) {

					if (isStartDateLessThenEnd && isLastDateLessThenEnd
							&& isValideStartDate && validZip && validState) {
						formSubmit=true;
						var errMsgId=document.getElementById("Perror");
						var zipcodeTest=$('#ZipCode').val();
						var oldEmail='${TeamMember_Update_Object.getPersonalEmailAddress()}';
						var newEmail=$('#Email').val();
						
						
					
						
						
						
						
						
						  //http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
								  $.post( "${pageContext.request.contextPath}/RestCheckZipCode",
										  {
									  	TeamMemberZipCode:zipcodeTest
									  	},
									  	function( data,status ) {
									
									 		if(data)
						        	 			{
									
						        	 		errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_ZIP_CODE()}';
						        	 		$('#ErrorMessage').show();
						        	 		isSubmit=false;
						        	 		}
								 
									 else{
						
										 
										 
										 var zipcode=$('#ZipCode').val();
										 var address1=$('#Addressline1').val();
										 var address2=$('#Addressline2').val();
										 var address3=$('#Addressline3').val();
										 var address4=$('#Addressline4').val();
										 var city=$('#City').val();
										 var Countrycode=$("#Countrycode option:selected").text();
										 var Statecode=$("#Statecode option:selected").text();
										 if(Statecode=='${TeamMemberSceernLabelObj.getTeamMemberMaintenance_Select()}...'){
											 Statecode="";
										 }
										 var ZipCode=$('#ZipCode').val();
										 
										 
										 //http://localhost:8080/JASCI/RestCheckZipCode?TeamMemberZipCode=250001
										 $.post( "${pageContext.request.contextPath}/RestCheckAddress",{
											 
											 TeamMemberAddress1:address1,
											 TeamMemberAddress2:address2,
											 TeamMemberAddress3:address3,
											 TeamMemberAddress4:address4,
											 TeamMemberCity:city,
											 TeamMemberStateCode:Statecode,
											 TeamMemberCountryCode:Countrycode,
											 TeamMemberZipCode:ZipCode										 
										 
										 }, function( data,status ) {
											 
											
											 if(data)
								        	 {
											
								        	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_ADDRESS()}';
								        	 $('#ErrorMessage').show();
								        	 isSubmit=false;
								        	 }
											 else{
												 
												 
												 
												 
												 if(oldEmail!=newEmail){
														
														$.post( "${pageContext.request.contextPath}/RestCheckEmail",
																{
															   TeamMemberEmail:newEmail
															   },
															 function( data,status) {
															 
															 if(data)
												        	 {
															
												        	 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+'${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS()}';
												        	 $('#ErrorMessage').show();
												        	 isSubmit=false;												        	 
												        	//document.location.href = '#top';
												        		window.scrollTo(0,0);
												             return isSubmit;
												        	// alert ("User Already Exists");
												        	 }
															 
															 else{
																 $('#ErrorMessage').hide();
																
														       //  alert('${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY()}');
														         
														         bootbox
										                          .alert(
										                            '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY()}',
										                            function() {
										                            	 $('#getSetupdate').val(getUTCDateTime());
																		 $('#getLastActivitydate').val(getUTCDateTime());
																		 $('#Startdate').val(ConvertLocalToUTCCurrentTimeZone($('#Startdate').val()));
																		 $('#Lastdate').val(ConvertLocalToUTCCurrentTimeZone($('#Lastdate').val()));	
																		 
																		 
																		 isSubmit=true;
																		 document.myForm.action = '${pageContext.request.contextPath}/Team_member_maintenance_update';
																		 document.myForm.submit();
										                             });
														        
															 }
														});
													}//end email validate
												 
												 
												 
													else{
												 $('#ErrorMessage').hide();
												 isSubmit=true;
										        // alert('${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY()}');
										         bootbox
						                          .alert(
						                            '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY()}',
						                            function() {
										         $('#getSetupdate').val(getUTCDateTime());
												 $('#getLastActivitydate').val(getUTCDateTime());
												 $('#Startdate').val(ConvertLocalToUTCCurrentTimeZone($('#Startdate').val()));
												 $('#Lastdate').val(ConvertLocalToUTCCurrentTimeZone($('#Lastdate').val()));															
												
										         document.myForm.action = '${pageContext.request.contextPath}/Team_member_maintenance_update';
												 document.myForm.submit();
						                            });
											 }}
											 
										 });//end address validation
										 
										 
										 
									 }
								  });//end zip code validation
						
					
					}

				}
			}
		} else {
			isSubmit = false;
		}
		//document.location.href = '#top';
		window.scrollTo(0,0);
		return isSubmit;
	}

	function isCompanySelected(checkBoxName, errMsgid, errMsg) {

		var errMsgId = document.getElementById(errMsgid);

		var i, chks = document.getElementsByName(checkBoxName), valid = false;
		for (i = 0; i < chks.length; i++) {
			if (chks[i].checked) {
				valid = true;
				break;
			}
		}

		if (valid) {
			errMsgId.innerHTML = '';
			return true;
		} else {
			errMsgId.innerHTML = errMsg;
			return false;
		}
	}

	function isCountrySelected() {

		var errMsgId = document.getElementById('ErrorMessageCountrycode');
		var ContryCode = $("#Countrycode :selected").val();
		if (ContryCode == 'none') {

			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}';
			return false;
		} else {
			errMsgId.innerHTML = '';
			return true;
		}

	}

	function isZipCodeValid() {
		var ContryCode = $("#Countrycode :selected").val();

		if (ContryCode == '${cd}') {

			
			var isBlankZip = isBlankField('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if(isBlankZip){
			var isNumericZip = isNumeric('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if (isNumericZip) {
				//$('#ErrorMessageZipCode').hide();
				isBlankStateCode('ZipCode', 'ErrorMessageZipCode',
		 	           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		 	             + '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				$('#idRequired').hide();
				return true;
			} else {
				$('#ErrorMessageZipCode').show();
				$('#idRequired').show();
				return false;
			}
				}else {
					$('#ErrorMessageZipCode').show();
					$('#idRequired').show();
					return false;
				}

		} else {
			
			var isBlankZip = isBlankField('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if(isBlankZip){
			var isNumericZip = isNumeric('ZipCode', 'ErrorMessageZipCode',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				if (isNumericZip) {
				//$('#ErrorMessageZipCode').hide();
				isBlankStateCode('ZipCode', 'ErrorMessageZipCode',
		 	           '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
		 	             + '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY()}');
				$('#idRequired').hide();
				return true;
			} else {
				$('#ErrorMessageZipCode').show();
				$('#idRequired').show();
				return false;
			}
				}else {
					$('#ErrorMessageZipCode').show();
					$('#idRequired').show();
					return false;
				}
		}

	}

	/* check validation for State*/
		function isStateValid() {
		var ContryCode = $("#Countrycode :selected").val();
		if (ContryCode == '${cd}') {

			var isBlankState = isBlankField(
					'Statecode',
					'ErrorMessageState',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
				

			if (isBlankState) {
			//	$('#ErrorMessageState').hide();
				$('#requiredState').hide();
				return true;
			} else {
			//	$('#ErrorMessageState').show();
				$('#requiredState').show();
				return false;
			}

		} else {
		//	$('#ErrorMessageState').hide();
		 isBlankStateCode('State', 'ErrorMessageState',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${TeamMemberSceernLabelObj.getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK()}');
			$('#requiredState').hide();
			return true;
		}

	}
	
	function setCheckBoxValues() {

		var currentStatusValue = '${TeamMember_Update_Object.getCurrentStatus()}';
		if (currentStatusValue.trim() != '') {
			document.getElementById('Currentstatus').value = currentStatusValue;
			//document.getElementById('Currentstatus').remove(0);
		} else {

		}

		
		var currentDepartmentValue = '${TeamMember_Update_Object.getDepartment()}';
		if (currentDepartmentValue.trim() != '') {
			document.getElementById('Department').value = currentDepartmentValue;
			//document.getElementById('Department').remove(0);
		} else {

		}

		var currentShiftCodeValue = '${TeamMember_Update_Object.getShiftCode()}';
		if (currentShiftCodeValue.trim() != '') {
			document.getElementById('Shiftcode').value = currentShiftCodeValue;
			//document.getElementById('Shiftcode').remove(0);
		} else {

		}

		var currentLanguageValue = '${TeamMember_Update_Object.getLanguage()}';
		if (currentLanguageValue.trim() != '') {
			document.getElementById('Language').value = currentLanguageValue;
			//document.getElementById('Language').remove(0);
		} else {

		}

		var currentCountryCode = '${TeamMember_Update_Object.getCountryCode()}';
		if (currentCountryCode.trim() != '') {
			document.getElementById('Countrycode').value = currentCountryCode;
			//document.getElementById('Language').remove(0);
		} else {

		}
		var ContryCode = $("#Countrycode :selected").val();

		  if (ContryCode == '${cd}') {
		  
		    $("#Statecode").removeAttr("disabled");
		    $("#Statecode").val($("#Statecode option:first").val());
			$("#requiredState").show();
			$('#idRequired').show();

		   } else {
		    $('#Statecode').attr('disabled', 'disabled');
			$("#requiredState").hide();
			$('#idRequired').hide();
		    $("#Statecode").val($("#Statecode option:first").val());
		   }
		
		var currentSystemUse = '${TeamMember_Update_Object.getSystemUse()}';
		if (currentSystemUse.trim() != '') {
			document.getElementById('Systemuse').value = currentSystemUse;
			//document.getElementById('Language').remove(0);
		} else {

		}
		
		
		var temmemberLanguage='${TeamMember_Update_Object.getLanguage()}';
		
		if (temmemberLanguage.trim() != '') {
			document.getElementById('Language').value = temmemberLanguage;
			//document.getElementById('Language').remove(0);
		} else {

		}

		var currentStateValue = '${TeamMember_Update_Object.getStateCode()}';
		if (currentStateValue.trim() != '') {
			document.getElementById('Statecode').value = currentStateValue;
			//document.getElementById('Statecode').remove(0);
		} else {

		}

	}

	function reloadPage(){
	window.parent.location = window.parent.location.href;
	}
	
	function cancelButton() {

		//window.history.back();
		var valueSort = '${SortName}';
					
		
		 if (valueSort == 'LastName' || valueSort=='FirstName' ) {

			
			window.location.href = 'Team_member_maintenance_lookup';
		}
		else{
			
	    	window.location.href = 'Team_member_maintenance';
		} 
 
	}
function headerChangeLanguage(){
		
		/* alert('in security look up'); */
		
	 $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
		
		
	}

	function headerInfoHelp(){
		
		//alert('in security look up');
		
	 	/* $
		.post(
				"${pageContext.request.contextPath}/HeaderInfoHelp",
				function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				});
		 */
	 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
				  {InfoHelp:'TeamMember',
	 		InfoHelpType:'PROGRAM'
			 
				  }, function( data1,status ) {
					  if (data1.boolStatus) {
						  window.open(data1.strMessage); 
						  					  
						}
					  else
						  {
						//  alert('No help found yet');
						  }
					  
					  
				  });
		
	}
	
	function sendToNoteScreen(){
		
		 var url = '${pageContext.request.contextPath}/Notes_Lookup';
		 var myform = document.createElement("form");
		 var TeamMemberValue = $("#TeamMemberIdValue").val();
		 
         var NotesLinkField = document.createElement("input");
         NotesLinkField.type = 'hidden';
         NotesLinkField.value = TeamMemberValue;
         NotesLinkField.name = "NOTE_LINK";
         
         var NotesIdField = document.createElement("input");
         NotesIdField.type = 'hidden';
         NotesIdField.value = "TEAMMEM";
         NotesIdField.name = "NOTE_ID";
         myform.action = url;
         myform.method = "get";
        
         myform.appendChild(NotesLinkField);
         myform.appendChild(NotesIdField);
         document.body.appendChild(myform);
         myform.submit();
		
	}
setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);	
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>