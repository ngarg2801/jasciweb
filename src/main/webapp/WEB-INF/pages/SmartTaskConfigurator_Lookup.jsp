<!-- 
Date Developed  May 14 2015
Description It is used to Search record on behalf of inputs.
Created By Aakash Bishnoi -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">

	<div class="page-head">
			<div class="container">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
				<h1>${ScreenLabels.getSmartTaskConfigurator_Smart_Task_Configurator_Lookup()}</h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
	</div>
	
        <!-- BEGIN PAGE CONTENT -->
      <div class="page-content" id="page-content">
            <div class="container">
                
                
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row margin-top-10">
                
                <div id="ErrorMessage" class="note note-danger" style="display:none">
		     		<p id="MessageRestFull" class="error error-Top"></p>	
		    	</div>
                
                
                    <div class="col-md-12">
                        
                        <form:form name="myForm"
									class="form-horizontal form-row-seperated" action="#"
									onsubmit="return isformSubmit();" method="get" modelAttribute="ObjSmartTaskConfigurator">
                        <div class="portlet">
                            <div class="portlet-body">
                                <div class="tabbable">
                                    <div class="tab-content no-space">
                                        <div class="tab-pane active" id="tab_general">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                       ${ScreenLabels.getSmartTaskConfigurator_Application()}:
                                                    </label>
                                                    <div class="col-md-10">
														<form:select path="APPLICATION_ID"  style="display:inline"  class="table-group-action-input form-control input-medium" id="APPLICATION_ID" name="APPLICATION_ID">
															<option value="">${ScreenLabels.getSmartTaskConfigurator_Select()}..</option>
															
														<c:forEach items="${SelectApplication}" var="DropDownApplication">
													    		<option value="${DropDownApplication.getGeneralCode()}">${DropDownApplication.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDAPPLICATION_ID"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','APPLICATION_ID');"></i>
													</div>
													
                                                  
                                                </div>
												 <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        ${ScreenLabels.getSmartTaskConfigurator_Task()}:
                                                    </label>
                                                    
                                                    
                                                    <div class="col-md-10">
														<form:select path="TASK"  style="display:inline"  class="table-group-action-input form-control input-medium" id="TASK" name="TASK">
															<option value="">${ScreenLabels.getSmartTaskConfigurator_Select()}..</option>
															
														<c:forEach items="${SelectTask}" var="DropDownTask">
													    		<option value="${DropDownTask.getGeneralCode()}">${DropDownTask.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDTASK"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','TASK');"></i>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        ${ScreenLabels.getSmartTaskConfigurator_Execution_Group()}:
                                                    </label>
                                                    
                                                    <div class="col-md-10">
														
														<form:select path="EXECUTION_SEQUENCE_GROUP"  style="display:inline"  class="table-group-action-input form-control input-medium" id="EXECUTION_SEQUENCE_GROUP" name="EXECUTION_SEQUENCE_GROUP">
															<option value="">${ScreenLabels.getSmartTaskConfigurator_Select()}..</option>
															
														<c:forEach items="${SelectExecutionGroup}" var="DropDownExecutionGroup">
													    		<option value="${DropDownExecutionGroup.getGeneralCode()}">${DropDownExecutionGroup.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDEXECUTION_SEQUENCE_GROUP"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','EXECUTION_SEQUENCE_GROUP');"></i>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                          ${ScreenLabels.getSmartTaskConfigurator_Execution_Type()}:
                                                    </label>
                                                   <div class="col-md-10">
														
														<form:select path="EXECUTION_TYPE"  style="display:inline"  class="table-group-action-input form-control input-medium" id="EXECUTION_TYPE" name="EXECUTION_TYPE">
															<option value="">${ScreenLabels.getSmartTaskConfigurator_Select()}..</option>
															
														<c:forEach items="${SelectExecutionType}" var="DropDownExecutionType">
													    		<option value="${DropDownExecutionType.getGeneralCode()}">${DropDownExecutionType.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDEXECUTION_TYPE"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','EXECUTION_TYPE');"></i>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                          ${ScreenLabels.getSmartTaskConfigurator_Execution_Device()}:
                                                    </label>
                                                    <div class="col-md-10">
														
														<form:select path="EXECUTION_DEVICE"  style="display:inline"  class="table-group-action-input form-control input-medium" id="EXECUTION_DEVICE" name="EXECUTION_DEVICE">
															<option value="">${ScreenLabels.getSmartTaskConfigurator_Select()}..</option>
															
														<c:forEach items="${SelectExecutionDevice}" var="DropDownExecutionDevice">
													    		<option value="${DropDownExecutionDevice.getGeneralCode()}">${DropDownExecutionDevice.getDescription20()}</option>														   
															 </c:forEach>	
														</form:select>
														<i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDEXECUTION_DEVICE"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','EXECUTION_DEVICE');"></i>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                         ${ScreenLabels.getSmartTaskConfigurator_Execution_Sequence()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                       <input type="text" class="form-controlwidth" id="EXECUTION_SEQUENCE_NAME" name="EXECUTION_SEQUENCE_NAME">
															<a href="#"><i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDEXECUTION_SEQUENCE_NAME"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','EXECUTION_SEQUENCE_NAME');"></i></a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                       ${ScreenLabels.getSmartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                      <input type="text" class="form-controlwidth" id="DESCRIPTION20" name="DESCRIPTION20">
															<a href="#"><i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDDESCRIPTION20"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','DESCRIPTION20');"></i></a>
                                                    </div>
                                                </div>
                                                	
                                                	<div id="HideTenant" class="form-group" style="display:none;">
                                                             <label class="col-md-2 control-label">
                                                         ${ScreenLabels.getSmartTaskConfigurator_Tenant()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-controlwidth" id="TENANT_ID" name="TENANT_ID">
															<a href="#"><i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDTENANT_ID"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','TENANT_ID');"></i></a>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        ${ScreenLabels.getSmartTaskConfigurator_Menu_Name()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-controlwidth" id="MENU_NAME" name="MENU_NAME">
															<a href="#"><i class="fa fa-search" style="color:rgba(68, 77, 88, 1); cursor:pointer;" id="BtnIDMENU_NAME"	
																		onclick="actionForm('SmartTaskConfigurator_SearchLookup','MENU_NAME');"></i></a>
                                                    </div>
                                                </div>
                                                <div class="margin-bottom-5-right-allign_Smart_Task_Configurator">
                                                   				 <button
																	class="btn btn-sm yellow filter-submit margin-bottom"id="BtnIdNew"
																	onclick="actionForm('SmartTaskConfigurator_Add','new');">
																	<i class="fa fa-plus"></i>${ScreenLabels.getSmartTaskConfigurator_New()}
																</button>
																
																	<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdDisplayAll"
																	onclick="actionForm('SmartTaskConfigurator_SearchLookup','displayall');">
																	<i class="fa fa-check"></i>${ScreenLabels.getSmartTaskConfigurator_Display_All()}
																</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form:form>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->
  
</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
		<link type="text/css"
	href="<c:url value="/resourcesValidate/css/SmartTaskConfigurator.css"/>"
	rel="stylesheet" /> 
	
<script>

function checkBlank(fieldID){
	 var fieldLength = document.getElementById(fieldID).value.trim().length;
	 if (fieldLength>0) {
	  
	  return true;

	 }
	 else {
	  
	  
	  return false;
	 }
	}

$('#ErrorMessage').hide();


$(window).keydown(function(event){
          if(event.keyCode == 13) {
           		   
           var BoolAPPLICATION_ID = checkBlank('APPLICATION_ID');
           var BoolTASK = checkBlank('TASK');
           var BoolEXECUTION_SEQUENCE_GROUP = checkBlank('EXECUTION_SEQUENCE_GROUP');
           var BoolEXECUTION_TYPE = checkBlank('EXECUTION_TYPE');
           var BoolEXECUTION_DEVICE= checkBlank('EXECUTION_DEVICE');		   
		   var BoolDESCRIPTION20 = checkBlank('DESCRIPTION20');
           var BoolEXECUTION_SEQUENCE_NAME = checkBlank('EXECUTION_SEQUENCE_NAME');
           var BoolTENANT_ID = checkBlank('TENANT_ID');
           var BoolMENU_NAME= checkBlank('MENU_NAME');
		   
      if(BoolAPPLICATION_ID){
			
			$('#BtnIDAPPLICATION_ID').click();
      }
      else if(BoolTASK){
			
			$('#BtnIDTASK').click();
      }
	   else if(BoolEXECUTION_SEQUENCE_GROUP){
		
			$('#BtnIDEXECUTION_SEQUENCE_GROUP').click();
      }
	  else if(BoolEXECUTION_TYPE){
			
			$('#BtnIDEXECUTION_TYPE').click();
      }
	  else if(BoolEXECUTION_DEVICE){
			
			$('#BtnIDEXECUTION_DEVICE').click();
      }
	  else if(BoolDESCRIPTION20){
			
			$('#BtnIDDESCRIPTION20').click();
      }
	  else if(BoolEXECUTION_SEQUENCE_NAME){
			
			$('#BtnIDEXECUTION_SEQUENCE_NAME').click();
      }
	  else if(BoolTENANT_ID){
			
			$('#BtnIDTENANT_ID').click();
      }
	  else if(BoolMENU_NAME){
			
			$('#BtnIDMENU_NAME').click();
      }else{
    	  $('#BtnIDAPPLICATION_ID').click();
      }
		event.preventDefault();
        return false;
          }
      });
      
      
jQuery(document).ready(function() {   

	
	if("${JASCI}" == "${ObjCommonSession.getJasci_Tenant()}"){
		$('#HideTenant').show();
	}
	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
});

var isSubmit = false;

function isformSubmit() {
	
		return isSubmit;
}

function actionForm(url, action) {
	$('#ErrorMessage').hide();

	var BlankAPPLICATION_ID = document.getElementById("APPLICATION_ID");
	var BlankTASK = document.getElementById("TASK");
	var BlankEXECUTION_SEQUENCE_GROUP = document.getElementById("EXECUTION_SEQUENCE_GROUP");
	var BlankEXECUTION_TYPE = document.getElementById("EXECUTION_TYPE");
	var BlankEXECUTION_DEVICE = document.getElementById("EXECUTION_DEVICE");
	var BlankDESCRIPTION20 = document.getElementById("DESCRIPTION20");
	var BlankEXECUTION_SEQUENCE_NAME = document.getElementById("EXECUTION_SEQUENCE_NAME");
	var BlankTENANT_ID = document.getElementById("TENANT_ID");
	var BlankMENU_NAME = document.getElementById("MENU_NAME");
	
	if (action == 'APPLICATION_ID') {

		var validAPPLICATION_ID = isBlankField('APPLICATION_ID', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_select_Application()}');
		
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";
		BlankTASK.value="";
		
		if (validAPPLICATION_ID) {

			var APPLICATION_IDValue = $('#APPLICATION_ID').val();
			
			$.post(
							"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : "",
								Application : APPLICATION_IDValue,
								ExecutionGroup : "",
								ExecutionType : "",
								ExecutionDevice : "",
								ExecutionSequence : "",
								Description : "",
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Application()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	
	
	
	else if (action == 'EXECUTION_SEQUENCE_GROUP') {

		var validEXECUTION_SEQUENCE_GROUP = isBlankField('EXECUTION_SEQUENCE_GROUP', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_select_Execution_Group()}');
		
		BlankAPPLICATION_ID.value = "";
		BlankTASK.value="";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";

		
		if (validEXECUTION_SEQUENCE_GROUP) {

			var EXECUTION_SEQUENCE_GROUPValue = $('#EXECUTION_SEQUENCE_GROUP').val();

			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : "",
								Application : "",
								ExecutionGroup : EXECUTION_SEQUENCE_GROUPValue,
								ExecutionType : "",
								ExecutionDevice : "",
								ExecutionSequence : "",
								Description : "",
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Execution_Group()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	else if (action == 'EXECUTION_TYPE') {

		var validEXECUTION_TYPE = isBlankField('EXECUTION_TYPE', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_select_Execution_Type()}');
		
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankTASK.value="";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";

		
		if (validEXECUTION_TYPE) {

			var EXECUTION_TYPEValue = $('#EXECUTION_TYPE').val();
			
			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : "",
								Application : "",
								ExecutionGroup : "",
								ExecutionType : EXECUTION_TYPEValue,
								ExecutionDevice : "",
								ExecutionSequence : "",
								Description : "",
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Execution_Type()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	else if (action == 'EXECUTION_DEVICE') {

		var validEXECUTION_DEVICE = isBlankField('EXECUTION_DEVICE', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_select_Execution_Device()}');
		
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankTASK.value="";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";

		
		if (validEXECUTION_DEVICE) {

			var EXECUTION_DEVICEValue = $('#EXECUTION_DEVICE').val();
			
			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : "",
								Application : "",
								ExecutionGroup : "",
								ExecutionType : "",
								ExecutionDevice : EXECUTION_DEVICEValue,
								ExecutionSequence : "",
								Description : "",
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Execution_Device()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	else if (action == 'DESCRIPTION20') {

		var validDESCRIPTION20 = isBlankField('DESCRIPTION20', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description()}');
		
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankTASK.value="";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";

		
		if (validDESCRIPTION20) {

			var DESCRIPTION20Value = $('#DESCRIPTION20').val();
			
			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : "",
								Application : "",
								ExecutionGroup : "",
								ExecutionType : "",
								ExecutionDevice : "",
								ExecutionSequence : "",
								Description : DESCRIPTION20Value,
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	else if (action == 'EXECUTION_SEQUENCE_NAME') {

		var validEXECUTION_SEQUENCE_NAME = isBlankField('EXECUTION_SEQUENCE_NAME', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_enter_Execution_Sequence()}');
		
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankTASK.value="";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";

		
		if (validEXECUTION_SEQUENCE_NAME) {

			var EXECUTION_SEQUENCE_NAMEValue = $('#EXECUTION_SEQUENCE_NAME').val();
			
			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : "",
								Application : "",
								ExecutionGroup : "",
								ExecutionType : "",
								ExecutionDevice : "",
								ExecutionSequence : EXECUTION_SEQUENCE_NAMEValue,
								Description : "",
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Execution_Sequence()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	else if (action == 'TENANT_ID') {

		var validTENANT_ID = isBlankField('TENANT_ID', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_enter_Tenant()}');
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTASK.value="";
		BlankMENU_NAME.value = "";

		
		if (validTENANT_ID) {

			var TENANT_IDValue = $('#TENANT_ID').val();
			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : TENANT_IDValue,
								Task : "",
								Application : "",
								ExecutionGroup : "",
								ExecutionType : "",
								ExecutionDevice : "",
								ExecutionSequence : "",
								Description : "",
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Tenant()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	else if (action == 'MENU_NAME') {

		var validMENU_NAME = isBlankField('MENU_NAME', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_enter_Menu_Name()}');
		
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankTASK.value="";
		
		
		if (validMENU_NAME) {

			var MENU_NAMEValue = $('#MENU_NAME').val();
			
			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : "",
								Application : "",
								ExecutionGroup : "",
								ExecutionType : "",
								ExecutionDevice : "",
								ExecutionSequence : "",
								Description : "",
								MenuName : MENU_NAMEValue
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Menu_Name()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	

	else if (action == 'TASK') {

		var validTASK = isBlankField('TASK', 'MessageRestFull',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${ScreenLabels.getSmartTaskConfigurator_Please_select_Task()}');
		
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";

		
		if (validTASK) {

			var TASKValue = $('#TASK').val();
			
			$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : "",
								Task : TASKValue,
								Application : "",
								ExecutionGroup : "",
								ExecutionType : "",
								ExecutionDevice : "",
								ExecutionSequence : "",
								Description : "",
								MenuName : ""
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${ScreenLabels.getSmartTaskConfigurator_Invalid_Task()}';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help

		}
		else {
			$('#ErrorMessage').show();
			document.myForm;
			isSubmit = false;
		}

	}
	
	else if (action == 'SearchAll') {

		var APPLICATION_IDValue = $('#APPLICATION_ID').val();
		var TASKValue = $('#TASK').val();
		var EXECUTION_SEQUENCE_GROUPValue = $('#EXECUTION_SEQUENCE_GROUP').val();
		var EXECUTION_TYPEValue = $('#EXECUTION_TYPE').val();
		var EXECUTION_DEVICEValue = $('#EXECUTION_DEVICE').val();
		var DESCRIPTION20Value = $('#DESCRIPTION20').val();
		var EXECUTION_SEQUENCE_NAMEValue = $('#EXECUTION_SEQUENCE_NAME').val();
		var TENANT_IDValue = $('#TENANT_ID').val();
		var MENU_NAMEValue = $('#MENU_NAME').val();
		
		
	if(!APPLICATION_IDValue && !TASKValue && !EXECUTION_SEQUENCE_GROUPValue && !EXECUTION_TYPEValue && !EXECUTION_DEVICEValue && !DESCRIPTION20Value && !EXECUTION_SEQUENCE_NAMEValue && !TENANT_IDValue && !MENU_NAMEValue){
	
	var errMsgId = document.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ 'Please Select atleast one of them.';
	$('#ErrorMessage').show();
	document.myForm;
	isSubmit = false;
	
	}	
	else{
	
						$.post(
					"${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_Search",
							{
								Tenant : TENANT_IDValue,
								Task : TASKValue,
								Application : APPLICATION_IDValue,
								ExecutionGroup : EXECUTION_SEQUENCE_GROUPValue,
								ExecutionType : EXECUTION_TYPEValue,
								ExecutionDevice : EXECUTION_DEVICEValue,
								ExecutionSequence : EXECUTION_SEQUENCE_NAMEValue,
								Description : DESCRIPTION20Value,
								MenuName : MENU_NAMEValue,
							},
							function(data, status) {

								if (data.length < 1) {
									var errMsgId = document
											.getElementById("MessageRestFull");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ 'None of the data here';
									$('#ErrorMessage').show();
									isSubmit = false;
									// alert ("User Already Exists");
								} else {
									isSubmit = true;
									$('#ErrorMessage').hide();
									document.myForm.action = url;
									document.myForm.submit();
								}
							});//end post info help
		}

	}
	
	
	else if (action == 'displayall') {
	   
	    
		BlankAPPLICATION_ID.value = "";
		BlankEXECUTION_SEQUENCE_GROUP.value = "";
		BlankEXECUTION_TYPE.value = "";
		BlankEXECUTION_DEVICE.value = "";
		BlankDESCRIPTION20.value = "";
		BlankEXECUTION_SEQUENCE_NAME.value = "";
		BlankTENANT_ID.value = "";
		BlankMENU_NAME.value = "";
		BlankTASK.value="";
	        isSubmit = true;
	        $('#ErrorMessage').hide();
	        document.myForm.action = url;
	        document.myForm.submit();
	      
	  
	  
	  
	  } 
	else if(action == 'new'){
		$('#ErrorMessage').hide();
		var url="${pageContext.request.contextPath}/SmartTaskConfigurator_Add";
		var myform = document.createElement("form");

          
          
			
    	 var PageNameField = document.createElement("input");
    	 PageNameField.type = 'hidden';
    	 PageNameField.value = "Search";
    	 PageNameField.name = "PageName";

          myform.action = url;
          myform.method = "get"
		  myform.appendChild(PageNameField);
          document.body.appendChild(myform);
          myform.submit();
	
	} 
}

function headerChangeLanguage(){
	  
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'SmartTaskConfigurator',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>