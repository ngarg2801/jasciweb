<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html lang="en" class="no-js">

<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
<link href="<c:url value="/resourcesValidate/css/MenuProfileAssigment.css"/>" rel="stylesheet" type="text/css">
<body>

<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>${objMenuAssignmentLBL.getLbl_Menu_Profiles_Assignment_Lookup()}</h1>
			</div>
			<!-- END PAGE TITLE -->
			
			
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content" id="page-content">
		<div class="container">
			
			<!-- BEGIN PAGE CONTENT INNER -->
			<div id="ErrorMessage1" class="note note-danger"
						style="display: none">
						<span class="error error-Top" id="error1"
							style="margin-left: -7px !important;"></span>
					</div>
			<div class="row margin-top-10">
				<div class="col-md-12">
					<form:form class="form-horizontal form-row-seperated" name="myForm"
					 onsubmit="return isformSubmit();" action="#" method="get">
						<div class="portlet">
							
							<div class="portlet-body">
								<div class="tabbable">
									
									<div class="tab-content no-space">
										<div class="tab-pane active" id="tab_general">
											<div class="form-body">
												
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuAssignmentLBL.getLbl_Team_Member_ID()}:
													</label>
													<div class="col-md-10">
														<input type="text" class="form-controlwidth" name="lblTeamMemberID" id="lblTeamMemberID" >
														<i class="fa fa-search" id="BtnTeamMemberID" 
														style="color:rgba(68, 77, 88, 1); cursor:pointer;"
														onclick="actionForm('MenuProfileAssigmentMaintenance','TeamMemberIDSearch');"></i>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-2 control-label">${objMenuAssignmentLBL.getLbl_Part_of_the_Team_Member_Name()}:
													</label>
													<div class="col-md-10">													
														<input type="text" class="form-controlwidth" name="lblPartOfTeamMemberNameID" id="lblPartOfTeamMemberNameID" >
														<i class="fa fa-search" id="BtnPartOfTeamMemberID"
														 style="color:rgba(68, 77, 88, 1); cursor:pointer;"
														onclick="actionForm('MenuProfileAssignmentSearchLookup','PartOfTeamMemberNameSearch');"></i>
													</div>
												</div>
											
												<div class="margin-bottom-5-right-allign_info_menuprofile_team">
											
										    
											<button class="btn btn-sm yellow filter-submit margin-bottom" id="BtnDisplayAllID"
											onclick="actionForm('MenuProfileAssignmentSearchLookup','DisplayAll');"><i class="fa fa-check"></i>&nbsp;${objMenuAssignmentLBL.getLbl_Display_All()}</button></div>
												
												
											</div>
										</div>
										
										
										
										
									</div>
								</div>
							</div>
						</div>
					</form:form>
				</div>
				
			</div>
			
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

</tiles:putAttribute>

</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	
	
	
	
<script>



function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
  $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  }


function actionForm(url, action) {
	$('#ErrorMessage').hide();
	if (action == 'TeamMemberIDSearch') {

		var valid = isBlankField('lblTeamMemberID', 'error1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuAssignmentLBL.getLbl_Please_enter_a_Team_Member_ID()}');

		if (valid) {

			var PartTeamMemberName= document
					.getElementById("lblPartOfTeamMemberNameID"); // Get text field
					PartTeamMemberName.value = "";

			
				

			var valTeamMemberID = $('#lblTeamMemberID').val();
			$('#ErrorMessage1').hide();
			var FieldValue = valTeamMemberID;
			var Field = 'lblTeamMemberID';

			$.post("${pageContext.request.contextPath}/RestFull_Check_TeamMemberID",
							{
								SearchValue : FieldValue,
								SearchField : Field
								
							},
							function(data, status) {

								if (!data) {

									error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${objMenuAssignmentLBL.getLbl_Invalid_Team_Member_ID()}';
									$('#ErrorMessage1').show();
									isSubmit = false;
								} else {
									$('#ErrorMessage1').hide();
									isSubmit = true;
									document.myForm.action = '${pageContext.request.contextPath}/'
											+ url;
									document.myForm.submit();
									//return isSubmit;
								}
							});//end check team member

		} else {
			$('#ErrorMessage1').show();
			document.myForm;
			var PartTeamMemberName= document
			.getElementById("lblPartOfTeamMemberNameID"); // Get text field
			PartTeamMemberName.value = "";
			isSubmit = false;

		}

	}

	else if (action == 'PartOfTeamMemberNameSearch') {

		var validPartAppl = isBlankField('lblPartOfTeamMemberNameID', 'error1',
				'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ '${objMenuAssignmentLBL.getLbl_Please_enter_part_of_Team_Member_Name()}');

		if (validPartAppl) {
			var teamMemberId = document
			.getElementById("lblTeamMemberID"); // Get text field
			teamMemberId.value = "";

	
	
			$('#ErrorMessage1').hide();

			var partofTeamMemberName = $('#lblPartOfTeamMemberNameID').val();

			var FieldValue = partofTeamMemberName;
			var Field = 'lblPartOfTeamMemberNameID';

			$
					.post(
							"${pageContext.request.contextPath}/RestFull_check_PartOfTeamMemberName",
							{
								SearchValue : FieldValue,
								SearchField : Field
							},
							function(data, status) {

								if (!data) {

									error1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${objMenuAssignmentLBL.getLbl_Invalid_part_of_Team_Member_Name()}';
									$('#ErrorMessage1').show();
									isSubmit = false;
								} else {
									$('#ErrorMessage1').hide();
									isSubmit = true;
									document.myForm.action = '${pageContext.request.contextPath}/'
											+ url;
									document.myForm.submit();
									//return isSubmit;
								}
							});//end check team member				

		} else {
			$('#ErrorMessage1').show();
			document.myForm;
			var teamMemberId = document
			.getElementById("lblTeamMemberID"); // Get text field
			teamMemberId.value = "";
			
			isSubmit = false;

		}

	} else if (action == 'DisplayAll') {
		
		var teamMemberID = document
		.getElementById("lblTeamMemberID"); // Get text field
		teamMemberID.value = "";


var PartOfTeamMemberName = document.getElementById("lblPartOfTeamMemberNameID");
PartOfTeamMemberName.value = "";
		document.myForm.action = '${pageContext.request.contextPath}/'
				+ url;
		document.myForm.submit();
		$('#ErrorMessage').hide();
		isSubmit = true;
	}

}





jQuery(document).ready(function() {  
	
	
	
	
	
	setInterval(function () {
		
	    var h = window.innerHeight;		    
	    if(window.innerHeight>=900){
	    	h=h-187;
	    	
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";		     
	    
		}, 30);
	 function checkBlank(fieldID){
		var fieldLength = document.getElementById(fieldID).value.trim().length;
		if (fieldLength>0) {
			
			return true;

		}
		else {
			
			
			return false;
		}
	} 

	$(window).keydown(function(event){
	    if(event.keyCode == 13) {
	     
	    	var BoolTeamID = checkBlank('lblTeamMemberID');
	    	var BoolPartOFTeamMember = checkBlank('lblPartOfTeamMemberNameID');
	    	
	    	if(BoolTeamID && BoolPartOFTeamMember){
	    		
	    		$('#BtnTeamMemberID').click();
	    		
	    	}else if((!BoolTeamID) &&  (BoolPartOFTeamMember)){
	    		$('#BtnPartOfTeamMemberID').click();
	    		
	    	}else if((BoolTeamID) &&  (!BoolPartOFTeamMember)){
	    		
	    		$('#BtnTeamMemberID').click();
	    	}/* else{
	    		$('#BtnDisplayAllID').click();
	    		
	    		
	    	} */
	    	
	    	 event.preventDefault();
	         return false;
	    }
	  });
	
   //Metronic.init(); // init metronic core componets
   //Layout.init(); // init layout
  // Demo.init(); // init demo(theme settings page)
  // Index.init(); // init index page
  // Tasks.initDashboardWidget(); // init tash dashboard widget
});


function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
  	$.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'MenuProfileAssignment',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>