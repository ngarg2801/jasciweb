<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
		<div class="page-container">
			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${viewLabels.getLblLookUPScreenTitle()}</h1>
					</div>
				</div>

				<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
				<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
				<!--[if !IE]><!-->



				<!-- END PAGE HEAD -->
				<!-- BEGIN PAGE CONTENT -->
			
				<div class="page-content" id = "page-content">
					<div class="container">


						<!-- END PAGE BREADCRUMB -->
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-10">
							<div id="ErrorMessage" class="note note-danger"
								style="display: none;">
								<p class="error error-Top"   id="Perror"  style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p>
							</div>

							<div class="col-md-12">
								<form:form name="myForm"
									class="form-horizontal form-row-seperated" action="#"
									onsubmit="return isformSubmit();" method="GET">
									<input type="hidden" name="backStatus" id="backStatus" value="lookup">
									
									<div class="portlet">

										<div class="portlet-body">
											<div class="tabbable">

												<div class="tab-content no-space">
													<div class="tab-pane active" id="tab_general">
														<div class="form-body">
															<div class="form-group">
																<label class="col-md-2 control-label">${viewLabels.getLblLookUpEnterTeamMember()}:</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="TeamMemberName" id="TeamMemberName"
																		>
																		<i class="fa fa-search" id="searchTeamMemberName" style="color:rgba(68, 77, 88, 1); cursor:pointer;" onclick="actionForm('Team_member_selection','searchTeamMember');"></i>
																</div>
															</div>
															<div class="form-group">
																<label class="col-md-2 control-label">${viewLabels.getLblLookUpPartOfTeamMember()}:</label>
																<div class="col-md-10">
																	<input type="text" class="form-controlwidth"
																		name="PartOfTeamMemberName" id="PartOfTeamMemberName"
																		>
																		<i  class="fa fa-search" id="searchPartOfTeamMemberName" style="color:rgba(68, 77, 88, 1); cursor:pointer;" onclick="actionForm('Team_member_selection','searchPartOfTeamMember');"></i>
																</div>
															</div>

															<div
																class="margin-bottom-5-right-allign_team_memeber_lookup">

										
																	
																	<button
																		class="btn btn-sm yellow filter-submit margin-bottom" id="btnDispID"
																		onclick="actionForm('Security_setup_maintenance','displayall');">
																		<i class="fa fa-check"></i>&nbsp; ${viewLabels.getLblLookUpDisplayAll()}
																	</button>
															

															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
										</div>
								</form:form>

							</div>


						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>

			</div>
	</tiles:putAttribute>

</tiles:insertDefinition>



<!-- END PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/resourcesValidate/form-validation.js"/>" type="text/javascript"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
	
		
		
		var value = '${TeamMemberInvalidMsg}';

		if (value.length > 5) {
			$('#ErrorMessage').show();
		} else {
			$('#ErrorMessage').hide();
		}

		//Metronic.init(); // init metronic core componets
		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)
		//Index.init(); // init index page
		//Tasks.initDashboardWidget(); // init tash dashboard widget

	});
</script>

<script type="text/javascript">
	var isSubmit = false;

	function isformSubmit() {

		return isSubmit;
	}

	function actionForm(url, action) {

		if (action == 'search') {
			var TeamMemberName = isBlankField('TeamMemberName', 'Perror',
					'At least one field is required for search.');

			var PartOfTeamMemberName = isBlankField('PartOfTeamMemberName', 'Perror',
					'At least one field is required for search.');

			if (TeamMemberName || PartOfTeamMemberName) {
				document.myForm.action = url;
				document.myForm.submit();
				$('#ErrorMessage').hide();
				isSubmit = true;

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}

		}
		else if (action=='searchTeamMember')
			{
			var elem1 = document.getElementById("PartOfTeamMemberName");
			elem1.value = "";
			var TeamMemberName = isBlankField('TeamMemberName', 'Perror',
			'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${viewLabels.getErrTeamMemberNotLeftBlank()}');
			if (TeamMemberName) {
				
				
				
				var varTeamMember = $('#TeamMemberName').val();
				var errMsgId = document.getElementById("Perror");
				
				$.post("${pageContext.request.contextPath}/GetTeamMemberById",
											{
											TeamMember :varTeamMember,
											},
											function(data, status) {

												if (data.length==0) {
													
													errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
														+ '${viewLabels.getErrLookUpNotAvalidTeamMember()}';
												$('#ErrorMessage').show();
												isSubmit = false;
												}
												else{
													document.myForm.action = url;
													document.myForm.submit();
													$('#ErrorMessage').hide();
													isSubmit = true;
													
												}
											});
				
				

			} else {
				$('#ErrorMessage').show();
				document.myForm;
				isSubmit = false;
			}
			
			
			}
		else if (action=='searchPartOfTeamMember')
		{
			var elem = document.getElementById("TeamMemberName");
			elem.value = "";
			var PartOfTeamMemberName = isBlankField('PartOfTeamMemberName', 'Perror',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${viewLabels.getErrPartOfTeamMemberNotLeftBlank()}');

	if (PartOfTeamMemberName) {
	
	var varPartOfTeamMember = $('#PartOfTeamMemberName').val();
	var errMsgId = document.getElementById("Perror");
	
	$.post("${pageContext.request.contextPath}/GetTeamMemberByPartName",
								{
									PartOfTeamMember :varPartOfTeamMember,
								},
								function(data, status) {

									if (data.length==0) {
									
									
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
												+ '${viewLabels.getErrLookUpNotAvalidPartOfTeamMember()}';
										$('#ErrorMessage').show();
										isSubmit = false;
									}
									else{
									document.myForm.action = url;
									document.myForm.submit();
									$('#ErrorMessage').hide();
									isSubmit = true;
									}
									});
	
	
	} else {
		$('#ErrorMessage').show();
		document.myForm;
		isSubmit = false;
	}
		}
		else if (action == 'displayall') {

			var elem = document.getElementById("TeamMemberName");
			elem.value = "";
			var elem1 = document.getElementById("PartOfTeamMemberName");
			elem1.value = "";

			document.myForm.action = url;
			document.myForm.submit();
			$('#ErrorMessage').hide();
			isSubmit = true;
		}

	}
</script>
<script>

function headerChangeLanguage(){
  	
  	/* alert('in security look up'); */
  	
   $.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
  	
  	
  }

  function headerInfoHelp(){
  	
  	//alert('in security look up');
  	
   	/* $
  	.post(
  			"${pageContext.request.contextPath}/HeaderInfoHelp",
  			function(
  					data1) {

  				if (data1.boolStatus) {

  					location.reload();
  				}
  			});
  	 */
   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
  			  {InfoHelp:'SecuritySetUp',
   		InfoHelpType:'PROGRAM'
  		 
  			  }, function( data1,status ) {
  				  if (data1.boolStatus) {
  					  window.open(data1.strMessage); 
  					  					  
  					}
  				  else
  					  {
  					//  alert('No help found yet');
  					  }
  				  
  				  
  			  });
  	
  }
	jQuery(document).ready(function() {

		  
		setInterval(function () {
			
		    var h = window.innerHeight;
		    if(window.innerHeight>=900 || window.innerHeight==1004 ){
		    	h=h-187;
		    }
		   else{
		    	h=h-239;
		    }
		      document.getElementById("page-content").style.minHeight = h+"px";
		    
			}, 30);
	    
		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)

		$(window).keydown(function(event){
		    if(event.keyCode == 13) {
			
			var BoolTeamMemberID = checkBlank('TeamMemberName');
            var BoolTeamPartName= checkBlank('PartOfTeamMemberName');
			 if(BoolTeamMemberID && BoolTeamPartName){
                  $('#searchTeamMemberName').click();
              }
			else if(BoolTeamMemberID && !BoolTeamPartName){
			$('#searchTeamMemberName').click();
			
			}
			else if(!BoolTeamMemberID && BoolTeamPartName){
			$('#searchPartOfTeamMemberName').click();
			}
			/* else{
			$('#btnDispID').click();
			} */
			
			
			
		      event.preventDefault();
		      return false;
		    }
		  });

	});
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>