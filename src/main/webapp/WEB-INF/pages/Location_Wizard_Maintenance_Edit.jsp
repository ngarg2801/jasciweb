<!-- 
Date Developed  Dec 30 2014
Description It is used to Search record on behalf of wizard control no,Area,LocationType and Any Part of the Wizard Description 
Created By Pradeep Kumar -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">

<style>
	
.k-grid td {
  
    overflow: visible;
    
}
	
	</style>

		<div class="page-head">
			<div class="container">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>${Location_Wizard_Maintenance_Level.getKP_Location_Wizard_Maintenance()}</h1>
				</div>
			</div>
			<%-- <!--Company : NGI VENTURES PVT. LTD. 
/*
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014É World Wide 
*/

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

	
	
		<!-- END PAGE HEAD -->
		<!-- BEGIN PAGE CONTENT --> --%>
			<div class="page-content" id="page-content">
				<div class="container">


					<!-- END PAGE BREADCRUMB -->
					<!-- BEGIN PAGE CONTENT INNER -->
					<div class="row margin-top-10">



						<%-- 	<div id="ErrorMessage" class="note note-danger"	style="display: none;">
								<p class="error"
									style="margin-left: -7px !important;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${TeamMemberInvalidMsg}</p>
							</div>
							 --%>
							 
						<div id="ErrorMessage" class="note note-danger CustomHide"
							style="display: none">
							<p id="MessageRestFull" class="error error-Top"></p>
						</div>
						<div class="col-md-12">
							<form name="myForm" id="myForm"
								class="form-horizontal form-row-seperated" action="#"
								onsubmit="return false"
								<%-- onsubmit="return isformSubmit();" --%>
								modelAttribute="LOCATIONWIZARD_REGISTER"
								method="get">
								<div class="portlet">

									<div class="portlet-body">
										<div class="tabbable">

											<div class="tab-content no-space">
												<div class="tab-pane active" id="tab_general">
													<div class="form-body">
														<%-- <div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Status()}:</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="Status" id="STATUS"
																	value="${Location_Wizard_Searched_Data.getStatus()}">

															</div>
														</div> --%>


														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Created_Date()}:</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	disabled="disabled" name="DateCreated1"
																	id="DATE_CREATED"
																	value="${Location_Wizard_Searched_Data.getDateCreated()}">

																<input type="hidden" class="form-controlwidth"
																	name="DateCreated" id="DATE_CREATED1"
																	value="${Location_Wizard_Searched_Data.getDateCreated()}">

															</div>
														</div>


														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Created_By()}:</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	disabled="disabled" name="CreatedByTeamMember"
																	id="CREATED_BY_TEAM_MEMBER"
																	value="${Location_Wizard_Searched_Data.getCreatedByTeamMember()}">

															</div>
														</div>
														
														

														<%-- <div class="form-group" id="AppliedDate">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Applied_Date()}:</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	disabled="disabled" name="LastActivityDate1"
																	id="LAST_ACTIVITY_DATE"
																	value="${Location_Wizard_Searched_Data.getLastActivityDate()}">

																<input type="hidden" class="form-controlwidth"
																	name="LastActivityDate" id="LAST_ACTIVITY_DATE1"
																	value="${Location_Wizard_Searched_Data.getLastActivityDate()}">
																
															</div>
														</div>


														<div class="form-group" id="AppliedBy">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Applied_By()}:</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	disabled="disabled" name="LastActivityTeamMember"
																	id="LAST_ACTIVITY_TEAM_MEMBER"
																	value="${Location_Wizard_Searched_Data.getLastActivityTeamMember()}">
																
															</div>
														</div> --%>

														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Fulfillment_Center()}:
																<span class="required">*</span>
															</label>
															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="FulfillmentCenterId" id="FULFILLMENT_CENTER_ID">
																	<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																	<c:forEach items="${Fulfillment_Drop_Down}"
																		var="fulfillmentList">
																		<option
																			value='${fulfillmentList.getId().getFulfillmentCenter()}'>${fulfillmentList.getName20()}</option>
																	</c:forEach>
																	
																</select> <span class="error" id="FulfillmentNoErrorMessage"></span>
															</div>
														</div>


														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Wizard_ID()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="WizardId1" id="WIZARD_ID" disabled="disabled">
																	<%-- <option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}..</option> --%>
																	<%-- <c:forEach items="${Drop_Down_Wizard_ID}"
																		var="Wizard_ID"> --%>

																		<option value="LOCATIONS">LOCATIONS</option>
																	<%-- </c:forEach> --%>
																	
																</select> <span class="error" id="WIZARD_IDErrorMessage"></span>
															</div>
														</div>
														
														<div class="form-group hidden">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Wizard_ID()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="WizardId1" id="WIZARD_ID">
																	<%-- <option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}..</option> --%>
																	<%-- <c:forEach items="${Drop_Down_Wizard_ID}"
																		var="Wizard_ID"> --%>

																		<option value="LOCATIONS">LOCATIONS</option>
																	<%-- </c:forEach> --%>
																	
																</select> <span class="error" id="WIZARD_IDErrorMessage"></span>
															</div>
														</div>
														
														<div class="form-group hidden">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Wizard_ID()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="WizardId" id="WIZARD_ID">
																	<%-- <option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}..</option> --%>
																	<c:forEach items="${Drop_Down_Wizard_ID}"
																		var="Wizard_ID">

																		<option value="LOCATIONS">LOCATIONS</option>
																	</c:forEach>
																	
																</select> <span class="error" id="WIZARD_IDErrorMessage"></span>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Wizard_Control_Number()}:</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="WizardConNumber" id="WIZARD_CONTROL_NUMBER"
																	maxlength="11" disabled="disabled"
																	value="${Location_Wizard_Searched_Data.getWizardControlNumber()}"><span
																	class="error" id="WIZARDControlNoErrorMessage"></span>
																
															</div>
														</div>

														<div class="form-group hidden">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Wizard_Control_Number()}:</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	name="WizardControlNumber" id="WIZARD_CONTROL_No"
																	maxlength="11"
																	value="${Location_Wizard_Searched_Data.getWizardControlNumber()}"><span
																	class="error" id="WIZARDControlNoErrorMessage"></span>
																
															</div>
														</div>
														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Description_Short()}:
																<span class="required">*</span>
															</label>
															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	maxlength="20" name="Description20" id="DESCRIPTION20"
																	value="${Location_Wizard_Searched_Data.getDescription20()}"><span
																	id="ErrorMessageshortId" class="error"> 
															</div>
														</div>


														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Description_Long()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<input type="text" class="form-controlwidth"
																	maxlength="50" name="Description50" id="DESCRIPTION50"
																	value="${Location_Wizard_Searched_Data.getDescription50()}">
																	<span
																	class="error" id="LongErrorMessage"></span> 
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Area()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${Location_Wizard_Maintenance_Drop_Down}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="Area" id="AREA">
																	<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																	<c:forEach
																		items="${Location_Wizard_Maintenance_Drop_Down}"
																		var="AreaList">
																		<option value='${AreaList.getGeneralCode()}'>${AreaList.getDescription20()}</option>
																	</c:forEach>
																	
																</select> <span class="error" id="AreaErrorMessage"></span>
															</div>
														</div>


														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_WorkGroup_Zone()}:
																<span class="required">*</span>
															</label>
															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="WorkGroupZone" id="WORK_GROUP_ZONE">
																	<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																	<c:forEach
																		items="${Location_Wizard_Maintenance_Drop_Down_WRKZONE}"
																		var="WorkGroup">
																		<option value='${WorkGroup.getGeneralCode()}'>${WorkGroup.getDescription20()}</option>
																	</c:forEach>
																	
																</select> <span class="error" id="WorkGroupErrorMessage"></span>
															</div>
														</div>


														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Work_Zone()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="WorkZone" id="WORK_ZONE">
																	<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																	<c:forEach
																		items="${Location_Wizard_Maintenance_Drop_Down_WZONE}"
																		var="WorkZone">
																		<option value='${WorkZone.getGeneralCode()}'>${WorkZone.getDescription20()}</option>
																	</c:forEach>
																	
																</select> <span class="error" id="WorkZoneErrorMessage"></span>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Location_Type()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="LocationType" id="LOCATION_TYPE">
																	<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																	<c:forEach items="${Drop_Down_Location_Type}"
																		var="LocationType">
																		<option
																			value='${LocationType.getGeneralCode()}'>${LocationType.getDescription20()}</option>
																	</c:forEach>
																	
																</select> <span class="error" id="Location_TypeErrorMessage"></span>
															</div>
														</div>

														<div class="form-group">
															<label class="col-md-2 control-label">${Location_Wizard_Maintenance_Level.getKP_Location_Profile()}:
																<span class="required">*</span>
															</label>

															<div class="col-md-10">
																<c:set var="StrLocationType"
																	value="${SelectLocationType}" />
																<select
																	class="table-group-action-input form-control input-medium"
																	name="LocationProfile" id="LOCATION_PROFILE">
																	<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																	<c:forEach items="${Drop_Down_Location_Profile}"
																		var="LocationProfile">
																		<option
																			value='${LocationProfile.getLocationProfileId().getProfile()}'>${LocationProfile.getDescription20()}</option>
																	</c:forEach>
																	
																</select> <span class="error" id="Location_ProfileErrorMessage"></span>
															</div>
														</div>
														<div class="form-group">
															<span class="error error-Table" id="TableErrorMessage"></span>
														</div>
														<div class="form-group">
															<table border="0" id="grid">
																<tr >
																	<th >${Location_Wizard_Maintenance_Level.getKP_Type()}</th>
																	<th >${Location_Wizard_Maintenance_Level.getKP_Row_Number_of_Characters()}
																	</th>
																	<th >${Location_Wizard_Maintenance_Level.getKP_Row_Characters_Set()}
																	</th>
																	<th >${Location_Wizard_Maintenance_Level.getKP_Row_Starting_at()}</th>
																	<th >${Location_Wizard_Maintenance_Level.getKP_Number_of_Row()}</th>
																	<th >${Location_Wizard_Maintenance_Level.getKP_Separator_Character_Location()}
																	</th>
																	<th >${Location_Wizard_Maintenance_Level.getKP_Separator_Character_Location_Print_only()}
																	</th>
																</tr>



																<tr  >
																	<td   >${Location_Wizard_Maintenance_Level.getKP_Row()}</td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="1" name="RowNumberCharacters"
																		id="ROW_NUMBER_CHARACTERS" 
																		value="${Location_Wizard_Searched_Data.getRowNumberCharacters()}"
																		onBlur="fieldsDesabled()" onclick="removeDisable('ROW_CHARACTER_SET')"></td>
																	<td ><select path="ROW_CHARACTER_SET"
																		class="input-table adjust" name="RowCharacterSet"
																		id="ROW_CHARACTER_SET">
																			<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																			<option value="Alpha">Alpha</option>
																			<option value="Numeric">Numeric</option>

																	</select></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="RowStarting" id="ROW_STARTING"
																		value="${Location_Wizard_Searched_Data.getRowStarting()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="RowNumberOf" id="ROW_NUMBER_OF"
																		value="${Location_Wizard_Searched_Data.getRowNumberOf()}"></td>
																	<td ><input type="text" name="RowSeperator"
																		class="input-table adjust" id="RowSeperator" maxlength="1"
																		disabled="disabled" /></td>
																	<td ><input type="text" name="RowSeperatorPrint"
																		class="input-table adjust" id="RowSeperatorPrint" maxlength="1"
																		disabled="disabled" /></td>

																</tr>
																<tr >
																	<td >${Location_Wizard_Maintenance_Level.getKP_Aisle()}</td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="1" name="AisleNumberCharacters"
																		id="AISLE_NUMBER_CHARACTERS"
																		value="${Location_Wizard_Searched_Data.getAisleNumberCharacters()}"
																		onBlur="fieldsDesabled()" onclick="removeDisable('AISLE_CHARACTER_SET')"></td>
																	<td ><select class="input-table adjust"
																		name="AisleCharacterSet" id="AISLE_CHARACTER_SET">
																			<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																			<option value="Alpha">Alpha</option>
																			<option value="Numeric">Numeric</option>

																	</select></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="AisleStarting" id="AISLE_STARTING"
																		value="${Location_Wizard_Searched_Data.getAisleStarting()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="AisleNumberOf"
																		id="AISLE_NUMBER_OF"
																		value="${Location_Wizard_Searched_Data.getAisleNumberOf()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorRowAisle"
																		id="SEPARATOR_ROW_AISLE" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorRowAisle()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorRowAislePrint"
																		id="SEPARATOR_ROW_AISLE_PRINT" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorRowAislePrint()}"></td>
																</tr>
																<tr >
																	<td  >${Location_Wizard_Maintenance_Level.getKP_Bay()}</td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="1" name="BayNumberCharacters"
																		id="BAY_NUMBER_CHARACTERS"
																		value="${Location_Wizard_Searched_Data.getBayNumberCharacters()}"
																		onBlur="fieldsDesabled()" onclick="removeDisable('BAY_CHARACTER_SET')"></td>
																	<td ><select class="input-table adjust"
																		name="BayCharacterSet" id="BAY_CHARACTER_SET">
																			<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																			<option value="Alpha">Alpha</option>
																			<option value="Numeric">Numeric</option>


																	</select></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="BayStarting" id="BAY_STARTING"
																		value="${Location_Wizard_Searched_Data.getBayStarting()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="BayNumberOf" id="BAY_NUMBER_OF"
																		value="${Location_Wizard_Searched_Data.getBayNumberOf()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorAisleBay"
																		id="SEPARATOR_AISLE_BAY" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorAisleBay()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorAislebayPrint"
																		id="SEPARATOR_AISLE_BAY_PRINT" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorAislebayPrint()}"></td>
																</tr>
																<tr >
																	<td >${Location_Wizard_Maintenance_Level.getKP_Level()}</td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="1" name="LevelNumberCharacters"
																		id="LEVEL_NUMBER_CHARACTERS"
																		value="${Location_Wizard_Searched_Data.getLevelNumberCharacters()}"
																		onBlur="fieldsDesabled()" onclick="removeDisable('LEVEL_CHARACTER_SET')"></td>
																	<td ><select class="input-table adjust"
																		name="LevelCharacterSet" id="LEVEL_CHARACTER_SET">
																			<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																			<option value="Alpha">Alpha</option>
																			<option value="Numeric">Numeric</option>

																	</select></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="LevelStarting"
																		id="LEVEL_STARTING"
																		value="${Location_Wizard_Searched_Data.getLevelStarting()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="LevelNumberOf"
																		id="LEVEL_NUMBER_OF"
																		value="${Location_Wizard_Searched_Data.getLevelNumberOf()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorBayLevel"
																		id="SEPARATOR_BAY_LEVEL" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorBayLevel()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorBaylevelPrint"
																		id="SEPARATOR_BAY_LEVEL_PRINT" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorBaylevelPrint()}"></td>
																</tr>
																<tr >
																	<td >${Location_Wizard_Maintenance_Level.getKP_Slot()}</td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="1" name="SlotNumberCharacters"
																		id="SLOT_NUMBER_CHARACTERS"
																		value="${Location_Wizard_Searched_Data.getSlotNumberCharacters()}"
																		onBlur="fieldsDesabled()" onclick="removeDisable('SLOT_CHARACTER_SET')"></td>
																	<td ><select class="input-table adjust"
																		name="SlotCharacterSet" id="SLOT_CHARACTER_SET">
																			<option value="">${Location_Wizard_Maintenance_Level.getKP_Select()}...</option>
																			<option value="Alpha">Alpha</option>
																			<option value="Numeric">Numeric</option>

																	</select></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="SlotStarting" id="SLOT_STARTING"
																		value="${Location_Wizard_Searched_Data.getSlotStarting()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="5" name="SlotNumberOf" id="SLOT_NUMBER_OF"
																		value="${Location_Wizard_Searched_Data.getSlotNumberOf()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorLevelSlot"
																		id="SEPARATOR_LEVEL_SLOT" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorLevelSlot()}"></td>
																	<td ><input type="text" class="input-table adjust"
																		maxlength="20" name="SeparatorLevelSlotPrint"
																		id="SEPARATOR_LEVEL_SLOT_PRINT" maxlength="1"
																		value="${Location_Wizard_Searched_Data.getSeparatorLevelSlotPrint()}"></td>
																</tr>

															</table>
														</div>

														<div
															class="margin-bottom-5-right-allign_icon_maintenanace Save-Cancel Maintenance_button">

															<button
																class="btn btn-sm yellow filter-submit margin-bottom button_all"
																onclick="actionForm('Location_Wizard_Maintenance','save');">
																<i class="fa fa-check"></i>${Location_Wizard_Maintenance_Level.getKP_Save_Update()}
															</button>
																<button
																class="btn btn-sm red filter-submit margin-bottom button_all "
																onclick="return resetPage();">
																<i class="fa fa-times"></i>${Location_Wizard_Maintenance_Level.getKP_Reset()}
															</button>
															<button
																class="btn btn-sm red filter-submit margin-bottom button_all"
																onclick="clearBtn();">
																<i class="fa fa-times"></i>${Location_Wizard_Maintenance_Level.getKP_Cancel()}
															</button>





														</div>


													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</form>

						</div>
						<!--end tabbable-->

						<!--end tabbable-->

					</div>

					<!-- END PAGE CONTENT INNER -->
				</div>
			</div>
			<!-- END PAGE CONTENT -->

		</div>
		<!-- pop up code -->
		<div class="cover back-cover"></div>

		<div id="show" class="Show_main show-main-div-popup-wizard"
			style="display: none">
			<div class="form-body">
				<div id="DivLabelCompany" class="form-group">
					<label class="col-md-2  wizard-location-width" id="LabelCompany"><b>&nbsp;&nbsp;&nbsp;</b>&nbsp;<span
						id="Label"></span></label>
					<hr id="borderline"></hr>
				</div>
			</div>
			<br /> <br /> <br />
			<div class="form-body" id="grid-form">
				<div class="form-group">
					<div id="ErrorMessageGrid" class="note note-danger"
						style="display: none;">
						<p id="MessageRestFull" class="error err-modified"></p>
					</div>
					<div class="grid-setting">

						<div class="form-group">
							<label class="col-md-4 control-label">${Location_Wizard_Maintenance_Level.getKP_Number_of_locations_created()}: </label>
							<div class="col-md-8">
							 <input
								type="text" class="form-controlwidth" id="SavedRecord"
								readonly="readonly" />
								</div>
						</div>
						<br /> <br /> <br />
						<div class="form-group">
							<label class="col-md-4 control-label"> ${Location_Wizard_Maintenance_Level.getKP_Number_of_duplicate_locations()}:
							</label> 
							<div class="col-md-8">
							<input type="text" class="form-controlwidth"
								id="DuplicateRecord" readonly="readonly" />
								</div>
						</div>
						<br /> <br />
						<div class="show form-group " id="duplicateRecordDiv" style="display: none">
						<label class="col-md-4 control-label" style="visibility: hidden;" id="lebelDuplecates"> Duplicate Locations:
							</label>   
							<div class="col-md-8">
							<textarea readonly="readonly" cols="57" rows="4" id="DuplicateRecords" style="visibility: hidden;"
							class="text_Area">
							
										</textarea>
										</div> 
							<!-- <input type="text" class="form-controlwidth"
								id="DuplicateRecords" readonly="readonly" /> -->
						</div>
						<br /> <br />
						
						<div class="form-group popupbutton popup-bottom-button">
								 <button class="btn btn-sm green  margin-bottom " onclick="" style="visibility: hidden;"
									id="download">
									<i class="fa fa-check"></i> ${Location_Wizard_Maintenance_Level.getKP_Download()}
								</button>
								<button class="btn btn-sm yellow filter-submit margin-bottom popbutton popup_button_margin"
									onclick="okBtn();" id="popup">
									<i class="fa fa-times"></i>${Location_Wizard_Maintenance_Level.getKP_Ok()}
								</button>
							</div>
						
					</div>

				</div>
			</div>

		</div>

<div id="loadingDiv" class="show-main-div-popup-loarder">

<img alt="" src="resourcesValidate/css/loading.gif" class="loader"/>


</div>

	</tiles:putAttribute>

</tiles:insertDefinition>
<%-- <script src="<c:url value="/resourcesValidate/bootstrap/js/bootstrap.js"/>"

	type="text/javascript"></script>
	
	<script src="<c:url value="/resourcesValidate/bootstrap/js/bootstrap.min.js"/>"

	type="text/javascript"></script>
	
	<link type="text/css"
	href="<c:url value="/resourcesValidate/bootstrap/css/bootstrap-responsive.css"/>"
	rel="stylesheet" />
	
	<link type="text/css"
	href="<c:url value="/resourcesValidate/bootstrap/css/bootstrap-responsive.min.css"/>"
	rel="stylesheet" />
	
	<link type="text/css"
	href="<c:url value="/resourcesValidate/bootstrap/css/bootstrap.css"/>"
	rel="stylesheet" />
	
	<link type="text/css"
	href="<c:url value="/resourcesValidate/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" />
 --%>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<!-- This is used to Set Custom Css that created by Pradeep Kumar for wizard Location's all Screens -->
<link type="text/css"
	href="<c:url value="/resourcesValidate/css/WizardLocation.css"/>"
	rel="stylesheet" />
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/jquery-1.8.2.js"/>"
	type="text/javascript"></script>
<script
	src="<c:url value="/resourcesValidate/jquery.ajaxfileupload.js"/>"
	type="text/javascript"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
	type="text/javascript"></script>
<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
	type="text/javascript"></script>
<script>

function resetPage(){
    
    window.parent.location = window.parent.location.href;
   }

$('#download').click(
		   function() {
		    var content = $('#DuplicateRecords').val();
		    $.ajax({
		     url : '${pageContext.request.contextPath}/DownloadLocFile',
		     type : 'GET',
		     cache : false,
		     data:{DuplicateLocPath:content},
		     success : function(data1) {
		      /* var dl = document.createElement('a');
		         dl.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(data1)); */
		         var splitloc = content.split("/");
		        // dl.setAttribute('download', splitloc[splitloc.length-1]);
		         //dl.click();
		         
		         tryCatch(splitloc[splitloc.length-1],data1)
		      
		     }
		    });
		   });
		 
		 
		 //for downloading file

		 function saveTextAsFile(fileName,data)  
		 {      
		 //var textToWrite = document.getElementById("inputTextToSave").value;
		 var textFileAsBlob = new Blob([data], {type:'text/plain'});
		 var fileNameToSaveAs = fileName+".txt";
		 var downloadLink = document.createElement("a");
		 downloadLink.download = fileNameToSaveAs;
		 downloadLink.innerHTML = "My Hidden Link";
		 window.URL = window.URL || window.webkitURL || window.saveBlob || window.msSaveBlob || window.mozSaveBlob ||window.webkitSaveBlob;
		    
		 downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		 downloadLink.onclick = destroyClickedElement;
		 downloadLink.style.display = "none";
		 document.body.appendChild(downloadLink);
		 downloadLink.click();
		 }
		 function destroyClickedElement(event)
		 {
		 // remove the link from the DOM
		  document.body.removeChild(event.target);
		 }
		 function saveCanvas(fileName,data) {
		 var textFileAsBlob = new Blob([data], {type:'text/plain'});
		 //var blobObject = new Blob( [canvas.toDataURL()] );
		 window.navigator.msSaveBlob(textFileAsBlob, fileName+".txt");
		 document.getElementById('hideWrapper').style.display = 'none';
		 }

		 function tryCatch(fileName,data){
		 try{
		   //alert("Welcome guest!");
		   saveCanvas(fileName,data);

		  
		 }catch(err ){

		 saveTextAsFile(fileName,data);

		  document.getElementById("save").innerHTML = err.message;
		 }
		 }



function readSingleFile(f) {

	jQuery.get(f,function(data){
		  alert(data);
		});
    if (f) {
      var r = new FileReader();
      r.onload = function(e) { 
       var contents = e.target.result;
        alert( "Got the file.n" 
              +"name: " + f.name + "n"
              +"type: " + f.type + "n"
              +"size: " + f.size + " bytesn"
              + "starts with: " + contents.substr(1, contents.indexOf("n"))
        ); 
        
        return contents;
      }
     
      // r.readAsText(f);
    } else { 
      alert("Failed to load file");
    }
   
  }

function removeDisable(dropdownid)
{
	var x = document.getElementById(dropdownid).removeAttribute('disabled');
	}

function locationProfileDropdown(){
	 
	  var firstDropVal = $('#FULFILLMENT_CENTER_ID').val().replace(/&quot/g,"\"");
	
	  if(firstDropVal != "")
	  {
	$.get("${pageContext.request.contextPath}/RestSelectFulfillmentCenterValueWizard",
			{
			Fullfilment_Center : firstDropVal
				
			},
			function(data, status) {
				if (data) {
					 $('#LOCATION_PROFILE').empty();
					  $('#LOCATION_PROFILE').each(function() {
			           // Create option
			           var option = $("<option />");
			           option.attr("value", '').text('Select..');
			           $('#LOCATION_PROFILE').append(option);
			           }); 
					 if(data==''){
								 var errMsgId = document
									.getElementById("MessageRestFull");
							 errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${Location_Wizard_Maintenance_Level.getKP_There_is_no_location_profile_for_selected_fulfillment_center()}';
							//document.location.href = '#top';
							window.scrollTo(0,0);
							$('#ErrorMessage').show();
							IsSubmit = false;
							return IsSubmit;		 
					 }else
						 {
						$('#ErrorMessage').hide();
					 
			      	
			          //$('#ApplicationSub').append( new Option("Select...",""));
			          for (var subApplication in data){
			        	
			           $('#LOCATION_PROFILE').append( new Option(data[subApplication].description20,data[subApplication].locationProfileId.profile));
			          }
					}
				} else {			
														
				}
			});
}
	  $('#ErrorMessage').hide();
}

$("#FULFILLMENT_CENTER_ID").change(function () {


locationProfileDropdown();

});

	fieldsDesabled();
	function fieldsDesabled() {
		var StrRowValue = $('#ROW_NUMBER_CHARACTERS').val();
		if (StrRowValue) {
			$("#ROW_STARTING").attr("maxlength", $('#ROW_NUMBER_CHARACTERS').val());
			$("#ROW_NUMBER_OF").attr("maxlength", $('#ROW_NUMBER_CHARACTERS').val());
			$("#ROW_STARTING").attr("disabled", false);
			$("#ROW_CHARACTER_SET").attr("disabled", false);
			$("#ROW_NUMBER_OF").attr("disabled", false);
		}
		if (!StrRowValue) {
			$("#ROW_STARTING").val('');
			$("#ROW_CHARACTER_SET").val('');
			$("#ROW_NUMBER_OF").val('');
			$("#ROW_STARTING").attr("disabled", true);
			$("#ROW_CHARACTER_SET").attr("disabled", true);
			$("#ROW_NUMBER_OF").attr("disabled", true);

		}

		var StrAliceValue = $('#AISLE_NUMBER_CHARACTERS').val();
		if (StrAliceValue) {
			$("#AISLE_STARTING").attr("maxlength", $('#AISLE_NUMBER_CHARACTERS').val());
			$("#AISLE_NUMBER_OF").attr("maxlength", $('#AISLE_NUMBER_CHARACTERS').val());
			$("#SEPARATOR_ROW_AISLE_PRINT").attr("maxlength", "1");
			$("#SEPARATOR_ROW_AISLE").attr("maxlength", "1");
			$("#AISLE_CHARACTER_SET").attr("disabled", false);
			$("#AISLE_STARTING").attr("disabled", false);
			$("#AISLE_NUMBER_OF").attr("disabled", false);
			$("#SEPARATOR_ROW_AISLE").attr("disabled", false);
			$("#SEPARATOR_ROW_AISLE_PRINT").attr("disabled", false);
		}
		if (!StrAliceValue) {

			$("#AISLE_CHARACTER_SET").val('');
			$("#AISLE_STARTING").val('');
			$("#AISLE_NUMBER_OF").val('');
			$("#SEPARATOR_ROW_AISLE").val('');
			$("#SEPARATOR_ROW_AISLE_PRINT").val('');

			$("#AISLE_CHARACTER_SET").attr("disabled", true);
			$("#AISLE_STARTING").attr("disabled", true);
			$("#AISLE_NUMBER_OF").attr("disabled", true);
			$("#SEPARATOR_ROW_AISLE").attr("disabled", true);
			$("#SEPARATOR_ROW_AISLE_PRINT").attr("disabled", true);

		}

		var StrBayValue = $('#BAY_NUMBER_CHARACTERS').val();
		if (StrBayValue) {
			$("#BAY_STARTING").attr("maxlength", $('#BAY_NUMBER_CHARACTERS').val());
			$("#BAY_NUMBER_OF").attr("maxlength", $('#BAY_NUMBER_CHARACTERS').val());
			
			$("#SEPARATOR_AISLE_BAY_PRINT").attr("maxlength", "1");
			$("#SEPARATOR_AISLE_BAY").attr("maxlength", "1");
			
			$("#BAY_CHARACTER_SET").attr("disabled", false);
			$("#BAY_STARTING").attr("disabled", false);
			$("#BAY_NUMBER_OF").attr("disabled", false);
			$("#SEPARATOR_AISLE_BAY").attr("disabled", false);
			$("#SEPARATOR_AISLE_BAY_PRINT").attr("disabled", false);
		}
		if (!StrBayValue) {
			$("#BAY_CHARACTER_SET").val('');
			$("#BAY_STARTING").val('');
			$("#BAY_NUMBER_OF").val('');
			$("#SEPARATOR_AISLE_BAY").val('');
			$("#SEPARATOR_AISLE_BAY_PRINT").val('');

			$("#BAY_CHARACTER_SET").attr("disabled", true);
			$("#BAY_STARTING").attr("disabled", true);
			$("#BAY_NUMBER_OF").attr("disabled", true);
			$("#SEPARATOR_AISLE_BAY").attr("disabled", true);
			$("#SEPARATOR_AISLE_BAY_PRINT").attr("disabled", true);

		}

		var StrLevelValue = $('#LEVEL_NUMBER_CHARACTERS').val();
		if (StrLevelValue) {
			$("#LEVEL_STARTING").attr("maxlength", $('#LEVEL_NUMBER_CHARACTERS').val());
			$("#LEVEL_NUMBER_OF").attr("maxlength", $('#LEVEL_NUMBER_CHARACTERS').val());
			
			$("#SEPARATOR_BAY_LEVEL_PRINT").attr("maxlength", "1");
			$("#SEPARATOR_BAY_LEVEL").attr("maxlength", "1");
			
			$("#LEVEL_CHARACTER_SET").attr("disabled", false);
			$("#LEVEL_STARTING").attr("disabled", false);
			$("#LEVEL_NUMBER_OF").attr("disabled", false);
			$("#SEPARATOR_BAY_LEVEL").attr("disabled", false);
			$("#SEPARATOR_BAY_LEVEL_PRINT").attr("disabled", false);
		}
		if (!StrLevelValue) {
			$("#LEVEL_CHARACTER_SET").val('');
			$("#LEVEL_STARTING").val('');
			$("#LEVEL_NUMBER_OF").val('');
			$("#SEPARATOR_BAY_LEVEL").val('');
			$("#SEPARATOR_BAY_LEVEL_PRINT").val('');

			$("#LEVEL_CHARACTER_SET").attr("disabled", true);
			$("#LEVEL_STARTING").attr("disabled", true);
			$("#LEVEL_NUMBER_OF").attr("disabled", true);
			$("#SEPARATOR_BAY_LEVEL").attr("disabled", true);
			$("#SEPARATOR_BAY_LEVEL_PRINT").attr("disabled", true);

		}

		var StrSlotValue = $('#SLOT_NUMBER_CHARACTERS').val();
		if (StrSlotValue) {
			$("#SLOT_STARTING").attr("maxlength", $('#SLOT_NUMBER_CHARACTERS').val());
			$("#SLOT_NUMBER_OF").attr("maxlength", $('#SLOT_NUMBER_CHARACTERS').val());
			
			$("#SEPARATOR_LEVEL_SLOT_PRINT").attr("maxlength", "1");
			$("#SEPARATOR_LEVEL_SLOT").attr("maxlength", "1");
			
			$("#SLOT_CHARACTER_SET").attr("disabled", false);
			$("#SLOT_STARTING").attr("disabled", false);
			$("#SLOT_NUMBER_OF").attr("disabled", false);
			$("#SEPARATOR_LEVEL_SLOT").attr("disabled", false);
			$("#SEPARATOR_LEVEL_SLOT_PRINT").attr("disabled", false);
		}
		if (!StrSlotValue) {
			$("#SLOT_CHARACTER_SET").val('');
			$("#SLOT_STARTING").val('');
			$("#SLOT_NUMBER_OF").val('');
			$("#SEPARATOR_LEVEL_SLOT").val('');
			$("#SEPARATOR_LEVEL_SLOT_PRINT").val('');

			$("#SLOT_CHARACTER_SET").attr("disabled", true);
			$("#SLOT_STARTING").attr("disabled", true);
			$("#SLOT_NUMBER_OF").attr("disabled", true);
			$("#SEPARATOR_LEVEL_SLOT").attr("disabled", true);
			$("#SEPARATOR_LEVEL_SLOT_PRINT").attr("disabled", true);

		}
	}
	jQuery(document)
			.ready(
					function() {
						
						 
						$('#loadingDiv').hide();
						
						fieldsDesabled();
						
						//	$('#LAST_ACTIVITY_DATE').val(getLocalDate());
						$('#DATE_CREATED').val(getLocalDate());
						$('#DATE_CREATED1').val(getUTCDateTime());
						$('#LAST_ACTIVITY_DATE1').val(getUTCDateTime());

						$("#grid").kendoGrid({

							sortable : true,
							resizable : true,
							reorderable : true,

						});
						setCheckbox();
						function setCheckbox() {

							//$("#AREA").attr("readonly", "true");
							//$("#AREA").css('pointer-events', 'none');

							var currentAREAValue = "${Location_Wizard_Searched_Data.getArea()}";
					//		var currentFULFILLMENT_CENTER_IDValue = "${Location_Wizard_Searched_Data.getFulfillmentCenterId()}";
							var currentWIZARD_IDValue = "${Location_Wizard_Searched_Data.getWizardId()}";
							var currentWORK_ZONEValue = "${Location_Wizard_Searched_Data.getWorkZone()}";
							var currentWORK_GROUP_ZONEValue = "${Location_Wizard_Searched_Data.getWorkGroupZone()}";
							var currentLOCATION_TYPEValue = "${Location_Wizard_Searched_Data.getLocationType()}";
						//	var currentLOCATION_PROFILEValue = "${Location_Wizard_Searched_Data.getLocationProfile()}";

							var currentRowCharacterSetValue = "${Location_Wizard_Searched_Data.getRowCharacterSet()}";
							var currentAisleCharacterSetValue = "${Location_Wizard_Searched_Data.getAisleCharacterSet()}";
							var currentBayCharacterSetValue = "${Location_Wizard_Searched_Data.getBayCharacterSet()}";
							var currentLevelCharacterSetValue = "${Location_Wizard_Searched_Data.getLevelCharacterSet()}";
							var currentSlotCharacterSetValue = "${Location_Wizard_Searched_Data.getSlotCharacterSet()}";

							/*  alert(currentAREAValue);
							alert(currentFULFILLMENT_CENTER_IDValue);
							alert(currentWIZARD_IDValue);
							alert(currentWORK_ZONEValue);
							alert(currentWORK_GROUP_ZONEValue);
							alert(currentLOCATION_TYPEValue);
							alert(currentLOCATION_PROFILEValue);  */

							/*  alert(currentRowCharacterSetValue);
							alert(currentAisleCharacterSetValue);
							alert(currentBayCharacterSetValue);
							alert(currentLevelCharacterSetValue);
							alert(currentSlotCharacterSetValue); */

							if (currentAREAValue.trim() != '') {
								document.getElementById('AREA').value = currentAREAValue;
							}
							/* if (currentAREAValue.trim() != '') {
								document
										.getElementById('FULFILLMENT_CENTER_ID').value = currentFULFILLMENT_CENTER_IDValue;
							} */
							if (currentAREAValue.trim() != '') {
								document.getElementById('WIZARD_ID').value = currentWIZARD_IDValue;
							}
							if (currentAREAValue.trim() != '') {
								document.getElementById('WORK_ZONE').value = currentWORK_ZONEValue;
							}
							if (currentAREAValue.trim() != '') {
								document.getElementById('WORK_GROUP_ZONE').value = currentWORK_GROUP_ZONEValue;
							}
							if (currentAREAValue.trim() != '') {
								document.getElementById('LOCATION_TYPE').value = currentLOCATION_TYPEValue;
							}
							/* if (currentAREAValue.trim() != '') {
								document.getElementById('LOCATION_PROFILE').value = currentLOCATION_PROFILEValue;
							} */
							//for location
							if (currentRowCharacterSetValue.trim() != '') {
								document.getElementById('ROW_CHARACTER_SET').value = currentRowCharacterSetValue;
							}
							if (currentAisleCharacterSetValue.trim() != '') {
								document.getElementById('AISLE_CHARACTER_SET').value = currentAisleCharacterSetValue;
							}
							if (currentBayCharacterSetValue.trim() != '') {
								document.getElementById('BAY_CHARACTER_SET').value = currentBayCharacterSetValue;
							}
							if (currentLevelCharacterSetValue.trim() != '') {
								document.getElementById('LEVEL_CHARACTER_SET').value = currentLevelCharacterSetValue;
							}
							if (currentSlotCharacterSetValue.trim() != '') {
								document.getElementById('SLOT_CHARACTER_SET').value = currentSlotCharacterSetValue;
							} else {

							}
						}

						var status = $('#STATUS').val();
						status = status.toUpperCase();
						if ('APPLIED' != status) {
							$('#AppliedDate').addClass('hidden');
							$('#AppliedBy').addClass('hidden');
						}
						$(window).keydown(function(event) {
							if (event.keyCode == 13) {
								event.preventDefault();
								return false;
							}
						});

						setInterval(
								function() {

									var h = window.innerHeight;
									if (window.innerHeight >= 900
											|| window.innerHeight == 1004) {
										h = h - 187;
									} else {
										h = h - 239;
									}
									document.getElementById("page-content").style.minHeight = h
											+ "px";

								}, 30);

					});

	function headerChangeLanguage() {

		/* alert('in security look up'); */

		$.ajax({
			url : '${pageContext.request.contextPath}/HeaderLanguageChange',
			type : 'GET',
			cache : false,
			success : function(data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});

	}

	function headerInfoHelp() {

		//alert('in security look up');

		/* $
		.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
		 */
		$.post("${pageContext.request.contextPath}/HeaderInfoHelp", {
			InfoHelp : 'LocationWizard',
			InfoHelpType : 'PROGRAM'

		}, function(data1, status) {
			if (data1.boolStatus) {
				window.open(data1.strMessage);

			} else {
				//  alert('No help found yet');
			}

		});

	}
</script>
<script>
	/* 	var isSubmit = false;

	 function isformSubmit() {

	 return isSubmit;
	 } */

	function actionForm(url, action) {

		var isTrue = false;
		if (action == 'save') {

			var isBlankWizardControlNo = isBlankField(
					'WIZARD_CONTROL_NUMBER',
					'WIZARDControlNoErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankWorkGroupZone = isBlankField(
					'WORK_GROUP_ZONE',
					'WorkGroupErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankWizardid = isBlankField(
					'WIZARD_ID',
					'WIZARD_IDErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankAREA = isBlankField(
					'AREA',
					'AreaErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankWORK_ZONE = isBlankField(
					'WORK_ZONE',
					'WorkZoneErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankLOCATION_TYPE = isBlankField(
					'LOCATION_TYPE',
					'Location_TypeErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankLOCATION_PROFILE = isBlankField(
					'LOCATION_PROFILE',
					'Location_ProfileErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankDescription20 = isBlankField(
					'DESCRIPTION20',
					'ErrorMessageshortId',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankDescription50 = isBlankField(
					'DESCRIPTION50',
					'LongErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isBlankFulfillment = isBlankField(
					'FULFILLMENT_CENTER_ID',
					'FulfillmentNoErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Mandatory_field_cannot_be_left_blank()}');

			var isNumericSLOT_NUMBER_CHARACTERS = false;
			var isNumericLEVEL_NUMBER_CHARACTERS = false;
			var isNumericBAY_NUMBER_CHARACTERS = false;
			var isNumericAISLE_NUMBER_CHARACTERS = false;
			var isNumericROW_NUMBER_CHARACTERS = false;

			isNumericROW_NUMBER_CHARACTERS = isNumericGreaterZeroLessFive(
					'ROW_NUMBER_CHARACTERS',
					'TableErrorMessage',
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
							+ '${Location_Wizard_Maintenance_Level.getKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only()}');
			if (isNumericROW_NUMBER_CHARACTERS) {

				isNumericAISLE_NUMBER_CHARACTERS = isNumericGreaterZeroLessFive(
						'AISLE_NUMBER_CHARACTERS',
						'TableErrorMessage',
						'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
								+ '${Location_Wizard_Maintenance_Level.getKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only()}');

				if (isNumericAISLE_NUMBER_CHARACTERS) {

					isNumericBAY_NUMBER_CHARACTERS = isNumericGreaterZeroLessFive(
							'BAY_NUMBER_CHARACTERS',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only()}');
					if (isNumericBAY_NUMBER_CHARACTERS) {

						isNumericLEVEL_NUMBER_CHARACTERS = isNumericGreaterZeroLessFive(
								'LEVEL_NUMBER_CHARACTERS',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only()}');

						if (isNumericLEVEL_NUMBER_CHARACTERS) {

							isNumericSLOT_NUMBER_CHARACTERS = isNumericGreaterZeroLessFive(
									'SLOT_NUMBER_CHARACTERS',
									'TableErrorMessage',
									'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only()}');
						}
					}

				}
			}

			
			
			if( isNumericROW_NUMBER_CHARACTERS
					&& isNumericAISLE_NUMBER_CHARACTERS
					&& isNumericBAY_NUMBER_CHARACTERS
					&& isNumericLEVEL_NUMBER_CHARACTERS
					&& isNumericSLOT_NUMBER_CHARACTERS
					&& isBlankWizardid  && isBlankAREA
					&& isBlankWizardControlNo && isBlankWORK_ZONE
					&& isBlankWorkGroupZone && isBlankLOCATION_TYPE
					&& isBlankFulfillment && isBlankLOCATION_PROFILE
					&& isBlankDescription20 && isBlankDescription50)
				{
			var isNumericRow_STARTING=true;
			var isNumericRow_NUMBER_OF=true;
			var isAlphaRow_STARTING=true;
			var isAlphaRow_NUMBER_OF=true;

			var isNumericAISLE_STARTING=true;
			var isNumericAISLE_NUMBER_OF=true;
			var isAlphaAISLE_STARTING=true;
			var isAlphaAISLE_NUMBER_OF =true;

			var isNumericBAY_STARTING=true;
			var isNumericBAY_NUMBER_OF=true;
			var isAlphaBAY_STARTING=true;
			var isAlphaBAY_NUMBER_OF=true;

			var isNumericLEVEL_STARTING=true;
			var isNumericLEVEL_NUMBER_OF=true;
			var isAlphaLEVEL_STARTING=true;
			var isAlphaLEVEL_NUMBER_OF=true;

			var isNumericSLOT_STARTING=true;
			var isNumericSLOT_NUMBER_OF=true;
			var isAlphaSLOT_STARTING=true;
			var isAlphaSLOT_NUMBER_OF=true;

		
			 
				if ($('#ROW_CHARACTER_SET').val() == 'Numeric') {
					 isNumericRow_STARTING = isNumericGreaterZero(
							'ROW_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					if (isNumericRow_STARTING) {
						isNumericRow_NUMBER_OF = isNumericGreaterZero(
								'ROW_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					}
				}
				else if($('#ROW_CHARACTER_SET').val() == 'Alpha') {
					isAlphaRow_STARTING = allLetter(
							'ROW_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					if (isAlphaRow_STARTING) {
						 isAlphaRow_NUMBER_OF = allLetter(
								'ROW_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					}
				}
				
				if(isAlphaRow_NUMBER_OF
						&& isNumericRow_STARTING
						&& isAlphaRow_STARTING
						&& isNumericRow_NUMBER_OF)
					{
				if ($('#AISLE_CHARACTER_SET').val() == 'Numeric') {
					 isNumericAISLE_STARTING = isNumericGreaterZero(
							'AISLE_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					if (isNumericAISLE_STARTING) {
						 isNumericAISLE_NUMBER_OF = isNumericGreaterZero(
								'AISLE_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					}
				}
				
				else if($('#AISLE_CHARACTER_SET').val() == 'Alpha') {
					isAlphaAISLE_STARTING = allLetter(
							'AISLE_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					if (isAlphaAISLE_STARTING) {
						 isAlphaAISLE_NUMBER_OF = allLetter(
								'AISLE_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					}
				}
					}
				
				
				if(isAlphaRow_NUMBER_OF
						&& isNumericRow_STARTING
						&& isAlphaRow_STARTING
						&& isNumericRow_NUMBER_OF

						&& isNumericAISLE_STARTING
						&& isNumericAISLE_NUMBER_OF
						&& isAlphaAISLE_STARTING
						&& isAlphaAISLE_NUMBER_OF)
					{
				if ($('#BAY_CHARACTER_SET').val() == 'Numeric') {
					 isNumericBAY_STARTING = isNumericGreaterZero(
							'BAY_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					if (isNumericBAY_STARTING) {
						 isNumericBAY_NUMBER_OF = isNumericGreaterZero(
								'BAY_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					}
				}
				else if($('#BAY_CHARACTER_SET').val() == 'Alpha'){
					isAlphaBAY_STARTING = allLetter(
							'BAY_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					if (isAlphaBAY_STARTING) {
						 isAlphaBAY_NUMBER_OF = allLetter(
								'BAY_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					}
				}
					}
				
				if(isAlphaRow_NUMBER_OF
						&& isNumericRow_STARTING
						&& isAlphaRow_STARTING
						&& isNumericRow_NUMBER_OF

						&& isNumericAISLE_STARTING
						&& isNumericAISLE_NUMBER_OF
						&& isAlphaAISLE_STARTING
						&& isAlphaAISLE_NUMBER_OF

						&& isNumericBAY_STARTING
						&& isNumericBAY_NUMBER_OF
						&& isAlphaBAY_STARTING
						&& isAlphaBAY_NUMBER_OF)
					{
				if ($('#LEVEL_CHARACTER_SET').val() == 'Numeric') {
					 isNumericLEVEL_STARTING = isNumericGreaterZero(
							'LEVEL_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					if (isNumericLEVEL_STARTING) {
						 isNumericLEVEL_NUMBER_OF = isNumericGreaterZero(
								'LEVEL_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					}
				}
				
				else if($('#LEVEL_CHARACTER_SET').val() == 'Alpha'){
					 isAlphaLEVEL_STARTING = allLetter(
							'LEVEL_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					if (isAlphaLEVEL_STARTING) {
						isAlphaLEVEL_NUMBER_OF = allLetter(
								'LEVEL_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					}
				}
					}
				if(isAlphaRow_NUMBER_OF
						&& isNumericRow_STARTING
						&& isAlphaRow_STARTING
						&& isNumericRow_NUMBER_OF

						&& isNumericAISLE_STARTING
						&& isNumericAISLE_NUMBER_OF
						&& isAlphaAISLE_STARTING
						&& isAlphaAISLE_NUMBER_OF

						&& isNumericBAY_STARTING
						&& isNumericBAY_NUMBER_OF
						&& isAlphaBAY_STARTING
						&& isAlphaBAY_NUMBER_OF

						&& isNumericLEVEL_STARTING
						&& isNumericLEVEL_NUMBER_OF
						&& isAlphaLEVEL_STARTING
						&& isAlphaLEVEL_NUMBER_OF)
					{
				if ($('#SLOT_CHARACTER_SET').val() == 'Numeric') {
					isNumericSLOT_STARTING = isNumericGreaterZero(
							'SLOT_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					if (isNumericSLOT_STARTING) {
						isNumericSLOT_NUMBER_OF = isNumericGreaterZero(
								'SLOT_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_Allowed_Only_Character_Set()}');
					}
				}
				
				else if($('#SLOT_CHARACTER_SET').val() == 'Alpha'){
					isAlphaSLOT_STARTING = allLetter(
							'SLOT_STARTING',
							'TableErrorMessage',
							'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					if (isAlphaSLOT_STARTING) {
						isAlphaSLOT_NUMBER_OF = allLetter(
								'SLOT_NUMBER_OF',
								'TableErrorMessage',
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Only_Alphabets_Allowed()}');
					}
				}
					}
				if( isNumericRow_STARTING
						&& isNumericRow_NUMBER_OF
						&& isAlphaRow_STARTING
						&& isAlphaRow_NUMBER_OF

						&& isNumericAISLE_STARTING
						&& isNumericAISLE_NUMBER_OF
						&& isAlphaAISLE_STARTING
						&& isAlphaAISLE_NUMBER_OF 

						&& isNumericBAY_STARTING
						&& isNumericBAY_NUMBER_OF
						&& isAlphaBAY_STARTING
						&& isAlphaBAY_STARTING

						&& isNumericLEVEL_STARTING
						&& isNumericLEVEL_NUMBER_OF
						&& isAlphaLEVEL_STARTING
						&& isAlphaLEVEL_NUMBER_OF

						&& isNumericSLOT_STARTING
						&& isNumericSLOT_NUMBER_OF
						&& isAlphaSLOT_STARTING
						&& isAlphaSLOT_NUMBER_OF
						)
					{
					 	isTrue=true;
					}
				else {
					isTrue=false;
				}

				var isGreaterRowStarting=true;
				var isGreaterAisleStarting=true;
				var isGreaterBayStarting=true;
				var isGreaterLevelStarting=true;
				var isGreaterSlotStarting=true;
				
			if (isBlankWizardid  && isBlankAREA
					&& isBlankWizardControlNo && isBlankWORK_ZONE
					&& isBlankWorkGroupZone && isBlankLOCATION_TYPE
					&& isBlankFulfillment && isBlankLOCATION_PROFILE
					&& isBlankDescription20 && isBlankDescription50) {
				

				 if ($('#SLOT_NUMBER_CHARACTERS').val() != '') {
						
						if ($('#SLOT_CHARACTER_SET').val() == '') {
							var errMsgId = document.getElementById("TableErrorMessage");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Slot_character_set()}';
							//$('#ErrorMessage').show();
							isSubmit = false;
							isTrue=false;
						} else if ($('#SLOT_STARTING').val() == '') {
							var errMsgId = document.getElementById("TableErrorMessage");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Slot_starting()}';
							//$('#ErrorMessage').show();
							isSubmit = false;
							isTrue=false;
						} else if ($('#SLOT_NUMBER_OF').val() == '') {
							var errMsgId = document.getElementById("TableErrorMessage");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Slot_number_of()}';
							//$('#ErrorMessage').show();
							isSubmit = false;
							isTrue=false;
						}
						else if ($('#SLOT_NUMBER_OF').val() <= 0) {
							var errMsgId = document.getElementById("TableErrorMessage");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_value_can_not_less_than_Zero_or_eual()}';
							//$('#ErrorMessage').show();
							isSubmit = false;
							isTrue=false;
						}
						/* else if ($('#SEPARATOR_LEVEL_SLOT').val() == '') {
							var errMsgId = document.getElementById("TableErrorMessage");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Level_Slot()}';
							//$('#ErrorMessage').show();
							isSubmit = false;
							isTrue=false;
						}

						else if ($('#SEPARATOR_LEVEL_SLOT_PRINT').val() == '') {
							var errMsgId = document.getElementById("TableErrorMessage");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
									+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Level_Slot_Print()}';
							//$('#ErrorMessage').show();
							isSubmit = false;
							isTrue=false;
						} */
						
					 }
						
				if ($('#LEVEL_NUMBER_CHARACTERS').val() != '') {
								
								if ($('#LEVEL_CHARACTER_SET').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_LEVEL_character_set()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								} else if ($('#LEVEL_STARTING').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_LEVEL_starting()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								} else if ($('#LEVEL_NUMBER_OF').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_LEVEL_number_of()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								}
								else if ($('#LEVEL_NUMBER_OF').val() <= 0) {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_value_can_not_less_than_Zero_or_eual()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								}
								/* else if ($('#SEPARATOR_BAY_LEVEL').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Bay_Level()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								}

								else if ($('#SEPARATOR_BAY_LEVEL_PRINT').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Bay_Level_Print()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								} */
								
								
							}	
					if ($('#BAY_NUMBER_CHARACTERS').val() != '') {
								
								if ($('#BAY_CHARACTER_SET').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Bay_character_set()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								} else if ($('#BAY_STARTING').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Bay_starting()}';
								//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								} else if ($('#BAY_NUMBER_OF').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Bay_number_of()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								}
								else if ($('#BAY_NUMBER_OF').val() <= 0) {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_value_can_not_less_than_Zero_or_eual()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								}
								/* else if ($('#SEPARATOR_AISLE_BAY').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Aisle_Bay()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								}

								else if ($('#SEPARATOR_AISLE_BAY_PRINT').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Aisle_Bay_Print()}';
									//$('#ErrorMessage').show();
									isSubmit = false;
									isTrue=false;
								} */
								
								
							}
					 if ($('#AISLE_NUMBER_CHARACTERS').val() != '') {
							if ($('#AISLE_CHARACTER_SET').val() == '') {
								var errMsgId = document.getElementById("TableErrorMessage");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Aisle_character_set()}';
								//$('#ErrorMessage').show();
								isSubmit = false;
								isTrue=false;
							} else if ($('#AISLE_STARTING').val() == '') {
								var errMsgId = document.getElementById("TableErrorMessage");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Aisle_starting()}';
								//$('#ErrorMessage').show();
								isSubmit = false;
								isTrue=false;
							} else if ($('#AISLE_NUMBER_OF').val() == '') {
								var errMsgId = document.getElementById("TableErrorMessage");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Aisle_number_of()}';
								//$('#ErrorMessage').show();
								isSubmit = false;
								isTrue=false;
							}
							else if ($('#AISLE_NUMBER_OF').val() <= 0) {
								var errMsgId = document.getElementById("TableErrorMessage");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_value_can_not_less_than_Zero_or_eual()}';
								//$('#ErrorMessage').show();
								isSubmit = false;
								isTrue=false;
							}
							/* else if ($('#SEPARATOR_ROW_AISLE').val() == '') {
								var errMsgId = document.getElementById("TableErrorMessage");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Row_Aisle()}';
								//$('#ErrorMessage').show();
								isSubmit = false;
								isTrue=false;
							}

							else if ($('#SEPARATOR_ROW_AISLE_PRINT').val() == '') {
								var errMsgId = document.getElementById("TableErrorMessage");
								errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
										+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Seperator_Row_Aisle_Print()}';
								$('#TableErrorMessage').show();
								isSubmit = false;
								isTrue=false;
							} */
							//isSubmit=true;
						 }
					if ($('#ROW_NUMBER_CHARACTERS').val() != '') {
								
								if ($('#ROW_CHARACTER_SET').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Row_character_set()}';
									//$('#ErrorMessage').show();
									isTrue=false;
									isSubmit = false;
								} else if ($('#ROW_STARTING').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Row_starting()}';
									//$('#ErrorMessage').show();
									isTrue=false;
									isSubmit = false;
								} else if ($('#ROW_NUMBER_OF').val() == '') {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_ERR_Please_Enter_Row_number_of()}';
									//$('#ErrorMessage').show();
									isTrue=false;
									isSubmit = false;
								}
								else if ($('#ROW_NUMBER_OF').val() <= 0) {
									var errMsgId = document.getElementById("TableErrorMessage");
									errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
											+ '${Location_Wizard_Maintenance_Level.getKP_Numeric_value_can_not_less_than_Zero_or_eual()}';
									//$('#ErrorMessage').show();
									isTrue=false;
									isSubmit = false;
								}
								else if (isTrue) {

									isGreaterRowStarting = findgreater(
											$('#ROW_STARTING').val(),
											$('#ROW_NUMBER_OF').val(),
											'TableErrorMessage',
											'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
													+ '${Location_Wizard_Maintenance_Level.getKP_You_are_not_allowed_to_create_locations_in_reverse_order()}',$('#ROW_CHARACTER_SET').val().toUpperCase());
									if (isGreaterRowStarting) {
										isGreaterAisleStarting = findgreater(
												$('#AISLE_STARTING').val(),
												$('#AISLE_NUMBER_OF').val(),
												'TableErrorMessage',
												'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
														+ '${Location_Wizard_Maintenance_Level.getKP_You_are_not_allowed_to_create_locations_in_reverse_order()}',$('#AISLE_CHARACTER_SET').val().toUpperCase());
										if (isGreaterAisleStarting) {
											isGreaterBayStarting = findgreater(
													$('#BAY_STARTING').val(),
													$('#BAY_NUMBER_OF').val(),
													'TableErrorMessage',
													'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
															+ '${Location_Wizard_Maintenance_Level.getKP_You_are_not_allowed_to_create_locations_in_reverse_order()}',$('#BAY_CHARACTER_SET').val().toUpperCase());
											if (isGreaterBayStarting) {
												isGreaterLevelStarting = findgreater(
														$('#LEVEL_STARTING').val(),
														$('#LEVEL_NUMBER_OF').val(),
														'TableErrorMessage',
														'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
																+ '${Location_Wizard_Maintenance_Level.getKP_You_are_not_allowed_to_create_locations_in_reverse_order()}',$('#LEVEL_CHARACTER_SET').val().toUpperCase());
												if (isGreaterLevelStarting) {
													isGreaterSlotStarting = findgreater(
															$('#SLOT_STARTING').val(),
															$('#SLOT_NUMBER_OF').val(),
															'TableErrorMessage',
															'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
																	+ '${Location_Wizard_Maintenance_Level.getKP_You_are_not_allowed_to_create_locations_in_reverse_order()}',$('#SLOT_CHARACTER_SET').val().toUpperCase());
												} else {
													isTrue = false;
												}
											} else {
												isTrue = false;
											}
										} else {
											isTrue = false;
										}
									} else {
										isTrue = false;
									}
								}
							}
			}

				}
			
			
			
			
			
			if(isGreaterRowStarting
			&& isGreaterAisleStarting
			&& isGreaterBayStarting
			&& isGreaterLevelStarting
			&& isGreaterSlotStarting)
				{
				
				if (isTrue)
			     {
			    if ($("#ROW_STARTING").val().length == $(
			      '#ROW_NUMBER_CHARACTERS').val()) {
			     if ($("#ROW_NUMBER_OF").val().length == $(
			       '#ROW_NUMBER_CHARACTERS').val())
			      
			     {
			      
			      if ($("#AISLE_STARTING").val().length == $(
			      '#AISLE_NUMBER_CHARACTERS').val())
			       {
			       
			       if ($("#AISLE_NUMBER_OF").val().length == $(
			       '#AISLE_NUMBER_CHARACTERS').val())
			        {
			        if ($("#BAY_STARTING").val().length == $(
			        '#BAY_NUMBER_CHARACTERS').val())
			         {
			         
			         if ($("#BAY_NUMBER_OF").val().length == $(
			         '#BAY_NUMBER_CHARACTERS').val())
			          {
			          if ($("#LEVEL_STARTING").val().length == $(
			          '#LEVEL_NUMBER_CHARACTERS').val())
			           {
			           
			           if ($("#LEVEL_NUMBER_OF").val().length == $(
			           '#LEVEL_NUMBER_CHARACTERS').val())
			            {
			            if ($("#SLOT_STARTING").val().length == $(
			            '#SLOT_NUMBER_CHARACTERS').val())
			             {
			             
			             if ($("#SLOT_NUMBER_OF").val().length == $(
			             '#SLOT_NUMBER_CHARACTERS').val())
			              {
			               /* isShowErrorMsg("Hide",'TableErrorMessage',
			                 '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			                 + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}'); */
			              }
			             else
			              {
			              isShowErrorMsg("Show",'TableErrorMessage',
			                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			                + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			              isTrue = false;
			              }
			             }
			            else
			             {
			             isShowErrorMsg("Show",'TableErrorMessage',
			               '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			               + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			             isTrue = false;
			             }
			            }
			           else
			            {
			            isShowErrorMsg("Show",'TableErrorMessage',
			              '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			              + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			            isTrue = false;
			            }
			           }
			          else
			          {
			          isShowErrorMsg("Show",'TableErrorMessage',
			            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			            + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			          isTrue = false;
			          }
			          }
			         else
			          {
			          isShowErrorMsg("Show",'TableErrorMessage',
			            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			            + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			          isTrue = false;
			          }
			         }
			        else
			        {
			        isShowErrorMsg("Show",'TableErrorMessage',
			          '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			          + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			        isTrue = false;
			        }
			        }
			       else
			        {
			        isShowErrorMsg("Show",'TableErrorMessage',
			          '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			          + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			        isTrue = false;
			        }
			       }
			      else
			       {
			       isShowErrorMsg("Show",'TableErrorMessage',
			         '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			         + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			       isTrue = false;
			       }
			     }
			     else
			      {
			      isShowErrorMsg("Show",'TableErrorMessage',
			        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			        + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			      isTrue = false;
			      }
			    }
			    else{
			     isShowErrorMsg("Show",'TableErrorMessage',
			       '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
			       + '${Location_Wizard_Maintenance_Level.getKP_Equality_Check_Error_Msg()}');
			     isTrue = false;
			    }
			     }
				
			if (isTrue) {

				var form = $('#myForm').serialize();

				

				$('#myForm').submit(function() {

					var form = $('#myForm').serialize();
					
					$.ajax({
						type : "GET",
						url : "Location_Wizard_Maintenance_Add_Update",
						data : form,
						cache: false,
						beforeSend: function() {
							waiter();
							$('#loadingDiv').show();
							$('.cover').show();
						  },
						 
						success : function(response, textStatus, xhr) {
							waiterRemove();
							$('#loadingDiv').hide();
							setData(response);
							
						},
						error : function(xhr, textStatus, errorThrown) {
							waiterRemove();
							$('#loadingDiv').hide();
						}
					});
					return false;

				});//end get 

			}
				}
			/* isSubmit=false;	 */
			//document.location.href = '#top';
			window.scrollTo(0,0);
			/* 	return isSubmit; */

		}
	}

	 function okBtn() {

			//location.reload();
			 window.location.href = '${pageContext.request.contextPath}/Wizard_Location_Search_Lookup'; 
		}
	function clearBtn() {
		window.history.back();
		//window.location.href = '${pageContext.request.contextPath}/Wizard_Location_Search_Lookup';
	}

	function setData(data)
	{
		/* 
		  if(data[0]==1)
			 {
			 $('.cover').hide();
			 alert('${Location_Wizard_Maintenance_Level.getKP_Location_is_greater()}');
			 /* var errMsgId = document.getElementById("TableErrorMessage");
				errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
						+ "Location length is greater "; */
		/* 	 }
		else  */ 
		   if(data[0]=='OnlyWizardCreated')
			{
			 
			   bootbox.alert('${Location_Wizard_Maintenance_Level.getKP_Record_has_been_saved_successfully()}', function() {
				   
				 
		//	alert('${Location_Wizard_Maintenance_Level.getKP_Record_has_been_saved_successfully()}');
			 window.location.href = '${pageContext.request.contextPath}/Wizard_Location_Search_Lookup';
			   });
			}
		 else
			{
			// alert('${Location_Wizard_Maintenance_Level.getKP_Record_has_been_saved_successfully()}');
			
			 bootbox.alert('${Location_Wizard_Maintenance_Level.getKP_Record_has_been_saved_successfully()}', function() {
				   
			
			 div_show();
			 $('#SavedRecord').val(data[1]);
			 $('#DuplicateRecord').val(data[2]);
			
				
			
			 if(data[3]!='0')
				{
					 $('.show').show();
					$('.show').css({
						"display" :  "inline-table"
					}); 
			/*	document.getElementById('lebelDuplecates').style.visibility = 'visible';
				document.getElementById('DuplicateRecords').style.visibility = 'visible'; */
				
				document.getElementById('download').style.visibility = 'visible';
				 $('#DuplicateRecords').val(data[3]);
				}
			
			 });
			}
	}
	function div_show() {
		$('#ErrorMessageGrid').hide();

		$('.Show_main').show();
		$('.Show_main').css({
			"display" : "inline-table"
		});
		$('.cover').show();
	}
	function waiter() {
		document.body.style.cursor='wait';
		
		
		} 
	function waiterRemove() {
		document.body.style.cursor='default';
		
		
		}
	
	

</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>