<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE"%>
<script>
var contextPath='<%=request.getContextPath()%>';
</script>
<style>

fa-remove:before, .fa-close:before, .fa-times:before {
	margin-right: 5px !important;
}

.icon-pencil:before {
	margin-right: 5px !important;
}

/* .k-grid-content {
	position: relative;
	width: 100%;
	overflow: auto;
	overflow-x: auto;
	overflow-y: hidden !important;
	zoom: 0;
}

.k-grid th.k-header, .k-grid-header {
	
	white-space: normal !important;
	padding-right: 0px !important;
}

.k-grid-content>table>tbody>tr {
	overflow: visible !important;
	white-space: normal !important;
}
.k-grid .k-button {
margin: 2px .16em !important;
} */
</style>
<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div id="container" style="position: relative" class="loader_div">
		<div class="page-container">

			<div class="page-head">
				<div class="container">
					<!-- BEGIN PAGE TITLE -->
					<div class="page-title">
						<h1>${ViewLabels.getLbl_Fullfillment_Centers_Search_Lookup()}</h1>
			</div>
			</div>
			</div>

				<div class="page-content" id="page-content">
					<div class="container">
						<!-- BEGIN PAGE BREADCRUMB -->
						
						<!-- END PAGE BREADCRUMB -->
						
						<div>
						<a id="AddBtn2"
									href="${pageContext.request.contextPath}/FullfillmentCenterMaintenance_new"
									class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
									<span class="hidden-480">
										${ViewLabels.getLbl_Add_New()}</span>
								</a>
						</div>
						<!-- BEGIN PAGE CONTENT INNER -->
						<div class="row margin-top-46">

							<div class="form-body">
								<!-- <div class="form-group">
									<label class="col-md-2 control-label"
										style="color:#000000; display:true; width: 100%; font-weight:600">Part of the Description:
										Lorem </label>
										
										<input type="hidden" id="selec_TeamMember" >
										<input type="hidden" id="selec_Tenant" >
										
								
							
							<br />
							<br />
							<br />
							<div class="form-group">
													<label class="col-md-2 control-label" style="color:#000000;display:true;width:100%;">Part of the  Fulfillment Center: Lorem 
													</label>
													
												</div></div> --><br/><br/><br/>
												
							<div class="col-md-6" style="width: 100%;margin-top: -53px;">
								<!-- BEGIN SAMPLE TABLE PORTLET-->


								<div>
<div class="portlet">

								<%-- <a id="AddBtn"
									href="${pageContext.request.contextPath}/FullfillmentCenterMaintenance_new"
									class="btn default yellow-stripe"> <i class="fa fa-plus"></i>
									<span class="hidden-480">
										${ViewLabels.getLbl_Add_New()}</span>
								</a> --%>

									<div class="row" >
         <kendo:grid name="FullfillmentCenterGrid" resizable="true"
          reorderable="true" sortable="true" autoSync="true" dataBound="gridDataBound">
          <kendo:grid-pageable refresh="true" pageSizes="true"
											buttonCount="5">
										</kendo:grid-pageable>
          <kendo:grid-editable mode="inline" confirmation="true" />

          <kendo:grid-columns>
           <kendo:grid-column
            title="${ViewLabels.getLbl_Fulfillment_Center()}"
            field="fullfillment" width="36%"/>
           <kendo:grid-column
            title="${ViewLabels.getLbl_Name()}"
            field="name50" width="47%"/>
             
           <kendo:grid-column title="${ViewLabels.getLbl_Actions()}" width="17%">
													<kendo:grid-column-command>
														<%-- <kendo:grid-column-commandItem name="edit" text="Edit" /> --%>
														<kendo:grid-column-commandItem
															className="icon-pencil btn btn-sm yellow filter-submit margin-bottom"
															name="editDetails" text="${ViewLabels.getLbl_Edit()}">
															<kendo:grid-column-commandItem-click>
																<script>
                            function showDetails(e) {
                               

                                e.preventDefault();

                                var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                          

									var varfulfillment = dataItem.fullfillment;
									
									var url = contextPath
									+ "/FullfillmentCenterMaintenanceUpdate"
									
									
									 var myform = document.createElement("form");

	                                 var fulfillmentField = document.createElement("input");
	                                 fulfillmentField.value = varfulfillment;
	                                 fulfillmentField.name = "FulfillmentID";
	                                 fulfillmentField.path="FulfillmentID";
	                                 fulfillmentField.setAttribute("type", "hidden");
	                                
	                                 
	                                 myform.action = url;
	                                 myform.method = "get";
	                                 myform.appendChild(fulfillmentField);
	                                 document.body.appendChild(myform);
	                                 myform.submit();
								
                                
                                
                                
                            }
                            </script>
															</kendo:grid-column-commandItem-click>
														</kendo:grid-column-commandItem>
														<kendo:grid-column-commandItem
														className="fa fa-times btn btn-sm red filter-cancel"
														name="deleteDetails"
														text=' ${ViewLabels.getLbl_Delete()}'>
														<kendo:grid-column-commandItem-click>
															<script>
																function deleteConfirmation(
																		e) {
																	e.preventDefault();
																	 var dataItem = this
																		.dataItem($(e.currentTarget).closest("tr"));


																	/* if (confirm('${ViewLabels.getERR_Are_you_sure_you_want_to_delete_the_selected_row()}') == true) {

																	 */
																	 bootbox
				                                                     .confirm(
				                                                       '${ViewLabels.getERR_Are_you_sure_you_want_to_delete_the_selected_row()}',
				                                                       function(okOrCancel) {

				                                                        if(okOrCancel == true)
				                                                        {
																	
																		//var dataSource = $("#FullfillmentCenterGrid").data("kendoGrid").dataSource;
																		var fulfillment=dataItem.fullfillment;
																		var tenant=dataItem.tenant;
																		
																		
																		
													                       
																		
																		//dataItem.seqStatus = 'X';
																		
																		/*
																		var tenant = '${viewTenant}';
																		var teamMember = dataItem.teamMember;
																		var Company='Abc';
																		var url = contextPath
																				+ "/deleteFromSEQ?Tenant="
																				+ tenant
																				+ "&TeamMember="
																				+ teamMember
																				+ "&Company=Abc"; */
																		
																		  $.post( "${pageContext.request.contextPath}/DeleteFullfillment",
																				  {Tenant:tenant,
																			  Fullfillment:fulfillment
																			 
																				  }, function( data1,status ) {
																					  if (!data1.boolStatus) {

																						  bootbox
										                                                     .alert('${ViewLabels.getERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table()}',
										                                                    		 function(){
										                                                    	
										                                                     });
																							
																						} else {

																							//alert('${ViewLabels.getERR_Selected_fulfillment_center_deleted_successfully()}');
																							 
																							 bootbox
										                                                     .alert('${ViewLabels.getERR_Selected_fulfillment_center_deleted_successfully()}',
										                                                    		 function(){
										                                                    	 $("#FullfillmentCenterGrid").data("kendoGrid").dataSource.read();
										                                                     });
																							
																							
																						}
																					  
																					  
																				  });
																		 
																		
															/* 			$
																				.get(
																						url,
																						function(
																								data1) {

																							if (!data1.boolStatus) {

																								if (data1.strStatus === "UserIsInactive") {
																									alert('${viewLabels.getErrSelectionUserIsAlreadyInActive()}');
																								} else {
																									alert('${viewLabels.getErrSelectionUserNotExists()}');
																								}
																							} else {

																								alert('${viewLabels.getErrSelectionStatusUpdated()}');
																							}

																						}); */

																		//   window.location=url;
																	

																	} else {

																	}
				                                                       });
																}
															</script>
														</kendo:grid-column-commandItem-click>
													</kendo:grid-column-commandItem>
													</kendo:grid-column-command>
												</kendo:grid-column>


          </kendo:grid-columns>

          <kendo:dataSource pageSize="10" autoSync="true">
           <kendo:dataSource-transport>
            <kendo:dataSource-transport-read cache="false"
             url="${pageContext.request.contextPath}${KendoReadURL}">
             </kendo:dataSource-transport-read>

            <kendo:dataSource-transport-parameterMap>
             <script>
                  function parameterMap(options,type) { 
                   
                    return JSON.stringify(options);
                   
                  }
                 </script>
            </kendo:dataSource-transport-parameterMap>

           </kendo:dataSource-transport>

          </kendo:dataSource>
         </kendo:grid>
         </div>
         
         </div>
								</div>

								<!-- END SAMPLE TABLE PORTLET-->
							</div>
							<!--end tabbable-->

							<!--end tabbable-->

						</div>

						<!-- END PAGE CONTENT INNER -->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		</div>
		
		<!-- END PAGE CONTENT -->
</div>
	</tiles:putAttribute>
</tiles:insertDefinition>
<script>
	function sortData() {
		//sortFirstName display:none;

		

	}
</script>
<script>

function gridDataBound(e) {
    var grid = e.sender;
    if (grid.dataSource.total() > 0) {
     var colCount = grid.columns.length;
     kendo.ui.progress(ajaxContainer, false);

    }
    else
     {
      kendo.ui.progress(ajaxContainer, false);
     }
   };
   
   var ajaxContainer = $("#container");
   kendo.ui.progress(ajaxContainer, true);

function headerChangeLanguage(){
	
	/* alert('in security look up'); */
	
 	$.ajax({
		url: '${pageContext.request.contextPath}/HeaderLanguageChange',
		type: 'GET',
        cache: false,
		success: function(
				data1) {

			if (data1.boolStatus) {

				location.reload();
			}
		}
	});
	
}

function headerInfoHelp(){
	
	//alert('in security look up');
	
 	/* $
	.post(
			"${pageContext.request.contextPath}/HeaderInfoHelp",
			function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			});
	 */
 	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
			  {InfoHelp:'fulfillment',
 		InfoHelpType:'PROGRAM'
		 
			  }, function( data1,status ) {
				  if (data1.boolStatus) {
					  window.open(data1.strMessage); 
					  					  
					}
				  else
					  {
					//  alert('No help found yet');
					  }
				  
				  
			  });
	
}



jQuery(document).ready(function() {

	//alert("Deepak");
	  
	setInterval(function () {
		//alert("hello");
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
	
	
	/* $("#FullfillmentCenterGrid").kendoTooltip({
		   filter: "td:nth-child(2)", //this filter selects the first column cells
		   position: "bottom", 
		   content: function(e){
		    var dataItem = $("#FullfillmentCenterGrid").data("kendoGrid").dataItem(e.target.closest("tr"));
		    var content = dataItem.fullfillment+'<br>'+dataItem.name50+'<br>'+dataItem.name20;
		    return content;
		   }
		 }).data("kendoTooltip"); */
	    
    
	//Layout.init(); // init layout
	//Demo.init(); // init demo(theme settings page)

	$(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });

});
</script>
</body>
<!-- END BODY -->
</html>