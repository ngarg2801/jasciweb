<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>JASCI</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"> 
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="<c:url value="/resourcesValidate/css/glass-style.css"/>"  rel="stylesheet" type="text/css">
</head>
<body>
<tiles:insertDefinition name="menuAssigmentTemplate">
	<tiles:putAttribute name="body">
	<div class="page-head">
	<div class="page-content" id="page-content">
	
					<div class="container">
					<div class="iframetopmargin" id="frame_tag_div">
					
				 <form id="moodleform" target="iframe" action="${ObjCommonSession.getXlaticsDashBoardPath()}" method="POST" >
         <input type="hidden" id="CompID" name="CompID" value="${ObjCommonSession.getCompIDValue()}"/>
         <input type="hidden" id="APIKEY" name="APIKEY" value="${ObjCommonSession.getAPIKEYValue()}"/>
         <input type="hidden" id="SECRETKEY" name="SECRETKEY" value="${ObjCommonSession.getSECRETKEYValue()}"/>
         <input type="hidden" id="UserID" name="UserID" value="${ObjCommonSession.getUserIDValue()}"/>
         <input type="hidden" id="ProjectName" name="ProjectName" value="${ObjCommonSession.getProjectNameValue()}"/>
         <input type="hidden" id="DepartmentID" name="DepartmentID" value="${ObjCommonSession.getDepartmentIDValue()}"/>
         <input type="hidden" id="DashboardId" name="DashboardId" value="${ObjCommonSession.getDashboardIdValue()}"/>
         <iframe   class="iframesetting"  id="iframe" name="iframe"></iframe>
     </form> 
					 <%-- <form id="moodleform" target="iframe" action="http://ec2-54-164-101-182.compute-1.amazonaws.com/JasciCheck/ExternalDashboard.aspx" method="POST" >
    <input type="hidden" id="CompID" name="CompID" value="140"/>
    <input type="hidden" id="APIKEY" name="APIKEY" value="APIKEY"/>
     <input type="hidden" id="SECRETKEY" name="SECRETKEY" value="SECRETKEY"/>
    <input type="hidden" id="UserID" name="UserID" value="jasci.demo@xtlytics.com"/>
     <input type="hidden" id="ProjectName" name="ProjectName" value="JASCI"/>
       <input type="hidden" id="DepartmentID" name="DepartmentID" value="043"/>
    <input type="hidden" id="DashboardId" name="DashboardId" value="bbaef167-1e46-11e5-819b-066d82643b57"/>
    <iframe id="iframe" name="iframe" style="height:600px;width:100%;"  ></iframe>
</form> --%>
			</div>
			<div class="tickettap_exection_main" id="removeclassfortablet"><marquee id="ticketTapDivID" style="color:#FFFFFF; margin-top:13px;" loop="-1" scrollamount="4">
    <span id="ticketTapeMessage" ></span> </marquee>
   </div>
			
		
					
					</div>
					</div>
	</div>

</tiles:putAttribute>

</tiles:insertDefinition>
<!-- END PAGE CONTAINER -->

<!-- END JAVASCRIPTS -->

<!-- END BODY -->


<script type="text/javascript">


function TicketTape() {
	   
	 var selectedValue=getUTCDateTime();

$.ajax({
url: '${pageContext.request.contextPath}/MenuMessages',
cache: false,
data : {DateTime : selectedValue},
error: function() {
   //$('#info').html('<p>An error has occurred</p>');
},

success: function(data) {

   $('#ticketTapDivID').empty();
 for (var message in data){

    if((data[message].ticket_Tape_Message)!=null){
        $('#ticketTapDivID').append('<span>'+data[message].ticket_Tape_Message+'</span></marquee>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
    }
 }
},
type: 'GET'
});

};


	$(document).ready(function() {
		
	  $('#moodleform').submit();
		setInterval(function () {
			TicketTape();			  
			},2000);
		 var ScreenType='FULLSCREEN';
			var agent = navigator.userAgent;      
		    var isWebkit = (agent.indexOf("AppleWebKit") > 0);      
		    var isIPad = (agent.indexOf("iPad") > 0);      
		    var isIOS = (agent.indexOf("iPhone") > 0 || agent.indexOf("iPod") > 0);     
		    var isAndroid = (agent.indexOf("Android")  > 0);     
		    var isNewBlackBerry = (agent.indexOf("AppleWebKit") > 0 && agent.indexOf("BlackBerry") > 0);     
		    var isWebOS = (agent.indexOf("webOS") > 0);      
		    var isWindowsMobile = (agent.indexOf("IEMobile") > 0);     
		    var isSmallScreen = (screen.width < 767 || (isAndroid && screen.width < 1000));     
		    var isUnknownMobile = (isWebkit && isSmallScreen);     
		    var isMobile = (isIOS || isAndroid || isNewBlackBerry || isWebOS || isWindowsMobile || isUnknownMobile);     
		    var isTablet = (isMobile && !isSmallScreen);  
		    var isGLASSX6 = (agent.indexOf("X6") > 0 || agent.indexOf("X7") > 0);
	if (isGLASSX6) {
		ScreenType = "GLASS";

		
		$('.page-content').attr('style',
				'background-color: black !important');
	
	$('#moodleform').remove();
	$('#iframe').remove();
	
	//	document.getElementsByTagName('BODY')[0].removeChild(document.getElementById('moodleform'));
	 document.getElementById("frame_tag_div").setAttribute(
				"class", "frame_tag_width"); 
	}
	 else if((isMobile && isSmallScreen)||isTablet)
     {
	    	
		 if(screen.width<700) 
	    	{	   
	    		
	    		$( "#removeclassfortablet" ).removeClass( "tickettap_exection_main" ).addClass( "tickettap_exection_main_tablet3" );
	    	}
	    	//this is tab4 css and condition
	    	else if(screen.width>750) 
	    	{	    
	    		
	    		$( "#removeclassfortablet" ).removeClass( "tickettap_exection_main" ).addClass( "tickettap_exection_main_tablet3" );	
	    	}
//$( "#removeclassfortablet" ).removeClass( "tickettap_exection_main" ).addClass( "tickettap_exection_main_tablet" );
     }
	
/* 	var isGLASSX7 = (agent.indexOf("X7") > 0);
	if (isGLASSX7) {
		ScreenType = "GLASS";

		//document.getElementById('body').style.display = "hidden";
		
		$('.page-content').attr('style',
				'background-color: black !important');
		//document.body.removeChild(formEl);
		//document.getElementById("moodleform").style.display="none";
	//	document.body.removeChild(document.getElementById('moodleform'));
	$('#moodleform').remove();
	//	document.getElementsByTagName('BODY')[0].removeChild(document.getElementById('moodleform'));
	 document.getElementById("frame_tag_div").setAttribute(
				"class", "frame_tag_width"); 
	} */
		  
	
		//Layout.init(); // init layout
		//Demo.init(); // init demo(theme settings page)
	});
	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900 || window.innerHeight==1004 ){
	    	h=h-100;
	    }
	   else{
	    	h=h-150;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
	    
	
	function getUTCDateTime(){

		var localdate =    moment.utc().format("YYYY-MM-DD");
		return   localdate;  
	
		}
	
	  function headerChangeLanguage(){
		  	
		  	/* alert('in security look up'); */
		  	
		  $.ajax({
				url: '${pageContext.request.contextPath}/HeaderLanguageChange',
				type: 'GET',
		        cache: false,
		       
				success: function(
						data1) {

					if (data1.boolStatus) {

						location.reload();
					}
				}
			});
		  	
		  }
	  
	
</script>
<style>
.tickettap_exection_main_tablet3 {
 
    width: 100.2% !important;
    height: 45px;
    background-color: black;
    margin-top: -16px !important;
    margin-left: 15px;

} 

 @media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait)
{
   .iframetopmargin{
  
   padding-top:2.5%;
   }
   .iframesetting{
  	width:100.2%;
    height:auto;
    border: 0px;
    margin-left:15px;
    background-color: #eff3f8;
   }
   }
/* .iframesetting{
 width:100.2%;
     height: 1111px;
    border: 1px solid #BBB4B4;
   margin-left:15px;
   background-color: #eff3f8;
   } */
   
   .iframesetting{
 	width:100.2%;
    height: 990px;
    border: 0px;
    margin-left:15px;
    background-color: #eff3f8;
   }
   #page-content{
 padding-top:5%;
 } 
</style>

</body>
</html> 