<!-- 
------------------------------------------------------------------------------------------------------
	Program Name : SmartTaskExecutions_Maintenance.jsp 
------------------------------------------------------------------------------------------------------	
Date Developed  May 14 2015
Description It is used to Search record on behalf of inputs.
Created By Aakash Bishnoi -->
<%@ page language="java" contentType="text/html; charset=UTF-8" 	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
	
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">

	<div class="page-head">
			<div class="container">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
				<h1>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Executions_Maintenance()}</h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				
				<!-- END PAGE TOOLBAR -->
			</div>
	</div>
	
        <!-- BEGIN PAGE CONTENT -->
      <div class="page-content" id="page-content">
            <div class="container">
                
                
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row margin-top-10">
                
                <div id="ErrorMessage" class="note note-danger" style="display:none">
		     		<p id="MessageRestFull" class="error error-Top"></p>	
		    	</div>
                
                
                    <div class="col-md-12">
                        
                        <form:form name="myForm"
									class="form-horizontal form-row-seperated" action="#"
									onsubmit="return isformSubmit();" method="post" 
									modelAttribute="ObjSmartTaskExecution">
                        <div class="portlet">
                            <div class="portlet-body">
                                <div class="tabbable">
                                    <div class="tab-content no-space">
                                        <div class="tab-pane active" id="tab_general">
                                            <div class="form-body">
                                            
                                                <div id="HideTenant" class="form-group" style="display:block;">
                                                    <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Tenant()}:
                                                    </label>
                                                    <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Tenant_Id" name="Tenant_Id"  path="Tenant_Id" 
                                                      		 	readonly="true" value="${ObjSmartTaskExecution.getTenant_Id()}" />
       															<span id="ErrorTenant_Id" class="error"> </span>
                                                    </div>
                                                </div>
                                                
                                            
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                       ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Application()}:
                                                    </label>
                                                    <div class="col-md-10">
														<form:select path="Application_Id"  style="display:inline"  class="table-group-action-input form-control input-medium" id="Application_Id" name="Application_Id">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectApplication}" var="DropDownApplication">
													    <%-- 		<option value="${DropDownApplication.getGeneralCode()}">${DropDownApplication.getDescription20()}</option>   --%>
													    
													    			<c:choose>
													    					<c:when
       																			test="${ObjSmartTaskExecution.getApplication_Id().equalsIgnoreCase(DropDownApplication.getGeneralCode())}">
       																					<option
       																							value='${DropDownApplication.getGeneralCode()}'
       																							selected="selected">${DropDownApplication.getDescription20()}
       																					</option>
       																	   </c:when>
       																	   <c:otherwise>
       																			<option
       																					value='${DropDownApplication.getGeneralCode()}'>${DropDownApplication.getDescription20()}
       																			</option>
       																	</c:otherwise>
													    			</c:choose>
													    																   
															 </c:forEach>	
														</form:select><span id="ErrorApplication_Id" class="error-select"> </span>
													</div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Group()}:
                                                    </label>
                                                    
                                                    <div class="col-md-10">
														
														<form:select path="Execution_Group"  style="display:inline"  class="table-group-action-input form-control input-medium" id="Execution_Group" name="Execution_Group">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectExecutionGroup}" var="DropDownExecutionGroup">
													    <%-- 		<option value="${DropDownExecutionGroup.getGeneralCode()}">${DropDownExecutionGroup.getDescription20()}</option>		--%>		
													    
													   			 <c:choose>
													    				<c:when
       																			test="${ObjSmartTaskExecution.getExecution_Group().equalsIgnoreCase(DropDownExecutionGroup.getGeneralCode())}">
       																					<option
       																							value='${DropDownExecutionGroup.getGeneralCode()}'
       																							selected="selected">${DropDownExecutionGroup.getDescription20()}
       																					</option>
       																	</c:when>
       																	<c:otherwise>
       																			<option
       																					value='${DropDownExecutionGroup.getGeneralCode()}'>${DropDownExecutionGroup.getDescription20()}
       																			</option>
       																	</c:otherwise>
													             </c:choose>
													    										   
															 </c:forEach>	
														</form:select><span id="ErrorExecution_Group" class="error-select"> </span>
													</div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                          ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Type()}:
                                                    </label>
                                                   <div class="col-md-10">
														
														<form:select path="Execution_Type"  style="display:inline"  class="table-group-action-input form-control input-medium" id="Execution_Type" name="Execution_Type">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectExecutionType}" var="DropDownExecutionType">
													    	<%-- 			<option value="${DropDownExecutionType.getGeneralCode()}">${DropDownExecutionType.getDescription20()}</option>		--%>
													    	
													    	<c:choose>
													    			<c:when
       																			test="${ObjSmartTaskExecution.getExecution_Type().equalsIgnoreCase(DropDownExecutionType.getGeneralCode())}">
       																					<option
       																							value='${DropDownExecutionType.getGeneralCode()}'
       																							selected="selected">${DropDownExecutionType.getDescription20()}
       																					</option>
       																</c:when>
       																<c:otherwise>
       																			<option
       																					value='${DropDownExecutionType.getGeneralCode()}'>${DropDownExecutionType.getDescription20()}
       																			</option>
       																</c:otherwise>
													    	</c:choose>
													    	
													    														   
															 </c:forEach>	
														</form:select><span id="ErrorExecution_Type" class="error-select"> </span>
													</div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                          ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Device()}:
                                                    </label>
                                                    <div class="col-md-10">
														
														<form:select path="Execution_Device"  style="display:inline"  class="table-group-action-input form-control input-medium" id="Execution_Device" name="Execution_Device">
															<option value="">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Select()}..</option>
															
														<c:forEach items="${SelectExecutionDevice}" var="DropDownExecutionDevice">
													    <%--		<option value="${DropDownExecutionDevice.getGeneralCode()}">${DropDownExecutionDevice.getDescription20()}</option> --%>
													    		
													    			<c:choose>	
													    					<c:when
       																			test="${ObjSmartTaskExecution.getExecution_Device().equalsIgnoreCase(DropDownExecutionDevice.getGeneralCode())}">
       																					<option
       																							value='${DropDownExecutionDevice.getGeneralCode()}'
       																							selected="selected">${DropDownExecutionDevice.getDescription20()}
       																					</option>
       																		</c:when>
       																		<c:otherwise>
       																			<option
       																					value='${DropDownExecutionDevice.getGeneralCode()}'>${DropDownExecutionDevice.getDescription20()}
       																			</option>
       																</c:otherwise>
													    			
													    				
													    			</c:choose>
													    																   
															 </c:forEach>		
														</form:select><span id="ErrorExecution_Device" class="error-select"> </span>
													</div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Name()}: <span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Execution_Name" name="Execution_Name"  path="Execution_Name" 
                                                      		 	value="${ObjSmartTaskExecution.getExecution_Name()}" />
       															<span id="ErrorExecution_Name" class="error"> </span>
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Path()}:<span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Execution_Path" name="Execution_Path"  path="Execution_Path" 
                                                      		 	value="${ObjSmartTaskExecution.getExecution_Path()}" />
       															<span id="ErrorExecution_Path" class="error"> </span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Description_Short()}:<span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Description20" name="Description20"  path="Description20" maxlength="20"
                                                      		 	value="${ObjSmartTaskExecution.getDescription20()}" />
       															<span id="ErrorDescription20" class="error"> </span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                       ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Description_Long()}:<span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Description50" name="Description50"  path="Description50" maxlength="50"
                                                      		 	value="${ObjSmartTaskExecution.getDescription50()}" />
       															<span id="ErrorDescription50" class="error"> </span>
                                                    </div>
                                                </div>
                                                	
                                                
                                               <div id="Buttons" class="form-group" >
   													 <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Buttons()}:<span class="required"> * </span>
                                                    </label>
                                                    <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Button" name="Button"  path="Button"  maxlength="1"
                                                      		 	value="${ObjSmartTaskExecution.getButton()}" />
                                                      		 	<span>(  Y/ N ) </span>
       															<span id="ErrorButton" class="error"> </span>
                                                    </div>
                                                </div>
                                                                                                                                                                                                
                                                <div id="Button_name" class="form-group" >
   													 <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Buttons_Name()}:<span class="required"  id="ButtonName" > * </span>
                                                    </label>
                                                     <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Button_Name" name="Button_Name"  path="Button_Name" maxlength="10" 
                                                      		 	value="${ObjSmartTaskExecution.getButton_Name()}" />
       															<span id="ErrorButton_Name" class="error"> </span>
                                                    </div>
                                                     
                                                </div>
                                                                                        
                                                <div id="Help_LineD" class="form-group" >
   													 <label class="col-md-2 control-label">
                                                         ${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Help_Line()}:<span class="required"> * </span>
                                                    </label>
                                                     <div class="col-md-10">
                                                      <form:input type="text"  class="form-controlwidth" id="Help_Line" name="Help_Line"  path="Help_Line" 
                                                      		 	value="${ObjSmartTaskExecution.getHelp_Line()}" />
       															<span id="ErrorHelp_Line" class="error"> </span>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="form-group">
														<label class="col-md-2 control-label">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Last_Activity_Date()}: </label>
														 <div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="Last_Activity_Date" path="Last_Activity_Date"
																readonly="true"
																 />
																
																<input type="hidden" class="form-controlwidth"
																name="Last_Activity_DateH" id="Last_Activity_DateH"
																 />
														</div>
												</div>
												
												<div class="form-group">
														<label class="col-md-2 control-label">${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Last_Activity_By()}: </label>
														<div class="col-md-10">
															<form:input type="text" class="form-controlwidth"
																name="Last_Activity_Team_Member" path="Last_Activity_Team_Member"
																readonly="true"
																value="${ObjSmartTaskExecution.getLast_Activity_Team_Member()}" />
														</div>
												</div>
                                                
                                                <div class="margin-bottom-5-right-allign_Smart_Task_Configurator">
                                                   				 <button
																	class="btn btn-sm yellow filter-submit margin-bottom"id="BtnIdNew"
																	onclick="actionForm('SmartTaskExecutions_Add','New');">
																	<i class="fa fa-plus"></i>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Save_Update()}
																</button>
																
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdReset"
																	onclick="reloadPage();">
																	<i class="fa fa-check"></i>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Reset()}
																</button>
																<button
																	class="btn btn-sm yellow filter-submit margin-bottom" id="BtnIdCancel"
																	onclick="backPage();">
																	<i class="fa fa-check"></i>${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Cancel()}
																</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form:form>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->
  
</tiles:putAttribute>
</tiles:insertDefinition>

<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
		<link type="text/css"	href="<c:url value="/resourcesValidate/css/SmartTaskConfigurator.css"/>"
	rel="stylesheet" /> 
	
<script>

var BlankEntryCount = 0;
function reloadPage() {													
	location.reload();
}

function backPage(){													
	window.location="${pageContext.request.contextPath}/SmartTaskExecutions_Lookup";
}

jQuery(document).ready(function() {   
	
	if("${JASCI}" == "${ObjCommonSession.getJasci_Tenant()}"){
		$('#HideTenant').show();
	} else {
		$('#HideTenant').hide();
	} 
	$('#ButtonName').hide();
	
	$("#Button").change(function() {
		var buttonValue = $('#Button').val().toUpperCase();
		if (buttonValue == "Y") {
			$('#ButtonName').show();
		}
		else {
			$('#ButtonName').hide();
		}
		document.getElementById("Button").innerHTML = buttonValue.toUpperCase();
		if (buttonValue != "Y" && buttonValue != "N") {
			document.getElementById("ErrorButton").innerHTML =   "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Must be either  Y or N ";
			document.getElementById("ErrorButton").focus();
		} else {
			document.getElementById("ErrorButton").innerHTML = "";
		}
	});

	var message='${Saved}';
	if(message=='Saved')	{
		 bootbox.alert('${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Your_record_has_been_saved_successfully()}',function(){
		window.location.href="SmartTaskExecutions_Lookup";
		 });
		}

	
	setInterval(function () {
		
	    var h = window.innerHeight;
	    if(window.innerHeight>=900){
	    	h=h-187;
	    }
	   else{
	    	h=h-239;
	    }
	      document.getElementById("page-content").style.minHeight = h+"px";
	    
		}, 30);
});


function checkBlank(fieldID) {
	 var fieldLength = document.getElementById(fieldID).value.trim().length;
	 if (fieldLength>0) {
	  return true;
	 }
	 else {
	  return false;
	 }
}

$('#ErrorMessage').hide();


$(window).keydown(function(event){
          if(event.keyCode == 13) {
           var BoolAPPLICATION_ID = checkBlank('APPLICATION_ID');
           var BoolEXECUTION_SEQUENCE_GROUP = checkBlank('EXECUTION_SEQUENCE_GROUP');
           var BoolEXECUTION_TYPE = checkBlank('EXECUTION_TYPE');
           var BoolEXECUTION_DEVICE= checkBlank('EXECUTION_DEVICE');		   
		   var BoolDESCRIPTION20 = checkBlank('DESCRIPTION20');
           var BoolEXECUTION_NAME = checkBlank('EXECUTION_NAME');
           var BoolTENANT_ID = checkBlank('TENANT_ID');
          }
      });
      
      

var isSubmit = false;

function isformSubmit() {
	
		return isSubmit;
}

function actionForm(url, action) {
	
	$('#ErrorMessage').hide();
	var BlankAPPLICATION_ID = document.getElementById("APPLICATION_ID");
	var BlankEXECUTION_SEQUENCE_GROUP = document.getElementById("EXECUTION_SEQUENCE_GROUP");
	var BlankEXECUTION_TYPE = document.getElementById("EXECUTION_TYPE");
	var BlankEXECUTION_DEVICE = document.getElementById("EXECUTION_DEVICE");
	var BlankDESCRIPTION20 = document.getElementById("DESCRIPTION20");
	var BlankEXECUTION_NAME = document.getElementById("EXECUTION_NAME");
	var BlankTENANT_ID = document.getElementById("TENANT_ID");
	
	var isBlankEntry = false;
	
 if (action == 'New') {
		var Application_Id = $('#Application_Id').val();
		var Execution_Group = $('#Execution_Group').val();
		var Execution_Type = $('#Execution_Type').val();
		var Execution_Device = $('#Execution_Device').val();
		var Execution_Name = $('#Execution_Name').val();
		var Execution_Path = $('#Execution_Path').val();
		var Description20 = $('#Description20').val();
		var Description50 = $('#Description50').val();
		var Tenant_Id = $('#Tenant_Id').val();
		var Button = $('#Button').val().toUpperCase();
		var Button_Name = $('#Button_Name').val();
		var Help_Line = $('#Help_Line').val();
		var Last_Activity_Date = $('#Last_Activity_Date').val();
		var Last_Activity_Team_Member = $('#Last_Activity_Team_Member').val();
		var Company_Id = " ${ObjSmartTaskExecution.getCompany_Id()}"
		var MandatoryErrMsg = 'Mandatory field cannot be left blank';
		
		isBlankEntry = isBlankFieldWithTrim('Application_Id', 'ErrorApplication_Id', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++; 
		
		isBlankEntry = isBlankFieldWithTrim('Execution_Group', 'ErrorExecution_Group', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++; 
		
		isBlankEntry = isBlankFieldWithTrim('Execution_Type', 'ErrorExecution_Type',	 '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++;
		
		isBlankEntry = isBlankFieldWithTrim('Execution_Device', 'ErrorExecution_Device',  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++;
		
		isBlankEntry = isBlankFieldWithTrim('Execution_Path', 'ErrorExecution_Path',  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++;
		
		isBlankEntry = isBlankFieldWithTrim('Execution_Name', 'ErrorExecution_Name',  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++;
		
		isBlankEntry = isBlankFieldWithTrim('Description20', 'ErrorDescription20',  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++;
		
		isBlankEntry = isBlankFieldWithTrim('Description50', 'ErrorDescription50', 	 '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++;
		
		isBlankEntry = isBlankFieldWithTrim('Button', 'ErrorButton',  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);   
		if (!isBlankEntry) BlankEntryCount++;
		
		if (Button == "Y") {
				isBlankEntry = isBlankFieldWithTrim('Button_Name', 'ErrorButton_Name',  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
				if (!isBlankEntry) BlankEntryCount++;
		}
		
		isBlankEntry = isBlankFieldWithTrim('Help_Line', 'ErrorHelp_Line',  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + MandatoryErrMsg);
		if (!isBlankEntry) BlankEntryCount++;
		
		if  (BlankEntryCount > 0)  {
			var errMsgId = document.getElementById("MessageRestFull");
			errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' 	+ 'Please enter the mandatory ' + BlankEntryCount  + ' fields .';
			$('#ErrorMessage').show();
			BlankEntryCount = 0;
			document.myForm;
			isSubmit = false;
	    }		
	   else
	   {
		   $.post("${pageContext.request.contextPath}/SmartTaskExecutions_Restfull_IsExist",
					{
			   			 Tenant_Id : Tenant_Id,
			   			 Company_Id : Company_Id,
			             Execution_Name : Execution_Name
			             
					},
					function(data, status) {
                       
                   	if (data) {
							var errMsgId = document.getElementById("MessageRestFull");
							errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ 'Execution_Name_already_Used';
						//	errMsgId.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ '${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Execution_Name_already_Used()}';
							$('#ErrorMessage').show();
							isSubmit = false;
							
					} 
                   	else {
						 $.post("${pageContext.request.contextPath}/SmartTaskExecutions_Add",
									{
							             Application_Id : Application_Id,
							             Execution_Group : Execution_Group,
							             Execution_Type : Execution_Type,
							             Execution_Device : Execution_Device,
							             Execution_Name : Execution_Name,
							             Execution_Path : Execution_Path,
							             Description20 : Description20,
							             Description50 : Description50,
							             Tenant_Id : Tenant_Id,
							             Button : Button,
							             Button_Name : Button_Name,
							             Help_Line : Help_Line,
							             Last_Activity_Date : Last_Activity_Date,
							             Last_Activity_Team_Member : Last_Activity_Team_Member
									},
									function(data, status) {
										
				                    	if(data) {
												bootbox.alert('${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Your_record_has_been_saved_successfully()}',function(){
												window.location.href="SmartTaskExecutions_Lookup";
												 });
										} else {
											 bootbox.alert('${SMARTTASKEXECUTIONSBE.getSmartTaskExecutions_Adding_record_failed()}',function(){
													window.location.href="SmartTaskExecutions_Lookup";
													 });
										}
									});
                   		}
					});
	   }	   
	} 
}

function headerChangeLanguage(){
	  
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp() {
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'SmartTaskConfigurator',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }
</script>