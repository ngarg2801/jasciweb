<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="IE=edge" />
<meta name="viewport"
	content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<title>Jasci</title>
<link rel="stylesheet" type="text/css" href="<c:url value="/resourcesValidate/AngularGoJS/angular/snap.css"/>" />
<link rel="stylesheet" href="<c:url value="/resourcesValidate/AngularGoJS/angular/angular-snap.css"/>" />
<link rel="stylesheet" href="<c:url value="/resourcesValidate/AngularGoJS/angular/bootstrap.min.css"/>" />
<link rel="stylesheet" href="<c:url value="/resourcesValidate/AngularGoJS/angular/app.css"/>" />
<link href="<c:url value="/resourcesValidate/AngularGoJS/angular/Style.css"/>" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resourcesValidate/AngularGoJS/angular/bootstrap.css"/> "rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
<link href="<c:url value="/resourcesValidate/AngularGoJS/angular/custome-style.css"/>" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resourcesValidate/AngularGoJS/angular/SmartTaskConfigurator.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resourcesValidate/AngularGoJS/angular/css/font-awesome.min.css"/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value="/resourcesValidate/AngularGoJS/angular/ng-table.css"/>" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resourcesValidate/AngularGoJS/angular/components.css"/>" rel="stylesheet"
	type="text/css" />

<style>
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
  display: none !important;
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape)
{

.sequenceviewbuttons{
	  float: right;
	  padding-left: 74% !important;
	 /*  padding-top: 12px; */
	  margin-top:-33%;
	  position: absolute;
	  z-index: 1000;
	}
	
}

@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait)
{
	
 .sequenceviewbuttons{
	  float: right;
	  padding-left: 68% !important;
	 /*  padding-top: 12px; */
	  margin-top:-33%;
	  position: absolute;
	  z-index: 1000;
	} 
	
}
.spacereduceupperlabel{
margin-top:-11px
}

section {
  position: relative;
  padding-top: 37px;
  background-color: #d9d9d9; 
      overflow: auto;
    min-height: 100px;
    max-height: 685px;
    max-width: 1105px;
}
section.positioned {
  position: absolute;
  top:100px;
  left:100px;
  width:800px;
  box-shadow: 0 0 15px #333;
}

.containertables {
  /*  overflow-y: auto;
   min-height: 100px;
  max-height: 250px;
   max-width: 1105px; 
  overflow-x: hidden;  */
}
table {
  border-spacing: 0;
  width:100%;
 /*  word-wrap : break-word;
  word-break : break-word;
  word-break : break-all; */
  margin-top:0%;
  
}
tr:nth-child(odd) { background: #f9f9f9; }
tr:nth-child(even) { background: #ffffff; }
td + td {
  border-left:1px solid #eee;
  
}
td, th {
  border-bottom:1px solid #eee;
  color: #000;
  padding: 10px 4px;
}
th {
  height: 0;
  line-height: 0;
  padding-top: 0;
  padding-bottom: 0;
  color: transparent;
  border: none;
  /* white-space: nowrap; */
}
th div{
  position: absolute;
  background: transparent;
  color: #000;
  padding: 4px 4px;
  top: 0;
  margin-left: -4px;
  line-height: normal;
  border-left: 1px solid #eee;
  
}
th:first-child div{
  border: none;
}
.SequenceNameLabel{
    float: left;
    margin-right: 2%;
    position: static;
    font-size: 16px;
    font-weight:600;
}
.paddingleftside{
padding-left:5px;
}

.loader{
    margin-left: 33%;
     margin-top: 5%
}
 .show-main-div-popup-loarder {

z-index: 999;
position: absolute;
margin-top: 30%;
left: 29%;
height: 206px;
display:none;
width:65%;
}
</style>
</head>
<body data-ng-app="jsbin" data-ng-controller="ScreenLabels"
	data-ng-model="ScreenLabels" data-ng-cloak>
	<div id="loadingDiv" class="show-main-div-popup-loarder">
   <img alt="" src="<c:url value="/resourcesValidate/AngularGoJS/angular/360.GIF"/>" class="loader" />
  </div>
	<div id="myImages" class = "myImages" style="width: 100%;height: 100%;">
    </div>
	<div id="wholeconfigurator">
		<div id="toolbar">
   <h1><span data-ng-bind="ScreenLabels.smartTaskConfigurator_Configurator"></span></h1>
  </div>
       <div class="sequenceviewbuttons">
     <button class="buttontransparentclass" onclick="return sequenceviewjson()" id="Allsavebutton">
     <img class="imagesetclass" src="<c:url value="/resourcesValidate/AngularGoJS/angular/Save.png"/>" id="dagger" />
    </button>
  
   
    <button id = "resetbutton" class="buttontransparentclass" onclick="document.location.reload(true);" >
     <img class="imagesetclass" src="<c:url value="/resourcesValidate/AngularGoJS/angular/Reset.png"/>"  />
   </button>
    
  
     <button class="buttontransparentclass" onclick="return backbutton()"  id="backbutton" >
     <img class="imagesetclass" src="<c:url value="/resourcesValidate/AngularGoJS/angular/Return.png"/>"/>
    </button>   
    
   
    <a  id="sequenceviewbutton" href="#" id="popup" class="sequenceviewgridlink" onclick="return div_show();"><span data-ng-bind="ScreenLabels.smartTaskConfigurator_Sequence_View"></span></a>
   </div>
		<div class="toggler-l">
			<a href="#" snap-toggle="leftt" onclick="mylefttb('lefttb')"> <span
				id="lefttb" data-ng-bind="ScreenLabels.smartTaskConfigurator_Task"></span>
			</a> <a href="#" snap-toggle="lefte" onclick="mylefteb('lefteb')"> <span
				id="lefteb" data-ng-bind="ScreenLabels.smartTaskConfigurator_Execution"></span>
			</a>
		</div>
		<div class="toggler-r">
			<div class="texttop">
				<a href="#" snap-toggle="right" onclick="myrightab('rightab')">
					<span id="rightab" data-ng-bind="ScreenLabels.smartTaskConfigurator_Action"></span>
				</a>
			</div>
		</div>
		<snap-drawers id = "hidesanpdrawers">
		<div snap-drawer="lefte" id="lefte" onclick="mylefte()">

			<div class="actiontext">
				<h3>{{ScreenLabels.smartTaskConfigurator_Execution}}</h3>
			</div>
			<div id="myPaletteexe"
				style="display: inline-block; height: 744px; width: 100%;"></div>
		</div>

  <div snap-drawer="leftt" id="leftt" onclick="myleftt()">
   <form name="edit_header_form" data-ng-controller="ScreenLabels" data-ng-submit="EditConfigurator_save(edit_header_form.$valid)" novalidate method="POST"> 
  <div style="display:none;">
<textarea id="EXTYPE" name="ExecutionTypeget">{{HeaderData.execution_TYPE}}</textarea>
<textarea id="EXDEVICE" name="ExecutionDeviceget">{{HeaderData.execution_DEVICE}}</textarea>
<textarea id="EXGROUP" name="ExecutionGroupget">{{HeaderData.execution_SEQUENCE_GROUP}}</textarea>
<textarea id="EXAPPL" name="Applicationget">{{HeaderData.application_ID}}</textarea>
<textarea id="EXTASK" name="Taskget">{{HeaderData.task}}</textarea>
 <!-- <textarea id="LABY">{{ScreenLabels.smartTaskConfigurator_TeamMemberName}}</textarea> -->
 <input type="text"  class="form-controlwidth" id="tenant_ID" name="tenant_ID" data-ng-model="HeaderData.tenant_ID">
 <input type="text"  class="form-controlwidth" id="company_ID" name="company_ID" data-ng-model="HeaderData.company_ID">
 <textarea id="message1">{{ScreenLabels.smartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel}}</textarea>
<textarea id="message2">{{ScreenLabels.smartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link}}</textarea>
<textarea id="message3">{{ScreenLabels.smartTaskConfigurator_Start_node_already_exist}}</textarea>
<textarea id="message4">{{ScreenLabels.smartTaskConfigurator_End_node_already_exist}}</textarea>
<textarea id="message5">{{ScreenLabels.smartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one}}</textarea>
<textarea id="message6">{{ScreenLabels.smartTaskConfigurator_You_can_not_delete_Start_node}}</textarea>
<textarea id="message7">{{ScreenLabels.smartTaskConfigurator_You_can_not_delete_End_node}}</textarea>
<textarea id="message8">{{ScreenLabels.smartTaskConfigurator_You_cannot_delete_selected_link}}</textarea>
<textarea id="message9">{{ScreenLabels.smartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel}}</textarea>
<textarea id="message10">{{ScreenLabels.smartTaskConfigurator_Please_connect_every_node_via_link_before_align_data}}</textarea>
<textarea id="message11">{{ScreenLabels.smartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking}}</textarea>
<textarea id="message12">{{ScreenLabels.smartTaskConfigurator_General_Tag_already_exist_with}}</textarea>
<textarea id="message13">{{ScreenLabels.smartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name}}</textarea>
<textarea id="message14">{{ScreenLabels.smartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid}}</textarea>
<textarea id="message15">{{ScreenLabels.smartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data}}</textarea>
<textarea id="message16">{{ScreenLabels.smartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid}}</textarea>
<textarea id="message17">{{ScreenLabels.smartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid}}</textarea>
<textarea id="message18">{{ScreenLabels.smartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking}}</textarea>
<textarea id="message19">{{ScreenLabels.smartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG}}</textarea>
<textarea id="message20">{{ScreenLabels.smartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG}}</textarea>
<textarea id="message21">{{ScreenLabels.smartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel}}</textarea>
<textarea id="message22">{{ScreenLabels.smartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG}}</textarea>
<textarea id="message23">{{ScreenLabels.smartTaskConfigurator_AND}}</textarea>
<textarea id="message24">{{ScreenLabels.smartTaskConfigurator_other_nodes_not_connected}}</textarea>
<textarea id="message25">{{ScreenLabels.smartTaskConfigurator_Please_enter_Display_text}}</textarea>
<textarea id="message26">{{ScreenLabels.smartTaskConfigurator_Please_enter_Comment}}</textarea>
<textarea id="message27">{{ScreenLabels.smartTaskConfigurator_Please_enter_General_Tag_Name}}</textarea>
<textarea id="message28">{{ScreenLabels.smartTaskConfigurator_Please_enter_Custom_Execution_Path}}</textarea>
<textarea id="message29">{{ScreenLabels.smartTaskConfigurator_Custom_Execution_Path_not_valid}}</textarea>
<textarea id="message30">{{ScreenLabels.smartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION}}</textarea>
<textarea id="message31">{{ScreenLabels.smartTaskConfigurator_Please_enter_different_value_except_COMMENT}}</textarea>
<textarea id="message32">{{ScreenLabels.smartTaskConfigurator_Please_enter_different_value_except_DISPLAY}}</textarea>
<textarea id="message33">{{ScreenLabels.smartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG}}</textarea>
<textarea id="message34">{{ScreenLabels.smartTaskConfigurator_Please_do_not_left_any_node_blank}}</textarea>
<textarea id="message35">{{ScreenLabels.smartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data}}</textarea>


	<textarea id="SaveLabel">{{ScreenLabels.smartTaskConfigurator_Save}}</textarea>
<textarea id="PrintLabel">{{ScreenLabels.smartTaskConfigurator_Print}}</textarea>
<textarea id="BacklLabel">{{ScreenLabels.smartTaskConfigurator_Back}}</textarea>
<textarea id="ReloadLabel">{{ScreenLabels.smartTaskConfigurator_Cancel}}</textarea>
<textarea id="AlignLabel">{{ScreenLabels.smartTaskConfigurator_Align}}</textarea>
<textarea id="SequenceVeiwLabel">{{ScreenLabels.smartTaskConfigurator_Sequence_View}}</textarea>
<textarea id="RefreshLabel">{{ScreenLabels.smartTaskConfigurator_Refresh}}</textarea>
<textarea id="CloseLabel">{{ScreenLabels.smartTaskConfigurator_Close}}</textarea>
</div>
   <div class="actiontext">
    <h3>{{ScreenLabels.smartTaskConfigurator_Task}}</h3>
    <div>
     <div class="form-group spacereduceupperlabel" >
      <label class="col-md-2 control-label"> {{ScreenLabels.smartTaskConfigurator_Execution_Sequence_Name}}:<span class="required">*</span>
      </label>
      <div class="col-md-10">
       <input type="text" class="form-control-width" maxlength="100" name="execution_SEQUENCE_NAME" id="execution_SEQUENCE_NAME" data-ng-model="HeaderData.execution_SEQUENCE_NAME" required="true">
		  <span class="error" data-ng-show="submitted && edit_header_form.execution_SEQUENCE_NAME.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
       </div>
     </div>

     <div class="form-group">
      <label class="col-md-2 control-label"> {{ScreenLabels.smartTaskConfigurator_Execution_Type}}:<span
       class="required">*</span>
      </label>
      <div class="col-md-10">
     
       <select class="table-group-action-input form-control-width "
        data-ng-model="HeaderData.execution_TYPE" name="ExecutionType" id="ExecutionType" required="true">
        <option value="">{{ScreenLabels.smartTaskConfigurator_Select}}...</option>
        <option  data-ng-repeat="ExecutionType in ExecutionTypes"
         value="{{ExecutionType.generalCode}}">{{ExecutionType.description50}}</option>
       </select>
	   <span class="error" data-ng-show="submitted && edit_header_form.ExecutionType.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
      
      </div>
     </div>
    </div>
    <div class="form-group">
     <label class="col-md-2 control-label"> {{ScreenLabels.smartTaskConfigurator_Execution_Device}}:<span
      class="required">*</span>
     </label>
     <div class="col-md-10">
      <select class="table-group-action-input form-control-width "
       data-ng-model="HeaderData.execution_DEVICE" name="ExecutionDevice" id="ExecutionDevice" required="true">
       <option value="" selected>{{ScreenLabels.smartTaskConfigurator_Select}}...</option>
       <option data-ng-repeat="ExecutionDevice in ExecutionDevices"
        value="{{ExecutionDevice.generalCode}}">{{ExecutionDevice.description50}}</option>
      </select>
	    <span class="error" data-ng-show="submitted && edit_header_form.ExecutionDevice.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
     </div>
    </div>
   </div>
   <div class="form-group">
    <label class="col-md-2 control-label">{{ScreenLabels.smartTaskConfigurator_Execution_Group}}:<span
     class="required">*</span>
    </label>
    <div class="col-md-10">
     <select class="table-group-action-input form-control-width "
      data-ng-model="HeaderData.execution_SEQUENCE_GROUP" name="ExecutionGroup" id="ExecutionGroup" required="true">
      <option value="" selected>{{ScreenLabels.smartTaskConfigurator_Select}}...</option>
      <option data-ng-repeat="ExecutionGroup in ExecutionGroups"
       value="{{ExecutionGroup.generalCode}}">{{ExecutionGroup.description50}}</option>
     </select>
	  <span class="error" data-ng-show="submitted && edit_header_form.ExecutionGroup.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
     </div>
   </div>
   <div class="form-group">
    <label class="col-md-2 control-label"> {{ScreenLabels.smartTaskConfigurator_Description_Short}}:<span
     class="required">*</span>
    </label>
    <div class="col-md-10">
     <input type="text" class="form-control-width" maxlength="20" name="description20" data-ng-model="HeaderData.description20" required="true">
  	<span class="error" data-ng-show="submitted && edit_header_form.description20.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
     </div>
   </div>
   <div class="form-group">
    <label class="col-md-2 control-label"> {{ScreenLabels.smartTaskConfigurator_Description_Long}}:<span
     class="required">*</span>
    </label>
    <div class="col-md-10">
     <input type="text" class="form-control-width" maxlength="50" name="description50" data-ng-model="HeaderData.description50" required="true">
	  <span class="error" data-ng-show="submitted && edit_header_form.description50.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
    </div>
   </div>
   <div class="form-group">
    <label class="col-md-2 control-label">{{ScreenLabels.smartTaskConfigurator_Menu_Name}}:<span
     class="required">*</span>
    </label>
    <div class="col-md-10">
     <input type="text" class="form-control-width" maxlength="100" name="menu_OPTION" id="menu_OPTION" data-ng-model="HeaderData.menu_OPTION" required="true">
	  <span class="error" data-ng-show="submitted && edit_header_form.menu_OPTION.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
    </div>
   </div>
   <div class="form-group">
    <label class="col-md-2 control-label"> {{ScreenLabels.smartTaskConfigurator_Application}}:<span
     class="required">*</span>
    </label>
    <div class="col-md-10" >
     <select class="table-group-action-input form-control-width "
      data-ng-model="HeaderData.application_ID" name="Application" id="Application" required="true">
      <option value="" selected>{{ScreenLabels.smartTaskConfigurator_Select}}...</option>
      <option data-ng-repeat="Application in Applications"
       value="{{Application.generalCode}}">{{Application.description50}}</option>
     </select>
	   <span class="error" data-ng-show="submitted && edit_header_form.Application.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
    </div>
   </div>
   <div class="form-group">
    <label class="col-md-2 control-label">  {{ScreenLabels.smartTaskConfigurator_Task}}:<span
     class="required">*</span>
    </label>
    <div class="col-md-10 control-box">
     <select class="table-group-action-input form-control-width"
      data-ng-model="HeaderData.task" name="Task" id="Task" required="true">
      <option value="" selected>{{ScreenLabels.smartTaskConfigurator_Select}}...</option>
      <option data-ng-repeat="Task in Tasks" value="{{Task.generalCode}}">{{Task.description50}}</option>
     </select>
	 <span class="error" data-ng-show="submitted && edit_header_form.Task.$error.required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_mandatory_field_can_not_be_left_blank}}</span>
    </div>
   </div>
   <div class="form-group">
  
      <label class="col-md-2 control-label spacereduceupperlabel">{{ScreenLabels.smartTaskConfigurator_Last_Activity_Date}}:<span
       class="required">*</span>
      </label>
      <div class="col-md-10">
      
      
       <input type="text" disabled="disabled" class="form-control-width" id="last_ACTIVITY_DATE" name="last_ACTIVITY_DATE" data-ng-model="HeaderData.last_ACTIVITY_DATE">
  
      </div>
     </div>

     <div class="form-group">
      <label class="col-md-2 control-label">{{ScreenLabels.smartTaskConfigurator_Last_Activity_By}}:<span
       class="required">*</span>
      </label>
      <div class="col-md-10">
       <input type="text" disabled="disabled" class="form-control-width" name="last_ACTIVITY_TEAM_MEMBER" id="last_ACTIVITY_TEAM_MEMBER" data-ng-model="HeaderData.last_ACTIVITY_TEAM_MEMBER" >

      </div>
     </div>
   <div style="display:none;">
    <button type="submit" class="btn btn-sm btn-success"  id="tasksubmitpanel" data-ng-click="submitted=true"></button>
  </div>
   </form>
  </div>

	<div snap-drawer="right" id="righta" onclick="myrighta()">
		<div class="actiontext">
			<h3>{{ScreenLabels.smartTaskConfigurator_Action}}</h3>
		</div>

		<div id="myPalette"
			style="display: inline-block; height: 744px; width: 100%;"></div>
	</div>
	</snap-drawers>
	<snap-content snap-opt-tap-to-close="false"
		style="transform: translate3d(0, 0, 0);">
	<div>
		<div id="sample">
		   <div class="saveload">
    <button class = "btnn btn-smm" id="SaveButton" onclick="saveload()">
    <img class="imagesetclassss" src="<c:url value="/resourcesValidate/AngularGoJS/angular/Align.png"/>" id="dagger" /></button>
    <button class = "btnn btn-smm"  id="PrintButton" onclick="printImages()">
    <img class="imagesetclassss" src="<c:url value="/resourcesValidate/AngularGoJS/angular/Print.png"/>" id="dagger" /></button>
   </div>
			
				<div style="width: 60%; margin-left: 413px;">

				<span
					style="display: inline-block; vertical-align: top; width: 100%">
					<div id="myDiagram"
						style="width: 100%; height: 770px""></div>
				</span>
			</div>

			
			<textarea id="mySavedModel" >  </textarea>
  
  
            <textarea id="mysequenceModel" >  </textarea>

		</div>


	</div>

	</snap-content>

	<div class="outerwrapper" >
		<form name="signup_form" novalidate data-ng-submit="SequenceViewList()">

			<div class="cover back-cover"></div>
			<div id="show"
				class="Show_main show-main-div-popup-locationprofilemaintenance">
				<div class="form-body">
     <div id="#DivLabelCompany-sequencegrid" class="form-group">
      <label class="col-md-2 control-label" id="LabelCompany">&nbsp;&nbsp;&nbsp;{{ScreenLabels.smartTaskConfigurator_Sequence_View}}&nbsp;
        <!-- <i class="fa fa-refresh" id="sequence_icon"
       onclick="div_refresh()"></i> -->
         <!-- <span class="glyphicon glyphicon-print"  id="print_icon" onclick="printData()"></span> -->
         <!-- <i class="fa fa-print" id="print_icon" onclick="printData()"></i> -->
      </label>

     </div>
     <div class="popupbutton popup-bottom-button_LocationProfile">
      <!-- <i class="fa fa-refresh" id="sequence_icon" onclick="div_refresh()"></i> --> 
      <span class="glyphicon glyphicon-print"  id="print_icon" onclick="printData()"></span>
      <i class="fa fa-close" id="close_icon" onclick="div_hide()"></i>

     </div>
    </div>
     <div>
     <label class="SequenceNameLabel">&nbsp;&nbsp;<b>{{ScreenLabels.smartTaskConfigurator_Sequence_Name}}:</b>&nbsp;&nbsp;<span id="SequenceNameSet"></span>
     </label>
     </div>
     <br><br><br>
				<div class="upperspace panel panel-default-sequencegrid">
				<section class="">
     <div class="containertables">
    <table data-ng-table="tableParams" id="printTable">
            <thead >
       <tr class="header" >
       
        <th width="8%" >{{ScreenLabels.smartTaskConfigurator_Sequence}}
        <div  style="width: 8%;" class = "TableHeaderHide">{{ScreenLabels.smartTaskConfigurator_Sequence}}</div>
        </th>
        <th width="12%">{{ScreenLabels.smartTaskConfigurator_Execution_Sequence_Type}}
        <div style="width: 12%;" class = "TableHeaderHide">{{ScreenLabels.smartTaskConfigurator_Execution_Sequence_Type}}</div>
        </th>
       <th width="13%">{{ScreenLabels.smartTaskConfigurator_Execution_TagName}}
        <div  style="width: 13%;" class = "TableHeaderHide">{{ScreenLabels.smartTaskConfigurator_Execution_TagName}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        </th>
        <th width="10%">{{ScreenLabels.smartTaskConfigurator_Action}}
        <div  style="width: 10%;" class = "TableHeaderHide">{{ScreenLabels.smartTaskConfigurator_Action}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        </th>
        <th width="10%">{{ScreenLabels.smartTaskConfigurator_Go_To_Tag}}
        <div  style="width: 10%;" class = "TableHeaderHide">{{ScreenLabels.smartTaskConfigurator_Go_To_Tag}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        </th>
        <th width="24%">{{ScreenLabels.smartTaskConfigurator_Description}}
        <div style="width: 24%;" class = "TableHeaderHide">{{ScreenLabels.smartTaskConfigurator_Description}}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        </th>
        <th width="23%">{{ScreenLabels.smartTaskConfigurator_Message}}
        <div style="width: 23%;" class = "TableHeaderHide">{{ScreenLabels.smartTaskConfigurator_Message}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        </th>
       </tr>
      <thead>
           
           <tbody class="datawrap">
        <tr data-ng-repeat="BindConfigurator in data">
         <td style="min-width: 70px;">{{BindConfigurator.execution_Sequence}}</td>
         <td style="min-width: 100px;" >{{BindConfigurator.execution_Sequence_Type}}</td>
         <td style="min-width: 120px;">{{BindConfigurator.execution_Name}}</td>
         <td style="min-width: 80px;">{{BindConfigurator.action_Name}}</td>
         <td style="min-width: 100px;">{{BindConfigurator.goto_Tag}}</td>
         <td style="min-width: 240px;" class ="Description6">{{BindConfigurator.execution_Sequence_Type_Description}}</td>
         <td style="min-width: 240px;"class ="Message7">{{BindConfigurator.message}}</td>


        </tr>

     </tbody>
        </table>
        </div>
        </section>
				</div>

			</div>
			 <div style="display:none;">
   	<button type="submit" class="btn btn-sm btn-success"  id="sequencesubmitted" data-ng-click="submitted=true"></button>
  </div>
		</form>
	</div>
	<div>
		<form name="gojs_popup_actionone">
			<div class="cover_gojs_actionone back-cover"></div>
			<div id="show"
				class="Show_gojs_actionone_main show-gojs-popupifexit-main-div">
				<div class="form-body" style="width: 100%; height: auto;">
					<div id="DivLabelCompany" class="form-group">
						<label class="gojs-popup-head"> <span
							id="gojs-popup-actionone"></span>
						</label> <label class="gojs-popup-close"> <i class="fa fa-close"
							onclick="div_gojs_actionone_close()" title="Close"></i>
						</label>
					</div>
				</div>
				<span class="error"  id="actiononecodeerrormsg" style='display:none;margin-left:15px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please select GoTo Tag first.</span>
				<br /><input type="hidden" id="hidden_key_actionone" />
				<div class="gojs-popupifexit-upperspace panel panel-default">
					<div class="div-gojs-popupifexit-content">
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label"> {{ScreenLabels.smartTaskConfigurator_GoTo_Tag}}:<span class="required_popup">*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<select class="table-group-action-input form-control "  style="border-radius: 5px !important"
									name="product[tax_class]" id = "gototagactionone" >
								</select>
							</div>
						</div>
						
					</div>
					<!-- <button class = "btn btn-sm yellow" onclick="div_gojs_actionone_hide()">Save</button> -->
					<a href="#" class="gojssaveimg"  onclick="div_gojs_actionone_hide()"> <img 	src="<c:url value="/resourcesValidate/AngularGoJS/angular/gojspopupsave.png"/>" alt="" /></a>
				</div>
			</div>
		</form>
	</div>
	<div>
	<form name="gojs_popup_actiontwo">
			<div class="cover_gojs_actiontwo back-cover"></div>
			<div id="show"
				class="Show_gojs_actiontwo_main show-gojs-popupiferror-main-div">
				<div class="form-body" style="width: 100%; height: auto;">
					<div id="DivLabelCompany" class="form-group">
						<label class="gojs-popup-head"> <span
							id="gojs-popup-actiontwo"></span>
						</label> <label class="gojs-popup-close"> <i class="fa fa-close"
							onclick="div_gojs_actiontwo_close()" title="Close"></i>
						</label>
					</div>
				</div>
				<span class="error"  id="actiontwocodeerrormsg" style='display:none;margin-left:15px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All fields are mandatory.</span>
				 <input type="hidden" id="hidden_key_actiontwo" />
				<div class="gojs-popupiferror-upperspace panel panel-default">
					<div class="div-gojs-popupiferror-content">
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label"> {{ScreenLabels.smartTaskConfigurator_GoTo_Tag}}:<span class="required_popup">*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<select class="table-group-action-input form-control " name="gototag" id="gototagactiontwo"  style="border-radius: 5px !important">
									
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label"> {{ScreenLabels.smartTaskConfigurator_Display_Text}}:<span class="required_popup">*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<textarea class="form-control" rows="" cols="" maxlength="500" id="display_text_actiontwo"  style="border-radius: 5px !important;height : 100px !important"></textarea>
							</div>
						</div>
					</div>
					
				</div>
				<!-- <button class = "btn btn-sm yellow"  onclick="div_gojs_actiontwo_hide()">Save</button> -->
				<a href="#" class="gojssaveimg"  onclick="div_gojs_actiontwo_hide()"> <img 	src="<c:url value="/resourcesValidate/AngularGoJS/angular/gojspopupsave.png"/>" alt="" /></a>
			</div>
		</form>
	</div>
	<div>
		<form name="gojs_popup_actionthree">
			<div class="cover_gojs_actionthree back-cover"></div>
			<div id="show"
				class="Show_gojs_actionthree_main show-gojs-popupifreturncode-main-div">
				<div class="form-body" style="width: 100%; height: auto;">
					<div id="DivLabelCompany" class="form-group">
						<label class="gojs-popup-head"> <span
							id="gojs-popup-actionthree"></span>
						</label> <label class="gojs-popup-close"> <i class="fa fa-close"
							onclick="div_gojs_actionthree_close()" title="Close"></i>
						</label>
					</div>
				</div>
				<span class="error"  id="returncodeerrormsg" style='display:none;margin-left:15px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All fields are mandatory.</span>
				<br /><input type="hidden" id="hidden_key_actionthree" />
				<div class="gojs-popupifreturncode-upperspace panel panel-default">
					<div class="div-gojs-popupifreturncode-content">
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label"> {{ScreenLabels.smartTaskConfigurator_GoTo_Tag}}:<span class="required_popup" >*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<select class="table-group-action-input form-control "  name="product[tax_class]" id = "gototagactionthree"  style="border-radius: 5px !important">
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label">{{ScreenLabels.smartTaskConfigurator_Return_Code}}:<span class="required_popup">*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<input class="form-control" type="text" name="returncode" maxlength="20" id="return_code_actionthree"  style="border-radius: 5px !important">
							</div>
						</div>
					</div>
					
				</div>
				<a href="#" class="gojssaveimg"  onclick="div_gojs_actionthree_hide()"> <img 	src="<c:url value="/resourcesValidate/AngularGoJS/angular/gojspopupsave.png"/>" alt="" /></a>
			</div>
		</form>
	</div>
	<div>
		<form name="gojs_popup_prevtext">
			<div class="cover_gojs_prevtext back-cover"></div>
			<div id="show"
				class="Show_gojs_prevtext_main show-gojs-popupprevtext-main-div">
				<div class="form-body" style="width: 100%; height: auto;">
					<div id="DivLabelCompany" class="form-group">
						<label class="gojs-popup-head"> <span
							id="gojs-popup-prevtext"></span>
						</label> <label class="gojs-popup-close"> <i class="fa fa-close"
							onclick="div_gojs_prevtext_close()" title="Close"></i>
						</label>
					</div>
				</div>
				<span class="error"  id="prevtextcodeerrormsg" style='display:none;margin-left:15px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter display text.</span>
				<br /> <input type="hidden" id="hidden_key_prevtext" />
				<input type="hidden" id="hidden_key_prevtextcategory" />
				<div class="gojs-popupprevtext-upperspace panel panel-default">
					<div class="div-gojs-popupprevtext-content">
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label"> {{ScreenLabels.smartTaskConfigurator_Display_Text}}:<span class="required_popup">*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<textarea class="form-control" rows="" cols="" id="display_text_prevtext" maxlength="500" style="border-radius: 5px !important;height : 100px !important"></textarea>
							</div>
						</div>
					</div>
					
				</div>
				<!-- <button class = "btn btn-sm yellow"  onclick="div_gojs_prevtext_hide()">Save</button> -->
				<a href="#" class="gojssaveimg"  onclick="div_gojs_prevtext_hide()"> <img 	src="<c:url value="/resourcesValidate/AngularGoJS/angular/gojspopupsave.png"/>" alt="" /></a>
			</div>
		</form>
	</div>
	<div>
	<form name="gojs_popup_prevtext">
			<div class="cover_gojs_prevtexttag back-cover"></div>
			<div id="show"
				class="Show_gojs_prevtexttag_main show-gojs-popupprevtext-main-div">
				<div class="form-body" style="width: 100%; height: auto;">
					<div id="DivLabelCompany" class="form-group">
						<label class="gojs-popup-head"> <span
							id="gojs-popup-prevtexttag"></span>
						</label> <label class="gojs-popup-close"> <i class="fa fa-close"
							onclick="div_gojs_prevtexttag_close()" title="Close"></i>
						</label>
					</div>
				</div>
				<span class="error"  id="prevtextcodeerrormsgtag" style='display:none;margin-left:15px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter display text.</span>
				<br /> <input type="hidden" id="hidden_key_prevtexttag" />
				<input type="hidden" id="hidden_key_prevtexttagcategory" />
				<div class="gojs-popupprevtext-upperspace panel panel-default">
					<div class="div-gojs-popupprevtext-content">
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label"> {{ScreenLabels.smartTaskConfigurator_GENERAL_TAG_NAME}}:<span class="required_popup">*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<textarea class="form-control" rows="" cols="" id="display_text_prevtexttag" maxlength="20" style="border-radius: 5px !important;height : 100px !important"></textarea>
							</div>
						</div>
					</div>
					
				</div>
				<!-- <button class = "btn btn-sm yellow"  onclick="div_gojs_prevtext_hide()">Save</button> -->
				<a href="#" class="gojssaveimg"  onclick="div_gojs_prevtexttag_hide()"> <img 	src="<c:url value="/resourcesValidate/AngularGoJS/angular/gojspopupsave.png"/>" alt="" /></a>
			</div>
		</form>
	</div>
	<div>
		<form name="gojs_popup_prevtext">
			<div class="cover_gojs_prevtextcustexe back-cover"></div>
			<div id="show"
				class="Show_gojs_prevtextcustexe_main show-gojs-popupprevtext-main-div">
				<div class="form-body" style="width: 100%; height: auto;">
					<div id="DivLabelCompany" class="form-group">
						<label class="gojs-popup-head"> <span
							id="gojs-popup-prevtextcustexe"></span>
						</label> <label class="gojs-popup-close"> <i class="fa fa-close"
							onclick="div_gojs_prevtextcustexe_close()" title="Close"></i>
						</label>
					</div>
				</div>
				<span class="error"  id="prevtextcodeerrormsgcustexe" style='display:none;margin-left:15px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please enter display text.</span>
				<br /> <input type="hidden" id="hidden_key_prevtextcustexe" />
				<input type="hidden" id="hidden_key_prevtextcustexecategory" />
				<div class="gojs-popupprevtext-upperspace panel panel-default">
					<div class="div-gojs-popupprevtext-content">
						<div class="form-group">
							<label class="col-md-2 gojs-popup-control-label">EXECUTION PATH:<span class="required_popup">*</span>
							</label>
							<div class="col-md-10 gojs-popup-control-box">
								<textarea  class="form-control" rows="" cols="" id="display_text_prevtextcustexe" maxlength="100" style="border-radius: 5px !important;height : 100px !important"></textarea>
							</div>
						</div>
					</div>
					
				</div>
				<!-- <button class = "btn btn-sm yellow"  onclick="div_gojs_prevtext_hide()">Save</button> -->
				<a href="#" class="gojssaveimg"  onclick="div_gojs_prevtextcustexe_hide()"> <img 	src="<c:url value="/resourcesValidate/AngularGoJS/angular/gojspopupsave.png"/>" alt="" /></a>
			</div>
		</form>
	</div>
	</div>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/snap.js"/>"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/angular.min.js"/>"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/angular-snap.min.js"/>"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/gojs/go.js"/>"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/angularcontroller.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/ng-table.js"/>" type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/jquery-1.4.3.min.js"/>"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/STC_AngularJS_Controller.js"/>"></script>
	<script src="<c:url value="/resourcesValidate/AngularGoJS/angular/jquery.min.js"/>"></script>
    <script src="<c:url value="/resourcesValidate/AngularGoJS/angular/bootstrap.min.js"/>"></script>
    <!-- bootbox code -->
    <script src="<c:url value="/resourcesValidate/AngularGoJS/angular/bootbox.min.js"/>" ></script>
	<script id="code">
	function backbutton(){		
		 $.post('${pageContext.request.contextPath}/SmartTaskConfiguratorPageNameBackButton').
	     success(function(data,status) {
	     	if(data==="Search"){
	     		window.top.location.href="${pageContext.request.contextPath}/SmartTaskConfigurator_Lookup";
	     	}
	     	else{
	     		window.top.location.href="${pageContext.request.contextPath}/Smart_Task_Configurator_SearchLookup";
	     	}
	     }); 
		 
		}
	/**Declaration of global variable*/
		var varexeactions = [];
		var varexe = [];
		var varexeinexepanel = [];
		var varexeinflowchartpanel = [];
		var varexeunwanteddata;
		var vargojsshapetextsize = [];
		var vargotodropdown = [];
		var varselectnodeOSCcategory;
		var varselectnodeOSCkey;
		var varselectnodeOSCtext;
		
		var varselectlinkOSCtoport ;
		var varselectlinkOSCfromport ;
		var keygeneration;
		var vargojsjsondata;
		var oneendnode = false;
		var onestartnode = false;
		var returnsequencevalue = true;
		var returngotonodevalue = false;
		var avoidinghangloopissue = false;
		var blankdisplaytextnode = false;
		var checkexecutionnode = 0;
		var addeditcopy;
		var disconnectnodelistlen;
		var disconnectnodename;
		
		/**Get execution data on filter basis and show in execution panel*/
		function againExecutionShow(){
		    var tivalues= document.getElementById("tenant_ID").value;
		    var civalues=document.getElementById("company_ID").value;
		    var EXTYPEs = document.getElementById('EXTYPE').value;
		    var EXDEVICEs = document.getElementById('EXDEVICE').value;
		    var EXGROUPs = document.getElementById('EXGROUP').value;
		    //alert("againExecutionShow function"+tivalues+" "+civalues+" "+EXTYPEs+" "+EXDEVICEs+" "+EXGROUPs); 
		   $.post("${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_getSTExecutionsData",
		     {   
		      Tenant:tivalues,
		      Company:civalues,
		      ExecutionGroup:EXGROUPs,
		      ExecutionType:EXTYPEs,
		      ExecutionDevice:EXDEVICEs
		     },
		     function(data, status) {
		    //alert(JSON.stringify(data))
		      try{
		       varexe = [];
		       varexeinexepanel = [];
		       //document.getElementById("myPaletteexe").innerHTML = "";
		      
		       for (var nodeindex = 0; nodeindex <= data.length; nodeindex++) {
		    	   var itemssss = {};
					itemssss["category"] = "RECTANGLE";
					itemssss["text"] = data[nodeindex].description20;
					itemssss["executionName"] = data[nodeindex].id.execution_Name;
					itemssss["figure"] = "Rectangle";
					itemssss["toolTipText"] = data[nodeindex].description20+', '+data[nodeindex].description50+', '+data[nodeindex].id.execution_Name+', '+data[nodeindex].help_Line;
					varexe.push(itemssss);
					varexeinexepanel.push(data[nodeindex].id.execution_Name);
					
		       }}catch(err){}
		       ///document.getElementById("myPaletteexe").innerHTML = varexe;
		       if(varexe.length === 0)
			    	   {
		    	   bootbox.alert(document.getElementById("message1").value);
		    	   }
		              myPaletteexe.model.nodeDataArray = varexe;
		       });
		  }
		/** Get All data on load and call init function*/
		function newExecuteQuery(TenantValue,CompanyValue,ExecutionGroupValue,ExecutionTypeValue,ExecutionDeviceValue){
			   $.post('${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_getGeneralCodeDropDown?GeneralCodeId=EXEACT').
			   success(function(data,status) {
			    try{
			     for (var nodeindex = 0; nodeindex <= data.length; nodeindex++) {
			      var itemssss = {};
			      if (data[nodeindex].generalCode.toUpperCase() === 'START') {
			    	  continue;
			    	  /* itemssss["category"] = "START"; */

					} else if (data[nodeindex].generalCode.toUpperCase() === 'END') {
						continue;
						/* itemssss["category"] = "END"; */

					} else if (data[nodeindex].generalCode.toUpperCase() === 'IF EXIT') {
						itemssss["category"] = "DIAMOND";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'IF ERROR') {
						itemssss["category"] = "DIAMOND";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'COMMENT') {
						itemssss["category"] = "COMMENT";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'MESSAGE CHECK') {
						itemssss["category"] = "MESSAGE CHECK";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'IF RETURN CODE') {
						itemssss["category"] = "DIAMOND";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'CUSTOM EXECUTION') {
						itemssss["category"] = "CUSTOM EXECUTION";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'RESET DISPLAY') {
						itemssss["category"] = "RESET DISPLAY";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'RESET SCREEN') {
						itemssss["category"] = "RESET SCREEN";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'DISPLAY') {
						itemssss["category"] = "DISPLAY";

					} else if (data[nodeindex].generalCode.toUpperCase() === 'GENERAL TAG') {
						itemssss["category"] = "GENERAL TAG";

					}
					else if (data[nodeindex].generalCode.toUpperCase() === 'GOTO TAG') {
						itemssss["category"] = "GOTO TAG";

					}else
					{
						itemssss["category"] = "DEFAULT";
					}

					itemssss["text"] = data[nodeindex].generalCode;

					itemssss["toolTipText"] = data[nodeindex].helpline;

					varexeactions.push(itemssss);
			     }}catch(err){}
			     
			     if(true){
			      
			    	 /**Get execution data on filter basis and show in execution panel*/
			      $.post("${pageContext.request.contextPath}/SmartTaskConfigurator_Restfull_getSTExecutionsData",
			        {   
			         Tenant:TenantValue,
			         Company:CompanyValue,
			         ExecutionGroup:ExecutionGroupValue,
			         ExecutionType:ExecutionTypeValue,
			         ExecutionDevice:ExecutionDeviceValue
			        },
			        function(data, status) {
			        	
			        	
			     ///alert(TenantValue + CompanyValue + ExecutionGroupValue+ExecutionTypeValue +ExecutionDeviceValue);
			       //alert(JSON.stringify(data))
			         try{
			        	// alert(JSON.stringify(data));
			          varexe = [];
			          varexeinexepanel = [];
			          
			          for (var nodeindex = 0; nodeindex <= data.length; nodeindex++) {
			        	  var itemssss = {};
							itemssss["category"] = "RECTANGLE";
							itemssss["text"] = data[nodeindex].description20;
							itemssss["executionName"] = data[nodeindex].id.execution_Name;
							itemssss["figure"] = "Rectangle";
							//itemssss["toolTipText"] = 'Description Short : '+data[i].description20+'\nDescription Long : '+data[i].description50+'\nExecution Name : '+data[i].id.execution_Name+'\nHelp Line : '+data[i].help_Line;
							itemssss["toolTipText"] = data[nodeindex].description20+', '+data[nodeindex].description50+', '+data[nodeindex].id.execution_Name+', '+data[nodeindex].help_Line;
							varexe.push(itemssss);
							varexeinexepanel.push(data[nodeindex].id.execution_Name);
										          
			          }}catch(err){}
			         
			         if (true) { 
			          if(addeditcopy === 'add')
			        	  {
			        	  varexe = [];
			        	  varexeinexepanel = [];
			        	  }
			          /**Get gojs config data*/
			          $.post('${pageContext.request.contextPath}/GOJS_Restfull_getGOJSConfigProp').
			          success(function(data,status) {


			           vargojsshapetextsize = data;
			           var sequencename = '';
			           var tenantvaluegojs='';
			           var companyvaluegojs='';
			           sequencename = document.getElementById("execution_SEQUENCE_NAME").value; 
			           tenantvaluegojs = document.getElementById("tenant_ID").value; 
			           companyvaluegojs = document.getElementById("company_ID").value;
			           /**Get sequence instruction data and set data on mysavemodel id*/
			           $.post("${pageContext.request.contextPath}/gojsdata",
			           {   
			            ExecutionSequence:sequencename,
			            Tenant:tenantvaluegojs,
			            Company:companyvaluegojs
			            
			           },
			           function(data, status) {
			         
			            
			            if(!vargojsshapetextsize.isSequenceViewGrid)
			            {
			                  document.getElementById('popup').disabled=true;
			                  document.getElementById('popup').removeAttribute('href');    
			                  document.getElementById('popup').style.textDecoration = 'none';
			                  document.getElementById('popup').style.cursor = 'default';
			                  document.getElementById('popup').style.color = 'red';
			            }
						try{
			              document.getElementById("mySavedModel").value = JSON.stringify(data);        
						  }catch(err){}
			              var stMenuOption = document.getElementById("menu_OPTION").value;
			              if(!stMenuOption)
			            	  {
			            	
			            		   
			                document.getElementById("execution_SEQUENCE_NAME").value='';
							document.getElementById("execution_SEQUENCE_NAME").disabled=false;
							document.getElementById("menu_OPTION").disabled=false;
			            	  }
			              init();
			              
			              
			           });
			           
			           
			           
			          });
			          
			          
			         }
			         
			        });
			        
			      
			      
			     }
			     
			   });
		}
		
		/**This function is used to set check box selected value on time of edit and copy functionality*/
		 function SetCheckBox(){
			 var EXTYPE = document.getElementById('EXTYPE').value;
				
				
				var EXDEVICE = document.getElementById('EXDEVICE').value;
				var EXGROUP = document.getElementById('EXGROUP').value;
				var EXAPPL = document.getElementById('EXAPPL').value;
				var EXTASK = document.getElementById('EXTASK').value;
				/* var LastActivityByValue = document.getElementById('LABY').value; */
				
				var tivalue= document.getElementById("tenant_ID").value;
				 var civalue=document.getElementById("company_ID").value;
				 
				   /* document.getElementById("last_ACTIVITY_TEAM_MEMBER").value = LastActivityByValue; */   
				   
				if (EXTYPE.trim() != '') {
					 document.getElementById('ExecutionType').value = EXTYPE;	
			}
				if (EXDEVICE.trim() != '') {
					 document.getElementById('ExecutionDevice').value = EXDEVICE;	
			}
				if (EXGROUP.trim() != '') {
					 document.getElementById('ExecutionGroup').value = EXGROUP;	
			}
				if (EXAPPL.trim() != '') {
					 document.getElementById('Application').value = EXAPPL;	
			}
				if (EXTASK.trim() != '') {
					 document.getElementById('Task').value = EXTASK;	
			}
				   var stMenuOption = document.getElementById("menu_OPTION").value;
		              if(stMenuOption)
		            	  {
		            	  
					document.getElementById("menu_OPTION").disabled=true;
					document.getElementById("execution_SEQUENCE_NAME").disabled=true;
					
				}
				
		 }
		 /**This function is used to set check box selected value on time of edit and copy functionality*/ 
		 function callingExecution(){
			    var sttivalues= document.getElementById("tenant_ID").value;
			    var stcivalues=document.getElementById("company_ID").value;
			    var stEXTYPEs = document.getElementById('EXTYPE').value;
			    var stEXDEVICEs = document.getElementById('EXDEVICE').value;
			    var stEXGROUPs = document.getElementById('EXGROUP').value;
			    var stEXSEQUENCENAMEs = document.getElementById("execution_SEQUENCE_NAME").value;
			    var stMenuOption = document.getElementById("menu_OPTION").value;
				var stEXAPPLs = document.getElementById('EXAPPL').value;
				var stEXTASKs = document.getElementById('EXTASK').value;
			    //alert("calloingexecution function"+tivalues+" "+civalues+" "+EXTYPEs+" "+EXDEVICEs+" "+EXGROUPs); 
			   if(!stEXSEQUENCENAMEs || !stMenuOption)
				   {
			    if((!stEXTYPEs && !stEXDEVICEs && !stEXGROUPs)){
			    addeditcopy = 'add';
				//alert("add load");
			     newExecuteQuery('','','','','');  
			     
			    }else{
			    	addeditcopy = 'copy';
			    	//alert("add copy");
			    	newExecuteQuery(sttivalues,stcivalues,stEXGROUPs,stEXTYPEs,stEXDEVICEs);
			    	
			    }
			   }else{
					   if((!stEXTYPEs && !stEXDEVICEs && !stEXGROUPs)){
						   addeditcopy = 'add';
						     newExecuteQuery('','','','','');  
						     
						     //alert("add load");
						    }
						    else{
						     addeditcopy = 'edit';
						     //alert("change/edit/copy/add change");
						     newExecuteQuery(sttivalues,stcivalues,stEXGROUPs,stEXTYPEs,stEXDEVICEs); 
						     
						    }
				   }
			   
			   if(addeditcopy === 'add')
				{
				 document.getElementById('lefttb').click();
				
				}else if(addeditcopy === 'copy')
				{
			    document.getElementById('ExecutionType').disabled=true;
				document.getElementById('ExecutionDevice').disabled=true;
				document.getElementById('ExecutionGroup').disabled=true;
				$("#ExecutionType option[value='" +"? string:"+ stEXTYPEs +" ?"+ "']").remove();
				$("#ExecutionDevice option[value='" +"? string:"+ stEXDEVICEs +" ?"+ "']").remove();
				$("#ExecutionGroup option[value='" +"? string:"+ stEXGROUPs +" ?"+ "']").remove();
				$("#Application option[value='" +"? string:"+ stEXAPPLs +" ?"+ "']").remove();
				$("#Task option[value='" +"? string:"+ stEXTASKs +" ?"+ "']").remove();
				document.getElementById('lefttb').click();
				
				}else if(addeditcopy === 'edit'){
					
				 document.getElementById('ExecutionType').disabled=true;
					document.getElementById('ExecutionDevice').disabled=true;
					document.getElementById('ExecutionGroup').disabled=true;	
					$("#ExecutionType option[value='" +"? string:"+ stEXTYPEs +" ?"+ "']").remove();
					$("#ExecutionDevice option[value='" +"? string:"+ stEXDEVICEs +" ?"+ "']").remove();
					$("#ExecutionGroup option[value='" +"? string:"+ stEXGROUPs +" ?"+ "']").remove();
					$("#Application option[value='" +"? string:"+ stEXAPPLs +" ?"+ "']").remove();
					$("#Task option[value='" +"? string:"+ stEXTASKs +" ?"+ "']").remove();
					document.getElementById('lefteb').click();	
				}
			   document.getElementById('rightab').click();
			  }
		 /**This function is used to set tooltips on button hover*/
		 function setTitleOnButton(){
			    
			    document.getElementById("Allsavebutton").title=document.getElementById("SaveLabel").value;
			    document.getElementById("resetbutton").title=document.getElementById("ReloadLabel").value;
			    document.getElementById("backbutton").title=document.getElementById("BacklLabel").value;
			    document.getElementById("sequenceviewbutton").title=document.getElementById("SequenceVeiwLabel").value;
			    document.getElementById("SaveButton").title=document.getElementById("AlignLabel").value;
			    document.getElementById("PrintButton").title=document.getElementById("PrintLabel").value;
			    
			    /* document.getElementById("sequence_icon").title=document.getElementById("RefreshLabel").value; */
			    document.getElementById("close_icon").title=document.getElementById("CloseLabel").value;
			    document.getElementById("print_icon").title=document.getElementById("PrintLabel").value;
			    
			    

			   }
			setTitleOnButton();
			
			function waiter() {
			    document.body.style.cursor = 'wait';

			   }
			   function waiterRemove() {
			    document.body.style.cursor = 'default';

			   }
			
		$(document).ready(function() {
			   waiter();
			   $('#loadingDiv').show();
			   $('.cover').show();
			      setTimeout(function(){ SetCheckBox();}, 3000);
			      setTimeout(function(){ callingExecution();}, 3000);
			                      $('#myImages').hide();
			       $('#mySavedModel').hide();
			       $('#mysequenceModel').hide();
			       $('#resetbutton').disabled=true;
			       setTimeout(function(){ 
			         waiterRemove();
			         $('#loadingDiv').hide();
			         $('.cover').hide();
			        },3000); 
			      });
		
		//This function is used to initialize gojs flow chart functionlaity and load all data of flow chart onload
		function init() {

			var $ = go.GraphObject.make; // for conciseness in defining templates

			myDiagram = $(go.Diagram, "myDiagram", // must name or refer to the DIV HTML element
			{
				initialContentAlignment : go.Spot.TopCenter,
				allowDrop : true, // must be true to accept drops from the Palette
				"LinkDrawn" : showLinkLabel, // this DiagramEvent listener is defined below
				"LinkRelinked" : showLinkLabel,
				"animationManager.duration" : 800, // slightly longer than default (600ms) animation
				"undoManager.isEnabled" : true	// enable undo & redo
			});

			// when the document is modified, add a "*" to the title and enable the "Save" button
			myDiagram.addDiagramListener("Modified", function(e) {
				var button = document.getElementById("SaveButton");
				/* if (button)
					button.disabled = !myDiagram.isModified; */
				var idx = document.title.indexOf("*");
				if (myDiagram.isModified) {
					if (idx < 0)
						document.title += "*";
				} else {
					if (idx >= 0)
						document.title = document.title.substr(0, idx);
				}
			});

			// helper definitions for node templates

			function nodeStyle() {
				return [
						// The Node.location comes from the "loc" property of the node data,
						// converted by the Point.parse static method.
						// If the Node.location is changed, it updates the "loc" property of the node data,
						// converting back using the Point.stringify static method.
						new go.Binding("location", "loc", go.Point.parse)
								.makeTwoWay(go.Point.stringify), {
							// the Node.location is at the center of each node
							locationSpot : go.Spot.Center,
							//isShadowed: true,
							//shadowColor: "#888",
							// handle mouse enter/leave events to show/hide the ports
							mouseEnter : function(e, obj) {
								showPorts(obj.part, true);
							},
							mouseLeave : function(e, obj) {
								showPorts(obj.part, false);
							}
						} ];
			}

			// Define a function for creating a "port" that is normally transparent.
			// The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
			// and where the port is positioned on the node, and the boolean "output" and "input" arguments
			// control whether the user can draw links from or to the port.
			function makePort(name, spot, output, input) {
				// the port is basically just a small circle that has a white stroke when it is made visible
				return $(go.Shape, "Circle", {
					fill : "transparent",
					stroke : null, // this is changed to "white" in the showPorts function
					desiredSize : new go.Size(8, 8),
					alignment : spot,
					alignmentFocus : spot, // align the port on the main Shape
					portId : name, // declare this object to be a "port"
					fromSpot : spot,
					toSpot : spot, // declare where links may connect at this port
					fromLinkable : output,
					toLinkable : input, // declare whether the user may draw links to/from here
					fromMaxLinks : 1,
					toMaxLinks : 1,// declare whether the user may draw links to/from here
					cursor : "pointer" // show a different cursor to indicate potential link point
				});
			}

			// define the Node templates for regular nodes

			var lightText = '#FFFFFF';

			//This node template create for Every EXECUTION node and Some Actions
			myDiagram.nodeTemplateMap.add("RECTANGLE", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Rectangle", {
				fill : "#959595",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight),
				maxSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight),

			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 5,
				maxSize : new go.Size(vargojsshapetextsize.rectangleTextWidth,
						vargojsshapetextsize.rectangleTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /* makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));
			//This node template create for DEFAULT Action
			myDiagram.nodeTemplateMap.add("DEFAULT", // the default category
					$(go.Node, "Spot", nodeStyle(),
					// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
					$(go.Panel, "Auto", $(go.Shape, "Rectangle", {
						fill : "#959595",
						stroke : null,
						minSize : new go.Size(vargojsshapetextsize.rectangleWidth,
								vargojsshapetextsize.rectangleHeight),
						maxSize : new go.Size(vargojsshapetextsize.rectangleWidth,
								vargojsshapetextsize.rectangleHeight)
					}, new go.Binding("figure", "figure")),

					$(go.TextBlock, {
						font : vargojsshapetextsize.textFontSizeForActionPanel,
						stroke : lightText,
						margin : 4,
						maxSize : new go.Size(vargojsshapetextsize.rectangleTextWidth,
								vargojsshapetextsize.rectangleTextHeight),
						minSize : new go.Size(vargojsshapetextsize.minTextWidth,
								vargojsshapetextsize.minTextHeight),
						wrap : go.TextBlock.WrapFit,
						overflow : go.TextBlock.OverflowEllipsis,
						maxLines :2,
						editable : false,
						isMultiline : true,
						textAlign : "center"
					}, new go.Binding("text", "text").makeTwoWay())),

					// four named ports, one on each side:
					makePort("T", go.Spot.Top, false, true), /* makePort("L",
							go.Spot.Left, true, true), makePort("R", go.Spot.Right,
							true, true), */ makePort("B", go.Spot.Bottom, true, false), {
						toolTip : // define a tooltip for each node that displays the color as text
						$(go.Adornment, "Auto", $(go.Shape, {
							fill : "#ffffff",
							minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
									vargojsshapetextsize.minToolTipHeight),
							maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
									vargojsshapetextsize.maxToolTipHeight),
							figure : "RoundedRectangle",
							stroke : "green",
							strokeWidth : 1
						}), $(go.TextBlock, {
							margin : 8,
							wrap : go.TextBlock.WrapFit,
							maxSize : new go.Size(280,380),
							minSize : new go.Size(vargojsshapetextsize.minTextWidth,
									vargojsshapetextsize.minTextHeight)
						}, new go.Binding("text", "toolTipText")))
					// end of Adornment
					}));
			
			//This node template create for START Action
			myDiagram.nodeTemplateMap.add("START", $(go.Node, "Spot",
					nodeStyle(), $(go.Panel, "Auto", $(go.Shape, "Circle", {
						minSize : new go.Size(vargojsshapetextsize.circleWidth,
								vargojsshapetextsize.circleHeight),
						maxSize : new go.Size(vargojsshapetextsize.circleWidth,
								vargojsshapetextsize.circleHeight),
						fill : "#8dc63f",
						stroke : null
					}), $(go.TextBlock,{
						margin : 5,
						font : vargojsshapetextsize.textFontSizeForActionPanel,
						stroke : lightText
					}, new go.Binding("text", "text").makeTwoWay())),
					// three named ports, one on each side except the top, all output only:
					/* makePort("L", go.Spot.Left, true, false), makePort("R",
							go.Spot.Right, true, false), */ makePort("B",
							go.Spot.Bottom, true, false),{
								toolTip : // define a tooltip for each node that displays the color as text
									$(go.Adornment, "Auto", $(go.Shape, {
										fill : "#ffffff",
										minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
												vargojsshapetextsize.minToolTipHeight),
										maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
												vargojsshapetextsize.maxToolTipHeight),
										figure : "RoundedRectangle",
										stroke : "green",
										strokeWidth : 1
									}), $(go.TextBlock, {
										margin : 8,
										wrap : go.TextBlock.WrapFit,
										maxSize : new go.Size(280,380),
										minSize : new go.Size(vargojsshapetextsize.minTextWidth,
												vargojsshapetextsize.minTextHeight)
									}, new go.Binding("text", "toolTipText")))
								// end of Adornment
								}
							));
			//This node template create for END Action
			myDiagram.nodeTemplateMap.add("END", $(go.Node, "Spot",
					nodeStyle(), $(go.Panel, "Auto", $(go.Shape, "Circle", {
						minSize : new go.Size(vargojsshapetextsize.circleWidth,
								vargojsshapetextsize.circleHeight),
						maxSize : new go.Size(vargojsshapetextsize.circleWidth,
								vargojsshapetextsize.circleHeight),
						fill : "#ff0000",
						stroke : null
					}), $(go.TextBlock,{
						margin : 5,
						font : vargojsshapetextsize.textFontSizeForActionPanel,
						stroke : lightText
					}, new go.Binding("text", "text").makeTwoWay())),
					// three named ports, one on each side except the bottom, all input only:
					makePort("T", go.Spot.Top, false, true), makePort("R",
							go.Spot.Right, false, false),{
						toolTip : // define a tooltip for each node that displays the color as text
							$(go.Adornment, "Auto", $(go.Shape, {
								fill : "#ffffff",
								minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
										vargojsshapetextsize.minToolTipHeight),
								maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
										vargojsshapetextsize.maxToolTipHeight),
								figure : "RoundedRectangle",
								stroke : "green",
								strokeWidth : 1
							}), $(go.TextBlock, {
								margin : 8,
								wrap : go.TextBlock.WrapFit,
								maxSize : new go.Size(280,380),
								minSize : new go.Size(vargojsshapetextsize.minTextWidth,
										vargojsshapetextsize.minTextHeight)
							}, new go.Binding("text", "toolTipText")))
						// end of Adornment
						}
							));
			//This node template create for COMMENT Action
			myDiagram.nodeTemplateMap.add("COMMENT", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "File", {
				fill : "#959595",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight),
				maxSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight)
			}, new go.Binding("figure", "figure")), $(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 5,
				maxSize : new go.Size(vargojsshapetextsize.rectangleTextWidth,
						vargojsshapetextsize.rectangleTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true),/*  makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));

			myDiagram.nodeTemplateMap.add("DISPLAY", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Display", {
				fill : "#959595",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.displayWidth,
						vargojsshapetextsize.displayHeight),
				maxSize : new go.Size(vargojsshapetextsize.displayWidth,
						vargojsshapetextsize.displayHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 5,
				maxSize : new go.Size(vargojsshapetextsize.displayTextWidth,
						vargojsshapetextsize.displayTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /*  makePort("L",
					go.Spot.Left, false, false),makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false),  {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));
			//This node template create for ERRORDISPALY Action
			myDiagram.nodeTemplateMap.add("ERRORDISPLAY", // the default category
					$(go.Node, "Spot", nodeStyle(),
					// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
					$(go.Panel, "Auto", $(go.Shape, "Display", {
						fill : "#959595",
						stroke : null,
						minSize : new go.Size(vargojsshapetextsize.displayWidth,
								vargojsshapetextsize.displayHeight),
						maxSize : new go.Size(vargojsshapetextsize.displayWidth,
								vargojsshapetextsize.displayHeight)
					}, new go.Binding("figure", "figure")),

					$(go.TextBlock, {
						font : vargojsshapetextsize.textFontSizeForActionPanel,
						stroke : lightText,
						margin : 4,
						maxSize : new go.Size(vargojsshapetextsize.displayTextWidth,
								vargojsshapetextsize.displayTextHeight),
						minSize : new go.Size(vargojsshapetextsize.minTextWidth,
								vargojsshapetextsize.minTextHeight),
						wrap : go.TextBlock.WrapFit,
						overflow : go.TextBlock.OverflowEllipsis,
						maxLines :2,
						editable : false,
						isMultiline : true,
						textAlign : "center"
					}, new go.Binding("text", "text").makeTwoWay())),

					// four named ports, one on each side:
					makePort("T", go.Spot.Top, false, false),  makePort("L",
							go.Spot.Left, false, false),/* makePort("R", go.Spot.Right,
							true, true),  makePort("B", go.Spot.Bottom, true, false),*/  {
						toolTip : // define a tooltip for each node that displays the color as text
						$(go.Adornment, "Auto", $(go.Shape, {
							fill : "#ffffff",
							minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
									vargojsshapetextsize.minToolTipHeight),
							maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
									vargojsshapetextsize.maxToolTipHeight),
							figure : "RoundedRectangle",
							stroke : "green",
							strokeWidth : 1
						}), $(go.TextBlock, {
							margin : 8,
							wrap : go.TextBlock.WrapFit,
							maxSize : new go.Size(280,380),
							minSize : new go.Size(vargojsshapetextsize.minTextWidth,
									vargojsshapetextsize.minTextHeight)
						}, new go.Binding("text", "toolTipText")))
					// end of Adornment
					}));

			//This node template create for GENERAL TAG Action
			myDiagram.nodeTemplateMap.add("GENERAL TAG", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Circle", {
				fill : "#959595",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.circleWidth,
						vargojsshapetextsize.circleHeight),
				maxSize : new go.Size(vargojsshapetextsize.circleWidth,
						vargojsshapetextsize.circleHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 12,
				maxSize : new go.Size(vargojsshapetextsize.circleTextWidth,
						vargojsshapetextsize.circleTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true),
			//makePort("L", go.Spot.Left, true, true),
			makePort("R", go.Spot.Right, false, false),
			makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));
			//This node template create for any circle node
			myDiagram.nodeTemplateMap.add("CIRCLE", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Circle", {
				fill : "#959595",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.circleWidth,
						vargojsshapetextsize.circleHeight),
				maxSize : new go.Size(vargojsshapetextsize.circleWidth,
						vargojsshapetextsize.circleHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 12,
				maxSize : new go.Size(vargojsshapetextsize.circleTextWidth,
						vargojsshapetextsize.circleTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /* makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));
			//This node template create for IF Error, If Exit and If Return Code Actions
			myDiagram.nodeTemplateMap.add("DIAMOND", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Diamond", {
				fill : "#959595",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.diamondWidth,
						vargojsshapetextsize.diamondHeight),
				maxSize : new go.Size(vargojsshapetextsize.diamondWidth,
						vargojsshapetextsize.diamondHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 4,
				maxSize : new go.Size(vargojsshapetextsize.diamondTextWidth,
						vargojsshapetextsize.diamondTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :4,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /* makePort("L",
					go.Spot.Left, true, true),  */ makePort("R", go.Spot.Right,
					false, false),makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));
			//This node template create for GOTO TAG Action
			myDiagram.nodeTemplateMap.add("GOTO TAG", // the default category
					$(go.Node, "Spot", nodeStyle(),
					// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
					$(go.Panel, "Auto", $(go.Shape, "SQUARE", {
						fill : "#959595",
						stroke : null,
						minSize : new go.Size(vargojsshapetextsize.squareWidth,
								vargojsshapetextsize.squareHeight),
						maxSize : new go.Size(vargojsshapetextsize.squareWidth,
								vargojsshapetextsize.squareHeight)
					}, new go.Binding("figure", "figure")),

					$(go.TextBlock, {
						font : vargojsshapetextsize.textFontSizeForActionPanel,
						stroke : lightText,
						margin : 4,
						maxSize : new go.Size(vargojsshapetextsize.squareTextWidth,
								vargojsshapetextsize.squareTextHeight),
						minSize : new go.Size(vargojsshapetextsize.minTextWidth,
								vargojsshapetextsize.minTextHeight),
						wrap : go.TextBlock.WrapFit,
						overflow : go.TextBlock.OverflowEllipsis,
						maxLines :2,
						editable : false,
						isMultiline : true,
						textAlign : "center"
					}, new go.Binding("text", "text").makeTwoWay())),

					// four named ports, one on each side:
					makePort("T", go.Spot.Top, false, true), /* makePort("L",
							go.Spot.Left, true, true), */  makePort("R", go.Spot.Right,
							false, false), /* makePort("B", go.Spot.Bottom, true, false), */ {
						toolTip : // define a tooltip for each node that displays the color as text
						$(go.Adornment, "Auto", $(go.Shape, {
							fill : "#ffffff",
							minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
									vargojsshapetextsize.minToolTipHeight),
							maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
									vargojsshapetextsize.maxToolTipHeight),
							figure : "RoundedRectangle",
							stroke : "green",
							strokeWidth : 1
						}), $(go.TextBlock, {
							margin : 8,
							wrap : go.TextBlock.WrapFit,
							maxSize : new go.Size(280,380),
							minSize : new go.Size(vargojsshapetextsize.minTextWidth,
									vargojsshapetextsize.minTextHeight)
						}, new go.Binding("text", "toolTipText")))
					// end of Adornment
					}));
			//This node template create for MESSAGE CHECK Action
			myDiagram.nodeTemplateMap.add("MESSAGE CHECK", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Rectangle", {
				fill : "#959595",
				stroke : "#6e6e6e",
				strokeWidth : 4,
				minSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight),
				maxSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 4,
				maxSize : new go.Size(vargojsshapetextsize.rectangleTextWidth,
						vargojsshapetextsize.rectangleTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /* makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));
			//This node template create for CUSTOM EXECUTION Action
			myDiagram.nodeTemplateMap.add("CUSTOM EXECUTION", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Rectangle", {
				fill : "#8dc63f",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight),
				maxSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 4,
				maxSize : new go.Size(vargojsshapetextsize.rectangleTextWidth,
						vargojsshapetextsize.rectangleTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /* makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));
			//This node template create for RESET SCREEN Action
			myDiagram.nodeTemplateMap.add("RESET SCREEN", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "RoundedRectangle", {
				fill : "#959595",
				stroke : null,
				minSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight),
				maxSize : new go.Size(vargojsshapetextsize.rectangleWidth,
						vargojsshapetextsize.rectangleHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 4,
				maxSize : new go.Size(vargojsshapetextsize.rectangleTextWidth,
						vargojsshapetextsize.rectangleTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /* makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));

			//This node template create for RESET DISPLAY Action 
			myDiagram.nodeTemplateMap.add("RESET DISPLAY", // the default category
			$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto", $(go.Shape, "Display", {
				fill : "#959595",
				stroke : "#6e6e6e",
				strokeWidth : 3,
				minSize : new go.Size(vargojsshapetextsize.displayWidth,
						vargojsshapetextsize.displayHeight),
				maxSize : new go.Size(vargojsshapetextsize.displayWidth,
						vargojsshapetextsize.displayHeight)
			}, new go.Binding("figure", "figure")),

			$(go.TextBlock, {
				font : vargojsshapetextsize.textFontSizeForActionPanel,
				stroke : lightText,
				margin : 4,
				maxSize : new go.Size(vargojsshapetextsize.displayTextWidth,
						vargojsshapetextsize.displayTextHeight),
				minSize : new go.Size(vargojsshapetextsize.minTextWidth,
						vargojsshapetextsize.minTextHeight),
				wrap : go.TextBlock.WrapFit,
				overflow : go.TextBlock.OverflowEllipsis,
				maxLines :2,
				editable : false,
				isMultiline : true,
				textAlign : "center"
			}, new go.Binding("text", "text").makeTwoWay())),

			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), /* makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), */ makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : 8,
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(280,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}));

			// replace the default Link template in the linkTemplateMap
			myDiagram.linkTemplate = $(go.Link, // the whole link panel
			{
				routing : go.Link.AvoidsNodes,
				curve : go.Link.JumpOver,
				corner : 5,
				toShortLength : 4,
				relinkableFrom : true,
				relinkableTo : true,
				reshapable : true,

			}, new go.Binding("points").makeTwoWay(), $(go.Shape, // the link path shape
			{
				isPanelMain : true,
				stroke : "gray",
				strokeWidth : 2
			}), $(go.Shape, // the arrowhead
			{
				toArrow : "standard",
				stroke : null,
				fill : "gray"
			}), $(go.Panel, "Auto", // the link label, normally not visible
			{
				visible : false,
				name : "LABEL",
				segmentIndex : 2,
				segmentFraction : 0.5
			}, new go.Binding("visible", "visible").makeTwoWay(), $(go.Shape,
					"RoundedRectangle", // the label shape
					{
						fill : "#F8F8F8",
						stroke : null
					}), $(go.TextBlock, "Yes", // the label
			{
				textAlign : "center",
				font : "10pt helvetica, arial, sans-sedrif",
				stroke : "#333333",
				editable : false
			}, new go.Binding("text", "text").makeTwoWay()),
			// four named ports, one on each side:
			makePort("T", go.Spot.Top, false, true), makePort("L",
					go.Spot.Left, true, true), makePort("R", go.Spot.Right,
					true, true), makePort("B", go.Spot.Bottom, true, false), {
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(40, 20),
					figure : "RoundedRectangle",
					stroke : "green"
				}), $(go.TextBlock, {
					margin : 4
				}, new go.Binding("text", "color")))
			// end of Adornment
			}));

//This function is used to monitor that by link, which two node connected
// And Restrict link drawn between Start and End node
          myDiagram.addDiagramListener("LinkDrawn",
				      function(e) {
				        var part = e.subject.part;
				        if (!(part instanceof go.Node))
				        	{
				        	var newjsondata = myDiagram.model.toJson();
							var vWprdsnew = JSON.parse(newjsondata);
							//alert(JSON.stringify(vWprdsnew));
							var lenlinkdataarray = vWprdsnew.linkDataArray.length-1;
							var lennodedataarray = vWprdsnew.nodeDataArray.length-1;
							var endkey;
							var startkey = "1";
							for(var nodeindex = 0; nodeindex <= lennodedataarray; nodeindex++)
								{
								//alert(vWprdsnew.nodeDataArray[nodeindex].category +'===='+'END');
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'END')
								    {										
									endkey = vWprdsnew.nodeDataArray[nodeindex].key;
									}
								}
							if((vWprdsnew.linkDataArray[lenlinkdataarray].from ===  startkey) && (vWprdsnew.linkDataArray[lenlinkdataarray].to ===  endkey))
								{
								bootbox.alert(document.getElementById("message2").value);
								myDiagram.rollbackTransaction();
								return false ;
								}
				        	}
				      });
				      //This function is used to check selection diagram part is copied or not, if copied then rollback transaction
				      myDiagram.addDiagramListener("SelectionCopied",
						      function(e) {
						        		myDiagram.rollbackTransaction();
										return false ;
										}
						      );
				    //This function is used to check clipboard is changed or not, if changed then rollback transaction
				      myDiagram.addDiagramListener("ClipboardChanged",
						      function(e) {
						        		myDiagram.rollbackTransaction();
										return false ;
										}
						      );
				    //This function is used to check clipboard is paste any thing or not, if paste then rollback transaction
				      myDiagram.addDiagramListener("ClipboardPasted",
						      function(e) {
						        		myDiagram.rollbackTransaction();
										return false ;
										}
					      );
				      
		    //This function is used to create key and set tooltip and executionname value on drag and drop external object.		  
			 myDiagram.addDiagramListener("ExternalObjectsDropped",
				      function(e) {
				        var part = e.subject.part;
				        if (!(part instanceof go.Link))
				        	{
				        	var newjsondata = myDiagram.model.toJson();
							var vWprdsnew = JSON.parse(newjsondata);
							//alert(JSON.stringify(vWprdsnew));
							
							/**Get Max key*/
							var maxnodekeyvalue = 0;
							for(var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
												{
												var comparekeyvalue = parseInt(vWprdsnew.nodeDataArray[nodeindex].key);
												
												if(maxnodekeyvalue < comparekeyvalue)
													{
													maxnodekeyvalue = comparekeyvalue;
													}														
											
											     }
							var lendataarray = vWprdsnew.nodeDataArray.length-1;
							
								if(vWprdsnew.nodeDataArray[lendataarray].category === 'START')
									{
									bootbox.alert(document.getElementById("message3").value);
									myDiagram.rollbackTransaction();
									return false ;
									}
								else if(vWprdsnew.nodeDataArray[lendataarray].category === 'END')
								    {
										
									bootbox.alert(document.getElementById("message4").value);
										myDiagram.rollbackTransaction();
										return false ; 
									}
									else
									{
										var strkey = maxnodekeyvalue + 1
										vWprdsnew.nodeDataArray[lendataarray]["key"] = strkey.toString();
										if(vWprdsnew.nodeDataArray[lendataarray].category === 'RECTANGLE')
										{
											var tooltipStr = vWprdsnew.nodeDataArray[lendataarray].toolTipText.split(', ');
											vWprdsnew.nodeDataArray[lendataarray]["toolTipText"] = tooltipStr[1].trim();
										}
										else if(vWprdsnew.nodeDataArray[lendataarray].category === 'GENERAL TAG' || vWprdsnew.nodeDataArray[lendataarray].category === 'COMMENT' || vWprdsnew.nodeDataArray[lendataarray].category === 'CUSTOM EXECUTION' || vWprdsnew.nodeDataArray[lendataarray].category === 'DISPLAY')
										{
											vWprdsnew.nodeDataArray[lendataarray]["toolTipText"] = '';
											vWprdsnew.nodeDataArray[lendataarray]["text"] = '';
										}
										else
										{
											vWprdsnew.nodeDataArray[lendataarray]["toolTipText"] = vWprdsnew.nodeDataArray[lendataarray].text;
										}
										
									
										/* if(vWprdsnew.nodeDataArray[lendataarray].category === 'GENERAL TAG')
											{
											var textvalue = vWprdsnew.nodeDataArray[lendataarray].text;
											var keyvalue = vWprdsnew.nodeDataArray[lendataarray].key;
											var categoryvalue = vWprdsnew.nodeDataArray[lendataarray].category;
											
											var oldjsondata = document.getElementById("mySavedModel").value;
											var vWordsold = JSON.parse(oldjsondata);
											
											for(var nodeindex = 0; nodeindex < vWordsold.nodeDataArray.length; nodeindex++)
												{
												var oldtext = vWordsold.nodeDataArray[nodeindex].text;
												if(oldtext.toUpperCase() === textvalue.toUpperCase())
													{
													bootbox.alert(document.getElementById("message12").value +' "'+ textvalue +'" '+document.getElementById("message13").value);
													myDiagram.rollbackTransaction();
													return false;
												    }
											
											
											
											}
									} */
										var IsdisabledTypeDeviceGroup = 0;
										for(var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
											{
											if(vWprdsnew.nodeDataArray[nodeindex].category === 'RECTANGLE')
											{
												IsdisabledTypeDeviceGroup = 1;	
											}
											
											}
										if(IsdisabledTypeDeviceGroup === 1)
											{
											document.getElementById('ExecutionType').disabled=true;
											document.getElementById('ExecutionDevice').disabled=true;
											document.getElementById('ExecutionGroup').disabled=true;											
											}else{
												
												document.getElementById('ExecutionType').disabled=false;
												document.getElementById('ExecutionDevice').disabled=false;
												document.getElementById('ExecutionGroup').disabled=false;
											}
							document.getElementById("mySavedModel").value = JSON.stringify(vWprdsnew);
							//alert(myDiagram.model.toJson());
							myDiagram.rollbackTransaction();
						    myDiagram.isModified = false;
						    //alert(JSON.stringify(vWprdsnew));
							myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
				        	}
				      }});
			 myDiagram.addDiagramListener("ObjectSingleClicked",
				      function(e) {
				        var part = e.subject.part;
				        if (!(part instanceof go.Link))
				        	{
				        	varselectnodeOSCcategory = part.data.category;
				        	varselectnodeOSCkey = part.data.key;
				        	varselectnodeOSCtext = part.data.text;
				        	}else{
				        		varselectlinkOSCtoport = part.data.toPort;
				        		varselectlinkOSCfromport =  part.data.fromPort;
				        	}
				        	
				      });
			myDiagram.addDiagramListener("SelectionDeleted", function(e) {

								
				//alert('wherer is start node');
				
				var newjsondata = myDiagram.model.toJson();
				var vWprdsnew = JSON.parse(newjsondata);
				
				
				var oldjsondata = document.getElementById("mySavedModel").value;
				var vWprdsold = JSON.parse(oldjsondata);
				//alert(vWprdsnew.nodeDataArray.length+ '<'+ (vWprdsold.nodeDataArray.length - 1));
				if(vWprdsnew.nodeDataArray.length < (vWprdsold.nodeDataArray.length - 1) )
					{
					//alert('Where is end node');
					bulknodedeleted();
                    load();
                    savechange();
					
					}
				else {
				if(varselectnodeOSCcategory === 'ERRORDISPLAY')
					{
					    //alert("You can delete selected node");
					    savechange();
					    load();
						
					}
				else if(varselectnodeOSCcategory === 'START')
					{
					bootbox.alert(document.getElementById("message6").value);
					    myDiagram.rollbackTransaction();
						varselectnodeOSCcategory = null ;
						varselectnodeOSCkey = '0';
						varselectnodeOSCtext = null;
						varselectlinkOSCtoport = null;
						varselectlinkOSCfromport = null ;

						return false ;
						
					}
				else if(varselectnodeOSCcategory === 'END')
				{
					bootbox.alert(document.getElementById("message7").value);
					myDiagram.rollbackTransaction();
					varselectnodeOSCcategory = null ;
					varselectnodeOSCkey = '0';
					varselectnodeOSCtext = null;
					varselectlinkOSCtoport = null;
					varselectlinkOSCfromport = null ;
					return false ;
					
				}
				 else if (varselectnodeOSCcategory === 'DIAMOND' || varselectnodeOSCcategory === 'GENERAL TAG' )
					 {
					 deleteDiamondTAGNode();
					 }
				try{
				if ((varselectlinkOSCtoport === 'R')|| (varselectlinkOSCfromport === 'R'))
				{
				bootbox.alert(document.getElementById("message8").value);
				myDiagram.rollbackTransaction();
				varselectnodeOSCcategory = null ;
				varselectnodeOSCkey = '0';
				varselectnodeOSCtext = null;
				varselectlinkOSCtoport = null;
				varselectlinkOSCfromport = null ;
				return false;
				}else 
					{
						savechange();
					    load();
					}
				}catch(err){}
				
					}
				
				varselectnodeOSCcategory = null ;
				varselectnodeOSCkey = 0;
				varselectnodeOSCtext = null;
				varselectlinkOSCtoport = null;
				varselectlinkOSCfromport = null ;
				
				var IsdisabledTypeDeviceGroup = 0;
				for(var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
					{
					if(vWprdsnew.nodeDataArray[nodeindex].category === 'RECTANGLE')
					{
						IsdisabledTypeDeviceGroup = 1;	
					}
					
					}
				if(IsdisabledTypeDeviceGroup === 1)
					{
					document.getElementById('ExecutionType').disabled=true;
					document.getElementById('ExecutionDevice').disabled=true;
					document.getElementById('ExecutionGroup').disabled=true;											
					}else{
						
						document.getElementById('ExecutionType').disabled=false;
						document.getElementById('ExecutionDevice').disabled=false;
						document.getElementById('ExecutionGroup').disabled=false;
					}
	
				savechange();
			    load();

			    
			});

			//This function is used to check right clicked on which obect node and open popup on that perticular node 
			myDiagram.addDiagramListener("ObjectContextClicked", function(e) {
				
				var part = e.subject.part;
				if (!(part instanceof go.Link))
	        	{
	        	varselectnodeOSCcategory = part.data.category;
	        	if(varselectnodeOSCcategory.toUpperCase() === 'START' || varselectnodeOSCcategory.toUpperCase() === 'END' || varselectnodeOSCcategory.toUpperCase() === 'MESSAGE CHECK' || varselectnodeOSCcategory.toUpperCase() === 'RESET DISPLAY' || varselectnodeOSCcategory.toUpperCase() === 'RESET SCREEN' || varselectnodeOSCcategory.toUpperCase() === 'DEFAULT')
	        		{
	        	varselectnodeOSCkey = part.data.key;
	        	varselectnodeOSCtext = part.data.text;
	        	}else{
					varselectnodeOSCcategory = null ;
					varselectnodeOSCkey = '0';
					varselectnodeOSCtext = null;
	        	}
	        	}else{
	        		varselectlinkOSCtoport = part.data.toPort;
	        		varselectlinkOSCfromport =  part.data.fromPort;
	        	}
				
				
				var keyvalue = part.data.key;
				var textvalue = part.data.text;
				var figurevalue = part.data.figure;
				var tooltipvalue = part.data.color;
				var categoryvalue = part.data.category;

				var newjsondatagoto = myDiagram.model.toJson();
				var vWprdsgoto = JSON.parse(newjsondatagoto);
				var selectederrordisplaytext;
				var selectedgototokey;
				for(var linkindex = 0; linkindex < vWprdsgoto.linkDataArray.length; linkindex++)
					{
					 
			    	 var nodekey = vWprdsgoto.linkDataArray[linkindex].from;
			    	 var displaykey = keyvalue + '.1';
			    	 if(nodekey === keyvalue || nodekey === displaykey)
			    		 {
			    		 var displaynodeiferrorkey = displaykey;
			    		    if(vWprdsgoto.linkDataArray[linkindex].fromPort === 'R' && vWprdsgoto.linkDataArray[linkindex].toPort === 'R'){
			    				 selectedgototokey =  vWprdsgoto.linkDataArray[linkindex].to;
			    			 }else if(vWprdsgoto.linkDataArray[linkindex].fromPort === 'T' && vWprdsgoto.linkDataArray[linkindex].toPort === 'R' && displaynodeiferrorkey === vWprdsgoto.linkDataArray[linkindex].from){
			    				 selectedgototokey =  vWprdsgoto.linkDataArray[linkindex].to;
			    			 }
			    		 }
					}
				for(var nodeindex = 0; nodeindex < vWprdsgoto.nodeDataArray.length; nodeindex++)
					{
					var dispalykeyvalue = keyvalue +'.1';
					if(dispalykeyvalue === vWprdsgoto.nodeDataArray[nodeindex].key)
						{
						selectederrordisplaytext = vWprdsgoto.nodeDataArray[nodeindex].text;
						}					
					}

				    if (textvalue === 'IF EXIT' || textvalue === 'GOTO TAG') {
				     
				     jQuery("#gototagactionone").empty();
				     if(!selectedgototokey)
				       {
				     jQuery("#gototagactionone").append('<option value="">Select</option>');
				       }
				     for (var nodeindex = 0; nodeindex < vWprdsgoto.nodeDataArray.length; nodeindex++) {
				      if (vWprdsgoto.nodeDataArray[nodeindex].category === 'GENERAL TAG') {

				       var keyval= vWprdsgoto.nodeDataArray[nodeindex].key;
				       var keytext = vWprdsgoto.nodeDataArray[nodeindex].text;
				       if(selectedgototokey != keyval)
				       {
				       jQuery("#gototagactionone").append('<option value="'+keyval+'">'+keytext+ '</option>');
				       }else{
				    	   jQuery("#gototagactionone").append('<option selected value="'+keyval+'">'+keytext+ '</option>');  
				       }
				      }else if(  vWprdsgoto.nodeDataArray[nodeindex].category === 'END'){
				       var keyval= vWprdsgoto.nodeDataArray[nodeindex].key;
				       var keytext = vWprdsgoto.nodeDataArray[nodeindex].category;
				       if(selectedgototokey != keyval)
				       {
				       jQuery("#gototagactionone").append('<option value="'+keyval+'">'+keytext+ '</option>');
				       }else{
				    	   jQuery("#gototagactionone").append('<option selected value="'+keyval+'">'+keytext+ '</option>');  
				       }
				      }

				     }
				     
				     div_gojs_actionone_show(keyvalue, textvalue,selectederrordisplaytext,selectedgototokey);
				    } else if (textvalue === 'IF ERROR') {
				     
				     
				     jQuery("#gototagactiontwo").empty();
				     if(!selectedgototokey)
				       {
				     jQuery("#gototagactiontwo").append('<option value="">Select</option>');
				       }
				     for (var nodeindex = 0; nodeindex < vWprdsgoto.nodeDataArray.length; nodeindex++) {
				      if (vWprdsgoto.nodeDataArray[nodeindex].category === 'GENERAL TAG') {

				       var keyval= vWprdsgoto.nodeDataArray[nodeindex].key;
				       var keytext = vWprdsgoto.nodeDataArray[nodeindex].text;
				       if(selectedgototokey != keyval)
				       {
				       jQuery("#gototagactiontwo").append('<option value="'+keyval+'">'+keytext+ '</option>');
				       }else{
				    	   jQuery("#gototagactiontwo").append('<option selected value="'+keyval+'">'+keytext+ '</option>');
				       }
				      }else if(  vWprdsgoto.nodeDataArray[nodeindex].category === 'END'){
				       var keyval= vWprdsgoto.nodeDataArray[nodeindex].key;
				       var keytext = vWprdsgoto.nodeDataArray[nodeindex].category;
				       if(selectedgototokey != keyval)
				       {
				       jQuery("#gototagactiontwo").append('<option value="'+keyval+'">'+keytext+ '</option>');
				       }else{
				    	   jQuery("#gototagactiontwo").append('<option selected value="'+keyval+'">'+keytext+ '</option>');
				       }
				      }

				     }
				     div_gojs_actiontwo_show(keyvalue, textvalue,selectederrordisplaytext,selectedgototokey);
				    } else if (categoryvalue === 'DIAMOND'
				      || textvalue === 'IF RETURN CODE') {
				     
				     jQuery("#gototagactionthree").empty();
				     if(!selectedgototokey)
				       {
				     jQuery("#gototagactionthree").append('<option value="">Select</option>');
				       }
				     
				     for (var nodeindex = 0; nodeindex < vWprdsgoto.nodeDataArray.length; nodeindex++) {
				      if (vWprdsgoto.nodeDataArray[nodeindex].category === 'GENERAL TAG') {

				       var keyval= vWprdsgoto.nodeDataArray[nodeindex].key;
				       var keytext = vWprdsgoto.nodeDataArray[nodeindex].text;
				       if(selectedgototokey != keyval)
				       {
				       jQuery("#gototagactionthree").append('<option value="'+keyval+'">'+keytext+ '</option>');
				       }else{
				    	   jQuery("#gototagactionthree").append('<option selected value="'+keyval+'">'+keytext+ '</option>');  
				       }
				      }else if(  vWprdsgoto.nodeDataArray[nodeindex].category === 'END'){
				       var keyval= vWprdsgoto.nodeDataArray[nodeindex].key;
				       var keytext = vWprdsgoto.nodeDataArray[nodeindex].category;
				       if(selectedgototokey != keyval)
				       {
				       jQuery("#gototagactionthree").append('<option value="'+keyval+'">'+keytext+ '</option>');
				       }else{
				    	   jQuery("#gototagactionthree").append('<option selected value="'+keyval+'">'+keytext+ '</option>');  
				       }
				      }

				     }
				     div_gojs_actionthree_show(keyvalue, textvalue,selectederrordisplaytext,selectedgototokey);
				    } else if (categoryvalue === 'GENERAL TAG') {
				     
				     div_gojs_prevtexttag_show(keyvalue, textvalue ,categoryvalue);
				    }
				    else if (categoryvalue === 'COMMENT'
					      || categoryvalue === 'DISPLAY' || categoryvalue === 'ERRORDISPLAY') {
					     
					     div_gojs_prevtext_show(keyvalue, textvalue ,categoryvalue);
					    }
				    else if (categoryvalue === 'CUSTOM EXECUTION') {
					     
					     div_gojs_prevtextcustexe_show(keyvalue, textvalue ,categoryvalue);
					    }
			});
			// Make link labels visible if coming out of a "conditional" node.
			// This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
			function showLinkLabel(e) {
				var label = e.subject.findObject("LABEL");
				if (label !== null)
					label.visible = (e.subject.fromNode.data.figure === "Diamond");
			}

			// temporary links used by LinkingTool and RelinkingTool are also orthogonal:
			myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
			myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;

			load(); // load an initial diagram from some JSON text

			// initialize the Palette that is on the right side of the page

			myPalette = $(go.Palette, "myPalette", // must name or refer to the DIV HTML element
			{
				"animationManager.duration" : 800, // slightly longer than default (600ms) animation
				nodeTemplateMap : myDiagram.nodeTemplateMap, // share the templates used by myDiagram
				model : new go.GraphLinksModel(varexeactions)


			});

	    	// create the Palette
			myPaletteexe = $(go.Palette, "myPaletteexe");

			// the Palette's node template is different from the main Diagram's
			myPaletteexe.nodeTemplate = $(go.Node, "Vertical", $(go.TextBlock, {
				width : 220,
				font : vargojsshapetextsize.textFontSizeForExecutionPanel,
				stroke : "#1a1a1a",
				editable : false
			},

			new go.Binding("text", "text")),$(go.Shape, {
				width : 220,
				height : 1,
				fill : "#cecece",
				stroke : null,
				margin : new go.Margin(9, 0, 0, 0)
			}, new go.Binding("fill", "#ebebeb")),
			
			{
				toolTip : // define a tooltip for each node that displays the color as text
				$(go.Adornment, "Auto", $(go.Shape, {
					fill : "#ffffff",
					minSize : new go.Size(vargojsshapetextsize.minToolTipWidth,
							vargojsshapetextsize.minToolTipHeight),
					maxSize : new go.Size(vargojsshapetextsize.maxToolTipWidth,
							vargojsshapetextsize.maxToolTipHeight),
					figure : "RoundedRectangle",
					stroke : "green",
					strokeWidth : 1
				}), $(go.TextBlock, {
					margin : new go.Margin(9, 0, 0, 0),
					wrap : go.TextBlock.WrapFit,
					maxSize : new go.Size(200,380),
					minSize : new go.Size(vargojsshapetextsize.minTextWidth,
							vargojsshapetextsize.minTextHeight)
				}, new go.Binding("text", "toolTipText")))
			// end of Adornment
			}
			);

			// the list of data to show in the Palette
			if(varexe.length === 0 && addeditcopy != 'add')
		    	   {
				bootbox.alert(document.getElementById("message9").value);
				}
	
			myPaletteexe.model.nodeDataArray = varexe;
			

		}

		// Make all ports on a node visible when the mouse is over the node
		function showPorts(node, show) {
			var diagram = node.diagram;
			if (!diagram || diagram.isReadOnly || !diagram.allowLink)
				return;
			node.ports.each(function(port) {
				port.stroke = (show ? "white" : null);
			});
		}
		
	//This function is used to delete decision and TAG node
		function deleteDiamondTAGNode()		
		{
		
			myDiagram.rollbackTransaction();
			var newjsondata = myDiagram.model.toJson();
			var vWprdsnew = JSON.parse(newjsondata);
			if(varselectnodeOSCcategory === 'DIAMOND')
			{
			for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++) {
				var selectnodedataarrkey ;
				if(vWprdsnew.nodeDataArray[nodeindex].key.toString().length >=3)
				{
				    selectnodedataarrkey = vWprdsnew.nodeDataArray[nodeindex].key.toString().split('.');
				}else{
					selectnodedataarrkey = vWprdsnew.nodeDataArray[nodeindex].key;
				}
				
				if ((vWprdsnew.nodeDataArray[nodeindex].category === 'ERRORDISPLAY') && (selectnodedataarrkey[0] === varselectnodeOSCkey) )
				{
					vWprdsnew.nodeDataArray.splice(nodeindex, 1);
					
				}
			}
			
			
			for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++) {
				var selectnodedataarrkey ;
				if(vWprdsnew.nodeDataArray[nodeindex].key.toString().length >=3)
					{
				    selectnodedataarrkey = vWprdsnew.nodeDataArray[nodeindex].key.toString().split('.');
				}else{
					selectnodedataarrkey = vWprdsnew.nodeDataArray[nodeindex].key;
				}
				
				if(vWprdsnew.nodeDataArray[nodeindex].key === varselectnodeOSCkey)
					{
					vWprdsnew.nodeDataArray.splice(nodeindex, 1);
					}
			}
				for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
					
					if((vWprdsnew.linkDataArray[linkindex].from === (varselectnodeOSCkey + '.1')) || (vWprdsnew.linkDataArray[linkindex].to === (varselectnodeOSCkey + '.1')) )
						{
						vWprdsnew.linkDataArray.splice(linkindex, 1);
						}
				}
               for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
					
					if((vWprdsnew.linkDataArray[linkindex].from === (varselectnodeOSCkey + '.1')) || (vWprdsnew.linkDataArray[linkindex].to === (varselectnodeOSCkey + '.1')) )
						{
						vWprdsnew.linkDataArray.splice(linkindex, 1);
						}
				}
               for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
					
					if((vWprdsnew.linkDataArray[linkindex].from === (varselectnodeOSCkey)) || (vWprdsnew.linkDataArray[linkindex].to === (varselectnodeOSCkey)) )
						{
						vWprdsnew.linkDataArray.splice(linkindex, 1);
						}
				}
               for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
					
					if((vWprdsnew.linkDataArray[linkindex].from === (varselectnodeOSCkey)) || (vWprdsnew.linkDataArray[linkindex].to === (varselectnodeOSCkey)) )
						{
						vWprdsnew.linkDataArray.splice(linkindex, 1);
						}
				}
			} else{
				var gotodisplaylinkkey;
				//var gotonormallinkkey;
 				for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
					
					if(vWprdsnew.linkDataArray[linkindex].to === varselectnodeOSCkey )
						{
						var linkdataarraykeylen = vWprdsnew.linkDataArray[linkindex].from.toString().split('.');
						if(linkdataarraykeylen.length > 1)
							{
							gotodisplaylinkkey = vWprdsnew.linkDataArray[linkindex].from;
						     vWprdsnew.linkDataArray.splice(linkindex, 1);
							}else
							{
								vWprdsnew.linkDataArray.splice(linkindex, 1);
							}
						}
				}
               for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
					
            	   if(vWprdsnew.linkDataArray[linkindex].to === gotodisplaylinkkey )
					{
				     vWprdsnew.linkDataArray.splice(linkindex, 1);
					}else if(vWprdsnew.linkDataArray[linkindex].from === varselectnodeOSCkey )
					{
					     vWprdsnew.linkDataArray.splice(linkindex, 1);
						}
				}

				
				for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++) {
										
					if ((vWprdsnew.nodeDataArray[nodeindex].key === varselectnodeOSCkey) )
					{
						//alert('delete gotomessage node with tag and display')
						vWprdsnew.nodeDataArray.splice(nodeindex, 1);
						
					}
				}
				
				
				for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++) {
					
					
					if(vWprdsnew.nodeDataArray[nodeindex].key === gotodisplaylinkkey)
						{
						vWprdsnew.nodeDataArray.splice(nodeindex, 1);
						}

				
			}
			}
			for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
				vWprdsnew.linkDataArray[linkindex]["points"] = [];

			}
			
			document.getElementById("mySavedModel").value = JSON.stringify(vWprdsnew);
		    myDiagram.isModified = false;

		    //savechange();
		    load();
			
		}
		

		// This function is used to compare two array
		
		Array.prototype.compare = function(testArr) {
    if (this.length != testArr.length) return false;
    for (var indexnumber = 0; indexnumber < testArr.length; indexnumber++) {
        if (this[indexnumber].compare) { 
            if (!this[indexnumber].compare(testArr[indexnumber])) return false;
        }
        if (this[indexnumber] !== testArr[indexnumber]) return false;
    }
    return true;
}
           // This Function is used to find uniq data in an array
			  Array.prototype.uniq = function uniq() {
			     return this.reduce(function(accum, cur) { 
			       if (accum.indexOf(cur) === -1) accum.push(cur); 
			       return accum; 
			     }, [] );
			   }
           //
			  Array.prototype.diff = function(a) {
				    return this.filter(function(keyindex) {return a.indexOf(keyindex) < 0;});
				};
			  
			  /**This function is used to Check key already exists or not in given new node data array*/
			  
			  function checkGeneralTagContains(generalTagKey)
			  {				  
				  for (var nodeindex = 0; nodeindex < newnodeDataArray.length; nodeindex++)
					  {
					  var keygerenraltag = newnodeDataArray[nodeindex].key;
					  if(keygerenraltag.toString() === generalTagKey.toString())
						  {
						  return true;
						  }
					  }
				  return false;
			  }
			  //This function is used to align flow chart
			  function saveload() {

					var newjsondata = myDiagram.model.toJson();
					var vWprdsnew = JSON.parse(newjsondata);
					
					//alert(vWprdsnew.nodeDataArray.length +' node and '+vWprdsnew.linkDataArray.length+' link');
					//alert(JSON.stringify(vWprdsnew));
					//document.write(JSON.stringify(vWprdsnew));
						var leftdistance = vargojsshapetextsize.marginLeft;
						var topdistance = vargojsshapetextsize.marginTop;
						var horizdistance = vargojsshapetextsize.horizontalDistance;
						var verticaldistance = vargojsshapetextsize.verticalDistance;
						var circleverticaldistance = vargojsshapetextsize.circleVerticalDistance;
						var diamondverticaldistance = vargojsshapetextsize.diamondVerticalDistance;
						var squareverticaldistance = vargojsshapetextsize.squareVerticalDistance;
						
						var gotoleftdistance = leftdistance + horizdistance;
						var diamondheight = vargojsshapetextsize.diamondHeight;
						var rectangleheight = vargojsshapetextsize.rectangleHeight;
						var squareheight = vargojsshapetextsize.squareHeight;
						var circleheight = vargojsshapetextsize.circleHeight;
						var displayheight = vargojsshapetextsize.displayHeight;
						var lastnodeheight = 0;
						var nextnodelocation = 0;
						var changeflag = 0;
						
						//var newlinkDataArray = [];
						newnodeDataArray = [];
						var newfromkey = -1;
						var vardataarraylength = 0 ;
						var tokeyarray = [-1,-1];
						var tocomparekeyarray = [-1,-1];
						var decisionmakingnodekey = [];
						var decisionmakingnodekeycount = 0;
						var skipnodedataarraycreationloop = 0;
						var Isnextdiamondnode = 0;
						var Isgototagnode = 0;
						var gotonodecount = 0;
						var gotolinkingcount = 0;
						var displayerrornode = 0;
						var endnode = true;
						//alert(JSON.stringify(vWprdsnew));
						vardataarraylength = vWprdsnew.nodeDataArray.length;
						var nodedataarraytokeylist = [];
						var nodedataarrayfromkeylist = [];
						var toendkeyvalue;
						var linkdataarrayfromkeylist = [];
						var totaldisconnectedfromkeylist = [];
						var linkdataarrayconnectfromkeylist = [];
						var totalnodedataarrayFromKeyList = [];
						var linkdataarraytokeylist = [];
						for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
							{
							if((vWprdsnew.nodeDataArray[nodeindex].category != 'START') && (vWprdsnew.nodeDataArray[nodeindex].category != 'END'))
								{
							nodedataarraytokeylist.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}							
							if(vWprdsnew.nodeDataArray[nodeindex].category != 'END')
								{
							nodedataarrayfromkeylist.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}
							if(vWprdsnew.nodeDataArray[nodeindex].category != 'END' && vWprdsnew.nodeDataArray[nodeindex].category != 'GOTO TAG' && vWprdsnew.nodeDataArray[nodeindex].category != 'ERRORDISPLAY'  )
							{
								totalnodedataarrayFromKeyList.push(vWprdsnew.nodeDataArray[nodeindex].key);
							}
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'END')
								{
							      toendkeyvalue = vWprdsnew.nodeDataArray[nodeindex].key;
								}
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'DIAMOND')
								{
							      decisionmakingnodekey.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'DIAMOND' || vWprdsnew.nodeDataArray[nodeindex].category === 'GOTO TAG')
								{
									gotonodecount = gotonodecount + 1;
								}								
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'GOTO TAG')
								{
									linkdataarrayfromkeylist.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}
								
								
							}

						//alert(JSON.stringify(vWprdsnew));
						for (var nodeindex = 0; nodeindex < vWprdsnew.linkDataArray.length; nodeindex++)
							{
							if(vWprdsnew.linkDataArray[nodeindex].to != toendkeyvalue)
							{
							linkdataarraytokeylist.push(vWprdsnew.linkDataArray[nodeindex].to);
							}
							if(vWprdsnew.linkDataArray[nodeindex].fromPort != 'R')
							{
							linkdataarrayfromkeylist.push(vWprdsnew.linkDataArray[nodeindex].from);//var gotolinkingcount = 0;
							}
							if(vWprdsnew.linkDataArray[nodeindex].fromPort === 'R')
							{
								gotolinkingcount = gotolinkingcount + 1;
							}
							if(vWprdsnew.linkDataArray[nodeindex].fromPort === 'B')
								{
								linkdataarrayconnectfromkeylist.push(vWprdsnew.linkDataArray[nodeindex].from);
								}
							}
						if(gotolinkingcount != gotonodecount)
						{
							//bootbox.alert(document.getElementById("message10").value);  
							bootbox.alert(document.getElementById("message19").value);
						    return false ;
						}
						//alert(nodedataarrayfromkeylist.uniq().sort()+' = '+linkdataarrayfromkeylist.uniq().sort()+' tonode '+nodedataarraytokeylist.uniq().sort()+' = '+linkdataarraytokeylist.uniq().sort())
						if(!(nodedataarrayfromkeylist.uniq().sort().compare(linkdataarrayfromkeylist.uniq().sort())) || !(nodedataarraytokeylist.uniq().sort().compare(linkdataarraytokeylist.uniq().sort())))
					     {   
							totaldisconnectedfromkeylist = totalnodedataarrayFromKeyList.sort().diff(linkdataarrayconnectfromkeylist.sort());
							if(totaldisconnectedfromkeylist.length > 0)
								{
								for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
								{
									if(vWprdsnew.nodeDataArray[nodeindex].key === totaldisconnectedfromkeylist[0].toString())
									{
										disconnectnodename = vWprdsnew.nodeDataArray[nodeindex].text;
									}															
								}
								}
							
							//bootbox.alert(document.getElementById("message10").value);
							disconnectnodelistlen = totaldisconnectedfromkeylist.length - 1;
							bootbox.alert(disconnectnodename +' '+document.getElementById("message23").value+' '+ disconnectnodelistlen +' '+document.getElementById("message24").value);
					     return false ;
					     }				
						
						for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
							{
							if(vWprdsnew.nodeDataArray[nodeindex].category === 'START')
								{
								   vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + topdistance;
									newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
									newfromkey = vWprdsnew.nodeDataArray[nodeindex].key;
									topdistance = topdistance + circleheight + circleverticaldistance;
									lastnodeheight = circleheight;
			     				}
							
							}
						var avoidhangissue = 0;
				while(endnode)
					{
					tokeyarray = [-1,-1];
					if((decisionmakingnodekey.length > decisionmakingnodekeycount) && (Isnextdiamondnode === 1) && Isgototagnode === 0)
					{					
					newfromkey = decisionmakingnodekey[decisionmakingnodekeycount];
					decisionmakingnodekeycount++;						
					}else if((decisionmakingnodekey.length === decisionmakingnodekeycount) && (Isnextdiamondnode === 1)){
						Isnextdiamondnode = 0;
						Isgototagnode = 0;
						skipnodedataarraycreationloop = 1;
					}
					
						for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) 
						{
							
							if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'B' && (Isnextdiamondnode === 0))
								{
								tokeyarray[1] = vWprdsnew.linkDataArray[linkindex].to;
								}
							else if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'R' && vWprdsnew.linkDataArray[linkindex].toPort === 'L')
								{
								tokeyarray[0] = vWprdsnew.linkDataArray[linkindex].to;
								}
							else if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'R' && vWprdsnew.linkDataArray[linkindex].toPort === 'R' && ((Isnextdiamondnode === 1) || (Isgototagnode === 1) ))
							{
							tokeyarray[0] = vWprdsnew.linkDataArray[linkindex].to;
							//Isnextdiamondnode = 0;
							}
							else if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'T' && vWprdsnew.linkDataArray[linkindex].toPort === 'R' && displayerrornode === 1)
							{
								tokeyarray[0] = vWprdsnew.linkDataArray[linkindex].to;
								//Isnextdiamondnode = 0;
							}
							
							avoidhangissue = avoidhangissue+1;
							
							if(avoidhangissue === 5000)
								{
								
								bootbox.alert(document.getElementById("message18").value);  
							    return false ;
							     
								}
													
						}
						
						if(tokeyarray[0] === toendkeyvalue || tokeyarray[1] === toendkeyvalue)
							{
							Isnextdiamondnode = 1;
							Isgototagnode = 0;
							continue;
							}
						if((tokeyarray.compare(tocomparekeyarray))){
							skipnodedataarraycreationloop = 1;	
							}else{
								Isnextdiamondnode = 0;
								displayerrornode = 0;
								Isgototagnode = 0;
							}
						
						
						if(skipnodedataarraycreationloop === 0)
						{
						for(var newtokeyindex = 0; newtokeyindex < tokeyarray.length; newtokeyindex++)
							{
							
							for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
								{
								
								var tolinkkeylen = tokeyarray[newtokeyindex].toString().split('.');
								/* if((tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key ) && (vWprdsnew.nodeDataArray[nodeindex].category === 'END') && (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 0) && (newnodeDataArray.length === vWprdsnew.nodeDataArray.length-1))
									{
				                        // logic for display node
		                                // alert('INSEDE First = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
				                        if(lastnodeheight === circleheight)
				                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
										vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
								       	newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
										tokeyarray[0] = -1;
				     				}
								else  */if((tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key ) && (vWprdsnew.nodeDataArray[nodeindex].category === 'ERRORDISPLAY') && (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 0))
								{
			                        // logic for display node		
			                        //alert('INSEDE Second DISPALY = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
			                         var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	newfromkey = tokeyarray[newtokeyindex];
			                        	displayerrornode = 1;
			                        	continue;
			                        	}
									var keylar = vWprdsnew.nodeDataArray[nodeindex].key.toString().split('.');
									var displaytopdistance = topdistance - diamondverticaldistance - diamondheight;
									vWprdsnew.nodeDataArray[nodeindex]["loc"] = gotoleftdistance + ' ' + nextnodelocation;
									newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);									
									newfromkey = tokeyarray[newtokeyindex];
								    var innerendkey = tokeyarray[newtokeyindex];
								    tokeyarray[0] = -1;
			     				}
								else if((tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key ) && (vWprdsnew.nodeDataArray[nodeindex].category === 'GENERAL TAG') && (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 0) )
								{
			                        // logic for display node		
			                        //alert('INSEDE Second DISPALY = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
			                        var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
									if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance + 5;
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + circleheight + circleverticaldistance;
							    lastnodeheight = circleheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
			     				}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'DIAMOND') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
									{
									// logic for add normal node
									//alert('INSEDE Third Diamond = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
			                        var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
									if(lastnodeheight === circleheight)
				                        	{		                        	
										 nextnodelocation = topdistance + 5;
				                        	
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
			                        	}
									vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
								    topdistance = topdistance + diamondheight + diamondverticaldistance;			
								    lastnodeheight = diamondheight;
								    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
								    newfromkey = tokeyarray[newtokeyindex];
								    var innerendkey = tokeyarray[newtokeyindex];
								    tokeyarray[1] = -1;
								   
									}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'DISPLAY') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
			                        var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
									if(lastnodeheight === circleheight)
		                        	{		                        	
							nextnodelocation = topdistance - 12;
		                      
		                        	}
		                        else if(lastnodeheight === squareheight)
		                    	{		                        	
		                        	nextnodelocation = topdistance +  0;
		                        	
		                    	}
		                        else if(lastnodeheight === rectangleheight)
		                    	{
		                        	nextnodelocation = topdistance - 10;
		                    	}
		                        else if(lastnodeheight === displayheight)
		                    	{
		                        	nextnodelocation = topdistance + 0;
		                        	
		                    	}
		                        else if(lastnodeheight === diamondheight)
		                    	{		                        	
		                        	nextnodelocation = topdistance - 12;
		                        	
		                    	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + displayheight + verticaldistance;
							    lastnodeheight = displayheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							   
								}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'GOTO TAG') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
								// logic for add normal node
								//alert('INSEDE Five GOTO = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
									var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
								if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance + 0;
				                        	
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance;
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{
				                        	nextnodelocation = topdistance + 0;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{
				                        	nextnodelocation = topdistance + 0;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + squareheight + squareverticaldistance;	
							    lastnodeheight = squareheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							    Isgototagnode = 1;    
							   
								}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'GENERAL TAG') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
								// logic for add normal node
								//alert('INSEDE Six end or General Tag Category = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
											                        var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
								if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance + 5;
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + circleheight + circleverticaldistance;
							    lastnodeheight = circleheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							   
								}
								else if(tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
								// logic for add normal node
								//alert('INSEDE seven Normal = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
											                        var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
								if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance - 12;
				                      
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance +  0;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{
				                        	nextnodelocation = topdistance - 10;
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{
				                        	nextnodelocation = topdistance + 0;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance - 12;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + rectangleheight + verticaldistance;	
							    lastnodeheight = rectangleheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							   
								}
								
								
								 if((newnodeDataArray.length) === (vardataarraylength-1))
							    	{
							    	endnode = false;
							    	break;
							    	}
								
								}				
							
							if(!endnode)
					    	{
					    	break;
					    	}
							}
						
						
						}else{
						
						for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
							{
							if(vWprdsnew.nodeDataArray[nodeindex].category === 'END')
								{
								
								if(lastnodeheight === circleheight)
	                        	{		                        	
						nextnodelocation = topdistance + 5;
	                        	}
	                        else if(lastnodeheight === squareheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 10;
	                        	
                        	}
	                        else if(lastnodeheight === rectangleheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 5;
	                        	
                        	}
	                        else if(lastnodeheight === displayheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 15;
	                        	
                        	}
	                        else if(lastnodeheight === diamondheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 5;
	                        	
                        	}
								   vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + topdistance;
									newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
									newfromkey = vWprdsnew.nodeDataArray[nodeindex].key;
									topdistance = topdistance + circleheight + circleverticaldistance;
									lastnodeheight = circleheight;
			     				}
							
							}
						endnode = false;
						}
						if(!endnode)
							{
							break;
							}
						
					}
						
				var endnodeexistance = 0;
				for (var nodeindex = 0; nodeindex < newnodeDataArray.length; nodeindex++)
				{
					//alert(newnodeDataArray[nodeindex].category +' = END');
				if(newnodeDataArray[nodeindex].category === 'END')
					{
					endnodeexistance = 1;
						}						
				}
			    if(endnodeexistance===0)
				{
			    	for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
					{
					if(vWprdsnew.nodeDataArray[nodeindex].category === 'END')
						{
						
						if(lastnodeheight === circleheight)
                    	{		                        	
				nextnodelocation = topdistance + 5;
                    	}
                    else if(lastnodeheight === squareheight)
                	{		                        	
                    	nextnodelocation = topdistance + 10;
                    	
                	}
                    else if(lastnodeheight === rectangleheight)
                	{		                        	
                    	nextnodelocation = topdistance + 5;
                    	
                	}
                    else if(lastnodeheight === displayheight)
                	{		                        	
                    	nextnodelocation = topdistance + 15;
                    	
                	}
                    else if(lastnodeheight === diamondheight)
                	{		                        	
                    	nextnodelocation = topdistance + 5;
                    	
                	}
						   vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + topdistance;
							newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							newfromkey = vWprdsnew.nodeDataArray[nodeindex].key;
							topdistance = topdistance + circleheight + circleverticaldistance;
							lastnodeheight = circleheight;
	     				}
					
					}
					}
						if(!endnode)
						{
							vWprdsnew.nodeDataArray = [];
							vWprdsnew.nodeDataArray = newnodeDataArray;
						}
						try{
						for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
							vWprdsnew.linkDataArray[linkindex]["points"] = [];

						}}catch(err){}

						document.getElementById("mySavedModel").value = JSON
								.stringify(vWprdsnew);
						myDiagram.isModified = false;
		                load();
				}
			//This function is used to save flow chart
			  function saveloadbeforecreatesequencegrid() {

					var newjsondata = myDiagram.model.toJson();
					var vWprdsnew = JSON.parse(newjsondata);
					
					//alert(vWprdsnew.nodeDataArray.length +' node and '+vWprdsnew.linkDataArray.length+' link');
					//alert(JSON.stringify(vWprdsnew));
					//document.write(JSON.stringify(vWprdsnew));
						var leftdistance = vargojsshapetextsize.marginLeft;
						var topdistance = vargojsshapetextsize.marginTop;
						var horizdistance = vargojsshapetextsize.horizontalDistance;
						var verticaldistance = vargojsshapetextsize.verticalDistance;
						var circleverticaldistance = vargojsshapetextsize.circleVerticalDistance;
						var diamondverticaldistance = vargojsshapetextsize.diamondVerticalDistance;
						var squareverticaldistance = vargojsshapetextsize.squareVerticalDistance;
						
						var gotoleftdistance = leftdistance + horizdistance;
						var diamondheight = vargojsshapetextsize.diamondHeight;
						var rectangleheight = vargojsshapetextsize.rectangleHeight;
						var squareheight = vargojsshapetextsize.squareHeight;
						var circleheight = vargojsshapetextsize.circleHeight;
						var displayheight = vargojsshapetextsize.displayHeight;
						var lastnodeheight = 0;
						var nextnodelocation = 0;
						var changeflag = 0;
						
						//var newlinkDataArray = [];
						newnodeDataArray = [];
						var newfromkey = -1;
						var vardataarraylength = 0 ;
						var tokeyarray = [-1,-1];
						var tocomparekeyarray = [-1,-1];
						var decisionmakingnodekey = [];
						var decisionmakingnodekeycount = 0;
						var skipnodedataarraycreationloop = 0;
						var Isnextdiamondnode = 0;
						var Isgototagnode = 0;
						var gotonodecount = 0;
						var gotolinkingcount = 0;
						var displayerrornode = 0;
						var endnode = true;
						//alert(JSON.stringify(vWprdsnew));
						vardataarraylength = vWprdsnew.nodeDataArray.length;
						var nodedataarraytokeylist = [];
						var nodedataarrayfromkeylist = [];
						var toendkeyvalue;
						var linkdataarrayfromkeylist = [];
						var totaldisconnectedfromkeylist = [];
						var linkdataarrayconnectfromkeylist = [];
						var totalnodedataarrayFromKeyList = [];
						var linkdataarraytokeylist = [];
						
						
						varexeinflowchartpanel = [];
						/**Check blank node exist or not*/
						for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
						{
						if((vWprdsnew.nodeDataArray[nodeindex].category === 'CUSTOM EXECUTION') || (vWprdsnew.nodeDataArray[nodeindex].category === 'COMMENT')  || (vWprdsnew.nodeDataArray[nodeindex].category === 'GENERAL TAG')  || (vWprdsnew.nodeDataArray[nodeindex].category === 'DISPLAY'))
							{
							if(vWprdsnew.nodeDataArray[nodeindex].text.trim() === '')
								{
						     blankdisplaytextnode = true;
						     return false;
								}
							}												
						}
						
						
						for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
							{
							if((vWprdsnew.nodeDataArray[nodeindex].category != 'START') && (vWprdsnew.nodeDataArray[nodeindex].category != 'END'))
								{
							nodedataarraytokeylist.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}
							
							if(vWprdsnew.nodeDataArray[nodeindex].category != 'END')
								{
							nodedataarrayfromkeylist.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}
							if(vWprdsnew.nodeDataArray[nodeindex].category != 'END' && vWprdsnew.nodeDataArray[nodeindex].category != 'GOTO TAG' && vWprdsnew.nodeDataArray[nodeindex].category != 'ERRORDISPLAY'  )
							{
								totalnodedataarrayFromKeyList.push(vWprdsnew.nodeDataArray[nodeindex].key);
							}
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'END')
								{
							      toendkeyvalue = vWprdsnew.nodeDataArray[nodeindex].key;
								}
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'DIAMOND')
								{
							      decisionmakingnodekey.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'DIAMOND' || vWprdsnew.nodeDataArray[nodeindex].category === 'GOTO TAG')
								{
									gotonodecount = gotonodecount + 1;
								}
								if((vWprdsnew.nodeDataArray[nodeindex].category === 'RECTANGLE') || (vWprdsnew.nodeDataArray[nodeindex].category === 'DISPLAY'))
								{
									checkexecutionnode = 1;
								}
								if((vWprdsnew.nodeDataArray[nodeindex].category === 'RECTANGLE'))
								{
									varexeinflowchartpanel.push(vWprdsnew.nodeDataArray[nodeindex].executionName);
								}
								if(vWprdsnew.nodeDataArray[nodeindex].category === 'GOTO TAG')
								{
									linkdataarrayfromkeylist.push(vWprdsnew.nodeDataArray[nodeindex].key);
								}
								
								
							}

						//alert(JSON.stringify(vWprdsnew));
						for (var nodeindex = 0; nodeindex < vWprdsnew.linkDataArray.length; nodeindex++)
							{
							if(vWprdsnew.linkDataArray[nodeindex].to != toendkeyvalue)
							{
							linkdataarraytokeylist.push(vWprdsnew.linkDataArray[nodeindex].to);
							}
							if(vWprdsnew.linkDataArray[nodeindex].fromPort != 'R')
							{
							linkdataarrayfromkeylist.push(vWprdsnew.linkDataArray[nodeindex].from);//var gotolinkingcount = 0;
							}							
							if(vWprdsnew.linkDataArray[nodeindex].fromPort === 'R')
							{
								gotolinkingcount = gotolinkingcount + 1;
							}
							if(vWprdsnew.linkDataArray[nodeindex].fromPort === 'B')
							{
							linkdataarrayconnectfromkeylist.push(vWprdsnew.linkDataArray[nodeindex].from);
							}
							}
						if(gotolinkingcount != gotonodecount)
						{
							//bootbox.alert(document.getElementById("message10").value);  
							returngotonodevalue = true;
							gotolinkingcount = 0;
							gotonodecount = 0;
							return false ;
						}

						//alert(nodedataarrayfromkeylist.uniq().sort()+' = '+linkdataarrayfromkeylist.uniq().sort()+' tonode '+nodedataarraytokeylist.uniq().sort()+' = '+linkdataarraytokeylist.uniq().sort())
						if(!(nodedataarrayfromkeylist.uniq().sort().compare(linkdataarrayfromkeylist.uniq().sort())) || !(nodedataarraytokeylist.uniq().sort().compare(linkdataarraytokeylist.uniq().sort())))
					     {     
							returnsequencevalue = false;
							totaldisconnectedfromkeylist = totalnodedataarrayFromKeyList.sort().diff(linkdataarrayconnectfromkeylist.sort());
							if(totaldisconnectedfromkeylist.length > 0)
								{
								for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
								{
									if(vWprdsnew.nodeDataArray[nodeindex].key === totaldisconnectedfromkeylist[0].toString())
									{
										disconnectnodename = vWprdsnew.nodeDataArray[nodeindex].text;
									}															
								}
								}
							//bootbox.alert(document.getElementById("message10").value);
							disconnectnodelistlen = totaldisconnectedfromkeylist.length - 1;
					     return false ;
					     }				
						
						for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
							{
							if(vWprdsnew.nodeDataArray[nodeindex].category === 'START')
								{
								   vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + topdistance;
									newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
									newfromkey = vWprdsnew.nodeDataArray[nodeindex].key;
									topdistance = topdistance + circleheight + circleverticaldistance;
									lastnodeheight = circleheight;
			     				}
							
							}
						var avoidhangissue = 0;
				while(endnode)
					{
					tokeyarray = [-1,-1];
					if((decisionmakingnodekey.length > decisionmakingnodekeycount) && (Isnextdiamondnode === 1) && Isgototagnode === 0)
					{					
					newfromkey = decisionmakingnodekey[decisionmakingnodekeycount];
					decisionmakingnodekeycount++;						
					}else if((decisionmakingnodekey.length === decisionmakingnodekeycount) && (Isnextdiamondnode === 1)){
						Isnextdiamondnode = 0;
						Isgototagnode = 0;
						skipnodedataarraycreationloop = 1;
					}
					
						for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) 
						{
							
							if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'B' && (Isnextdiamondnode === 0))
								{
								tokeyarray[1] = vWprdsnew.linkDataArray[linkindex].to;
								}
							else if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'R' && vWprdsnew.linkDataArray[linkindex].toPort === 'L')
								{
								tokeyarray[0] = vWprdsnew.linkDataArray[linkindex].to;
								}
							else if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'R' && vWprdsnew.linkDataArray[linkindex].toPort === 'R' && ((Isnextdiamondnode) === 1 || (Isgototagnode === 1)))
							{
							tokeyarray[0] = vWprdsnew.linkDataArray[linkindex].to;
							//Isnextdiamondnode = 0;
							}
							else if(vWprdsnew.linkDataArray[linkindex].from === newfromkey  && vWprdsnew.linkDataArray[linkindex].fromPort === 'T' && vWprdsnew.linkDataArray[linkindex].toPort === 'R' && displayerrornode === 1)
							{
								tokeyarray[0] = vWprdsnew.linkDataArray[linkindex].to;
								//Isnextdiamondnode = 0;
							}
							
							avoidhangissue = avoidhangissue+1;
							
							if(avoidhangissue === 5000)
								{
								
								bootbox.alert(document.getElementById("message18").value);  
								avoidinghangloopissue = true ;
								return false;
							     
								}
													
						}
						
						if(tokeyarray[0] === toendkeyvalue || tokeyarray[1] === toendkeyvalue)
						{
						Isnextdiamondnode = 1;
						Isgototagnode = 0;
						continue;
						}
						if(tokeyarray.compare(tocomparekeyarray)){
							skipnodedataarraycreationloop = 1;	
							}else{
								Isnextdiamondnode = 0;
								displayerrornode = 0;
								Isgototagnode = 0;
							}
						
						
						if(skipnodedataarraycreationloop === 0)
						{
						for(var newtokeyindex = 0; newtokeyindex < tokeyarray.length; newtokeyindex++)
							{
							
							for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
								{
								
								var tolinkkeylen = tokeyarray[newtokeyindex].toString().split('.');
								/* if((tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key ) && (vWprdsnew.nodeDataArray[nodeindex].category === 'END') && (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 0) && (newnodeDataArray.length === vWprdsnew.nodeDataArray.length-1))
									{
				                        // logic for display node
		                                // alert('INSEDE First = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
				                        if(lastnodeheight === circleheight)
				                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
										vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
								       	newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
										tokeyarray[0] = -1;
				     				}
								else */ if((tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key ) && (vWprdsnew.nodeDataArray[nodeindex].category === 'ERRORDISPLAY') && (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 0))
								{
			                        // logic for display node		
			                        //alert('INSEDE Second DISPALY = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
			                         var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	newfromkey = tokeyarray[newtokeyindex];
			                        	displayerrornode = 1;
			                        	continue;
			                        	}
									var keylar = vWprdsnew.nodeDataArray[nodeindex].key.toString().split('.');
									var displaytopdistance = topdistance - diamondverticaldistance - diamondheight;
									vWprdsnew.nodeDataArray[nodeindex]["loc"] = gotoleftdistance + ' ' + nextnodelocation;
									newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);									
									newfromkey = tokeyarray[newtokeyindex];
								    var innerendkey = tokeyarray[newtokeyindex];
								    tokeyarray[0] = -1;
			     				}
								else if((tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key ) && (vWprdsnew.nodeDataArray[nodeindex].category === 'GENERAL TAG') && (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 0) )
								{
			                        // logic for display node		
			                        //alert('INSEDE Second DISPALY = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
			                        var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
									if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance + 5;
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + circleheight + circleverticaldistance;
							    lastnodeheight = circleheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
			     				}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'DIAMOND') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
									{
									// logic for add normal node
									//alert('INSEDE Third Diamond = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
									
									var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}if(lastnodeheight === circleheight)
				                        	{		                        	
										 nextnodelocation = topdistance + 5;
				                        	
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
			                        	}
									vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
								    topdistance = topdistance + diamondheight + diamondverticaldistance;			
								    lastnodeheight = diamondheight;
								    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
								    newfromkey = tokeyarray[newtokeyindex];
								    var innerendkey = tokeyarray[newtokeyindex];
								    tokeyarray[1] = -1;
								   
									}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'DISPLAY') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
									var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
									if(lastnodeheight === circleheight)
		                        	{		                        	
							nextnodelocation = topdistance - 12;
		                      
		                        	}
		                        else if(lastnodeheight === squareheight)
		                    	{		                        	
		                        	nextnodelocation = topdistance +  0;
		                        	
		                    	}
		                        else if(lastnodeheight === rectangleheight)
		                    	{
		                        	nextnodelocation = topdistance - 10;
		                    	}
		                        else if(lastnodeheight === displayheight)
		                    	{
		                        	nextnodelocation = topdistance + 0;
		                        	
		                    	}
		                        else if(lastnodeheight === diamondheight)
		                    	{		                        	
		                        	nextnodelocation = topdistance - 12;
		                        	
		                    	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + displayheight + verticaldistance;
							    lastnodeheight = displayheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							   
								}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'GOTO TAG') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
								// logic for add normal node
								//alert('INSEDE Five GOTO = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
								var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
								if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance + 0;
				                        	
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance;
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{
				                        	nextnodelocation = topdistance + 0;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{
				                        	nextnodelocation = topdistance + 0;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + squareheight + squareverticaldistance;	
							    lastnodeheight = squareheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							    Isgototagnode = 1; 
							    
							   
								}
								else if((vWprdsnew.nodeDataArray[nodeindex].category === 'GENERAL TAG') && tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
								// logic for add normal node
								//alert('INSEDE Six end or General Tag Category = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
								var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
								if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance + 5;
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 10;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 15;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance + 5;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + circleheight + circleverticaldistance;
							    lastnodeheight = circleheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							   
								}
								else if(tokeyarray[newtokeyindex] === vWprdsnew.nodeDataArray[nodeindex].key &&  (tokeyarray[newtokeyindex] != -1) &&  (newtokeyindex == 1))
								{
								// logic for add normal node
								//alert('INSEDE seven Normal = '+vWprdsnew.nodeDataArray[nodeindex].category+' '+vWprdsnew.nodeDataArray[nodeindex].key);
								var checkexistkey = checkGeneralTagContains(tokeyarray[newtokeyindex]);
			                        if(checkexistkey)
			                        	{
			                        	Isnextdiamondnode = 1;
			                        	continue;
			                        	}
								if(lastnodeheight === circleheight)
				                        	{		                        	
									nextnodelocation = topdistance - 12;
				                      
				                        	}
				                        else if(lastnodeheight === squareheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance +  0;
				                        	
			                        	}
				                        else if(lastnodeheight === rectangleheight)
			                        	{
				                        	nextnodelocation = topdistance - 10;
			                        	}
				                        else if(lastnodeheight === displayheight)
			                        	{
				                        	nextnodelocation = topdistance + 0;
				                        	
			                        	}
				                        else if(lastnodeheight === diamondheight)
			                        	{		                        	
				                        	nextnodelocation = topdistance - 12;
				                        	
			                        	}
								vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + nextnodelocation;
							    topdistance = topdistance + rectangleheight + verticaldistance;	
							    lastnodeheight = rectangleheight;
							    newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							    newfromkey = tokeyarray[newtokeyindex];
							    var innerendkey = tokeyarray[newtokeyindex];
							    tokeyarray[1] = -1;
							   
								}
								
								
								 if((newnodeDataArray.length) === (vardataarraylength - 1))
							    	{
							    	endnode = false;
							    	break;
							    	}
								
								}				
							
							if(!endnode)
					    	{
					    	break;
					    	}
							}
						
						
						}else{
						
						for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
							{
							if(vWprdsnew.nodeDataArray[nodeindex].category === 'END')
								{
								
								if(lastnodeheight === circleheight)
	                        	{		                        	
						      nextnodelocation = topdistance + 5;
	                        	}
	                        else if(lastnodeheight === squareheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 10;
	                        	
                        	}
	                        else if(lastnodeheight === rectangleheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 5;
	                        	
                        	}
	                        else if(lastnodeheight === displayheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 15;
	                        	
                        	}
	                        else if(lastnodeheight === diamondheight)
                        	{		                        	
	                        	nextnodelocation = topdistance + 5;
	                        	
                        	}
								   vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + topdistance;
									newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
									newfromkey = vWprdsnew.nodeDataArray[nodeindex].key;
									topdistance = topdistance + circleheight + circleverticaldistance;
									lastnodeheight = circleheight;
			     				}
							
							}
						endnode = false;
						}
						if(!endnode)
							{
							break;
							}
						
					}
						
				var endnodeexistance = 0;
				for (var nodeindex = 0; nodeindex < newnodeDataArray.length; nodeindex++)
				{
					//alert(newnodeDataArray[nodeindex].category +' = END');
				if(newnodeDataArray[nodeindex].category === 'END')
					{
					endnodeexistance = 1;
						}						
				}
			    if(endnodeexistance===0)
				{
			    	for (var nodeindex = 0; nodeindex < vWprdsnew.nodeDataArray.length; nodeindex++)
					{
					if(vWprdsnew.nodeDataArray[nodeindex].category === 'END')
						{
						if(lastnodeheight === circleheight)
                    	{		                        	
				nextnodelocation = topdistance + 5;
                    	}
                    else if(lastnodeheight === squareheight)
                	{		                        	
                    	nextnodelocation = topdistance + 10;
                    	
                	}
                    else if(lastnodeheight === rectangleheight)
                	{		                        	
                    	nextnodelocation = topdistance + 5;
                    	
                	}
                    else if(lastnodeheight === displayheight)
                	{		                        	
                    	nextnodelocation = topdistance + 15;
                    	
                	}
                    else if(lastnodeheight === diamondheight)
                	{		                        	
                    	nextnodelocation = topdistance + 5;
                    	
                	}
						   vWprdsnew.nodeDataArray[nodeindex]["loc"] = leftdistance + ' ' + topdistance;
							newnodeDataArray.push(vWprdsnew.nodeDataArray[nodeindex]);
							newfromkey = vWprdsnew.nodeDataArray[nodeindex].key;
							topdistance = topdistance + circleheight + circleverticaldistance;
							lastnodeheight = circleheight;
	     				}
					
					}
					}
						if(!endnode)
						{
							vWprdsnew.nodeDataArray = [];
							vWprdsnew.nodeDataArray = newnodeDataArray;
						}
						try{
						for (var linkindex = 0; linkindex < vWprdsnew.linkDataArray.length; linkindex++) {
							vWprdsnew.linkDataArray[linkindex]["points"] = [];

						}}catch(err){}

						document.getElementById("mySavedModel").value = JSON
								.stringify(vWprdsnew);
						myDiagram.isModified = false;

				}


		// Show the diagram's model in JSON format that the user may edit data of node
		function saveprevtext(keyvalue, textvalue, categoryvalue) {

			var jsondata = myDiagram.model.toJson();

			var vWords = JSON.parse(jsondata);

			var nodedatakey ;
			var nodedatatext;
			var nodedatacategory;
			
			for (var nodeindex = 0; nodeindex < vWords.nodeDataArray.length; nodeindex++) {
				nodedatakey = vWords.nodeDataArray[nodeindex].key;
				nodedatatext = vWords.nodeDataArray[nodeindex].text;
				nodedatacategory = vWords.nodeDataArray[nodeindex].category;
				if ((categoryvalue.toUpperCase() === 'GENERAL TAG') && ((nodedatacategory.toUpperCase() === 'GENERAL TAG')  || (nodedatacategory.toUpperCase() === 'END')))
				{
					if((nodedatatext.toUpperCase() === textvalue.toUpperCase()) && (nodedatakey != keyvalue.toUpperCase()))
						{
						bootbox.alert(document.getElementById("message12").value +' "'+ textvalue +'" '+document.getElementById("message13").value);
						return false;
						}
				}

			}

			for (var nodeindex = 0; nodeindex < vWords.nodeDataArray.length; nodeindex++) {
				nodedatakey = vWords.nodeDataArray[nodeindex].key;
				if (nodedatakey === keyvalue) {
					vWords.nodeDataArray[nodeindex]["text"] = textvalue;
					vWords.nodeDataArray[nodeindex]["toolTipText"] = textvalue;
				}

			}
			for (var linkindex = 0; linkindex < vWords.linkDataArray.length; linkindex++) {
				vWords.linkDataArray[linkindex]["points"] = [];

			}
			document.getElementById("mySavedModel").value = JSON
					.stringify(vWords);
			myDiagram.isModified = false;
		}
		// Show the diagram's model in JSON format that the user may edit data of node
		function saveiferror(keyvalue, textvalue, gototagkey) {

			var jsondata = myDiagram.model.toJson();

			var vWords = JSON.parse(jsondata);

			var nodedatakey ;
			var vargotonodealreadyexist = 0;
			
			for (var nodeindex = 0; nodeindex < vWords.nodeDataArray.length; nodeindex++) 
			{				
				if ((vWords.nodeDataArray[nodeindex].key === (keyvalue +'.1')) && (vWords.nodeDataArray[nodeindex].category === 'ERRORDISPLAY'))
				{
					vWords.nodeDataArray[nodeindex].text = textvalue;
					vWords.nodeDataArray[nodeindex].toolTipText = textvalue;
					for(var linkindex = 0; linkindex < vWords.linkDataArray.length; linkindex++)
						{
						if(vWords.linkDataArray[linkindex].from === (keyvalue +'.1'))							
							{							
							vWords.linkDataArray[linkindex].to = gototagkey;
							vargotonodealreadyexist = 1
							}						
						}
				}
			}
			
			if(vargotonodealreadyexist === 0)
			{
			for (var nodeindex = 0; nodeindex < vWords.nodeDataArray.length; nodeindex++) 
			{
				nodedatakey = vWords.nodeDataArray[nodeindex].key;				
				if ((nodedatakey === keyvalue) && (vWords.nodeDataArray[nodeindex].text === 'IF ERROR')) {
					var len = vWords.nodeDataArray[nodeindex].loc;
					var lenarr = len.split(' ');
					var leftdist = parseInt(lenarr[0])
							+ vargojsshapetextsize.horizontalDistance;
					var topdist = lenarr[1];
					var newnodedata = {};
					var newlinkdataone = {};
					newnodedata["text"] = textvalue;
					newnodedata["toolTipText"] = textvalue;
					newnodedata["key"] = nodedatakey + '.1';
					newnodedata["figure"] = 'Display';
					newnodedata["category"] = 'ERRORDISPLAY';
					newnodedata["loc"] = parseInt(leftdist) + ' ' + topdist;
					vWords.nodeDataArray.push(newnodedata);
					/** add link data*/
					newlinkdataone["from"] = nodedatakey;
					newlinkdataone["to"] = nodedatakey + '.1';
					newlinkdataone["fromPort"] = 'R';
					newlinkdataone["toPort"] = 'L';
					vWords.linkDataArray.push(newlinkdataone);
				} else if (nodedatakey === gototagkey )
				{
					var newlinkdatatwo = {};
					newlinkdatatwo["from"] = keyvalue +'.1';
					newlinkdatatwo["to"] = gototagkey;
					newlinkdatatwo["fromPort"] = 'T';
					newlinkdatatwo["toPort"] = 'R';
					vWords.linkDataArray.push(newlinkdatatwo);
				}
			}
			}
			for (var linkindex = 0; linkindex < vWords.linkDataArray.length; linkindex++) {
				vWords.linkDataArray[linkindex]["points"] = [];

			}
			document.getElementById("mySavedModel").value = JSON
					.stringify(vWords);
			myDiagram.isModified = false;
		}
		// Show the diagram's model in JSON format that the user may edit data of node
		
		function saveifreturncode(keyvalue, returncodevalue, gototagkey) {
			var jsondata = myDiagram.model.toJson();
			var vWords = JSON.parse(jsondata);

           var vargotonodealreadyexist = 0;
			
			for (var nodeindex = 0; nodeindex < vWords.nodeDataArray.length; nodeindex++) 
			{				
				if ((vWords.nodeDataArray[nodeindex].key === keyvalue) && (vWords.nodeDataArray[nodeindex].category === 'DIAMOND'))
				{
					vWords.nodeDataArray[nodeindex].text = 'IF RETURN CODE' + ' ' +returncodevalue;
					vWords.nodeDataArray[nodeindex].toolTipText = 'IF RETURN CODE' + ' ' +returncodevalue;
					
				}
			}
			
			for(var linkindex = 0; linkindex < vWords.linkDataArray.length; linkindex++)
			{
			if(vWords.linkDataArray[linkindex].from === keyvalue && vWords.linkDataArray[linkindex].toPort === 'R')							
				{							
				vWords.linkDataArray[linkindex].to = gototagkey;
				vargotonodealreadyexist = 1
				}						
			}
			if(vargotonodealreadyexist === 0)
				{
			var newlinkdatatwo = {};
			newlinkdatatwo["from"] = keyvalue;
			
			newlinkdatatwo["to"] = gototagkey;
			newlinkdatatwo["fromPort"] = 'R';
			newlinkdatatwo["toPort"] = 'R';
			vWords.linkDataArray.push(newlinkdatatwo);
				}
			for (var linkindex = 0; linkindex < vWords.linkDataArray.length; linkindex++) {
				vWords.linkDataArray[linkindex]["points"] = [];

			}
			document.getElementById("mySavedModel").value = JSON
					.stringify(vWords);
			myDiagram.isModified = false;
		}
		
		// Show the diagram's model in JSON format that the user may edit data of node
		function saveifexit(keyvalue, textvalue, gototagkey) {

			var jsondata = myDiagram.model.toJson();

			var vWords = JSON.parse(jsondata);
			var vargotonodealreadyexist = 0;
			
			for(var linkindex = 0; linkindex < vWords.linkDataArray.length; linkindex++)
			{
			if((vWords.linkDataArray[linkindex].from === keyvalue) && (vWords.linkDataArray[linkindex].toPort === 'R'))							
				{							
				vWords.linkDataArray[linkindex].to = gototagkey;
				vargotonodealreadyexist = 1
				}						
			}
			if(vargotonodealreadyexist === 0)
				{
			var newlinkdatatwo = {};
			newlinkdatatwo["from"] = keyvalue;
			
			newlinkdatatwo["to"] = gototagkey;
			newlinkdatatwo["fromPort"] = 'R';
			newlinkdatatwo["toPort"] = 'R';
			vWords.linkDataArray.push(newlinkdatatwo);
				}
			
			for (var linkindex = 0; linkindex < vWords.linkDataArray.length; linkindex++) {
				vWords.linkDataArray[linkindex]["points"] = [];

			}
			document.getElementById("mySavedModel").value = JSON
					.stringify(vWords);
			myDiagram.isModified = false;
		}
		//This function is used to delete more than one node or link
		function bulknodedeleted()
		{
			var newjsondata = myDiagram.model.toJson();
			var vWprdsnew = JSON.parse(newjsondata);
			
			var oldjsondata = document.getElementById("mySavedModel").value;
			var vWprdsold = JSON.parse(oldjsondata);
			
			myDiagram.rollbackTransaction();
			
			var startnotdelete = true;
			var endnotdelete = true;
			var startnodejson = {};
			var endnodejson = {};
			

			for(var oldnodeindex = 0 ; oldnodeindex < vWprdsold.nodeDataArray.length;oldnodeindex++)
			{
				if(vWprdsold.nodeDataArray[oldnodeindex].category == 'START' )
				{
					startnodejson = vWprdsold.nodeDataArray[oldnodeindex];
				}
				if(vWprdsold.nodeDataArray[oldnodeindex].category == 'END' )
				{
					endnodejson = vWprdsold.nodeDataArray[oldnodeindex];
				}
				
			}
			
			if(vWprdsnew.nodeDataArray.length>0)
			{
			for(var nodeindex = 0 ; nodeindex < vWprdsnew.nodeDataArray.length ; nodeindex++)
			{
			
				
			if(vWprdsnew.nodeDataArray[nodeindex].category == 'START')
			{
			startnotdelete = false;
			}
			else if(vWprdsnew.nodeDataArray[nodeindex].category == 'END')
			{
			endnotdelete = false;			
			}			
			}
			}
			
			if(startnotdelete)
				{
					vWprdsnew.nodeDataArray.push(startnodejson);	
				}
				if(endnotdelete)
				{
					vWprdsnew.nodeDataArray.push(endnodejson);
				}
			
			if(vWprdsnew.nodeDataArray.length == 2)
				{
				var leftdistance = vargojsshapetextsize.marginLeft;
				var topdistance = vargojsshapetextsize.marginTop;
				var verticaldistance = vargojsshapetextsize.verticalDistance;
				
				vWprdsnew.nodeDataArray[0]["loc"] = leftdistance + ' ' +topdistance;
				topdistance = topdistance + vargojsshapetextsize.circleHeight +verticaldistance;
				vWprdsnew.nodeDataArray[1]["loc"] = leftdistance + ' ' +topdistance;
				
				}
			document.getElementById("mySavedModel").value = JSON.stringify(vWprdsnew);
		    myDiagram.isModified = false;			
		}
		
		
		
		  // Show the diagram's model in JSON format that the user may edit
		  function savechange() {
		    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
		    myDiagram.isModified = false;
		  }
		  /**Load flow chart in to canvas*/
		function load() {
			
			
			myDiagram.model = go.Model.fromJson(document
					.getElementById("mySavedModel").value);
			
			if(!oneendnode || !onestartnode)
				{
			var jsondata = myDiagram.model.toJson();

			var vWords = JSON.parse(jsondata);

			for(var nodeindex = 0; nodeindex < vWords.nodeDataArray.length; nodeindex++)
				{
				if(vWords.nodeDataArray[nodeindex].category === 'END')
					{
				oneendnode = true;
					}
				else if(vWords.nodeDataArray[nodeindex].category === 'START')
					{
						onestartnode = true;
					}
				}
			
				}
			
		}
	
		  // if width or height are below 50, they are set to 50
	     function generateImages(width, height) {
	       // sanitize input
	       width = parseInt(width);
	       height = parseInt(height);
	       if (isNaN(width)) width = 100;
	       if (isNaN(height)) height = 100;
	       // Give a minimum size of 50x50
	       width = Math.max(width, 50);
	       height = Math.max(height, 50);

	       //Get Node heigt array from Node data array
	       
	       var gojsjsondata = JSON.parse(myDiagram.model.toJson());
/* 	       var jsondata = myDiagram.model.toJson();

			var vWords = JSON.parse(jsondata);
*/	       var nodedataarray = gojsjsondata.nodeDataArray;
	       //alert(nodedataarray);
	       var NineNodeHeight = [];
	       //alert(nodedataarray.length);
	       var nodedataarraylen = nodedataarray.length
	       for(var nodejsonindex = 0 , nodejsoncount = 0 ; nodejsonindex < nodedataarraylen ; nodejsoncount++,nodejsonindex++ )
	    	   {
	    	   
	    	   if(nodejsoncount === 8)
	    		   {
	    		   var nodeloc = nodedataarray[nodejsonindex].loc;
	    	       var nodeloclist = nodeloc.split(' ');
	    	       var topheight = parseInt(nodeloclist[1]);
	    	       NineNodeHeight.push(topheight);	
	    	       nodejsoncount = 0;
	    		   }
	    	   else if( nodejsonindex === (nodedataarraylen-1) )
	    		   {
	    		   var nodeloc = nodedataarray[nodejsonindex].loc;
	    	       var nodeloclist = nodeloc.split(' ');
	    	       var topheight = parseInt(nodeloclist[1]);
	    	       NineNodeHeight.push(topheight);	
	    	       nodejsoncount = 0;
	    		   }
	    	   }
	       
	       

          //alert(NineNodeHeight);
	       var imgDiv = document.getElementById('myImages');
	       imgDiv.innerHTML = ''; // clear out the old images, if any
	       var db = myDiagram.documentBounds.copy();
	       var boundswidth = db.width;
	       var boundsheight = db.height;
	       var imgWidth = width;
	       var imgHeight = height;
	       var p = db.position.copy();
	       //alert(NineNodeHeight[0]);
	       imgHeight = NineNodeHeight[0] - 5;
	       for (var pointheight = 0,nodeheightlist = 0; pointheight < boundsheight; nodeheightlist++) {
	         for (var pointwidth = 0; pointwidth < boundswidth; pointwidth += imgWidth) {

	           img = myDiagram.makeImage({
	             scale: 1,
	             position: new go.Point(p.x + pointwidth, p.y + pointheight),
	             size: new go.Size(imgWidth, imgHeight),
	             //textAlign : "right"
	           });
	           // Append the new HTMLImageElement to the #myImages div
	           img.className = 'images';
	           //img.style.textAlign = 'right';
	           imgDiv.appendChild(img);
	           imgDiv.appendChild(document.createElement('br'));
	         }
	         //alert(pointheight +' ' +boundsheight);
	        // alert(NineNodeHeight[nodeheightlist]+' - '+NineNodeHeight[nodeheightlist - 1]);
	         var imgHeightnew = 0;
	         if(nodeheightlist)
	         {
	         imgHeightnew = NineNodeHeight[nodeheightlist] - NineNodeHeight[nodeheightlist - 1];
	         }
	         if(imgHeightnew < 570)
	        	 {
	        	 //alert(' less= '+imgHeight);
	        	 imgHeightnew = NineNodeHeight[nodeheightlist+1] - NineNodeHeight[nodeheightlist];
	        	 if(imgHeightnew > 570)
	        		 {
	        	 pointheight = pointheight + imgHeight;
	        	 imgHeight = imgHeightnew - 5;
	        		 }else{
	        			 pointheight = pointheight + imgHeight;
	        		 }
	        	 }else{
	        		 imgHeight = imgHeightnew; 
	        		 //pointheight = pointheight + imgHeight;
	        		 //alert(' great= '+imgHeight);
	        		 pointheight = pointheight + imgHeight - 5;
	        	 }
	         
	       }
	     }
	     
		
	      /** Print an image of GOJS flow chart*/
    function printImages()
     {
      $('#myImages').show();
      //saveload();
      generateImages(500, 870);
      var divToPrintImages=document.getElementById("myImages");
      divToPrintImages.style.textAlign = 'right'
      newWin= window.open("");
      newWin.document.write(divToPrintImages.outerHTML);
      newWin.document.close();
      newWin.focus();
      newWin.print();
      newWin.close();
      $('#myImages').hide();
     }
   
      /** Print an image of sequence view grid data*/
          
      function printData()
      {

    	   //document.getElementByTagName("table").style.border = "1";
    	  // document.getElementByTagName("table").style.cellpadding = "1";
    	//$('.Description6').css("word-break","break-all");//word-break: break-all;
     	//$('.Message7').css("word-break","break-all");//word-break: break-all;//word-wrap : break-word;
     	  $('table').css("border-collapse","collapse");
    	  $('table,td,th').css("border","1px solid #000");
    	  $('table,td,th').css("cellpadding","1px solid #000");
     	 $('.TableHeaderHide').hide();
     	  
          //$( "table" ).removeClass( "gradientcolor" );
         var divToPrint=document.getElementById("printTable");
         newWin= window.open("");
         newWin.document.write(divToPrint.outerHTML);
         newWin.document.close();
         newWin.focus();
         newWin.print();
         newWin.close();
         //$( "table" ).addClass( "gradientcolor" );
          $('table').css("border-collapse","collapse");
  	     $('table,td,th').css("border","1px solid #eee");
  	     $('table,td,th').css("cellpadding","1px solid #eee");
         $('.TableHeaderHide').show();
        // $('.Description6').css("word-break","normal");//word-break: break-all;
     	// $('.Message7').css("word-break","normal");//word-break: break-all;//word-wrap : break-word;
       
      }
	 /**Determining whether one array contains the contents of another array or not*/
      function arrayContainsAnotherArray(innerarray, outerarray)
      {
    	  varexeunwanteddata = '';
        for(var elementindex = 0; elementindex < innerarray.length; elementindex++){
          if(outerarray.indexOf(innerarray[elementindex]) === -1)
        	  {
        	   varexeunwanteddata = innerarray[elementindex];
               return false;
        	  }
        }
        return true;
      }
		  /**Load flow chart in to canvas*/
			function sequenceviewjson() 
		 {
				
				returnsequencevalue = true;
				returngotonodevalue = false;
				avoidinghangloopissue = false;
				blankdisplaytextnode = false
				saveloadbeforecreatesequencegrid();
				
				/**Double check for all executions is exists in current filter in execution panel*/
				
				 if(!arrayContainsAnotherArray(varexeinflowchartpanel,varexeinexepanel))
					 {
					 bootbox.alert('"'+varexeunwanteddata+'" '+ document.getElementById("message35").value);
					
				     return false ;					 
					 }
				
				
				
				 if(blankdisplaytextnode)
				  {
					 bootbox.alert(document.getElementById("message34").value);
				     return false ;
				  }
				 if(checkexecutionnode === 0)
				  {
					 bootbox.alert(document.getElementById("message14").value);
				  return false ;
				  }

				 if(returngotonodevalue)
					 {

						bootbox.alert(document.getElementById("message20").value);
					    return false;
					 }
				if(returnsequencevalue)
					{				
					if(avoidinghangloopissue)
					{
					return false;
					}
					  var flowchartjson = document.getElementById('mySavedModel').value;
					
					 $.post( "${pageContext.request.contextPath}/SmartTaskConfiguratorSequenceViewData",
						  {JSONSEQUENCEVIEW:flowchartjson}, function( data1,status ) {
							  if (data1,status) {							  
			  
								}	  	  
					});
				

					 if(righta === 1)
						 {
				    				document.getElementById('rightab').click();
						 }
					 if(leftt === 0)
					 {
			    		document.getElementById('lefttb').click();
			    		
					 }
					var returnval = document.getElementById('tasksubmitpanel').click();
					///alert('returnval'+returnval);
					}else{
						
						//bootbox.alert(document.getElementById("message15").value);
						bootbox.alert(disconnectnodename +' '+document.getElementById("message23").value+' '+ disconnectnodelistlen +' '+document.getElementById("message24").value);
					     return false ;
					}
			}
	</script>
	<script>
		var leftt = 0;
		var lefte = 0;
		var righta = 0;
		function mylefte() {

			/*   document.getElementById("lefte").style.zIndex  = 1;
			  document.getElementById("leftt").style.zIndex  = 3; */

		}
		function myrighta() {
			/* document.getElementById("righta").style.zIndex  = 1; */
			return false;
		}
		function myleftt() {
			/*  document.getElementById("lefte").style.zIndex  = 3;
			 document.getElementById("leftt").style.zIndex  = 1;
			 */
			return false;
		}

		function myrightab() {
			/* document.getElementById("righta").style.zIndex  = 3; */
			return false;
		}
		function mylefttb(id) {
			if (leftt === 0) {
				document.getElementById("leftt").style.zIndex = 3;
				document.getElementById("lefte").style.zIndex = 0;
				document.getElementById(id).style.color = "#007acc";
				document.getElementById(id).style.borderBottom = "7px solid #007acc";
				document.getElementById("lefteb").style.color = "#1a1a1a";
				document.getElementById("lefteb").style.borderBottom = "7px solid rgba(128, 128, 128, 0.75)";
				leftt = 1;
				lefte = 0;
			} else {
				document.getElementById("leftt").style.zIndex = 0;
				document.getElementById("lefte").style.zIndex = 0;
				document.getElementById(id).style.color = "#1a1a1a";
				document.getElementById(id).style.borderBottom = "7px solid rgba(128, 128, 128, 0.75)";
				document.getElementById("lefteb").style.color = "#1a1a1a";
				document.getElementById("lefteb").style.borderBottom = "7px solid rgba(128, 128, 128, 0.75)";
				leftt = 0;

			}

			
		}
		function mylefteb(id) {
			if (lefte === 0) {

				var stEXTYPEs = document.getElementById('EXTYPE').value;
			    var stEXDEVICEs = document.getElementById('EXDEVICE').value;
			    var stEXGROUPs = document.getElementById('EXGROUP').value;

			    if(stEXTYPEs && stEXDEVICEs && stEXGROUPs)
			    	{
			    	
			    	}else{
			    		bootbox.alert(document.getElementById("message21").value);
				    	return false;
			    	}
				
				document.getElementById("lefte").style.zIndex = 3;
				document.getElementById("leftt").style.zIndex = 0;
				document.getElementById(id).style.color = "#007acc";
				document.getElementById(id).style.borderBottom = "7px solid #007acc";
				document.getElementById("lefttb").style.color = "#1a1a1a";
				document.getElementById("lefttb").style.borderBottom = "7px solid rgba(128, 128, 128, 0.75)";
				lefte = 1;
				leftt = 0;				
				againExecutionShow();

			} else {
				document.getElementById("lefte").style.zIndex = 0;
				document.getElementById("leftt").style.zIndex = 0;
				document.getElementById(id).style.color = "#1a1a1a";
				document.getElementById(id).style.borderBottom = "7px solid rgba(128, 128, 128, 0.75)";
				document.getElementById("lefttb").style.color = "#1a1a1a";
				document.getElementById("lefttb").style.borderBottom = "7px solid rgba(128, 128, 128, 0.75)";
				lefte = 0;
			}


		}
		function myrightab(id) {

			if (righta === 0) {
				document.getElementById("righta").style.zIndex = 3;
				document.getElementById(id).style.color = "#007acc";
				document.getElementById(id).style.borderBottom = "7px solid #007acc";
				righta = 1;
			} else {
				document.getElementById("righta").style.zIndex = 0;
				document.getElementById(id).style.color = "#1a1a1a";
				document.getElementById(id).style.borderBottom = "7px solid rgba(128, 128, 128, 0.75)";
				righta = 0;
			}



		}
	</script>
	<script>
	var refreshsequencegrid = 0;
	//This function is used to show sequence view grid
		function div_show() {
			
			if(!vargojsshapetextsize.isSequenceViewGrid)
			{
				return false;
			}
			returnsequencevalue = true;
			returngotonodevalue = false;
			avoidinghangloopissue = false;
			blankdisplaytextnode = false;
			saveloadbeforecreatesequencegrid();
			if(blankdisplaytextnode)
			  {
				 bootbox.alert(document.getElementById("message34").value);
			     return false ;
			  }
			 if(checkexecutionnode === 0)
			  {
				 bootbox.alert(document.getElementById("message17").value);
			  return false ;
			  }

			 if(returngotonodevalue)
				 {

					bootbox.alert(document.getElementById("message22").value);
				    return false;
				 }
			
			if(returnsequencevalue)
				{
				
				if(avoidinghangloopissue)
				{
				return false;
				}
			var flowchartjson = document.getElementById('mySavedModel').value;
			var sequenceName = document.getElementById('execution_SEQUENCE_NAME').value;
			  
			document.getElementById('SequenceNameSet').innerHTML = sequenceName;
			 $.post( "${pageContext.request.contextPath}/SmartTaskConfiguratorSequenceViewData",
				  {JSONSEQUENCEVIEW:flowchartjson}, function( data1,status ) {
					  if (data1,status) {							  
							  
						}	  	  
			});
			 
			 $.post( "${pageContext.request.contextPath}/SmartTaskConfiguratorSequenceName",
					  {JSONSEQUENCENAME:sequenceName}, function( data1,status ) {
						  if (data1,status) {							  
								  
							}	  	  
				});
			 
			 
			
            document.getElementById('sequencesubmitted').click();
			$('.Show_main').show();
			$('.Show_main').css({
				"display" : "inline-table"
			});
			$('.cover').show();
				
			if(refreshsequencegrid === 0)
				{
				refreshsequencegrid = 1;
				
				 setTimeout(function(){  
					    $('.Show_main').hide();
						$('.cover').hide();
						div_show();}, 100);
				
				}
				
				}else{
					//bootbox.alert(document.getElementById("message16").value);  
					bootbox.alert(disconnectnodename +' '+document.getElementById("message23").value+' '+ disconnectnodelistlen +' '+document.getElementById("message24").value);
					     return false ;
				}
			
		}
		//This function is used to close sequence view grid
		function div_hide() {

			$('.Show_main').hide();
			$('.cover').hide();
			refreshsequencegrid = 0;

		}
		//This function is used to refresh sequence view grid
		function div_refresh() {
			div_hide();
			div_show();

		}

		//This function is used to show popup for display,errorDispaly and comment node
		function div_gojs_prevtext_show(keyvalue, textvalue,categoryvalue) {

			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			var prevMsgId = document.getElementById('gojs-popup-prevtext');
			if(categoryvalue === 'ERRORDISPLAY')
				{
				prevMsgId.innerHTML = 'DISPLAY';
				} 
			else
			{
			prevMsgId.innerHTML = categoryvalue;
			}
			if((textvalue.toUpperCase() === 'COMMENT') && (categoryvalue.toUpperCase() === 'COMMENT'))
			{
			$('#display_text_prevtext').val('');
			}
			else if((textvalue.toUpperCase() === 'ERRORDISPLAY') && (categoryvalue.toUpperCase() === 'ERRORDISPLAY'))
			{
			$('#display_text_prevtext').val('');
			}
			else if((textvalue.toUpperCase() === 'DISPLAY') && (categoryvalue.toUpperCase() === 'DISPLAY'))
			{
			$('#display_text_prevtext').val('');
			}else{
				$('#display_text_prevtext').val(textvalue);	
			}
			$('#hidden_key_prevtext').val(keyvalue);
			$('#hidden_key_prevtextcategory').val(categoryvalue);
			$('.Show_gojs_prevtext_main').show();
			$('.Show_gojs_prevtext_main').css({
				"display" : "inline-table"
			});
			$('.cover_gojs_prevtext').show();

		}
		//This function is used to close popup for display,errorDispaly and comment node
		function div_gojs_prevtext_hide() {
			//prevtextcodeerrormsgid.innerHTML ='';
			$('#prevtextcodeerrormsg').hide();
			var prevtextcodeerrormsgid=document.getElementById("prevtextcodeerrormsg");
			var retextvalue = $('#display_text_prevtext').val();
			var rekeyvalue = $('#hidden_key_prevtext').val();
            var categoryvalue = $('#hidden_key_prevtextcategory').val();
            retextvalue = retextvalue.trim();
            if(retextvalue.toUpperCase() === 'DISPLAY' && categoryvalue.toUpperCase() ==='DISPLAY')
			{
			prevtextcodeerrormsgid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message32").value;
	    	 $('#prevtextcodeerrormsg').show();
	    	 return false;
			}
            if(retextvalue.toUpperCase() === 'COMMENT' && categoryvalue.toUpperCase() ==='COMMENT')
			{
			prevtextcodeerrormsgid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message31").value;
	    	 $('#prevtextcodeerrormsg').show();
	    	 return false;
			}
            
			   if(retextvalue && retextvalue != ''){
				   
				   $('.Show_gojs_prevtext_main').hide();
					$('.cover_gojs_prevtext').hide();
					saveprevtext(rekeyvalue, retextvalue,categoryvalue);
					load();
					
					   $('#prevtextcodeerrormsg').hide();
					   return true;
					   }
					   else
					   {
						   if(categoryvalue.toUpperCase() === 'COMMENT')
							   {
					   prevtextcodeerrormsgid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message26").value;
							   }else{
								   prevtextcodeerrormsgid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message25").value;  
							   }
					   $('#prevtextcodeerrormsg').show();
					   return false;
					   }
		}
		//This function is used to close popup for display,errorDispaly and comment node
		function div_gojs_prevtext_close() {
			$('#prevtextcodeerrormsg').hide();
			$('.Show_gojs_prevtext_main').hide();
			$('.cover_gojs_prevtext').hide();
			savechange();
			load();

		}


		//This function is used to show popup for General Tag node
		function div_gojs_prevtexttag_show(keyvalue, textvalue,categoryvalue) {

			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			var prevMsgId = document.getElementById('gojs-popup-prevtexttag');

				prevMsgId.innerHTML = categoryvalue + ' NAME';
				if(textvalue.toUpperCase() === 'GENERAL TAG')
				{
			      $('#display_text_prevtexttag').val('');
				}else{
					$('#display_text_prevtexttag').val(textvalue);	
				}
			$('#hidden_key_prevtexttag').val(keyvalue);
			$('#hidden_key_prevtexttagcategory').val(categoryvalue);
			$('.Show_gojs_prevtexttag_main').show();
			$('.Show_gojs_prevtexttag_main').css({
				"display" : "inline-table"
			});
			$('.cover_gojs_prevtexttag').show();

		}
		//This function is used to close popup for General Tag node
		function div_gojs_prevtexttag_hide() {
			//prevtextcodeerrormsgtagid.innerHTML ='';
			 $('#prevtextcodeerrormsgtag').hide();
			var prevtextcodeerrormsgtagid=document.getElementById("prevtextcodeerrormsgtag");
			var retextvalue = $('#display_text_prevtexttag').val();
			var rekeyvalue = $('#hidden_key_prevtexttag').val();
            var categoryvalue = $('#hidden_key_prevtexttagcategory').val();
            retextvalue = retextvalue.trim();
            if(retextvalue.toUpperCase() === 'GENERAL TAG')
			{
			 prevtextcodeerrormsgtagid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message33").value;
	    	 $('#prevtextcodeerrormsgtag').show();
	    	 return false;
			}
            
			   if(retextvalue && retextvalue != ''){
				   
				   $('.Show_gojs_prevtexttag_main').hide();
					$('.cover_gojs_prevtexttag').hide();
					saveprevtext(rekeyvalue, retextvalue, categoryvalue);
					load();
					
					   $('#prevtextcodeerrormsgtag').hide();
					   return true;
					   }
					   else
					   {
					   prevtextcodeerrormsgtagid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message27").value;
					   $('#prevtextcodeerrormsgtag').show();
					   return false;
					   }
		}
		//This function is used to close popup for General Tag node
		function div_gojs_prevtexttag_close() {
			$('#prevtextcodeerrormsgtag').hide();
			$('.Show_gojs_prevtexttag_main').hide();
			$('.cover_gojs_prevtexttag').hide();
			savechange();
			load();

		}

		//This function is used to show popup for Custom Execution node
		function div_gojs_prevtextcustexe_show(keyvalue, textvalue,categoryvalue) {

			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			var prevMsgId = document.getElementById('gojs-popup-prevtextcustexe');

				prevMsgId.innerHTML = categoryvalue + ' PATH';
				if(textvalue.toUpperCase() === 'CUSTOM EXECUTION')
				{
			        $('#display_text_prevtextcustexe').val('');
				}else{
					$('#display_text_prevtextcustexe').val(textvalue);
				}
			$('#hidden_key_prevtextcustexe').val(keyvalue);
			$('#hidden_key_prevtextcustexecategory').val(categoryvalue);
			$('.Show_gojs_prevtextcustexe_main').show();
			$('.Show_gojs_prevtextcustexe_main').css({
				"display" : "inline-table"
			});
			$('.cover_gojs_prevtextcustexe').show();

		}
		//This function is used to close popup for Custom Execution node
		function div_gojs_prevtextcustexe_hide() {
			//prevtextcodeerrormsgcustexeid.innerHTML = '';
			$('#prevtextcodeerrormsgcustexe').hide();
			var prevtextcodeerrormsgcustexeid=document.getElementById("prevtextcodeerrormsgcustexe");
			var retextvalue = $('#display_text_prevtextcustexe').val();
			var rekeyvalue = $('#hidden_key_prevtextcustexe').val();
			var categoryvalue = $('#hidden_key_prevtextcustexecategory').val();
			retextvalue = retextvalue.trim();
			if(retextvalue.toUpperCase() === 'CUSTOM EXECUTION')
				{
				 prevtextcodeerrormsgcustexeid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message30").value;
		    	 $('#prevtextcodeerrormsgcustexe').show();
		    	 return false;
				}

			   
			   if(retextvalue && retextvalue != ''){
				   
				   
                   var splitretextvalue =  retextvalue.split('/');
				   if(splitretextvalue.length > 1)
                    {	 
					 $.ajax({
					     url: retextvalue,
					     cache: false,
					     error: function() {
					    	 prevtextcodeerrormsgcustexeid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message29").value;
					    	 $('#prevtextcodeerrormsgcustexe').show();
					    	 return false;
				        	 
							
					     },
					     
					     success: function(data) {							    	 
					    	 $('.Show_gojs_prevtextcustexe_main').hide();
								$('.cover_gojs_prevtextcustexe').hide();
								saveprevtext(rekeyvalue, retextvalue, categoryvalue);
								load();
								
								   $('#prevtextcodeerrormsgcustexe').hide();
								   return true;
					     },
					     type: 'GET'
					  });
                    }else{
                    	 $.post( "${pageContext.request.contextPath}/SmartTaskConfiguratorValidateCustomExecution",
           					  {CUSTOMEXECUTIONVVALUE:retextvalue}, function( data1,status ) {
           						  if (data1,status) {
           							  if(data1)
           							{	  
           							$('.Show_gojs_prevtextcustexe_main').hide();
    								$('.cover_gojs_prevtextcustexe').hide();
    								saveprevtext(rekeyvalue, retextvalue, categoryvalue);
    								load();
    								
    								   $('#prevtextcodeerrormsgcustexe').hide();
    								   return true;
           							}else{
           								prevtextcodeerrormsgcustexeid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message29").value;
           					    	    $('#prevtextcodeerrormsgcustexe').show();
           					    	    return false;
           							}
           							}	  	  
           				});
                    }
				    
					   }
					   else
					   {
					   prevtextcodeerrormsgcustexeid.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+document.getElementById("message28").value;
					   $('#prevtextcodeerrormsgcustexe').show();
					   return false;
					   }
			   
		}
		//This function is used to close popup for Custom Execution node
		function div_gojs_prevtextcustexe_close() {
			$('#prevtextcodeerrormsgcustexe').hide();
			$('.Show_gojs_prevtextcustexe_main').hide();
			$('.cover_gojs_prevtextcustexe').hide();
			savechange();
			load();

		}		
		
		//This function is used to show popup for IF Exit node
		function div_gojs_actionone_show(keyvalue, textvalue,selectederrordisplaytext,selectedgototokey) {

			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			var actiononeMsgId = document
					.getElementById('gojs-popup-actionone');

			actiononeMsgId.innerHTML = textvalue;

			//$('#gojs-popup_actionthree').val(textvalue);
			$('#hidden_key_actionone').val(keyvalue);
			$('.Show_gojs_actionone_main').show();
			$('.Show_gojs_actionone_main').css({
				"display" : "inline-table"
			});
			$('.cover_gojs_actionone').show();

		}
		//This function is used to close popup for IF Exit node
		function div_gojs_actionone_hide() {
			$('#actiononecodeerrormsg').hide();
			var retextvalue = ''// $('#display_text_actionone').val();
			var rekeyvalue = $('#hidden_key_actionone').val();
			var gototagkey = $('#gototagactionone').val();
			
			
		   if(gototagkey){
				   
			   $('.Show_gojs_actionone_main').hide();
				$('.cover_gojs_actionone').hide();
				saveifexit(rekeyvalue, retextvalue, gototagkey);
				load();
				   $('#actiononecodeerrormsg').hide();
				   return true;
				   }
				   else
				   {
				   $('#actiononecodeerrormsg').show();
				   return false;
				   }
		}
		//This function is used to close popup for IF Exit node
		function div_gojs_actionone_close() {
			$('#actiononecodeerrormsg').hide();
			$('.Show_gojs_actionone_main').hide();
			$('.cover_gojs_actionone').hide();
			savechange();
			load();

		}

		//This function is used to show popup for IF Error node
		function div_gojs_actiontwo_show(keyvalue, textvalue,selectederrordisplaytext,selectedgototokey) {

			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			var actiontwoMsgId = document
					.getElementById('gojs-popup-actiontwo');

			actiontwoMsgId.innerHTML = textvalue;
			//$('#gototagactiontwo').val(textvalue);
			$('#display_text_actiontwo').val(selectederrordisplaytext);
			$('#hidden_key_actiontwo').val(keyvalue);
			$('.Show_gojs_actiontwo_main').show();
			$('.Show_gojs_actiontwo_main').css({
				"display" : "inline-table"
			});
			$('.cover_gojs_actiontwo').show();

		}
		//This function is used to close popup for IF Error node
		function div_gojs_actiontwo_hide() {
			$('#actiontwocodeerrormsg').hide();
			var retextvalue = $('#display_text_actiontwo').val();
			var rekeyvalue = $('#hidden_key_actiontwo').val();
			var gototagkey = $('#gototagactiontwo').val();
			retextvalue = retextvalue.trim();
			   if(retextvalue && gototagkey && retextvalue != ''){
				   
				   $('.Show_gojs_actiontwo_main').hide();
					$('.cover_gojs_actiontwo').hide();;
				   
					saveiferror(rekeyvalue, retextvalue, gototagkey);
					load();
				   $('#actiontwocodeerrormsg').hide();
				   return true;
				   }
				   else
				   {
				   $('#actiontwocodeerrormsg').show();
				   return false;
				   }
		}
		//This function is used to close popup for IF Error node
		function div_gojs_actiontwo_close() {
			$('#actiontwocodeerrormsg').hide();
			$('.Show_gojs_actiontwo_main').hide();
			$('.cover_gojs_actiontwo').hide();
			savechange();
			load();

		}
		//This function is used to show popup for IF Return Code node
		function div_gojs_actionthree_show(keyvalue, textvalue,selectederrordisplaytext,selectedgototokey) {

			document.addEventListener("contextmenu", function(e){
			    e.preventDefault();
			}, false);
			var actionthreeMsgId = document
					.getElementById('gojs-popup-actionthree');

			actionthreeMsgId.innerHTML = 'RETURN CODE';
			if(textvalue.toUpperCase() === 'IF RETURN CODE')
			{
				$('#return_code_actionthree').val('');
			}else{
				$('#return_code_actionthree').val(textvalue.substring(15));
			}
			$('#hidden_key_actionthree').val(keyvalue);
			//$('#return_code_actionthree').val('');
			$('.Show_gojs_actionthree_main').show();
			$('.Show_gojs_actionthree_main').css({
				"display" : "inline-table"
			});
			$('.cover_gojs_actionthree').show();

		}
		//This function is used to close popup for IF Return Code node
		function div_gojs_actionthree_hide() {
			$('#returncodeerrormsg').hide();
			   var returncodevalue = $('#return_code_actionthree').val();
			   var rekeyvalue = $('#hidden_key_actionthree').val();
			   var gototagkey = $('#gototagactionthree').val();
			   returncodevalue =  returncodevalue.trim();
			   if(returncodevalue && gototagkey  && returncodevalue != ''){
			   
			   $('.Show_gojs_actionthree_main').hide();
			   $('.cover_gojs_actionthree').hide();
			   
			   saveifreturncode(rekeyvalue, returncodevalue, gototagkey);
			   load(); 
			   $('#returncodeerrormsg').hide();
			   return true;
			   }
			   else
			   {
			   $('#returncodeerrormsg').show();
			   return false;
			   }
			   
			  }
		//This function is used to close popup for IF Return Code node
		function div_gojs_actionthree_close() {
			$('#returncodeerrormsg').hide();
			$('.Show_gojs_actionthree_main').hide();
			$('.cover_gojs_actionthree').hide();
			savechange();
			load();

		}
		
		//This function is used to hide whole configurator div id and show only myImages div id
		  function div_showImages() {

			  
		   $('#wholeconfigurator').hide();
		   //$('#hidesanpdrawers').hide();
		   //$('#myDiagram').hide();
		   $('#myImages').show();
		   /* $('.myImages').css({
		    "display" : "inline-table"
		   });
		   $('.coverImages').show(); */

		  }
		//This function is used to show whole configurator div id and hide only myImages div id
		function div_hideImages() {

		   $('#myImages').hide();
		   //$('.coverImages').hide();
		   $('#wholeconfigurator').show();
		   //$('#hidesanpdrawers').show();
		   //$('#myDiagram').show();

		  }
		//This function is used to show whole configurator div id and hide only myImages div id
		  function div_refreshImages() {
		   div_hideImages();
		   div_showImages();

		  }
	</script>
<style>
 
 .buttontransparentclass{
	  BACKGROUND-COLOR: TRANSPARENT;
  BORDER: NONE;
	}
	.imagesetclass{
	  HEIGHT: 13PX;
  WIDTH: 13PX;
  margin-left: -5px;
	}
	.sequenceviewbuttons{
	  float: right;
	  padding-left: 81%;
	  padding-top: 12px;
	  position: absolute;
	  z-index: 1000;
	}
	.sequenceviewgridlink{
	color: rgb(68, 77, 88);
    font-size: 14px;
    text-decoration: underline;
    font-weight: 300;
	}
 input:disabled {
    background-color: rgb(235, 235, 228) !important;
    color: rgb(84,84,84);
}
 select:disabled {
    background-color: rgb(235, 235, 228) !important;
    color: rgb(84,84,84);
}
 
/* @media print specifies CSS rules that only apply when printing */
@media print {
  /* CSS reset to clear styles for printing */
  html, body, div {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }

  /* Hide everything on the page */
  body * {
    display: none;
  }

  #content, #myImages, #myImages * {
    /* Only display the images we want printed */
    /* all of the image's parent divs
       leading up to the body must be un-hidden (displayed) too
    */
    display: block;
    /* CSS reset to clear the specific visible divs for printing */
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
  }

  /* We have line breaks in the DIV
     to separate the images in the browser,
     but do not want those line breaks when printing
  */
  #myImages br {
    display: none;
  }

  img {
    /* Only some browsers respect this rule: */
    page-break-inside: avoid;
    /* Almost all browsers respect this rule: */
    page-break-after:always;
  }
}

/* The @page rules specify additional printing directives that browsers may respect
   Here we suggest printing in landscape (instead of portrait) with a small margin.
   Browsers, however, are free to ignore or override these suggestions.
   See also:
    https://developer.mozilla.org/en-US/docs/CSS/@page
    http://dev.w3.org/csswg/css3-page/#at-page-rule
*/
@page {
  /* Some browsers respect rules such as size: landscape */
  margin: 1cm;
}

.datawrap
{
    word-wrap: break-word !important;
  /*   border-left: none !important;
    border-right: none !important; */
   
}
/* #Grid_Heading{
 word-wrap: break-word !important;
  /*   border-left: none !important;
    border-right: none !important; */
   
} */

input.ng-invalid.ng-dirty {
    background-color: #ffffff;
}
.bootbox-body{
    
 font-family: "Open Sans", sans-serif;
    font-size: 14px;
    padding-top: 18px;
    padding-left: 15px;
    padding-right: 10px;
   }
.modal-body {
    position: relative;
    padding:1px; 
}
textarea
{
resize : none;
}
canvas {
    
    outline: none;
}
 </style>
</body>
</html>