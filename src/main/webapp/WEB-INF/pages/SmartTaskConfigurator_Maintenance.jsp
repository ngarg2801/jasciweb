<!-- 
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014� World Wide 
Date Developed  Sep 18 2014
Description It is used to Add value of Executions Maintenance Screen
Created By Aakash Bishnoi -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,java.util.ArrayList,java.text.DateFormat,java.text.SimpleDateFormat,java.util.Date,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL" %>

  
   
	<tiles:insertDefinition name="subMenuAssigmentTemplate">
	<tiles:putAttribute name="body">
    <!-- BEGIN PAGE CONTAINER -->
    <div class="page-container">
        <!-- BEGIN PAGE HEAD -->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                  
                        	<%-- <h1>${ScreenLabels.getSmartTaskExecutions_Executions_Maintenance()}</h1> --%>
                        	<h1>${ScreenLabels.getSmartTaskConfigurator_Smart_Task_Congigurator_Maintenance()}</h1>
                </div>
                <!-- END PAGE TITLE -->


            </div>
        </div>
        <!-- END PAGE HEAD -->
        <!-- BEGIN PAGE CONTENT -->
        	<div class="page-content" id="page-content" style="padding: 0px 0 15px;">
            <div class="container">
               <iframe src="${pageContext.request.contextPath}/Configurator" style="width:100%; height:870px; border: 1px solid #000; margin-left:1.4%"></iframe>
	   <div id="ErrorMessage" class="note note-danger" style="display:none">
     	<p id="MessageRestFull" class="error error-Top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Messages</p>	
       </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="row margin-top-10">
                
                    <div class="col-md-12">


                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->
  </tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value="/resourcesValidate/form-validation.js"/>"
	type="text/javascript"></script>
	<link type="text/css"
	href="<c:url value="/resourcesValidate/css/SmartTaskExecutions.css"/>"
	rel="stylesheet" />
<script>

function headerChangeLanguage(){
	  
	   $.ajax({
			url: '${pageContext.request.contextPath}/HeaderLanguageChange',
			type: 'GET',
	        cache: false,
			success: function(
					data1) {

				if (data1.boolStatus) {

					location.reload();
				}
			}
		});
	  	
	  	
	  }

	  function headerInfoHelp(){
	  	
	  	
	   	 $.post( "${pageContext.request.contextPath}/HeaderInfoHelp",
	  			  {InfoHelp:'SmartTaskConfigurator',
	   		InfoHelpType:'PROGRAM'
	  		 
	  			  }, function( data1,status ) {
	  				  if (data1.boolStatus) {
	  					  window.open(data1.strMessage); 
	  					  					  
	  					}
	  				  else
	  					  {
	  					//  alert('No help found yet');
	  					  }
	  				  
	  				  
	  			  });
	  	
	  }

</script>
  

