<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>


.ahrefminwidth{
    min-width: 140px;
}
.directionchange {
	
    direction: rtl;
}
.page-header .page-header-menu .hor-menu .navbar-nav > li > a > i {
    color: #bcc2cb ;
    float: right;
    padding-top: 2px;
}
.page-header .page-header-menu .hor-menu .navbar-nav > li .dropdown-menu li > a > i {
    color: #6fa7d7 ;
    float: left;
}
.setheightmargin {
    min-width: 223px;
    margin-right: 82px;
    float: left;
}
.megamenuwidthforIE{
width:750px;
}

.setrigthtoleftheightmargin {
    min-width: 231px;
    margin-left: 20px;
    float: left;
    position: relative;
    }
     @media screen\0 {
     
     .ahrefminwidth{
    min-width: 140px;
}
.directionchange {
	
    direction: rtl;
}
.page-header .page-header-menu .hor-menu .navbar-nav > li > a > i {
    color: #bcc2cb ;
    float: none;
    padding-top: 0px;
}
.ichangeforIE {
    color: #bcc2cb ;
    float: right !important;
    padding-top: 2px !important;
    padding-left:6px;
}
.page-header .page-header-menu .hor-menu .navbar-nav > li .dropdown-menu li > a > i {
    color: #6fa7d7 ;
    float: left;
}
.setheightmargin {
    min-width: 223px;
    margin-right: 82px;
    float: left;
    
}
.megamenuwidthforIE{
width:750px;
}

.setrigthtoleftheightmargin {
    min-width: 231px;
    margin-left: 20px;
    float: left;
    position: relative;
    }
    
     }
/* #cssmenu > ul#cssmenu_ul li#cssmenu_li.clicked a#cssmenuaahref {
    background:GREEN;
}
 */

/* The list elements which contain the links */

/* #cssmenu_ul #cssmenu_li.clicked a#cssmenuaahref {
    background:GREEN;
}
 */

</style>
<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu">
		<div class="container">
			<!-- BEGIN HEADER SEARCH BOX -->
			<!--<form class="search-form" action="extra_search.html" method="GET">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form>-->
			<!-- END HEADER SEARCH BOX -->
			<!-- BEGIN MEGA MENU -->
			<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
			<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
			<div class="hor-menu" id="cssmenu">
		
			<c:set var = "ObjMenuprofileHm" value ="${ObjCommonSession.getMenu()}" />
			<script>
			profilevalueskey = new Array();
			profilecount=0;
			profile4boolean=false;
			profile5boolean=false;
			profile6boolean=false;
			</script>
			<c:if test="${not empty ObjMenuprofileHm}">
			
			<c:forEach var="ObjMenuprofileHmList" items="${ObjMenuprofileHm}">			
			<ul class="nav navbar-nav" id="newul">
			<li class="menu-dropdown mega-menu-dropdown selected">
						<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
						${ObjMenuprofileHmList.key} 
						<i class="fa fa-angle-down ichangertl"></i>
						</a>
						<c:set var="ObjSubApplication" value="${ObjMenuprofileHmList.value}" />
						
						<ul class="dropdown-menu">
							<li>
								<div class="mega-menu-content">
									<div class="row">
								<script>
									subapplicationvalueskey = new Array();
									subapplicationcount=0;
								</script>
										<c:forEach var="ObjSubApplicationList" items="${ObjSubApplication}">
											<c:if test="${not empty ObjSubApplicationList}">
										<div class="col-md-4">
									
											<ul class="mega-menu-submenu setheightmargin">
												<li class="setsebapplicationid">
													<h3>${ObjSubApplicationList.key}</h3>
												</li>
												
												
												<c:if test="${not empty ObjSubApplicationList.value}">
													<c:forEach var="ObjHashMapSubList" items="${ObjSubApplicationList.value}">
													
													<c:set var="descrptionAndVlaue" value="${ObjHashMapSubList.value}" />
													
													<c:set var="tooltip" value="${fn:split(descrptionAndVlaue, '##')}" />
													
														<li id="cssmenu_li">
															<a class="ahrefminwidth" id="cssmenuaahref" href="${pageContext.request.contextPath}/${tooltip[0]}">
															<i class="fa fa-angle-right"></i>
															${ObjHashMapSubList.key}</a>
															
															
														</li>
														</c:forEach>
														</c:if>
							
													
												
											</ul>
											
										</div>
										</c:if>
												<script>
												subapplicationvalueskey.push("${ObjSubApplicationList.key}");
													subapplicationcount=subapplicationcount+1;
													
												</script>
										</c:forEach>

									</div>
								</div>
							</li>
						</ul>
						
					</li>
					</ul>
					

												
													<script>
													profilevalueskey.push("${ObjMenuprofileHmList.key}");
													profilecount=profilecount+1;
													
													if(profilecount==4){
														try{
														if(subapplicationcount>=2)
															{
															 profile4boolean=true;
																												
															}
														 }catch(err){}
													}
													if(profilecount==5){
														try{
														if(subapplicationcount>=2)
															{
															 profile5boolean=true;
																												
															}
														 }catch(err){}
													}
													if(profilecount==6){
														try{
														if(subapplicationcount>=2)
															{
															 profile6boolean=true;
																												
															}
														 }catch(err){}
														
													}
												</script>
				
					</c:forEach>
					<script>
					$("li.selected").each(function(profileidvalue) {
						profileidvalue=profileidvalue+1
						 $(this).attr("id", "profilenumber" + profileidvalue);
						});
					
						$("i.ichangertl").each(function(iidvalue) {
							iidvalue=iidvalue+1
							 $(this).attr("id", "inumber" + iidvalue);
							});
						
						$("div.mega-menu-content").each(function(megamenuidvalue) {
							megamenuidvalue=megamenuidvalue+1
							 $(this).attr("id", "megamenuid" + megamenuidvalue);
							});
						
					 $("ul.mega-menu-submenu").each(function(i) {
						i=i+1;
						 $(this).attr("id", "subappliaction" + i);
						}); 
					
					 if(profile4boolean){
						 try{
							 
						 document.getElementById('profilenumber4').classList.add('directionchange');
						 document.getElementById('inumber4').classList.add('ichangeforIE');
						 document.getElementById('megamenuid4').classList.add('megamenuwidthforIE');
						
						 }catch(err){}
						 
					 } 
					 if(profile5boolean){
						 try{
						
						
						 document.getElementById('profilenumber5').classList.add('directionchange');
						 document.getElementById('inumber5').classList.add('ichangeforIE');
						 document.getElementById('megamenuid5').classList.add('megamenuwidthforIE');
					 }catch(err){}
						 
					 } 
					 if(profile6boolean){
						 try{
							 
						
						 document.getElementById('profilenumber6').classList.add('directionchange');
						 document.getElementById('inumber6').classList.add('ichangeforIE');
						 document.getElementById('megamenuid6').classList.add('megamenuwidthforIE');
					 }catch(err){}
					 } 
					
				
				 
					</script>
					</c:if>
					<!-- <script>
													var valueskey = new Array();
													
													/* <c:forEach var="ObjHashMapSubListdemo" items="${ObjSubApplicationList.value}">
													 values.push("${ObjHashMapSubListdemo.key}");   
													</c:forEach>	 */	
													<c:forEach var="ObjMenuprofileHmListdemo" items="${ObjMenuprofileHm}">	
													 valueskey.push("${ObjMenuprofileHmListdemo.key}");  
													 
													
													 
														</c:forEach>
														var valueskeylength=valueskey.length;
														var valueskeylengthminus=valueskey.length-1;
														valueskeylengthminus=valueskeylengthminus+1;
														if(valueskeylength==valueskeylengthminus){
															document.getElementById('addclassdynamic').classList.add('directionchange');
															
															alert("in");
															
														}
													</script> -->
												<!-- <script>
													var valueskey = new Array();
													
													/* <c:forEach var="ObjHashMapSubListdemo" items="${ObjSubApplicationList.value}">
													 values.push("${ObjHashMapSubListdemo.key}");   
													</c:forEach>	 */	
													<c:forEach var="ObjMenuprofileHmListdemo" items="${ObjMenuprofileHm}">	
													 valueskey.push("${ObjMenuprofileHmListdemo.key}");  
													 
													
													 
														</c:forEach>
														
														if(valueskeylength==valueskeylengthminus){
															
															alert("in");
															
														}
													</script> -->
<%-- 			<c:set var = "ObjHashMap" value ="${ObjCommonSession.getSubMenuListHM()}" />
			<c:if test="${not empty ObjHashMap}">
			<c:forEach var="ObjHashMapList" items="${ObjHashMap}">
			<c:set var="SubMenuHashMap" value="${ObjHashMapList.value}" />
			<c:if test="${not empty SubMenuHashMap}">
			
				<ul class="nav navbar-nav">
					<li class="menu-dropdown classic-menu-dropdown ">
						<a data-hover="megamenu-dropdown" id="1" data-close-others="true" data-toggle="dropdown" href="#">
						${ObjHashMapList.key} <i class="fa fa-angle-down"></i>
						</a>
						
						<ul class="dropdown-menu pull-left">
						<c:if test="${not empty ObjHashMapList.value}">
						<c:forEach var="ObjHashMapSubList" items="${ObjHashMapList.value}">
						
						<c:set var="descrptionAndVlaue" value="${ObjHashMapSubList.value}" />
						
						<c:set var="tooltip" value="${fn:split(descrptionAndVlaue, '##')}" />
						
							<li class="" title="${tooltip[1]}">
								<a href="${pageContext.request.contextPath}/${tooltip[0]}">
								<!--<i class="icon-briefcase"></i>-->
								${ObjHashMapSubList.key}</a>
								
								
							</li>
							</c:forEach>
							</c:if>
						</ul>
					</li>
					
			
					
					<!-- end: mega menu with custom content -->
				</ul>
				</c:if>	
				
				
						
				<c:if test="${empty SubMenuHashMap}">
				<c:set var="MenuOption" value="${ObjHashMapList.key}" />
				<c:set var="headerValue" value="${fn:split(MenuOption, '##')}" />
				
				<ul class="nav navbar-nav">
				<li class="" title="${headerValue[2]}">
								<a href="${headerValue[1]}">
								<!--<i class="icon-briefcase"></i>-->
								${headerValue[0]}</a>
								
								
							</li>	
				
				</ul>
				
				</c:if>
				</c:forEach>
				
				</c:if> --%>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
	
	<script>
	
	/* var menuItems = document.querySelectorAll('#cssmenu_ul #cssmenu_li');
	for (var i = 0; i < menuItems.length; i++) {
	    (function (i) {
	        menuItems[i].onmouseover = function () {
	        	$('.ahrefminwidth').parents('.newClass').attr('style','background-color:red !important;')
	            //clearSelection();
	            this.className = 'menu-dropdown mega-menu-dropdown clicked';
	        }
	    })(i);
	}

	function clearSelection() {
	    for (var i = 0; i < menuItems.length; i++) {
	        menuItems[i].className = ' ';
	    }
	} */
	

   $(function() { 
    	
	   var style=document.getElementById( 'style_color' );
    	
	   if('${ObjCommonSession.getMenuType()}'==='FULLSCREEN')
	   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Full_Display()}.css";
	   }
   else if('${ObjCommonSession.getMenuType()}'==='MOBILE' || '${ObjCommonSession.getMenuType()}'==='GLASS')
   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Mobile()}.css";
   }
   else if('${ObjCommonSession.getMenuType()}'==='RF')
   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_RF()}.css";
   }

	 
        });
	
    </script>