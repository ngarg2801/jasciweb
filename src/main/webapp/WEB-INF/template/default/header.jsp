<!-- 
Date Developed  Sep 18 2014
Description It is used to show for header
Created By Aakash Bishnoi -->


<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL" %><head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<!-- KENDO UI CSS AND JS -->
<link href="<c:url value="/KendoUI/styles/kendo.common.min.css" />
	"	rel="stylesheet" />
<link href="<c:url value="/KendoUI/styles/kendo.default.min.css" />" rel="stylesheet" type="text/css" />
<script src="<c:url value="/KendoUI/js/jquery.min.js"/>"></script>
<script src="<c:url value="/KendoUI/js/kendo.all.min.js"/>"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/uniform/css/uniform.default.css"/>" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/morris/morris.css"/>" rel="stylesheet" type="text/css">

<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<c:url value="/resources/assets/admin/pages/css/tasks.css"/>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<c:url value="/resources/assets/global/css/components.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/css/plugins.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/layout.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/themes/default.css"/>" rel="stylesheet" type="text/css" id="style_color">
<link href="<c:url value="/resources/assets/admin/layout3/css/custom.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/error-style.css"/>"  rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/custome-style.css"/>"  rel="stylesheet" type="text/css">

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico">
	<title>JASCI</title>
	<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<%-- <script src="<c:url value="/resources/assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script> --%>
<script src="<c:url value="/resources/assets/global/plugins/jquery-migrate.min.js"/>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<c:url value="/resources/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.cokie.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/uniform/jquery.uniform.min.js"/>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/select2/select2.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/resources/assets/global/scripts/metronic.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/layout.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/demo.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/scripts/datatable.js"/>"></script>
<script src="<c:url value="/resources/assets/admin/pages/scripts/ecommerce-orders.js"/>"></script>
<script src="<c:url value="/resources/assets/global/plugins/morris/morris.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/morris/raphael-min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.sparkline.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/pages/scripts/tasks.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/pages/scripts/index3.js"/>" type="text/javascript"></script>
</head>
		<% COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe; %>
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
			<img src="<c:url value="/resources/assets/admin/layout3/img/companylogo2.png"/>" alt="logo" class="logo-default">
			</div>
			<span class="companyname_middle"><%=objCommonsessionbe.getCompany()%></span>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						  <a href="javascript:headerChangeLanguage();"> <img class="logolanguage" src="<c:url value="/resources/assets/admin/layout3/img/Language_Icon.png"/>" alt=""/></a>
						
					</li>
					<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
						<a href="javascript:history.back();"><img class="logoreturn" src="<c:url value="/resources/assets/admin/layout3/img/Return.png"/>" alt=""/></a>				<!--</a>-->
					
						
					</li>
					<!-- END NOTIFICATION DROPDOWN -->
					<!-- BEGIN TODO DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-tasks" id="header_task_bar">
						<a href="javascript:headerInfoHelp();"><img class="logoinfo" src="<c:url value="/resources/assets/admin/layout3/img/Info.png"/>" alt=""/></li></a>
						
					</li>
					<!-- END TODO DROPDOWN -->
					
					<!-- BEGIN INBOX DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						  <a href="${pageContext.request.contextPath}/ResponsePage"> <img class="logoexit" src="<c:url value="/resources/assets/admin/layout3/img/Exit.png"/>" alt=""/></a>
						
					</li>
					<!-- END INBOX DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<%-- <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
					<%=objCommonsessionbe.getTeam_Member()%>&nbsp;&nbsp;&nbsp;&nbsp;  </span><img src="<c:url value="/resources/assets/admin/layout3/img/Logo2.png"/>" alt="logo" class="logo-default">
						</a> --%>
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
					<%=objCommonsessionbe.getTeam_Member_Name()%>&nbsp;&nbsp;&nbsp;&nbsp;  </span></a>
					<img src="<c:url value="/resources/assets/admin/layout3/img/Logo2.png"/>" alt="logo" class="rightcompanylogo">
						<ul class="dropdown-menu dropdown-menu-default">
							
							<li>
								<a href="${pageContext.request.contextPath}/LogoutPage">
								<i class="icon-key"></i> Log Out </a>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<script>
	
	
	
   $(function() { 
    	
	   var style=document.getElementById( 'style_color' );
	   if('${ObjCommonSession.getMenuType()}'==='FULLSCREEN')
	   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Full_Display()}.css";
	   }
   else if('${ObjCommonSession.getMenuType()}'==='MOBILE'  || '${ObjCommonSession.getMenuType()}'==='GLASS')
   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Mobile()}.css";
   }
   else if('${ObjCommonSession.getMenuType()}'==='RF')
   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_RF()}.css";
   }

   
    
    	//style.href  ="/${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/green-haze.css";
    	//document.getElementsByTagName( 'head' )[0].appendChild( style );
    	

        });
    </script>
	<script>
    jQuery(document).ready(function() {    
        
Layout.init(); // init current layout


        });
    </script>