<!-- 
Date Developed  Sep 18 2014
Description It is used to show for header
Created By Aakash Bishnoi -->


<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL" %>
	<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=10.0; user-scalable=1;"> 
<meta content="" name="description">
<meta content="" name="author">
<!-- KENDO UI CSS AND JS -->
<link href="<c:url value="/KendoUI/styles/kendo.common.min.css" />"	rel="stylesheet" />
<link href="<c:url value="/KendoUI/styles/kendo.default.min.css" />" rel="stylesheet" type="text/css" />
<script src="<c:url value="/KendoUI/js/jquery.min.js"/>"></script>
<script src="<c:url value="/KendoUI/js/kendo.all.min.js"/>"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/uniform/css/uniform.default.css"/>" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/morris/morris.css"/>" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<c:url value="/resources/assets/admin/pages/css/tasks.css"/>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<c:url value="/resources/assets/global/css/components.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/css/plugins.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/layout.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/themes/default.css"/>" rel="stylesheet" type="text/css" id="style_color">
<link href="<c:url value="/resources/assets/admin/layout3/css/custom.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/error-style.css"/>"  rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/custome-style.css"/>"  rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/Header.css"/>"  rel="stylesheet" type="text/css">
<%-- <link href="<c:url value="/resourcesValidate/css/menu-app-icon-style.css"/>"  rel="stylesheet" type="text/css" > --%>

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico">
	<title>JASCI</title>
	<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<%-- <script src="<c:url value="/resources/assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script> --%>
<script src="<c:url value="/resources/assets/global/plugins/jquery-migrate.min.js"/>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<c:url value="/resources/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.cokie.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/uniform/jquery.uniform.min.js"/>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/select2/select2.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/resources/assets/global/scripts/metronic.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/layout.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/demo.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/scripts/datatable.js"/>"></script>
<script src="<c:url value="/resources/assets/admin/pages/scripts/ecommerce-orders.js"/>"></script>
<script src="<c:url value="/resourcesValidate/js/moment.js"/>" type="text/javascript"></script>

<script src="<c:url value="/resourcesValidate/js/bootbox.js"/>"
	type="text/javascript"></script>
	<script src="<c:url value="/resourcesValidate/js/bootbox.min.js"/>"
	type="text/javascript"></script>

</head>
		
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
			
							<!-- //objModelView.addObject(GLOBALCONSTANT.MenuAssignmentView_CompanyLogoURL,ObjCommon.getCompanyLogo());
				//objModelView.addObject(GLOBALCONSTANT.MenuAssignmentView_Key_CompanyName,ObjCommon.getCompany_Name_20());
				//objModelView.addObject(GLOBALCONSTANT.MenuAssignmentView_Key_TeamMemberName,ObjCommon.getTeam_Member_Name()); -->
			
			<img src="${ObjCommonSession.getCompanyLogo()}" alt="logo" class="logo-default">
			</div>
			<span class="companyname_middle">${ObjCommonSession.getCompany_Name_20()}</span>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						  <a href="javascript:headerChangeLanguage();" id="languageID" title="Current Language: ${ObjCommonSession.getCurrentLanguage()}"> <img class="logolanguage" src="<c:url value="/resources/assets/admin/layout3/img/Language_Icon.png"/>" alt=""/></a>
						
					</li>
					<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
						<a href="javascript:history.back();" title="Back"><img class="logoreturn" src="<c:url value="/resources/assets/admin/layout3/img/Return.png"/>" alt=""/></a>				<!--</a>-->
					
						
					</li>
					<!-- END NOTIFICATION DROPDOWN -->
					<!-- BEGIN TODO DROPDOWN -->
					 <li class="dropdown dropdown-extended dropdown-dark dropdown-tasks" id="header_task_bar">
						<a href="javascript:headerInfoHelp();" title="Info Help"><img class="menuinfo" src="<c:url value="/resources/assets/admin/layout3/img/Info.png"/>" alt=""/></li></a>
						
					</li>
					<!-- END TODO DROPDOWN -->
					
					<!-- BEGIN INBOX DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						  <a href="${pageContext.request.contextPath}/menu_profile_assignment" title="Main Menu"> <img class="logoexit" src="<c:url value="/resources/assets/admin/layout3/img/Exit.png"/>" alt=""/></a>
						
					</li>
					
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="circle" id="circlecount">0</span>
						<span class="corner"></span>
						</a>
						<ul class="dropdown-menu" style="border-radius:5px !important;margin-right:8px;">
							<li class="external">
								<h3><strong>Your messages</strong></h3>
								
							</li>
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">	
									
									<li id="MenuMessageMessageDivID">
										
									</li>
									<li id="teamMemberMessageDivID">
										
									</li>
								
									
								</ul>
							</li>
						</ul>
					
					<!-- END INBOX DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<%-- <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
					<%=objCommonsessionbe.getTeam_Member()%>&nbsp;&nbsp;&nbsp;&nbsp;  </span><img src="<c:url value="/resources/assets/admin/layout3/img/Logo2.png"/>" alt="logo" class="logo-default">
						</a> --%>
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
					${ObjCommonSession.getTeam_Member_Name()}&nbsp;&nbsp;&nbsp;&nbsp;  </span></a>
					<img src="<c:url value="/resources/assets/admin/layout3/img/Logo2.png"/>" alt="logo" class="rightcompanylogo">
						
					</li>
						<li id="header_inbox_bar">
        <a href="${pageContext.request.contextPath}/LogoutPage" style="margin-left:-102px;width:30px;" title="Logout"> <img src="<c:url value="/resources/assets/admin/layout3/img/Logout_16X16.png"/>" alt=""/></a>
        
      
     </li>
		
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<script>
	
	function getUTCDateTime(){

		var localdate =    moment.utc().format("YYYY-MM-DD");
		return   localdate;  
	
		}
	   function executeQuery() {
		   
			 var selectedValue=getUTCDateTime();
	   
	   $.ajax({
	     url: '${pageContext.request.contextPath}/MenuMessages',
	     cache: false,
	     data : {DateTime : selectedValue},
	     error: function() {
	        //$('#info').html('<p>An error has occurred</p>');
	     },
	      success: function(data) {
	    	
	      $('#MenuMessageMessageDivID').empty();
	        var MenuMessageSize=0;
	      for (var message in data){
	    	 
	      if((data[message].message)!=null){
	    	  MenuMessageSize=MenuMessageSize+1;
	             $('#MenuMessageMessageDivID').append('<a><span>'+data[message].message+'</span></a>');
	     
	      }
	        
	         
	      }
	    
	    $("#circlecount").val(MenuMessageSize);
	   
	    
	     },
	     type: 'GET'
	  });

	  };	
	  
	  function executeTeamMemberQuery() {

			 var selectedValue=getUTCDateTime();
	$.ajax({
	   url: '${pageContext.request.contextPath}/TeamMemberMessages',
	   cache: false,
	   data : {DateTime : selectedValue},
	   error: function() {
	      //$('#info').html('<p>An error has occurred</p>');
	   },
	   
	   success: function(data) {
	    $('#teamMemberMessageDivID').empty();
	    var teamMemberSize=0;
	      for (var team in data){
	       
	       if(data[team].message!=null && data[team].message != 'null' &&  data[team].message != 'Null'  &&  data[team].message != 'NULL' ){
	           $('#teamMemberMessageDivID').append('<a><span>'+data[team].message+'</span></a>');
	           teamMemberSize=teamMemberSize+1;
	       }
	         }
	      var teammessagecount= $("#circlecount").val();
	      var totalcount=Number(teamMemberSize)+Number(teammessagecount);
	      $("#circlecount").text(totalcount);
	     
	   },
	   type: 'GET'
	});


	};
	
   $(function() { 
    	
	   var style=document.getElementById( 'style_color' );
    	
	   if('${ObjCommonSession.getMenuType()}'==='FULLSCREEN')
	   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Full_Display()}.css";
	   }
   else if('${ObjCommonSession.getMenuType()}'==='MOBILE' || '${ObjCommonSession.getMenuType()}'==='GLASS')
   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Mobile()}.css";
   }
   else if('${ObjCommonSession.getMenuType()}'==='RF')
   {
	   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_RF()}.css";
   }

   


        });
   
   jQuery(document).ready(function() { 
   	
	   setInterval(function () {
		   executeQuery();
		   executeTeamMemberQuery();
		},1000);

	   Metronic.init();       
	   Layout.init(); 
		
   });
   
    </script>
    
    
    <style>
    
    .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > i, .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > i {
 
  color:#000000 !important ;

}

.page-header .page-header-top .top-menu .navbar-nav > li.dropdown.open .dropdown-toggle {
  
   background-color: transparent !important;
}


    </style>