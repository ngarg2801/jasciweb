<!-- 
Date Developed  Sep 18 2014
Description It is used to show the footer
Created By Aakash Bishnoi -->


<div class="page-footer">
	<div class="container" style="text-align: center;">
		${ObjCommonSession.getFooterText()}
	</div>
</div>

