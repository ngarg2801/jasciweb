<!-- 
Date Developed  Sep 18 2014
Description It is used to manege the sequence of tiles part
Created By Aakash Bishnoi -->


<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<body>
<tiles:insertAttribute name="MenuExecutionHeader" />
<tiles:insertAttribute name="Sub_Menu_List_Header" />

<tiles:insertAttribute name="body" />
<tiles:insertAttribute name="footer" />
</body>
</html>