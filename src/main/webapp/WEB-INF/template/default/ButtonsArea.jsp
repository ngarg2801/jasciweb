<!-- 
Created By  Shailendra Rajput 
 Created Date Oct 16 2015
Description It is used to show the Button Area.
 -->
<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<style>

</style>
<div style="text-align: center;" id="buttonDivArea">
<!-- 
<div class="upperdiv_buttonarea">
	<div class="innerdiv_buttonarea"> -->
		<%-- ${ButtonsArea} --%>
		
		<button type="button" id="buttonContinue" class="btn btn-success btnsizechanges"></button>
        <button type="button" id="buttonExit" class="btn btn-danger btnsizechanges">${ExecutionLabels.getEXIT()}</button>
		
	</div>
<!-- </div>
</html> -->
