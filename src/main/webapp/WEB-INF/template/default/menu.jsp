<!-- 
Date Developed  Sep 18 2014
Description It is used to show for Menu options
Created By Aakash Bishnoi -->


<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page
	import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL"%>
<% 	COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;	 %>
<div class="page-header-menu">
	<div class="container">
		<!-- BEGIN HEADER SEARCH BOX -->

		<!-- END HEADER SEARCH BOX -->
		<!-- BEGIN MEGA MENU -->
		<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
		<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
		<div class="hor-menu ">
			<ul class="nav navbar-nav">
				<li class="active"><a href="${pageContext.request.contextPath}/ResponsePage">Dashboard</a></li>
				<li class="menu-dropdown mega-menu-dropdown "><a
					data-hover="megamenu-dropdown" data-close-others="true"
					data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
						Features <i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu" style="min-width: 510px">
						<li>
							<div class="mega-menu-content">
								<div class="row">
									<div class="col-md-4">
										<ul class="mega-menu-submenu">
											<li>
												<h3>Admin</h3>
											</li>
											<li><a
												href="${pageContext.request.contextPath}/GeneralCodeList">
													<i class="icon-basket"></i> General Code
											</a></li>

											<li><a
												href="${pageContext.request.contextPath}/General_code_identificationdata?MenuValue=APPLICATIONS">
													<i class="icon-pencil"></i> Application
											</a></li>

											<li><a
												href="${pageContext.request.contextPath}/General_code_identificationdata?MenuValue=Department">
													<i class="icon-pencil"></i> Department
											</a></li>
											<li><a
												href="${pageContext.request.contextPath}/General_code_identificationdata?MenuValue=Shift">
													<i class="icon-pencil"></i> Shift
											</a></li>
											<li><a
												href="${pageContext.request.contextPath}/General_code_identificationdata?MenuValue=Color">
													<i class="icon-pencil"></i> Color
											</a></li>
										<%-- 	<li><a
												href="${pageContext.request.contextPath}/General_code_identificationdata?MenuValue=Product">
													<i class="icon-pencil"></i> Product
											</a></li> --%>
											<li><a
												href="${pageContext.request.contextPath}/Team_member_maintenance">
													<i class="icon-pencil"></i> Team Member Maintenance
											</a></li>
											<li><a
												href="${pageContext.request.contextPath}/Team_member_lookup">
													<i class="icon-pencil"></i> Security Setup Maintenance
											</a></li>
											
											<li><a
												href="${pageContext.request.contextPath}/InfoHelpAssignmentSearch">
													<i class="icon-pencil"></i> Info Help Assignment Search
											</a></li>
											
											<li><a
												href="${pageContext.request.contextPath}/Company_Lookup">
													<i class="icon-pencil"></i> Company Maintenance
											</a></li>
										</ul>
									</div>
								</div>
							</div>
						</li>
					</ul></li>

				<!-- begin: mega menu with custom content -->
				<li class="menu-dropdown mega-menu-dropdown mega-menu-full hide">
					<a data-hover="megamenu-dropdown" data-close-others="true"
					data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
						Mega Menu <i class="fa fa-angle-down"></i>
				</a>
					<ul class="dropdown-menu">
						<li>
							<div class="mega-menu-content">
								<div class="row">
									<div class="col-md-6">
										<div id="accordion" class="panel-group">
											<div class="panel panel-success">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion"
															href="#collapseOne" class="collapsed"> Mega Menu Info
															#1 </a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse in">
													<div class="panel-body">
														<p>Metronic Mega Menu Works for fixed and responsive
															layout and has the facility to include (almost) any
															Bootstrap elements and jquery plugins.</p>
														<p>Duis mollis, est non commodo luctus, nisi erat
															mattis consectetur purus sit amet porttitor ligula. nisi
															erat mattis consectetur purus</p>
													</div>
												</div>
											</div>
											<div class="panel panel-danger">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion"
															href="#collapseTwo" class="collapsed"> Mega Menu Info
															#2 </a>
													</h4>
												</div>
												<div id="collapseTwo" class="panel-collapse collapse">
													<div class="panel-body">
														<p>Metronic Mega Menu Works for fixed and responsive
															layout and has the facility to include (almost) any
															Bootstrap elements and jquery plugins.</p>
														<p>Duis mollis, est non commodo luctus, nisi erat
															mattis consectetur purus sit amet porttitor ligula. nisi
															erat mattis consectetur purus</p>
													</div>
												</div>
											</div>
											<div class="panel panel-info">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" data-parent="#accordion"
															href="#collapseThree"> Mega Menu Info #3 </a>
													</h4>
												</div>
												<div id="collapseThree" class="panel-collapse collapse">
													<div class="panel-body">
														<p>Metronic Mega Menu Works for fixed and responsive
															layout and has the facility to include (almost) any
															Bootstrap elements and jquery plugins.</p>
														<p>Duis mollis, est non commodo luctus, nisi erat
															mattis consectetur purus sit amet porttitor ligula. nisi
															erat mattis consectetur purus</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="portlet light">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-cogs font-red-sunglo"></i> <span
														class="caption-subject font-red-sunglo bold uppercase">Notes</span>
													<span class="caption-helper">notes samples...</span>
												</div>
												<div class="tools">
													<a href="javascript:;" class="collapse"> </a> <a
														href="#portlet-config" data-toggle="modal" class="config">
													</a> <a href="javascript:;" class="reload"> </a> <a
														href="javascript:;" class="remove"> </a>
												</div>
											</div>
											<div class="portlet-body">
												<div class="note note-success">
													<h4 class="block">Success! Some Header Goes Here</h4>
													<p>Duis mollis, est non commodo luctus, nisi erat
														mattis consectetur purus sit amet porttitor ligula, eget
														lacinia odio sem nec elit. Cras mattis consectetur purus
														sit amet fermentum.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</li>
				<!-- end: mega menu with custom content -->
			</ul>
		</div>
		<!-- END MEGA MENU -->
	</div>
</div>

