<!-- 
Created By  Shailendra Rajput 
Created Date Oct 16 2015
Description It is used to manege the sequence of tiles part
 -->
<%-- 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<body>
<tiles:insertAttribute name="LongTermDisplay" />
<tiles:insertAttribute name="DisplayMessageArea" />
<tiles:insertAttribute name="body" />
<tiles:insertAttribute name="ImageArea" />
<tiles:insertAttribute name="ButtonsArea" />
</body>
</html> --%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
    <body style="width:100%;height:100%">
<table style="width:100%;height:100%">
<tr style="width:100%;height:10%"><td>
  <tiles:insertAttribute name="LongTermDisplay" />
</td></tr>
<tr style="width:100%;height:10%" id="DisplayMessageAreaid"><td>
  <tiles:insertAttribute name="DisplayMessageArea"  />
</td></tr>
<tr style="width:100%;height:60%" ><td>
  <tiles:insertAttribute name="body" />
</td></tr>
<tr  style="width:100%;height:10%" class="upperdiv_imagearea"><td>
  <tiles:insertAttribute name="ImageArea"/>
</td></tr>
<tr style="width:100%;height:10%" ><td>
  <tiles:insertAttribute name="ButtonsArea" />
</td></tr>
</table>
</body>
</html>