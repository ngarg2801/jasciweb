<!-- 
Property of JASCI, LLC a corporation filled in United States of America 
Copyright 2014� World Wide 
Date Developed  Sep 18 2014
Description It is used to show for header
Created By Aakash Bishnoi -->


<%@taglib prefix="kendo" uri="http://www.kendoui.com/jsp/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.Properties,java.io.IOException,java.io.InputStream,com.jasci.common.constant.GLOBALCONSTANT,com.jasci.biz.AdminModule.be.GENERALCODESBE,com.jasci.common.utilbe.COMMONSESSIONBE,com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL" %>
	<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=10.0; user-scalable=1;"> 
<meta content="" name="description">
<meta content="" name="author">
<!-- KENDO UI CSS AND JS -->
<link href="<c:url value="/KendoUI/styles/kendo.common.min.css" />"	rel="stylesheet" />
<link href="<c:url value="/KendoUI/styles/kendo.default.min.css" />" rel="stylesheet" type="text/css" />
<script src="<c:url value="/KendoUI/js/jquery.min.js"/>"></script>
<script src="<c:url value="/KendoUI/js/kendo.all.min.js"/>"></script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>" rel="stylesheet" type="text/css"/>
<link href="<c:url value="/resources/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/uniform/css/uniform.default.css"/>" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<c:url value="/resources/assets/global/plugins/jqvmap/jqvmap/jqvmap.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/plugins/morris/morris.css"/>" rel="stylesheet" type="text/css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<c:url value="/resources/assets/admin/pages/css/tasks.css"/>" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<c:url value="/resources/assets/global/css/components.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/global/css/plugins.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/layout.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resources/assets/admin/layout3/css/themes/default.css"/>" rel="stylesheet" type="text/css" id="style_color">
<link href="<c:url value="/resources/assets/admin/layout3/css/custom.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/error-style.css"/>"  rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/custome-style.css"/>"  rel="stylesheet" type="text/css">
<link href="<c:url value="/resourcesValidate/css/Header.css"/>"  rel="stylesheet" type="text/css">
<%-- <link href="<c:url value="/resourcesValidate/css/menu-app-icon-style.css"/>"  rel="stylesheet" type="text/css" > --%>

<!-- END THEME STYLES -->
<link rel="shortcut icon" href="#">
	<title>JASCI</title>
	<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<%-- <script src="<c:url value="/resources/assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script> --%>
<script src="<c:url value="/resources/assets/global/plugins/jquery-migrate.min.js"/>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<c:url value="/resources/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>

<script src="<c:url value="/resources/assets/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/jquery.cokie.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/plugins/uniform/jquery.uniform.min.js"/>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/select2/select2.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"/>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/resources/assets/global/scripts/metronic.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/layout.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/admin/layout3/scripts/demo.js"/>" type="text/javascript"></script>
<script src="<c:url value="/resources/assets/global/scripts/datatable.js"/>"></script>
<script src="<c:url value="/resources/assets/admin/pages/scripts/ecommerce-orders.js"/>"></script>
<script src="<c:url value="/resourcesValidate/js/moment.js"/>" type="text/javascript"></script>
</head>
		
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			
			<div class="page-logo">
				<img src="${ObjCommonSession.getCompanyLogo()}" alt="logo" class="logo-default">
			</div>
			<div class="companyname_middle" id="idcompanyname_middle">
			${ObjCommonSession.getCompany_Name_20()}
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu top-menu-align-setting">
				<ul class="nav navbar-nav pull-right" id="pullleftfortab4">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<!-- <div id="toolTiponLanguage"></div> -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						<a href="javascript:headerChangeLanguage();" rel="tooltip" id="languageID" title="Current Language: ${ObjCommonSession.getCurrentLanguage()}" > <img class="logolanguage" src="<c:url value="/resources/assets/admin/layout3/img/Language_Icon.png"/>" alt=""/></a>
						
					</li>
					<li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
						<a href="javascript:history.back();" rel="tooltip" title="Back" id="backId" ><img class="logoreturn" src="<c:url value="/resources/assets/admin/layout3/img/Return.png"/>" alt=""/></a>				<!--</a>-->
					
						
					</li>
					<!-- END NOTIFICATION DROPDOWN -->
					<!-- BEGIN TODO DROPDOWN -->
					<!--  <li class="dropdown dropdown-extended dropdown-dark dropdown-tasks" id="header_task_bar">
						<a href="#"><img class="menuinfo" src="<c:url value="/resources/assets/admin/layout3/img/menu_option.jpg"/>" alt=""/></li></a>
						
					</li>-->
					<!-- END TODO DROPDOWN -->
					
					<!-- BEGIN INBOX DROPDOWN -->
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						  <a href="javascript:location.reload();" rel="tooltip" id="mainMenuId" title="Main Menu"> <img class="logoexit"  src="<c:url value="/resources/assets/admin/layout3/img/Exit.png"/>" alt=""/></a> 
						
					</li>
					<li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						<a href="javascript:;" class="dropdown-toggle" rel="tooltip"  data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="circle" id="circlecount">0</span>
						<span class="corner"></span>
						</a>
						<ul class="dropdown-menu" style="border-radius:5px !important;margin-right:8px;">
							<li class="external">
								<h3><strong>Your messages</strong></h3>
								
							</li>
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">	
									
									<li id="MenuMessageMessageDivID">
										
									</li>
									<li id="teamMemberMessageDivID">
										
									</li>
								
									
								</ul>
							</li>
						</ul>
					</li>

					<!-- END INBOX DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<%-- <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
					<%=objCommonsessionbe.getTeam_Member()%>&nbsp;&nbsp;&nbsp;&nbsp;  </span><img src="<c:url value="/resources/assets/admin/layout3/img/Logo2.png"/>" alt="logo" class="logo-default">
						</a> --%>
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username username-hide-on-mobile">
					${ObjCommonSession.getTeam_Member_Name()}&nbsp;&nbsp;&nbsp;&nbsp;  </span></a>
					<img id="changeimagelogofortablet" src="<c:url value="/resources/assets/admin/layout3/img/Logo2.png"/>" alt="logo" class="rightcompanylogo">
						
					
					
				
					
					<li id="header_inbox_bar">
        <a href="${pageContext.request.contextPath}/LogoutPage" id="logoutId" style="margin-left:-102px;width:30px;" rel="tooltip" title="Logout"> <img src="<c:url value="/resources/assets/admin/layout3/img/Logout_16X16.png"/>" alt=""/></a>
        
      
     </li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
		
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>

	<script>
	function getUTCDateTime(){

		var localdate =    moment.utc().format("YYYY-MM-DD");
		return   localdate;  
	
		}
	   function executeQuery() {
		   
			 var selectedValue=getUTCDateTime();
	   
	   $.ajax({
	     url: '${pageContext.request.contextPath}/MenuMessages',
	     cache: false,
	     data : {DateTime : selectedValue},
	     error: function() {
	        //$('#info').html('<p>An error has occurred</p>');
	     },
	      success: function(data) {
	    	
	      $('#MenuMessageMessageDivID').empty();
	        var MenuMessageSize=0;
	      for (var message in data){
	    	 
	      if((data[message].message)!=null){
	    	  MenuMessageSize=MenuMessageSize+1;
	             $('#MenuMessageMessageDivID').append('<a><span>'+data[message].message+'</span></a>');
	     
	      }
	        
	         
	      }
	    
	    $("#circlecount").val(MenuMessageSize);
	   
	    
	     },
	     type: 'GET'
	  });

	  };	
	  
	  function executeTeamMemberQuery() {

			 var selectedValue=getUTCDateTime();
	$.ajax({
	   url: '${pageContext.request.contextPath}/TeamMemberMessages',
	   cache: false,
	   data : {DateTime : selectedValue},
	   error: function() {
	      //$('#info').html('<p>An error has occurred</p>');
	   },
	   
	   success: function(data) {
	    $('#teamMemberMessageDivID').empty();
	    var teamMemberSize=0;
	      for (var team in data){
	       
	       if(data[team].message!=null && data[team].message != 'null' &&  data[team].message != 'Null'  &&  data[team].message != 'NULL' ){
	           $('#teamMemberMessageDivID').append('<a><span>'+data[team].message+'</span></a>');
	           teamMemberSize=teamMemberSize+1;
	       }
	         }
	      var teammessagecount= $("#circlecount").val();
	      var totalcount=Number(teamMemberSize)+Number(teammessagecount);
	      $("#circlecount").text(totalcount);
	    
	   },
	   type: 'GET'
	});


	};
	
	
	$(function() {
	    $( document ).tooltip();
	  });
	
   $(function() { 
    	
	   var style=document.getElementById( 'style_color' );
    	
	   if('${ObjCommonSession.getMenuType()}'==='FULLSCREEN')
		   {
		   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Full_Display()}.css";
		   }
	   else if('${ObjCommonSession.getMenuType()}'==='MOBILE' || '${ObjCommonSession.getMenuType()}'==='GLASS')
	   {
		   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_Mobile()}.css";
	   }
	   else if('${ObjCommonSession.getMenuType()}'==='RF')
	   {
		   style.href  ="${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/${ObjCommonSession.getTheme_RF()}.css";
	   }
   
   
	   
	   
    	//style.href  ="/${pageContext.request.contextPath}/resources/assets/admin/layout3/css/themes/green-haze.css";
    	//document.getElementsByTagName( 'head' )[0].appendChild( style );
    	
        
//Layout.init(); // init current layout

        });
   
  /*  $( "#LanguageChangeID" ).mouseover(function() {
	   $( "#toolTiponLanguage" ).clear();
	   $( "#toolTiponLanguage" ).append( "<div>Handler for .mouseover() called.</div>" );
	 }); */
    </script>
	<script>

	
    jQuery(document).ready(function() { 
    	
    	var ScreenType='FULLSCREEN';
		var agent = navigator.userAgent;      
	    var isWebkit = (agent.indexOf("AppleWebKit") > 0);      
	    var isIPad = (agent.indexOf("iPad") > 0);      
	    var isIOS = (agent.indexOf("iPhone") > 0 || agent.indexOf("iPod") > 0);     
	    var isAndroid = (agent.indexOf("Android")  > 0);     
	    var isNewBlackBerry = (agent.indexOf("AppleWebKit") > 0 && agent.indexOf("BlackBerry") > 0);     
	    var isWebOS = (agent.indexOf("webOS") > 0);      
	    var isWindowsMobile = (agent.indexOf("IEMobile") > 0);     
	    var isSmallScreen = (screen.width < 767 || (isAndroid && screen.width < 1000));     
	    var isUnknownMobile = (isWebkit && isSmallScreen);     
	    var isMobile = (isIOS || isAndroid || isNewBlackBerry || isWebOS || isWindowsMobile || isUnknownMobile);     
	    var isTablet = (isMobile && !isSmallScreen);  
	    var isGLASSX6 = (agent.indexOf("X6") > 0 || agent.indexOf("X7") > 0);
	    
	    if(isGLASSX6){
	    	
	    }
	    else if((isMobile && isSmallScreen)||isTablet)
	     {
	    	
	    	//this is tab3 css and condition
	    	if(screen.width<700) 
	    	{	   
	    		
	    		$("#changeimagelogofortablet").removeClass("rightcompanylogo").addClass("rightcompanylogo_tablet_tab3");
	    		$("#idcompanyname_middle").removeClass("companyname_middle").addClass("companyname_middle_tab4");
		    	
	    	}
	    	//this is tab4 css and condition
	    	else if(screen.width>750) 
	    	{	    
	    		
	    		$("#changeimagelogofortablet").removeClass("rightcompanylogo").addClass("rightcompanylogo_tablet_tab4");
	    		$("#idcompanyname_middle").removeClass("companyname_middle").addClass("companyname_middle_tab4");
	    		$("#pullleftfortab4").addClass("page_header_menu_tablet_tab4");
	    	
	    	}
	    	  
	     }
	    else{
    		
            document.getElementById("languageID").removeAttribute("rel");
            document.getElementById("mainMenuId").removeAttribute("rel");
            document.getElementById("logoutId").removeAttribute("rel");
            document.getElementById("backId").removeAttribute("rel");
            }
    	setInterval(function () {
			   executeQuery();
			   executeTeamMemberQuery();
			},1000);
  
    		Metronic.init();       
Layout.init(); // init current layout

        });
    
    window.AETid = "AE";

    function tooltip(tid) {
     document.getElementById(tid).style.display="block";
    }

    function hideTooltip(tid) {
     document.getElementById(tid).style.display="none";
    }
    
    $( function()
    		{
    		    var targets = $( '[rel~=tooltip]' ),
    		        target  = false,
    		        tooltip = false,
    		        title   = false;
    		 
    		    targets.bind( 'mouseenter', function()
    		    {
    		        target  = $( this );
    		        tip     = target.attr( 'title' );
    		        tooltip = $( '<div id="tooltip"></div>' );
    		 
    		        if( !tip || tip == '' )
    		            return false;
    		 
    		        target.removeAttr( 'title' );
    		        tooltip.css( 'opacity', 0 )
    		               .html( tip )
    		               .appendTo( 'body' );
    		 
    		        var init_tooltip = function()
    		        {
    		            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
    		                tooltip.css( 'max-width', $( window ).width() / 2 );
    		            else
    		                tooltip.css( 'max-width', 340 );
    		 
    		            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
    		                pos_top  = target.offset().top - tooltip.outerHeight() - 20;
    		 
    		            if( pos_left < 0 )
    		            {
    		                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
    		                tooltip.addClass( 'left' );
    		            }
    		            else
    		                tooltip.removeClass( 'left' );
    		 
    		            if( pos_left + tooltip.outerWidth() > $( window ).width() )
    		            {
    		                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
    		                tooltip.addClass( 'right' );
    		            }
    		            else
    		                tooltip.removeClass( 'right' );
    		 
    		            if( pos_top < 0 )
    		            {
    		                var pos_top  = target.offset().top + target.outerHeight();
    		                tooltip.addClass( 'top' );
    		            }
    		            else
    		                tooltip.removeClass( 'top' );
    		 
    		            tooltip.css( { left: pos_left, top: pos_top } )
    		                   .animate( { top: '+=10', opacity: 1 }, 50 );
    		        };
    		 
    		        init_tooltip();
    		        $( window ).resize( init_tooltip );
    		 
    		        var remove_tooltip = function()
    		        {
    		            tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
    		            {
    		                $( this ).remove();
    		            });
    		 
    		            target.attr( 'title', tip );
    		            target.attr( 'title', tip );
    		        };
    		 
    		        target.bind( 'mouseleave', remove_tooltip );
    		        tooltip.bind( 'click', remove_tooltip );
    		    });
    		});
    </script>
    <style>
 
 .companyname_middle_tab4 {
    float: left;
    display: block;
    width: 65%;
    height: 0px;
    text-align: center;
    position: absolute;
    top: 22px;
    font-size: 20px;
    font-weight: 600;
}
   
    .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > i, .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > i {
 
  color:#000000 !important ;

}
.dropdown-menu li:hover .sub-menu {
    visibility: visible;
}

.dropdown:hover .dropdown-menu {
    display: block;
}
#tooltip
{
    text-align: center;
    color: #fff;
    background: #111;
    position: absolute;
    z-index: 100;
    padding: 15px;
}
 
    #tooltip:after /* triangle decoration */
    {
        width: 0;
        height: 0;
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        border-top: 10px solid #111;
        content: '';
        position: absolute;
        left: 50%;
        bottom: -10px;
        margin-left: -10px;
    }
 
        #tooltip.top:after
        {
            border-top-color: transparent;
            border-bottom: 10px solid #111;
            top: -20px;
            bottom: auto;
        }
 
        #tooltip.left:after
        {
            left: 10px;
            margin: 0;
        }
 
        #tooltip.right:after
        {
            right: 10px;
            left: auto;
            margin: 0;
        }
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown.open .dropdown-toggle {
  
   background-color: transparent !important;
}
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : portrait)
{
   .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > i, .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > i {
 
  color:#000000 !important ;

}

.page-header .page-header-top .top-menu .navbar-nav > li.dropdown.open .dropdown-toggle {
  
   background-color: transparent !important;
}
}

@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
and (orientation : landscape)
{
   .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user:hover > .dropdown-toggle > i, .page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > .username,
.page-header .page-header-top .top-menu .navbar-nav > li.dropdown-user.open > .dropdown-toggle > i {
 
  color:#000000 !important ;

}

.page-header .page-header-top .top-menu .navbar-nav > li.dropdown.open .dropdown-toggle {
  
   background-color: transparent !important;
}
}
.tooltip {
    display: none;  
    position: absolute;
    z-index: 1000;
}
.rightcompanylogo_tablet_tab3{
     margin-left: 105px;
    margin-top: 0px;
     position: absolute;
    width: 116px;
}
.rightcompanylogo_tablet_tab4{
     margin-left: -50px;
    margin-top: 0px;
     position: absolute;
    width: 116px;
}
.page_header_menu_tablet_tab4 {
    padding: 0;
    margin-right: 80px !important;
    display: block;
    margin-top: -2px;
}
    </style>