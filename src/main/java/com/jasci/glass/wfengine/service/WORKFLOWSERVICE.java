/**
Date created: July 1 2015
Description: This class decide which engine will be run 
Created By: Pradeep Kumar
 */
package com.jasci.glass.wfengine.service;


public class WORKFLOWSERVICE {
	
	
	

	/**
	 * @description:This Method is used to insert data in Work History table
	 * @param: objProcessBE
	 * @param: objSharedData
	 * @return: True/False
	 * @developedby: Pradeep Kumar
	 * @Date: June 30 2015
	 *//*
	public boolean insertWorkHistory(SharedData obSharedData){
		
		
		return objProcessEngine.WorkHistoryLog(obSharedData);
		
	}
	
	*//**
	 * @description:This Method is used to insert data in Labor_performed table before execution task 
	 * @param: objProcessBE
	 * @param: objSharedData
	 * @return: True/False
	 * @developedby: Pradeep Kumar
	 * @Date: Jun 17 2015
	 *//*
	
	public boolean insertTaskBeforeExecution(SequenceBE objSequence,SharedData objSharedData,
			String strUUIDForTask,String strUUIDProcess,Date date){
		return objProcessEngine.TaskLogBeforeEntry(objSequence,objSharedData,strUUIDForTask,strUUIDProcess,date);
	}
	
	*//**
	 * @description:This Method is used to insert data in Process History
	 * @param: objProcessBE
	 * @param: objSharedData
	 * @return: True/False
	 * @developedby: Pradeep Kumar
	 * @Date: Jun 17 2015
	 *//*
	public boolean insertProcessBeforeExecution(SharedData objSharedData,String strUUID){
		return objProcessEngine.processBeforeLog(objSharedData, strUUID);
	}
	
	
	*//**
	 * @description:This Method is used to insert data in Process History after process complete
	 * @param: objProcessBE
	 * @param: objSharedData
	 * @return: True/False
	 * @developedby: Pradeep Kumar
	 * @Date: Jun 17 2015
	 *//*
	public boolean insertProcessAfterExecution(SharedData objSharedData,String strUUID){
		return objProcessEngine.processAfterLog(objSharedData, strUUID);
	}
	
	*//**
	 * @description:This Method is used to insert data in Labor_performed table after execution task 
	 * @param: objProcessBE
	 * @param: objSharedData
	 * @return: True/False
	 * @developedby: Pradeep Kumar
	 * @Date: jun 17 2015
	 *//*
	public boolean insertTaskAfterExecution(SequenceBE objSequence,SharedData objSharedData, 
			String strUUIDForTask,String strUUIDProcess,Date date){
		
		return objProcessEngine.TaskLogAfterEntry(objSequence, objSharedData, strUUIDForTask, strUUIDProcess, date);
	}
	
	
	
	
	

	*//**
	 * @description: This method get next task which will be execute and check current task status
	 * @param: objProcessBE
	 * @param: objSharedData
	 * @return:objTaskBE
	 * @developedby: Pradeep Kumar
	 * @Date: may 14 2015
	 * @updated: Jun 17 2015
	 *//*
	public SequenceBE getNextSequence(SharedData objSharedData){
		
		return objProcessEngine.getNextSequence(objSharedData);
	}
	
*//**
 * @description: This method is used for calling process engine or work flow engine 
 * @param objInputValue
 * @Developedby: Pradeep Kumar
 * @Date: Jun 2 2015
 *//*
	
	public Object startEngine(InputValueBE objInputValue){
		
		//declare shared class instance variable 
		SharedData objSharedData=null;
		
		//create instance of configuration file for getting config parameter
		CONFIGURATIONFILE objConfigurationFile=new CONFIGURATIONFILE();
		
		//getting config parameter flagProcessEngine boolean 
		Boolean flagProcessEngine=Boolean.valueOf(objConfigurationFile.getConfigProperty(GlobalConstant.strFlagWorkFLowEngine));
		
		// getting application context instance for getting object which is configure in spring-config.xml file
		ApplicationContext contextProcess = new ClassPathXmlApplicationContext(GlobalConstant.strSpringXMl);
		
		//check flagProcessEngine is true then call process engine instance.
		if(flagProcessEngine){
			
			//getting object of process engine here 
		ProcessEngine objProcessEngine=(ProcessEngine)contextProcess.getBean(GlobalConstant.strobjProcessEngine);
		
		//call init() method of process engine to initialize all value in process BE and objShared Data
		objSharedData=objProcessEngine.init(objInputValue);
		
		
		//calling process method of process Engine 
		objSharedData=objProcessEngine.Process(objSharedData,objInputValue);
		return objSharedData.getObjectView();
		
		}else{

			//getting object of work flow engine 
			WorkflowEngine objWorkFlowEngine=(WorkflowEngine)contextProcess.getBean(GlobalConstant.strobjWorkFlowEngine);
			
			
			//call init() method of work flow engine where we set all initial value
			objSharedData=objWorkFlowEngine.init(objInputValue);
			return objSharedData.getObjectView();
		}
		
		
		
		
	}*/
	

}
