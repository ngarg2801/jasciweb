/*

Date Developed  Sep 18 2014
Description   lookup General Codes in Table GENERAL_CODES
 */

package com.jasci.common.util;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utildal.LOOKUPGENERALCODEDAOIMPL;
import com.jasci.common.utilbe.GENERAL_CODES;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class LOOKUPGENERALCODE {

	/**
	 *Description That function design for get LookupGeneralCode and return LOOKUPGENERALCODEBE" 
	 *Input parameter "CommonSessionObj"
	 *Return Type "LOOKUPGENERALCODEBE"
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 */


	@Autowired
	private LOOKUPGENERALCODEDAOIMPL Obj_LookupGeneralCodeDao;

	@Transactional
	public List<GENERAL_CODES> GetLookupGeneralCode(COMMONSESSIONBE Obj_CommonSession) throws JASCIEXCEPTION {
		List<GENERAL_CODES> Obj_LookupGeneralCodeBE = new ArrayList<GENERAL_CODES>();
		if(Obj_CommonSession.getAttribute_Request().equalsIgnoreCase(GLOBALCONSTANT.Yes)){
			Obj_LookupGeneralCodeBE = Attribute_Request(Obj_CommonSession.getTenant(),Obj_CommonSession.getCompany(),Obj_CommonSession.getGeneralCodeID(),Obj_CommonSession.getGeneralCode());
			return Obj_LookupGeneralCodeBE;	
		}
		else if(Obj_CommonSession.getDrop_Down_Selection().equalsIgnoreCase(GLOBALCONSTANT.Yes)){
			Obj_LookupGeneralCodeBE = Drop_Down_Selection(Obj_CommonSession.getTenant(),Obj_CommonSession.getCompany(),Obj_CommonSession.getGeneralCodeID(),Obj_CommonSession.getGeneralCode());
			return Obj_LookupGeneralCodeBE;	
		}
		else{
			
			throw new JASCIEXCEPTION("Note :: Please do not fill Same value in both Attribute_Request , Drop_Down_Selection");
		}
			 
	}

	/**
	 *Description That function design for get LookupGeneralCode on bases of Attribute_Request return LOOKUPGENERALCODEBE" 
	 *Input parameter "CommonSessionObj"
	 *Return Type "LOOKUPGENERALCODEBE"
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 */
	@Transactional
	public List<GENERAL_CODES> Attribute_Request(String tenant, String company, String generalCodeID,String generalCode) throws JASCIEXCEPTION{


		List<GENERAL_CODES> Obj_GeneralCodeList = Obj_LookupGeneralCodeDao.GetLookupGeneralCode(tenant,company,generalCodeID,generalCode);

		GENERAL_CODES Obj_LookupGeneralCode = new GENERAL_CODES();
		if(!Obj_GeneralCodeList.isEmpty())
		{
			return Obj_GeneralCodeList;

		}

		Obj_LookupGeneralCode.setDescription20(GLOBALCONSTANT.NotFound);
		Obj_LookupGeneralCode.setDescription50(GLOBALCONSTANT.NotFound);

		Obj_GeneralCodeList.add(Obj_LookupGeneralCode);

		return Obj_GeneralCodeList;
	}

	/**
	 *Description That function design for get LookupGeneralCode on bases of Drop_Down_Selection return LOOKUPGENERALCODEBE" 
	 *Input parameter "CommonSessionObj"
	 *Return Type "LOOKUPGENERALCODEBE"
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 */
	@Transactional
	public List<GENERAL_CODES> Drop_Down_Selection(String tenant, String company, String generalCodeID,	String generalCode) throws JASCIEXCEPTION{

		List<Object> Obj_LookupGeneralArray =  Obj_LookupGeneralCodeDao.GetLookupGeneralCodeDropDown(tenant,company,generalCodeID,generalCode);
		List<GENERAL_CODES> Obj_LookupGeneralCodeList =  new ArrayList<GENERAL_CODES>();
		if(!Obj_LookupGeneralArray.isEmpty())
		{
		Iterator<Object> Obj_LookupGeneralCodeItr = Obj_LookupGeneralArray.iterator();

		while(Obj_LookupGeneralCodeItr.hasNext())
		{	
			Object[] GCObject = (Object[]) Obj_LookupGeneralCodeItr.next();
			
			GENERAL_CODES Obj_GeneralCode = new GENERAL_CODES();
			
			try{
			Obj_GeneralCode.setGeneralCode(GCObject[GLOBALCONSTANT.INT_ZERO].toString());
			}catch(Exception ObjException){}
			try{
			Obj_GeneralCode.setDescription20(GCObject[GLOBALCONSTANT.IntOne].toString());
			}catch(Exception ObjException){}
			try{
			Obj_GeneralCode.setDescription50(GCObject[GLOBALCONSTANT.INT_TWO].toString());
			}catch(Exception ObjException){}
			Obj_LookupGeneralCodeList.add(Obj_GeneralCode);


		}
		}

		return Obj_LookupGeneralCodeList;
	}

}
