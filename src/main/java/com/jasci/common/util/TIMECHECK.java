/*

Date Developed  Sep 18 2014
Description   This is used to set the time In UTC
 */
package com.jasci.common.util;

 
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TIMECHECK {
	
	
	
	/**
	 * @Description  Convert UTC To ClientTimeZone
	 * @author Rahul Kumar
	 * @Date Feb 05, 2015
	 * @param InputDate
	 * @param InputDateFormat
	 * @param InoutTimeZone
	 * @param OutputDateFormat
	 * @param OutputTimeZone
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public static String ConvertUTCToClientTimeZone(String InputDate,String InputDateFormat,String InoutTimeZone,String OutputDateFormat,String OutputTimeZone){
		
		//input data
		  
		 SimpleDateFormat sdfgmt = new SimpleDateFormat(InputDateFormat);
		 sdfgmt.setTimeZone(TimeZone.getTimeZone(InoutTimeZone));
		 
		 //output data
		 SimpleDateFormat sdfmad = new SimpleDateFormat(OutputDateFormat);
		 sdfmad.setTimeZone(TimeZone.getTimeZone(OutputTimeZone));
		    
		 Date inptdate = null;
		    try {
		    	
		        inptdate = sdfgmt.parse(InputDate);
		    } catch (ParseException e) {}

		    String resultOutputDate=sdfmad.format(inptdate);
		
		return resultOutputDate;
	}
	
	
}
