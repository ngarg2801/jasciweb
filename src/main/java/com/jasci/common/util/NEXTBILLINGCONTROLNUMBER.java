/**

Date Developed  Jan 07 2015
Developed By : Manish Chaurasia
Description   Get the next Billing Control Number and assignment for billing purposes 
 */

package com.jasci.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.common.utildal.NEXTBILLINGCONTROLNUMBERDAL;


@Service
public class NEXTBILLINGCONTROLNUMBER {
	
	
	@Autowired 
	NEXTBILLINGCONTROLNUMBERDAL OBJNextbillingcontrolnumberdal;
	
	  private static NEXTBILLINGCONTROLNUMBER objNextbillingcontrolnumber;
	    /**
	     * Create private constructor
	     */
	   //private NEXTBILLINGCONTROLNUMBER(){
	         
	  // }
	    /**
	     * Create a static method to get instance.
	     */
	    public static NEXTBILLINGCONTROLNUMBER getInstance(){
	        if(objNextbillingcontrolnumber == null){
	        	objNextbillingcontrolnumber = new NEXTBILLINGCONTROLNUMBER();
	        }
	        return objNextbillingcontrolnumber;
	    }
	     
    
	@Transactional    
	public Long getNextBillingControlNumber()
	
	{	
	
     Long NextBillingNumber =  OBJNextbillingcontrolnumberdal.getNextBillingControlNumber(); 
     NextBillingNumber = NextBillingNumber+1;
	  return NextBillingNumber; 
	}
	
}
