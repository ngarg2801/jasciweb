/*

Date Developed  Sep 18 2014
Description   lookup Team Member Information in Table TEAM_MEMBERS
 */

package com.jasci.common.util;




import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utildal.TEAMMEMBERLOOKUPDAOIMPL;
import com.jasci.common.utilbe.TEAM_MEMBERS;

import com.jasci.exception.JASCIEXCEPTION;

@Service
public class TEAMMEMBERLOOKUP {


	/*
	 *Description That function design for get teammember and return related data" 
	 *Input parameter "Team Member"
	 *Return Type "ArrayList"
	 *Created By "ANOOP SINGH"
	 *Created Date "23-09-2014" 
	 * */

	@Autowired
	private TEAMMEMBERLOOKUPDAOIMPL Obj_TeammemberLookupDAL;

	@Transactional
	public TEAM_MEMBERS GetTeammemberDetail(String TeamMember, String Tenant) throws JASCIEXCEPTION {

		List<TEAM_MEMBERS> Obj_TemaMembersList = Obj_TeammemberLookupDAL.GetTeammemberLookup(TeamMember, Tenant);
		TEAM_MEMBERS Obj_TeammemberLookup = new TEAM_MEMBERS();
		if(!Obj_TemaMembersList.isEmpty())
		{
			Iterator<TEAM_MEMBERS> Obj_TeamMemberIterator =Obj_TemaMembersList.iterator(); 

			while(Obj_TeamMemberIterator.hasNext())
			{
				Obj_TeammemberLookup = Obj_TeamMemberIterator.next(); 
				return Obj_TeammemberLookup;
			}
			
		}

		Obj_TeammemberLookup.setLastName(GLOBALCONSTANT.NoLongerInSystem);
		Obj_TeammemberLookup.setFirstName(TeamMember);

		return Obj_TeammemberLookup;
	}


}