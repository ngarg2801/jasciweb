/**


Description   Validates Addresses 
created by Aakash bishnoi
created date 25-9-14
*/

package com.jasci.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;


public class ADDRESSVALIDATION {

	public String StrURL;
	NETWORK Obj_Network = new NETWORK();
	HttpClient client = new DefaultHttpClient();
	char Result = '\0';
	
	/**
	 * @Description this function is use to validate address using google api
	 * @param addressLine1
	 * @param addressLine2
	 * @param addressLine3
	 * @param addressLine4
	 * @param City
	 * @param stateCode
	 * @param countryCode
	 * @param zipCode
	 * @return
	 * @throws IOException
	 * @throws JASCIEXCEPTION
	 */
	public char IsValidAddress(String addressLine1, String addressLine2, String addressLine3, String addressLine4,String City, String stateCode, String countryCode, String zipCode) throws IOException, JASCIEXCEPTION
	{
		String StreetAddress=(addressLine1.trim()+GLOBALCONSTANT.Single_Space.concat(addressLine2.trim()+GLOBALCONSTANT.Single_Space).concat(addressLine3.trim()+GLOBALCONSTANT.Single_Space).concat(addressLine4.trim())).trim();
		String FCity=City.trim();
		String StateCode=stateCode.trim();
		String CountryCode=countryCode.trim();
		String PostalCode=zipCode.trim();
		try {
		    HttpPost request = new HttpPost(GLOBALCONSTANT.AVAPIURL);
		    List <NameValuePair> Input = new ArrayList<NameValuePair>();
		    Input.add(new BasicNameValuePair(GLOBALCONSTANT.AVStreetAddress, StreetAddress));
		    Input.add(new BasicNameValuePair(GLOBALCONSTANT.AVCity, FCity));
		    Input.add(new BasicNameValuePair(GLOBALCONSTANT.AVPostalCode, PostalCode));
		    Input.add(new BasicNameValuePair(GLOBALCONSTANT.AVState, StateCode));
		    Input.add(new BasicNameValuePair(GLOBALCONSTANT.AVCountryCode, CountryCode));
		    Input.add(new BasicNameValuePair(GLOBALCONSTANT.AVLocale, GLOBALCONSTANT.AVLocale));
		    Input.add(new BasicNameValuePair(GLOBALCONSTANT.AVAPIKey, GLOBALCONSTANT.AVAPIKey));
		    request.setEntity(new UrlEncodedFormEntity(Input));
		    HttpResponse response = client.execute(request);
		    HttpEntity entity = response.getEntity();
		    String Output = EntityUtils.toString(entity, GLOBALCONSTANT.UTF8);
		    JSONParser parser = new JSONParser();
		    Object obj = parser.parse(Output);
		    JSONObject jsonObject = (JSONObject) obj;
		    String result = (String) jsonObject.get(GLOBALCONSTANT.AVStatus);
		    if (result.equalsIgnoreCase(GLOBALCONSTANT.AVVALID)) {
		        Result = GLOBALCONSTANT.Y;
		    }else if (result.equalsIgnoreCase(GLOBALCONSTANT.AVINVALID)){

		    	 Result =  GLOBALCONSTANT.N;
		    }else{
		    	String address=(addressLine1.trim()+GLOBALCONSTANT.Single_Space.concat(addressLine2.trim()+GLOBALCONSTANT.Single_Space).concat(addressLine3.trim()+GLOBALCONSTANT.Single_Space).concat(addressLine4.trim())).trim().replaceAll(GLOBALCONSTANT.space,GLOBALCONSTANT.PercentSignEncoder);
				
				String Gcity=City.trim().replaceAll(GLOBALCONSTANT.space,GLOBALCONSTANT.PercentSignEncoder);
				
				
				String GStateCode=stateCode.trim().replaceAll(GLOBALCONSTANT.space,GLOBALCONSTANT.PercentSignEncoder);
				
				String GCountryCode=countryCode.trim().replaceAll(GLOBALCONSTANT.space,GLOBALCONSTANT.PercentSignEncoder);
				
				String GZipCode=zipCode.trim().replaceAll(GLOBALCONSTANT.space,GLOBALCONSTANT.PercentSignEncoder);
				
				String StrTempAddressJson=GLOBALCONSTANT.AddressGoogleUrlAddress+address.trim();
				
					
					if(!GLOBALCONSTANT.BlankString.equalsIgnoreCase(GZipCode.trim())){
						StrTempAddressJson=StrTempAddressJson+GLOBALCONSTANT.GoogleUrlZip+GZipCode.trim();
					}
						
					if(!GLOBALCONSTANT.BlankString.equalsIgnoreCase(Gcity.trim())){
						StrTempAddressJson=StrTempAddressJson+GLOBALCONSTANT.GoogleUrlCity+Gcity.trim();
					}
					
					if(!GLOBALCONSTANT.BlankString.equalsIgnoreCase(GStateCode.trim())){
						StrTempAddressJson=StrTempAddressJson+GLOBALCONSTANT.GoogleUrlState+GStateCode.trim();
					}
					if(!GLOBALCONSTANT.BlankString.equalsIgnoreCase(GCountryCode.trim())){
						StrTempAddressJson=StrTempAddressJson+GLOBALCONSTANT.GoogleUrlCountry+GCountryCode.trim();
					}
						
						
					StrTempAddressJson=StrTempAddressJson+GLOBALCONSTANT.GoogleUrlClosing;
				
				String AddressJson=StrTempAddressJson.replaceAll(GLOBALCONSTANT.space,GLOBALCONSTANT.PercentSignEncoder);
				StrURL=GLOBALCONSTANT.AddressGoogleUrl+ AddressJson +GLOBALCONSTANT.AddressGoogleUrlKey;			

				try {
					String StrResponse=Obj_Network.getServerData(StrURL,GLOBALCONSTANT.Blank);

					JSONParser jsonParser = new JSONParser();
					Object object = jsonParser.parse(StrResponse);


					JSONObject GeocodeResponse = (JSONObject) object;
					if(GeocodeResponse.get(GLOBALCONSTANT.Status).toString().trim().equalsIgnoreCase(GLOBALCONSTANT.OK))
					{
						return GLOBALCONSTANT.Y;

					}
					else
					{

						return GLOBALCONSTANT.N;

					}
				} catch (Exception obj_e) {

					throw new JASCIEXCEPTION(obj_e.getMessage());
				}
		    }
		    
		} catch (IOException obj_ioe) {
			throw new JASCIEXCEPTION(obj_ioe.getMessage());
		} catch (ParseException obj_pe) {
			throw new JASCIEXCEPTION(obj_pe.getMessage());
		} finally {
		    client.getConnectionManager().shutdown();
		   
		}

		return Result;
	}
	
}
