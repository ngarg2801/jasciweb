//File purpose: This class sends an email
//File CreatedBy: Udit Saini
//Company Developed: NextGen Invent Corporation
//Created On: jan 21, 2013

package com.jasci.common.util;

import java.util.Date;
import java.util.Properties;


import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import com.jasci.biz.AdminModule.be.EMAILBE;
import com.jasci.common.constant.GLOBALCONSTANT;




public class SENDEMAIL {
	String to;String CC;String BCC;String subject;String message;String from;

	public static void main(String...  r){
		
		EMAILBE objEmailBE=new EMAILBE();
		new SENDEMAIL().sendMail(objEmailBE);
		
	}
	
	
	public boolean sendMail(EMAILBE objEmailBE){

		//	Email email=new Email();
		boolean result=false;
		to= objEmailBE.getUserEmail();
		CC=objEmailBE.getCC();
		BCC=objEmailBE.getBCC();
		subject=objEmailBE.getSubject();
		message=objEmailBE.getEventMessage();
		from=MAILCONFIGURATION.FROM;
		result=sendEmail(to,CC,BCC,subject,message,from);
		return result;
	}


	public static Boolean sendEmail(String to,String CC,String Bcc,String subject,String message,String from){

		Boolean result=false;

		if(to!=null){//edit by sarvendra tyagi
			if((!to.equals(GLOBALCONSTANT.BlankString))){//edit by sarvendra tyagi

				Properties PropsObj = new Properties();

				PropsObj.put(MAILCONFIGURATION.MailSmtpAuth, MAILCONFIGURATION.MailSmtpAuthValue);
				PropsObj.put(MAILCONFIGURATION.MailSmtpStarttlsEnable,MAILCONFIGURATION.MailSmtpStarttlsEnableValue );
				PropsObj.put(MAILCONFIGURATION.MailSmtpHost, MAILCONFIGURATION.MailSmtpHostValue); 
				PropsObj.put(MAILCONFIGURATION.MailSmtpPort, MAILCONFIGURATION.MailSmtpPortValue);
				Session session = Session.getInstance(PropsObj, new Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(MAILCONFIGURATION.SERVER_USERNAME, MAILCONFIGURATION.SERVER_PASSWORD);
					}
				});

				try {

					MimeMessage message1 = new MimeMessage(session);
					message1.setFrom(new InternetAddress(from));
					message1.setSentDate(new Date());

					message1.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
					if(CC!=null){//CC.length()!=0){
						message1.addRecipients(RecipientType.CC, CC);}
					if(Bcc!=null){//||Bcc.length()!=0){
						message1.addRecipients(RecipientType.BCC, Bcc);}
					message1.setSubject(subject);
					message1.setContent(message,GLOBALCONSTANT.HTMLCONTENT);
					Transport.send(message1);
					
					result=true;


				} catch (MessagingException mex) {
					mex.printStackTrace();
					result=false;

				}
			}
		}

		
		return result;
	}




}
