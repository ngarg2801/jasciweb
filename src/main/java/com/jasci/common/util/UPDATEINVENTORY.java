/*

Date Developed  Sep 18 2014
Description   Adjust Quantity in a location 
 */

package com.jasci.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.model.INVENTORY_LEVELS;
import com.jasci.biz.AdminModule.model.INVENTORY_LEVELSPK;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.INVENTORY_TRANSACTIONS;
import com.jasci.common.utilbe.INVENTORY_TRANSACTIONSPK;
import com.jasci.common.utildal.UPDATEINVENTORYDAOIMPL;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class UPDATEINVENTORY {

	/*
	 *Description That function design for Update And insert Inventory InfoHelp" 
	 *Input parameter "CommonSessionObj"
	 *Return Type ""
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 * */

	@Autowired
	private UPDATEINVENTORYDAOIMPL Obj_UPDATEINVENTORYDAL;

	@Transactional
	public String addorUpdateINVENTORY(COMMONSESSIONBE Obj_CommonSession) throws JASCIEXCEPTION {
		

		//Set value of commonsessionbe into inventry levels pojo
		INVENTORY_LEVELS Obj_Inventorylev = new INVENTORY_LEVELS();
		INVENTORY_LEVELSPK Obj_InventorylevPK = new INVENTORY_LEVELSPK();
		Obj_InventorylevPK.setTenant(Obj_CommonSession.getTenant());
		Obj_InventorylevPK.setCompany(Obj_CommonSession.getCompany());
		Obj_InventorylevPK.setFulfillmentCenter(Obj_CommonSession.getFulfillmentCenter());
		Obj_InventorylevPK.setArea(Obj_CommonSession.getArea());
		Obj_InventorylevPK.setInventroryControl01(Obj_CommonSession.getInventroryControl01());
		Obj_InventorylevPK.setInventroryControl02(Obj_CommonSession.getInventroryControl02());
		Obj_InventorylevPK.setInventroryControl03(Obj_CommonSession.getInventroryControl03());
		Obj_InventorylevPK.setInventroryControl04(Obj_CommonSession.getInventroryControl04());
		Obj_InventorylevPK.setInventroryControl05(Obj_CommonSession.getInventroryControl05());
		Obj_InventorylevPK.setInventroryControl06(Obj_CommonSession.getInventroryControl06());
		Obj_InventorylevPK.setInventroryControl07(Obj_CommonSession.getInventroryControl07());
		Obj_InventorylevPK.setInventroryControl08(Obj_CommonSession.getInventroryControl08());
		Obj_InventorylevPK.setInventroryControl09(Obj_CommonSession.getInventroryControl09());
		Obj_InventorylevPK.setInventroryControl10(Obj_CommonSession.getInventroryControl10());
		Obj_InventorylevPK.setSerial01(Obj_CommonSession.getSerial01());
		Obj_InventorylevPK.setSerial02(Obj_CommonSession.getSerial02());
		Obj_InventorylevPK.setSerial03(Obj_CommonSession.getSerial03());
		Obj_InventorylevPK.setSerial04(Obj_CommonSession.getSerial04());
		Obj_InventorylevPK.setSerial05(Obj_CommonSession.getSerial05());
		Obj_InventorylevPK.setSerial06(Obj_CommonSession.getSerial06());
		Obj_InventorylevPK.setSerial07(Obj_CommonSession.getSerial07());
		Obj_InventorylevPK.setSerial08(Obj_CommonSession.getSerial08());
		Obj_InventorylevPK.setSerial09(Obj_CommonSession.getSerial09());
		Obj_InventorylevPK.setSerial10(Obj_CommonSession.getSerial10());
		Obj_InventorylevPK.setSerial11(Obj_CommonSession.getSerial11());
		Obj_InventorylevPK.setSerial12(Obj_CommonSession.getSerial12());
		Obj_InventorylevPK.setQuality(Obj_CommonSession.getQuality());
		Obj_InventorylevPK.setLocation(Obj_CommonSession.getLocation());
		Obj_InventorylevPK.setLot(Obj_CommonSession.getLot());
		Obj_InventorylevPK.setLPN(Obj_CommonSession.getLPN());
		Obj_InventorylevPK.setProduct(Obj_CommonSession.getProduct());
		
		Obj_Inventorylev.setLastActivityTask(Obj_CommonSession.getTask());
		Obj_Inventorylev.setLastActivityEmployee(Obj_CommonSession.getTeam_Member());
		Obj_Inventorylev.setProductValue(Obj_CommonSession.getProductValue());
		Obj_Inventorylev.setUnitofMeasureCode(Obj_CommonSession.getUnitofMeasureCode());
		Obj_Inventorylev.setUnitofMeasureQty(Obj_CommonSession.getUnitofMeasureQty());
		Obj_Inventorylev.setId(Obj_InventorylevPK);


		//Set value of commonsessionbe into inventry transaction pojo
		INVENTORY_TRANSACTIONS Obj_Inventorytxn = new INVENTORY_TRANSACTIONS();
		INVENTORY_TRANSACTIONSPK Obj_InventorytxnPK = new INVENTORY_TRANSACTIONSPK();
		Obj_InventorytxnPK.setTenant(Obj_CommonSession.getTenant());
		Obj_InventorytxnPK.setCompany(Obj_CommonSession.getCompany());
		Obj_InventorytxnPK.setArea(Obj_CommonSession.getArea());
		Obj_InventorytxnPK.setFulfillmentCenter(Obj_CommonSession.getFulfillmentCenter());
		Obj_InventorytxnPK.setInventroryControl01(Obj_CommonSession.getInventroryControl01());
		Obj_InventorytxnPK.setInventroryControl02(Obj_CommonSession.getInventroryControl02());
		Obj_InventorytxnPK.setInventroryControl03(Obj_CommonSession.getInventroryControl03());
		Obj_InventorytxnPK.setInventroryControl04(Obj_CommonSession.getInventroryControl04());
		Obj_InventorytxnPK.setInventroryControl05(Obj_CommonSession.getInventroryControl05());
		Obj_InventorytxnPK.setInventroryControl06(Obj_CommonSession.getInventroryControl06());
		Obj_InventorytxnPK.setInventroryControl07(Obj_CommonSession.getInventroryControl07());
		Obj_InventorytxnPK.setInventroryControl08(Obj_CommonSession.getInventroryControl08());
		Obj_InventorytxnPK.setInventroryControl09(Obj_CommonSession.getInventroryControl09());
		Obj_InventorytxnPK.setInventroryControl10(Obj_CommonSession.getInventroryControl10());
		Obj_InventorytxnPK.setSerial(Obj_CommonSession.getSerial01());
		Obj_InventorytxnPK.setLocation(Obj_CommonSession.getLocation());
		Obj_InventorytxnPK.setLot(Obj_CommonSession.getLot());
		Obj_InventorytxnPK.setLPN(Obj_CommonSession.getLPN());
		Obj_InventorytxnPK.setProduct(Obj_CommonSession.getProduct());
		Obj_InventorytxnPK.setQuality(Obj_CommonSession.getQuality());
		Obj_Inventorytxn.setLastActivityTask(Obj_CommonSession.getTask());
		Obj_Inventorytxn.setLastActivityTeamMember(Obj_CommonSession.getTeam_Member());
		Obj_Inventorytxn.setInventoryValue(Obj_CommonSession.getInventoryValue());
		Obj_Inventorytxn.setInventoryPickSequence(Obj_CommonSession.getInventoryPickSequence());
		Obj_Inventorytxn.setCasePackQty(Obj_CommonSession.getCasePackQty());
		Obj_Inventorytxn.setPurchaseOrderNumber(Obj_CommonSession.getPurchaseOrderNumber());
		Obj_Inventorytxn.setQuantityBefore(GLOBALCONSTANT.INT_ZERO);
		Obj_Inventorytxn.setQuantityAfter(Obj_CommonSession.getAdjusted_Quantity());
		Obj_Inventorytxn.setQuantityTransaction(Obj_CommonSession.getAdjustment_Quantity());
		Obj_Inventorytxn.setReference(Obj_CommonSession.getReference());
		Obj_Inventorytxn.setWorkControlNumber(Obj_CommonSession.getWorkControlNumber());
		Obj_Inventorytxn.setWorkType(Obj_CommonSession.getWorkType());
		
		Obj_Inventorytxn.setId(Obj_InventorytxnPK);

		if(Obj_CommonSession.getLocationFlag().equalsIgnoreCase(GLOBALCONSTANT.Yes)){
			Obj_UPDATEINVENTORYDAL.NewProduct(Obj_Inventorylev, Obj_Inventorytxn);
			return GLOBALCONSTANT.InsertInventoryMessage;
		}else if(Obj_CommonSession.getLocationFlag().equalsIgnoreCase(GLOBALCONSTANT.No)){
			Obj_Inventorylev.setQuantity(Obj_CommonSession.getAdjusted_Quantity());
			Obj_Inventorytxn.setQuantityBefore(Obj_CommonSession.getAdjusted_Quantity());
			
			Obj_UPDATEINVENTORYDAL.UpdateProduct(Obj_Inventorylev, Obj_Inventorytxn);
			return GLOBALCONSTANT.UpdateInventoryMessage;
			
		}
		else{
			return GLOBALCONSTANT.InventoryErrorMessage;
			
		}
	}
}
