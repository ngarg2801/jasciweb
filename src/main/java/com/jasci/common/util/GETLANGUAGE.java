/*

Date Developed  Sep 18 2014
Description   Get Language Translation
 */

package com.jasci.common.util;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utildal.LANGUAGEDAOIMPL;
import com.jasci.common.utilbe.GETLANGUAGES;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class GETLANGUAGE {
	/*
	 *Description That function design for convert data according to language" 
	 *Input parameter "KeyPhrase , Language"
	 *Return Type "ArrayList"
	 *Created By "Aakash Bishnoi"
	 *Created Date "24-9-14"
	 * */
	
@Autowired
private LANGUAGEDAOIMPL Obj_GETLANGUAGEDAL;
		 
@Transactional
public GETLANGUAGES getLanguage(String keyPhrase, String language) throws JASCIEXCEPTION{
	try{
	List<GETLANGUAGES> ObjLangugeList = new ArrayList<GETLANGUAGES>();  
	ObjLangugeList = Obj_GETLANGUAGEDAL.GetLanguage(keyPhrase, language);
	Iterator<GETLANGUAGES> ObjIterator=ObjLangugeList.iterator();
	GETLANGUAGES ObjLanguge=ObjIterator.next();
	return ObjLanguge;
	}catch(Exception obj_e)
	{
		throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString+obj_e);
	}
}


}
