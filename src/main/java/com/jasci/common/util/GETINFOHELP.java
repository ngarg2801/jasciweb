/*

Date Developed  Sep 18 2014
Description   Get Info & Help and display
 */

package com.jasci.common.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;




import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utildal.GETINFOHELPDAL;
import com.jasci.common.utilbe.GETINFOHELPS;
import com.jasci.exception.JASCIEXCEPTION;


@Service
public class GETINFOHELP {

	/**
	 *Description That function design for get InfoHelp and return GETINFOHELPBE" 
	 *Input parameter "CommonSessionObj"
	 *Return Type "GETINFOHELPBE"
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 * */
	@Autowired
	private GETINFOHELPDAL objgetinfohelp;

	@Transactional
	public void GetInfoHelp(String infoHelp, String language, String infoHelpType, HttpServletRequest request, HttpServletResponse response) {
		//objgetinfohelp = new GETINFOHELPDAL();
		//HttpServletResponse response = null;
		GETINFOHELPS Obj_GETINFOHELPBE = new GETINFOHELPS();
		try{
		List<GETINFOHELPS> Obj_getinfohelps = objgetinfohelp.GetInfoHelps(infoHelp, language, infoHelpType);
		
		try{
			if(!Obj_getinfohelps.isEmpty())
			{		
				Iterator<GETINFOHELPS> ObjIterator=Obj_getinfohelps.iterator();
				Obj_GETINFOHELPBE=ObjIterator.next();
				String PDFPath = Obj_GETINFOHELPBE.getExecution();
				FileInputStream fileInputStream = null;
				BufferedInputStream bufferedInputStream = null;

			
				String []PDFFilePath = PDFPath.split(GLOBALCONSTANT.Forword_SLASH);
			
				String fileName = PDFFilePath[PDFFilePath.length-GLOBALCONSTANT.IntOne];
				try {
					fileInputStream = new java.io.FileInputStream(PDFPath);
					OutputStream outputStream = response.getOutputStream();
					response.setContentType(GLOBALCONSTANT.application_pdf);
					response.setHeader(GLOBALCONSTANT.Content_Disposition, GLOBALCONSTANT.attachment_filename+ fileName);
					bufferedInputStream = new java.io.BufferedInputStream(
							fileInputStream);
					byte[] bytes = new byte[bufferedInputStream.available()];
					response.setContentLength(bytes.length);
					int aByte = GLOBALCONSTANT.INT_ZERO;
					while ((aByte = bufferedInputStream.read()) != -GLOBALCONSTANT.IntOne) {
						outputStream.write(aByte);
					}
					outputStream.flush();
					bufferedInputStream.close();
					response.flushBuffer();
				}
				catch(Exception ex)
				{
					throw new JASCIEXCEPTION(GLOBALCONSTANT.File_is_not_exists);
				}
			}
			else
			{
				Obj_GETINFOHELPBE.setDescription20(GLOBALCONSTANT.No_InfoHelp_record_found);
			}
		
		}catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}
		}catch(Exception e)
		{
			
		}
	}

	
	@Transactional
	public List<GETINFOHELPS> GetInfoHelpDetails(String infoHelp, String language, String infoHelpType, HttpServletRequest request, HttpServletResponse response) throws JASCIEXCEPTION {
	
		List<GETINFOHELPS> Obj_getinfohelps = objgetinfohelp.GetInfoHelps(infoHelp, language, infoHelpType);
		
		return Obj_getinfohelps;
	}

}
