

/*

Date Developed  Sep 18 2014
Description   lookup Fulfillment Center Attributes in table FULFILLMENT_CENTERS
 */

package com.jasci.common.util;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utildal.GETFULFILLMENTCENTERSDAOIMPL;
import  com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.GETFULFILLMENTCENTERS;
import com.jasci.exception.JASCIEXCEPTION;
@Service
public class FULFILLMENTCENTERLOOKUP {

	/*
	 *Description That function is designed to get FulfillmentCenterLookupRecord and return FULFILLMENTCENTERLOOKUPBE Object" 
	 *Input parameter "CommonSession Object"
	 *Return Type "FULFILLMENTCENTERLOOKUPBE"
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 * */
	@Autowired
	private GETFULFILLMENTCENTERSDAOIMPL obj_FulfillmentCenterLookup;

	@Transactional
	public GETFULFILLMENTCENTERS GetFulfillmentCenterLookupRecord(COMMONSESSIONBE Obj_CommonSession) throws JASCIEXCEPTION {

		List<GETFULFILLMENTCENTERS> Obj_FulfillmentCenterLookupList = obj_FulfillmentCenterLookup.GetDataFromFulfillmentCenter(Obj_CommonSession);	
		

		GETFULFILLMENTCENTERS ObjFULFILLMENTCENTERS = new GETFULFILLMENTCENTERS();
		if(!Obj_FulfillmentCenterLookupList.isEmpty())
		{
			Iterator<GETFULFILLMENTCENTERS> ObjIterator=Obj_FulfillmentCenterLookupList.iterator();
			while(ObjIterator.hasNext())
			{
				ObjFULFILLMENTCENTERS=ObjIterator.next();
				return ObjFULFILLMENTCENTERS;
			}
		}


		ObjFULFILLMENTCENTERS.setName20(GLOBALCONSTANT.InvalidFulfillment);
		ObjFULFILLMENTCENTERS.setName50(GLOBALCONSTANT.InvalidFulfillmentCenter);

		return ObjFULFILLMENTCENTERS;

	}
}
