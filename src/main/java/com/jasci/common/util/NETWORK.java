/*

Date Developed  Sep 18 2014
Description   This class is use for Zip validation and address validation 
 */

package com.jasci.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public class NETWORK {

	/**
	 * 
	 * @param stream
	 * @param len
	 * @return
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	public String readIt(InputStream stream, int len)
			throws IOException, UnsupportedEncodingException {
		Reader reader = null;
		reader = new InputStreamReader(stream, GLOBALCONSTANT.UTF8);

		int character;
		StringBuffer strContent = new StringBuffer(GLOBALCONSTANT.BlankString);

		while ((character = reader.read()) != -1)
			strContent.append((char) character);

		String lines = strContent.toString();

		return lines;
	}

	//get the data from server
	
	/**
	 * 
	 * @param strURL
	 * @param strXmlToBeSent
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public String getServerData(String strURL,String strXmlToBeSent) throws JASCIEXCEPTION {
		String DataGet = GLOBALCONSTANT.BlankString;
		InputStream IsResponseStream = null;

		try {

			URL UrlObject = new URL(strURL+strXmlToBeSent);
			HttpURLConnection connection = (HttpURLConnection) UrlObject.openConnection();  // optional default is GET
			connection.setRequestMethod(GLOBALCONSTANT.MethodType);   //add request header
			connection.setRequestProperty(GLOBALCONSTANT.User_Agent,GLOBALCONSTANT.Mozilla);
			connection.getResponseCode();
			IsResponseStream = connection.getInputStream();
			DataGet = readIt(IsResponseStream,GLOBALCONSTANT.INT_ZERO);
			return DataGet;
		} catch (Exception e) {
			
			throw new JASCIEXCEPTION(e.getMessage());

		}


	}

}
