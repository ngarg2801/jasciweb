/**

Date Developed  Sep 18 2014
Description   Zip Code Addresses 
 */

package com.jasci.common.util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;


public class ZIPVALIDATION {


	public String StrURL;
	NETWORK Obj_Network = new NETWORK();

	/**
	 * Description That function design to check for zipcode validation and returns a value Y or N
	 * Input parameter "objAddress"
	 * Return Type "Char"
	 * Created By "Diksha Gupta"
	 * Created Date "25-9-14"
	 */

	public char IsValidZipcode(String Obj_CommonSession) throws JASCIEXCEPTION {

		String StrZip = Obj_CommonSession.trim().replaceAll(GLOBALCONSTANT.space, GLOBALCONSTANT.PercentSignEncoder);
		String StrTempZipcodeJson = StrZip;
		StrURL = GLOBALCONSTANT.GoogleUrlAddress + StrTempZipcodeJson + GLOBALCONSTANT.GoogleUrlSensor;

		try {
			String strServerResponse = Obj_Network.getServerData(StrURL, GLOBALCONSTANT.Blank);
			JSONParser jsonParser = new JSONParser();
			Object obj = jsonParser.parse(strServerResponse);
			JSONObject jsonObject = (JSONObject) obj;

			if (jsonObject.get(GLOBALCONSTANT.Status).toString().trim().equalsIgnoreCase(GLOBALCONSTANT.OK)) {
				JSONObject objObject1 = (JSONObject) ((JSONArray) jsonObject.get(GLOBALCONSTANT.Results)).get(GLOBALCONSTANT.INT_ZERO);
				JSONArray objArray = (JSONArray) objObject1.get(GLOBALCONSTANT.Types);

				if (objArray.contains(GLOBALCONSTANT.PostalCode)) {
					return GLOBALCONSTANT.Y;
				} else {
					return GLOBALCONSTANT.N;
				}
			} else {
				return GLOBALCONSTANT.N;

			}

		} catch (Exception obj_e) {
			
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}



	}



}