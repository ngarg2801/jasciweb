/**

Date Developed :September 15 2014
Created by: Shailendra Rajput
Description :Validate Custom execution path
 */
package com.jasci.common.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public class CUSTOMEXECUTIONPATHVALIDATION {
	
	/**
     * Parameters of the method to add an URL to the System classes. 
     */
    private static final Class<?>[] parameters = new Class[]{URL.class};

    /**
     * Adds a file to the classpath.
     * @param s a String pointing to the file
     * @throws IOException
     * @throws JASCIEXCEPTION 
     */
    public static void addFile(String CustomExecutionPath) throws IOException, JASCIEXCEPTION {
        File CustomExecutionFilePath = new File(CustomExecutionPath);
        addFile(CustomExecutionFilePath);
    }

    /**
     * Adds a file to the classpath
     * @param f the file to be added
     * @throws IOException
     * @throws JASCIEXCEPTION 
     */
    public static void addFile(File CustomExecutionFilePath) throws IOException, JASCIEXCEPTION {
        addURL(CustomExecutionFilePath.toURI().toURL());
    }

    /**
     * Adds the content pointed by the URL to the classpath.
     * @param u the URL pointing to the content to be added
     * @throws IOException
     */
    public static void addURL(URL CustomExecutionFileURL) throws IOException,JASCIEXCEPTION {
        URLClassLoader sysloader = (URLClassLoader)ClassLoader.getSystemClassLoader();
        Class<?> sysclass = URLClassLoader.class;
        try {
            Method method = sysclass.getDeclaredMethod(GLOBALCONSTANT.ADDURL,parameters);
            method.setAccessible(true);
            method.invoke(sysloader,new Object[]{ CustomExecutionFileURL }); 
        } catch (Throwable t) {
            throw new JASCIEXCEPTION(GLOBALCONSTANT.ERRORCOULDNOTADDURLTOSYSTEMCLASSLOADER);
        }        
    }

    @SuppressWarnings(GLOBALCONSTANT.Strunused)
	public static boolean validateCustomExecutionPath(String CustomExecutionPath) throws IOException, SecurityException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException, JASCIEXCEPTION{
        //addFile("E:/ExternalJAR/Addition.jar");
        try {
        	CustomExecutionPath = "com.ngi.test.Addition";
			Constructor<?> cs = ClassLoader.getSystemClassLoader().loadClass(CustomExecutionPath).getConstructor(String.class);
		} catch (ClassNotFoundException e) {
			System.out.println("Class not exist in this jar file.");
			return false;
		} catch (NoSuchMethodException e) {
			System.out.println("Class exist in this jar file.");
			return true;
		}
        //Addition instance = (Addition)cs.newInstance();
        //instance.test();\
        return true;
    }
	

}
