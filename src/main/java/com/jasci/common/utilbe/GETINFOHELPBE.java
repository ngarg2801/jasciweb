/*

Date Developed  Sep 18 2014
Description   Business Entity  
 */

package com.jasci.common.utilbe;

public class GETINFOHELPBE {
	
	private String InfoHelp;
	private String Language;
	private String InfoHelpType;
	private String Description20;
	private String Description50; 
	private String Execution;
	private String LastActivityDate;
	private String LastActivityTeamMember;
	
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getExecution() {
		return Execution;
	}
	public void setExecution(String execution) {
		Execution = execution;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}		
	public String getInfoHelp() {
		return InfoHelp;
	}
	public void setInfoHelp(String infoHelp) {
		InfoHelp = infoHelp;
	}
	public String getLanguage() {
		return Language;
	}
	public void setLanguage(String language) {
		Language = language;
	}
	public String getInfoHelpType() {
		return InfoHelpType;
	}
	public void setInfoHelpType(String infoHelpType) {
		InfoHelpType = infoHelpType;
	}

}
