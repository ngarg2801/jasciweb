/*

Date Developed  Oct 16 2014
Description   Infohelps
 */

package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_Infohelps)
public class GETINFOHELPS 
{
	@Id
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_InfoHelp)
	private String InfoHelp;

	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Language)
	private String Language;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_InfoHelpType)
	private String InfoHelpType;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Description20)
	private String Description20;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Description50)
	private String Description50;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Execution)
	private String Execution;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_LastActivityDate)
	private String LastActivityDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_LastActivityTeamMember)
	private String LastActivityTeamMember;

	public String getInfoHelp() {
		return InfoHelp;
	}

	public void setInfoHelp(String infoHelp) {
		InfoHelp = infoHelp;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getInfoHelpType() {
		return InfoHelpType;
	}

	public void setInfoHelpType(String infoHelpType) {
		InfoHelpType = infoHelpType;
	}

	public String getDescription20() {
		return Description20;
	}

	public void setDescription20(String description20) {
		Description20 = description20;
	}

	public String getDescription50() {
		return Description50;
	}

	public void setDescription50(String description50) {
		Description50 = description50;
	}

	public String getExecution() {
		return Execution;
	}

	public void setExecution(String execution) {
		Execution = execution;
	}

	public String getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	public GETINFOHELPS(String infoHelp, String language, String infoHelpType,
			String description20, String description50, String execution,
			String lastActivityDate, String lastActivityTeamMember) {
		super();
		InfoHelp = infoHelp;
		Language = language;
		InfoHelpType = infoHelpType;
		Description20 = description20;
		Description50 = description50;
		Execution = execution;
		LastActivityDate = lastActivityDate;
		LastActivityTeamMember = lastActivityTeamMember;
	}

	public GETINFOHELPS() 
	{
	
	}
	
	
	
	
	
}
