/*

Date Developed  Sep 18 2014
Description   Business Entity  
 */

package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;
@Entity
@Table(name=GLOBALCONSTANT.TableName_GENERAL_CODES)

public class GENERAL_CODES {
	
	@Id
	@GeneratedValue
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Tenant)
	private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Company)
	private String Company;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Application)
	private String Application;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_GeneralCodeID)
	private String GeneralCodeID;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_GeneralCode)
	private String GeneralCode;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_SystemUse)
	private String SystemUse;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Description20)
	private String Description20;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Description50)
	private String Description50;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_MenuOptionName)
	private String MenuOptionName;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber01)
	private String ControlNumber01;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber02)
	private String ControlNumber02;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber03)
	private String ControlNumber03;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber04)
	private String ControlNumber04;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber05)
	private String ControlNumber05;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber06)
	private String ControlNumber06;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber07)
	private String ControlNumber07;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber08)
	private String ControlNumber08;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber09)
	private String ControlNumber09;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_ControlNumber10)
	private String ControlNumber10;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control01Description)
	private String Control01Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control01Value)
	private String Control01Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control02Description)
	private String Control02Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control02Value)
	private String Control02Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control03Description)
	private String Control03Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control03Value)
	private String Control03Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control04Description)
	private String Control04Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control04Value)
	private String Control04Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control05Description)
	private String Control05Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control05Value)
	private String Control05Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control06Description)
	private String Control06Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control06Value)
	private String Control06Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control07Description)
	private String Control07Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control07Value)
	private String Control07Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control08Description)
	private String Control08Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control08Value)
	private String Control08Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control09Description)
	private String Control09Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control09Value)
	private String Control09Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control10Description)
	private String Control10Description;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Control10Value)
	private String Control10Value;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_Helpline)
	private String Helpline ;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_LastActivityDate)
	private String LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_GENERAL_CODES_LastActivityTeamMember)
	private String LastActivityTeamMember;
	
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getApplication() {
		return Application;
	}
	public void setApplication(String application) {
		Application = application;
	}
	public String getGeneralCodeID() {
		return GeneralCodeID;
	}
	public void setGeneralCodeID(String generalCodeID) {
		GeneralCodeID = generalCodeID;
	}
	public String getGeneralCode() {
		return GeneralCode;
	}
	public void setGeneralCode(String generalCode) {
		GeneralCode = generalCode;
	}
	public String getSystemUse() {
		return SystemUse;
	}
	public void setSystemUse(String systemUse) {
		SystemUse = systemUse;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getMenuOptionName() {
		return MenuOptionName;
	}
	public void setMenuOptionName(String menuOptionName) {
		MenuOptionName = menuOptionName;
	}
	public String getControlNumber01() {
		return ControlNumber01;
	}
	public void setControlNumber01(String controlNumber01) {
		ControlNumber01 = controlNumber01;
	}
	public String getControlNumber02() {
		return ControlNumber02;
	}
	public void setControlNumber02(String controlNumber02) {
		ControlNumber02 = controlNumber02;
	}
	public String getControlNumber03() {
		return ControlNumber03;
	}
	public void setControlNumber03(String controlNumber03) {
		ControlNumber03 = controlNumber03;
	}
	public String getControlNumber04() {
		return ControlNumber04;
	}
	public void setControlNumber04(String controlNumber04) {
		ControlNumber04 = controlNumber04;
	}
	public String getControlNumber05() {
		return ControlNumber05;
	}
	public void setControlNumber05(String controlNumber05) {
		ControlNumber05 = controlNumber05;
	}
	public String getControlNumber06() {
		return ControlNumber06;
	}
	public void setControlNumber06(String controlNumber06) {
		ControlNumber06 = controlNumber06;
	}
	public String getControlNumber07() {
		return ControlNumber07;
	}
	public void setControlNumber07(String controlNumber07) {
		ControlNumber07 = controlNumber07;
	}
	public String getControlNumber08() {
		return ControlNumber08;
	}
	public void setControlNumber08(String controlNumber08) {
		ControlNumber08 = controlNumber08;
	}
	public String getControlNumber09() {
		return ControlNumber09;
	}
	public void setControlNumber09(String controlNumber09) {
		ControlNumber09 = controlNumber09;
	}
	public String getControlNumber10() {
		return ControlNumber10;
	}
	public void setControlNumber10(String controlNumber10) {
		ControlNumber10 = controlNumber10;
	}
	public String getControl01Description() {
		return Control01Description;
	}
	public void setControl01Description(String control01Description) {
		Control01Description = control01Description;
	}
	public String getControl01Value() {
		return Control01Value;
	}
	public void setControl01Value(String control01Value) {
		Control01Value = control01Value;
	}
	public String getControl02Description() {
		return Control02Description;
	}
	public void setControl02Description(String control02Description) {
		Control02Description = control02Description;
	}
	public String getControl02Value() {
		return Control02Value;
	}
	public void setControl02Value(String control02Value) {
		Control02Value = control02Value;
	}
	public String getControl03Description() {
		return Control03Description;
	}
	public void setControl03Description(String control03Description) {
		Control03Description = control03Description;
	}
	public String getControl03Value() {
		return Control03Value;
	}
	public void setControl03Value(String control03Value) {
		Control03Value = control03Value;
	}
	public String getControl04Description() {
		return Control04Description;
	}
	public void setControl04Description(String control04Description) {
		Control04Description = control04Description;
	}
	public String getControl04Value() {
		return Control04Value;
	}
	public void setControl04Value(String control04Value) {
		Control04Value = control04Value;
	}
	public String getControl05Description() {
		return Control05Description;
	}
	public void setControl05Description(String control05Description) {
		Control05Description = control05Description;
	}
	public String getControl05Value() {
		return Control05Value;
	}
	public void setControl05Value(String control05Value) {
		Control05Value = control05Value;
	}
	public String getControl06Description() {
		return Control06Description;
	}
	public void setControl06Description(String control06Description) {
		Control06Description = control06Description;
	}
	public String getControl06Value() {
		return Control06Value;
	}
	public void setControl06Value(String control06Value) {
		Control06Value = control06Value;
	}
	public String getControl07Description() {
		return Control07Description;
	}
	public void setControl07Description(String control07Description) {
		Control07Description = control07Description;
	}
	public String getControl07Value() {
		return Control07Value;
	}
	public void setControl07Value(String control07Value) {
		Control07Value = control07Value;
	}
	public String getControl08Description() {
		return Control08Description;
	}
	public void setControl08Description(String control08Description) {
		Control08Description = control08Description;
	}
	public String getControl08Value() {
		return Control08Value;
	}
	public void setControl08Value(String control08Value) {
		Control08Value = control08Value;
	}
	public String getControl09Description() {
		return Control09Description;
	}
	public void setControl09Description(String control09Description) {
		Control09Description = control09Description;
	}
	public String getControl09Value() {
		return Control09Value;
	}
	public void setControl09Value(String control09Value) {
		Control09Value = control09Value;
	}
	public String getControl10Description() {
		return Control10Description;
	}
	public void setControl10Description(String control10Description) {
		Control10Description = control10Description;
	}
	public String getControl10Value() {
		return Control10Value;
	}
	public void setControl10Value(String control10Value) {
		Control10Value = control10Value;
	}
	public String getHelpline() {
		return Helpline;
	}
	public void setHelpline(String helpline) {
		Helpline = helpline;
	}
		public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	
	
}
