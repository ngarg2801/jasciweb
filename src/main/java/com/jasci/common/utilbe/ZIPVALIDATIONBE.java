/*

Date Developed  Sep 18 2014
Description   create the getter and setter for zip validation  
 */

package com.jasci.common.utilbe;

public class ZIPVALIDATIONBE {
	
	private String City;
	private String StateCode;
	private String CountryCode;
	private String ZipCode;
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getStateCode() {
		return StateCode;
	}
	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	
	

}
