/*

Date Developed  Oct 20 2014
Description   getter and setter for NOTES Table
 */
package com.jasci.common.utilbe;

public class NOTESBE {
	
	
	private String Tenant_Id;
	private String Company_Id;
	private String Note_Id;
	private String Note_Link;
	private int ID;
	private String	Last_Activity_Date;
	private String  Last_Activity_Team_Member;
	private String	Note;

	
	public String getTenant_Id()
	{
		return Tenant_Id;
	}

	
	public void setTenant_Id(String tenant_Id)
	{
		Tenant_Id=tenant_Id;
	}
	
	
	public String getCompany_Id()
	{
		return Company_Id;
	}

	
	public void setCompany_Id(String company_Id)
	{
		Company_Id=company_Id;
	}
	
	
	public String getNote_Id()
	{
		return Note_Id;
	}

	
	public void setNote_Id(String note_Id)
	{
		Note_Id=note_Id;
	}
	
	
	public int getID()
	{
		return ID;
	}

	
	public void setID(int Id)
	{
		ID=Id;
	}
	
	
	
	public String getNote_Link()
	{
		return Note_Link;
	}

	
	public void setNote_Link(String note_Link)
	{
		Note_Link=note_Link;
	}
	
	
	public String getNotes_Date()
	{
		return Last_Activity_Date;
	}

	
	public void setNotes_Date(String last_Activity_Date)
	{
		Last_Activity_Date=last_Activity_Date;
	}
	
	
	public String getNotes_Team_Member()
	{
		return Last_Activity_Team_Member;
	}

	
	public void setNotes_Team_Member(String last_Activity_Team_Member)
	{
		Last_Activity_Team_Member=last_Activity_Team_Member;
	}
	
	public String getNotes_note()
	{
		return Note;
	}

	
	public void setNotes_note(String note)
	{
		Note=note;
	}

}
