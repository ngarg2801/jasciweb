/*

Date Developed  Oct 15 2014
Description   pojo class and gettter and setter  for INVENTORY_TRANSACTIONSPK table 
 */
package com.jasci.common.utilbe;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

/**
 * @author HP-USA-004
 *
 */
@Embeddable
public class INVENTORY_TRANSACTIONSPK  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Tenant)
	private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_FulfillmentCenter)
	private String FulfillmentCenter;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Company)
	private String Company;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Area)
	private String Area;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Location)
	private String Location;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Product)
	private String Product;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Quality)
	private String Quality;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Lot)
	private String Lot;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Serial)
	private String Serial;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_LPN)
	private String LPN;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl01)
	private String InventroryControl01;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl02)
	private String InventroryControl02;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl03)
	private String InventroryControl03;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl04)
	private String InventroryControl04;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl05)
	private String InventroryControl05;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl06)
	private String InventroryControl06;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl07)
	private String InventroryControl07;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl08)
	private String InventroryControl08;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl09)
	private String InventroryControl09;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventroryControl10)
	private String InventroryControl10;
	
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getProduct() {
		return Product;
	}
	public void setProduct(String product) {
		Product = product;
	}
	public String getQuality() {
		return Quality;
	}
	public void setQuality(String quality) {
		Quality = quality;
	}
	public String getLot() {
		return Lot;
	}
	public void setLot(String lot) {
		Lot = lot;
	}
	public String getSerial() {
		return Serial;
	}
	public void setSerial(String serial) {
		Serial = serial;
	}
	public String getLPN() {
		return LPN;
	}
	public void setLPN(String lPN) {
		LPN = lPN;
	}
	public String getInventroryControl01() {
		return InventroryControl01;
	}
	public void setInventroryControl01(String inventroryControl01) {
		InventroryControl01 = inventroryControl01;
	}
	public String getInventroryControl02() {
		return InventroryControl02;
	}
	public void setInventroryControl02(String inventroryControl02) {
		InventroryControl02 = inventroryControl02;
	}
	public String getInventroryControl03() {
		return InventroryControl03;
	}
	public void setInventroryControl03(String inventroryControl03) {
		InventroryControl03 = inventroryControl03;
	}
	public String getInventroryControl04() {
		return InventroryControl04;
	}
	public void setInventroryControl04(String inventroryControl04) {
		InventroryControl04 = inventroryControl04;
	}
	public String getInventroryControl05() {
		return InventroryControl05;
	}
	public void setInventroryControl05(String inventroryControl05) {
		InventroryControl05 = inventroryControl05;
	}
	public String getInventroryControl06() {
		return InventroryControl06;
	}
	public void setInventroryControl06(String inventroryControl06) {
		InventroryControl06 = inventroryControl06;
	}
	public String getInventroryControl07() {
		return InventroryControl07;
	}
	public void setInventroryControl07(String inventroryControl07) {
		InventroryControl07 = inventroryControl07;
	}
	public String getInventroryControl08() {
		return InventroryControl08;
	}
	public void setInventroryControl08(String inventroryControl08) {
		InventroryControl08 = inventroryControl08;
	}
	public String getInventroryControl09() {
		return InventroryControl09;
	}
	public void setInventroryControl09(String inventroryControl09) {
		InventroryControl09 = inventroryControl09;
	}
	public String getInventroryControl10() {
		return InventroryControl10;
	}
	public void setInventroryControl10(String inventroryControl10) {
		InventroryControl10 = inventroryControl10;
	}
	
}
