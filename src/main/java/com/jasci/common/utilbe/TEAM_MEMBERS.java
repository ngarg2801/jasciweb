/*

Date Developed  Oct 15 2014
Description   getter and setter for TEAM_MEMBERS
 */

package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_TEAM_MEMBERS)
public class TEAM_MEMBERS {
	
	@Id
	@GeneratedValue
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Tenant)
	private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_TeamMember)
	private String TeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_FulfillmentCenter)
	private long FulfillmentCenter;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Department)
	private String Department;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_LastName)
	private String LastName;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_FirstName)
	private String FirstName;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_MiddleName)
	private String MiddleName ;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_AddressLine1)
	private String AddressLine1;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_AddressLine2)
	private String AddressLine2;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_AddressLine3)
	private String AddressLine3;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_AddressLine4)
	private String AddressLine4;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_City)
	private String City;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_StateCode)
	private String StateCode;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_CountryCode)
	private String CountryCode;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_ZipCode)
	private String ZipCode;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_WorkPhone)
	private String WorkPhone;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_WorkExtension)
	private String WorkExtension;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_WorkCell)
	private String WorkCell;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_WorkFax)
	private String WorkFax;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_HomePhone)
	private String HomePhone;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Cell)
	private String Cell;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_EmergencyContactName)
	private String EmergencyContactName;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_EmergencyContactHomePhone)
	private String EmergencyContactHomePhone;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_EmergencyContactCell)
	private String EmergencyContactCell;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_ShiftCode)
	private String ShiftCode;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_SystemUse)
	private String SystemUse;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_AuthorityProfile)
	private String AuthorityProfile;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_TaskProfile)
	private String TaskProfile;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Menu_Profile_Glass)
	private String Menu_Profile_Glass;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Menu_Profile_Tablet)
	private String Menu_Profile_Tablet;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Menu_Profile_Mobile)
	private String Menu_Profile_Mobile;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Menu_Profile_Station)
	private String Menu_Profile_Station;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Menu_Profile_RF)
	private String Menu_Profile_RF;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_EquipmentCertification)
	private String EquipmentCertification;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_Langage)
	private String LANGUAGE;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_StartDate)
	private String StartDate;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_LastDate)
	private String LastDate;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_SetupDate)
	private int SetupDate;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_SetupBy)
	private String SetupBy;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_LastActivityDate)
	private String LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_LastActivityTeamMember)
	private String LastActivityTeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_TEAM_MEMBERS_CurrentStatus)
	private String CurrentStatus;
	
	public String getDepartment() {
		return Department;
	}
	public void setDepartment(String department) {
		Department = department;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}
	public String getAddressLine1() {
		return AddressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return AddressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return AddressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		AddressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return AddressLine4;
	}
	public void setAddressLine4(String addressLine4) {
		AddressLine4 = addressLine4;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getStateCode() {
		return StateCode;
	}
	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getWorkPhone() {
		return WorkPhone;
	}
	public void setWorkPhone(String workPhone) {
		WorkPhone = workPhone;
	}
	public String getWorkExtension() {
		return WorkExtension;
	}
	public void setWorkExtension(String workExtension) {
		WorkExtension = workExtension;
	}
	public String getWorkCell() {
		return WorkCell;
	}
	public void setWorkCell(String workCell) {
		WorkCell = workCell;
	}
	public String getWorkFax() {
		return WorkFax;
	}
	public void setWorkFax(String workFax) {
		WorkFax = workFax;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getCell() {
		return Cell;
	}
	public void setCell(String cell) {
		Cell = cell;
	}
	public String getEmergencyContactName() {
		return EmergencyContactName;
	}
	public void setEmergencyContactName(String emergencyContactName) {
		EmergencyContactName = emergencyContactName;
	}
	public String getEmergencyContactHomePhone() {
		return EmergencyContactHomePhone;
	}
	public void setEmergencyContactHomePhone(String emergencyContactHomePhone) {
		EmergencyContactHomePhone = emergencyContactHomePhone;
	}
	public String getEmergencyContactCell() {
		return EmergencyContactCell;
	}
	public void setEmergencyContactCell(String emergencyContactCell) {
		EmergencyContactCell = emergencyContactCell;
	}
	public String getShiftCode() {
		return ShiftCode;
	}
	public void setShiftCode(String shiftCode) {
		ShiftCode = shiftCode;
	}
	public String getSystemUse() {
		return SystemUse;
	}
	public void setSystemUse(String systemUse) {
		SystemUse = systemUse;
	}
	public String getAuthorityProfile() {
		return AuthorityProfile;
	}
	public void setAuthorityProfile(String authorityProfile) {
		AuthorityProfile = authorityProfile;
	}
	public String getTaskProfile() {
		return TaskProfile;
	}
	public void setTaskProfile(String taskProfile) {
		TaskProfile = taskProfile;
	}
	public String getMenu_Profile_Glass() {
		return Menu_Profile_Glass;
	}
	public void setMenu_Profile_Glass(String menu_Profile_Glass) {
		Menu_Profile_Glass = menu_Profile_Glass;
	}
	public String getMenu_Profile_Tablet() {
		return Menu_Profile_Tablet;
	}
	public void setMenu_Profile_Tablet(String menu_Profile_Tablet) {
		Menu_Profile_Tablet = menu_Profile_Tablet;
	}
	public String getMenu_Profile_Mobile() {
		return Menu_Profile_Mobile;
	}
	public void setMenu_Profile_Mobile(String menu_Profile_Mobile) {
		Menu_Profile_Mobile = menu_Profile_Mobile;
	}
	public String getMenu_Profile_Station() {
		return Menu_Profile_Station;
	}
	public void setMenu_Profile_Station(String menu_Profile_Station) {
		Menu_Profile_Station = menu_Profile_Station;
	}
	public String getMenu_Profile_RF() {
		return Menu_Profile_RF;
	}
	public void setMenu_Profile_RF(String menu_Profile_RF) {
		Menu_Profile_RF = menu_Profile_RF;
	}
	public String getEquipmentCertification() {
		return EquipmentCertification;
	}
	public void setEquipmentCertification(String equipmentCertification) {
		EquipmentCertification = equipmentCertification;
	}
	
	public String getLANGUAGE() {
		return LANGUAGE;
	}
	public void setLANGUAGE(String lANGUAGE) {
		LANGUAGE = lANGUAGE;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getLastDate() {
		return LastDate;
	}
	public void setLastDate(String lastDate) {
		LastDate = lastDate;
	}
	public long getSetupDate() {
		return SetupDate;
	}
	public void setSetupDate(int setupDate) {
		SetupDate = setupDate;
	}
	public String getSetupBy() {
		return SetupBy;
	}
	public void setSetupBy(String setupBy) {
		SetupBy = setupBy;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getCurrentStatus() {
		return CurrentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		CurrentStatus = currentStatus;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getTeamMember() {
		return TeamMember;
	}
	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}
	public long getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(long fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
}
