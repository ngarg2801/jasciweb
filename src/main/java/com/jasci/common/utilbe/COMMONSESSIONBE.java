/*

Date Developed  Sep 18 2014
Description   Business Entity  
 */

package com.jasci.common.utilbe;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;





public class COMMONSESSIONBE implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String ClietnDeviceTimeZone= "";
	
	public String getClietnDeviceTimeZone() {
		return ClietnDeviceTimeZone;
	}
	public void setClietnDeviceTimeZone(String clietnDeviceTimeZone) {
		ClietnDeviceTimeZone = clietnDeviceTimeZone;
	}
	private String Tenant;
	private String Tenant_NAME20;
	private String Tenant_NAME50;
	private String Company;
	private String Company_Name_20;
	private String Company_Name_50;
	private String Fulfillment_Center_Name_20;
	private String Fulfillment_Center_Name_50;
	private String PurchaseOrdersRequireApproval;
	private String Team_Member;
	private String Tenant_Language;
	private String Team_Member_Language;
	private String Team_Member_Name;
	private String Authority_Profile;
	private String Menu_Profile_Tablet;
	private String Menu_Profile_Station;
	private String Menu_Profile_RF;
	private String Menu_Profile_Glass;
	private String Equipment_Certification;
	private String Theme_Mobile;
	private String Theme_RF;
	private String Theme_Full_Display;
	private String FulfillmentCenter;
	private String Area;
	private String Location;
	private String LocationFlag;
	private String Product;
	private String Quality;
	private String Lot;
	private String Serial;
	private String Serial01;
	private String Serial02;
	private String Serial03;
	private String Serial04;
	private String Serial05;
	private String Serial06;
	private String Serial07;
	private String Serial08;
	private String Serial09;
	private String Serial10;
	private String Serial11;
	private String Serial12;
	private String LPN;
	private String InventroryControl01;
	private String InventroryControl02;
	private String InventroryControl03;
	private String InventroryControl04;
	private String InventroryControl05;
	private String InventroryControl06;
	private String InventroryControl07;
	private String InventroryControl08;
	private String InventroryControl09;
	private String InventroryControl10;
	private String Quantity_being_adjusted;
	private String Adjustment_Quantity;
	private int Adjusted_Quantity;
	private String Task;
	private String WorkType;
	private String WorkControlNumber;
	private String UnitofMeasureCode;
	private int UnitofMeasureQty;
	private int ProductValue;
	private String PurchaseOrderNumber;
	private String Reference;
	private String InventoryValue;
	private String InventoryPickSequence;
	private String CasePackQty;	
	private String Language;
	private String InfoHelp;
	private String InfoHelpType;
	private String Attribute_Request;
	private String Drop_Down_Selection;
	private String GeneralCode;
	private String GeneralCodeID;
	private String KeyPhrase;

	private String AddressLine1;
	private String AddressLine2;
	private String AddressLine3;
	private String AddressLine4;
	private String City;
	private String StateCode;
	private String CountryCode;
	private String ZipCode;
	
	
	private String Status = "";
	private String PasswordDate = "";
	private String PasswordExpirationDays = "";
	private String UserID = "";
	private String Password = "";
	private String SystemUse = "";
	private String UserLoginDetails = "";
	private String CurrentLanguage = "";
	private String FooterText = "";
	private String ForValidEmaildID = "";

	/** Created for get and set Tenant value from Config file **/
	private String Jasci_Tenant;
	
	public String getJasci_Tenant() {
		return Jasci_Tenant;
	}
	public void setJasci_Tenant(String jasci_Tenant) {
		Jasci_Tenant = jasci_Tenant;
	}
	
	public String getForValidEmaildID() {
		return ForValidEmaildID;
	}
	public void setForValidEmaildID(String forValidEmaildID) {
		ForValidEmaildID = forValidEmaildID;
	}
	private String MenuType="FULLSCREEN";

	public String getMenuType() {
		return MenuType;
	}
	public void setMenuType(String menuType) {
		MenuType = menuType;
	}
	public String getCompanyLogo() {
		return CompanyLogo;
	}
	public void setCompanyLogo(String companyLogo) {
		CompanyLogo = companyLogo;
	}
	private String CompanyLogo;



	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}

	public String getSystemUse() {
		return SystemUse;
	}
	public void setSystemUse(String systemUse) {
		SystemUse = systemUse;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getPasswordDate() {
		return PasswordDate;
	}
	public void setPasswordDate(String passwordDate) {
		PasswordDate = passwordDate;
	}
	public String getPasswordExpirationDays() {
		return PasswordExpirationDays;
	}
	public void setPasswordExpirationDays(String passwordExpirationDays) {
		PasswordExpirationDays = passwordExpirationDays;
	}



	public String getInfoHelpType() {
		return InfoHelpType;
	}
	public void setInfoHelpType(String infoHelpType) {
		InfoHelpType = infoHelpType;
	}
	public String getAddressLine1() {
		return AddressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return AddressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return AddressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		AddressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return AddressLine4;
	}
	public void setAddressLine4(String addressLine4) {
		AddressLine4 = addressLine4;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getStateCode() {
		return StateCode;
	}
	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}


	public String getKeyPhrase() {
		return KeyPhrase;
	}
	public void setKeyPhrase(String keyPhrase) {
		KeyPhrase = keyPhrase;
	}
	public String getGeneralCode() {
		return GeneralCode;
	}
	public void setGeneralCode(String generalCode) {
		GeneralCode = generalCode;
	}
	public String getGeneralCodeID() {
		return GeneralCodeID;
	}
	public void setGeneralCodeID(String generalCodeID) {
		GeneralCodeID = generalCodeID;
	}
	public String getAttribute_Request() {
		return Attribute_Request;
	}
	public void setAttribute_Request(String attribute_Request) {
		Attribute_Request = attribute_Request;
	}
	public String getDrop_Down_Selection() {
		return Drop_Down_Selection;
	}
	public void setDrop_Down_Selection(String drop_Down_Selection) {
		Drop_Down_Selection = drop_Down_Selection;
	}
	public String getLanguage() {
		return Language;
	}
	public void setLanguage(String language) {
		Language = language;
	}
	public String getInfoHelp() {
		return InfoHelp;
	}
	public void setInfoHelp(String infoHelp) {
		InfoHelp = infoHelp;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getTenant_NAME20() {
		return Tenant_NAME20;
	}
	public void setTenant_NAME20(String tenant_NAME20) {
		Tenant_NAME20 = tenant_NAME20;
	}
	public String getTenant_NAME50() {
		return Tenant_NAME50;
	}
	public void setTenant_NAME50(String tenant_NAME50) {
		Tenant_NAME50 = tenant_NAME50;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getCompany_Name_20() {
		return Company_Name_20;
	}
	public void setCompany_Name_20(String company_Name_20) {
		Company_Name_20 = company_Name_20;
	}
	public String getCompany_Name_50() {
		return Company_Name_50;
	}
	public void setCompany_Name_50(String company_Name_50) {
		Company_Name_50 = company_Name_50;
	}

	public String getFulfillment_Center_Name_20() {
		return Fulfillment_Center_Name_20;
	}
	public void setFulfillment_Center_Name_20(String fulfillment_Center_Name_20) {
		Fulfillment_Center_Name_20 = fulfillment_Center_Name_20;
	}
	public String getFulfillment_Center_Name_50() {
		return Fulfillment_Center_Name_50;
	}
	public void setFulfillment_Center_Name_50(String fulfillment_Center_Name_50) {
		Fulfillment_Center_Name_50 = fulfillment_Center_Name_50;
	}
	public String getPurchaseOrdersRequireApproval() {
		return PurchaseOrdersRequireApproval;
	}
	public void setPurchaseOrdersRequireApproval(String purchaseOrdersRequireApproval) {
		PurchaseOrdersRequireApproval = purchaseOrdersRequireApproval;
	}
	public String getTeam_Member() {
		return Team_Member;
	}
	public void setTeam_Member(String team_Member) {
		Team_Member = team_Member;
	}
	public String getTenant_Language() {
		return Tenant_Language;
	}
	public void setTenant_Language(String tenant_Language) {
		Tenant_Language = tenant_Language;
	}
	public String getTeam_Member_Language() {
		return Team_Member_Language;
	}
	public void setTeam_Member_Language(String team_Member_Language) {
		Team_Member_Language = team_Member_Language;
	}
	public String getTeam_Member_Name() {
		return Team_Member_Name;
	}
	public void setTeam_Member_Name(String team_Member_Name) {
		Team_Member_Name = team_Member_Name;
	}
	public String getAuthority_Profile() {
		return Authority_Profile;
	}
	public void setAuthority_Profile(String authority_Profile) {
		Authority_Profile = authority_Profile;
	}
	public String getMenu_Profile_Tablet() {
		return Menu_Profile_Tablet;
	}
	public void setMenu_Profile_Tablet(String menu_Profile_Tablet) {
		Menu_Profile_Tablet = menu_Profile_Tablet;
	}
	public String getMenu_Profile_Station() {
		return Menu_Profile_Station;
	}
	public void setMenu_Profile_Station(String menu_Profile_Station) {
		Menu_Profile_Station = menu_Profile_Station;
	}
	public String getMenu_Profile_RF() {
		return Menu_Profile_RF;
	}
	public void setMenu_Profile_RF(String menu_Profile_RF) {
		Menu_Profile_RF = menu_Profile_RF;
	}
	public String getMenu_Profile_Glass() {
		return Menu_Profile_Glass;
	}
	public void setMenu_Profile_Glass(String menu_Profile_Glass) {
		Menu_Profile_Glass = menu_Profile_Glass;
	}
	public String getEquipment_Certification() {
		return Equipment_Certification;
	}
	public void setEquipment_Certification(String equipment_Certification) {
		Equipment_Certification = equipment_Certification;
	}
	public String getTheme_Mobile() {
		return Theme_Mobile;
	}
	public void setTheme_Mobile(String theme_Mobile) {
		Theme_Mobile = theme_Mobile;
	}
	public String getTheme_RF() {
		return Theme_RF;
	}
	public void setTheme_RF(String theme_RF) {
		Theme_RF = theme_RF;
	}
	public String getTheme_Full_Display() {
		return Theme_Full_Display;
	}
	public void setTheme_Full_Display(String theme_Full_Display) {
		Theme_Full_Display = theme_Full_Display;
	}
	public String getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getLocationFlag() {
		return LocationFlag;
	}
	public void setLocationFlag(String locationFlag) {
		LocationFlag = locationFlag;
	}
	public String getProduct() {
		return Product;
	}
	public void setProduct(String product) {
		Product = product;
	}
	public String getQuality() {
		return Quality;
	}
	public void setQuality(String quality) {
		Quality = quality;
	}
	public String getLot() {
		return Lot;
	}
	public void setLot(String lot) {
		Lot = lot;
	}
	public String getSerial() {
		return Serial;
	}
	public void setSerial(String serial) {
		Serial = serial;
	}
	public String getSerial01() {
		return Serial01;
	}
	public void setSerial01(String serial01) {
		Serial01 = serial01;
	}
	public String getSerial02() {
		return Serial02;
	}
	public void setSerial02(String serial02) {
		Serial02 = serial02;
	}
	public String getSerial03() {
		return Serial03;
	}
	public void setSerial03(String serial03) {
		Serial03 = serial03;
	}
	public String getSerial04() {
		return Serial04;
	}
	public void setSerial04(String serial04) {
		Serial04 = serial04;
	}
	public String getSerial05() {
		return Serial05;
	}
	public void setSerial05(String serial05) {
		Serial05 = serial05;
	}
	public String getSerial06() {
		return Serial06;
	}
	public void setSerial06(String serial06) {
		Serial06 = serial06;
	}
	public String getSerial07() {
		return Serial07;
	}
	public void setSerial07(String serial07) {
		Serial07 = serial07;
	}
	public String getSerial08() {
		return Serial08;
	}
	public void setSerial08(String serial08) {
		Serial08 = serial08;
	}
	public String getSerial09() {
		return Serial09;
	}
	public void setSerial09(String serial09) {
		Serial09 = serial09;
	}
	public String getSerial10() {
		return Serial10;
	}
	public void setSerial10(String serial10) {
		Serial10 = serial10;
	}
	public String getSerial11() {
		return Serial11;
	}
	public void setSerial11(String serial11) {
		Serial11 = serial11;
	}
	public String getSerial12() {
		return Serial12;
	}
	public void setSerial12(String serial12) {
		Serial12 = serial12;
	}
	public String getLPN() {
		return LPN;
	}
	public void setLPN(String lPN) {
		LPN = lPN;
	}
	public String getInventroryControl01() {
		return InventroryControl01;
	}
	public void setInventroryControl01(String inventroryControl01) {
		InventroryControl01 = inventroryControl01;
	}
	public String getInventroryControl02() {
		return InventroryControl02;
	}
	public void setInventroryControl02(String inventroryControl02) {
		InventroryControl02 = inventroryControl02;
	}
	public String getInventroryControl03() {
		return InventroryControl03;
	}
	public void setInventroryControl03(String inventroryControl03) {
		InventroryControl03 = inventroryControl03;
	}
	public String getInventroryControl04() {
		return InventroryControl04;
	}
	public void setInventroryControl04(String inventroryControl04) {
		InventroryControl04 = inventroryControl04;
	}
	public String getInventroryControl05() {
		return InventroryControl05;
	}
	public void setInventroryControl05(String inventroryControl05) {
		InventroryControl05 = inventroryControl05;
	}
	public String getInventroryControl06() {
		return InventroryControl06;
	}
	public void setInventroryControl06(String inventroryControl06) {
		InventroryControl06 = inventroryControl06;
	}
	public String getInventroryControl07() {
		return InventroryControl07;
	}
	public void setInventroryControl07(String inventroryControl07) {
		InventroryControl07 = inventroryControl07;
	}
	public String getInventroryControl08() {
		return InventroryControl08;
	}
	public void setInventroryControl08(String inventroryControl08) {
		InventroryControl08 = inventroryControl08;
	}
	public String getInventroryControl09() {
		return InventroryControl09;
	}
	public void setInventroryControl09(String inventroryControl09) {
		InventroryControl09 = inventroryControl09;
	}
	public String getInventroryControl10() {
		return InventroryControl10;
	}
	public void setInventroryControl10(String inventroryControl10) {
		InventroryControl10 = inventroryControl10;
	}
	public String getQuantity_being_adjusted() {
		return Quantity_being_adjusted;
	}
	public void setQuantity_being_adjusted(String quantity_being_adjusted) {
		Quantity_being_adjusted = quantity_being_adjusted;
	}
	public String getAdjustment_Quantity() {
		return Adjustment_Quantity;
	}
	public void setAdjustment_Quantity(String adjustment_Quantity) {
		Adjustment_Quantity = adjustment_Quantity;
	}
	public int getAdjusted_Quantity() {
		return Adjusted_Quantity;
	}
	public void setAdjusted_Quantity(int adjusted_Quantity) {
		Adjusted_Quantity = adjusted_Quantity;
	}
	public String getTask() {
		return Task;
	}
	public void setTask(String task) {
		Task = task;
	}
	public String getWorkType() {
		return WorkType;
	}
	public void setWorkType(String workType) {
		WorkType = workType;
	}
	public String getWorkControlNumber() {
		return WorkControlNumber;
	}
	public void setWorkControlNumber(String workControlNumber) {
		WorkControlNumber = workControlNumber;
	}
	public String getUnitofMeasureCode() {
		return UnitofMeasureCode;
	}
	public void setUnitofMeasureCode(String unitofMeasureCode) {
		UnitofMeasureCode = unitofMeasureCode;
	}
	public int getUnitofMeasureQty() {
		return UnitofMeasureQty;
	}
	public void setUnitofMeasureQty(int unitofMeasureQty) {
		UnitofMeasureQty = unitofMeasureQty;
	}
	public int getProductValue() {
		return ProductValue;
	}
	public void setProductValue(int productValue) {
		ProductValue = productValue;
	}
	public String getPurchaseOrderNumber() {
		return PurchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		PurchaseOrderNumber = purchaseOrderNumber;
	}
	public String getReference() {
		return Reference;
	}
	public void setReference(String reference) {
		Reference = reference;
	}
	public String getInventoryValue() {
		return InventoryValue;
	}
	public void setInventoryValue(String inventoryValue) {
		InventoryValue = inventoryValue;
	}
	public String getInventoryPickSequence() {
		return InventoryPickSequence;
	}
	public void setInventoryPickSequence(String inventoryPickSequence) {
		InventoryPickSequence = inventoryPickSequence;
	}
	public String getCasePackQty() {
		return CasePackQty;
	}
	public void setCasePackQty(String casePackQty) {
		CasePackQty = casePackQty;
	}
	public String getUserLoginDetails() {
		return UserLoginDetails;
	}
	public void setUserLoginDetails(String userLoginDetails) {
		UserLoginDetails = userLoginDetails;
	}


	private Map<String,Map<String,Map<String,String>>> menu=new HashMap<String,Map<String,Map<String,String>>>();
	
	public Map<String, Map<String, Map<String, String>>> getMenu() {
		return menu;
	}
	public void setMenu(Map<String, Map<String, Map<String, String>>> menu) {
		this.menu = menu;
	}
	
	
	private Map<String, Map<String, String>> SubMenuListHM=new HashMap<String, Map<String,String>>();

	public Map<String, Map<String, String>> getSubMenuListHM() {
		return SubMenuListHM;
	}

	public void setSubMenuListHM(Map<String, Map<String, String>> SubMenuListHM) {
		this.SubMenuListHM = SubMenuListHM;
	}
	/**
	 * @return the currentLanguage
	 */
	public String getCurrentLanguage() {
		return CurrentLanguage;
	}
	/**
	 * @param currentLanguage the currentLanguage to set
	 */
	public void setCurrentLanguage(String currentLanguage) {
		CurrentLanguage = currentLanguage;
	}
	/** 
	 *  
	 * @return
	 */
	public String getFooterText() {
		return FooterText;
	}
	/** 
	 * 
	 * @param footerText
	 */
	public void setFooterText(String footerText) {
		FooterText = footerText;
	}
	
	 private String XlaticsDashBoardPath;
	 private String CompIDValue;
	 private String APIKEYValue;
	 private String SECRETKEYValue;
	 private String UserIDValue;
	 private String ProjectNameValue;
	 private String DepartmentIDValue;
	 private String DashboardIdValue;
	  
	 public String getCompIDValue() {
	  return CompIDValue;
	 }
	 public void setCompIDValue(String compIDValue) {
	  CompIDValue = compIDValue;
	 }
	 public String getAPIKEYValue() {
	  return APIKEYValue;
	 }
	 public void setAPIKEYValue(String aPIKEYValue) {
	  APIKEYValue = aPIKEYValue;
	 }
	 public String getSECRETKEYValue() {
	  return SECRETKEYValue;
	 }
	 public void setSECRETKEYValue(String sECRETKEYValue) {
	  SECRETKEYValue = sECRETKEYValue;
	 }
	 public String getUserIDValue() {
	  return UserIDValue;
	 }
	 public void setUserIDValue(String userIDValue) {
	  UserIDValue = userIDValue;
	 }
	 public String getProjectNameValue() {
	  return ProjectNameValue;
	 }
	 public void setProjectNameValue(String projectNameValue) {
	  ProjectNameValue = projectNameValue;
	 }
	 public String getDepartmentIDValue() {
	  return DepartmentIDValue;
	 }
	 public void setDepartmentIDValue(String departmentIDValue) {
	  DepartmentIDValue = departmentIDValue;
	 }
	 public String getDashboardIdValue() {
	  return DashboardIdValue;
	 }
	 public void setDashboardIdValue(String dashboardIdValue) {
	  DashboardIdValue = dashboardIdValue;
	 }
	 public String getXlaticsDashBoardPath() {
	  return XlaticsDashBoardPath;
	 }
	 public void setXlaticsDashBoardPath(String xlaticsDashBoardPath) {
	  XlaticsDashBoardPath = xlaticsDashBoardPath;
	 }
}
