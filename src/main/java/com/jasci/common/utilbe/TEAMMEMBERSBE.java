/*

Date Developed  Nov 18 2014
Description   getter and setter for TEAMMEMBERSBE class
Created by: Rahul Kumar  
 */

package com.jasci.common.utilbe;

public class TEAMMEMBERSBE {




	int intKendoID;

	private String Sortname;
	private String Tenant;
	private String Teammember;
	private String FulfillmentcenterId;
	private String Department;
	private String Lastname;
	private String Firstname;
	private String Middlename;
	private String Addressline1;
	private String Addressline2;
	private String Addressline3;
	private String Addressline4;
	private String City;
	private String Statecode;
	private String Countrycode;
	private String Zipcode;
	private String Workphone;
	private String Workextension;
	private String Workcell;
	private String Workfax;
	private String Homephone;
	private String Cell;
	private String Emergencycontactname;
	private String Emergencycontacthomephone;
	private String Emergencycontactcell;
	private String Shiftcode;
	private String Systemuse;
	private String Authorityprofile;
	private String Taskprofile;
	private String MenuProfileGlass;
	private String MenuProfileTablet;
	private String MenuProfileMobile;
	private String MenuProfileStation;
	private String MenuProfileRf;
	private String Equipmentcertification;
	private String Language;
	private String Startdate;
	private String Lastdate;
	private String Setupdate;
	private String Setupby;
	private String Lastactivitydate;
	private String Lastactivityteammember;
	private String Currentstatus;
	private String Personalemailaddress;
	private String Workemailaddress;
	private String Emergencyemailaddress;

	
	
	public String getWorkemailaddress() {
		return Workemailaddress;
	}
	
	public void setWorkemailaddress(String workemailaddress) {
		Workemailaddress = workemailaddress;
	}
	
	public String getEmergencyemailaddress() {
		return Emergencyemailaddress;
	}
	public void setEmergencyemailaddress(String emergencyemailaddress) {
		Emergencyemailaddress = emergencyemailaddress;
	}
	public int getIntKendoID() {
		return intKendoID;
	}
	public void setIntKendoID(int intKendoID) {
		this.intKendoID = intKendoID;
	}


	
	public String getSortname() {
		return Sortname;
	}
	public void setSortname(String sortname) {
		Sortname = sortname;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getTeammember() {
		return Teammember;
	}
	public void setTeammember(String teammember) {
		Teammember = teammember;
	}
	public String getFulfillmentcenterId() {
		return FulfillmentcenterId;
	}
	public void setFulfillmentcenterId(String fulfillmentcenterId) {
		FulfillmentcenterId = fulfillmentcenterId;
	}
	public String getDepartment() {
		return Department;
	}
	public void setDepartment(String department) {
		Department = department;
	}
	public String getLastname() {
		return Lastname;
	}
	public void setLastname(String lastname) {
		Lastname = lastname;
	}
	public String getFirstname() {
		return Firstname;
	}
	public void setFirstname(String firstname) {
		Firstname = firstname;
	}
	public String getMiddlename() {
		return Middlename;
	}
	public void setMiddlename(String middlename) {
		Middlename = middlename;
	}
	public String getAddressline1() {
		return Addressline1;
	}
	public void setAddressline1(String addressline1) {
		Addressline1 = addressline1;
	}
	public String getAddressline2() {
		return Addressline2;
	}
	public void setAddressline2(String addressline2) {
		Addressline2 = addressline2;
	}
	public String getAddressline3() {
		return Addressline3;
	}
	public void setAddressline3(String addressline3) {
		Addressline3 = addressline3;
	}
	public String getAddressline4() {
		return Addressline4;
	}
	public void setAddressline4(String addressline4) {
		Addressline4 = addressline4;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getStatecode() {
		return Statecode;
	}
	public void setStatecode(String statecode) {
		Statecode = statecode;
	}
	public String getCountrycode() {
		return Countrycode;
	}
	public void setCountrycode(String countrycode) {
		Countrycode = countrycode;
	}
	public String getZipcode() {
		return Zipcode;
	}
	public void setZipcode(String zipcode) {
		Zipcode = zipcode;
	}
	public String getWorkphone() {
		return Workphone;
	}
	public void setWorkphone(String workphone) {
		Workphone = workphone;
	}
	public String getWorkextension() {
		return Workextension;
	}
	public void setWorkextension(String workextension) {
		Workextension = workextension;
	}
	public String getWorkcell() {
		return Workcell;
	}
	public void setWorkcell(String workcell) {
		Workcell = workcell;
	}
	public String getWorkfax() {
		return Workfax;
	}
	public void setWorkfax(String workfax) {
		Workfax = workfax;
	}
	public String getHomephone() {
		return Homephone;
	}
	public void setHomephone(String homephone) {
		Homephone = homephone;
	}
	public String getCell() {
		return Cell;
	}
	public void setCell(String cell) {
		Cell = cell;
	}
	public String getEmergencycontactname() {
		return Emergencycontactname;
	}
	public void setEmergencycontactname(String emergencycontactname) {
		Emergencycontactname = emergencycontactname;
	}
	public String getEmergencycontacthomephone() {
		return Emergencycontacthomephone;
	}
	public void setEmergencycontacthomephone(String emergencycontacthomephone) {
		Emergencycontacthomephone = emergencycontacthomephone;
	}
	public String getEmergencycontactcell() {
		return Emergencycontactcell;
	}
	public void setEmergencycontactcell(String emergencycontactcell) {
		Emergencycontactcell = emergencycontactcell;
	}
	public String getShiftcode() {
		return Shiftcode;
	}
	public void setShiftcode(String shiftcode) {
		Shiftcode = shiftcode;
	}
	public String getSystemuse() {
		return Systemuse;
	}
	public void setSystemuse(String systemuse) {
		Systemuse = systemuse;
	}
	public String getAuthorityprofile() {
		return Authorityprofile;
	}
	public void setAuthorityprofile(String authorityprofile) {
		Authorityprofile = authorityprofile;
	}
	public String getTaskprofile() {
		return Taskprofile;
	}
	public void setTaskprofile(String taskprofile) {
		Taskprofile = taskprofile;
	}
	public String getMenuProfileGlass() {
		return MenuProfileGlass;
	}
	public void setMenuProfileGlass(String menuProfileGlass) {
		MenuProfileGlass = menuProfileGlass;
	}
	public String getMenuProfileTablet() {
		return MenuProfileTablet;
	}
	public void setMenuProfileTablet(String menuProfileTablet) {
		MenuProfileTablet = menuProfileTablet;
	}
	public String getMenuProfileMobile() {
		return MenuProfileMobile;
	}
	public void setMenuProfileMobile(String menuProfileMobile) {
		MenuProfileMobile = menuProfileMobile;
	}
	public String getMenuProfileStation() {
		return MenuProfileStation;
	}
	public void setMenuProfileStation(String menuProfileStation) {
		MenuProfileStation = menuProfileStation;
	}
	public String getMenuProfileRf() {
		return MenuProfileRf;
	}
	public void setMenuProfileRf(String menuProfileRf) {
		MenuProfileRf = menuProfileRf;
	}
	public String getEquipmentcertification() {
		return Equipmentcertification;
	}
	public void setEquipmentcertification(String equipmentcertification) {
		Equipmentcertification = equipmentcertification;
	}
	public String getLanguage() {
		return Language;
	}
	public void setLanguage(String language) {
		Language = language;
	}
	public String getStartdate() {
		return Startdate;
	}
	public void setStartdate(String startdate) {
		Startdate = startdate;
	}
	public String getLastdate() {
		return Lastdate;
	}
	public void setLastdate(String lastdate) {
		Lastdate = lastdate;
	}
	public String getSetupdate() {
		return Setupdate;
	}
	public void setSetupdate(String setupdate) {
		Setupdate = setupdate;
	}
	public String getSetupby() {
		return Setupby;
	}
	public void setSetupby(String setupby) {
		Setupby = setupby;
	}
	public String getLastactivitydate() {
		return Lastactivitydate;
	}
	public void setLastactivitydate(String lastactivitydate) {
		Lastactivitydate = lastactivitydate;
	}
	public String getLastactivityteammember() {
		return Lastactivityteammember;
	}
	public void setLastactivityteammember(String lastactivityteammember) {
		Lastactivityteammember = lastactivityteammember;
	}
	public String getCurrentstatus() {
		return Currentstatus;
	}
	public void setCurrentstatus(String currentstatus) {
		Currentstatus = currentstatus;
	}
	public String getPersonalemailaddress() {
		return Personalemailaddress;
	}
	public void setPersonalemailaddress(String personalemailaddress) {
		Personalemailaddress = personalemailaddress;
	}




}
