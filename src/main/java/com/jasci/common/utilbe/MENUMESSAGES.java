/*

Date Developed  Oct 20 2014
Description   getter and setter for MENU_MESSAGES
 */
package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_MenuMessages)
//@NamedNativeQuery(name = "findByLastName", query = "call findByLastName(?, :vLastName)", callable = true, resultClass = MENUMESSAGES.class)

/*@javax.persistence.NamedNativeQuery(name = "GetMenuMessage", query = "{CALL UPDATEMENUMESSAGE(:Tenant,:Status) }", hints = {
@QueryHint(name = "org.hibernate.callable", value = "true")})
*/

public class MENUMESSAGES {
	
	@Id
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_Tenant)
	private String Tenant;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_Company)
	private String Company;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_Status)
	private String Status;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_TicketTapeMessage)
	private String TicketTapeMessage;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_Message)
	private String Message;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_Message_ID)
	private String MessageID;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_StartDate)
	private String StartDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuMessages_EndDate)
	private String EndDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastActivityDate)
	private String LastActivityDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastActivityTeamMember)
	private String LastActivityTeamMember;

	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getTicketTapeMessage() {
		return TicketTapeMessage;
	}

	public void setTicketTapeMessage(String ticketTapeMessage) {
		TicketTapeMessage = ticketTapeMessage;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}

	public String getMessageID() {
		return MessageID;
	}

	public void setMessage2(String messageID) {
		MessageID = messageID;
	}

	
	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}

	public String getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	

	
	
	
	
	

}
