/*

Date Developed  Oct 15 2014
Description   getter and setter for LOCATIONS table
 */

package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_LOCATIONS)
public class LOCATIONS {
	
	@Id
	@GeneratedValue
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_Tenant)
	private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_FulfillmentCenter)
	private String FulfillmentCenter;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_Company)
	private String Company;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_Area)
	private String Area;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_Location)
	private String Location;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_Description20)
	private String Description20;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_Description50)
	private String Description50;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_WorkGroupZone)
	private String WorkGroupZone;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_WorkZone)
	private String WorkZone;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationType)
	private String LocationType;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationProfile)
	private String LocationProfile;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_WizardID)
	private String WizardID;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_WizardControlNumber)
	private String WizardControlNumber;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationHeight)
	private String LocationHeight;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationWidth)
	private String LocationWidth;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationDepth)
	private String LocationDepth;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationWeightCapacity)
	private String LocationWeightCapacity;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationHeightCapacity)
	private String LocationHeightCapacity;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_NumberPalletsFit)
	private String NumberPalletsFit;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_NumberFloorLocations)
	private String NumberFloorLocations;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_AllocationAllowable)
	private String AllocationAllowable;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_MultipleItems )
	private String MultipleItems ;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_Slotting )
	private String Slotting ;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_StorageType)
	private String StorageType;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_CheckDigit)
	private String CheckDigit;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationLabelType)
	private String LocationLabelType;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LocationReprint)
	private String LocationReprint;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_CycleCountPoints)
	private int CycleCountPoints;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LastCycleCountDate)
	private String LastCycleCountDate;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_InventoryPickSequence)
	private String InventoryPickSequence;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_AlternateLocation )
	private String AlternateLocation ;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LastActivityDate)
	private String LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LastActivityTeamMember)
	private String LastActivityTeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATIONS_LastActivityTask)
	private String LastActivityTask;
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getWorkGroupZone() {
		return WorkGroupZone;
	}
	public void setWorkGroupZone(String workGroupZone) {
		WorkGroupZone = workGroupZone;
	}
	public String getWorkZone() {
		return WorkZone;
	}
	public void setWorkZone(String workZone) {
		WorkZone = workZone;
	}
	public String getLocationType() {
		return LocationType;
	}
	public void setLocationType(String locationType) {
		LocationType = locationType;
	}
	public String getLocationProfile() {
		return LocationProfile;
	}
	public void setLocationProfile(String locationProfile) {
		LocationProfile = locationProfile;
	}
	public String getWizardID() {
		return WizardID;
	}
	public void setWizardID(String wizardID) {
		WizardID = wizardID;
	}
	public String getWizardControlNumber() {
		return WizardControlNumber;
	}
	public void setWizardControlNumber(String wizardControlNumber) {
		WizardControlNumber = wizardControlNumber;
	}
	public String getLocationHeight() {
		return LocationHeight;
	}
	public void setLocationHeight(String locationHeight) {
		LocationHeight = locationHeight;
	}
	public String getLocationWidth() {
		return LocationWidth;
	}
	public void setLocationWidth(String locationWidth) {
		LocationWidth = locationWidth;
	}
	public String getLocationDepth() {
		return LocationDepth;
	}
	public void setLocationDepth(String locationDepth) {
		LocationDepth = locationDepth;
	}
	public String getLocationWeightCapacity() {
		return LocationWeightCapacity;
	}
	public void setLocationWeightCapacity(String locationWeightCapacity) {
		LocationWeightCapacity = locationWeightCapacity;
	}
	public String getLocationHeightCapacity() {
		return LocationHeightCapacity;
	}
	public void setLocationHeightCapacity(String locationHeightCapacity) {
		LocationHeightCapacity = locationHeightCapacity;
	}
	public String getNumberPalletsFit() {
		return NumberPalletsFit;
	}
	public void setNumberPalletsFit(String numberPalletsFit) {
		NumberPalletsFit = numberPalletsFit;
	}
	public String getNumberFloorLocations() {
		return NumberFloorLocations;
	}
	public void setNumberFloorLocations(String numberFloorLocations) {
		NumberFloorLocations = numberFloorLocations;
	}
	public String getAllocationAllowable() {
		return AllocationAllowable;
	}
	public void setAllocationAllowable(String allocationAllowable) {
		AllocationAllowable = allocationAllowable;
	}
	public String getMultipleItems() {
		return MultipleItems;
	}
	public void setMultipleItems(String multipleItems) {
		MultipleItems = multipleItems;
	}
	public String getSlotting() {
		return Slotting;
	}
	public void setSlotting(String slotting) {
		Slotting = slotting;
	}
	public String getStorageType() {
		return StorageType;
	}
	public void setStorageType(String storageType) {
		StorageType = storageType;
	}
	public String getCheckDigit() {
		return CheckDigit;
	}
	public void setCheckDigit(String checkDigit) {
		CheckDigit = checkDigit;
	}
	public String getLocationLabelType() {
		return LocationLabelType;
	}
	public void setLocationLabelType(String locationLabelType) {
		LocationLabelType = locationLabelType;
	}
	public String getLocationReprint() {
		return LocationReprint;
	}
	public void setLocationReprint(String locationReprint) {
		LocationReprint = locationReprint;
	}
	public int getCycleCountPoints() {
		return CycleCountPoints;
	}
	public void setCycleCountPoints(int cycleCountPoints) {
		CycleCountPoints = cycleCountPoints;
	}
	public String getLastCycleCountDate() {
		return LastCycleCountDate;
	}
	public void setLastCycleCountDate(String lastCycleCountDate) {
		LastCycleCountDate = lastCycleCountDate;
	}
	public String getInventoryPickSequence() {
		return InventoryPickSequence;
	}
	public void setInventoryPickSequence(String inventoryPickSequence) {
		InventoryPickSequence = inventoryPickSequence;
	}
	public String getAlternateLocation() {
		return AlternateLocation;
	}
	public void setAlternateLocation(String alternateLocation) {
		AlternateLocation = alternateLocation;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getLastActivityTask() {
		return LastActivityTask;
	}
	public void setLastActivityTask(String lastActivityTask) {
		LastActivityTask = lastActivityTask;
	}

	
	
}
