/**

Description : Location wizard Be in which getter and setter for LOCATION_WIZARD table
Created By : Pradeep Kumar  
Created Date: JAN 05 2015
 */
package com.jasci.common.utilbe;

public class LOCATIONWIZARDBE {

	private String WizardId;	
	private String Status;
	private String WizardControlNumber;
	private String Description20;
	private String Description50;
	private String WorkGroupZone;
	private String Area;
	private String WorkZone;
	private long NoOfLocations;
	private String FulfillmentDesc50;
	private String LocationType;
	private String LocationProfile;
	private String DateCreated;
	private String LastActivityDate;
	private String LastActivityTeamMember;
	private String TeamMemberId;
	private String CreatedByTeamMember;
	private String FulfillmentCenterId;
	private String RowNumberCharacters;
	private String RowCharacterSet;
	private String RowStarting ;
	private String RowNumberOf ;
	private String AisleNumberCharacters ;
	private String AisleCharacterSet ;
	private String AisleStarting ;
	private String AisleNumberOf ;
	private String BayNumberCharacters; 
	private String BayCharacterSet ;
	private String BayStarting ;
	private String BayNumberOf ;
	private String LevelNumberCharacters; 
	private String LevelCharacterSet ;
	private String LevelStarting ;
	private String LevelNumberOf ;
	private String SlotNumberCharacters; 
	private String SlotCharacterSet ;
	private String SlotStarting ;
	private String SlotNumberOf ;
	private String SeparatorRowAisle; 
	private String SeparatorAisleBay ;
	private String SeparatorBayLevel ;
	private String SeparatorLevelSlot ;
	private String SeparatorRowAislePrint; 
	private String SeparatorAislebayPrint ;
	private String SeparatorBaylevelPrint ;
	private String SeparatorLevelSlotPrint ;
	private String Notes;
	
	
	
	public String getNotes() {
		return Notes;
	}
	public void setNotes(String notes) {
		Notes = notes;
	}
	public String getTeamMemberId() {
		return TeamMemberId;
	}
	public void setTeamMemberId(String teamMemberId) {
		TeamMemberId = teamMemberId;
	}
	public String getWizardId() {
		return WizardId;
	}
	public void setWizardId(String wizardId) {
		WizardId = wizardId;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getWizardControlNumber() {
		return WizardControlNumber;
	}
	public void setWizardControlNumber(String wizardControlNumber) {
		WizardControlNumber = wizardControlNumber;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getWorkGroupZone() {
		return WorkGroupZone;
	}
	public void setWorkGroupZone(String workGroupZone) {
		WorkGroupZone = workGroupZone;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getWorkZone() {
		return WorkZone;
	}
	public void setWorkZone(String workZone) {
		WorkZone = workZone;
	}
	public long getNoOfLocations() {
		return NoOfLocations;
	}
	public void setNoOfLocations(long noOfLocations) {
		NoOfLocations = noOfLocations;
	}
	public String getFulfillmentDesc50() {
		return FulfillmentDesc50;
	}
	public void setFulfillmentDesc50(String fulfillmentDesc50) {
		FulfillmentDesc50 = fulfillmentDesc50;
	}
	public String getLocationType() {
		return LocationType;
	}
	public void setLocationType(String locationType) {
		LocationType = locationType;
	}
	public String getLocationProfile() {
		return LocationProfile;
	}
	public void setLocationProfile(String locationProfile) {
		LocationProfile = locationProfile;
	}
	public String getDateCreated() {
		return DateCreated;
	}
	public void setDateCreated(String dateCreated) {
		DateCreated = dateCreated;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getCreatedByTeamMember() {
		return CreatedByTeamMember;
	}
	public void setCreatedByTeamMember(String createdByTeamMember) {
		CreatedByTeamMember = createdByTeamMember;
	}
	public String getFulfillmentCenterId() {
		return FulfillmentCenterId;
	}
	public void setFulfillmentCenterId(String fulfillmentCenterId) {
		FulfillmentCenterId = fulfillmentCenterId;
	}
	public String getRowNumberCharacters() {
		return RowNumberCharacters;
	}
	public void setRowNumberCharacters(String rowNumberCharacters) {
		RowNumberCharacters = rowNumberCharacters;
	}
	public String getRowCharacterSet() {
		return RowCharacterSet;
	}
	public void setRowCharacterSet(String rowCharacterSet) {
		RowCharacterSet = rowCharacterSet;
	}
	public String getRowStarting() {
		return RowStarting;
	}
	public void setRowStarting(String rowStarting) {
		RowStarting = rowStarting;
	}
	public String getRowNumberOf() {
		return RowNumberOf;
	}
	public void setRowNumberOf(String rowNumberOf) {
		RowNumberOf = rowNumberOf;
	}
	public String getAisleNumberCharacters() {
		return AisleNumberCharacters;
	}
	public void setAisleNumberCharacters(String aisleNumberCharacters) {
		AisleNumberCharacters = aisleNumberCharacters;
	}
	public String getAisleCharacterSet() {
		return AisleCharacterSet;
	}
	public void setAisleCharacterSet(String aisleCharacterSet) {
		AisleCharacterSet = aisleCharacterSet;
	}
	public String getAisleStarting() {
		return AisleStarting;
	}
	public void setAisleStarting(String aisleStarting) {
		AisleStarting = aisleStarting;
	}
	public String getAisleNumberOf() {
		return AisleNumberOf;
	}
	public void setAisleNumberOf(String aisleNumberOf) {
		AisleNumberOf = aisleNumberOf;
	}
	public String getBayNumberCharacters() {
		return BayNumberCharacters;
	}
	public void setBayNumberCharacters(String bayNumberCharacters) {
		BayNumberCharacters = bayNumberCharacters;
	}
	public String getBayCharacterSet() {
		return BayCharacterSet;
	}
	public void setBayCharacterSet(String bayCharacterSet) {
		BayCharacterSet = bayCharacterSet;
	}
	public String getBayStarting() {
		return BayStarting;
	}
	public void setBayStarting(String bayStarting) {
		BayStarting = bayStarting;
	}
	public String getBayNumberOf() {
		return BayNumberOf;
	}
	public void setBayNumberOf(String bayNumberOf) {
		BayNumberOf = bayNumberOf;
	}
	public String getLevelNumberCharacters() {
		return LevelNumberCharacters;
	}
	public void setLevelNumberCharacters(String levelNumberCharacters) {
		LevelNumberCharacters = levelNumberCharacters;
	}
	public String getLevelCharacterSet() {
		return LevelCharacterSet;
	}
	public void setLevelCharacterSet(String levelCharacterSet) {
		LevelCharacterSet = levelCharacterSet;
	}
	public String getLevelStarting() {
		return LevelStarting;
	}
	public void setLevelStarting(String levelStarting) {
		LevelStarting = levelStarting;
	}
	public String getLevelNumberOf() {
		return LevelNumberOf;
	}
	public void setLevelNumberOf(String levelNumberOf) {
		LevelNumberOf = levelNumberOf;
	}
	public String getSlotNumberCharacters() {
		return SlotNumberCharacters;
	}
	public void setSlotNumberCharacters(String slotNumberCharacters) {
		SlotNumberCharacters = slotNumberCharacters;
	}
	public String getSlotCharacterSet() {
		return SlotCharacterSet;
	}
	public void setSlotCharacterSet(String slotCharacterSet) {
		SlotCharacterSet = slotCharacterSet;
	}
	public String getSlotStarting() {
		return SlotStarting;
	}
	public void setSlotStarting(String slotStarting) {
		SlotStarting = slotStarting;
	}
	public String getSlotNumberOf() {
		return SlotNumberOf;
	}
	public void setSlotNumberOf(String slotNumberOf) {
		SlotNumberOf = slotNumberOf;
	}
	public String getSeparatorRowAisle() {
		return SeparatorRowAisle;
	}
	public void setSeparatorRowAisle(String separatorRowAisle) {
		SeparatorRowAisle = separatorRowAisle;
	}
	public String getSeparatorAisleBay() {
		return SeparatorAisleBay;
	}
	public void setSeparatorAisleBay(String separatorAisleBay) {
		SeparatorAisleBay = separatorAisleBay;
	}
	public String getSeparatorBayLevel() {
		return SeparatorBayLevel;
	}
	public void setSeparatorBayLevel(String separatorBayLevel) {
		SeparatorBayLevel = separatorBayLevel;
	}
	public String getSeparatorLevelSlot() {
		return SeparatorLevelSlot;
	}
	public void setSeparatorLevelSlot(String separatorLevelSlot) {
		SeparatorLevelSlot = separatorLevelSlot;
	}
	public String getSeparatorRowAislePrint() {
		return SeparatorRowAislePrint;
	}
	public void setSeparatorRowAislePrint(String separatorRowAislePrint) {
		SeparatorRowAislePrint = separatorRowAislePrint;
	}
	public String getSeparatorAislebayPrint() {
		return SeparatorAislebayPrint;
	}
	public void setSeparatorAislebayPrint(String separatorAislebayPrint) {
		SeparatorAislebayPrint = separatorAislebayPrint;
	}
	public String getSeparatorBaylevelPrint() {
		return SeparatorBaylevelPrint;
	}
	public void setSeparatorBaylevelPrint(String separatorBaylevelPrint) {
		SeparatorBaylevelPrint = separatorBaylevelPrint;
	}
	public String getSeparatorLevelSlotPrint() {
		return SeparatorLevelSlotPrint;
	}
	public void setSeparatorLevelSlotPrint(String separatorLevelSlotPrint) {
		SeparatorLevelSlotPrint = separatorLevelSlotPrint;
	}
	
	
	
}
