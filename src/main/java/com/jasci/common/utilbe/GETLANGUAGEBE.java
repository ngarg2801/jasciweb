/*
 Description this class is bean class of GETLANGUAGE and there is getter and setter method
 Created By "Aakash Bishnoi"
 Created Date "24-9-14"
 */
package com.jasci.common.utilbe;

public class GETLANGUAGEBE {

	private String Langage;
	private String KeyPhrase;
	private String Translation; 
	private String LastActivityDate;
	private String LastActivityTeamMember;
	
	public String getLangage() {
		return Langage;
	}
	public void setLangage(String langage) {
		Langage = langage;
	}
	public String getKeyPhrase() {
		return KeyPhrase;
	}
	public void setKeyPhrase(String keyPhrase) {
		KeyPhrase = keyPhrase;
	}
	public String getTranslation() {
		return Translation;
	}
	public void setTranslation(String translation) {
		Translation = translation;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	

}
