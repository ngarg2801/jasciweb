/*

Date Developed  Oct 15 2014
Description getter and setter for LOCATION_PROFILES
 */


package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_LOCATION_PROFILES)
public class LOCATION_PROFILES {
	
	@Id
	@GeneratedValue
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Tenant)
	private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_FulfillmentCenter)
	private int FulfillmentCenter;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Company)
	private String Company;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_ProfileGroup)
	private String ProfileGroup;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Profile)
	private String Profile;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Description20)
	private String Description20;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Description50)
	private String Description50;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Type)
	private String Type;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationHeight)
	private String LocationHeight;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationWidth)
	private String LocationWidth;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationDepth)
	private String LocationDepth;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationWeightCapacity)
	private String LocationWeightCapacity;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationHeightCapacity)
	private String LocationHeightCapacity;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_NumberPalletsFit)
	private String NumberPalletsFit;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_NumberFloorLocations)
	private String NumberFloorLocations;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_AllocationAllowable)
	private String AllocationAllowable;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationCheck )
	private String LocationCheck ;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_FreeLocationWhenZero)
	private String FreeLocationWhenZero;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_FreePrimeDays)
	private String FreePrimeDays;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_MutipleProductsInLocation)
	private String MutipleProductsInLocation;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_StorageType)
	private String StorageType;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Slotting )
	private String Slotting ;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_CCActivityPoint)
	private String CCActivityPoint;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_CCHighValueAmount)
	private String CCHighValueAmount;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_CCHighValueFactor)
	private String CCHighValueFactor;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastUsedDate)
	private String LastUsedDate;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastUsedTeamMember)
	private String LastUsedTeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastActivityDate)
	private String LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastActivityTeamMember)
	private String LastActivityTeamMember;
	
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public int getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(int fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getProfileGroup() {
		return ProfileGroup;
	}
	public void setProfileGroup(String profileGroup) {
		ProfileGroup = profileGroup;
	}
	public String getProfile() {
		return Profile;
	}
	public void setProfile(String profile) {
		Profile = profile;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getLocationHeight() {
		return LocationHeight;
	}
	public void setLocationHeight(String locationHeight) {
		LocationHeight = locationHeight;
	}
	public String getLocationWidth() {
		return LocationWidth;
	}
	public void setLocationWidth(String locationWidth) {
		LocationWidth = locationWidth;
	}
	public String getLocationDepth() {
		return LocationDepth;
	}
	public void setLocationDepth(String locationDepth) {
		LocationDepth = locationDepth;
	}
	public String getLocationWeightCapacity() {
		return LocationWeightCapacity;
	}
	public void setLocationWeightCapacity(String locationWeightCapacity) {
		LocationWeightCapacity = locationWeightCapacity;
	}
	public String getLocationHeightCapacity() {
		return LocationHeightCapacity;
	}
	public void setLocationHeightCapacity(String locationHeightCapacity) {
		LocationHeightCapacity = locationHeightCapacity;
	}
	public String getNumberPalletsFit() {
		return NumberPalletsFit;
	}
	public void setNumberPalletsFit(String numberPalletsFit) {
		NumberPalletsFit = numberPalletsFit;
	}
	public String getNumberFloorLocations() {
		return NumberFloorLocations;
	}
	public void setNumberFloorLocations(String numberFloorLocations) {
		NumberFloorLocations = numberFloorLocations;
	}
	public String getAllocationAllowable() {
		return AllocationAllowable;
	}
	public void setAllocationAllowable(String allocationAllowable) {
		AllocationAllowable = allocationAllowable;
	}
	public String getLocationCheck() {
		return LocationCheck;
	}
	public void setLocationCheck(String locationCheck) {
		LocationCheck = locationCheck;
	}
	public String getFreeLocationWhenZero() {
		return FreeLocationWhenZero;
	}
	public void setFreeLocationWhenZero(String freeLocationWhenZero) {
		FreeLocationWhenZero = freeLocationWhenZero;
	}
	public String getFreePrimeDays() {
		return FreePrimeDays;
	}
	public void setFreePrimeDays(String freePrimeDays) {
		FreePrimeDays = freePrimeDays;
	}
	public String getMutipleProductsInLocation() {
		return MutipleProductsInLocation;
	}
	public void setMutipleProductsInLocation(String mutipleProductsInLocation) {
		MutipleProductsInLocation = mutipleProductsInLocation;
	}
	public String getStorageType() {
		return StorageType;
	}
	public void setStorageType(String storageType) {
		StorageType = storageType;
	}
	public String getSlotting() {
		return Slotting;
	}
	public void setSlotting(String slotting) {
		Slotting = slotting;
	}
	public String getCCActivityPoint() {
		return CCActivityPoint;
	}
	public void setCCActivityPoint(String cCActivityPoint) {
		CCActivityPoint = cCActivityPoint;
	}
	public String getCCHighValueAmount() {
		return CCHighValueAmount;
	}
	public void setCCHighValueAmount(String cCHighValueAmount) {
		CCHighValueAmount = cCHighValueAmount;
	}
	public String getCCHighValueFactor() {
		return CCHighValueFactor;
	}
	public void setCCHighValueFactor(String cCHighValueFactor) {
		CCHighValueFactor = cCHighValueFactor;
	}
	public String getLastUsedDate() {
		return LastUsedDate;
	}
	public void setLastUsedDate(String lastUsedDate) {
		LastUsedDate = lastUsedDate;
	}
	public String getLastUsedTeamMember() {
		return LastUsedTeamMember;
	}
	public void setLastUsedTeamMember(String lastUsedTeamMember) {
		LastUsedTeamMember = lastUsedTeamMember;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	
}
