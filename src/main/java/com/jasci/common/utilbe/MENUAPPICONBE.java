/** 
 *Description : getter and setter for MENU_APP_ICON table
 *@author Rahul Kumar
 *@Date Dec 14, 2014
 *
 */
package com.jasci.common.utilbe;

/**
 * @author Rahul Kumar
 *
 */
public class MENUAPPICONBE {
	
private String Application;
private String AppIcon;
private String DescriptionShort;
private String DescriptionLong;
private String AppIconAddress;
private String LastActivityDate;
private String LastActivityTeamMember;

public String getApplication() {
	return Application;
}
public void setApplication(String application) {
	Application = application;
}
public String getAppIcon() {
	return AppIcon;
}
public void setAppIcon(String appIcon) {
	AppIcon = appIcon;
}
public String getDescriptionShort() {
	return DescriptionShort;
}
public void setDescriptionShort(String descriptionShort) {
	DescriptionShort = descriptionShort;
}
public String getDescriptionLong() {
	return DescriptionLong;
}
public void setDescriptionLong(String descriptionLong) {
	DescriptionLong = descriptionLong;
}
public String getAppIconAddress() {
	return AppIconAddress;
}
public void setAppIconAddress(String appIconAddress) {
	AppIconAddress = appIconAddress;
}
public String getLastActivityDate() {
	return LastActivityDate;
}
public void setLastActivityDate(String lastActivityDate) {
	LastActivityDate = lastActivityDate;
}
public String getLastActivityTeamMember() {
	return LastActivityTeamMember;
}
public void setLastActivityTeamMember(String lastActivityTeamMember) {
	LastActivityTeamMember = lastActivityTeamMember;
}





}
