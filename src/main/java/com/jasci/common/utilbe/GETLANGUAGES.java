/*

Date Developed  Oct 16 2014
Description   Infohelps
 */
package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_Languages)
public class GETLANGUAGES 
{
	@Id
	@Column(name=GLOBALCONSTANT.DataBase_Languages_Language)
	private String Language;
	
	@Column(name=GLOBALCONSTANT.DataBase_Languages_KeyPhrase)
	private String KeyPhrase;
	
	@Column(name=GLOBALCONSTANT.DataBase_Languages_Translation)
	private String Translation;
	
	@Column(name=GLOBALCONSTANT.DataBase_Languages_LastActivityDate)
	private String LastActivityDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_Languages_LastActivityTeamMember)
	private String LastActivityTeamMember;

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getKeyPhrase() {
		return KeyPhrase;
	}

	public void setKeyPhrase(String keyPhrase) {
		KeyPhrase = keyPhrase;
	}

	public String getTranslation() {
		return Translation;
	}

	public void setTranslation(String translation) {
		Translation = translation;
	}

	public String getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

		
	
}
