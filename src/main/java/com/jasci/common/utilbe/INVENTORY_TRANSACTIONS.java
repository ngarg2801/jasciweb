/*

Date Developed  Oct 15 2014
Description   getter and setter for INVENTORY_LEVELSPK 
 */

package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_INVENTORY_TRANSACTIONS)
public class INVENTORY_TRANSACTIONS {
	
	@EmbeddedId
	private INVENTORY_TRANSACTIONSPK Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_QuantityTransaction )
	private String QuantityTransaction;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_QuantityBefore )
	private int QuantityBefore;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_QuantityAfter )
	private int QuantityAfter;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_CasePackQty)
	private String CasePackQty;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_DateControl)
	private String DateControl;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventoryPickSequence)
	private String InventoryPickSequence;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_InventoryValue)
	private String InventoryValue;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_WorkType)
	private String WorkType;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_WorkControlNumber)
	private String WorkControlNumber;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_Reference )
	private String Reference;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_PurchaseOrderNumber)
	private String PurchaseOrderNumber;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_LastActivityDate)
	private String LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_LastActivityTeamMember)
	private String LastActivityTeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_TRANSACTIONS_LastActivityTask)
	private String LastActivityTask;
	
	public INVENTORY_TRANSACTIONSPK getId() {
		return Id;
	}
	public void setId(INVENTORY_TRANSACTIONSPK id) {
		Id = id;
	}
	public String getQuantityTransaction() {
		return QuantityTransaction;
	}
	public void setQuantityTransaction(String quantityTransaction) {
		QuantityTransaction = quantityTransaction;
	}
	public int getQuantityBefore() {
		return QuantityBefore;
	}
	public void setQuantityBefore(int quantityBefore) {
		QuantityBefore = quantityBefore;
	}
	public int getQuantityAfter() {
		return QuantityAfter;
	}
	public void setQuantityAfter(int quantityAfter) {
		QuantityAfter = quantityAfter;
	}
	public String getCasePackQty() {
		return CasePackQty;
	}
	public void setCasePackQty(String casePackQty) {
		CasePackQty = casePackQty;
	}
	public String getDateControl() {
		return DateControl;
	}
	public void setDateControl(String dateControl) {
		DateControl = dateControl;
	}
	public String getInventoryPickSequence() {
		return InventoryPickSequence;
	}
	public void setInventoryPickSequence(String inventoryPickSequence) {
		InventoryPickSequence = inventoryPickSequence;
	}
	public String getInventoryValue() {
		return InventoryValue;
	}
	public void setInventoryValue(String inventoryValue) {
		InventoryValue = inventoryValue;
	}
	public String getWorkType() {
		return WorkType;
	}
	public void setWorkType(String workType) {
		WorkType = workType;
	}
	public String getWorkControlNumber() {
		return WorkControlNumber;
	}
	public void setWorkControlNumber(String workControlNumber) {
		WorkControlNumber = workControlNumber;
	}
	public String getReference() {
		return Reference;
	}
	public void setReference(String reference) {
		Reference = reference;
	}
	public String getPurchaseOrderNumber() {
		return PurchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		PurchaseOrderNumber = purchaseOrderNumber;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getLastActivityTask() {
		return LastActivityTask;
	}
	public void setLastActivityTask(String lastActivityTask) {
		LastActivityTask = lastActivityTask;
	}

	

}
