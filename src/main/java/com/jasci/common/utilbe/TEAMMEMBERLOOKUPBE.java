/*

Date Developed  Sep 18 2014
Description   getter and setter for TEAMMEMBERLOOKUPBE class
 */

package com.jasci.common.utilbe;

public class TEAMMEMBERLOOKUPBE {
	
	private String Tenant;
	private String TeamMember;
	private long FulfillmentCenter;
	private String Department;
	private String LastName;
	private String FirstName;
	private String MiddleName ;
	private String AddressLine1;
	private String AddressLine2;
	private String AddressLine3;
	private String AddressLine4;
	private String City;
	private String StateCode;
	private String CountryCode;
	private String ZipCode;
	private String WorkPhone;
	private String WorkExtension;
	private String WorkCell;
	private String WorkFax;
	private String HomePhone;
	private String Cell;
	private String EmergencyContactName;
	private String EmergencyContactHomePhone;
	private String EmergencyContactCell;
	private String ShiftCode;
	private String SystemUse;
	private String AuthorityProfile;
	private String TaskProfile;
	private String Menu_Profile_Glass;
	private String Menu_Profile_Tablet;
	private String Menu_Profile_Mobile;
	private String Menu_Profile_Station;
	private String Menu_Profile_RF;
	private String EquipmentCertification;
	private String Langage;
	private String StartDate;
	private String LastDate;
	private long SetupDate;
	private String SetupBy;
	private String LastActivityDate;
	private String LastActivityTeamMember;
	private String CurrentStatus;
	
	public String getDepartment() {
		return Department;
	}
	public void setDepartment(String department) {
		Department = department;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}
	public String getAddressLine1() {
		return AddressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return AddressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return AddressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		AddressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return AddressLine4;
	}
	public void setAddressLine4(String addressLine4) {
		AddressLine4 = addressLine4;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getStateCode() {
		return StateCode;
	}
	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getWorkPhone() {
		return WorkPhone;
	}
	public void setWorkPhone(String workPhone) {
		WorkPhone = workPhone;
	}
	public String getWorkExtension() {
		return WorkExtension;
	}
	public void setWorkExtension(String workExtension) {
		WorkExtension = workExtension;
	}
	public String getWorkCell() {
		return WorkCell;
	}
	public void setWorkCell(String workCell) {
		WorkCell = workCell;
	}
	public String getWorkFax() {
		return WorkFax;
	}
	public void setWorkFax(String workFax) {
		WorkFax = workFax;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getCell() {
		return Cell;
	}
	public void setCell(String cell) {
		Cell = cell;
	}
	public String getEmergencyContactName() {
		return EmergencyContactName;
	}
	public void setEmergencyContactName(String emergencyContactName) {
		EmergencyContactName = emergencyContactName;
	}
	public String getEmergencyContactHomePhone() {
		return EmergencyContactHomePhone;
	}
	public void setEmergencyContactHomePhone(String emergencyContactHomePhone) {
		EmergencyContactHomePhone = emergencyContactHomePhone;
	}
	public String getEmergencyContactCell() {
		return EmergencyContactCell;
	}
	public void setEmergencyContactCell(String emergencyContactCell) {
		EmergencyContactCell = emergencyContactCell;
	}
	public String getShiftCode() {
		return ShiftCode;
	}
	public void setShiftCode(String shiftCode) {
		ShiftCode = shiftCode;
	}
	public String getSystemUse() {
		return SystemUse;
	}
	public void setSystemUse(String systemUse) {
		SystemUse = systemUse;
	}
	public String getAuthorityProfile() {
		return AuthorityProfile;
	}
	public void setAuthorityProfile(String authorityProfile) {
		AuthorityProfile = authorityProfile;
	}
	public String getTaskProfile() {
		return TaskProfile;
	}
	public void setTaskProfile(String taskProfile) {
		TaskProfile = taskProfile;
	}
	public String getMenu_Profile_Glass() {
		return Menu_Profile_Glass;
	}
	public void setMenu_Profile_Glass(String menu_Profile_Glass) {
		Menu_Profile_Glass = menu_Profile_Glass;
	}
	public String getMenu_Profile_Tablet() {
		return Menu_Profile_Tablet;
	}
	public void setMenu_Profile_Tablet(String menu_Profile_Tablet) {
		Menu_Profile_Tablet = menu_Profile_Tablet;
	}
	public String getMenu_Profile_Mobile() {
		return Menu_Profile_Mobile;
	}
	public void setMenu_Profile_Mobile(String menu_Profile_Mobile) {
		Menu_Profile_Mobile = menu_Profile_Mobile;
	}
	public String getMenu_Profile_Station() {
		return Menu_Profile_Station;
	}
	public void setMenu_Profile_Station(String menu_Profile_Station) {
		Menu_Profile_Station = menu_Profile_Station;
	}
	public String getMenu_Profile_RF() {
		return Menu_Profile_RF;
	}
	public void setMenu_Profile_RF(String menu_Profile_RF) {
		Menu_Profile_RF = menu_Profile_RF;
	}
	public String getEquipmentCertification() {
		return EquipmentCertification;
	}
	public void setEquipmentCertification(String equipmentCertification) {
		EquipmentCertification = equipmentCertification;
	}
	public String getLangage() {
		return Langage;
	}
	public void setLangage(String langage) {
		Langage = langage;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getLastDate() {
		return LastDate;
	}
	public void setLastDate(String lastDate) {
		LastDate = lastDate;
	}
	public long getSetupDate() {
		return SetupDate;
	}
	public void setSetupDate(long setupDate) {
		SetupDate = setupDate;
	}
	public String getSetupBy() {
		return SetupBy;
	}
	public void setSetupBy(String setupBy) {
		SetupBy = setupBy;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getCurrentStatus() {
		return CurrentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		CurrentStatus = currentStatus;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getTeamMember() {
		return TeamMember;
	}
	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}
	public long getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(long fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}

}
