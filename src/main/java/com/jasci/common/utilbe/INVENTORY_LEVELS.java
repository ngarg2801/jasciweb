/*

Date Developed  Oct 15 2014
Description   getter and setter for INVENTORY_LEVELSPK 
 */

package com.jasci.common.utilbe;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_INVENTORY_LEVELS)
public class INVENTORY_LEVELS {
	
	@EmbeddedId
	public INVENTORY_LEVELSPK Id;

	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Quantity )
	private int Quantity;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_UnitofMeasureCode)
	private String UnitofMeasureCode;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_UnitofMeasureQty)
	private int UnitofMeasureQty;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_DateControl)
	private int DateControl;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_ProductValue)
	private int ProductValue;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_DateReceived)
	private String DateReceived;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_LastActivityDate)
	private String LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_LastActivityEmployee)
	private String LastActivityEmployee;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_LastActivityTask)
	private String LastActivityTask;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_HoldCode)
	private String HoldCode;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_SystemCode)
	private String SystemCode;


	public INVENTORY_LEVELSPK getId() {
		return Id;
	}
	public void setId(INVENTORY_LEVELSPK id) {
		Id = id;
	}
	public int getQuantity() {
		return Quantity;
	}
	public void setQuantity(int quantity) {
		Quantity = quantity;
	}
	public String getUnitofMeasureCode() {
		return UnitofMeasureCode;
	}
	public void setUnitofMeasureCode(String unitofMeasureCode) {
		UnitofMeasureCode = unitofMeasureCode;
	}
	public int getUnitofMeasureQty() {
		return UnitofMeasureQty;
	}
	public void setUnitofMeasureQty(int unitofMeasureQty) {
		UnitofMeasureQty = unitofMeasureQty;
	}
	public int getDateControl() {
		return DateControl;
	}
	public void setDateControl(int dateControl) {
		DateControl = dateControl;
	}
	public int getProductValue() {
		return ProductValue;
	}
	public void setProductValue(int productValue) {
		ProductValue = productValue;
	}
	public String getDateReceived() {
		return DateReceived;
	}
	public void setDateReceived(String dateReceived) {
		DateReceived = dateReceived;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityEmployee() {
		return LastActivityEmployee;
	}
	public void setLastActivityEmployee(String lastActivityEmployee) {
		LastActivityEmployee = lastActivityEmployee;
	}
	public String getLastActivityTask() {
		return LastActivityTask;
	}
	public void setLastActivityTask(String lastActivityTask) {
		LastActivityTask = lastActivityTask;
	}
	public String getHoldCode() {
		return HoldCode;
	}
	public void setHoldCode(String holdCode) {
		HoldCode = holdCode;
	}
	public String getSystemCode() {
		return SystemCode;
	}
	public void setSystemCode(String systemCode) {
		SystemCode = systemCode;
	}

	
}
