/**

Date Developed  Sep 18 2014
Description   lookup Fulfillment Center Attributes in table FULFILLMENT_CENTERS
*/

package com.jasci.common.utildal;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.GETFULFILLMENTCENTERS;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class GETFULFILLMENTCENTERSDAOIMPL implements IGETFULFILLMENTCENTERSDAO
{

	/**
	 *Description That function design to get database connection
	 *Input parameter ObjCommonSession
	 *Return Type "List<FULFILLMENTCENTERS>"
	 *Created By "Diksha Gupta"
	 *Created Date "16-10-14" 
	 */
	@Autowired
	SessionFactory sessionFactory;
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GETFULFILLMENTCENTERS> GetDataFromFulfillmentCenter(COMMONSESSIONBE ObjCommonSession) throws JASCIEXCEPTION
	{
		try{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.GETFulfillmentCenter_Query).setParameter(GLOBALCONSTANT.INT_ZERO,ObjCommonSession.getTenant().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,ObjCommonSession.getFulfillmentCenter()).list();
		}
		catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}
	}
}
