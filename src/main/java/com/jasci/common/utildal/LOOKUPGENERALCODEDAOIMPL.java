/**

Date Developed  Sep 18 2014
Description   lookup General Codes in Table GENERAL_CODES
*/

package com.jasci.common.utildal;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.GENERAL_CODES;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class LOOKUPGENERALCODEDAOIMPL {

	/**
	 *Description That function design for get LookupGeneralCode and return LOOKUPGENERALCODEBE" 
	 *Input parameter "CommonSessionObj"
	 *Return Type "LOOKUPGENERALCODEBE"
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 */


	@Autowired
	private SessionFactory sessionFactory ;


	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	
	/** 
	 * 
	 * @param tenant
	 * @param company
	 * @param generalCodeID
	 * @param generalCode
	 * @return
	 */
	public List<GENERAL_CODES> GetLookupGeneralCode(String tenant, String company, String generalCodeID, String generalCode) throws JASCIEXCEPTION
	{

	
		try{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.GENERAL_CODES)
					.setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, generalCodeID.toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_THREE, generalCode.toUpperCase())
					.list();
			
		}
		catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}
	}


	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	/**  
	 * 
	 * @param tenant
	 * @param company
	 * @param generalCodeID
	 * @param generalCode
	 * @return
	 */
	public List<Object> GetLookupGeneralCodeDropDown(String tenant, String company, String generalCodeID, String generalCode) throws JASCIEXCEPTION
	{

		
		try{
			return sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.GENERAL_CODES_DROPDOWN)
					.setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, generalCodeID.toUpperCase()).list();
		}
		catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}
	}

}
