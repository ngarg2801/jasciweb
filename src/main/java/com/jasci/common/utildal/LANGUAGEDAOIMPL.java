/**

Date Developed  Sep 18 2014
Description   Get Language Translation
*/
package com.jasci.common.utildal;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.GETLANGUAGES;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class LANGUAGEDAOIMPL implements ILANGUAGEDAO
{
	@Autowired
	private SessionFactory sessionFactory ;

	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> GetLanguageByKeyPhases(String strInKeyPhases,String StrLanguageCode)
	{
		
		return sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.getlanguagekeyphrasequery+strInKeyPhases.toUpperCase()+GLOBALCONSTANT.RightBraces).setParameter(GLOBALCONSTANT.INT_ZERO, StrLanguageCode.toUpperCase()).list();
		
	}

	
	/**
	 *Description That function design to get database connection
	 *Input parameter ObjCommonSessionBe
	 *Return Type "List<LANGUAGES>"
	 *Created By "Diksha Gupta"
	 *Created Date "17-10-14" 
	 * @throws JASCIEXCEPTION 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GETLANGUAGES> GetLanguage(String keyPhrase, String language) throws JASCIEXCEPTION
	{
		try{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.GetLanguagesKeyPhrase_Query).setParameter(GLOBALCONSTANT.INT_ZERO, keyPhrase.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, language.toUpperCase()).list();
		}
		catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}

	}
	

}
