/**

Date Developed  Sep 18 2014
Description   lookup Team Member Information in Table TEAM_MEMBERS
*/

package com.jasci.common.utildal;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.TEAM_MEMBERS;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class TEAMMEMBERLOOKUPDAOIMPL {

	@Autowired
	private SessionFactory sessionFactory ;	
	
/**
 *Description That function design for get teammember and return related data" 
 *Input parameter "Team Member"
 *Return Type "ArrayList"
 *Created By "Aakash Bishnoi"
 *Created Date "23-9-14"
 * @throws JASCIEXCEPTION 
 */
 
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAM_MEMBERS> GetTeammemberLookup(String TeamMember, String Tenant) throws JASCIEXCEPTION {		
		
	
		try{
		return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.GetTEAM_MEMBERS).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase()).list();
		}
		catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}
	    }
}
