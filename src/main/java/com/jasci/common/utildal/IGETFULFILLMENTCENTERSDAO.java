/**

Date Developed  Oct 15 2014
Description  Provide database connection
*/
package com.jasci.common.utildal;

import java.util.List;

import com.jasci.common.utilbe.GETFULFILLMENTCENTERS;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface IGETFULFILLMENTCENTERSDAO 
{
	public List<GETFULFILLMENTCENTERS> GetDataFromFulfillmentCenter(COMMONSESSIONBE ObjCommonSession) throws JASCIEXCEPTION;
}
