/**

Date Developed  Sep 18 2014
Description   Get Info & Help and display
*/

package com.jasci.common.utildal;


import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.GETINFOHELPS;
import com.jasci.exception.JASCIEXCEPTION;


@Repository
public class GETINFOHELPDAL
{

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	/** 
	 * 
	 * @param infoHelp
	 * @param language
	 * @param infoHelpType
	 * @return
	 */
	public List<GETINFOHELPS> GetInfoHelps(String infoHelp, String language, String infoHelpType) throws JASCIEXCEPTION {
		
		try{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.CommonInfoHelps_Query).setParameter(GLOBALCONSTANT.INT_ZERO, infoHelp.toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, language.toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, infoHelpType.toUpperCase()).list();
		}
		catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}
	}


}
