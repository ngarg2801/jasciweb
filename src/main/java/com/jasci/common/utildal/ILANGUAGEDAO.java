/**

Date Developed  Sep 18 2014
Description   Get Language Translation
*/
package com.jasci.common.utildal;

import java.util.List;

import com.jasci.common.utilbe.GETLANGUAGES;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILANGUAGEDAO {
	
	public  List<GETLANGUAGES>  GetLanguage(String keyPhrase, String language) throws JASCIEXCEPTION;

}
