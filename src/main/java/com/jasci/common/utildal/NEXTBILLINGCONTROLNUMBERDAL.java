/**

Date Developed  Jan 07 2015
Developed By : Manish Chaurasia
Description   Get the next Billing Control Number and assignment for billing purposes 
 */

package com.jasci.common.utildal;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;

@Repository
public class NEXTBILLINGCONTROLNUMBERDAL {

	@Autowired
	SessionFactory sessionFactory;
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public Long getNextBillingControlNumber()
	
	{
		
		List<Object> NextBillingList = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.Query_GetMaxBillingControlNumber).list();
		if(NextBillingList.isEmpty())
		{
			return 0l;
		}
		return Long.parseLong(NextBillingList.get(GLOBALCONSTANT.INT_ZERO).toString());
		
	}
	
}
