/**

Date Developed  Sep 18 2014
Description   Adjust Quantity in a location 
*/

package com.jasci.common.utildal;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.INVENTORY_LEVELS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.INVENTORY_TRANSACTIONS;
import com.jasci.common.utilbe.LOCATIONS;
import com.jasci.common.utilbe.LOCATION_PROFILES;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class UPDATEINVENTORYDAOIMPL {

	/**
	 *Description That function design for Update And insert Inventory InfoHelp" 
	 *Input parameter "CommonSessionObj"
	 *Return Type ""
	 *Created By "ANOOP SINGH"
	 *Created Date "23-9-14" 
	 */

	@Autowired
	private SessionFactory sessionFactory ;	


	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public void UpdateProduct(INVENTORY_LEVELS Obj_InventoryInv, INVENTORY_TRANSACTIONS Obj_Inventorytxn) throws JASCIEXCEPTION {

		/**Set date format and create data instance*/

		DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.DateFormat);
		Date date = new Date();
		/** declare session and transaction object*/ 
		Session Obj_sess = null;
		

		try{

			/** instanciate session and transaction object*/
			Obj_sess = sessionFactory.getCurrentSession();

			/**Adjust Quantity on the basis of Adjusted Quantity to Quantity in Inventory_Levels*/
			
			Obj_InventoryInv.setQuantity(Obj_Inventorytxn.getQuantityAfter());/**Question*/ 

			if(Obj_InventoryInv.getQuantity() == GLOBALCONSTANT.Zero){
				List<LOCATION_PROFILES> Location_Profileslist=new ArrayList<LOCATION_PROFILES>();
				Location_Profileslist = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.UpdateProductLOCATION_PROFILES)
						.setParameter(GLOBALCONSTANT.IntZero, Obj_InventoryInv.getId().getTenant().toUpperCase())
						.setParameter(GLOBALCONSTANT.IntOne, Obj_InventoryInv.getId().getFulfillmentCenter())
						.setParameter(GLOBALCONSTANT.INT_TWO, Obj_InventoryInv.getId().getCompany().toUpperCase())
						.list();
				Iterator<LOCATION_PROFILES> Obj_LocationProfileIterator =Location_Profileslist.iterator();
				LOCATION_PROFILES Obj_LocationProfile = Obj_LocationProfileIterator.next(); 
				String FreeLocationWhenZero = GLOBALCONSTANT.BlankString;
				FreeLocationWhenZero = Obj_LocationProfile.getFreeLocationWhenZero();
				String LocationCheck = GLOBALCONSTANT.BlankString;
				LocationCheck = Obj_LocationProfile.getLocationCheck();


				try{
					if(FreeLocationWhenZero.equalsIgnoreCase(GLOBALCONSTANT.Yes)){


						Obj_sess.delete(Obj_InventoryInv);

					}

					if(LocationCheck.equalsIgnoreCase(GLOBALCONSTANT.Yes)){
						/**Execute Common Class LOCATIONCHECK*/
					}

				}catch(Exception e){

				}
			}else{
				/** Update LastActivityDate with current Date in Inventory_Levels*/

		//		Obj_InventoryInv.setLastActivityDate(dateFormat.format(date));
				Obj_InventoryInv.setLastActivityDate(date);

				/**Update data into INVENTORY_LEVELS table*/

				Obj_sess.saveOrUpdate(Obj_InventoryInv);
				

			}


			/** Update LastActivityDate with current Date in Inventory_Transaction*/

			Obj_Inventorytxn.setLastActivityDate(dateFormat.format(date));

			/**Update data into INVENTORY_LEVELS table*/
			
			Obj_sess.saveOrUpdate(Obj_Inventorytxn);
			

			/**Update Cycle Points in Table LOCATIONS
			 * Get Location_Profile from Locations table on the basis of Inventry_Levels.Location
			 */

			List<LOCATIONS> LOCATIONSlist=new ArrayList<LOCATIONS>();
			LOCATIONSlist = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LOCATIONS)
					.setParameter(GLOBALCONSTANT.INT_ZERO, Obj_InventoryInv.getId().getLocation().toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, Obj_InventoryInv.getId().getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, Obj_InventoryInv.getId().getFulfillmentCenter().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_THREE, Obj_InventoryInv.getId().getCompany().toUpperCase())
					.list();
			Iterator<LOCATIONS> ObjLocationIterator = LOCATIONSlist.iterator();
			LOCATIONS ObjLocation = ObjLocationIterator.next(); 
			String LocationProfile = ObjLocation.getLocationProfile();

			/**Get Profile from Location_Profiles table on the basis of Locations.Location_Profile*/

			List<LOCATION_PROFILES> LOCATION_PROFILES_list=new ArrayList<LOCATION_PROFILES>();
			LOCATION_PROFILES_list = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LOCATION_PROFILES)
					.setParameter(GLOBALCONSTANT.INT_ZERO, LocationProfile.toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, Obj_InventoryInv.getId().getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, Obj_InventoryInv.getId().getFulfillmentCenter().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_THREE, Obj_InventoryInv.getId().getCompany().toUpperCase())
					.list();
			Iterator<LOCATION_PROFILES> ObjLocationProfileIterator =LOCATION_PROFILES_list.iterator();
			LOCATION_PROFILES ObjLocationProfile = ObjLocationProfileIterator.next(); 
			double CCActivityPoint = Integer.parseInt(ObjLocationProfile.getCCActivityPoint());
			double CCHighValueAmout = Integer.parseInt(ObjLocationProfile.getCCHighValueAmount());
			double CCHighValueFactor = Integer.parseInt(ObjLocationProfile.getCCHighValueFactor());
			double CYCLECOUNTPOINTS = GLOBALCONSTANT.INT_ZERO;
			if(Obj_InventoryInv.getProductValue() >= CCHighValueAmout){
				CYCLECOUNTPOINTS = CCActivityPoint * CCHighValueFactor;
			}

			/** Set value and default value on the basis of Inventory_Levels and Inventory_Transaction in Locations table*/

			ObjLocation.setCycleCountPoints((int) CYCLECOUNTPOINTS);
			ObjLocation.setLastActivityTeamMember(Obj_InventoryInv.getLastActivityEmployee());

			/**Update data into Locations table*/

			Obj_sess.saveOrUpdate(ObjLocation);

		}catch(Exception obj_e)
		{

			throw new JASCIEXCEPTION(obj_e.getMessage());
		}


	}


	/**
	 *Description That function design for Update And insert Inventory InfoHelp" 
	 *Input parameter "CommonSessionObj"
	 *Return Type ""
	 *Created By "ANOOP SINGH"
	 *Updated By "Shailendra Rajput"
	 *Created Date "09-23-14" 
	 *Updated Date "11-09-14"
	 * @throws JASCIEXCEPTION 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public void NewProduct(INVENTORY_LEVELS Obj_InventoryInv, INVENTORY_TRANSACTIONS Obj_Inventorytxn) throws JASCIEXCEPTION
	{
		/**Set date format and create data instance*/
		DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.DateFormat);
		Date date = new Date();

		/**declare session and transaction object*/ 
		Session Obj_sess = null;
		/**Set default value some fields of INVENTORY_LEVELS*/
		try{
			Obj_sess = sessionFactory.getCurrentSession();


			Obj_InventoryInv.setHoldCode(GLOBALCONSTANT.BlankString);
			Obj_InventoryInv.setSystemCode(GLOBALCONSTANT.BlankString);
			Obj_InventoryInv.setLastActivityDate(date);
			Obj_InventoryInv.setDateControl(GLOBALCONSTANT.INT_ZERO);
			Obj_InventoryInv.setDateReceived(date);

			/**Insert data into INVENTORY_LEVELS table*/

			Obj_sess.save(Obj_InventoryInv);

			/**Set default value some fields of INVENTORY_TRANSACTIONS**/
			Obj_Inventorytxn.setLastActivityDate(dateFormat.format(date));
			Obj_Inventorytxn.setQuantityBefore(GLOBALCONSTANT.INT_ZERO);

			/**Insert data into INVENTORY_TRANSACTIONS table*/

			Obj_sess.save(Obj_Inventorytxn);

			/**Update Cycle Points in Table LOCATIONS
			 * Get Location_Profile from Locations table on the basis of Inventry_Levels.Location
			 */
              

			List<LOCATIONS> LOCATIONSlist=new ArrayList<LOCATIONS>();
			LOCATIONSlist = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LOCATIONS)
					.setParameter(GLOBALCONSTANT.INT_ZERO, Obj_InventoryInv.getId().getLocation().toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, Obj_InventoryInv.getId().getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, Obj_InventoryInv.getId().getFulfillmentCenter().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_THREE, Obj_InventoryInv.getId().getCompany().toUpperCase())
					.list();
			Iterator<LOCATIONS> ObjLocationIterator = LOCATIONSlist.iterator();
			LOCATIONS ObjLocation = ObjLocationIterator.next(); 
			String LocationProfile = ObjLocation.getLocationProfile();


			/**Get Profile from Location_Profiles table on the basis of Locations.Location_Profile*/

			List<LOCATION_PROFILES> LOCATION_PROFILESlist=new ArrayList<LOCATION_PROFILES>();
			LOCATION_PROFILESlist = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LOCATION_PROFILES)
					.setParameter(GLOBALCONSTANT.INT_ZERO, LocationProfile.toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, Obj_InventoryInv.getId().getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, Obj_InventoryInv.getId().getFulfillmentCenter().toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_THREE, Obj_InventoryInv.getId().getCompany().toUpperCase())
					.list();
			Iterator<LOCATION_PROFILES> ObjLocationProfileIterator =LOCATION_PROFILESlist.iterator();
			LOCATION_PROFILES ObjLocationProfile = ObjLocationProfileIterator.next(); 
			double CCActivityPoint = Integer.parseInt(ObjLocationProfile.getCCActivityPoint());
			double CCHighValueAmout = Integer.parseInt(ObjLocationProfile.getCCHighValueAmount());
			double CCHighValueFactor = Integer.parseInt(ObjLocationProfile.getCCHighValueFactor());
			double CYCLECOUNTPOINTS = GLOBALCONSTANT.INT_ZERO;
			if(Obj_InventoryInv.getProductValue() >= CCHighValueAmout){
				CYCLECOUNTPOINTS = CCActivityPoint * CCHighValueFactor;
			}

			/** Set value and default value on the basis of Inventory_Levels and Inventory_Transaction in Locations table*/

			ObjLocation.setCycleCountPoints((int) CYCLECOUNTPOINTS);
			ObjLocation.setLastActivityDate(dateFormat.format(date));
			ObjLocation.setLastCycleCountDate(dateFormat.format(date));
			ObjLocation.setLastActivityTeamMember(Obj_InventoryInv.getLastActivityEmployee());

			/**Update data into Locations table*/
			Obj_sess.saveOrUpdate(ObjLocation);

		}catch(Exception obj_e)
		{
			throw new JASCIEXCEPTION(obj_e.getMessage());
		}


	}



}
