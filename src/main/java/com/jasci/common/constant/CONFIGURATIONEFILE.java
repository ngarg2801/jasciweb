/**
Description In This class we read data from config file 
Created By Aakash Bishnoi
Created Date Nov 6 2014
*/
package com.jasci.common.constant;


import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import com.jasci.biz.AdminModule.be.GOJSPROPERTIESBE;
import com.jasci.exception.JASCIEXCEPTION;

public class CONFIGURATIONEFILE {
	/**This function return object of propert class*/ 
	public String getConfigProperty(String StrPropertyName) throws JASCIEXCEPTION{
		
		Properties ObjectProperty = new Properties();
		String StrPropertyFileName = GLOBALCONSTANT.ConfigFilePathWithName;
		String PropName = "";
		try{ 
		/**read the file config.properties*/
			InputStream ObjectInputStream =  new FileInputStream(StrPropertyFileName);
			ObjectProperty.load(ObjectInputStream);
		PropName = ObjectProperty.getProperty(StrPropertyName).toString();
		return PropName.toString();
		}catch(Exception ObjectIOException){
			throw new JASCIEXCEPTION("Config file not found exception");
		}
	}
	
	
	/**This function return object of propert class*/ 
	public GOJSPROPERTIESBE getGoJSConfigProperty() throws JASCIEXCEPTION{
		
        GOJSPROPERTIESBE OBJGOJSPROPERTIESBE = new GOJSPROPERTIESBE();		
		
		Properties ObjectProperty = new Properties();
		String StrPropertyFileName = GLOBALCONSTANT.GOJSConfigFilePathWithName;
		
		try{ 
		/**read the file config.properties*/
			InputStream ObjectInputStream =  new FileInputStream(StrPropertyFileName);
			ObjectProperty.load(ObjectInputStream);
			/**get and set property info*/
			OBJGOJSPROPERTIESBE.setCircleHeight(Integer.parseInt(ObjectProperty.getProperty("circleheight").toString()));
			OBJGOJSPROPERTIESBE.setCircleWidth(Integer.parseInt(ObjectProperty.getProperty("circlewidth").toString()));
			OBJGOJSPROPERTIESBE.setCircleTextHeight(Integer.parseInt(ObjectProperty.getProperty("circletextheight").toString()));
			OBJGOJSPROPERTIESBE.setCircleTextWidth(Integer.parseInt(ObjectProperty.getProperty("circletextwidth").toString()));
			
			OBJGOJSPROPERTIESBE.setRectangleHeight(Integer.parseInt(ObjectProperty.getProperty("rectangleheight").toString()));
			OBJGOJSPROPERTIESBE.setRectangleWidth(Integer.parseInt(ObjectProperty.getProperty("rectanglewidth").toString()));
			OBJGOJSPROPERTIESBE.setRectangleTextHeight(Integer.parseInt(ObjectProperty.getProperty("rectangletextheight").toString()));
			OBJGOJSPROPERTIESBE.setRectangleTextWidth(Integer.parseInt(ObjectProperty.getProperty("rectangletextwidth").toString()));

			OBJGOJSPROPERTIESBE.setDiamondHeight(Integer.parseInt(ObjectProperty.getProperty("diamondheight").toString()));
			OBJGOJSPROPERTIESBE.setDiamondWidth(Integer.parseInt(ObjectProperty.getProperty("diamondwidth").toString()));
			OBJGOJSPROPERTIESBE.setDiamondTextHeight(Integer.parseInt(ObjectProperty.getProperty("diamondtextheight").toString()));
			OBJGOJSPROPERTIESBE.setDiamondTextWidth(Integer.parseInt(ObjectProperty.getProperty("diamondtextwidth").toString()));

			OBJGOJSPROPERTIESBE.setDisplayHeight(Integer.parseInt(ObjectProperty.getProperty("displayheight").toString()));
			OBJGOJSPROPERTIESBE.setDisplayWidth(Integer.parseInt(ObjectProperty.getProperty("displaywidth").toString()));
			OBJGOJSPROPERTIESBE.setDisplayTextHeight(Integer.parseInt(ObjectProperty.getProperty("displaytextheight").toString()));
			OBJGOJSPROPERTIESBE.setDisplayTextWidth(Integer.parseInt(ObjectProperty.getProperty("displaytextwidth").toString()));

			OBJGOJSPROPERTIESBE.setHorizontalDistance(Integer.parseInt(ObjectProperty.getProperty("horizontaldistance").toString()));
			OBJGOJSPROPERTIESBE.setVerticalDistance(Integer.parseInt(ObjectProperty.getProperty("verticaldistance").toString()));
			OBJGOJSPROPERTIESBE.setDiamondVerticalDistance(Integer.parseInt(ObjectProperty.getProperty("diamondverticaldistance").toString()));
			OBJGOJSPROPERTIESBE.setCircleVerticalDistance(Integer.parseInt(ObjectProperty.getProperty("circleverticaldistance").toString()));
			OBJGOJSPROPERTIESBE.setSquareVerticalDistance(Integer.parseInt(ObjectProperty.getProperty("squareverticaldistance").toString()));
			
			
			OBJGOJSPROPERTIESBE.setMaxToolTipHeight(Integer.parseInt(ObjectProperty.getProperty("maxtooltipheight").toString()));
			OBJGOJSPROPERTIESBE.setMaxToolTipWidth(Integer.parseInt(ObjectProperty.getProperty("maxtooltipwidth").toString()));
			OBJGOJSPROPERTIESBE.setMinToolTipWidth(Integer.parseInt(ObjectProperty.getProperty("mintooltipheight").toString()));
			OBJGOJSPROPERTIESBE.setMinToolTipWidth(Integer.parseInt(ObjectProperty.getProperty("mintooltipwidth").toString()));

			OBJGOJSPROPERTIESBE.setMinTextHeight(Integer.parseInt(ObjectProperty.getProperty("mintextheight").toString()));
			OBJGOJSPROPERTIESBE.setMinTextWidth(Integer.parseInt(ObjectProperty.getProperty("mintextwidth").toString()));

			OBJGOJSPROPERTIESBE.setMarginTop(Integer.parseInt(ObjectProperty.getProperty("margintop").toString()));
			OBJGOJSPROPERTIESBE.setMarginLeft(Integer.parseInt(ObjectProperty.getProperty("marginleft").toString()));
			
			OBJGOJSPROPERTIESBE.setSquareHeight(Integer.parseInt(ObjectProperty.getProperty("squareheight").toString()));
			OBJGOJSPROPERTIESBE.setSquareWidth(Integer.parseInt(ObjectProperty.getProperty("squarewidth").toString()));
			OBJGOJSPROPERTIESBE.setSquareTextHeight(Integer.parseInt(ObjectProperty.getProperty("squaretextheight").toString()));
			OBJGOJSPROPERTIESBE.setSquareTextWidth(Integer.parseInt(ObjectProperty.getProperty("squaretextwidth").toString()));
			OBJGOJSPROPERTIESBE.setIsSequenceViewGrid(Boolean.parseBoolean(ObjectProperty.getProperty("issequenceviewgrid").toString()));
			
			OBJGOJSPROPERTIESBE.setTextFontSizeForActionPanel(ObjectProperty.getProperty("textfontsizeforactionpanel").toString());
			OBJGOJSPROPERTIESBE.setTextFontSizeForExecutionPanel(ObjectProperty.getProperty("textfontsizeforexecutionpanel").toString());
			OBJGOJSPROPERTIESBE.setTextFontSizeForFlowChartPanel(ObjectProperty.getProperty("textfontsizeforflowchartpanel").toString());



		
		return OBJGOJSPROPERTIESBE;
		}catch(Exception ObjectIOException){
			throw new JASCIEXCEPTION("Config file not found exception");
		}
	}
}
