/**
Description Declaration of constants
Created By Shailendra Rajput
Created Date sept 18 2014
 */

package com.jasci.common.constant;

import com.jasci.biz.AdminModule.be.SHAREDDATA;


public class GLOBALCONSTANT 
{

	/**
	 * Global constant for common class 
	 *  
	 */

	/**Start Config file Path*/
	public static final String ConfigFilePathWithName = "C:/Production/JASCI/PropertiesFile/config.properties";
	public static final String GOJSConfigFilePathWithName = "C:/Production/JASCI/PropertiesFile/gojsconfig.properties";
	public static final String ADDITIONALEXECUTIONFILEPATHNAME = "C:/Production/JASCI/AdditionalExecution/";
	public static String DestinationDirectory = "C:/Production/JASCI/DuplicateFile/";
	public static final String FileExtention = ".txt";
	/**End config file path*/
	public static final String AVLocale = "en";
	public static final String AVAPIKey = "av-70510e8f7ca0a612f6b383ce8e1bf7e2";
	public static final String AVAPIURL = "http://api.address-validator.net/api/verify";
	public static final String AVStreetAddress="StreetAddress";
	public static final String AVPostalCode="PostalCode";
	public static final String AVCity="City";
	public static final String AVState="State";
	public static final String AVCountryCode="CountryCode";
	public static final String AVStatus="status";
	public static final String AVFormattedAddress = "formattedaddress";
	public static final String AVVALID = "VALID";
	public static final String AVINVALID = "INVALID";
	public static final String HiphenSign="-";
	public static final String Equal="=";
	public static final String And="&";
	public static final String Question="?";
	public static final String space = "\\s+";
	public static final String Blank = "";
	public static final String PercentSignEncoder = "%20";
	public static final String MethodType="GET";
	public static final String User_Agent="User-Agent";
	public static final String Mozilla="Mozilla/5.0";
	public static final String Null="null";
	public static final String InvalidFulfillment="Invalid Fulfillment";
	public static final String InvalidFulfillmentCenter="Invalid Fulfillment Center ID";
	public static final String Yes="Y";
	public static final String No="N";
	public static final String NotFound="Not Found";
	public static final String NoLongerInSystem="No Longer in System";
	public static final String Error="Error";
	public static final String UTF8="UTF-8";
	public static final String GoogleUrlAddress="http://maps.googleapis.com/maps/api/geocode/json?address=";
	public static final String GoogleUrlSensor="&sensor=true";
	public static final String Status="status";
	public static final String OK="OK";
	public static final String Results="results";
	public static final String Types="types";
	public static final String PostalCode="postal_code";
	public static final char Y='Y';
	public static final char N='N';
	public static final String AddressGoogleUrlAddress="{\"address\":\"";
	public static final String GoogleUrlZip="\",\"Zip\":\"";
	public static final String GoogleUrlCity="\",\"city\":\"";
	public static final String GoogleUrlState="\",\"State\":\"";
	public static final String GoogleUrlCountry="\",\"Country\":\"";
	public static final String GoogleUrlClosing="\"}";
	public static final String AddressGoogleUrl="http://maps.google.com/maps/api/geocode/json?address=";
	public static final String AddressGoogleUrlKey="&Key=AIzaSyA-BjZMLjGxPmerohTn_iKJKiF5o8bmABA&sensor=false";
	public static final String RequestMapping_LoginScreen_Action="/loginForm";
	public static final String RequestMapping_ResponseScreen_Action="/loginForm";
	public static final String RequestMapping_SetPasswordScreen_Action="/setPassword";
	public static final String RequestMapping_ForgotPasswordScreen_Action="/forgetpassword";
	public static final String RequestMapping_SendEmailResponse_Action="/SendMailResponsePage";
	public static final String RequestMapping_SendEmailResponse_Page="SendMailResponsePage";
	public static final String RequestMapping_LoginScreen_loginPage="login";
	public static final String RequestMapping_ResponseScreen_ResponsePage="ResponsePage";
	public static final String RequestMapping__LoginScreen_ResponseObject="ObjMenuMessages";
	public static final String RequestMapping__LoginScreen_ScreenLabelsObject="ObjLoginBe";
	public static final String RequestMapping__LoginScreen_Validate="validate";
	public static final String RequestMapping_ResponseScreen_ResponseObject="message";
	public static final String RequestMapping_ResponseScreen_ValidationResult="ValidationResult";
	public static final String RequestMapping_LoginScreen_ForgetPasswordPage="forgetpassword";
	public static final String ModelAttribute_LoginScreen_ForgotPassword= "forgotpassword";
	public static final String Result="Result";
	public static final String RequestMapping_SetPasswordScreen_SetPassword="getNewPassword";
	public static final String RequestMapping_Login_SendEmail="SendEmail";
	//public static final String SuppressWarnings_value="unchecked";
	public static final String RequestMapping_NewPassword_Page="setPassword";
	public static final String Only_Date_Format="yyyyMMdd";
	public static final String Date_Format_yy_MM_dd="yy-MM-dd";
	public static final String RequestMapping_PasswordExpiryScreen_ScreenLabelsObject="ObjPasswordBe";
	public static final String RequestMapping_ResponseScreen_ErrorPagePage="ErrorPage";
	public static final String CompanyLogo="CompanyLogo";
	public static final String FOOTERTEXT="footertext";
	public static final String FORVALIDEMAILID="forvalidemailid";
	public static final String Remember = "remember";
	public static final String ON = "on";
	public static final String CookieLoginUserId = "cookieLoginUserId";
	public static final String Currentdate = "Pdate";
	public static final String Current_Date_Format = "dd-MMM-yyyy";
	public static final String Password_Regex = "(?=.*\\d.*\\d.*\\d)(?=.*[A-Z]).{8,30}";
	public static final String Password_matched_Response="Password matched";
	public static final String Password_Expired="password expired";
	public static final String Password_Not_Matched="Password not matched";
	public static final String Not_Allowed="NotAllowed";
	public static final String AllowedMoreLogin="AllowedMoreLogin";
	public static final String ErrorMessagetoInvalidAttemptForgot="You are not allowed to do forgot password as your invalid attempts have been exceeded to 5. So, Please contact Tenant's security officer.";
	public static final String TemporaryPassword="TemporaryPassword";
	public static final String StrUserId="StrUserId";
	public static final String Param_Languages_Loginscreen_ObjectCode="LoginScreen";
	public static final String Param_Languages_Language="ENG";
	public static final String Param_Languages_PasswordExpiryscreen_ObjectCode="PasswordExpiryScreen";
	public static final String RequestMapping_GeneralCodes_MainForm="/MainForm";
	public static final String RequestMapping_GeneralCodes_EditDelete="/GeneralCodeEditDelete";
	public static final String redirect="redirect";
	public static final String forward="forward";
	public static final String colon=":";
	public static final String RequestMapping_GeneralCodes_GeneralCodeList="/GeneralCodeList";
	public static final String RequestMapping_GeneralCodes_register="/register";
	public static final String RequestMapping_GeneralCodes_GeneralCodeUpdate="GeneralCodeUpdate";
	public static final String RequestMapping_GeneralCodes_GeneralCodeEdit="/GeneralCodeEdit/{tenant}";
	public static final String GeneralCodeEdit="GeneralCodeEdit";
	public static final String GeneralCodesObject="GeneralCodesObject";
	public static final String GeneralCode_ModelAttribute="GENERAL_CODES";
	public static final String RequestMapping_GeneralCodes_GeneralCodeDelete="/GeneralCodeDelete/{tenant}";
	public static final String GeneralCodeList="GeneralCodeList";
	public static final String PathVaraible_Tenant="tenant";
	public static final String RequestMapping_GeneralCodes_GeneralCodesObject="GeneralCodesObject";
	public static final String GeneralCodes_Config_Company="company";
	public static final String GeneralCodes_Config_Tenant="tenant";
	public static final String Login_Config_Login_Tenant="tenant_login";
	public static final String Login_Config_Status="status";
	public static final String GeneralCodes_Config_TeamMember="TeamMember";
	public static final String Hyphen="-";
	public static final String NoWhiteSpace="";
	public static final String	ErrorMessage="ErrorMessage";
	/**
	 * constant for Password Expiry Beans
	 * 
	 */
	public static final String CreateNew="CreateNew";
	public static final String NewPassword="NewPassword";
	public static final String ConfirmPassword="ConfirmPassword";
	public static final String PasswordExpiredMessage="PasswordExpiredMessage";
	public static final String PasswordNotMatchedMessage="PasswordNotMatchedMessage";
	public static final String PasswordUsedMessage="PasswordUsedMessage";
	public static final String PasswordPatternMessage="PasswordPatternMessage";
	public static final String TemporaryPasswordMessage="TemporaryPasswordMessage";
	public static final String GeneralCodes_Edit="Edit";
	public static String Global_Config_Login_Tenant = "";
	public static final String BlankString = "";
	public static final String EmailAddressDoesNotExit = "Email address you have entered does not exist.";
	/**
	 * constant for Get Common session data
	 * 
	 */
	public static final String GETCOMMONSESSIONBE= "GETCOMMONSESSIONBE";
	public static final String STRUSERNAME= "strUsername";
	public static final String RESULTSET_COMMONSESSIONBE= "RESULTSET_COMMONSESSIONBE";
	public static final String TENANT = "TENANT";
	public static final String TEAMMEMBER = "TEAMMEMBER";
	public static final String COMPANY = "COMPANY";
	public static final String PASSWORDDATE = "PASSWORDDATE";
	public static final String STATUS = "STATUS";
	public static final String CNAME20 = "CNAME20";
	public static final String CNAME50 = "CNAME50";
	public static final String PURCHASEORDERSREQUIREAPPROVAL = "PURCHASEORDERSREQUIREAPPROVAL";
	public static final String FULFILLMENTCENTER = "FULFILLMENTCENTER";
	public static final String FCNAME20 = "FCNAME20";
	public static final String FCNAME50 = "FCNAME50";
	public static final String TLANGUAGE = "TLANGUAGE";
	public static final String TMLANGUAGE = "TMLANGUAGE";
	//public static final String THEMERF = "THEMERF";
	//public static final String THEMEFULLDISPLAY = "THEMEFULLDISPLAY";
	public static final String PASSWORDEXPIRATIONDAYS = "PASSWORDEXPIRATIONDAYS";
	public static final String TEAMMEMBERNAME = "TEAMMEMBERNAME";
	public static final String AUTHORITYPROFILE = "AUTHORITYPROFILE";
	public static final String MENU_PROFILE_TABLET = "MENU_PROFILE_TABLET";
	public static final String MENU_PROFILE_STATION = "MENU_PROFILE_STATION";
	public static final String MENU_PROFILE_RF = "MENU_PROFILE_RF";
	public static final String MENU_PROFILE_GLASS = "MENU_PROFILE_GLASS";
	public static final String EQUIPMENTCERTIFICATION = "EQUIPMENTCERTIFICATION";
	public static final String SYSTEMUSE = "SYSTEMUSE";
	public static final String PASSWORD = "PASSWORD";
	public static final String INVALIDATTEMPTDATE = "INVALIDATTEMPTDATE";
	public static final String NUMBERATTEMPTS = "NUMBERATTEMPTS";
	public static final String TNAME20 = "TNAME20";
	public static final String TNAME50 = "TNAME50";
	public static final String RequestMapping_GeneralCodes_DeleteGeneralCodeID="/General_code_identification_delete/{tenant}/{company}/{application}/{generalcodeid}/{generalcode}";
	/**
	 * Constant for Common Class
	 * 	 
	 */
	public static final String RequestMapping_ResponseScreen_Actiontestcommonclasess="/testcommonclasses";
	public static final String RequestMapping_ResponseScreen_Actiontestcommonclasess_jap="/commonclassresult";
	public static final String RequestMapping__LoginScreen_Validatetestcommonclasess="testcommonclassesvalidate";

	public static final String DateFormat = "yyyy/MM/dd HH:mm:ss";
	public static final String Date_Format_DD_MM_YYYY = "dd-MM-yyyy";
	public static final int Zero = 0;
	public static final String GeneralCodes_StrApplication="StrApplication";
	public static final String GeneralCodes_StrGeneralCode="StrGeneralCode";
	public static final String GeneralCodes_ApplicationValueList="ApplicationValue";
	public static final String GeneralCodes_GeneralCodeValueList="GeneralCodeValue";
	public static final String GeneralCodes_ScreenLabel="ScreenLabel";
	public static final String GeneralCodes_JspSelectApplication="SelectApplication";//for getting Application value form jsp page.
	public static final String GeneralCodes_JspTextBoxGeneralCode="TextBoxGeneralCode";//for getting generalcode value form jsp page.
	public static final String GeneralCodes_JspSelectGeneralCode="SelectGeneralCode";//for getting generalcodeid value form jsp page.
	public static final String GeneralCodes_ConstraintViolation="ConstraintViolation";
	public static final String GeneralCodes_actionValue="actionValue";//getting the button value that used in menu calling
	public static final String GeneralCodes_FilterGeneralCodes="GeneralCodes";//for get the list of generalcodes on generalcodelist page
	public static final String GeneralCodes_MenuValue="MenuValue";
	public static final String RequestMapping_GeneralCodes_GeneralCodeIdentificationdata="/General_code_identificationdata";
	public static final String RequestMapping_GeneralCodes_General_code_identification="/General_code_identification";
	public static final String RequestMapping_GeneralCodes_GeneralCodeEditDelete="/GeneralCodeEditDelete/{tenant}/{company}/{application}/{generalcodeid}";
	public static final String GeneralCodes_QueryNameGeneralCodeEditListUnique="GeneralCodeEditListUnique";
	public static final String GeneralCodes_QueryNameGeneralCodeIdentifiedEdit="GeneralCodeIdentifiedEdit";
	public static final String GeneralCodes_QueryNameGeneralCodeidentificationList="GeneralCodeidentificationList";
	//public static final String GeneralCodes_QueryNameSelectApplicationGeneralCode="SelectApplicationGeneralCode";
	//public static final String GeneralCodes_QueryNameSelectGeneralCodeComboBox="SelectGeneralCodeComboBox";
	public static final String GeneralCodes_QueryNameGeneralCodeMainList="GeneralCodeMainList";
	public static final String GeneralCodes_QueryNameGeneralCodeSubList="GeneralCodeSubList";	 
	public static final String GeneralCodes_SqlResultSetMapping_SelectApplicationGeneralCodeResultSet="SelectApplicationGeneralCodeResultSet";
	public static final String GeneralCodes_SqlResultSetMapping_SelectGeneralCodeComboBoxResultSet="SelectGeneralCodeComboBoxResultSet";
	public static final String GeneralCodes_ColumnResult_GENERALCODE="GENERAL_CODE";
	public static final String MenuOptions_ColumnResult_MenuName="MENU_OPTION";
	public static final String GeneralCodeEditDelete_GeneralCode_NewEdit="GeneralCodeEditDelete_General Code New/Edit";
	public static final String GeneralCodeList_General_Code="GeneralCodeList_General Code";
	public static final String GeneralCodes_exception_CannotCreateTransactionException="Transactional Probelm.";
	public static final String GeneralCodes_exception_GenericJDBCException="Database Probelem.";
	public static final String GeneralCodes_exception_NullPointerException="Null pointer exception.";
	public static final String GeneralCodes_exception_Exception="Unknown Exception.";
	public static final String GeneralCodes_exception_IndexOutOfBoundsException="Index out of Bound exception";
	public static final String GeneralCodes_Kendo_GeneralCodeDelete="GeneralCodeList/Grid/delete";
	public static final String GeneralCodes_Kendo_GeneralCodeIDDelete="GeneralCodeID/Grid/delete";
	public static final String GeneralCodes_Kendo_GeneralCodeRead="/GeneralCodeList/Grid/read";
	public static final String GeneralCodes_Kendo_GeneralCodeIDReadMain="/GeneralCodeListID/Grid/readlists";
	public static final String GeneralCodes_Kendo_GeneralCodeIDReadSub="/GeneralCodeSubListID/Grid/readSublists";
	public static final String ZeroValue="0";
	public static final String OneValue="1";
	public static final String GeneralCodes_exception_AddException="Unable to Add Record.";
	public static final String GeneralCodes_exception_Delete="Unable to delete Record.";
	public static final String GeneralCodes_exception_Update="Unable to update Record.";
	public static final String RequestMapping_GeneralCodesidentification="/General_code_identificationdata/";
	public static final String RequestMapping_MenuValue="?MenuValue=";
	public static final String MenuList="MenuList";
	public static final String SubList="SubList";
	public static final String GeneralCodeEditDelete="/GeneralCodeEditDelete/";
	public static final String StrTenant="Tenant";
	public static final String StrCompany="Company";
	public static final String StrApplication="Application";
	public static final String StrGeneralCodeID="GeneralCodeID";
	public static final String StrGeneralCode="GeneralCode";
	public static final String ScreenGeneralCodeEditDelete="GeneralCodeEditDelete";
	public static final String ScreenGeneralCodeList="GeneralCodeList";
	public static final String LanguageEnglish="ENGLISH";
	public static final String RequestMapping_Teammember_AfterUpdate_Action="/Teammember_searchlookup_after_update";
	public static final String RequestMapping_Teammember_AfterNotUpdate_Action="/Teammember_searchlookup_after_notupdate";
	public static final String RequestMapping_Teammember_searchlookup_Action="/Teammember_searchlookup";
	public static final String RequestMapping_Teammember_Sort_searchlookup_Action="/Teammember_Sort_searchlookup/{SortValue}";
	public static final String RequestMapping_Teammember_UpdateTeamMember_searchlookup_Action="/Teammember_searchlookup_update";
	public static final String RequestMapping_Teammember_Delete_searchlookup_Action="/Teammember_searchlookup_delete";
	public static final String RequestMapping_Teammember_searchlookup_Page="Teammember_searchlookup";
	public static final String RequestMapping_Teammember_searchlookup_Page_Sort="Teammember_searchlookup_sort";
	public static final String RequestMapping_Team_member_maintenance_Action="/Team_member_maintenance";
	public static final String RequestMapping_Team_member_maintenance_Action_lokup="/Team_member_maintenance_lookup";
	public static final String RequestMapping_Team_member_maintenance_Page="Team_member_maintenance";
	public static final String RequestMapping_Team_member_maintenancenew_Action="/Team_member_maintenancenew";
	public static final String RequestMapping_Team_member_Insertnew_Action="/Team_member_maintenancenew";
	public static final String RequestMapping_Team_member_maintenancenew_Page="Team_member_maintenancenew";
	public static final String RequestMapping_Team_member_maintenanceUpdate_Action="/Team_member_maintenance_update";
	public static final String RequestMapping_Team_member_maintenanceUpdate_Page="Team_member_maintenance_update";
	public static final String RequestMapping_TeamMember_company_Action="/TeamMember_company";
	public static final String RequestMapping_TeamMember_company_Page="TeamMember_company";
	public static final String TeamMembersListObject="TeamMembersListObject";
	public static final String TeamMemberInvalidMsg="TeamMemberInvalidMsg";
	public static final String TeamMemberInvalidTeamMemberIdMsg="TeamMemberInvalidTeamMemberIdMsg";
	public static final String TeamMembers_Register_ModelAttribute="TEAM_MEMBERS_REGISTER";
	public static final String INFO_RECORD_SAVED="Your record has been saved successfully.";
	public static final String INFO_RECORD_UPDATE="Your record has been updated successfully.";
	public static final String INFO_INVALID_TEAMMEMEBR="Invalid Team Member.";
	public static final String INFO_INVALID_PART_OF_TEAMMEMEBR="Invalid part of Team Member Name.";
	public static final String INFO_TEAMMEMBER_USED="Team member already Used.";
	public static final String TeamMemberName="TeamMemberName";
	public static final String PartOfTeamMemberName="PartOfTeamMemberName";
	public static final String SortName="SortName";
	public static final String PartOfTeamMemberObj="PartOfTeamMemberObj";
	public static final String FirstName="FirstName";
	public static final String LastName="LastName";
	public static final String TeamMemberNameFromUpdate="TeamMemberNameFromUpdate";
	public static final String AcityvityDate="AcityvityDate";
	public static final String CompaniesListObject="CompaniesListObject";
	public static final String CompaniesTeamMemberListObject="CompaniesTeamMemberListObject";
	public static final String GeneralCodeDepartmentListObject="GeneralCodeDepartmentListObject";
	public static final String GeneralCodeShiftListObject="GeneralCodeShiftListObject";
	public static final String GeneralCodeLanguageListObject="GeneralCodeLanguageListObject";
	public static final String GeneralCodeLanguageCountryCodeListObject="GeneralCodeLanguageCountryCodeListObject";
	public static final String GeneralCodeStatesListObject="GeneralCodeStatesListObject";
	public static final String TeamMemberIdValue="TeamMemberIdValue";
	public static final String TeamMember_Update_Object="TeamMember_Update_Object";
	public static final String Param_Languages_TeamMemberMaintenance_ObjectCode="TeamMemberMaintenance";
	public static final String RequestMapping_Team_member_maintenance_Action_grid="/Team_member_maintenance_grid";
	public static final String CompanyCheckBox="checkBoxCompany";
	public static final String TeamMember_Kendo_TeamMemberRead="/Teammember_searchlookup/Grid/read";//"/GeneralCodeList/Grid/read";
	public static final String TeamMember_Kendo_TeamMemberRead_Sort="/Teammember_searchlookup_sort/Grid/read";//"/GeneralCodeList/Grid/read";
	//public static final String MenuTypes="MenuTypes";
	public static final String TeamMemberDeleteException="first delete record from SecurityAuthorization";
	public static final String DataBaseException="No Records found";
	public static final String TeamMembersNamePart="TeamMemberNamePart";
	public static final String GeneralCodesListObject="GeneralCodesListObject"; 
	public static final String RequestMapping_Teammember_Selection_Delete_Action="/Teammember_selection_delete/{Tenant}/{TeamMember}";
	/**
	 * Constant for General Code screen
	 * 
	 */
	public static final String GeneralCodes_Restfull_RestGeneralCodeList="/RestGeneralCodeList";
	public static final String GeneralCodes_Restfull_RestGeneralCodeListMain="/RestGeneralCodeListMain";
	public static final String GeneralCodes_Restfull_RestGeneralCodeListSub="/RestGeneralCodeListSub";
	public static final String GeneralCodes_Restfull_tenant="tenant";
	public static final String GeneralCodes_Restfull_company="company";
	public static final String GeneralCodes_Restfull_generalcode="generalcode";
	public static final String GeneralCodes_Restfull_generalcodeid="generalcodeid";
	public static final String GeneralCodes_Restfull_application="application";
	public static final String GeneralCodes_Restfull_RestGeneralCodeAdd="/RestGeneralCodeAdd";
	public static final String GeneralCodes_Restfull_RestManageGeneralCodeAdd="/RestManageGeneralCodeAdd";
	public static final String GeneralCodes_Restfull_RestGeneralCodeUpdate="/RestGeneralCodeUpdate";
	public static final String GeneralCodes_Restfull_RestManageGeneralCodeUpdate="/RestManageGeneralCodeUpdate";
	public static final String GeneralCodes_Restfull_RestGeneralCodeDelete="/RestGeneralCodeDelete";
	public static final String GeneralCodes_Restfull_RestManageGeneralCodeDelete="/RestManageGeneralCodeDelete";
	public static final String GeneralCodes_Restfull_RestGeneralCodeApplicationCombobox="/RestGeneralCodeApplicationCombobox";
	public static final String GeneralCodes_Restfull_RestGeneralCodeGeneralCodeCombobox="/RestGeneralCodeGeneralCodeCombobox";
	public static final String GeneralCodes_Restfull_RestGeneralCodeEditDeleteScreenlabel="/RestGeneralCodeEditDeleteScreenlabel";
	public static final String GeneralCodes_Restfull_RestGeneralCodeListScreenlabel="/RestGeneralCodeListScreenlabel";
	public static final String GeneralCodes_Restfull_GeneralCodeListFetch="/GeneralCodeListFetch";
	public static final String GeneralCodes_QueryNameDeleteRelatedGeneralCode="DeleteRelatedGeneralCode";
	public static final String RequestMapping_ResponsePage="/ResponsePage";
	public static final String GeneralCodes_exception_ConstraintViolationException="General code or Menu name already used.";
	public static final String GeneralCodes_exception_GeneralCodeAlreadyUsed="General code alredy used.";
	public static final String GeneralCodes_QueryNameCheckUniqueGeneralCode="CheckUniqueGeneralCode";
	public static final String MenuOptions_QueryNameCheckUniqueMenuNameCode="CheckUniqueMenuName";
	public static final String GeneralCodes_SqlResultSetMapping_CheckUniqueGeneralCodeResultSet="CheckUniqueGeneralCodeResultSet";
	public static final String MenuOptions_SqlResultSetMapping_CheckUniqueMenuOptionResultSet="CheckUniqueMenuOptionResultSet";
	public static final String GeneralCodes_QueryNameCheckUniqueManageGeneralCode="CheckUniqueManageGeneralCode";
	public static final String GeneralCodes_SqlResultSetMapping_CheckUniqueManageGeneralCodeResultSet="CheckUniqueManageGeneralCodeResultSet";
	public static final String editdata="/GeneralCodeEdit";
	public static final String PageStatus="PageStatus";
	public static final String GeneralCodes_JspStrTenant="Tenant";
	public static final String GeneralCodes_JspStrCompany="Company";
	public static final String GeneralCodes_RequestMapping_UpdateGeneralCode="/GeneralCodeUpdates";
	public static final String Single_Space=" ";
	public static final String INFO_TEAMMEMBER_Email_AllreadyExist="Email address already exist.";
	public static final String TeamMemberSceernLabelObj="TeamMemberSceernLabelObj";
	public static final String OldEmail="OE";				
	public static final String NewEmail="NE";
	/**
	 * Constant declaration for Restfull Service
	 * 
	 */
	public static final String TeamMemebers_Restfull_GetList="/RestGetTeamMembersList";
	public static final String TeamMemebers_Restfull_AddEntry="/RestGetTeamMembersAddEntry";
	public static final String TeamMemebers_Restfull_DeleteEntry="/RestGetTeamMembersDeleteEntry";
	public static final String TeamMemebers_Restfull_UpdateEntry="/RestGetTeamMembersUpdateEntry";
	public static final String TeamMemebers_Restfull_GetCompanies="/RestGetTeamMembersGetCompanies";
	public static final String TeamMemebers_Restfull_GetGeneralCode="/RestGetTeamMembersGetGeneralCode";
	public static final String TeamMemebers_Restfull_SetScreenLanguage="/RestGetTeamMembersSetScreenLanguage";
	public static final String TeamMemebers_Restfull_GetTeamMemberList="/RestGetTeamMembersGetTeamMemberList";
	public static final String TeamMemebers_Restfull_GetTeamMemberCompanies="/RestGetTeamMembersGetTeamMemberCompanies";
	public static final String TeamMemebers_Restfull_Company="Company";
	public static final String TeamMemebers_Restfull_TeamMember_name="TeamMemberName";
	public static final String TeamMemebers_Restfull_Part_of_TeamMember_name="PartOfTeamMember";
	public static final String TeamMemebers_Restfull_Tenant="Tenant";
	public static final String TeamMemebers_Restfull_FulfillmentCenter="FulfillmentCenter";
	public static final String TeamMemebers_Restfull_TeamMember="TeamMember";
	public static final String TeamMemebers_Restfull_TeamMemberOldid="TeamMemberOldId";
	public static final String TeamMemebers_Restfull_GeneralCodeId="GeneralCodeId";
	public static final String TeamMemebers_Restfull_ScreenName="ScreenName";
	public static final String TeamMemebers_Restfull_ScreenLanguage="ScreenLanguage";
	public static final String TeamMemebers_Restfull_SortOrder="SortOrder";
	public static final String TeamMemebers_Restfull_ObjTeamMembersBe="ObjectTeamMembersBe";
	public static final String TeamMemebers_Restfull_CompaniesList="CompaniesList";
	public static final String TeamMemebers_Restfull_LastActivityBy="strLastActivityBy";
	public static final String StrLastName="LastName";
	public static final String StrTeamMember="teamMember";	
	public static final String StrTenantF="tenant";
	public static final String StrfullFillmentCenter="fullFillmentCenter";
	public static final String StrsortValue="sortValue";
	public static final String RequestMapping_Team_member_maintenance_lookup_JSP="Team_member_maintenance_lookup";
	public static final String RequestMapping_Team_member_maintenancenew_JSP="Team_member_maintenancenew";
	public static final String RequestMapping_Team_member_maintenance_JSP="Team_member_maintenance";
	public static final String TeamMemebers_RestCheckTeammember="/RestCheckTemmmember"; 
	public static final String TeamMemebers_RestCheckEmail="/RestCheckEmail"; 
	public static final String TeamMemebers_RestCheckZipCode="/RestCheckZipCode"; 
	public static final String TeamMemebers_RestCheckAddress="/RestCheckAddress"; 
	public static final String TeamMemberOBJ="TeamMemberObj";
	public static final String TeamMemberTenant="TeamMemberTenant";
	public static final String TenantLanguage="TenantLanguage";
	public static final String TeamMemberFulfilmentCenter="TeamMemberFulfilmentCenter";
	public static final String TeamMemberEmail="TeamMemberEmail";
	public static final String TeamMemberAddress1="TeamMemberAddress1";
	public static final String TeamMemberAddress2="TeamMemberAddress2";
	public static final String TeamMemberAddress3="TeamMemberAddress3";
	public static final String TeamMemberAddress4="TeamMemberAddress4";
	public static final String TeamMemberCity="TeamMemberCity";
	public static final String TeamMemberStateCode="TeamMemberStateCode";
	public static final String TeamMemberCountryCode="TeamMemberCountryCode";
	public static final String TeamMemberZipCode="TeamMemberZipCode";
	public static final String TeamMemebers_Restfull_checkTeamMember="/RestCheckTeamMember";
	public static String Date_Format="yyyy-MM-dd HH:mm:ss";
	public static final String yyyyMMdd_Format="yy-MM-dd";
	public static final String yyyyMMdd_Format_v1="yyyy-MM-dd";
	public static final String yyyy_MM_dd_Format="yyyy-MM-dd";
	public static final String dd_MMM_yyyy="dd-MMM-YY";
	/**
	 * Constant for Security Setup screen
	 * 
	 */
	public static final String RequestMapping_Teammember_lookup_Action="/Team_member_lookup";
	public static final String RequestMapping_Teammember_lookup_Page="Team_member_lookup";
	public static final String RequestMapping_Teammember_selection_Action="/Team_member_selection";
	public static final String RequestMapping_Teammember_selection_Page="Team_member_selection";
	public static final String RequestMapping_Teammember_security_setup_maintenance_Action="/Security_setup_maintenance";
	public static final String RequestMapping_Teammember_security_setup_maintenance_Page="Security_setup_maintenance"; 
	public static final String SecurityAuthorizationsListObject="SecurityAuthorizationsListObject";
	//public static final String GeneralCode_SequrityQuestions="SECURITYQUESTION";
	public static final String Security_SingleQuote="'";
	public static final String SingleQuote="'";
	public static final String Security_UserInactiveStatus="X";
	public static final String Security_UserTemporaryStatus="T";
	public static final String Security_WebServiceUserUpdatedMessage="User Updated";
	public static final String Security_WebServiceSuccessStatus="Success";
	public static final String Security_WebServiceErrorStatus="Error";
	public static final String Security_WebServiceUserIsInactiveStatus="UserIsInactive";
	public static final String Security_WebServiceUserNotExistsMessage="User Not Exists";
	public static final String Security_M_Team_member_lookup="/Team_member_lookup";
	public static final String Security_M_notAccess="/notAccess";
	public static final String Security_M_AddUser="/AddUser";
	public static final String Security_M_AddUpdateTeamMember="/AddUpdateTeamMember";
	public static final String Security_M_Team_member_selection="/Team_member_selection";
	public static final String Security_M_Menu="/Menu";
	public static final String Security_M_DisplayAll="/DisplayAll";
	public static final String Security_M_getSequrityQuestions="/getSequrityQuestions";
	public static final String Security_M_checkUserAvailability="/checkUserAvailability";
	public static final String Security_M_getSecuritySetUpScreenLabel="/getSecuritySetUpScreenLabel";
	public static final String Security_M_deleteFromSEQ="/deleteFromSEQ";
	public static final String Security_M_getAllTeamMembers="/getAllTeamMembers";
	public static final String Security_M_getLoginUserDetails="/getLoginUserDetails";
	public static final String Security_M_getTeamMemberEmail="/getTeamMemberEmail";
	public static final String Security_ModelAttributeSecurity="Security";
	public static final String Security_R_P_SecurityQuestion1="SecurityQuestion1";
	public static final String Security_R_P_SecurityQuestion2="SecurityQuestion2";
	public static final String Security_R_P_SecurityQuestion3="SecurityQuestion3";
	public static final String Security_R_P_TeamMember="TeamMember";
	public static final String Security_R_P_Tenant="Tenant";
	public static final String Security_R_P_TeamMemberName="TeamMemberName";//TeamMemberName
	public static final String Security_R_P_PartOfTeamMemberName="PartOfTeamMemberName";//TeamMemberName
	public static final String Security_R_P_GeneralCode="GeneralCode";//TeamMemberName
	public static final String Security_R_P_UserName="UserName";//TeamMemberName
	public static final String Security_R_P_LanguageCode="LanguageCode";//TeamMemberName
	public static final String Security_R_P_FullfillmentCenter="FullfillmentCenter";//TeamMemberName
	public static final String Security_StringTrue="true";
	public static final String Security_StringFalse="false";
	public static final String Security_V_obj_Status="Status";	
	public static final String Security_V_obj_Message="Message";
	public static final String Security_V_obj_viewLabels="viewLabels";	
	public static final String Security_V_obj_viewCompanyID="viewCompanyID";
	public static final String Security_V_obj_viewLoginUserName="viewLoginUserName";
	public static final String Security_V_obj_viewIsUserExists="viewIsUserExists";
	public static final String Security_V_obj_KendoReadURL="KendoReadURL";
	public static final String Security_V_obj_lblHidden="lblHidden";
	public static final String Security_V_obj_viewTenant="viewTenant";
	public static final String inline="inline";
	public static final String none="none";
	public static final String Security_NotaccessMessage="Please request Seq officer for getting access of this page.";
	public static final String Security_WebServiceNotAbleToDeleteMessage="Not Able To delete";
	public static final String EnterYourEmailAgain= "Please enter your mail again.";
	public static final String Security_TeamMember_Active_Status="A";
	public static final String Security_TeamMember_InActive_Status="I";
	public static final String Security_TeamMember_LeaveOfAbsence_Status="L";
	public static final String Security_TeamMember_Delete_Status="X";
	public static final String Security_TeamMember_Active_Status_Text="Active";
	public static final String Security_TeamMember_InActive_Status_Text="Not Active";
	public static final String Security_TeamMember_LeaveOfAbsence_Text="Leave of Absence";
	public static final String Security_TeamMember_Delete_Status_Text="Deleted";
	public static final String Status_Active="A";
	public static final String Status_NotActive="I";
	public static final String Status_InActive="I";
	public static final String Status_Delete="X";
	public static final String Status_SQRTActive="A";
	public static final String Status_LeavOfabsense="L";
	public static final String Status_Leav="L";
	public static final String SetupDateObj="SetupDateObj";
	public static final String LastActivityDateObj="LastActivityDateObj";
	public static final String StartDateObj="StartDateObj";
	public static final String LastDateObj="LastDateObj";

	/**
	 * Constant for Security Setup Mail Configuration
	 * 
	 */
	public static final String Security_MailSubject="Temporary Email" ;
	public static final String Security_MailPart1="Welcome " ;
	public static final String Security_MailPart2=",<br><br>UserID:";
	public static final String Security_MailPart3="<br><br>Temporary Password:";
	public static final String Security_MailPart4="<br><br>Please see your temporary password, if you have any issues signing on please contact Your Security Offices at ";
	public static final String Security_MailPart5=" Or Email ";
	public static final String Security_MailPart6=" .<br> After logging in with your temporary password, you will be required to change your password to something else that you can remember.<br><br>Sincerely,<br>Security Officer";
	public static final String Forgot_MailPart6=" .<br> After logging in with your temporary password, you will be required to change your password to something else that you can remember.<br><br>Regards,<br>JASCI Support Team";
	/**
	 * Constant Forgot Password Mail Configuration
	 * 
	 */
	public static final String Forget_MailSubject="Reset Password" ;
	public static final String Forget_MailPart1="Dear " ;
	public static final String Forget_MailPart2=",\n\nUserID:";
	public static final String Forget_MailPart3="\n\nYour temporary password is ";
	public static final String Forget_MailPart4="\n\n<u><a href = \"http://216.49.149.88:9015/JASCI/loginForm\">CLICK HERE TO LOGIN AND SET YOUR NEW PASSWORD</a></u> \n\nRegards,\nJASCI Support Team";
	/** 
	 * Email Parameter
	 * 
	 */
	//public static final String ConfigFilePathWithName = "config.properties";
	public static final String SecurityOfficerContactNumber = "SecurityOfficerContactNumber";
	public static final String SecurityOfficerEmailId = "SecurityOfficerEmailId";
	public static final String GeneralCodes_SystemUse_N="N";
	public static final String Date_Format_yyyy_MM_dd="yyyy-MM-dd";
	public static final String TeamMemberMaintenance_GeneralCodeId_Department="TeamMemberMaintenance_GeneralCodeId_Department";
	public static final String TeamMemberMaintenance_GeneralCodeId_Shift="TeamMemberMaintenance_GeneralCodeId_Shift";
	public static final String TeamMemberMaintenance_GeneralCodeId_State="TeamMemberMaintenance_GeneralCodeId_State";
	public static final String TeamMemberMaintenance_GeneralCodeId_Language="TeamMemberMaintenance_GeneralCodeId_Language";
	public static final String TeamMemberMaintenance_GeneralCodeId_CountryCode="TeamMemberMaintenance_GeneralCodeId_CountryCode";

	public static final String RequestMapping_Logout_Action="/LogoutPage";
	public static final String ColumnName_CompanyLogo="CompanyLogo";
	public static final String ColumnName_Help="Help";
	public static final String ColumnName_Profile="PROFILE";
	public static final String ColumnName_APPICON="APPICON";
	public static final String ColumnName_APPICONADDRESS="APPICONADDRESS";
	public static final String ColumnName_PurchaseOrderApproval="PurchaseOrderApproval";
	public static final String ColumnName_TRANSLATION="TRANSLATION";
	public static final String DOUBLEHASH="##";
	public static final String RequestMapping_menu_profile_assignments_Action="menu_profile_assignment";
	public static final String RequestMapping_MenuExecutionScreen_ResponseObject="objTeamMemberCompanies";
	public static final String RequestMapping_MenuExecutionScreen_Action="/MenuExecutionForm";
	public static final String RequestMapping_MenuExecutionView_Action="Menu_execution_view";
	public static final String RequestMapping_MenuAssignmentView_Action="/MenuAssignment";
	public static final String RequestMapping_SubMenuScreen_Action="Sub_Menu";
	public static final String RequestMapping_SubMenu_Action="/SubMenuForm";
	public static final String RequestParameter_MenuProfile="MenuProfile";
	public static final String RequestMapping_MenuAssignmentView_RestFullService_Action="/MenuKendoList";
	public static final String MenuAssignmentView_MappingObject="AssignedMenuList";
	public static final String SubMenuHashMap_MappingObject="ObjHashMap";
	public static final String MenuAssignmentView_CompanyLogoURL="CompanyLogoURL";
	public static final String MenuAssignmentView_Key_TeamMemberName="TeamMemberName";
	public static final String MenuAssignmentView_ObjMenuExecution="MenuExecution";
	public static final String MenuAssignmentView_Key_CompanyName="CompanyName";
	public static final String STR_A="A";
	public static final String STR_X="X";
	public static final String RESTSERVICE_MenuMessage_Action="/MenuMessages";
	public static final String RESTSERVICE_TeamMemberMessage_Action="/TeamMemberMessages";
	public static final String RequestMapping_MenuExecutionLabel_Action="/MenuExecutionLabel";
	public static final String ObjCommonSession = "ObjCommonSession";

	public static final String RequestMapping_Company_maintenancenew_Action="/Company_maintenancenew";
	public static final String RequestMapping_Company_Insertnew_Action="/Company_maintenancenew";
	public static final String RequestMapping_Company_Lookup="/Company_Lookup";
	public static final String RequestMapping_Company_search_lookup_Action="/Company_search_lookup";
	public static final String RequestMapping_Company_Delete_search_lookup_Action="/Company_search_lookup_delete";
	public static final String RequestMapping_Company_maintenanceUpdate_Action="/Company_maintenance_update";
	public static final String RequestMapping_Company_maintenanceUpdate_Page="Company_maintenance_update";
	public static final String RequestMapping_Company_AfterUpdate_Action="/Company_search_lookup_after_update";
	public static final String RequestMapping_Company_AfterNotUpdate_Action="/Company_search_lookup_after_notupdate";
	public static final String RequestMapping_Company_UpdateTeamMember_searchlookup_Action="/Company_search_lookup_update";
	public static final String Company_Kendo_CompanyRead="/Company_search_lookup/Grid/read";//"/GeneralCodeList/Grid/read";
	public static final String Company_Kendo_CompanyRead_Sort="/Company_search_lookup_sort/Grid/read";//"/GeneralCodeList/Grid/read";
	public static final String RequestMapping_Company_maintenance_Action_lokup="/Company_maintenance_lookup";
	/**
	 * Constant for info help
	 * 
	 */
	public static final String StatusFlase="False";
	public static final String StatusTrue="True";
	public static final String InfoHelps_SearchQuery="InfoHelps_SearchQuery";	
	public static final String InfoHelps_GetAllDataQuery="InfoHelps_GetAllDataQuery";					
	public static final String RequestMapping_Infohelp_AssignmentSearch="/InfoHelpAssignmentSearch";
	public static final String InfoHelps_ModelAttribute="Infohelps";
	public static final String InfoHelps_SelectHelpTypeQuery="InfoHelps_SelectHelpTypeQuery";
	public static final String InfoHelps_RequestAttribute_EnterInfoHelp="EnterInfoHelp";
	public static final String InfoHelps_RequestAttribute_PartoftheDescription="PartoftheDescription";
	public static final String InfoHelps_RequestAttribute_InfoHelpType="InfoHelpType";
	public static final String InfoHelps_Language="ENG";
	public static final String RequestMapping_Infohelp_SearchLookUp="/InfoHelpSearchlookup";
	public static final String RequestMapping_InfoHelps_Kendo_DisplayAll="/InfoHelps_Kendo_DisplayAll";
	public static final String RequestMapping_InfoHelps_assignment_maintenance="/InfoHelpassignmentmaintenance_update";
	public static final String RequestMapping_InfoHelps_assignment_maintenance_add="/InfoHelpassignmentmaintenance_add";
	public static final String RequestMapping_InfoHelps_assignment_maintenance_add_Page="InfoHelpassignmentmaintenance_add";
	public static final String InfoHelps_QueryNameGetAllList="InfoHelps_GetAllList";
	//public static final String InfoHelps_QueryNameSelectInfoHelpType="SelectInfoHelpType";
	public static final String InfoHelp_Restfull_RestInfohelpListAllCheck="/RestInfohelpListAllCheck";
	public static final String InfoHelps_Restfull_StrLastActivityBy="LastActivityBy";
	public static final String InfoHelps_Restfull_StrDescription20="Description20";
	public static final String InfoHelps_Restfull_StrDescription50="Description50";
	public static final String InfoHelps_Restfull_StrExecutionPath="ExecutionPath";
	public static final String InfoHelp_DataBaseException="DataBase Exception";
	public static final String InfoHelp_InfoHelpsObject="InfoHelpsObject";
	public static final String InfoHelp_Restfull_CheckInfoHelp="/RestInfohelpCheck";			
	public static final String InfoHelp_Restfull_InsertInfoHelp="/RestInsertInfoHelp";		
	public static final String InfoHelp_Restfull_RestInfohelpListAll="/RestInfohelpListAll";
	public static final String InfoHelp_Restfull_RestSelectInfoHelpType="/RestSelectInfoHelpType";
	public static final String InfoHelp_Restfull_InfoHelpsListFetch="/InfoHelpsListFetch";
	public static final String InfoHelps_Restfull_StrInfoHelp="InfoHelp";
	public static final String InfoHelps_Restfull_StrPartoftheDescription="Description";
	public static final String InfoHelps_Restfull_StrInfoHelpType="InfoHelpType";
	public static final String InfoHelps_Restfull_StrLanguage="Language";
	public static final String StrGetInfoHelpValue="GetInfoHelpValue";
	public static final String RequestMapping_InfoHelps_Update="/InfoHelpUpdate";
	public static final String RequestMapping_InfoHelps_ObjectInfohelps="InfoHelpsObject";
	public static final String GeneralCodes_Restfull_UpdateInfoHelps="/UpdateInfoHelps";
	public static final String RequestMapping_InfoHelps_assignment_Edit="/InfoHelpassignmentEdit";
	public static final String RequestMapping_InfoHelps_Delete="/InfoHelpsdelete";
	public static final String RequestMapping_InfoHelps_Kindo_Delete="/KindoInfoHelpsdelete";
	public static final String Restfull_InfoHelps_Delete="/RestFullInfoHelpsdelete";		
	public static final String InfoHelps_QueryNameDeleteInfoHelp="DeleteInfoHelp";
	public static final String InfoHelps_ResultSetSelectInfoHelpType="ResultSetSelectInfoHelpType";
	public static final String InfoHelps_ColumnResultInfohelps="DESCRIPTION20";
	/*public static final String InfoHelp_GeneralCodeID_InfoHelp="INFOHELP";
	public static final String InfoHelp_GeneralCodeID_Language="LANGUAGES";*/
	public static final String InfoHelp_Restfull_RestSelectLanguage="/RestSelectLanguage";			
	//public static final String InfoHelps_QueryNameLanguage="Language";
	public static final String InfoHelps_ResultSetLanguage="ResultSetLanguage";
	public static final String InfoHelps_ColumnGeneralCodeID="DESCRIPTION20";
	public static final String InfoHelps_Combobox_SelectInfoHelpType="SelectInfoHelpType";
	public static final String InfoHelps_Combobox_SelectLanguage="SelectLanguage";
	public static final String InfoHelps_TeamMember="TeamMember";
	public static final String InfoHelps_CurrentDate="CurrentDate";
	public static final String InfoHelps_Restfull_RestInfoHelpAssignmentSearchScreenlabel="/RestInfoHelpAssignmentSearchScreenlabel";
	public static final String InfoHelps_Restfull_RestInfoHelpAssignmentMaintenanceScreenlabel="/RestInfoHelpAssignmentMaintenanceScreenlabel";
	public static final String InfoHelps_Restfull_RestInfoHelpAssignmentSearchLookupScreenlabel="/RestInfoHelpAssignmentSearchLookupScreenlabel";
	public static final String Infohelps_Restfull_ScreenName="InfoHelpsScreen";
	public static final String InfoHelps_Restfull_Language="language";
	public static final String InfoHelpAssignmentSearch_ScreenName="InfoHelpAssignmentSearch";
	public static final String InfoHelpSearchlookup_ScreenName="InfoHelpSearchlookup";
	public static final String InfoHelpassignmentmaintenance_ScreenName="InfoHelpassignmentmaintenance";
	public static final String Infohelp_ScreenLabels="Infohelp_ScreenLabels";
	public static final String CurrentDateFormat="yy-MM-dd";
	public static final String InfoHelps_yyyyMMdd_Format="yyyy-MM-dd";
	/**
	 * Constant for menu messages
	 */
	public static final String MenuMessage_QueryNameGetAllList="MenuMessageAllList";
	public static final String RequestMapping_MenuMessageSearchlookUp="/Menu_messages_search_lookup";
	public static final String RequestMapping_Menumessagesmaintenance="/Menu_messages_maintenance_insert";
	public static final String RequestMapping_Menumessagesmaintenance_Update="/Menu_messages_maintenance_update";
	public static final String Object_Menumessagesmaintenance_Update="Menu_messages_maintenance_update";
	public static final String MenuMessages_Kendo_ReadAllMessages="/MenuMessagesreadAllMessages";
	public static final String MenuMessages_Restfull_AllMessages="/RestfullAllMessages";
	public static final String MenuMessages_Restfull_Tenant_ID="Tenant_ID";
	public static final String MenuMessages_Restfull_Message_ID="Message_ID";
	public static final String MenuMessages_Restfull_Ticket_Type_Message="TICKET_TAPE_MESSAGE";
	public static final String MenuMessages_Restfull_Message1="Message1";
	public static final String MenuMessages_Restfull_Company_ID="Company_ID";
	public static final String MenuMessages_Restfull_Status="Status";
	public static final String MenuMessages_Restfull_TeamMember_ID="Team_Member_ID";
	public static final String MenuMessages_Status="A";
	public static final String temp_MenuMessages_Tenant_ID="43310ASP-D6D4-4715-8762-6DCBA01010A";
	public static final String temp_MenuMessages_Company_ID="NGI";
	public static final String TicketTypeMenuMessage="Ticket";
	public static final String TypeMenuMessage="Message";
	public static final String jasci_Exception="Internal Error";
	public static final String RequestMapping_MenuMessages_Edit="MenuMessageEdit";
	public static final String ObjectMenuMessages="ObjectMenuMessages";
	public static final String MenuMessages_Restfull_MenuMessagesListFetch="/MenuMessagesRestfullMenuMessagesListFetch";
	public static final String MenuMessages_Restfull_Type="type";
	public static final String RequestMapping_MenuMessage_Update="/MenuMessageUpdate";
	public static final String MenuMessage_Restfull_RestMenuMessageUpdate="/RestMenuMessageUpdate";
	public static final String MenuMessage_Restfull_RestMenuMessageAdd="/RestMenuMessageAdd";
	public static final String MenuMessage_yyyyMMdd_Format="yyyy-MM-dd";
	public static final String MenuMessage_yyyyMMdd_Format1="yy-MM-dd";
	public static final String MenuMessage_Kendo_Delete="/MenuMessageDelete";
	public static final String MenuMessages_Kendo_ReadCompanyList="/TeamMemberCompanyList";
	public static final String MenuMessages_Restfull_ReadCompanyList="/RestTeamMemberCompanyList";
	public static final String MenuMessage_ColumnMessage_ID="Message_ID";
	public static final String MenuMessage_ColumnTenant_ID="Tenant_ID";
	public static final String MenuMessage_ColumnCompany_ID="Company_ID";
	public static final String MenuMessage_ColumnStatus="Status";
	public static final String MenuMessage_ColumnSTART_DATE="START_DATE";
	public static final String MenuMessage_ColumnEND_DATE="END_DATE";
	public static final String MenuMessage_ColumnTICKET_TAPE_MESSAGE="TICKET_TAPE_MESSAGE";
	public static final String MenuMessage_ColumnMESSAGE="MESSAGE";
	public static final String MenuMessage_ColumnName50="Name50";
	public static final String MenuMessage_ResultSetCompnayList="ResultSetCompnayList";
	public static final String MenuMessage_CompanyListCheckController="/CompanyListCheckController";
	public static final String MenuMessage_CompanyList="CompanyList";
	public static final String OpenBracket="[";
	public static final String CloseBracket="]";
	public static final String BackSpaceBracket="\"";
	public static final String Comma=",";
	public static final String InfoHelp_Restfull_CheckTicketTypeMessage="/CheckTicketTypeMessage";
	public static final String MenuMessages_Restfull_RestMenuMessageDelete="/RestMenuMessageDelete";
	public static final String MenuMessages_Result_Status="True";
	public static final String MenuMessages_Restfull_RestScreenlabel="/RestfullRestScreenlabel";
	public static final String MenuMessages_Language="ENG";
	public static final String MenuMessages_ScreenLabels="ScreenLabels";
	/**
	 * General Code execution path
	 * 
	 */
	public static final String RequestMapping_GeneralCodes_GeneralCodeList_Execution="GeneralCodeList";
	public static final String RequestMapping_GeneralCodes_GeneralCodeIdentification_Execution="General_code_identificationdata?MenuValue=";
	public static final String RequestMapping_Teammember_lookup_Execution="Team_member_lookup";
	public static final String RequestMapping_Team_member_maintenance_Action_Execution="Team_member_maintenance";
	public static final String RequestMapping_Infohelp_AssignmentSearch_Execution="InfoHelpAssignmentSearch";
	public static final String RequestMapping_Menu_Option_Maintenmence_Execution="MenuMaintenance";
	public static final String RequestMapping_Menu_APPICON_Maintenmence_Execution="Menu_app_icon_lookup";
	public static final String RequestMapping_Menu_Message_Maintenance_Execution="Menu_messages_search_lookup";
	public static final String RequestMapping_Language_Translations_Execution="Language_Translations_Lookup";
	public static final String RequestMapping_Teammember_Message_Maintenance_Execution="TeamMemberMessagesMaintenance";
	public static final String RequestMapping_Company_Maintenmence_Execution="Company_lookup";
	public static final String RequestMapping_LocationLabelPrint_Maintenmence_Execution="Location_label_print";
	public static final String RequestMapping_FullfillmentCenter_lookup_Execution="fullfillmentCenterLookup";
	public static final String RequestMapping_Note_lookup_Execution = "Notes_Lookup";
	public static final String RequestMapping_Wizard_Location_maintenance_Execution="Location_Wizard_Lookup";
	public static final String RequestMapping_Wizard_Location_maintenance_Search_Execution="Location_Maintenance_Search";
	public static final String RequestMapping_Wizard_Location_Profile_maintenance_Execution="Location_Profile_Maintenance_Search";
	/**
	 * Constant for menu option 
	 * 
	 */
	public static final String MENUOPTION_Application="Application";
	public static final String MENUOPTION_MenuOption="MenuOption";
	public static final String MENUOPTION_MenuType="MenuType";
	public static final String MENUOPTION_Description20="Description20";
	public static final String MENUOPTION_Description50="Description50";
	public static final String MENUOPTION_Tenant="Tenant";
	public static final String MENUOPTION_lblApplication="lblApplication";
	public static final String MENUOPTION_lblMenuOption="lblMenuOption";
	public static final String MENUOPTION_lblMenuType="lblMenuType";
	public static final String MENUOPTION_lblPartMenuType="lblPartMenuType";
	public static final String MENUOPTION_lblPartMenuOption="lblPartMenuOption";
	public static final String MENUOPTION_lblPartApplication="lblPartApplication";
	public static final String MENUOPTION_lblPartDescription20="lblPartDescription20";
	public static final String MENUOPTION_lblPartDescription50="lblPartDescription50";
	public static final String MENUOPTION_txtTenant="txtTenant";
	public static final String MenuOptionInvalidID="MenuOptionInvalidId";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceLookUp="/MenuMaintenance";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaitenanceSearchLookUp="/MenuMaintenanceSearchLookUp";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaitenanceSearchLookUpViaDelete="/MenuMaintenanceSearchLookUp_delete";
	public static final String MenuOption_Company="company";
	public static final String ResultSetMapping_MenuOptions_GeneralCode_MenuType="MenuTypesForDropDown";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceLookUpScreen="Menu_options_maintenance_lookup";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceLookUp_ObjectList="ObjMenuTypesOption";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceSearchLookUpScreen="Menu_option_searchlookup";
	public static final String Forword_SLASH="/";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreen_Action="/Menu_Maintenance_Screen";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreen="Menu_options_maintenance";
	public static final String REQUESTMAPPING_MENUOPTION_Maintenance_ListPojoObject="MenuOption_Update_Object";
	public static final String REQUESTMAPPING_MENUOPTION_Maintenance_MenuType="menuTypeOption";
	public static final String REQUESTMAPPING_MENUOPTION_Maintenance_SearchLookUp="Screen_lookup";
	public static final String REQUESTMAPPING_MENUOPTION_Maintenance_MapApplication="MapApplication";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreenNew="Menu_option_Maintenancenew";
	public static final String REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreenNew_Action="/Menu_option_Maintenancenew";
	public static final String REQUESTMAPPING_MENUOPTION_Maintenance_SearchScreen="SearchScreen";
	public static final String REQUESTMAPPING_MENUOPTION_Maintenance_LookUpScreen="LookUpScreen";
	public static final String REQUESTMAPPING_MENUOPTION_MaintenanceUpdate_Action="/Menu_Maintenance_Screen_Update";
	public static final String REQUESTMAPPING_MENUOPTION_MaintenanceUpdate_AfterSearchAction="/Menu_Maintenance_searchScreenAfter";
	public static final String NativeQueryName_MenuOptions_GeneralCode_ApplicationQueryName="ApplicationGeneralCode";
	public static final String Parameter_MenuOption_Tenant="tenant";
	public static final String REQUESTMAPPING_MENUOPTION_delete_controller_action="/MenuOption_searchlookup_delete";
	public static final String MenuOptions_Restfull_SearchFieldValue="MenuOptionSearchFieldValue";
	public static final String MenuOptions_Restfull_SearchField="MenuOptionFieldSearch";
	public static final String REQUESTMAPPING_MENUOPTION_RestFull_Kendo="/RestFull_SearchLookUp";
	public static final String REQUESTMAPPING_MENUOPTION_RestFull_DropDown="/RestFull_AddValueDropDown";
	public static final String REQUESTMAPPING_MENUOPTION_RestFull_Check_MenuOption="/RestFull_Check_MenuOption";
	public static final String REQUESTMAPPING_MENUOPTION_RestFull_Check_MenuOption_Profile="/Check_MenuOption";
	public static final String REQUESTMAPPING_MENUOPTION_RestFull_MenuOption_searchlookup="/MenuOption_searchlookup";
	public static final String REQUESTMAPPING_MENUOPTION_Parameter_MenuOptionValue="MenuOptionValue";
	public static final String REQUESTMAPPING_MENUOPTION_Parameter_valApplication="ValApplication";
	public static final String INPUT_PARAMETER_LANGUAGE="language";
	public static final String MappingObj_ScreenLBl="objScreenLabel";
	public static final String objMenuOptionLabel="objMenuOptionLabel";
	/**Menu AppIcon Constant*/
	public static final String RequestMapping_MenuAppIcon_LookupScreen_Action="/Menu_app_icon_lookup";
	public static final String RequestMapping_MenuAppIcon_LookupScreen_Page="Menu_app_icon_lookup";	
	public static final String RequestMapping_MenuAppIcon_Maintenance_New_Screen_Action="/Menu_app_icon_maintenance_new";
	public static final String RequestMapping_MenuAppIcon_Maintenance_Delete_Screen_Action="/Menu_app_icon_maintenance_delete";
	public static final String RequestMapping_MenuAppIcon_Maintenance_Update_Screen_Page="Menu_app_icon_maintenance_update";
	public static final String RequestMapping_MenuAppIcon_Maintenance_Screen_Page="Menu_app_icon_maintenance_new";	
	public static final String RequestMapping_MenuAppIcon_SearchLookup_Screen_Action="/Menu_app_icon_search_lookup";
	public static final String RequestMapping_MenuAppIcon_SearchLookup_By_Part_Screen_Action="/Menu_app_icon_search_by_part_lookup";
	public static final String RequestMapping_MenuAppIcon_SearchLookup_Screen_Page="Menu_app_icon_search_lookup";
	public static final String RequestMapping_MenuAppIcon_AddOrUpdate_Action="/Menuappicon_addorupdate";
	public static final String RequestMapping_MenuAppIcon_AddOrUpdate_Request_Action="/Menuappicon_addorupdate_request";
	public static final String RequestMapping_MenuAppIcon_Update_Request_From_Lookup_Action="/Menuappicon_addorupdate_lookup_request";
	public static final String MenuAppIcons_Restfull_SetScreenLanguage="/RestGetMenuAppIconsSetScreenLanguage";
	public static final String MenuAppIcons_Restfull_AddOrUpdate="/RestGetMenuAppIconsAddOrUpdate";
	public static final String MenuAppIcons_Restfull_GetMenuAppIconsList="/RestGetMenuAppIconsList";
	public static final String MenuAppIcons_Restfull_GetTeamMember="/RestGetTeamMember";
	public static final String MenuAppIcons_Restfull_GetMenuAppIconsListByAppIcon="/RestGetMenuAppIconsListByAppIcon";
	public static final String MenuAppIcons_Restfull_GetMenuAppIconsListByApplication="/RestGetMenuAppIconsListByApplication";
	public static final String MenuAppIcons_Restfull_checkIcon="/RestCheckIcon";
	public static final String MenuAppIcons_Restfull_checkAppIcon="/RestCheckAppIcon";
	public static final String MenuAppIcons_Restfull_UploadIcon="/RestCheckUploadIcon";
	public static final String MenuAppIcons_Restfull_GetApplication="/RestGetApplication";
	public static final String MenuAppIcons_Restfull_Delete_Application="/RestGetDeleteApplication";
	public static final String MenuAppIcons_Restfull_UploadImage="/uploadimage";
	public static final String MenuAppIcons_Restfull_Tenant="Tenant";
	public static final String MenuAppIcons_Restfull_Company="Company";
	public static final String MenuAppIcons_Restfull_AppIcon="StrAppIcon";
	public static final String MenuAppIcons_Restfull_AppLication="StrAppLication";
	public static final String MenuAppIcons_Restfull_GeneralCodeId="StrGeneralCodeId";
	public static final String MenuAppIcons_Restfull_AppIconName="StrAppIconName";
	public static final String MenuAppIcons_Restfull_AppIconPart="StrAppIconPart";
	public static final String MenuAppIcons_Restfull_SearchBy="StrSearchBy";
	public static final String QUESTION_MARK="?";
	public static final String MenuAppIcons_Restfull_ObjMenuAppIcons="ObjMenuAppIcons";
	public static final String Param_Languages_MenuAppIconMaintenance_ObjectCode="MenuAppIconMaintenance";
	public static final String MenuAppIconMaintenanceSceernLabelObj="MenuAppIconMaintenanceSceernLabelObj";
	public static final String GeneralCodeApplicationListObject="GeneralCodeApplicationListObject";
	public static final String StrMenuTypeFullScreen="FullScreen";
	public static final String StrMenuOptionMenuAppIcon="Menu App Icon";
	public static final String MenuAppIcon_Register_ModelAttribute="MENUAPPICON_REGISTER";
	public static final String MenuAppIcon_AppIcon="AppIcon";
	public static final String MenuAppIcon_Application="Application";
	public static final String MenuAppIcon_AppIconName="AppIconName";
	public static final String MenuAppIcon_AppIconPart="AppIconPart";
	public static final String MenuAppIcon_DescriptionShort="DescriptionShort";
	public static final String MenuAppIcon_DescriptionLong="DescriptionLong";
	public static final String MenuAppIcon_IconAddress="iconAddress";
	public static final String MenuAppIcon_ApplicationId="ApplicationId";
	public static final String MenuAppIcon_KendoUrl="KendoUrl";
	public static final String MenuAppIcon_Format_LastActivityDate="LastActivityDate";
	public static final String MenuAppIcon_List_Object="MenuAppIconListObj";
	public static final String MenuAppIcon_Kendo_Read_URL="KendoReadUrl";
	public static final String MenuAppIcon_Kendo_Read_AppIconName="AppIconName";
	public static final String MenuAppIcon_Kendo_Read_AppIconPart="AppIconPart";
	public static final String MenuAppIcon_Kendo_Read_Description="Description";
	public static final String MenuAppIcon_FlagForReturn="FlagForReturn";
	public static final String MenuAppIcon_FlagForReturnObj="FlagForReturnObj";
	public static final String MenuAppIcon_AppIconNameForCheck="AppIconNameForCheck";
	public static final String MenuAppIcon_FlagForReturn_SearchLookup="SearchLookup";
	public static final String MenuAppIcon_SearchLookup_BackScreen="BackScreen";
	public static final String MenuAppIcon_TeamMemberName="TeamMemberName";
	public static final String Forward_Slash="/";
	public static final String Forward_Double_Slash="://";
	public static final String DOT=".";
	public static final String ASTRIK="*";
	public static final String MenuAppIcon_AccessKey="accessKey";
	public static final String MenuAppIcon_SecrateKey="secretKey";
	public static final String MenuAppIcon_BucketName="menuAppIconBucketName";
	public static final String MenuAppIcon_ServerName="serverName";
	public static final String MenuAppIcon_UploadFilePath="UploadFilePath";
	public static final int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	public static final int MAX_FILE_SIZE = 1024 * 1024 * 140; // 140MB
	public static final int MAX_REQUEST_SIZE = 1024 * 1024 * 150; // 150MB
	public static final String UUID_STRING = "uuid";
	public static final String Access_Control_Allow_Origin="Access-Control-Allow-Origin";
	public static final String Access_Control_Allow_Methods="Access-Control-Allow-Methods";
	public static final String Access_Control_Allow_Headers="Access-Control-Allow-Headers";
	public static final String Access_Control_MaxAge="Access-Control-Max-Age";
	public static final String Access_Control_Allow_Headers_Content_Type="Content-Type";
	public static final String Access_Control_Allow_Max_Age_Value="86400";
	public static final String POST="POST";
	//public static final String Config_Property_File_Name="config.properties";
	public static final String RequestMapping_MenuAppIcon_Read_Kendo_UI="/Menuappicon_searchlookup/Grid/read";
	public static final String RequestMapping_MenuAppIcon_Read_Kendo_UI_By_Part="/Menuappicon_searchlookup/Grid/readbyPart";
	/** 
	 * Constant for Language and Team member messages 
	 * 
	 */
	public static final String RequestMapping_TeamMemberMessagesMaintenanceScreen_Action="/TeamMemberMessagesMaintenance";
	public static final String RequestMapping_TeamMemberMessagesScreen_MaintenancePage="Team_member_message_lookup";
	public static final String RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action="/TeamMemberMessagesMaintenanceNew";
	public static final String RequestMapping_TeamMemberMessagesScreen_MaintenanceNewPage="Team_member_message_maintenancenew";
	public static final String RequestMapping_TeamMemberMessagesMaintenanceEditScreen_Action="/TeamMemberMessagesMaintenanceEdit";
	public static final String RequestMapping_TeamMemberMessagesScreen_MaintenanceEditPage="Team_member_message_edit";
	public static final String RequestMapping_TeamMemberMessagesMaintenanceLookup_Action="/TeamMemberMessagesMaintenancelookup";
	public static final String RequestMapping_TeamMemberMessagesScreen_MaintenanceLookupPage="Team_member_message_search_lookup";
	public static final String RequestMapping_TeamMemberMessagesMaintenanceNewEditScreen_Action="/TeamMemberMessagesMaintenanceNew";
	public static final String RequestMapping_TeamMemberMessagesMaintenance_InsertRow="InsertRow";
	public static final String Param_Languages_TeamMemberMessagesMaintenanceScreen_ObjectCode="TeamMemberMessageMaintenanceScreen";
	public static final String RequestMapping_TeamMemberMessagesGetMessages_Action= "TeamMemberMessages_Get/{TeamMember}";
	public static final String RequestMapping_TeamMemberMessagesDeleteMessages_Action="Teammember_Message_delete";
	public static final String RequestMapping_TeamMemberMessages_RedirectToActiveMessagesPage="/TeamMemberMessages_Get/";
	public static final String RequestMapping_TeamMemberMessagesGetToEditMessages_Action="Teammember_Message_Edit"; 
	public static final String RequestMapping_TeamMemberMessagesEditMessages_Action="/TeamMemberMessagesMaintenanceUpdate"; 
	public static final String RequestMapping_TeamMemberMessagesScreen_ActiveMessagesPage="Team_member_message_active";
	public static final String RequestMapping_TeamMemberMessagesScreen_ActiveMessagesAction="/ActiveTeamMemberMessages";
	public static final String TeamMemberList="TeamMemberList";
	public static final String TeamMemberMessagesList="TeamMemberMessagesList";
	public static final String Param_TeamMemberMessage_TeamMember="TeamMember";
	public static final String Param_TeamMemberMessage_PartTeamMember="PartTeamMember";
	public static final String Param_TeamMemberMessage_Department="Department";
	public static final String Param_TeamMemberMessage_Shift="Shift";
	public static final String TicketTypeTeamMessage="TicketTape";
	public static final String TypeTeamMessage="Message";
	public static final String ViewLabels = "ViewLabels";
	public static final String TeamMember="TeamMember";
	public static final String Type="Type";
	public static final String TMMCompany="Company";
	public static final String MessageNumber="MessageNumber";
	public static final String TeamMemberMessagesMaintenance_ScreenName="TeamMemberMessagesMaintenance";
	public static final String TeamMemberMessageStatus = "A";
	public static final String InvalidTeammemberMessage="InvalidTeammemberMessage";
	public static final String InvalidTeamMemberNameMessage="InvalidTeamMemberNameMessage";
	public static final String InvalidDepartmentMessage="InvalidDepartmentMessage";
	public static final String InvalidShiftMessage="InvalidShiftMessage";
	public static final String BracketLeft="[";
	public static final String BracketRight="]";
	public static final String Coma=",";
	public static final String DoubleQuatos="\"";
	public static final String TeamMemberListParam="TeamMemberList";
	public static final String TeamMember_Kendo_TeamMembersRead="/Teammember_search/Grid/read";
	public static final String PartOfTeamMember="PartOfTeamMember";
	public static final String SavedMessage="Record has been saved successfully.";
	public static final String SavedMessageToPage="SavedSuccessfully";
	public static final String UpdatedMessageToPage="UpdatedSuccessfully";
	public static final String EditedMessage="Record has been updated successfully.";
	public static final String TeamMemberSearchErrorMessage="ErrorMessage";
	public static final String ValueDelete = "Delete";
	public static final String ValueUpdate = "Update";
	public static final String TransactionFail = "TransactionFail";

	public static final String TeamMemmberMessages_Rest_TeamMember="teamMember";
	public static final String TeamMemmberMessages_Rest_Company="company";
	public static final String TeamMemmberMessages_Rest_Tenant="tenant";
	public static final String TeamMemmberMessages_Rest_PartTeamMember="partTeamMember";
	public static final String TeamMemmberMessages_Rest_Department="department";
	public static final String TeamMemmberMessages_Rest_Shift="shift";
	public static final String TeamMemmberMessages_Rest_type="type";
	public static final String TeamMemmberMessages_Rest_FulfillmentCenter="fulfillmentCenter";
	public static final String TeamMemmberMessages_Rest_Language="language";
	public static final String TeamMemmberMessages_Rest_MessageNumber="messagenumber";
	public static final String TeamMemmberMessages_Rest_TeamMemberList="/RestTeamMemberMessageMembersList";
	public static final String TeamMemmberMessages_Rest_TeamMemberMessagesList1="/RestTeamMemberMessagesList1";
	public static final String TeamMemmberMessages_Rest_TeamMemberMessagesList="/RestTeamMemberMessagesList";
	public static final String TeamMemmberMessages_Rest_AddMessage="/RestTeamMemberMessagesAdd";
	public static final String TeamMemmberMessages_Rest_DeleteMessage="/RestTeamMemberMessagesDelete";
	public static final String TeamMemmberMessages_Rest_UpdateMessage="/RestTeamMemberMessagesUpdate";
	public static final String TeamMemmberMessages_Rest_GetTeamMemberMessage="/RestGetTeamMemberMessages";
	public static final String TeamMemmberMessages_Rest_ScreenLabels="/RestTMMMembersLabelsList";
	public static final String TeamMemmberMessages_Rest_ALLTeamMembers ="/controllerMethod";
	public static final String TeamMembersMessages_ResultDepartment="TeamMembersMessages_ResultDepartment";
	public static final String TeamMembersMessages_ResultMaxMessageNumber="TeamMembersMessages_ResultMaxMessageNumber";
	public static final String TeamMember_Kendo_TeamMembersToMessagesRead="/Teammember_Messages_search/Grid/read";
	public static final String Date_YYYY_MM_DD="yyyy-MM-dd";
	public static final String Date_YYYY_mm_DD="yyyy-mm-dd";
	public static final String ValidLanguage="Valid";
	public static final String InValidLanguage="InValid";
	public static final String NotExist="NotExist";
	public static final String AlreadyExist="AlreadyExist";
	public static final String SavedValue="Saved";
	public static final String LanguageTranslation_ScreenName = "Language_Translation";
	public static final String LanguageTranslation_GeneralCodeId_Languages="languages";
	public static final String MenuOption="Team Member";
	public static final String FullScreen="FullScreen";
	public static final String LanguageTranslation_Restfull_GetGeneralCodeLanguageList="/RestGetGeneralCodeLanguageList";
	public static final String LanguageTranslation_Restfull_CheckValidLanguage="/RestCheckValidLanguage";
	public static final String LanguageTranslation_Restfull_CheckValidKeyPhrase="/RestCheckValidKeyPhrase";
	public static final String LanguageTranslation_Restfull_CheckAlreadyExist="/RestAlreadyExist";
	public static final String Language_Translation_LanguageData_Read="Language_Translation_LanguageData/Grid/read";
	public static final String LanguageTranslation_Restfull_Tenant="Tenant";
	public static final String LanguageTranslation_Restfull_Company="Company";
	public static final String LanguageTranslation_Restfull_GeneralCodeId="GeneralCodeId";
	public static final String LanguageTranslation_Restfull_Language = "Language";
	public static final String LanguageTranslation_Restfull_KeyPhrase = "KeyPhrase";
	public static final String LanguageTranslation_LanguageList="LanguageList";
	public static final String LanguageTranslation_LanguageLabels="ViewLabels";
	public static final String Language_Translations_LanguageBe = "LanguageBe";
	public static final String Description = "Description";
	public static final String LanguageVal = "Language";
	public static final String KeyPhraseVal = "KeyPhrase";
	public static final String LanguageTranslation_DefaultLanguage="DefaultLanguage";
	public static final String DefaultLanguageValue="DefaultLanguageValue";


	public static final String Language_Translations_Lookup_Page= "Language_Translations_Lookup";
	public static final String Language_Translations_Lookup_Action= "/Language_Translations_Lookup";
	public static final String Language_Translations_Maintenance_Action="/Language_Translations_Maintenance";
	public static final String Language_Translations_Maintenance_Page="Language_Translations_Maintenance";
	public static final String Language_Translations_Search_Lookup_Page="Language_Translations_Search_Lookup";
	public static final String Language_Translations_Search_Lookup_Action="/Language_Translations_Search_Lookup";
	public static final String Language_Translations_Search_Lookup_ActionLanguage="/Language_Translations_Search_Lookup_action";
	public static final String Language_Translations_Search_Lookup_ActionInsert="/Language_Translation_New";
	public static final String Language_Translations_Add_Action="Language_Translation_New";
	public static final String Language_Translations_CommandNameAdd="InsertRow";
	public static final String Language_Translations_GetToEdit="Language_Edit";
	public static final String Language_Translations_Delete="Language_Delete";
	public static final String Language_Translations_Maintenance_EditPage="Language_Translations_MaintenanceEdit";
	public static final String Language_Translations_MaintenanceEdit_Action="/Language_Translations_MaintenanceEdit";
	public static final String Language_Translations_Search_Lookup_ActionEdit="/Language_Translation_Edit";
	public static final String Language_Translations_Edit_Action="Language_Translation_Edit";
	public static final String Language_Translations_CommandNameEdit="EditRow";
	/**
	 * Integer constant part
	 * 
	 */
	public static final int INT_ZERO=0;
	public static final int INT_TWO=2;
	public static final int INT_THREE=3;
	public static final int INT_FOUR=4;
	public static final int INT_FIVE=5;
	public static final int INT_SIX=6;
	public static final int INT_SEVEN=7;
	public static final int INT_EIGHT=8;
	public static final int INT_NINE=9;
	public static final int IntZero = 0;
	public static final int IntOne = 1;
	public static final int INT_TEN=10;
	public static final int INT_ELEVEN=11;
	public static final int INT_TWELVE = 12;
	public static final int INT_THIRTEEN = 13;
	public static final int INT_FOURTEEN = 14;
	public static final int INT_FIFTEEN = 15;
	public static final int INT_SIXTEEN=16;
	public static final int INT_SEVENTEEN = 17;
	public static final int INT_EIGHTEEN = 18;
	public static final int INT_NINETEEN = 19;
	public static final int INT_TWENTY = 20;
	public static final int INT_TWENTYONE = 21;
	public static final int INT_TWENTYTWO = 22;
	public static final int INT_TWENTYTHREE = 23;
	public static final int INT_TWENTYFOUR = 24;
	public static final int INT_TWENTYFIVE = 25;
	public static final int INT_TWENTYSIX = 26;
	public static final int INT_TWENTYSEVEN = 27;
	public static final int INT_TWENTYEIGHT = 28;
	public static final int INT_TWENTYNINE = 29;
	public static final int INT_THIRTY = 30;
	public static final int INT_THIRTYONE = 31;
	public static final int INT_THIRTYTWO = 32;
	public static final int INT_THIRTYTHREE = 33;
	public static final int INT_THIRTYFOUR = 34;
	public static final int INT_THIRTYFIVE = 35;
	public static final int INT_THIRTYSIX = 36;
	public static final int INT_THIRTYSEVEN = 37;
	public static final int INT_THIRTYEIGHT = 38;
	public static final int INT_THIRTYNINE = 39;	
	public static final int INT_FOURTY = 40;
	public static final int INT_FOURTYONE = 41;
	public static final int INT_FOURTYTWO = 42;
	public static final int INT_FOURTYTHREE = 43;
	public static final int INT_FOURTYFOUR = 44;
	public static final int INT_FOURTYFIVE = 45;
	public static final int INT_FOURTYSIX = 46;
	public static final int INT_FOURTYSEVEN = 47;
	public static final int INT_FOURTYEIGHT = 48;
	public static final int INT_FOURTYNINE = 49;
	public static final int INT_SIXTY = 60;
	public static final int INT_THOUSAND = 1000;
	
	 public static final String Strstatic_access="static-access";
	    public static final String fullfillmentCenter="fullfillmentCenter";
	    public static final String UrlsetFulfillmentId="/setFulfillmentId";
	    public static final String ffid="ffid";
	    public static final String UrlRestGetLocationLabelRePrintGetLocation="/RestGetLocationLabelRePrintGetLocation";
	    public static final int INT_ONELAKH = 100000;
	    public static final String dffr="dffr";
	    public static final String ALPHA="ALPHA";
	    public static final String SYMBOLATTHERATE="@";
	    public static final int INT_MINUSONE=-1;
	    
	    public static final String A="A";

	    public static final String C="C";
	    public static final String D="D";
	    public static final String E="E";
	    public static final String F="F";
	    public static final String G="G";
	    public static final String H="H";
	    public static final String I="I";
	    public static final String J="J";
	    public static final String K="K";
	 
	    public static final String M="M";
	  
	    public static final String O="O";
	    public static final String P="P";
	    public static final String Q="Q";
	   
	    public static final String S="S";
	 
	    public static final String U="U";
	    public static final String V="V";
	    public static final String W="W";
	    public static final String X="X";
	   
	    public static final String Z="Z";
	    
	    public static final String NSTRING="N";
	    public static final String YSTRING="Y";
	    
	    
	/**
	 * 
	 * String constant part
	 * 
	 */
	public static final String STRING_ZERO="0";
	public static final String STRING_ONE = "1";
	public static final String STRING_TWO="2";
	public static final String STRING_THREE="3";
	public static final String STRING_FOUR="4";
	public static final String STRING_FIVE="5";
	public static final String STRING_SIX="6";
	public static final String STRING_SEVEN="7";
	public static final String STRING_EIGHT="8";
	public static final String STRING_NINE="9";
	/**
	 * Team members message Constant
	 * 
	 */

	public static final String DepartmentList="DepartmentList";
	public static final String ShiftCodeList="ShiftCodeList";
	public static final String TeamMemmberMessages_Restfull_GetGeneralCode="/RestGetTeamMemmberMessagesGetGeneralCode";
	public static final String SetupDate="SetupDate";
	public static final String RequestMapping_Company_LookupScreen_Action="/Company_lookup";
	public static final String RequestMapping_Company_LookupScreen_Page="Company_Lookup";		
	public static final String Param_Languages_CompanyMaintenance_ObjectCode="CompanyMaintenance";
	public static final String Companies_name2="name2";	
	public static final String Companies_name5="name5";

	public static final String RequestMapping_Company_Maintenance_New_Screen_Action="/Company_maintenance_new";
	public static final String RequestMapping_Company_Maintenance_New_Page="Company_maintenance_new";
	public static final String RequestMapping_Company_SearchLookup_Screen_Action="/Company_search_lookup";
	public static final String RequestMapping_Company_SearchLookup_Screen_Page="Company_Search_Lookup";
	public static final String RequestMapping_Company_Read_Kendo_UI="/Company_searchlookup/Grid/read";
	public static final String RequestMapping_Company_Read_Kendo_UI_By_Part="/Company_searchlookup/Grid/readbyPart";
	public static final String RequestMapping_Company_SearchLookup_By_Part_Screen_Action="/Company_search_by_part_lookup";
	public static final String RequestMapping_Company_AddOrUpdate_Action="/Comapany_addorupdate";
	public static final String RequestMapping_Company_AddOrUpdate_Request_Action="/Comapany_addorupdate_request";
	public static final String RequestMapping_Company_Maintenance_Update_Page="Company_maintenance_update";
	public static final String RequestMapping_Company_Maintenance_Delete_Screen_Action="/Company_maintenance_delete";

	public static final String Companies_Restfull_GetCompanies="/RestGetAllGetCompanies";
	public static final String Company_Restfull_SetScreenLanguage="/RestGetCompanySetScreenLanguage";
	public static final String Company_Restfull_GetCompanyByID="/RestGetCompanyByID";
	public static final String Company_Restfull_GetCompanyByPartName="/RestGetCompanyByPartName";
	public static final String Company_Restfull_GeneralCode="/RestGetCompanyGeneralCode";
	public static final String Company_Restfull_AddOrUpdate="/RestGetCompanyAddOrUpdate";
	public static final String Company_Restfull_UploadImage="/uploadimageCompany";
	public static final String Company_Restfull_GetCompanyByIdTenant="/RestGetCompanybyIdTenant";
	public static final String Company_Restfull_DeleteCompanyByIdTenant="/RestDeleteCompanybyIdTenant";

	public static final String CompanyMaintenanceSceernLabelObj="CompanyMaintenanceSceernLabelObj";
	public static final String CompanyPartName="CompanyPartName";
	public static final String Company_Kendo_Read_URL="KendoReadUrl";
	public static final String GeneralCodeThemeFullDisplayListObject="GeneralCodeThemeFullDisplayListObject";
	public static final String GeneralCodeThemeMobileListObject="GeneralCodeThemeMobileListObject";
	public static final String GeneralCodeThemeRFListObject="GeneralCodeThemeRFListObject";
	public static final String CountryCodeObject="CountryCodeObject";
	public static final String Company_Register_ModelAttribute="COMPANY_REGISTER";
	public static final String Company_Bean_Object="Company_Bean_Object";
	public static final String MenuAppIcon_SetupTeamMemberName="SetupTeamMemberName";
	public static final String CompanyMaintenanceIcon_BucketName="companyMaintenanceIconBucketName";
	public static final String Load_Type="loadType";
	/**
	 * Fulfillment center constant
	 * 
	 */
	public static final boolean IsFullfillmentLogging=true;
	public static final String Fullfullment_M_FullfillmentCenterLookupFormSubmit="/FullfillmentCenterLookupFormSubmit";
	public static final String Fullfullment_M_FullfillmentCenterLookupDisplayAllFormSubmit="/FullfillmentCenterLookupDisplayAllFormSubmit";
	public static final String Fullfullment_M_FullfillmentCenterSearchLookUp="/FullfillmentCenterSearchLookUp";
	public static final String Fullfullment_M_FullfillmentCenterDisplayAllLookUp="/FullfillmentCenterDisplayAllLookUp";
	public static final String Fullfullment_M_FullfillmentCenterSearchByName="/FullfillmentCenterSearchByName";
	public static final String Fullfullment_M_FullfillmentCenterLookupNewRecordFormSubmit="/FullfillmentCenterLookupNewRecordFormSubmit";
	public static final String Fullfullment_M_FullfillmentCenterMaintenance="/FullfillmentCenterMaintenance";
	public static final String Fullfullment_M_FullfillmentCenterMaintenance_new="/FullfillmentCenterMaintenance_new";
	public static final String Fullfullment_M_SearchFullfillment="/SearchFullfillment";
	public static final String Fullfullment_M_DisplayAllFulfillmentAPI="/DisplayAllFulfillmentAPI";
	public static final String Fullfullment_M_DeleteFullfillment="/DeleteFullfillment";
	public static final String Fullfullment_M_IsFulfillmentAvailable="/IsFulfillmentAvailable";
	public static final String Fullfullment_M_addUpdateFulfillment="/addUpdateFulfillment";
	public static final String Fullfullment_M_AddUpdateFullfillmentCenter="AddUpdateFullfillmentCenter";
	public static final String Fullfullment_M_FullfillmentCenterMaintenanceUpdate="/FullfillmentCenterMaintenanceUpdate";
	public static final String Fullfullment_M_fullfillmentCenterLookup="/fullfillmentCenterLookup";
	public static final String Fullfullment_M_DisplayAllFulfillment="/DisplayAllFulfillment";
	public static final String Fullfullment_M_SearchByFulfillmentName="/SearchByFulfillmentName";
	public static final String Fullfullment_V_N_fullfillment_center_lookup="fullfillment_center_lookup";
	public static final String Fullfullment_V_N_Fulfillment_search_lookup="Fulfillment_search_lookup";
	public static final String Fullfullment_V_N_Fullfillment_Centre_Maintenance="Fullfillment_Centre_Maintenance";
	public static final String Fullfullment_V_P_KendroReadURL="KendoReadURL";
	public static final String Fullfullment_V_P_ViewFulfillment="ViewFulfillment";
	public static final String Fullfullment_V_P_PageRedirectLoadMessage="PageRedirectLoadMessage";
	public static final String Fullfullment_V_P_ViewLabels="ViewLabels";
	public static final String Fullfullment_V_P_ViewShowSecondContactDiv="ViewShowSecondContactDiv";
	public static final String Fullfullment_V_P_ViewShowThirdContactDiv="ViewShowThirdContactDiv";
	public static final String Fullfullment_V_P_ViewShowFourthContactDiv="ViewShowFourthContactDiv";
	public static final String Fullfullment_V_P_ViewCountryCodelst="ViewCountryCodelst";
	public static final String Fullfullment_V_P_viewIsFulfillmentExists="viewIsFulfillmentExists";
	public static final String Fullfullment_V_P_ViewStatesCodelst="ViewStatesCodelst";
	public static final String Fullfullment_V_P_ViewTenant="ViewTenant";
	public static final String Fullfullment_V_P_ViewIsSuccessMessage="ViewIsSuccessMessage";
	public static final String Fullfullment_R_P_FufffillmentID="FufffillmentID";
	public static final String Fullfullment_R_P_PartOfFullfillMentCenterName="PartOfFullfillMentCenterName";
	public static final String Fullfullment_R_P_PartOfFullfillment="PartOfFullfillment";
	public static final String Fullfullment_R_P_Fullfillment="Fullfillment";
	public static final String Fullfullment_R_P_IsSearchByFullfillmentID="IsSearchByFullfillmentID";
	public static final String Fullfullment_R_P_IsSearchByPartOfFullfillMentCenterName="IsSearchByPartOfFullfillMentCenterName";
	public static final String Fullfullment_R_P_IsSearchByPartOfFullfillment="IsSearchByPartOfFullfillment";
	public static final String Fullfullment_R_P_Tenant="Tenant";
	public static final String Fullfullment_R_P_FulfillmentPojo="FulfillmentPojo";
	public static final String Fullfullment_R_P_FulfillmentID="FulfillmentID";
	public static final String Fullfullment_R_P_PageRedirectLoadMessage="PageRedirectLoadMessage";
	public static final String Session_Fullfullment_Key_Session_PartOfFullfillmentName="Session_PartOfFullfillmentName";
	public static final String Session_Fullfullment_Key_Session_FullfillmentID="Session_FullfillmentID";
	public static final String Session_Fullfullment_Key_Session_lstOfFullFillmentCenterView="Session_lstOfFullFillmentCenterView";
	public static final String common_M_GetScreenLabels="/GetScreenLabels";
	public static final String common_P_ModuleName="ModuleName";
	public static final String common_P_LanguageCode="LanguageCode";
	public static final String StrPercentSign="%";
	/**
	 * Constant for COMMON HEADER of Application
	 * 
	 */
	public static final String HEADER_M_HeaderLanguageChange="/HeaderLanguageChange";
	public static final String HEADER_M_HeaderInfoHelp="/HeaderInfoHelp";
	public static final String COMMON_M_SetDeviceType="/SetDeviceType";
	public static final String HEADER_R_P_InfoHelp="InfoHelp";
	public static final String HEADER_R_P_InfoHelpType="InfoHelpType";
	public static final String COMMON_R_P_DeviceType="DeviceType";

	//public static final String MENUOPTION_JASCI="JASCI";
	public static final String MENUOPTION_LastActivityDate="LastActivityDate";
	public static final String InputParaMeter_MenuOption_valApplication="valApplication";
	public static final String INPUT_PARAMETER_MenuOption="MenuOption";

	public static final String Shift="Shift";
	public static final String Language="Languages";
	public static final String CONTRYCODE="CountryCode";
	public static final String States="StateCode";
	public static final String CountryCode="CountryCode";
	public static final String CompanyMaintenanceBillingControlNumberObj="CompanyMaintenanceBillingControlNumberObj";
	public static final String NextBillingControlNumber="NextBillingControlNumber";
	public static final String GetSetupdate="getSetupdate";
	public static final String GetLastActivitydate="getLastActivitydate";
	public static final String BillingControlNumber="BillingControlNumber";
	public static final String ShiftCode="ShiftCode";
	public static final String MenuProfile_Tenant = "tenant";
	public static final String InfoHelpTenantLanguage="TenantLanguage";

	public static final String MENU_OPTION_TEAMMEMBER="TEAMMEMBER";
	public static final String MENU_OPTION_CURRENTDATE="CURRENTDATE";
	public static final String Insert_DateFormat_DD_MMM_YY="yyyy-MM-dd HH:mm:ss";
	public static final String Insert_DateFormat_DD_MM_YY="yyyy-MM-dd";
	/**
	 * Constant for Team member message constant
	 * 
	 */
	public static final String TeamMemmberMessages_Rest_CheckTeamMemberId="/RestTeamMemberMessage_CheckMemberId";
	public static final String TeamMemmberMessages_Rest_CheckTeamMemberName="/RestTeamMemberMessage_CheckMemberName";
	public static final String TeamMemmberMessages_Rest_CheckDepartment="/RestTeamMemberMessage_CheckDepartment";
	public static final String TeamMemmberMessages_Rest_CheckShift="/RestTeamMemberMessage_CheckShift";
	/**
	 * Constant for Info help 
	 * 
	 */
	public static final String InfoHelp_Restfull_UploadPdf="/uploadpdf";
	public static final String InfoHelp_BucketName="InfoHelpBucketName";
	public static final String Location_Restfull_GetTeamMember="/GetTeamMember";
	public static final String	Location_Restfull_Tenant="Tenant_ID";
	public static final String Location_Restfull_TeamMember="TeamMember";
	public static final String MenuMessage_dd_MMM_yyyy="dd-MMM-yy";
	public static final String MENU_OPTION_MapSubApplication="MapSubApplication";

	/**
	 * Constant for Info Helps, Menu Messages and General Codes
	 * 
	 */
	public static final String GeneralCodeList_Code_Identification="Code Identification";
	public static final String GeneralCodeList_Description="Description";
	public static final String GeneralCodeList_Actions="Actions";
	public static final String GeneralCodeList_Edit="Edit";
	public static final String GeneralCodeList_Delete="Delete";	
	public static final String GeneralCodeList_AddNewBtn="Add New";
	public static final String GeneralCodeList_Application="Application";
	public static final String GeneralCodeList_General_Codes="General Codes";
	public static final String GeneralCodeList_Confirm_Delete="Are you sure you want to delete this record?";
	public static final String GeneralCodeEditDelete_Confirm_Save="Your record has been saved successfully.";
	public static final String GeneralCodeEditDelete_Confirm_Update="Your record has been updated successfully.";
	public static final String GeneralCodes_ApplicationValue="SYSTEM";
	public static final String yyyy_mm_dd_hh_mm_ss_Format="yyyy-MM-dd HH:mm:ss";
	public static final String hh_mm_ss_Value=" 00:00:00";
	public static final String RequestMapping_MenuProfileMaintenanceLookUp_Action="/MenuProfileMaintenanceLookUp";
	public static final String RequestMapping_MenuProfileMaintenanceLookUp_Execution="MenuProfileMaintenanceLookUp";
	public static final String RequestMapping_MenuProfileMaintenanceSearchLookUp_Action="/MenuProfileMaintenanceSearchLookUp";
	public static final String RequestMapping_MenuProfileMaintenance_Action="/MenuProfileMaintenance";
	public static final String RequestMapping_MenuProfileMaintenance_Delete_Action="/MenuProfile_searchlookup_delete";
	public static final String MENUPROFILE_MenuProfile="MenuProfile";
	public static final String MappingObject_MENUPROFILE_objHeader="objHeader";
	public static final String MappingObject_MENUPROFILE_objGeneralCodeMenuType="objGeneralCodeMenuType";
	public static final String MappingObject_MENUPROFILE_objGeneralCodeApplication="objGeneralCodeApplication";
	public static final String MappingObject_MENUPROFILE_objMenuAppIcon="objMenuAppIcon";
	public static final String RequestMapping_MenuProfileMaintenanceLookUp_Page="Menu_Profiles_Maintenance_Lookup";
	public static final String RequestMapping_MenuProfileMaintenance_Page="Menu_Profile_Maintenance";
	public static final String RequestMapping_MenuProfileMaintenanceSearchLookUp_Page="Menu_Profiles_Maintenance_Search_Lookup";
	public static final String REQUESTMAPPING_MENUPROFILE_RestFull_searchlookup="/MenuProfile_check_lookup";
	public static final String REQUESTMAPPING_MENUProfile_RestFull_Check_MenuProfile="/RestFull_Check_MenuProfile";
	public static final String MenuProfile_Restfull_SearchFieldValue="MenuProfileSearchFieldValue";
	public static final String MenuProfile_CheckVariable_objIsEdit="objIsEdit";
	public static final String MenuProfile_CheckVariable_EditScreen="EditScreen";
	public static final String MenuProfile_CheckVariable_MenuOptionList="MenuOptionList";
	public static final String MenuProfile_Menu_LookUp="Menu_LookUp";
	public static final String INUPUT_Parameter_MenuProfile_Id_Tenant="Id.Tenant";
	public static final String REQUESTMAPPING_MENUProfiles_MaintenanceUpdate_Action="/MenuProfile_Maintenance_UpdateTable";
	public static final String MENUProfile_lblMenuProfile="lblMenuProfile";
	public static final String MENUProfile_lblPartMenuProfileDescription="lblPartOfMneuProfileDescription";
	public static final String MENUProfile_lblAssignList="AssignList";
	public static final String MENUProfile_JsonKeyMenuOption="menuOption";
	public static final String REQUESTMAPPING_MENUProfile_RestFull_Kendo="/RestFull_MenuProfile_SearchLookUp";
	public static final String REQUESTMAPPING_MENUProfile_RestFull_MenuOptionList="/MenuOptionList";
	public static final String REQUESTMAPPING_MENUProfile_Parameter_MenuOptionApplication="MenuOptionApplication";
	public static final String REQUESTMAPPING_MENUProfile_Parameter_MenuOptionDisplayAll="MenuOptionDisplayAll";
	public static final String MenuProfile_StrColomnTenant = "Tenant";
	public static final String REQUESTMAPPING_MENUProfile_RestFull_Check_MenuOption_Profile="/Check_MenuProfile";
	public static final String REQUESTMAPPING_MENUProfile_Maintenance_ListPojoObject="MenuProfile_Update_Object";
	public static final String MenuProfile_Maintenance_Input_MenuProfileTenant="MenuProfileTenant";
	public static final String REQUESTMAPPING_MENUOPTION_Parameter_MenuProfileTenant="MenuProfileTenant";
	/**
	 * 
	 * Constant for Note screen
	 * 
	 */
	public static final String RequestMapping_NotesScreen_Action="/Notes_Lookup";
	public static final String RequestMapping_Rest_NotesScreen_Action="/NotesLookup";
	public static final String RequestMapping_NotesScreen_Page="NotesLookup";
	public static final String RequestMapping_NotesScreen_Maintenance_New_Screen_Action="/New_Notes_Maintenance_Screen";
	public static final String RequestMapping_notes_Lookup_Screen_Action="NotesLookup_add_update";
	public static final String RequestMapping_Notes_Update_Request_Action="/Notes_Maintenance_Screen_Action";
	public static final String RequestMapping_Notes_Update_Request_page="Notes_Maintenance_Screen";
	public static final String RequestMapping_Notes_AddNewNote_Request_page="NotesMaitenance_Screen_Add_new";
	public static final String RequestMapping_Notes_Update_Request_Action_rest="/Update_Notes_Maintenance_Screen_rest";
	public static final String RequestMapping_Notes_saveOrupdate_Request_Action="/Notes_Maintenance_Screen_saveOrupdate";
	public static final String RequestMapping_Notes_AddOrUpdate_Request_page="Notes_Maintenance_Screen_addupdate";
	public static final String RequestMapping_Notes_AddOrUpdate_Request_Action="/Addupdate_Notes_Maintenance_Screen";
	public static final String Notes_Restfull_AddOrUpdate="/Notes_Maintenance_Screen_saveOrupdate_rest";
	public static final String RequestMapping_Notes_Add_Request_Action="/Notes_Maintenace_Screen_addnew";
	public static final String RequestMapping_Notes_Delete_Screen_Action="/Notes_Maintenance_delete_Action";
	public static final String Notes_Restfull_Delete_note="/RestGetDeleteNotes";
	public static final String Notes_Lookup_KendoUrl="KendoUrl";
	public static final String Add_Notes_ModelAttribute="ADD_NOTE";
	public static final String Note_operation_ModelAttribute="NOTE_OPERATION";
	public static final String Notes_FlagForReturn_NotesLookup="SearchLookup";
	public static final String Notes_FlagForReturn="FlagForReturn";
	public static final String Notes_saveOrupdate_ModelAttribute="SAVEORUPDATE_NOTE";
	public static final String notes_Restfull_Objnotes="ObjNotes";
	public static final String Notes_id="NOTE_ID";
	public static final String Notes_link="NOTE_LINK";
	public static final String Common_get_notesScreenLabel="notesLabel";
	public static final int RecordCounter=1;
	public static final String VarTeam_Member_getNotesList="Team_Member";
	public static final String VarTenant_ID_getNotesList="Tenant_ID";
	public static final String VarCompnay_ID_getNotesList="Company_ID";
	public static final String Param_Notes_Loginscreen_ObjectCode="LoginScreen";
	public static final String Param_Notes_Notes="Notes";
	public static final String RequestMapping_Notes_Read_Kendo_UI="/NotesLookup/Grid/read";
	public static final String Notes_notes_id="Noteid";
	public static final String Notes_notes_link="Notelink";
	public static final String NOTE_objnoteFields= "objnoteFields";
	public static final String NOTE_viewLabels= "viewLabels";
	/**  
	 * Constant for location label print 
	 * 
	 */
	public static final String RequestMapping_LocationLabelPrint_Action="/Location_label_print";
	public static final String LocationLabelPrintSceernLabelObj="LocationLabelPrintSceernLabelObj";
	public static final String LocationLabelPrintLocLabelListObj="LocationLabelPrintLocLabelListObj";
	public static final String LocationLabelPrintAreasListObj="LocationLabelPrintAreasListObj";
	public static final String LocationLabelPrint_Restfull_GeneralCode="/RestGetLocationLabelPrintGeneralCode";
	public static final String LocationLabelPrint_Restfull_GetLocation="/RestGetLocationLabelPrintGetLocation";
	public static final String Location_Restfull_Area="Area";
	public static final String Location_Restfull_Location="Location";
	/*public static final String AREAS="AREAS";
	public static final String LOCLABEL="LOCLABEL";*/
	public static final String Company_Restfull_CheckUniqueEmail="/RestCheckCompanyUniqueEmail";
	public static final String Company_Restfull_CompanyByID="CompanyID";
	public static final String Company_Restfull_Tenant="Tenant";
	public static final String Company_Restfull_CompanyPartName="CompanyPartName";
	public static final String Company_Restfull_Companies="Companies";
	public static final String Company_Restfull_CompaniesEmail="Email";
	public static final String FulfillmentCenterModule= "Fulfillment Center";
	public static final String StrMenuType="MenuType";
	public static final String REQUESTMAPPING_MENUProfile_RestLoadEditGrid="/RestLoadEditGrid";
	public static final String REQUESTMAPPING_MENUProfile_MenuOptionList_Edit="/MenuOptionList_Edit";
	public static final String REQUESTMAPPING_MENUProfile_MenuAppIconUrl="/MenuAppIconUrl";
	public static final String MenuProfile_AppIcon="AppIcon";
	public static final String NOTE_ID_Value = "NOTE_ID";
	public static final String NOTE_LINK_Value = "NOTE_LINK";
	public static final String LastActivitydate_Value = "LastActivitydate";
	public static final String LastActivityBy_Value = "LastActivityBy";
	public static final String ID_Value = "ID";
	public static final String RequestMapping_Locations_Profile_SearchAction = "/Location_Profile_Maintenance_Search";
	public static final String RequestMapping_Locations_Profile_SearchPage = "Location_Profile_Maintenance_Lookup";
	public static final String RequestMapping_Locations_Profile_SearchLookupAction = "/Location_Profile_Maintenance_SearchLookup";
	public static final String RequestMapping_Locations_Profile_SearchLookupPage = "Location_Profile_Maintenance_SearchLookup";
	public static final String RequestMapping_Locations_Profile_MaintenanceAction = "/Location_Profile_Maintenance";
	public static final String RequestMapping_Locations_Profile_MaintenancePage = "Location_Profile_Maintenance_New";
	public static final String RequestMapping_Locations_Profile_InsertAction = "/Location_Profile_Add";
	public static final String ModelAttribute_Locations_Profile_Insert ="ObjectLocationProfiles";
	public static final String RequestMapping_Locations_Profile_DeleteAction = "/Location_Profile_Delete";
	public static final String RequestMapping_Locations_Profile_SearchLookup="/LocationProfileMaintenanceSearchLookup";
	public static final String RequestMapping_LocationProfile_maintenance_edit="/LocationProfile_maintenance_edit";
	public static final String RequestMapping_LocationProfile_maintenance_Copy="/LocationProfile_maintenance_copy";
	public static final String RequestMapping_Locations_Profile_Maintenance_EditCopy_Page = "Location_Profile_Maintenance_Edit";
	public static final String RequestMapping_Locations_Profile_Maintenance_EditAction = "/LocationProfileMaintenance";
	public static final String RequestMapping_Locations_Profile_EditCopyAction = "/Location_Profile_EditCopy";
	public static final String RequestMapping_Locations_Profile_CopyAction = "/Location_Profile_Copy";
	public static final String LocationsProfileLabels= "LocationsProfileLabels";
	public static final String Locations_Profile_Restfull_GetGeneralCode="/RestGetLocationsProfile_GeneralCode";
	/*	public static final String PROFILEGROUPCODE="LOCPFGRP";
	public static final String LOCATIONTYPECODE="LOCTYPE";
	public static final String STORAGETYPECODE="STRGTYPES";*/
	public static final String GeneralCodeStorageType="GeneralCodeStorageType";
	public static final String GeneralCodeLocationType= "GeneralCodeLocationType";
	public static final String GeneralCodeProfileGroup="GeneralCodeProfileGroup";
	public static final String FulfilmentCenterList="FulfilmentCenterList";
	public static final String RestParam_Profile="LocationProfile";
	public static final String RestParam_ProfileGroup="ProfileGroup";
	public static final String RestParam_Description="Description";
	public static final String RestParam_ToSelectLocation="ToSelectLocation";
	public static final String RestParam_FromlocationProfile="FromlocationProfile";
	public static final String LocationProfile_NotesId="LOCPROFILE";
	public static final String LastActivityTeamMember="LastActivityTeamMember";
	public static final String LastActivityDate="LastActivityDate";
	public static final String StrLocationProfileParam="LocationProfile";
	public static final String StrLocationProfileGroup="ProfileGroup";
	public static final String StrLocationProfileDescription="PartofLocationProfileDescription";
	public static final String StrLocationProfileSaved="Saved";
	public static final String StrLocationProfileUpdated="Updated";
	public static final String NumberOfLocations="NumberOfLocations";
	public static final String LocationProfileBe="LocationProfileBe";
	public static final String CopyOrEdit="CopyOrEdit";
	public static final String Edit="Edit";
	public static final String Copy="Copy";
	public static final String LocationProfileList="LocationProfileList";
	public static final String FulFillmentCenterTextBox="FulFillmentCenterTextBox";
	public static final String ProfileGroupTextBox="ProfileGroupTextBox";
	public static final String LocationProfileTextBox="LocationProfileTextBox";
	public static final String RestParam_FulFillmentCenter="FulFillmentCenter";
	public static final String RestFull_CheckAlreadyExists="/RestCheck_AlreadyExists_LocationProfile";
	public static final String RestFull_CheckLocationProfile="/RestCheckLocationProfile";
	public static final String RestFull_CheckProfileGroup="/RestCheckProfileGroup";
	public static final String RestFull_CheckDescription="/RestCheckLocationProfileDescription";
	public static final String RestFull_Delete="/RestLocationProfileDelete";
	public static final String RequestMapping_LocationsProfileData_Grid_Read = "LocationProfileData/grid/read";
	public static final String RestFull_ReassignLocation="/ReassignLocationProfile";
	/**
	 *Constant for Location Maintenance
	 * 
	 */	
	public static final String RequestMapping_Locations_Search="/Location_Maintenance_Search";
	public static final String RequestMapping_Locations_Search_Lookup="/Location_Maintenance_SearchLookup";
	public static final String RequestMapping_Locations_SearchLookup="/Location_Maintenance_Search_Lookup";
	public static final String RequestMapping_Locations_Maintenance_New="/Location_Maintenance_New";
	public static final String RequestMapping_Locations_Maintenance_Add="/Location_Maintenance_Add";
	public static final String Location_ModelAttribute="ObjectLocation";
	public static final String LocationMaintenance_Restfull_RestLocationMaintenanceAdd="/RestLocationMaintenanceAdd";
	public static final String LocationMaintenance_Restfull_RestLocationMaintenanceUpdate="/RestLocationMaintenanceUpdate";
	public static final String	LocationMaintenanace_Restfull_RestSelectGeneralCode="/RestSelectGeneralCode";
	public static final String Location_Restfull_Company="Company_ID";
	public static final String Location_Restfull_FulfillmentCenter="FulfillmentCenter";
	public static final String Location_Restfull_Alternate_Location="Alternate_Location";
	public static final String Location_Restfull_Description="Description";
	public static final String Location_Restfull_LocationType="LocationType";
	public static final String Location_Restfull_Search="/LocationSearch";
	public static final String Location_Restfull_CheckUniqueAlternateLocation="/RestCheckUniqueAlternateLocation";
	public static final String Location_Restfull_CheckUniqueAlternateLocationEdit="/RestCheckUniqueAlternateLocationEdit";
	public static final String Location_Restfull_CheckUniqueLocation="/RestCheckUniqueLocation";
	public static final String	Location_Restfull_GeneralCodeID="General_Code_ID";
	public static final String	LocationMaintenanace_Restfull_RestSelectLocationProfile="RestSelectLocationProfile";
	public static final String	Location_Restfull_FullfilmentCenter="Fullfilment_Center";
	public static final String	Location_Combobox_SelectLocationProfile="SelectLocationProfilevalue";
	/*public static final String	LocationMaintenance_LastActivityTask_GeneralCodeID="TASK";
	public static final String	LocationMaintenance_Area_GeneralCodeID="AREAS";
	public static final String	LocationMaintenance_LocationType_GeneralCodeID="LOCTYPE";
	public static final String	LocationMaintenance_Wizard_ID_GeneralCodeID="WIZARDS";
	public static final String	LocationMaintenance_Work_Group_Zone_GeneralCodeID="GRPWRKZONE";
	public static final String	LocationMaintenance_Work_Zone_GeneralCodeID="WORKZONES";
	public static final String	LocationMaintenance_Storage_Type_GeneralCodeID="STRGTYPES";*/
	public static final String	Location_Combobox_SelectLastActivityTask="SelectLastActivityTask";
	public static final String	Location_Combobox_SelectArea="SelectArea";
	public static final String	Location_Combobox_SelectLocationType="SelectLocationType";
	public static final String	Location_Combobox_SelectWizardID="SelectWizardID";
	public static final String	Location_Combobox_SelectWorkGroupZone="SelectWorkGroupZone";
	public static final String	Location_Combobox_SelectWorkZone="SelectWorkZone";
	public static final String	Location_Combobox_SelectStorageType="SelectStorageType";
	public static final String	SuppressWarningsfinally="finally";			
	public static final String Location_TeamMember_Name="TeamMemberValue";
	public static final String Location_Area_JspPageValue="Area";
	public static final String Location_Description_JspPageValue="PartofLocationDescription";
	public static final String Location_Location_JspPageValue="Location";
	public static final String Location_Locationtype_JspPageValue="Location_Type";
	public static final String Location_FulfillmentCenter_JspPageValue="Fulfillment_Center";
	public static final String RequestMapping_Location_Kendo_DisplayAll="/LocationDisplayAll";
	public static final String Location_Restfull_FetchLocation="/FetchLocation";
	public static final String RequestMapping_LocationMaintenance_Update="/Location_Maintenance_Update";
	public static final String RequestMapping_Location_Maintenance_Update="Location_Maintenance_Update";
	public static final String RequestMapping_LocationMaintenance_Update_Record="/LocationMaintenance_Update_record";
	public static final String ObjectLocation="ObjectLocation";
	public static final String JspHiddenFullfillment_Center_ID="HiddenFullfillment_Center_ID";
	public static final String JspHiddenLocation="HiddenLocation";
	public static final String JspHiddenArea="HiddenArea";
	public static final String JspHiddenLocation_Profile="HiddenLocation_Profile";
	public static final String JspHiddenWizard_ID="HiddenWizard_ID";
	public static final String JspHiddenWork_Group_Zone="HiddenWork_Group_Zone";
	public static final String JspHiddenWork_Zone="HiddenWork_Zone";
	public static final String RequestMapping_LocationMaintenance_CopyPage="/Location_Maintenance_Copy";
	public static final String RequestMapping_Location_Maintenance_Copy="Location_Maintenance_Update";
	public static final String Location_Pages="LocationPage";
	public static final String LocationEditScreen="Edit";
	public static final String LocationCopyScreen="Copy";
	public static final String Location_Restfull_LocationExistForDelete="/RestLocationExistForDelete";
	public static final String Location_QueryNameLocationExistForDelete="QueryNameLocationExistForDelete";
	public static final Long LongZero=0L; 
	public static final Double DecimalZero=0.0;
	public static final String LocationMaintenance_Restfull_GetTeamMember="/LocationGetTeamMember";
	public static final String Location_Combobox_SelectFullfillment="SelectFulFillment";
	public static final String LocationMaintenance_Delete="/Location_Meaintenance_Delete";
	public static final String LocationMaintenance_Restfull_Delete="/RestLocation_Meaintenance_Delete";
	public static final String LocationMaintenance_Response_Zero="Zero";
	public static final String LocationMaintenance_Response_One="One";
	public static final String LocationMaintenance_Response_Delete="Delete";
	public static final String LocationMaintenance_ScreenLabels="ScreenLabels";
	public static final String Locations_NotesId="LOCATION";
	public static final String Location_NotesErrorMessage="No Notes exist for this Location.";
	/**
	 * Constant for  Menu Profile Assigment Mainenance
	 * 
	 */
	public static final String RequestMapping_MenuProfileAssigmentLookUp_Action="/MenuProfileAssigmentLookUp";
	public static final String RequestMapping_MenuProfileAssigmentLookUp_Execution="MenuProfileAssigmentLookUp";
	public static final String RequestMapping_MenuProfileAssigmentMaintenance_Action="/MenuProfileAssigmentMaintenance";
	public static final String RequestMapping_MenuProfileAssigmentSearchLookup_Action="/MenuProfileAssignmentSearchLookup";
	public static final String RequestMapping_MenuProfileAssigment_Delete_Action="/MenuProfileAssignment_Delete";
	public static final String RequestMapping_MenuProfileAssigment_SaveAndUpdate_Action="/MenuProfileAssignment_SaveAndUpdate";
	public static final String RequestMapping_MenuProfileAssigmentLookUp_Page="Menu_profiles_assignment_lookup";
	public static final String RequestMapping_MenuProfileAssigmentMaintenance_Page="Menu_Profiles_Assignment_Maintenance";
	public static final String RequestMapping_MenuProfileAssigmentSearchLookup_Page="Menu_Profiles_Assignment_Search_Lookup";
	public static final String MenuProfileAssigment_SearchValue="SearchValue";
	public static final String MenuProfileAssigment_SearchField="SearchField";
	public static final String MenuProfileAssigment_TeamMemberID="TeamMemberID";
	public static final String MenuProfileAssigment_tenant1="tenant1";
	public static final String MenuProfileAssigment_strMenuProfileJson="strMenuProfileJson";
	public static final String MenuProfileAssigment_lblTeamMemberID="lblTeamMemberID";
	public static final String MenuProfileAssigment_SelectedTeamMember="SelectedTeamMember";
	public static final String MenuProfileAssigment_PartOfTeamMemberName="PartOfTeamMemberName";
	public static final String MenuProfileAssigment_lblPartOfTeamMemberNameID="lblPartOfTeamMemberNameID";
	public static final String MenuProfileAssigment_lblTeamMemberName="lblTeamMemberName";
	public static final String RequestObjectMapping_MenuProfileAssigment_objMenuAssignmentLBL="objMenuAssignmentLBL";
	public static final String RequestObjectMapping_MenuProfileAssigment_objMenuTypes="objMenuTypes";
	public static final String RequestObjectMapping_MenuProfileAssigment_objTeamMemberID="objTeamMemberID";
	public static final String RequestObjectMapping_MenuProfileAssigment_objMenuProfileAssignmentPojo="objMenuProfileAssignmentPojo";
	public static final String RestFullMapping_MenuAssignment_check_PartOfTeamMemberName="/RestFull_check_PartOfTeamMemberName";
	public static final String RestFullMapping_MenuAssignment_RestFull_Check_TeamMemberID="/RestFull_Check_TeamMemberID";
	public static final String RestFullMapping_MenuAssignment_MenuProfileAssignmentSearchList="/MenuProfileAssignmentSearchList";
	public static final String RestFullMapping_RestFull_MenuProfileAssignment_SaveAndUpdate="/RestFull_MenuProfileAssignment_SaveAndUpdate";
	public static final String RestFullMapping_RestFull_MenuProfileAssignment_Delete="/RestFull_MenuProfileAssignment_Delete";
	public static final String RestFullMapping_RestFull_MenuProfileAssignment_fromMenuHeader="/RestFull_MenuProfileAssignment_fromMenuHeader";
	public static final String RestFullMapping_RestFull_MenuProfile_fromMenuHeader="/RestFull_MenuProfile_fromMenuHeader";
	public static final String RestFullMapping_RestFull_MenuAssignment_AllTeamMember="/RestFull_MenuAssignment_AllTeamMember";
	public static final String RestFullMapping_RestFull_MenuAssignment_TeamMemberID="/RestFull_MenuAssignment_TeamMemberID";
	public static final String RestFullMapping_RestFull_MenuAssignment_PartOFTeamMemberName="/RestFull_MenuAssignment_PartOFTeamMemberName";
	public static final String RestFullMapping_RestFull_AssignedMenuProfile="/RestFull_AssignedMenuProfile";
	public static final String RestFullMapping_RestFull_TeamMemberName_MenuProfileAssignment="/RestFull_TeamMemberName_MenuProfileAssignment";
	public static final String RestFullMapping_Kendo_MenuProfileAssignment_List="/Kendo_MenuProfileAssignment_List";

	public static final String MenuProfile_objChangeDropDown="objChangeDropDown";
	public static final String MenuProfile_objApplication="objApplication";
	public static final String RequestMapping_MenuProfile_Menu_Profile_searchLookup="/Menu_Profile_searchLookup";
	public static final String MenuProfile_lbllastActivityTeamMember="lbllastActivityTeamMember";
	public static final String MenuProfile_lbllastDate="lbllastDate";
	public static final String MenuProfile_lbldescShort="lbldescShort";
	public static final String MenuProfile_lbldescLong="lbldescLong";
	public static final String MenuProfile_lblhelpLine="lblhelpLine";
	public static final String MenuProfile_lblAppIcon="lblAppIcon";
	public static final String strTrue="true";
	public static final String RequestMapping_MenuProfileAssigment_SearchLookup_Action="/MenuProfileAssignment_SearchLookup";
	public static final String Last_Activity_DateH="Last_Activity_DateH";
	public static final String Generalcode_Restfull_RestCheckUniqueDescriptionShort="/RestCheckUniqueDescriptionShort";
	public static final String Generalcode_Restfull_RestCheckUniqueDescriptionLong="/RestCheckUniqueDescriptionLong";
	public static final String Generalcode_Restfull_RestCheckUniqueGeneralCode="/RestCheckUniqueGeneralCode";
	public static final String Generalcode_Restfull_RestCheckUniqueMenuName="/RestCheckUniqueMenuName";
	public static final String ScreenName="ScreenName";
	public static final String LocationProfile_Restfull_RestCheckUniqueLocationProfileDescriptionShort="/RestCheckUniqueLocationProfileDescriptionShort";
	public static final String LocationProfile_Restfull_RestCheckUniqueLocationProfileDescriptionLong="/RestCheckUniqueLocationProfileDescriptionLong";
	public static final String RestParam_Description50="Description50";
	public static final String RestParam_Description20="Description20";
	public static final String LocationProfile_Restfull_RestGetNumberOfLocation="/RestGetNumberOfLocation";
	/**
	 * Constant for Location Wizard
	 * 
	 */
	public static final String RequestMapping_Location_Wizard_Lookup_Action="/Location_Wizard_Lookup";
	public static final String RequestMapping_Location_Wizard_Lookup_Page="Location_Wizard_Lookup";
	public static final String RequestMapping_Location_Wizard_Maintenance_Action="/Location_Wizard_Maintenance";
	public static final String RequestMapping_Location_Wizard_Maintenance_Edit_Action="/Location_Wizard_Maintenance_Edit";
	public static final String RequestMapping_Location_Wizard_Maintenance_Edit_And_Copy_OnSearch_Lookup_Action="/Location_Wizard_Maintenance_Edit_And_Copy_OnSearch_Lookup";
	public static final String RequestMapping_Location_Wizard_Maintenance_Add_Update="/Location_Wizard_Maintenance_Add_Update";
	public static final String RequestMapping_Location_Wizard_Maintenance_Page="Location_Wizard_Maintenance";
	public static final String RequestMapping_Location_Wizard_Maintenance_Edit_Page="Location_Wizard_Maintenance_Edit";
	public static final String RequestMapping_Location_Wizard_Search_Lookup_Action="/Wizard_Location_Search_Lookup";
	public static final String RequestMapping_Location_Wizard_Delete_WizardOnly_Action="/Delete_WizardOnly";
	public static final String RequestMapping_Location_Wizard_Delete_Wizard_And_Location_Action="/Delete_WizardAndLocation";
	public static final String RequestMapping_Location_Wizard_Search_Lookup_Page="Wizard_Location_Search_Lookup";
	public static final String RequestMapping_Location_Wizard_Print_Popup_Page="PrintPopup";
	public static final String Loction_Wizard_RestFull_Wizard_Control_Number="WIZARD_CONTROL_NUMBER";
	public static final String Loction_Wizard_RestFull_Area="Area";
	public static final String Loction_Wizard_RestFull_Is_Search="Is_Search";
	public static final String Loction_Wizard_RestFull_Wizard_ID="WIZARD_ID";
	public static final String Loction_Wizard_RestFull_LocationWizardObj="RestFullLocationWizardObj";
	public static final String Loction_Wizard_RestFull_Tenant_ID="TENANT_ID";
	public static final String Loction_Wizard_RestFull_General_Code_ID="General_Code_ID";
	public static final String Location_Wizard_Restfull_GetData="/RestGetLocationWizardData";
	public static final String Location_Wizard_Restfull_GetDataSearchArea="/RestGetLocationWizardAreaSearch";
	public static final String Location_Wizard_Restfull_GetDataSearchLocationType="/RestGetLocationWizardLocationTypeSearch";
	public static final String Location_Wizard_Restfull_AnyPartofWizardDiscription="/RestGetAnyPartofWizardDiscriptionSearch";
	public static final String Location_Wizard_Restfull_GetDrop_DownValue="/RestGetLocationWizardGetDropDown";
	public static final String Location_Wizard_Restfull_GetDrop_DownValue_Fulfillment="/RestGetFulfillmentDropDown";
	public static final String Location_Wizard_Restfull_GetDrop_DownValue_LocationProfile="/RestGetLocationProfileDropDown";
	public static final String Location_Wizard_Restfull_GetData_for_copy="/RestGetDataForCopy";
	public static final String Location_Wizard_Restfull_CheckExisting="/RestGetDataCheckExisting";
	public static final String Location_Wizard_Restfull_FulfillmentList="/RestGetDataFulfillmentList";
	public static final String Location_Wizard_Restfull_GetData_Check_Location_List="/RestCheckLocationList";
	public static final String Location_Wizard_Restfull_Check_Location_List_And_Inventory_Level="/RestCheckLocationListInventory_LevelList";
	public static final String Location_Wizard_Restfull_Delete_Wizard_And_Location="/RestDeleteWizardAndLocations";
	public static final String Location_Wizard_Restfull_Delete_Wizard_Only="/RestDeleteWizardOnly";
	public static final String Location_Wizard_Restfull_Get_Data_For_Grid="/RestGetDataForGrid";
	public static final String Location_Wizard_Restfull_Get_Data_SaveAndUpdate="/RestGetDataSaveAndUpdate";
	public static final String Location_Wizard_Restfull_Company="COMPANY_ID";
	public static final String Location_Wizard_Rest_Fulfillment="FULFILLMENT_CENTER_ID";
	public static final String Location_Wizard_TENANT_ID="TENANT_ID";
	public static final String InfoHelp_DateFormat = "yyyy-MM-dd HH:mm:ss";
	public static final String Wizard_ID="LOCATIONS";
	public static final String Location_Wizard_Restfull_InsertLocations="InsertLocation";
	public static final String Data="DATA";
	public static final String FromLookup="FromLookup";
	public static final String Location_Wizard_FULFILLMENT_CENTER_ID="FULFILLMENT_CENTER_ID";
	public static final String Location_Wizard_COMPANY_ID="COMPANY_ID";
	public static final String Location_Wizard_WIZARD_ID="WIZARD_ID";
	public static final String Location_Wizard_Search_Based="search";
	public static final String Location_Wizard_WIZARD_CONTROL_NUMBER="WIZARD_CONTROL_NUMBER";
	public static final long Location_Wizard_WIZARD_Con_No=1;
	public static final String Location_Wizard_Searched_Value="searchValue";
	public static final String Location_Wizard_DESCRIPTION20="DESCRIPTION20";
	public static final String Location_Wizard_DESCRIPTION50="DESCRIPTION50";
	public static final String Location_Wizard_AREA="AREA";
	public static final String Location_Wizard_Location_Type="Location_Type";
	public static final String Location_Wizard_WORK_GROUP_ZONE="WORK_GROUP_ZONE";
	public static final String Location_Wizard_WORK_ZONE="WORK_ZONE";
	public static final String Location_Wizard_LOCATION_TYPE="LOCATION_TYPE";
	public static final String Location_Wizard_AnyPartofWizardDiscription="AnyPartofWizardDiscription";
	public static final String Location_Wizard_LOCATION_PROFILE="LOCATION_PROFILE";
	public static final String Location_Wizard_CREATED_BY_TEAM_MEMBER="CREATED_BY_TEAM_MEMBER";
	public static final String Location_Wizard_DATE_CREATED="DATE_CREATED";
	public static final String Location_Wizard_LAST_ACTIVITY_DATE="LAST_ACTIVITY_DATE";
	public static final String Location_Wizard_LAST_ACTIVITY_TEAM_MEMBER="LAST_ACTIVITY_TEAM_MEMBER";
	public static final String Location_Wizard_Searched_Data="Location_Wizard_Searched_Data";
	public static final String Location_Wizard_Maintenance_Level="Location_Wizard_Maintenance_Level";
	public static final String Location_Wizard_Lookup_Level="Location_Wizard_Lookup_Level";
	public static final String Location_Wizard_Search_Lookup_Level="Location_Wizard_Search_Lookup_Level";
	public static final String Location_Wizard_Kendo_Grid_Data="Grid_Data";
	public static final String Location_Wizard_Maintenance_Drop_Down="Location_Wizard_Maintenance_Drop_Down";
	public static final String Location_Wizard_Maintenance_Drop_Down_WRKZONE="Location_Wizard_Maintenance_Drop_Down_WRKZONE";
	public static final String Location_Wizard_Maintenance_Drop_Down_WZONE="Location_Wizard_Maintenance_Drop_Down_WZONE";
	public static final String Location_Wizard_Maintenance_Drop_Down_Location_Type="Drop_Down_Location_Type";
	public static final String Location_Wizard_Maintenance_Drop_Down_Wizard_Id="Drop_Down_Wizard_ID";
	public static final String Location_Wizard_Maintenance_Drop_Down_Location_Profile="Drop_Down_Location_Profile";
	public static final String Location_Wizard_General_Code_ID_AREAS="AREAS";
	public static final String Location_Wizard_General_Code_ID_GRPWRKZONE="GRPWRKZONE";
	public static final String Location_Wizard_General_Code_ID_WORKZONES="WORKZONES";
	public static final String Location_Wizard_General_Code_ID_Location_Type="LOCTYPE";
	public static final String Location_Wizard_General_Code_ID_Wizard_Id="WIZARDS";
	public static final String Location_Wizard_Fulfillment_Drop_Down="Fulfillment_Drop_Down";
	public static final String Location_Wizard_Set_Date_On_Maintenance="Date_On_Maintenance";
	public static final String Location_Wizard_IncreasedWizardControlNumber="IncreasedWizardControlNumber";
	public static final String AppliedOrNot="APPLIED";
	public static final String AppliedNot="Not APPLIED";
	public static final String Location_Wizard_RecordList="RecordList";
	public static final String Location_Wizard_duplicateRecordNo="duplicateRecordNo";
	public static final String Location_Wizard_SavedNo="SavedNo";
	public static final String Location_Wizard_Register_ModelAttribute="Location_Wizard_Register";
	public static final String Location_Wizard_Team_MemberName="TeammemberName";
	public static final String LocationWizard_Restfull_GetTeamMember="/LocationWizard_Restfull_GetTeamMember";
	public static final String LocationLabelPrint_Restfull_GetWizardControlNumber="/RestGetLocationLabelPrintGetGetWizardControlNumber";
	public static final String LocationLabelPrint_Restfull_GetLocationWizardControlNumber="/RestGetLocationLabelPrintGetLocationWizardControlNumber";
	public static final String LocationLabelPrint_Restfull_GetLocationLabelTypePrint="/RestGetLocationLabelPrintType";
	public static final String LocationLabelPrint_Restfull_GetWizardLocationList="/RestGetLocationLabelPrintGetWizardLocationList";
	public static final String Location_Restfull_WizardControlNumber="WizardControlNumber";
	public static final String MenuProfile_objDeviceType="objDeviceType";
	public static final String ClientDeviceTimeZone = "ClientDeviceTimeZone";
	public static final String COMPANYSTATUS = "COMPANYSTATUS";
	public static final String Not_Allowed_Company_Deleted="NotAllowedCompanyDeleted";
	public static final String LocationLabelPrint_Restfull_Generate_Barcode_LocationLabel_Part1="/RestGenerateBarcodeLabelPart1";
	public static final String LocationLabelPrint_Restfull_Generate_Barcode_LocationLabel_Part2="/RestGenerateBarcodeLabelPart2";
	public static final String Location_Restfull_Label_Type="LabelType";
	public static final String Location_Restfull_Barcode_Type="BarcodeType";
	public static final String InputTimezoneUTC="UTC";
	public static final String RequestMapping_LocationsProfileData_Grid_Read_Reassign = "LocationProfileData/grid/read/reassign";
	public static final String LocationProfile_RassignLocationProfile="update LOCATION set LOCATION_PROFILE=? where upper(LOCATION_PROFILE)=? and upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(FULFILLMENT_CENTER_ID)=?";
	public static final String RequestMapping_LocationProfile_Reassign_JspValue="Location_Profile_Reassign_JspValue";
	public static final String LocationProfile_Jsp_ReassignprofileGroup="ReassignprofileGroup";
	public static final String LocationProfile_Jsp_ReassignfulFillmentCenter="ReassignfulFillmentCenter";
	public static final String LocationProfile_Jsp_ReassignlocationProfile="ReassignlocationProfile";
	public static final String DoubleQuote="\"";
	public static final String SLASHDOT = "\\.";
	public static final String StringDoubleQuote="&quot";
	public static final String UTCLastActivityDate="getLastActivitydate";
	public static final String	LocationMaintenanace_Restfull_RestSelectFulfillmentCenterValue="RestSelectFulfillmentCenterValue";
	public static final String Location_Wizard_Fulfilment_Status="A";
	public static final String LocationMaintenanaceWizard_Restfull_RestSelectFulfillmentCenterValue="RestSelectFulfillmentCenterValueWizard";
	public static final String RequestMapping_Duplicate_List="/Duplicate_List";
	public static final String Char_A = "A";
	public static final String Char_Z = "Z";
	public static final String Fulfillment_Restfull_RestCheckUniqueFulfillmentDescriptionShort="/RestCheckUniqueFulfillmentDescriptionShort";
	public static final String Fulfillment_Restfull_RestCheckUniqueFulfillmentDescriptionLong="/RestCheckUniqueFulfillmentDescriptionLong";
	public static final String LocationNotCreated="LocationNotCreated";
	public static final String OnlyWizardCreated="OnlyWizardCreated";
	public static final String RequestMapping_Infohelp_Search_LookUp="/InfoHelpSearch_lookup";
	public static final String LocationWizard_ProcedureCall_TENANT="INTENANT";
	public static final String LocationWizard_ProcedureCall_FULFILLMENTCENTERID="INFULFILLMENTCENTERID";
	public static final String LocationWizard_ProcedureCall_AREA="INAREA";
	public static final String LocationWizard_ProcedureCall_COMPANY="INCOMPANY";
	public static final String LocationWizard_ProcedureCall_WIZARDID="INWIZARDID";
	public static final String LocationWizard_ProcedureCall_WIZARDCONTROLNUMBER="INWIZARDCONTROLNUMBER";
	public static final String LocationWizard_ProcedureCall_TENANT1="INTENANT1";
	public static final String LocationWizard_ProcedureCall_FULFILLMENTCENTERID1="INFULFILLMENTCENTERID1";
	public static final String LocationWizard_ProcedureCall_AREA1="INAREA1";
	public static final String LocationWizard_ProcedureCall_COMPANY1="INCOMPANY1";


	/**
	 * Constant declaration for File Destination
	 * 
	 */
	public static final String SetUpDateH="SetUpDateH";										
	public static final String LastActivityDateStringH="LastActivityDateStringH";
	public static final String InvalidAttemptDateH="InvalidAttemptDateH";
	public static final String LocationLabelPrintFullfillmentCenterListObj="LocationLabelPrintFullfillmentCenterListObj";
	public static final char BlankSpace=' ';
	public static final String FNAME="fname";
	public static final String MNAME="mname";
	public static final String LNAME="lname";
	public static final String BACKSTATUS="backStatus"; 
	public static final String GeneralCode_MenuExceutionpath="General_code_identificationdata?MenuValue=";
	public static final String GeneralCodes_Restfull_MenuOption="StrMenuOptionName";
	public static final String GeneralCodes_Restfull_RestMenuOptionValueSet="/RestMenuOptionValueSet";
	public static final String Security_R_P_FullName ="FullName";
	public static final String Security_V_FullName="FullName";
	public static final String YOUDONTHAVEACCESS = "You don't have access";
	/**
	 * Database fields constant for all tables 
	 * That used in hibernate tables fields mapping
	 * */
	/**
	 * Constant for SECURITY_AUTHORIZATIONS table name and fields name
	 * 
	 */
	public static final String TableName_SecurityAuthorizations="SECURITY_AUTHORIZATIONS";
	public static final String DataBase_SecurityAuthorizations_UserId="User_Id";
	public static final String DataBase_SecurityAuthorizations_Password="Password";
	public static final String DataBase_SecurityAuthorizations_PasswordDate="Password_Date";
	public static final String DataBase_SecurityAuthorizations_Tenant="Tenant_Id";
	public static final String DataBase_SecurityAuthorizations_Company="Default_Company_Id";
	public static final String DataBase_SecurityAuthorizations_TeamMember="Team_Member_Id";
	public static final String DataBase_SecurityAuthorizations_SecurityQuestion1="Security_Question1";
	public static final String DataBase_SecurityAuthorizations_SecurityQuestionAnswer1="Security_Question_Answer1";
	public static final String DataBase_SecurityAuthorizations_SecurityQuestion2="Security_Question2";
	public static final String DataBase_SecurityAuthorizations_SecurityQuestionAnswer2="Security_Question_Answer2";
	public static final String DataBase_SecurityAuthorizations_SecurityQuestion3="Security_Question3";
	public static final String DataBase_SecurityAuthorizations_SecurityQuestionAnswer3="Security_Question_Answer3";
	public static final String DataBase_SecurityAuthorizations_LastPassword1="Last_Password1";
	public static final String DataBase_SecurityAuthorizations_LastPassword2="Last_Password2";
	public static final String DataBase_SecurityAuthorizations_LastPassword3="Last_Password3";
	public static final String DataBase_SecurityAuthorizations_LastPassword4="Last_Password4";
	public static final String DataBase_SecurityAuthorizations_LastPassword5="Last_Password5";
	public static final String DataBase_SecurityAuthorizations_NumberAttempts="Number_Attempts";
	public static final String DataBase_SecurityAuthorizations_InvalidAttemptDate="Invalid_Attempt_Date";
	public static final String DataBase_SecurityAuthorizations_Status="Status";
	public static final String DataBase_SecurityAuthorizations_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_SecurityAuthorizations_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_SecurityAuthorizations_IsReset="Is_Reset";
	/**
	 * Constant for FULFILLMENT_CENTERS table name and fields name
	 * 
	 */
	public static final String TableName_FulfillmentCenters="FULFILLMENT_CENTERS";
	public static final String DataBase_FulfillmentCenters_Tenant="Tenant_Id";
	public static final String DataBase_FulfillmentCenters_FulfillmentCenter="Fulfillment_Center_ID";
	public static final String DataBase_FulfillmentCenters_Name20="Name20";
	public static final String DataBase_FulfillmentCenters_Name50="Name50";
	public static final String DataBase_FulfillmentCenters_AddressLine1="Address_Line1";
	public static final String DataBase_FulfillmentCenters_AddressLine2="Address_Line2";
	public static final String DataBase_FulfillmentCenters_AddressLine3="Address_Line3";
	public static final String DataBase_FulfillmentCenters_AddressLine4="Address_Line4";
	public static final String DataBase_FulfillmentCenters_City="City";
	public static final String DataBase_FulfillmentCenters_StateCode="State_Code";
	public static final String DataBase_FulfillmentCenters_CountryCode="Country_Code";
	public static final String DataBase_FulfillmentCenters_ZipCode="Zip_Code";
	public static final String DataBase_FulfillmentCenters_ContactName1="Contact_Name1";
	public static final String DataBase_FulfillmentCenters_ContactPhone1="Contact_Phone1";
	public static final String DataBase_FulfillmentCenters_ContactExtension1="Contact_Extension1";
	public static final String DataBase_FulfillmentCenters_ContactCell1="Contact_Cell1";
	public static final String DataBase_FulfillmentCenters_ContactFax1="Contact_Fax1";
	public static final String DataBase_FulfillmentCenters_ContactEmail1="Contact_Email1";
	public static final String DataBase_FulfillmentCenters_ContactName2="Contact_Name2";
	public static final String DataBase_FulfillmentCenters_ContactPhone2="Contact_Phone2";
	public static final String DataBase_FulfillmentCenters_ContactExtension2="Contact_Extension2";
	public static final String DataBase_FulfillmentCenters_ContactCell2="Contact_Cell2";
	public static final String DataBase_FulfillmentCenters_ContactFax2="Contact_Fax2";
	public static final String DataBase_FulfillmentCenters_ContactEmail2="Contact_Email2";
	public static final String DataBase_FulfillmentCenters_ContactName3="Contact_Name3";
	public static final String DataBase_FulfillmentCenters_ContactPhone3="Contact_Phone3";
	public static final String DataBase_FulfillmentCenters_ContactExtension3="Contact_Extension3";
	public static final String DataBase_FulfillmentCenters_ContactCell3="Contact_Cell3";
	public static final String DataBase_FulfillmentCenters_ContactFax3="Contact_Fax3";
	public static final String DataBase_FulfillmentCenters_ContactEmail3="Contact_Email3";
	public static final String DataBase_FulfillmentCenters_ContactName4="Contact_Name4";
	public static final String DataBase_FulfillmentCenters_ContactPhone4="Contact_Phone4";
	public static final String DataBase_FulfillmentCenters_ContactExtension4="Contact_Extension4";
	public static final String DataBase_FulfillmentCenters_ContactCell4="Contact_Cell4";
	public static final String DataBase_FulfillmentCenters_ContactFax4="Contact_Fax4";
	public static final String DataBase_FulfillmentCenters_ContactEmail4="Contact_Email4";
	public static final String DataBase_FulfillmentCenters_MainFax="Main_Fax";
	public static final String DataBase_FulfillmentCenters_MainEmail="Main_Email";
	public static final String DataBase_FulfillmentCenters_SetUpSelected="SetUp_Selected";
	public static final String DataBase_FulfillmentCenters_SetUpDate="SetUp_Date";
	public static final String DataBase_FulfillmentCenters_SetUpBy="SetUp_By";
	public static final String DataBase_FulfillmentCenters_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_FulfillmentCenters_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_FulfillmentCenters_LastActivityTask="Last_Activity_Task";
	public static final String DataBase_FulfillmentCenters_TimeZone="TimeZone";
	public static final String DataBase_FulfillmentCenters_STATUS="STATUS";
	/**
	 * Constant for LANGUAGES table name and fields name
	 * 
	 */
	public static final String TableName_Languages="LANGUAGES";
	public static final String DataBase_Languages_Language="Language";
	public static final String DataBase_Languages_KeyPhrase="Key_Phrase";
	public static final String DataBase_Languages_Translation="Translation";
	public static final String DataBase_Languages_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_Languages_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_Languages_ObjectFieldCode="Object_Field_Code";
	public static final String DataBase_Languages_ObjectCode="Object_Code";
	/**
	 * Constant for MENU_MESSAGES table name and fields name
	 * 
	 */
	public static final String TableName_MenuMessages="MENU_MESSAGES";
	public static final String DataBase_MenuMessages_Tenant="Tenant_Id";
	public static final String DataBase_MenuMessages_Company="Company_Id";
	public static final String DataBase_MenuMessages_Status="Status";
	public static final String DataBase_MenuMessages_TicketTapeMessage="Ticket_Tape_Message";
	public static final String DataBase_MenuMessages_Message="Message";
	public static final String DataBase_MenuMessages_Message_ID="Message_ID";
	public static final String DataBase_MenuMessages_Message3="Message3";
	public static final String DataBase_MenuMessages_Message4="Message4";
	public static final String DataBase_MenuMessages_Message5="Message5";
	public static final String DataBase_MenuMessages_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_MenuMessages_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_MenuMessages_StartDate="Start_Date";
	public static final String DataBase_MenuMessages_EndDate="End_Date";
	/**
	 * Constant for WIZARD_LOCATIONS table name and fields name
	 * 
	 */
	public static final String TableName_Location_Wizard="WIZARD_LOCATIONS";
	public static final String DataBase_GeneralCodes_Tenant="Tenant_Id";
	public static final String DataBase_GeneralCodes_Company="Company_Id";
	public static final String DataBase_GeneralCodes_Application="Application_ID";
	public static final String DataBase_GeneralCodes_GeneralCodeID="General_Code_ID";
	public static final String DataBase_GeneralCodes_GeneralCode="General_Code";
	public static final String DataBase_GeneralCodes_SystemUse="System_Use";
	public static final String DataBase_GeneralCodes_Description20="Description20";
	public static final String DataBase_GeneralCodes_Description50="Description50";
	public static final String DataBase_GeneralCodes_MenuOptionName="Menu_Option_Name";
	public static final String DataBase_GeneralCodes_ControlNumber01="Control_Number01";
	public static final String DataBase_GeneralCodes_ControlNumber02="Control_Number02";
	public static final String DataBase_GeneralCodes_ControlNumber03="Control_Number03";
	public static final String DataBase_GeneralCodes_ControlNumber04="Control_Number04";
	public static final String DataBase_GeneralCodes_ControlNumber05="Control_Number05";
	public static final String DataBase_GeneralCodes_ControlNumber06="Control_Number06";
	public static final String DataBase_GeneralCodes_ControlNumber07="Control_Number07";
	public static final String DataBase_GeneralCodes_ControlNumber08="Control_Number08";
	public static final String DataBase_GeneralCodes_ControlNumber09="Control_Number09";
	public static final String DataBase_GeneralCodes_ControlNumber10="Control_Number10";
	public static final String DataBase_GeneralCodes_Control01Description="Control01_Description";
	public static final String DataBase_GeneralCodes_Control01Value="Control01_Value";
	public static final String DataBase_GeneralCodes_Control02Description="Control02_Description";
	public static final String DataBase_GeneralCodes_Control02Value="Control02_Value";
	public static final String DataBase_GeneralCodes_Control03Description="Control03_Description";
	public static final String DataBase_GeneralCodes_Control03Value="Control03_Value";
	public static final String DataBase_GeneralCodes_Control04Description="Control04_Description";
	public static final String DataBase_GeneralCodes_Control04Value="Control04_Value";
	public static final String DataBase_GeneralCodes_Control05Description="Control05_Description";
	public static final String DataBase_GeneralCodes_Control05Value="Control05_Value";
	public static final String DataBase_GeneralCodes_Control06Description="Control06_Description";
	public static final String DataBase_GeneralCodes_Control06Value="Control06_Value";
	public static final String DataBase_GeneralCodes_Control07Description="Control07_Description";
	public static final String DataBase_GeneralCodes_Control07Value="Control07_Value";
	public static final String DataBase_GeneralCodes_Control08Description="Control08_Description";
	public static final String DataBase_GeneralCodes_Control08Value="Control08_Value";
	public static final String DataBase_GeneralCodes_Control09Description="Control09_Description";
	public static final String DataBase_GeneralCodes_Control09Value="Control09_Value";
	public static final String DataBase_GeneralCodes_Control10Description="Control10_Description";
	public static final String DataBase_GeneralCodes_Control10Value="Control10_Value";
	public static final String DataBase_GeneralCodes_Helpline="Help_line";
	public static final String DataBase_GeneralCodes_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_GeneralCodes_LastActivityTeamMember="Last_Activity_Team_Member";
	/**
	 * Constant for MENU_OPTIONS table name and fields name
	 * 
	 */
	public static final String TableName_Menu_Options="MENU_OPTIONS";
	public static final String DataBase_MenuOptions_MenuOption="Menu_Option";
	public static final String DataBase_MenuOptions_Tenant="Tenant_Id";
	public static final String DataBase_MenuOptions_Application="Application_ID";
	public static final String DataBase_MenuOptions_ApplicationSub="Application_Sub";
	public static final String DataBase_MenuOptions_MenuType="Menu_type";
	public static final String DataBase_MenuOptions_Description20="Description20";
	public static final String DataBase_MenuOptions_Description50="Description50";
	public static final String DataBase_MenuOptions_Helpline="Help_line";
	public static final String DataBase_MenuOptions_Execution="Execution";
	public static final String DataBase_MenuOptions_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_MenuOptions_LastActivityTeamMember="Last_Activity_Team_Member";
	//for class Tenants
	/**
	 * Constant for Tenants table name and fields name
	 * 
	 */
	public static final String TableName_Tenants="TENANTS";
	public static final String DataBase_Tenants_Tenant="Tenant_Id";
	public static final String DataBase_Tenants_Name20="Name20";
	public static final String DataBase_Tenants_Name50="Name50";
	public static final String DataBase_Tenants_AddressLine1="Address_Line1";
	public static final String DataBase_Tenants_AddressLine2="Address_Line2";
	public static final String DataBase_Tenants_AddressLine3="Address_Line3";
	public static final String DataBase_Tenants_AddressLine4="Address_Line4";
	public static final String DataBase_Tenants_City="City";
	public static final String DataBase_Tenants_StateCode="State_Code";
	public static final String DataBase_Tenants_CountryCode="Country_Code";
	public static final String DataBase_Tenants_ZipCode="Zip_Code";
	public static final String DataBase_Tenants_ContactName1="Contact_Name1";
	public static final String DataBase_Tenants_ContactPhone1="Contact_Phone1";
	public static final String DataBase_Tenants_ContactExtension1="Contact_Extension1";
	public static final String DataBase_Tenants_ContactCell1="Contact_Cell1";
	public static final String DataBase_Tenants_ContactFax1="Contact_Fax1";
	public static final String DataBase_Tenants_ContactEmail1="Contact_Email1";
	public static final String DataBase_Tenants_ContactName2="Contact_Name2";
	public static final String DataBase_Tenants_ContactPhone2="Contact_Phone2";
	public static final String DataBase_Tenants_ContactExtension2="Contact_Extension2";
	public static final String DataBase_Tenants_ContactCell2="Contact_Cell2";
	public static final String DataBase_Tenants_ContactFax2="Contact_Fax2";
	public static final String DataBase_Tenants_ContactEmail2="Contact_Email2";
	public static final String DataBase_Tenants_ContactName3="Contact_Name3";
	public static final String DataBase_Tenants_ContactPhone3="Contact_Phone3";
	public static final String DataBase_Tenants_ContactExtension3="Contact_Extension3";
	public static final String DataBase_Tenants_ContactCell3="Contact_Cell3";
	public static final String DataBase_Tenants_ContactFax3="Contact_Fax3";
	public static final String DataBase_Tenants_ContactEmail3="Contact_Email3";
	public static final String DataBase_Tenants_ContactName4="Contact_Name4";
	public static final String DataBase_Tenants_ContactPhone4="Contact_Phone4";
	public static final String DataBase_Tenants_ContactExtension4="Contact_Extension4";
	public static final String DataBase_Tenants_ContactCell4="Contact_Cell4";
	public static final String DataBase_Tenants_ContactFax4="Contact_Fax4";
	public static final String DataBase_Tenants_ContactEmail4="Contact_Email4";
	public static final String DataBase_Tenants_MainFax="Main_Fax";
	public static final String DataBase_Tenants_MainEmail="Main_Email";
	public static final String DataBase_Tenants_PasswordExpirationDays="Password_Expiration_Days";
	public static final String DataBase_Tenants_Language="Language";
	public static final String DataBase_Tenants_ThemeMobile="Theme_Mobile";
	public static final String DataBase_Tenants_ThemeRF="Theme_RF";
	public static final String DataBase_Tenants_ThemeFullDisplay="Theme_Full_Display";	
	public static final String DataBase_Tenants_ModelTenant="Model_Tenant";
	public static final String DataBase_Tenants_SetupDate="Setup_Date";
	public static final String DataBase_Tenants_SetupBy="Setup_By";
	public static final String DataBase_Tenants_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_Tenants_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_Tenants_LastActivityTask="Last_Activity_Task";
	/**
	 * Constant for TEAM_MEMBERS table name and fields name
	 * 
	 */
	public static final String TableName_TeamMembers="TEAM_MEMBERS";
	public static final String DataBase_TeamMembers_Tenant="Tenant_Id";
	public static final String DataBase_TeamMembers_TeamMember ="Team_Member_Id";
	public static final String DataBase_TeamMembers_FULFILLMENT_CENTER_ID="Fulfillment_Center_ID";
	public static final String DataBase_TeamMembers_Department="Department";
	public static final String DataBase_TeamMembers_LastName="Last_Name";
	public static final String DataBase_TeamMembers_FirstName="First_Name";
	public static final String DataBase_TeamMembers_MiddleName ="Middle_Name";
	public static final String DataBase_TeamMembers_AddressLine1="Address_Line1";
	public static final String DataBase_TeamMembers_AddressLine2="Address_Line2";
	public static final String DataBase_TeamMembers_AddressLine3="Address_Line3";
	public static final String DataBase_TeamMembers_AddressLine4="Address_Line4";
	public static final String DataBase_TeamMembers_City="City";
	public static final String DataBase_TeamMembers_StateCode="State_Code";
	public static final String DataBase_TeamMembers_CountryCode="Country_Code";
	public static final String DataBase_TeamMembers_ZipCode="Zip_Code";
	public static final String DataBase_TeamMembers_WorkPhone="Work_Phone";
	public static final String DataBase_TeamMembers_WorkExtension="Work_Extension";
	public static final String DataBase_TeamMembers_WorkCell="Work_Cell";
	public static final String DataBase_TeamMembers_WorkFax="Work_Fax";
	public static final String DataBase_TeamMembers_HomePhone="Home_Phone";
	public static final String DataBase_TeamMembers_Cell="Cell";
	public static final String DataBase_TeamMembers_EmergencyContactName="Emergency_Contact_Name";
	public static final String DataBase_TeamMembers_EmergencyContactHomePhone="Emergency_Contact_Home_Phone";
	public static final String DataBase_TeamMembers_EmergencyContactCell="Emergency_Contact_Cell";
	public static final String DataBase_TeamMembers_ShiftCode="Shift_Code";
	public static final String DataBase_TeamMembers_SystemUse="System_Use";
	public static final String DataBase_TeamMembers_AuthorityProfile="Authority_Profile";
	public static final String DataBase_TeamMembers_TaskProfile="Task_Profile";
	public static final String DataBase_TeamMembers_Menu_Profile_Glass="Menu_Profile_Glass";
	public static final String DataBase_TeamMembers_Menu_Profile_Tablet="Menu_Profile_Tablet";
	public static final String DataBase_TeamMembers_Menu_Profile_Mobile="Menu_Profile_Mobile";
	public static final String DataBase_TeamMembers_Menu_Profile_Station="Menu_Profile_Station";
	public static final String DataBase_TeamMembers_Menu_Profile_RF="Menu_Profile_RF";
	public static final String DataBase_TeamMembers_EquipmentCertification="Equipment_Certification";
	public static final String DataBase_TeamMembers_Language="Language";
	public static final String DataBase_TeamMembers_StartDate="Start_Date";
	public static final String DataBase_TeamMembers_LastDate="Last_Date";
	public static final String DataBase_TeamMembers_SetupDate="Setup_Date";
	public static final String DataBase_TeamMembers_SetupBy="Setup_By";
	public static final String DataBase_TeamMembers_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_TeamMembers_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_TeamMembers_CurrentStatus="Current_Status";
	public static final String DataBase_TeamMembers_PersonalEmailAddress="Personal_Email_Address";
	public static final String DataBase_TeamMembers_EMERGENCYEMAIL="Emergency_Email";
	public static final String DataBase_TeamMembers_WORKEMAIL="Work_Email";
	/**
	 * Constant for TEAM_MEMBER_COMPANIES table name and fields name
	 * 
	 */
	public static final String TableName_TeamMembersCompanies="TEAM_MEMBER_COMPANIES";
	public static final String DataBase_TeamMembersCompanies_Tenant="Tenant_Id";
	public static final String DataBase_TeamMembersCompanies_TeamMember ="Team_Member_Id";
	public static final String DataBase_TeamMembersCompanies_Company="Company_Id";
	public static final String DataBase_TeamMembersCompanies_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_TeamMembersCompanies_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_TeamMembersCompanies_CurrentStatus="Current_Status";
	/**
	 * Constant for TEAM_MEMBERS table name and fields name
	 * 
	 */
	public static final String TableName_TEAM_MEMBERS = "TEAM_MEMBERS";
	public static final String DataBase_TEAM_MEMBERS_Tenant = "Tenant_Id";
	public static final String DataBase_TEAM_MEMBERS_TeamMember = "Team_Member_Id";
	public static final String DataBase_TEAM_MEMBERS_FulfillmentCenter = "Fulfillment_Center_ID";
	public static final String DataBase_TEAM_MEMBERS_Department = "Department";
	public static final String DataBase_TEAM_MEMBERS_LastName = "Last_Name";
	public static final String DataBase_TEAM_MEMBERS_FirstName = "First_Name";
	public static final String DataBase_TEAM_MEMBERS_MiddleName = "Middle_Name ";
	public static final String DataBase_TEAM_MEMBERS_AddressLine1 = "Address_Line1";
	public static final String DataBase_TEAM_MEMBERS_AddressLine2 = "Address_Line2";
	public static final String DataBase_TEAM_MEMBERS_AddressLine3 = "Address_Line3";
	public static final String DataBase_TEAM_MEMBERS_AddressLine4 = "Address_Line4";
	public static final String DataBase_TEAM_MEMBERS_City = "City";
	public static final String DataBase_TEAM_MEMBERS_StateCode = "State_Code";
	public static final String DataBase_TEAM_MEMBERS_CountryCode = "Country_Code";
	public static final String DataBase_TEAM_MEMBERS_ZipCode = "Zip_Code";
	public static final String DataBase_TEAM_MEMBERS_WorkPhone = "Work_Phone";
	public static final String DataBase_TEAM_MEMBERS_WorkExtension = "Work_Extension";
	public static final String DataBase_TEAM_MEMBERS_WorkCell = "Work_Cell";
	public static final String DataBase_TEAM_MEMBERS_WorkFax = "Work_Fax";
	public static final String DataBase_TEAM_MEMBERS_HomePhone = "Home_Phone";
	public static final String DataBase_TEAM_MEMBERS_Cell = "Cell";
	public static final String DataBase_TEAM_MEMBERS_EmergencyContactName = "Emergency_Contact_Name";
	public static final String DataBase_TEAM_MEMBERS_EmergencyContactHomePhone = "Emergency_Contact_Home_Phone";
	public static final String DataBase_TEAM_MEMBERS_EmergencyContactCell = "Emergency_Contact_Cell";
	public static final String DataBase_TEAM_MEMBERS_ShiftCode = "Shift_Code";
	public static final String DataBase_TEAM_MEMBERS_SystemUse = "System_Use";
	public static final String DataBase_TEAM_MEMBERS_AuthorityProfile = "Authority_Profile";
	public static final String DataBase_TEAM_MEMBERS_TaskProfile = "Task_Profile";
	public static final String DataBase_TEAM_MEMBERS_Menu_Profile_Glass = "Menu_Profile_Glass";
	public static final String DataBase_TEAM_MEMBERS_Menu_Profile_Tablet = "Menu_Profile_Tablet";
	public static final String DataBase_TEAM_MEMBERS_Menu_Profile_Mobile = "Menu_Profile_Mobile";
	public static final String DataBase_TEAM_MEMBERS_Menu_Profile_Station = "Menu_Profile_Station";
	public static final String DataBase_TEAM_MEMBERS_Menu_Profile_RF = "Menu_Profile_RF";
	public static final String DataBase_TEAM_MEMBERS_EquipmentCertification = "Equipment_Certification";
	public static final String DataBase_TEAM_MEMBERS_Langage = "LANGUAGE";
	public static final String DataBase_TEAM_MEMBERS_StartDate = "Start_Date";
	public static final String DataBase_TEAM_MEMBERS_LastDate = "Last_Date";
	public static final String DataBase_TEAM_MEMBERS_SetupDate = "Setup_Date";
	public static final String DataBase_TEAM_MEMBERS_SetupBy = "Setup_By";
	public static final String DataBase_TEAM_MEMBERS_LastActivityDate = "Last_Activity_Date";
	public static final String DataBase_TEAM_MEMBERS_LastActivityTeamMember = "Last_Activity_Team_Member";
	public static final String DataBase_TEAM_MEMBERS_CurrentStatus = "Current_Status";
	public static final String DataBase_TEAM_MEMBERS_WorkEmail = "Work_Email";
	public static final String DataBase_TEAM_MEMBERS_EmergencyEmail = "Emergency_Email";
	public static final String DataBase_TEAM_MEMBERS_Personal_Email_Address = "PERSONAL_EMAIL_ADDRESS";
	/**
	 * Constant for GENERAL_CODES table name and fields name
	 * 
	 */
	public static final String TableName_GENERAL_CODES = "GENERAL_CODES";
	public static final String DataBase_GENERAL_CODES_Tenant = "Tenant_Id";
	public static final String DataBase_GENERAL_CODES_Company = "Company_Id";
	public static final String DataBase_GENERAL_CODES_Application = "Application_ID";
	public static final String DataBase_GENERAL_CODES_GeneralCodeID = "General_Code_ID";
	public static final String DataBase_GENERAL_CODES_GeneralCode = "General_Code";
	public static final String DataBase_GENERAL_CODES_SystemUse = "System_Use";
	public static final String DataBase_GENERAL_CODES_Description20 = "Description20";
	public static final String DataBase_GENERAL_CODES_Description50 = "Description50";
	public static final String DataBase_GENERAL_CODES_MenuOptionName = "Menu_Option_Name";
	public static final String DataBase_GENERAL_CODES_ControlNumber01 = "Control_Number01";
	public static final String DataBase_GENERAL_CODES_ControlNumber02 = "Control_Number02";
	public static final String DataBase_GENERAL_CODES_ControlNumber03 = "Control_Number03";
	public static final String DataBase_GENERAL_CODES_ControlNumber04 = "Control_Number04";
	public static final String DataBase_GENERAL_CODES_ControlNumber05 = "Control_Number05";
	public static final String DataBase_GENERAL_CODES_ControlNumber06 = "Control_Number06";
	public static final String DataBase_GENERAL_CODES_ControlNumber07 = "Control_Number07";
	public static final String DataBase_GENERAL_CODES_ControlNumber08 = "Control_Number08";
	public static final String DataBase_GENERAL_CODES_ControlNumber09 = "Control_Number09";
	public static final String DataBase_GENERAL_CODES_ControlNumber10 = "Control_Number10";
	public static final String DataBase_GENERAL_CODES_Control01Description = "Control01_Description";
	public static final String DataBase_GENERAL_CODES_Control01Value = "Control01_Value";
	public static final String DataBase_GENERAL_CODES_Control02Description = "Control02_Description";
	public static final String DataBase_GENERAL_CODES_Control02Value = "Control02_Value";
	public static final String DataBase_GENERAL_CODES_Control03Description = "Control03_Description";
	public static final String DataBase_GENERAL_CODES_Control03Value = "Control03_Value";
	public static final String DataBase_GENERAL_CODES_Control04Description = "Control04_Description";
	public static final String DataBase_GENERAL_CODES_Control04Value = "Control04_Value";
	public static final String DataBase_GENERAL_CODES_Control05Description = "Control05_Description";
	public static final String DataBase_GENERAL_CODES_Control05Value = "Control05_Value";
	public static final String DataBase_GENERAL_CODES_Control06Description = "Control06_Description";
	public static final String DataBase_GENERAL_CODES_Control06Value = "Control06_Value";
	public static final String DataBase_GENERAL_CODES_Control07Description = "Control07_Description";
	public static final String DataBase_GENERAL_CODES_Control07Value = "Control07_Value";
	public static final String DataBase_GENERAL_CODES_Control08Description = "Control08_Description";
	public static final String DataBase_GENERAL_CODES_Control08Value = "Control08_Value";
	public static final String DataBase_GENERAL_CODES_Control09Description = "Control09_Description";
	public static final String DataBase_GENERAL_CODES_Control09Value = "Control09_Value";
	public static final String DataBase_GENERAL_CODES_Control10Description = "Control10_Description";
	public static final String DataBase_GENERAL_CODES_Control10Value = "Control10_Value";
	public static final String DataBase_GENERAL_CODES_Helpline = "Help_line";
	public static final String DataBase_GENERAL_CODES_LastActivityDate = "Last_Activity_Date";
	public static final String DataBase_GENERAL_CODES_LastActivityTeamMember = "Last_Activity_Team_Member";
	/**
	 * Constant for LOCATION_PROFILES table name and fields name
	 * 
	 */
	public static final String TableName_LOCATION_PROFILES = "LOCATION_PROFILES";
	public static final String DataBase_LOCATION_PROFILES_Tenant = "Tenant_Id";
	public static final String DataBase_LOCATION_PROFILES_FulfillmentCenter = "Fulfillment_Center_ID";
	public static final String DataBase_LOCATION_PROFILES_Company = "Company_Id";
	public static final String DataBase_LOCATION_PROFILES_ProfileGroup = "Profile_Group";
	public static final String DataBase_LOCATION_PROFILES_Profile = "Profile";
	public static final String DataBase_LOCATION_PROFILES_Description20 = "Description20";
	public static final String DataBase_LOCATION_PROFILES_Description50 = "Description50";
	public static final String DataBase_LOCATION_PROFILES_Type = "Location_Type";
	public static final String DataBase_LOCATION_PROFILES_LocationHeight = "Location_Height";
	public static final String DataBase_LOCATION_PROFILES_LocationWidth = "Location_Width";
	public static final String DataBase_LOCATION_PROFILES_LocationDepth = "Location_Depth";
	public static final String DataBase_LOCATION_PROFILES_LocationWeightCapacity = "Location_Weight_Capacity";
	public static final String DataBase_LOCATION_PROFILES_LocationHeightCapacity = "Location_Height_Capacity";
	public static final String DataBase_LOCATION_PROFILES_NumberPalletsFit = "Number_Pallets_Fit";
	public static final String DataBase_LOCATION_PROFILES_NumberFloorLocations = "Number_Floor_Locations";
	public static final String DataBase_LOCATION_PROFILES_AllocationAllowable = "Allocation_Allowable";
	public static final String DataBase_LOCATION_PROFILES_LocationCheck = "Location_Check ";
	public static final String DataBase_LOCATION_PROFILES_FreeLocationWhenZero = "Free_Location_When_Zero";
	public static final String DataBase_LOCATION_PROFILES_FreePrimeDays = "Free_Prime_Days";
	public static final String DataBase_LOCATION_PROFILES_MutipleProductsInLocation = "Mutiple_Products_In_Location";
	public static final String DataBase_LOCATION_PROFILES_StorageType = "Storage_Type";
	public static final String DataBase_LOCATION_PROFILES_Slotting = "Slotting ";
	public static final String DataBase_LOCATION_PROFILES_CCActivityPoint = "CC_Activity_Point";
	public static final String DataBase_LOCATION_PROFILES_CCHighValueAmount = "CC_High_Value_Amount";
	public static final String DataBase_LOCATION_PROFILES_CCHighValueFactor = "CC_High_Value_Factor";
	public static final String DataBase_LOCATION_PROFILES_LastUsedDate = "Last_Used_Date";
	public static final String DataBase_LOCATION_PROFILES_LastUsedTeamMember = "Last_Used_Team_Member";
	public static final String DataBase_LOCATION_PROFILES_LastActivityDate = "Last_Activity_Date";
	public static final String DataBase_LOCATION_PROFILES_LastActivityTeamMember = "Last_Activity_Team_Member";
	/**
	 * Constant for INVENTORY_LEVELS table name and fields name
	 * 
	 */
	public static final String TableName_INVENTORY_LEVELS = "INVENTORY_LEVELS";
	public static final String DataBase_INVENTORY_LEVELS_Tenant = "Tenant_Id";
	public static final String DataBase_INVENTORY_LEVELS_FulfillmentCenter = "Fulfillment_Center_ID";
	public static final String DataBase_INVENTORY_LEVELS_Company = "Company_Id";
	public static final String DataBase_INVENTORY_LEVELS_Area = "Area";
	public static final String DataBase_INVENTORY_LEVELS_Location = "Location";
	public static final String DataBase_INVENTORY_LEVELS_Product = "Product";
	public static final String DataBase_INVENTORY_LEVELS_Quality = "Quality";
	public static final String DataBase_INVENTORY_LEVELS_Lot = "Lot";
	public static final String DataBase_INVENTORY_LEVELS_Serial01 = "Serial01";
	public static final String DataBase_INVENTORY_LEVELS_Serial02 = "Serial02";
	public static final String DataBase_INVENTORY_LEVELS_Serial03 = "Serial03";
	public static final String DataBase_INVENTORY_LEVELS_Serial04 = "Serial04";
	public static final String DataBase_INVENTORY_LEVELS_Serial05 = "Serial05";
	public static final String DataBase_INVENTORY_LEVELS_Serial06 = "Serial06";
	public static final String DataBase_INVENTORY_LEVELS_Serial07 = "Serial07";
	public static final String DataBase_INVENTORY_LEVELS_Serial08 = "Serial08";
	public static final String DataBase_INVENTORY_LEVELS_Serial09 = "Serial09";
	public static final String DataBase_INVENTORY_LEVELS_Serial10 = "Serial10";
	public static final String DataBase_INVENTORY_LEVELS_Serial11 = "Serial11";
	public static final String DataBase_INVENTORY_LEVELS_Serial12 = "Serial12";
	public static final String DataBase_INVENTORY_LEVELS_LPN = "LPN";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl01 = "Inventory_Control01";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl02 = "Inventory_Control02";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl03 = "Inventory_Control03";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl04 = "Inventory_Control04";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl05 = "Inventory_Control05";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl06 = "Inventory_Control06";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl07 = "Inventory_Control07";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl08 = "Inventory_Control08";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl09 = "Inventory_Control09";
	public static final String DataBase_INVENTORY_LEVELS_InventroryControl10 = "Inventory_Control10";
	public static final String DataBase_INVENTORY_LEVELS_Quantity = "Quantity";
	public static final String DataBase_INVENTORY_LEVELS_UnitofMeasureCode = "Unit_of_Measure_Code";
	public static final String DataBase_INVENTORY_LEVELS_UnitofMeasureQty = "Unit_of_Measure_Qty";
	public static final String DataBase_INVENTORY_LEVELS_DateControl = "Date_Control";
	public static final String DataBase_INVENTORY_LEVELS_ProductValue = "Product_Value";
	public static final String DataBase_INVENTORY_LEVELS_DateReceived = "Date_Received";
	public static final String DataBase_INVENTORY_LEVELS_LastActivityDate = "Last_Activity_Date";
	public static final String DataBase_INVENTORY_LEVELS_LastActivityEmployee = "Last_Activity_Employee";
	public static final String DataBase_INVENTORY_LEVELS_LastActivityTask = "Last_Activity_Task";
	public static final String DataBase_INVENTORY_LEVELS_HoldCode = "Hold_Code";
	public static final String DataBase_INVENTORY_LEVELS_SystemCode = "System_Code";
	/**
	 * Constant for INVENTORY_TRANSACTIONS table name and fields name
	 * 
	 */
	public static final String TableName_INVENTORY_TRANSACTIONS = "INVENTORY_TRANSACTIONS";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Tenant = "Tenant_Id";
	public static final String DataBase_INVENTORY_TRANSACTIONS_FulfillmentCenter = "Fulfillment_Center_ID";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Company = "Company_Id";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Area = "Area";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Location = "Location";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Product = "Product";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Quality = "Quality";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Lot = "Lot";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Serial = "Serial";
	public static final String DataBase_INVENTORY_TRANSACTIONS_LPN = "LPN";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl01 = "Inventory_Control01";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl02 = "Inventory_Control02";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl03 = "Inventory_Control03";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl04 = "Inventory_Control04";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl05 = "Inventory_Control05";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl06 = "Inventory_Control06";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl07 = "Inventory_Control07";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl08 = "Inventory_Control08";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl09 = "Inventory_Control09";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventroryControl10 = "Inventory_Control10";
	public static final String DataBase_INVENTORY_TRANSACTIONS_QuantityTransaction = "Quantity_Transaction";
	public static final String DataBase_INVENTORY_TRANSACTIONS_QuantityBefore = "Quantity_Before";
	public static final String DataBase_INVENTORY_TRANSACTIONS_QuantityAfter = "Quantity_After";
	public static final String DataBase_INVENTORY_TRANSACTIONS_CasePackQty = "Case_Pack_Qty";
	public static final String DataBase_INVENTORY_TRANSACTIONS_DateControl = "Date_Control";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventoryPickSequence = "Inventory_Pick_Sequence";
	public static final String DataBase_INVENTORY_TRANSACTIONS_InventoryValue = "Inventory_Value";
	public static final String DataBase_INVENTORY_TRANSACTIONS_WorkType = "Work_Type";
	public static final String DataBase_INVENTORY_TRANSACTIONS_WorkControlNumber = "Work_Control_Number";
	public static final String DataBase_INVENTORY_TRANSACTIONS_Reference = "Reference";
	public static final String DataBase_INVENTORY_TRANSACTIONS_PurchaseOrderNumber = "Purchase_Order_Number";
	public static final String DataBase_INVENTORY_TRANSACTIONS_LastActivityDate = "Last_Activity_Date";
	public static final String DataBase_INVENTORY_TRANSACTIONS_LastActivityTeamMember = "Last_Activity_Team_Member";
	public static final String DataBase_INVENTORY_TRANSACTIONS_LastActivityTask = "Last_Activity_Task";
	/**
	 * Constant for LOCATIONS table name and fields name
	 * 
	 */
	public static final String TableName_LOCATIONS = "LOCATIONS";
	public static final String DataBase_LOCATIONS_Tenant = "Tenant_Id";
	public static final String DataBase_LOCATIONS_FulfillmentCenter = "Fulfillment_Center_ID";
	public static final String DataBase_LOCATIONS_Company = "Company_Id";
	public static final String DataBase_LOCATIONS_Area = "Area";
	public static final String DataBase_LOCATIONS_Location = "Location";
	public static final String DataBase_LOCATIONS_Description20 = "Description20";
	public static final String DataBase_LOCATIONS_Description50 = "Description50";
	public static final String DataBase_LOCATIONS_WorkGroupZone = "Work_Group_Zone";
	public static final String DataBase_LOCATIONS_WorkZone = "Work_Zone";
	public static final String DataBase_LOCATIONS_LocationType = "Location_Type";
	public static final String DataBase_LOCATIONS_LocationProfile = "Location_Profile";
	public static final String DataBase_LOCATIONS_WizardID = "Wizard_ID";
	public static final String DataBase_LOCATIONS_WizardControlNumber = "Wizard_Control_Number";
	public static final String DataBase_LOCATIONS_LocationHeight = "Location_Height";
	public static final String DataBase_LOCATIONS_LocationWidth = "Location_Width";
	public static final String DataBase_LOCATIONS_LocationDepth = "Location_Depth";
	public static final String DataBase_LOCATIONS_LocationWeightCapacity = "Location_Weight_Capacity";
	public static final String DataBase_LOCATIONS_LocationHeightCapacity = "Location_Height_Capacity";
	public static final String DataBase_LOCATIONS_NumberPalletsFit = "Number_Pallets_Fit";
	public static final String DataBase_LOCATIONS_NumberFloorLocations = "Number_Floor_Locations";
	public static final String DataBase_LOCATIONS_AllocationAllowable = "Allocation_Allowable";
	public static final String DataBase_LOCATIONS_MultipleItems = "Multiple-Items ";
	public static final String DataBase_LOCATIONS_Slotting = "Slotting";
	public static final String DataBase_LOCATIONS_StorageType = "Storage_Type";
	public static final String DataBase_LOCATIONS_CheckDigit = "Check_Digit";
	public static final String DataBase_LOCATIONS_LocationLabelType = "Location_Label_Type";
	public static final String DataBase_LOCATIONS_LocationReprint = "Location_Reprint";
	public static final String DataBase_LOCATIONS_CycleCountPoints = "Cycle_Count_Points";
	public static final String DataBase_LOCATIONS_LastCycleCountDate = "Last_Cycle_Count_Date";
	public static final String DataBase_LOCATIONS_InventoryPickSequence = "Inventory_Pick_Sequence";
	public static final String DataBase_LOCATIONS_AlternateLocation = "Alternate_Location ";
	public static final String DataBase_LOCATIONS_LastActivityDate = "Last_Activity_Date";
	public static final String DataBase_LOCATIONS_LastActivityTeamMember = "Last_Activity_Team_Member";
	public static final String DataBase_LOCATIONS_LastActivityTask = "Last_Activity_Task";
	/**
	 * Constant for COMPANIES table name and fields name
	 * 
	 */
	public static final String TabelName_Companies="COMPANIES";
	public static final String DataBase_Companies_Tenant="Tenant_Id";
	public static final String DataBase_Companies_Company="Company_Id";
	public static final String DataBase_Companies_Name20="Name20";
	public static final String DataBase_Companies_Name50="Name50";
	public static final String DataBase_Companies_AddressLine1="Address_Line1";
	public static final String DataBase_Companies_AddressLine2="Address_Line2";
	public static final String DataBase_Companies_AddressLine3="Address_Line3";
	public static final String DataBase_Companies_AddressLine4="Address_Line4";
	public static final String DataBase_Companies_City="City";
	public static final String DataBase_Companies_StateCode="State_Code";
	public static final String DataBase_Companies_CountryCode="Country_Code";
	public static final String DataBase_Companies_ZipCode="Zip_Code";
	public static final String DataBase_Companies_BillToName20="Bill_To_Name20";
	public static final String DataBase_Companies_BillToName50="Bill_To_Name50";
	public static final String DataBase_Companies_BillToAddressLine1="Bill_To_Address_Line1";
	public static final String DataBase_Companies_BillToAddressLine2="Bill_To_Address_Line2";
	public static final String DataBase_Companies_BillToAddressLine3="Bill_To_Address_Line3";
	public static final String DataBase_Companies_BillToAddressLine4="Bill_To_Address_Line4";
	public static final String DataBase_Companies_BillToFromCity="Bill_To_FromCity";
	public static final String DataBase_Companies_BillToStateCode="Bill_To_State_Code";
	public static final String DataBase_Companies_BillToCountryCode="Bill_To_Country_Code";
	public static final String DataBase_Companies_ContactName1="Contact_Name1";
	public static final String DataBase_Companies_ContactPhone1="Contact_Phone1";
	public static final String DataBase_Companies_ContactExtension1="Contact_Extension1";
	public static final String DataBase_Companies_ContactCell1="Contact_Cell1";
	public static final String DataBase_Companies_ContactFax1="Contact_Fax1";
	public static final String DataBase_Companies_ContactEmail1="Contact_Email1";
	public static final String DataBase_Companies_ContactName2="Contact_Name2";
	public static final String DataBase_Companies_ContactPhone2="Contact_Phone2";
	public static final String DataBase_Companies_ContactExtension2="Contact_Extension2";
	public static final String DataBase_Companies_ContactCell2="Contact_Cell2";
	public static final String DataBase_Companies_ContactFax2="CONTACT_FAX2";
	public static final String DataBase_Companies_ContactEmail2="CONTACT_EMAIL2";
	public static final String DataBase_Companies_ContactName3="CONTACT_NAME3";
	public static final String DataBase_Companies_ContactPhone3="CONTACT_PHONE3";
	public static final String DataBase_Companies_ContactExtension3="Contact_Extension3";
	public static final String DataBase_Companies_ContactCell3="CONTACT_CELL3";
	public static final String DataBase_Companies_ContactFax3="CONTACT_FAX3";
	public static final String DataBase_Companies_ContactEmail3="CONTACT_EMAIL3";
	public static final String DataBase_Companies_ContactName4="CONTACT_NAME4";
	public static final String DataBase_Companies_ContactPhone4="CONTACT_PHONE4";
	public static final String DataBase_Companies_ContactExtension4="Contact_Extension4";
	public static final String DataBase_Companies_ContactCell4="CONTACT_CELL4";
	public static final String DataBase_Companies_ContactFax4="CONTACT_FAX4";
	public static final String DataBase_Companies_ContactEmail4="CONTACT_EMAIL4";
	public static final String DataBase_Companies_MainFax="MAIN_FAX";
	public static final String DataBase_Companies_MainEmail="Main_Email";
	public static final String DataBase_Companies_BillingControlNumber="Billing_Control_Number";
	public static final String DataBase_Companies_ThemeMobile="Theme_Mobile";
	public static final String DataBase_Companies_ThemeRF="Theme_RF";
	public static final String DataBase_Companies_BillToZipCode="Bill_To_ZIP_Code";
	public static final String DataBase_Companies_ThemeFullDisplay="Theme_Full_Display";
	public static final String DataBase_Companies_Logo="Logo";
	public static final String DataBase_Companies_PurchageOrdersRequireApproval="PURCHASE_ORDER_REQ_APPROVAL";
	public static final String DataBase_Companies_Status="Status";
	public static final String DataBase_Companies_SetUpSelected="SetUp_Selected";
	public static final String DataBase_Companies_SetUpDate="SetUp_Date";
	public static final String DataBase_Companies_SetUpBy="SetUp_By";
	public static final String DataBase_Companies_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_Companies_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_Companies_TimeZone="TIMEZONE";
	/**
	 * Constant for MENU_APP_ICONS table name and fields name
	 * 
	 */
	public static final String TabelName_MenuAppIcons="MENU_APP_ICONS";
	public static final String DataBase_MenuAppIcon_Application="APPLICATION_ID";
	public static final String DataBase_MenuAppIcon_AppIcon="APP_ICON";
	public static final String DataBase_MenuAppIcon_Name20="Name20";
	public static final String DataBase_MenuAppIcon_Name50="Name50";
	public static final String DataBase_MenuAppIcon_AppIconAddress="APP_ICON_ADDRESS";
	public static final String DataBase_MenuAppIcon_LastActivityDate="LAST_ACTIVITY_DATE";
	public static final String DataBase_MenuAppIcon_LastActivityTeamMember="LAST_ACTIVITY_TEAM_MEMBER";
	/**
	 * Constant for MENU_PROFILE_ASSIGNMENTS table name and fields name
	 * 
	 */
	public static final String TabelName_MenuProfileAssignment="MENU_PROFILE_ASSIGNMENTS";
	public static final String DataBase_MenuProfileAssignment_Tenant="TENANT_ID";
	public static final String DataBase_MenuProfileAssignment_MenuProfile="MENU_PROFILE";
	public static final String DataBase_MenuProfileAssignment_TeamMember="TEAM_MEMBER_ID";
	public static final String DataBase_MenuProfileAssignment_LastActivityDate="LAST_ACTIVITY_DATE";
	public static final String DataBase_MenuProfileAssignment_LastActivityTeamMember="LAST_ACTIVITY_TEAM_MEMBER";
	/**
	 * Constant for MENU_PROFILE_HEADERS table name and fields name
	 * 
	 */ 
	public static final String TabelName_MenuProfileHeader="MENU_PROFILE_HEADERS";
	public static final String DataBase_MenuProfileHeader_Tenant="TENANT_ID";
	public static final String DataBase_MenuProfileHeader_MenuType="MENU_TYPE";
	public static final String DataBase_MenuProfileHeader_MenuProfile="Menu_profile";
	public static final String DataBase_MenuProfileHeader_Description20="Description20";
	public static final String DataBase_MenuProfileHeader_Description50="Description50";
	public static final String DataBase_MenuProfileHeader_AppIcon="APP_ICON";
	public static final String DataBase_MenuProfileHeader_HelpLine="HELP_LINE";
	public static final String DataBase_MenuProfileHeader_LastActivityDate="LAST_ACTIVITY_DATE";
	public static final String DataBase_MenuProfileHeader_LastActivityTeamMember="LAST_ACTIVITY_TEAM_MEMBER";
	/**
	 * Constant for MENU_PROFILE_OPTIONS table name and fields name
	 * 
	 */
	public static final String TableName_MenuProfileOption="MENU_PROFILE_OPTIONS";
	public static final String DataBase_MenuProfileOption_Tenant="TENANT_ID";
	public static final String DataBase_MENU_PROFILE_COLUMN="MENU_PROFILE";
	public static final String DataBase_MenuProfileOption_MenuOption="MENU_OPTION";
	/**
	 * Constant for INFO_HELPS table name and fields name
	 * 
	 */
	public static final String TableName_Infohelps="INFO_HELPS";
	public static final String DataBase_InfoHelps_InfoHelp="Info_Help";
	public static final String DataBase_InfoHelps_Language="Language";
	public static final String DataBase_InfoHelps_InfoHelpType="Info_Help_Type";
	public static final String DataBase_InfoHelps_Description20="Description20";
	public static final String DataBase_InfoHelps_Description50="Description50";
	public static final String DataBase_InfoHelps_Execution="Execution";
	public static final String DataBase_InfoHelps_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_InfoHelps_LastActivityTeamMember="Last_Activity_Team_Member";
	/**
	 * Constant for MENU_MESSAGES table name and fields name
	 * 
	 */
	public static final String TableName_Menu_Messages="MENU_MESSAGES";
	public static final String DataBase_MenuMessage_Tenant_ID="Tenant_ID";
	public static final String DataBase_MenuMessage_Company_ID="Company_ID";
	public static final String DataBase_MenuMessage_Status="Status";	
	public static final String DataBase_MenuMessage_Message_ID="Message_ID";
	public static final String DataBase_MenuMessages_Ticket_Tape_Message="Ticket_Tape_Message";
	public static final String DataBase_MenuMessages_Start_Date="Start_Date";
	public static final String DataBase_MenuMessages_End_Date="End_Date";
	public static final String DataBase_MenuMessages_Last_Activity_Date="Last_Activity_Date";
	public static final String DataBase_MenuMessages_Last_Activity_Team_Member="Last_Activity_Team_Member";
	/**
	 * Constant for MENU_APP_ICONS table name and fields name
	 * 
	 */
	public static final String TableName_MenuAppIcons="MENU_APP_ICONS";
	public static final String DataBase_MenuAppIcons_APPLICATION="APPLICATION_ID";
	public static final String DataBase_MenuAppIcons_APP_ICON="APP_ICON";
	public static final String DataBase_MenuAppIcons_NAME20="NAME20";
	public static final String DataBase_MenuAppIcons_NAME50="NAME50";
	public static final String DataBase_MenuAppIcons_APP_ICON_ADDRESS="APP_ICON_ADDRESS";
	public static final String DataBase_MenuAppIcons_LAST_ACTIVITY_DATE="LAST_ACTIVITY_DATE";
	public static final String DataBase_MenuAppIcons_LAST_ACTIVITY_TEAM_MEMBER="LAST_ACTIVITY_TEAM_MEMBER";
	/**
	 * Constant for TEAM_MEMBER_MESSAGES table name and fields name
	 * 
	 */
	public static final String TableName_TeamMembersMessages="TEAM_MEMBER_MESSAGES";
	public static final String DataBase_TeamMembersMessages_Tenant="Tenant_Id";
	public static final String DataBase_TeamMembersMessages_Company="Company_Id";
	public static final String DataBase_TeamMembersMessages_TeamMember="Team_Member_Id";
	public static final String DataBase_TeamMembersMessages_Status="Status";
	public static final String DataBase_TeamMembersMessages_TicketTapeMessge="Ticket_Tape_Message";
	public static final String DataBase_TeamMembersMessages_Message="Message";
	public static final String DataBase_TeamMembersMessages_StartDate="Start_Date";
	public static final String DataBase_TeamMembersMessages_EndDate="End_Date";
	public static final String DataBase_TeamMembersMessages_LastActivityDate="Last_Activity_Date";
	public static final String DataBase_TeamMembersMessages_LastActivityTeamMember="Last_Activity_Team_Member";
	public static final String DataBase_TeamMembersMessages_MessageId="MESSAGE_ID";
	/**
	 * Constant for NOTES table name and fields name
	 * 
	 */
	public static final String TableName_NOTES="NOTES";
	public static final String DataBase_Notes_Tenant_Id="TENANT_ID";
	public static final String DataBase_Notes_Company_Id="COMPANY_ID";
	public static final String DataBase_Notes_Note_Id="NOTE_ID";
	public static final String DataBase_Notes_Note_Link="NOTE_LINK";
	public static final String DataBase_Notes_ID="ID";
	public static final String DataBase_Notes_Last_Acitivity_Date="LAST_ACTIVITY_DATE";
	public static final String DataBase_Notes_Note="NOTE";
	public static final String DataBase_Notes_Last_Activity_Team_Member="LAST_ACTIVITY_TEAM_MEMBER";
	public static final String DataBase_Notes_CONTINUATIONS_SEQUENCE="CONTINUATIONS_SEQUENCE";
	/**
	 * Constant for LOCATIONS table name and fields name
	 * 
	 */
	public static final String TableName_Locations="LOCATIONS";
	public static final String DataBase_Location_TENANT_ID="TENANT_ID";
	public static final String DataBase_Location_FULFILLMENT_CENTER_ID="FULFILLMENT_CENTER_ID";
	public static final String DataBase_Location_COMPANY_ID="COMPANY_ID";
	public static final String DataBase_Location_AREA="AREA";
	public static final String DataBase_Location_LOCATION="LOCATION";
	public static final String DataBase_Location_DESCRIPTION20="DESCRIPTION20";
	public static final String DataBase_Location_DESCRIPTION50="DESCRIPTION50";
	public static final String DataBase_Location_WORK_GROUP_ZONE="WORK_GROUP_ZONE";
	public static final String DataBase_Location_WORK_ZONE="WORK_ZONE";
	public static final String DataBase_Location_LOCATION_TYPE="LOCATION_TYPE";
	public static final String DataBase_Location_LOCATION_PROFILE="LOCATION_PROFILE";
	public static final String DataBase_Location_WIZARD_ID="WIZARD_ID";
	public static final String DataBase_Location_WIZARD_CONTROL_NUMBER="WIZARD_CONTROL_NUMBER";
	public static final String DataBase_Location_LOCATION_HEIGHT="LOCATION_HEIGHT";
	public static final String DataBase_Location_LOCATION_WIDTH="LOCATION_WIDTH";
	public static final String DataBase_Location_LOCATION_DEPTH="LOCATION_DEPTH";
	public static final String DataBase_Location_LOCATION_WEIGHT_CAPACITY="LOCATION_WEIGHT_CAPACITY";
	public static final String DataBase_Location_LOCATION_HEIGHT_CAPACITY="LOCATION_HEIGHT_CAPACITY";
	public static final String DataBase_Location_NUMBER_PALLETS_FIT="NUMBER_PALLETS_FIT";
	public static final String DataBase_Location_NUMBER_FLOOR_LOCATIONS="NUMBER_FLOOR_LOCATIONS";
	public static final String DataBase_Location_ALLOCATION_ALLOWABLE="ALLOCATION_ALLOWABLE";
	public static final String DataBase_Location_MULTIPLE_ITEMS="MULTIPLE_ITEMS";
	public static final String DataBase_Location_SLOTTING="SLOTTING";
	public static final String DataBase_Location_STORAGE_TYPE="STORAGE_TYPE";
	public static final String DataBase_Location_CHECK_DIGIT="CHECK_DIGIT";
	public static final String DataBase_Location_LOCATION_LABEL_TYPE="LOCATION_LABEL_TYPE";
	public static final String DataBase_Location_LOCATION_REPRINT="LOCATION_REPRINT";
	public static final String DataBase_Location_CYCLE_COUNT_POINTS="CYCLE_COUNT_POINTS";
	public static final String DataBase_Location_LAST_CYCLE_COUNT_DATE="LAST_CYCLE_COUNT_DATE";
	public static final String DataBase_Location_INVENTORY_PICK_SEQUENCE="INVENTORY_PICK_SEQUENCE";
	public static final String DataBase_Location_ALTERNATE_LOCATION="ALTERNATE_LOCATION";
	public static final String DataBase_Location_LAST_ACTIVITY_DATE="LAST_ACTIVITY_DATE";
	public static final String DataBase_Location_LAST_ACTIVITY_TEAM_MEMBER="LAST_ACTIVITY_TEAM_MEMBER";
	public static final String DataBase_Location_LAST_ACTIVITY_TASK="LAST_ACTIVITY_TASK";
	public static final String DataBase_Location_FREE_LOCATION_WHEN_ZERO="FREE_LOCATION_WHEN_ZERO";
	public static final String DataBase_Location_FREE_PRIME_DAYS="FREE_PRIME_DAYS";
	/**
	 * Constant for WIZARD_LOCATIONS table name and fields name
	 * 
	 */
	public static final String TableName_WIZARD_LOCATIONS="WIZARD_LOCATIONS";
	public static final String Database_Location_Wizard_ROW_NUMBER_CHARACTERS	="ROW_NUMBER_CHARACTERS";
	public static final String Database_Location_Wizard_ROW_CHARACTER_SET	="ROW_CHARACTER_SET";
	public static final String Database_Location_Wizard_ROW_STARTING	="ROW_STARTING";
	public static final String Database_Location_Wizard_ROW_NUMBER_OF	="ROW_NUMBER_OF";
	public static final String Database_Location_Wizard_AISLE_NUMBER_CHARACTERS	="AISLE_NUMBER_CHARACTERS";
	public static final String Database_Location_Wizard_AISLE_CHARACTER_SET	="AISLE_CHARACTER_SET";
	public static final String Database_Location_Wizard_AISLE_STARTING	="AISLE_STARTING";
	public static final String Database_Location_Wizard_AISLE_NUMBER_OF	="AISLE_NUMBER_OF";
	public static final String Database_Location_Wizard_BAY_NUMBER_CHARACTERS	="BAY_NUMBER_CHARACTERS";
	public static final String Database_Location_Wizard_BAY_CHARACTER_SET	="BAY_CHARACTER_SET";
	public static final String Database_Location_Wizard_BAY_STARTING	="BAY_STARTING";
	public static final String Database_Location_Wizard_BAY_NUMBER_OF	="BAY_NUMBER_OF";
	public static final String Database_Location_Wizard_LEVEL_NUMBER_CHARACTERS	="LEVEL_NUMBER_CHARACTERS";
	public static final String Database_Location_Wizard_LEVEL_CHARACTER_SET	="LEVEL_CHARACTER_SET";
	public static final String Database_Location_Wizard_LEVEL_STARTING	="LEVEL_STARTING";
	public static final String Database_Location_Wizard_LEVEL_NUMBER_OF	="LEVEL_NUMBER_OF";
	public static final String Database_Location_Wizard_SLOT_NUMBER_CHARACTERS	="SLOT_NUMBER_CHARACTERS";
	public static final String Database_Location_Wizard_SLOT_CHARACTER_SET	="SLOT_CHARACTER_SET";
	public static final String Database_Location_Wizard_SLOT_STARTING	="SLOT_STARTING";
	public static final String Database_Location_Wizard_SLOT_NUMBER_OF	="SLOT_NUMBER_OF";
	public static final String Database_Location_Wizard_SEPARATOR_ROW_AISLE	="SEPARATOR_ROW_AISLE";
	public static final String Database_Location_Wizard_SEPARATOR_AISLE_BAY	="SEPARATOR_AISLE_BAY";
	public static final String Database_Location_Wizard_SEPARATOR_BAY_LEVEL	="SEPARATOR_BAY_LEVEL";
	public static final String Database_Location_Wizard_SEPARATOR_LEVEL_SLOT	="SEPARATOR_LEVEL_SLOT";
	public static final String Database_Location_Wizard_SEPARATOR_ROW_AISLE_PRINT	="SEPARATOR_ROW_AISLE_PRINT";
	public static final String Database_Location_Wizard_SEPARATOR_AISLE_BAY_PRINT	="SEPARATOR_AISLE_BAY_PRINT";
	public static final String Database_Location_Wizard_SEPARATOR_BAY_LEVEL_PRINT	="SEPARATOR_BAY_LEVEL_PRINT";
	public static final String Database_Location_Wizard_SEPARATOR_LEVEL_SLOT_PRINT	="SEPARATOR_LEVEL_SLOT_PRINT";
	public static final String Database_Location_Wizard_WIZARD_ID="WIZARD_ID";
	public static final String Database_Location_Wizard_Company_ID="COMPANY_ID";
	public static final String Database_Location_Wizard_Tenant_ID="TENANT_ID";
	public static final String Database_Location_Wizard_WIZARD_CONTROL_NUMBER="WIZARD_CONTROL_NUMBER";
	public static final String Database_Location_Wizard_WIZARD_DESCRIPTION20="DESCRIPTION20";
	public static final String Database_Location_Wizard_WIZARD_DESCRIPTION50="DESCRIPTION50";
	public static final String Database_Location_Wizard_WIZARD_WORK_GROUP_ZONE="WORK_GROUP_ZONE";
	public static final String Database_Location_Wizard_WIZARD_AREA="AREA";
	public static final String Database_Location_Wizard_WIZARD_WORK_ZONE="WORK_ZONE";
	public static final String Database_Location_Wizard_WIZARD_LOCATION_TYPE="LOCATION_TYPE";
	public static final String Database_Location_Wizard_WIZARD_LOCATION_PROFILE="LOCATION_PROFILE";
	public static final String Database_Location_Wizard_WIZARD_DATE_CREATED="DATE_CREATED";
	public static final String Database_Location_Wizard_WIZARD_LAST_ACTIVITY_DATE="LAST_ACTIVITY_DATE";
	public static final String Database_Location_Wizard_WIZARD_LAST_ACTIVITY_TEAM_MEMBER="LAST_ACTIVITY_TEAM_MEMBER";
	public static final String Database_Location_Wizard_WIZARD_CREATED_BY_TEAM_MEMBER="CREATED_BY_TEAM_MEMBER";
	public static final String Database_Location_Wizard_Status="STATUS";
	public static final String Database_Location_Wizard_WIZARD_FULFILLMENT_CENTER_ID="FULFILLMENT_CENTER_ID";
	public static final String DataBase_TENANT_ID="TENANT_ID";
	public static final String DataBase_FULFILLMENT_CENTER_ID="FULFILLMENT_CENTER_ID";
	public static final String DataBase_COMPANY_ID="COMPANY_ID";
	public static final String DataBase_AREA="AREA";
	public static final String DataBase_LOCATION="LOCATION";
	public static final String DataBase_PRODUCT="PRODUCT";
	public static final String DataBase_QUALITY="QUALITY";
	/**
	 * Constant for LOCATION_REPLENISHMENTS table name and fields name
	 * 
	 */
	public static final String TableName_LOCATION_REPLENISHMENTS="LOCATION_REPLENISHMENTS";
	public static final String DataBase_MINIMUM_INVENTORY_LEVEL="MINIMUM_INVENTORY_LEVEL";
	public static final String DataBase_MAXIMUM_QTY="MAXIMUM_QTY";
	public static final String DataBase_MAXIMUM_NUMBER_OF_CASES="MAXIMUM_NUMBER_OF_CASES";
	public static final String DataBase_MAXIMUM_NUMBER_OF_PALLETS="MAXIMUM_NUMBER_OF_PALLETS";
	public static final String DataBase_RE_STOCK_TYPE="RE_STOCK_TYPE";
	public static final String DataBase_RE_STOCK_MODEL="RE_STOCK_MODEL";
	public static final String DataBase_GENERATED_TYPE="GENERATED_TYPE";
	public static final String DataBase_LAST_ACTIVITY_DATE="LAST_ACTIVITY_DATE";
	public static final String DataBase_LAST_ACTIVITY_TEAM_MEMBER="LAST_ACTIVITY_TEAM_MEMBER";

	// Phase 2 Pojo Variables Created by Pradeep Kumar

	public static final String DataBase_TableName_Smart_Task_Seq_Headers="SMART_TASK_SEQ_HEADERS";

	// Smart_Task_Seq_Headers PK Variables
	public static final String DataBase_Tenant_Id="TENANT_ID";
	public static final String DataBase_Company_Id="COMPANY_ID";
	public static final String DataBase_Application_Id="APPLICATION_ID";
	public static final String DataBase_Execution_Sequence_Group="EXECUTION_SEQUENCE_GROUP";
	public static final String DataBase_Execution_Sequence_Name="EXECUTION_SEQUENCE_NAME";
	public static final String DataBase_ForeignKey="ForeignKeyFromCompany";
	public static final String DataBase_ForeignKey_Tenant="ForeignKeyFromTenant";

	// Smart_Task_Seq_Headers only pojo variables
	public static final String DataBase_Description20="DESCRIPTION20";
	public static final String DataBase_Description50="DESCRIPTION50";
	public static final String DataBase_Menu_Option="MENU_OPTION";
	public static final String DataBase_Menu_Name="MENU_NAME";

	public static final String DataBase_Execution_Device="EXECUTION_DEVICE";
	public static final String DataBase_Execution_Type="EXECUTION_TYPE";
	public static final String DataBase_Last_Activity_Date="LAST_ACTIVITY_DATE";
	public static final String DataBase_Last_Activity_Team_Member="LAST_ACTIVITY_TEAM_MEMBER";

	// Pojo variables Work Folw Steps

	public static final String DataBase_TableName_Work_Flow_Steps="WORK_FLOW_STEPS";

	public static final String DataBase_Work_Flow_Id="WORK_FLOW_ID";
	public static final String DataBase_Work_Flow_Step="WORK_FLOW_STEP";

	public static final String DataBase_Area="AREA";
	public static final String DataBase_Work_Type="WORK_TYPE";

	//PHASE 2 Aakash Bishnoi Mar 30 2015
	public static final String TableName_Smart_Task_Seq_Instruction="SMART_TASK_SEQ_INSTRUCTIONS";
	public static final String TableName_Smart_Task_Executions="SMART_TASK_EXECUTIONS";
	public static final String DateBase_Tenant_Id="TENANT_ID";
	public static final String DateBase_Company_Id="COMPANY_ID";
	public static final String DateBase_Execution_Sequence="EXECUTION_SEQUENCE";
	public static final String DateBase_Execution_Sequence_Name ="EXECUTION_SEQUENCE_NAME";
	public static final String DateBase_Execution_Sequence_Type="EXECUTION_SEQUENCE_TYPE";
	public static final String DateBase_Execution_Name="EXECUTION_NAME";
	public static final String DateBase_Action_Name="ACTION_NAME";
	public static final String DateBase_Comments="COMMENTS";
	public static final String DateBase_Goto_Tag="GOTO_TAG";
	public static final String DateBase_Message="MESSAGE";


	public static final String DateBase_Custom_Execution="CUSTOM_EXECUTION";
	public static final String DateBase_Return_Code_Value="RETURN_CODE_VALUE";

	public static final String DataBase_Execution_Group="EXECUTION_GROUP";


	public static final String DataBase_Help_Line="HELP_LINE";

	public static final String DataBase_Button="BUTTON";
	public static final String DataBase_Button_Name="BUTTON_NAME";
	public static final String DataBase_Execution_Path="EXECUTION_PATH";


	public static final String DataBase_Fulfillment_Center_Id="FULFILLMENT_CENTER_ID";
	// Table Names
	public static final String DataBase_TableName_Work_Flow_Headers="WORK_FLOW_HEADERS";
	public static final String DataBase_TableName_Work_Type_Lines="WORK_TYPE_LINES";  
	public static final String DataBase_TableName_Supplier="SUPPLIERS";
	public static final String DataBase_TableName_Supplier_Item_Pricing="SUPPLIER_ITEM_PRICING";
	public static final String DataBase_TableName_Supplier_Item="SUPPLIER_ITEMS";
	public static final String DataBase_TableName_Executions="EXECUTIONS";
	public static final String DataBase_TableName_Exchange_Rates="EXCHANGE_RATES";
	public static final String DataBase_TableName_Cyl_Count_Yrly_Summaries="CYL_COUNT_YRLY_SUMMARIES";
	public static final String DataBase_TableName_Purchase_Order_Headers="PURCHASE_ORDER_HEADERS";
	public static final String DataBase_TableName_Product_Reorder_Levels="PRODUCT_REORDER_LEVELS";
	public static final String DataBase_TableName_Executions_Sequence_Insts="EXECUTION_SEQUENCE_INSTS";
	public static final String DataBase_TableName_Executions_Sequence_Headers="EXECUTION_SEQUENCE_HEADERS";
	public static final String DataBase_TableName_Purchase_Order_Products="PURCHASE_ORDER_PRODUCTS";
	public static final String DataBase_TableName_Carriers="CARRIERS";
	// Variables names
	public static final String DataBase_Work_Control_Number="WORK_CONTROL_NUMBER";
	public static final String DataBase_Line_Number="LINE_NUMBER";


	public static final String DataBase_Location="LOCATION";
	public static final String DataBase_Product="PRODUCT";
	public static final String DataBase_Quality="QUALITY";
	public static final String DataBase_Lot="LOT";
	public static final String DataBase_Serial01="SERIAL01";
	public static final String DataBase_Serial02="SERIAL02";
	public static final String DataBase_Serial03="SERIAL03";
	public static final String DataBase_Serial04="SERIAL04";
	public static final String DataBase_Serial05="SERIAL05";
	public static final String DataBase_Serial06="SERIAL06";
	public static final String DataBase_Serial07="SERIAL07";
	public static final String DataBase_Serial08="SERIAL08";
	public static final String DataBase_Serial09="SERIAL09";
	public static final String DataBase_Serial10="SERIAL10";
	public static final String DataBase_Lpn="LPN";
	public static final String DataBase_Inventory_Control01="INVENTORY_CONTROL01";
	public static final String DataBase_Inventory_Control02="INVENTORY_CONTROL02";
	public static final String DataBase_Inventory_Control03="INVENTORY_CONTROL03";
	public static final String DataBase_Inventory_Control04="INVENTORY_CONTROL04";
	public static final String DataBase_Inventory_Control05="INVENTORY_CONTROL05";
	public static final String DataBase_Inventory_Control06="INVENTORY_CONTROL06";
	public static final String DataBase_Inventory_Control07="INVENTORY_CONTROL07";
	public static final String DataBase_Inventory_Control08="INVENTORY_CONTROL08";
	public static final String DataBase_Inventory_Control09="INVENTORY_CONTROL09";
	public static final String DataBase_Inventory_Control10="INVENTORY_CONTROL10";

	public static final String DataBase_Work_Group_Zone="WORK_GROUP_ZONE";
	public static final String DataBase_Work_Zone="WORK_ZONE";
	public static final String DataBase_Inventory_Pick_Sequence="INVENTORY_PICK_SEQUENCE";
	public static final String DataBase_Quantity_Requied="QUANTITY_REQUIRED";
	public static final String DataBase_Quantity_Processed="QUANTITY_PROCESSED";
	public static final String DataBase_Quantity_Could_Not_Process="QUANTITY_COULD_NOT_PROCESS";
	public static final String DataBase_To_Area="TO_AREA";
	public static final String DataBase_To_Location="TO_LOCATION";
	public static final String DataBase_Status="STATUS";
	public static final String DataBase_Last_Sequence_Executed="LAST_SEQUENCE_EXECUTED";
	public static final String DataBase_Last_Activity_Task="LAST_ACTIVITY_TASK";


	// Supplier Table Variables
	public static final String DataBase_Supplier="SUPPLIER";


	public static final String DataBase_Suppier_Account_Identification=" SUPPIER_ACCOUNT_IDENTIFICATION";
	public static final String DataBase_Prefered_Supplier="PREFERED_SUPPLIER";
	public static final String DataBase_Supplier_Rating="SUPPLIER_RATING";
	public static final String DataBase_Ship_From_Name20="SHIP_FROM_NAME20";
	public static final String DataBase_Ship_From_Name50="SHIP_FROM_NAME50";
	public static final String DataBase_Ship_From_Address_Line1="SHIP_FROM_ADDRESS_LINE1";
	public static final String DataBase_Ship_From_Address_Line2="SHIP_FROM_ADDRESS_LINE2";
	public static final String DataBase_Ship_From_Address_Line3="SHIP_FROM_ADDRESS_LINE3";
	public static final String DataBase_Ship_From_Address_Line4="SHIP_FROM_ADDRESS_LINE4";
	public static final String DataBase_Ship_From_City="SHIP_FROM_CITY";
	public static final String DataBase_Ship_From_State_Code="SHIP_FROM_STATE_CODE";
	public static final String DataBase_Ship_From_Country_Code="SHIP_FROM_COUNTRY_CODE";
	public static final String DataBase_Ship_From_Zip_Code="SHIP_FROM_ZIP_CODE";
	public static final String DataBase_Bill_To_Name20="BILL_TO_NAME20";
	public static final String DataBase_Bill_To_Name50="BILL_TO_NAME50";
	public static final String DataBase_Bill_To_Address_Line1="BILL_TO_ADDRESS_LINE1";
	public static final String DataBase_Bill_To_Address_Line2="BILL_TO_ADDRESS_LINE2";
	public static final String DataBase_Bill_To_Address_Line3="BILL_TO_ADDRESS_LINE3";
	public static final String DataBase_Bill_To_Address_Line4="BILL_TO_ADDRESS_LINE4";
	public static final String DataBase_Bill_To_From_City="BILL_TO_FROM_CITY";
	public static final String DataBase_Bill_To_State_Code="BILL_TO_STATE_CODE";
	public static final String DataBase_Bill_To_Country_Code="BILL_TO_COUNTRY_CODE";
	public static final String DataBase_Bill_To_Zip_Code="BILL_TO_ZIP_CODE";
	public static final String DataBase_Contact_Name1="CONTACT_NAME1";
	public static final String DataBase_Contact_Phone1="CONTACT_PHONE1";
	public static final String DataBase_Contact_Extension1="CONTACT_EXTENSION1";
	public static final String DataBase_Contact_Cell1="CONTACT_CELL1";
	public static final String DataBase_Contact_Fax1="CONTACT_FAX1";
	public static final String DataBase_Contact_Name2="CONTACT_NAME2";
	public static final String DataBase_Contact_Phone2="CONTACT_PHONE2";
	public static final String DataBase_Contact_Extension2="CONTACT_EXTENSION2";
	public static final String DataBase_Contact_Cell2="CONTACT_CELL2";
	public static final String DataBase_Contact_Fax2="CONTACT_FAX2";
	public static final String DataBase_Contact_Name3="CONTACT_NAME3";
	public static final String DataBase_Contact_Phone3="CONTACT_PHONE3";
	public static final String DataBase_Contact_Extension3="CONTACT_EXTENSION3";
	public static final String DataBase_Contact_Cell3="CONTACT_CELL3";
	public static final String DataBase_Contact_Fax3="CONTACT_FAX3";
	public static final String DataBase_Contact_Name4="CONTACT_NAME4";
	public static final String DataBase_Contact_Phone4="CONTACT_PHONE4";
	public static final String DataBase_Contact_Extension4="CONTACT_EXTENSION4";
	public static final String DataBase_Contact_Cell4="CONTACT_CELL4";
	public static final String DataBase_Contact_Fax4="CONTACT_FAX4";
	public static final String DataBase_Main_Fax="MAIN_FAX";
	public static final String DataBase_Main_Email="MAIN_EMAIL";
	public static final String DataBase_Account_Number="ACCOUNT_NUMBER";
	public static final String DataBase_Currency="CURRENCY";
	public static final String DataBase_Payment_Terms1="PAYMENT_TERMS1";
	public static final String DataBase_Buying_Terms="BUYING_TERMS";
	public static final String DataBase_Duties_Percent="DUTIES_PERCENT";
	public static final String DataBase_Tax_Percent="TAX_PERCENT";
	public static final String DataBase_Brokers_Fees="BROKERS_FEES";
	public static final String DataBase_Harbor_Fees="HARBOR_FEES";
	public static final String DataBase_Incentivies="INCENTIVIES";
	public static final String DataBase_Incentivies_Days="INCENTIVIES_DAYS";
	public static final String DataBase_Penalties="PENALTIES";
	public static final String DataBase_Penalties_Days="PENALTIES_DAYS";
	public static final String DataBase_Transmission_Method="TRANSMISSION_METHOD";
	public static final String DataBase_Special_Instructions="SPECIAL_INSTRUCTIONS";
	public static final String DataBase_Cancellation_Notice_Days="CANCELLATION_NOTICE_DAYS";
	public static final String DataBase_Cancellation_Notice_Email="CANCELLATION_NOTICE_EMAIL";
	public static final String DataBase_Ship_Via="SHIP_VIA";
	public static final String DataBase_Freight_Terms="FREIGHT_TERMS";
	public static final String DataBase_Payment_Terms2="PAYMENT_TERMS2";
	public static final String DataBase_Landing_Factor_Percent="LANDING_FACTOR_PERCENT";

	// Suppier item Pricing Table Variables

	public static final String DataBase_Supplier_Item="SUPPLIER_ITEM";
	public static final String DataBase_From_Date="FROM_DATE";
	public static final String DataBase_To_Date="TO_DATE";
	public static final String DataBase_Break1_Price="BREAK1_PRICE";
	public static final String DataBase_Break1_Qty="BREAK1_QTY";
	public static final String DataBase_Break2_Price="BREAK2_PRICE";
	public static final String DataBase_Break2_Qty="BREAK2_QTY";
	public static final String DataBase_Break3_Price="BREAK3_PRICE";
	public static final String DataBase_Break3_Qty="BREAK3_QTY";
	public static final String DataBase_Break4_Price="BREAK4_PRICE";
	public static final String DataBase_Break4_Qty="BREAK4_QTY";
	public static final String DataBase_Break5_Price="BREAK5_PRICE";
	public static final String DataBase_Break5_Qty="BREAK5_QTY";
	public static final String DataBase_Crating_Cost="CRATING_COST";

	// Table Supplier Item variables

	public static final String DataBase_Order_Instructions="ORDER_INSTRUCTIONS";
	public static final String DataBase_Order_By_Qty="ORDER_BY_QTY";
	public static final String DataBase_Minimum_Order_Qty="MINIMUM_ORDER_QTY";
	public static final String DataBase_Returns_Allowed="RETURNS_ALLOWED";
	public static final String DataBase_Last_Order_Date="LAST_ORDER_DATE";
	public static final String DataBase_Last_Order_Quanitity="LAST_ORDER_QUANITITY";
	public static final String DataBase_Last_Order_Price="LAST_ORDER_PRICE";
	public static final String DataBase_Team_Member_Id="Team_Member_Id";
	public static final String DataBase_Supplier_Bar_Code="SUPPLIER_BAR_CODE";
	public static final String DataBase_Supplier_Bar_Code_Type="SUPPLIER_BAR_CODE_TYPE";

	public static final String DataBase_Execution_Name="EXECUTION_NAME";
	public static final String DataBase_Execution_Sequence="EXECUTION_SEQUENCE";
	public static final String DataBase_Action_Name="ACTION_NAME";
	public static final String DataBase_Comment="COMMENT";
	public static final String DataBase_Goto_Sequence_Number="GOTO_SEQUENCE_NUMBER";
	public static final String DataBase_Message="MESSAGE";
	public static final String DataBase_Execution_Sequence_Type="EXECUTION_SEQUENCE_TYPE";
	public static final String DataBase_Year="YEAR";
	public static final String DataBase_Month="MONTH";
	public static final String DataBase_Currence_Code = "CURRENCE_CODE";
	public static final String DataBase_Last_Activity_Time="LAST_ACTIVITY_TIME";

	public static final String DataBase_Cycle_Count_Points="CYCLE_COUNT_POINTS";
	public static final String DataBase_Last_Cycle_Count_Date="LAST_CYCLE_COUNT_DATE";
	public static final String DataBase_Negative_Qty="NEGATIVE_QTY";
	public static final String DataBase_Positive_Qty="POSITIVE_QTY";
	public static final String DataBase_Number_Cycle_Counts="NUMBER_CYCLE_COUNTS";
	public static final String DataBase_Minimum_Days_Suppy="MINIMUM_DAYS_SUPPLY";
	public static final String DataBase_Minimum_Inventory_Level="MINIMUM_INVENTORY_LEVEL";
	public static final String DataBase_Notes="NOTES";
	public static final String DataBase_Sequence_Number="SEQUENCE_NUMBER";
	public static final String DataBase_Side="SIDE";

	public static final String DataBase_Heading="HEADING";
	public static final String DataBase_Description_Line01="DESCRIPTION_LINE01";
	public static final String DataBase_Description_Line02="DESCRIPTION_LINE02";
	public static final String DataBase_Description_Line03="DESCRIPTION_LINE03";
	public static final String DataBase_Description_Line04="DESCRIPTION_LINE04";
	public static final String DataBase_Description_Line05="DESCRIPTION_LINE05";
	public static final String DataBase_Description_Line06="DESCRIPTION_LINE06";
	public static final String DataBase_Description_Line07="DESCRIPTION_LINE07";
	public static final String DataBase_Description_Line08="DESCRIPTION_LINE08";
	public static final String DataBase_Description_Line09="DESCRIPTION_LINE09";
	public static final String DataBase_Description_Line10="DESCRIPTION_LINE10";
	public static final String DataBase_Description_Line11="DESCRIPTION_LINE11";
	public static final String DataBase_Description_Line12="DESCRIPTION_LINE12";
	public static final String DataBase_Description_Line13="DESCRIPTION_LINE13";
	public static final String DataBase_Description_Line14="DESCRIPTION_LINE14";
	public static final String DataBase_Description_Line15="DESCRIPTION_LINE15";
	public static final String DataBase_Description_Line16="DESCRIPTION_LINE16";
	public static final String DataBase_Description_Line17="DESCRIPTION_LINE17";
	public static final String DataBase_Description_Line18="DESCRIPTION_LINE18";
	public static final String DataBase_Description_Line19="DESCRIPTION_LINE19";
	public static final String DataBase_Description_Line20="DESCRIPTION_LINE20";
	public static final String DataBase_Purchase_Order_Number="PURCHASE_ORDER_NUMBER";

	public static final String DataBase_Shipping_Charge="SHIPPING_CHARGE";

	public static final String DataBase_Fob="FOB";
	public static final String DataBase_Type="TYPE";


	public static final String DataBase_Delivered_By="DELIVERED_BY";
	public static final String DataBase_Expected_Delivery_Date="EXPECTED_DELIVERY_DATE";
	public static final String DataBase_Date_Received="DATE_RECEIVED";
	public static final String DataBase_Received_By="RECEIVED_BY";
	public static final String DataBase_Buyer="BUYER";
	public static final String DataBase_Number_Of_Shipments="NUMBER_OF_SHIPMENTS";
	public static final String DataBase_Created_By="CREATED_BY";
	public static final String DataBase_Date_Created="DATE_CREATED";
	public static final String DataBase_Shipment_Number="SHIPMENT_NUMBER";


	public static final String DataBase_Quantity="QUANTITY";
	public static final String DataBase_Unit_Of_Measure_Code="UNIT_OF_MEASURE_CODE";
	public static final String DataBase_Quantity_In_Transit="QUANTITY_IN_TRANSIT";
	public static final String DataBase_Quantity_Received="QUANTITY_RECEIVED";
	public static final String DataBase_Quantity_Damaged="QUANTITY_DAMAGED";
	public static final String DataBase_Quantity_Qc_Issue  ="QUANTITY_QC_ISSUE  ";




	public static final String DataBase_Po_Terms="PO_TERMS";

	public static final String DataBase_Crating_Cost1="CRATING_COST1";

	public static final String DataBase_Quantity_Task="QUANTITY_TASK";
	public static final String DataBase_Price="PRICE";
	public static final String DataBase_Discount_Percent="DISCOUNT_PERCENT";
	public static final String DataBase_Crating_Cost2="CRATING_COST2";
	public static final String DataBase_Quote_Reference_Number="QUOTE_REFERENCE_NUMBER";
	public static final String DataBase_Shipment_Number2="SHIPMENT_NUMBER2";
	public static final String DataBase_Container_Number="CONTAINER_NUMBER";
	public static final String DataBase_Boat_Name="BOAT_NAME";

	//Table Carrier Variables
	public static final String DataBase_Carrier="CARRIER";
	public static final String DataBase_Carrier_Id="CARRIER_ID";
	public static final String DataBase_Carrier_Process="CARRIER_PROCESS";
	public static final String DataBase_Type_Of_Carrier="TYPE_OF_CARRIER";
	public static final String DataBase_Name20="NAME20";
	public static final String DataBase_Name50="NAME50";
	public static final String DataBase_Address_Line1="ADDRESS_LINE1";
	public static final String DataBase_Address_Line2="ADDRESS_LINE2";
	public static final String DataBase_Address_Line3="ADDRESS_LINE3";
	public static final String DataBase_Address_Line4="ADDRESS_LINE4";
	public static final String DataBase_City="CITY";
	public static final String DataBase_State_Code="STATE_CODE";
	public static final String DataBase_Country_Code="COUNTRY_CODE";
	public static final String DataBase_Zip_Code="ZIP_CODE";

	public static final String DataBase_Contact_Email1="CONTACT_EMAIL1";

	public static final String DataBase_Contact_Email2="CONTACT_EMAIL2";

	public static final String DataBase_Contact_Email3="CONTACT_EMAIL3";

	public static final String DataBase_Contact_Email4="CONTACT_EMAIL4";

	public static final String DataBase_Handling_Charge_Percent="HANDLING_CHARGE_PERCENT";

	public static final String DataBase_Minimal_Charge="MINIMAL_CHARGE";
	public static final String DataBase_Broker_Name="BROKER_NAME";
	public static final String DataBase_Tractable="TRACTABLE";
	public static final String DataBase_Ship_To_Type="SHIP_TO_TYPE";
	public static final String DataBase_Ship_To_Pobox="SHIP_TO_POBOX";
	public static final String DataBase_Returns_Carrier="RETURNS_CARRIER";
	public static final String DataBase_Ship_To_Agent="SHIP_TO_AGENT";
	public static final String DataBase_Pro_Number_Start="PRO_NUMBER_START ";
	public static final String DataBase_Pro_Number_End="PRO_NUMBER_END ";
	public static final String DataBase_Pro_Number_Last_Used="PRO_NUMBER_LAST_USED";
	public static final String DataBase_Pro_Number_Remaining_Warring="PRO_NUMBER_REMAINING_WARRING";
	public static final String DataBase_Pro_Num_Remaining_Warn_Email="PRO_NUM_REMAINING_WARN_EMAIL";
	public static final String DataBase_Pro_Number_Prefix="PRO_NUMBER_PREFIX";
	public static final String DataBase_Pro_Number_Check_Digit_Type="PRO_NUMBER_CHECK_DIGIT_TYPE";
	public static final String DataBase_Accepts_Electronic_Manifies="ACCEPTS_ELECTRONIC_MANIFIES";
	public static final String DataBase_Carrer_Interface="CARRER_INTERFACE";
	public static final String DataBase_Payment_Name20="PAYMENT_NAME20";
	public static final String DataBase_Payment_Name50="PAYMENT_NAME50";
	public static final String DataBase_Payment_Address_Line1="PAYMENT_ADDRESS_LINE1";
	public static final String DataBase_Payment_Address_Line2="PAYMENT_ADDRESS_LINE2";
	public static final String DataBase_Payment_Address_Line3="PAYMENT_ADDRESS_LINE3";
	public static final String DataBase_Payment_Address_Line4="PAYMENT_ADDRESS_LINE4";
	public static final String DataBase_Payment_City="PAYMENT_CITY";
	public static final String DataBase_Payment_State_Code="PAYMENT_STATE_CODE";
	public static final String DataBase_Payment_Country_Code="PAYMENT_COUNTRY_CODE";
	public static final String DataBase_Payment_Zip_Code="PAYMENT_ZIP_CODE";
	public static final String DataBase_Payment_Contact_Name1="PAYMENT_CONTACT_NAME1";
	public static final String DataBase_Payment_Contact_Phone1="PAYMENT_CONTACT_PHONE1";
	public static final String DataBase_Payment_Contact_Extension1="PAYMENT_CONTACT_EXTENSION1";
	public static final String DataBase_Payment_Contact_Cell1="PAYMENT_CONTACT_CELL1";
	public static final String DataBase_Payment_Contact_Fax1="PAYMENT_CONTACT_FAX1";
	public static final String DataBase_Payment_Contact_Email1="PAYMENT_CONTACT_EMAIL1";
	public static final String DataBase_Payment_Contact_Name2="PAYMENT_CONTACT_NAME2";
	public static final String DataBase_Payment_Contact_Phone2="PAYMENT_CONTACT_PHONE2";
	public static final String DataBase_Payment_Contact_Extension2="PAYMENT_CONTACT_EXTENSION2";
	public static final String DataBase_Payment_Contact_Cell2="PAYMENT_CONTACT_CELL2";
	public static final String DataBase_Payment_Contact_Fax2="PAYMENT_CONTACT_FAX2";
	public static final String DataBase_Payment_Routing="PAYMENT_ROUTING";
	public static final String DataBase_Payment_Type="PAYMENT_TYPE";
	public static final String DataBase_Payment_Terms="PAYMENT_TERMS";



	//Aakash Bishnoi phase1 pojo

	//PRODUCT TABLE POJO
	public static final String DataBase_TableName_Products="PRODUCTS";

	public static final String DataBase_Measurement_Type="MEASUREMENT_TYPE";
	public static final String DataBase_Style_Code="STYLE_CODE";
	public static final String DataBase_Size_Code="SIZE_CODE";
	public static final String DataBase_Color_Code="COLOR_CODE";
	public static final String DataBase_Product_Group_Code="PRODUCT_GROUP_CODE";
	public static final String DataBase_Product_Sub_Group_Code="PRODUCT_SUB_GROUP_CODE";
	public static final String DataBase_Accounting_Code="ACCOUNTING_CODE";
	public static final String DataBase_Country_Of_Origin="COUNTRY_OF_ORIGIN";
	public static final String DataBase_Harmonized_Tariff_Code="HARMONIZED_TARIFF_CODE";
	public static final String DataBase_Serial_Type01="SERIAL_TYPE01";
	public static final String DataBase_Serial_Type02="SERIAL_TYPE02";
	public static final String DataBase_Serial_Type03="SERIAL_TYPE03";
	public static final String DataBase_Serial_Type04="SERIAL_TYPE04";
	public static final String DataBase_Serial_Type05="SERIAL_TYPE05";
	public static final String DataBase_Serial_Type06="SERIAL_TYPE06";
	public static final String DataBase_Serial_Type07="SERIAL_TYPE07";
	public static final String DataBase_Serial_Type08="SERIAL_TYPE08";
	public static final String DataBase_Serial_Type09="SERIAL_TYPE09";
	public static final String DataBase_Serial_Type10="SERIAL_TYPE10";
	public static final String DataBase_Lot_Control_Product="LOT_CONTROL_PRODUCT";
	public static final String DataBase_Number_Of_Days_To_Expiration="NUMBER_OF_DAYS_TO_EXPIRATION";
	public static final String DataBase_Ship_As_Is="SHIP_AS_IS";
	public static final String DataBase_Drop_Ship_Item_Flag="DROP_SHIP_ITEM_FLAG";
	public static final String DataBase_Division="DIVISION";
	public static final String DataBase_Product_Abc_Codes="PRODUCT_ABC_CODES";
	public static final String DataBase_Product_Value="PRODUCT_VALUE";
	public static final String DataBase_Date_Sequence="DATE_SEQUENCE";

	public static final String DataBase_Season_Code="SEASON_CODE";
	public static final String DataBase_Requires_Cube="REQUIRES_CUBE";
	public static final String DataBase_Handling_Code="HANDLING_CODE";
	public static final String DataBase_First_Received_Date="FIRST_RECEIVED_DATE";
	public static final String DataBase_Hazmat_Code="HAZMAT_CODE";
	public static final String DataBase_Secured_Level="SECURED_LEVEL";
	public static final String DataBase_Non_Stock_Item="NON_STOCK_ITEM";
	public static final String DataBase_Case_Pickable="CASE_PICKABLE";
	public static final String DataBase_Special_Handling_Code="SPECIAL_HANDLING_CODE";
	public static final String DataBase_Pallet_Block="PALLET_BLOCK";
	public static final String DataBase_Pallet_Tier="PALLET_TIER";
	public static final String DataBase_Max_Number_Pallet_Stacked="MAX_NUMBER_PALLET_STACKED";
	public static final String DataBase_Product_Image_High="PRODUCT_IMAGE_HIGH";
	public static final String DataBase_Product_Image_Low="PRODUCT_IMAGE_LOW";
	public static final String DataBase_Last_Landed_Cost="LAST_LANDED_COST";

	//PRODUCT_VARIABLES pojo
	public static final String DataBase_TableName_ProductVariables="PRODUCT_VARIABLES";

	public static final String DataBase_Unit_Of_Measure_Qty="UNIT_OF_MEASURE_QTY";

	public static final String DataBase_Bar_Code="BAR_CODE";
	public static final String DataBase_Bar_Code_Type="BAR_CODE_TYPE";
	public static final String DataBase_Rfid="RFID";
	public static final String DataBase_Height="HEIGHT";
	public static final String DataBase_Width="WIDTH";
	public static final String DataBase_Length="LENGTH";
	public static final String DataBase_Weight="WEIGHT";


	//pojo for work_history

	public static final String DataBase_TableName_Work_History="WORK_HISTORY";	          
	public static final String DataBase_Task="TASK";
	public static final String DataBase_Team_Member="TEAM_MEMBER";
	public static final String DataBase_Dates="DATES";	          

	public static final String DataBase_Asn="ASN";
	public static final String DataBase_Zone="ZONE";
	public static final String DataBase_Divert="DIVERT";	          
	public static final String DataBase_Work_History_Code="WORK_HISTORY_CODE";

	//pojo year_task_qty
	public static final String DataBase_TableName_Year_Task_Qty="YEAR_TASK_QTY";
	/*public static final String DataBase_Tenant_Id="TENANT_ID";
     public static final String DataBase_Company_Id="COMPANY_ID";*/

	/* public static final String DataBase_Task="TASK";
     public static final String DataBase_Product="PRODUCT";
     public static final String DataBase_Quality="QUALITY";*/
	public static final String DataBase_Month01Qty="MONTH01QTY";
	public static final String DataBase_Month02Qty="MONTH02QTY";
	public static final String DataBase_Month03Qty="MONTH03QTY";
	public static final String DataBase_Month04Qty="MONTH04QTY";
	public static final String DataBase_Month05Qty="MONTH05QTY";
	public static final String DataBase_Month06Qty="MONTH06QTY";
	public static final String DataBase_Month07Qty="MONTH07QTY";
	public static final String DataBase_Month08Qty="MONTH08QTY";
	public static final String DataBase_Month09Qty="MONTH09QTY";
	public static final String DataBase_Month10Qty="MONTH10QTY";
	public static final String DataBase_Month11Qty="MONTH11QTY";
	public static final String DataBase_Month12Qty="MONTH12QTY";
	public static final String DataBase_Week01Qty="WEEK01QTY";
	public static final String DataBase_Week02Qty="WEEK02QTY";
	public static final String DataBase_Week03Qty="WEEK03QTY";
	public static final String DataBase_Week04Qty="WEEK04QTY";
	public static final String DataBase_Week05Qty="WEEK05QTY";
	public static final String DataBase_Week06Qty="WEEK06QTY";
	public static final String DataBase_Week07Qty="WEEK07QTY";
	public static final String DataBase_Week08Qty="WEEK08QTY";
	public static final String DataBase_Week09Qty="WEEK09QTY";
	public static final String DataBase_Week10Qty="WEEK10QTY";
	public static final String DataBase_Week11Qty="WEEK11QTY";
	public static final String DataBase_Week12Qty="WEEK12QTY";
	public static final String DataBase_Week13Qty="WEEK13QTY";
	public static final String DataBase_Week14Qty="WEEK14QTY";
	public static final String DataBase_Week15Qty="WEEK15QTY";
	public static final String DataBase_Week16Qty="WEEK16QTY";
	public static final String DataBase_Week17Qty="WEEK17QTY";
	public static final String DataBase_Week18Qty="WEEK18QTY";
	public static final String DataBase_Week19Qty="WEEK19QTY";
	public static final String DataBase_Week20Qty="WEEK20QTY";
	public static final String DataBase_Week21Qty="WEEK21QTY";
	public static final String DataBase_Week22Qty="WEEK22QTY";
	public static final String DataBase_Week23Qty="WEEK23QTY";
	public static final String DataBase_Week24Qty="WEEK24QTY";
	public static final String DataBase_Week25Qty="WEEK25QTY";
	public static final String DataBase_Week26Qty="WEEK26QTY";
	public static final String DataBase_Week27Qty="WEEK27QTY";
	public static final String DataBase_Week28Qty="WEEK28QTY";
	public static final String DataBase_Week29Qty="WEEK29QTY";
	public static final String DataBase_Week30Qty="WEEK30QTY";
	public static final String DataBase_Week31Qty="WEEK31QTY";
	public static final String DataBase_Week32Qty="WEEK32QTY";
	public static final String DataBase_Week33Qty="WEEK33QTY";
	public static final String DataBase_Week34Qty="WEEK34QTY";
	public static final String DataBase_Week35Qty="WEEK35QTY";
	public static final String DataBase_Week36Qty="WEEK36QTY";
	public static final String DataBase_Week37Qty="WEEK37QTY";
	public static final String DataBase_Week38Qty="WEEK38QTY";
	public static final String DataBase_Week39Qty="WEEK39QTY";
	public static final String DataBase_Week40Qty="WEEK40QTY";
	public static final String DataBase_Week41Qty="WEEK41QTY";
	public static final String DataBase_Week42Qty="WEEK42QTY";
	public static final String DataBase_Week43Qty="WEEK43QTY";
	public static final String DataBase_Week44Qty="WEEK44QTY";
	public static final String DataBase_Week45Qty="WEEK45QTY";
	public static final String DataBase_Week46Qty="WEEK46QTY";
	public static final String DataBase_Week47Qty="WEEK47QTY";
	public static final String DataBase_Week48Qty="WEEK48QTY";
	public static final String DataBase_Week49Qty="WEEK49QTY";
	public static final String DataBase_Week50Qty="WEEK50QTY";
	public static final String DataBase_Week51Qty="WEEK51QTY";
	public static final String DataBase_Week52Qty="WEEK52QTY";

	public static final String DataBase_TableName_Work_Type_Headers="WORKTYPEHEADERS";
	public static final String DataBase_FulFillment_Center_Id="FULFILLMENT_CENTER_ID";

	public static final String DataBase_Priority="PRIORITY";

	public static final String DataBase_Sales_Order_Id="SALES_ORDER_ID";
	public static final String DataBase_Purchase_order_number="PURCHASE_ORDER_NUMBER";
	public static final String DataBase_Order_Id="ORDER_ID";
	public static final String DataBase_Accountion_Code="ACCOUNTING_CODE";
	public static final String DataBase_Foreign_Key_Name_CMPY="FK_CMPY";
	public static final String DataBase_Foreign_Key_Name_TNTS="FK_TNTS";


	/**
	 *constant of all database queries 
	 * 
	 */
	public static final String SecurityAuthorizations_SelectInvalidAttemptQuery="from SECURITYAUTHORIZATIONS where UPPER(User_Id)=?";
	public static final String SecurityAuthorizations_SelectQuery="from SECURITYAUTHORIZATIONS";
	public static final String MenuMessages_ProcedureCallQuery="CALL UPDATEMENUMESSAGESTATUS(:Tenant_Id,:Status)";
	public static final String SecurityAuthorizations_ProcedureCallQuery="CALL UPDATENEWPASSWORD(:User_Id,:Password,:Pdate)";
	public static final String SecurityAuthorizations_InvalidAttempt_Query="update SECURITYAUTHORIZATIONS set number_attempts=? where UPPER(user_Id)=?";
	public static final String SecurityAuthorizations_SetNumberAttempt_ToZero_Query="update SECURITYAUTHORIZATIONS set number_attempts=?, invalid_attempt_date=? where UPPER(user_Id)=?";
	public static final String GETFulfillmentCenter_Query="from GETFULFILLMENTCENTERS where UPPER(Tenant_Id)=? and UPPER(Fulfillment_Center_ID)=?";
	public static final String FulfillmentCenter_Query="from FULFILLMENT_CENTERS where UPPER(Tenant_Id)=? and UPPER(Fulfillment_Center_ID)=?";
	public static final String GetLanguagesKeyPhrase_Query="from GETLANGUAGES where UPPER(Key_Phrase)=? and UPPER(Language)=?";
	public static final String GetLanguagesObjectCode_Query="from GETLANGUAGES where UPPER(Language)=? and UPPER(Object_Code)=?";
	public static final String Languages_Query="from LANGUAGES where UPPER(object_code)=? and UPPER(language)=?";
	public static final String GeneralCode_SelectQuery="from GENERALCODES where Company_Id=?";
	public static final String MenuMessages_Query="from MENUMESSAGES where Tenant_Id=? and Status=?";
	public static final String Tenants_Query="from TENANTS where Tenant_Id=?";
	public static final String GetTEAM_MEMBERS = "from TEAM_MEMBERS where UPPER(TEAMMEMBER)=?";
	public static final String SecurityAuthorizations_SetTemporaryPassword_Query="update SECURITYAUTHORIZATIONS set password=?, status=?,PASSWORD_DATE=?, number_attempts=? where upper(TENANT_ID)=? and upper(TEAM_MEMBER_ID)=?";
	public static final String SecurityAuthorizations_getInvalidAttempts_Query="from SECURITYAUTHORIZATIONS where upper(TENANT_Id)=? and upper(TEAM_MEMBER_Id)=?";
	public static final String TeamMembers_EmailId_Query="from TEAMMEMBERS where UPPER(PERSONAL_EMAIL_ADDRESS)=?";
	public static final String MenuMessgaes_ProcedureCallQuery="CALL UPDATEMENUMESSAGESTATUS(:Tenant,:Status)";
	public static final String InfoHelps_Query = "from INFOHELPS where UPPER(Info_Help)=? and UPPER(Language)=? and UPPER(Info_Help_Type)=?";
	public static final String CommonInfoHelps_Query = "from GETINFOHELPS where UPPER(Info_Help)=? and UPPER(Language)=? and UPPER(Info_Help_Type)=?";
	public static final String LoginDaoImpl_Query = "from SECURITYAUTHORIZATIONS Where UPPER(User_Id) =?";
	public static final String UpdateProductLOCATION_PROFILES = "from LOCATION_PROFILES where UPPER(Tenant_id)=? and UPPER(Fulfillment_Center_ID)=? and UPPER(Company_Id)=?";
	public static final String UpdateProductDeletINVENTORY_LEVELS = "delete INVENTORY_LEVELS where UPPER(Tenant_ID) = ? AND UPPER(Fulfillment_Center_ID) = ? AND UPPER(Company_Id) = ?";
	public static final String UpdateProductUPDATEINVENTORY_LEVELS ="UPDATE INVENTORY_LEVELS SET Quantity = ? , Last_Activity_Date = ? , Last_Activity_Employee = ? , Last_Activity_Task = ? WHERE Tenant_id = ? AND Fulfillment_Center_ID = ? AND Company_id = ? AND Area = ? AND Location = ? AND Product = ? AND Quality = ? AND Lot = ? AND Serial01 = ? AND Serial02 = ? AND Serial03 = ? AND Serial04 = ? AND Serial05 = ? AND Serial06 = ? AND Serial07 = ? AND Serial08 = ? AND Serial09 = ? AND Serial10 = ? AND Serial01 = ? AND Serial01 = ? AND LPN = ? AND Inventory_Control01 = ? AND Inventory_Control02 = ? AND Inventory_Control03 = ? AND Inventory_Control04 = ? AND Inventory_Control05 = ? AND Inventory_Control06 = ? AND Inventory_Control07 = ? AND Inventory_Control08 = ? AND Inventory_Control09 = ? AND Inventory_Control10 = ?";
	public static final String INVENTORY_TRANSACTIONS = "insert into INVENTORY_TRANSACTIONS (Tenant_id, Fulfillment_Center_ID, Company_id, Area, Location, Product, Quality, Lot, Serial, LPN, Inventory_Control01, Inventory_Control02, Inventory_Control03, Inventory_Control04, Inventory_Control05, Inventory_Control06, Inventory_Control07, Inventory_Control08, Inventory_Control09, Inventory_Control10, Quantity_Transaction, Quantity_Before, Quantity_After, Case_Pack_Qty, Date_Control, Inventory_Pick_Sequence, Inventory_Value, Work_Type, Work_Control_Number, Reference, Purchase_Order_Number, Last_Activity_Date, Last_Activity_Team_Member, Last_Activity_Task) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String LOCATIONS = "From LOCATIONS WHERE UPPER(LOCATION_PROFILE) = ?";
	public static final String LOCATION_PROFILES = "From LOCATION_PROFILES WHERE UPPER(PROFILE) = ?";
	public static final String UPDATELOCATIONS = "UPDATE LOCATIONS SET CCActivity_Point = ? WHERE UPPER(Tenant_Id) = ? AND Fulfillment_Center_ID = ? AND UPPER(Company_id) = ? AND UPPER(Location) = ?";
	public static final String INVENTORY_LEVELS = "Insert into INVENTORY_LEVELS (TENANT_Id,Fulfillment_Center_ID,COMPANY_ID,AREA,LOCATION,PRODUCT,QUALITY,LOT,SERIAL01,SERIAL02,SERIAL03,SERIAL04,SERIAL05,SERIAL06,SERIAL07,SERIAL08,SERIAL09,SERIAL10,SERIAL11,SERIAL12,LPN,INVENTORY_CONTROL01,INVENTORY_CONTROL02,INVENTORY_CONTROL03,INVENTORY_CONTROL04,INVENTORY_CONTROL05,INVENTORY_CONTROL06,INVENTORY_CONTROL07,INVENTORY_CONTROL08,INVENTORY_CONTROL09,INVENTORY_CONTROL10,QUANTITY,UNIT_OF_MEASURE_CODE,UNIT_OF_MEASURE_QTY,DATE_CONTROL,PRODUCT_VALUE,DATE_RECEIVED,LAST_ACTIVITY_DATE,LAST_ACTIVITY_EMPLOYEE,LAST_ACTIVITY_TASK,HOLD_CODE,SYSTEM_CODE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String GENERAL_CODES = "from GENERALCODES WHERE UPPER(Tenant_id) = ? AND UPPER(Company_Id) = ? AND UPPER(General_Code_ID) = ? AND UPPER(General_Code) = ?";
	public static final String GENERAL_CODES_DROPDOWN = "select General_Code,Description20,Description50 from GENERAL_CODES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(General_Code_ID) = ?";
	//public static final String GeneralCodes_QuerySelectGeneralCodeComboBox= "select distinct GENERAL_CODE from GENERAL_CODES where UPPER(GENERAL_CODE_ID)='GENERALCODES' and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?";
	public static final String GeneralCodes_QueryGeneralCodeNewSubList="from GENERALCODES where UPPER(Application_ID)=? and UPPER(Tenant_Id)=? and UPPER(Company_Id)=?";
	public static final String GeneralCodes_getID="from GENERALCODES where Tenant = ?";
	public static final String TeamMembers_AllTeamMember_Query="select tm.TENANT_ID,tm.TEAM_MEMBER_ID,tm.LAST_NAME,tm.FIRST_NAME,tm.MIDDLE_NAME,tm.CURRENT_STATUS,nvl2(sa.USER_ID, sa.STATUS, 'X') as Status from team_members tm left join SECURITY_AUTHORIZATIONS sa on tm.TEAM_MEMBER_ID=sa.TEAM_MEMBER_ID where upper(tm.tenant_id) = ?";
	public static final String GeneralCodes_QuerySelectGeneralCodeList= "select distinct GENERAL_CODE from GENERAL_CODES where GENERAL_CODE_ID='GENERALC'";
	public static final String TeamMembers_SelectTeamMember_AllTeamMember_OrderByActivityDateConditionalQuery="from TEAMMEMBERS where UPPER(TENANT_ID)=? AND UPPER(Fulfillment_Center_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_ConditionalQuery="TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_ConditionalQuery";
	public static final String TeamMembers_QueryNamed_SelectTeamMember_PartOfTeamMember_ConditionalQuery="TeamMembers_QueryNamed_SelectTeamMember_PartOfTeamMember_ConditionalQuery";
	public static final String SecurityAuthorizations_QueryNamed_Update_Status_Query="SecurityAuthorizations_QueryNamed_Update_Status_Query";
	public static final String Companies_QueryNamed_SetInactive1_TeamMember_Companies_Query="Companies_QueryNamed_SetInactive1_TeamMember_Companies_Query";
	public static final String Companies_QueryNamed_Select_Tenant_Companies_Query="Companies_QueryNamed_Select_Tenant_Companies_Query";
	public static final String Generalcode_QueryNamed_Select_Conditional_Query="Generalcode_QueryNamed_Select_Conditional_Query";
	public static final String Companies_QueryNamed_Select_TeamMember_Companies_Query="Companies_QueryNamed_Select_TeamMember_Companies_Query";
	public static final String Companies_QueryNamed_Delete_TeamMember_Companies_Query="Companies_QueryNamed_Delete_TeamMember_Companies_Query";
	public static final String Languages_QueryNamed="Languages_QueryNamed";
	public static final String TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_OrderByActivityDateConditionalQuery="TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_OrderByActivityDateConditionalQuery";
	public static final String TeamMembers_QueryNamed_SelectTeamMember_ALLPartOfTeamMember_LastNameConditionalQuery="TeamMembers_QueryNamed_SelectTeamMember_ALLPartOfTeamMember_LastNameConditionalQuery";
	public static final String TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_OrderByLastNameConditionalQuery="TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_OrderByLastNameConditionalQuery";
	public static final String TeamMembers_QueryNamed_Select_Email_Query="TeamMembers_QueryNamed_Select_Email_Query";
	public static final String TeamMembers_QueryNamed_Availability_Query="TeamMembers_QueryNamed_Availability_Query";	
	public static final String SecurityAuthorizations_TeamMember_ConditionalQuery="from SECURITYAUTHORIZATIONS WHERE UPPER(TEAM_MEMBER_ID)=UPPER(?) and UPPER(TENANT_ID)=UPPER(?)"; 
	public static final String Query_GeneralCodes_SecurityQuestion="from GENERALCODES where GENERAL_CODE_ID=?";
	public static final String GeneralCodes_QuerySelectGeneralCodeListPart1= "select DESCRIPTION50 from GENERAL_CODES where UPPER(GENERAL_CODE_ID)=UPPER('";
	public static final String GeneralCodes_QuerySelectGeneralCodeListPart2="')";
	public static final String GeneralCodes_QuerySelectDescription50 = "select DESCRIPTION50 from GENERAL_CODES where UPPER(GENERAL_CODE_ID)=? and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?";
	public static final String Security_QuerySelectUserIDPart1= "select * from SECURITY_AUTHORIZATIONS where Upper(user_id)=Upper('";
	public static final String Security_QuerySelectUserIDPart2="')";
	public static final String Security_CommonQueryEnd="')";
	/**
	 *Check Screen Access query
	 * 
	 */
	/*public static final String Security_SelectScrrenAccessPart1="select MO.Menu_Option from Menu_Profile_Assignments MPA join Menu_Profile_Headers MPH on MPA.Menu_Profile=MPH.Menu_Profile join MENU_PROFILE_Options MPO on MPH.Menu_Profile=MPO.Menu_Profile join Menu_Options MO on MPO.Menu_Option=Mo.Menu_Option where upper(MPA.Tenant_ID)=upper('";
	public static final String Security_SelectScrrenAccessPart2="') and upper(MPA.Team_Member_ID)=upper('";
	public static final String Security_SelectScrrenAccessPart3="') and upper(MPH.Menu_Type)=upper('";
	public static final String Security_SelectScrrenAccessPart4="') and upper(MO.MENU_TYPE)=upper('";
	public static final String Security_SelectScrrenAccessPart5="') and upper(Mo.Menu_Option) = upper('";*/
	public static final String Security_SelectScrrenAccess = "select MO.Menu_Option from Menu_Profile_Assignments MPA join Menu_Profile_Headers MPH on "
			+ "MPA.Menu_Profile=MPH.Menu_Profile and MPA.Tenant_ID = MPH.Tenant_ID join MENU_PROFILE_Options MPO on MPH.Menu_Profile=MPO.Menu_Profile and MPA.Tenant_ID = MPO.Tenant_ID "
			+ "join Menu_Options MO on MPO.Menu_Option=MO.Menu_Option and (MPO.Tenant_ID=MO.Tenant_ID or UPPER(MO.Tenant_ID)=?) where upper(MPA.Tenant_ID)=? and "
			+ "upper(MPA.Team_Member_ID)=? and upper(MO.EXECUTION) = ? and UPPER(MO.Menu_Type) = ? and UPPER(MPH.Menu_Type) = ?"; 

	public static final String Security_SelectScrrenAccess_JASCI = "select MO.Menu_Option from Menu_Profile_Assignments MPA join Menu_Profile_Headers MPH on "
			+ "MPA.Menu_Profile=MPH.Menu_Profile and MPA.Tenant_ID = MPH.Tenant_ID join MENU_PROFILE_Options MPO on MPH.Menu_Profile=MPO.Menu_Profile and MPA.Tenant_ID = MPO.Tenant_ID "
			+ "join Menu_Options MO on MPO.Menu_Option=MO.Menu_Option where upper(MPA.Tenant_ID)=? and "
			+ "upper(MPA.Team_Member_ID)=? and upper(MO.EXECUTION) = ? and UPPER(MO.Menu_Type) = ? and UPPER(MPH.Menu_Type) = ?"; 

	public static final String Security_ScreenLabels= " from LANGUAGES where OBJECT_CODE in ('TeamMemberLookUpScreen','TeamMemberSelectionScreen','SequrityMantinenceScreen') and UPPER(LANGUAGE)=UPPER('";
	public static final String Security_Q_UpdateUSerStatusPart1="delete from SECURITY_AUTHORIZATIONS where UPPER(TENANT_ID)='";
	public static final String Security_Q_UpdateUSerStatusPart2="' AND Upper(TEAM_MEMBER_ID)='";
	public static final String Security_CheckForUserExistence="select User_ID from SECURITY_AUTHORIZATIONS where Upper(USER_ID)=UPPER('";
	public static final String Security_CheckForUserCurrentStatus="select STATUS from SECURITY_AUTHORIZATIONS where Upper(TEAM_MEMBER_ID)=UPPER('";
	public static final String Security_Q_SelectTeamMemberEmailID="select PERSONAL_EMAIL_ADDRESS,FIRST_NAME,LAST_NAME from team_members where UPPER(TEAM_MEMBER_ID)=UPPER('";
	public static final String Generalcode_Select_Conditional_Query="from GENERALCODES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(GENERAL_CODE_ID)=? ORDER BY UPPER(DESCRIPTION20)";
	public static final String TeamMembers_SelectTeamMember_TeamMemberName_ConditionalQuery="from TEAMMEMBERS where UPPER(TEAM_MEMBER_ID)=? AND UPPER(TENANT_ID)=?";
	public static final String TeamMembers_SelectTeamMember_AllTeamMember_ConditionalQuery="from TEAMMEMBERS where UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembers_SelectTeamMember_AllTeamMember_OrderByLastNameConditionalQuery="from TEAMMEMBERS where UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembers_SelectTeamMember_AllTeamMember_OrderByFirstNameConditionalQuery="from TEAMMEMBERS where UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembers_SelectTeamMember_ALLPartOfTeamMember_LastNameConditionalQuery="from TEAMMEMBERS where (UPPER(LAST_NAME)=? OR UPPER(FIRST_NAME)=? OR UPPER(MIDDLE_NAME)=?) AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembers_SelectTeamMember_ALLPartOfTeamMember_FirstNameConditionalQuery="from TEAMMEMBERS where (UPPER(LAST_NAME)=? OR UPPER(FIRST_NAME)=? OR UPPER(MIDDLE_NAME)=?) AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC ";
	public static final String TeamMembers_Select_Email_Query="from TEAMMEMBERS where UPPER(PERSONAL_EMAIL_ADDRESS)=?";
	public static final String Companies_Select_TeamMember_Companies_Query="from TEAMMEMBERCOMPANIES where UPPER(TENANT_ID)=? AND UPPER(TEAM_MEMBER_ID)=?";
	public static final String Companies_Delete_TeamMember_Companies_Query="delete from TEAMMEMBERCOMPANIES where UPPER(TENANT_ID)= :"+GLOBALCONSTANT.DataBase_TeamMembersCompanies_Tenant+" AND UPPER(TEAM_MEMBER_ID)= :"+GLOBALCONSTANT.DataBase_TeamMembersCompanies_TeamMember;
	public static final String SecurityAuthorizations_Update_Status_Query="update SECURITY_AUTHORIZATIONS set STATUS=? where UPPER(TENANT_ID)=? AND UPPER(TEAM_MEMBER_ID)=? AND UPPER(DEFAULT_COMPANY_ID)=?";
	public static final String Companies_SetInactive1_TeamMember_Companies_Query="update TEAMMEMBERCOMPANIES set CURRENT_STATUS=? where UPPER(TENANT_ID)=? AND UPPER(TEAM_MEMBER_ID)=?";
	public static final String Companies_SetInactive_TeamMember_Companies_Query="update TEAMMEMBERCOMPANIES set CURRENT_STATUS=? where UPPER(TENANT_ID)= :"+GLOBALCONSTANT.DataBase_TeamMembersCompanies_Tenant+" AND UPPER(TEAM_MEMBER_ID)= :"+GLOBALCONSTANT.DataBase_TeamMembersCompanies_TeamMember;
	public static final String TeamMembers_Availability_Query="from TEAMMEMBERS where UPPER(TEAM_MEMBER_ID)=? AND UPPER(TENANT_ID)=? ";
	public static final String Companies_Select_Tenant_Companies_Query="from COMPANIES where UPPER(TENANT_ID)=? AND UPPER(STATUS)=?";
	public static final String Delete_TeamMember_Securty_Authorization_Query="delete from SECURITYAUTHORIZATIONS where UPPER(TENANT_ID)= :"+GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant+" AND UPPER(Default_Company_Id)= :"+GLOBALCONSTANT.DataBase_SecurityAuthorizations_Company+" AND UPPER(TEAM_MEMBER_ID)= :"+GLOBALCONSTANT.DataBase_SecurityAuthorizations_TeamMember;
	public static final String Companies_Delete_TeamMember_Securty_Authorization_Query="delete from SECURITY_AUTHORIZATIONS where UPPER(TENANT_ID)= :"+GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant+" AND UPPER(Default_Company_Id)= :"+GLOBALCONSTANT.DataBase_SecurityAuthorizations_Company+" AND UPPER(TEAM_MEMBER_ID)= :"+GLOBALCONSTANT.DataBase_SecurityAuthorizations_TeamMember;
	public static final String InputQuery_MenuOption_MenuProfile="menuProfile";

	public static final String NativeQueryName_MenuOption_query="select MO.MENU_OPTION as Menu_Option,"
			+ " MO.APPLICATION_ID as Application_ID,"
			+ "MO.APPLICATION_SUB as Application_Sub, "
			+ "MO.EXECUTION as Execution,"
			+ " MO.DESCRIPTION50 || ' ' || MO.HELP_LINE as Help"
			+ " from MENU_PROFILE_OPTIONS MPO, MENU_OPTIONS MO"
			+ " where Upper(MPO.MENU_PROFILE)=:menuProfile and Upper(MPO.TENANT_ID)=:tenant"
			+ " and Upper(MO.MENU_OPTION)=Upper(MPO.MENU_OPTION) and (Upper(MO.TENANT_ID) = upper(MPO.TENANT_ID) or Upper(MO.TENANT_ID) = :tenant1 )";
	public static final String NativeQueryName_MenuOption_query_JASCI="select MO.MENU_OPTION as Menu_Option,"
			+ " MO.APPLICATION_ID as Application_ID,"
			+ "MO.APPLICATION_SUB as Application_Sub, "
			+ "MO.EXECUTION as Execution,"
			+ " MO.DESCRIPTION50 || ' ' || MO.HELP_LINE as Help"
			+ " from MENU_PROFILE_OPTIONS MPO, MENU_OPTIONS MO"
			+ " where Upper(MPO.MENU_PROFILE)=:menuProfile and Upper(MPO.TENANT_ID)=:tenant"
			+ " and Upper(MO.MENU_OPTION)=Upper(MPO.MENU_OPTION)";

	public static final String NativeQueryName_MenuOption_query_SystemUseY="select MO.MENU_OPTION as Menu_Option,"
			+ " MO.APPLICATION_ID as Application_ID,"
			+ "MO.APPLICATION_SUB as Application_Sub, "
			+ "MO.EXECUTION as Execution,"
			+ " MO.DESCRIPTION50 || ' ' || MO.HELP_LINE as Help"
			+ " from MENU_PROFILE_OPTIONS MPO, MENU_OPTIONS MO"
			+ " where Upper(MPO.MENU_PROFILE)=:menuProfile and Upper(MPO.TENANT_ID)=:tenant"
			+ " and Upper(MO.MENU_OPTION)=Upper(MPO.MENU_OPTION) and (Upper(MO.TENANT_ID) = upper(MPO.TENANT_ID) or Upper(MO.TENANT_ID) = :tenant1 )"
			+ "and Upper(MO.EXECUTION) <> :EXECUTIONPATH";
	public static final String NativeQueryName_MenuOption_query_SystemUseY_JASCI="select MO.MENU_OPTION as Menu_Option,"
			+ " MO.APPLICATION_ID as Application_ID,"
			+ "MO.APPLICATION_SUB as Application_Sub, "
			+ "MO.EXECUTION as Execution,"
			+ " MO.DESCRIPTION50 || ' ' || MO.HELP_LINE as Help"
			+ " from MENU_PROFILE_OPTIONS MPO, MENU_OPTIONS MO"
			+ " where Upper(MPO.MENU_PROFILE)=:menuProfile and Upper(MPO.TENANT_ID)=:tenant"
			+ " and Upper(MO.MENU_OPTION)=Upper(MPO.MENU_OPTION) "
			+ "and Upper(MO.EXECUTION) <> :EXECUTIONPATH";

	public static final String NativeQueryName_MenuProfileOptions_ResultSetMapping="RESELT_SET_COMMON_MENUPROFILEOPTION";
	public static final String NativeQueryName_MenuProfileOptions="MenuProfileOptionList";
	public static final String NativeQueryName_Team_member_Companies_ResultSetMapping="RESELT_SET_COMMON_COMPNYBE";
	public static final String InputQuery_TeamMember_TeamMember="teamMember";
	public static final String NativeQueryName_MenuProfileAssigment="MENU_PROFILE_TABEL";
	public static final String InputQuery_MenuProfileAssigment_MenuType="menuType";
	public static final String InputQuery_MenuProfileAssigment_Tenant="tenant";
	public static final String NativeQueryName_MenuProfileAssigment_ResultSetMapping="RESELT_SET_COMMON_MENUPROFILE";

	public static final String NativeQueryName_MenuProfileAssigment_query = "select MPA.MENU_PROFILE AS PROFILE,MPH.APP_ICON AS APPICON,MAI.APP_ICON as MAI_AppICon,"
			+ "MAI.APP_ICON_ADDRESS AS APPICONADDRESS, MPH.DESCRIPTION50 ||' '|| MPH.HELP_LINE as Help from MENU_PROFILE_ASSIGNMENTS MPA join MENU_PROFILE_HEADERS MPH "
			+ "on Upper(MPA.TENANT_ID)=Upper(MPH.TENANT_ID)left join MENU_APP_ICONS MAI on Upper(MAI.APP_ICON)=Upper(MPH.APP_ICON) where Upper(MPA.MENU_PROFILE)="
			+ "Upper(MPH.MENU_PROFILE ) and Upper(MPA.TENANT_ID)=:tenant and Upper(MPA.TEAM_MEMBER_ID)=:teamMember and Upper(MPH.MENU_TYPE)=:menuType";

	public static final String InputQuery_MenuMessageStatus="status";
	public static final String InputQuery_MenuMessage_updateStatus="updateStatus";

	public static final String NativeQueryName_MainMenu_GetMessages_query="select TICKET_TAPE_MESSAGE,MESSAGE "
			+ "from MENU_MESSAGES where UPPER(tenant_ID)=:tenant "
			+ "and UPPER(status)=:status and END_DATE > SYSDATE";

	public static final String NativeQueryName_MainMenu_UpdateMessagesStatus_query="update MENU_MESSAGES "
			+ "set STATUS=:updateStatus where UPPER(TENANT_ID) =:tenant AND UPPER(STATUS)"
			+ " =:status AND ENDDATE < SYSDATE";

	public static final String QueryName_MenuMessage_Select="GetMessagesfromMenuMessages";
	public static final String QueryName_MenuMessage_Update="UpdateMessagesStatus";
	public static final String NativeQueryName_MenuExecutionLabel_ResultSetMapping="RESELT_SET_COMMON_MENUEXECUTION";

	public static final String NativeQueryName_MenuExecutionLabel_query="select TRANSLATION as TRANSLATION from "
			+ "LANGUAGES where Upper(LANGUAGE)=:languageName "
			+ "AND Upper(OBJECT_CODE)=:ScreenName";

	public static final String InfoHelps_QueryNameSearchLookup="SearchLookup";
	public static final String InfoHelps_QueryDeleteInfoHelp="delete from INFO_HELPS where UPPER(INFO_HELP)=? and UPPER(LANGUAGE)=?";
	public static final String InfoHelps_QueryNameFetchInfoHelp="FetchInfoHelp";
	public static final String InfoHelps_QueryFetchInfoHelp="select * from INFO_HELPS where UPPER(INFO_HELP)=? and UPPER(LANGUAGE)=?";
	public static final String InfoHelpSearchlookup_Language_Query="from LANGUAGES where OBJECT_CODE in ('InfoHelpAssignmentSearch','InfoHelpSearchlookup','InfoHelpassignmentmaintenance') and UPPER(LANGUAGE)=?";
	public static final String Infohelps_Query="from INFO_HELPS where UPPER(Info_Help)=? and UPPER(Language)=?";
	public static final String InfoHelps_QueryNameGeneralCodeList="QueryNameGeneralCodeList";
	public static final String InfoHelps_QueryGeneralCodeList="Select * from GENERAL_CODES where GENERAL_CODE_ID in ('LANGUAGES','INFOHELP')";
	public static final String MenuMessage_QueryNameGetCompanyList="CompanyList";
	public static final String MenuMessage_QueryGetCompanyList="Select * from TEAM_MEMBER_COMPANIES tmc join COMPANIES cm on Upper(tmc.TENANT_ID) = UPPER(cm.TENANT_ID) and UPPER(tmc.COMPANY_ID) = UPPER(cm.COMPANY_ID) where UPPER(tmc.TENANT_ID)=? and UPPER(tmc.Team_Member_ID)=? and UPPER(cm.STATUS) <> 'X'";
	public static final String MenuMessage_QueryName_UniqueTicketTapeMessage="UniqueTicketTapeMessage";
	public static final String MenuMessage_Query_UniqueTicketTapeMessage="Select * from MENU_MESSAGES where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(STATUS)=? and (UPPER(TICKET_TAPE_MESSAGE)=? or UPPER(MESSAGE1)=?)";
	public static final String MenuMessage_QueryNamefetchMenuMessage="fetchMenuMessage";
	public static final String MenuMessage_QueryfetchMenuMessage="select * from MENU_MESSAGES where UPPER(Tenant_ID)=? and UPPER(Company_ID)=? and UPPER(Status)=? and UPPER(Message_ID)=?";
	public static final String MenuMessages_Language_Query="from LANGUAGES where OBJECT_CODE = 'MenuMessage' and UPPER(LANGUAGE)=?";
	public static final String GeneralCodes_QueryGeneralCodeEditListUnique="select * from GENERAL_CODES where UPPER(TENANT_ID) = ? and UPPER(COMPANY_ID)=? and UPPER(APPLICATION_ID)=? and UPPER(GENERAL_CODE_ID)=? and UPPER(GENERAL_CODE)=?";
	public static final String GeneralCodes_QueryGeneralCodeIdentifiedEdit="select * from GENERAL_CODES where UPPER(TENANT_ID) = ? and UPPER(COMPANY_ID)=? and UPPER(APPLICATION_ID)=? and UPPER(GENERAL_CODE_ID)=? and UPPER(GENERAL_CODE)=?";
	public static final String GeneralCodes_QueryGeneralCodeidentificationList="select * from GENERAL_CODES gc1 where UPPER(gc1.APPLICATION_ID) not in (select UPPER(gc2.GENERAL_CODE) from GENERAL_CODES gc2 where UPPER(gc2.GENERAL_CODE_ID)=UPPER(gc2.APPLICATION_ID)) and UPPER(gc1.TENANT_ID)=? and UPPER(gc1.COMPANY_ID)=?";
	public static final String GeneralCodes_QueryCheckUniqueGeneralCode="Select GENERAL_CODE from GENERAL_CODES where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(APPLICATION_ID)=? and UPPER(GENERAL_CODE_ID)=? and (UPPER(GENERAL_CODE)=?)";
	public static final String GeneralCodes_QueryCheckUniqueManageGeneralCode="Select GENERAL_CODE from GENERAL_CODES where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(APPLICATION_ID)=? and UPPER(GENERAL_CODE_ID)=? and UPPER(GENERAL_CODE)=?";
	public static final String NativeQueryName_CheckingMenuProfileOptions_ResultSetMapping="RESELT_MENUPROFILEOPTION_CECKMENUOPTION";
	public static final String NativeQueryName_CheckingMenuProfileOptions="MenuProfileOptionListCheck";

	public static final String NativeQueryName_MenuOptionProfile_query="Select MENU_OPTION from MENU_PROFILE_OPTIONS "
			+ "where Upper(MENU_OPTION)=?";

	public static final String NativeQueryName_MenuOptions_GeneralCode_MenuTypeQuery="select * from GENERAL_CODES where Upper(GENERAL_CODE_ID)=:menuType And Upper(TENANT_ID)=:tenant"
			+ " and Upper(COMPANY_ID)=:company order by UPPER(DESCRIPTION20) asc";

	public static final String NativeQueryName_MenuOptions_ResultSetMapping="RESELT_MENUPROFILEOPTION_CECKMENUOPTION";
	public static final String NativeQueryName_MenuOptions="MenuOptionListBE";
	public static final String NativeQueryName_MenuOptions_query="";
	public static final String NativeQueryName_MenuOptions_GeneralCode_MenuTypeQueryName="MenuTypesGeneralCode";
	public static final String InputMenuTypeName_MenuOptions_GeneralCode_MenuTypeQueryName="menuType";
	public static final String NativeQueryName_MenuOptions_MenuOptionQuery_CheckOne="Select * from MENU_OPTIONS where UPPER(MENU_OPTION)=:Menu_Option";
	public static final String NativeQueryName_MenuOptions_MenuOptionQueryName_CheckOne="SelectDataOne";
	public static final String NativeQueryName_MenuOptions_MenuOptionQuery_CheckTwo="Select * from MENU_OPTIONS where UPPER(MENU_OPTION)=? and (UPPER(TENANT_ID)=? or UPPER(TENANT_ID)=?)";
	public static final String NativeQueryName_MenuOptions_MenuOptionQueryName_CheckTwo="SelectDataTwo";

	public static final String NativeQueryName_MenuOptions_LabelQuery="select * from LANGUAGES where "
			+ "Upper(KEY_PHRASE)in (Upper('actions'),upper('edit'),upper('MenuOptionSearchLookUp')"
			+",upper('tenant'),upper('menutype'),upper('menuoption'),upper('description'),upper('application'),upper('applicationSub'),"
			+"upper('Actions'), upper('Cancel'), upper('Delete'), upper('Edit'), upper('Add New'), upper('new'), upper('partOfApplication')"
			+", upper('PartOfMenuOption'),upper('PartOFtheMenuOptionDescription'),upper('tenantID'),upper('LastActivityDate'),upper('lastactivityby')"
			+",upper('helpline'),upper('ExecutionPath'),upper('MenuOptionMantenance'),upper('MenuOptionMaintenanceLookUp'),upper('Company'),Upper('Save/Update')"
			+",upper('displayall'),upper('ERR_INVALID_APPLICATION'),upper('ERR_APPLICATION_LEFT_BLANK'),upper('ERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK'),"
			+"upper('ERR_INVALID_MENU_OPTION'),upper('ERR_INVALID_PART_OF_THE_APPLICATION_NAME'),upper('ERR_MENU_OPTION_LEFT_BLANK'),upper('ERR_INVALID_PART_OF_THE_MENU_OPTION'),"
			+"upper('ERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK'),upper('ERR_MENU_TYPE_LEFT_BLANK'),upper('ERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION'),"
			+"upper('ERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK'),upper('ERR_WHILE_DELETING_MENU_PROFILE'),"
			+"upper('ERR_WHILE_DELETING_MENU_OPTION'),upper('ERR_MENU_OPTION_ALREADY_USED'),upper('ERR_MANDATORY FIELD_LEFT BLANK'),"
			+"upper('ON_SAVE_SUCCESSFULLY'),upper('ON_UPDATE_SUCCESSFULLY'),upper('descriptionshrot'),"
			+"upper('descriptionlong'), upper('Applications') ,upper('MenuTypes'),upper('Select')"
			+") and Upper(LANGUAGE)=:language";

	public static final String NativeQueryName_MenuOptions_LabelQueryName="MenuOptionLabelQury";
	public static final String MenuAppIcon_GetList_NamedQuery="MenuAppIconGetList";
	public static final String MenuAppIcon_GetListByAppIcon_NamedQuery="MenuAppIconByAppIcon";
	public static final String MenuAppIcon_GetListByAppIcon_Query="select * from MENU_APP_ICONS where UPPER(App_Icon)=?";
	public static final String MenuAppIcon_GetListByApplication_NamedQuery="MenuAppIconByApplicationAppIcon";
	public static final String MenuAppIcon_GetListByApplication_Query="select * from MENU_APP_ICONS where UPPER(APPLICATION_ID)=?";
	public static final String MenuAppIcon_GetListByAppIconName_NamedQuery="MenuAppIconByAppIconName";
	public static final String MenuAppIcon_GetListByAppIconPart_NamedQuery="MenuAppIconByAppIconPart";
	public static final String MenuAppIcon_GetListByAppIconPart_Query="select * from MENU_APP_ICONS where UPPER(NAME20)=? OR UPPER(NAME50)=?";
	public static final String MenuAppIcon_Delete_NamedQuery="MenuAppIcon_Delete_NamedQuery";
	public static final String MenuAppIcon_Delete_Query="delete from MENU_APP_ICONS where UPPER(APPLICATION_ID)= :"+GLOBALCONSTANT.DataBase_MenuAppIcons_APPLICATION+" AND UPPER(APP_ICON)= :"+GLOBALCONSTANT.DataBase_MenuAppIcons_APP_ICON;
	public static final String MenuAppIcon_GetTeamMember_Name_Query="from TEAMMEMBERS where UPPER(TENANT_ID)=? and UPPER(TEAM_MEMBER_ID)=?";
	/**
	 *  Queries to Fetch data from TeamMembers.
	 *  
	 */
	public static final String TeamMembersMessages_GeneralCodes_Department_ShiftQuery="from GENERALCODES where UPPER(GENERAL_CODE_ID)=upper('department') or UPPER(GENERAL_CODE_ID)=UPPER('shiftcode') AND UPPER(TENANT_ID)=? AND UPPER(COMPANY_Id)=?";
	public static final String TeamMembersMessages_SelectTeamMember_TeamMember_ConditionalQuery="from TEAMMEMBERS where UPPER(TEAM_MEMBER_ID)=? AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembersMessages_SelectTeamMember_PartOfTeamMember_ConditionalQuery="from TEAMMEMBERS where (UPPER(LAST_NAME)=? OR UPPER(FIRST_NAME)=? OR UPPER(MIDDLE_NAME)=?) AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembersMessages_SelectTeamMember_Department_ConditionalQuery="select * from TEAM_MEMBERS where UPPER(Department)=(select Upper(GC.GENERAL_CODE) from GENERAL_CODES GC where Upper(GC.DESCRIPTION20)=? and upper(GC.GENERAL_CODE_ID)=upper('DEPARTMENT') and Upper(GC.TENANT_ID)=? and Upper(GC.COMPANY_Id)=?) AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembersMessages_QueryName_SelectTeamMember_Department="TeamMembersMessages_QueryName_SelectTeamMember_Department";
	public static final String TeamMembersMessages_SelectTeamMember_Shift_ConditionalQuery= "select * from TEAM_MEMBERS where UPPER(SHIFT_CODE)=(select Upper(GC.GENERAL_CODE) from GENERAL_CODES GC where Upper(GC.DESCRIPTION20)=? and upper(GC.GENERAL_CODE_ID)=upper('SHIFTCODE') and Upper(GC.TENANT_ID)=? and Upper(GC.COMPANY_Id)=? ) AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembersMessages_QueryName_SelectTeamMember_Shift="TeamMembersMessages_QueryName_SelectTeamMember_Shift";
	public static final String TeamMembersMessages_SelectTeamMember_AllTeamMember_ConditionalQuery="from TEAMMEMBERS where UPPER(TENANT_ID)=? ";
	public static final String TeamMembersMessages_SelectDepartment_GeneralCode_Query="Select GENERAL_CODE, DESCRIPTION20, GENERAL_CODE_ID from GENERAL_CODES where ( Upper(GENERAL_CODE_ID)=upper('department') or upper(GENERAL_CODE_ID)=upper('shiftcode')) and upper(TENANT_ID)=? and upper(COMPANY_ID)=?";
	public static final String TeamMembersMessages_SelectDepartment_GeneralCodeQuery_Name="TeamMembersMessages_SelectDepartment_GeneralCodeQuery_Name";
	public static final String TeamMembersMessages_SelectTeamMemberMessages_Edit_Query="from TEAMMEMBERMESSAGES where UPPER(TEAM_MEMBER_ID)=? AND upper(TENANT_ID)=? AND upper(COMPANY_ID)=? and message_Id=?";
	public static final String TeamMembersMessages_SelectTeamMemberMessages_MaxMessageNumber="select max(message_number) as MESSAGENUMBER from team_member_messages where UPPER(TEAM_MEMBER)=? AND upper(TENANT_ID)=? AND upper(COMPANY_ID)=?";
	public static final String TeamMembersMessages_SelectTeamMemberMessages_MaxMessageNumber_QueryName="TeamMembersMessages_SelectTeamMemberMessages_MaxMessageNumber_QueryName=";
	public static final String TeamMemberMessages_ScreenLabels_Query="from LANGUAGES where object_code=? and UPPER(language)=?";
	/**
	 *  queries for language translation
	 *  
	 */
	public static final String LanguageTranslation_ScreenLabelsQuery = "from LANGUAGES where object_code=? and UPPER(language)=?";

	public static final String NativeQueryName_MenuExecutionLabel_LabelQuery="select * from LANGUAGES where "
			+ "Upper(KEY_PHRASE)in (Upper('Team_Member'),upper('CompanyID'),"
			+ "upper('Name'),upper('MESSAGES'),upper('APPLICATIONS')) "
			+ "and Upper(LANGUAGE)=:language";

	public static final String NativeQueryName_MenuExecution_LabelQueryName="GetLanguageOfScreenMENUEXECUTION";
	/**
	 * For getting languages translation
	 * 
	 */
	public static final String NativeQueryName_CommonLanguageTranslation="GetLanguageTranslation";
	public static final String NativeQuery_CommonLanguageTranslation="select LANGUAGE,KEY_PHRASE,nvl2(TRANSLATION,TRANSLATION,KEY_PHRASE) as TRANSLATION,OBJECT_CODE,OBJECT_FIELD_CODE,LAST_ACTIVITY_DATE,LAST_ACTIVITY_TEAM_MEMBER from languages ";
	public static final String TeamMembersMessages_SelectTeamMember_Department_Query="from TEAMMEMBERS where UPPER(Department)=? AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembersMessages_SelectTeamMember_Shift_Query="from TEAMMEMBERS where UPPER(Shift_Code)=? AND UPPER(TENANT_ID)=? ORDER BY UPPER(LAST_NAME) ASC";
	public static final String Companies_Select_Tenant_All_Companies_Query="from COMPANIES where STATUS='A' AND UPPER(TENANT_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String Companies_GetCompanyById_Query="select * from COMPANIES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String Companies_GetCompanyById_NamedQuery="CompanyGetById";
	public static final String Companies_GetCompanyByPartName_Query="select * from COMPANIES where STATUS='A' AND UPPER(TENANT_ID)=? AND ( UPPER(NAME20) like :name2 OR UPPER(NAME50) like :name5)";
	public static final String Companies_GetCompanyByPartName_NamedQuery="CompanyGetByPartName";
	public static final String Companies_Delete_NamedQuery="Companies_Delete_NamedQuery";
	public static final String Companies_Delete_Query="update Companies set STATUS='X' where UPPER(TENANT_ID)= :"+GLOBALCONSTANT.DataBase_Companies_Tenant+" AND UPPER(COMPANY_ID)= :"+GLOBALCONSTANT.DataBase_Companies_Company;
	public static final String Fulfillment_Q_SearchBy_Fulfilment_ID=" from FULFILLMENTCENTERSPOJO where UPPER(Status)=UPPER('A') and upper(FULFILLMENT_CENTER_ID)=? and UPPER(Tenant_id)=?";
	public static final String Fulfillment_Q_SearchBy_FulfillmentName=" from FULFILLMENTCENTERSPOJO where UPPER(Status)=UPPER('A') and (UPPER(Name20) like ? OR UPPER(Name50) like ?) and UPPER(Tenant_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String Fulfillment_Q_GetAllFulfillment=" from FULFILLMENTCENTERSPOJO where UPPER(Status)=UPPER('A') and UPPER(Tenant_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String Fulfillment_Q_Update_FulfillmentBy_ID="update FULFILLMENT_CENTERS Set Status='X' where UPPER(Tenant_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=?";
	public static final String Fulfillment_Q_Is_Fulfillment_AVAILABLE_By_ID=" from FULFILLMENTCENTERSPOJO where UPPER(Tenant_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=?";
	public static final String MenuMessages_ProcedureCallQuery_Company="CALL UPDATEMENUMESSAGECOMPANY(:Tenant_Id,:Company_Id,:Status,:DateTime)";
	//public static final String MenuMessages_company_Query="from MENUMESSAGES where upper(Tenant_ID)=? and upper(Status)=? and upper(Company_ID)=? and START_DATE <= sys_extract_utc(localtimestamp)";
	public static final String TeamMemberMenuMessages_ProcedureCallQuery_Company="CALL UPDATETEAMMEMBERMESSAGESSTATUS(:Tenant_Id,:Status,:Team_Member_Id,:Company_Id,:DateTime)";
	//public static final String TeamMemberMenuMessages_company_Query="from TEAMMEMBERMESSAGES where upper(Tenant_ID)=? and upper(Status)=? and upper(Company_ID)=? and upper(Team_Member_ID)=? and START_DATE <= sys_extract_utc(localtimestamp)";
	public static final String NativeQueryName_MenuOption_delete_From_MenuProfileOption="MENU_OPTION_DELETE_FROM_MENU_PROFILE";
	public static final String NativeQuery_MenuOption_delete_From_MenuProfileOption="delete from MENU_PROFILE_OPTIONS where Upper(MENU_OPTION)=:MenuOption";
	/**
	 *  menu option query
	 *  
	 */
	public static final String Language_Label_Query="from LANGUAGES where Upper(LANGUAGE)=Upper('English') and Upper(OBJECT_FIELD_CODE)=Upper('MenuOptionsScreenLookup')";
	public static final String NativeQueryName_MenuOptions_DisplayAllQueryOne="select * from MENU_OPTIONS order by LAST_ACTIVITY_DATE desc";
	public static final String NativeQueryName_MenuOptions_DisplayAllQueryTwo="select * from MENU_OPTIONS where UPPER(TENANT_ID)= ? or UPPER(TENANT_ID)= ? order by LAST_ACTIVITY_DATE desc";
	public static final String NativeQueryName_MenuOptions_DisplayAllQueryNameOne="DisplayALLDataOne";
	public static final String NativeQueryName_MenuOptions_DisplayAllQueryNameTwo="DisplayALLDataTwo";
	public static final String NativeQueryName_Team_Member_company="TEAM_MEMBER_COMPANYBE";

	public static final String NativeQueryName_Team_member_Companies_query=
			"select TMC.COMPANY_ID as Company_Id,CMPY.NAME50 as Name20,"
					+ "CMPY.PURCHASE_ORDER_REQ_APPROVAL as PurchaseOrderApproval, CMPY.LOGO as CompanyLogo ,CMPY.THEME_MOBILE as TMobile"
					+ ",CMPY.THEME_RF as TRF, CMPY.THEME_FULL_DISPLAY as TFullDisplay "
					+ "from TEAM_MEMBER_COMPANIES TMC "
					+ "join COMPANIES CMPY on Upper(TMC.COMPANY_ID)=Upper(CMPY.COMPANY_ID) where UPPER(TMC.TENANT_ID)=UPPER(CMPY.TENANT_ID) and"
					+ " UPPER(TMC.TEAM_MEMBER_ID)=:teamMember and UPPER(TMC.TENANT_ID)=:tenant and Upper(CMPY.STATUS)='A' order by UPPER(TMC.COMPANY_ID) asc ";

	public static final String MenuAppIcon_GetTeamMember_Name_Query_By_TEAMID="from TEAMMEMBERS where UPPER(TEAM_MEMBER_ID)=? AND ROWNUM=1";
	public static final String TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery="from TEAMMEMBERMESSAGES where UPPER(TEAM_MEMBER_ID)=? AND upper(TENANT_ID)=? AND upper(COMPANY_ID)=? AND UPPER(STATUS)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String MenuAppIcon_GetList_Query="select * from MENU_APP_ICONS ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String LanguageTranslation_Generalcode_Select_Conditional_Query="from GENERALCODES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(GENERAL_CODE_ID)=? ORDER BY UPPER(DESCRIPTION20) ASC";
	public static final String TeamMemberMessages_Generalcode_Select_Conditional_Query="from GENERALCODES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(GENERAL_CODE_ID)=? ORDER BY UPPER(DESCRIPTION20) ASC";
	public static final String LanguageTranslation_CheckLanguage_Query="from LANGUAGES where UPPER(Language)=? order by LAST_ACTIVITY_DATE DESC ";
	public static final String LanguageTranslation_GetLanguageList_Query="select * from languages where upper(language)=(select distinct upper(gc.general_code) from general_codes gc, languages lang where upper(gc.general_code_id)=? and upper(gc.tenant_id)=? and upper(gc.company_id)=? and upper(gc.description20)=?) order by LAST_ACTIVITY_DATE DESC";
	public static final String LanguageTranslation_CheckKeyPhrase_Query = "from LANGUAGES where UPPER(Key_Phrase)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String LanguageTranslation_GetLanguageData_Query = "from LANGUAGES where Upper(Language)=? and UPPER(Key_Phrase)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String Generalcode_Select_Applications_Query="from GENERALCODES where UPPER(GENERAL_CODE_ID)=? AND UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? ORDER BY UPPER(DESCRIPTION20)";
	public static final String MenuAppIcon_GetListByAppIconName_Query="select * from MENU_APP_ICONS where UPPER(NAME20) like :name2 OR UPPER(NAME50) like :name5 ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String Location_GetTeamMember_Name_Query="from TEAMMEMBERS where UPPER(TENANT_ID)=? and UPPER(TEAM_MEMBER_ID)=?";
	public static final String InfoHelps_QuerySearchLookup="select * from INFO_HELPS where UPPER(INFO_HELP)=? order by LAST_ACTIVITY_DATE DESC";													
	public static final String InfoHelps_QuerySearchInfoHelpType="from INFOHELPS where UPPER(InfoHelpType)=? order by LastActivityDate DESC";
	public static final String InfoHelps_QuerySearchPartOfTheDescription="from INFOHELPS where UPPER(Description20) like ? or UPPER(Description50) like ? order by LastActivityDate DESC";
	//public static final String InfoHelps_QuerySelectInfoHelpType="select * from GENERAL_CODES where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODE_ID)=? ORDER BY DESCRIPTION20";
	//public static final String InfoHelps_QueryLanguage="select * from GENERAL_CODES where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODE_ID)=? ORDER BY DESCRIPTION20";
	public static final String InfoHelps_QueryGetAllList="Select * from INFO_HELPS order by LAST_ACTIVITY_DATE DESC";
	public static final String GeneralCodes_QueryGeneralCodeList="select * from GENERAL_CODES gc left join TEAM_MEMBERS tm on UPPER(gc.TENANT_ID) = UPPER(tm.TENANT_ID) where UPPER(tm.SYSTEM_USE)='Y' and UPPER(gc.GENERAL_CODE_ID)='GENERALCODES' and UPPER(gc.TENANT_ID) =? and UPPER(gc.COMPANY_ID)=? and UPPER(tm.TEAM_MEMBER_ID)=? order by gc.LAST_ACTIVITY_DATE DESC";
	//public static final String GeneralCodes_QuerySelectApplicationGeneralCode="select distinct GENERAL_CODE from GENERAL_CODES where UPPER(GENERAL_CODE_ID)='APPLICATIONS' and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY GENERAL_CODE";
	public static final String MenuOption_UpdateQuery = "update MENU_OPTIONS set APPLICATION_ID =?,APPLICATION_SUB =?, MENU_TYPE =?, DESCRIPTION20=?, DESCRIPTION50=?, HELP_LINE=?, EXECUTION=?, LAST_ACTIVITY_DATE=?, LAST_ACTIVITY_TEAM_MEMBER=? where UPPER(MENU_OPTION) = ? ";
	public static final String MenuOption_SelectQuery = "from MENUOPTIONS where MENU_OPTION = ?";
	public static final String MenuProfileOption_deleteQuery = "delete from MENU_PROFILE_OPTIONS where upper(MENU_OPTION) = ? ";
	public static final String NativeQueryName_MenuProfile_ProfileList_QueryName="MenuProfileHeader";
	public static final String NativeQueryName_MenuProfile_ProfileDisplayAll_QueryName="MenuProfileHeaderDisplayALL";
	public static final String NativeQueryName_MenuProfile_ProfileLike_QueryName="MenuProfileHeaderLike";
	public static final String NativeQueryName_MenuProfile_Delete_QueryName="DELETE_FROM_MENU_PROFILE_HEADER";

	public static final String NativeQueryName_MenuProfileHEADER_Delete_Query="delete from MENU_PROFILE_HEADERS where "
			+ "Upper(TENANT_ID)=:tenant And Upper(MENU_PROFILE)=:MenuProfile";

	public static final String NativeQueryName_MenuProfile_Assigment_Delete_QueryName="DELETE_FROM_MENU_PROFILE_ASSIGMENT";

	public static final String NativeQueryName_MenuProfileHEADER_FromAssigment_Delete_Query="delete from MENU_PROFILE_ASSIGNMENTS where "
			+ "Upper(TENANT_ID)=:tenant And Upper(MENU_PROFILE)=:MenuProfile";

	public static final String NativeQueryName_MenuProfileOPTION_Delete_QueryName="DELETE_FROM_MENU_PROFILE_OPTION";

	public static final String NativeQueryName_MenuProfileOPTION_Delete_Query="delete from MENU_PROFILE_OPTIONS where "
			+ "Upper(TENANT_ID)=:tenant And Upper(MENU_PROFILE)=:MenuProfile";

	public static final String NativeQuery_MenuProfile_ProfileList_QueryName="select * from MENU_PROFILE_HEADERS where Upper(MENU_PROFILE) =:MenuProfile and Upper(TENANT_ID)=:tenant";
	public static final String NativeQuery_MenuProfile_ProfileDisplayAll_Query="select * from MENU_PROFILE_HEADERS where Upper(TENANT_ID)=:tenant order by LAST_ACTIVITY_DATE desc";

	public static final String NativeQuery_MenuProfile_ProfileLike_Query="select * from MENU_PROFILE_HEADERS where "
			+ "Upper(TENANT_ID)=:tenant And Like and "
			+ "(DESCRIPTION20 Like :Description20 OR DESCRIPTION50 Like :Description50) And MENU_TYPE Like :MenuType ";

	public static final String NativeQueryName_MenuProfiles_GeneralCode_MenuTypeQuery="select * from GENERAL_CODES where Upper(GENERAL_CODE_ID)=:menuType ORDER BY DESCRIPTION20 asc";
	public static final String NativeQueryName_MenuProfiles_GeneralCode_MenuTypeQueryName="MenuProfiles_MenuTypesGeneralCode";
	public static final String NativeQueryName_CheckingMenuProfileOptionsList="MenuProfileOptionListCheckRest";

	public static final String NativeQueryName_MenuOptionProfile_MEnuProfile_query="Select MENU_PROFILE from MENU_PROFILE_OPTIONS "
			+ "where Upper(MENU_PROFILE)=:MenuProfile and Upper(TENANT_ID)=:tenant";

	public static final String NativeQueryName_CheckingMenu_Profile_Option_ResultSet="CHECKMENUPROFILEINMENUPROFILEOPTION";
	public static final String MenuProfile_Maintenance_AssignMenuProfileOption_Query="select * from MENU_PROFILE_OPTIONS MPO, MENU_OPTIONS MO where Upper(MPO.MENU_PROFILE)=:MenuProfile AND Upper(MPO.TENANT_ID)=:tenant AND Upper(MPO.MENU_OPTION)=Upper(MO.MENU_OPTION) order by LAST_ACTIVITY_DATE asc";
	public static final String MenuProfile_Maintenance_AssignMenuProfileOption_QueryName="ASSIGNMENUPROFILEINMENUPROFILEHEADER";
	public static final String MenuProfile_Maintenance_AssignMenuProfileOption_Display_All_QueryName="ASSIGNMENUPROFILEINMENUPROFILEHEADERDISPLAYALL";

	public static final String MenuProfile_Maintenance_AssignMenuProfileOption_Display_All_Query="from MENUOPTIONS where MenuOption NOT IN(select MO.MenuOption from MENUPROFILEOPTIONS MPO, MENUOPTIONS MO where Upper(MPO.Id.MenuProfile)=:MenuProfile AND Upper(MPO.Id.Tenant)=:MenuProfileTenant AND Upper(MPO.Id.MenuOption)=Upper(MO.MenuOption) ) AND "
			+ "(Upper(Tenant)=:tenant or Upper(Tenant)=:tenant1) and Upper(MenuType)=:MenuType order by Upper(Application) asc";

	public static final String MenuProfile_Maintenance_AssignMenuProfileOption_Display_All_Query_JASCI="from MENUOPTIONS where MenuOption NOT IN(select MO.MenuOption from MENUPROFILEOPTIONS MPO, MENUOPTIONS MO where Upper(MPO.Id.MenuProfile)=:MenuProfile AND Upper(MPO.Id.Tenant)=:MenuProfileTenant AND Upper(MPO.Id.MenuOption)=Upper(MO.MenuOption) ) AND "
			+ "Upper(MenuType)=:MenuType order by Upper(Application) asc";

	public static final String MenuProfile_Maintenance_Label_Query="from LANGUAGES where "
			+"Upper(KEY_PHRASE)in (Upper('actions'),upper('edit'),upper('MenuProfilesMaintenanceLookup')"
			+",upper('menutype'),upper('menuoption'),upper('description'),upper('application'),upper('Subapplication'),"
			+" upper('Actions'), upper('Cancel'), upper('Delete'), upper('Edit'), upper('Add New'), upper('new')"
			+",upper('PartoftheMenuProfileDescription'),upper('ERR_INVALID_MENU_PROFILE'),upper('LastActivityDate'),upper('lastactivityby')"
			+",upper('helpline'),upper('ERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK'),upper('ERR_MENU_TYPE_LEFT_BLANK'),"
			+"upper('AvailableMenuOptions'),upper('MenuProfilesOptions'),Upper('Save/Update')"
			+",upper('displayall'),upper('ERR_MENU_PROFILE_LEFT_BLANK'),upper('ERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION'),"
			+" upper('ERR_MENU_PROFILE_ALREADY_USED'),upper('OR'), upper('MenuProfilesMaintenanceSearchLookup'),upper('ERR_MENU_TYPE_LEFT_BLANK'),"
			+"upper('MenuProfilesMaintenance'),upper('SelectApplication'),upper('ERR_MANDATORY FIELD_LEFT BLANK'),"
			+" upper('ON_SAVE_SUCCESSFULLY'),upper('ON_UPDATE_SUCCESSFULLY'),upper('descriptionshrot'),"
			+"upper('descriptionlong'), upper('Applications') ,upper('MenuTypes'),upper('SubApplication'),upper('Select')"
			+",upper('Menu_App_Icon'),upper('MenuProfile'),Upper('ERR_MANDATORY_FIELD_LEFT_BLANK'),"
			+ "upper('ERR_INVALID_MENU_TYPE_LEFT_BLANK'),Upper('ERR_WHILE_DELETING_MENU_PROFILE') ) and Upper(LANGUAGE)=:language";

	/** 
	 * Notes Screen Constant
	 * 
	 */
	public static final String GetNotesListQuery="from NOTES where UPPER(Tenant_ID)=? and UPPER(Company_ID)=? and UPPER(Note_Id)=? and UPPER(Note_Link)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String InsertNotesQuery="from INSERTNOTES where UPPER(Notes_Id)=? and UPPER(Notes_Link)=?";

	public static final String UpdateNotesEntery="from NOTES where UPPER(Note_Link)=? and UPPER(Note_Id)=? "
			+ "									 and UPPER(TENANT_ID)=?and UPPER(COMPANY_ID)=? and ID=?";

	public static final String Notes_Delete_NamedQuery="Notes_Delete_NamedQuery";

	public static final String Notes_Delete_Query="delete from NOTES where UPPER(TENANT_ID)= :"+GLOBALCONSTANT.DataBase_Notes_Tenant_Id+
			" AND UPPER(COMPANY_ID)= :"+GLOBALCONSTANT.DataBase_Notes_Company_Id+
			" AND UPPER(NOTE_ID)= :"+GLOBALCONSTANT.DataBase_Notes_Note_Id+
			" AND UPPER(NOTE_LINK)= :"+GLOBALCONSTANT.DataBase_Notes_Note_Link+
			" AND ID= :"+GLOBALCONSTANT.DataBase_Notes_ID;

	public static final String Query_GetMaxBillingControlNumber="Select max(BILLING_CONTROL_NUMBER) as BillingNumber from COMPANIES";
	public static final String Companies_Select_Email_Query="from COMPANIES where UPPER(Contact_Email1)=?";

	public static final String NativeQueryName_MenuOptionProfile_MEnuProfileAssignment_query="Select MENU_PROFILE from MENU_PROFILE_ASSIGNMENTS "
			+ "where Upper(MENU_PROFILE)=:MenuProfile and Upper(TENANT_ID)=:tenant";

	/**
	 * Queries for location profile.
	 * 
	 */
	public static final String LocationProfile_Generalcode_Select_Conditional_Query="from GENERALCODES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(GENERAL_CODE_ID)=? ORDER BY DESCRIPTION20";
	public static final String LocationProfile_FulFillmentCenter_Query="select FULFILLMENT_CENTER_ID,NAME20 from FULFILLMENT_CENTERS where UPPER(TENANT_ID)=? and UPPER(STATUS)='A' ORDER BY UPPER(NAME20)" ;
	public static final String LocationProfile_FulFillmentCenter_QueryName="LocationProfile_FulFillmentCenter_QueryName";
	public static final String LocationProfile_LocationProfileList_Select_Query="from LOCATIONPROFILES where UPPER(PROFILE)=? AND UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(FULFILLMENT_CENTER_ID)=? order by LAST_ACTIVITY_DATE DESC ";
	public static final String LocationProfile_LocationProfile_Select_Conditional_Query="from LOCATIONPROFILES where UPPER(PROFILE)=?AND UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? order by LAST_ACTIVITY_DATE DESC ";
	public static final String LocationProfile_ProfileGroup_Select_Conditional_Query="from LOCATIONPROFILES where UPPER(PROFILE_GROUP)=? AND UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? order by LAST_ACTIVITY_DATE DESC ";
	public static final String LocationProfile_Description_Select_Conditional_Query="from LOCATIONPROFILES where (UPPER(DESCRIPTION20) like ? OR UPPER(DESCRIPTION50) like ?) AND UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? order by LAST_ACTIVITY_DATE DESC ";
	public static final String LocationProfile_ShowAll_Query="from LOCATIONPROFILES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String LocationProfile_DistinctLocationProfile_Query="select distinct(upper(PROFILE)) from LOCATION_PROFILES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=?";
	public static final String LocationProfile_Notes_Query="from NOTES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(NOTE_LINK)=? AND UPPER(NOTE_ID)=? order by LAST_ACTIVITY_DATE Desc";
	public static final String LocationProfile_Count_NumberOfLocations_Query="select count(*) as NumberOfLocations from locations where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(FULFILLMENT_CENTER_ID)=? and upper(LOCATION_PROFILE)=? ";
	public static final String LocationProfile_Count_NumberOfLocations_QueryName="LocationProfile_Count_NumberOfLocations_QueryName";
	public static final String LocationProfile_Count_NumberOfLocations_ResultMapping="LocationProfile_Count_NumberOfLocations_ResultMapping";
	public static final String LocationProfile_RassignLocationProfile_QueryName="LocationProfile_RassignLocationProfile_QueryName";
	public static final String	Location_QueryNameSelectGeneralCode="SelectGeneralCode";
	public static final String	Location_QuerySelectGeneralCode="select * from GENERAL_CODES where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODE_ID)=? ORDER BY UPPER(DESCRIPTION20)";
	public static final String	Location_QueryNameSelectLocationProfile="SelectLocationProfile";
	public static final String	Location_QueryNameUniqueLocationCheck="UniqueLocationCheck";
	public static final String	Location_QueryUniqueLocationCheck="Select * from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(AREA)=? and UPPER(LOCATION)=?";
	public static final String	Location_QueryNameSearch="LocationMaintenanceSearch";
	public static final String	Location_QuerySearch="Select * from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(Location)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String	Location_QuerySearchArea="from LOCATION where UPPER(Tenant_Id)=? and UPPER(Company_Id)=? and UPPER(Area)=? ORDER BY Last_Activity_Date DESC";
	public static final String	Location_QuerySearchLocationType="from LOCATION where UPPER(Tenant_Id)=? and UPPER(Company_Id)=? and UPPER(Location_Type)=? ORDER BY Last_Activity_Date DESC";
	public static final String	Location_QuerySearchPartOfTheDescription="from LOCATION where UPPER(Tenant_Id)=? and UPPER(Company_Id)=? and ( UPPER(Description20) like ? or UPPER(Description50) like ?) ORDER BY Last_Activity_Date DESC";
	public static final String	Location_QueryNameDisplayAll="LocationMaintenanceDisplayAll";
	public static final String	Location_QueryDisplayAll="Select * from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String	Location_QueryNameGeneralCode="LocationMaintenanceGeneralCode";
	public static final String	Location_QueryGeneralCode="Select * from GENERAL_CODES where GENERAL_CODE_ID in ('TASK','AREAS','LOCTYPE') and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String	Location_QueryNameFetchLocation="LocationMaintenanceFetchLocation";
	public static final String	Location_QueryFetchLocation="Select * from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(AREA)=? and UPPER(LOCATION)=?";
	public static final String Location_QueryNameFulfillmentCenter="LocationFullfillmentCenterList";
	public static final String Location_QueryFulfillmentCenter="Select * from FULFILLMENT_CENTERS where UPPER(Tenant_Id)=?";
	public static final String	Location_QueryNameUniqueAlternateLocationCheck="UniqueAlternateLocationCheck";
	public static final String	Location_QueryUniqueAlternateLocationCheck="Select * from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(ALTERNATE_LOCATION)=?";
	public static final String Location_QueryLocationExistForDelete="select * from INVENTORY_LEVELS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(AREA)=? and UPPER(LOCATION)=?";
	public static final String Location_Notes_Query="from NOTES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(NOTE_LINK)=? AND UPPER(NOTE_ID)=? order by LAST_ACTIVITY_DATE Desc";
	/**
	 * query for menu maintenance assignment
	 * 
	 */
	public static final String MenuProfileAssignment_DisplayAll_TeamMember_Query="select DISTINCT tm.TENANT_ID,tm.TEAM_MEMBER_ID,tm.LAST_NAME,tm.FIRST_NAME,tm.MIDDLE_NAME,tm.CURRENT_STATUS,nvl2(MPA.MENU_PROFILE,'A', 'X') as Status "
			+"from team_members tm left join MENU_PROFILE_ASSIGNMENTS MPA on tm.TEAM_MEMBER_ID=MPA.TEAM_MEMBER_ID and tm.tenant_id=MPA.TENANT_ID where upper(tm.tenant_id) =:tenant"
			+" order by Upper(tm.LAST_NAME) asc";

	public static final String MenuProfileAssignment_TeamMemberID_Query="select DISTINCT tm.TENANT_ID,tm.TEAM_MEMBER_ID,tm.LAST_NAME,tm.FIRST_NAME,tm.MIDDLE_NAME,tm.CURRENT_STATUS,nvl2(MPA.MENU_PROFILE,'A', 'X') as Status from team_members tm "
			+" left JOIN MENU_PROFILE_ASSIGNMENTS MPA on tm.TEAM_MEMBER_ID=MPA.TEAM_MEMBER_ID and tm.tenant_id=MPA.TENANT_ID where "
			+" Upper(tm.TEAM_MEMBER_ID)=? and upper(tm.tenant_id) = ? ";

	public static final String MenuProfileAssignment_SearchByNameLike_Query="select DISTINCT tm.TENANT_ID,tm.TEAM_MEMBER_ID,tm.LAST_NAME,tm.FIRST_NAME,tm.MIDDLE_NAME,tm.CURRENT_STATUS,nvl2(MPA.MENU_PROFILE, 'A', 'X')" 
			+" as MENU_PROFILE from team_members tm left join MENU_PROFILE_ASSIGNMENTS MPA on " 
			+"tm.TEAM_MEMBER_ID=MPA.TEAM_MEMBER_ID and tm.tenant_id=MPA.TENANT_ID where upper(tm.tenant_id) = ?"
			+" and (upper(tm.FIRST_NAME) like ? or upper(tm.MIDDLE_NAME) like ? or upper(tm.LAST_NAME) like ? ) order by Upper(tm.LAST_NAME) asc";

	public static final String MenuProfileAssignment_TeamMemberProfile_Query="select MPH.* from MENU_PROFILE_HEADERS MPH, MENU_PROFILE_ASSIGNMENTS MPA, TEAM_MEMBERS TM where Upper(TM.TENANT_ID)=? "
			+"and Upper(TM.TEAM_MEMBER_ID)=? and TM.TEAM_MEMBER_ID=MPA.TEAM_MEMBER_ID And MPH.MENU_PROFILE=MPA.MENU_PROFILE and " 
			+" TM.TENANT_ID=MPA.TENANT_ID AND MPH.TENANT_ID=MPA.TENANT_ID";

	public static final String MenuProfileAssignment_TeamMemberProfile_DispalyAll_Query="select MPH.TENANT_ID,MPH.MENU_PROFILE,MPH.MENU_TYPE,MPH.DESCRIPTION20,MPH.DESCRIPTION50,MPH.HELP_LINE from MENU_PROFILE_HEADERS MPH where "
			+" UPPER(MPH.TENANT_ID) = :tenant And MPH.MENU_PROFILE not in(select MPH.MENU_PROFILE from MENU_PROFILE_HEADERS MPH, MENU_PROFILE_ASSIGNMENTS MPA, TEAM_MEMBERS TM where Upper(TM.TENANT_ID)=:tenant1 "
			+"and Upper(TM.TEAM_MEMBER_ID)=:TeamMemberID and TM.TEAM_MEMBER_ID=MPA.TEAM_MEMBER_ID And MPH.MENU_PROFILE=MPA.MENU_PROFILE and " 
			+" TM.TENANT_ID=MPA.TENANT_ID AND MPH.TENANT_ID=MPA.TENANT_ID)";

	public static final String MenuProfileAssignment_MenuProfile_MenuType_Query="select MPH.TENANT_ID,MPH.MENU_PROFILE,MPH.MENU_TYPE,MPH.DESCRIPTION20,MPH.DESCRIPTION50,MPH.HELP_LINE from MENU_PROFILE_HEADERS MPH where "
			+" UPPER(MPH.TENANT_ID) = ? and Upper(MPH.MENU_TYPE)= ?";

	public static final String MenuProfileAssignment_AllMenuProfile_MenuType_Query="select MPH.TENANT_ID,MPH.MENU_PROFILE,MPH.MENU_TYPE,MPH.DESCRIPTION20,MPH.DESCRIPTION50,MPH.HELP_LINE from MENU_PROFILE_HEADERS MPH where "
			+" UPPER(MPH.TENANT_ID) = ?";

	public static final String MenuProfileAssignment_Delete_MenuProfile_Query="delete from MENU_PROFILE_ASSIGNMENTS where Upper(Tenant_ID) = ? And Upper(Team_Member_ID)= ?";

	public static final String MenuProfileAssignment_TeamMemberName_Query=" SELECT TM.first_Name,TM.Middle_Name,TM.last_Name From TEAM_MEMBERS TM where Upper(TM.TENANT_ID)= ? " 
			+ "and Upper(TM.TEAM_MEMBER_ID)= ?";

	public static final String MenuProfile_Maintenance_AssignMenuProfileOption_Display_Application_Query="from MENUOPTIONS where MenuOption NOT IN(select MO.MenuOption from MENUPROFILEOPTIONS MPO, MENUOPTIONS MO where Upper(MPO.Id.MenuProfile)=:MenuProfile AND Upper(MPO.Id.Tenant)=:MenuProfileTenant AND Upper(MPO.Id.MenuOption)=Upper(MO.MenuOption) ) AND "
			+ "(Upper(Tenant)=:tenant or Upper(Tenant)=:tenant1 ) and Upper(MenuType)=:MenuType and upper(Application)=:Application order by LastActivityDate desc";

	public static final String MenuProfile_Maintenance_AssignMenuProfileOption_Display_Application_Query_JASCI="from MENUOPTIONS where MenuOption NOT IN(select MO.MenuOption from MENUPROFILEOPTIONS MPO, MENUOPTIONS MO where Upper(MPO.Id.MenuProfile)=:MenuProfile AND Upper(MPO.Id.Tenant)=:MenuProfileTenant AND Upper(MPO.Id.MenuOption)=Upper(MO.MenuOption) ) AND "
			+ " Upper(MenuType)=:MenuType and upper(Application)=:Application order by LastActivityDate desc";

	/** 
	 * menu assignment
	 * 
	 */
	public static final String MenuProfileAssignment_TeamMemberProfile_MenuType_Query="select MPH.TENANT_ID,MPH.MENU_PROFILE,MPH.MENU_TYPE,MPH.DESCRIPTION20,MPH.DESCRIPTION50,MPH.HELP_LINE from MENU_PROFILE_HEADERS MPH where "
			+" UPPER(MPH.TENANT_ID) = :tenant And Upper(MPH.Menu_Type)=:MenuType And MPH.MENU_PROFILE not in(select MPH.MENU_PROFILE from MENU_PROFILE_HEADERS MPH, MENU_PROFILE_ASSIGNMENTS MPA, TEAM_MEMBERS TM where Upper(TM.TENANT_ID)=:tenant1 "
			+"and Upper(TM.TEAM_MEMBER_ID)=:TeamMemberID and TM.TEAM_MEMBER_ID=MPA.TEAM_MEMBER_ID And MPH.MENU_PROFILE=MPA.MENU_PROFILE and " 
			+" TM.TENANT_ID=MPA.TENANT_ID AND MPH.TENANT_ID=MPA.TENANT_ID)";

	public static final String Generalcode_Description20Unique_Query="from GENERALCODES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(General_Code_ID) = ? AND UPPER(Description20) = ?";
	public static final String Generalcode_Description50Unique_Query="from GENERALCODES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(General_Code_ID) = ? AND UPPER(Description50) = ?";
	public static final String GeneralCodes_QueryCheckUniqueGeneralCodeCreateQuery="Select GENERAL_CODE from GENERAL_CODES where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(APPLICATION_ID)=? and UPPER(GENERAL_CODE_ID)=? and (UPPER(GENERAL_CODE)=?)";
	public static final String Generalcode_Description20Unique_Query_Edit="from GENERALCODES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(General_Code_ID) = ? AND UPPER(General_Code) <> ? AND UPPER(Description20) = ?";
	public static final String Generalcode_Description50Unique_Query_Edit="from GENERALCODES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(General_Code_ID) = ? AND UPPER(General_Code) <> ? AND UPPER(Description50) = ?";
	public static final String LocationProfile_Description20Unique_Query="from LOCATIONPROFILES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(Fulfillment_Center_ID) = ? AND UPPER(Description20) = ?";
	public static final String LocationProfile_Description50Unique_Query="from LOCATIONPROFILES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(Fulfillment_Center_ID) = ? AND UPPER(Description50) = ?";
	public static final String LocationProfile_Description20Unique_Query_Edit="from LOCATIONPROFILES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(Fulfillment_Center_ID) = ? AND UPPER(Description20) = ? AND UPPER(Profile) <> ?";
	public static final String LocationProfile_Description50Unique_Query_Edit="from LOCATIONPROFILES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(Fulfillment_Center_ID) = ? AND UPPER(Description50) = ? AND UPPER(Profile) <> ?";
	/**
	 * wizard location for Queries
	 * 
	 */
	public static final String Location_Wizard_QueryNameFetchDataForUpdate="FetchDataForUpdate";
	public static final String Location_Wizard_QueryNameFetchDataForAreaSearch="FetchDataForAreaSearch";
	public static final String Location_Wizard_QueryNameFetchDataForLocationTypeSearch="FetchDataForLocationTypeSearch";
	public static final String Location_Wizard_QueryNameFetchDataForAnyPartOfDiscription="FetchDataForAnyPartofDiscriptionSearch";
	public static final String Location_Wizard_QueryNameFetchDataForGrid="FetchDataForGrid";
	public static final String Location_Wizard_QueryNameFetchDataForCopy="FetchDataForCopy";
	public static final String Location_Wizard_QueryNameFetchListLocations="FetchListLocations";
	public static final String Location_Wizard_QueryNameFetchListInventoryLevel="FetchListInventoryLevel";
	public static final String Location_Wizard_QueryNameFetchListALLInventoryLevel="FetchListAllInventoryLevel";
	public static final String Location_Wizard_QueryNameUpdateLocations="UpdateLocations";
	public static final String Location_Wizard_QueryNameFetchMaxWizardControlNumber="FetchWizardControlNumber";
	public static final String Location_Wizard_QueryNameFetchFulfillment="FetchFulfillment";
	public static final String Location_Wizard_QueryNameFetchLocationProfile="FetchLocationProfile";
	public static final String Location_Wizard_QueryNameFetchDataForSelect="FetchDataForSelect";
	public static final String Location_Wizard_QueryUpdateLocations="Update LOCATIONS set WIZARD_ID='' , WIZARD_CONTROL_NUMBER='' where UPPer(TENANT_ID)=? and upper(FULFILLMENT_CENTER_ID)=? and UPPER(COMPANY_ID)=? and UPPER(WIZARD_ID)=? and UPPER(WIZARD_CONTROL_NUMBER)=?";
	public static final String Location_Wizard_QueryFetchDataForUpdate="select * from WIZARD_LOCATIONS where UPPER(WIZARD_CONTROL_NUMBER) like :wizardControl and UPPER(TENANT_ID) =:TenantID and UPPER(COMPANY_ID) =:Company and UPPER(WIZARD_ID) =:wizardID";
	public static final String Location_Wizard_QueryFetchDataForCopy="select * from WIZARD_LOCATIONS where UPPER(WIZARD_ID)=? and UPPER(WIZARD_CONTROL_NUMBER)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?";
	public static final String Location_Wizard_QueryFetchListLocations="select * from LOCATIONS where UPPER(WIZARD_ID)=? and UPPER(WIZARD_CONTROL_NUMBER)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?";
	public static final String Location_Wizard_QueryNumberOfLocations="select COUNT(*) from LOCATIONS where UPPER(WIZARD_ID)=? and UPPER(WIZARD_CONTROL_NUMBER)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?";
	public static final String Location_Wizard_QueryFetchListAllInventoryLevel="select * from INVENTORY_LEVELS where UPPER(Tenant_Id)=? and UPPER(Company_Id)=? and UPPER(Fulfillment_Center_ID)=? and UPPER(Area)=? and UPPER(Location)=?";
	public static final String Location_Wizard_QueryFetchMaxWizardControlNumber="select Max(WIZARD_CONTROL_NUMBER) from WIZARD_LOCATIONS";
	public static final String Location_Wizard_QueryFetchDataForGrid="select * from WIZARD_LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String Location_Wizard_Select_Applications_Query="select * from GENERAL_CODES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(GENERAL_CODE_ID)=? ORDER BY UPPER(DESCRIPTION20)";
	public static final String Location_Wizard_Fulfillment_Query="from FULFILLMENTCENTERSPOJO where UPPER(TENANT_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=?";
	public static final String Location_Wizard_Select_Location_Profile_Query="from LOCATIONPROFILES where UPPER(Tenant_ID)=? and UPPER(Company_Id)=? and UPPER(FULFILLMENT_CENTER_ID)=? ORDER BY UPPER(DESCRIPTION20)";
	public static final String Location_Wizard_QueryNameFetchWizardList="FetchWizardList";
	public static final String Location_Wizard_QueryFetchWizardList="select * from WIZARD_LOCATIONS where UPPER(TENANT_ID)= ? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(COMPANY_ID)=?";
	public static final String Location_QueryNameUniqueWizardControlNumberCheck="UniqueWizardControlNumberCheck";
	public static final String Location_QueryWizardControlNumberCheck="Select * from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=? and WIZARD_CONTROL_NUMBER=?";
	public static final String Location_QueryNameLocationLabelTypePrint="QueryLocationLabelTypePrint";
	public static final String Location_QueryLocationLabelTypePrint="Select * from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(AREA)=?";
	public static final String Query_Authenticate_User_Active_Company="select TMC.COMPANY_ID from TEAM_MEMBER_COMPANIES TMC,companies CMP where UPPER(TMC.TENANT_ID)=:tenant and UPPER(TMC.TEAM_MEMBER_ID)=:teamMember and UPPER(TMC.TENANT_ID)=UPPER(CMP.TENANT_ID) and UPPER(TMC.COMPANY_ID)=UPPER(CMP.COMPANY_ID) and CMP.STATUS='A'";

	public static final String SecurityAuthorizations_SelectConditionalQuery="select SA.TENANT_ID as TENANT,SA.TEAM_MEMBER_ID as TEAMMEMBER,SA.DEFAULT_COMPANY_ID as COMPANY, SA.PASSWORD_DATE "
			+ "as PASSWORDDATE, SA.STATUS as STATUS,C.NAME20 as CNAME20,C.NAME50 as CNAME50,C.PURCHASE_ORDER_REQ_APPROVAL as PURCHASEORDERSREQUIREAPPROVAL,"
			+ "TM.FULFILLMENT_CENTER_ID as FULFILLMENTCENTER,FC.NAME20 as FCNAME20,FC.NAME50 as FCNAME50,T.LANGUAGE as TLANGUAGE,TM.LANGUAGE as TMLANGUAGE,T.THEME_RF as THEMERF,"
			+ "T.THEME_FULL_DISPLAY as THEMEFULLDISPLAY, T.PASSWORD_EXPIRATION_DAYS as PASSWORDEXPIRATIONDAYS, TM.FIRST_NAME ||' '|| TM.LAST_NAME as TEAMMEMBERNAME,TM.AUTHORITY_PROFILE as AUTHORITYPROFILE,"
			+ "TM.MENU_PROFILE_TABLET as MENU_PROFILE_TABLET,TM.MENU_PROFILE_STATION as MENU_PROFILE_STATION,TM.MENU_PROFILE_RF as MENU_PROFILE_RF,TM.MENU_PROFILE_GLASS as MENU_PROFILE_GLASS,"
			+ "TM.EQUIPMENT_CERTIFICATION as EQUIPMENTCERTIFICATION ,SA.PASSWORD as PASSWORD,TM.SYSTEM_USE as SYSTEMUSE, SA.INVALID_ATTEMPT_DATE as INVALIDATTEMPTDATE, SA.NUMBER_ATTEMPTS"
			+ " as NUMBERATTEMPTS, T.NAME20 as TNAME20, T.NAME50 as TNAME50,C.STATUS as COMPANYSTATUS from SECURITY_AUTHORIZATIONS SA join TENANTS T on "
			+ "SA.TENANT_ID=T.TENANT_ID join COMPANIES C on C.TENANT_ID=SA.TENANT_ID and C.COMPANY_ID=SA.DEFAULT_COMPANY_ID "
			+ "join TEAM_MEMBERS TM on TM.TENANT_ID=SA.TENANT_ID and TM.TEAM_MEMBER_ID=SA.TEAM_MEMBER_ID "
			+ "join FULFILLMENT_CENTERS FC on FC.TENANT_ID=SA.TENANT_ID where upper(SA.USER_ID)=:strUsername";

	public static final String	Location_QueryDisplayAll_First="Select AREA,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "LOCATIONS.AREA and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('AREAS') "
			+ "and ROWNUM <= 1) as AREA_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID="
			+ "LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION, LOCATION, DESCRIPTION50, "
			+ "(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)="
			+ "? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE, "
			+ "trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String	Location_QueryDisplayAll_Second="') as LASTACTIVITYDATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS"
			+ " where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as "
			+ "LAST_ACTIVITY_TEAM_MEMBER,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LAST_ACTIVITY_TASK"
			+ " and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK')and ROWNUM <= 1) as"
			+ " LAST_ACTIVITY_TASK, (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCATION') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATIONS.LOCATION) AND ROWNUM = 1) "
			+ "as Note from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String	Location_QueryDisplayAll_Location_First="Select AREA,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "LOCATIONS.AREA and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('AREAS') "
			+ "and ROWNUM <= 1) as AREA_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID="
			+ "LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION, LOCATION, DESCRIPTION50, "
			+ "(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)="
			+ "? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE, "
			+ "trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '"; 

	public static final String	Location_QueryDisplayAll_Location_Second="') as LASTACTIVITYDATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS"
			+ " where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as "
			+ "LAST_ACTIVITY_TEAM_MEMBER,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LAST_ACTIVITY_TASK"
			+ " and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK')and ROWNUM <= 1) as"
			+ " LAST_ACTIVITY_TASK, (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCATION') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATIONS.LOCATION) AND ROWNUM = 1) "
			+ "as Note from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String	Location_QueryDisplayAll_Area_First="Select AREA,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "LOCATIONS.AREA and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('AREAS') "
			+ "and ROWNUM <= 1) as AREA_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID="
			+ "LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION, LOCATION, DESCRIPTION50, "
			+ "(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)="
			+ "? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE, "
			+ "trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String	Location_QueryDisplayAll_Area_Second="') as LASTACTIVITYDATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS"
			+ " where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as "
			+ "LAST_ACTIVITY_TEAM_MEMBER,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LAST_ACTIVITY_TASK"
			+ " and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK')and ROWNUM <= 1) as"
			+ " LAST_ACTIVITY_TASK, (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCATION') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATIONS.LOCATION) AND ROWNUM = 1) "
			+ "as Note from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(Area)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String	Location_QueryDisplayAll_Description_First="Select AREA,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "LOCATIONS.AREA and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('AREAS') "
			+ "and ROWNUM <= 1) as AREA_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID="
			+ "LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION, LOCATION, DESCRIPTION50, "
			+ "(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)="
			+ "? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE, "
			+ "trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String	Location_QueryDisplayAll_Description_Second="') as LASTACTIVITYDATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS"
			+ " where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as "
			+ "LAST_ACTIVITY_TEAM_MEMBER,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LAST_ACTIVITY_TASK"
			+ " and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK')and ROWNUM <= 1) as"
			+ " LAST_ACTIVITY_TASK, (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCATION') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATIONS.LOCATION) AND ROWNUM = 1) "
			+ "as Note from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and (UPPER(DESCRIPTION20) like ? or UPPER(DESCRIPTION50) like ?) ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String	Location_QueryDisplayAll_LocationType_First="Select AREA,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "LOCATIONS.AREA and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('AREAS') "
			+ "and ROWNUM <= 1) as AREA_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID="
			+ "LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION, LOCATION, DESCRIPTION50, "
			+ "(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)="
			+ "? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE, "
			+ "trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String	Location_QueryDisplayAll_LocationType_Second="') as LASTACTIVITYDATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS"
			+ " where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as "
			+ "LAST_ACTIVITY_TEAM_MEMBER,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATIONS.LAST_ACTIVITY_TASK"
			+ " and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK')and ROWNUM <= 1) as"
			+ " LAST_ACTIVITY_TASK, (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCATION') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATIONS.LOCATION) AND ROWNUM = 1) "
			+ "as Note from LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION_TYPE)=? ORDER BY LAST_ACTIVITY_DATE DESC"; 

	public static final String Location_Wizard_QueryFetchDataForGrid_First="Select WIZARD_ID, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from "
			+ "FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID= WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? "
			+ "and ROWNUM <= 1) as FCID_DESCRIPTION,WIZARD_CONTROL_NUMBER,DESCRIPTION50,(select COUNT(*) from LOCATIONS where UPPER(WIZARD_ID)="
			+ "UPPER(WIZARD_LOCATIONS.WIZARD_ID) and UPPER(WIZARD_CONTROL_NUMBER)= UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) and "
			+ "UPPER(FULFILLMENT_CENTER_ID)=UPPER(WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ) as "
			+ "NUMBER_OF_LOCATION, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE= "
			+ "WIZARD_LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)="
			+ "UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE,(select LOCATION_PROFILES.DESCRIPTION20 from LOCATION_PROFILES where "
			+ "UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION_PROFILES.PROFILE)=UPPER(WIZARD_LOCATIONS.LOCATION_PROFILE) "
			+ "and ROWNUM <= 1) as LOCATION_PROFILE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || "
			+ "TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=WIZARD_LOCATIONS.CREATED_BY_TEAM_MEMBER and "
			+ "UPPER(TENANT_ID)=? and ROWNUM <= 1) as CREATED_BY_TEAM_MEMBER,trunc(FROM_TZ(CAST(DATE_CREATED AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForGrid_Second="') as DATECREATED,trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForGrid_Third="') as LAST_ACTIVITY_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' "
			+ "|| TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID="
			+ "WIZARD_LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_ACTIVITY_TEAM_MEMBER,STATUS,LAST_ACTIVITY_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('WIZARDLOC') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) AND ROWNUM = 1) as Notes from "
			+ "WIZARD_LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY DATE_CREATED DESC";

	public static final String Location_QuerySelectLocationProfile="from LOCATIONPROFILES where UPPER(Tenant_ID)=? and UPPER(Company_Id)=? and upper(Fulfillment_Center_Id)=?ORDER BY Upper(Description20)";
	public static final String LocationProfile_Reassign_Query="from LOCATIONPROFILES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? and UPPER(FULFILLMENT_CENTER_ID)=? and UPPER(PROFILE) <> ? order by Upper(PROFILE)";

	public static final String Location_Wizard_QueryFetchDataForLocationTypeSearch_First="Select WIZARD_ID, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from "
			+ "FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID= WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? "
			+ "and ROWNUM <= 1) as FCID_DESCRIPTION,WIZARD_CONTROL_NUMBER,DESCRIPTION50,(select COUNT(*) from LOCATIONS where UPPER(WIZARD_ID)="
			+ "UPPER(WIZARD_LOCATIONS.WIZARD_ID) and UPPER(WIZARD_CONTROL_NUMBER)= UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) and "
			+ "UPPER(FULFILLMENT_CENTER_ID)=UPPER(WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ) as "
			+ "NUMBER_OF_LOCATION, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE= "
			+ "WIZARD_LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)="
			+ "UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE,(select LOCATION_PROFILES.DESCRIPTION20 from LOCATION_PROFILES where "
			+ "UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION_PROFILES.PROFILE)=UPPER(WIZARD_LOCATIONS.LOCATION_PROFILE) "
			+ "and ROWNUM <= 1) as LOCATION_PROFILE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || "
			+ "TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=WIZARD_LOCATIONS.CREATED_BY_TEAM_MEMBER and "
			+ "UPPER(TENANT_ID)=? and ROWNUM <= 1) as CREATED_BY_TEAM_MEMBER,trunc(FROM_TZ(CAST(DATE_CREATED AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForLocationTypeSearch_Second="') as DATECREATED,trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForLocationTypeSearch_Third="') as LAST_ACTIVITY_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' "
			+ "|| TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID="
			+ "WIZARD_LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_ACTIVITY_TEAM_MEMBER,STATUS,LAST_ACTIVITY_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('WIZARDLOC') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) AND ROWNUM = 1) as Notes from "
			+ "WIZARD_LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION_TYPE)=? ORDER BY DATE_CREATED DESC";

	public static final String Location_Wizard_QueryFetchDataForWizardDiscription_First="Select WIZARD_ID, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from "
			+ "FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID= WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? "
			+ "and ROWNUM <= 1) as FCID_DESCRIPTION,WIZARD_CONTROL_NUMBER,DESCRIPTION50,(select COUNT(*) from LOCATIONS where UPPER(WIZARD_ID)="
			+ "UPPER(WIZARD_LOCATIONS.WIZARD_ID) and UPPER(WIZARD_CONTROL_NUMBER)= UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) and "
			+ "UPPER(FULFILLMENT_CENTER_ID)=UPPER(WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ) as "
			+ "NUMBER_OF_LOCATION, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE= "
			+ "WIZARD_LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)="
			+ "UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE,(select LOCATION_PROFILES.DESCRIPTION20 from LOCATION_PROFILES where "
			+ "UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION_PROFILES.PROFILE)=UPPER(WIZARD_LOCATIONS.LOCATION_PROFILE) "
			+ "and ROWNUM <= 1) as LOCATION_PROFILE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || "
			+ "TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=WIZARD_LOCATIONS.CREATED_BY_TEAM_MEMBER and "
			+ "UPPER(TENANT_ID)=? and ROWNUM <= 1) as CREATED_BY_TEAM_MEMBER,trunc(FROM_TZ(CAST(DATE_CREATED AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForWizardDiscription_Second="') as DATECREATED,trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForWizardDiscription_Third="') as LAST_ACTIVITY_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' "
			+ "|| TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID="
			+ "WIZARD_LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_ACTIVITY_TEAM_MEMBER,STATUS,LAST_ACTIVITY_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('WIZARDLOC') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) AND ROWNUM = 1) as Notes from "
			+ "WIZARD_LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and (UPPER(DESCRIPTION20) like:AnyPartOfWizardDiscription or UPPER(DESCRIPTION50) like:AnyPartOfWizardDiscription) ORDER BY DATE_CREATED DESC";

	public static final String Location_Wizard_QueryFetchDataForAreaSearch_First="Select WIZARD_ID, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from "
			+ "FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID= WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? "
			+ "and ROWNUM <= 1) as FCID_DESCRIPTION,WIZARD_CONTROL_NUMBER,DESCRIPTION50,(select COUNT(*) from LOCATIONS where UPPER(WIZARD_ID)="
			+ "UPPER(WIZARD_LOCATIONS.WIZARD_ID) and UPPER(WIZARD_CONTROL_NUMBER)= UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) and "
			+ "UPPER(FULFILLMENT_CENTER_ID)=UPPER(WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ) as "
			+ "NUMBER_OF_LOCATION, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE= "
			+ "WIZARD_LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)="
			+ "UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE,(select LOCATION_PROFILES.DESCRIPTION20 from LOCATION_PROFILES where "
			+ "UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION_PROFILES.PROFILE)=UPPER(WIZARD_LOCATIONS.LOCATION_PROFILE) "
			+ "and ROWNUM <= 1) as LOCATION_PROFILE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || "
			+ "TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=WIZARD_LOCATIONS.CREATED_BY_TEAM_MEMBER and "
			+ "UPPER(TENANT_ID)=? and ROWNUM <= 1) as CREATED_BY_TEAM_MEMBER,trunc(FROM_TZ(CAST(DATE_CREATED AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForAreaSearch_Second="') as DATECREATED,trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_QueryFetchDataForAreaSearch_Third="') as LAST_ACTIVITY_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' "
			+ "|| TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID="
			+ "WIZARD_LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_ACTIVITY_TEAM_MEMBER,STATUS,LAST_ACTIVITY_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('WIZARDLOC') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) AND ROWNUM = 1) as Notes from "
			+ "WIZARD_LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(AREA)=? ORDER BY DATE_CREATED DESC";	

	public static final String Location_Wizard_Query_WizardControlNo_Search_First="Select WIZARD_ID, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from "
			+ "FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID= WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? "
			+ "and ROWNUM <= 1) as FCID_DESCRIPTION,WIZARD_CONTROL_NUMBER,DESCRIPTION50,(select COUNT(*) from LOCATIONS where UPPER(WIZARD_ID)="
			+ "UPPER(WIZARD_LOCATIONS.WIZARD_ID) and UPPER(WIZARD_CONTROL_NUMBER)= UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) and "
			+ "UPPER(FULFILLMENT_CENTER_ID)=UPPER(WIZARD_LOCATIONS.FULFILLMENT_CENTER_ID) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ) as "
			+ "NUMBER_OF_LOCATION, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE= "
			+ "WIZARD_LOCATIONS.LOCATION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)="
			+ "UPPER('LOCTYPE')and ROWNUM <= 1) as LOCATION_TYPE,(select LOCATION_PROFILES.DESCRIPTION20 from LOCATION_PROFILES where "
			+ "UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(LOCATION_PROFILES.PROFILE)=UPPER(WIZARD_LOCATIONS.LOCATION_PROFILE) "
			+ "and ROWNUM <= 1) as LOCATION_PROFILE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || "
			+ "TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=WIZARD_LOCATIONS.CREATED_BY_TEAM_MEMBER and "
			+ "UPPER(TENANT_ID)=? and ROWNUM <= 1) as CREATED_BY_TEAM_MEMBER,trunc(FROM_TZ(CAST(DATE_CREATED AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_Query_WizardControlNo_Search_Second="') as DATECREATED,trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";

	public static final String Location_Wizard_Query_WizardControlNo_Search_Third="') as LAST_ACTIVITY_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' "
			+ "|| TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID="
			+ "WIZARD_LOCATIONS.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_ACTIVITY_TEAM_MEMBER,STATUS,LAST_ACTIVITY_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('WIZARDLOC') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(WIZARD_LOCATIONS.WIZARD_CONTROL_NUMBER) AND ROWNUM = 1) as Notes from "
			+ "WIZARD_LOCATIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(WIZARD_CONTROL_NUMBER) like:wizardControl ORDER BY DATE_CREATED DESC";

	public static final String Location_Wizard_Select_Fulfillment_Query="select * from FULFILLMENT_CENTERS where UPPER(TENANT_ID)=? and UPPER(STATUS)=? ORDER BY UPPER(NAME20)";
	public static final String LocationProfile_FulFillmentCenter_Query_For_Edit="select FULFILLMENT_CENTER_ID,NAME20 from FULFILLMENT_CENTERS where UPPER(TENANT_ID)=? ORDER BY UPPER(NAME20)" ;
	public static final String Fulfillment_Description20Unique_Query="from FULFILLMENTCENTERSPOJO WHERE UPPER(Tenant_Id) = ? AND UPPER(Name20) = ?";
	public static final String Fulfillment_Description50Unique_Query="from FULFILLMENTCENTERSPOJO WHERE UPPER(Tenant_Id) = ? AND UPPER(Name50) = ?";
	public static final String Fulfillment_Description20Unique_Query_Edit="from FULFILLMENTCENTERSPOJO WHERE UPPER(Tenant_Id) = ? AND UPPER(Fulfillment_Center_ID) <> ? AND UPPER(Name20) = ?";
	public static final String Fulfillment_Description50Unique_Query_Edit="from FULFILLMENTCENTERSPOJO WHERE UPPER(Tenant_Id) = ? AND UPPER(Fulfillment_Center_ID) <> ? AND UPPER(Name50) = ?";
	public static final String Location_Wizard_QueryFetchListInventoryLevel="select * from INVENTORY_LEVELS where QUANTITY > 0 AND UPPER(Tenant_Id)=? and UPPER(Company_Id)=? and UPPER(Fulfillment_Center_ID)=? and UPPER(Area)=? and UPPER(Location)=?";
	public static final String Location_Lebel_Print_Location="from LOCATION where UPPER(TENANT_ID)=? AND upper(COMPANY_ID)=? and and upper(FULFILLMENT_CENTER_ID)=? upper(AREA)= ? and upper(LOCATION_REPRINT)=upper('y')";
	public static final String TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_First="select TENANT_ID,trunc(FROM_TZ(CAST(START_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_Second="') as START_DATE,trunc(FROM_TZ(CAST(END_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_Third="') as END_DATE,COMPANY_ID,MESSAGE,TEAM_MEMBER_ID,TICKET_TAPE_MESSAGE,MESSAGE_ID from TEAM_MEMBER_MESSAGES where UPPER(TEAM_MEMBER_ID)=? AND upper(TENANT_ID)=? AND upper(COMPANY_ID)=? AND UPPER(STATUS)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String TeamMembers_SelectTeamMember_PartOfTeamMember_ConditionalQuery="from TEAMMEMBERS where UPPER(TENANT_ID)=? AND (UPPER(LAST_NAME) like :lname OR UPPER(FIRST_NAME) like :fname OR UPPER(MIDDLE_NAME) like :mname) ORDER BY UPPER(LAST_NAME) ASC";
	public static final String TeamMembers_SelectTeamMember_TeamMemberName_Query="select tm.TENANT_ID,tm.TEAM_MEMBER_ID,tm.LAST_NAME,tm.FIRST_NAME,tm.MIDDLE_NAME,tm.CURRENT_STATUS,nvl2(sa.USER_ID, sa.STATUS, 'X') as Status from team_members tm left join SECURITY_AUTHORIZATIONS sa on tm.TEAM_MEMBER_ID=sa.TEAM_MEMBER_ID where upper(tm.tenant_id) = ? and upper(tm.Fulfillment_Center_ID)=? and upper(tm.TEAM_MEMBER_ID)=?";
	public static final String TeamMembers_SelectTeamMember_PartOfTeamMember_Query="select tm.TENANT_ID,tm.TEAM_MEMBER_ID,tm.LAST_NAME,tm.FIRST_NAME,tm.MIDDLE_NAME,tm.CURRENT_STATUS,nvl2(sa.USER_ID, sa.STATUS, 'X') as Status from team_members tm left join SECURITY_AUTHORIZATIONS sa on tm.TEAM_MEMBER_ID=sa.TEAM_MEMBER_ID where upper(tm.tenant_id) = ? and Upper(tm.Fulfillment_Center_ID)=? and (UPPER(tm.LAST_NAME) like :lname OR UPPER(tm.FIRST_NAME) like :fname OR UPPER(tm.MIDDLE_NAME) like :mname)";
	public static final String LocationProfile_ShowAll_Query_First ="SELECT PROFILE_GROUP, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATION_PROFILES.PROFILE_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCPFGRP') and ROWNUM <= 1) as PROFILE_GROUP_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID=LOCATION_PROFILES.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION,PROFILE, DESCRIPTION20, DESCRIPTION50, (select COUNT(*) from LOCATIONS where UPPER(FULFILLMENT_CENTER_ID)=UPPER(LOCATION_PROFILES.FULFILLMENT_CENTER_ID) and UPPER(LOCATION_PROFILE)=UPPER(LOCATION_PROFILES.PROFILE) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?) as NUMBER_OF_LOCATION, trunc(FROM_TZ(CAST(LAST_USED_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_ShowAll_Query_Second ="')as LAST_USED_DATE, (select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_USED_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_USED_TEAM_MEMBER, trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_ShowAll_Query_Third = "') as LAST_CHANGE_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_CHANGE_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCPROFILE') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATION_PROFILES.PROFILE) AND ROWNUM = 1) as Notes from LOCATION_PROFILES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String LocationProfile_LocationProfile_Select_Conditional_Query_First ="SELECT PROFILE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATION_PROFILES.PROFILE_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCPFGRP') and ROWNUM <= 1) as PROFILE_GROUP_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID=LOCATION_PROFILES.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION,PROFILE, DESCRIPTION20, DESCRIPTION50, (select COUNT(*) from LOCATIONS where UPPER(FULFILLMENT_CENTER_ID)=UPPER(LOCATION_PROFILES.FULFILLMENT_CENTER_ID) and UPPER(LOCATION_PROFILE)=UPPER(LOCATION_PROFILES.PROFILE) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?) as NUMBER_OF_LOCATION, trunc(FROM_TZ(CAST(LAST_USED_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_LocationProfile_Select_Conditional_Query_Second ="')as LAST_USED_DATE, (select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_USED_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_USED_TEAM_MEMBER, trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_LocationProfile_Select_Conditional_Query_Third = "') as LAST_CHANGE_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_CHANGE_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCPROFILE') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATION_PROFILES.PROFILE) AND ROWNUM = 1) as Notes from LOCATION_PROFILES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(PROFILE)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String LocationProfile_ProfileGroup_Select_Conditional_Query_First ="SELECT PROFILE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATION_PROFILES.PROFILE_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCPFGRP') and ROWNUM <= 1) as PROFILE_GROUP_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID=LOCATION_PROFILES.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION,PROFILE, DESCRIPTION20, DESCRIPTION50, (select COUNT(*) from LOCATIONS where UPPER(FULFILLMENT_CENTER_ID)=UPPER(LOCATION_PROFILES.FULFILLMENT_CENTER_ID) and UPPER(LOCATION_PROFILE)=UPPER(LOCATION_PROFILES.PROFILE) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?) as NUMBER_OF_LOCATION, trunc(FROM_TZ(CAST(LAST_USED_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_ProfileGroup_Select_Conditional_Query_Second ="')as LAST_USED_DATE, (select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_USED_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_USED_TEAM_MEMBER, trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_ProfileGroup_Select_Conditional_Query_Third = "') as LAST_CHANGE_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_CHANGE_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCPROFILE') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATION_PROFILES.PROFILE) AND ROWNUM = 1) as Notes from LOCATION_PROFILES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND UPPER(PROFILE_GROUP)=? order by LAST_ACTIVITY_DATE DESC";
	public static final String LocationProfile_Description_Select_Conditional_Query_First ="SELECT PROFILE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=LOCATION_PROFILES.PROFILE_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('LOCPFGRP') and ROWNUM <= 1) as PROFILE_GROUP_DESCRIPTION, FULFILLMENT_CENTER_ID, (select FULFILLMENT_CENTERS.NAME20 from FULFILLMENT_CENTERS where FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID=LOCATION_PROFILES.FULFILLMENT_CENTER_ID and UPPER(TENANT_ID)=? and ROWNUM <= 1) as FC_ID_DESCRIPTION,PROFILE, DESCRIPTION20, DESCRIPTION50, (select COUNT(*) from LOCATIONS where UPPER(FULFILLMENT_CENTER_ID)=UPPER(LOCATION_PROFILES.FULFILLMENT_CENTER_ID) and UPPER(LOCATION_PROFILE)=UPPER(LOCATION_PROFILES.PROFILE) and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?) as NUMBER_OF_LOCATION, trunc(FROM_TZ(CAST(LAST_USED_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_Description_Select_Conditional_Query_Second ="')as LAST_USED_DATE, (select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_USED_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_USED_TEAM_MEMBER, trunc(FROM_TZ(CAST(LAST_ACTIVITY_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String LocationProfile_Description_Select_Conditional_Query_Third = "') as LAST_CHANGE_DATE,(select (TEAM_MEMBERS.FIRST_NAME || ' ' || TEAM_MEMBERS.MIDDLE_NAME || ' ' || TEAM_MEMBERS.LAST_NAME) from TEAM_MEMBERS where TEAM_MEMBERS.TEAM_MEMBER_ID=LOCATION_PROFILES.LAST_ACTIVITY_TEAM_MEMBER and UPPER(TENANT_ID)=? and ROWNUM <= 1) as LAST_CHANGE_TEAM_MEMBER,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('LOCPROFILE') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(LOCATION_PROFILES.PROFILE) AND ROWNUM = 1) as Notes from LOCATION_PROFILES where UPPER(TENANT_ID)=? AND UPPER(COMPANY_ID)=? AND (UPPER(DESCRIPTION20) like ? OR UPPER(DESCRIPTION50) like ?) order by LAST_ACTIVITY_DATE DESC";
	public static final String GeneralCodes_QueryGeneralCodeMainList="select * from GENERAL_CODES where UPPER(GENERAL_CODE_ID)='GENERALCODES' and UPPER(TENANT_ID) = ? and UPPER(COMPANY_ID)=? and UPPER(MENU_OPTION_NAME)=?";
	public static final String GeneralCodes_QueryGeneralCodeSubList="select * from GENERAL_CODES where UPPER(TENANT_ID) = ? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODE_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	public static final String MenuMessage_QueryGetAllList_First= "Select mm.Message_ID,mm.Tenant_ID,mm.Company_ID,mm.Status,trunc(FROM_TZ(CAST(mm.START_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String MenuMessage_QueryGetAllList_Second="')as START_DATE,trunc(FROM_TZ(CAST(mm.END_DATE AS TIMESTAMP), 'UTC') AT TIME ZONE '";
	public static final String MenuMessage_QueryGetAllList_Third= "')as END_DATE,mm.TICKET_TAPE_MESSAGE,mm.MESSAGE,cm.NAME50 from MENU_MESSAGES mm join COMPANIES cm on Upper(mm.TENANT_ID) = UPPER(cm.TENANT_ID) and UPPER(mm.COMPANY_ID) = UPPER(cm.COMPANY_ID) where UPPER(mm.Tenant_ID)=? and UPPER(mm.Company_ID)=? and UPPER(mm.Status)=? order by mm.LAST_ACTIVITY_DATE DESC";
	public static final String MenuProfileOption_QueryDeleteMenuProfileOption="delete from MENU_PROFILE_OPTIONS where Upper(MENU_OPTION)=? and Upper(TENANT_ID)=?";
	public static final String MenuProfileOption_QueryNameDeleteMenuProfileOption="MenuProfileOption_QueryNameDeleteMenuProfileOption";
	public static final String MenuOption_CheckMenuOption_Query="select * from MENU_OPTIONS where upper(MENU_OPTION)=?";
	public static final String LocationWizard_ProcedureCallQuery_DeleteAndLocation="CALL DELETEWIZARDWITHLOCATION(:INTENANT,:INFULFILLMENTCENTERID,:INAREA,:INCOMPANY,:INWIZARDID,:INWIZARDCONTROLNUMBER)";
	public static final String Location_Wizard_QueryCheckDeletion="select LOC.* from LOCATIONS LOC where LOC.LOCATION in (select INV.LOCATION from INVENTORY_LEVELS INV where QUANTITY >0 and UPPER(INV.FULFILLMENT_CENTER_ID) = :INFULFILLMENTCENTERID and UPPER(INV.TENANT_ID) = :INTENANT and UPPER(INV.COMPANY_ID) = :INCOMPANY and UPPER(INV.AREA) = :INAREA) and UPPER(LOC.FULFILLMENT_CENTER_ID) = :INFULFILLMENTCENTERID1 and UPPER(LOC.TENANT_ID) = :INTENANT1 and UPPER(LOC.COMPANY_ID) = :INCOMPANY1 and UPPER(LOC.AREA) = :INAREA1 and LOC.WIZARD_CONTROL_NUMBER = :INWIZARDCONTROLNUMBER and UPPER(LOC.WIZARD_ID) = :INWIZARDID";
	public static final String GeneralCode_Languages_Query="from LANGUAGES where OBJECT_CODE in ('GeneralCodeEditDelete','GeneralCodeList') and UPPER(LANGUAGE)=?";
	public static final String MenuOptions_QueryCheckUniqueMenuName="Select MENU_OPTION from MENU_OPTIONS where UPPER(MENU_OPTION)=?";
	public static final String GeneralCodes_QueryDeleteRelatedGeneralCode="Delete from GENERAL_CODES where UPPER(GENERAL_CODE_ID)=?";
	public static final String GeneralCodes_QueryNameGeneralCodeList="GeneralCodeList";	 
	public static final String InputQuery_MenuExecutionLabel_Language="languageName";
	public static final String InputQuery_MenuExecutionLabel_ScreenName="ScreenName";
	public static final String NativeQueryName_MenuExecutionLabel="MenuExecutionLabel";

	public static final	String GetMaxNextWorkControlNumber_Query="Select max(WORK_CONTROL_NUMBER) as NextWorkControlNumber from WORK_TYPE_HEADERS";
	public static String CHECK_LOCATION_STORE_PROCEDURE="{ CALL CREATELOCATIONWORKTASK(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
	public static String STR_Inserted="Inserted";

	//--------------------------------------------------Aakash Bishnoi 13-4-15 Smart Task Execution Search-----------------------------------------------//
//	public static final String RequestMapping_SmartTaskExecution_Search="/SmartTaskExecutions_lookup";
	public static final String SmartTaskExecution_ModelAttribute="ObjSmartTaskExecution";
	public static final String SmartTaskExecutions_Combobox_SelectApplication="SelectApplication";
	public static final String SmartTaskExecutions_Combobox_SelectExecutionType="SelectExecutionType";
	public static final String SmartTaskExecutions_Combobox_SelectExecutionDevice="SelectExecutionDevice";
	public static final String SmartTaskExecutions_Combobox_SelectExecutionGroup="SelectExecutionGroup";
	public static final String GIC_EXEGRP="EXEGRP";
	public static final String GIC_EXETYPE="EXETYPE";
	public static final String GIC_EXEDEVICE="EXEDEVICE";
	public static final String GIC_APPLICATIONS="APPLICATIONS";
	public static final String GIC_GENERALCODES="GENERALCODES";
	public static final String  GIC_INFOHELP="INFOHELP";
	public static final String  GIC_LANGUAGES="LANGUAGES";

	public static final String GIC_APPLICATIONSUBS="APPLICATIONSUBS";
	public static final String	GIC_TASK="TASK";
	public static final String	GIC_AREAS="AREAS";
	public static final String	GIC_LOCTYPE="LOCTYPE";
	public static final String	GIC_WIZARDS="WIZARDS";
	public static final String	GIC_GRPWRKZONE="GRPWRKZONE";
	public static final String	GIC_WORKZONES="WORKZONES";
	public static final String	GIC_STRGTYPES="STRGTYPES";

	public static final String GIC_LOCPFGRP="LOCPFGRP";
	public static final String GIC_MENUTYPES="MENUTYPES";
	/*public static final String LOCATIONTYPECODE="LOCTYPE";
		public static final String STORAGETYPECODE="STRGTYPES";*/

	public static final String GIC_THEMERF = "THEMERF";
	public static final String GIC_COUNTRYCODE="COUNTRYCODE";
	public static final String GIC_STATECODE="STATECODE";
	public static final String GIC_THEMEMOBILE="THEMEMOBILE";
	public static final String GIC_THEMEFULLDISPLAY = "THEMEFULLDISPLAY";

	public static final String GIC_LOCLABEL="LOCLABEL";
	public static final String GIC_SECURITYQUESTION="SECURITYQUESTION";

	/*public static final String DEPARTMENTGENERALCODE="DEPARTMENT";
		public static final String SHIFTGENERALCODE="SHIFTCODE";*/

	public static final String GIC_DEPARTMENT="DEPARTMENT";
	public static final String GIC_SHIFTCODE="SHIFTCODE";

	public static final String ScreenLabels="ScreenLabels";
	public static final String SmartTaskExecutions_Restfull_Search="/SmartTaskExecutions_Restfull_Search";
	public static final String SmartTaskExecutions_Restfull_DisplayAll="/SmartTaskExecutions_Restfull_DisplayAll";
//	public static final String WebServiceStatus_SmartTaskExecutions_lookup="SmartTaskExecutions_lookup";
	public static final String YES="YES";
	public static final String NO="NO";
	public static final String JASCI="JASCI";
	public static final String SmartTaskExecutions_Restfull_Delete="/SmartTaskExecutions_Restfull_Delete";
	public static final String SmartTaskExecutions_Delete="/SmartTaskExecutions_Delete";
	public static final String SmartTaskExecutions_Restfull_FetchExecution="/SmartTaskExecutions_Restfull_FetchExecution";
//	public static final String RequestMapping_SmartTaskExecutions_Kendo_DisplayAll="/SmartTaskExecutions_Kendo_DisplayAll";
//	public static final String RequestMapping_SmartTaskExecutions_Search_Lookup="/SmartTaskExecution_SearchLookup";
//	public static final String RequestMapping_Smart_Task_Executions_Search_Lookup="/Smart_Task_Executions_Search_Lookup";
	public static final String SmartTaskExecutions_Restfull_ExecutionNameExist="/Rest_ExecutionName_ExistForDelete";
	public static final String RequestMapping_SmartTaskExecutions_Maintenance_New="/SmartTaskExecutions_Maintenance_New";
	public static final String SmartTaskExecutions_Maintenance_Update="SmartTaskExecutions_Maintenance_Update";

	public static final String SmartTaskExecution_Restfull_GetTeamMember="/SmartTaskExecution_Restfull_GetTeamMember";
	public static final String SmartTaskExecutions_Restfull_RestExecutionsAdd="/SmartTaskExecutions_Restfull_RestExecutionsAdd";
//	public static final String RequestMapping_SmartTaskExecutions_Maintenance_Add="/SmartTaskExecutions_Maintenance_Add";
	public static final String SmartTaskExecutions_Restfull_CheckUniqueExecutionName="/CheckUniqueExecutionName";
	public static final String RequestMapping_SmartTaskExecutions_Edit="/SmartTaskExecutions_Edit";
	public static final String RequestMapping_SmartTaskExecutions_Update="/RequestMapping_SmartTaskExecutions_Update";
	public static final String SmartTaskExecution_Restfull_Tenant = "Tenant";
	public static final String SmartTaskExecution_Restfull_Application = "Application";
	public static final String SmartTaskExecution_Restfull_ExecutionGroup = "ExecutionGroup";
	public static final String SmartTaskExecution_Restfull_ExecutionType = "ExecutionType";
//	public static final String SmartTaskExecution_Restfull_ExecutionDevice = "ExecutionDevice";
	public static final String SmartTaskExecution_Restfull_Execution = "Execution";
	public static final String SmartTaskExecution_Restfull_Description = "Description";
	public static final String SmartTaskExecution_Restfull_Buttons = "Buttons";	  
//	public static final String SmartTaskExecution_Application_Id_JspPageValue="Application_Id";
//	public static final String SmartTaskExecution_Execution_Group_JspPageValue="Execution_Group";
//	public static final String SmartTaskExecution_Execution_Type_JspPageValue="Execution_Type";
//	public static final String SmartTaskExecution_Execution_Device_JspPageValue="Execution_Device";
//	public static final String SmartTaskExecution_Execution_Name_JspPageValue="Execution_Name";
//	public static final String SmartTaskExecution_Execution_Name_Hidden_JspPageValue="Execution_NameH";
//	public static final String SmartTaskExecution_Description20_JspPageValue="Description20";
//	public static final String SmartTaskExecution_Description50_JspPageValue="Description50";
//	public static final String SmartTaskExecution_HelpLine_JspPageValue="Help_Line";
//	public static final String SmartTaskExecution_ExecutionPath_JspPageValue="Execution";
//	public static final String SmartTaskExecution_Tenant_Id_JspPageValue="Tenant_Id";
//	public static final String SmartTaskExecution_Button_JspPageValue="Button";
	public static final String Strfinally="finally";
	public static final String Strunchecked="unchecked";
	public static final String Strunused="unused";
	public static final String Strrawtypes="rawtypes";
	public static final String SmartTaskExecutions_QueryNameSelectExecutionNameAssign="QueryNameSelectExecutionNameAssign";
	//public static final String SmartTaskExecutions_QuerySelectExecutionNameAssign="select * from SMART_TASK_SEQ_INSTRUCTIONS where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_NAME)=?";
	public static final String SmartTaskExecutions_QuerySelectExecutionNameAssign="from SMARTTASKSEQINSTRUCTIONS where upper(Tenant_Id)=? and upper(Company_Id)=? and upper(Execution_Name)=?";
	public static final String SmartTaskExecutions_QueryNameFetchExecution="QueryNameFetchExecution";
	public static final String SmartTaskExecutions_QueryFetchExecution="select * from SMART_TASK_EXECUTIONS where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_NAME)=?";
	public static final String SmartTaskExecution_Query_Application="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(APPLICATION_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskExecution_Query_Execution_Group="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_GROUP)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskExecution_Query_Execution_Type="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_TYPE)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskExecution_Query_Execution_Device="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_DEVICE)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskExecution_Query_Execution_Button="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(BUTTON)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskExecution_Query_Execution_Description="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON, APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and (UPPER(DESCRIPTION20) like ? or UPPER(DESCRIPTION50) like ?) ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskExecution_Query_Execution_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID) = ? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskExecution_Query_Execution_Name="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON, APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ " ,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_NAME)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	/*
	public static final String SmartTaskExecution_Query_DisplayAll="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON, APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
    */

	//Servandra Global Constant
	//for work flow configurator
	public static final String STR_Copy="Copy";
	public static final String REQUESTMAPPING_WorkFlowConfigurator_RestFull_Check_FulFillment_Center_lookup="/Check_FulFillment_Center_lookup";
	public static final String GIC_WORKTYPE="WORKTYPE";

	public static final String WorkFlowConfigurator_FulfillmentCenter_Query="select WFH.*,(select General_Codes.DESCRIPTION20 from General_Codes WHERE Upper(General_Codes.GENERAL_CODE)=Upper(WFH.WORK_TYPE) and"
			+" General_Codes.GENERAL_CODE_ID='WORKTYPE' and upper(General_Codes.TENANT_ID)=upper(WFH.TENANT_ID) and "
			+ "Upper(General_Codes.COMPANY_ID)=? ) as WorkTypeDesc,"
			+"(select FULFILLMENT_CENTERS.NAME20  from FULFILLMENT_CENTERS where upper(FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID)=Upper(WFH.FULFILLMENT_CENTER_ID)"
			+ " and upper(FULFILLMENT_CENTERS.TENANT_ID)= upper(WFH.TENANT_ID)  and FULFILLMENT_CENTERS.Status='A') as FulFillmentDescription, "
			+ "(select NOTES.NOTE from NOTES where NOTES.NOTE_ID='WORKFLOW' and upper(NOTES.NOTE_LINK)=upper(WFH.WORK_FLOW_ID) and upper(NOTES.TENANT_ID)=upper(WFH.TENANT_ID)"
			+" and upper(NOTES.COMPANY_ID)=upper(WFH.COMPANY_ID)) as NotesStatus"
			+" from WORK_FLOW_HEADERS WFH where upper(WFH.TENANT_ID)=? and upper(WFH.COMPANY_ID)=?"
			+" and upper(WFH.FULFILLMENT_CENTER_ID)=?";

	public static final String WorkFlowConfigurator_WorkType_Query="select WFH.*,(select General_Codes.DESCRIPTION20 from General_Codes WHERE Upper(General_Codes.GENERAL_CODE)=Upper(WFH.WORK_TYPE) and"
			+" General_Codes.GENERAL_CODE_ID='WORKTYPE' and upper(General_Codes.TENANT_ID)=upper(WFH.TENANT_ID) and "
			+ "Upper(General_Codes.COMPANY_ID)=? ) as WorkTypeDesc,"
			+"(select FULFILLMENT_CENTERS.NAME20  from FULFILLMENT_CENTERS where upper(FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID)=Upper(WFH.FULFILLMENT_CENTER_ID)"
			+ " and upper(FULFILLMENT_CENTERS.TENANT_ID)= upper(WFH.TENANT_ID)  and FULFILLMENT_CENTERS.Status='A') as FulFillmentDescription, "
			+"(select NOTES.NOTE from NOTES where NOTES.NOTE_ID='WORKFLOW' and upper(NOTES.NOTE_LINK)=upper(WFH.WORK_FLOW_ID) and upper(NOTES.TENANT_ID)=upper(WFH.TENANT_ID)"
			+" and upper(NOTES.COMPANY_ID)=upper(WFH.COMPANY_ID)) as NotesStatus"
			+" from WORK_FLOW_HEADERS WFH where upper(WFH.TENANT_ID)=? and upper(WFH.COMPANY_ID)=?"
			+" and upper(WFH.WORK_TYPE)=?";
	public static final String WorkFlowConfigurator_PartOfdescription_Query="select WFH.*,(select General_Codes.DESCRIPTION20 from General_Codes WHERE Upper(General_Codes.GENERAL_CODE)=Upper(WFH.WORK_TYPE) and"
			+" General_Codes.GENERAL_CODE_ID='WORKTYPE' and upper(General_Codes.TENANT_ID)=upper(WFH.TENANT_ID) and "
			+ "Upper(General_Codes.COMPANY_ID)=? ) as WorkTypeDesc,"
			+"(select FULFILLMENT_CENTERS.NAME20  from FULFILLMENT_CENTERS where upper(FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID)=Upper(WFH.FULFILLMENT_CENTER_ID)"
			+ " and upper(FULFILLMENT_CENTERS.TENANT_ID)= upper(WFH.TENANT_ID)  and FULFILLMENT_CENTERS.Status='A') as FulFillmentDescription, "
			+"(select NOTES.NOTE from NOTES where NOTES.NOTE_ID='WORKFLOW' and upper(NOTES.NOTE_LINK)=upper(WFH.WORK_FLOW_ID) and upper(NOTES.TENANT_ID)=upper(WFH.TENANT_ID)"
			+" and upper(NOTES.COMPANY_ID)=upper(WFH.COMPANY_ID)) as NotesStatus"
			+" from WORK_FLOW_HEADERS WFH where upper(WFH.TENANT_ID)=? and upper(WFH.COMPANY_ID)=?"
			+" and (Upper(WFH.DESCRIPTION20) Like ? OR Upper(WFH.DESCRIPTION50) Like ? )";

	public static final String WorkFlowConfigurator_DisplayAll_JASCI_Query="select WFH.*,(select General_Codes.DESCRIPTION20 from General_Codes WHERE Upper(General_Codes.GENERAL_CODE)=Upper(WFH.WORK_TYPE) and"
			+" General_Codes.GENERAL_CODE_ID='WORKTYPE' and upper(General_Codes.TENANT_ID)=upper(WFH.TENANT_ID) and "
			+ "Upper(General_Codes.COMPANY_ID)=? ) as WorkTypeDesc,"
			+"(select FULFILLMENT_CENTERS.NAME20  from FULFILLMENT_CENTERS where upper(FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID)=Upper(WFH.FULFILLMENT_CENTER_ID)"
			+ " and upper(FULFILLMENT_CENTERS.TENANT_ID)= upper(WFH.TENANT_ID)  and FULFILLMENT_CENTERS.Status='A') as FulFillmentDescription, "
			+"(select NOTES.NOTE from NOTES where NOTES.NOTE_ID='WORKFLOW' and upper(NOTES.NOTE_LINK)=upper(WFH.WORK_FLOW_ID) and upper(NOTES.TENANT_ID)=upper(WFH.TENANT_ID)"
			+" and upper(NOTES.COMPANY_ID)=upper(WFH.COMPANY_ID)) as NotesStatus"
			+" from WORK_FLOW_HEADERS WFH where upper(WFH.COMPANY_ID)=?";

	public static final String WorkFlowConfigurator_DisplayAll_Query="select WFH.*,(select General_Codes.DESCRIPTION20 from General_Codes WHERE Upper(General_Codes.GENERAL_CODE)=Upper(WFH.WORK_TYPE) and"
			+" General_Codes.GENERAL_CODE_ID='WORKTYPE' and upper(General_Codes.TENANT_ID)=upper(WFH.TENANT_ID) and "
			+ "Upper(General_Codes.COMPANY_ID)=? ) as WorkTypeDesc,"
			+"(select FULFILLMENT_CENTERS.NAME20  from FULFILLMENT_CENTERS where upper(FULFILLMENT_CENTERS.FULFILLMENT_CENTER_ID)=Upper(WFH.FULFILLMENT_CENTER_ID)"
			+ " and upper(FULFILLMENT_CENTERS.TENANT_ID)= upper(WFH.TENANT_ID)  and FULFILLMENT_CENTERS.Status='A') as FulFillmentDescription"
			+", (select NOTES.NOTE from NOTES where NOTES.NOTE_ID='WORKFLOW' and upper(NOTES.NOTE_LINK)=upper(WFH.WORK_FLOW_ID) and upper(NOTES.TENANT_ID)=upper(WFH.TENANT_ID)"
			+" and upper(NOTES.COMPANY_ID)=upper(WFH.COMPANY_ID)) as NotesStatus"
			+" from WORK_FLOW_HEADERS WFH where (upper(WFH.TENANT_ID)=? OR upper(WFH.TENANT_ID)=?) and upper(WFH.COMPANY_ID)=?";

	public static final String RequestMapping_WorkFlowConfigurator_WorkFlowSearchLookup="/Work_Flow_Search_Lookup";
	public static final String RequestMapping_WorkFlowConfigurator_SmartWorkFlowLookup="/SmartWorkFlowLookup";
	public static final String RequestMapping_WorkFlowConfigurator_SmartWorkflowConfiguratorKendo="/SmartWorkflowConfiguratorKendo";

	public static final String RequestParameter_WorkFlowConfigurator_WorkFlowConfiguratorFieldValue="WorkFlowConfiguratorFieldValue";
	public static final String RequestParameter_WorkFlowConfigurator_WorkFlowConfiguratorFieldSearch="WorkFlowConfiguratorFieldSearch";



	public static final String WorkFlowConfigurator_WorkFlowSearchLookup_Screen="Work_Flow_Search_Lookup";
	public static final String WorkFlowConfigurator_WorkFlowlookup_Screen="Work_Flow_lookup";
	public static final String WorkFlowConfigurator_objScreenLabel="objScreenLabel";
	public static final String WorkFlowConfigurator_ObjGeneralCode="ObjGeneralCode";
	public static final String WorkFlowConfigurator_objFulfillmentCenter="objFulfillmentCenter";


	public static final String WorkFlowConfigurator_lblPartOFDescpID="lblPartOFDescpID";
	public static final String WorkFlowConfigurator_lblWorkFlowTypeID="lblWorkFlowTypeID";
	public static final String WorkFlowConfigurator_lblFulfillmentCenterID="lblFulfillmentCenterID";

	//Smart Task COnfigurator

	public static final String SmartTaskConfigurator_ModelAttribute="ObjSmartTaskConfigurator";
	public static final String SmartTaskConfigurator_Combobox_SelectTask="SelectTask";
	public static final String SmartTaskConfigurator_Combobox_SelectApplication="SelectApplication";
	public static final String SmartTaskConfigurator_Combobox_SelectExecutionType="SelectExecutionType";
	public static final String SmartTaskConfigurator_Combobox_SelectExecutionDevice="SelectExecutionDevice";
	public static final String SmartTaskConfigurator_Combobox_SelectExecutionGroup="SelectExecutionGroup";
	public static final String WebServiceStatus_SmartTaskConfigurator_lookup="SmartTaskConfigurator_lookup";
	public static final String SmartTaskConfigurator_Restfull_Tenant = "Tenant";
	public static final String SmartTaskConfigurator_Menu_Name="EngineStart?MenuName=";
	public static final String SmartTaskConfigurator_Restfull_Company = "Company";
	public static final String SmartTaskConfigurator_Restfull_Task = "Task";
	public static final String SmartTaskConfigurator_Restfull_Application = "Application";
	public static final String SmartTaskConfigurator_Restfull_ExecutionGroup = "ExecutionGroup";
	public static final String SmartTaskConfigurator_Restfull_ExecutionType = "ExecutionType";
	public static final String SmartTaskConfigurator_Restfull_ExecutionDevice = "ExecutionDevice";
	public static final String SmartTaskConfigurator_Restfull_ExecutionSequence = "ExecutionSequence";
	public static final String SmartTaskConfigurator_Restfull_Description = "Description";
	public static final String SmartTaskConfigurator_Restfull_MenuName = "MenuName";
	public static final String SmartTaskConfigurator_Restfull_Search="/SmartTaskConfigurator_Restfull_Search";
	public static final String SmartTaskConfigurator_Restfull_Search_All="/SmartTaskConfigurator_Restfull_Search_All";
	public static final String SmartTaskConfigurator_Restfull_DisplayAll="/SmartTaskConfigurator_Restfull_DisplayAll";
	public static final String RequestMapping_SmartTaskConfigurator_Search="/SmartTaskConfigurator_Lookup";
	public static final String RequestMapping_SmartTaskConfigurator_SearchLookup="/SmartTaskConfigurator_SearchLookup";

	public static final String SmartTaskConfigurator_Application_Id_JspPageValue="APPLICATION_ID";
	public static final String SmartTaskConfigurator_Task_JspPageValue="TASK";
	public static final String SmartTaskConfigurator_Description20_JspPageValue="DESCRIPTION20";
	public static final String SmartTaskConfigurator_Execution_Sequence_Group_JspPageValue="EXECUTION_SEQUENCE_GROUP";
	public static final String SmartTaskConfigurator_Execution_Type_JspPageValue="EXECUTION_TYPE";
	public static final String SmartTaskConfigurator_Execution_Device_JspPageValue="EXECUTION_DEVICE";
	public static final String SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue="EXECUTION_SEQUENCE_NAME";
	public static final String SmartTaskConfigurator_Tenant_Id_JspPageValue="TENANT_ID";
	public static final String SmartTaskConfigurator_Menu_Name_JspPageValue="MENU_NAME";
	public static final String RequestMapping_Smart_Task_Configurator_Search_Lookup="/Smart_Task_Configurator_SearchLookup";
	public static final String RequestMapping_SmartTaskConfigurator_Kendo_DisplayAll="/SmartTaskConfigurator_Kendo_DisplayAll";
	public static final String RequestMapping_SmartTaskConfigurator_Delete="/SmartTaskConfigurator_Delete";
	public static final String SmartTaskConfigurator_Restfull_ExecutionSequenceNameExist="/SmartTaskConfigurator_Restfull_ExecutionSequenceNameExist";
	public static final String SmartTaskConfigurator_Restfull_Delete="/SmartTaskConfigurator_Restfull_Delete";

	/**Add By Shailendra Rajput*/
	public static final String StrRequestMapping_SmartTaskConfigurator_AddEntry="/SmartTaskConfigurator_Add";
	public static final String StrRequestMapping_SmartTaskConfigurator_Maintenance="/SmartTaskConfigurator_Maintenance";


	public static final String SmartTaskConfigurator_Restfull_ExecutionName = "ExecutionName";
	public static final String SmartTaskConfigurator_Restfull_getSTSHeadersData="/SmartTaskConfigurator_Restfull_getSTSHeadersData";
	public static final String SmartTaskConfigurator_Restfull_getSTExecutionsData="/SmartTaskConfigurator_Restfull_getSTExecutionsData";
	public static final String SmartTaskConfigurator_Restfull_getGeneralCodeDropDown="/SmartTaskConfigurator_Restfull_getGeneralCodeDropDown";
	public static final String SmartTaskConfigurator_Restfull_GeneralCodeId = "GeneralCodeId";
	public static final String SmartTaskConfigurator_Restfull_getSTSeqInstructions="/SmartTaskConfigurator_Restfull_getSTSeqInstructions";
	public static final String SmartTaskConfigurator_Restfull_getLabels="SmartTaskConfigurator_Restfull_getLabels";
	public static final String GOJS_Restfull_getGOJSConfigProp="GOJS_Restfull_getGOJSConfigProp";


	public static final String SmartTaskConfigurator_Query_DisplayAll="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_DisplayAll_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Application="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and  UPPER(APPLICATION_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Application_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(APPLICATION_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";



	public static final String SmartTaskConfigurator_Query_Task="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and  UPPER(TASK)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Task_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(TASK)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Execution_Group="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and  UPPER(EXECUTION_SEQUENCE_GROUP)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Execution_Group_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(EXECUTION_SEQUENCE_GROUP)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Execution_Type="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and  UPPER(EXECUTION_TYPE)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Execution_Type_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(EXECUTION_TYPE)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Execution_Device="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and  UPPER(EXECUTION_DEVICE)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Execution_Device_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(EXECUTION_DEVICE)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Execution_Sequence="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and  UPPER(EXECUTION_SEQUENCE_NAME)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Execution_Sequence_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(EXECUTION_SEQUENCE_NAME)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Description="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and (Upper(DESCRIPTION20) Like ? OR Upper(DESCRIPTION50) Like ? ) ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Description_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and (Upper(DESCRIPTION20) Like ? OR Upper(DESCRIPTION50) Like ? ) ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Tenant_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_Query_Menu_Name="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=?  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and  UPPER(NT.COMPANY_ID)= ?  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=? and  UPPER(COMPANY_ID)=? and  UPPER(MENU_OPTION)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Query_Menu_Name_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_TYPE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION,"
			+ " EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') and ROWNUM <= 1)  as EXECUTION_DEVICE_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_GROUP,(select GENERAL_CODES.DESCRIPTION20 from  GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_GROUP and  UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID)  and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') and ROWNUM <= 1) as  EXE_SEQ_GROUP_DESCRIPTION,"
			+ " EXECUTION_SEQUENCE_NAME,DESCRIPTION50,MENU_OPTION,"
			+ " APPLICATION_ID, (select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.APPLICATION_ID  and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1)  as APPLICATION_DESCRIPTION ,"
			+ " TASK,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_HEADERS.TASK and UPPER(GENERAL_CODES.TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID)  and UPPER(GENERAL_CODES.COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and  UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('TASK') and ROWNUM <= 1) as TASK_DESCRIPTION,"
			+ " (select NOTE from (select * from NOTES NT,SMART_TASK_SEQ_HEADERS STSQ where UPPER(NT.TENANT_ID)=UPPER(STSQ.TENANT_ID) and  UPPER(NT.COMPANY_ID)=UPPER(STSQ.COMPANY_ID)  AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where  UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_SEQ_HEADERS.EXECUTION_SEQUENCE_NAME) AND ROWNUM = 1) as Note "
			+ " ,COMPANY_ID from SMART_TASK_SEQ_HEADERS where UPPER(TENANT_ID)=UPPER(SMART_TASK_SEQ_HEADERS.TENANT_ID) and  UPPER(COMPANY_ID)=UPPER(SMART_TASK_SEQ_HEADERS.COMPANY_ID) and UPPER(MENU_OPTION)=? ORDER BY LAST_ACTIVITY_DATE DESC";


	public static final String SmartTaskConfigurator_ExistExecutionSequenceName="from WORKFLOWSTEPS where upper(Tenant_Id)=? and upper(Company_Id)=? and upper(Execution_Sequence_Name)=?";

	public static final String SmartTaskConfigurator_QueryNameDeleteMenuProfileOption="delete MENUPROFILEOPTIONS where upper(Tenant_Id)=? and upper(Menu_Option)=?";
	public static final String SmartTaskConfigurator_QueryNameDeleteMenuOption="delete MENUOPTIONS where upper(Tenant_Id)=? and upper(Menu_Option)=?";
	public static final String SmartTaskConfigurator_QueryNameDeleteSmartTaskSeqInstruction="delete SMARTTASKSEQINSTRUCTIONS where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_SEQUENCE_NAME)=?";
	public static final String SmartTaskConfigurator_QueryNameDeleteSmartTaskSeqHeaders="delete SMARTTASKSEQHEADERS where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_SEQUENCE_NAME)=?";
	public static final String SmartTaskConfigurator_QueryNameSelectSmartTaskSeqHeaders="from SMARTTASKSEQHEADERS where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_SEQUENCE_NAME)=?";
	//public static final String SmartTaskConfigurator_QueryNameSelectSmartTaskExecution="from SMARTTASKEXECUTIONS where upper(Tenant_Id)=? and upper(Company_Id)=?  and upper(Execution_Group)=? and upper(Execution_Type)=? and upper(Execution_Device)=? order by Description20 asc";
	public static final String SmartTaskConfigurator_QueryNameSelectSmartTaskExecutionData="from SMARTTASKEXECUTIONS where upper(Tenant_Id)=? and upper(Company_Id)=? order by Description20 asc";
	public static final String  SmartTaskConfigurator_QueryNameSelectSmartTaskExecutionDescriotions= "from SMARTTASKEXECUTIONS where UPPER(EXECUTION_NAME) = ? and ROWNUM = 1";
	//public static final String SmartTaskConfigurator_QueryNameGetSequenceView="select TENANT_ID,COMPANY_ID,EXECUTION_SEQUENCE,EXECUTION_SEQUENCE_NAME,EXECUTION_SEQUENCE_TYPE,EXECUTION_NAME,ACTION_NAME,COMMENTS,GOTO_TAG,MESSAGE,CUSTOM_EXECUTION,RETURN_CODE_VALUE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where  GENERAL_CODES.GENERAL_CODE=SMART_TASK_SEQ_INSTRUCTIONS.EXECUTION_SEQUENCE_TYPE and UPPER(TENANT_ID)=?  and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXESEQTYP') and ROWNUM <= 1) as EXE_SEQ_TYPE_DESCRIPTION FROM SMART_TASK_SEQ_INSTRUCTIONS where  upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_SEQUENCE_NAME)=?";
	public static final String SmartTaskConfigurator_QueryNameGetSequenceView="FROM SMARTTASKSEQINSTRUCTIONS where  upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_SEQUENCE_NAME)=? order by EXECUTION_SEQUENCE";
	public static final String MenuMessages_company_Query="from MENUMESSAGES where upper(Tenant_ID)=? and upper(Status)=? and upper(Company_ID)=? and START_DATE <= sys_extract_utc(localtimestamp) order by LAST_ACTIVITY_DATE DESC";  
	public static final String TeamMemberMenuMessages_company_Query="from TEAMMEMBERMESSAGES where upper(Tenant_ID)=? and upper(Status)=? and upper(Company_ID)=? and upper(Team_Member_ID)=? and START_DATE <= sys_extract_utc(localtimestamp) order by LAST_ACTIVITY_DATE DESC";

	public static final String SmartTaskConfigurator_Restfull_RestSmartTaskConfiguratorAdd="/SmartTaskConfigurator_Restfull_RestSmartTaskConfiguratorAdd";
	public static final String SmartTaskConfigurator_Restfull_RestSmartTaskConfiguratorUpdate="/SmartTaskConfigurator_Restfull_RestSmartTaskConfiguratorUpdate";
	public static final String StrRequestMapping_SmartTaskConfigurator_EditEntry="/SmartTaskConfigurator_Edit";
	public static final String SmartTaskConfigurator_Company_Id_JspPageValue="COMPANY_ID";
	public static final String SmartTaskConfigurator_Description50_JspPageValue="DESCRIPTION50";
	public static final String SmartTaskConfigurator_Menu_Option_JspPageValue="MENU_OPTION";
	public static final String SmartTaskConfigurator_Last_Activity_Date_JspPageValue="LAST_ACTIVITY_DATE";
	public static final String SmartTaskConfigurator_Last_Activity_Team_Member_JspPageValue="LAST_ACTIVITY_TEAM_MEMBER";
	public static final String SmartTaskConfigurator_Restfull_getSTSHeadersData_AngularJS="/SmartTaskConfigurator_Restfull_getSTSHeadersData_AngularJS";
	public static final String SmartTaskConfigurator_Restfull_GetTeamMember="/SmartTaskConfigurator_Restfull_GetTeamMember";
	public static final String nodeDataArray="nodeDataArray";
	public static final String linkDataArray="linkDataArray";
	public static final String  key="key";
	public static final String figure="figure";
	public static final String Category="category";
	public static final String Diamond="DIAMOND";
	public static final String Rectangle="RECTANGLE";
	public static final String COMMENTS="COMMENT";
	public static final String Default="DEFAULT";
	public static final String DISPLAY="DISPLAY";
	public static final String ERRORDISPLAY="ERRORDISPLAY";
	public static final String CUSTOMEXECUTION="CUSTOM EXECUTION";
	public static final String IFRETURNCODE="IF RETURN CODE";
	public static final String IFEXIT="IF EXIT";
	public static final String IFERROR="IF ERROR";
	public static final String GOTOTAG="GOTO TAG";
	public static final String GENERALTAG="GENERAL TAG";
	public static final String End="END";
	public static final String  Action="ACT";
	public static final String Execution="EXEC";

	public static final String  ActionFull="ACTION";
	public static final String ExecutionFULL="EXECUTION";

	public static final String Start="START";
	public static final String text="text";
	public static final String ExecutionName="executionName";
	public static final String ToolTipText="toolTipText";
	public static final String StrRequestMapping_SmartTaskConfigurator_CopyEntry="/SmartTaskConfigurator_Copy";
	public static final String SmartTaskConfigurator_Restfull_existExecutionSequenceNameSTSH="/SmartTaskConfigurator_Restfull_existExecutionSequenceNameSTSH";
	public static final String SmartTaskConfigurator_ExistExecutionSequenceNameSTSH="from SMARTTASKSEQHEADERS where upper(Tenant_Id)=? and upper(Company_Id)=? and upper(Execution_Sequence_Name)=?";
	public static final String SmartTaskConfigurator_ValidCustomExecution = "from SMARTTASKEXECUTIONS where UPPER(EXECUTION_PATH) = ?";
	public static final String SmartTaskConfigurator_ExistMenuOptionSTSH="from MENUOPTIONS where upper(MenuOption)=?";
	public static final String SmartTaskConfigurator_JsonSequenceView_JspPageValue="JSONSEQUENCEVIEW";
	public static final String SmartTaskConfigurator_JsonSequenceNAME_JspPageValue="JSONSEQUENCENAME";
	public static final String SmartTaskConfigurator_CustomExecutionPatValue="CUSTOMEXECUTIONVVALUE";
	public static final String SmartTaskConfigurator_Restfull_SmartTaskConfiguratorSequenceViewData="/SmartTaskConfiguratorSequenceViewData";
	public static final String SmartTaskConfigurator_Restfull_SmartTaskConfiguratorSequenceName="/SmartTaskConfiguratorSequenceName";
	public static final String GENERAL_CODES_DROPDOWN_HELPLINE = "select General_Code,Description20,Description50,Help_Line from GENERAL_CODES WHERE UPPER(Tenant_Id) = ? AND UPPER(Company_Id) = ? AND UPPER(General_Code_ID) = ? order by General_Code asc";

	public static final String XlaticsDashBoardPath="XlaticsDashBoardPath";
	public static final String CompIDValue="CompIDValue";
	public static final String APIKEYValue="APIKEYValue";
	public static final String SECRETKEYValue="SECRETKEYValue";
	public static final String UserIDValue="UserIDValue";
	public static final String ProjectNameValue="ProjectNameValue";
	public static final String DepartmentIDValue="DepartmentIDValue";
	public static final String DashboardIdValue="DashboardIdValue";
	public static final String SmartTaskConfigurator_Restfull_SmartTaskConfiguratorPageNameBackButton="/SmartTaskConfiguratorPageNameBackButton";
	public static final String SmartTaskConfigurator_Restfull_SmartTaskConfiguratorValidateCustomExecution="/SmartTaskConfiguratorValidateCustomExecution";
	public static final String PageName="PageName";
	//public static final String VISUAL="VISUAL";
	//public static final String SmartTaskConfigurator_QueryNameSelectSmartTaskExecution_Without_Visual="from SMARTTASKEXECUTIONS where upper(Tenant_Id)=? and upper(Company_Id)=?  and upper(Execution_Group)=? and upper(Execution_Device)=? order by Description20 asc";
	public static final String MENU_EXECUTION_NAME="MENU_EXECUTION_NAME";
    public static final String COMMON_SESSION_VALUES="COMMON_SESSION_VALUES";
    /**Configurator constant*/
    
    public static final String Configurator="/Configurator";
    //public static final String RedirectConfiguratorHtmlPage="redirect:/pages/Configurator.html";
    public static final String RedirectConfiguratorHtmlPage="Configurator";
    public static final String gojsdata="gojsdata";
    public static final String B="B";
    public static final String T="T";
    public static final String R="R";
    public static final String L="L";
    public static final String Stringpointone=".1";
    public static final String toPort="toPort";
    public static final String fromPort="fromPort";
    public static final String go_GraphLinksModel="go.GraphLinksModel";
    public static final String from="from";
    public static final String to="to";
    public static final String Cache_Control="Cache-Control";
    public static final String no_cache="no-cache";
    public static final String  no_store="no-store";
    public static final String Expires="Expires";
    public static final String 	Pragma="Pragma";
    
    public static final String getlanguagekeyphrasequery="select LANGUAGE,KEY_PHRASE,nvl2(TRANSLATION,TRANSLATION,KEY_PHRASE) as TRANSLATION,LAST_ACTIVITY_DATE,LAST_ACTIVITY_TEAM_MEMBER from languages where UPPER(language)=? and UPPER(KEY_PHRASE) in (";
    public static final String RightBraces=")";
    public static final String HTMLCONTENT="text/html; charset=utf-8";
    
    public static final String WIZARDCONTROL = "wizardControl";
    public static final String GETDISTINCTAPPICONADDRESS = "select DISTINCT APP_ICON_ADDRESS from MENU_APP_ICONS where Upper(APP_ICON)= ?";
    public static final String  GETLOCATIONDATA= "from LOCATION where UPPER(Id.Tenant_ID)=? AND upper(Id.Company_ID)=? and upper(Id.Fullfillment_Center_ID)=? and upper(Id.Area)= ? and upper(Location_Reprint)=upper('y')";
    
    
    public static final String DownloadLocFile="/DownloadLocFile";
    public static final String  DuplicateLocPath="DuplicateLocPath";
    public static final String display_all="display all";
    public static final String No_Help_exist_for_menu_profile="No Help exist for menu profile";
    public static final String strDate="strDate";
    public static final String profile="profile";
    public static final String UrlcheckScreenAccess="/checkScreenAccess";
    public static final String You_have_access="You have access";
    public static final String Success="Success";
    public static final String Fail="Fail";
    public static final String You_don_have_access="You don't have access";
    public static final String Something_happen_wrong="Something happen wrong";
    public static final String UrlAddTeamMemberInSecurityAuthorization="/AddTeamMemberInSecurityAuthorization";
    public static final String UrlGetTeamMember="/GetTeamMember";
    public static final String UrlGetTeamMemberByPartName="/GetTeamMemberByPartName";
    public static final String UrlGetTeamMemberById="/GetTeamMemberById";
    public static final String Addressvalidation="Addressvalidation";
    public static final String LookupGeneralCode="LookupGeneralCode";
    public static final String Attrebutes_Request="Attrebutes_Request";
    public static final String LookupGeneralCodeList="LookupGeneralCodeList";
    public static final String TeammemberLookup="TeammemberLookup";
    public static final String zipvalidation="zipvalidation";
    public static final String Fulfillment_center_look_up_controller_is_called="Fulfillment center look up controller is called";
    public static final String viewTenant="viewTenant";
    public static final String Manage_GENERALCODES="Manage GENERALCODES";
    public static final String objSelectedMenuType="objSelectedMenuType";
    public static final String objLblTeamId="objLblTeamId";
    public static final String objchangedDropDown="objchangedDropDown";
    public static final String MenuAssignmentJson="MenuAssignmentJson";
    public static final String LastActivityDateH="LastActivityDateH";
    public static final String TMobile="TMobile";
    public static final String TRF="TRF";
    public static final String TFullDisplay="TFullDisplay";
    public static final String lblEdit="lblEdit";
    public static final String DateTime="DateTime";
    public static final String Security_Set_Up="Security Set Up";
    public static final String Security="Security";
    public static final String STR_GeneralCodeidentificationList="GeneralCodeidentificationList";
    public static final String STR_GeneralCodeIdentifiedEdit="GeneralCodeIdentifiedEdit";
    public static final String InsertInventoryMessage="Insert record in Inventory_Levels, Inventory_Transaction and Update CyclePoint in Locations table succefully";
    public static final String UpdateInventoryMessage="Update record in Inventory_Levels, Inventory_Transaction and Update CyclePoint in Locations table succefully";
    public static final String InventoryErrorMessage="Please insert Y () or N value in LocationFlag field";
    public static final String No_InfoHelp_record_found="No InfoHelp record found";
    public static final String File_is_not_exists="File is not exists";
    public static final String Content_Disposition="Content-Disposition";
    public static final String attachment_filename="attachment; filename=";
    public static final String application_pdf="application/pdf";
    /**Config Infomation*/
    
    public static final String COMPANYLOGOVALUE = "https://s3.amazonaws.com/jasci/companylogo.png";
    public static final String FOOTERTEXTVALUE = "Copyright &copy;2014 - JASCI, LLC. All Rights Reserved.";
    public static final String FORVALIDEMAILIDVALUE = "If the data corresponds to a valid account, you will be sent a special URL via e-mail. Please follow the link to change your password.";

    /**Xtlytics dashboard credential*/
    public static final String  XlaticsDashBoardPathCatch="http://ec2-54-164-101-182.compute-1.amazonaws.com/XtLytics_demo8/ExternalDashboard11.aspx";
    public static final String  CompIDCatch="139";
    public static final String  APIKEYCatch="APIKEY";
    public static final String  SECRETKEYCatch="SECRETKEY";
    public static final String  UserIDCatch="jasci.demo@xtlytics.com";
    public static final String  ProjectNameCatch="JASCI";
    public static final String  DepartmentIDCatch="043";
    public static final String  DashboardIdCatch="bbaef167-1e46-11e5-819b-066d82643b57";
    public static final String MENU_TYPE_GLASS="GLASS";
    public static final String MENU_TYPE_MOBILE="MOBILE";
    
    public static final String SmartTaskConfigurator_QueryNameSelectSmartTaskExecution="from SMARTTASKEXECUTIONS where upper(Tenant_Id)=? and upper(Company_Id)=?  and upper(Execution_Group)=? and (upper(Execution_Type)=? OR upper(Execution_Type)=?) and upper(Execution_Device)=? order by Description20 asc";
    public static final String BACKEND="BACKEND";
    public static final String EXEONLY="EXEONLY";
    public static final String SmartTaskConfigurator_QueryNameSelectSmartTaskExecution_BeckendOnly="from SMARTTASKEXECUTIONS where upper(Tenant_Id)=? and upper(Company_Id)=?  and upper(Execution_Group)=? and upper(Execution_Type)=? and upper(Execution_Device)=? order by Description20 asc";
    
    // Add constant for Custome Execution validate
    public static final String ERRORCOULDNOTADDURLTOSYSTEMCLASSLOADER = "Error, could not add URL to system classloader";
    public static final String ADDURL = "addURL";
  //  public static final String CURRENTLANGUAGE = "CURRENTLANGUAGE";
    public static final String  DEFAULT="Default";
    public static final String NextLineRegex="\n";
    public static final String Char_z = "z";
    
	//----------------------------------------- For Class CARTS-----------------------------------------------------
    public static final String	DATABASE_TENANT_ID="TENANT_ID";
    public static final String	DATABASE_COMPANY_ID="COMPANY_ID";
    public static final String	DATABASE_FULFILLMENT_CENTER_ID="FULFILLMENT_CENTER_ID";
    public static final String	DATABASE_CART_ID="CART_ID";
    public static final String	DATABASE_CART_TYPE="CART_TYPE";
    public static final String	DATABASE_NAME20="NAME20";
    public static final String	DATABASE_NAME50="NAME50";
    public static final String	DATABASE_DEPARTMENT="DEPARTMENT";
    public static final String	DATABASE_CREATED_BY="CREATED_BY";
    public static final String	DATABASE_DATE_CREATED="DATE_CREATED";
    public static final String	DATABASE_TEAMMEMBER_ASSIGNEDTO_CART="TEAMMEMBER_ASSIGNEDTO_CART";
    public static final String	DATABASE_NUMBER_CART_SLOTS="NUMBER_CART_SLOTS";
    public static final String	DATABASE_CART_SLOT01="CART_SLOT01";
    public static final String	DATABASE_CART_SLOT02="CART_SLOT02";
    public static final String	DATABASE_CART_SLOT03="CART_SLOT03";
    public static final String	DATABASE_CART_SLOT04="CART_SLOT04";
    public static final String	DATABASE_CART_SLOT05="CART_SLOT05";
    public static final String	DATABASE_CART_SLOT06="CART_SLOT06";
    public static final String	DATABASE_CART_SLOT07="CART_SLOT07";
    public static final String	DATABASE_CART_SLOT08="CART_SLOT08";
    public static final String	DATABASE_CART_SLOT09="CART_SLOT09";
    public static final String	DATABASE_CART_SLOT10="CART_SLOT10";
    public static final String	DATABASE_CART_SLOT11="CART_SLOT11";
    public static final String	DATABASE_CART_SLOT12="CART_SLOT12";
    public static final String	DATABASE_CART_SLOT13="CART_SLOT13";
    public static final String	DATABASE_CART_SLOT14="CART_SLOT14";
    public static final String	DATABASE_CART_SLOT15="CART_SLOT15";
    public static final String	DATABASE_CART_SLOT16="CART_SLOT16";
    public static final String	DATABASE_CART_SLOT17="CART_SLOT17";
    public static final String	DATABASE_CART_SLOT18="CART_SLOT18";
    public static final String	DATABASE_CART_SLOT19="CART_SLOT19";
    public static final String	DATABASE_CART_SLOT20="CART_SLOT20";
    public static final String	DATABASE_CART_SLOT21="CART_SLOT21";
    public static final String	DATABASE_CART_SLOT22="CART_SLOT22";
    public static final String	DATABASE_CART_SLOT23="CART_SLOT23";
    public static final String	DATABASE_CART_SLOT24="CART_SLOT24";
    public static final String	DATABASE_CART_SLOT25="CART_SLOT25";
    public static final String	DATABASE_CART_SLOT26="CART_SLOT26";
    public static final String	DATABASE_CART_SLOT27="CART_SLOT27";
    public static final String	DATABASE_CART_SLOT28="CART_SLOT28";
    public static final String	DATABASE_CART_SLOT29="CART_SLOT29";
    public static final String	DATABASE_CART_SLOT30="CART_SLOT30";
    public static final String	DATABASE_CART_SLOT31="CART_SLOT31";
    public static final String	DATABASE_CART_SLOT32="CART_SLOT32";
    public static final String	DATABASE_CART_SLOT33="CART_SLOT33";
    public static final String	DATABASE_CART_SLOT34="CART_SLOT34";
    public static final String	DATABASE_CART_SLOT35="CART_SLOT35";
    public static final String	DATABASE_CART_SLOT36="CART_SLOT36";
    public static final String	DATABASE_CART_SLOT37="CART_SLOT37";
    public static final String	DATABASE_CART_SLOT38="CART_SLOT38";
    public static final String	DATABASE_CART_SLOT39="CART_SLOT39";
    public static final String	DATABASE_CART_SLOT40="CART_SLOT40";
    public static final String	DATABASE_CART_SLOT41="CART_SLOT41";
    public static final String	DATABASE_CART_SLOT42="CART_SLOT42";
    public static final String	DATABASE_CART_SLOT43="CART_SLOT43";
    public static final String	DATABASE_CART_SLOT44="CART_SLOT44";
    public static final String	DATABASE_CART_SLOT45="CART_SLOT45";
    public static final String	DATABASE_CART_SLOT46="CART_SLOT46";
    public static final String	DATABASE_CART_SLOT47="CART_SLOT47";
    public static final String	DATABASE_CART_SLOT48="CART_SLOT48";
    public static final String	DATABASE_CART_SLOT49="CART_SLOT49";
    public static final String	DATABASE_CART_SLOT50="CART_SLOT50";
    public static final String	DATABASE_WORK_CONTROL_NUMBER01="WORK_CONTROL_NUMBER01";
    public static final String	DATABASE_WORK_CONTROL_NUMBER02="WORK_CONTROL_NUMBER02";
    public static final String	DATABASE_WORK_CONTROL_NUMBER03="WORK_CONTROL_NUMBER03";
    public static final String	DATABASE_WORK_CONTROL_NUMBER04="WORK_CONTROL_NUMBER04";
    public static final String	DATABASE_WORK_CONTROL_NUMBER05="WORK_CONTROL_NUMBER05";
    public static final String	DATABASE_WORK_CONTROL_NUMBER06="WORK_CONTROL_NUMBER06";
    public static final String	DATABASE_WORK_CONTROL_NUMBER07="WORK_CONTROL_NUMBER07";
    public static final String	DATABASE_WORK_CONTROL_NUMBER08="WORK_CONTROL_NUMBER08";
    public static final String	DATABASE_WORK_CONTROL_NUMBER09="WORK_CONTROL_NUMBER09";
    public static final String	DATABASE_WORK_CONTROL_NUMBER10="WORK_CONTROL_NUMBER10";
    public static final String	DATABASE_WORK_CONTROL_NUMBER11="WORK_CONTROL_NUMBER11";
    public static final String	DATABASE_WORK_CONTROL_NUMBER12="WORK_CONTROL_NUMBER12";
    public static final String	DATABASE_WORK_CONTROL_NUMBER13="WORK_CONTROL_NUMBER13";
    public static final String	DATABASE_WORK_CONTROL_NUMBER14="WORK_CONTROL_NUMBER14";
    public static final String	DATABASE_WORK_CONTROL_NUMBER15="WORK_CONTROL_NUMBER15";
    public static final String	DATABASE_WORK_CONTROL_NUMBER16="WORK_CONTROL_NUMBER16";
    public static final String	DATABASE_WORK_CONTROL_NUMBER17="WORK_CONTROL_NUMBER17";
    public static final String	DATABASE_WORK_CONTROL_NUMBER18="WORK_CONTROL_NUMBER18";
    public static final String	DATABASE_WORK_CONTROL_NUMBER19="WORK_CONTROL_NUMBER19";
    public static final String	DATABASE_WORK_CONTROL_NUMBER20="WORK_CONTROL_NUMBER20";
    public static final String	DATABASE_WORK_CONTROL_NUMBER21="WORK_CONTROL_NUMBER21";
    public static final String	DATABASE_WORK_CONTROL_NUMBER22="WORK_CONTROL_NUMBER22";
    public static final String	DATABASE_WORK_CONTROL_NUMBER23="WORK_CONTROL_NUMBER23";
    public static final String	DATABASE_WORK_CONTROL_NUMBER24="WORK_CONTROL_NUMBER24";
    public static final String	DATABASE_WORK_CONTROL_NUMBER25="WORK_CONTROL_NUMBER25";
    public static final String	DATABASE_WORK_CONTROL_NUMBER26="WORK_CONTROL_NUMBER26";
    public static final String	DATABASE_WORK_CONTROL_NUMBER27="WORK_CONTROL_NUMBER27";
    public static final String	DATABASE_WORK_CONTROL_NUMBER28="WORK_CONTROL_NUMBER28";
    public static final String	DATABASE_WORK_CONTROL_NUMBER29="WORK_CONTROL_NUMBER29";
    public static final String	DATABASE_WORK_CONTROL_NUMBER30="WORK_CONTROL_NUMBER30";
    public static final String	DATABASE_WORK_CONTROL_NUMBER31="WORK_CONTROL_NUMBER31";
    public static final String	DATABASE_WORK_CONTROL_NUMBER32="WORK_CONTROL_NUMBER32";
    public static final String	DATABASE_WORK_CONTROL_NUMBER33="WORK_CONTROL_NUMBER33";
    public static final String	DATABASE_WORK_CONTROL_NUMBER34="WORK_CONTROL_NUMBER34";
    public static final String	DATABASE_WORK_CONTROL_NUMBER35="WORK_CONTROL_NUMBER35";
    public static final String	DATABASE_WORK_CONTROL_NUMBER36="WORK_CONTROL_NUMBER36";
    public static final String	DATABASE_WORK_CONTROL_NUMBER37="WORK_CONTROL_NUMBER37";
    public static final String	DATABASE_WORK_CONTROL_NUMBER38="WORK_CONTROL_NUMBER38";
    public static final String	DATABASE_WORK_CONTROL_NUMBER39="WORK_CONTROL_NUMBER39";
    public static final String	DATABASE_WORK_CONTROL_NUMBER40="WORK_CONTROL_NUMBER40";
    public static final String	DATABASE_WORK_CONTROL_NUMBER41="WORK_CONTROL_NUMBER41";
    public static final String	DATABASE_WORK_CONTROL_NUMBER42="WORK_CONTROL_NUMBER42";
    public static final String	DATABASE_WORK_CONTROL_NUMBER43="WORK_CONTROL_NUMBER43";
    public static final String	DATABASE_WORK_CONTROL_NUMBER44="WORK_CONTROL_NUMBER44";
    public static final String	DATABASE_WORK_CONTROL_NUMBER45="WORK_CONTROL_NUMBER45";
    public static final String	DATABASE_WORK_CONTROL_NUMBER46="WORK_CONTROL_NUMBER46";
    public static final String	DATABASE_WORK_CONTROL_NUMBER47="WORK_CONTROL_NUMBER47";
    public static final String	DATABASE_WORK_CONTROL_NUMBER48="WORK_CONTROL_NUMBER48";
    public static final String	DATABASE_WORK_CONTROL_NUMBER49="WORK_CONTROL_NUMBER49";
    public static final String	DATABASE_WORK_CONTROL_NUMBER50="WORK_CONTROL_NUMBER50";
    public static final String	DATABASE_WORK_TYPE01="WORK_TYPE01";
    public static final String	DATABASE_WORK_TYPE02="WORK_TYPE02";
    public static final String	DATABASE_WORK_TYPE03="WORK_TYPE03";
    public static final String	DATABASE_WORK_TYPE04="WORK_TYPE04";
    public static final String	DATABASE_WORK_TYPE05="WORK_TYPE05";
    public static final String	DATABASE_WORK_TYPE06="WORK_TYPE06";
    public static final String	DATABASE_WORK_TYPE07="WORK_TYPE07";
    public static final String	DATABASE_WORK_TYPE08="WORK_TYPE08";
    public static final String	DATABASE_WORK_TYPE09="WORK_TYPE09";
    public static final String	DATABASE_WORK_TYPE10="WORK_TYPE10";
    public static final String	DATABASE_WORK_TYPE11="WORK_TYPE11";
    public static final String	DATABASE_WORK_TYPE12="WORK_TYPE12";
    public static final String	DATABASE_WORK_TYPE13="WORK_TYPE13";
    public static final String	DATABASE_WORK_TYPE14="WORK_TYPE14";
    public static final String	DATABASE_WORK_TYPE15="WORK_TYPE15";
    public static final String	DATABASE_WORK_TYPE16="WORK_TYPE16";
    public static final String	DATABASE_WORK_TYPE17="WORK_TYPE17";
    public static final String	DATABASE_WORK_TYPE18="WORK_TYPE18";
    public static final String	DATABASE_WORK_TYPE19="WORK_TYPE19";
    public static final String	DATABASE_WORK_TYPE20="WORK_TYPE20";
    public static final String	DATABASE_WORK_TYPE21="WORK_TYPE21";
    public static final String	DATABASE_WORK_TYPE22="WORK_TYPE22";
    public static final String	DATABASE_WORK_TYPE23="WORK_TYPE23";
    public static final String	DATABASE_WORK_TYPE24="WORK_TYPE24";
    public static final String	DATABASE_WORK_TYPE25="WORK_TYPE25";
    public static final String	DATABASE_WORK_TYPE26="WORK_TYPE26";
    public static final String	DATABASE_WORK_TYPE27="WORK_TYPE27";
    public static final String	DATABASE_WORK_TYPE28="WORK_TYPE28";
    public static final String	DATABASE_WORK_TYPE29="WORK_TYPE29";
    public static final String	DATABASE_WORK_TYPE30="WORK_TYPE30";
    public static final String	DATABASE_WORK_TYPE31="WORK_TYPE31";
    public static final String	DATABASE_WORK_TYPE32="WORK_TYPE32";
    public static final String	DATABASE_WORK_TYPE33="WORK_TYPE33";
    public static final String	DATABASE_WORK_TYPE34="WORK_TYPE34";
    public static final String	DATABASE_WORK_TYPE35="WORK_TYPE35";
    public static final String	DATABASE_WORK_TYPE36="WORK_TYPE36";
    public static final String	DATABASE_WORK_TYPE37="WORK_TYPE37";
    public static final String	DATABASE_WORK_TYPE38="WORK_TYPE38";
    public static final String	DATABASE_WORK_TYPE39="WORK_TYPE39";
    public static final String	DATABASE_WORK_TYPE40="WORK_TYPE40";
    public static final String	DATABASE_WORK_TYPE41="WORK_TYPE41";
    public static final String	DATABASE_WORK_TYPE42="WORK_TYPE42";
    public static final String	DATABASE_WORK_TYPE43="WORK_TYPE43";
    public static final String	DATABASE_WORK_TYPE44="WORK_TYPE44";
    public static final String	DATABASE_WORK_TYPE45="WORK_TYPE45";
    public static final String	DATABASE_WORK_TYPE46="WORK_TYPE46";
    public static final String	DATABASE_WORK_TYPE47="WORK_TYPE47";
    public static final String	DATABASE_WORK_TYPE48="WORK_TYPE48";
    public static final String	DATABASE_WORK_TYPE49="WORK_TYPE49";
    public static final String	DATABASE_WORK_TYPE50="WORK_TYPE50";
    public static final String	DATABASE_LPN01="LPN01";
    public static final String	DATABASE_LPN02="LPN02";
    public static final String	DATABASE_LPN03="LPN03";
    public static final String	DATABASE_LPN04="LPN04";
    public static final String	DATABASE_LPN05="LPN05";
    public static final String	DATABASE_LPN06="LPN06";
    public static final String	DATABASE_LPN07="LPN07";
    public static final String	DATABASE_LPN08="LPN08";
    public static final String	DATABASE_LPN09="LPN09";
    public static final String	DATABASE_LPN10="LPN10";
    public static final String	DATABASE_LPN11="LPN11";
    public static final String	DATABASE_LPN12="LPN12";
    public static final String	DATABASE_LPN13="LPN13";
    public static final String	DATABASE_LPN14="LPN14";
    public static final String	DATABASE_LPN15="LPN15";
    public static final String	DATABASE_LPN16="LPN16";
    public static final String	DATABASE_LPN17="LPN17";
    public static final String	DATABASE_LPN18="LPN18";
    public static final String	DATABASE_LPN19="LPN19";
    public static final String	DATABASE_LPN20="LPN20";
    public static final String	DATABASE_LPN21="LPN21";
    public static final String	DATABASE_LPN22="LPN22";
    public static final String	DATABASE_LPN23="LPN23";
    public static final String	DATABASE_LPN24="LPN24";
    public static final String	DATABASE_LPN25="LPN25";
    public static final String	DATABASE_LPN26="LPN26";
    public static final String	DATABASE_LPN27="LPN27";
    public static final String	DATABASE_LPN28="LPN28";
    public static final String	DATABASE_LPN29="LPN29";
    public static final String	DATABASE_LPN30="LPN30";
    public static final String	DATABASE_LPN31="LPN31";
    public static final String	DATABASE_LPN32="LPN32";
    public static final String	DATABASE_LPN33="LPN33";
    public static final String	DATABASE_LPN34="LPN34";
    public static final String	DATABASE_LPN35="LPN35";
    public static final String	DATABASE_LPN36="LPN36";
    public static final String	DATABASE_LPN37="LPN37";
    public static final String	DATABASE_LPN38="LPN38";
    public static final String	DATABASE_LPN39="LPN39";
    public static final String	DATABASE_LPN40="LPN40";
    public static final String	DATABASE_LPN41="LPN41";
    public static final String	DATABASE_LPN42="LPN42";
    public static final String	DATABASE_LPN43="LPN43";
    public static final String	DATABASE_LPN44="LPN44";
    public static final String	DATABASE_LPN45="LPN45";
    public static final String	DATABASE_LPN46="LPN46";
    public static final String	DATABASE_LPN47="LPN47";
    public static final String	DATABASE_LPN48="LPN48";
    public static final String	DATABASE_LPN49="LPN49";
    public static final String	DATABASE_LPN50="LPN50";
    public static final String	DATABASE_CONTAINER_TYPE01="CONTAINER_TYPE01";
    public static final String	DATABASE_CONTAINER_TYPE02="CONTAINER_TYPE02";
    public static final String	DATABASE_CONTAINER_TYPE03="CONTAINER_TYPE03";
    public static final String	DATABASE_CONTAINER_TYPE04="CONTAINER_TYPE04";
    public static final String	DATABASE_CONTAINER_TYPE05="CONTAINER_TYPE05";
    public static final String	DATABASE_CONTAINER_TYPE06="CONTAINER_TYPE06";
    public static final String	DATABASE_CONTAINER_TYPE07="CONTAINER_TYPE07";
    public static final String	DATABASE_CONTAINER_TYPE08="CONTAINER_TYPE08";
    public static final String	DATABASE_CONTAINER_TYPE09="CONTAINER_TYPE09";
    public static final String	DATABASE_CONTAINER_TYPE10="CONTAINER_TYPE10";
    public static final String	DATABASE_CONTAINER_TYPE11="CONTAINER_TYPE11";
    public static final String	DATABASE_CONTAINER_TYPE12="CONTAINER_TYPE12";
    public static final String	DATABASE_CONTAINER_TYPE13="CONTAINER_TYPE13";
    public static final String	DATABASE_CONTAINER_TYPE14="CONTAINER_TYPE14";
    public static final String	DATABASE_CONTAINER_TYPE15="CONTAINER_TYPE15";
    public static final String	DATABASE_CONTAINER_TYPE16="CONTAINER_TYPE16";
    public static final String	DATABASE_CONTAINER_TYPE17="CONTAINER_TYPE17";
    public static final String	DATABASE_CONTAINER_TYPE18="CONTAINER_TYPE18";
    public static final String	DATABASE_CONTAINER_TYPE19="CONTAINER_TYPE19";
    public static final String	DATABASE_CONTAINER_TYPE20="CONTAINER_TYPE20";
    public static final String	DATABASE_CONTAINER_TYPE21="CONTAINER_TYPE21";
    public static final String	DATABASE_CONTAINER_TYPE22="CONTAINER_TYPE22";
    public static final String	DATABASE_CONTAINER_TYPE23="CONTAINER_TYPE23";
    public static final String	DATABASE_CONTAINER_TYPE24="CONTAINER_TYPE24";
    public static final String	DATABASE_CONTAINER_TYPE25="CONTAINER_TYPE25";
    public static final String	DATABASE_CONTAINER_TYPE26="CONTAINER_TYPE26";
    public static final String	DATABASE_CONTAINER_TYPE27="CONTAINER_TYPE27";
    public static final String	DATABASE_CONTAINER_TYPE28="CONTAINER_TYPE28";
    public static final String	DATABASE_CONTAINER_TYPE29="CONTAINER_TYPE29";
    public static final String	DATABASE_CONTAINER_TYPE30="CONTAINER_TYPE30";
    public static final String	DATABASE_CONTAINER_TYPE31="CONTAINER_TYPE31";
    public static final String	DATABASE_CONTAINER_TYPE32="CONTAINER_TYPE32";
    public static final String	DATABASE_CONTAINER_TYPE33="CONTAINER_TYPE33";
    public static final String	DATABASE_CONTAINER_TYPE34="CONTAINER_TYPE34";
    public static final String	DATABASE_CONTAINER_TYPE35="CONTAINER_TYPE35";
    public static final String	DATABASE_CONTAINER_TYPE36="CONTAINER_TYPE36";
    public static final String	DATABASE_CONTAINER_TYPE37="CONTAINER_TYPE37";
    public static final String	DATABASE_CONTAINER_TYPE38="CONTAINER_TYPE38";
    public static final String	DATABASE_CONTAINER_TYPE39="CONTAINER_TYPE39";
    public static final String	DATABASE_CONTAINER_TYPE40="CONTAINER_TYPE40";
    public static final String	DATABASE_CONTAINER_TYPE41="CONTAINER_TYPE41";
    public static final String	DATABASE_CONTAINER_TYPE42="CONTAINER_TYPE42";
    public static final String	DATABASE_CONTAINER_TYPE43="CONTAINER_TYPE43";
    public static final String	DATABASE_CONTAINER_TYPE44="CONTAINER_TYPE44";
    public static final String	DATABASE_CONTAINER_TYPE45="CONTAINER_TYPE45";
    public static final String	DATABASE_CONTAINER_TYPE46="CONTAINER_TYPE46";
    public static final String	DATABASE_CONTAINER_TYPE47="CONTAINER_TYPE47";
    public static final String	DATABASE_CONTAINER_TYPE48="CONTAINER_TYPE48";
    public static final String	DATABASE_CONTAINER_TYPE49="CONTAINER_TYPE49";
    public static final String	DATABASE_CONTAINER_TYPE50="CONTAINER_TYPE50";
    public static final String	DATABASE_CONTAINER_ID01="CONTAINER_ID01";
    public static final String	DATABASE_CONTAINER_ID02="CONTAINER_ID02";
    public static final String	DATABASE_CONTAINER_ID03="CONTAINER_ID03";
    public static final String	DATABASE_CONTAINER_ID04="CONTAINER_ID04";
    public static final String	DATABASE_CONTAINER_ID05="CONTAINER_ID05";
    public static final String	DATABASE_CONTAINER_ID06="CONTAINER_ID06";
    public static final String	DATABASE_CONTAINER_ID07="CONTAINER_ID07";
    public static final String	DATABASE_CONTAINER_ID08="CONTAINER_ID08";
    public static final String	DATABASE_CONTAINER_ID09="CONTAINER_ID09";
    public static final String	DATABASE_CONTAINER_ID10="CONTAINER_ID10";
    public static final String	DATABASE_CONTAINER_ID11="CONTAINER_ID11";
    public static final String	DATABASE_CONTAINER_ID12="CONTAINER_ID12";
    public static final String	DATABASE_CONTAINER_ID13="CONTAINER_ID13";
    public static final String	DATABASE_CONTAINER_ID14="CONTAINER_ID14";
    public static final String	DATABASE_CONTAINER_ID15="CONTAINER_ID15";
    public static final String	DATABASE_CONTAINER_ID16="CONTAINER_ID16";
    public static final String	DATABASE_CONTAINER_ID17="CONTAINER_ID17";
    public static final String	DATABASE_CONTAINER_ID18="CONTAINER_ID18";
    public static final String	DATABASE_CONTAINER_ID19="CONTAINER_ID19";
    public static final String	DATABASE_CONTAINER_ID20="CONTAINER_ID20";
    public static final String	DATABASE_CONTAINER_ID21="CONTAINER_ID21";
    public static final String	DATABASE_CONTAINER_ID22="CONTAINER_ID22";
    public static final String	DATABASE_CONTAINER_ID23="CONTAINER_ID23";
    public static final String	DATABASE_CONTAINER_ID24="CONTAINER_ID24";
    public static final String	DATABASE_CONTAINER_ID25="CONTAINER_ID25";
    public static final String	DATABASE_CONTAINER_ID26="CONTAINER_ID26";
    public static final String	DATABASE_CONTAINER_ID27="CONTAINER_ID27";
    public static final String	DATABASE_CONTAINER_ID28="CONTAINER_ID28";
    public static final String	DATABASE_CONTAINER_ID29="CONTAINER_ID29";
    public static final String	DATABASE_CONTAINER_ID30="CONTAINER_ID30";
    public static final String	DATABASE_CONTAINER_ID31="CONTAINER_ID31";
    public static final String	DATABASE_CONTAINER_ID32="CONTAINER_ID32";
    public static final String	DATABASE_CONTAINER_ID33="CONTAINER_ID33";
    public static final String	DATABASE_CONTAINER_ID34="CONTAINER_ID34";
    public static final String	DATABASE_CONTAINER_ID35="CONTAINER_ID35";
    public static final String	DATABASE_CONTAINER_ID36="CONTAINER_ID36";
    public static final String	DATABASE_CONTAINER_ID37="CONTAINER_ID37";
    public static final String	DATABASE_CONTAINER_ID38="CONTAINER_ID38";
    public static final String	DATABASE_CONTAINER_ID39="CONTAINER_ID39";
    public static final String	DATABASE_CONTAINER_ID40="CONTAINER_ID40";
    public static final String	DATABASE_CONTAINER_ID41="CONTAINER_ID41";
    public static final String	DATABASE_CONTAINER_ID42="CONTAINER_ID42";
    public static final String	DATABASE_CONTAINER_ID43="CONTAINER_ID43";
    public static final String	DATABASE_CONTAINER_ID44="CONTAINER_ID44";
    public static final String	DATABASE_CONTAINER_ID45="CONTAINER_ID45";
    public static final String	DATABASE_CONTAINER_ID46="CONTAINER_ID46";
    public static final String	DATABASE_CONTAINER_ID47="CONTAINER_ID47";
    public static final String	DATABASE_CONTAINER_ID48="CONTAINER_ID48";
    public static final String	DATABASE_CONTAINER_ID49="CONTAINER_ID49";
    public static final String	DATABASE_CONTAINER_ID50="CONTAINER_ID50";
    public static final String	DATABASE_CART_LOCATION_STATUS01="CART_LOCATION_STATUS01";
    public static final String	DATABASE_CART_LOCATION_STATUS02="CART_LOCATION_STATUS02";
    public static final String	DATABASE_CART_LOCATION_STATUS03="CART_LOCATION_STATUS03";
    public static final String	DATABASE_CART_LOCATION_STATUS04="CART_LOCATION_STATUS04";
    public static final String	DATABASE_CART_LOCATION_STATUS05="CART_LOCATION_STATUS05";
    public static final String	DATABASE_CART_LOCATION_STATUS06="CART_LOCATION_STATUS06";
    public static final String	DATABASE_CART_LOCATION_STATUS07="CART_LOCATION_STATUS07";
    public static final String	DATABASE_CART_LOCATION_STATUS08="CART_LOCATION_STATUS08";
    public static final String	DATABASE_CART_LOCATION_STATUS09="CART_LOCATION_STATUS09";
    public static final String	DATABASE_CART_LOCATION_STATUS10="CART_LOCATION_STATUS10";
    public static final String	DATABASE_CART_LOCATION_STATUS11="CART_LOCATION_STATUS11";
    public static final String	DATABASE_CART_LOCATION_STATUS12="CART_LOCATION_STATUS12";
    public static final String	DATABASE_CART_LOCATION_STATUS13="CART_LOCATION_STATUS13";
    public static final String	DATABASE_CART_LOCATION_STATUS14="CART_LOCATION_STATUS14";
    public static final String	DATABASE_CART_LOCATION_STATUS15="CART_LOCATION_STATUS15";
    public static final String	DATABASE_CART_LOCATION_STATUS16="CART_LOCATION_STATUS16";
    public static final String	DATABASE_CART_LOCATION_STATUS17="CART_LOCATION_STATUS17";
    public static final String	DATABASE_CART_LOCATION_STATUS18="CART_LOCATION_STATUS18";
    public static final String	DATABASE_CART_LOCATION_STATUS19="CART_LOCATION_STATUS19";
    public static final String	DATABASE_CART_LOCATION_STATUS20="CART_LOCATION_STATUS20";
    public static final String	DATABASE_CART_LOCATION_STATUS21="CART_LOCATION_STATUS21";
    public static final String	DATABASE_CART_LOCATION_STATUS22="CART_LOCATION_STATUS22";
    public static final String	DATABASE_CART_LOCATION_STATUS23="CART_LOCATION_STATUS23";
    public static final String	DATABASE_CART_LOCATION_STATUS24="CART_LOCATION_STATUS24";
    public static final String	DATABASE_CART_LOCATION_STATUS25="CART_LOCATION_STATUS25";
    public static final String	DATABASE_CART_LOCATION_STATUS26="CART_LOCATION_STATUS26";
    public static final String	DATABASE_CART_LOCATION_STATUS27="CART_LOCATION_STATUS27";
    public static final String	DATABASE_CART_LOCATION_STATUS28="CART_LOCATION_STATUS28";
    public static final String	DATABASE_CART_LOCATION_STATUS29="CART_LOCATION_STATUS29";
    public static final String	DATABASE_CART_LOCATION_STATUS30="CART_LOCATION_STATUS30";
    public static final String	DATABASE_CART_LOCATION_STATUS31="CART_LOCATION_STATUS31";
    public static final String	DATABASE_CART_LOCATION_STATUS32="CART_LOCATION_STATUS32";
    public static final String	DATABASE_CART_LOCATION_STATUS33="CART_LOCATION_STATUS33";
    public static final String	DATABASE_CART_LOCATION_STATUS34="CART_LOCATION_STATUS34";
    public static final String	DATABASE_CART_LOCATION_STATUS35="CART_LOCATION_STATUS35";
    public static final String	DATABASE_CART_LOCATION_STATUS36="CART_LOCATION_STATUS36";
    public static final String	DATABASE_CART_LOCATION_STATUS37="CART_LOCATION_STATUS37";
    public static final String	DATABASE_CART_LOCATION_STATUS38="CART_LOCATION_STATUS38";
    public static final String	DATABASE_CART_LOCATION_STATUS39="CART_LOCATION_STATUS39";
    public static final String	DATABASE_CART_LOCATION_STATUS40="CART_LOCATION_STATUS40";
    public static final String	DATABASE_CART_LOCATION_STATUS41="CART_LOCATION_STATUS41";
    public static final String	DATABASE_CART_LOCATION_STATUS42="CART_LOCATION_STATUS42";
    public static final String	DATABASE_CART_LOCATION_STATUS43="CART_LOCATION_STATUS43";
    public static final String	DATABASE_CART_LOCATION_STATUS44="CART_LOCATION_STATUS44";
    public static final String	DATABASE_CART_LOCATION_STATUS45="CART_LOCATION_STATUS45";
    public static final String	DATABASE_CART_LOCATION_STATUS46="CART_LOCATION_STATUS46";
    public static final String	DATABASE_CART_LOCATION_STATUS47="CART_LOCATION_STATUS47";
    public static final String	DATABASE_CART_LOCATION_STATUS48="CART_LOCATION_STATUS48";
    public static final String	DATABASE_CART_LOCATION_STATUS49="CART_LOCATION_STATUS49";
    public static final String	DATABASE_CART_LOCATION_STATUS50="CART_LOCATION_STATUS50";
    public static final String	DATABASE_LAST_ACTIVITY_DATE="LAST_ACTIVITY_DATE";
    public static final String	DATABASE_LAST_ACTIVITY_TEAM_MEMBER="LAST_ACTIVITY_TEAM_MEMBER";
    public static final String	DATABASE_STATUS="STATUS";
    public static final String	DATABASE_TABLENAME_CARTS="CARTS";
    
    //Create the pojo according to new requirements
    public static final String DATABASE_TABLENAME_PUTWALLS="PUTWALLS";
    public static final String DATABASE_PUTWALL_ID="PUTWALL_ID";
    public static final String DATABASE_SINGLE_MULTI_SIDED="SINGLE_MULTI_SIDED";
    public static final String DATABASE_PUTWALL_TYPE="PUTWALL_TYPE";
    
    public static final String DATABASE_TABLENAME_PUTWALL_CUBES="PUTWALL_CUBES";
    public static final String DATABASE_PUTWALL_CUBE_ID="PUTWALL_CUBE_ID";
    public static final String DATABASE_PRIORITY ="PRIORITY";
    public static final String DATABASE_LEVEL ="LEVEL";
    public static final String DATABASE_CUBE_SLOT ="CUBE_SLOT";
    public static final String DATABASE_FRONT_BACK ="FRONT_BACK";
    public static final String DATABASE_CONTAINER_TYPE ="CONTAINER_TYPE";
    public static final String DATABASE_CONTAINER_ID ="CONTAINER_ID";
    public static final String DATABASE_RELATIVE_CUBE_ID ="RELATIVE_CUBE_ID";
    public static final String DATABASE_ALTERNATE_CUBE_ID ="ALTERNATE_CUBE_ID";
    public static final String DATABASE_WORK_CONTROL_NUMBER ="WORK_CONTROL_NUMBER";
    public static final String DATABASE_WORK_TYPE ="WORK_TYPE";
    public static final String DATABASE_LPN ="LPN";
    public static final String DATABASE_ORDER_ID ="ORDER_ID";
    public static final String DATABASE_STORE_NUMBER ="STORE_NUMBER";
    public static final String DATABASE_TYPE ="TYPE";
    public static final String DATABASE_PUT_UNITS_REQUIRED ="PUT_UNITS_REQUIRED";
    public static final String DATABASE_PUT_UNITS_PUT ="PUT_UNITS_PUT";
    public static final String DATABASE_CUT_UNITS ="CUT_UNITS";
    public static final String DATABASE_DATE_TIME_ASSIGNED ="DATE_TIME_ASSIGNED";
    public static final String DATABASE_FIRST_PUT_TIME ="FIRST_PUT_TIME";
    public static final String DATABASE_LAST_PUT_TIME ="LAST_PUT_TIME";
    public static final String DATABASE_PULL_TIME ="PULL_TIME";
    public static final String DATABASE_PUSH_TIME ="PUSH_TIME";
    public static final String DATABASE_LAST_ACTIVITY_TASK="LAST_ACTIVITY_TASK";
    public static final String DATABASE_LAST_DIVERT="LAST_DIVERT";
    public static final String DATABASE_PUT_WALL_ID="PUT_WALL_ID";
    public static final String DATABASE_PUT_WALL_CUBE_ID="PUT_WALL_CUBE_ID";
    public static final String DATABASE_TABLENAME_LPNS="LPNS";
    public static final String DATABASE_HEIGHT="HEIGHT";
    public static final String DATABASE_WIDTH="WIDTH";
    public static final String DATABASE_LENGTH="LENGTH";
    public static final String DATABASE_STORAGE_CUBE="STORAGE_CUBE";
    public static final String DATABASE_STORAGE_CUBE_FACTOR="STORAGE_CUBE_FACTOR";
    public static final String DATABASE_STORAGE_CUBE_NET="STORAGE_CUBE_NET";
    public static final String DATABASE_WEIGHT="WEIGHT";
    public static final String DATABASE_MAXIUM_WEIGHT="MAXIUM_WEIGHT";
    public static final String DATABASE_MAXIUM_QTY="MAXIUM_QTY";
    public static final String DATABASE_CONTAINER_USE_CODE="CONTAINER_USE_CODE";
    public static final String DATABASE_CONTAINER_MATERIAL_CODE="CONTAINER_MATERIAL_CODE";
    public static final String DATABASE_TABLENAME_CONTAINERS="CONTAINERS";
    
    //FOR UPDATE EXISTING POJOS
 //   public static final String DATABASE_PARAMETERS_SYSTEM_KEY="PARAMETERS_SYSTEM_KEY";
    public static final String DATABASE_COMPARISON_OPERATOR="COMPARISON_OPERATOR";
    public static final String DATABASE_COMPARISON_TYPE="COMPARISON_TYPE";
    public static final String DATABASE_SCREEN_SECTION_ID="SCREEN_SECTION_ID";
    public static final String DATABASE_UOM="UOM";
    public static final String DATABASE_UOW="UOW";
    public static final String DATABASE_WAVE_ID="WAVE_ID";
    public static final String SEC_ODON="SEC_ODON";
    public static final String NOTES_SEQ="NOTES_SEQ";
    public static final String NOTES_INSERT_QUERY="Insert into NOTES (TENANT_ID,COMPANY_ID,NOTE_ID,NOTE_LINK,NOTE,CONTINUATIONS_SEQUENCE,LAST_ACTIVITY_DATE,LAST_ACTIVITY_TEAM_MEMBER) values (?,?,?,?,?,?,?,?)";
    
    
    public static final String SERVERIP = "http://192.168.1.79:8181";
    public static final String WEBSERVERNAME = "/JASCI_GlassWebservice";
    public static final String STRWEBSERVER=SERVERIP+WEBSERVERNAME;
    public static final String NextGlassWebServiceURL =SERVERIP+WEBSERVERNAME+"/processEngineExecutionGlass";
    public static final String StartGlassWebServiceURL =SERVERIP+WEBSERVERNAME+"/GlassEngine";

    public static final String TEXT_STYLE_CLASS_RED="text-style-class-red";
    public static final String TEXT_STYLE_CLASS_GREEN="text-style-class-green";
    public static final String TEXT_STYLE_CLASS_BLACK="text-style-class-black";
    public static final String TEXT_STYLE_CLASS_WHITE="text-style-class-white";
    public static final String BG_COLOR_CODE="#32cd32";
    public static String WORKING = "Working";
    public static final String Accept ="Accept";
    public static final String unchecked ="unchecked";
    public static final String Next_Execution_is  ="Next Execution is ";
    public static final String Failed_to_get_the_data_the_next_data_or_unable_to_parse_the_response_into_shared_data="Failed to get the data the next data or unable to parse the response into shared data";
    public static final String Unable_to_encode_the_data="Unable to encode the data";
    public static final String AMPERSANDFIRSTEXECUTIONNAME = "&executionName=";
    //[]&IsCompleted=false

    public static String URL_WORK_TYPE_STATUS = "/updateWorkTypeHeaderStatus?input=";

    public static String WORK_HISTORY_CODE_ASSIGNED="ASSIGNED";
    public static String TEAM_MEMBER = "TeamMember";
    public static String DATE = "Date";

    public static String ZONE = "ZONE";
    public static String DIVERT = "DIVERT";
    public static String WORKTYPE = "WorkType";
    public static final String WORK_FLOW_ID="WorkFlowId";
    public static final String WORK_FLOW_STEPS="WorkFlowSteps";
    public static String WORK_HISTORY_CODE = "WorkHistoryCode";
    public static String WORK_HISTORY_CODE_VALUE = "WORKED";
    public static String URL_INSERT_WORK_HISTORY = "/insertIntoWorkHistory?input=";
    public static String URL_GET_WORK_TYPE_LINES = "/getWorkTypeLinesJson?input=";
    public static final String URL_GETDATA_WORK_TYPE="/getWorkTypeson?input=";
    public static final String URL_GETDATA_AREA_LOCATION="/getAreaLocationJson?input=";
    public static String HISTORY_STATUS = "";
    public static final String STATUS_LPN="Status";

    public static final String EXECUTION_HISTORY="{\"EXCECUTIONHISTORY\":[]}";
    public static final String EXECUTIONHISTORY="executionHistory";
    public static final String STRY="Y";

    public static final String AMPERSANDCOMPLETEDTRUE="&IsCompleted=true";
    
  //for Execution 
    public static final String EXECUTION_URL_DISPLAYPRODUCTLOCATION="/DisplayProductLocation";
    public static final String EXECUTION_URL_DISPLAYWORKINFORMATIONSTART="/DisplayWorkInformationStart";
    public static final String REQUESTMAPPINGSCANLPN = "/ScanLPN";
    public static final String MODELVIEWNAMESCANLPN = "/ScanLPN";

    public static final String REQUESTMAPPINGSCANLOCATION = "/ScanLocation";
    public static final String MODELVIEWNAMESCANLOCATION = "/ScanLocation";

    public static final String REQUESTMAPPINGSCANWORKZONE = "/ScanWorkZone";
    public static final String MODELVIEWNAMESCANWORKZONE = "/ScanWorkZone";

    public static  String MAPPEDTEAMMEMBERNAME = "TeamMemberName";
    public static  String DISPLAYAREA = "DisplayArea";
    public static String BUTTONAREA = "ButtonsArea";
    public static String IMAGEAREA ="ImageArea";
    public static final String EXECUTIONLABELS = "ExecutionLabels";

    public static final String URL_CHECK_LPN="/checkValidLpn";

    public static final String TENANT_SMALL = "Tenant";
    public static final String FULFILLMENT_CENTER = "FulfillmentCenter";
    public static final String COMPANY_SMALL = "Company";	
    public static final String LPN_SCAN = "LPN";
    public static final String LPN_TASK= "Task";
    public static final String LPN_WORK_TYPE= "WorkType";
    public static String URL_GET_WORK_BY_LPN = "/getWorkByLPNJson?input=";

    //
    public static final String NEXT_EXECUTION="/NextExecution";
    public static final String EXECUTION_NAME="executionName";
    public static final String STR_JSON="strJSON";
    public static final String ISCOMPLETED="IsCompleted";
    public static final String SUCESS="sucess";
    public static final String VISUAL="VISUAL";
    public static final String AMPERSANDEXECUTIONNAME="&&executionName=";
    public static final String AMPERSANDCOMPLETED="&IsCompleted=false";
    public static final String ENGINESTART="EngineStart";
    public static final String MENUNAME="MenuName";
    public static final String MENUNAMEKEY="menuName";
    public static final String TENANTKEY="tenant";
    public static final String COMPANYKEY="company";
    public static final String TENENANTMEMBERKEY="teamMember";

    public static String GET_WORK_TYPE_LPN= "GetWorkByLPNJson";
    public static String  FULFILLMENT_CENTER_ID="FulFillment_Center_Id";
    public static String  WORK_CONTROL_NUMBER="Work_Control_Number";
    public static String WORK_CONTROLNUMBER = "WorkControlNumber";
    public static String TENANT_ID  ="Tenant_Id";
    public static String COMPANY_ID = "Company_Id";
    public static String PRODUCT_DESC ="Product";
    public static String QUALITY_DESC="Quality";

    public static String  WORK_TYPE="Work_Type";
    public static String  WORK_TYPE_TASK="Task";
    public static String  WORK_FLOW_ID_WITH_UNDERSCORE="Work_Flow_Id";
    public static String  WORK_FLOW_STEP_WITH_UNDERSCORE="Work_Flow_Step";
    public static String  WORK_GROUP_ZONE_WITH_UNDERSCORE="Work_Group_Zone";
    public static String  WORK_ZONE="Work_Zone";
    public static String  PRIORITY_CODE="Priority_Code";
    public static String  PRIORITY="Priority";
    public static String  SALES_ORDER_ID="Sales_Order_Id";
    public static String PURCHASE_ORDER_NUMBER="Purchase_order_number";
    public static String ORDER_ID="Order_Id";
    public static String  ACCOUNTATION_CODE="Accountion_Code";
    public static String  CREATED_BY="Created_By";
    public static String  DATE_CREATED="Date_Created";
    public static String  LAST_ACTIVITY_TEAM_MEMBER="Last_Activity_Team_Member";

    public static String  LAST_ACTIVITY_TASK="Last_Activity_Task";
    
    public static String LPN = "LPN";
    public static String ASN = "ASN";
    public static final String url_inventoryLevelPicDetail="inventoryLevelPicDetail";
    public static String InventoryLevelPictureDetail ="InventoryLevelPictureDetail";
    public static final String PRODUCT="PRODUCT";
    public static final String DESCRIPTION="DESCRIPTION";
    public static final String QUALITY="QUALITY";
    public static final String SCAN_LPN="SCAN LPN";
    public static final String LABELS_DATA="LABELS_DATA";
    public static final String RequestMapping_LocationPictureRightBar="/Location_Picture_Right_Bar";
    public static final String RequestMapping_LocationRightBar="/Location_Right_Bar";

    public static final String Display_Product_In_Location="/Display_Product_In_Location";
    public static final String DisplayproductLocation="DisplayproductLocation";
    public static final String url_displayproductLocation="displayproductLocation";
    public static final String RequestMapping_InventoryLocationSearch="/Inventory_Location_Search";
    public static final String display_inventory_level="/display_area_location";
    public static final String url_inventoryLevel="inventoryLevel";
    public static final String url_inventoryLevelDetail="inventoryLevelDetail";
    public static final String EXECUTIONName="executionName";
    public static final String EXECUTIONNameList="executionNameList";

    public static final String STRGLASS_STE_ODG_SHOW_Company="GLASS_STE_ODG_SHOW_Company";
    public static final String STRGLASS_STE_ODG_SHOW_FulfillmentCenter="GLASS_STE_ODG_SHOW_FulfillmentCenter";
    public static final String STRPATH_GLASS_STE_ODG_SHOW_Name="GLASS_STE_ODG_SHOW_Name";
    public static final String APPLICATION_JSON="application/json";
    public static final String ACCEPT="Accept";
    public static final String NEXT_LINE="\n";
    public static final String PRODUCT_VALUE="productValue";
    public static final String SERVICE_OUTPUT="SERVICEAPI |DATA SIze=";
    public static final String SERVICE_OUTPUT_DETAIL="SERVICEAPI Detail |DATA SIze=";
    public static final String SHARED_OBJECT_JSON="SharedObjJson";
    public static final String SHARED_OBJECT="SharedObject";
    public static final String EXECUTIONBE="ExecutionBe";
    public static final String IsCompleted="IsCompleted";
    public static final String TOP_BAR_TEXT_LIST="TOP_BAR_TEXT_LIST";
    public static final String VALUESTATUS_CAP="STATUS";
    public static String STATUS_FAILURE = "failure";
    public static String SUCCESS = "Success";
    
    public static final  String BLANKSPACESTRING="";
    public static  boolean ISDEBUG=true;
    public static final String STRENGINESTARTURL="GlassEngine?strJSON=";
    public final static String METHODTYPE_GET="GET";
    public final static String METHODTYPE_POST="POST";
    public static final String USER_AGENT="User-Agent";
    public static final String MOZILLA="Mozilla/5.0";
    public static final String SQUREOPENBRACKET="[";
    public static final String SQURECLOSEBRACKET="]";	
    public static final Integer INT_NUMBER_ZERO=0;
    public static final String HTTP="HTTP";
    public static final String ERROR_IN_HTTP="Error in http connection : "; 
    public static String UTFFORMATE="UTF-8";
    public static int EMPTY=-1;
    public static final String STRJSON="strJSON";
    public static final String STRJARPATH="strJARPATH";	
    public static  String JARPATHVALUE="";
    
    public static String VALUE_ID ="Id";
    public static String SINGLE_SPACE=" ";
    public final  static String SYMBOL_PRCNT_20="%20";
    public static final String LANGUAGE="Language";

    public static final String GETSCREENLABELS="/GetScreenLabelTranslations?input=";

    public static final String KEYPHASE="KeyPhase";
    public static final String MODULENAME="ModuleName";
    
    public static final String GETLANGUAGELABELS="GetLanguageLavels";
    public static final String KEYPHRASE="KeyPhrase";
    public static final String TRANSLATION="Translation";
    public static final String MODULENAMEGLASS="Glass";
    public static final String LANGUAGEHASHMAP="LanguageHashMap";
    public static final String REQUESTMAPPING_REST_GETDATA_INVENTORY_LEVELS="/getInventoryLevelsJson?input=";
    public static final String GETINVENTORYLEVELS="GetInventoryLevels";	
    public static final String LOT="Lot";
    public static final String SERIAL="Serial";
    public static final String Tenant_Id="TENANT_ID";
    public static final String Company_Id="COMPANY_ID";
    public static SHAREDDATA OBJECT_SHAREDDATE=null;
    
    
//  ################################################################

//public static final String SmartTaskExecution_ModelAttribute="ObjSmartTaskExecution";
//public static final String SmartTaskExecutions_Combobox_SelectApplication="SelectApplication";
//public static final String SmartTaskExecutions_Combobox_SelectExecutionType="SelectExecutionType";
//public static final String SmartTaskExecutions_Combobox_SelectExecutionDevice="SelectExecutionDevice";
//public static final String SmartTaskExecutions_Combobox_SelectExecutionGroup="SelectExecutionGroup";
public static final String SmartTaskExcutions_Combobox_SelectTask="SelectTask";

//public static final String GIC_EXEGRP="EXEGRP";
//public static final String GIC_EXETYPE="EXETYPE";
//public static final String GIC_EXEDEVICE="EXEDEVICE";

//public static final String GIC_EXEGRP="EXEGRP";
//public static final String GIC_EXETYPE="EXETYPE";
//public static final String GIC_EXEDEVICE="EXEDEVICE";

//public static final String GIC_APPLICATIONS="APPLICATIONS";
//public static final String GIC_GENERALCODES="GENERALCODES";
//public static final String GIC_INFOHELP="INFOHELP";
//public static final String GIC_LANGUAGES="LANGUAGES";

//public static final String GIC_APPLICATIONSUBS="APPLICATIONSUBS";

//public static final String GIC_TASK="TASK";
//public static final String GIC_AREAS="AREAS";
//public static final String GIC_LOCTYPE="LOCTYPE";
//public static final String GIC_WIZARDS="WIZARDS";
//public static final String GIC_GRPWRKZONE="GRPWRKZONE";
//public static final String GIC_WORKZONES="WORKZONES";
//public static final String GIC_STRGTYPES="STRGTYPES";

//public static final String GIC_LOCPFGRP="LOCPFGRP";
//public static final String GIC_MENUTYPES="MENUTYPES";
/*public static final String LOCATIONTYPECODE="LOCTYPE";
public static final String STORAGETYPECODE="STRGTYPES";*/

//public static final String GIC_THEMERF = "THEMERF";
//public static final String GIC_COUNTRYCODE="COUNTRYCODE";
//public static final String GIC_STATECODE="STATECODE";
//public static final String GIC_THEMEMOBILE="THEMEMOBILE";
//public static final String GIC_THEMEFULLDISPLAY = "THEMEFULLDISPLAY";

//public static final String GIC_LOCLABEL="LOCLABEL";
//public static final String GIC_SECURITYQUESTION="SECURITYQUESTION";
//public static final String GIC_DEPARTMENT="DEPARTMENT";
//public static final String GIC_SHIFTCODE="SHIFTCODE";

//public static final String ScreenLabels="ScreenLabels";
public static final String SMARTTASKEXECUTIONSBE="SMARTTASKEXECUTIONSBE";
	
public static final String SmartTaskConfiguratorBe="SmartTaskConfiguratorBe";
	
//	public static final String SmartTaskExecutions_Restfull_Search="/SmartTaskExecutions_Restfull_Search";
//	public static final String SmartTaskExecutions_Restfull_DisplayAll="/SmartTaskExecutions_Restfull_DisplayAll";
//	public static final String WebServiceStatus_SmartTaskExecutions_lookup="SmartTaskExecutions_lookup";
//	public static final String YES="YES";
//	public static final String NO="NO";
//	public static final String JASCI="JASCI";
//	public static final String SmartTaskExecutions_Restfull_Delete="/SmartTaskExecutions_Restfull_Delete";
	
	//comment out by Toe-On 10/12/2015
	// public static final String SmartTaskExecutions_Delete="/SmartTaskExecutions_Delete";
	
	
//	public static final String SmartTaskExecutions_Restfull_FetchExecution="/SmartTaskExecutions_Restfull_FetchExecution";
	

	
	public static final String RequestMapping_Smart_Task_Executions_Search_Lookup="/Smart_Task_Executions_Search_Lookup";
//	public static final String SmartTaskExecutions_Restfull_ExecutionNameExist="/Rest_ExecutionName_ExistForDelete";
//	public static final String RequestMapping_SmartTaskExecutions_Maintenance_New="/SmartTaskExecutions_Maintenance_New";
//	public static final String SmartTaskExecutions_Maintenance_Update="SmartTaskExecutions_Maintenance_Update";

//	public static final String SmartTaskExecution_Restfull_GetTeamMember="/SmartTaskExecution_Restfull_GetTeamMember";
//	public static final String SmartTaskExecutions_Restfull_RestExecutionsAdd="/SmartTaskExecutions_Restfull_RestExecutionsAdd";
//	public static final String SmartTaskExecutions_Restfull_CheckUniqueExecutionName="/CheckUniqueExecutionName";

//	public static final String SmartTaskExecution_Restfull_Tenant = "Tenant";
//	public static final String SmartTaskExecution_Restfull_Application = "Application";
//	public static final String SmartTaskExecution_Restfull_ExecutionGroup = "ExecutionGroup";
//	public static final String SmartTaskExecution_Restfull_ExecutionType = "ExecutionType";
	public static final String SmartTaskExecution_Restfull_ExecutionDevice = "ExecutionDevice";

//	public static final String SmartTaskExecution_Restfull_Execution = "Execution";
//	public static final String SmartTaskExecution_Restfull_Description = "Description";
//	public static final String SmartTaskExecution_Restfull_Buttons = "Buttons";
	
	
	public static final String SmartTaskExecution_Application_Id      = "Application_Id";
	public static final String SmartTaskExecution_Execution_Group  = "Execution_Group";
	public static final String SmartTaskExecution_Execution_Type    = "Execution_Type";
	public static final String SmartTaskExecution_Execution_Device = "Execution_Device";
	public static final String SmartTaskExecution_Execution_Name  = "Execution_Name";
	public static final String SmartTaskExecution_Execution_Path    = "Execution_Path";
	public static final String SmartTaskExecution_Description20       = "Description20";
	public static final String SmartTaskExecution_Description50 = "Description50";
	public static final String SmartTaskExecution_Tenant_Id = "Tenant_Id";
	public static final String SmartTaskExecution_Button = "Button";
	public static final String SmartTaskExecution_Button_Name = "Button_Name";
	public static final String SmartTaskExecution_Help_Line = "Help_Line";
	public static final String SmartTaskExecution_Last_Activity_Date = "Last_Activity_Date";
	public static final String SmartTaskExecution_Last_Activity_Team_Member = "Last_Activity_Team_Member";
				
	
			
	
	//-----------------------------------------------------------------------------------------------------------------------
	//				Smart Task Executions RequestMapping
	//-----------------------------------------------------------------------------------------------------------------------
	public static final String RequestMapping_SmartTaskExecutions_AddEntry="/SmartTaskExecutions_Add";
	public static final String RequestMapping_SmartTaskExecutions_Add="/SmartTaskExecutions_Add";
//	public static final String RequestMapping_SmartTaskExecutions_Edit="/SmartTaskExecutions_Edit";
	public static final String RequestMapping_SmartTaskExecutions_Kendo_DisplayAll="/SmartTaskExecutions_Kendo_DisplayAll";
	public static final String RequestMapping_SmartTaskExecutions_Maintenance="/SmartTaskExecutions_Maintenance";
	public static final String RequestMapping_SmartTaskExecutions_Maintenance_Add="/SmartTaskExecutions_Maintenance_Add";
	public static final String RequestMapping_SmartTaskExecutions_ParametersSearch_Lookup="/SmartTaskExecutions_ParametersSearch_Lookup";
	public static final String RequestMapping_SmartTaskExecution_Search="/SmartTaskExecutions_Lookup";
	public static final String RequestMapping_SmartTaskExecutions_Search_Lookup="/SmartTaskExecutions_SearchLookup";
//	public static final String RequestMapping_SmartTaskExecutions_Update="/RequestMapping_SmartTaskExecutions_Update";
	
	
	
	
	
	///-----------------------------------------------------------------------------------------------------------------------
	//				Smart Task Executions JSP Page Value
	///-----------------------------------------------------------------------------------------------------------------------
	public static final String SmartTaskExecution_Application_Id_JspPageValue="APPLICATION_ID";
	public static final String SmartTaskExecution_Button_JspPageValue="BUTTONS_ID";
	public static final String SmartTaskExecution_Description20_JspPageValue="DESCRIPTION20";
	public static final String SmartTaskExecution_Description50_JspPageValue="Description50";
	
	public static final String SmartTaskExecution_Execution_Device_JspPageValue="EXECUTION_DEVICE";
	public static final String SmartTaskExecution_Execution_Group_JspPageValue="EXECUTION_SEQUENCE_GROUP";
	public static final String SmartTaskExecution_Execution_Name_JspPageValue="EXECUTION_NAME";
	public static final String SmartTaskExecution_Execution_Name_Hidden_JspPageValue="Execution_NameH";
	public static final String SmartTaskExecution_ExecutionPath_JspPageValue="Execution";
	public static final String SmartTaskExecution_Execution_Type_JspPageValue="EXECUTION_TYPE";
	public static final String SmartTaskExecution_HelpLine_JspPageValue="Help_Line";
	public static final String SmartTaskExecution_Tenant_Id_JspPageValue="TENANT_ID";
	


	
//	public static final String Strfinally="finally";
//	public static final String Strunchecked="unchecked";
//	public static final String Strunused="unused";
//	public static final String Strrawtypes="rawtypes";
//	public static final String SmartTaskExecutions_QueryNameSelectExecutionNameAssign="QueryNameSelectExecutionNameAssign";
	//public static final String SmartTaskExecutions_QuerySelectExecutionNameAssign="select * from SMART_TASK_SEQ_INSTRUCTIONS where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_NAME)=?";
//	public static final String SmartTaskExecutions_QuerySelectExecutionNameAssign="from SMARTTASKSEQINSTRUCTIONS where upper(Tenant_Id)=? and upper(Company_Id)=? and upper(Execution_Name)=?";
	
//	public static final String SmartTaskExecutions_QueryNameFetchExecution="QueryNameFetchExecution";
//	public static final String SmartTaskExecutions_QueryFetchExecution=" from SMART_TASK_EXECUTIONS where upper(TENANT_ID)=? and upper(COMPANY_ID)=? and upper(EXECUTION_NAME)=?";
//	public static final String SmartTaskExecution_Query_Application="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
//			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
//			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
//			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
//			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
//			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
//			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
//			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
//			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
//			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(APPLICATION_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";

	/*
	public static final String SmartTaskExecution_Query_Execution_Group="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_GROUP)=? ORDER BY LAST_ACTIVITY_DATE DESC";
*/
	
	/*
	public static final String SmartTaskExecution_Query_Execution_Type="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_TYPE)=? ORDER BY LAST_ACTIVITY_DATE DESC";
*/
	
	
	/*
	public static final String SmartTaskExecution_Query_Execution_Device="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_DEVICE)=? ORDER BY LAST_ACTIVITY_DATE DESC";

*/
	/*
	public static final String SmartTaskExecution_Query_Execution_Button="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(BUTTON)=? ORDER BY LAST_ACTIVITY_DATE DESC";
*/
	
	/*
	public static final String SmartTaskExecution_Query_Execution_Description="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON, APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and (UPPER(DESCRIPTION20) like ? or UPPER(DESCRIPTION50) like ?) ORDER BY LAST_ACTIVITY_DATE DESC";

*/
	
	/*
	public static final String SmartTaskExecution_Query_Execution_Tenant="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID) = ? ORDER BY LAST_ACTIVITY_DATE DESC";

*/
	
	/*
	public static final String SmartTaskExecution_Query_Execution_Name="Select TENANT_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON, APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ " ,(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(EXECUTION_NAME)=? ORDER BY LAST_ACTIVITY_DATE DESC";

*/
	//modified by Toe-On 10/12/2015
	public static final String SmartTaskExecution_Query_DisplayAll="Select TENANT_ID, COMPANY_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON, APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ORDER BY LAST_ACTIVITY_DATE DESC";
	
	public static final String SmartTaskExecution_Query_DisplayAll_Tenant="Select TENANT_ID, COMPANY_ID,EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
			+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
			+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
			+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON, APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
			+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
			+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
			+ "as Note from SMART_TASK_EXECUTIONS  ORDER BY LAST_ACTIVITY_DATE DESC";

	public static final String CURRENTLANGUAGE="ENG";
	public static final String DATABASE_PARAMETERS_SYSTEM_KEY="PARAMETERS_SYSTEM_KEY";
	
	public static final int INT_ONE = 1;
	public static final String SmartTaskExecutions_QuerySelectExecution = "  from SMARTTASKEXECUTIONS where TENANT_ID=? and COMPANY_ID=?  and EXECUTION_NAME=?";

	public static final String SmartTaskExecutions_Restfull_IsExist="/SmartTaskExecutions_Restfull_IsExist";
	public static final String SmartTaskExecution_Company_Id = "Company_Id";
	public static final String RequestMapping_SmartTaskExecutions_Delete="/SmartTaskExecutions_Delete";
	public static final String SmartTaskExecutions_Seq_Instructions_Select="from SMARTTASKSEQINSTRUCTIONS where upper(Tenant_Id)=? and upper(Company_Id)=? and upper(Execution_Name)=?";
	public static final String SmartTaskExecutions_IsSeqInstructionsExist="/SmartTaskExecutions_IsSeqInstructionsExist";
	public static final String RequestMapping_SmartTaskExecution_GetExecutions="/SmartTaskExecutions_GetExecutions";
	public static final String RequestMapping_SmartTaskExecutions_Maintenance_Update="/SmartTaskExecutions_Maintenance_Update";
	public static final String SmartTaskExecution_Company_id_JspPageValue="COMPANY_ID";

	//(Put Wall)
	public static final String RequestMapping_PutWall_Wizard_Create="/PutWall_Wizard_Create";
	public static final String RequestMapping_PutWall_Wizard_ADD="/PutWall_Wizard_Add";
	
	public static final String GIC_CONTAINERTYPE="CONTAINERTYPE";
	public static final String GIC_CONTAINERID="CONTAINERID";
	public static final String GIC_PUTWALLTYPE="PUTWALLTYPE";
	public static final String GIC_SINGLEMULTI="SINGLEMULTI";
	

	public static final String PutWall_ModelAttribute="ObjSmartTaskExecution";
	public static final String PutWall_Combobox_PutWallType="PutWallType";
	public static final String PutWall_Combobox_ContainerType="ContainerType";
	public static final String PutWall_Combobox_ContainerId="ContainerId";
	public static final String PutWall_Combobox_SingleMulti="SingleMulti";
	public static final String PUTWALLBE="PUTWALLBE";
	public static final String PutWallBean_ModelAttribute="PutWallBean";





}



