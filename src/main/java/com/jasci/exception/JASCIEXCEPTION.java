/**
Description :This class is used to handle exception at insertion time
Created By Aakash Bishnoi
Created Date Nov 10 2014
 */
package com.jasci.exception;

public class JASCIEXCEPTION extends Exception{

	private static final long serialVersionUID = 1L;
	
	private String ErrorMessage;
	
	//For Set Messages
	public JASCIEXCEPTION(String Message){
		super(Message);
		this.ErrorMessage=Message;
	}
	
	//For get Messages
	public String getMessage(){
		return ErrorMessage;
	}
}
