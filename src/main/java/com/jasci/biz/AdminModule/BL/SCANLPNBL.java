/**
 *   

 * @author Shailendra Rajput

 * @Date 19 October,2015 

 * @Description:used for calling web services
 * **/
package com.jasci.biz.AdminModule.BL;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jasci.biz.AdminModule.be.PRODUCTSBE;
import com.jasci.biz.AdminModule.be.SCANLPNBE;
import com.jasci.biz.AdminModule.be.SCANLPNHISTORYBE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public class SCANLPNBL {

	PRODUCTSBE productBeObj =new PRODUCTSBE();
	PRODUCTSBE productBeObj2=new PRODUCTSBE();;
	final Logger log = LoggerFactory.getLogger(SCANLPNBL.class.getName());

	/**
	 *   
	 * @author Shailendra Rajput
	 * @Date 1 October,2015 	 .
	 * @Description:call WebService to getworkbylpn.
	 * @param strTenant
	 * @param FulfillmentCenter
	 * @param StrCompany
	 * @param StrLPN
	 * @return PRODUCTSBE
	 * @throws JASCIEXCEPTION
	 * **/

	 	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
		public SCANLPNBE getWorkByLPN(String StrTenant,String FulfillmentCenter, String StrCompany, String StrLPN) throws JASCIEXCEPTION {


		 SCANLPNBE objScanlpnbe=null;
		
		 String url_obj = GLOBALCONSTANT.STRWEBSERVER + GLOBALCONSTANT.URL_GET_WORK_BY_LPN;

		 StringBuilder sb = new StringBuilder();

		 try {


			 JSONObject input = new JSONObject();
			 input.put(GLOBALCONSTANT.TENANT_SMALL, StrTenant);
			 input.put(GLOBALCONSTANT.FULFILLMENT_CENTER, FulfillmentCenter);
			 input.put(GLOBALCONSTANT.COMPANY_SMALL, StrCompany);
			 input.put(GLOBALCONSTANT.LPN_SCAN, StrLPN);


			 URL url = new URL(url_obj+URLEncoder.encode(input.toString(), GLOBALCONSTANT.UTF8));
			 HttpURLConnection con = (HttpURLConnection) url.openConnection();
			 con.setDoOutput(true);
			 con.setDoInput(true);
			 con.setRequestProperty(GLOBALCONSTANT.Access_Control_Allow_Headers_Content_Type, GLOBALCONSTANT.APPLICATION_JSON);
			 con.setRequestProperty(GLOBALCONSTANT.ACCEPT,  GLOBALCONSTANT.APPLICATION_JSON);
			 // con.setRequestMethod("POST");// optional default is GET
			 if (url_obj.contains(GLOBALCONSTANT.STRENGINESTARTURL)) {
				 con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_GET);
			 } else {
				 con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_POST);
			 }

			 int HttpResult = con.getResponseCode();
			 if (HttpResult == HttpURLConnection.HTTP_OK) {
				 BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), GLOBALCONSTANT.UTF8));
				 String line = null;
				 while ((line = br.readLine()) != null) {
					 sb.append(line + GLOBALCONSTANT.NEXT_LINE);
				 }

				 br.close();

				 objScanlpnbe=validateResponse(sb.toString());
			 } else {
				 log.info(con.getResponseMessage());
			 }

		 } catch (Exception e) {
			 log.info(e.getMessage());
		 }

		 return objScanlpnbe;
	 }

	 /**
	  *   


	  * @author Shailendra Rajput

	  * @Date 1 October,2015 

	  * @Description:show data in right bar with picture of product location with picture Screen
	  * @param strValue
	  * @return SCANLPNBE 
	  * @throws JASCIEXCEPTION
	  * **/

	 public SCANLPNBE validateResponse(String strValue)throws JASCIEXCEPTION {
		 String status = null;
		 SCANLPNBE scanLpnBe = new SCANLPNBE();
		 JSONParser jsonP = new JSONParser();

		 try {

			 // Parse URL Here

			 JSONObject jsonObject = (JSONObject) jsonP.parse(strValue);
			 status = jsonObject.get(GLOBALCONSTANT.VALUESTATUS_CAP).toString();

			 if(GLOBALCONSTANT.SUCCESS.equalsIgnoreCase(status))
			 {
				 JSONObject jsonObject2 = (JSONObject) jsonObject
						 .get(GLOBALCONSTANT.GET_WORK_TYPE_LPN);
				 JSONObject jsonObject3 = (JSONObject) jsonObject2.get(GLOBALCONSTANT.VALUE_ID);

				 // fetch data from "Id" json Onject

				 scanLpnBe.setTENANT_ID(jsonObject3.get(GLOBALCONSTANT.TENANT_ID).toString());
				 scanLpnBe.setCOMPANY_ID(jsonObject3.get(GLOBALCONSTANT.COMPANY_ID).toString());
				 scanLpnBe.setFULFILLMENT_CENTER_ID(jsonObject3.get(
						 GLOBALCONSTANT.FULFILLMENT_CENTER_ID).toString());
				 scanLpnBe.setWORK_CONTROL_NUMBER(jsonObject3.get(
						 GLOBALCONSTANT.WORK_CONTROL_NUMBER).toString());
				 // fetch data from GetWorkByLPNJson Object
				 scanLpnBe.setLPN(jsonObject2.get(GLOBALCONSTANT.LPN).toString());
				 scanLpnBe.setASN(jsonObject2.get(GLOBALCONSTANT.ASN).toString());
				 scanLpnBe.setWORK_TYPE(jsonObject2.get(GLOBALCONSTANT.WORK_TYPE).toString());
				 scanLpnBe.setTASK(jsonObject2.get(GLOBALCONSTANT.WORK_TYPE_TASK).toString());
				 scanLpnBe.setWORK_FLOW_ID(jsonObject2.get(GLOBALCONSTANT.WORK_FLOW_ID_WITH_UNDERSCORE)
						 .toString());
				 scanLpnBe.setWORK_FLOW_STEP(jsonObject2.get(GLOBALCONSTANT.WORK_FLOW_STEP_WITH_UNDERSCORE)
						 .toString());
				 scanLpnBe.setWORK_GROUP_ZONE(jsonObject2.get(GLOBALCONSTANT.WORK_GROUP_ZONE_WITH_UNDERSCORE)
						 .toString());
				 scanLpnBe.setWORK_ZONE(jsonObject2.get(GLOBALCONSTANT.WORK_ZONE).toString());
				 scanLpnBe.setPRIORITY(jsonObject2.get(GLOBALCONSTANT.PRIORITY).toString());
				 scanLpnBe.setPRIORITYCODE(jsonObject2.get(GLOBALCONSTANT.PRIORITY_CODE)
						 .toString());
				 scanLpnBe.setSALES_ORDER_ID(jsonObject2.get(GLOBALCONSTANT.SALES_ORDER_ID)
						 .toString());
				 scanLpnBe.setPURCHASE_ORDER_NUMBER(jsonObject2.get(
						 GLOBALCONSTANT.PURCHASE_ORDER_NUMBER).toString());
				 scanLpnBe.setORDER_ID(jsonObject2.get(GLOBALCONSTANT.ORDER_ID).toString());

				 scanLpnBe.setACCOUNTING_CODE(jsonObject2.get(GLOBALCONSTANT.ACCOUNTATION_CODE)
						 .toString());

				 scanLpnBe.setCREATED_BY(jsonObject2.get(GLOBALCONSTANT.CREATED_BY).toString());

				 scanLpnBe.setDATE_CREATED(jsonObject2.get(GLOBALCONSTANT.DATE_CREATED)
						 .toString());
				 try{
					 scanLpnBe.setSTATUS(jsonObject2.get(GLOBALCONSTANT.STATUS_LPN).toString());
				 }catch(Exception objException){}

				 try{
					 scanLpnBe.setLAST_ACTIVITY_TEAM_MEMBER(jsonObject2.get(
							 GLOBALCONSTANT.LAST_ACTIVITY_TEAM_MEMBER).toString());
				 }catch(Exception objException){}

				 try{
					 scanLpnBe.setLAST_ACTIVITY_TASK(jsonObject2.get(
							 GLOBALCONSTANT.LAST_ACTIVITY_TASK).toString());
				 }catch(Exception objException){}

			 }
			 else{
				 scanLpnBe.setSTATUS(GLOBALCONSTANT.STATUS_FAILURE);

			 }

		 } catch (Exception e) {

			 scanLpnBe.setSTATUS(GLOBALCONSTANT.STATUS_FAILURE);
			 e.getMessage();
		 }


		 return scanLpnBe;
	 }


	 /**
	  *   


	  * @author Shailendra Rajput

	  * @Date 21 October,2015 

	  * @Description:call WebService to update workstatus.
	  * @param strTenant
	  * @param FulfillmentCenter
	  * @param StrCompany
	  * @param StrLPN
	  * @return 

	  * @throws JASCIEXCEPTION
	  * **/


	 @SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strunused })
	public void updateWorkStatus(String StrTenant,String FulfillmentCenter, String StrCompany, String StrLPN) {


		 SCANLPNBE objScanlpnbe=null;
		 Boolean Result=false;
		 String url_obj = GLOBALCONSTANT.STRWEBSERVER + GLOBALCONSTANT.URL_WORK_TYPE_STATUS;

		 StringBuilder sb = new StringBuilder();

		 try {


			 JSONObject input = new JSONObject();
			 input.put(GLOBALCONSTANT.TENANT_SMALL, StrTenant);
			 input.put(GLOBALCONSTANT.FULFILLMENT_CENTER, FulfillmentCenter);
			 input.put(GLOBALCONSTANT.VALUESTATUS_CAP, GLOBALCONSTANT.WORKING);
			 input.put(GLOBALCONSTANT.COMPANY_SMALL, StrCompany);
			 input.put(GLOBALCONSTANT.LPN_SCAN, StrLPN);


			 URL url = new URL(url_obj+URLEncoder.encode(input.toString(), GLOBALCONSTANT.UTF8));
			 HttpURLConnection con = (HttpURLConnection) url.openConnection();
			 con.setDoOutput(true);
			 con.setDoInput(true);
			 con.setRequestProperty(GLOBALCONSTANT.Access_Control_Allow_Headers_Content_Type, GLOBALCONSTANT.APPLICATION_JSON);
			 con.setRequestProperty(GLOBALCONSTANT.ACCEPT,  GLOBALCONSTANT.APPLICATION_JSON);
			 // con.setRequestMethod("POST");// optional default is GET
			 if (url_obj.contains(GLOBALCONSTANT.STRENGINESTARTURL)) {
				 con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_GET);
			 } else {
				 con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_POST);
			 }

			 int HttpResult = con.getResponseCode();
			 if (HttpResult == HttpURLConnection.HTTP_OK) {
				 BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), GLOBALCONSTANT.UTF8));
				 String line = null;
				 while ((line = br.readLine()) != null) {
					 sb.append(line + GLOBALCONSTANT.NEXT_LINE);
				 }

				 br.close();

			 } else {
				 log.info(con.getResponseMessage());
			 }

		 } catch (Exception e) {
			 log.info(e.getMessage());
		 }

	 }	


	 /**
	  *   


	  * @author Shailendra Rajput

	  * @Date 21 October,2015 

	  * @Description:call WebService to update workstatus.
	  * @param strTenant
	  * @param FulfillmentCenter
	  * @param StrCompany
	  * @param StrLPN
	  * @return 

	  * @throws JASCIEXCEPTION
	  * **/


	 @SuppressWarnings(GLOBALCONSTANT.unchecked)
	public void insertWorkingHistory(SCANLPNHISTORYBE objScanLpnHistoryBE) {


		
		 String url_obj = GLOBALCONSTANT.STRWEBSERVER + GLOBALCONSTANT.URL_INSERT_WORK_HISTORY;

		 StringBuilder sb = new StringBuilder();

		 try {


			 JSONObject input = new JSONObject();
			 input.put(GLOBALCONSTANT.TENANT_SMALL,objScanLpnHistoryBE.getTenant().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.FULFILLMENT_CENTER,objScanLpnHistoryBE.getFulfillmentCenter().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.WORK_CONTROLNUMBER,objScanLpnHistoryBE.getWorkControlNumber().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.COMPANY,objScanLpnHistoryBE.getCompany().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.LPN_TASK,objScanLpnHistoryBE.getTask().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.TEAM_MEMBER,objScanLpnHistoryBE.getTeamMember().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.DATE,GLOBALCONSTANT.BLANKSPACESTRING);
			 input.put(GLOBALCONSTANT.LPN,objScanLpnHistoryBE.getLPN().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.ASN,objScanLpnHistoryBE.getASN().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));			
			 input.put(GLOBALCONSTANT.ZONE,objScanLpnHistoryBE.getZONE().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.DIVERT,objScanLpnHistoryBE.getDIVERT().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.WORKTYPE,objScanLpnHistoryBE.getWorkType().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));
			 input.put(GLOBALCONSTANT.WORK_HISTORY_CODE,objScanLpnHistoryBE.getWorkHistoryCode().replaceAll(GLOBALCONSTANT.SINGLE_SPACE,GLOBALCONSTANT.SYMBOL_PRCNT_20));			


			 URL url = new URL(url_obj+URLEncoder.encode(input.toString(), GLOBALCONSTANT.UTF8));
			 HttpURLConnection con = (HttpURLConnection) url.openConnection();
			 con.setDoOutput(true);
			 con.setDoInput(true);
			 con.setRequestProperty(GLOBALCONSTANT.Access_Control_Allow_Headers_Content_Type, GLOBALCONSTANT.APPLICATION_JSON);
			 con.setRequestProperty(GLOBALCONSTANT.ACCEPT,  GLOBALCONSTANT.APPLICATION_JSON);
			 // con.setRequestMethod("POST");// optional default is GET
			 if (url_obj.contains(GLOBALCONSTANT.STRENGINESTARTURL)) {
				 con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_GET);
			 } else {
				 con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_POST);
			 }

			 int HttpResult = con.getResponseCode();
			 if (HttpResult == HttpURLConnection.HTTP_OK) {
				 BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), GLOBALCONSTANT.UTF8));
				 String line = null;
				 while ((line = br.readLine()) != null) {
					 sb.append(line + GLOBALCONSTANT.NEXT_LINE);
				 }

				 br.close();

			 } else {
				 log.info(con.getResponseMessage());
			 }

		 } catch (Exception e) {
			 log.info(e.getMessage());
		 }

	 }	


}
