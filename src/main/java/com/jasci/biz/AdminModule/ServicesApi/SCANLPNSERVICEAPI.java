/**
 *     
 * @author Shailendra Rajput

 * @Date 10 October,2015 
 * @Description PRODUCTLOCATIONPICTURESERVICEAPI Controller TO HANDLE THE REQUESTS FROM Service Interface
 */
package com.jasci.biz.AdminModule.ServicesApi;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.BL.SCANLPNBL;
import com.jasci.biz.AdminModule.be.EXECUTIONSBE;
import com.jasci.biz.AdminModule.be.SCANLPNBE;
import com.jasci.biz.AdminModule.be.SCANLPNHISTORYBE;
import com.jasci.biz.AdminModule.service.IEXECUTIONSLABELSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class SCANLPNSERVICEAPI {

	//@Autowired
	//private IGETPRODUCTLOCATIONPICTUREDAL productLocationPictureDAl;
	@Autowired
	IEXECUTIONSLABELSSERVICE ObjScanLPNService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	final Logger log = LoggerFactory.getLogger(SCANLPNSERVICEAPI.class.getName());
	/**
	 * @Description get All data for specific tenant
	 * @author Shailendra Rajput
	 * @Date Oct 10, 2015 
	 * @param StrTenant
	 * @param strProduct
	 * @param StrCompanyId
	 * @param StrQuality
	 * @return PRODUCTSBE
	 * @throws JASCIEXCEPTION

	 */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.URL_CHECK_LPN, method = RequestMethod.POST)
	public @ResponseBody  JSONObject  CheckLPN(@RequestParam(value=GLOBALCONSTANT.Tenant_Id) String StrTenant,
			@RequestParam(value=GLOBALCONSTANT.Company_Id) String StrCompanyId,
			@RequestParam(value=GLOBALCONSTANT.FULFILLMENT_CENTER) String FulfillmentCenter,
			@RequestParam(value=GLOBALCONSTANT.LPN_SCAN) String StrLPN,
			@RequestParam(value=GLOBALCONSTANT.LPN_TASK) String StrLPNTask,
			@RequestParam(value=GLOBALCONSTANT.LPN_WORK_TYPE) String StrWorkType) {


		SCANLPNBL objScanlpnbl=new SCANLPNBL();
		String result=GLOBALCONSTANT.BLANKSPACESTRING;

		SCANLPNBE objScanlpnbe=null; 
		EXECUTIONSBE objEXECUTIONSBE=null;

		String strTask=StrLPNTask;
		String strWorkType=StrWorkType;
		String teamMember=OBJCOMMONSESSIONBE.getTeam_Member().trim();
		try {
			objEXECUTIONSBE=ObjScanLPNService.getScanLPNLabels(LANGUANGELABELSAPI.strWebExecutionModule, OBJCOMMONSESSIONBE.getCurrentLanguage());
		} catch (JASCIEXCEPTION e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			objScanlpnbe=objScanlpnbl.getWorkByLPN(StrTenant,FulfillmentCenter,StrCompanyId,StrLPN);

			if(objScanlpnbe.getSTATUS().equalsIgnoreCase(GLOBALCONSTANT.STATUS_FAILURE)){
				result=objEXECUTIONSBE.getERR_MSG_INVALIDE_LPN();
			}
			else if (objScanlpnbe.getSTATUS().equalsIgnoreCase(GLOBALCONSTANT.WORKING) && (!objScanlpnbe.getLAST_ACTIVITY_TEAM_MEMBER()
					.equalsIgnoreCase(teamMember))) {
				result=objEXECUTIONSBE.getERR_MSG_LPN_ALREADY_ASSIGNED();
			}
			else if (!objScanlpnbe.getWORK_TYPE().equalsIgnoreCase(strWorkType)) {

				result= objEXECUTIONSBE.getERR_MSG_INVALID_WORK_TYPE();
			}

			else if (!objScanlpnbe.getTASK().equalsIgnoreCase(
					strTask)) {
				result=objEXECUTIONSBE.getERR_MSG_INVALIDE_TASK_FOR_SEQUENCE();

			}
			else{
				result=GLOBALCONSTANT.SUCCESS;
				try{
					objScanlpnbl.updateWorkStatus(StrTenant, FulfillmentCenter, StrCompanyId, StrLPN);
				
				}catch(Exception objException){
					objException.getMessage();
				}
				
				try{
					SCANLPNHISTORYBE scanHistoryBe = new SCANLPNHISTORYBE();						
					setResultScanLpnHistory(objScanlpnbe,scanHistoryBe,teamMember);
					scanHistoryBe.setWorkHistoryCode(GLOBALCONSTANT.WORK_HISTORY_CODE_ASSIGNED);					
					objScanlpnbl.insertWorkingHistory(scanHistoryBe);
				
				}catch(Exception objException){
					objException.getMessage();
				}
			}

		}catch(Exception ex){
			log.info(ex.getMessage());
			result=objEXECUTIONSBE.getERR_MSG_INVALIDE_LPN();
		}
		JSONObject objJsonObject = new JSONObject();
		objJsonObject.put(GLOBALCONSTANT.Result, result);
		

		return objJsonObject;


	}
	
	
	/**
	 * @Description set Result To SharedObject
	 * @param SCANLPNBE
	 * @param SCANLPNHISTORYBE
	 * @date 28 july 2015
	 * @author Pradeep Kumar
	 */
	public static void setResultScanLpnHistory(SCANLPNBE objScanLpnBE,SCANLPNHISTORYBE objScanLpnHistoryBE, String TeamMember){
		objScanLpnHistoryBE.setASN(objScanLpnBE.getASN());
		objScanLpnHistoryBE.setCompany(objScanLpnBE.getCOMPANY_ID());
		objScanLpnHistoryBE.setDate(objScanLpnBE.getDATE_CREATED());
		objScanLpnHistoryBE.setDIVERT(GLOBALCONSTANT.BLANKSPACESTRING);
		objScanLpnHistoryBE.setFulfillmentCenter(objScanLpnBE.getFULFILLMENT_CENTER_ID());
		objScanLpnHistoryBE.setLPN(objScanLpnBE.getLPN());
		objScanLpnHistoryBE.setTask(objScanLpnBE.getTASK());
		objScanLpnHistoryBE.setTeamMember(GLOBALCONSTANT.BLANKSPACESTRING);
		objScanLpnHistoryBE.setTenant(objScanLpnBE.getTENANT_ID());
		objScanLpnHistoryBE.setWorkControlNumber(objScanLpnBE.getWORK_CONTROL_NUMBER());
		objScanLpnHistoryBE.setWorkHistoryCode(GLOBALCONSTANT.BLANKSPACESTRING);
		objScanLpnHistoryBE.setWorkType(objScanLpnBE.getWORK_TYPE());
		objScanLpnHistoryBE.setZONE(objScanLpnBE.getWORK_ZONE());
		objScanLpnHistoryBE.setTeamMember(TeamMember);
		
	}

}
