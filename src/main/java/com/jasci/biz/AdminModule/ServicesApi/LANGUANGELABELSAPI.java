/**

Date Developed :Dec 17 2014
Created by: Deepka Sharma
Description :This is used to declare the Key Phrase and set the language lables .
 */

package com.jasci.biz.AdminModule.ServicesApi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONBE;
import com.jasci.biz.AdminModule.model.LANGUAGEPK;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utildal.LANGUAGEDAOIMPL;

@Controller
public class LANGUANGELABELSAPI {

	@Autowired
	LANGUAGEDAOIMPL objLanguagesDaoImpl;

	final Logger log = LoggerFactory.getLogger(LANGUANGELABELSAPI.class.getName());
	
	LANGUAGETRANSLATIONBE objLanguageTranslationBE;
	String[] keyphraseArray;

	public static final String KP_Location_Weight_Capacity_must_be_less_then_or_equal_to_Location_Width="Location Weight Capacity must be less then or equal to Location Width.";
	public static final String KP_Location_Height_Capacity_must_be_less_then_or_equal_to_Location_Height="Location Height Capacity must be less then or equal to Location Height.";
	public static final String KP_Equality_Check_Error_Msg="Length of characters in Starting At and Number Of must be equal to value entered in Number Of Characters field.";
	public static final String KP_Row="Row";
	public static final String KP_Aisle="Aisle";
	public static final String KP_Bay="Bay";
	public static final String KP_Level="Level";
	public static final String KP_Slot="Slot";
	public static final String KP_CONTINUATIONS_SEQUENCE="CONTINUATIONS SEQUENCE";
	public static final String KP_Wizard_Control_Number_not_found_for_selected_Fulfillment_Center="Wizard Control Number not found for selected Fulfillment Center.";
	public static final String KP_Please_Select_Fullfillment_Center="Please select Fulfillment Center.";
	public static final String KP_Reset="Reset";
	public static final String KP_Please_enter_valid_Execution_Path="Please enter valid Execution Path.";
	public static final String KP_Mandatory_field_cannot_be_left_blank_and_must_be_numeric_only = "Mandatory field cannot be left blank and must be numeric only.";
	public static final String KP_Mandatory_field_cannot_be_left_blank_and_must_be_valid_email_address = "Mandatory field cannot be left blank and must be valid email address.";
	public static final String KP_Description_long_already_used = "Description Long already used.";
	public static final String KP_Description_Short_already_used = "Description Short already used.";
	public static final String KP_Name_Short_already_used = "Name Short already used.";
	public static final String KP_Name_Long_already_used = "Name Long already used.";
	public static final String KP_You_are_not_allowed_to_create_locations_in_reverse_order="Locations can only be created in ascending order.";
	public static final String KP_You_are_not_allowed_to_login_as_the_company_you_are_associated_with_is_inactive_Please_contact_administrator="You are not allowed to login as the company you are associated with is inactive. Please contact administrator.";
	public static final String KP_Loaction_and_Alternate_Location_can_not_be_same="Location and Alternate Location can not be same.";
	public static final String KP_From_Location="From Location";
	public static final String KP_Barcode_Type128="Barcode Type (128)";
	public static final String KP_Barcode_Type="Barcode Type";
	public static final String KP_Please_select_from_Area="Please select from Area.";
	public static final String KP_Please_select_Wizard_Control_Number="Please select Wizard Control Number.";
	public static final String KP_Invalid_To_Location="Invalid To Location.";
	public static final String KP_Invalid_And_Location="Invalid And Location.";
	public static final String KP_Invalid_From_Location="Invalid From Location.";
	public static final String KP_Wizard_Location="Wizard Location";
	public static final String KP_Company_already_exist_with_inactive_status="Company already exist with inactive status.";
	public static final String KP_Fulfillment_Center_aready_exist_with_inactive_status="Fulfillment Center already exist with inactive status.";
	public static final String KP_Enter_Fulfillment = "Enter Fulfillment";
	public static final String KP_Part_of_the_Fulfillment_Center_Name = "Part of the Fulfillment Center Name";
	public static final String KP_New = "New";
	public static final String KP_Display_All = "Display All";
	public static final String KP_Fullfillment_Centers_Lookup = "Fulfillment Centers Lookup";
	public static final String KP_Fullfillment_Centers_Search_Lookup = "Fulfillment Centers Search Lookup";
	public static final String KP_Name = "Name";
	public static final String KP_Edit = "Edit";
	public static final String KP_Delete = "Delete";
	public static final String KP_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table = "Selected Fulfillment Center ID cannot be deleted as it is used in another table.";
	public static final String KP_Selected_fulfillment_center_deleted_successfully = "Selected Fulfillment Center ID has been deleted successfully.";
	public static final String KP_Invalid_Part_of_the_fulfillment = "Invalid Part of Fulfillment Center Name.";
	public static final String KP_Invalid_Fulfillment_Center = "Invalid Fulfillment Center ID.";
	public static final String KP_Add_New = "Add New";
	public static final String KP_Fulfillment_Center_Maintenance = "Fulfillment Centers Maintenance";
	public static final String KP_Fulfillment_Center = "Fulfillment Center ID";
	public static final String KP_Fulfillment_Name = "Fulfillment Center Name";
	public static final String KP_Setup_By = "Setup By";
	public static final String KP_Last_Activity_Date = "Last Activity Date (YYYY-MM-DD)";
	public static final String KP_Last_Activity_By = "Last Activity By";
	public static final String KP_Name_Short = "Name Short";
	public static final String KP_Name_Long = "Name Long";
	public static final String KP_Address_Line_1 = "Address Line 1";
	public static final String KP_Address_Line_2 = "Address Line 2";
	public static final String KP_Address_Line_3 = "Address Line 3";
	public static final String KP_Address_Line_4 = "Address Line 4";
	public static final String KP_City = "City";
	public static final String KP_Country_Code = "Country Code";
	public static final String KP_State = "State";
	public static final String KP_Zip_Code = "Zip Code";
	public static final String KP_Contact_Name = "Contact Name";
	public static final String KP_Contact_Extension = "Contact Extension";
	public static final String KP_Contact_Cell = "Contact Cell";
	public static final String KP_Contact_Fax = "Contact Fax";
	public static final String KP_Contact_Email = "Contact Email";
	public static final String KP_Contact_Phone = "Contact Phone";
	public static final String KP_Main_Fax = "Main Fax";
	public static final String KP_Main_Email = "Main Email";
	public static final String KP_Save_Update = "Save/Update";
	public static final String KP_Cancel = "Cancel";
	public static final String KP_Mandatory_field_cannot_be_left_blank = "Mandatory field cannot be left blank.";
	public static final String KP_Fulfillment_center_already_used = "Fulfillment center already used.";
	public static final String KP_Your_record_has_been_saved_successfully = "Your record has been saved successfully.";
	public static final String KP_Your_record_has_been_updated_successfully="Your record has been updated successfully.";
	public static final String KP_Contact="Contact";
	public static final String KP_Not_a_valid_number = "Not a valid number";
	public static final String KP_Not_a_valid_email_id = "Not a valid email id";
	public static final String KP_Please_enter_only_numeric_values="Please enter only numeric values.";
	public static final String KP_Please_enter_valid_Email_Address="Please enter valid Email Address.";
	public static final String KP_Please_enter_Fulfillment_center_ID="Please enter Fulfillment Center ID.";
	public static final String KP_Please_enter_Part_of_Fulfillment_Center_Name="Please enter Part of Fulfillment Center Name.";
	public static final String KP_Fulfillment_Center_ID_already_used="Fulfillment Center ID already used.";
	public static final String KP_Security_Setup_Lookup="Security Setup Lookup";
	public static final String KP_Invalid_Team_Member_ID="Invalid Team Member ID.";
	public static final String KP_Last_Password1="Last Password1";
	public static final String KP_Last_Password2="Last Password2";
	public static final String KP_You_are_not_allowed_to_edit_as_this_team_member_is_not_active="You are not allowed to edit as this team member is not active.";
	public static final String KP_Security_Question_Answer2="Security Question Answer2";
	public static final String KP_Last_Password3="Last Password3";
	public static final String KP_Status="Status";
	public static final String KP_All_Security_questions_must_be_unique="All Security questions must be unique.";
	public static final String KP_Are_you_sure_you_want_to_delete_this_record="Are you sure you want to delete this record?";
	public static final String KP_Sort_Last_Name="Sort Last Name";
	public static final String KP_Security_Setup_Maintenance="Security Setup Maintenance";
	public static final String KP_Security_Question1="Security Question1";
	public static final String KP_Security_Question_Answer3="Security Question Answer3";
	public static final String KP_Last_Password5="Last Password5";
	public static final String KP_UserID_must_be_of_atleast_6_characters_and_must_include_one_number="UserID must be of at least 6 characters and must include one number.";
	public static final String KP_UsedID_already_used="UsedID already used.";
	public static final String KP_Part_of_the_Team_Member_Name="Part of the Team Member Name";
	public static final String KP_Please_enter_a_Team_Member_ID="Please enter a Team Member ID.";
	public static final String KP_Sort_First_Name="Sort First Name";
	public static final String KP_Tenant="Tenant";
	public static final String KP_Last_Password4="Last Password4";
	public static final String KP_Number_of_Last_Attempts="Number of Last Attempts";
	public static final String KP_Part_of_Team_Member="Part of Team Member";
	public static final String KP_Security_Setup_Search_Lookup="Security Setup Search Lookup";
	public static final String KP_User_ID="User ID";
	public static final String KP_Last_Invalid_Date_YY_MM_DD="Last Invalid Date (YYYY-MM-DD)";
	public static final String KP_Your_record_has_been_added_successfully="Your record has been added successfully.";
	public static final String KP_Actions="Actions";
	public static final String KP_Company="Company";
	public static final String KP_Selected_record_has_been_deleted_successfully="Selected record has been deleted successfully.";
	public static final String KP_You_are_not_allowed_to_delete_this_team_member_as_it_does_not_exist_in_security_autorization_table="You are not allowed to delete this team member as it does not exist in security autorization table.";
	public static final String KP_Security_Question_Answer1="Security Question Answer1";
	public static final String KP_Security_Question2="Security Question2";
	public static final String KP_Security_Question3="Security Question3";
	public static final String KP_Team_Member="Team Member";
	public static final String KP_Company_ID="Company ID";
	public static final String KP_MESSAGES="MESSAGES";
	public static final String KP_APPLICATIONS="APPLICATIONS";
	public static final String KP_Select_Company_to_Work_With="Select Company to Work With";
	public static final String KP_OR="OR";
	public static final String KP_Part_of_the_Application_Name="Part of the Application Name";
	public static final String KP_Part_of_the_Menu_Option_Description="Part of the Menu Option Description";
	public static final String KP_Part_of_the_Menu_Profile_Description="Part of the Menu Profile Description";
	public static final String KP_SelectApplication="Select Application";
	public static final String KP_Sub_Application="Sub Application";
	public static final String KP_Tenant_ID="Tenant ID";
	public static final String KP_Menu_Option="Menu Option";
	public static final String KP_Invalid_Part_Of_The_Application_Name="Invalid Part Of The Application Name";
	public static final String KP_Invalid_Part_Of_The_Menu_Option_Description="Invalid Part Of The Menu Option Description.";
	public static final String KP_Invalid_Part_Of_The_Menu_Option="Invalid Part Of The Menu Option.";
	public static final String KP_Invalid_Menu_Option="Invalid Menu Option";
	public static final String KP_Invalid_Application="Invalid Application.";
	public static final String KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button="Are you sure you want to delete the selected row?";
	public static final String KP_Add="Add";
	public static final String KP_MenuType="Menu Type";
	public static final String KP_Menu_Option_Search_LookUp="Menu Option Search Lookup";
	public static final String KP_Menu_Option_already_used="Menu Option already used.";
	public static final String KP_Menu_Option_is_currently_assigned_to_Menu_Profiles_if_Deleted_this_option_will_be_removed_from_all_Menu_Profiles="Menu Option is currently assigned to Menu Profiles, if Deleted this option will be removed from all Menu Profiles.";
	public static final String KP_Help_Line="Help Line";
	public static final String KP_Execution_Path="Execution Path";
	public static final String KP_Please_select_Menu_Type="Please select Menu Type.";
	public static final String KP_Please_enter_Part_Of_the_Application="Please enter Part Of the Application.";
	public static final String KP_Please_enter_Part_Of_the_Menu_Option="Please enter Part Of the Menu Option.";
	public static final String KP_Please_enter_Application="Please enter Application.";
	public static final String KP_Please_enter_Menu_Option="Please enter Menu Option.";
	public static final String KP_Menu_Option_Mantenance="Menu Option Maintenance";
	public static final String KP_Menu_Option_Maintenance_LookUp="Menu Option Maintenance Lookup";
	public static final String KP_Invalid_Menu_Type="Invalid Menu Type.";
	public static final String KP_Invalid_Tenant_ID="Invalid Tenant ID.";
	public static final String KP_Please_enter_Tenant_ID="Please enter Tenant ID.";
	public static final String KP_No_menu_option_has_been_assigned_to_the_profile="No menu option has been assigned to the profile.";
	public static final String KP_Application="Application";
	public static final String KP_Application_Sub="Application Sub";
	public static final String KP_Available_Menu_Options="Available Menu Options";
	public static final String KP_Code_Identification="Code Identification";
	public static final String KP_Control01_Description="Control01 Description";
	public static final String KP_Control01_Value="Control01 Value";
	public static final String KP_Control02_Description="Control02 Description";
	public static final String KP_Control02_Value="Control02 Value";
	public static final String KP_Control03_Description="Control03 Description";
	public static final String KP_Control03_Value="Control03 Value";
	public static final String KP_Control04_Description="Control04 Description";
	public static final String KP_Control04_Value="Control04 Value";
	public static final String KP_Control05_Description="Control05 Description";
	public static final String KP_Control05_Value="Control05 Value";
	public static final String KP_Control06_Description="Control06 Description";
	public static final String KP_Control06_Value="Control06 Value";
	public static final String KP_Control07_Description="Control07 Description";
	public static final String KP_Control07_Value="Control07 Value";
	public static final String KP_Control08_Description="Control08 Description";
	public static final String KP_Control08_Value="Control08 Value";
	public static final String KP_Control09_Description="Control09 Description";
	public static final String KP_Control09_Value="Control09 Value";
	public static final String KP_Control10_Description="Control10 Description";
	public static final String KP_Control10_Value="Control10 Value";
	public static final String KP_General_Code="General Code";
	public static final String KP_General_Code_Id="General Code Id";
	public static final String KP_General_Code_New_Edit="General Code New/Edit";
	public static final String KP_General_Codes="General Codes";
	public static final String KP_Invalid_Menu_Profile="Invalid Menu Profile.";
	public static final String KP_Invalid_Part_Of_Menu_Profile_Description="Invalid Part Of Menu Profile Description.";
	public static final String KP_Menu_App_Icon="Menu App Icon";
	public static final String KP_Menu_Profile="Menu Profile";
	public static final String KP_Menu_Profile_already_used="Menu Profile already used.";
	public static final String KP_Menu_Profile_is_currently_assigned_to_Menu_Assignments_if_Deleted_this_option_will_be_removed_from_all_Menu_Assignments="Menu Profile is currently assigned to Menu Assignments, if deleted this option will be removed from all Menu Assignments.";
	public static final String KP_Menu_Profiles_Maintenance="Menu Profiles Maintenance";
	public static final String KP_Menu_Profiles_Maintenance_Lookup="Menu Profiles Maintenance Lookup";
	public static final String KP_Menu_Profiles_Maintenance_Search_Lookup="Menu Profiles Maintenance Search Lookup";
	public static final String KP_Menu_Profiles_Options="Assign Menu Profiles Options";
	public static final String KP_Password_cant_be_reused_in_the_past_5_times="Password can't be reused in the past 5 times.";
	public static final String KP_Please_enter_Menu_Profile="Please enter Menu Profile.";
	public static final String KP_Please_enter_Part_Of_The_Menu_Option_Description="Please enter Part Of The Menu Option Description.";
	public static final String KP_Please_enter_Part_of_Menu_Profile_Description="Please enter Part of Menu Profile Description.";
	public static final String KP_Enter_your_e_mail_address_below_to_reset_your_password="Enter your e-mail address below to reset your password.";
	public static final String KP_Email_address_you_have_entered_does_not_exist="Email address you have entered does not exist.";
	public static final String KP_Login_="Login";
	public static final String KP_Login_to_your_account="Login to your account";
	public static final String KP_Invalid_Sign_on_Please_contact_Tenant_s_security_officer="Invalid Sign-on. Please contact Tenant's security officer.";
	public static final String KP_Password="Password";
	public static final String KP_Submit="Submit";
	public static final String KP_UserID="UserID";
	public static final String KP_Your_Password_has_been_expired="Your Password has been expired";
	public static final String KP_no_worries_click="no worries, click ";
	public static final String KP_Confirm_Password="Confirm Password";
	public static final String KP_New_Password="New Password";
	public static final String KP_New_password_and_confirm_password_do_not_match="New password and confirm password do not match.";
	public static final String KP_Password_must_be_of_atleast_8_characters_including_one_capital_letter_and_three_numbers="Password must be of at least 8 characters including one capital letter and three numbers.";
	public static final String KP_Update="Update";
	public static final String KP_Your_Password_has_been_expired_Please_create_new_password="Your Password has been expired. Please create new password.";
	public static final String KP_Your_Password_is_temporary_Please_create_new_password="Your Password is temporary. Please create new password.";
	public static final String KP_Label_Select="Label Selection";
	public static final String KP_Select="Select";
	public static final String KP_Location_Label_Selection="Location Label Selection";
	public static final String KP_Reprint_Flagged_Locations="Reprint Flagged Locations";	
	public static final String KP_Area="Area"; 
	public static final String KP_Wizard_Control_Number="Wizard Control Number"; 
	public static final String KP_And_Location="And Location"; 
	public static final String KP_From_Area="From Area";
	public static final String KP_To_Location="To Location";
	public static final String KP_Label_Type="Label Type";
	public static final String KP_Barcode_Type3of9="Barcode Type(3of9)"; 
	public static final String KP_Or_Barcode_Type128="Or Barcode Type(128)";
	public static final String KP_Print_Labels="Print Labels";
	public static final String KP_Lookup_All_with_Selection="Lookup All with Selection"; 
	public static final String KP_Please_enter_Wizard_Control_Number="Please enter Wizard Control Number.";
	public static final String KP_Please_enter_numeric_Wizard_Control_Number="Please enter numeric Wizard Control Number.";
	public static final String KP_Please_select_Label_Type="Please select Label Type.";
	public static final String KP_Please_select_Barcode_Type="Please select Barcode Type.";
	public static final String KP_YY_MM_DD="(YY-MM-DD)";
	public static final String KP_Any_Part_of_the_App_Icon="Any Part of the App Icon";
	public static final String KP_App_Icon="App Icon";
	public static final String KP_App_Icon_Address="App Icon Address";
	public static final String KP_App_Icon_already_used="App Icon already used.";
	public static final String KP_Application_already_used="Application already used.";
	public static final String KP_Are_sure_they_want_to_delete_this_App_Icon="Are sure you want to delete this App Icon?";
	public static final String KP_Choose_File="Choose File";
	public static final String KP_Description="Description";
	public static final String KP_Description_Long="Description Long";
	public static final String KP_Description_Short="Description Short";
	public static final String KP_Invalid_App_Icon_Address="Invalid App Icon Address.";
	public static final String KP_Invalid_App_Icon="Invalid App Icon.";
	public static final String KP_Invalid_Part_of_the_App_Icon_Name="Invalid Part of the App Icon Name.";
	public static final String KP_Invalid_Part_of_the_App_Icon="Invalid Part of the App Icon.";
	public static final String KP_Loading="Loading";
	public static final String KP_Menu_App_Icon_LooKup="Menu App Icon Lookup";
	public static final String KP_Menu_App_Icon_Maintenance="Menu App Icon Maintenance";
	public static final String KP_Menu_App_Icon_Search_LooKup="Menu App Icon Search Lookup";
	public static final String KP_Part_of_the_App_Icon_Name="Part of the App Icon Name";
	public static final String KP_Part_of_the_Description="Part of the Description";
	public static final String KP_Please_enter_a_App_Icon="Please enter an App Icon.";
	public static final String KP_Please_enter_part_of_the_App_Icon_Name="Please enter Part of the App Icon Name.";
	public static final String KP_Please_enter_part_of_the_App_Icon="Please enter part of the App Icon.";
	public static final String KP_Preferred_Application="Preferred Application";
	public static final String KP_Supported_formats_are_Jpeg_jpg_png_gif="Supported formats are(.Jpeg,.jpg,.png,.gif).";
	public static final String KP_ShiftCode_does_not_exist="ShiftCode does not exist.";
	public static final String KP_Language_does_not_exist="Language does not exist.";
	public static final String KP_Start_date_must_be_before_End_date="Start date must be before End date.";
	public static final String KP_Last_date_must_be_after_Start_date="Last date must be after Start date.";
	public static final String KP_Work_Cell_must_be_numeric_only="Work Cell must be numeric only.";
	public static final String KP_Work_Fax_must_be_numeric_only="Work Fax must be numeric only.";
	public static final String KP_Cell_must_be_numeric_only="Cell must be numeric only.";
	public static final String KP_Emergency_Home_Phone_must_be_numeric_only="Emergency Home Phone must be numeric only.";
	public static final String KP_Emergency_Cell_must_be_numeric_only="Emergency Cell must be numeric only.";
	public static final String KP_Please_select_atleast_one_company_to_proceed="Please select at least one company to proceed.";
	public static final String KP_Work_Extension_must_be_numeric_only="Work Extension must be numeric only.";
	public static final String KP_Please_enter_part_of_the_Team_Member_Name="Please enter Part of Team Member Name.";
	public static final String KP_First_Name="First Name";
	public static final String KP_Last_Name="Last Name";
	public static final String KP_Middle_Name="Middle Name";
	public static final String KP_Work_Phone="Work Phone";
	public static final String KP_Work_Extension="Work Extension";
	public static final String KP_Work_Cell="Work Cell";
	public static final String KP_Work_Fax="Work Fax";
	public static final String KP_Work_Email="Work Email";
	public static final String KP_Team_member_already_Used="Team member already Used.";
	public static final String KP_Work_Phone_must_be_numeric_only="Work Phone must be numeric only.";
	public static final String KP_Department_does_not_exist="Department does not exist.";
	public static final String KP_Email_address_already_exist="Email address already exist.";
	public static final String KP_Home_Phone_must_be_numeric_only="Home Phone must be numeric only.";
	public static final String KP_Invalid_Zip_Code="Invalid Zip Code.";
	public static final String KP_Invalid_Address="Invalid Address.";
	public static final String KP_Are_you_sure_want_to_delete="Are you sure want to delete?";
	public static final String KP_You_are_not_allowed_to_delete_as_this_team_member_is_already_Inactive="You are not allowed to delete as this team member is already Inactive.";
	public static final String KP_Leave_of_Absence="Leave of Absence";
	public static final String KP_Not_Active="Not Active";
	public static final String KP_Active="Active";
	public static final String KP_Deleted="Deleted";
	public static final String KP_System_Use="System Use";
	public static final String KP_Team_Members_Lookup="Team Members Lookup";
	public static final String KP_Part_of_Team_Member_Name="Part of Team Member Name";
	public static final String KP_Team_Members_Maintenance="Team Members Maintenance";
	public static final String KP_Team_Member_Id="Team Member ID";
	public static final String KP_Currrent_Status="Current Status";
	public static final String KP_Home_Phone="Home Phone";
	public static final String KP_Cell="Cell";
	public static final String KP_Email="Email";
	public static final String KP_Emergency_Contact_Name="Emergency Contact Name";
	public static final String KP_Emergency_Home_Phone="Emergency Home Phone";
	public static final String KP_Emergency_Cell="Emergency Cell";
	public static final String KP_Emergency_Email="Emergency Email";
	public static final String KP_Department="Department";
	public static final String KP_Shift_Code="Shift Code";
	public static final String KP_Language="Language";
	public static final String KP_Last_Date_YYYY_MM_DD="Last Date (YYYY-MM-DD)";
	public static final String KP_Company_Name="Company Name";
	public static final String KP_Notes="Notes";
	public static final String KP_Team_Members_Search_Lookup="Team Members Search Lookup";
	public static final String KP_Language_Translation_Lookup="Language Translation Lookup";
	public static final String KP_English_Key_Phrase="English Key Phrase";
	public static final String KP_Key_Phrase="Key Phrase";
	public static final String KP_Translation="Translation";
	public static final String KP_Language_Translation_Search_Lookup="Language Translation Search Lookup";
	public static final String KP_Language_Translation_Maintenance="Language Translation Maintenance";
	public static final String KP_Invalid_Language="Invalid Language.";
	public static final String KP_Invalid_Key_Phrase="Invalid Key Phrase.";
	public static final String KP_Please_select_Language="Please select Language.";
	public static final String KP_Please_enter_KeyPhrase="Please enter KeyPhrase.";
	public static final String KP_Already_Used="Already Used.";
	public static final String KP_Please_select_a_team_member_to_proceed="Please select a team member to proceed.";
	public static final String KP_Shift="Shift";
	public static final String KP_Search="Search";
	public static final String KP_Team_Member_Selection="Team Member Selection";
	public static final String KP_End_Date="End Date";
	public static final String KP_Message_All_Selected="Message All Selected";
	public static final String KP_Type="Type";
	public static final String KP_End_date_cannot_be_older_then_start_date="End date cannot be older then start date.";
	public static final String KP_Invalid_Part_of_Team_Member_Name="Invalid Part of Team Member Name.";
	public static final String KP_Invalid_Department="Invalid Department.";
	public static final String KP_Invalid_Shift="Invalid Shift.";
	public static final String KP_Please_enter_Team_Member_ID="Please enter Team Member ID.";
	public static final String KP_Please_select_Department="Please select Department.";
	public static final String KP_Please_select_Shift="Please select Shift.";
	public static final String KP_Are_you_sure_you_want_to_delete_this_message="Are you sure you want to delete this message?";
	public static final String KP_SEARCH_LOOKUP="SEARCH LOOKUP";	
	public static final String KP_Active_Team_Member_Messages="Active Team Member Messages";
	public static final String KP_Team_Member_Message_Lookup="Team Member Message Lookup";
	public static final String KP_Team_Member_Message_Maintenance_New_Edit="Team Member Message Maintenance New/Edit";
	public static final String KP_Team_Member_Search_Lookup="Team Member Search Lookup";
	public static final String KP_Start_Date_YYYY_MM_DD="Start Date (YYYY-MM-DD)";
	public static final String KP_End_Date_YYYY_MM_DD="End Date (YYYY-MM-DD)";
	public static final String KP_Message="Message";
	public static final String KP_Menu_Messages_Search_Lookup="Menu Messages Search Lookup";
	public static final String KP_Start_Date="Start Date";
	public static final String KP_Start_date_cannot_be_older_then_current_date="Start date cannot be older then current date.";
	public static final String KP_Please_enter_either_Ticket_Tape_Message_or_Message="Please enter either Ticket Tape Message or Message.";
	public static final String KP_Message_Already_Used="Message already used.";
	public static final String KP_Menu_Messages_Maintenance="Menu Messages Maintenance";
	public static final String KP_Ticket_Tape_Message="Ticket Tape Message";
	public static final String KP_Companies="Companies";
	public static  String KP_Info_Help_Assignment_Search="Info-Help Assignment Search";
	public static  String KP_Invalid_Description="Invalid Description.";
	public static  String KP_Please_enter_Info_Help="Please enter Info-Help.";
	public static  String KP_Please_select_Info_Help_Type="Please select Info-Help Type.";
	public static  String KP_Invalid_Info_Help_Type="Invalid Info-Help Type.";
	public static  String KP_Invalid_Info_Help="Invalid Info-Help.";
	public static  String KP_Please_enter_Part_of_the_Description="Please enter Part of the Description.";
	public static  String KP_Infohelps_already_used="Infohelps already used.";
	public static  String KP_Info_Help="Info-Help";
	public static  String KP_Info_Help_Type="Info-Help Type";
	public static  String KP_Last_ActivityDate="Last Activity Date";
	public static  String KP_Internal_Error="Internal Error.";
	public static  String KP_InfoHelp_Assignment_Maintenance="InfoHelp Assignment Maintenance";
	public static  String KP_InfoHelp_Assignment_Search_Lookup="InfoHelp Assignment Search Lookup";
	public static  String KP_Supported_formats_is_pdf="Supported formats is(.pdf).";
	public static final String KP_MenuOptionName="Menu Name";
	public static final String KP_Help="Help";
	public static final String KP_General_Codes_MainTenance="General Codes Maintenance";
	public static final String KP_GENERALCODES="GENERALCODES";
	public static final String KP_YES="YES";
	public static final String KP_NO="NO";
	public static final String KP_General_code_or_Menu_name_already_used="General code or Menu name already used.";
	public static final String KP_General_code_already_used="General code already used.";
	public static final String KP_SYSTEM="SYSTEM";
	public static final String KP_Invalid_bill_to_zip_code="Invalid Bill To Zip Code.";
	public static final String KP_Invalid_bill_to_address="Invalid Bill To Address.";
	public static final String KP_Invalid_email_address="Invalid email address.";
	public static final String KP_Only_numeric__value_allowed="Only numeric value allowed.";
	public static final String KP_Part_of_the_Company_Name="Part of the Company Name";
	public static final String KP_Bill_To_Address_Line_1="Bill To Address Line 1";
	public static final String KP_Bill_To_Address_Line_2="Bill To Address Line 2";
	public static final String KP_Bill_To_Address_Line_3="Bill To Address Line 3";
	public static final String KP_Bill_To_Address_Line_4="Bill To Address Line 4";
	public static final String KP_Bill_To_City="Bill To City";
	public static final String KP_Bill_To_State_Code="Bill To State Code";
	public static final String KP_Bill_To_Country_Code="Bill To Country Code";
	public static final String KP_Bill_To_Zip_Code="Bill To Zip Code";
	public static final String KP_Contact_Name_2="Contact Name 2";
	public static final String KP_Contact_Phone_2="Contact Phone 2";
	public static final String KP_Contact_Extension_2="Contact Extension 2";
	public static final String KP_Contact_Cell_2="Contact Cell 2";
	public static final String KP_Contact_Fax_2="Contact Fax 2";
	public static final String KP_Contact_Email_2="Contact Email 2";
	public static final String KP_Contact_Name_3="Contact Name 3";
	public static final String KP_Contact_Phone_3="Contact Phone 3";
	public static final String KP_Contact_Extension_3="Contact Extension 3";
	public static final String KP_Contact_Cell_3="Contact Cell 3";
	public static final String KP_Contact_Fax_3="Contact Fax 3";
	public static final String KP_Contact_Email_3="Contact Email 3";
	public static final String KP_Contact_Name_4="Contact Name 4";
	public static final String KP_Contact_Phone_4="Contact Phone 4";
	public static final String KP_Contact_Extension_4="Contact Extension 4";
	public static final String KP_Contact_Cell_4="Contact Cell 4";
	public static final String KP_Contact_Fax_4="Contact Fax 4";
	public static final String KP_Contact_Email_4="Contact Email 4";
	public static final String KP_Billing_Control_Number="Billing Control Number";
	public static final String KP_Theme_Mobile="Theme Mobile";
	public static final String KP_Theme_RF="Theme RF";
	public static final String KP_Theme_Full_Display="Theme Full Display";
	public static final String KP_Logo="Logo";
	public static final String KP_Purchase_Order_Require_Approval="Purchase Order Require Approval";
	public static final String KP_Time_Zone="Time Zone";
	public static final String KP_Company_Lookup="Company Lookup";
	public static final String KP_Company_Maintenance="Company Maintenance";
	public static final String KP_Company_Search_Lookup="Company Search Lookup";
	public static final String KP_At_least_one_field_is_required_for_search="At least one field is required for search.";
	public static final String KP_Length_of_Address_Line_1_must_not_exceed_100="Length of Address Line 1 must not exceed 100.";
	public static final String KP_Length_of_Address_Line_2_must_not_exceed_100="Length of Address Line 2 must not exceed 100.";
	public static final String KP_Length_of_Address_Line_3_must_not_exceed_100="Length of Address Line 3 must not exceed 100.";
	public static final String KP_Length_of_Address_Line_4_must_not_exceed_100="Length of Address Line 4 must not exceed 100.";
	public static final String KP_Length_of_City_must_not_exceed_100="Length of City must not exceed 100.";
	public static final String KP_Length_of_Bill_To_Address_Line_1_must_not_exceed_100="Length of Bill To Address Line 1 must not exceed 100.";
	public static final String KP_Length_of_Bill_To_Address_Line_2_must_not_exceed_100="Length of Bill To Address Line 2 must not exceed 100.";
	public static final String KP_Length_of_Bill_To_Address_Line_3_must_not_exceed_100="Length of Bill To Address Line 3 must not exceed 100.";
	public static final String KP_Length_of_Bill_To_Address_Line_4_must_not_exceed_100="Length of Bill To Address Line 4 must not exceed 100.";
	public static final String KP_Length_of_Bill_To_City_must_not_exceed_100="Length of Bill To City must not exceed 100.";
	public static final String KP_Invalid_Part_of_the_Company="Invalid Part of the Company.";
	public static final String KP_Invalid_Part_of_the_Company_Name="Invalid Part of the Company Name.";
	public static final String KP_Selected_company_cannot_be_deleted_as_it_is_used_in_another_table="Selected company cannot be deleted as it is used in another table.";
	public static final String KP_Company_already_used="Company already used.";
	public static final String KP_Setup_Selected_Wizard="Setup Selected Wizard";
	public static final String KP_Bill_To_Name20="Bill To Name 20";
	public static final String KP_Bill_To_Name50="Bill To Name 50";
	public static final String KP_Please_enter_part_of_the_company_name="Please enter Part of the Company Name.";
	public static final String KP_Invalid_Company_Id="Invalid Company ID.";
	public static final String KP_Please_enter_company_Id="Please enter Company ID.";
	public static final String KP_Set_up_Date = "Setup Date (YYYY-MM-DD)";	
	public static final String KP_Contact_Email_address_already_exist="Contact Email address already exist.";
	public static final String KP_By_noteslookup="Last Activity Team Member";
	public static final String KP_Note="Note";
	public static final String KP_NotesMaintenance_title="Notes Maintenance";
	public static final String KP_NoteID_NotesMaintenance="Note Id";
	public static final String KP_NoteLink_NotesMaintenance="Note Link";
	public static final String KP_Note_blank_field_error_msg="Note cannot be left blank.";
	public static final String KP_Location_Profile_Lookup="Location Profile Lookup";
	public static final String KP_There_is_no_other_location_profile_for_this_fulfillment_center="There is no other location profile for this fulfillment center.";
	public static final String KP_Location_Profile="Location Profile";
	public static final String KP_Profile_Group="Profile Group";
	public static final String KP_Part_of_Location_Profile_Description="Part of Location Profile Description";
	public static final String KP_Location_Profile_Search_Lookup="Location Profile Search Lookup";
	public static final String KP_Number_of_Locations="Number of Locations";
	public static final String KP_Last_Used_Date="Last Used Date";
	public static final String KP_Last_Used_By="Last Used By";
	public static final String KP_Last_Change_Date="Last Change Date";
	public static final String KP_Last_Change_By="Last Change By";
	public static final String KP_Copy="Copy";
	public static final String KP_Reassign_Locations="Reassign Locations";
	public static final String KP_Last_Assigned_Date_YYYY_MM_DD="Last Assigned Date (YYYY-MM-DD)";
	public static final String KP_Last_Assigned_TeamMember="Last Assigned Team Member";
	public static final String KP_Location_Type="Location Type";
	public static final String KP_Location_Height="Location Height";
	public static final String KP_Location_Width="Location Width";
	public static final String KP_Location_Depth="Location Depth";
	public static final String KP_Location_Weight_Capacity="Location Weight Capacity";
	public static final String KP_Location_Height_Capacity="Location Height Capacity";
	public static final String KP_Number_of_Pallets_That_Fit_In_Location="Number of Pallets That Fit In Location";
	public static final String KP_Number_of_Floor_Pallet_Location="Number of Floor Pallet Location";
	public static final String KP_Allocation_Allowable="Allocation Allowable";
	public static final String KP_Free_Location_When_Zero="Free Location When Zero";
	public static final String KP_Free_Prime_Location_in_99999_Days="Free Prime Location in 99999 Days";
	public static final String KP_Multiple_Products_in_The_Same_Locations="Multiple Products in The Same Locations";
	public static final String KP_Storage_Type="Storage Type";
	public static final String KP_Slotting="Slotting";
	public static final String KP_Location_Check="Location Check";
	public static final String KP_Cycle_Count_Activity_Points="Cycle Count Activity Points";
	public static final String KP_Cycle_Count_High_Value_Amount="Cycle Count High Value Amount";
	public static final String KP_Cycle_Count_High_Value_Factor="Cycle Count High Value Factor";
	public static final String KP_From_Location_Profile="From Location Profile";
	public static final String KP_To_Location_Profile="To Location Profile";
	public static final String KP_Apply="Apply";
	public static final String KP_Location_Profile_Maintenance="Location Profile Maintenance";
	public static final String KP_Must_Reassign_Location_Profile_before_Deletion="Must Reassign Location Profile before Deletion.";
	public static final String KP_Please_select_a_To_Profile_first_to_proceed="Please select a To Profile first to proceed.";
	public static final String KP_There_are_no_locations_to_reassign_for_the_selected_Location_Profile="There are no locations to reassign for the selected Location Profile.";
	public static final String KP_Location_Profile_Already_Exist="Location Profile Already Exist.";
	public static final String KP_Invalid_Location_Profile="Invalid Location Profile.";
	public static final String KP_Invalid_Profile_Group="Invalid Profile Group.";
	public static final String KP_Please_enter_Location_Profile="Please enter Location Profile.";
	public static final String KP_Please_select_Profile_Group="Please select Profile Group.";
	public static final String KP_Invalid_part_of_location_profile_description="Invalid Part of Location Profile Description.";
	public static final String KP_Please_enter_part_of_location_profile_description="Please enter Part of Location Profile Description.";
	public static final String KP_Location_Height_must_be_numeric_with_3_decimal_values_only="Location Height must be numeric only with maximum 3 decimal values.";
	public static final String KP_Location_Width_must_be_numeric_with_3_decimal_values_only="Location Width must be numeric only with maximum 3 decimal values.";
	public static final String KP_Location_Depth_must_be_numeric_with_3_decimal_values_only="Location Depth must be numeric only with maximum 3 decimal values.";
	public static final String KP_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only="Location Weight Capacity must be numeric only with maximum 3 decimal values.";
	public static final String KP_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only="Location Height Capacity must be numeric only with maximum 3 decimal values.";
	public static final String KP_Number_of_Pallets_that_fit_in_the_Location_must_be_numeric_only="Number of Pallets that fit in the Location must be numeric only.";
	public static final String KP_Number_of_Floor_Pallet_Location_must_be_numeric_only="Number of Floor Pallet Location must be numeric only.";
	public static final String KP_Free_Prime_Location_in_99999_Days_must_be_numeric_only="Free Prime Location in 99999 Days must be numeric only.";
	public static final String KP_Cycle_Count_Activity_Points_must_be_numeric_only="Cycle Count Activity Points must be numeric only.";
	public static final String KP_Cycle_Count_High_Value_Amount_must_be_numeric_only="Cycle Count High Value Amount must be numeric only.";
	public static final String KP_Cycle_Count_High_Value_Factor_must_be_numeric_only="Cycle Count High Value Factor must be numeric only.";
	public static final String KP_FulFillment_Center="Fulfillment Center";
	public static final String KP_Please_select_a_From_Profile_first_to_proceed="Please select a From Profile first to proceed.";
	public static final String KP_Location_Search_Lookup="Location Search Lookup";
	public static final String KP_Location="Location";
	public static final String KP_Last_Activity_Task="Last Activity Task";
	public static final String KP_Any_Part_of_Location_Description="Any Part of Location Description";
	public static final String KP_Location_Lookup="Location Lookup";
	public static final String KP_Location_Maintenance="Location Maintenance";
	public static final String KP_Location_Short="Location Short";
	public static final String KP_Location_Long="Location Long";
	public static final String KP_Wizard_ID="Wizard ID";
	public static final String KP_Wizard_Name="Wizard Name";
	public static final String KP_Work_Group_Zone="Work Group Zone";
	public static final String KP_Work_Zone="Work Zone";
	public static final String KP_Check_Digit="Check Digit";
	public static final String KP_Alternate_Location="Alternate Location";
	public static final String KP_Number_of_Pallets="Number of Pallets";
	public static final String KP_Multiple_Products_In_The_Same_Location="Multiple Products In The Same Location";
	public static final String KP_Manual_Assigned_Location="Manual Assigned Location";
	public static final String KP_Used_In_Slotting="Used In Slotting";
	public static final String KP_Invalid_Location="Invalid Location.";
	public static final String KP_Please_select_an_Area="Please select an Area.";
	public static final String KP_Please_select_a_Location_Type_first="Please select a Location Type.";
	public static final String KP_Invalid_Any_Part_of_Location_Description="Invalid Any Part of Location Description.";
	public static final String KP_Please_enter_Location="Please enter Location.";
	public static final String KP_Please_enter_To_Location="Please enter To Location.";//KP_Please_enter_From_Location
	public static final String KP_Please_enter_From_Location="Please enter From Location.";
	public static final String KP_Please_enter_And_Location="Please enter And Location.";
	public static final String KP_Please_enter_Any_Part_of_Location_Description="Please enter Any Part of Location Description.";
	public static final String KP_Location_is_about_to_be_deleted="Location is about to be deleted.";
	public static final String KP_Location_has_Inventory_and_can_not_be_deleted="Location has Inventory and cannot be deleted.";
	public static final String KP_Location_is_still_assigned_and_can_not_be_deleted="Location is still assigned and cannot be deleted.";
	public static final String KP_Locaton_already_Used="Location already Used.";
	public static final String KP_Alternate_Locaton_already_Used="Alternate Location already Used.";
	public static final String KP_Wizard_Control_Number_must_be_numeric_only="Wizard Control Number must be numeric only.";
	public static final String KP_Number_of_Pallets_must_be_numeric_only="Number of Pallets must be numeric only.";
	public static final String KP_Please_Select_Location_Type="Please select Location Type.";
	public static final String KP_Team_Member_Name="Team Member Name";
	public static final String KP_Available_Menu_Profiles="Available Menu Profiles";
	public static final String KP_Menu_Profiles_Assigned="Menu Profiles Assigned";
	public static final String KP_Team_Member_Menu_Profile_Assignment_SEARCH_LOOKUP="Team Member Menu Profile Assignment Search Lookup";
	public static final String KP_Select_Menu_Type="Select Menu Type";
	public static final String KP_Menu_Profiles_Assignment_Maintenance="Menu Profiles Assignment Maintenance";
	public static final String KP_Menu_Profiles_Assignment_Lookup="Menu Profiles Assignment Lookup";
	public static final String KP_No_menu_profile_has_been_assigned_to_the_TeamMember="No menu profile has been assigned to the team member.";
	public static String	KP_Numeric_Allowed_Only_Character_Set="Only numeric and greater than zero values allowed in Starting at and Number of if characters set is Numeric.";
	public static  String 	KP_Only_Alphabets_Allowed="Only alphabets allowed in Starting at and Number of if characters set is Alpha.";
	public static  String KP_Location_WIzard_is_about_to_be_deleted="Location wizard is about to be deleted.";
	public static  String KP_Deletion_will_remove_the_Wizard_from_the_locations="Deletion will remove the wizard from the locations.";
	public static  String KP_Location_WIzard_and_its_Locations_are_about_to_be_deleted="Location wizard and its Locations are about to be deleted.";
	public static String	KP_Ok="Ok";
	public static String	KP_Download="Download";
	public static String	KP_Location_is_greater="Location is greater.";
	public static String	KP_Number_of_characters_must_be_numeric_and_between_1_to_5_only="Number of characters must be numeric and between 1 to 5 only.";
	public static String	KP_Numeric_Allowed_Only="Number of Characters must be numeric and greater than zero.";
	public  static String	KP_ERR_Please_Enter_Seperator_Level_Slot_Print	="Please enter Separator between Level and Slot Print.";
	public  static String	KP_ERR_Please_Enter_Seperator_Level_Slot="Please enter Separator between Level and Slot.";
	public  static String	KP_ERR_Please_Enter_Slot_number_of="Please enter Slot number of.";
	public  static String	KP_ERR_Please_Enter_Slot_starting="Please enter Slot starting.";
	public  static String	KP_ERR_Please_Enter_Slot_character_set="Please select Slot character set.";
	public  static String	KP_ERR_Please_Enter_Seperator_Bay_Level_Print="Please enter Separator between Bay and Level Print.";
	public  static String	KP_ERR_Please_Enter_Seperator_Bay_Level="Please enter Separator between Bay and Level.";
	public  static String	KP_ERR_Please_Enter_LEVEL_number_of="Please enter Level number of.";
	public  static String	KP_ERR_Please_Enter_LEVEL_starting="Please enter Level starting.";
	public  static String	KP_ERR_Please_Enter_LEVEL_character_set="Please select Level character set.";
	public  static String	KP_ERR_Please_Enter_Seperator_Aisle_Bay_Print="Please enter Separator between Aisle and Bay Print.";
	public  static String	KP_ERR_Please_Enter_Seperator_Aisle_Bay="Please enter Separator between Aisle and Bay.";
	public  static String	KP_ERR_Please_Enter_Bay_number_of="Please enter Bay number of.";
	public  static String	KP_ERR_Please_Enter_Bay_starting="Please enter Bay starting.";
	public  static String	KP_ERR_Please_Enter_Bay_character_set="Please select Bay character set.";
	public  static String	KP_ERR_Please_Enter_Seperator_Row_Aisle_Print="Please enter Separator between Row and Aisle Print.";
	public  static String	KP_ERR_Please_Enter_Seperator_Row_Aisle="Please enter Separator between Row and Aisle.";
	public  static String	KP_ERR_Please_Enter_Aisle_number_of="Please enter Aisle number of.";
	public  static String	KP_ERR_Please_Enter_Aisle_starting="Please enter Aisle starting.";
	public  static String	KP_ERR_Please_Enter_Aisle_character_set="Please select Aisle character set.";
	public  static String	KP_ERR_Please_Enter_Row_number_of="Please enter Row number of.";
	public  static String	KP_ERR_Please_Enter_Row_starting="Please enter Row starting.";
	public  static String	KP_ERR_Please_Enter_Row_character_set="Please select Row character set.";
	public static  String 	KP_Numeric_value_can_not_less_than_Zero_or_eual="Numeric value must be greater than zero in Starting at and Number of.";
	public  static String	KP_Level_Number_of_Characters="Level Number of Characters";
	public  static String	KP_Location_Wizard_Maintenance="Location Wizard Maintenance";
	public  static String	KP_Applied_By	=	"Applied By";
	public  static String	KP_Created_Date	=	"Created Date";
	public  static String	KP_Created_By	=	"Created By";
	public  static String	KP_Applied_Date	=	"Applied Date";
	public static String	KP_Row_Number_of_Characters ="Number of Characters:";
	public static String	KP_Row_Characters_Set="Characters Set";
	public static String	KP_Row_Starting_at="Starting at";
	public static String	KP_Number_of_Row="Number of";
	public static String	KP_Separator_Character_Location="Separator Character Location";
	public static String	KP_Separator_Character_Location_Print_only="Separator Character Location Print only";
	public static  String 	KP_Number_of_locations_created="Number of locations created";
	public static  String 	KP_Number_of_duplicate_locations="Number of duplicate locations";
	public static  String 	KP_Print="Print";
	public static  String 	KP_There_is_no_location_profile_for_selected_fulfillment_center="There is no location profile for selected fulfillment center.";
	public static final String KP_Either_no_locations_assigned_or_no_locations_flagged_to_reprint_for_the_selected_area="Either no locations assigned or no locations flagged to reprint for the selected area.";
	public  static String   KP_ERR_INVALID_WIZARD_CONTROL_NUMBER= "Invalid Wizard Control Number.";
	public  static String	KP_ERR_INVALID_AREA="Invalid Area.";
	public  static String	KP_ERR_AREA_LEFT_BLANK="Please select an Area.";
	public  static String	KP_ERR_INVALID_LOCATION_TYPE="Invalid Location Type.";
	public  static String	KP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION="Invalid Any part of Wizard Description.";
	public  static String	KP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK="Please enter Any Part of Wizard Description.";
	public  static String	KP_Any_Part_of_Wizard_Description="Any Part of Wizard Description:";
	public  static String	KP_Location_Wizard_Lookup="Location Wizard Lookup";
	public  static String	KP_Delete_Wizard_Only="Delete Wizard Only";
	public  static String	KP_Delete_Wizard_And_Location="Delete Wizard & Location";
	public  static String	KP_Location_Wizard_Search_Lookup="Location Wizard Search Lookup";
	public static  String KP_Deletion_will_not_remove_these="Deletion will not remove the Locations and Inventory records for these locations :";
	public static  String KP_Locations_and_Inventory_Records="Locations and Inventory Records.";
	/** Key Phrase declaration and initialization end */

	/** Key Phrase declaration and initialization for login screen */
	public static final String KP_Enter_correct_UserId_and_Password="Enter correct UserId and Password.";
	public static final String KP_to_reset_your_password="to reset your password.";
	public static final String KP_Please_enter_a_valid_UserId_and_Password="Please enter a valid UserId and Password.";
	public static final String KP_here="here ";
	public static final String KP_Forgot_Password="Forgot Password ?";
	public static final String KP_Forgot_your_password="Forgot your password ?";
	public static final String KP_Current_Password="Current Password";
	public static final String KP_Create_New_Password="Create New Password";
	public static final String KP_Remember_me_="Remember me";
	public static final String KP_MessageBoard="Message Board";

	/** SmartTaskExecution KP Start**/
    public static String KP_Execution_Group="Execution Group";
	public static String KP_Execution_Type="Execution Type";
	public static String KP_Execution_Device="Execution Device";
	public static String KP_Execution_Name="Execution Name";
	public static String KP_Any_Part_of_the_Execution_Description="Any Part of the Execution Description";
	public static String KP_Buttons="Buttons";
	public static String KP_Executions_Lookup="Executions Lookup";
	public static String KP_Please_select_Application="Please select Application.";
	public static String KP_Please_select_Execution_Group="Please select Execution Group.";
	public static String KP_Please_select_Execution_Type="Please select Execution Type.";
	public static String KP_Please_select_Execution_Device="Please select Execution Device.";
	public static String KP_Please_enter_Execution_Name="Please enter Execution Name.";
	public static String KP_Please_enter_any_part_of_the_Execution_Description="Please enter any part of the Execution Description.";
	public static String KP_Please_enter_Tenant="Please enter Tenant.";
	public static String KP_Please_select_Buttons="Please select Buttons.";
	public static String KP_Invalid_Execution_Group="Invalid Execution Group.";
	public static String KP_Invalid_Execution_Type="Invalid Execution Type.";
	public static String KP_Invalid_Execution_Device="Invalid Execution Device.";
	public static String KP_Invalid_Execution_Name="Invalid Execution Name.";
	public static String KP_Invalid_any_part_of_the_Execution_Description="Invalid any part of the Execution Description.";
	public static String KP_Invalid_Tenant="Invalid Tenant.";
	public static String KP_Invalid_Buttons="Invalid Buttons.";
	public static String KP_Executions_Search_Lookup="Executions Search Lookup";
	public static String KP_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted="Execution is currently assigned to Execution Sequences, this can not be deleted.";
	public static String KP_Executions_Maintenance="Executions Maintenance";
	public static String KP_Execution_Name_already_Used="Execution Name already Used.";
		public static String KP_Work_Flow_ID="Work Flow ID";
		public static String KP_Work_Flow_Type="Work Flow Type";
		public static String KP_Any_Part_of_the_Work_Flow_Description="Any Part of the Work Flow Description";
		public static String KP_Work_Flow_Lookup="Work Flow Lookup";
		public static String KP_Work_Flow_Search_Lookup="Work Flow Search Lookup";
		public static String KP_Work_Flow_Maintenance="Work Flow Maintenance";
		public static String KP_Please_enter_Fulfillment_center="Please enter Fulfillment Center.";
		public static String KP_Please_enter_Work_Flow_ID="Please enter Work Flow ID.";
		public static String KP_Please_enter_Any_Part_of_the_Work_Flow_Description="Please enter Any Part of the Work Flow Description.";
		public static String KP_Please_select_Work_Flow_Type="Please select Work Flow Type.";
		public static String KP_Invalid_Work_Flow_Type="Invalid Work Flow Type.";
		public static String KP_Invalid_part_of_Work_Flow_Description="Invalid part of Work Flow Description.";
		public static String KP_Invalid_Work_Flow_ID="Invalid Work Flow ID.";
		public static String KP_Invalid_Fulfillment_center="Invalid Fulfillment Center.";
		public static String KP_WORK_TYPE="Work Type";
		public static String KP_Flow_ID="Flow ID";
		public static String KP_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data="Execution does not exist in Execution Panel filter data, So you must not add this execution in flow chart panel. Please remove this execution before save flow chart data.";
		/** SmartTaskExecution KP End**
	
		/** SmartTaskConfigurator KP Start**/
		public static String KP_Task="Task";
		public static String KP_Group="Group";		
		public static String KP_Device="Device";
		public static String KP_Sequence="Sequence";
		public static String KP_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted="Smart Task is currently assigned to Execution Sequences, this can not be deleted.";
		public static String KP_Execution_Sequence="Execution Sequence";
		public static String KP_Any_Part_of_the_Execution_Sequence_Description="Any Part of the Execution Sequence Description";
		public static String KP_Menu_Name="Menu Name";
		public static String KP_Smart_Task_Configurator_Lookup="Smart Task Configurator Lookup";
		public static String KP_Please_select_Task="Please select Task.";
		public static String KP_Please_enter_Execution_Sequence="Please enter Execution Sequence.";
		public static String KP_Please_enter_any_part_of_the_Execution_Sequence_Description="Please enter Any Part of the Execution Sequence Description.";
		public static String KP_Please_enter_Menu_Name="Please enter Menu Name.";
		public static String KP_Invalid_Task="Invalid Task.";
		public static String KP_Invalid_Execution_Sequence="Invalid Execution Sequence.";
		public static String KP_Invalid_any_part_of_the_Execution_Sequence_Description="Invalid any part of the Execution Sequence Description.";
		public static String KP_Invalid_Menu_Name="Invalid Menu Name.";
		public static String KP_Smart_Task_Configurator_Search_Lookup="Smart Task Configurator Search Lookup";
		public static String KP_Search_All="Search All";
		public static String KP_Execution_Sequence_Name="Execution Sequence Name";
		public static String KP_Configurator="Configurator";
		public static String KP_Execution="Execution";
		public static String KP_Action="Action";
		public static String KP_Sequence_View="Sequence View";
		public static String KP_Smart_Task_Configurator_Maintenance="Smart Task Configurator Maintenance";
		public static String KP_Execution_Sequence_Type="Execution Sequence Type";
		public static String KP_Go_To_Tag="Go To Tag";
		public static String KP_Execution_Sequence_Name_or_Menu_Name_already_exist="Execution Sequence Name or Menu Name already exist.";
		public static String KP_Please_first_of_all_connect_every_node_via_link="Please first of all connect every node via link."; 
		public static String KP_IF_EXIT="IF EXIT";
		public static String KP_GoTo_Tag="GoTo Tag";
		public static String KP_Save="Save";
		public static String KP_Display_Text="Display Text";
		public static String KP_IF_ERROR="IF ERROR";
		public static String KP_GENERAL_TAG_NAME="GENERAL TAG NAME";
		public static String KP_CUSTOM_EXECUTION_PATH="CUSTOM EXECUTION PATH"; 
		public static String KP_Return_Code="Return Code Value";
		public static String KP_IF_RETURN_CODE="RETURN CODE";
		public static String KP_COMMENT="COMMENT";
		public static String KP_Start_node_already_exist="Start node already exist.";
		public static String KP_End_node_already_exist="End node already exist.";
		public static String KP_You_can_not_delete_Start_node="You can not delete Start node.";
		public static String KP_You_can_not_delete_End_node="You can not delete End node.";
		public static String KP_You_cannot_delete_selected_link="You cannot delete selected link.";
		public static String KP_Please_select_GoTo_Tag_first="Please select GoTo Tag first.";
		public static String KP_All_fields_are_mandatory="All fields are mandatory.";
		public static String KP_Please_enter_display_text="Please enter display text.";
		public static String KP_You_must_not_connect_Start_node_to_End_node_via_link="You are not allowed to connect Start node to End node via link.";
		public static String KP_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one="General tag already exist with same name. Please rename the existing tag first to add another one.";
		public static String KP_Please_select_execution_data_filter_first_in_task_panel="Sorry, we do not have executions for the selected filters on task panel. Please change the filters.";
		public static String KP_Please_connect_every_node_via_link_before_saving_data="Please connect every node via link before saving data.";
		public static String KP_Please_connect_every_node_via_link_before_align_data="Please connect every node via link before align flowchart.";
		public static String KP_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking="Please check all node is connect via sequential linking except goto linking.";
		public static String KP_General_Tag_already_exist_with="General Tag already exist with";
		public static String KP_name_So_Please_provide_another_uniq_general_tag_name="name. So Please provide another unique general tag name.";
		public static String KP_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid="Minimum 1 execution node is mandatory except start and end node for saving flowchart.";
		public static String KP_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid="Minimum 1 execution node is mandatory except start and end node to display sequence view.";
		public static String KP_Please_connect_every_node_via_link_before_see_sequence_grid="Please connect every node via link before see sequence grid.";
		public static String KP_Please_fill_all_mandatory_fields_value_in_task_panel="Please fill all mandatory fields value in task panel.";
		public static String KP_Align="Align";
		public static String KP_Back="Back";
		public static String KP_Reload="Cancel";
		public static String KP_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking = "You are not allowed to create loop between two nodes. Please change your linking.";
		public static String KP_Sequence_Name="Sequence Name";
		public static String KP_Execution_TagName="Execution / Tag Name";
		public static String KP_Refresh="Refresh";
		public static String KP_Close="Close";
		public static String KP_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG="Goto linking must before saving flowchart data for given nodes : IF ERROR, IF EXIT, IF RETURN CODE and GOTO TAG.";
		public static String KP_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG="Goto linking must before align flowchart data for given nodes : IF ERROR, IF EXIT, IF RETURN CODE and GOTO TAG.";
		public static String KP_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel="Please select execution filters type/device/group in task panel before opening execution panel.";
        public static String KP_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG = "Goto linking must before see sequence view grid for given nodes : IF ERROR, IF EXIT, IF RETURN CODE and GOTO TAG.";
        public static String KP_other_nodes_not_connected = "other nodes not connected.";
        public static String KP_AND = "and";
        public static String KP_Please_enter_Display_text = "Please enter Display text";
        public static String KP_Please_enter_Comment = "Please enter Comment";
        public static String KP_Please_enter_General_Tag_Name = "Please enter General Tag Name";
        public static String KP_Please_enter_Custom_Execution_Path = "Please enter Custom Execution Path";
        public static String KP_Custom_Execution_Path_not_valid = "Custom Execution Path not valid";
        public static String KP_Please_enter_different_value_except_CUSTOM_EXECUTION = "Please enter different value except CUSTOM EXECUTION";
        public static String KP_Please_enter_different_value_except_COMMENT = "Please enter different value except COMMENT";
        public static String KP_Please_enter_different_value_except_DISPLAY = "Please enter different value except DISPLAY";
        public static String KP_Please_enter_different_value_except_GENERAL_TAG = "Please enter different value except GENERAL TAG";
        public static String KP_Please_do_not_left_any_node_blank = "Please do not left any node blank";
	
        /**WEB execution labels*/
    	
   	 public static final String KPSCAN="SCAN";		
   		public static final String KPPRODUCTS="PRODUCTS";
   		public static final String KPQUANTITY="QUANTITY";
   		public static final String KPLINES="LINES";
   		public static final String KPPRIORITY="PRIORITY";
   		public static final String KPPICK="Pick";
   		public static final String KPLOCATION="LOCATION";	
   		public static final String KPLOCATION_PRIME="PRIME";
   		public static final String KPTEXT_OK="OK";
   		public static final String KPDESCRIPTION="DESCRIPTION";
   		public static final String KPPRODUCT="PRODUCT";	
   		public static final String KPQUALITY="QUALITY";
   		public static final String KPMESSAGE_CHECK=" Message ";
   		public static final String KPLPN="LPN";
   		public static final String KPITEM ="ITEM";	
   		public static final String KPSCAN_LPN="SCAN LPN";
   		public static final String KPSCAN_ITEM ="SCAN ITEM";	
   		//Error msg
   		public static final String KPERR_MSG_INVALID_SCAN_LOCATION="Invalid Location.";
   		public static final String KPERR_MSG_LPN_ALREADY_ASSIGNED = "LPN Already Assigned";	
   		public static final String KPERR_MSG_INVALID_WORK_TYPE = "Invalid Work Type.";	
   		public static final String KPERR_MSG_INVALIDE_TASK_FOR_SEQUENCE = "Invalid Task for Sequence.";
   		public static final String KPERR_MSG_INVALIDE_LPN = "Invalid LPN.";
   		public static final String KPERR_MSG_INVALIDE_EXECUTION = "Invalid Execution.";
   		public static final String KPERR_MSG_WORK_TYPE_NOT_FOUND = "Work Type not found.";
   		public static final String KPERR_MSG_INTERNET_CONNECTION="Please check internet connection.";
   		public static final String KPERR_MSG_INVALIDE_AREA_LOCATION = "Invalid area and location.";
   		public static final String KPERR_MSG_SERVER_NOT_RESPOND = "Server not responding.";
   		public static final String KPSTRLOADING="Loading...";
   		public static final String KPAPK_UPDATE_MSG="Jasci app updated version available, Do you want to install updated version ?";
   		public static final String KPAPK_UPDATE_YES="Yes";
   		public static final String KPAPK_UPDATE_NO="No";
   		public static final String KPDIRECTION_FORWARD="FORWARD";
   		public static final String KPDIRECTION_LEFT="LEFT";
   		public static final String KPDIRECTION_RIGHT="RIGHT";
   		public static final String KEND="End";
   		public static final String KEXIT="Exit";
   		public static final String KSTART="Start";
   		public static final String KPSCANWORKZONE="SCAN WORK ZONE";
   		public static final String KPERR_INVALIDWORKZONE="Invalid Work Zone";
   		public static final String KPEND="End";
   		public static final String KPEXIT="Exit";
   		public static final String KPSTART="Start";
   		public static final String KPCONTINUE="Continue";
   	
   		public static String KP_Button_Name="Button Name";
   		public static String KP_Smart_Task_Executions_Lookup="Smart Task Executions Lookup";
   		public static final String KP_Y="Y";
   		public static final String KP_N="N";
   		public static String KP_Smart_Task_Executions_Search_Lookup="Smart Task Executions Search Lookup";
   		public static String KP_Smart_Task_Execution_Maintenance="Smart Task Execution Maintenance";
   		public static final String KP_Adding_Record_Failed = "Adding Record Failed.";
   		public static String StrPutWallModule="Put_Wall";
 
   		
   		/** Put Wall section **/
    	public static final String KP_PutWall_Type="Putwall Type";
    	public static final String KP_PutWall_Id="Putwall ID";
    	public static final String KP_SingleOrMulti="Single Or Multi";
    	public static final String KP_NumberOfLevel="Number of Levels";
    	public static final String KP_NumberOfSlots="Number of Slots";
    	public static final String KP_Default="Default";
    	public static final String KP_ContainerType="Container Type";
    	public static final String KP_ContainerId="Container ID";
    	public static final String KP_Priority="Priority";
    	public static final String KP_Section="Section";
    	public static final String KP_Constant_Value="Constant Value";
    	public static final String KP_Create="Create";
    	public static final String KP_Wizard_PutWall_Creation="Wizard PutWall Creation";
    	public static final String KP_Continue="Continue";
   		
   		
   		public static String[] arrExecutionKeyPhraseArray={KPCONTINUE,KPERR_INVALIDWORKZONE,KPSCANWORKZONE,KPEND,KPSTART,KPEXIT,KPSTRLOADING,KPSCAN,KPPRODUCTS,KPQUANTITY,KPLINES,KPPRIORITY,KPPICK,KPLOCATION,KPLOCATION_PRIME,KPTEXT_OK,KPDESCRIPTION,KPPRODUCT,KPQUALITY,KPMESSAGE_CHECK,KPLPN,KPITEM,KPSCAN_LPN,KPSCAN_ITEM,KPERR_MSG_INVALID_SCAN_LOCATION,KPERR_MSG_LPN_ALREADY_ASSIGNED,KPERR_MSG_INVALID_WORK_TYPE,KPERR_MSG_INVALIDE_TASK_FOR_SEQUENCE,KPERR_MSG_INVALIDE_LPN,KPERR_MSG_INVALIDE_EXECUTION,KPERR_MSG_WORK_TYPE_NOT_FOUND,KPERR_MSG_INTERNET_CONNECTION,KPERR_MSG_INVALIDE_AREA_LOCATION,KPERR_MSG_SERVER_NOT_RESPOND,KPAPK_UPDATE_MSG,KPAPK_UPDATE_YES,KPAPK_UPDATE_NO,KPDIRECTION_FORWARD,KPDIRECTION_LEFT,KPDIRECTION_RIGHT};
   	/** End execution labels*/
	/** SmartTaskConfigurator KP End**/
	public static HashMap<String, String> hmOfLoginObjFieldAndKeyPhases= new HashMap<String, String>();
	public static HashMap<String, String> hmOfPasswordExpObjFieldAndKeyPhases= new HashMap<String, String>();
	public static HashMap<String, String> hmOfSecurityAuthorizationObjFieldAndKeyPhases= new HashMap<String, String>();

	public static String[] arrMenuExecutionKeyPhases=
		{KP_Team_Member,KP_Company_ID,KP_MESSAGES,KP_APPLICATIONS,KP_Name,KP_Select_Company_to_Work_With
		};

	public static String[] arrMenuProfileAssignmentKeyPhases=
		{KP_Part_of_the_Team_Member_Name,KP_Team_Member_Id,KP_Available_Menu_Profiles,KP_Menu_Profiles_Assigned
		,KP_Mandatory_field_cannot_be_left_blank,KP_Please_enter_a_Team_Member_ID,KP_Please_enter_part_of_the_Team_Member_Name,
		KP_Invalid_Team_Member_ID,KP_Invalid_Part_of_Team_Member_Name,KP_Delete,KP_Add_New,KP_Actions,KP_Cancel,KP_Save_Update,
		KP_Display_All,KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,KP_Name,
		KP_Select_Menu_Type,KP_Menu_Profile,KP_MenuType,KP_Description,KP_Team_Member_Menu_Profile_Assignment_SEARCH_LOOKUP,KP_Reset,
		KP_Menu_Profiles_Assignment_Lookup,KP_Edit,KP_Company,KP_Menu_Profiles_Assignment_Maintenance,KP_Team_Member,
		KP_No_menu_profile_has_been_assigned_to_the_TeamMember,KP_Select,KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button,
		KP_Sort_Last_Name,KP_Sort_First_Name};

	public static String[] arrSecuritySetupLabel={KP_Select,KP_Reset,KP_Security_Setup_Lookup,KP_Part_of_the_Team_Member_Name,
		KP_Team_Member_Id,KP_Display_All,KP_Invalid_Team_Member_ID,KP_Security_Setup_Search_Lookup,
		KP_Part_of_Team_Member,KP_Name,KP_Edit,KP_Actions,KP_Delete,KP_Are_you_sure_you_want_to_delete_this_record,
		KP_You_are_not_allowed_to_edit_as_this_team_member_is_not_active,
		KP_You_are_not_allowed_to_delete_this_team_member_as_it_does_not_exist_in_security_autorization_table,
		KP_Please_enter_part_of_the_Team_Member_Name,KP_Please_enter_a_Team_Member_ID,KP_Sort_Last_Name,
		KP_Team_Member_Id,KP_Sort_First_Name,KP_Security_Setup_Maintenance,KP_Last_Activity_Date,
		KP_Last_Activity_By,KP_User_ID,KP_Tenant,KP_Company,KP_Current_Password,KP_Security_Question1,KP_Security_Question_Answer1,
		KP_Security_Question2,KP_Security_Question_Answer2,KP_Security_Question3,KP_Security_Question_Answer3,KP_Last_Password1,
		KP_Last_Password2,KP_Last_Password3,KP_Last_Password4,KP_Last_Password5,KP_Number_of_Last_Attempts,
		KP_Last_Invalid_Date_YY_MM_DD,KP_Save_Update,KP_Mandatory_field_cannot_be_left_blank,
		KP_UserID_must_be_of_atleast_6_characters_and_must_include_one_number,KP_UsedID_already_used,
		KP_All_Security_questions_must_be_unique,KP_Selected_record_has_been_deleted_successfully,KP_Cancel,
		KP_Mandatory_field_cannot_be_left_blank,KP_Mandatory_field_cannot_be_left_blank,
		KP_Mandatory_field_cannot_be_left_blank,KP_Your_record_has_been_updated_successfully,
		KP_Your_record_has_been_added_successfully,KP_Status,KP_You_are_not_allowed_to_edit_as_this_team_member_is_not_active,
		KP_Invalid_Part_of_Team_Member_Name,KP_Team_Member_Name};

	public static String[] arrLoginSignOn={KP_Login_to_your_account,KP_MessageBoard,KP_Enter_correct_UserId_and_Password,
		KP_UserID,KP_Password,KP_Remember_me_,KP_Login_,KP_Forgot_your_password,KP_no_worries_click,KP_here,
		KP_to_reset_your_password,KP_Forgot_Password,KP_Enter_your_e_mail_address_below_to_reset_your_password,KP_Cancel,
		KP_Submit,KP_Please_enter_a_valid_UserId_and_Password,KP_Invalid_Sign_on_Please_contact_Tenant_s_security_officer,
		KP_You_are_not_allowed_to_login_as_the_company_you_are_associated_with_is_inactive_Please_contact_administrator};

	public static String[] arrExpireLoginPassword={KP_Create_New_Password,KP_New_Password,KP_Submit,KP_Confirm_Password,
		KP_Your_Password_has_been_expired_Please_create_new_password,KP_New_password_and_confirm_password_do_not_match,
		KP_Password_must_be_of_atleast_8_characters_including_one_capital_letter_and_three_numbers,
		KP_Password_cant_be_reused_in_the_past_5_times,
		KP_Your_Password_is_temporary_Please_create_new_password,KP_Cancel};


	public static String[] arrSecurityAuthorizationKeyPhases=
		{KP_Reset,KP_Security_Setup_Lookup,KP_Invalid_Team_Member_ID,KP_Team_Member_Id,
		KP_Edit,KP_Last_Password1,KP_Last_Password2,KP_You_are_not_allowed_to_edit_as_this_team_member_is_not_active,
		KP_You_are_not_allowed_to_edit_as_this_team_member_is_not_active,KP_Security_Question_Answer2,
		KP_Last_Password3,KP_Your_record_has_been_updated_successfully,KP_Status,KP_All_Security_questions_must_be_unique,
		KP_Are_you_sure_you_want_to_delete_this_record,KP_Sort_Last_Name,KP_Security_Setup_Maintenance,
		KP_Security_Question1,KP_Security_Question_Answer3,KP_Last_Password5,
		KP_UserID_must_be_of_atleast_6_characters_and_must_include_one_number,KP_UsedID_already_used,KP_Team_Member_Id,
		KP_Part_of_the_Team_Member_Name,KP_Please_enter_a_Team_Member_ID,KP_Sort_First_Name,KP_Last_Activity_Date,
		KP_Tenant,KP_Last_Password4,KP_Number_of_Last_Attempts,KP_Cancel,KP_Display_All,KP_Part_of_Team_Member,
		KP_Name,KP_Security_Setup_Search_Lookup,KP_Please_enter_part_of_the_Team_Member_Name,
		KP_User_ID,KP_Invalid_Part_of_Team_Member_Name,KP_Last_Invalid_Date_YY_MM_DD,KP_Your_record_has_been_added_successfully,
		KP_Actions,KP_Company,KP_Current_Password,KP_Mandatory_field_cannot_be_left_blank,KP_Selected_record_has_been_deleted_successfully,
		KP_Save_Update,KP_Delete,KP_You_are_not_allowed_to_delete_this_team_member_as_it_does_not_exist_in_security_autorization_table,
		KP_Last_Activity_By,KP_Security_Question_Answer1};

	public static String[] arrfulfillmentCenterKeyPhases = { KP_Reset,KP_Select,KP_Enter_Fulfillment,
		KP_Part_of_the_Fulfillment_Center_Name, KP_New, KP_Display_All, KP_Fullfillment_Centers_Lookup,
		KP_Fullfillment_Centers_Search_Lookup, KP_Fulfillment_Center, KP_Name, KP_Edit, KP_Delete, KP_Add_New,
		KP_Fulfillment_Center_Maintenance, KP_Fulfillment_Center, KP_Setup_Selected_Wizard, KP_Set_up_Date, KP_Setup_By,
		KP_Last_Activity_Date, KP_Name_Short, KP_Name_Long, KP_Address_Line_1, KP_Address_Line_2,
		KP_Address_Line_3, KP_Address_Line_4, KP_City, KP_Country_Code, KP_State, KP_Zip_Code, KP_Contact_Name,
		KP_Contact_Extension, KP_Contact_Cell, KP_Contact_Fax, KP_Contact_Email, KP_Contact_Name_2,
		KP_Contact_Extension_2, KP_Contact_Cell_2, KP_Contact_Fax_2, KP_Contact_Email_2, KP_Contact_Name_3,
		KP_Contact_Extension_3, KP_Contact_Cell_3, KP_Contact_Fax_3, KP_Contact_Email_3, KP_Contact_Name_4,
		KP_Contact_Extension_4, KP_Contact_Cell_4, KP_Contact_Fax_4, KP_Contact_Email_4, KP_Main_Fax, KP_Main_Email,
		KP_Save_Update, KP_Cancel, KP_Mandatory_field_cannot_be_left_blank, KP_Fulfillment_center_already_used,
		KP_Your_record_has_been_saved_successfully, KP_Not_a_valid_number, KP_Not_a_valid_email_id,
		KP_Are_you_sure_you_want_to_delete_this_record,
		KP_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table,
		KP_Selected_fulfillment_center_deleted_successfully, KP_Invalid_Fulfillment_Center,
		KP_Invalid_Part_of_the_fulfillment,KP_Contact_Phone,KP_Contact_Phone_2,KP_Contact_Phone_3,KP_Contact_Phone_4,
		KP_Your_record_has_been_updated_successfully,KP_Last_Activity_By,KP_Please_enter_only_numeric_values,KP_Please_enter_valid_Email_Address,
		KP_Please_enter_Fulfillment_center_ID,KP_Please_enter_Part_of_Fulfillment_Center_Name,KP_Contact,KP_Actions,
		KP_Invalid_Address,KP_Invalid_Zip_Code,KP_Fulfillment_Center_ID_already_used,KP_Fulfillment_Center_aready_exist_with_inactive_status
		,KP_Name_Short_already_used,KP_Name_Long_already_used,KP_Mandatory_field_cannot_be_left_blank_and_must_be_numeric_only,KP_Mandatory_field_cannot_be_left_blank_and_must_be_valid_email_address
	};


	public static String arrMenuOptionKeyPhares[]={KP_OR,KP_Part_of_the_Application_Name,KP_Part_of_the_Menu_Option_Description,
		KP_Part_of_the_Menu_Profile_Description,KP_Select,KP_SelectApplication,     KP_Sub_Application,     
		KP_Tenant_ID,KP_Application,KP_Description,KP_Display_All,KP_New,KP_Last_Activity_By,
		KP_Menu_Option,KP_Invalid_Part_Of_The_Application_Name,     KP_Invalid_Part_Of_The_Menu_Option_Description,    
		KP_Invalid_Part_Of_The_Menu_Option,     KP_Invalid_Menu_Option,     KP_Invalid_Application,KP_Description,    
		KP_Description_Long,     KP_Description_Short,KP_APPLICATIONS,
		KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button,     KP_Add,
		KP_Menu_Option_Maintenance_LookUp,KP_Menu_Option_Search_LookUp,
		KP_Menu_Option_already_used,KP_Invalid_Tenant_ID,KP_Please_enter_Tenant_ID,
		KP_Menu_Option_is_currently_assigned_to_Menu_Profiles_if_Deleted_this_option_will_be_removed_from_all_Menu_Profiles,KP_Edit ,
		KP_Delete,KP_New,KP_Actions,KP_Last_Activity_Date ,KP_Save_Update,
		KP_Help_Line,KP_Please_select_Menu_Type,KP_Please_enter_Part_Of_the_Application,
		KP_Please_enter_Part_Of_the_Menu_Option,KP_Please_enter_Application,KP_Please_enter_Menu_Option,KP_MenuType,KP_Tenant,
		KP_Menu_Option_Mantenance,KP_Menu_Option_Maintenance_LookUp,KP_Execution_Path,
		KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button,KP_Invalid_Menu_Type,
		KP_Please_enter_Part_Of_The_Menu_Option_Description,KP_Add_New,KP_Last_Activity_Date,KP_Reset,
		KP_Mandatory_field_cannot_be_left_blank,KP_Cancel,KP_Please_enter_valid_Execution_Path,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully};

	public static String arrMenuProfileKeyPharses[]={KP_Menu_Profile_is_currently_assigned_to_Menu_Assignments_if_Deleted_this_option_will_be_removed_from_all_Menu_Assignments,
		KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button,
		KP_Your_record_has_been_updated_successfully,KP_Your_record_has_been_saved_successfully,
		KP_APPLICATIONS,KP_Invalid_Menu_Type,KP_Select,KP_Menu_App_Icon,KP_MenuType,KP_Display_All,
		KP_Application,KP_Save_Update,KP_SelectApplication,KP_Part_of_the_Menu_Profile_Description,KP_OR,KP_New,
		KP_Menu_Profiles_Options,KP_Menu_Profiles_Maintenance_Search_Lookup,KP_Menu_Profiles_Maintenance_Lookup,
		KP_Menu_Profiles_Maintenance,KP_Menu_Profile,KP_Menu_Option,KP_Last_Activity_Date,KP_Last_Activity_By,
		KP_Help_Line,KP_Please_enter_Part_of_Menu_Profile_Description,KP_Please_select_Menu_Type,KP_Please_enter_Menu_Profile,
		KP_Menu_Profile_already_used,KP_Mandatory_field_cannot_be_left_blank,KP_Invalid_Part_Of_Menu_Profile_Description,
		KP_Invalid_Menu_Profile,KP_Edit,KP_Description_Short,KP_Description_Long,KP_Description,KP_Delete,KP_Cancel,KP_Reset,
		KP_Available_Menu_Options,KP_Add_New,KP_Actions,KP_New,KP_Sub_Application,KP_No_menu_option_has_been_assigned_to_the_profile};


	public static String []arrLoginScreenKeyPhases={KP_Cancel,KP_Remember_me_,KP_Enter_correct_UserId_and_Password,
		KP_Enter_your_e_mail_address_below_to_reset_your_password,KP_Email_address_you_have_entered_does_not_exist,KP_Forgot_Password,KP_Forgot_your_password,
		KP_Login_,KP_Login_to_your_account,KP_Invalid_Sign_on_Please_contact_Tenant_s_security_officer,
		KP_Please_enter_a_valid_UserId_and_Password,KP_MessageBoard,KP_Password,KP_Submit,KP_UserID,
		KP_Your_Password_has_been_expired,KP_here,KP_no_worries_click,KP_to_reset_your_password};

	public static String []arPasswordExpiryScreenKeyPhases={KP_Cancel,KP_Confirm_Password,KP_Create_New_Password,KP_New_Password,KP_New_password_and_confirm_password_do_not_match,KP_Password_cant_be_reused_in_the_past_5_times,KP_Password_must_be_of_atleast_8_characters_including_one_capital_letter_and_three_numbers,KP_Submit
		,KP_Update,KP_Your_Password_has_been_expired,KP_Your_Password_has_been_expired_Please_create_new_password,KP_Your_Password_is_temporary_Please_create_new_password};


	/** Menu App icon maintenance  */
	public static String [] arrMenuAppIcon={KP_Reset,KP_App_Icon,KP_YY_MM_DD,KP_Actions,KP_Add_New,KP_Any_Part_of_the_App_Icon,KP_App_Icon,KP_App_Icon_Address,
		KP_App_Icon_already_used,KP_Application_already_used,KP_Are_sure_they_want_to_delete_this_App_Icon,KP_Cancel,KP_Choose_File,
		KP_Delete,KP_Description,KP_Description_Long,KP_Description_Short,KP_Display_All,KP_Edit,KP_App_Icon,KP_Invalid_App_Icon_Address,
		KP_Invalid_App_Icon,KP_Invalid_Part_of_the_App_Icon_Name,KP_Invalid_Part_of_the_App_Icon,KP_Last_Activity_By,KP_Last_Activity_Date,
		KP_Loading,KP_Mandatory_field_cannot_be_left_blank,KP_Menu_App_Icon_LooKup,KP_Menu_App_Icon_Maintenance,KP_Menu_App_Icon_Search_LooKup,
		KP_New,KP_Part_of_the_App_Icon_Name,KP_Part_of_the_Description,KP_Please_enter_a_App_Icon,KP_Please_enter_part_of_the_App_Icon_Name,
		KP_Please_enter_part_of_the_App_Icon,KP_Preferred_Application,KP_Save_Update,
		KP_Select,KP_Supported_formats_are_Jpeg_jpg_png_gif,KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully};

	/** team member maintenance  */


	public static String [] arrTeamMemberMaintenanceKeyPhrases={KP_Reset,KP_Name,KP_Team_Member_Id,KP_Actions,KP_Delete,KP_ShiftCode_does_not_exist,KP_Language_does_not_exist,
		KP_Start_date_must_be_before_End_date,KP_Last_date_must_be_after_Start_date,KP_Please_enter_valid_Email_Address,
		KP_Work_Cell_must_be_numeric_only,KP_Work_Fax_must_be_numeric_only,KP_Cell_must_be_numeric_only,
		KP_Emergency_Home_Phone_must_be_numeric_only,KP_Emergency_Cell_must_be_numeric_only,KP_Department,KP_Your_record_has_been_saved_successfully,
		KP_Your_record_has_been_updated_successfully,KP_Please_select_atleast_one_company_to_proceed,
		KP_Work_Extension_must_be_numeric_only,KP_Select,KP_Please_enter_a_Team_Member_ID,KP_Please_enter_part_of_the_Team_Member_Name,
		KP_First_Name,KP_Last_Name,KP_Middle_Name,KP_Address_Line_1,KP_Address_Line_2,KP_Address_Line_3,KP_Address_Line_4,KP_City,
		KP_Country_Code,KP_State,KP_Zip_Code,KP_Work_Phone,KP_Work_Extension,KP_Work_Cell,KP_Work_Fax,KP_Invalid_Team_Member_ID,
		KP_Invalid_Part_of_Team_Member_Name,KP_Work_Email,KP_Edit,KP_Sort_First_Name,KP_Sort_Last_Name,KP_Add_New,KP_Mandatory_field_cannot_be_left_blank,
		KP_Team_member_already_Used,KP_Work_Phone_must_be_numeric_only,KP_Department_does_not_exist,KP_Email_address_already_exist,
		KP_Home_Phone_must_be_numeric_only,KP_Invalid_Zip_Code,KP_Invalid_Address,KP_Are_you_sure_want_to_delete,KP_You_are_not_allowed_to_delete_as_this_team_member_is_already_Inactive,
		KP_NO,KP_YES,KP_Status,KP_YY_MM_DD,KP_Leave_of_Absence,KP_Not_Active,KP_Active,KP_Deleted,
		KP_System_Use,KP_Cancel,KP_Team_Members_Lookup,KP_Part_of_Team_Member_Name,KP_New,KP_Search,KP_Display_All,
		KP_Team_Members_Maintenance,KP_Team_Member_Id,KP_Set_up_Date,KP_Setup_By,KP_Last_Activity_Date,KP_Last_Activity_By,
		KP_Currrent_Status,KP_Home_Phone,KP_Cell,KP_Email,KP_Emergency_Contact_Name,KP_Emergency_Home_Phone,KP_Emergency_Cell,
		KP_Emergency_Email,KP_Department,KP_Shift_Code,KP_Language,KP_Start_Date_YYYY_MM_DD,KP_Last_Date_YYYY_MM_DD,KP_Company_ID,KP_Company_Name,
		KP_Save_Update,KP_Notes,KP_Team_Members_Search_Lookup,KP_Mandatory_field_cannot_be_left_blank_and_must_be_numeric_only,KP_Mandatory_field_cannot_be_left_blank_and_must_be_valid_email_address};

	/** location label print array of key phrases*/

	public static String[] arrLocationLabelPrintKeyPhrases={KP_Label_Select,
		KP_Select,KP_Location_Label_Selection,KP_Reprint_Flagged_Locations,KP_Area,KP_Wizard_Control_Number,
		KP_From_Location,KP_And_Location,KP_From_Area,KP_To_Location,KP_Label_Type,KP_Barcode_Type,KP_Barcode_Type3of9,
		KP_Barcode_Type128,KP_Print_Labels,KP_Lookup_All_with_Selection,KP_OR,KP_Please_select_an_Area,
		KP_Please_enter_Location,KP_Please_enter_Wizard_Control_Number,KP_Please_select_from_Area,
		KP_Please_enter_Location,KP_Please_select_an_Area,KP_Please_select_Label_Type,KP_Please_select_Barcode_Type,
		KP_Wizard_Control_Number_must_be_numeric_only,KP_Submit,KP_Cancel,KP_Description,KP_Wizard_ID,KP_Please_select_Wizard_Control_Number,KP_Invalid_To_Location,
		KP_Invalid_And_Location,KP_Invalid_From_Location,KP_ERR_INVALID_WIZARD_CONTROL_NUMBER,KP_Wizard_Location,
		KP_Either_no_locations_assigned_or_no_locations_flagged_to_reprint_for_the_selected_area,
		KP_FulFillment_Center,KP_Please_Select_Fullfillment_Center,KP_Wizard_Control_Number_not_found_for_selected_Fulfillment_Center,
		KP_Please_enter_To_Location,KP_Please_enter_And_Location,KP_Please_enter_From_Location

	};


	/** Company maintenance array of key phrase */

	public static String [] arrCompanyMaintenanceKeyPhrases={KP_Reset,KP_Company_already_exist_with_inactive_status,KP_Invalid_Zip_Code,KP_Invalid_bill_to_zip_code,KP_Invalid_Address,
		KP_Invalid_bill_to_address,KP_Invalid_email_address,KP_Only_numeric__value_allowed,KP_Part_of_the_Company_Name,
		KP_Company_ID,KP_Name_Short,KP_Name_Long,KP_Address_Line_1,KP_Address_Line_2,KP_Address_Line_3,KP_Address_Line_4,
		KP_City,KP_State,KP_Country_Code,KP_Zip_Code,KP_Supported_formats_are_Jpeg_jpg_png_gif,KP_Choose_File,KP_Contact,
		KP_Loading,KP_Bill_To_Name20,KP_Bill_To_Name50,KP_Bill_To_Address_Line_1,KP_Bill_To_Address_Line_2,KP_Bill_To_Address_Line_3,
		KP_Bill_To_Address_Line_4,KP_Bill_To_City,KP_Bill_To_State_Code,KP_Bill_To_Country_Code,KP_Bill_To_Zip_Code,KP_Contact_Name,
		KP_Contact_Phone,KP_Contact_Extension,KP_Contact_Cell,KP_Contact_Fax,KP_Contact_Email,KP_Contact_Name_2,KP_Contact_Phone_2,
		KP_Contact_Extension_2,KP_Contact_Cell_2,KP_Contact_Fax_2,KP_Contact_Email_2,KP_Contact_Name_3,KP_Contact_Phone_3,KP_Contact_Extension_3,
		KP_Contact_Cell_3,KP_Contact_Fax_3,KP_Contact_Email_3,KP_Contact_Name_4,KP_Contact_Phone_4,KP_Contact_Extension_4,KP_Contact_Cell_4,
		KP_Contact_Fax_4,KP_Contact_Email_4,KP_Main_Fax,KP_Main_Email,KP_Billing_Control_Number,KP_Theme_Mobile,KP_Theme_RF,KP_Theme_Full_Display,
		KP_Logo,KP_Purchase_Order_Require_Approval,KP_Status,KP_Set_up_Date,KP_Setup_By,KP_Time_Zone,KP_Last_Activity_Date,
		KP_Last_Activity_By,KP_Add_New,KP_Cancel,KP_Actions,KP_Delete,KP_Edit,KP_Display_All,KP_Company_Lookup,KP_Company_Maintenance,
		KP_Company_Search_Lookup,KP_Mandatory_field_cannot_be_left_blank,KP_At_least_one_field_is_required_for_search,KP_Please_enter_company_Id,
		KP_Please_enter_part_of_the_company_name,KP_Invalid_Company_Id,KP_Length_of_Address_Line_1_must_not_exceed_100,
		KP_Length_of_Address_Line_2_must_not_exceed_100,KP_Length_of_Address_Line_3_must_not_exceed_100,KP_Length_of_Address_Line_4_must_not_exceed_100,
		KP_Length_of_City_must_not_exceed_100,KP_Length_of_Bill_To_Address_Line_1_must_not_exceed_100,KP_Length_of_Bill_To_Address_Line_2_must_not_exceed_100,
		KP_Length_of_Bill_To_Address_Line_3_must_not_exceed_100,KP_Length_of_Bill_To_Address_Line_4_must_not_exceed_100,KP_Length_of_Bill_To_City_must_not_exceed_100,
		KP_Invalid_Part_of_the_Company,KP_Invalid_Part_of_the_Company_Name,KP_Selected_company_cannot_be_deleted_as_it_is_used_in_another_table,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,KP_Are_you_sure_you_want_to_delete_this_record,
		KP_Company_already_used,KP_New,KP_Name,KP_Setup_Selected_Wizard,KP_Save_Update,KP_Company,KP_Select,KP_YES,KP_NO,
		KP_Contact_Email_address_already_exist,KP_Mandatory_field_cannot_be_left_blank_and_must_be_numeric_only,
		KP_Mandatory_field_cannot_be_left_blank_and_must_be_valid_email_address,KP_Company_Name
		};

	public static String[] arrLanguageTranslationKeyPhrases = {KP_Select,KP_Language,KP_Language_Translation_Lookup,
		KP_English_Key_Phrase,KP_Key_Phrase,KP_Translation,KP_Language_Translation_Search_Lookup,KP_Language_Translation_Maintenance,
		KP_Invalid_Language,KP_Invalid_Key_Phrase, KP_Please_select_Language,KP_Please_enter_KeyPhrase,KP_Already_Used,
		KP_Mandatory_field_cannot_be_left_blank,KP_Edit,KP_Delete,KP_Actions,KP_Add_New,KP_Display_All,KP_New,KP_Last_Activity_Date,
		KP_Last_Activity_By,KP_Save_Update,KP_Cancel,KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,
		KP_Are_you_sure_you_want_to_delete_this_record,KP_Reset};


	public static String[] arrTeammemberMessagesKeyPhrases = {KP_Please_select_a_team_member_to_proceed,KP_Select,KP_Message,
		KP_Part_of_the_Team_Member_Name,KP_Department,KP_Shift,KP_Search,KP_Display_All,KP_Team_Member_Selection,
		KP_Start_Date,KP_End_Date,KP_Ticket_Tape_Message,
		KP_Save_Update,KP_Name,KP_Sort_First_Name,KP_Sort_Last_Name,KP_Message_All_Selected,KP_Team_Member_Id,KP_Type,KP_Actions,
		KP_Edit,KP_Delete,KP_Mandatory_field_cannot_be_left_blank,KP_End_date_cannot_be_older_then_start_date,KP_Your_record_has_been_saved_successfully,
		KP_Invalid_Team_Member_ID,KP_Invalid_Part_of_Team_Member_Name,KP_Invalid_Department,KP_Invalid_Shift,KP_Please_enter_Team_Member_ID,
		KP_Please_enter_part_of_the_Team_Member_Name,KP_Please_select_Department,KP_Please_select_Shift,KP_Please_enter_either_Ticket_Tape_Message_or_Message,
		KP_Are_you_sure_you_want_to_delete_this_message,KP_SEARCH_LOOKUP,KP_Cancel,KP_Your_record_has_been_updated_successfully,
		KP_Active_Team_Member_Messages,KP_Company,KP_Team_Member,KP_Add_New,KP_Team_Member_Message_Lookup,KP_Reset,
		KP_Team_Member_Message_Maintenance_New_Edit,KP_Team_Member_Search_Lookup,KP_Start_Date_YYYY_MM_DD,KP_End_Date_YYYY_MM_DD,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully
		};


	/* Menu Message*/

	public static String arrMenuMessage[]={
		KP_Menu_Messages_Search_Lookup,KP_Start_Date,KP_End_Date,KP_End_Date_YYYY_MM_DD,KP_Type,KP_Message,KP_Actions,KP_Edit,KP_Delete,KP_Add_New,
		KP_Start_date_cannot_be_older_then_current_date,KP_End_date_cannot_be_older_then_start_date,KP_Are_you_sure_you_want_to_delete_this_record,
		KP_Please_enter_either_Ticket_Tape_Message_or_Message,KP_Message_Already_Used,KP_Mandatory_field_cannot_be_left_blank,
		KP_Menu_Messages_Maintenance,KP_Ticket_Tape_Message,KP_Save_Update,KP_Companies,KP_Cancel,KP_Start_Date_YYYY_MM_DD,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,KP_Company_Name,KP_Company_ID,
		KP_Please_select_atleast_one_company_to_proceed,KP_Reset
	};

	/* Menu Message*/

	/*InfoHelps start*/

	public static String arrInfohelps[]={
		KP_Part_of_the_Description,KP_Info_Help_Type,KP_Info_Help_Assignment_Search,
		KP_Display_All,KP_Invalid_Description,KP_Please_enter_Info_Help,KP_Please_select_Info_Help_Type,
		KP_Invalid_Info_Help_Type,KP_Invalid_Info_Help,KP_Please_enter_Part_of_the_Description,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,
		KP_Infohelps_already_used,KP_Mandatory_field_cannot_be_left_blank,KP_Execution_Path,KP_Description_Long,
		KP_Description_Short,KP_Language,KP_Info_Help,KP_Last_Activity_By,KP_Last_Activity_Date,KP_Internal_Error
		,KP_InfoHelp_Assignment_Maintenance,KP_Select,KP_Cancel,KP_Save_Update,KP_Edit,KP_Delete,
		KP_Actions,KP_Description,KP_InfoHelp_Assignment_Search_Lookup,KP_Add_New,
		KP_Are_you_sure_you_want_to_delete_this_record,KP_Choose_File,KP_Loading,KP_Supported_formats_is_pdf,KP_New,KP_Reset
	};

	/* InfoHelps end*/


	/* General Code*/

	public static String arrGeneralCodes[]={
		KP_Code_Identification,KP_Edit,KP_Delete,KP_General_Codes,KP_System_Use,KP_SYSTEM,
		KP_Are_you_sure_you_want_to_delete_this_record,KP_Company,KP_Description,KP_Actions,
		KP_Add_New,KP_Save_Update,KP_Cancel,KP_Application,KP_General_Code_Id,KP_General_Code,
		KP_General_Code,KP_Description_Short,KP_Description_Long,KP_MenuOptionName,KP_Control01_Description,KP_Control01_Value,
		KP_Control02_Description,KP_Control02_Value,KP_Control03_Description,KP_Control03_Value,KP_Control04_Description,
		KP_Control04_Value,KP_Control05_Description,KP_Control05_Value,KP_Control06_Description,KP_Control06_Value,
		KP_Control07_Description,KP_Control07_Value,KP_Control08_Description,KP_Control08_Value,KP_Control09_Description,
		KP_Control09_Value,KP_Control10_Description,KP_Control10_Value,KP_Help,KP_General_Codes_MainTenance,KP_Select,
		KP_GENERALCODES,KP_YES,KP_NO,KP_General_code_or_Menu_name_already_used,KP_General_code_already_used,
		KP_Mandatory_field_cannot_be_left_blank,KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,
		KP_Description_long_already_used,KP_Description_Short_already_used,KP_Menu_Option_already_used,KP_Reset
	};

	/**Note screen*/
	public static String[] arrNotesScreenKeyPhases={KP_Notes,KP_Add_New,KP_Actions,KP_Edit,
		KP_Delete,KP_Last_ActivityDate,KP_By_noteslookup,KP_Note,KP_NotesMaintenance_title,KP_NoteID_NotesMaintenance,
		KP_NoteLink_NotesMaintenance,KP_Last_Activity_Date,KP_Last_Activity_By,KP_Note,KP_Save_Update,
		KP_Cancel,KP_Are_you_sure_you_want_to_delete_this_record,KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,KP_Note_blank_field_error_msg,KP_Reset,
		KP_CONTINUATIONS_SEQUENCE,KP_Add
	};

	/** Location ProfileMiantenance */

	public static String[] arrLocationProfileKeyPhrases = {KP_Location_Profile_Lookup,KP_Location_Profile,KP_Profile_Group,KP_Part_of_Location_Profile_Description,
		KP_New,KP_Display_All,KP_Location_Profile_Search_Lookup,KP_Description,KP_Number_of_Locations,KP_Last_Used_Date,KP_Last_Used_By,KP_Last_Change_Date,
		KP_Last_Change_By,KP_Actions,KP_Add_New,KP_Copy,KP_Notes,KP_Edit,KP_Delete,KP_Reassign_Locations,KP_Last_Activity_By,KP_Last_Activity_Date,
		KP_Last_Assigned_Date_YYYY_MM_DD,KP_Last_Assigned_TeamMember,KP_FulFillment_Center,	KP_Description_Short,KP_Description_Long,KP_Location_Type,KP_Location_Height,
		KP_Location_Width,KP_Location_Depth,KP_Location_Weight_Capacity,KP_Location_Height_Capacity,KP_Number_of_Pallets_That_Fit_In_Location,KP_Number_of_Floor_Pallet_Location,
		KP_Allocation_Allowable,KP_Free_Location_When_Zero,KP_Free_Prime_Location_in_99999_Days,KP_Multiple_Products_in_The_Same_Locations,KP_Storage_Type,KP_Slotting,KP_Location_Check,
		KP_Cycle_Count_Activity_Points,KP_Cycle_Count_High_Value_Amount,KP_Cycle_Count_High_Value_Factor,KP_Save_Update,KP_Cancel,KP_From_Location_Profile,KP_To_Location_Profile,
		KP_Apply,KP_Location_Profile_Maintenance,KP_Must_Reassign_Location_Profile_before_Deletion,KP_Please_select_a_To_Profile_first_to_proceed,
		KP_There_are_no_locations_to_reassign_for_the_selected_Location_Profile,KP_Are_you_sure_you_want_to_delete_this_record,KP_Location_Profile_Already_Exist,
		KP_Mandatory_field_cannot_be_left_blank,KP_Invalid_Location_Profile,KP_Please_enter_Location_Profile,KP_Please_select_Profile_Group,KP_Invalid_Profile_Group,
		KP_Invalid_part_of_location_profile_description,KP_Please_enter_part_of_location_profile_description,KP_Location_Height_must_be_numeric_with_3_decimal_values_only,
		KP_Location_Width_must_be_numeric_with_3_decimal_values_only,	KP_Location_Depth_must_be_numeric_with_3_decimal_values_only,KP_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only,
		KP_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only,KP_Number_of_Pallets_that_fit_in_the_Location_must_be_numeric_only,
		KP_Number_of_Floor_Pallet_Location_must_be_numeric_only,KP_Free_Prime_Location_in_99999_Days_must_be_numeric_only,
		KP_Cycle_Count_Activity_Points_must_be_numeric_only,KP_Cycle_Count_High_Value_Amount_must_be_numeric_only,
		KP_Cycle_Count_High_Value_Factor_must_be_numeric_only,KP_Select,
		KP_Please_select_a_From_Profile_first_to_proceed,KP_There_is_no_other_location_profile_for_this_fulfillment_center,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,
		KP_Description_long_already_used,KP_Description_Short_already_used,KP_Reset,
		KP_Location_Weight_Capacity_must_be_less_then_or_equal_to_Location_Width,
		KP_Location_Height_Capacity_must_be_less_then_or_equal_to_Location_Height

	};

	/** Location Maintenance*/
	public static String arrLocationMaintenance[]={
		KP_Location_Search_Lookup,KP_Area,KP_FulFillment_Center,KP_Location,KP_Description,KP_Location_Type,
		KP_Last_Activity_Task,KP_Copy,KP_Notes,KP_Any_Part_of_Location_Description,KP_Location_Lookup,
		KP_Location_Maintenance,KP_Location_Short,KP_Location_Long,KP_Location_Profile,
		KP_Wizard_ID,KP_Wizard_Control_Number,KP_Work_Group_Zone,KP_Work_Zone,KP_Check_Digit,
		KP_Alternate_Location,KP_Location_Height,KP_Location_Width,KP_Location_Depth,KP_Location_Weight_Capacity,
		KP_Location_Height_Capacity,KP_Number_of_Pallets,KP_Number_of_Floor_Pallet_Location,KP_Allocation_Allowable,
		KP_Free_Location_When_Zero,KP_Free_Prime_Location_in_99999_Days,KP_Multiple_Products_In_The_Same_Location,
		KP_Storage_Type,KP_Slotting,KP_Manual_Assigned_Location,KP_Used_In_Slotting,KP_Select,KP_YES,KP_NO,
		KP_Invalid_Location,KP_Please_select_an_Area,KP_Please_select_a_Location_Type_first,
		KP_Please_enter_Location,KP_Please_enter_Any_Part_of_Location_Description,
		KP_Location_is_about_to_be_deleted,KP_Location_has_Inventory_and_can_not_be_deleted,
		KP_Location_is_still_assigned_and_can_not_be_deleted,KP_Locaton_already_Used,
		KP_Alternate_Locaton_already_Used,KP_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only
		,KP_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only,
		KP_Location_Depth_must_be_numeric_with_3_decimal_values_only,KP_Location_Width_must_be_numeric_with_3_decimal_values_only,
		KP_Location_Height_must_be_numeric_with_3_decimal_values_only,KP_Wizard_Control_Number_must_be_numeric_only,
		KP_Number_of_Floor_Pallet_Location_must_be_numeric_only,KP_Free_Prime_Location_in_99999_Days_must_be_numeric_only,
		KP_Number_of_Pallets_must_be_numeric_only,KP_Mandatory_field_cannot_be_left_blank,KP_New,
		KP_Display_All,KP_Add_New,KP_Edit,KP_Cancel,KP_Delete,KP_Save_Update,KP_Invalid_Any_Part_of_Location_Description,
		KP_ERR_INVALID_AREA,KP_ERR_INVALID_LOCATION_TYPE,KP_Please_Select_Location_Type,KP_Actions,KP_Last_Activity_Date,
		KP_Last_Activity_By,KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,
		KP_Are_you_sure_you_want_to_delete_this_record,KP_Last_ActivityDate,KP_Loaction_and_Alternate_Location_can_not_be_same
		,KP_There_is_no_location_profile_for_selected_fulfillment_center,KP_Reset,
		KP_Location_Weight_Capacity_must_be_less_then_or_equal_to_Location_Width,
		KP_Location_Height_Capacity_must_be_less_then_or_equal_to_Location_Height
	};
	/**
	 * LOcation Wizard Array
	 */
	public static String[] arrlocationWizardMaintenance ={KP_Location_Profile,	
		KP_Location_Type,
		KP_You_are_not_allowed_to_create_locations_in_reverse_order,
		KP_Select,
		KP_Equality_Check_Error_Msg,
		KP_Row,
		KP_Aisle,
		KP_Bay,
		KP_Level,
		KP_Slot,
		KP_Numeric_Allowed_Only_Character_Set,
		KP_Only_Alphabets_Allowed,
		KP_Location_WIzard_is_about_to_be_deleted,
		KP_Deletion_will_remove_the_Wizard_from_the_locations,
		KP_Location_WIzard_and_its_Locations_are_about_to_be_deleted,
		KP_Ok,
		KP_Download,
		KP_Location_is_greater,
		KP_Mandatory_field_cannot_be_left_blank,
		KP_Save_Update,
		KP_Cancel,
		KP_Your_record_has_been_saved_successfully,
		KP_Numeric_value_can_not_less_than_Zero_or_eual,
		KP_Work_Zone,
		KP_Only_Alphabets_Allowed,
		KP_Work_Group_Zone,
		KP_Numeric_Allowed_Only,
		KP_Number_of_characters_must_be_numeric_and_between_1_to_5_only,
		KP_Area,
		KP_Description_Long,
		KP_Description_Short,
		KP_Wizard_Control_Number,
		KP_Wizard_ID,
		KP_Applied_By,
		KP_Fulfillment_Center,
		KP_Status,
		KP_Created_Date,
		KP_Created_By,
		KP_Applied_Date,
		KP_Number_of_locations_created,
		KP_Number_of_duplicate_locations,
		KP_Print,
		KP_Apply,
		KP_Notes,
		KP_Type,
		KP_ERR_Please_Enter_Seperator_Level_Slot_Print,
		KP_ERR_Please_Enter_Seperator_Level_Slot,
		KP_ERR_Please_Enter_Slot_number_of,
		KP_ERR_Please_Enter_Slot_starting,
		KP_ERR_Please_Enter_Slot_character_set,
		KP_ERR_Please_Enter_Seperator_Bay_Level_Print,
		KP_ERR_Please_Enter_Seperator_Bay_Level,
		KP_ERR_Please_Enter_LEVEL_number_of,
		KP_ERR_Please_Enter_LEVEL_starting,
		KP_ERR_Please_Enter_LEVEL_character_set,
		KP_ERR_Please_Enter_Seperator_Aisle_Bay_Print,
		KP_ERR_Please_Enter_Seperator_Aisle_Bay,
		KP_ERR_Please_Enter_Bay_number_of,
		KP_ERR_Please_Enter_Bay_starting,
		KP_ERR_Please_Enter_Bay_character_set,
		KP_ERR_Please_Enter_Seperator_Row_Aisle_Print,
		KP_ERR_Please_Enter_Seperator_Row_Aisle,
		KP_ERR_Please_Enter_Aisle_number_of,
		KP_ERR_Please_Enter_Aisle_starting,
		KP_ERR_Please_Enter_Aisle_character_set,
		KP_ERR_Please_Enter_Row_number_of,
		KP_ERR_Please_Enter_Row_starting,
		KP_ERR_Please_Enter_Row_character_set,
		KP_Reset,
		KP_Level_Number_of_Characters,
		KP_Location_Wizard_Maintenance,
		KP_Row_Number_of_Characters,
		KP_Row_Characters_Set,
		KP_Row_Starting_at,
		KP_Number_of_Row,
		KP_Separator_Character_Location,
		KP_Separator_Character_Location_Print_only,
		KP_Numeric_Allowed_Only_Character_Set,
		KP_There_is_no_location_profile_for_selected_fulfillment_center


	};


	public static String[] arrlocationWizardLookup ={
		KP_ERR_INVALID_WIZARD_CONTROL_NUMBER,
		KP_Please_enter_Wizard_Control_Number,
		KP_ERR_INVALID_AREA,
		KP_ERR_AREA_LEFT_BLANK,
		KP_ERR_INVALID_LOCATION_TYPE,
		KP_Please_Select_Location_Type,
		KP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION,
		KP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK,
		KP_Mandatory_field_cannot_be_left_blank,
		KP_Location_Type,
		KP_Area,
		KP_Wizard_ID,
		KP_Select,
		KP_Wizard_Control_Number,
		KP_Any_Part_of_Wizard_Description,
		KP_Location_Wizard_Lookup,
		KP_New,
		KP_Display_All,
		KP_Reset
	};

	
	
	public static String[] arrlocationWizardSearchLookup ={	
		KP_Wizard_Control_Number,
		KP_Description,
		KP_Number_of_Locations,
		KP_Wizard_Name,
		KP_Wizard_ID,
		KP_Location_Type,
		KP_Are_you_sure_you_want_to_delete_this_record,
		KP_Location_Profile,
		KP_Created_Date,
		KP_Created_By,
		KP_Applied_Date,
		KP_Applied_By,
		KP_Fulfillment_Name,
		KP_Status,
		KP_Actions,
		KP_Copy,
		KP_Reset,
		KP_Notes,
		KP_Delete_Wizard_Only,
		KP_Delete_Wizard_And_Location,
		KP_Edit,
		KP_Location_Wizard_Search_Lookup,
		KP_Add_New,
		KP_Location_WIzard_is_about_to_be_deleted,
		KP_Deletion_will_remove_the_Wizard_from_the_locations,
		KP_Location_WIzard_and_its_Locations_are_about_to_be_deleted,
		KP_Deletion_will_not_remove_these,
		KP_Locations_and_Inventory_Records

	};
	
	public static String[] arrSmartTaskExecutions = {
			KP_Select,                      
			KP_Application,				
			KP_Task,
			KP_Execution_Group,		
			KP_Execution_Type,			
			KP_Execution_Device,			
			KP_Execution_Sequence,
			KP_Last_Activity_Date,
			KP_Last_Activity_By,
			KP_Description_Short,
			KP_Description_Long,
			KP_Any_Part_of_the_Execution_Description,
			KP_Tenant,						
			KP_Buttons,						
			KP_Button_Name,
			KP_New,KP_Display_All,
			KP_Search,
			KP_Executions_Lookup,
			KP_Y,
			KP_N,
			KP_YES,              
			KP_NO,        					    
			KP_Execution_Name,
			KP_Execution_Path,
			KP_Smart_Task_Executions_Lookup,
			KP_Executions_Maintenance,
			KP_Last_Activity_Date,
			KP_Last_Activity_By,
			KP_Adding_Record_Failed,
			KP_Company,
			
			KP_Please_select_Application,KP_Please_select_Execution_Group,
			KP_Please_select_Execution_Type,KP_Please_select_Execution_Device,
			KP_Please_enter_Execution_Name,KP_Please_enter_any_part_of_the_Execution_Description,
			KP_Please_enter_Tenant,KP_Please_select_Buttons,KP_Invalid_Application,
			KP_Invalid_Execution_Group,KP_Invalid_Execution_Type,KP_Invalid_Execution_Device,KP_Invalid_Execution_Name,
			KP_Invalid_any_part_of_the_Execution_Description,KP_Invalid_Tenant,KP_Invalid_Buttons,
			KP_Executions_Search_Lookup,KP_Add_New,KP_Edit,KP_Notes,KP_Delete,KP_Actions,KP_Description,
			KP_Are_you_sure_you_want_to_delete_this_record,
			KP_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted,
			KP_Help_Line,KP_Save_Update,
			KP_Reset,KP_Cancel,KP_Mandatory_field_cannot_be_left_blank,KP_Execution_Name_already_Used,
			KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,
			KP_Please_enter_valid_Execution_Path,
			KP_Group,
			KP_Type,
			KP_Device,
			KP_Execution,
			KP_Smart_Task_Executions_Search_Lookup,
			KP_Smart_Task_Execution_Maintenance
		};

	public static String[] arrWorkFlowConfigurator={KP_Work_Flow_ID,KP_Work_Flow_Type,KP_Work_Flow_Maintenance,KP_Work_Flow_Search_Lookup,KP_Work_Flow_Lookup,
		KP_Description,KP_FulFillment_Center,KP_Save_Update,KP_Display_All,KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,KP_Mandatory_field_cannot_be_left_blank,
		KP_Reset,KP_Delete,KP_Cancel,KP_Add_New,KP_Add,KP_Edit,KP_Please_enter_Any_Part_of_the_Work_Flow_Description,
		KP_Please_enter_Any_Part_of_the_Work_Flow_Description,KP_New,KP_Please_select_Work_Flow_Type
		,KP_Actions,KP_Please_enter_Fulfillment_center,KP_Please_enter_Work_Flow_ID,KP_Any_Part_of_the_Work_Flow_Description,
		KP_Invalid_Work_Flow_Type,KP_Invalid_part_of_Work_Flow_Description,KP_Invalid_Work_Flow_ID,
		KP_Invalid_Fulfillment_center,KP_Select,KP_Note,KP_Notes,KP_Copy,KP_WORK_TYPE,KP_Flow_ID,KP_Actions};
	
	public static String[] arrSmartTaskConfigurator ={	
		KP_Select,KP_Application,KP_Execution_Group,KP_Execution_Type,
		KP_Execution_Device,KP_Task,KP_Execution_Sequence,KP_Any_Part_of_the_Execution_Sequence_Description,
		KP_Tenant,KP_New,KP_Display_All,KP_Menu_Name,KP_Smart_Task_Configurator_Lookup,
		KP_Please_select_Application,KP_Please_select_Execution_Group,
		KP_Please_select_Execution_Type,KP_Please_select_Execution_Device,
		KP_Please_enter_Tenant,KP_Invalid_Application,
		KP_Invalid_Execution_Group,KP_Invalid_Execution_Type,KP_Invalid_Execution_Device,
		KP_Invalid_Tenant,KP_Please_select_Task,KP_Please_enter_Execution_Sequence,
		KP_Please_enter_any_part_of_the_Execution_Sequence_Description,
		KP_Add_New,KP_Edit,KP_Notes,KP_Delete,KP_Actions,KP_Description,
		KP_Are_you_sure_you_want_to_delete_this_record,
		KP_Please_enter_Menu_Name,KP_Invalid_Task,KP_Invalid_Execution_Sequence,
		KP_Invalid_any_part_of_the_Execution_Sequence_Description,KP_Invalid_Menu_Name,
		KP_Last_Activity_Date,KP_Last_Activity_By,
		KP_Description_Short,KP_Description_Long,KP_Help_Line,KP_Save_Update,
		KP_Reset,KP_Cancel,KP_Mandatory_field_cannot_be_left_blank,
		KP_Your_record_has_been_saved_successfully,KP_Your_record_has_been_updated_successfully,
		KP_Smart_Task_Configurator_Search_Lookup,KP_Group,KP_Type,
		KP_Device,KP_Sequence,KP_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted
		,KP_Company,KP_Search_All,KP_Execution_Sequence_Name,KP_Configurator,KP_Execution,KP_Action,KP_Sequence_View
		,KP_Smart_Task_Configurator_Maintenance,KP_Last_Activity_By,KP_Last_Activity_Date,KP_Execution_Sequence_Name_or_Menu_Name_already_exist,
		KP_Execution,KP_Sequence,KP_Execution_Sequence_Type,KP_Go_To_Tag,KP_Description,KP_Message,
		KP_Start_node_already_exist,
		KP_End_node_already_exist,
		KP_You_can_not_delete_Start_node,
		KP_You_can_not_delete_End_node,
		KP_You_cannot_delete_selected_link,
		KP_Please_first_of_all_connect_every_node_via_link, 
		KP_IF_EXIT,
		KP_GoTo_Tag,
		KP_Save,
		KP_Display_Text,
		KP_IF_ERROR,
		KP_GENERAL_TAG_NAME,
		KP_CUSTOM_EXECUTION_PATH, 
		KP_Return_Code,
		KP_IF_RETURN_CODE,
		KP_COMMENT,
		KP_Please_select_GoTo_Tag_first,
		KP_All_fields_are_mandatory,
		KP_Please_enter_display_text,
		KP_You_must_not_connect_Start_node_to_End_node_via_link,
		KP_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one,
		KP_Please_select_execution_data_filter_first_in_task_panel,
		KP_Please_connect_every_node_via_link_before_saving_data,
		KP_Please_connect_every_node_via_link_before_align_data,
		KP_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking,
		KP_General_Tag_already_exist_with,
		KP_name_So_Please_provide_another_uniq_general_tag_name,
		KP_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid,
		KP_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid,
		KP_Please_connect_every_node_via_link_before_see_sequence_grid,KP_Please_fill_all_mandatory_fields_value_in_task_panel
		,KP_Align,KP_Back,KP_Reload,KP_Print,KP_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking
		,KP_Execution_TagName,KP_Sequence_Name,KP_Copy,KP_Refresh,KP_Close,KP_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG
		,KP_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG
		,KP_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel
		,KP_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG
		,KP_other_nodes_not_connected,KP_AND
		,KP_Please_enter_Display_text,
		KP_Please_enter_Comment,
		KP_Please_enter_General_Tag_Name,
		KP_Please_enter_Custom_Execution_Path,
		KP_Custom_Execution_Path_not_valid,
		KP_Please_enter_different_value_except_CUSTOM_EXECUTION,
		KP_Please_enter_different_value_except_COMMENT,
		KP_Please_enter_different_value_except_DISPLAY,
		KP_Please_enter_different_value_except_GENERAL_TAG,
		KP_Please_do_not_left_any_node_blank
	};
	
	public static String[] arrPutWall ={
			KP_Company,
			KP_Fulfillment_Center,
			KP_PutWall_Type,
			KP_PutWall_Id,
			KP_Description_Short,
			KP_Description_Long,
			KP_SingleOrMulti,
			KP_NumberOfLevel,
			KP_NumberOfSlots,
			KP_Default,
			KP_ContainerType,
			KP_ContainerId,
			KP_Priority,
			KP_Level,
			KP_Section,
			 KP_Sequence,
			KP_Constant_Value,
			KP_Create,
			KP_Select,
			KP_Continue,
			KP_Wizard_PutWall_Creation
	};

	
	public static String StrSmartWorkFlowConfiguratorModule="Smart Work Flow Configurator";
	public static String StrPasswordExpiryModule="Password Expiry";
	public static String StrDefaultLanguageCode="ENG";
	public static String StrNotesModule="Notes";
	public static String StrFulfillmentCenterModule="Fulfillment Center";
	public static String StrSecurityAuthorizationModule="Security Set Up";
	public static String StrLoginModule="login";
	public static String strMenuExecutionModule="Menu Execution";
	public static String StrMenuOptionModule="Menu Option";
	public static String StrMenuProfileModule="Menu Profile";
	public static String StrLocationLabelPrintModule="Location Label Print";
	public static String strMenuAppIconModule="Menu App Icon";
	public static String strTeamMemberModule="Team Member Maintenance";
	public static String strLanguageTranslationModule="Language Translation";
	public static String strTeamMemberMessageModule="TeamMember Message";
	public static String StrMenuMessage="Menu Message";
	public static String StrInfoHelps="InfoHelps";
	public static String StrGeneralCodes="GeneralCodes";
	public static String strCompanyMaintenanceModule="Company Maintenance";
	public static String strLocationProfileModule="Location Profile";
	public static String StrLocationMaintenance="Location Maintenance";
	public static String StrMenuProfileAssignmentModule="Menu Profile Assigment";
	public static String LocationWizardMaintenanceModule="Location_Wizard_Maintenance";
	public static String LocationWizardLookupModule="Location_Wizard_Lookup";
	public static String LocationWizardSearchLookupModule="Wizard_Location_Search_Lookup";
	public static String StrSmartTaskExecutionsModule="Smart_Task_Executions";
	public static String StrSmartTaskConfiguratorModule="Smart_Task_Configurator";
	public static String strWebExecutionModule="Web Execution";
	
	
	/** 
	 * @param StrModuleName
	 * @param StrLanguageCode
	 * @return
	 */
	@RequestMapping(value = GLOBALCONSTANT.common_M_GetScreenLabels, method = RequestMethod.GET)
	@Transactional
	public @ResponseBody List<LANGUAGES> GetScreenLabels(@RequestParam(value = GLOBALCONSTANT.common_P_ModuleName)String StrModuleName,@RequestParam(value =GLOBALCONSTANT.common_P_LanguageCode) String StrLanguageCode)
	{
		List<LANGUAGES> lstLanguages=null;
		if(StrModuleName.equalsIgnoreCase(StrFulfillmentCenterModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrfulfillmentCenterKeyPhases);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrfulfillmentCenterKeyPhases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrfulfillmentCenterKeyPhases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}
		}
		else if(StrModuleName.equalsIgnoreCase(StrSecurityAuthorizationModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrSecuritySetupLabel);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrSecuritySetupLabel), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrSecuritySetupLabel);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}
		}
		else if(StrModuleName.equalsIgnoreCase(strMenuExecutionModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrMenuExecutionKeyPhases);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrMenuExecutionKeyPhases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrMenuExecutionKeyPhases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}else if(StrModuleName.equalsIgnoreCase(StrMenuOptionModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrMenuOptionKeyPhares);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrMenuOptionKeyPhares), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrMenuOptionKeyPhares);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}else if(StrModuleName.equalsIgnoreCase(StrMenuProfileModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrMenuProfileKeyPharses);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrMenuProfileKeyPharses), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrMenuProfileKeyPharses);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrLoginModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrLoginSignOn);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrLoginSignOn), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrLoginSignOn);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrPasswordExpiryModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrExpireLoginPassword);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrExpireLoginPassword), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrExpireLoginPassword);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}
		}
		/** get menu app icon screen labels*/
		else if(StrModuleName.equalsIgnoreCase(strMenuAppIconModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrMenuAppIcon);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrMenuAppIcon), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrMenuAppIcon);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}
		}

		/** get location label print screen labels*/
		else if(StrModuleName.equalsIgnoreCase(StrLocationLabelPrintModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrLocationLabelPrintKeyPhrases);
			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrLocationLabelPrintKeyPhrases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrLocationLabelPrintKeyPhrases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}

			}

		}

		/** get team member  screen labels*/
		else if(StrModuleName.equalsIgnoreCase(strTeamMemberModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrTeamMemberMaintenanceKeyPhrases);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrTeamMemberMaintenanceKeyPhrases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrTeamMemberMaintenanceKeyPhrases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}

			}

		}
		else if(StrModuleName.equalsIgnoreCase(strLanguageTranslationModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrLanguageTranslationKeyPhrases);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrLanguageTranslationKeyPhrases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrLanguageTranslationKeyPhrases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}

		else if(StrModuleName.equalsIgnoreCase(strTeamMemberMessageModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrTeammemberMessagesKeyPhrases);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrTeammemberMessagesKeyPhrases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrTeammemberMessagesKeyPhrases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrMenuMessage)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrMenuMessage);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrMenuMessage), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrMenuMessage);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		/** get execution  screen labels*/
		else if(StrModuleName.equalsIgnoreCase(strWebExecutionModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrExecutionKeyPhraseArray);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrExecutionKeyPhraseArray), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrExecutionKeyPhraseArray);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}

			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrInfoHelps)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrInfohelps);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrInfohelps), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrInfohelps);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrGeneralCodes)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrGeneralCodes);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrGeneralCodes), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrGeneralCodes);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		/** get company maintenance  screen labels*/
		else if(StrModuleName.equalsIgnoreCase(strCompanyMaintenanceModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrCompanyMaintenanceKeyPhrases);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrCompanyMaintenanceKeyPhrases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrCompanyMaintenanceKeyPhrases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}

			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrNotesModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrNotesScreenKeyPhases);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrNotesScreenKeyPhases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrNotesScreenKeyPhases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}

			}
		}

		else if(StrModuleName.equalsIgnoreCase(strLocationProfileModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrLocationProfileKeyPhrases);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrLocationProfileKeyPhrases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrLocationProfileKeyPhrases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrLocationMaintenance)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrLocationMaintenance);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrLocationMaintenance), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrLocationMaintenance);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}


		}else if(StrModuleName.equalsIgnoreCase(StrMenuProfileAssignmentModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrMenuProfileAssignmentKeyPhases);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrMenuProfileAssignmentKeyPhases), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrMenuProfileAssignmentKeyPhases);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}else if(StrModuleName.equalsIgnoreCase(StrMenuProfileModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrMenuProfileKeyPharses);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrMenuProfileKeyPharses), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrMenuProfileKeyPharses);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}
		}
		// LOcation wizard
		else if(StrModuleName.equalsIgnoreCase(LocationWizardMaintenanceModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrlocationWizardMaintenance);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrlocationWizardMaintenance), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrlocationWizardMaintenance);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(LocationWizardLookupModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrlocationWizardLookup);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrlocationWizardLookup), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrlocationWizardLookup);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(LocationWizardSearchLookupModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrlocationWizardSearchLookup);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrlocationWizardSearchLookup), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrlocationWizardSearchLookup);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrSmartTaskExecutionsModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrSmartTaskExecutions);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrSmartTaskExecutions), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrSmartTaskExecutions);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrSmartWorkFlowConfiguratorModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrWorkFlowConfigurator);

			}else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrWorkFlowConfigurator), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrWorkFlowConfigurator);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}
		}
		
		else if(StrModuleName.equalsIgnoreCase(StrSmartTaskConfiguratorModule)){
			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrSmartTaskConfigurator);

			}else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrSmartTaskConfigurator), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrSmartTaskConfigurator);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}
		}
		else if(StrModuleName.equalsIgnoreCase(StrSmartTaskExecutionsModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrSmartTaskExecutions);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrSmartTaskExecutions), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrSmartTaskExecutions);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		else if(StrModuleName.equalsIgnoreCase(StrPutWallModule)){

			if(StrLanguageCode.equalsIgnoreCase(StrDefaultLanguageCode))
			{
				lstLanguages=getDefaultLanguageData(arrPutWall);

			}
			else
			{
				List<Object> lstObject=objLanguagesDaoImpl.GetLanguageByKeyPhases(getInQuery(arrPutWall), StrLanguageCode);
				if(lstObject.isEmpty())
				{
					lstLanguages=getDefaultLanguageData(arrPutWall);	
				}
				else{
					lstLanguages=GetLanguagesList(lstObject);
				}
			}

		}
		return lstLanguages;				

	}

	/**  
	 * @param arrOfPhases
	 * @return
	 */

	public  List<LANGUAGES> getDefaultLanguageData(String []arrOfPhases)
	{
		List<LANGUAGES> lstLanguages= new ArrayList<LANGUAGES>();
		for (String StrKeyPhase : arrOfPhases) {
			LANGUAGEPK objlanguagepk=new LANGUAGEPK();
			objlanguagepk.setLanguage(StrDefaultLanguageCode);
			objlanguagepk.setKeyPhrase(StrKeyPhase);
			LANGUAGES objLanguages= new LANGUAGES();
			objLanguages.setId(objlanguagepk);
			objLanguages.setTranslation(StrKeyPhase);
			lstLanguages.add(objLanguages);
		}
		return lstLanguages;
	}

	/**
	 * @param hmOfObjectFieldKeyPhases
	 * @return
	 */

	public  List<LANGUAGES> getDefaultLanguageData(HashMap<String, String> hmOfObjectFieldKeyPhases)
	{
		List<LANGUAGES> lstLanguages= new ArrayList<LANGUAGES>();
		Iterator<Map.Entry<String, String>> iterator = hmOfObjectFieldKeyPhases.entrySet().iterator() ;
		//hmOfObjectFieldKeyPhases.
		while(iterator.hasNext()){
			Map.Entry<String, String> languageEntity = iterator.next();
			LANGUAGEPK objlanguagepk=new LANGUAGEPK();
			LANGUAGES objLanguages= new LANGUAGES();
			objlanguagepk.setLanguage(StrDefaultLanguageCode);
			objlanguagepk.setKeyPhrase(languageEntity.getValue());
			objLanguages.setId(objlanguagepk);
			objLanguages.setTranslation(languageEntity.getValue());
			lstLanguages.add(objLanguages);
			iterator.remove();
		}
		return lstLanguages;
	}

	/**
	 * @param arrKeyPhases
	 * @return
	 */
	public String getInQuery(String []arrKeyPhases)
	{
		String SingleQuote = GLOBALCONSTANT.SingleQuote;
		String StrInQuery = GLOBALCONSTANT.BlankString;

		for (String StrKeyPhase : arrKeyPhases) {
			StrInQuery=StrInQuery+SingleQuote+StrKeyPhase+SingleQuote+GLOBALCONSTANT.Comma;
		}
		StrInQuery=StrInQuery.substring(GLOBALCONSTANT.INT_ZERO, StrInQuery.length()-GLOBALCONSTANT.IntOne);
		return StrInQuery;
	}

	/**
	 *  @param hmOfObjectFieldKeyPhases
	 * @return
	 */

	public String getInQuery(HashMap<String, String> hmOfObjectFieldKeyPhases) {
		String SingleQuote = GLOBALCONSTANT.SingleQuote;
		String StrInQuery = GLOBALCONSTANT.BlankString;
		Iterator<Map.Entry<String, String>> iterator = hmOfObjectFieldKeyPhases.entrySet().iterator() ;
		//hmOfObjectFieldKeyPhases.
		while(iterator.hasNext()){
			Map.Entry<String, String> languageEntity = iterator.next();
			StrInQuery = StrInQuery + SingleQuote + languageEntity.getValue() + SingleQuote + GLOBALCONSTANT.Comma;
		}
		StrInQuery = StrInQuery.substring(GLOBALCONSTANT.INT_ZERO, StrInQuery.length() - GLOBALCONSTANT.IntOne);
		return StrInQuery;
	}


	@SuppressWarnings(GLOBALCONSTANT.Strunused)
	public List<LANGUAGES> GetLanguagesList(List<Object> lstObject)
	{
		List<LANGUAGES> lstLanguages = new ArrayList<LANGUAGES>();

		for (Object object : lstObject) {
			Object []Languages_Fields = (Object[])object;

			String LANGUAGE = GLOBALCONSTANT.BlankString,KEY_PHRASE = GLOBALCONSTANT.BlankString,TRANSLATION= GLOBALCONSTANT.BlankString,LAST_ACTIVITY_DATE= GLOBALCONSTANT.BlankString,LAST_ACTIVITY_TEAM_MEMBER= GLOBALCONSTANT.BlankString;
			try{ LANGUAGE=Languages_Fields[GLOBALCONSTANT.INT_ZERO].toString();}catch(Exception e){}
			try{ KEY_PHRASE=Languages_Fields[GLOBALCONSTANT.IntOne].toString();}catch(Exception e){}
			try{ TRANSLATION=Languages_Fields[GLOBALCONSTANT.INT_TWO].toString();}catch(Exception e){}
			try{ LAST_ACTIVITY_DATE=Languages_Fields[GLOBALCONSTANT.INT_THREE].toString();}catch(Exception e){}
			try{ LAST_ACTIVITY_TEAM_MEMBER=Languages_Fields[GLOBALCONSTANT.INT_FOUR].toString();}catch(Exception e){}
			LANGUAGEPK objLanguagepk= new LANGUAGEPK();
			objLanguagepk.setKeyPhrase(KEY_PHRASE);
			objLanguagepk.setLanguage(LANGUAGE);
			LANGUAGES objLanguages= new LANGUAGES();
			objLanguages.setId(objLanguagepk);
			objLanguages.setLastActivityDate(null);
			objLanguages.setLastActivityTeamMember(LAST_ACTIVITY_TEAM_MEMBER);
			objLanguages.setTranslation(TRANSLATION);
			lstLanguages.add(objLanguages);
		}
		return lstLanguages;
	}
	HashMap<String, String> getReveseHashMap(HashMap<String, String> hmIn)
	{
		HashMap< String, String> hmOu= new HashMap<String, String>();
		Iterator<Map.Entry<String, String>> iterator = hmIn.entrySet().iterator() ;
		//hmOfObjectFieldKeyPhases.
		while(iterator.hasNext()){
			Map.Entry<String, String> languageEntity = iterator.next();
			hmOu.put(languageEntity.getValue().toUpperCase(), languageEntity.getKey());
		}
		return hmOu;
	}
	@SuppressWarnings(GLOBALCONSTANT.Strunused)
	public List<LANGUAGES> GetLanguagesList(List<Object> lstObject,HashMap<String, String> hmKeyPhases)
	{
		List<LANGUAGES> lstLanguages = new ArrayList<LANGUAGES>();

		HashMap<String, String> hmUses=getReveseHashMap(hmKeyPhases);

		for (Object object : lstObject) {
			Object []Languages_Fields = (Object[])object;

			String LANGUAGE = GLOBALCONSTANT.BlankString,KEY_PHRASE = GLOBALCONSTANT.BlankString,TRANSLATION= GLOBALCONSTANT.BlankString,OBJECT_CODE= GLOBALCONSTANT.BlankString,OBJECT_FIELD_CODE= GLOBALCONSTANT.BlankString,LAST_ACTIVITY_DATE= GLOBALCONSTANT.BlankString,LAST_ACTIVITY_TEAM_MEMBER= GLOBALCONSTANT.BlankString;
			try{ LANGUAGE=Languages_Fields[GLOBALCONSTANT.INT_ZERO].toString();}catch(Exception e){}
			try{ KEY_PHRASE=Languages_Fields[GLOBALCONSTANT.IntOne].toString();}catch(Exception e){}
			try{ TRANSLATION=Languages_Fields[GLOBALCONSTANT.INT_TWO].toString();}catch(Exception e){}
			try{ OBJECT_CODE=Languages_Fields[GLOBALCONSTANT.INT_THREE].toString();}catch(Exception e){}
			try{ OBJECT_FIELD_CODE=Languages_Fields[GLOBALCONSTANT.INT_FOUR].toString();}catch(Exception e){}
			try{ LAST_ACTIVITY_DATE=Languages_Fields[GLOBALCONSTANT.INT_FIVE].toString();}catch(Exception e){}
			try{ LAST_ACTIVITY_TEAM_MEMBER=Languages_Fields[GLOBALCONSTANT.INT_SIX].toString();}catch(Exception e){}

			OBJECT_FIELD_CODE=hmUses.get(KEY_PHRASE.toUpperCase());
			LANGUAGEPK objLanguagepk= new LANGUAGEPK();
			objLanguagepk.setKeyPhrase(KEY_PHRASE);
			objLanguagepk.setLanguage(LANGUAGE);
			LANGUAGES objLanguages= new LANGUAGES();
			objLanguages.setId(objLanguagepk);
			objLanguages.setLastActivityDate(null);
			objLanguages.setLastActivityTeamMember(LAST_ACTIVITY_TEAM_MEMBER);
			objLanguages.setTranslation(TRANSLATION);
			lstLanguages.add(objLanguages);
		}
		return lstLanguages;
	}
	
	
	/**
	 * 
	*
	* @auther  Anshu Sharma
	* @description hit url 
	* @createdon Oct 09, 2015		
	* @@param LANGUAGETRANSLATIONBE,keyphraseArray
	* @@return	
	* @return HashMap<String, String>
	*
	 */	
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public HashMap<String, String> getServiceResponseLanguageTranslationLabels(LANGUAGETRANSLATIONBE objLanguageTranslationBE,String[] keyphraseArray
){
	JSONObject input = new JSONObject();
	StringBuilder sb = new StringBuilder();
	HashMap<String, String> response = new HashMap<String, String>();
	this.objLanguageTranslationBE=objLanguageTranslationBE;
    this.keyphraseArray=keyphraseArray;
	try {
	

		input.put(GLOBALCONSTANT.LANGUAGE,objLanguageTranslationBE.getLanguage());
		input.put(GLOBALCONSTANT.KEYPHASE,getInQuery(keyphraseArray));
		input.put(GLOBALCONSTANT.MODULENAME,objLanguageTranslationBE.getModuleName());

		
		String url_obj=GLOBALCONSTANT.SERVERIP+ GLOBALCONSTANT.WEBSERVERNAME+ GLOBALCONSTANT.GETSCREENLABELS;
		URL url = new URL(url_obj+URLEncoder.encode(input.toString(), "UTF-8"));
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		// con.setRequestMethod("POST");// optional default is GET
		if (url_obj.contains(GLOBALCONSTANT.STRENGINESTARTURL)) {
			con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_GET);
		} else {
			con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_POST);
		}

		// ...

	
		int HttpResult = con.getResponseCode();
		if (HttpResult == HttpURLConnection.HTTP_OK) {
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), GLOBALCONSTANT.UTF8));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + GLOBALCONSTANT.NEXT_LINE);
			}

			br.close();

//			System.out.println("" + sb.toString());
			response = validate(sb.toString());
		} else {
			log.info(con.getResponseMessage());
		}}catch(Exception e){
			
		}

	return response;
}
/**
 * 
*
* @auther  Anshu Sharma
* @description validate response
* @createdon Oct 09, 2015		
* @@param strValue
* @@return	
* @return LanguageTranslationBE	
*
 */
public HashMap<String, String> validate(String strValue) {
	String status = null;
	HashMap<String, String> languageTranslationHashMap=new HashMap<String, String>();
	JSONParser jsonP = new JSONParser();
	
	try {

		// Parse URL Here
		
		JSONObject jsonObject = (JSONObject) jsonP.parse(strValue);
		status = jsonObject.get(GLOBALCONSTANT.VALUESTATUS_CAP).toString();
		GLOBALCONSTANT.STATUS_FAILURE = status;
		
	
		
		if(GLOBALCONSTANT.STATUS_FAILURE.equalsIgnoreCase(GLOBALCONSTANT.SUCCESS))
		{
			JSONArray objJsonArray = (JSONArray) jsonObject
				.get(GLOBALCONSTANT.GETLANGUAGELABELS);
		
		for(int index=GLOBALCONSTANT.INT_NUMBER_ZERO;index<objJsonArray.size();index++)
		{
			
			Object ArrayOfArrayObject= objJsonArray.get(index);
			JSONArray objJsonArraySecond = (JSONArray) ArrayOfArrayObject;
			Object ArrayObject=objJsonArraySecond.get(GLOBALCONSTANT.INT_NUMBER_ZERO);
			JSONObject listLanguage=(JSONObject)ArrayObject;
			try {
				languageTranslationHashMap.put(listLanguage.get(GLOBALCONSTANT.KEYPHRASE).toString(),listLanguage.get(GLOBALCONSTANT.TRANSLATION).toString());
			} catch (Exception e) {
				
				e.getMessage();
			}
			
		}
		
		
		}
		else{
			
		}

	} catch (Exception e) {
		e.getMessage();

	}

	return languageTranslationHashMap;

}
	
	
}
