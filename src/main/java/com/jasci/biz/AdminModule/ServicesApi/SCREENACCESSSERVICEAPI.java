/*

Description This class is used to make restfull service for check screen access 
Created By Deepak Sharma
Created Date Dec 20 2014

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.dao.SCREENACCESSDAO;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.common.constant.GLOBALCONSTANT;

@Controller
public class SCREENACCESSSERVICEAPI {

	@Autowired
	private SCREENACCESSDAO screenAccessDAL;
	
	@Transactional
	 @RequestMapping(value = GLOBALCONSTANT.UrlcheckScreenAccess, method = RequestMethod.GET)
		public @ResponseBody  WEBSERVICESTATUS  checkScreenAccess(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember) String StrTeamMember,@RequestParam(value=GLOBALCONSTANT.StrMenuType) String StrMenuType,@RequestParam(value=GLOBALCONSTANT.MENUOPTION_MenuOption) String StrMenuOption) {
		 
		 List<Object> listUser=null;
		 WEBSERVICESTATUS objWebServiceStatus= new WEBSERVICESTATUS();
		 
		try {
			listUser = screenAccessDAL.checkScreenAccess(StrTenant, StrTeamMember, StrMenuType, StrMenuOption);
			if(listUser.size()>GLOBALCONSTANT.INT_ZERO)
			{
				objWebServiceStatus.setStrStatus(GLOBALCONSTANT.Success);
				objWebServiceStatus.setStrMessage(GLOBALCONSTANT.You_have_access);
				objWebServiceStatus.setBoolStatus(true);
				
			}
			else
			{

				objWebServiceStatus.setStrStatus(GLOBALCONSTANT.Fail);
				objWebServiceStatus.setStrMessage(GLOBALCONSTANT.You_don_have_access);
				objWebServiceStatus.setBoolStatus(false);
			}}
			catch(Exception e)
			{
				objWebServiceStatus.setStrStatus(GLOBALCONSTANT.Fail);
				objWebServiceStatus.setStrMessage(GLOBALCONSTANT.Something_happen_wrong);
				objWebServiceStatus.setBoolStatus(false);
			}
			return objWebServiceStatus;
	 }
	
	@Transactional
	public @ResponseBody  WEBSERVICESTATUS  checkScreenAccess(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember) String StrTeamMember,@RequestParam(value=GLOBALCONSTANT.DataBase_MenuOptions_Execution) String strExecution) {
		 
		 List<Object> listUser=null;
		 WEBSERVICESTATUS objWebServiceStatus= new WEBSERVICESTATUS();
		 
		try {
			listUser = screenAccessDAL.checkScreenAccess(StrTenant, StrTeamMember, strExecution);
			if(listUser.size()>GLOBALCONSTANT.INT_ZERO)
			{
				objWebServiceStatus.setStrStatus(GLOBALCONSTANT.Success);
				objWebServiceStatus.setStrMessage(GLOBALCONSTANT.You_have_access);
				objWebServiceStatus.setBoolStatus(true);
				
			}
			else
			{

				objWebServiceStatus.setStrStatus(GLOBALCONSTANT.Fail);
				objWebServiceStatus.setStrMessage(GLOBALCONSTANT.You_don_have_access);
				objWebServiceStatus.setBoolStatus(false);
			}}
			catch(Exception e)
			{
				objWebServiceStatus.setStrStatus(GLOBALCONSTANT.Fail);
				objWebServiceStatus.setStrMessage(GLOBALCONSTANT.Something_happen_wrong);
				objWebServiceStatus.setBoolStatus(false);
			}
			return objWebServiceStatus;
	 }
	

	
}
