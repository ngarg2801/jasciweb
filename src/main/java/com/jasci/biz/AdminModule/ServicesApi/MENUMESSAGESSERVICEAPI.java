/*

Description make a RestfullServices for communication between dao to services
Created By Aakash Bishnoi
Created Date Dec 15 2014

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.dao.IMENUMESSAGEDAO;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGESBEAN;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;



@Controller
public class MENUMESSAGESSERVICEAPI {
	@Autowired
	private IMENUMESSAGEDAO ObjectMenuMessagesDao;
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
/***
 * Description It is used to get the MenuMessage List according to his type
 * @param StrTenant_ID
 * @param StrCompany_ID
 * @param StrStatus
 * @return  List<MENUMESSAGESBEAN> 
 * @throws JASCIEXCEPTION
 */
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuMessages_Restfull_AllMessages, method = RequestMethod.GET)
	public @ResponseBody  List<MENUMESSAGESBEAN>  getList(@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Tenant_ID) String StrTenant_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Company_ID) String StrCompany_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Status) String StrStatus) throws JASCIEXCEPTION {	

		List MenuMessagesList= ObjectMenuMessagesDao.getList(StrTenant_ID.toUpperCase(),StrCompany_ID.toUpperCase(),StrStatus.toUpperCase());	
		
		
		List<MENUMESSAGESBEAN> objMenuMessageBeanList=new ArrayList<MENUMESSAGESBEAN>();

		for(Object ObjectValue:MenuMessagesList)
		{
			Object[] valueMenuMessages = (Object[]) ObjectValue;
			
			if(valueMenuMessages[GLOBALCONSTANT.INT_SIX]!=null)
			{
				MENUMESSAGESBEAN objMenuMessagesbean=new MENUMESSAGESBEAN();
				objMenuMessagesbean.setType(GLOBALCONSTANT.TicketTypeMenuMessage);
				try{objMenuMessagesbean.setMessage_ID(valueMenuMessages[GLOBALCONSTANT.INT_ZERO].toString());}catch(Exception e){objMenuMessagesbean.setMessage_ID(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setTenant_ID(valueMenuMessages[GLOBALCONSTANT.IntOne].toString());}catch(Exception e){objMenuMessagesbean.setTenant_ID(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setCompany_ID(valueMenuMessages[GLOBALCONSTANT.INT_TWO].toString());}catch(Exception e){objMenuMessagesbean.setCompany_ID(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setStatus(valueMenuMessages[GLOBALCONSTANT.INT_THREE].toString());}catch(Exception e){objMenuMessagesbean.setStatus(GLOBALCONSTANT.BlankString);}
			
			    try{objMenuMessagesbean.setStart_Date(valueMenuMessages[GLOBALCONSTANT.INT_FOUR].toString());}catch(Exception e){objMenuMessagesbean.setStart_Date(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setEnd_Date(valueMenuMessages[GLOBALCONSTANT.INT_FIVE].toString());}catch(Exception e){objMenuMessagesbean.setEnd_Date(GLOBALCONSTANT.BlankString);}
				
				try{objMenuMessagesbean.setMessage(valueMenuMessages[GLOBALCONSTANT.INT_SIX].toString());}catch(Exception e){objMenuMessagesbean.setMessage(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setCompany_Name(valueMenuMessages[GLOBALCONSTANT.INT_EIGHT].toString());}catch(Exception e){objMenuMessagesbean.setCompany_Name(GLOBALCONSTANT.BlankString);}

				objMenuMessageBeanList.add(objMenuMessagesbean);
			}
			
		
			
			
			if(valueMenuMessages[GLOBALCONSTANT.INT_SEVEN]!=null)
			{
				MENUMESSAGESBEAN objMenuMessagesbean=new MENUMESSAGESBEAN();
				objMenuMessagesbean.setType(GLOBALCONSTANT.TypeMenuMessage);
				try{objMenuMessagesbean.setMessage_ID(valueMenuMessages[GLOBALCONSTANT.INT_ZERO].toString());}catch(Exception e){objMenuMessagesbean.setMessage_ID(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setTenant_ID(valueMenuMessages[GLOBALCONSTANT.IntOne].toString());}catch(Exception e){objMenuMessagesbean.setTenant_ID(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setCompany_ID(valueMenuMessages[GLOBALCONSTANT.INT_TWO].toString());}catch(Exception e){objMenuMessagesbean.setCompany_ID(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setStatus(valueMenuMessages[GLOBALCONSTANT.INT_THREE].toString());}catch(Exception e){objMenuMessagesbean.setStatus(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setStart_Date(valueMenuMessages[GLOBALCONSTANT.INT_FOUR].toString());}catch(Exception e){objMenuMessagesbean.setStart_Date(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setEnd_Date(valueMenuMessages[GLOBALCONSTANT.INT_FIVE].toString());}catch(Exception e){objMenuMessagesbean.setEnd_Date(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setMessage(valueMenuMessages[GLOBALCONSTANT.INT_SEVEN].toString());}catch(Exception e){objMenuMessagesbean.setMessage(GLOBALCONSTANT.BlankString);}
				try{objMenuMessagesbean.setCompany_Name(valueMenuMessages[GLOBALCONSTANT.INT_EIGHT].toString());}catch(Exception e){objMenuMessagesbean.setCompany_Name(GLOBALCONSTANT.BlankString);}

				objMenuMessageBeanList.add(objMenuMessagesbean);
				
			}
		
			

		}
	
			
		return objMenuMessageBeanList;
			
	}

	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 17, 2014
	  * Description this is used to get the List of Team Member Company
	  * @param StrTenant
	  * @param StrTeamMember
	  * @return List<TEAMMEMBERCOMPANIESBEAN>
	  * @throws JASCIEXCEPTION
	  */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuMessages_Restfull_ReadCompanyList, method = RequestMethod.GET)
	public @ResponseBody  List<COMPANIES>  getCompanyList(@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Tenant_ID) String StrTenant_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_TeamMember_ID) String StrTeam_Member_ID) throws JASCIEXCEPTION {	

		return ObjectMenuMessagesDao.getCompanyList(StrTenant_ID,StrTeam_Member_ID);
		 
	 }
		 
	 
	/***
	 * Description It is used to fetch data from MenuMessageTable for update selected Record
	 * @param StrTenant_ID
	 * @param StrCompany_ID
	 * @param StrStatus
	 * @return MENUMESSAGES 
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuMessages_Restfull_MenuMessagesListFetch, method = RequestMethod.GET)
	public @ResponseBody  MENUMESSAGESBEAN fetchMenuMessages(@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Tenant_ID) String StrTenant_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Company_ID) String StrCompany_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Status) String StrStatus,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Type) String StrType,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Message_ID) String StrMessage_ID) throws JASCIEXCEPTION {	

		MENUMESSAGES MenuMessages= ObjectMenuMessagesDao.fetchMenuMessages(StrTenant_ID,StrCompany_ID,StrStatus,StrType,StrMessage_ID); 
		MENUMESSAGESBEAN objMenuMessagesbean=null;
		 DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		 String startDate=null,Enddate=null;
		 
		
		if(StrType.equalsIgnoreCase(GLOBALCONSTANT.TicketTypeMenuMessage))
		{
			
			 objMenuMessagesbean=new MENUMESSAGESBEAN();
			
			 try{
					startDate= ObjDateFormat1.format(MenuMessages.getStart_Date());
					Enddate= ObjDateFormat1.format(MenuMessages.getEnd_Date());

				}catch(Exception e){
					
				}
				objMenuMessagesbean.setType(GLOBALCONSTANT.TicketTypeMenuMessage);
				objMenuMessagesbean.setMessage_ID(MenuMessages.getId().getMessage_ID());
				objMenuMessagesbean.setTenant_ID(MenuMessages.getId().getTenant_ID());
				objMenuMessagesbean.setCompany_ID(MenuMessages.getId().getCompany_ID());
				objMenuMessagesbean.setStatus(MenuMessages.getId().getStatus());
				objMenuMessagesbean.setTicket_Tape_Message(MenuMessages.getTicket_Tape_Message());
				objMenuMessagesbean.setMessage(MenuMessages.getTicket_Tape_Message());
				objMenuMessagesbean.setMessage1(MenuMessages.getMessage());
				objMenuMessagesbean.setStart_Date(startDate);
				objMenuMessagesbean.setEnd_Date(Enddate);
				objMenuMessagesbean.setLast_Activity_Date(MenuMessages.getLast_Activity_Date().toString());
				objMenuMessagesbean.setLast_Activity_Team_Member(MenuMessages.getLast_Activity_Team_Member());
				return objMenuMessagesbean;
				
		}
		if(StrType.equalsIgnoreCase(GLOBALCONSTANT.TypeMenuMessage))
		{
			
			objMenuMessagesbean=new MENUMESSAGESBEAN();
			 try{
					startDate= ObjDateFormat1.format(MenuMessages.getStart_Date());
					Enddate= ObjDateFormat1.format(MenuMessages.getEnd_Date());

				}catch(Exception e){
					
				}
			objMenuMessagesbean.setType(GLOBALCONSTANT.TypeMenuMessage);
			objMenuMessagesbean.setMessage_ID(MenuMessages.getId().getMessage_ID());
			objMenuMessagesbean.setTenant_ID(MenuMessages.getId().getTenant_ID());
			objMenuMessagesbean.setCompany_ID(MenuMessages.getId().getCompany_ID());
			objMenuMessagesbean.setStatus(MenuMessages.getId().getStatus());
			objMenuMessagesbean.setTicket_Tape_Message(MenuMessages.getTicket_Tape_Message());
			objMenuMessagesbean.setMessage(MenuMessages.getTicket_Tape_Message());
			objMenuMessagesbean.setMessage1(MenuMessages.getMessage());
			objMenuMessagesbean.setStart_Date(startDate);
			objMenuMessagesbean.setEnd_Date(Enddate);
			objMenuMessagesbean.setLast_Activity_Date(MenuMessages.getLast_Activity_Date().toString());
			objMenuMessagesbean.setLast_Activity_Team_Member(MenuMessages.getLast_Activity_Team_Member());
			return objMenuMessagesbean;
		}
		return objMenuMessagesbean;	
	}
	
	
		/**
		  * @author Aakash Bishnoi
		  * @Date Dec 16, 2014
		  * Description This RestFull Service used to dao Method for update menu messages
		  * @param ObjectMenuMessage
		  * @throws JASCIEXCEPTION
		  */
		@Transactional
		@RequestMapping(value = GLOBALCONSTANT.MenuMessage_Restfull_RestMenuMessageUpdate, method = RequestMethod.GET)
		public @ResponseBody  String updateEntry(MENUMESSAGES ObjectMenuMessages) throws JASCIEXCEPTION {	
			String Result=null;

			
				Result= ObjectMenuMessagesDao.updateEntry(ObjectMenuMessages);
				
			return Result;
		}
		
		

		/**
		  * @author Aakash Bishnoi
		  * @Date Dec 17, 2014
		  * Description This RestFull Service used to dao Method for update menu messages
		  * @param ObjectMenuMessage
		  * @throws JASCIEXCEPTION
		  */
		@Transactional
		@RequestMapping(value = GLOBALCONSTANT.MenuMessage_Restfull_RestMenuMessageAdd, method = RequestMethod.POST)
		public @ResponseBody  String addEntry(MENUMESSAGES ObjectMenuMessages,String[] CompanyList) throws JASCIEXCEPTION {	
			String Result=null;

			
			
				Result= ObjectMenuMessagesDao.addEntry(ObjectMenuMessages);
			
				
			return Result;
		}
		
		/**
		 * @author Aakash Bishnoi
		 * @Date Dec 17, 2014
		 * Description This is used to convert date format
		 * @param dateInString
		 * @param StrInFormat
		 * @param StrOutFormat
		 * @return String
		 */
		public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
		{
			SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
			DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
			String StrFormattedString=dateInString ;
		 
			try {
		 
				Date date = formatter.parse(dateInString);
				StrFormattedString=dateFormat.format(date);
		 
			} catch (Exception e) {
				
			}
			
			return StrFormattedString;
			
		}
		

				/**
				 * @author Aakash Bishnoi
				 * @Date Dec 18, 2014
				 * Description this is used to check the uniqueness of Ticket Tape Message and Message
				 * @param Tenant_ID
				 * @param Company_ID
				 * @param StrStatus
				 * @param TicketTapeMessage
				 * @param Message
				 * @return MENUMESSAGES
				 * @throws JASCIEXCEPTION
				 */
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.InfoHelp_Restfull_CheckTicketTypeMessage, method = RequestMethod.POST)
				public @ResponseBody  Boolean  checkTicketTypeMessage(@RequestParam(value=GLOBALCONSTANT.MenuMessages_Status) String StrStatus,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Ticket_Type_Message) String StrTicketTypeMessage,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Message1) String StrMessage) throws JASCIEXCEPTION{	

					MENUMESSAGES objMenuMessages= ObjectMenuMessagesDao.CheckTicketTypeMessage(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrStatus,StrTicketTypeMessage,StrMessage);
					
					if(objMenuMessages!=null){
						return true;
					}
					else{

					return false;
					}

				}
				
				/**
				  * @author Aakash Bishnoi
				  * @Date Dec 19, 2014
				  * Description this is used to delete the selected menu messages
				  * @param model
				  * @return Boolean
				  * @throws JASCIEXCEPTION
				  */
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.MenuMessages_Restfull_RestMenuMessageDelete, method = RequestMethod.POST)
				public @ResponseBody  Boolean deleteEntry(@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Message_ID) String StrMessage_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Tenant_ID) String StrTenant_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Company_ID) String StrCompany_ID,@RequestParam(value=GLOBALCONSTANT.MenuMessages_Restfull_Status) String StrStatus) throws JASCIEXCEPTION {	
					Boolean Result=false;

					
						Result= ObjectMenuMessagesDao.deleteEntry(StrMessage_ID,StrTenant_ID,StrCompany_ID,StrStatus);
						
					
					return Result;
				}
				
				
				//It is used to get the label of GeneralCode Management Screen(Add/Edit)
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.MenuMessages_Restfull_RestScreenlabel, method = RequestMethod.POST)
				public @ResponseBody  List<LANGUAGES> getMenuMessagesLabels(String StrLanguage) throws JASCIEXCEPTION {	

					
					
					List<LANGUAGES> ObjMenuMessagesBe = ObjectMenuMessagesDao.getMenuMessagesLabels(StrLanguage); 
					
					return ObjMenuMessagesBe;
				}
}
