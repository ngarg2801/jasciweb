/*

Description This class is used to make restfull service for Security Authorization screen
Created By Deepak Sharma
Created Date Dec 1 2014

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.SECURITYAUTHORIZATIONSCREENBE;
import com.jasci.biz.AdminModule.dao.SECUTITYAUTHORIZATIONDAO;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONS;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONSBE;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class SECURITYAUTHORIZATIONSERVICESAPI {
	@Autowired
	private SECUTITYAUTHORIZATIONDAO ObjSecurityAuthorizationDAO;
	
	@Autowired
	private LANGUANGELABELSAPI ObjLanguageLabelsAPI;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	
	/**
	 * 
	 * @param StrGenaralCode
	 * @param strCompany 
	 * @param strCommTenant 
	 * @return
	 */
	@Transactional
	 @RequestMapping(value = GLOBALCONSTANT.Security_M_getSequrityQuestions, method = RequestMethod.GET)
		public @ResponseBody  List<Object>  getSequrityQuestions(@RequestParam(value=GLOBALCONSTANT.Security_R_P_GeneralCode) String StrGenaralCode, String strCommTenant, String strCompany) throws JASCIEXCEPTION{
		 
		 List<Object> listGeneralCodes=null;
	
			listGeneralCodes = ObjSecurityAuthorizationDAO.getGeneralCodeDataByCodeId(StrGenaralCode,strCommTenant,strCompany);
		
			return listGeneralCodes;
	 }
	/**
	 * 
	 * @param StrUserName
	 * @return
	 * @throws JASCIEXCEPTION 
	 */
	@Transactional
	 @RequestMapping(value = GLOBALCONSTANT.Security_M_checkUserAvailability, method = RequestMethod.POST)
		public @ResponseBody  List<Object>  checkUserAvailability(@RequestParam(value=GLOBALCONSTANT.Security_R_P_UserName) String StrUserName) throws JASCIEXCEPTION {
		 
		 List<Object> listUser=null;
		try {
			listUser = ObjSecurityAuthorizationDAO.checkUserAvailability(StrUserName);
		} catch(Exception obj_e){
			
			throw new JASCIEXCEPTION(obj_e.getMessage());
			
		}
			return listUser;
	 }


	/**
	 * 
	 * @param StrLanguageCode
	 * @return
	 */
	@Transactional
	 @RequestMapping(value =  GLOBALCONSTANT.Security_M_getSecuritySetUpScreenLabel, method = RequestMethod.GET)
		public @ResponseBody  SECURITYAUTHORIZATIONSCREENBE getSecuritySetUpScreenLabel(@RequestParam(value=GLOBALCONSTANT.Security_R_P_LanguageCode) String StrLanguageCode) throws JASCIEXCEPTION {
		 
		 List<LANGUAGES> listLanguages=null;
		 SECURITYAUTHORIZATIONSCREENBE objScreenLabels= new SECURITYAUTHORIZATIONSCREENBE();
		
			listLanguages = ObjLanguageLabelsAPI.GetScreenLabels(LANGUANGELABELSAPI.StrSecurityAuthorizationModule, StrLanguageCode);
			objScreenLabels=ConvertLanguageListToTeamMemberBean(listLanguages);
		
			return objScreenLabels;
	 }
	
	/**
	 * 
	 * @param lstLanguages
	 * @return
	 */
	public SECURITYAUTHORIZATIONSCREENBE ConvertLanguageListToTeamMemberBean(List<LANGUAGES> lstLanguages)
	{
		 SECURITYAUTHORIZATIONSCREENBE objScreenLabels= new SECURITYAUTHORIZATIONSCREENBE();
		 for (LANGUAGES languages : lstLanguages) {
			
			 
			 if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
			    {
			     objScreenLabels.setLblSecurity_ButtonResetText(languages.getTranslation());
			    }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Setup_Lookup))
			 {
				 objScreenLabels.setLblLookUPScreenTitle(languages.getTranslation());
			 }
			/* else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_ID))
			 {
				 objScreenLabels.setLblLookUpEnterTeamMember(languages.getTranslation());
			 }*/
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_the_Team_Member_Name))
			 {
				 objScreenLabels.setLblLookUpPartOfTeamMember(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
			 {
				 objScreenLabels.setLblLookUpDisplayAll(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Team_Member_ID))
			 {
				 objScreenLabels.setErrLookUpNotAvalidTeamMember(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_Team_Member))
			 {
				 objScreenLabels.setLblSelectionPartOfTeamMember(languages.getTranslation());
			 }
			 
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name))
			 {
				 objScreenLabels.setLblSelectionGridName(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
			 {
				 objScreenLabels.setLblSelectionGridActions(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
			 {
				 objScreenLabels.setLblSelectionGridEdit(languages.getTranslation());
			 } else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
			 {
				 objScreenLabels.setLblSelectionGridDelete(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
			 {
				 objScreenLabels.setErrSelectionConfirmDelete(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_edit_as_this_team_member_is_not_active))
			 {
				 objScreenLabels.setErrSelectionUserIsAlreadyInActive(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_delete_this_team_member_as_it_does_not_exist_in_security_autorization_table))
			 {
				 objScreenLabels.setErrSelectionUserNotExists(languages.getTranslation());
			 } else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Setup_Search_Lookup))
			 {
				 objScreenLabels.setLblTeamMemberSelectionTitle(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_part_of_the_Team_Member_Name))
			 {
				 objScreenLabels.setErrPartOfTeamMemberNotLeftBlank(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_a_Team_Member_ID))
			 {
				 objScreenLabels.setErrTeamMemberNotLeftBlank(languages.getTranslation());
			 } 
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Id))
			 {
				 objScreenLabels.setLblSelectionGridTeamMember(languages.getTranslation());
				 objScreenLabels.setLblLookUpEnterTeamMember(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sort_Last_Name))
			 {
				 objScreenLabels.setLblSelectionSortLastName(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sort_First_Name))
			 {
				 objScreenLabels.setLblSelectionSortFirstName(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Setup_Maintenance))
			 {
				 objScreenLabels.setLblSecurityTitle(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
			 {
				 objScreenLabels.setLblSecurityLAstActivityDate(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
			 {
				 objScreenLabels.setLblSecurityLastActivityBy(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_User_ID))
			 {
				 objScreenLabels.setLblSecurityUserID(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Tenant))
			 {
				 objScreenLabels.setLblSecurityTenant(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company))
			 {
				 objScreenLabels.setLblSecurityCompany(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Current_Password))
			 {
				 objScreenLabels.setLblSecurityCurrentPwd(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Question1))
			 {
				 objScreenLabels.setLblSecuritySeqQues1(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Question_Answer1))
			 {
				 objScreenLabels.setLblSecuritySeqAns1(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Question2))
			 {
				 objScreenLabels.setLblSecuritySeqQues2(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Question_Answer2))
			 {
				 objScreenLabels.setLblSecuritySeqAns2(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Question3))
			 {
				 objScreenLabels.setLblSecuritySeqQues3(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Security_Question_Answer3))
			 {
				 objScreenLabels.setLblSecuritySeqAns3(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Password1))
			 {
				 objScreenLabels.setLblSecurityLstPwd1(languages.getTranslation());
			 }
			 
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Password2))
			 {
				 objScreenLabels.setLblSecurityLstPwd2(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Password3))
			 {
				 objScreenLabels.setLblSecurityLstPwd3(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Password4))
			 {
				 objScreenLabels.setLblSecurityLstPwd4(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Password5))
			 {
				 objScreenLabels.setLblSecurityLstPwd5(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Last_Attempts))
			 {
				 objScreenLabels.setLblSecurityNoOfAtmp(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Invalid_Date_YY_MM_DD))
			 {
				 objScreenLabels.setLblSecurityLstInvalidDate(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			 {
				 objScreenLabels.setErrSecurityUserNotLeftBlank(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_UserID_must_be_of_atleast_6_characters_and_must_include_one_number))
			 {
				 objScreenLabels.setErrSecurityUserValid(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			 {
				 objScreenLabels.setErrSecurityPasswordBlank(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(GLOBALCONSTANT.BlankString))
			 {
				 objScreenLabels.setErrSecurityPasswordValid(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_UsedID_already_used))
			 {
				 objScreenLabels.setErrSecurityUserNameNotAvailable(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_All_Security_questions_must_be_unique))
			 {
				 objScreenLabels.setErrSecuritySeqQuesNotValid(languages.getTranslation());
			 }else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Selected_record_has_been_deleted_successfully))
			 {
				 objScreenLabels.setErrSelectionStatusUpdated(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
			 {
				 objScreenLabels.setLblSequrityUpdate(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
			 {
				 objScreenLabels.setLblSequrityDelete(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			 {
				 objScreenLabels.setErrSecuritySeqAns1NotLeftBlank(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			 {
				 objScreenLabels.setErrSecuritySeqAns2NotLeftBlank(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			 {
				 objScreenLabels.setErrSecuritySeqAns3NotLeftBlank(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
			 {
				 objScreenLabels.setInfoStatusUpdatedSuccessFully(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_added_successfully))
			 {
				 objScreenLabels.setInfoStatusAddedSuccessFully(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Status))
			 {
				 objScreenLabels.setLblSelectionGridStatus(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_edit_as_this_team_member_is_not_active))
			 {
				 objScreenLabels.setErrSelectionNotAbleToEdit(languages.getTranslation());
			 }
			 else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_Team_Member_Name))
			    {
			     objScreenLabels.setErrLookUpNotAvalidPartOfTeamMember(languages.getTranslation());
			    }
			    else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Name))
			    {
			     objScreenLabels.setSecurity_labels_TeamMemberName(languages.getTranslation());
			    }
			    else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
			       {
			        objScreenLabels.setSecurity_labels_Select(languages.getTranslation());
			       }
			 
			 // lblSequrityUpdate
			 
		}
		 
		 
		 return objScreenLabels;
	}
	/**
	 * 
	 * @param StrTenant
	 * @param StrTeamMember
	 * @param StrCompany
	 * @return
	 * @throws JASCIEXCEPTION 
	 */
	@Transactional
	 @RequestMapping(value =  GLOBALCONSTANT.Security_M_deleteFromSEQ, method = RequestMethod.POST)
		public @ResponseBody  WEBSERVICESTATUS  deleteUser(@RequestParam(value=GLOBALCONSTANT.Security_R_P_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Security_R_P_TeamMember) String StrTeamMember,@RequestParam(value="Company") String StrCompany) throws JASCIEXCEPTION {
		 
		 WEBSERVICESTATUS listUser=null;
		try {
			listUser = ObjSecurityAuthorizationDAO.deleteTeamMember(StrTenant,StrTeamMember,StrCompany);
		}  catch (Exception Obj_e) {
			
			throw new JASCIEXCEPTION(Obj_e.getMessage());
		}
			return listUser;
	 }
	
	/**
	 * 
	 * @param StrTenant
	 * @param StrFullfilmentCenter
	 * @return
	 */
	
	@Transactional
	 @RequestMapping(value = GLOBALCONSTANT.Security_M_getAllTeamMembers, method = RequestMethod.GET)
		public @ResponseBody  List<Object>  getAllTeamMembers(@RequestParam(value=GLOBALCONSTANT.Security_R_P_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Security_R_P_FullfillmentCenter) String StrFullfilmentCenter) throws JASCIEXCEPTION{
		
		
		List<Object> ObjListTeamMembers=ObjSecurityAuthorizationDAO.getAllTeamMember(StrTenant,StrFullfilmentCenter);
		return ObjListTeamMembers; 
	}
	/**
	 * 
	 * @param StrTenant
	 * @param StrTeamMember
	 * @return
	 * @throws INTERNETCONNECTIONERROREXCEPTION
	 * @throws DATABASENOTFOUNDEXCEPTION
	 * @throws DATABASEEXCEPTION
	 */
	
	@Transactional
	 @RequestMapping(value = GLOBALCONSTANT.Security_M_getLoginUserDetails, method = RequestMethod.GET)
		public @ResponseBody  List<SECURITYAUTHORIZATIONS>  getSecurityAuthorizationData(@RequestParam(value=GLOBALCONSTANT.Security_R_P_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Security_R_P_TeamMember) String StrTeamMember) throws JASCIEXCEPTION {
		
		
		List<SECURITYAUTHORIZATIONS> ObjListSecurityAuthorizations = ObjSecurityAuthorizationDAO.getSecurityAuthorizationDataByTeamMemberName(StrTeamMember,StrTenant);
		
	//	List<TEAMMEMBERS> ObjListTeamMembers=ObjTeamMemberDao.getAllTeamMember(StrTenant,StrFullfilmentCenter);
		return ObjListSecurityAuthorizations; 
	}

	@Transactional
	 @RequestMapping(value = GLOBALCONSTANT.UrlAddTeamMemberInSecurityAuthorization, method = RequestMethod.POST)
		public @ResponseBody  String  addOrUpdateSequrityAuthorization(SECURITYAUTHORIZATIONSBE objUpdateSecurityAuthBe) throws JASCIEXCEPTION {
		
		SECURITYAUTHORIZATIONS objUpdateSecurityAuth =new SECURITYAUTHORIZATIONS();
		Date lastDate=null;
		Date pwdDate=null;
		Date invalideDate=null;
		SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

		try {
			 lastDate = formatter.parse(objUpdateSecurityAuthBe.getLastActivityDate());			
		}catch(Exception objException){}
		
		objUpdateSecurityAuth.setLastActivityDate(lastDate);
		
		try {
			invalideDate = formatter.parse(objUpdateSecurityAuthBe.getInvalidAttemptDate());			
		}catch(Exception objException){
			
		}
		
		objUpdateSecurityAuth.setInvalidAttemptDate(invalideDate);
		
		try {
			pwdDate = formatter.parse(objUpdateSecurityAuthBe.getPasswordDate());			
		}catch(Exception objException){}
		objUpdateSecurityAuth.setPasswordDate(pwdDate);		
		objUpdateSecurityAuth.setCompany(trimValues(objUpdateSecurityAuthBe.getCompany()));
		objUpdateSecurityAuth.setLastActivityTeamMember(trimValues(objUpdateSecurityAuthBe.getLastActivityTeamMember()));
		objUpdateSecurityAuth.setLastPassword1(trimValues(objUpdateSecurityAuthBe.getLastPassword1()));
		objUpdateSecurityAuth.setLastPassword2(trimValues(objUpdateSecurityAuthBe.getLastPassword2()));
		objUpdateSecurityAuth.setLastPassword3(trimValues(objUpdateSecurityAuthBe.getLastPassword3()));
		objUpdateSecurityAuth.setLastPassword4(trimValues(objUpdateSecurityAuthBe.getLastPassword4()));
		objUpdateSecurityAuth.setLastPassword5(trimValues(objUpdateSecurityAuthBe.getLastPassword5()));		
		objUpdateSecurityAuth.setNumberAttempts(objUpdateSecurityAuthBe.getNumberAttempts());
		objUpdateSecurityAuth.setPassword(trimValues(objUpdateSecurityAuthBe.getPassword()));
		objUpdateSecurityAuth.setSecurityQuestion1(trimValues(objUpdateSecurityAuthBe.getSecurityQuestion1()));
		objUpdateSecurityAuth.setSecurityQuestion2(trimValues(objUpdateSecurityAuthBe.getSecurityQuestion2()));
		objUpdateSecurityAuth.setSecurityQuestion3(trimValues(objUpdateSecurityAuthBe.getSecurityQuestion3()));
		objUpdateSecurityAuth.setSecurityQuestionAnswer1(trimValues(objUpdateSecurityAuthBe.getSecurityQuestionAnswer1()));
		objUpdateSecurityAuth.setSecurityQuestionAnswer2(trimValues(objUpdateSecurityAuthBe.getSecurityQuestionAnswer2()));
		objUpdateSecurityAuth.setSecurityQuestionAnswer3(trimValues(objUpdateSecurityAuthBe.getSecurityQuestionAnswer3()));		
		objUpdateSecurityAuth.setTeamMember(trimValues(objUpdateSecurityAuthBe.getTeamMember()));
		objUpdateSecurityAuth.setTenant(trimValues(objUpdateSecurityAuthBe.getTenant()));
		objUpdateSecurityAuth.setUserId(trimValues(objUpdateSecurityAuthBe.getUserId()));
		objUpdateSecurityAuth.setStatus(trimValues(objUpdateSecurityAuthBe.getStatus()));
		
		//objUpdateSecurityAuth.getLastActivityTeamMember()
		String StrStatus=ObjSecurityAuthorizationDAO.addOrUpdateSequrityAuthorization(objUpdateSecurityAuth);
		
	//	List<TEAMMEMBERS> ObjListTeamMembers=ObjTeamMemberDao.getAllTeamMember(StrTenant,StrFullfilmentCenter);
		return StrStatus; 
	}
	
	@Transactional
	@RequestMapping(value =  GLOBALCONSTANT.UrlGetTeamMember, method = RequestMethod.POST)
	public List<Object> getTeamMemberByName(String TeamMemberName,String PartOfTeamMember,String StrTenant1,String StrFullFillmentCenter1) throws JASCIEXCEPTION 
	{
List<Object> ObjListTeamMembers=null;
String StrTenant=OBJCOMMONSESSIONBE.getTenant();
String StrFullFillmentCenter=OBJCOMMONSESSIONBE.getFulfillmentCenter();
		
		if((!TeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && TeamMemberName!=null && TeamMemberName.length()>0) || (PartOfTeamMember.equalsIgnoreCase(GLOBALCONSTANT.BlankString) || (PartOfTeamMember==null))){
			ObjListTeamMembers=ObjSecurityAuthorizationDAO.getTeamMemberByTeamMemberID(TeamMemberName, StrTenant, StrFullFillmentCenter);
		}else if((!TeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && TeamMemberName!=null) && PartOfTeamMember!=null ){

			ObjListTeamMembers=ObjSecurityAuthorizationDAO.getTeamMemberByTeamMemberID(TeamMemberName, StrTenant, StrFullFillmentCenter);
		}
		else{
			ObjListTeamMembers=ObjSecurityAuthorizationDAO.getTeamMemberByPartOfTeamMember(PartOfTeamMember, StrTenant, StrFullFillmentCenter);//(TeamMemberName, StrTenant, StrFullFillmentCenter);
		}
		return ObjListTeamMembers;
	}
	
	@Transactional
	@RequestMapping(value =  GLOBALCONSTANT.UrlGetTeamMemberByPartName, method = RequestMethod.POST)
	public @ResponseBody List<Object> getTeamMemberByPartName(@RequestParam(value=GLOBALCONSTANT.PartOfTeamMember) String PartOfTeamMember) throws JASCIEXCEPTION 
	{
		List<Object> ObjListTeamMembers=null;		
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrFullFillmentCenter=OBJCOMMONSESSIONBE.getFulfillmentCenter();
		ObjListTeamMembers=ObjSecurityAuthorizationDAO.getTeamMemberByPartOfTeamMember(PartOfTeamMember, StrTenant, StrFullFillmentCenter);//(TeamMemberName, StrTenant, StrFullFillmentCenter);
		return ObjListTeamMembers;
	}
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.UrlGetTeamMemberById, method = RequestMethod.POST)
	public @ResponseBody List<Object> getTeamMemberByPartId(@RequestParam(value=GLOBALCONSTANT.TeamMember) String TeamMemberId) throws JASCIEXCEPTION 
	{
		List<Object> ObjListTeamMembers=null;		
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrFullFillmentCenter=OBJCOMMONSESSIONBE.getFulfillmentCenter();
		ObjListTeamMembers=ObjSecurityAuthorizationDAO.getTeamMemberByTeamMemberID(TeamMemberId, StrTenant, StrFullFillmentCenter);
		//ObjListTeamMembers=ObjSecurityAuthorizationDAO.getTeamMemberByPartOfTeamMember(TeamMemberId, StrTenant, StrFullFillmentCenter);//(TeamMemberName, StrTenant, StrFullFillmentCenter);
		return ObjListTeamMembers;
	}
	
	
	/**
	 * 
	 * @param StrTeamMember
	 * @return
	 */
	@Transactional
	  @RequestMapping(value = GLOBALCONSTANT.Security_M_getTeamMemberEmail, method = RequestMethod.GET)
	  public @ResponseBody  List<Object> getTeamMemberEmailID(@RequestParam(value=GLOBALCONSTANT.Security_R_P_TeamMember) String StrTeamMember){
	  
	  
	  List<Object> lstEmailID = ObjSecurityAuthorizationDAO.getTeamMemberEmailID(StrTeamMember);
	  
	 // List<TEAMMEMBERS> ObjListTeamMembers=ObjTeamMemberDao.getAllTeamMember(StrTenant,StrFullfilmentCenter);
	  return lstEmailID; 
	 }
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 31, 2014
	 * Description This function is used to trim the space
	 * @param Value
	 * @return String
	 */
	public static String trimValues(String Value){
		if(Value == null){
			return null;
		}
		else{
			String CheckValue=Value.trim();
			if(CheckValue.length()<GLOBALCONSTANT.IntOne){
				return null;
			}else{						
				return CheckValue;
			}
		}
	}
}
