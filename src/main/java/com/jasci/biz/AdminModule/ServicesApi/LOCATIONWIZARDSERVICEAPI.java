/**

Description : This class provide all of the functionality related to database call DAO Class for Location Wizard screen 
Created By : Pradeep Kumar  
Created Date: JAN 05 2015
 */
package com.jasci.biz.AdminModule.ServicesApi;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARD;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARDPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;



@Controller
public class LOCATIONWIZARDSERVICEAPI {

	@Autowired
	private COMMONSESSIONBE objCommonsessionbe;
	@Autowired
	private ILOCATIONWIZARDDAO ObjectLocationWizardDao;

	String SelectedFulfillmentCenterValue=GLOBALCONSTANT.BlankString;


	/*String StrTenant=objCommonsessionbe.getTenant();
	String StrCompany=objCommonsessionbe.getCompany();
	 */
	/**
	 * 
	 * Created By:Pradeep Kumar
	 * @return List<LOCATIONWIZARD>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description :Search For Wizard Control Number and give data 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetData, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  getSearchByWizardControlNumber(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_Control_Number) String wizardControlNumber) throws JASCIEXCEPTION {


		List<Object> ListLocationWizard=null;

		ListLocationWizard = ObjectLocationWizardDao.getSearchBYWizardControlNumber(wizardID, wizardControlNumber);			

		return ListLocationWizard;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * @return List<LOCATIONWIZARD>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description : Search For Area and give data 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetDataSearchArea, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  getSearchByArea(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Area) String Area) throws JASCIEXCEPTION {


		List<Object> ListLocationWizard=null;

		ListLocationWizard = ObjectLocationWizardDao.getSearchByArea(wizardID, Area);			

		return ListLocationWizard;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * @return List<LOCATIONWIZARD>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description :Search For Location Typer and give data 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetDataSearchLocationType, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  getSearchByLocationType(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_LOCATION_TYPE) String LocationType) throws JASCIEXCEPTION {


		List<Object> ListLocationWizard=null;

		ListLocationWizard = ObjectLocationWizardDao.getSearchByLocationType(wizardID, LocationType);			

		return ListLocationWizard;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * @return List<LOCATIONWIZARD>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 12, 2015
	 * @Description :Search For AnyPartOfWizardDiscription and give data 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_AnyPartofWizardDiscription, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  getSearchByAnyPartofWizardDiscription(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_AnyPartofWizardDiscription) String AnyPartofWizardDiscription) throws JASCIEXCEPTION {


		List<Object> ListLocationWizard=null;

		ListLocationWizard = ObjectLocationWizardDao.getSearchByAnyPartofWizardDiscription(wizardID, AnyPartofWizardDiscription);			

		return ListLocationWizard;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * @return List<GENERALCODES>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description : Give DropDown Value
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetDrop_DownValue, method = RequestMethod.GET)
	public @ResponseBody List<GENERALCODES> getDropDownValuesList(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_General_Code_ID) String GeneralCodeId) throws JASCIEXCEPTION
	{
		List<GENERALCODES> ListLocationWizardSelect=null;

		ListLocationWizardSelect = ObjectLocationWizardDao.getDropDownValuesList(GeneralCodeId);	
		return ListLocationWizardSelect;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * @return List<FULFILLMENTCENTERSPOJO>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description :give fulfillment dropdown values
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetDrop_DownValue_Fulfillment, method = RequestMethod.GET)
	public @ResponseBody List<FULFILLMENTCENTERSPOJO> getDropDownValuesFulfillmentList() throws JASCIEXCEPTION
	{
		List<FULFILLMENTCENTERSPOJO> ListLocationWizardSelect=null;

		ListLocationWizardSelect = ObjectLocationWizardDao.getDropDownValueFulfillmentList();	
		return ListLocationWizardSelect;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * @return List<LOCATIONWIZARD>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 12, 2015
	 * @Description :give Data for grid
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_Get_Data_For_Grid, method = RequestMethod.GET)
	public @ResponseBody List<Object> getLocationWizardData(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID) throws JASCIEXCEPTION
	{
		List<Object> locationwizardsList=ObjectLocationWizardDao.getLocationWizardData(wizardID);

		return locationwizardsList;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar        
	 * @return List<LOCATIONWIZARD>
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 13, 2015
	 * @Description :save and update
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_Get_Data_SaveAndUpdate, method = RequestMethod.GET)
	public @ResponseBody List<Object> saveAndUpdate(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_LocationWizardObj) LOCATIONWIZARD objLocationwizard) throws JASCIEXCEPTION
	{

		//	ObjectLocationWizardDao.saveAndUpdate(objLocationWizard);

		@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
		List<Object> recordList=new ArrayList();
		List<String> location= new ArrayList<String>();
		long DuplicateRecordNumber = GLOBALCONSTANT.INT_ZERO;
		long SaveRecordNumber = GLOBALCONSTANT.INT_ZERO;
		
		
		try{					
			/**Insert Wizard Location*/
			//boolean saveorupdate = true;
			objLocationwizard  = ObjectLocationWizardDao.saveAndUpdate(objLocationwizard,true);
		    			
			/**End*/
		}catch(Exception e){}
		
		
		try
		{
			String rowNumberOfCharacters=objLocationwizard.getROW_NUMBER_CHARACTERS();
			String aisleNumberOfCharacters=objLocationwizard.getAISLE_NUMBER_CHARACTERS();
			String bayNumberOfCharacters=objLocationwizard.getBAY_NUMBER_CHARACTERS();
			String levelNumberOfCharacters=objLocationwizard.getLEVEL_NUMBER_CHARACTERS();
			String slotNumberOfCharacters=objLocationwizard.getSLOT_NUMBER_CHARACTERS();
			if(rowNumberOfCharacters!=GLOBALCONSTANT.BlankString || aisleNumberOfCharacters!=GLOBALCONSTANT.BlankString || bayNumberOfCharacters!=GLOBALCONSTANT.BlankString || levelNumberOfCharacters!=GLOBALCONSTANT.BlankString || slotNumberOfCharacters!=GLOBALCONSTANT.BlankString)
			{
				
				/**Create alpha and numeric array and insert locations into database*/

				recordList.add(GLOBALCONSTANT.INT_ZERO,SaveRecordNumber);
				recordList.add(GLOBALCONSTANT.IntOne,DuplicateRecordNumber);

				String rowAlphaOrNumeric = GLOBALCONSTANT.BlankString;
				String rowNumberOfCharacter = GLOBALCONSTANT.BlankString;
				String rowStarting = GLOBALCONSTANT.BlankString; 
				String rowEnding = GLOBALCONSTANT.BlankString;
				try{
				rowAlphaOrNumeric=objLocationwizard.getROW_CHARACTER_SET().toUpperCase();
				}catch(Exception e){}
				try{
				rowNumberOfCharacter=objLocationwizard.getROW_NUMBER_CHARACTERS().toUpperCase();
				}catch(Exception e){}
				try{
				rowStarting=objLocationwizard.getROW_STARTING().toUpperCase();
				}catch(Exception e){}
				try{
				rowEnding=objLocationwizard.getROW_NUMBER_OF().toUpperCase();
				}catch(Exception e){}
				


				List<String> rowArray=null;
				String aisleAlphaOrNumeric=GLOBALCONSTANT.BlankString,aisleNumberOfCharacter=GLOBALCONSTANT.BlankString,
						aisleStarting=GLOBALCONSTANT.BlankString,aisleEnding=GLOBALCONSTANT.BlankString,aisleSeparatorCharacter=GLOBALCONSTANT.BlankString;
				if(objLocationwizard.getAISLE_CHARACTER_SET()!=null )
					aisleAlphaOrNumeric=objLocationwizard.getAISLE_CHARACTER_SET().toUpperCase();
				if(objLocationwizard.getAISLE_NUMBER_CHARACTERS()!=null)
					aisleNumberOfCharacter=objLocationwizard.getAISLE_NUMBER_CHARACTERS().toUpperCase();
				if(objLocationwizard.getAISLE_STARTING()!=null )
					aisleStarting=objLocationwizard.getAISLE_STARTING().toUpperCase();
				if(objLocationwizard.getAISLE_NUMBER_OF()!=null )
					aisleEnding=objLocationwizard.getAISLE_NUMBER_OF().toUpperCase();
				if(objLocationwizard.getSEPARATOR_ROW_AISLE()!=null )
					aisleSeparatorCharacter=objLocationwizard.getSEPARATOR_ROW_AISLE().toUpperCase();

				List<String> aisleArray=null;
				String bayAlphaOrNumeric=GLOBALCONSTANT.BlankString,bayNumberOfCharacter=GLOBALCONSTANT.BlankString,
						bayStarting=GLOBALCONSTANT.BlankString,bayEnding=GLOBALCONSTANT.BlankString,baySeparatorCharacter=GLOBALCONSTANT.BlankString;
				if(objLocationwizard.getBAY_CHARACTER_SET()!=null)
					bayAlphaOrNumeric=objLocationwizard.getBAY_CHARACTER_SET().toUpperCase();
				if(objLocationwizard.getBAY_NUMBER_CHARACTERS()!=null )
					bayNumberOfCharacter=objLocationwizard.getBAY_NUMBER_CHARACTERS().toUpperCase();
				if(objLocationwizard.getBAY_STARTING()!=null )
					bayStarting=objLocationwizard.getBAY_STARTING().toUpperCase();
				if(objLocationwizard.getBAY_NUMBER_OF()!=null )
					bayEnding=objLocationwizard.getBAY_NUMBER_OF().toUpperCase();
				if(objLocationwizard.getSEPARATOR_AISLE_BAY()!=null )
					baySeparatorCharacter=objLocationwizard.getSEPARATOR_AISLE_BAY().toUpperCase();

				List<String> bayArray=null;

				String levelAlphaOrNumeric=GLOBALCONSTANT.BlankString;
				try{
					levelAlphaOrNumeric=objLocationwizard.getLEVEL_CHARACTER_SET().toUpperCase();
				}catch(Exception objException){
					levelAlphaOrNumeric=objLocationwizard.getLEVEL_CHARACTER_SET();
				}
				String levelNumberOfCharacter=GLOBALCONSTANT.BlankString;
				try{
					levelNumberOfCharacter=objLocationwizard.getLEVEL_NUMBER_CHARACTERS().toUpperCase();
				}catch(Exception objException){
					levelNumberOfCharacter=objLocationwizard.getLEVEL_NUMBER_CHARACTERS();
				}
				String levelStarting=GLOBALCONSTANT.BlankString;
				try{
					levelStarting=objLocationwizard.getLEVEL_STARTING().toUpperCase();
				}catch(Exception objException){
					levelStarting=objLocationwizard.getLEVEL_STARTING();
				}
				String levelEnding;
				try{
					levelEnding=objLocationwizard.getLEVEL_NUMBER_OF().toUpperCase();
				}catch(Exception objException){
					levelEnding=objLocationwizard.getLEVEL_NUMBER_OF();
				}
				String levelSeparatorCharacter=GLOBALCONSTANT.BlankString;
				try{
					levelSeparatorCharacter=objLocationwizard.getSEPARATOR_BAY_LEVEL().toUpperCase();
				}catch(Exception objException){
					levelSeparatorCharacter=objLocationwizard.getSEPARATOR_BAY_LEVEL();
				}


				List<String> levelArray=null;

				String slotAlphaOrNumeric=GLOBALCONSTANT.BlankString,slotNumberOfCharacter=GLOBALCONSTANT.BlankString,
						slotStarting=GLOBALCONSTANT.BlankString,slotEnding=GLOBALCONSTANT.BlankString,slotSeparatorCharacter=GLOBALCONSTANT.BlankString;
				if(objLocationwizard.getSLOT_CHARACTER_SET()!=null)
					slotAlphaOrNumeric=objLocationwizard.getSLOT_CHARACTER_SET().toUpperCase();
				if(objLocationwizard.getSLOT_NUMBER_CHARACTERS()!=null)
					slotNumberOfCharacter=objLocationwizard.getSLOT_NUMBER_CHARACTERS().toUpperCase();
				if(objLocationwizard.getSLOT_STARTING()!=null)
					slotStarting=objLocationwizard.getSLOT_STARTING().toUpperCase();
				if(objLocationwizard.getSLOT_NUMBER_OF()!=null)
					slotEnding=objLocationwizard.getSLOT_NUMBER_OF().toUpperCase();
				if(objLocationwizard.getSEPARATOR_LEVEL_SLOT()!=null )
					slotSeparatorCharacter=objLocationwizard.getSEPARATOR_LEVEL_SLOT().toUpperCase();
				List<String> slotArray=null;


				// for  Row Array 
				if(objLocationwizard.getROW_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
				{
					rowArray=getArrayOfIncrementStrings(rowNumberOfCharacter, rowStarting, rowEnding, rowAlphaOrNumeric);

				}

				// for  Aisle Array 
				if(objLocationwizard.getAISLE_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
				{
					aisleArray=getArrayOfIncrementStrings(aisleNumberOfCharacter, aisleStarting, aisleEnding, aisleAlphaOrNumeric);
				}
				// for  Bay Array 
				if(objLocationwizard.getBAY_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
				{
					bayArray=getArrayOfIncrementStrings(bayNumberOfCharacter, bayStarting, bayEnding, bayAlphaOrNumeric);
				}

				// for  Level Array 

				if(objLocationwizard.getLEVEL_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
				{
					levelArray=getArrayOfIncrementStrings(levelNumberOfCharacter, levelStarting, levelEnding, levelAlphaOrNumeric);

				}

				// for  Slot Array 
				if(objLocationwizard.getSLOT_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
				{
					slotArray=getArrayOfIncrementStrings(slotNumberOfCharacter, slotStarting, slotEnding, slotAlphaOrNumeric);
				}
                /**Create File Name*/
				UUID UuidObjForMsg = UUID.randomUUID();
				String DuplicateLocationFileName = GLOBALCONSTANT.DestinationDirectory+UuidObjForMsg.toString()+GLOBALCONSTANT.FileExtention;
				//String DuplicateLocationFileName = "JASCI_PHASEONE/DuplicateLocFile/"+UuidObjForMsg.toString()+GLOBALCONSTANT.FileExtention;
				/**End*/


				if(rowArray!=null)
				{
					for(String rowValue : rowArray)
					{
						if(aisleArray!=null)
						{
							for(String aisleValue : aisleArray)
							{
								if(bayArray!=null)
								{
									for(String bayValue : bayArray)
									{
										if(levelArray!=null)
										{
											for(String levelValue : levelArray)
											{
												if(slotArray!=null)
												{
													for(String slotValue : slotArray)
													{
														String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
														location.add(locationValue);
														if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
														{
															recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
															location.clear();
														}


													}
												}
												else
												{
													String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue;
													location.add(locationValue);
													if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
													{
														recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
														location.clear();
													}
												}
											}
										}
										else
										{
											if(slotArray!=null)
											{
												for(String slotValue : slotArray)
												{
													String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue+slotSeparatorCharacter+slotValue;
													location.add(locationValue);
													if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
													{
														recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
														location.clear();
													}
												}
											}
											else
											{
												String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
									}
								}
								else
								{
									if(levelArray!=null)
									{
										for(String levelValue : levelArray)
										{
											if(slotArray!=null)
											{
												for(String slotValue : slotArray)
												{
													String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
													location.add(locationValue);
													if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
													{
														recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
														location.clear();
													}
												}
											}
											else
											{
												String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+levelSeparatorCharacter+levelValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
									}
									else
									{
										if(slotArray!=null)
										{
											for(String slotValue : slotArray)
											{
												String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+slotSeparatorCharacter+slotValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
										else
										{
											String locationValue=rowValue+aisleSeparatorCharacter+aisleValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
								}
							}
						}
						else
						{
							if(bayArray!=null)
							{
								for(String bayValue : bayArray)
								{
									if(levelArray!=null)
									{
										for(String levelValue : levelArray)
										{
											if(slotArray!=null)
											{
												for(String slotValue : slotArray)
												{
													String locationValue=rowValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
													location.add(locationValue);
													if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
													{
														recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
														location.clear();
													}
												}
											}
											else
											{
												String locationValue=rowValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
									}
									else
									{
										if(slotArray!=null)
										{
											for(String slotValue : slotArray)
											{
												String locationValue=rowValue+baySeparatorCharacter+bayValue+slotSeparatorCharacter+slotValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
										else
										{
											String locationValue=rowValue+baySeparatorCharacter+bayValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
								}
							}
							else
							{
								if(levelArray!=null)
								{
									for(String levelValue : levelArray)
									{
										if(slotArray!=null)
										{
											for(String slotValue : slotArray)
											{
												String locationValue=rowValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
										else
										{
											String locationValue=rowValue+levelSeparatorCharacter+levelValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
								}
								else
								{
									if(slotArray!=null)
									{
										for(String slotValue : slotArray)
										{
											String locationValue=rowValue+slotSeparatorCharacter+slotValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
									else
									{
										String locationValue=rowValue;
										location.add(locationValue);
										if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
										{
											recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
											location.clear();
										}
									}
								}
							}
						}
					}
				}
				else
				{
					if(aisleArray!=null)
					{
						for(String aisleValue : aisleArray)
						{
							if(bayArray!=null)
							{
								for(String bayValue : bayArray)
								{
									if(levelArray!=null)
									{
										for(String levelValue : levelArray)
										{
											if(slotArray!=null)
											{
												for(String slotValue : slotArray)
												{
													String locationValue=aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
													location.add(locationValue);
													if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
													{
														recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
														location.clear();
													}
												}
											}
											else
											{
												String locationValue=aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
									}
									else
									{
										if(slotArray!=null)
										{
											for(String slotValue : slotArray)
											{
												String locationValue=aisleValue+baySeparatorCharacter+bayValue+slotSeparatorCharacter+slotValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
										else
										{
											String locationValue=aisleValue+baySeparatorCharacter+bayValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
								}
							}
							else
							{
								if(levelArray!=null)
								{
									for(String levelValue : levelArray)
									{
										if(slotArray!=null)
										{
											for(String slotValue : slotArray)
											{
												String locationValue=aisleValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
										else
										{
											String locationValue=aisleValue+levelSeparatorCharacter+levelValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
								}
								else
								{
									if(slotArray!=null)
									{
										for(String slotValue : slotArray)
										{
											String locationValue=aisleValue+slotSeparatorCharacter+slotValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
									else
									{
										String locationValue=aisleSeparatorCharacter+aisleValue;
										location.add(locationValue);
										if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
										{
											recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
											location.clear();
										}
									}
								}
							}
						}
					}
					else
					{
						if(bayArray!=null)
						{
							for(String bayValue : bayArray)
							{
								if(levelArray!=null)
								{
									for(String levelValue : levelArray)
									{
										if(slotArray!=null)
										{
											for(String slotValue : slotArray)
											{
												String locationValue=bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
												location.add(locationValue);
												if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
												{
													recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
													location.clear();
												}
											}
										}
										else
										{
											String locationValue=bayValue+levelSeparatorCharacter+levelValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
								}
								else
								{
									if(slotArray!=null)
									{
										for(String slotValue : slotArray)
										{
											String locationValue=bayValue+slotSeparatorCharacter+slotValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
									else
									{
										String locationValue=bayValue;
										location.add(locationValue);
										if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
										{
											recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
											location.clear();
										}
									}
								}
							}
						}
						else
						{
							if(levelArray!=null)
							{
								for(String levelValue : levelArray)
								{
									if(slotArray!=null)
									{
										for(String slotValue : slotArray)
										{
											String locationValue=levelValue+slotSeparatorCharacter+slotValue;
											location.add(locationValue);
											if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
											{
												recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
												location.clear();
											}
										}
									}
									else
									{
										String locationValue=levelValue;
										location.add(locationValue);
										if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
										{
											recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
											location.clear();
										}
									}
								}
							}
							else
							{
								if(slotArray!=null)
								{
									for(String slotValue : slotArray)
									{
										String locationValue=slotValue;
										location.add(locationValue);
										if(location.size() == GLOBALCONSTANT.INT_ONELAKH)
										{
											recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
											location.clear();
										}
									}
								}

							}
						}
					}
				}
				/**End*/
			recordList=ObjectLocationWizardDao.saveLocations(location,objLocationwizard,recordList,DuplicateLocationFileName);
			//int listSize=recordList.size();
			SaveRecordNumber = (Long) recordList.get(GLOBALCONSTANT.INT_ZERO);
			recordList.add(DuplicateLocationFileName);
			
			if (SaveRecordNumber > GLOBALCONSTANT.INT_ZERO) {
				objLocationwizard.setSTATUS(GLOBALCONSTANT.AppliedOrNot);
			} else {
				objLocationwizard.setSTATUS(GLOBALCONSTANT.AppliedNot);
			}

			ObjectLocationWizardDao.saveAndUpdate(objLocationwizard,false);
		}
		else
		{
			objLocationwizard.setLAST_ACTIVITY_DATE(null);
			objLocationwizard.setLAST_ACTIVITY_TEAM_MEMBER(GLOBALCONSTANT.BlankString);
			objLocationwizard.setSTATUS(GLOBALCONSTANT.AppliedNot);
			ObjectLocationWizardDao.saveAndUpdate(objLocationwizard,false);
			recordList.add(GLOBALCONSTANT.INT_ZERO,GLOBALCONSTANT.OnlyWizardCreated);
			
		}
	}
	catch(Exception e)
	{
	
	}
	return recordList;
}

/*@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetDrop_DownValue_LocationProfile, method = RequestMethod.GET)*/
public @ResponseBody List<LOCATIONPROFILES> getDropDownValueLocationProfile(String fulfillmentCenter) throws JASCIEXCEPTION
{
	List<LOCATIONPROFILES> ListLocationWizardSelect=null;

	ListLocationWizardSelect = ObjectLocationWizardDao.getDropDownValueLocationProfile(fulfillmentCenter);	
	return ListLocationWizardSelect;
}
/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :List<LOCATIONWIZARD>
 * @throws JASCIEXCEPTION
 * @Date : Jan 14, 2015
 * @Description :get copy
 */

@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetData_for_copy, method = RequestMethod.GET)
public @ResponseBody List<LOCATIONWIZARD> getLocationWizardCopyEdit(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER) String wizardControlNumber,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Rest_Fulfillment) String fulfillmentID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Tenant_ID) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Restfull_Company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.Copy) String copyOrEdit) throws JASCIEXCEPTION
{
	List<LOCATIONWIZARD> ListLocationWizard=null;

	ListLocationWizard = ObjectLocationWizardDao.getLocationWizardCopyEdit(wizardID,wizardControlNumber,fulfillmentID,StrTenant,StrCompany, copyOrEdit);	
	return ListLocationWizard;
}

/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :List<LOCATION>
 * @throws LOCATIONWIZARDSERVICEAPI
 * @Date : Jan 16, 2015
 * @Description :check location list
 */
@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_GetData_Check_Location_List, method = RequestMethod.POST)
public @ResponseBody List<LOCATION> checkLocationList(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER) String wizardControlNumber,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Rest_Fulfillment) String fulfillmentID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Tenant_ID) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Restfull_Company) String StrCompany) throws JASCIEXCEPTION    
{
	StrTenant=objCommonsessionbe.getTenant();
	StrCompany=objCommonsessionbe.getCompany();


	return ObjectLocationWizardDao.checkLocationforDeletion(wizardID,wizardControlNumber,fulfillmentID,StrTenant,StrCompany);	

}

/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :boolean
 * @throws LOCATIONWIZARDSERVICEAPI
 * @Date : Jan 15, 2015
 * @Description : used for delete wizard Only
 */

@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_Delete_Wizard_Only, method = RequestMethod.POST)
public @ResponseBody boolean deleteWizardOnly(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER) String wizardControlNumber,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Rest_Fulfillment) String fulfillmentID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Tenant_ID) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Restfull_Company) String StrCompany) throws JASCIEXCEPTION    
{
	StrTenant=objCommonsessionbe.getTenant();
	StrCompany=objCommonsessionbe.getCompany();
	LOCATIONWIZARDPK objPK = new LOCATIONWIZARDPK();
	objPK.setCOMPANY_ID(StrCompany);
	objPK.setTENANT_ID(StrTenant);
	objPK.setWIZARD_CONTROL_NUMBER(Long.parseLong(wizardControlNumber));
	objPK.setWIZARD_ID(wizardID);
	objPK.setFULFILLMENT_CENTER_ID(fulfillmentID);


	boolean status=ObjectLocationWizardDao.deleteWizardOnly(objPK);
	return status;
}
/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :List<INVENTORY_LEVELS>
 * @throws LOCATIONWIZARDSERVICEAPI
 * @Date : Jan 16, 2015
 * @Description :check list and Inventory level both
 */

@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_Check_Location_List_And_Inventory_Level, method = RequestMethod.POST)
public @ResponseBody List<LOCATION> checkListInventryLevels(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER) String wizardControlNumber,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Rest_Fulfillment) String fulfillmentID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Tenant_ID) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Restfull_Company) String StrCompany) throws JASCIEXCEPTION    
{
	StrTenant=objCommonsessionbe.getTenant();
	StrCompany=objCommonsessionbe.getCompany();
	List<LOCATION> listLocations= ObjectLocationWizardDao.checkLocationforDeletion(wizardID,wizardControlNumber,fulfillmentID,StrTenant,StrCompany);

	return listLocations;

}


/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :boolean
 * @throws LOCATIONWIZARDSERVICEAPI
 * @Date : Jan 16, 2015
 * @Description :Delete wizard and locations
 */

@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_Delete_Wizard_And_Location, method = RequestMethod.POST)
public @ResponseBody boolean deleteWizardAndLocation(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER) String wizardControlNumber,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Rest_Fulfillment) String fulfillmentID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Tenant_ID) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Restfull_Company) String StrCompany) throws JASCIEXCEPTION    
{
	StrTenant=objCommonsessionbe.getTenant();
	StrCompany=objCommonsessionbe.getCompany();
	boolean status=false;
	

				//delete from location  all
				ObjectLocationWizardDao.deleteWizardAndLocation(wizardID,wizardControlNumber,fulfillmentID);

				status=true;
	return status;

}

/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :long
 * @throws LOCATIONWIZARDSERVICEAPI
 * @Date : Jan 16, 2015
 * @Description : No of location for particular wizard value
 */
@Transactional
public long getNumberOfLocation(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER) Long wizardControlNumber,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Rest_Fulfillment) String fulfillmentID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Tenant_ID) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Restfull_Company) String StrCompany) throws JASCIEXCEPTION
{
	LOCATIONWIZARDPK objpk=new LOCATIONWIZARDPK();
	objpk.setCOMPANY_ID(StrCompany);
	objpk.setFULFILLMENT_CENTER_ID(fulfillmentID);
	objpk.setTENANT_ID(StrTenant);
	objpk.setWIZARD_CONTROL_NUMBER(wizardControlNumber);
	objpk.setWIZARD_ID(wizardID);
	long nooflocations=ObjectLocationWizardDao.getNumberOfLocation(objpk);
	return nooflocations;
}

/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :List<LOCATIONWIZARD>
 * @throws LOCATIONWIZARDSERVICEAPI
 * @Date : Jan 20, 2015
 * @Description :check for adding time Primary key Exist or not
 */
@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_CheckExisting, method = RequestMethod.POST)
public @ResponseBody List<LOCATIONWIZARD> getLocationWizardCheckExit(@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Wizard_ID) String wizardID,@RequestParam(value=GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER) String wizardControlNumber) throws JASCIEXCEPTION
{
	List<LOCATIONWIZARD> ListLocationWizard=null;
	String fulfillmentID=objCommonsessionbe.getFulfillmentCenter();
	String fulfilmentCenterId=fulfillmentID;
	String StrTenant=objCommonsessionbe.getTenant();
	String StrCompany=objCommonsessionbe.getCompany();
	String copyOrEdit=GLOBALCONSTANT.dffr;
	ListLocationWizard = ObjectLocationWizardDao.getLocationWizardCopyEdit(wizardID,wizardControlNumber,fulfilmentCenterId,StrTenant,StrCompany, copyOrEdit);	
	return ListLocationWizard;
} 
/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :List<FULFILLMENTCENTERSPOJO>
 * @throws LOCATIONWIZARDSERVICEAPI
 * @Date : Jan 21, 2015
 * @Description :getFulfillmentList
 */

@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_FulfillmentList, method = RequestMethod.POST)
public @ResponseBody List<FULFILLMENTCENTERSPOJO> getFulfillmentList(@RequestParam(value=GLOBALCONSTANT.Location_Wizard_Rest_Fulfillment) String fulfillmentID,@RequestParam(value=GLOBALCONSTANT.Loction_Wizard_RestFull_Tenant_ID) String StrTenant) throws JASCIEXCEPTION
{

	FULFILLMENTCENTERS_PK objfulfillPK=new FULFILLMENTCENTERS_PK();
	objfulfillPK.setFulfillmentCenter(fulfillmentID);
	objfulfillPK.setTenant(StrTenant);


	return ObjectLocationWizardDao.getFulfillmentList(objfulfillPK);
}

/**
 * 
 * 
 * Created By:Pradeep Kumar
 * @return :LOCATIONWIZARD
 * @throws JASCIEXCEPTION
 * @Date : Jan 21, 2015
 * @Description :
 */
@Transactional
public LOCATIONWIZARD getMaxWizardControlNumber() throws JASCIEXCEPTION
{
	LOCATIONWIZARD locationWizard=new LOCATIONWIZARD();
	try
	{
		LOCATIONWIZARDPK objPk =new LOCATIONWIZARDPK();
		objPk.setWIZARD_CONTROL_NUMBER(GLOBALCONSTANT.Location_Wizard_WIZARD_Con_No);
		objPk.setCOMPANY_ID(GLOBALCONSTANT.Location_Wizard_COMPANY_ID);
		objPk.setFULFILLMENT_CENTER_ID(GLOBALCONSTANT.Location_Wizard_FULFILLMENT_CENTER_ID);
		objPk.setWIZARD_ID(GLOBALCONSTANT.Wizard_ID);
		objPk.setTENANT_ID(GLOBALCONSTANT.Location_Wizard_TENANT_ID);
		locationWizard.setId(objPk);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return ObjectLocationWizardDao.getMaxWizardControlNumber(locationWizard);

}

/**
 * @Description it is used to get team member name from TeamMembers
 * @author Aakash Bishnoi
 * @date 31 dec 2014
 * @param Tenant
 * @param TeamMember
 * @return List<TEAMMEMBERS>
 * @throws JASCIEXCEPTION
 */
@Transactional
@RequestMapping(value = GLOBALCONSTANT.LocationWizard_Restfull_GetTeamMember, method = RequestMethod.GET)
public @ResponseBody String getTeamMemberName(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_TeamMember) String TeamMember) throws JASCIEXCEPTION {	


	//return ObjectLocationMaintenanceDao.getTeamMemberName(Tenant, TeamMember);
	String TeamMemberName=GLOBALCONSTANT.BlankString;

	List<TEAMMEMBERS> objTeammembers= ObjectLocationWizardDao.getTeamMemberName(Tenant, TeamMember);

	TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
	return TeamMemberName;


}


public List<String> getArrayOfIncrementStrings(String NumberOfCharacter,String Starting,String Ending,String CharacterSetType)
{

	List<String> array=new ArrayList<String>();

	if(CharacterSetType.equalsIgnoreCase(GLOBALCONSTANT.ALPHA))
	{

		String StrFirst = GLOBALCONSTANT.BlankString;
		String StrSecond = GLOBALCONSTANT.BlankString;
		
		
		if(Starting.length() < Ending.length())
		{
			int LengthDifference = Ending.length() - Starting.length();
			for(int index = GLOBALCONSTANT.INT_ZERO ; index < LengthDifference;index++ )
			{
				Starting = GLOBALCONSTANT.SYMBOLATTHERATE + Starting;
			}
		}else{
			int LengthDifference = Starting.length() - Ending.length();
			for(int index = GLOBALCONSTANT.INT_ZERO ; index < LengthDifference;index++ )
			{
				Ending = GLOBALCONSTANT.SYMBOLATTHERATE + Ending;
			}
		}

		String returngreaterstring = findgreater(Starting,Ending);
		if(Starting==returngreaterstring)			
		{
			StrFirst = Ending;
			StrSecond = Starting;
		}else{
			StrFirst = Starting;
			StrSecond = Ending;
		}
		array.add(StrFirst.replaceAll(GLOBALCONSTANT.SYMBOLATTHERATE, GLOBALCONSTANT.BlankString));
		while(true)
		{

			String returnString=GLOBALCONSTANT.BlankString;
			if(Starting.equalsIgnoreCase(Ending)){
				break;

			}else{
				returnString = StringIncrement(StrFirst);
			}
			StrFirst = returnString;
			array.add(StrFirst.replaceAll(GLOBALCONSTANT.SYMBOLATTHERATE, GLOBALCONSTANT.BlankString));
			if(returnString.equals(StrSecond))
			{
				break;
			}
		}
	}
	else
	{
		String StrFirst = GLOBALCONSTANT.BlankString;
		String StrSecond = GLOBALCONSTANT.BlankString;
		
		if(Starting.length() < Ending.length())
		{
			
			int LengthDifference = Ending.length() - Starting.length();
			for(int lengthindex = GLOBALCONSTANT.INT_ZERO ; lengthindex < LengthDifference;lengthindex++ )
			{
				Starting = GLOBALCONSTANT.STRING_ZERO + Starting;
			}
		}else{
			int LengthDifference = Starting.length() - Ending.length();
			for(int lengthindex = GLOBALCONSTANT.INT_ZERO ; lengthindex < LengthDifference;lengthindex++ )
			{
				Ending = GLOBALCONSTANT.STRING_ZERO + Ending;
			}
		}
		
		String returngreaterstring=GLOBALCONSTANT.BlankString;
		returngreaterstring = findgreater(Starting,Ending);

		if(Starting==returngreaterstring)			
		{
			StrFirst = Ending;
			StrSecond = Starting;

		}else{
			StrFirst = Starting;
			StrSecond = Ending;
		}	
		array.add(StrFirst);
		while(true)	

		{	
			String returnString=GLOBALCONSTANT.BlankString;
			if(Starting.equalsIgnoreCase(Ending)){
				break;

			}else{

				returnString = IntegerIncrement(StrFirst);
				
				StrFirst = returnString;
			}

			array.add(StrFirst);
			if(returnString.equals(StrSecond))
			{
				break;
			}

		}
	}

	return array;
}

public List<String> getArray(String NumberOfCharacter,String Starting,String Ending,String CharacterSetType)
{





	List<String> array=new ArrayList<String>();

	if(CharacterSetType.equalsIgnoreCase(GLOBALCONSTANT.ALPHA))
	{
		HashMap<String,Integer> alphaValues=new HashMap<String, Integer>();
		HashMap<Integer,String> alphaValuesOnkey=new HashMap<Integer,String>();

		alphaValues.put(GLOBALCONSTANT.A,GLOBALCONSTANT.IntOne);
		alphaValues.put(GLOBALCONSTANT.B,GLOBALCONSTANT.INT_TWO);
		alphaValues.put(GLOBALCONSTANT.C,GLOBALCONSTANT.INT_THREE);
		alphaValues.put(GLOBALCONSTANT.D,GLOBALCONSTANT.INT_FOUR);
		alphaValues.put(GLOBALCONSTANT.E,GLOBALCONSTANT.INT_FIVE);
		alphaValues.put(GLOBALCONSTANT.F,GLOBALCONSTANT.INT_SIX);
		alphaValues.put(GLOBALCONSTANT.G,GLOBALCONSTANT.INT_SEVEN);
		alphaValues.put(GLOBALCONSTANT.H,GLOBALCONSTANT.INT_EIGHT);
		alphaValues.put(GLOBALCONSTANT.I,GLOBALCONSTANT.INT_NINE);
		alphaValues.put(GLOBALCONSTANT.J,GLOBALCONSTANT.INT_TEN);
		alphaValues.put(GLOBALCONSTANT.K,GLOBALCONSTANT.INT_ELEVEN);
		alphaValues.put(GLOBALCONSTANT.L,GLOBALCONSTANT.INT_TWELVE);
		alphaValues.put(GLOBALCONSTANT.M,GLOBALCONSTANT.INT_THIRTEEN);
		alphaValues.put(GLOBALCONSTANT.NSTRING,GLOBALCONSTANT.INT_FOURTEEN);
		alphaValues.put(GLOBALCONSTANT.O,GLOBALCONSTANT.INT_FIFTEEN);
		alphaValues.put(GLOBALCONSTANT.P,GLOBALCONSTANT.INT_SIXTEEN);
		alphaValues.put(GLOBALCONSTANT.Q,GLOBALCONSTANT.INT_SEVENTEEN);
		alphaValues.put(GLOBALCONSTANT.R,GLOBALCONSTANT.INT_EIGHTEEN);
		alphaValues.put(GLOBALCONSTANT.S,GLOBALCONSTANT.INT_NINETEEN);
		alphaValues.put(GLOBALCONSTANT.T,GLOBALCONSTANT.INT_TWENTY);
		alphaValues.put(GLOBALCONSTANT.U,GLOBALCONSTANT.INT_TWENTYONE);
		alphaValues.put(GLOBALCONSTANT.V,GLOBALCONSTANT.INT_TWENTYTWO);
		alphaValues.put(GLOBALCONSTANT.W,GLOBALCONSTANT.INT_TWENTYTHREE);
		alphaValues.put(GLOBALCONSTANT.X,GLOBALCONSTANT.INT_TWENTYFOUR);
		alphaValues.put(GLOBALCONSTANT.YSTRING,GLOBALCONSTANT.INT_TWENTYFIVE);
		alphaValues.put(GLOBALCONSTANT.Z,GLOBALCONSTANT.INT_TWENTYSIX);


		alphaValuesOnkey.put(GLOBALCONSTANT.IntOne,GLOBALCONSTANT.A);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWO,GLOBALCONSTANT.B);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_THREE,GLOBALCONSTANT.C);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_FOUR,GLOBALCONSTANT.D);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_FIVE,GLOBALCONSTANT.E);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_SIX,GLOBALCONSTANT.F);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_SEVEN,GLOBALCONSTANT.G);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_EIGHT,GLOBALCONSTANT.H);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_NINE,GLOBALCONSTANT.I);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TEN,GLOBALCONSTANT.J);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_ELEVEN,GLOBALCONSTANT.K);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWELVE,GLOBALCONSTANT.L);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_THIRTEEN,GLOBALCONSTANT.M);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_FOURTEEN,GLOBALCONSTANT.NSTRING);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_FIFTEEN,GLOBALCONSTANT.O);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_SIXTEEN,GLOBALCONSTANT.P);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_SEVENTEEN,GLOBALCONSTANT.Q);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_EIGHTEEN,GLOBALCONSTANT.R);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_NINETEEN,GLOBALCONSTANT.S);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWENTY,GLOBALCONSTANT.T);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWENTYONE,GLOBALCONSTANT.U);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWENTYTWO,GLOBALCONSTANT.V);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWENTYTHREE,GLOBALCONSTANT.W);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWENTYFOUR,GLOBALCONSTANT.X);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWENTYFIVE,GLOBALCONSTANT.YSTRING);
		alphaValuesOnkey.put(GLOBALCONSTANT.INT_TWENTYSIX,GLOBALCONSTANT.Z);

		int StartingAt=alphaValues.get(Starting.toUpperCase());
		int EndingAt=alphaValues.get(Ending.toUpperCase());
		if(StartingAt<EndingAt)
		{
			

			for(int Position=StartingAt;Position<=EndingAt;Position++)
			{
				array.add(alphaValuesOnkey.get(Position));

			}
			
		}
		else 
		{
			for(int Position=StartingAt;Position>=EndingAt;Position--)
			{
				array.add(alphaValuesOnkey.get(Position));

			}
		}
	}
	else
	{



		Integer.parseInt(NumberOfCharacter);
		int StaringIntValue=Integer.parseInt(Starting);
		int EndingIntValue=Integer.parseInt(Ending);

		if(StaringIntValue<EndingIntValue)
		{
			
			for(int Position=StaringIntValue;Position<=EndingIntValue;Position++)
			{
				array.add(String.valueOf(Position));
			}
		}
		else 
		{
			for(int Position=StaringIntValue;Position>=EndingIntValue;Position--)
			{
				array.add(String.valueOf(Position));

			}
		}

	}

	return array;
}



@Transactional
@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Restfull_InsertLocations, method = RequestMethod.POST)
public List<String> getAllListForLocationInsert(LOCATIONWIZARD objLocationwizard)
{

	String rowAlphaOrNumeric=objLocationwizard.getROW_CHARACTER_SET().toUpperCase();
	String rowNumberOfCharacter=objLocationwizard.getROW_NUMBER_CHARACTERS().toUpperCase();
	String rowStarting=objLocationwizard.getROW_STARTING().toUpperCase();
	String rowEnding=objLocationwizard.getROW_NUMBER_OF().toUpperCase();


	List<String> rowArray=null;
	String aisleAlphaOrNumeric=GLOBALCONSTANT.BlankString,aisleNumberOfCharacter=GLOBALCONSTANT.BlankString,
			aisleStarting=GLOBALCONSTANT.BlankString,aisleEnding=GLOBALCONSTANT.BlankString,aisleSeparatorCharacter=GLOBALCONSTANT.BlankString;
	if(objLocationwizard.getAISLE_CHARACTER_SET()!=null )
		aisleAlphaOrNumeric=objLocationwizard.getAISLE_CHARACTER_SET().toUpperCase();
	if(objLocationwizard.getAISLE_NUMBER_CHARACTERS()!=null)
		aisleNumberOfCharacter=objLocationwizard.getAISLE_NUMBER_CHARACTERS().toUpperCase();
	if(objLocationwizard.getAISLE_STARTING()!=null )
		aisleStarting=objLocationwizard.getAISLE_STARTING().toUpperCase();
	if(objLocationwizard.getAISLE_NUMBER_OF()!=null )
		aisleEnding=objLocationwizard.getAISLE_NUMBER_OF().toUpperCase();
	if(objLocationwizard.getSEPARATOR_ROW_AISLE()!=null )
		aisleSeparatorCharacter=objLocationwizard.getSEPARATOR_ROW_AISLE().toUpperCase();

	List<String> aisleArray=null;
	String bayAlphaOrNumeric=GLOBALCONSTANT.BlankString,bayNumberOfCharacter=GLOBALCONSTANT.BlankString,
			bayStarting=GLOBALCONSTANT.BlankString,bayEnding=GLOBALCONSTANT.BlankString,baySeparatorCharacter=GLOBALCONSTANT.BlankString;
	if(objLocationwizard.getBAY_CHARACTER_SET()!=null)
		bayAlphaOrNumeric=objLocationwizard.getBAY_CHARACTER_SET().toUpperCase();
	if(objLocationwizard.getBAY_NUMBER_CHARACTERS()!=null )
		bayNumberOfCharacter=objLocationwizard.getBAY_NUMBER_CHARACTERS().toUpperCase();
	if(objLocationwizard.getBAY_STARTING()!=null )
		bayStarting=objLocationwizard.getBAY_STARTING().toUpperCase();
	if(objLocationwizard.getBAY_NUMBER_OF()!=null )
		bayEnding=objLocationwizard.getBAY_NUMBER_OF().toUpperCase();
	if(objLocationwizard.getSEPARATOR_AISLE_BAY()!=null )
		baySeparatorCharacter=objLocationwizard.getSEPARATOR_AISLE_BAY().toUpperCase();

	List<String> bayArray=null;

	String levelAlphaOrNumeric=GLOBALCONSTANT.BlankString;
	try{
		levelAlphaOrNumeric=objLocationwizard.getLEVEL_CHARACTER_SET().toUpperCase();
	}catch(Exception objException){
		levelAlphaOrNumeric=objLocationwizard.getLEVEL_CHARACTER_SET();
	}
	String levelNumberOfCharacter=GLOBALCONSTANT.BlankString;
	try{
		levelNumberOfCharacter=objLocationwizard.getLEVEL_NUMBER_CHARACTERS().toUpperCase();
	}catch(Exception objException){
		levelNumberOfCharacter=objLocationwizard.getLEVEL_NUMBER_CHARACTERS();
	}
	String levelStarting=GLOBALCONSTANT.BlankString;
	try{
		levelStarting=objLocationwizard.getLEVEL_STARTING().toUpperCase();
	}catch(Exception objException){
		levelStarting=objLocationwizard.getLEVEL_STARTING();
	}
	String levelEnding;
	try{
		levelEnding=objLocationwizard.getLEVEL_NUMBER_OF().toUpperCase();
	}catch(Exception objException){
		levelEnding=objLocationwizard.getLEVEL_NUMBER_OF();
	}
	String levelSeparatorCharacter=GLOBALCONSTANT.BlankString;
	try{
		levelSeparatorCharacter=objLocationwizard.getSEPARATOR_BAY_LEVEL().toUpperCase();
	}catch(Exception objException){
		levelSeparatorCharacter=objLocationwizard.getSEPARATOR_BAY_LEVEL();
	}


	List<String> levelArray=null;

	String slotAlphaOrNumeric=GLOBALCONSTANT.BlankString,slotNumberOfCharacter=GLOBALCONSTANT.BlankString,
			slotStarting=GLOBALCONSTANT.BlankString,slotEnding=GLOBALCONSTANT.BlankString,slotSeparatorCharacter=GLOBALCONSTANT.BlankString;
	if(objLocationwizard.getSLOT_CHARACTER_SET()!=null)
		slotAlphaOrNumeric=objLocationwizard.getSLOT_CHARACTER_SET().toUpperCase();
	if(objLocationwizard.getSLOT_NUMBER_CHARACTERS()!=null)
		slotNumberOfCharacter=objLocationwizard.getSLOT_NUMBER_CHARACTERS().toUpperCase();
	if(objLocationwizard.getSLOT_STARTING()!=null)
		slotStarting=objLocationwizard.getSLOT_STARTING().toUpperCase();
	if(objLocationwizard.getSLOT_NUMBER_OF()!=null)
		slotEnding=objLocationwizard.getSLOT_NUMBER_OF().toUpperCase();
	if(objLocationwizard.getSEPARATOR_LEVEL_SLOT()!=null )
		slotSeparatorCharacter=objLocationwizard.getSEPARATOR_LEVEL_SLOT().toUpperCase();
	List<String> slotArray=null;


	// for  Row Array 
	if(objLocationwizard.getROW_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
	{
		rowArray=getArrayOfIncrementStrings(rowNumberOfCharacter, rowStarting, rowEnding, rowAlphaOrNumeric);

	}

	// for  Aisle Array 
	if(objLocationwizard.getAISLE_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
	{
		aisleArray=getArrayOfIncrementStrings(aisleNumberOfCharacter, aisleStarting, aisleEnding, aisleAlphaOrNumeric);
	}
	// for  Bay Array 
	if(objLocationwizard.getBAY_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
	{
		bayArray=getArrayOfIncrementStrings(bayNumberOfCharacter, bayStarting, bayEnding, bayAlphaOrNumeric);
	}

	// for  Level Array 

	if(objLocationwizard.getLEVEL_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
	{
		levelArray=getArrayOfIncrementStrings(levelNumberOfCharacter, levelStarting, levelEnding, levelAlphaOrNumeric);

	}

	// for  Slot Array 
	if(objLocationwizard.getSLOT_NUMBER_CHARACTERS()!=GLOBALCONSTANT.BlankString)
	{
		slotArray=getArrayOfIncrementStrings(slotNumberOfCharacter, slotStarting, slotEnding, slotAlphaOrNumeric);
	}


	List<String> location= new ArrayList<String>();
	if(rowArray!=null)
	{
		for(String rowValue : rowArray)
		{
			if(aisleArray!=null)
			{
				for(String aisleValue : aisleArray)
				{
					if(bayArray!=null)
					{
						for(String bayValue : bayArray)
						{
							if(levelArray!=null)
							{
								for(String levelValue : levelArray)
								{
									if(slotArray!=null)
									{
										for(String slotValue : slotArray)
										{
											String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
											location.add(locationValue);


										}
									}
									else
									{
										String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue;
										location.add(locationValue);
									}
								}
							}
							else
							{
								if(slotArray!=null)
								{
									for(String slotValue : slotArray)
									{
										String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue+slotSeparatorCharacter+slotValue;
										location.add(locationValue);
										
									}
								}
								else
								{
									String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+baySeparatorCharacter+bayValue;
									location.add(locationValue);
								}
							}
						}
					}
					else
					{
						if(levelArray!=null)
						{
							for(String levelValue : levelArray)
							{
								if(slotArray!=null)
								{
									for(String slotValue : slotArray)
									{
										String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
										location.add(locationValue);
									}
								}
								else
								{
									String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+levelSeparatorCharacter+levelValue;
									location.add(locationValue);
								}
							}
						}
						else
						{
							if(slotArray!=null)
							{
								for(String slotValue : slotArray)
								{
									String locationValue=rowValue+aisleSeparatorCharacter+aisleValue+slotSeparatorCharacter+slotValue;
									location.add(locationValue);
								}
							}
							else
							{
								String locationValue=rowValue+aisleSeparatorCharacter+aisleValue;
								location.add(locationValue);
							}
						}
					}
				}
			}
			else
			{
				if(bayArray!=null)
				{
					for(String bayValue : bayArray)
					{
						if(levelArray!=null)
						{
							for(String levelValue : levelArray)
							{
								if(slotArray!=null)
								{
									for(String slotValue : slotArray)
									{
										String locationValue=rowValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
										location.add(locationValue);
									}
								}
								else
								{
									String locationValue=rowValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue;
									location.add(locationValue);
								}
							}
						}
						else
						{
							if(slotArray!=null)
							{
								for(String slotValue : slotArray)
								{
									String locationValue=rowValue+baySeparatorCharacter+bayValue+slotSeparatorCharacter+slotValue;
									location.add(locationValue);
								}
							}
							else
							{
								String locationValue=rowValue+baySeparatorCharacter+bayValue;
								location.add(locationValue);
							}
						}
					}
				}
				else
				{
					if(levelArray!=null)
					{
						for(String levelValue : levelArray)
						{
							if(slotArray!=null)
							{
								for(String slotValue : slotArray)
								{
									String locationValue=rowValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
									location.add(locationValue);
								}
							}
							else
							{
								String locationValue=rowValue+levelSeparatorCharacter+levelValue;
								location.add(locationValue);
							}
						}
					}
					else
					{
						if(slotArray!=null)
						{
							for(String slotValue : slotArray)
							{
								String locationValue=rowValue+slotSeparatorCharacter+slotValue;
								location.add(locationValue);
							}
						}
						else
						{
							String locationValue=rowValue;
							location.add(locationValue);
						}
					}
				}
			}
		}
	}
	else
	{
		if(aisleArray!=null)
		{
			for(String aisleValue : aisleArray)
			{
				if(bayArray!=null)
				{
					for(String bayValue : bayArray)
					{
						if(levelArray!=null)
						{
							for(String levelValue : levelArray)
							{
								if(slotArray!=null)
								{
									for(String slotValue : slotArray)
									{
										String locationValue=aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
										location.add(locationValue);
									}
								}
								else
								{
									String locationValue=aisleValue+baySeparatorCharacter+bayValue+levelSeparatorCharacter+levelValue;
									location.add(locationValue);
								}
							}
						}
						else
						{
							if(slotArray!=null)
							{
								for(String slotValue : slotArray)
								{
									String locationValue=aisleValue+baySeparatorCharacter+bayValue+slotSeparatorCharacter+slotValue;
									location.add(locationValue);
								}
							}
							else
							{
								String locationValue=aisleValue+baySeparatorCharacter+bayValue;
								location.add(locationValue);
							}
						}
					}
				}
				else
				{
					if(levelArray!=null)
					{
						for(String levelValue : levelArray)
						{
							if(slotArray!=null)
							{
								for(String slotValue : slotArray)
								{
									String locationValue=aisleValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
									location.add(locationValue);
								}
							}
							else
							{
								String locationValue=aisleValue+levelSeparatorCharacter+levelValue;
								location.add(locationValue);
							}
						}
					}
					else
					{
						if(slotArray!=null)
						{
							for(String slotValue : slotArray)
							{
								String locationValue=aisleValue+slotSeparatorCharacter+slotValue;
								location.add(locationValue);
							}
						}
						else
						{
							String locationValue=aisleSeparatorCharacter+aisleValue;
							location.add(locationValue);
						}
					}
				}
			}
		}
		else
		{
			if(bayArray!=null)
			{
				for(String bayValue : bayArray)
				{
					if(levelArray!=null)
					{
						for(String levelValue : levelArray)
						{
							if(slotArray!=null)
							{
								for(String slotValue : slotArray)
								{
									String locationValue=bayValue+levelSeparatorCharacter+levelValue+slotSeparatorCharacter+slotValue;
									location.add(locationValue);
								}
							}
							else
							{
								String locationValue=bayValue+levelSeparatorCharacter+levelValue;
								location.add(locationValue);
							}
						}
					}
					else
					{
						if(slotArray!=null)
						{
							for(String slotValue : slotArray)
							{
								String locationValue=bayValue+slotSeparatorCharacter+slotValue;
								location.add(locationValue);
							}
						}
						else
						{
							String locationValue=bayValue;
							location.add(locationValue);
						}
					}
				}
			}
			else
			{
				if(levelArray!=null)
				{
					for(String levelValue : levelArray)
					{
						if(slotArray!=null)
						{
							for(String slotValue : slotArray)
							{
								String locationValue=levelValue+slotSeparatorCharacter+slotValue;
								location.add(locationValue);
							}
						}
						else
						{
							String locationValue=levelValue;
							location.add(locationValue);
						}
					}
				}
				else
				{
					if(slotArray!=null)
					{
						for(String slotValue : slotArray)
						{
							String locationValue=slotValue;
							location.add(locationValue);
						}
					}

				}
			}
		}
	}
	return location;
}

/**
 * @author Pradeep Kumar
 * @Date Feb 10 ,2015
 * Description:It is used to get the selected fulfillment center for showing the location profile dropdown.
 * @param StrFullfilmentCenter
 * @return Boolean
 * @throws JASCIEXCEPTION
 */

@Transactional
@RequestMapping(value = GLOBALCONSTANT.LocationMaintenanaceWizard_Restfull_RestSelectFulfillmentCenterValue, method = RequestMethod.GET)
public @ResponseBody  List<LOCATIONPROFILES>  getFulfillmentValue(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FullfilmentCenter) String StrFullfilmentCenter) throws JASCIEXCEPTION {
	SelectedFulfillmentCenterValue=StrFullfilmentCenter;
	List<LOCATIONPROFILES> ConvertListLocationProfile= getDropDownValueLocationProfile(StrFullfilmentCenter);
	return ConvertListLocationProfile;
}

/** 
 * @author Shailendra Kumar
 * @Date Feb 20 ,2015 
 * @Description: It is used to get the next string for a given string.
 * @param startstr
 * @return return next string
 */
public String StringIncrement(String startstr)
{

	String endCharter = GLOBALCONSTANT.Char_Z;
	int endCharterSmall = GLOBALCONSTANT.Char_z.charAt(GLOBALCONSTANT.INT_ZERO);
	String firstvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.IntOne,startstr.length());
	int firstcharValue = firstvalue.charAt(GLOBALCONSTANT.INT_ZERO);
	int endcharvalue = endCharter.charAt(GLOBALCONSTANT.INT_ZERO);
	String FinalString = GLOBALCONSTANT.BlankString;
	if(((firstcharValue == endCharterSmall) || (firstcharValue == endcharvalue)) && startstr.length()>=GLOBALCONSTANT.INT_TWO)
	{
		String secondvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_TWO,startstr.length()-GLOBALCONSTANT.IntOne);
		int secondcharValue = secondvalue.charAt(GLOBALCONSTANT.INT_ZERO);
		if(secondcharValue == endcharvalue && startstr.length()>=GLOBALCONSTANT.INT_THREE)
		{
			String thirdvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_THREE,startstr.length()-GLOBALCONSTANT.INT_TWO);
			int thirdcharValue = thirdvalue.charAt(GLOBALCONSTANT.INT_ZERO);
			if(thirdcharValue == endcharvalue && startstr.length()>=GLOBALCONSTANT.INT_FOUR)
			{
				String fourthvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_FOUR,startstr.length()-GLOBALCONSTANT.INT_THREE);
				int fourthcharValue = fourthvalue.charAt(GLOBALCONSTANT.INT_ZERO);
				if(fourthcharValue == endcharvalue && startstr.length()>=GLOBALCONSTANT.INT_FIVE)
				{
					String fifthvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_FIVE,startstr.length()-GLOBALCONSTANT.INT_FOUR);
					int fifthcharValue = fifthvalue.charAt(GLOBALCONSTANT.INT_ZERO);

					String next = String.valueOf( (char) (fifthcharValue + GLOBALCONSTANT.IntOne));
					FinalString = next+GLOBALCONSTANT.Char_A+GLOBALCONSTANT.Char_A+GLOBALCONSTANT.Char_A+GLOBALCONSTANT.Char_A;

				}else
				{
					String next = String.valueOf( (char) (fourthcharValue + GLOBALCONSTANT.IntOne));
					FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.INT_FOUR)+next+GLOBALCONSTANT.Char_A+GLOBALCONSTANT.Char_A+GLOBALCONSTANT.Char_A;
				}
			}else
			{
				String next = String.valueOf( (char) (thirdcharValue + GLOBALCONSTANT.IntOne));
				FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.INT_THREE)+next+GLOBALCONSTANT.Char_A+GLOBALCONSTANT.Char_A;
			}
		}else
		{
			String next = String.valueOf( (char) (secondcharValue + GLOBALCONSTANT.IntOne));
			FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.INT_TWO)+next+GLOBALCONSTANT.Char_A;
		}
	}else
	{
		String next = String.valueOf( (char) (firstcharValue + GLOBALCONSTANT.IntOne));
		FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.IntOne)+next;
	}

	return FinalString;
}

/** 
 * @author Shailendra Kumar
 * @Date Feb 20 ,2015 
 * @Description: It is used to get the next string for a given string.
 * @param startstr
 * @return return next string
 */
public String IntegerIncrement(String startstr)
{

	String endCharter = GLOBALCONSTANT.STRING_NINE;
	String firstvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.IntOne,startstr.length());
	int firstcharValue = firstvalue.charAt(GLOBALCONSTANT.INT_ZERO);
	int endcharvalue = endCharter.charAt(GLOBALCONSTANT.INT_ZERO);
	String FinalString = GLOBALCONSTANT.BlankString;
	if(firstcharValue == endcharvalue && startstr.length()>=GLOBALCONSTANT.INT_TWO)
	{
		String secondvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_TWO,startstr.length()-GLOBALCONSTANT.IntOne);
		int secondcharValue = secondvalue.charAt(GLOBALCONSTANT.INT_ZERO);
		if(secondcharValue == endcharvalue && startstr.length()>=GLOBALCONSTANT.INT_THREE)
		{
			String thirdvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_THREE,startstr.length()-GLOBALCONSTANT.INT_TWO);
			int thirdcharValue = thirdvalue.charAt(GLOBALCONSTANT.INT_ZERO);
			if(thirdcharValue == endcharvalue && startstr.length()>=GLOBALCONSTANT.INT_FOUR)
			{
				String fourthvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_FOUR,startstr.length()-GLOBALCONSTANT.INT_THREE);
				int fourthcharValue = fourthvalue.charAt(GLOBALCONSTANT.INT_ZERO);
				if(fourthcharValue == endcharvalue && startstr.length()>=GLOBALCONSTANT.INT_FIVE)
				{
					String fifthvalue = startstr.substring(startstr.length()-GLOBALCONSTANT.INT_FIVE,startstr.length()-GLOBALCONSTANT.INT_FOUR);
					int fifthcharValue = fifthvalue.charAt(GLOBALCONSTANT.INT_ZERO);

					String next = String.valueOf( (char) (fifthcharValue + GLOBALCONSTANT.IntOne));
					FinalString = next+GLOBALCONSTANT.INT_ZERO+GLOBALCONSTANT.INT_ZERO+GLOBALCONSTANT.INT_ZERO+GLOBALCONSTANT.INT_ZERO;

				}else
				{
					String next = String.valueOf( (char) (fourthcharValue + GLOBALCONSTANT.IntOne));
					FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.INT_FOUR)+next+GLOBALCONSTANT.INT_ZERO+GLOBALCONSTANT.INT_ZERO+GLOBALCONSTANT.INT_ZERO;
				}
			}else
			{
				String next = String.valueOf( (char) (thirdcharValue + GLOBALCONSTANT.IntOne));
				FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.INT_THREE)+next+GLOBALCONSTANT.INT_ZERO+GLOBALCONSTANT.INT_ZERO;
			}
		}else
		{
			String next = String.valueOf( (char) (secondcharValue + GLOBALCONSTANT.IntOne));
			FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.INT_TWO)+next+GLOBALCONSTANT.INT_ZERO;
		}
	}else
	{
		String next = String.valueOf( (char) (firstcharValue + GLOBALCONSTANT.IntOne));
		FinalString = startstr.substring(GLOBALCONSTANT.INT_ZERO,startstr.length()-GLOBALCONSTANT.IntOne)+next;
	}

	return FinalString;
}

/** 
 * @author Shailendra Kumar
 * @Date Feb 20 ,2015 
 * @Description: It is used to get the greater string from given two string.
 * @param startstr
 * @return return next string
 */
public static String findgreater(String startstr, String Endstr)
{

	String returnString = GLOBALCONSTANT.BlankString;
	if(startstr.charAt(GLOBALCONSTANT.INT_ZERO)==Endstr.charAt(GLOBALCONSTANT.INT_ZERO) && Endstr.length()>GLOBALCONSTANT.IntOne && startstr.length()>GLOBALCONSTANT.IntOne)
	{
		if(startstr.charAt(GLOBALCONSTANT.IntOne)==Endstr.charAt(GLOBALCONSTANT.IntOne) && Endstr.length()>GLOBALCONSTANT.INT_TWO && startstr.length()>GLOBALCONSTANT.INT_TWO)
		{

			if(startstr.charAt(GLOBALCONSTANT.INT_TWO)==Endstr.charAt(GLOBALCONSTANT.INT_TWO) && Endstr.length()>GLOBALCONSTANT.INT_THREE && startstr.length()>GLOBALCONSTANT.INT_THREE)
			{

				if(startstr.charAt(GLOBALCONSTANT.INT_THREE)==Endstr.charAt(GLOBALCONSTANT.INT_THREE) && Endstr.length()>GLOBALCONSTANT.INT_FOUR && startstr.length()>GLOBALCONSTANT.INT_FOUR)
				{

					if(startstr.charAt(GLOBALCONSTANT.INT_FOUR)==Endstr.charAt(GLOBALCONSTANT.INT_FOUR))
					{

					}else if(startstr.charAt(GLOBALCONSTANT.INT_FOUR)>Endstr.charAt(GLOBALCONSTANT.INT_FOUR))
					{
						returnString = startstr;
					}else{

						returnString = Endstr;
					}

				}else if(startstr.charAt(GLOBALCONSTANT.INT_THREE)>Endstr.charAt(GLOBALCONSTANT.INT_THREE))
				{
					returnString = startstr;
				}else{

					returnString = Endstr;
				}

			}else if(startstr.charAt(GLOBALCONSTANT.INT_TWO)>Endstr.charAt(GLOBALCONSTANT.INT_TWO))
			{
				returnString = startstr;
			}else{

				returnString = Endstr;
			}

		}else if(startstr.charAt(GLOBALCONSTANT.IntOne)>Endstr.charAt(GLOBALCONSTANT.IntOne))
		{
			returnString = startstr;
		}else{

			returnString = Endstr;
		}

	}else if(startstr.charAt(GLOBALCONSTANT.INT_ZERO)>Endstr.charAt(GLOBALCONSTANT.INT_ZERO))
	{
		returnString = startstr;
	}else{

		returnString = Endstr;
	} 

	return returnString;
}


@RequestMapping(value = GLOBALCONSTANT.DownloadLocFile, method = RequestMethod.GET)
public @ResponseBody String getFileData(@RequestParam(value=GLOBALCONSTANT.DuplicateLocPath) String DuplicateLOCFile) throws JASCIEXCEPTION {	

    File DuplicateFile = new File(DuplicateLOCFile);
    StringBuilder sf = new StringBuilder();
    BufferedReader DuplicateBr;
	try {
		DuplicateBr = new BufferedReader(new FileReader(DuplicateFile));
	
    String LOC = GLOBALCONSTANT.BlankString;
   
		while((LOC=DuplicateBr.readLine()) != null)
		{
			sf.append(LOC+GLOBALCONSTANT.Comma);
			
		}
		DuplicateBr.close();
		DuplicateFile.delete();
    return sf.toString().substring(GLOBALCONSTANT.INT_ZERO, sf.length()-GLOBALCONSTANT.IntOne);
	} catch (FileNotFoundException e) {
		
		e.printStackTrace();
	} catch (IOException e) {
		
		e.printStackTrace();
	}
	return sf.toString().substring(GLOBALCONSTANT.INT_ZERO, sf.length()-GLOBALCONSTANT.IntOne);
}

}



