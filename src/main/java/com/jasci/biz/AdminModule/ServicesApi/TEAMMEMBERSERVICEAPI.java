/*

Date Developed :Nov 28 2014
Created by: Rahul kumar
Description :TEAMMEMBERS service TO HANDLE Restful service
 */

package com.jasci.biz.AdminModule.ServicesApi;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jasci.biz.AdminModule.dao.ITEAMMEMBERSDAO;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.ADDRESSVALIDATION;
import com.jasci.common.util.ZIPVALIDATION;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class TEAMMEMBERSERVICEAPI {


	@Autowired
	private ITEAMMEMBERSDAO ObjTeamMemberDao;



	/**
	 * Created on:Nov 28  2014
	 * Created by:Rahul Kumar
	 * Description: This service function get TeamMember based on TeamMemeber or PartOfTeamMember and Tenant and fulfillmentcenter
	 * Input parameter:String TeamMemberName,String PartOfTeamMember,String Tenant,Long FulfillmentCenter
	 * Return Type :List<TEAMMEMBERS>
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_GetList, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERS>  getList(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember_name) String TeamMemberName,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Part_of_TeamMember_name) String PartOfTeamMember,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant) throws JASCIEXCEPTION {	

		List<TEAMMEMBERS> ObjListTeamMembers=null;

		ObjListTeamMembers=ObjTeamMemberDao.getList(TeamMemberName,PartOfTeamMember,Tenant);		
		return ObjListTeamMembers;
	}

	/**
	 * Created on:Nov 28  2014
	 * Created by:Rahul Kumar
	 * Description: This service function get TeamMember based on TeamMemeber or PartOfTeamMember and Tenant and fulfillmentcenter
	 * Input parameter:String TeamMemberName,String PartOfTeamMember,String Tenant,Long FulfillmentCenter
	 * Return Type :List<TEAMMEMBERS> 
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_AddEntry, method = RequestMethod.GET)
	public @ResponseBody  Serializable addEntry(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_ObjTeamMembersBe) TEAMMEMBERSBE ObjectTeamMembersBe,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_CompaniesList) List<String> CompaniesList,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_LastActivityBy) String strLastActivityBy) throws JASCIEXCEPTION {	

		Serializable objSerializable=null;
		objSerializable=ObjTeamMemberDao.save(ObjectTeamMembersBe,CompaniesList,strLastActivityBy);
		return objSerializable;
	}




	/**
	 * Created on:Nov 28  2014
	 * Created by:Rahul Kumar
	 * Description: This service function delete existing team member
	 * Input parameter:String Tenant,String TeamMember,String FulfillmentCenter,String Company
	 * Return Type :TEAMMEMBERS 
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_DeleteEntry, method = RequestMethod.GET)
	public @ResponseBody  TEAMMEMBERS deleteEntry(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember) String TeamMember,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Company) String Company) throws JASCIEXCEPTION {	


		return ObjTeamMemberDao.delete(Tenant,TeamMember,Company);
	}



	/**
	 * Created on:Nov 28  2014
	 * Created by:Rahul Kumar
	 * Description: This service function update existing team member
	 * Input parameter:TEAMMEMBERSBE ObjectTeamMembersBe, List CompaniesList,String TeamMemberoldId,String Company
	 * Return Type :TEAMMEMBERS 
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_UpdateEntry, method = RequestMethod.GET)
	public @ResponseBody  TEAMMEMBERS updateEntry(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_ObjTeamMembersBe) TEAMMEMBERSBE ObjectTeamMembersBe,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_CompaniesList) List<String> CompaniesList,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMemberOldid) String TeamMemberoldId,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Company) String Company,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_LastActivityBy) String strLastActivityBy) throws JASCIEXCEPTION {	

		return ObjTeamMemberDao.update(ObjectTeamMembersBe, CompaniesList,TeamMemberoldId,Company,strLastActivityBy);
	}



	/**
	 * Created on:Nov 28  2014
	 * Created by:Rahul Kumar
	 * Description: This service function get companies list based on tenant
	 * Input parameter:String Tenant
	 * Return Type :List<COMPANIES>  
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_GetCompanies, method = RequestMethod.GET)
	public @ResponseBody   List<COMPANIES> getCompanies(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant) throws JASCIEXCEPTION {	

		return ObjTeamMemberDao.getCompanies(Tenant);
	}




	/**
	 * Created on:Nov 28  2014
	 * Created by:Rahul Kumar
	 * Description: This service function get companies list based on TeamMember
	 * Input parameter:String Tenant,String TeamMember
	 * Return Type :TEAMMEMBERS 
	 * 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_GetTeamMemberCompanies, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERCOMPANIES> getTeamMemberCompanies(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember) String TeamMember) throws JASCIEXCEPTION {	

		return ObjTeamMemberDao.getTeamMemberCompanies(Tenant, TeamMember);

	}


	/**
	 * Created on:Nov 28  2014
	 * Created by:Rahul Kumar
	 * Description: This function  get General code list based on tenant,company,application,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_GetGeneralCode, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODES> getGeneralCode(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Company) String Company,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_GeneralCodeId) String GeneralCodeId) throws JASCIEXCEPTION {	

		return ObjTeamMemberDao.getGeneralCode(Tenant, Company, GeneralCodeId);

	}


	/**
	 * Created on:Nov 28 2014
	 * Created by:Rahul Kumar
	 * Description: This function get TeamMember based on TeamMemeber or PartOfTeamMember
	 * Input parameter: String TeamMemberName,String PartOfTeamMember,String TeamMemberName,String PartOfTeamMember,String Tenant,Long FulfillmentCenter
	 * Return Type :List<TEAMMEMBERSBE>
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_GetTeamMemberList, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERSBE> getTeamMemberList(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_SortOrder) String SortOrder,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Part_of_TeamMember_name) String StrPartOfTeamMember,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant) throws JASCIEXCEPTION {	

		return ObjTeamMemberDao.getTeamMemberList(SortOrder,StrPartOfTeamMember,Tenant);

	}



	/**
	 * Created on:Dec 04 2014
	 * Created by:Rahul Kumar
	 * Description: This function check team member existing
	 * Input parameter: String TeamMemberName,String PartOfTeamMember,String TeamMemberName,String PartOfTeamMember,String Tenant,Long FulfillmentCenter
	 * Return Type :List<TEAMMEMBERSBE>
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_RestCheckTeammember, method = RequestMethod.POST)
	public @ResponseBody  List<Object> checkTemmember(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember) String TeamMember,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant) {	

		return ObjTeamMemberDao.checkTeamember(TeamMember,Tenant);

	}

	/**
	 * Created on:Dec 04 2014
	 * Created by:Rahul Kumar
	 * Description: This function check Email uniquenes
	 * Input parameter: String Email
	 * Return Type :List<TEAMMEMBERSBE>
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_RestCheckEmail, method = RequestMethod.POST)
	public @ResponseBody  Boolean checkEmail(@RequestParam(value=GLOBALCONSTANT.TeamMemberEmail) String Email) {	

		return ObjTeamMemberDao.isEmailUnique(Email);

	}


	/**
	 * Created on:Dec 04 2014
	 * Created by:Rahul Kumar
	 * Description: This function check Zip validation
	 * Input parameter: ZipCode
	 * Return Type :List<TEAMMEMBERSBE>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_RestCheckZipCode, method = RequestMethod.POST)
	public @ResponseBody  Boolean checkZip(@RequestParam(value=GLOBALCONSTANT.TeamMemberZipCode) String ZipCode) throws JASCIEXCEPTION {	


		ZIPVALIDATION objZipvalidation = new ZIPVALIDATION();

		if(GLOBALCONSTANT.BlankString.equalsIgnoreCase(ZipCode)){
			return false;
		}
		
		char result=objZipvalidation.IsValidZipcode(ZipCode);

		if(result==GLOBALCONSTANT.Y){		
			return false;
		}
		else{
			return true;
		}

	}



	/**
	 * Created on:Dec 04 2014
	 * Created by:Rahul Kumar
	 * Description: This function check Address
	 * Input parameter: ZipCode
	 * Return Type :List<TEAMMEMBERSBE>
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_RestCheckAddress, method = RequestMethod.POST)
	public @ResponseBody  Boolean checkAddress(@RequestParam(value=GLOBALCONSTANT.TeamMemberAddress1) String Address1,
			@RequestParam(value=GLOBALCONSTANT.TeamMemberAddress2) String Address2,
			@RequestParam(value=GLOBALCONSTANT.TeamMemberAddress3) String Address3,
			@RequestParam(value=GLOBALCONSTANT.TeamMemberAddress4) String Address4,
			@RequestParam(value=GLOBALCONSTANT.TeamMemberCity) String City,
			@RequestParam(value=GLOBALCONSTANT.TeamMemberStateCode) String StateCode,
			@RequestParam(value=GLOBALCONSTANT.TeamMemberCountryCode) String CountryCode,
			@RequestParam(value=GLOBALCONSTANT.TeamMemberZipCode) String ZipCode) {	

		char result=GLOBALCONSTANT.N;
		try{
			ADDRESSVALIDATION objAddressvalidation = new ADDRESSVALIDATION();
			result=objAddressvalidation.IsValidAddress(Address1,Address2,Address3,Address4,City,StateCode,CountryCode,ZipCode);
		}catch(Exception e){

		}

		if(result==GLOBALCONSTANT.Y){		
			return false;
		}
		else{
			return true;
		}

	}

	
	
	/**
	 * Created on:Dec 12  2014
	 * Created by:Rahul Kumar
	 * Description: This service function get TeamMember based on TeamMemeber or PartOfTeamMember and Tenant and fulfillmentcenter
	 * Input parameter:String TeamMemberName,String PartOfTeamMember,String Tenant,Long FulfillmentCenter
	 * Return Type :List<TEAMMEMBERS>
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemebers_Restfull_checkTeamMember, method = RequestMethod.POST)
	public @ResponseBody  Boolean  checkTeamMember(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember_name) String TeamMemberName,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Part_of_TeamMember_name) String PartOfTeamMember,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant) throws JASCIEXCEPTION {	

		List<TEAMMEMBERS> ObjListTeamMembers=null;
		ObjListTeamMembers=ObjTeamMemberDao.getList(TeamMemberName,PartOfTeamMember,Tenant);	
		
		if(ObjListTeamMembers.size()>GLOBALCONSTANT.INT_ZERO){
			return false;
		}
		else{
		return true;
		}
	}

}
