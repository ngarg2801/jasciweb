/**

Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :LANGUAGE TRANSLATIONServiceApi a RestFullservice
 */


package com.jasci.biz.AdminModule.ServicesApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONBE;
import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONSCREENLABELSBE;
import com.jasci.biz.AdminModule.dao.ILANGUAGETRANSLATIONDAL;
import com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGEPK;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class LANGUAGETRANSLATIONSERVICEAPI 
{

	@Autowired
	private ILANGUAGETRANSLATIONDAL ObjLanguageTranslationDal;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	@Autowired
	LANGUANGELABELSAPI ObjLanguageLabelsApi;
	@Autowired
	IMENUAPPICONDAL ObjMenuAppIconDal;
	 
	
	

	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 17, 2014
	 * @param Tenant
	 * @param Company
	 * @param GeneralCodeId
	 * @throws JASCIEXCEPTION
	 * @Return List<LANGUAGETRANSLATIONBE>
	 *
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LanguageTranslation_Restfull_GetGeneralCodeLanguageList, method = RequestMethod.GET)
	public @ResponseBody  List<LANGUAGETRANSLATIONBE> getLanguageList(@RequestParam(value=GLOBALCONSTANT.LanguageTranslation_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.LanguageTranslation_Restfull_Company) String Company,@RequestParam(value=GLOBALCONSTANT.LanguageTranslation_Restfull_GeneralCodeId) String GeneralCodeId) throws JASCIEXCEPTION{	

		
		List<GENERALCODES> objList= ObjLanguageTranslationDal.getLanguageList(Tenant, Company,GeneralCodeId);
		List<LANGUAGETRANSLATIONBE> ObjLanguageBeList=new ArrayList<LANGUAGETRANSLATIONBE>();
		
		if(objList!=null && !objList.isEmpty())
		{
		
			for(GENERALCODES ObjGeneralCode:objList)
		{
			LANGUAGETRANSLATIONBE ObjLanguageTranslationBe=new LANGUAGETRANSLATIONBE();
			ObjLanguageTranslationBe.setDescription20(StringEscapeUtils.escapeHtml(ObjGeneralCode.getDescription20()));
			ObjLanguageTranslationBe.setLanguage(StringEscapeUtils.escapeHtml(ObjGeneralCode.getId().getGeneralCode()));
			ObjLanguageBeList.add(ObjLanguageTranslationBe);
			
		}
		
		}
		return ObjLanguageBeList;
	
	}
	
	
	
	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 17, 2014
	 * @param Language
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LanguageTranslation_Restfull_CheckValidLanguage, method = RequestMethod.POST) 
	public @ResponseBody  Boolean CheckLanguage(@RequestParam(value=GLOBALCONSTANT.LanguageTranslation_Restfull_Language) String Language)
	{	
        List<LANGUAGES> ObjLanguageList=null;
       try{
    	   ObjLanguageList=ObjLanguageTranslationDal.CheckLanguage(Language);
       }catch(JASCIEXCEPTION objJasciException)
       {}
		if(ObjLanguageList!=null && !ObjLanguageList.isEmpty())
		{
			return true;
		}
		else
		{
		    return false;
		}
			
		
	
	}
	
	
	
	
	
	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 17, 2014
	 * @param Language
	 * @param keyPhrase
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LanguageTranslation_Restfull_CheckAlreadyExist, method = RequestMethod.POST)
	public @ResponseBody  Boolean CheckAlreadyExist(@RequestParam(value=GLOBALCONSTANT.LanguageTranslation_Restfull_Language) String Language,@RequestParam(value=GLOBALCONSTANT.LanguageTranslation_Restfull_KeyPhrase) String KeyPhrase) 
	{	
        List<LANGUAGES> ObjLanguageList=null;
        try{
        	ObjLanguageList=ObjLanguageTranslationDal.CheckAlreadyExist(Language,KeyPhrase);
        }catch(JASCIEXCEPTION objException){}
		if(ObjLanguageList!=null && !ObjLanguageList.isEmpty())
		{
			return true;
		}
		else
		{
		    return false;
		}
			
		
	
	}
	
	
	
	
	
	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 18, 2014
	 * @param KeyPhrase
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LanguageTranslation_Restfull_CheckValidKeyPhrase, method = RequestMethod.POST)
	public @ResponseBody  Boolean CheckKeyPhrase(@RequestParam(value=GLOBALCONSTANT.LanguageTranslation_Restfull_KeyPhrase) String KeyPhrase) 
	{	
        List<LANGUAGES> ObjLanguageList=null;
        try{
        	ObjLanguageList=ObjLanguageTranslationDal.CheckKeyPhrase(KeyPhrase);
        }catch(JASCIEXCEPTION ObjJasciException){}
		if(ObjLanguageList!=null && !ObjLanguageList.isEmpty())
		{
			return true;
		}
		else
		{
		    return false;
		}
			
		
	
	}

	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 18, 2014
	 * @param LANGUAGETRANSLATIONBE
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */
    @Transactional
	public Boolean addLanguageData(LANGUAGETRANSLATIONBE objLanguageBe) throws JASCIEXCEPTION 
	{
    	
	  LANGUAGES ObjLanguage=new LANGUAGES();
	  LANGUAGEPK ObjLanguagePk=new LANGUAGEPK();
	  Boolean Status =false;
	 
		Date LastActivitydate=null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

			 LastActivitydate = formatter.parse(objLanguageBe.getLastActivityDate());
			
		}catch(Exception objException){}
		ObjLanguagePk.setKeyPhrase(objLanguageBe.getKeyPhrase());
		ObjLanguagePk.setLanguage(objLanguageBe.getLanguage());
	 ObjLanguage.setLastActivityDate(LastActivitydate);
	 ObjLanguage.setLastActivityTeamMember(OBJCOMMONSESSIONBE.getTeam_Member());
	 ObjLanguage.setTranslation(objLanguageBe.getTranslation());
	 ObjLanguage.setId(ObjLanguagePk);
	 try{
		 Status= ObjLanguageTranslationDal.addLanguageData(ObjLanguage);
	 }catch(JASCIEXCEPTION objJasciException){}
		return Status;
	}



	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 18, 2014
	 * @param String
	 * @throws JASCIEXCEPTION
	 * @Return List<LANGUAGETRANSLATIONBE>
	 *
	 */
	public List<LANGUAGETRANSLATIONBE> GetLanguageData(String strSelectedLanguage, String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION
	{
		List<LANGUAGES> ObjLanguageList=ObjLanguageTranslationDal.CheckLanguage(strSelectedLanguage);
		List<GENERALCODES> objList=null;
		try{
			 objList= ObjLanguageTranslationDal.getLanguageList(Tenant, Company,GeneralCodeId);
		}catch(JASCIEXCEPTION objJasciException){}
		List<LANGUAGETRANSLATIONBE> ObjLanguageBeList=new ArrayList<LANGUAGETRANSLATIONBE>();
		
		
		
		    DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
		    
		for(LANGUAGES ObjLanguage:ObjLanguageList)
		{
			
			LANGUAGETRANSLATIONBE ObjLanguageTranslationBe=new LANGUAGETRANSLATIONBE();
			ObjLanguageTranslationBe.setKeyPhrase(ObjLanguage.getId().getKeyPhrase());
			ObjLanguageTranslationBe.setLanguage(ObjLanguage.getId().getLanguage());
			String TeamMemberName=GLOBALCONSTANT.BlankString;
			List<TEAMMEMBERS> objTeammembers=null;
			try{
				objTeammembers=ObjMenuAppIconDal.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(),ObjLanguage.getLastActivityTeamMember());
			}catch(Exception e){}
			try{
				TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
			}catch(Exception e){}

			ObjLanguageTranslationBe.setLastActivityBy(TeamMemberName);
			
			 Date ObjDate =ObjLanguage.getLastActivityDate();
			 String StrLastActivityDate=GLOBALCONSTANT.BlankString;
			 try{
				 StrLastActivityDate=ObjDateFormat1.format(ObjDate);
			 }catch(Exception e)
			 {}
			 ObjLanguageTranslationBe.setLastActivityDate(StrLastActivityDate);
			ObjLanguageTranslationBe.setTranslation(ObjLanguage.getTranslation());
			for(GENERALCODES ObjGeneralCode:objList)
		       {
				if(ObjLanguage.getId().getLanguage().equalsIgnoreCase(ObjGeneralCode.getId().getGeneralCode()))
				{
		     	  ObjLanguageTranslationBe.setDescription20(ObjGeneralCode.getDescription20());
		        }
		       }
			ObjLanguageBeList.add(ObjLanguageTranslationBe);
		}
		
		return ObjLanguageBeList;
	}


	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 18, 2014
	 * @param String
	 * @throws JASCIEXCEPTION
	 * @Return List<LANGUAGETRANSLATIONBE>
	 *
	 */

	public List<LANGUAGETRANSLATIONBE> GetKeyPhrase(String strKeyPhrase, String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION
	{
         List<LANGUAGES> ObjLanguageList=ObjLanguageTranslationDal.CheckKeyPhrase(strKeyPhrase);
		
		List<GENERALCODES> objList= ObjLanguageTranslationDal.getLanguageList(Tenant, Company,GeneralCodeId);
		List<LANGUAGETRANSLATIONBE> ObjLanguageBeList=new ArrayList<LANGUAGETRANSLATIONBE>();
		
	    DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);

		for(LANGUAGES ObjLanguage:ObjLanguageList)
		{
		
			LANGUAGETRANSLATIONBE ObjLanguageTranslationBe=new LANGUAGETRANSLATIONBE();
			ObjLanguageTranslationBe.setKeyPhrase(ObjLanguage.getId().getKeyPhrase());
			ObjLanguageTranslationBe.setLanguage(ObjLanguage.getId().getLanguage());
			String TeamMemberName=GLOBALCONSTANT.BlankString;
			List<TEAMMEMBERS> objTeammembers=null;
			try{
				objTeammembers=ObjMenuAppIconDal.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(),ObjLanguage.getLastActivityTeamMember());
			}catch(Exception e){}
			try{
				TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception e){}

			ObjLanguageTranslationBe.setLastActivityBy(TeamMemberName);
						Date ObjDate =ObjLanguage.getLastActivityDate();
			 String StrLastActivityDate=GLOBALCONSTANT.BlankString;
			 try{
				 StrLastActivityDate=ObjDateFormat1.format(ObjDate);
			 }catch(Exception e)
			 {}
			 ObjLanguageTranslationBe.setLastActivityDate(StrLastActivityDate);
			ObjLanguageTranslationBe.setTranslation(ObjLanguage.getTranslation());
			for(GENERALCODES ObjGeneralCode:objList)
		       {
				if(ObjLanguage.getId().getLanguage().equalsIgnoreCase(ObjGeneralCode.getId().getGeneralCode()))
				{
		     	  ObjLanguageTranslationBe.setDescription20(ObjGeneralCode.getDescription20());
		        }
		       }
			ObjLanguageBeList.add(ObjLanguageTranslationBe);
		}
		
		return ObjLanguageBeList;
		
	}



	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 19, 2014
	 * @param Language
	 * @param KeyPhrase
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */

	public Boolean DeleteLanguage(String language, String keyPhrase)  throws JASCIEXCEPTION
	{
	Boolean Status=false;
	LANGUAGES ObjLanguages=new LANGUAGES();
	LANGUAGEPK ObjLanguagesPk=new LANGUAGEPK();
	ObjLanguagesPk.setKeyPhrase(keyPhrase);
	ObjLanguagesPk.setLanguage(language);
	ObjLanguages.setId(ObjLanguagesPk);
    /** Call DeleteLanguage() to delete record from language. */
	Status=ObjLanguageTranslationDal.DeleteLanguage(ObjLanguages);
		return Status;
	}

	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 19, 2014
	 * @param Language
	 * @param KeyPhrase
	 * @throws JASCIEXCEPTION
	 * @Return List<LANGUAGES>
	 *
	 */

	public List<LANGUAGES> GetLanguage(String strLanguage, String strKeyPhrase) 
	{
		 List<LANGUAGES> ObjList=null;
		    /** Call GetLanguage() to get translation according to language. */
		 try{
			 ObjList =ObjLanguageTranslationDal.getLanguage(strLanguage,strKeyPhrase);
		 }catch(Exception e)
		 {
			
		 }
		return ObjList;
	
		
	}
	
	
	
	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 17, 2014
	 * @param Language
	 * @throws JASCIEXCEPTION
	 * @Return LANGUAGETRANSLATIONSCREENLABELSBE
	 *
	 */
	
	
	
	 @SuppressWarnings(GLOBALCONSTANT.Strstatic_access)
	@Transactional
	public LANGUAGETRANSLATIONSCREENLABELSBE GetScreenLabels(String StrLanguage) throws JASCIEXCEPTION
	{
		    /** Call GetLanguage() to get translation according to language. */
		 
		  // 	List<LANGUAGES> ObjListLanguages=ObjLanguageTranslationDal.GetLanguage(StrLanguage);
		 List<LANGUAGES> ObjListLanguages=ObjLanguageLabelsApi.GetScreenLabels(ObjLanguageLabelsApi.strLanguageTranslationModule,StrLanguage);
			LANGUAGETRANSLATIONSCREENLABELSBE ObjLanguageScreenBe=SetTeamMemberMessagesScreenBe(ObjListLanguages);
			return ObjLanguageScreenBe;
	}

		    
		    
		   	    
		/**
		 * 
		 * @author Diksha Gupta
		 * @Date Dec 18, 2014
		 * @param List<LANGUAGES>
		 * @throws JASCIEXCEPTION
		 * @Return LANGUAGETRANSLATIONSCREENLABELSBE
		 *
		 */ 
	 
		    public LANGUAGETRANSLATIONSCREENLABELSBE SetTeamMemberMessagesScreenBe(List<LANGUAGES> ObjListLanguages)
			{
				
		    	LANGUAGETRANSLATIONSCREENLABELSBE ObjLanguageScreenBe=new LANGUAGETRANSLATIONSCREENLABELSBE();
				/** to set LanguageTranslationScreen data from languages data from table LANGUAGES */
				for (LANGUAGES ObjLanguages : ObjListLanguages) 
				{
					String StrKeyPhrase=ObjLanguages.getId().getKeyPhrase().trim();
					 if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Action(ObjLanguages.getTranslation()); 
					 }
				     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
				     {
				    	 ObjLanguageScreenBe.setLanguageTranslation_AddNew(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_KeyPhrase))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_BlankKeyphraseMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Language))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_BlankLanguageMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Cancel(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_DeleteMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_DisplayAll(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_English_Key_Phrase))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_EnglishKeyPhrase(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Key_Phrase))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_InValidKeyPhraseMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Language))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_InValidLanguageMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Key_Phrase))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_KeyPhrase(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Already_Used))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_KeyPhraseAlreadyExist(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Language))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Language(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_LastActivityBy(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_LastActivityDate(ObjLanguages.getTranslation()); 
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Language_Translation_Lookup))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_LTLookup(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Language_Translation_Maintenance))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_LTMaintenance(ObjLanguages.getTranslation());
					 }
					/* else if(StrKeyPhrase.equalsIgnoreCase(GLOBALCONSTANT.LanguageTranslation_LTMkeyPhrase))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_LTMkeyPhrase(ObjLanguages.getTranslation());
					 }*/
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Language_Translation_Search_Lookup))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_LTSearchLookup(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_MandatoryFieldMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_New(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_SaveMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_SaveUpdate(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Translation))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Translation(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_UpdateMessage(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Edit(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Delete(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Select(ObjLanguages.getTranslation());
					 }
					 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
					 {
						 ObjLanguageScreenBe.setLanguageTranslation_Reset(ObjLanguages.getTranslation());
					 }
							
					 
				}
		return ObjLanguageScreenBe;
		
	}


		    /**
			 * 
			 * @author Diksha Gupta
			 * @Date Dec 18, 2014
			 * @param LANGUAGETRANSLATIONBE
			 * @throws JASCIEXCEPTION
			 * @Return Boolean
			 *
			 */ 
             @Transactional
			public Boolean editLanguageData(LANGUAGETRANSLATIONBE objLanguageBe)  throws JASCIEXCEPTION
			{
				    LANGUAGES ObjLanguage=new LANGUAGES();
				    LANGUAGEPK ObjLanguagePk=new LANGUAGEPK();
				 
					Date LastActivitydate=null;
					try {
						SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

						 LastActivitydate = formatter.parse(objLanguageBe.getLastActivityDate());
						
					}catch(Exception objException){}
					ObjLanguagePk.setKeyPhrase(objLanguageBe.getKeyPhrase());
					ObjLanguagePk.setLanguage(objLanguageBe.getLanguage());
				    ObjLanguage.setLastActivityDate(LastActivitydate);
				    ObjLanguage.setLastActivityTeamMember(objLanguageBe.getLastActivityBy());
				    ObjLanguage.setTranslation(objLanguageBe.getTranslation());
				    ObjLanguage.setId(ObjLanguagePk);
				    /** Call editLanguagedata() to update record in language. */
				 	Boolean Status = ObjLanguageTranslationDal.editLanguageData(ObjLanguage);
					return Status;
				}
             

 		    /**
 			 * 
 			 * @author Diksha Gupta
 			 * @Date Dec 18, 2014
 			 * @param LANGUAGETRANSLATIONBE
 			 * @throws JASCIEXCEPTION
 			 * @Return Boolean
 			 *
 			 */ 
              @Transactional
 			public String getTeamMemberName(String Teammember)
 			{           
             String TeamMemberName=GLOBALCONSTANT.BlankString;
 			List<TEAMMEMBERS> objTeammembers=null;
 			try{
 				objTeammembers=ObjMenuAppIconDal.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(),Teammember);
 			}catch(Exception e){}
 			try{
 				TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
 			}catch(Exception e){}
 			
             return TeamMemberName;
 			
 			}
 			
}
	
	
	

