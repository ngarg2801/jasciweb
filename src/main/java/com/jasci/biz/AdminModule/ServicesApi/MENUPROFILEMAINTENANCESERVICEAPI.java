/**

Date Developed: Dec 25 2014
Description: make a RestfullServices for communication between dao to services
Created By :Sarvendra tyagi
Created Date: Dec 25 2014

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.dao.IMENUPROFILEMAINTENANCEDAO;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEHEADER;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class MENUPROFILEMAINTENANCESERVICEAPI {



	@Autowired
	IMENUPROFILEMAINTENANCEDAO objMenuProfileDao;

	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSION;

	/**
	 * Created on:Dec 26  2014
	 * Created by:Sarvendra Tyagi
	 * Description: This service function check MenuProfile based on SearchFieldvalue, FieldSearch 
	 * Input parameter:String SearchFieldvalue, String FieldSearch
	 * Return Type :Boolean
	 * 
	 */


	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUPROFILE_RestFull_searchlookup, method = RequestMethod.POST)
	public @ResponseBody  Boolean  getList(@RequestParam(value=GLOBALCONSTANT.MenuProfile_Restfull_SearchFieldValue) String SearchFieldValue,@RequestParam(value=GLOBALCONSTANT.MenuOptions_Restfull_SearchField) String SearchField) throws JASCIEXCEPTION {	




		Boolean flagForSearch=false;
		List<MENUPROFILEHEADER> ObjListMenuProfile=null;

		String strPartOfProfileDesc=GLOBALCONSTANT.BlankString;
		String strMenuType=GLOBALCONSTANT.BlankString;


		try{
			if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUProfile_lblPartMenuProfileDescription)){

				strPartOfProfileDesc=SearchFieldValue;

				ObjListMenuProfile=objMenuProfileDao.getMenuProfileListByDescription(strPartOfProfileDesc);	
				if(ObjListMenuProfile.size()>GLOBALCONSTANT.INT_ZERO){
					flagForSearch=true;
				}
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblMenuType)){

				strMenuType=SearchFieldValue;

				ObjListMenuProfile=objMenuProfileDao.getMenuProfileListByMenuType(strMenuType);

				if(ObjListMenuProfile.size()>GLOBALCONSTANT.INT_ZERO){
					flagForSearch=true;
				}



			}




		}catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}	
		return flagForSearch;
	}





	/**
	 * Created on:Dec 27  2014
	 * Created by:Sarvendra Tyagi
	 * Description: This service function check MenuProfile is invalid or not  
	 * Input parameter:Request parameter
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_RestFull_Check_MenuProfile, method = RequestMethod.POST)
	public @ResponseBody  Boolean  checkMenuOptionInMenuOption(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Parameter_MenuOptionValue) String strMenuProfile) throws JASCIEXCEPTION {	

		List<MENUPROFILEHEADER> objlistMenuProfile=null;
		Boolean flagForMenuOption=false;


		try{

			objlistMenuProfile=objMenuProfileDao.getMenuProfileList(strMenuProfile);

			if(objlistMenuProfile.size()>GLOBALCONSTANT.INT_ZERO){
				flagForMenuOption=true;
			}
		}catch(Exception objException){


			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return flagForMenuOption;
	}




	/**
	 * Created on:Dec 27  2014
	 * Created by:Sarvendra Tyagi
	 * Description: This service function check MenuProfile is invalid or not  
	 * Input parameter:Request parameter
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_RestFull_MenuOptionList, method = RequestMethod.POST)
	public @ResponseBody  List<MENUOPTIONS>  getMenuOptionList(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_Parameter_MenuOptionApplication) String strMenuOptionApplication
			,@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_Parameter_MenuOptionDisplayAll) String strDisplayAll
			,@RequestParam(value=GLOBALCONSTANT.StrMenuType) String strMenuType) throws JASCIEXCEPTION {	

		List<MENUOPTIONS> objlistMenuOption=null;



		try{
			if(strMenuOptionApplication.equalsIgnoreCase(strDisplayAll)){

				objlistMenuOption=objMenuProfileDao.getMenuAllOptionList(strMenuType);

			}else{

				objlistMenuOption=objMenuProfileDao.getMenuOptionList(strMenuOptionApplication,strMenuType);
			}

		}catch(Exception objException){


			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objlistMenuOption;
	}



	/** 
	 * @description: This function check menu option assigned in menu profile option table or not
	 * @param: strMenuOption
	 * @return: boolean
	 * @throws: DATABASEEXCEPTION
	 * @Developer: Sarvendra Tyagi
	 * @Date	 : Dec 17 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_RestFull_Check_MenuOption_Profile, method = RequestMethod.POST)
	public @ResponseBody  Boolean  checkMenuOptionInMenuProfile(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Parameter_MenuOptionValue) String strMenuProfile,
			@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Parameter_MenuProfileTenant) String strTenant) throws JASCIEXCEPTION {	



		List<Object> objlistMenuProfile=null;
		Boolean flagForMenuOption=false;


		try{


			objlistMenuProfile=objMenuProfileDao.CheckDeleteMenuProfile(strMenuProfile,strTenant);

			if(objlistMenuProfile.size()>GLOBALCONSTANT.INT_ZERO){
				flagForMenuOption=true;
			}


		}catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());



		}	
		return flagForMenuOption;
	}




	/**
	 * Created on:JAN   2014
	 * Created by:Sarvendra Tyagi
	 * Description: This service function check MenuProfile is invalid or not  
	 * Input parameter:Request parameter
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_RestLoadEditGrid, method = RequestMethod.POST)
	public @ResponseBody  List<MENUOPTIONS>  getMenuOptionList(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_Parameter_MenuOptionApplication) String strMenuProfile) throws JASCIEXCEPTION {	

		List<MENUOPTIONS> objlistMenuProfile=null;

		try{

			objlistMenuProfile=objMenuProfileDao.getAssignedMenurofile(OBJCOMMONSESSION.getTenant(), strMenuProfile);


		}catch(Exception objException){


		}

		return objlistMenuProfile;
	}
	
	
	/**
	 * Created on:JAN 10  2014
	 * Created by:Sarvendra Tyagi
	 * Description: This service function check MenuProfile is invalid or not  
	 * Input parameter:Request parameter
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_MenuOptionList_Edit, method = RequestMethod.POST)
	public @ResponseBody  List<MENUOPTIONS>  getMenuOptionList_Edit(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_Parameter_MenuOptionApplication) String strMenuProfile,
			@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_Parameter_MenuOptionDisplayAll) String strDisplayAll,
			@RequestParam(value=GLOBALCONSTANT.MENUOPTION_MenuType) String strMenuType) throws JASCIEXCEPTION {	

		List<MENUOPTIONS> objlistMenuProfile=null;

		try{

			if(strDisplayAll.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All)){
			objlistMenuProfile=objMenuProfileDao.getAssignedMenuProfileDispalyAll(OBJCOMMONSESSION.getTenant(), strMenuProfile,strMenuType);
			}else{
				
				objlistMenuProfile=objMenuProfileDao.getAssignedMenuProfileDispalyApplication(OBJCOMMONSESSION.getTenant(), strMenuProfile,strMenuType,strDisplayAll);
			}
			

		}catch(Exception objException){


		}

		return objlistMenuProfile;
	}
	
	
	/**
	 * Created on:JAN 10  2014
	 * Created by:Sarvendra Tyagi
	 * Description: This service function check MenuProfile is invalid or not  
	 * Input parameter:Request parameter
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_MenuAppIconUrl, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  getMenuAppiconUrl(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_Parameter_MenuOptionApplication) String strappIcon) throws JASCIEXCEPTION {	

		List<Object> objlistMenuProfile=null;

		try{

			objlistMenuProfile=objMenuProfileDao.getAppIConURl(strappIcon);


		}catch(Exception objException){


		}

		return objlistMenuProfile;
	}
	
	
}
