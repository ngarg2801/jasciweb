/*

Created By Aakash Bishnoi
Created Date Oct 29 2014
Description   InfoHelps Restfullservice class
 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.jasci.biz.AdminModule.dao.IINFOHELPSDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.INFOHELPS;
import com.jasci.biz.AdminModule.model.INFOHELPSBEAN;
import com.jasci.biz.AdminModule.model.INFOHELPSPK;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;



@Controller
public class INFOHELPSSERVICEAPI {

	@Autowired		
	private IINFOHELPSDAO ObjectInfohelpDao;
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
	//RestFull Service GetInfoHelpAll List for fetch all data 
	@SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strrawtypes })
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.InfoHelp_Restfull_RestInfohelpListAll, method = RequestMethod.GET)
 @ResponseBody
 public  List<INFOHELPSBEAN>  getInfoHelpList(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp) String StrInfoHelp,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrPartoftheDescription) String StrPartoftheDescription,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelpType) String StrInfoHelpType) throws JASCIEXCEPTION {	

		List ListInfohelpAll=ObjectInfohelpDao.getInfoHelpList(StrInfoHelp,StrPartoftheDescription,StrInfoHelpType);
			List<INFOHELPSBEAN> ReturnInfoHelpsList=new ArrayList<INFOHELPSBEAN>();
			List<INFOHELPS> getInfoHelpList=(List<INFOHELPS>) ListInfohelpAll.get(GLOBALCONSTANT.INT_ZERO);
			List<GENERALCODES> getGeneralCodes=(List<GENERALCODES>) ListInfohelpAll.get(GLOBALCONSTANT.IntOne);
	
	for(int InfoHelpListLength=GLOBALCONSTANT.INT_ZERO;InfoHelpListLength<getInfoHelpList.size();InfoHelpListLength++ ){
		
		INFOHELPSBEAN OnjectInfoHelps=new INFOHELPSBEAN();
		OnjectInfoHelps.setDescription20(getInfoHelpList.get(InfoHelpListLength).getDescription20());
		OnjectInfoHelps.setDescription50(getInfoHelpList.get(InfoHelpListLength).getDescription50());
		OnjectInfoHelps.setExecution(getInfoHelpList.get(InfoHelpListLength).getExecution());
		OnjectInfoHelps.setInfoHelp(getInfoHelpList.get(InfoHelpListLength).getId().getInfoHelp());
		String LastActivityDateValue=ConvertDate(getInfoHelpList.get(InfoHelpListLength).getLastActivityDate().toString(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format);
		OnjectInfoHelps.setLastActivityDate(LastActivityDateValue);
		OnjectInfoHelps.setLastActivityTeamMember(getInfoHelpList.get(InfoHelpListLength).getLastActivityTeamMember());
		OnjectInfoHelps.setLanguageCode(getInfoHelpList.get(InfoHelpListLength).getId().getLanguage());
		OnjectInfoHelps.setInfoHelpType(getInfoHelpList.get(InfoHelpListLength).getInfoHelpType());
			for(int GeneralCodesLength=GLOBALCONSTANT.INT_ZERO;GeneralCodesLength<getGeneralCodes.size();GeneralCodesLength++){
				if(getGeneralCodes.get(GeneralCodesLength).getId().getGeneralCode().equalsIgnoreCase(getInfoHelpList.get(InfoHelpListLength).getInfoHelpType())){
					OnjectInfoHelps.setInfoHelpType(getGeneralCodes.get(GeneralCodesLength).getDescription20());
				}
				if(getGeneralCodes.get(GeneralCodesLength).getId().getGeneralCode().equalsIgnoreCase(getInfoHelpList.get(InfoHelpListLength).getId().getLanguage())){
					OnjectInfoHelps.setLanguage(getGeneralCodes.get(GeneralCodesLength).getDescription20());
				}
			}
			ReturnInfoHelpsList.add(OnjectInfoHelps);
	}		
		return ReturnInfoHelpsList;
	}

	
	
	
	//RestFull Service GetInfoHelpAll List for fetch all data 
	@SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strrawtypes })
		@Transactional
		@RequestMapping(value = GLOBALCONSTANT.InfoHelp_Restfull_RestInfohelpListAllCheck, method = RequestMethod.POST)
		public @ResponseBody  List<INFOHELPS>  getInfoHelpListCheck(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp) String InfoHelp,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrPartoftheDescription) String Description,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelpType) String InfoHelpType){	

			
			List<INFOHELPS> getInfoHelpList=null;
				try {
					List ListInfohelpAll = ObjectInfohelpDao.getInfoHelpList(InfoHelp,Description,InfoHelpType);
					 getInfoHelpList=(List<INFOHELPS>) ListInfohelpAll.get(GLOBALCONSTANT.INT_ZERO);
				} catch (JASCIEXCEPTION e) {
					
				}
			
		    	return getInfoHelpList;
		      }

		//It is used to fetch the data for edit
		@Transactional
		@RequestMapping(value = GLOBALCONSTANT.InfoHelp_Restfull_InfoHelpsListFetch, method = RequestMethod.GET)
		public @ResponseBody  INFOHELPSBEAN fetchInfoHelp(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp) String StrInfoHelp,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrLanguage) String StrLanguage) throws JASCIEXCEPTION{	

			INFOHELPS ObjInfoHelps=ObjectInfohelpDao.fetchInfoHelp(StrInfoHelp,StrLanguage);
			
			
			INFOHELPSBEAN ObjInfoHelpsIterate=new INFOHELPSBEAN();
		
			ObjInfoHelpsIterate.setInfoHelp(ObjInfoHelps.getId().getInfoHelp());
			ObjInfoHelpsIterate.setLanguage(ObjInfoHelps.getId().getLanguage());
		
			DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelps_yyyyMMdd_Format);
			String testDateString = ObjDateFormat.format(ObjInfoHelps.getLastActivityDate());
			
				ObjInfoHelpsIterate.setLastActivityDate(testDateString);
			
			
			//ObjInfoHelpsIterate.setLastActivityDate(LastActivityDateShow);
			ObjInfoHelpsIterate.setDescription20(ObjInfoHelps.getDescription20());
			ObjInfoHelpsIterate.setDescription50(ObjInfoHelps.getDescription50());
			ObjInfoHelpsIterate.setExecution(ObjInfoHelps.getExecution());			
			ObjInfoHelpsIterate.setInfoHelpType(ObjInfoHelps.getInfoHelpType());
			String StrTeamMember=getTeamMemberName(objCommonsessionbe.getTenant(), ObjInfoHelps.getLastActivityTeamMember()).toString();
			ObjInfoHelpsIterate.setLastActivityTeamMember(StrTeamMember);
			return ObjInfoHelpsIterate;
		}
		
		//it is used to update record of infohelps
		
		@Transactional
		@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_UpdateInfoHelps, method = RequestMethod.POST)
		public @ResponseBody  Boolean  updateInfoHelps(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrLastActivityBy) String LastActivityBy,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelpType) String InfoHelpType,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp) String InfoHelp,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrLanguage) String Language,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrDescription20) String Description20,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrDescription50) String Description50,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrExecutionPath) String ExecutionPath,@RequestParam(value=GLOBALCONSTANT.LastActivityDate) String LastActivityDate) throws JASCIEXCEPTION{	

			INFOHELPS objInfohelps=new INFOHELPS();	 			
			INFOHELPSPK ObjInfoHelpsIteratePK=new INFOHELPSPK();
			objInfohelps.setLastActivityTeamMember(trimValues(objCommonsessionbe.getTeam_Member()));
			objInfohelps.setInfoHelpType(trimValues(InfoHelpType));
			
			ObjInfoHelpsIteratePK.setInfoHelp(trimValues(InfoHelp));
			ObjInfoHelpsIteratePK.setLanguage(trimValues(Language));
			objInfohelps.setId(ObjInfoHelpsIteratePK);
			
		
			objInfohelps.setDescription20(trimValues(Description20));
			objInfohelps.setDescription50(trimValues(Description50));
			objInfohelps.setExecution(trimValues(ExecutionPath));
			//Date CurrentDate=new Date();
			DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
			//String testDateString = ObjDateFormat.format(CurrentDate);	
			
			
			
			
			try {
				objInfohelps.setLastActivityDate(ObjDateFormat.parse(LastActivityDate));
			} catch (ParseException e) {
				
			}
			
			ObjectInfohelpDao.updateInfoHelps(objInfohelps);					
		
			
			return false;

		}
		
		//RestFull Service check infohelp
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.InfoHelp_Restfull_CheckInfoHelp, method = RequestMethod.POST)
				public @ResponseBody  Boolean  checkInfoHelp(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp) String InfoHelp,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrLanguage) String Language){	

					INFOHELPS objInfohelps= ObjectInfohelpDao.checkInfoHelp(InfoHelp,Language);
					
					if(objInfohelps!=null){
						return true;
					}
					else{
					
					return false;
					}

				}

				
				
				
				
				//RestFull Service insert into new help
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.InfoHelp_Restfull_InsertInfoHelp, method = RequestMethod.POST)
				public @ResponseBody  Boolean  insertInfoHelp(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrLastActivityBy) String LastActivityBy,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelpType) String InfoHelpType,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp) String InfoHelp,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrLanguage) String Language,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrDescription20) String Description20,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrDescription50) String Description50,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrExecutionPath) String ExecutionPath,@RequestParam(value=GLOBALCONSTANT.LastActivityDate) String LastActivityDate) throws JASCIEXCEPTION{	

					
					INFOHELPS objInfohelps=new INFOHELPS();				
					INFOHELPSPK ObjInfoHelpsIteratePK=new INFOHELPSPK();
					objInfohelps.setLastActivityTeamMember(trimValues(objCommonsessionbe.getTeam_Member()));
					objInfohelps.setInfoHelpType(trimValues(InfoHelpType));
					ObjInfoHelpsIteratePK.setInfoHelp(trimValues(InfoHelp));
					ObjInfoHelpsIteratePK.setLanguage(trimValues(Language));
					objInfohelps.setId(ObjInfoHelpsIteratePK);
					objInfohelps.setDescription20(trimValues(Description20));
					objInfohelps.setDescription50(trimValues(Description50));
					objInfohelps.setExecution(trimValues(ExecutionPath));
					
				
					DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
				
					try {
						objInfohelps.setLastActivityDate(ObjDateFormat.parse(LastActivityDate));
					} catch (ParseException e) {
						
					}				
					ObjectInfohelpDao.insertInfoHelps(objInfohelps);														
					return false;

				}
				
				
				//Delete records form infohelps
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.Restfull_InfoHelps_Delete, method = RequestMethod.GET)
				
				public @ResponseBody  Boolean deleteInfoHelps(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp) String InfoHelp,@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_StrLanguage) String Language) throws JASCIEXCEPTION{	
					Boolean Result=false;

					
						Result= ObjectInfohelpDao.deleteInfoHelps(InfoHelp,Language);
						
					
					return Result;
				}

				//It is used to get the label of InfoHelp Assignment Search screen
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.InfoHelps_Restfull_RestInfoHelpAssignmentSearchScreenlabel, method = RequestMethod.GET)
				public @ResponseBody   List<LANGUAGES> getInfoHelpAssignmentSearchLabels(@RequestParam(value=GLOBALCONSTANT.InfoHelps_Restfull_Language) String StrLanguage)throws JASCIEXCEPTION {	

									
					List<LANGUAGES> ObjInfoHelpsList = ObjectInfohelpDao.getInfoHelpAssignmentSearchLabels(StrLanguage); 
					
					return ObjInfoHelpsList;
				}
				
				//It is used to convert the date format
				public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
				{
					SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
					DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
					String StrFormattedString=dateInString ;
				 
					try {
				 
						Date date = formatter.parse(dateInString);
						StrFormattedString=dateFormat.format(date);
				 
					} catch (Exception e) {
						
					}
					
					return StrFormattedString;
					
				}
				
				/**
				 * @author Aakash Bishnoi
				 * @Date Dec 17, 2014
				 * Description This function is used to trim the space
				 * @param Value
				 * @return
				 */
				public static String trimValues(String Value){
					if(Value == null){
						return null;
					}
					else{
						String CheckValue=Value.trim();
						if(CheckValue.length()<GLOBALCONSTANT.IntOne){
							return null;
						}else{						
						return CheckValue;
						}
					}
				}
				/**
				 * @Description upload pdf on amazon server
				 * @param request
				 * @param response
				 * @return
				 * @throws JASCIEXCEPTION
				 */
				@SuppressWarnings({ GLOBALCONSTANT.Strfinally, GLOBALCONSTANT.Strrawtypes })
				
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.InfoHelp_Restfull_UploadPdf, method = RequestMethod.POST)
				public @ResponseBody  List<Object>  uploadIcon(HttpServletRequest request,HttpServletResponse response) throws JASCIEXCEPTION {
					
					List<Object> obj=new ArrayList<Object>();
					CONFIGURATIONEFILE ObjConfiguration=new CONFIGURATIONEFILE();
					
					/*Properties ObjectProperty = new Properties();
					String propFileName = GLOBALCONSTANT.ConfigFilePathWithName;
					try {
						InputStream ObjectInputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
						ObjectProperty.load(ObjectInputStream);
					} catch (IOException ObjectIOException) {
					}*/
					
					String AMAZON_ACCESS_KEY = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_AccessKey);
					String AMAZON_SECRET_KEY = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_SecrateKey);
						
					String S3_BUCKET_NAME =ObjConfiguration.getConfigProperty(GLOBALCONSTANT.InfoHelp_BucketName);	//Change
					
					
					ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_ServerName);
					
					  String urlfile=GLOBALCONSTANT.BlankString;
				      
					
					try{
					// needed for cross-domain communication
			        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Origin, GLOBALCONSTANT.ASTRIK);
			        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Methods,GLOBALCONSTANT.POST);
			        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Headers, GLOBALCONSTANT.Access_Control_Allow_Headers_Content_Type);
			        response.setHeader(GLOBALCONSTANT.Access_Control_MaxAge, GLOBALCONSTANT.Access_Control_Allow_Max_Age_Value);
			        
			       
					
			        
			     // checks if the request actually contains upload file
			        if (!ServletFileUpload.isMultipartContent(request)) {
			           // PrintWriter writer = response.getWriter();
			           // writer.println("Request does not contain upload data");
			           // writer.flush();
			            //return GLOBALCONSTANT.BlankString;
			        }
			        
			        
			        
			     // configures upload settings
			        DiskFileItemFactory factory = new DiskFileItemFactory();
			        factory.setSizeThreshold(GLOBALCONSTANT.THRESHOLD_SIZE);
			 
			        ServletFileUpload upload = new ServletFileUpload(factory);
			        upload.setFileSizeMax(GLOBALCONSTANT.MAX_FILE_SIZE);
			        upload.setSizeMax(GLOBALCONSTANT.MAX_REQUEST_SIZE);
			         
			        
			        String uuidValue =UUID.randomUUID().toString();
			        FileItem itemFile = null;
			        
			        try {
			            // parses the request's content to extract file data
			            List formItems = upload.parseRequest(request);
			            Iterator iter = formItems.iterator();
			 
			            // iterates over form's fields to get UUID Value
			            while (iter.hasNext()) {
			                FileItem item = (FileItem) iter.next();
			                if (item.isFormField()) {
			                    if (item.getFieldName().equalsIgnoreCase(GLOBALCONSTANT.UUID_STRING)) {
			                        uuidValue = UUID.randomUUID().toString();
			                    }
			                }
			                // processes only fields that are not form fields
			                if (!item.isFormField()) {
			                    itemFile = item;
			                }
			            }
			 
			            if (itemFile != null) {
			                // get item inputstream to upload file into s3 aws
			 
			                BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY);
			 
			                AmazonS3 s3client = new AmazonS3Client(awsCredentials);
			             
			                try {
			 
			                    ObjectMetadata om = new ObjectMetadata();
			                    om.setContentLength(itemFile.getSize());
			                    String StrName=itemFile.getName();
			                    FilenameUtils.getExtension(itemFile.getName());
			                   // String keyName = uuidValue + GLOBALCONSTANT.DOT + ext;
			                    String keyName= uuidValue +GLOBALCONSTANT.HiphenSign +StrName;
			                    s3client.putObject(new PutObjectRequest(S3_BUCKET_NAME, keyName, itemFile.getInputStream(), om));
			                    s3client.setObjectAcl(S3_BUCKET_NAME, keyName, CannedAccessControlList.PublicRead);
			                    
			                    
			                    
			                    try{
			              		  GeneratePresignedUrlRequest requestUrl = new GeneratePresignedUrlRequest(S3_BUCKET_NAME, keyName);
			              		  URL url=s3client.generatePresignedUrl(requestUrl);
			              		  String protocol=url.getProtocol();
			              		  String host=url.getHost();
			              		  String file=url.getFile();
			              		  urlfile=protocol+GLOBALCONSTANT.Forward_Double_Slash+host+file;
			              		  
			              		  }catch(Exception e){
			              			 // e.printStackTrace();
			              		  }
			                    
			                    
			 
			                } catch (AmazonServiceException ase) {
			                 
			                } catch (AmazonClientException ace) {
			                
			                }
			 
			 
			            } else {
			              
			            }
			 
			        } catch (Exception ex) {
			        	
			        	
			        }
			        
			        
			        
			        
			        
			        
			 
					
					}catch(Exception objException){
						
					}
				
					finally
					{
						urlfile=urlfile.substring(GLOBALCONSTANT.INT_ZERO,urlfile.indexOf(GLOBALCONSTANT.QUESTION_MARK));			
						obj.add(urlfile);
						return obj;
					}
					
				}
				
				/**
				 * @Description it is used to get team member name from TeamMembers
				 * @author Aakash Bishnoi
				 * @date 31 dec 2014
				 * @param Tenant
				 * @param TeamMember
				 * @return List<TEAMMEMBERS>
				 * @throws JASCIEXCEPTION
				 */
				@Transactional
				@RequestMapping(value = GLOBALCONSTANT.Location_Restfull_GetTeamMember, method = RequestMethod.GET)
				public @ResponseBody String getTeamMemberName(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_TeamMember) String TeamMember) throws JASCIEXCEPTION {	

					
					//return ObjectLocationMaintenanceDao.getTeamMemberName(Tenant, TeamMember);
					String TeamMemberName=GLOBALCONSTANT.BlankString;
					
					List<TEAMMEMBERS> objTeammembers= ObjectInfohelpDao.getTeamMemberName(Tenant, TeamMember);
					
					TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
					return TeamMemberName;
					
				
				}
				
		}
				
