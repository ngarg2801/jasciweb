/**

Description This class is used to make restfull service for SMART TASK CONFIGURATOR'S SCREENS
Created By Shailendra Rajput
Created Date May 15 2014
*/
package com.jasci.biz.AdminModule.ServicesApi;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.CONFIGURATORBE;
import com.jasci.biz.AdminModule.be.GOJSJSONDATABE;
import com.jasci.biz.AdminModule.be.GOJSLINKDATABE;
import com.jasci.biz.AdminModule.be.GOJSNODEDATABE;
import com.jasci.biz.AdminModule.be.GOJSPROPERTIESBE;
import com.jasci.biz.AdminModule.be.SMARTTASKCONFIGURATORBE;
import com.jasci.biz.AdminModule.dao.ISMARTTASKCONFIGURATORDAO;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERSBEAN;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERSPK;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONSBEAN;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONSPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.WORKFLOWSTEPS;
import com.jasci.biz.AdminModule.service.ISMARTTASKCONFIGURATORSERVICE;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.CUSTOMEXECUTIONPATHVALIDATION;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.GENERAL_CODES;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class SMARTTASKCONFIGURATORSERVICEAPI {

	/** It is used to get the value form Common Session**/
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;

	/** It is use to call the function which is implemented in SMARTTASKCONFIGURATORDAOIMPL**/
	@Autowired
	ISMARTTASKCONFIGURATORDAO objSmartTaskConfiguratorDao;	

	/** Make the instance of ISMARTTASKEXECUTIONSSERVICE for calling service method*/
	@Autowired
	private ISMARTTASKCONFIGURATORSERVICE ObjSmartTaskConfiguratorService;
	
	@Autowired
	CONFIGURATORBE OBJConfiguratorBE;


	/**This is used to set the value globally */
	String StrExecutionSequenceName_G=GLOBALCONSTANT.BlankString;
	String StrExecutionSequenceName_ForJson=GLOBALCONSTANT.BlankString;
	String GoJSJsonData = GLOBALCONSTANT.BlankString;
	/**
	 * @author Shailendra Rajput
	 * @Date May 18, 2015
	 * @Description:This is used to search the list from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
	 *@param StrTenant
	 *@param StrTask
	 *@param StrApplication
	 *@param StrExecutionGroup
	 *@param StrExecutionType
	 *@param StrExecutionDevice
	 *@param StrExecutionSequenceName
	 *@param StrDescription
	 *@param StrMenuName
	 *@return List<SMARTTASKSEQHEADERSBEAN> 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Search, method = RequestMethod.POST)
	public @ResponseBody List<SMARTTASKSEQHEADERSBEAN> getList(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Task) String StrTask,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Application) String StrApplication,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionGroup) String StrExecutionGroup,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionType) String StrExecutionType,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionDevice) String StrExecutionDevice,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionSequence) String StrExecutionSequenceName,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Description) String StrDescription,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_MenuName) String StrMenuName)  throws JASCIEXCEPTION {	

		/**Create the Instance of List<Object>*/
		List<Object> ListSmartTaskConfiguratorAll=null;
		/** Check the Condition Tenant is blank or not if blank so pass tenant from Commonsession */
		if(GLOBALCONSTANT.BlankString.equals(StrTenant)){
			ListSmartTaskConfiguratorAll=objSmartTaskConfiguratorDao.getList(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrTask,StrApplication,StrExecutionGroup,StrExecutionType,StrExecutionDevice,StrExecutionSequenceName,StrDescription,StrMenuName);
		}
		else {	
			ListSmartTaskConfiguratorAll=objSmartTaskConfiguratorDao.getList(StrTenant,objCommonsessionbe.getCompany(),StrTask,StrApplication,StrExecutionGroup,StrExecutionType,StrExecutionDevice,StrExecutionSequenceName,StrDescription,StrMenuName);
		}
		/**Create the Instance of List<SMARTTASKSEQHEADERSBEAN>*/
		List<SMARTTASKSEQHEADERSBEAN> ReturnConfiguratorList=new ArrayList<SMARTTASKSEQHEADERSBEAN>();
		/**An iterator is a mechanism that permits all elements of a collection to be accessed sequentially*/
		for(Iterator<Object> LocationListLength=ListSmartTaskConfiguratorAll.iterator();LocationListLength.hasNext(); ){
			/** Craete the bean Class Object for set the data from Object to SMARTTASKSEQHEADERSBEAN*/
			SMARTTASKSEQHEADERSBEAN ObjectSmartTaskConfigurator=new SMARTTASKSEQHEADERSBEAN();
			Object []ObjectLocObj =  (Object[]) LocationListLength.next();
			try{
				ObjectSmartTaskConfigurator.setTENANT_ID(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			}catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}
			catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_TYPE(ObjectLocObj[GLOBALCONSTANT.IntOne].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_TYPE_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_TYPE_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP(ObjectLocObj[GLOBALCONSTANT.INT_FIVE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator	.setEXECUTION_SEQUENCE_NAME(ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator	.setEXECUTION_SEQUENCE_NAME(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setDESCRIPTION50(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setDESCRIPTION50(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setMENU_OPTION(ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setMENU_OPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID(ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_ELEVEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setTASK(ObjectLocObj[GLOBALCONSTANT.INT_TWELVE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTASK(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setTASK_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_THIRTEEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTASK_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				String notes=(String) ObjectLocObj[GLOBALCONSTANT.INT_FOURTEEN];
				/** If Notes is null or blank so set No for Grid otherwise set Yes */
				if(notes.isEmpty()){
					ObjectSmartTaskConfigurator.setNOTES(GLOBALCONSTANT.BlankString);
				}
				else{
					ObjectSmartTaskConfigurator.setNOTES(GLOBALCONSTANT.YES);
				}
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setNOTES(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setCOMPANY_ID(ObjectLocObj[GLOBALCONSTANT.INT_FIFTEEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setCOMPANY_ID(GLOBALCONSTANT.BlankString);
			}
			/** Add the Bean into List<SMARTTASKSEQHEADERSBEAN>*/
			ReturnConfiguratorList.add(ObjectSmartTaskConfigurator);
		}
		return ReturnConfiguratorList;
	}

	/**
	 * @author Shailendra Rajput
	 * @Date May 18, 2015
	 * @Description:This is used to search the list from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen click on display All Button.
	 *@return List<SMARTTASKEXECUTIONSBEAN> 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_DisplayAll, method = RequestMethod.POST)
	public @ResponseBody List<SMARTTASKSEQHEADERSBEAN> getDisplayAll()  throws JASCIEXCEPTION {	
		/**Create the Instance of List<Object>*/
		List<Object> ListSmartTaskConfiguratorAll=null;
		ListSmartTaskConfiguratorAll=objSmartTaskConfiguratorDao.getDisplayAll(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany());

		/**Create the Instance of SMARTTASKSEQHEADERSBEAN */
		List<SMARTTASKSEQHEADERSBEAN> ReturnConfiguratorList=new ArrayList<SMARTTASKSEQHEADERSBEAN>();
		
		/**An iterator is a mechanism that permits all elements of a collection to be accessed sequentially*/
		for(Iterator<Object> LocationListLength=ListSmartTaskConfiguratorAll.iterator();LocationListLength.hasNext(); ){
		
			/** Craete the bean Class Object for set the data from Object to SMARTTASKSEQHEADERSBEAN*/
			SMARTTASKSEQHEADERSBEAN ObjectSmartTaskConfigurator=new SMARTTASKSEQHEADERSBEAN();

			Object []ObjectLocObj =  (Object[]) LocationListLength.next();

			try{
				ObjectSmartTaskConfigurator.setTENANT_ID(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_TYPE(ObjectLocObj[GLOBALCONSTANT.IntOne].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_TYPE_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_TYPE_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_DEVICE_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP(ObjectLocObj[GLOBALCONSTANT.INT_FIVE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setEXECUTION_SEQUENCE_GROUP_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator	.setEXECUTION_SEQUENCE_NAME(ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator	.setEXECUTION_SEQUENCE_NAME(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setDESCRIPTION50(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setDESCRIPTION50(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setMENU_OPTION(ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setMENU_OPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID(ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_ELEVEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setAPPLICATION_ID_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setTASK(ObjectLocObj[GLOBALCONSTANT.INT_TWELVE].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTASK(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setTASK_DESCRIPTION(ObjectLocObj[GLOBALCONSTANT.INT_THIRTEEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setTASK_DESCRIPTION(GLOBALCONSTANT.BlankString);
			}
			try{
				String notes=(String) ObjectLocObj[GLOBALCONSTANT.INT_FOURTEEN];
				/** If Notes is null or blank so set No for Grid otherwise set Yes */
				if(notes.isEmpty()){
					ObjectSmartTaskConfigurator.setNOTES(GLOBALCONSTANT.BlankString);
				}
				else{
					ObjectSmartTaskConfigurator.setNOTES(GLOBALCONSTANT.YES);
				}
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setNOTES(GLOBALCONSTANT.BlankString);
			}
			try{
				ObjectSmartTaskConfigurator.setCOMPANY_ID(ObjectLocObj[GLOBALCONSTANT.INT_FIFTEEN].toString());
			}
			catch(ArrayIndexOutOfBoundsException obj_e)	{
				ObjectSmartTaskConfigurator.setTENANT_ID(GLOBALCONSTANT.BlankString);
			}catch(Exception obj_e)	{
				ObjectSmartTaskConfigurator.setCOMPANY_ID(GLOBALCONSTANT.BlankString);
			}
			/** Add the Bean into List<SMARTTASKSEQHEADERSBEAN>*/
			ReturnConfiguratorList.add(ObjectSmartTaskConfigurator);
		}
		return ReturnConfiguratorList;
	}
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:it is used to check Execution_Sequence_Name assigned in table SMART_TASK_sEQ_INSTRUCTIONS
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrApplication_Id
	 * @param StrExecution_Sequence_Group
	 * @param StrExecution_Sequence_Name
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionSequenceNameExist, method = RequestMethod.POST)
	public @ResponseBody boolean getExistExecutionSequenceName(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Tenant) String StrTenant_Id,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Company) String StrCompany_Id,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionSequence) String StrExecution_Sequence_Name)  throws JASCIEXCEPTION {
		/** Create the instance of WORKFLOWSTEPS*/
		List<WORKFLOWSTEPS> ObjWorkFlowSteps=null;

		ObjWorkFlowSteps = objSmartTaskConfiguratorDao.getExistExecutionSequenceName(StrTenant_Id,StrCompany_Id,StrExecution_Sequence_Name); 

		/**Check the list size is more than zero*/
		if(ObjWorkFlowSteps.size()>GLOBALCONSTANT.INT_ZERO){
			return true;     
		}
		else{
			return false;
		}

	}

	/**
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:It is used to call the dao function and delete the selected entry into table SMART_TASK_SEQUENCE_HEADERS,SMART_TASK_SEQUENCE_INSTRUCTIONS, MENU_OPTIONS and MENU_PROFILE.
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrExecutionSequenceName
	 * @param StrMenuName
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Delete, method = RequestMethod.POST)
	public @ResponseBody  boolean deleteEntry(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Company) String StrComapny,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionSequence) String StrExecutionSequenceName,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_MenuName) String StrMenuName)  throws JASCIEXCEPTION {  
		
		boolean Result=false;
		/**call SMARTTASKCONFIGURATORDAOIMPL class method for deleting exists record */
		Result= objSmartTaskConfiguratorDao.deleteEntry(StrTenant,StrComapny,StrExecutionSequenceName,StrMenuName);
		return Result;
	}

	/**
	 * @author Shailendra Rajput
	 * @Date July,2 2015
	 * @Description:it is used to get the parameter when we click on edit.
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrExecutionSequenceName
	 * @return void
	 * @throws JASCIEXCEPTION
	 */
	public void SetFetchParameter(String StrTenant_ID,String StrCompany_ID,String StrExecutionSequenceName,String StrActionName){

		StrExecutionSequenceName_G = StrExecutionSequenceName;
		
		OBJConfiguratorBE.setStrActionName_S(StrActionName);
		OBJConfiguratorBE.setStrCompany_ID_S(StrCompany_ID);
		OBJConfiguratorBE.setStrExecutionSequenceName_S(StrExecutionSequenceName);
		OBJConfiguratorBE.setStrTenant_ID_S(StrTenant_ID);
	}
	/**
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It is used to call the dao function which is used to get the record into table SMART_TASK_SEQ_HEADERS.
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrExecutionSequenceName
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	 @Transactional
	 @RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_getSTSHeadersData_AngularJS, method = RequestMethod.POST)
	 public @ResponseBody SMARTTASKSEQHEADERSBEAN getheadersData_Angularjs()  throws JASCIEXCEPTION {  

		 SMARTTASKSEQHEADERSBEAN ObjSmartTaskHeadersbean=new SMARTTASKSEQHEADERSBEAN();
	  /**call SMARTTASKCONFIGURATORDAOIMPL class method for get record from SMART_TASK_SEQ_HEADERS table
	   * these is used to get Header data when we click on Edit
	   */
	  if(GLOBALCONSTANT.Edit.equals(OBJConfiguratorBE.getStrActionName_S())){
	   SMARTTASKSEQHEADERS ObjSmartTaskHeaders= objSmartTaskConfiguratorDao.getSTSheadersData(OBJConfiguratorBE.getStrTenant_ID_S(),OBJConfiguratorBE.getStrCompany_ID_S(),OBJConfiguratorBE.getStrExecutionSequenceName_S());
	   ObjSmartTaskHeadersbean.setAPPLICATION_ID(ObjSmartTaskHeaders.getAPPLICATION_ID());
	   ObjSmartTaskHeadersbean.setCOMPANY_ID(ObjSmartTaskHeaders.getId().getCOMPANY_ID());
	   ObjSmartTaskHeadersbean.setDESCRIPTION20(ObjSmartTaskHeaders.getDESCRIPTION20());
	   ObjSmartTaskHeadersbean.setDESCRIPTION50(ObjSmartTaskHeaders.getDESCRIPTION50());
	   ObjSmartTaskHeadersbean.setEXECUTION_DEVICE(ObjSmartTaskHeaders.getEXECUTION_DEVICE());
	   ObjSmartTaskHeadersbean.setEXECUTION_SEQUENCE_GROUP(ObjSmartTaskHeaders.getEXECUTION_SEQUENCE_GROUP());
	   ObjSmartTaskHeadersbean.setEXECUTION_SEQUENCE_NAME(ObjSmartTaskHeaders.getId().getEXECUTION_SEQUENCE_NAME());
	   ObjSmartTaskHeadersbean.setEXECUTION_TYPE(ObjSmartTaskHeaders.getEXECUTION_TYPE());
	   ObjSmartTaskHeadersbean.setLAST_ACTIVITY_DATE_INTERNALUSE(ObjSmartTaskHeaders.getLAST_ACTIVITY_DATE().toString()); 
	   String StrLast_Activity_date=ConvertDate(ObjSmartTaskHeaders.getLAST_ACTIVITY_DATE().toString(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format);
	   ObjSmartTaskHeadersbean.setLAST_ACTIVITY_DATE(StrLast_Activity_date); 
	   ObjSmartTaskHeadersbean.setLAST_ACTIVITY_TEAM_MEMBER_INTERNALUSE(ObjSmartTaskHeaders.getLAST_ACTIVITY_TEAM_MEMBER());
	   String StrTeamMember=getTeamMemberName(objCommonsessionbe.getTenant(), ObjSmartTaskHeaders.getLAST_ACTIVITY_TEAM_MEMBER());
	   ObjSmartTaskHeadersbean.setLAST_ACTIVITY_TEAM_MEMBER(StrTeamMember);
	   ObjSmartTaskHeadersbean.setMENU_OPTION(ObjSmartTaskHeaders.getMENU_OPTION());
	   ObjSmartTaskHeadersbean.setTENANT_ID(ObjSmartTaskHeaders.getId().getTENANT_ID());
	   ObjSmartTaskHeadersbean.setTASK(ObjSmartTaskHeaders.getTASK());
	  }
	  else if(GLOBALCONSTANT.Copy.equals(OBJConfiguratorBE.getStrActionName_S())){

	   SMARTTASKSEQHEADERS ObjSmartTaskHeaders= objSmartTaskConfiguratorDao.getSTSheadersData(OBJConfiguratorBE.getStrTenant_ID_S(),OBJConfiguratorBE.getStrCompany_ID_S(),OBJConfiguratorBE.getStrExecutionSequenceName_S());
	   Date CurrentDate=new Date();
	   DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
	   String testDateString = ObjDateFormat.format(CurrentDate);
	   /** these is used to get Header data when we click on Copy*/
	   ObjSmartTaskHeadersbean.setAPPLICATION_ID(ObjSmartTaskHeaders.getAPPLICATION_ID());
	   ObjSmartTaskHeadersbean.setCOMPANY_ID(ObjSmartTaskHeaders.getId().getCOMPANY_ID());
	   ObjSmartTaskHeadersbean.setEXECUTION_DEVICE(ObjSmartTaskHeaders.getEXECUTION_DEVICE());
	   ObjSmartTaskHeadersbean.setEXECUTION_SEQUENCE_GROUP(ObjSmartTaskHeaders.getEXECUTION_SEQUENCE_GROUP());
	   ObjSmartTaskHeadersbean.setEXECUTION_SEQUENCE_NAME(ObjSmartTaskHeaders.getId().getEXECUTION_SEQUENCE_NAME());
	   ObjSmartTaskHeadersbean.setEXECUTION_TYPE(ObjSmartTaskHeaders.getEXECUTION_TYPE());
	   try{

	    String StrLast_Activity_date=ConvertDate(testDateString.toString(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format);
	    ObjSmartTaskHeadersbean.setLAST_ACTIVITY_DATE(StrLast_Activity_date); 
	    ObjSmartTaskHeadersbean.setLAST_ACTIVITY_DATE_INTERNALUSE(StrLast_Activity_date);
	   }
	   catch(Exception ObjExe){

	   }
	   ObjSmartTaskHeadersbean.setLAST_ACTIVITY_TEAM_MEMBER_INTERNALUSE(ObjSmartTaskHeaders.getLAST_ACTIVITY_TEAM_MEMBER());
	   String StrTeamMember=getTeamMemberName(objCommonsessionbe.getTenant(), ObjSmartTaskHeaders.getLAST_ACTIVITY_TEAM_MEMBER());
	   ObjSmartTaskHeadersbean.setLAST_ACTIVITY_TEAM_MEMBER(StrTeamMember);
	   ObjSmartTaskHeadersbean.setTENANT_ID(ObjSmartTaskHeaders.getId().getTENANT_ID());
	   ObjSmartTaskHeadersbean.setTASK(ObjSmartTaskHeaders.getTASK());
	  }
	  else {
	   /** these is used to get Header data when we click on Add*/
	   objSmartTaskConfiguratorDao.getSTSheadersData(OBJConfiguratorBE.getStrTenant_ID_S(),OBJConfiguratorBE.getStrCompany_ID_S(),OBJConfiguratorBE.getStrExecutionSequenceName_S());
	   Date CurrentDate=new Date();
	   DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
	   String testDateString = ObjDateFormat.format(CurrentDate);
	   try{

	    String StrLast_Activity_date=ConvertDate(testDateString.toString(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format);
	    ObjSmartTaskHeadersbean.setLAST_ACTIVITY_DATE(StrLast_Activity_date); 
	    ObjSmartTaskHeadersbean.setLAST_ACTIVITY_DATE_INTERNALUSE(StrLast_Activity_date);
	   }
	   catch(Exception ObjExe){}
	   try{
	    ObjSmartTaskHeadersbean.setLAST_ACTIVITY_TEAM_MEMBER_INTERNALUSE(objCommonsessionbe.getTeam_Member());
	    String StrTeamMember=getTeamMemberName(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member());
	    ObjSmartTaskHeadersbean.setLAST_ACTIVITY_TEAM_MEMBER(StrTeamMember);
	   }
	   catch(Exception ObjExe){}
	  }
	  return ObjSmartTaskHeadersbean;
	 }

	/** 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_EXECUTIONS table on the behalf of Parameters.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Group
	 * @param StrExecution_Type
	 * @param StrExecution_Device
	 * @return List<SMARTTASKEXECUTIONS>
	 * @throws JASCIEXCEPTION
	 */

	@Transactional 
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_getSTExecutionsData, method = RequestMethod.POST)
	public @ResponseBody List<SMARTTASKEXECUTIONS> getSmartTaskExecutionData(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionGroup) String StrExecutionGroup,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionType) String StrExecutionType,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionDevice) String StrExecutionDevice)  throws JASCIEXCEPTION {  

		List<SMARTTASKEXECUTIONS> ObjSmartTaskExecutions = null;
		/**call SMARTTASKCONFIGURATORDAOIMPL class method for get record from SMART_TASK_EXECUTIONS table*/
		ObjSmartTaskExecutions= objSmartTaskConfiguratorDao.getSmartTaskExecutionData(StrTenant,StrCompany,StrExecutionGroup,StrExecutionType,StrExecutionDevice);
		return ObjSmartTaskExecutions;
    }
	/**
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from GENERAL_CODES table on the behalf of Parameters for DropDown.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrGeneral_Code_Id
	 * @return List<GENERAL_CODES>
	 * @throws JASCIEXCEPTION
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_getGeneralCodeDropDown, method = RequestMethod.POST)
	public @ResponseBody List<GENERAL_CODES> getGeneralCodeDropDown(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_GeneralCodeId) String StrGeneralCodeId)  throws JASCIEXCEPTION {		

		/**call SMARTTASKCONFIGURATORDAOIMPL class method for get record from GENERAL_CODES table*/
		List<Object> ObjGenearalCodes= objSmartTaskConfiguratorDao.getGeneralCodeDropDown(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrGeneralCodeId);
		/**Create an Instance of GENERAL_CODES*/
		List<GENERAL_CODES> Obj_LookupGeneralCodeList =  new ArrayList<GENERAL_CODES>();
		if(!ObjGenearalCodes.isEmpty())
		{
			/**An iterator is a mechanism that permits all elements of a collection to be accessed sequentially*/
			Iterator<Object> Obj_LookupGeneralCodeItr = ObjGenearalCodes.iterator();
			while(Obj_LookupGeneralCodeItr.hasNext())
			{	
				Object[] GCObject = (Object[]) Obj_LookupGeneralCodeItr.next();
				/** Craete the GENERAL_CODES Class Object for set the data from Object to GENERAL_CODES*/
				GENERAL_CODES Obj_GeneralCode = new GENERAL_CODES();
				try{
					/**Set the Value of generalCode*/
					Obj_GeneralCode.setGeneralCode(GCObject[GLOBALCONSTANT.INT_ZERO].toString());
				}catch(ArrayIndexOutOfBoundsException obj_e){}
				catch(Exception ObjException){}
				try{
					/**Set the Value of Description20*/
					Obj_GeneralCode.setDescription20(GCObject[GLOBALCONSTANT.IntOne].toString());
				}
				catch(ArrayIndexOutOfBoundsException obj_e){}
				catch(Exception ObjException){}
				try{
					/**Set the Value of Description50*/
					Obj_GeneralCode.setDescription50(GCObject[GLOBALCONSTANT.INT_TWO].toString());
				}catch(ArrayIndexOutOfBoundsException obj_e){}
				catch(Exception ObjException){}
				try{
					/**Set the Value of Description50*/
					Obj_GeneralCode.setHelpline(GCObject[GLOBALCONSTANT.INT_THREE].toString());
				}catch(ArrayIndexOutOfBoundsException obj_e){}
				catch(Exception ObjException){}
				Obj_LookupGeneralCodeList.add(Obj_GeneralCode);
			}
		}
		return Obj_LookupGeneralCodeList;
	}

	/** 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_SEQ_INSTRUCTIONS table on the behalf of Parameters for Sequence View.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @return List<SMARTTASKSEQINSTRUCTIONS>
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	//@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_getSTSeqInstructions, method = RequestMethod.POST)
	@RequestMapping(value = GLOBALCONSTANT.gojsdata, method = RequestMethod.POST)			
	public @ResponseBody GOJSJSONDATABE getSequenceViewData(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionSequence) String StrExecutionSequenceName,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Company) String StrCompany)  throws JASCIEXCEPTION {

		List<SMARTTASKSEQINSTRUCTIONS> ObjSmartTaskSeqInstructions = null;
		//List<GOJSCOMBINEDDATABE> OBJGojscombineddatabes = null;
		List<GOJSNODEDATABE> OBJGojsnodedatabes = new ArrayList<GOJSNODEDATABE>();
		List<GOJSLINKDATABE> OBJGojslinkdatabes = new ArrayList<GOJSLINKDATABE>();
		GOJSJSONDATABE OBJGojsjsondatabe = new GOJSJSONDATABE();		
		/**call SMARTTASKCONFIGURATORDAOIMPL class method for get record from SMART_TASK_SEQ_INSTRUCTIONS table*/

		ObjSmartTaskSeqInstructions= objSmartTaskConfiguratorDao.getSequenceViewData(StrTenant,StrCompany,StrExecutionSequenceName);
		GOJSPROPERTIESBE OBJGojspropertiesbe = new GOJSPROPERTIESBE();
		CONFIGURATIONEFILE OBJCOConfigurationefile = new CONFIGURATIONEFILE();
		OBJGojspropertiesbe = OBJCOConfigurationefile.getGoJSConfigProperty();
		int topdistance = OBJGojspropertiesbe.getMarginTop();
		int verticaldistance = OBJGojspropertiesbe.getVerticalDistance();
		int horizontaldistance = OBJGojspropertiesbe.getHorizontalDistance();
		int leftdistance = OBJGojspropertiesbe.getMarginLeft();

		int circleheight = OBJGojspropertiesbe.getCircleHeight();
		int rectangleheight = OBJGojspropertiesbe.getRectangleHeight();
		int diamondheight = OBJGojspropertiesbe.getDiamondHeight();
		int squareheight = OBJGojspropertiesbe.getSquareHeight();
		int displayheight = OBJGojspropertiesbe.getDisplayHeight();
		int lastnodehieght = GLOBALCONSTANT.INT_ZERO;
		int nextnodelocation = GLOBALCONSTANT.INT_ZERO;
	    int IsLastNodeSquare = GLOBALCONSTANT.INT_ZERO;

		GOJSNODEDATABE OBJGojsnodedatabe = null;
		GOJSLINKDATABE OBJGojslinkdatabe = null;

		Map<String, String> GOTAGNODEHM = new HashMap<String, String>();
		if(ObjSmartTaskSeqInstructions.size()>GLOBALCONSTANT.INT_ZERO)
		{
			for(int GotoNodeNumber = GLOBALCONSTANT.INT_ZERO ; GotoNodeNumber < ObjSmartTaskSeqInstructions.size();GotoNodeNumber++)
			{try{
				SMARTTASKSEQINSTRUCTIONS OBJSmarttaskseqinstructionsbean = ObjSmartTaskSeqInstructions.get(GotoNodeNumber);

				if(GLOBALCONSTANT.GENERALTAG.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
				{
					GOTAGNODEHM.put(OBJSmarttaskseqinstructionsbean.getExecution_Name(), String.valueOf(GotoNodeNumber+GLOBALCONSTANT.IntOne));
				}
				else if(GLOBALCONSTANT.End.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
				{
					GOTAGNODEHM.put(OBJSmarttaskseqinstructionsbean.getAction_Name(), String.valueOf(GotoNodeNumber+GLOBALCONSTANT.IntOne));
				}}catch(Exception objEx)
			{
					throw new JASCIEXCEPTION(objEx.getMessage());
			}

			}
			List<SMARTTASKEXECUTIONS> oBJSmarttaskexecutionslist = new ArrayList<SMARTTASKEXECUTIONS>();
			SMARTTASKEXECUTIONS objSmarttaskexecutions = new SMARTTASKEXECUTIONS();

			for(int SequenceNumber = GLOBALCONSTANT.INT_ZERO ; SequenceNumber < ObjSmartTaskSeqInstructions.size();SequenceNumber++)
			{
				try{
					SMARTTASKSEQINSTRUCTIONS OBJSmarttaskseqinstructionsbean = ObjSmartTaskSeqInstructions.get(SequenceNumber);
					OBJGojsnodedatabe = new GOJSNODEDATABE();
					OBJGojslinkdatabe = new GOJSLINKDATABE();


					if(GLOBALCONSTANT.Start.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
					{
						OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Start);
						OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
						OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+topdistance);
						topdistance = topdistance + OBJGojspropertiesbe.getCircleHeight()+OBJGojspropertiesbe.getCircleVerticalDistance();
						lastnodehieght = OBJGojspropertiesbe.getCircleHeight();
						OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
						OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());	
						OBJGojsnodedatabes.add(OBJGojsnodedatabe);
					}
					else
					{
						if(GLOBALCONSTANT.Execution.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getExecution_Sequence_Type()))
						{				
							OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Rectangle);
							OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
							/**Get Next node location*/
							nextnodelocation = setRectangleShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

							OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
							topdistance = topdistance + OBJGojspropertiesbe.getRectangleHeight()+verticaldistance;
							lastnodehieght = OBJGojspropertiesbe.getRectangleHeight();
							
							/**Get Execution Description20 and Description50 from ST_Execution table on the basis
							   of execution name*/
							oBJSmarttaskexecutionslist = new ArrayList<SMARTTASKEXECUTIONS>();
							objSmarttaskexecutions = new SMARTTASKEXECUTIONS();
							oBJSmarttaskexecutionslist = objSmartTaskConfiguratorDao.getExecutionData(OBJSmarttaskseqinstructionsbean.getExecution_Name());
							try{
							objSmarttaskexecutions = (SMARTTASKEXECUTIONS)oBJSmarttaskexecutionslist.get(GLOBALCONSTANT.INT_ZERO);
							}catch(ArrayIndexOutOfBoundsException obj_e){}
							catch(Exception e){}
							String Description20 = GLOBALCONSTANT.BlankString;
							String Description50 = GLOBALCONSTANT.BlankString;
							try{
							Description20 = objSmarttaskexecutions.getDescription20();
							}catch(Exception e){}
							try{
							Description50 = objSmarttaskexecutions.getDescription50();
							}catch(Exception e){}
							
							if(Description20.isEmpty())
							{
								Description20 = OBJSmarttaskseqinstructionsbean.getExecution_Name();	
							}
							if(Description50.isEmpty())
							{
								Description50 = OBJSmarttaskseqinstructionsbean.getExecution_Name();	
							}						
							
							try{						
							OBJGojsnodedatabe.setText(Description20);
							}catch(Exception e){}
							OBJGojsnodedatabe.setExecutionName(OBJSmarttaskseqinstructionsbean.getExecution_Name());
							try{
							OBJGojsnodedatabe.setToolTipText(Description50);
							}catch(Exception e){}
							OBJGojsnodedatabes.add(OBJGojsnodedatabe);

							OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
							OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
							OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
							OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
							OBJGojslinkdatabes.add(OBJGojslinkdatabe);

						}
						else
						{			
							if(GLOBALCONSTANT.IFERROR.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								if((OBJSmarttaskseqinstructionsbean.getMessage()!= GLOBALCONSTANT.BlankString && OBJSmarttaskseqinstructionsbean.getMessage()!= null) && (OBJSmarttaskseqinstructionsbean.getGoto_Tag()!= GLOBALCONSTANT.BlankString && OBJSmarttaskseqinstructionsbean.getGoto_Tag() != null))
								{
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Diamond);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setDiamondShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getDiamondHeight()+OBJGojspropertiesbe.getDiamondVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getDiamondHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);

									OBJGojsnodedatabe = new GOJSNODEDATABE();
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.ERRORDISPLAY);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne)+GLOBALCONSTANT.Stringpointone);
									int gotodisplaynodelocation = leftdistance + horizontaldistance;
									OBJGojsnodedatabe.setLoc(gotodisplaynodelocation+GLOBALCONSTANT.Single_Space+nextnodelocation);
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getMessage());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getMessage());	
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe = new GOJSLINKDATABE();
									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne)+GLOBALCONSTANT.Stringpointone);
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.R);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.L);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);

									OBJGojslinkdatabe = new GOJSLINKDATABE();
									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne)+GLOBALCONSTANT.Stringpointone);
									OBJGojslinkdatabe.setTo(GOTAGNODEHM.get(OBJSmarttaskseqinstructionsbean.getGoto_Tag()));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.T);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.R);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);

								}else{
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Diamond);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setDiamondShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getDiamondHeight()+OBJGojspropertiesbe.getDiamondVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getDiamondHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);						

								}
							}
							else if(GLOBALCONSTANT.IFEXIT.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								if(OBJSmarttaskseqinstructionsbean.getGoto_Tag()!= GLOBALCONSTANT.BlankString && OBJSmarttaskseqinstructionsbean.getGoto_Tag()!= null)
								{
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Diamond);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setDiamondShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getDiamondHeight()+OBJGojspropertiesbe.getDiamondVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getDiamondHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);

									OBJGojslinkdatabe = new GOJSLINKDATABE();
									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setTo(GOTAGNODEHM.get(OBJSmarttaskseqinstructionsbean.getGoto_Tag()));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.R);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.R);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}else{
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Diamond);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setDiamondShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getDiamondHeight()+OBJGojspropertiesbe.getDiamondVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getDiamondHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}


							}
							else if(GLOBALCONSTANT.IFRETURNCODE.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								if((OBJSmarttaskseqinstructionsbean.getReturn_Code_Value()!= GLOBALCONSTANT.BlankString && OBJSmarttaskseqinstructionsbean.getReturn_Code_Value()!= null) && (OBJSmarttaskseqinstructionsbean.getGoto_Tag()!= GLOBALCONSTANT.BlankString &&  OBJSmarttaskseqinstructionsbean.getGoto_Tag()!= null))
								{
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Diamond);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setDiamondShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getDiamondHeight()+OBJGojspropertiesbe.getDiamondVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getDiamondHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name()+ GLOBALCONSTANT.Single_Space + OBJSmarttaskseqinstructionsbean.getReturn_Code_Value());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name()+ GLOBALCONSTANT.Single_Space + OBJSmarttaskseqinstructionsbean.getReturn_Code_Value());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);

									OBJGojslinkdatabe = new GOJSLINKDATABE();

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setTo(GOTAGNODEHM.get(OBJSmarttaskseqinstructionsbean.getGoto_Tag()));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.R);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.R);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}else{
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Diamond);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setDiamondShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getDiamondHeight()+OBJGojspropertiesbe.getDiamondVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getDiamondHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}


							}
							else if(GLOBALCONSTANT.DISPLAY.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.DISPLAY);
								OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								/**Get Next node location*/
								nextnodelocation = setDisplayShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

								OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
								topdistance = topdistance + OBJGojspropertiesbe.getDisplayHeight()+verticaldistance;
								lastnodehieght = OBJGojspropertiesbe.getDisplayHeight();
								OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getMessage());
								OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getMessage());
								OBJGojsnodedatabes.add(OBJGojsnodedatabe);

								OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
								OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
								OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
								OBJGojslinkdatabes.add(OBJGojslinkdatabe);


							}
							else if(GLOBALCONSTANT.CUSTOMEXECUTION.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.CUSTOMEXECUTION);
								OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								/**Get Next node location*/
								nextnodelocation = setRectangleShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

								OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
								topdistance = topdistance + OBJGojspropertiesbe.getRectangleHeight()+verticaldistance;
								lastnodehieght = OBJGojspropertiesbe.getRectangleHeight();
								OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getCustom_Execution());
								OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getCustom_Execution());
								OBJGojsnodedatabes.add(OBJGojsnodedatabe);

								OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
								OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
								OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
								OBJGojslinkdatabes.add(OBJGojslinkdatabe);


							}
							else if(GLOBALCONSTANT.GOTOTAG.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{	
								if(OBJSmarttaskseqinstructionsbean.getGoto_Tag()!= GLOBALCONSTANT.BlankString &&  OBJSmarttaskseqinstructionsbean.getGoto_Tag()!= null)
								{
									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.GOTOTAG);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setSquareShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getSquareHeight()+OBJGojspropertiesbe.getSquareVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getSquareHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);

									OBJGojslinkdatabe = new GOJSLINKDATABE();
									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setTo(GOTAGNODEHM.get(OBJSmarttaskseqinstructionsbean.getGoto_Tag()));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.R);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.R);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}else{

									OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.GOTOTAG);
									OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									/**Get Next node location*/
									nextnodelocation = setSquareShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

									OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
									topdistance = topdistance + OBJGojspropertiesbe.getSquareHeight()+OBJGojspropertiesbe.getSquareVerticalDistance();
									lastnodehieght = OBJGojspropertiesbe.getSquareHeight();
									OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
									OBJGojsnodedatabes.add(OBJGojsnodedatabe);

									OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
									OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
									OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
									OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
									OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}


							}
							else if(GLOBALCONSTANT.GENERALTAG.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.GENERALTAG);
								OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								
								/**Get Next node location*/
								nextnodelocation = setCircleShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);
								if(lastnodehieght == squareheight)
								{		                        	
								IsLastNodeSquare = GLOBALCONSTANT.IntOne;
								}
								OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
								topdistance = topdistance + OBJGojspropertiesbe.getCircleHeight()+OBJGojspropertiesbe.getCircleVerticalDistance();
								lastnodehieght = OBJGojspropertiesbe.getCircleHeight();
								OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getExecution_Name());
								OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getExecution_Name());
								OBJGojsnodedatabes.add(OBJGojsnodedatabe);

								if(IsLastNodeSquare == GLOBALCONSTANT.INT_ZERO)
								{
								OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
								OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
								OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
								OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}else
								{
									IsLastNodeSquare = GLOBALCONSTANT.INT_ZERO;
								}


							}
							else if(GLOBALCONSTANT.COMMENTS.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.COMMENTS);
								OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								/**Get Next node location*/
								nextnodelocation = setRectangleShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);
								
								OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
								topdistance = topdistance + OBJGojspropertiesbe.getRectangleHeight()+verticaldistance;
								lastnodehieght = OBJGojspropertiesbe.getRectangleHeight();
								OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getComments());
								OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getComments());
								OBJGojsnodedatabes.add(OBJGojsnodedatabe);

								OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
								OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
								OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
								OBJGojslinkdatabes.add(OBJGojslinkdatabe);


							}
							else if(GLOBALCONSTANT.End.equalsIgnoreCase(OBJSmarttaskseqinstructionsbean.getAction_Name()))
							{
								OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.End);
								OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								/**Get Next node location*/
								nextnodelocation = setCircleShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);
								if(lastnodehieght == squareheight)
								{		                        	
								IsLastNodeSquare = GLOBALCONSTANT.IntOne;
								}
								OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
								topdistance = topdistance + OBJGojspropertiesbe.getCircleHeight()+OBJGojspropertiesbe.getCircleVerticalDistance();
								lastnodehieght = OBJGojspropertiesbe.getCircleHeight();
								OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
								OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
								OBJGojsnodedatabes.add(OBJGojsnodedatabe);

								if(IsLastNodeSquare == GLOBALCONSTANT.INT_ZERO)
								{
								OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
								OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
								OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
								OBJGojslinkdatabes.add(OBJGojslinkdatabe);
								}else{
									IsLastNodeSquare = GLOBALCONSTANT.INT_ZERO;
								}


							}
							else
							{
								OBJGojsnodedatabe.setCategory(OBJSmarttaskseqinstructionsbean.getAction_Name());
								OBJGojsnodedatabe.setKey(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								/**Get Next node location*/
								nextnodelocation = setRectangleShapeNodeLocation(topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight);

								OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+nextnodelocation);
								topdistance = topdistance + OBJGojspropertiesbe.getRectangleHeight()+verticaldistance;
								lastnodehieght = OBJGojspropertiesbe.getRectangleHeight();
								OBJGojsnodedatabe.setText(OBJSmarttaskseqinstructionsbean.getAction_Name());
								OBJGojsnodedatabe.setToolTipText(OBJSmarttaskseqinstructionsbean.getAction_Name());
								OBJGojsnodedatabes.add(OBJGojsnodedatabe);

								OBJGojslinkdatabe.setFrom(String.valueOf(SequenceNumber));
								OBJGojslinkdatabe.setTo(String.valueOf(SequenceNumber+GLOBALCONSTANT.IntOne));
								OBJGojslinkdatabe.setFromPort(GLOBALCONSTANT.B);
								OBJGojslinkdatabe.setToPort(GLOBALCONSTANT.T);				
								OBJGojslinkdatabes.add(OBJGojslinkdatabe);


							}			
						}
					}


				}catch (Exception obj_ex) {}
				}}
		else{
			OBJGojsnodedatabe = new GOJSNODEDATABE();
			OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.Start);
			OBJGojsnodedatabe.setKey(GLOBALCONSTANT.STRING_ONE);
			OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+topdistance);
			OBJGojsnodedatabe.setText(GLOBALCONSTANT.Start);
			OBJGojsnodedatabe.setToolTipText(GLOBALCONSTANT.Start);	
			OBJGojsnodedatabes.add(OBJGojsnodedatabe);

			OBJGojsnodedatabe = new GOJSNODEDATABE();
			OBJGojsnodedatabe.setCategory(GLOBALCONSTANT.End);
			OBJGojsnodedatabe.setKey(GLOBALCONSTANT.STRING_TWO);
			topdistance = topdistance + OBJGojspropertiesbe.getCircleHeight()+verticaldistance;
			OBJGojsnodedatabe.setLoc(leftdistance+GLOBALCONSTANT.Single_Space+topdistance);
			OBJGojsnodedatabe.setText(GLOBALCONSTANT.End);
			OBJGojsnodedatabe.setToolTipText(GLOBALCONSTANT.End);
			OBJGojsnodedatabes.add(OBJGojsnodedatabe);


		}

		/**Add data into gojs jason onject*/

		OBJGojsjsondatabe.setclass(GLOBALCONSTANT.go_GraphLinksModel);
		OBJGojsjsondatabe.setLinkFromPortIdProperty(GLOBALCONSTANT.fromPort);
		OBJGojsjsondatabe.setLinkToPortIdProperty(GLOBALCONSTANT.toPort);
		OBJGojsjsondatabe.setLinkDataArray(OBJGojslinkdatabes);
		OBJGojsjsondatabe.setNodeDataArray(OBJGojsnodedatabes);


		return OBJGojsjsondatabe;
	}

	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date Jun,29 2015
	 * @Description:It  is used to get the label of Smart task Configurator screen.
	 * @param StrLanguage
	 * @return SMARTTASKCONFIGURATORBE
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_getLabels, method = RequestMethod.POST)
	public @ResponseBody SMARTTASKCONFIGURATORBE getSmartTaskConfiguratorLabels(String StrLanguage) throws JASCIEXCEPTION{ 
		return ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(objCommonsessionbe.getCurrentLanguage());
	}


	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Add Record in Smart_Task_Seq_Headers and Smart_Task_Seq_Instruction Table Form SmartTaskConfigurator Sreen
	 *@param ObjectSTSHeadersBean
	 *@param ObjectSTSInstructionsBean
	 *@return String
	 *@throws JASCIEXCEPTION
	 * @throws ParseException 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_RestSmartTaskConfiguratorAdd, method = RequestMethod.POST)
	public @ResponseBody  String addEntry(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue) String Execution_Sequence_Name,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Application_Id_JspPageValue) String Application_Id,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Group_JspPageValue) String Execution_Sequence_Group,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Description20_JspPageValue) String Description20,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Description50_JspPageValue) String Description50,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Menu_Option_JspPageValue) String Menu_Option,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Device_JspPageValue) String Execution_Device,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Type_JspPageValue) String Execution_Type,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Task_JspPageValue) String Task) throws JASCIEXCEPTION{
		StrExecutionSequenceName_ForJson=Execution_Sequence_Name;
		String Result=null;
		Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
		String testDateString = ObjDateFormat.format(CurrentDate);
		//set menuoption data
		MENUOPTIONS Obj_MENUOPTIONS=new MENUOPTIONS();
		Obj_MENUOPTIONS.setTenant(trimValues(objCommonsessionbe.getTenant()));
		Obj_MENUOPTIONS.setApplication(trimValues(Application_Id));
		Obj_MENUOPTIONS.setMenuOption(trimValues(Menu_Option));
		Obj_MENUOPTIONS.setDescription20(trimValues(Description20));
		Obj_MENUOPTIONS.setDescription50(trimValues(Description50));


		Obj_MENUOPTIONS.setMenuType(trimValues(Execution_Device));
		Obj_MENUOPTIONS.setApplicationSub(trimValues(GLOBALCONSTANT.GeneralCodes_ApplicationValue));
		Obj_MENUOPTIONS.setHelpline(trimValues(Menu_Option));
		Obj_MENUOPTIONS.setExecution(trimValues(GLOBALCONSTANT.SmartTaskConfigurator_Menu_Name+Menu_Option));


		try {
			Obj_MENUOPTIONS.setLastActivityDate(ObjDateFormat.parse(testDateString));
		} catch (ParseException ObjParseException) {

		}

		Obj_MENUOPTIONS.setLastActivityTeamMember(trimValues(objCommonsessionbe.getTeam_Member()));
		Result=objSmartTaskConfiguratorDao.addEntrySTCMO(Obj_MENUOPTIONS);	



		SMARTTASKSEQHEADERS ObjectSTSHeaders=new SMARTTASKSEQHEADERS();
		SMARTTASKSEQHEADERSPK ObjectSTSHeadersPk=new SMARTTASKSEQHEADERSPK();

		ObjectSTSHeadersPk.setTENANT_ID(trimValues(objCommonsessionbe.getTenant()));
		ObjectSTSHeadersPk.setCOMPANY_ID(trimValues(objCommonsessionbe.getCompany()));
		ObjectSTSHeadersPk.setEXECUTION_SEQUENCE_NAME(trimValues(Execution_Sequence_Name));	
		ObjectSTSHeaders.setId(ObjectSTSHeadersPk);

		ObjectSTSHeaders.setAPPLICATION_ID(trimValues(Application_Id));
		ObjectSTSHeaders.setDESCRIPTION20(trimValues(Description20));
		ObjectSTSHeaders.setDESCRIPTION50(trimValues(Description50));
		ObjectSTSHeaders.setEXECUTION_DEVICE(trimValues(Execution_Device));
		ObjectSTSHeaders.setEXECUTION_SEQUENCE_GROUP(trimValues(Execution_Sequence_Group));
		ObjectSTSHeaders.setEXECUTION_TYPE(trimValues(Execution_Type));


		try {
			ObjectSTSHeaders.setLAST_ACTIVITY_DATE(ObjDateFormat.parse(testDateString));

		} catch (ParseException ObjParseException) {

		}

		ObjectSTSHeaders.setLAST_ACTIVITY_TEAM_MEMBER(trimValues(objCommonsessionbe.getTeam_Member()));
		ObjectSTSHeaders.setMENU_OPTION(trimValues(Menu_Option));
		ObjectSTSHeaders.setTASK(trimValues(Task));




		Result=objSmartTaskConfiguratorDao.addEntrySTSH(ObjectSTSHeaders);	

		List<SMARTTASKSEQINSTRUCTIONSBEAN> objSTSIBean=createSequenceView(StrExecutionSequenceName_G);

		for(SMARTTASKSEQINSTRUCTIONSBEAN iterateObjSTSIBean:objSTSIBean){
			SMARTTASKSEQINSTRUCTIONS ObjectSTSInstructions=new SMARTTASKSEQINSTRUCTIONS();
			SMARTTASKSEQINSTRUCTIONSPK ObjectSTSInstructionsPk=new SMARTTASKSEQINSTRUCTIONSPK();
			ObjectSTSInstructionsPk.setTenant_Id(trimValues(objCommonsessionbe.getTenant()));
			ObjectSTSInstructionsPk.setCompany_Id(trimValues(objCommonsessionbe.getCompany()));
			try{
				ObjectSTSInstructionsPk.setExecution_Sequence(Long.parseLong(iterateObjSTSIBean.getExecution_Sequence()));
			}catch(Exception e)
			{
				ObjectSTSInstructionsPk.setExecution_Sequence(GLOBALCONSTANT.LongZero);

			}
			ObjectSTSInstructionsPk.setExecution_Sequence_Name(trimValues(iterateObjSTSIBean.getExecution_Sequence_Name()));

			ObjectSTSInstructions.setId(ObjectSTSInstructionsPk);
			ObjectSTSInstructions.setAction_Name(trimValues(iterateObjSTSIBean.getAction_Name()));

			ObjectSTSInstructions.setComments(trimValues(iterateObjSTSIBean.getComments()));
			ObjectSTSInstructions.setCustom_Execution(trimValues(iterateObjSTSIBean.getCustom_Execution()));
			ObjectSTSInstructions.setExecution_Name(trimValues(iterateObjSTSIBean.getExecution_Name()));
			ObjectSTSInstructions.setExecution_Sequence_Type(trimValues(iterateObjSTSIBean.getExecution_Sequence_Type()));
			ObjectSTSInstructions.setGoto_Tag(trimValues(iterateObjSTSIBean.getGoto_Tag()));
			ObjectSTSInstructions.setMessage(trimValues(iterateObjSTSIBean.getMessage()));
			ObjectSTSInstructions.setReturn_Code_Value(trimValues(iterateObjSTSIBean.getReturn_Code_Value()));
			Result=objSmartTaskConfiguratorDao.addEntrySTSI(ObjectSTSInstructions);	
		}




		return Result;
	}


	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Update Record in Smart_Task_Seq_Headers and Smart_Task_Seq_Instruction Table Form SmartTaskConfigurator Sreen
	 *@param ObjectSTSHeadersBean
	 *@param ObjectSTSInstructionsBean
	 *@return String
	 *@throws JASCIEXCEPTION
	 * @throws ParseException 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_RestSmartTaskConfiguratorUpdate, method = RequestMethod.POST)
	public @ResponseBody  String updateEntry(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Tenant_Id_JspPageValue) String Tenant_ID,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Company_Id_JspPageValue) String Company_ID,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue) String Execution_Sequence_Name,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Application_Id_JspPageValue) String Application_Id,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Group_JspPageValue) String Execution_Sequence_Group,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Description20_JspPageValue) String Description20,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Description50_JspPageValue) String Description50,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Menu_Option_JspPageValue) String Menu_Option,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Device_JspPageValue) String Execution_Device,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Type_JspPageValue) String Execution_Type,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Last_Activity_Date_JspPageValue) String Last_Activity_Date,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Last_Activity_Team_Member_JspPageValue) String Last_Activity_Team_Member,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Task_JspPageValue) String Task) throws JASCIEXCEPTION{
		StrExecutionSequenceName_ForJson=Execution_Sequence_Name;
		String Result=null;
		Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
		String testDateString = ObjDateFormat.format(CurrentDate);
		//set menu option data
		MENUOPTIONS Obj_MENUOPTIONS=new MENUOPTIONS();
		Obj_MENUOPTIONS.setTenant(trimValues(objCommonsessionbe.getTenant()));
		Obj_MENUOPTIONS.setApplication(trimValues(Application_Id));
		Obj_MENUOPTIONS.setMenuOption(trimValues(Menu_Option));
		Obj_MENUOPTIONS.setDescription20(trimValues(Description20));
		Obj_MENUOPTIONS.setDescription50(trimValues(Description50));
		Obj_MENUOPTIONS.setMenuType(trimValues(Execution_Device));
		Obj_MENUOPTIONS.setApplicationSub(trimValues(GLOBALCONSTANT.GeneralCodes_ApplicationValue));
		Obj_MENUOPTIONS.setHelpline(trimValues(Menu_Option));
		Obj_MENUOPTIONS.setExecution(trimValues(GLOBALCONSTANT.SmartTaskConfigurator_Menu_Name+Menu_Option));
		try {
			Obj_MENUOPTIONS.setLastActivityDate(ObjDateFormat.parse(testDateString));
		} catch (ParseException ObjParseException) {}

		Obj_MENUOPTIONS.setLastActivityTeamMember(trimValues(objCommonsessionbe.getTeam_Member()));
		Result=objSmartTaskConfiguratorDao.updateEntrySTCMO(Obj_MENUOPTIONS);	

		SMARTTASKSEQHEADERS ObjectSTSHeaders=new SMARTTASKSEQHEADERS();
		SMARTTASKSEQHEADERSPK ObjectSTSHeadersPk=new SMARTTASKSEQHEADERSPK();

		ObjectSTSHeadersPk.setTENANT_ID(trimValues(Tenant_ID));
		ObjectSTSHeadersPk.setCOMPANY_ID(trimValues(Company_ID));
		ObjectSTSHeadersPk.setEXECUTION_SEQUENCE_NAME(trimValues(Execution_Sequence_Name));	
		ObjectSTSHeaders.setId(ObjectSTSHeadersPk);

		ObjectSTSHeaders.setAPPLICATION_ID(trimValues(Application_Id));
		ObjectSTSHeaders.setDESCRIPTION20(trimValues(Description20));
		ObjectSTSHeaders.setDESCRIPTION50(trimValues(Description50));
		ObjectSTSHeaders.setEXECUTION_DEVICE(trimValues(Execution_Device));
		ObjectSTSHeaders.setEXECUTION_SEQUENCE_GROUP(trimValues(Execution_Sequence_Group));
		ObjectSTSHeaders.setEXECUTION_TYPE(trimValues(Execution_Type));
		try {
			ObjectSTSHeaders.setLAST_ACTIVITY_DATE(ObjDateFormat.parse(testDateString));
		} catch (ParseException ObjParseException) {
		}
		ObjectSTSHeaders.setLAST_ACTIVITY_TEAM_MEMBER(trimValues(objCommonsessionbe.getTeam_Member()));
		ObjectSTSHeaders.setMENU_OPTION(trimValues(Menu_Option));
		ObjectSTSHeaders.setTASK(trimValues(Task));
		Result=objSmartTaskConfiguratorDao.updateEntrySTSH(ObjectSTSHeaders);	

		List<SMARTTASKSEQINSTRUCTIONSBEAN> objSTSIBean=createSequenceView(StrExecutionSequenceName_G);

		/**Delete Record before update in sequence instruction*/
		int checkdeleteflag = GLOBALCONSTANT.INT_ZERO;
		for(SMARTTASKSEQINSTRUCTIONSBEAN iterateObjSTSIBean:objSTSIBean){
			SMARTTASKSEQINSTRUCTIONS ObjectSTSInstructions=new SMARTTASKSEQINSTRUCTIONS();
			SMARTTASKSEQINSTRUCTIONSPK ObjectSTSInstructionsPk=new SMARTTASKSEQINSTRUCTIONSPK();

			ObjectSTSInstructionsPk.setTenant_Id(trimValues(Tenant_ID));
			ObjectSTSInstructionsPk.setCompany_Id(trimValues(Company_ID));
			try{
				ObjectSTSInstructionsPk.setExecution_Sequence(Long.parseLong(iterateObjSTSIBean.getExecution_Sequence()));
			}catch(Exception objex)
			{
				ObjectSTSInstructionsPk.setExecution_Sequence(GLOBALCONSTANT.LongZero);
			}
			ObjectSTSInstructionsPk.setExecution_Sequence_Name(trimValues(iterateObjSTSIBean.getExecution_Sequence_Name()));

			if(checkdeleteflag == GLOBALCONSTANT.INT_ZERO)
			{
				objSmartTaskConfiguratorDao.deleteEntrySTSI(ObjectSTSInstructionsPk);
				checkdeleteflag = GLOBALCONSTANT.IntOne;
			}
			ObjectSTSInstructions.setId(ObjectSTSInstructionsPk);
			ObjectSTSInstructions.setAction_Name(trimValues(iterateObjSTSIBean.getAction_Name()));
			ObjectSTSInstructions.setComments(trimValues(iterateObjSTSIBean.getComments()));
			ObjectSTSInstructions.setCustom_Execution(trimValues(iterateObjSTSIBean.getCustom_Execution()));
			ObjectSTSInstructions.setExecution_Name(trimValues(iterateObjSTSIBean.getExecution_Name()));
			ObjectSTSInstructions.setExecution_Sequence_Type(trimValues(iterateObjSTSIBean.getExecution_Sequence_Type()));
			ObjectSTSInstructions.setGoto_Tag(trimValues(iterateObjSTSIBean.getGoto_Tag()));
			ObjectSTSInstructions.setMessage(trimValues(iterateObjSTSIBean.getMessage()));
			ObjectSTSInstructions.setReturn_Code_Value(trimValues(iterateObjSTSIBean.getReturn_Code_Value()));
			Result=objSmartTaskConfiguratorDao.updateEntrySTSI(ObjectSTSInstructions);	
		}
		return Result;
	}


	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description This function is used to trim the space
	 * @param Value
	 * @return String
	 */
	public static String trimValues(String Value){
		if(Value == null){
			return null;
		}
		else{
			String CheckValue=Value.trim();
			if(CheckValue.length()<GLOBALCONSTANT.IntOne){
				return null;
			}else{						
				return CheckValue;
			}
		}
	}		

	/**
	 * @author Shailendra Rajput
	 * @Date July 3, 2015
	 * Description This is used to convert date format
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return String
	 */
	public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
		DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
		String StrFormattedString=dateInString ;
		try {
			Date date = formatter.parse(dateInString);
			StrFormattedString=dateFormat.format(date);
		} catch (Exception e) {
		}
		return StrFormattedString;
	}
	
	/**
	 * @author Shailendra Rajput
	 * @Date July 3, 2015
	 * @Description This is used to get team member name.
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return String
	 */
	 public String getTeamMemberName(String Tenant,String TeamMember) throws JASCIEXCEPTION {		 
	 /**declare and intialize team member variable*/ 
	  String TeamMemberName=GLOBALCONSTANT.BlankString;
	  List<TEAMMEMBERS> objTeammembers= objSmartTaskConfiguratorDao.getTeamMemberName(Tenant,TeamMember);
	  TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
	  return TeamMemberName;
	 }
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date Jun 23, 2015
	 * @Description:This Function is used to get the json form flowchart and create the data for insert data into ST_Instruction table
	 * @return List<SMARTTASKSEQINSTRUCTIONSBEAN>
	 * @throws JASCIEXCEPTION
	 */
	public List<SMARTTASKSEQINSTRUCTIONSBEAN> createSequenceView(String ExecutionName){

		//create the instance of JSONParser
		JSONParser parser = new JSONParser();
		//create the instance of List<SMARTTASKSEQINSTRUCTIONSBEAN> for getting the list
		List<SMARTTASKSEQINSTRUCTIONSBEAN> ObjSmartTaskSeqInstructionsBeanList = new ArrayList<SMARTTASKSEQINSTRUCTIONSBEAN>();
		try{
			//parse the json into object
			Object obj = parser.parse(GoJSJsonData);
			//cast the object into JSONObject
			JSONObject jsonObject = (JSONObject) obj;
			//create the array of JSONArray for getting the main node from flow chart json
			JSONArray nodedataObj = (JSONArray) jsonObject.get(GLOBALCONSTANT.nodeDataArray);
			JSONArray linkdataObj = (JSONArray) jsonObject.get(GLOBALCONSTANT.linkDataArray);
			// cretae a hash map for gotag list
			Map<String, String> GOTOTagListHM = new HashMap<String, String>();
			for(int gotonodeindex=GLOBALCONSTANT.INT_ZERO; gotonodeindex < nodedataObj.size(); gotonodeindex++)
			{
				JSONObject nodeJsonObj = (JSONObject) nodedataObj.get(gotonodeindex);
				//get the sequence value from json
				String key =  (String) nodeJsonObj.get(GLOBALCONSTANT.key);
				String value = (String) nodeJsonObj.get(GLOBALCONSTANT.text);
				String category = (String) nodeJsonObj.get(GLOBALCONSTANT.Category);
				if(category.equalsIgnoreCase(GLOBALCONSTANT.GENERALTAG) || category.equalsIgnoreCase(GLOBALCONSTANT.End) || (key.length()>GLOBALCONSTANT.INT_TWO))
				{
					GOTOTagListHM.put(key, value);
				}
			}
			//iterate the loop size of the jsonarray
             int sequenceNumber = GLOBALCONSTANT.INT_ZERO;
			for(int nodeindex=GLOBALCONSTANT.INT_ZERO; nodeindex < nodedataObj.size(); nodeindex++){

				sequenceNumber = sequenceNumber + GLOBALCONSTANT.IntOne;
				SMARTTASKSEQINSTRUCTIONSBEAN ObjSmartTaskSeqInstructionBean=new SMARTTASKSEQINSTRUCTIONSBEAN();
				JSONObject nodeJsonObj = (JSONObject) nodedataObj.get(nodeindex);
				//get the sequence value from json
				String key =  (String) nodeJsonObj.get(GLOBALCONSTANT.key);
				if(key.length()>GLOBALCONSTANT.INT_TWO)
				{
					sequenceNumber = sequenceNumber - GLOBALCONSTANT.IntOne;
					continue;
				}
				ObjSmartTaskSeqInstructionBean.setExecution_Sequence(String.valueOf(sequenceNumber));

				String Category=GLOBALCONSTANT.BlankString;
				try{
					//get the figure from json
					Category=(String) nodeJsonObj.get(GLOBALCONSTANT.Category);
					if(GLOBALCONSTANT.Rectangle.equalsIgnoreCase(Category)){
						//set the Execution into type when figure will not rectangle
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Execution);
						ObjSmartTaskSeqInstructionBean.setExecution_Name(nodeJsonObj.get(GLOBALCONSTANT.ExecutionName).toString());

					}
					else if(GLOBALCONSTANT.COMMENTS.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setComments(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						ObjSmartTaskSeqInstructionBean.setAction_Name(Category);
					}
					else if(GLOBALCONSTANT.DISPLAY.equalsIgnoreCase(Category) && key.length()<GLOBALCONSTANT.INT_THREE ){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setMessage(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						ObjSmartTaskSeqInstructionBean.setAction_Name(Category);
					}
					else if(GLOBALCONSTANT.IFERROR.equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.text).toString())){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) && (linkobj.get(GLOBALCONSTANT.to).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString()+GLOBALCONSTANT.Stringpointone)) && (linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)) && (linkobj.get(GLOBALCONSTANT.toPort).toString().equalsIgnoreCase(GLOBALCONSTANT.L)) )
							{
								ObjSmartTaskSeqInstructionBean.setMessage(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString()));
							}
							else if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString()+GLOBALCONSTANT.Stringpointone)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString())); 
							}			    		 
						}
					}
					else if(GLOBALCONSTANT.IFEXIT.equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.text).toString())){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) && (linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString())); 
							}		    		 
						}			    	 
					}
					else if(GLOBALCONSTANT.Diamond.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						String IfReturnCodeValue = nodeJsonObj.get(GLOBALCONSTANT.text).toString();
						ObjSmartTaskSeqInstructionBean.setAction_Name(IfReturnCodeValue.substring(GLOBALCONSTANT.INT_ZERO, GLOBALCONSTANT.INT_FOURTEEN));

						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) &&
									(linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString()));
								String ReturnCodeValue = GLOBALCONSTANT.BlankString;
								try{
									ReturnCodeValue = IfReturnCodeValue.substring(GLOBALCONSTANT.INT_FOURTEEN);
								}catch(Exception e){}
								ObjSmartTaskSeqInstructionBean.setReturn_Code_Value(ReturnCodeValue);
							}		    		 
						}
					}
					else if(GLOBALCONSTANT.CUSTOMEXECUTION.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setAction_Name(Category.toString());
						ObjSmartTaskSeqInstructionBean.setCustom_Execution(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
					}
					else if(GLOBALCONSTANT.GOTOTAG.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());

						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) &&
									(linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString())); 
							}		    		 
						}
					}
					else if(GLOBALCONSTANT.GENERALTAG.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.Category).toString());
						ObjSmartTaskSeqInstructionBean.setExecution_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
					}
					else {
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.Action);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
					}
				}
				catch(Exception objEx){
					throw new JASCIEXCEPTION(objEx.getMessage());
				}
				ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Name(StrExecutionSequenceName_ForJson);
				ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type_Description(nodeJsonObj.get(GLOBALCONSTANT.ToolTipText).toString());
				//Add the data into list from SMARTTASKSEQINSTRUCTIONSBEAN
				ObjSmartTaskSeqInstructionsBeanList.add(ObjSmartTaskSeqInstructionBean);
				//StrExecutionSequenceName_ForJson=GLOBALCONSTANT.BlankString;
			}
		}catch(Exception e){}
		return ObjSmartTaskSeqInstructionsBeanList;
	}

	/**
	 * @author Shailendra Rajput
	 * @Date Jun 23, 2015
	 * @Description:This Function is used to get the json form flowchart and create the sequence view
	 * @return List<SMARTTASKSEQINSTRUCTIONSBEAN>
	 * @throws JASCIEXCEPTION
	 */

	public List<SMARTTASKSEQINSTRUCTIONSBEAN> createDataForSequenceView(){


		//create the instance of JSONParser
		JSONParser parser = new JSONParser();
		//create the instance of List<SMARTTASKSEQINSTRUCTIONSBEAN> for getting the list
		List<SMARTTASKSEQINSTRUCTIONSBEAN> ObjSmartTaskSeqInstructionsBeanList = new ArrayList<SMARTTASKSEQINSTRUCTIONSBEAN>();
		try{
			//parse the json into object
			Object obj = parser.parse(GoJSJsonData);

			//cast the object into JSONObject
			JSONObject jsonObject = (JSONObject) obj;

			//create the array of JSONArray for getting the main node from flow chart json
			JSONArray nodedataObj = (JSONArray) jsonObject.get(GLOBALCONSTANT.nodeDataArray);
			JSONArray linkdataObj = (JSONArray) jsonObject.get(GLOBALCONSTANT.linkDataArray);

			// cretae a hash map for gotag list
			Map<String, String> GOTOTagListHM = new HashMap<String, String>();
			for(int gotonodeindex= GLOBALCONSTANT.INT_ZERO; gotonodeindex < nodedataObj.size(); gotonodeindex++)
			{

				JSONObject nodeJsonObj = (JSONObject) nodedataObj.get(gotonodeindex);
				//get the sequence value from json
				String key =  (String) nodeJsonObj.get(GLOBALCONSTANT.key);
				String value = (String) nodeJsonObj.get(GLOBALCONSTANT.text);
				String category = (String) nodeJsonObj.get(GLOBALCONSTANT.Category);

				if(category.equalsIgnoreCase(GLOBALCONSTANT.GENERALTAG) || category.equalsIgnoreCase(GLOBALCONSTANT.End) || (key.length()>2))
				{
					GOTOTagListHM.put(key, value);
				}
			}
			//iterate the loop size of the jsonarray
            int sequenceNumber = GLOBALCONSTANT.INT_ZERO;
			for(int nodeindex=GLOBALCONSTANT.INT_ZERO; nodeindex < nodedataObj.size(); nodeindex++){

				sequenceNumber = sequenceNumber + GLOBALCONSTANT.IntOne;
				SMARTTASKSEQINSTRUCTIONSBEAN ObjSmartTaskSeqInstructionBean=new SMARTTASKSEQINSTRUCTIONSBEAN();
				JSONObject nodeJsonObj = (JSONObject) nodedataObj.get(nodeindex);
				//get the sequence value from json
				String key =  (String) nodeJsonObj.get(GLOBALCONSTANT.key);
				if(key.length()>GLOBALCONSTANT.INT_TWO)
				{
					sequenceNumber = sequenceNumber - GLOBALCONSTANT.IntOne;
					continue;
				}
				ObjSmartTaskSeqInstructionBean.setExecution_Sequence(String.valueOf(sequenceNumber));

				String Category=GLOBALCONSTANT.BlankString;

				try{
					//get the figure from json
					Category=(String) nodeJsonObj.get(GLOBALCONSTANT.Category);
					if(GLOBALCONSTANT.Rectangle.equalsIgnoreCase(Category)){
						//set the Execution into type when figure will not rectangle
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ExecutionFULL);
						ObjSmartTaskSeqInstructionBean.setExecution_Name(nodeJsonObj.get(GLOBALCONSTANT.ExecutionName).toString());

					}
					else if(GLOBALCONSTANT.COMMENTS.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setComments(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						ObjSmartTaskSeqInstructionBean.setAction_Name(Category);
					}
					else if(GLOBALCONSTANT.DISPLAY.equalsIgnoreCase(Category) && key.length()<3 ){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setMessage(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						ObjSmartTaskSeqInstructionBean.setAction_Name(Category);
					}
					else if(GLOBALCONSTANT.IFERROR.equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.text).toString())){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) && (linkobj.get(GLOBALCONSTANT.to).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString()+GLOBALCONSTANT.Stringpointone)) && (linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)) && (linkobj.get(GLOBALCONSTANT.toPort).toString().equalsIgnoreCase(GLOBALCONSTANT.L)) )
							{
								ObjSmartTaskSeqInstructionBean.setMessage(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString()));

							}
							else if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString()+GLOBALCONSTANT.Stringpointone)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString())); 
							}			    		 
						}
					}
					else if(GLOBALCONSTANT.IFEXIT.equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.text).toString())){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) && (linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString())); 
							}		    		 
						}			    	 
					}
					else if(GLOBALCONSTANT.Diamond.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						String IfReturnCodeValue = nodeJsonObj.get(GLOBALCONSTANT.text).toString();
						ObjSmartTaskSeqInstructionBean.setAction_Name(IfReturnCodeValue.substring(GLOBALCONSTANT.INT_ZERO, GLOBALCONSTANT.INT_FOURTEEN));

						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) &&
									(linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString()));
								String ReturnCodeValue = GLOBALCONSTANT.BlankString;
								try{
									ReturnCodeValue = IfReturnCodeValue.substring(GLOBALCONSTANT.INT_FOURTEEN);
								}catch(Exception e){}
								ObjSmartTaskSeqInstructionBean.setReturn_Code_Value(ReturnCodeValue);

							}		    		 
						}
					}
					else if(GLOBALCONSTANT.CUSTOMEXECUTION.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setAction_Name(Category.toString());
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type_Description(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
					}
					else if(GLOBALCONSTANT.GOTOTAG.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());

						JSONObject linkobj = null;
						for(int linkindex = GLOBALCONSTANT.INT_ZERO; linkindex < linkdataObj.size();linkindex++)
						{
							linkobj = (JSONObject) linkdataObj.get(linkindex);

							if((linkobj.get(GLOBALCONSTANT.from).toString().equalsIgnoreCase(nodeJsonObj.get(GLOBALCONSTANT.key).toString())) &&
									(linkobj.get(GLOBALCONSTANT.fromPort).toString().equalsIgnoreCase(GLOBALCONSTANT.R)))
							{
								ObjSmartTaskSeqInstructionBean.setGoto_Tag(GOTOTagListHM.get(linkobj.get(GLOBALCONSTANT.to).toString())); 

							}		    		 
						}
					}
					else if(GLOBALCONSTANT.GENERALTAG.equalsIgnoreCase(Category)){
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.Category).toString());
						ObjSmartTaskSeqInstructionBean.setExecution_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
					}
					else {
						//set the action into type when figure is action shape
						ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type(GLOBALCONSTANT.ActionFull);
						ObjSmartTaskSeqInstructionBean.setAction_Name(nodeJsonObj.get(GLOBALCONSTANT.text).toString());
					}
				}
				catch(Exception objEx){
					throw new JASCIEXCEPTION(objEx.getMessage());
				}
				ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Type_Description(nodeJsonObj.get(GLOBALCONSTANT.ToolTipText).toString());
				ObjSmartTaskSeqInstructionBean.setExecution_Sequence_Name(StrExecutionSequenceName_ForJson);
				//Add the data into list from SMARTTASKSEQINSTRUCTIONSBEAN
				ObjSmartTaskSeqInstructionsBeanList.add(ObjSmartTaskSeqInstructionBean);
			}
		}catch(Exception e){}
		return ObjSmartTaskSeqInstructionsBeanList;
	}

	/** 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_SEQ_INSTRUCTIONS table on the behalf of Parameters for Sequence View.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @return List<SMARTTASKSEQINSTRUCTIONS>
	 * @throws JASCIEXCEPTION
	 * @throws IOException 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_getSTSeqInstructions, method = RequestMethod.POST)
	public @ResponseBody List<SMARTTASKSEQINSTRUCTIONSBEAN> getSequenceViewData( HttpServletResponse response)  throws JASCIEXCEPTION, IOException {		

		/**There is calling sequence view function when webapi hit*/
		return createDataForSequenceView();
	}

	/**
	 * @author Shailendra Rajput
	 * @Date july 18, 2015
	 * @Description:It  is used to check the Execution Sequence Name is exist or not  in the table SMART_TASK_SEQ_HEADER table on the behalf of Parameters from Task Panel.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @return List<SMARTTASKSEQINSTRUCTIONS>
	 * @throws JASCIEXCEPTION
	 */

	@Transactional
	@RequestMapping(value =  GLOBALCONSTANT.SmartTaskConfigurator_Restfull_existExecutionSequenceNameSTSH, method = RequestMethod.POST)
	public @ResponseBody boolean getExistExecutionSequenceNameSTSH(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue) String Execution_Sequence_Name,@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_Menu_Option_JspPageValue) String Menu_Option)  throws JASCIEXCEPTION {

		/** Create the instance of SMARTTASKSEQHEADERS*/
		List<SMARTTASKSEQHEADERS> ObjSmartTaskSeqHeaders=null;

		ObjSmartTaskSeqHeaders = objSmartTaskConfiguratorDao.getExistExecutionSequenceNameinSTSH(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),Execution_Sequence_Name); 

		List<MENUOPTIONS> ObjMenuOption=null;

		ObjMenuOption = objSmartTaskConfiguratorDao.getExistMenuOptioninSTSH(Menu_Option); 


		/**Check the list size is more than zero*/
		if(ObjSmartTaskSeqHeaders.size()>GLOBALCONSTANT.INT_ZERO){

			return true;
		}
		else{
			if(ObjMenuOption.size()>GLOBALCONSTANT.INT_ZERO){
				return true;	
			}
			else {
				return false;
			}
		}

	}

	/**
	 * @author Shailendra Rajput
	 * @Date july 19, 2015
	 * @Description:It  is used to set the flow chart json from Smart Task Maintenance page.
	 * @param Sequence_View_Json
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value =  GLOBALCONSTANT.SmartTaskConfigurator_Restfull_SmartTaskConfiguratorSequenceViewData, method = RequestMethod.POST)
	public void getSequenceViewJsonData(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_JsonSequenceView_JspPageValue) String Sequence_View_Json)  throws JASCIEXCEPTION {

		/** Create the instance of SMARTTASKSEQHEADERS*/
		//System.out.println(Sequence_View_Json);
		GoJSJsonData = Sequence_View_Json;

	}
	
	/**
	 * @author Shailendra Rajput
	 * @Date August 21, 2015
	 * @Description:It  is used to set the sequence name from Smart Task Maintenance page.
	 * @param Sequence_name
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value =  GLOBALCONSTANT.SmartTaskConfigurator_Restfull_SmartTaskConfiguratorSequenceName, method = RequestMethod.POST)
	public void setSequenceName(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_JsonSequenceNAME_JspPageValue) String Sequence_Name)  throws JASCIEXCEPTION {
		/** Create the instance of SMARTTASKSEQHEADERS*/
		StrExecutionSequenceName_ForJson = Sequence_Name;
	}

	/** 
	 * @author Shailendra Rajput
	 * @Date Aug 12, 2015
	 * @Description:It  is used to set the page Name when click on back button into configurator iframe.
	 * @param Sequence_View_Json
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value =  GLOBALCONSTANT.SmartTaskConfigurator_Restfull_SmartTaskConfiguratorPageNameBackButton, method = RequestMethod.POST)
	public @ResponseBody String backButtonClick()  throws JASCIEXCEPTION {
		/** Create the instance of SMARTTASKSEQHEADERS*/
		String pageNameSend=OBJConfiguratorBE.getStrPageName_S().toString();
		return pageNameSend;
	}
	
	/** 
	 * @author Shailendra Rajput
	 * @Date September 14, 2015
	 * @Description:It  is used to validate the custom execution.
	 * @param CustomExecutionPath
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 * @throws IOException 
	 */
	@Transactional
	@RequestMapping(value =  GLOBALCONSTANT.SmartTaskConfigurator_Restfull_SmartTaskConfiguratorValidateCustomExecution, method = RequestMethod.POST)
	public @ResponseBody boolean validateCustomExecution(@RequestParam(value=GLOBALCONSTANT.SmartTaskConfigurator_CustomExecutionPatValue) String CustomExecutionPath)  throws JASCIEXCEPTION, IOException {
		/** call method of CUSTOMEXECUTIONPATHVALIDATION class*/
		String []CustomExecutionClass = CustomExecutionPath.split(GLOBALCONSTANT.SLASHDOT);
		if(CustomExecutionClass.length > GLOBALCONSTANT.IntOne)
		{
		String CustomExecutionJarFilePath = GLOBALCONSTANT.ADDITIONALEXECUTIONFILEPATHNAME+CustomExecutionClass[CustomExecutionClass.length-  GLOBALCONSTANT.IntOne]+".jar";
		
		CUSTOMEXECUTIONPATHVALIDATION.addFile(CustomExecutionJarFilePath);
		try {
			@SuppressWarnings(GLOBALCONSTANT.Strunused)
			Constructor<?> cs = ClassLoader.getSystemClassLoader().loadClass(CustomExecutionPath).getConstructor(String.class);
		} catch (ClassNotFoundException e) {
			return false;
		} catch (NoSuchMethodException e) {
			return true;
		}
		}else
		{			
	        return objSmartTaskConfiguratorDao.validateCustomExecution(CustomExecutionPath);
		}		
		return true;
	}
	
	/**
	 * @author Shailendra Rajput
	 * @Date Aug,12 2015
	 * @Description:it is used to set the parameter when we click on Add Button on SmartTaskSearchLookup Screen.
	 * @param StrPageName
	 * @return void	
	 */
	public void setPageName(String StrPageName){
        /**Set page name in ConfiguratorBE object*/
		OBJConfiguratorBE.setStrPageName_S(StrPageName);
	}
	
	/**
	 * @author Shailendra Kumar
	 * @Date Aug,12 2015
	 * @Description:it is used to set the next location of rectangle node.
	 * @param topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight
	 * @return nextnodelocation	
	 */
	public int setRectangleShapeNodeLocation(int topdistance, int lastnodehieght, int circleheight,int squareheight, int rectangleheight, int displayheight, int diamondheight){
		int nextnodelocation = GLOBALCONSTANT.INT_ZERO;
		if(lastnodehieght == circleheight)
		{		                        	
			nextnodelocation = topdistance - GLOBALCONSTANT.INT_TWELVE;
		}
		else if(lastnodehieght == squareheight)
		{		                        	
			nextnodelocation = topdistance +  GLOBALCONSTANT.IntZero;
		}
		else if(lastnodehieght == rectangleheight)
		{
			nextnodelocation = topdistance - GLOBALCONSTANT.INT_TEN;
		}
		else if(lastnodehieght == displayheight)
		{
			nextnodelocation = topdistance + GLOBALCONSTANT.IntZero;
		}
		else if(lastnodehieght == diamondheight)
		{		                        	
			nextnodelocation = topdistance - GLOBALCONSTANT.INT_TWELVE;
		}
		return nextnodelocation;
	}
	/**
	 * @author Shailendra Kumar
	 * @Date Aug,12 2015
	 * @Description:it is used to set the next location of Diamond node.
	 * @param topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight
	 * @return nextnodelocation	
	 */
	public int setDiamondShapeNodeLocation(int topdistance, int lastnodehieght, int circleheight,int squareheight, int rectangleheight, int displayheight, int diamondheight){
		int nextnodelocation = GLOBALCONSTANT.INT_ZERO;
		if(lastnodehieght == circleheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIVE;
		}
		else if(lastnodehieght == squareheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_TEN;
		}
		else if(lastnodehieght == rectangleheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIVE;
		}
		else if(lastnodehieght == displayheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIFTEEN;
		}
		else if(lastnodehieght == diamondheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIVE;
		}
		return nextnodelocation;
	}
	/**
	 * @author Shailendra Kumar
	 * @Date Aug,12 2015
	 * @Description:it is used to set the next location of Square node.
	 * @param topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight
	 * @return nextnodelocation	
	 */
	public int setSquareShapeNodeLocation(int topdistance, int lastnodehieght, int circleheight,int squareheight, int rectangleheight, int displayheight, int diamondheight){
		int nextnodelocation = GLOBALCONSTANT.INT_ZERO;
		if(lastnodehieght == circleheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_ZERO;
		}
		else if(lastnodehieght== squareheight)
		{		                        	
			nextnodelocation = topdistance;
		}
		else if(lastnodehieght== rectangleheight)
		{
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_ZERO;
		}
		else if(lastnodehieght== displayheight)
		{
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_ZERO;
		}
		else if(lastnodehieght== diamondheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIFTEEN;
		}
		return nextnodelocation;
	}
	/**
	 * @author Shailendra Kumar
	 * @Date Aug,12 2015
	 * @Description:it is used to set the next location of circle node.
	 * @param topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight
	 * @return nextnodelocation	
	 */
	public int setCircleShapeNodeLocation(int topdistance, int lastnodehieght, int circleheight,int squareheight, int rectangleheight, int displayheight, int diamondheight){
		int nextnodelocation = GLOBALCONSTANT.INT_ZERO;
		if(lastnodehieght == circleheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIVE;
		}
		else if(lastnodehieght == squareheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_TEN;
		}
		else if(lastnodehieght == rectangleheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIVE;
		}
		else if(lastnodehieght == displayheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIFTEEN;
		}
		else if(lastnodehieght == diamondheight)
		{		                        	
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_FIVE;
		}
		return nextnodelocation;
	}
	/**
	 * @author Shailendra Kumar
	 * @Date Aug,12 2015
	 * @Description:it is used to set the next location of display node.
	 * @param topdistance, lastnodehieght, circleheight, squareheight, rectangleheight, displayheight, diamondheight
	 * @return nextnodelocation	
	 */
	public int setDisplayShapeNodeLocation(int topdistance, int lastnodehieght, int circleheight,int squareheight, int rectangleheight, int displayheight, int diamondheight){
		int nextnodelocation = GLOBALCONSTANT.INT_ZERO;
		if(lastnodehieght == circleheight)
		{		                        	
			nextnodelocation = topdistance - GLOBALCONSTANT.INT_TWELVE;
		}
		else if(lastnodehieght == squareheight)
		{		                        	
			nextnodelocation = topdistance +  GLOBALCONSTANT.INT_ZERO;
		}
		else if(lastnodehieght == rectangleheight)
		{
			nextnodelocation = topdistance - GLOBALCONSTANT.INT_TEN;
		}
		else if(lastnodehieght == displayheight)
		{
			nextnodelocation = topdistance + GLOBALCONSTANT.INT_ZERO;
		}
		else if(lastnodehieght == diamondheight)
		{		                        	
			nextnodelocation = topdistance - GLOBALCONSTANT.INT_TWELVE;
		}		
		return nextnodelocation;
	}
}