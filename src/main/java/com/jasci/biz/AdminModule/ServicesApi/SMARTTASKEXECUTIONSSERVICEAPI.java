/**

Description This class is used to make restfull service for SMART TASK CONFIGURATOR'S SCREENS
Created By Shailendra Rajput
Created Date May 15 2014
*/
package com.jasci.biz.AdminModule.ServicesApi;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.CONFIGURATORBE;
import com.jasci.biz.AdminModule.be.GOJSJSONDATABE;
import com.jasci.biz.AdminModule.be.GOJSLINKDATABE;
import com.jasci.biz.AdminModule.be.GOJSNODEDATABE;
import com.jasci.biz.AdminModule.be.GOJSPROPERTIESBE;
import com.jasci.biz.AdminModule.model.LANGUAGEPK;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONSBEAN;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONSPK;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERSBEAN;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERSPK;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONSBEAN;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONSPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.WORKFLOWSTEPS;

import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.CUSTOMEXECUTIONPATHVALIDATION;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.GENERAL_CODES;
import com.jasci.exception.JASCIEXCEPTION;

//import com.jasci.biz.AdminModule.be.SMARTTASKCONFIGURATORBE;
//import com.jasci.biz.AdminModule.dao.ISMARTTASKCONFIGURATORDAO;
//import com.jasci.biz.AdminModule.service.ISMARTTASKEXECUTIONSSERVICE;

import com.jasci.biz.AdminModule.be.SMARTTASKEXECUTIONSBE;
import com.jasci.biz.AdminModule.controller.SMARTTASKEXECUTIONSCONTROLLER;
import com.jasci.biz.AdminModule.dao.ISMARTTASKEXECUTIONSDAO;
import com.jasci.biz.AdminModule.service.ISMARTTASKEXECUTIONSSERVICE;

@Controller
public class SMARTTASKEXECUTIONSSERVICEAPI {

	private static Logger log = LoggerFactory.getLogger(SMARTTASKEXECUTIONSSERVICEAPI.class.getName());

	/** It is used to get the value form Common Session **/
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;

	/**
	 * It is use to call the function which is implemented in
	 * SMARTTASKCONFIGURATORDAOIMPL
	 **/
	@Autowired
	ISMARTTASKEXECUTIONSDAO objSmartTaskExecutionsDao;

	/**
	 * Make the instance of ISMARTTASKEXECUTIONSSERVICE for calling service
	 * method
	 */
	@Autowired
	private ISMARTTASKEXECUTIONSSERVICE ObjSmartTaskExecutionsService;

	@Autowired
	CONFIGURATORBE OBJConfiguratorBE;

	/** This is used to set the value globally */
	String StrExecutionSequenceName_G = GLOBALCONSTANT.BlankString;
	String StrExecutionSequenceName_ForJson = GLOBALCONSTANT.BlankString;
	String GoJSJsonData = GLOBALCONSTANT.BlankString;

	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskExecutions_Restfull_Search, method = RequestMethod.POST)
	public @ResponseBody List<SMARTTASKEXECUTIONSBEAN> getList(
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_Application) String StrApplication,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_ExecutionGroup) String StrExecutionGroup,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_ExecutionType) String StrExecutionType,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_ExecutionDevice) String StrExecutionDevice,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_Execution) String StrExecutionName,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_Description) String StrDescription20,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_Tenant) String StrTenant,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Restfull_Buttons) String StrButton)
					throws JASCIEXCEPTION {

		List<Object> ListSmartTaskExecutionsAll = null;

		if (GLOBALCONSTANT.BlankString.equals(StrTenant)) {
			StrTenant = objCommonsessionbe.getTenant();
		}
		String StrCompany = objCommonsessionbe.getCompany();
		ListSmartTaskExecutionsAll = objSmartTaskExecutionsDao.getList(StrTenant, StrCompany, StrApplication,
				StrExecutionGroup, StrExecutionType, StrExecutionDevice, StrExecutionName, StrDescription20, StrButton);

		List<SMARTTASKEXECUTIONSBEAN> smartTaskExecutionsBeanList = new ArrayList<SMARTTASKEXECUTIONSBEAN>();

		for (Iterator<Object> ExecutionsListLength = ListSmartTaskExecutionsAll.iterator(); ExecutionsListLength.hasNext();) {

			SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean = new SMARTTASKEXECUTIONSBEAN();
			Object[] ObjectLocObj = (Object[]) ExecutionsListLength.next();

			try {
				smartTaskExecutionsBean.setTenant_Id(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setTenant_Id(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setTenant_Id(GLOBALCONSTANT.BlankString);
			}

			try {
				smartTaskExecutionsBean.setCompany_Id(ObjectLocObj[GLOBALCONSTANT.INT_ONE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setCompany_Id(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setCompany_Id(GLOBALCONSTANT.BlankString);
			}

			try {
				smartTaskExecutionsBean.setExecution_Type(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Type(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Type(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Type_Description(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Type_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Type_Description(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Device(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Device(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Device(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Device_Description(ObjectLocObj[GLOBALCONSTANT.INT_FIVE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Device_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Device_Description(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Group(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Group(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Group(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Group_Description(ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Group_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Group_Description(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Name(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Name(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Name(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setDescription50(ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setDescription50(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setDescription50(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setButton(ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setButton(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setButton(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setApplication_Id(ObjectLocObj[GLOBALCONSTANT.INT_ELEVEN].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setApplication_Id(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setApplication_Id(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setApplication_Id_Description(ObjectLocObj[GLOBALCONSTANT.INT_TWELVE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setApplication_Id_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setApplication_Id_Description(GLOBALCONSTANT.BlankString);
			}
			smartTaskExecutionsBeanList.add(smartTaskExecutionsBean);
		}
		return smartTaskExecutionsBeanList;
	}

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	public @ResponseBody List<SMARTTASKEXECUTIONSBEAN> getDisplayAll() throws JASCIEXCEPTION {

		List<Object> ListSmartTaskExecutionsAll = null;
		ListSmartTaskExecutionsAll = objSmartTaskExecutionsDao.getDisplayAll(objCommonsessionbe.getTenant(), objCommonsessionbe.getCompany());

		List<SMARTTASKEXECUTIONSBEAN> smartTaskExecutionsBeanList = new ArrayList<SMARTTASKEXECUTIONSBEAN>();

		for (Iterator<Object> ExecutionsListLength = ListSmartTaskExecutionsAll.iterator(); ExecutionsListLength.hasNext();) {
			
			SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean = new SMARTTASKEXECUTIONSBEAN();
			Object[] ObjectLocObj = (Object[]) ExecutionsListLength.next();

			try {
				smartTaskExecutionsBean.setTenant_Id(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setTenant_Id(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setTenant_Id(GLOBALCONSTANT.BlankString);
			}

			try {
				smartTaskExecutionsBean.setCompany_Id(ObjectLocObj[GLOBALCONSTANT.INT_ONE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setCompany_Id(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setCompany_Id(GLOBALCONSTANT.BlankString);
			}

			try {
				smartTaskExecutionsBean.setExecution_Type(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Type(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Type(GLOBALCONSTANT.BlankString);
			}

			try {
				smartTaskExecutionsBean.setExecution_Type_Description(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Type_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Type_Description(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Device(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Device(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Device(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Device_Description(ObjectLocObj[GLOBALCONSTANT.INT_FIVE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Device_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Device_Description(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Group(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Group(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Group(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Group_Description(ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Group_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Group_Description(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setExecution_Name(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setExecution_Name(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setExecution_Name(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setDescription50(ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setDescription50(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setDescription50(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setButton(ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setButton(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setButton(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setApplication_Id(ObjectLocObj[GLOBALCONSTANT.INT_ELEVEN].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setApplication_Id(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setApplication_Id(GLOBALCONSTANT.BlankString);
			}
			try {
				smartTaskExecutionsBean.setApplication_Id_Description(ObjectLocObj[GLOBALCONSTANT.INT_TWELVE].toString());
			} catch (ArrayIndexOutOfBoundsException obj_e) {
				smartTaskExecutionsBean.setApplication_Id_Description(GLOBALCONSTANT.BlankString);
			} catch (Exception obj_e) {
				smartTaskExecutionsBean.setApplication_Id_Description(GLOBALCONSTANT.BlankString);
			}
			smartTaskExecutionsBeanList.add(smartTaskExecutionsBean);
		}
		return smartTaskExecutionsBeanList;
	}


	
	@Transactional
	public @ResponseBody List<SMARTTASKEXECUTIONS> getSmartTaskExecutionData(
			@RequestParam(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Tenant) String StrTenant,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_Company) String StrCompany,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionGroup) String StrExecutionGroup,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionType) String StrExecutionType,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_ExecutionDevice) String StrExecutionDevice)
					throws JASCIEXCEPTION {

				List<SMARTTASKEXECUTIONS> ObjSmartTaskExecutions = null;
				ObjSmartTaskExecutions = objSmartTaskExecutionsDao.getSmartTaskExecutionData(StrTenant, StrCompany, StrExecutionGroup, StrExecutionType, StrExecutionDevice);
		return ObjSmartTaskExecutions;
	}

	@Transactional
	public @ResponseBody List<GENERAL_CODES> getGeneralCodeDropDown(
			@RequestParam(value = GLOBALCONSTANT.SmartTaskConfigurator_Restfull_GeneralCodeId) String StrGeneralCodeId)
					throws JASCIEXCEPTION {

		List<Object> ObjGenearalCodes = objSmartTaskExecutionsDao.getGeneralCodeDropDown(objCommonsessionbe.getTenant(),
				objCommonsessionbe.getCompany(), StrGeneralCodeId);

		List<GENERAL_CODES> Obj_LookupGeneralCodeList = new ArrayList<GENERAL_CODES>();
		if (!ObjGenearalCodes.isEmpty()) {
			Iterator<Object> Obj_LookupGeneralCodeItr = ObjGenearalCodes.iterator();
			while (Obj_LookupGeneralCodeItr.hasNext()) {
				Object[] GCObject = (Object[]) Obj_LookupGeneralCodeItr.next();
		
				GENERAL_CODES Obj_GeneralCode = new GENERAL_CODES();
				try {
							Obj_GeneralCode.setGeneralCode(GCObject[GLOBALCONSTANT.INT_ZERO].toString());
				} catch (ArrayIndexOutOfBoundsException obj_e) {
				} catch (Exception ObjException) {
				}
				try {
							Obj_GeneralCode.setDescription20(GCObject[GLOBALCONSTANT.IntOne].toString());
				} catch (ArrayIndexOutOfBoundsException obj_e) {
				} catch (Exception ObjException) {
				}
				try {
							Obj_GeneralCode.setDescription50(GCObject[GLOBALCONSTANT.INT_TWO].toString());
				} catch (ArrayIndexOutOfBoundsException obj_e) {
				} catch (Exception ObjException) {
				}
				try {
							Obj_GeneralCode.setHelpline(GCObject[GLOBALCONSTANT.INT_THREE].toString());
				} catch (ArrayIndexOutOfBoundsException obj_e) {
				} catch (Exception ObjException) {
				}
				Obj_LookupGeneralCodeList.add(Obj_GeneralCode);
			}
		}
		return Obj_LookupGeneralCodeList;
	}
    

	@Transactional
	public @ResponseBody SMARTTASKEXECUTIONSBE getSmartTaskExecutionsLabels(String StrLanguage) throws JASCIEXCEPTION {
		return ObjSmartTaskExecutionsService.getSmartTaskExecutionsLabels(objCommonsessionbe.getCurrentLanguage());
	}

	public static String trimValues(String Value) {
		if (Value == null) {
			return null;
		} else {
			String CheckValue = Value.trim();
			if (CheckValue.length() < GLOBALCONSTANT.IntOne) {
				return null;
			} else {
				return CheckValue;
			}
		}
	}

	public String ConvertDate(String dateInString, String StrInFormat, String StrOutFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
		DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
		String StrFormattedString = dateInString;
		try {
			Date date = formatter.parse(dateInString);
			StrFormattedString = dateFormat.format(date);
		} catch (Exception e) {
		}
		return StrFormattedString;
	}

	public String getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
		/** declare and intialize team member variable */
		String TeamMemberName = GLOBALCONSTANT.BlankString;
		List<TEAMMEMBERS> objTeammembers = objSmartTaskExecutionsDao.getTeamMemberName(Tenant, TeamMember);
		TeamMemberName = objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName() + GLOBALCONSTANT.Single_Space
				+ objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
		return TeamMemberName;
	}

	@Transactional
	public @ResponseBody String backButtonClick() throws JASCIEXCEPTION {
		// String pageNameSend=OBJConfiguratorBE.getStrPageName_S().toString();
		// return pageNameSend;
		return "";
	}

	public void setPageName(String StrPageName) {
		// OBJConfiguratorBE.setStrPageName_S(StrPageName);
	}

	public Boolean addExecutions(SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean) {
		SMARTTASKEXECUTIONS smartTaskExecutions = new SMARTTASKEXECUTIONS();
		SMARTTASKEXECUTIONSPK smartTaskExecutionsPk = new SMARTTASKEXECUTIONSPK();
		Boolean Status = false;
		Date LastActivitydate = null;
		
		try {
					SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.yyyyMMdd_Format_v1);
					LastActivitydate = formatter.parse(smartTaskExecutionsBean.getLast_Activity_Date());

					smartTaskExecutionsPk.setTenant_Id(smartTaskExecutionsBean.getTenant_Id());
					smartTaskExecutionsPk.setCompany_Id(smartTaskExecutionsBean.getCompany_Id());
					smartTaskExecutionsPk.setExecution_Name(smartTaskExecutionsBean.getExecution_Name());

					smartTaskExecutions.setApplication_Id(smartTaskExecutionsBean.getApplication_Id());
					smartTaskExecutions.setExecution_Group(smartTaskExecutionsBean.getExecution_Group());
					smartTaskExecutions.setDescription20(smartTaskExecutionsBean.getDescription20());
					smartTaskExecutions.setDescription50(smartTaskExecutionsBean.getDescription50());
					smartTaskExecutions.setHelp_Line(smartTaskExecutionsBean.getHelp_Line()); 
					smartTaskExecutions.setExecution_Type(smartTaskExecutionsBean.getExecution_Type());
					smartTaskExecutions.setExecution_Device(smartTaskExecutionsBean.getExecution_Device());
					smartTaskExecutions.setButton(smartTaskExecutionsBean.getButton());
					smartTaskExecutions.setExecution(smartTaskExecutionsBean.getExecution_Path());
					smartTaskExecutions.setLast_Activity_Date(LastActivitydate);
					smartTaskExecutions.setLast_Activity_Team_Member(smartTaskExecutionsBean.getLast_Activity_Team_Member());
					smartTaskExecutions.setButton_Name(smartTaskExecutionsBean.getButton_Name());
					smartTaskExecutions.setId(smartTaskExecutionsPk);
					Status = objSmartTaskExecutionsDao.addExecutions(smartTaskExecutions);
			
		} catch (ParseException e) {
			log.error("addExecutions ParseException : " + e.getMessage());
			e.printStackTrace();
		} catch (JASCIEXCEPTION objJasciException) {
			log.error("addExecutions exception : " + objJasciException.getMessage());
		}
		return Status;
	}

	@Transactional
	public @ResponseBody Boolean deleteExecutions(
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Tenant_Id_JspPageValue) String StrTenantId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Company_id_JspPageValue) String StrCompanyId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Execution_Name_JspPageValue) String StrExecutionName) {

				Boolean delete_status = false;
				SMARTTASKEXECUTIONS smartTaskExecutions = new SMARTTASKEXECUTIONS();
				SMARTTASKEXECUTIONSPK smartTaskExecutionsPk = new SMARTTASKEXECUTIONSPK();
				smartTaskExecutionsPk.setTenant_Id(StrTenantId.trim());
				smartTaskExecutionsPk.setCompany_Id(StrCompanyId.trim());
				smartTaskExecutionsPk.setExecution_Name(StrExecutionName.trim());
				smartTaskExecutions.setId(smartTaskExecutionsPk);

				try {
						delete_status = objSmartTaskExecutionsDao.deleteExecutions(smartTaskExecutions);

				} catch (JASCIEXCEPTION objJasciException) {
						log.error("IsExecutionExist exception : " + objJasciException.getMessage());
				}
				return delete_status;
		}

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskExecutions_Restfull_IsExist, method = RequestMethod.POST)
	public @ResponseBody Boolean IsExecutionExist(
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Tenant_Id) String StrTenantId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Company_Id) String StrCompanyId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Execution_Name) String StrExecutionName) {

			List<SMARTTASKEXECUTIONS> SmartTaskExecutionsList = null;
			try {
						SmartTaskExecutionsList = objSmartTaskExecutionsDao.isExecutionExist(StrCompanyId, StrTenantId, StrExecutionName);
			} catch (JASCIEXCEPTION objJasciException) {
				log.error("IsExecutionExist exception : " + objJasciException.getMessage());
			}

			if (SmartTaskExecutionsList != null && !SmartTaskExecutionsList.isEmpty()) {
				return true;
			} else {
				return false;
			}
		}

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.SmartTaskExecutions_IsSeqInstructionsExist, method = RequestMethod.POST)
	public @ResponseBody Boolean IsSeqInstructionExist(
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Tenant_Id) String StrTenantId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Company_Id) String StrCompanyId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Execution_Name) String StrExecutionName) {

			List<SMARTTASKSEQINSTRUCTIONS> SmartTaskSeqInstructionsList = null;
			try {
						SmartTaskSeqInstructionsList = objSmartTaskExecutionsDao.getSmartTaskSeqInsttructions(StrTenantId, 	StrCompanyId, StrExecutionName);
			} catch (JASCIEXCEPTION objJasciException) {
				log.error("IsExecutionExist exception : " + objJasciException.getMessage());
			}

			if (SmartTaskSeqInstructionsList != null && !SmartTaskSeqInstructionsList.isEmpty()) {
				return true;
			} else {
				return false;
			}
		}
	
	@Transactional
	public @ResponseBody List<SMARTTASKEXECUTIONS>  getExecutions(
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Tenant_Id_JspPageValue) String StrTenantId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Company_id_JspPageValue) String StrCompanyId,
			@RequestParam(value = GLOBALCONSTANT.SmartTaskExecution_Execution_Name_JspPageValue) String StrExecutionName) {

				List<SMARTTASKEXECUTIONS> SmartTaskExecutionsList = null;
				if (GLOBALCONSTANT.BlankString.equals(StrTenantId)) {
					StrTenantId = objCommonsessionbe.getTenant();
				}

				try {
						SmartTaskExecutionsList = objSmartTaskExecutionsDao.isExecutionExist(StrCompanyId, StrTenantId,	StrExecutionName);
				} catch (Exception ex) {
					log.error("getExecutions Exeception = " + ex.getMessage());
				}
				return SmartTaskExecutionsList;
			}
		
}
