/**

Date Developed :Jan 15 2015
Created by: Rahul kumar
Description :LOCATIONLABELPRINTSERVICEAPI Controller TO HANDLE THE REQUESTS FROM SCREENS
 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jasci.biz.AdminModule.dao.ILOCATIONLABELPRINTDAL;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARD;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.LOCATIONWIZARDBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class LOCATIONLABELPRINTSERVICEAPI {
	@Autowired
	private ILOCATIONLABELPRINTDAL objIlocationlabelprintdal;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	String FulfillmentCenterID=GLOBALCONSTANT.Blank;

	/**
	 * 
	 *@Description get general code from general code table base on general code id
	 *@Auther Rahul Kumar
	 *@Date Jan 15, 2015			
	 *@Return type List<GENERALCODES>
	 *@param Tenant
	 *@param Company
	 *@param GeneralCodeId
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationLabelPrint_Restfull_GeneralCode, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODES> getGeneralCode(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Company) String Company,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_GeneralCodeId) String GeneralCodeId) throws JASCIEXCEPTION {	

		return objIlocationlabelprintdal.getGeneralCode(Tenant, Company, GeneralCodeId);

	}


	/**
	 * 
	 *@Description get location name from table Locations 
	 *@Auther Rahul Kumar
	 *@Date Jan 19, 2015			
	 *@Return type List<LOCATION>
	 *@param Area
	 *@param Loaction
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationLabelPrint_Restfull_GetLocation, method = RequestMethod.POST)
	public @ResponseBody  List<LOCATION> getLocation(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String StrLocation,
			@RequestParam(value=GLOBALCONSTANT.fullfillmentCenter) String fullfillmentCenter)  throws JASCIEXCEPTION { 

		List<LOCATION> ObjLocation=null;
		OBJCOMMONSESSIONBE.setFulfillmentCenter(fullfillmentCenter);
		ObjLocation =objIlocationlabelprintdal.getLocation(OBJCOMMONSESSIONBE, StrArea, StrLocation);
		return ObjLocation;

	}

	
	

	/**
	 * 
	 *@Description get WizardControlNumber from table Locations 
	 *@Auther Rahul Kumar
	 *@Date Jan 20, 2015			
	 *@Return type List<LOCATION>
	 *@param WizardControlNumber
	 *@param StrLocation
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationLabelPrint_Restfull_GetWizardControlNumber, method = RequestMethod.POST)
	public @ResponseBody  List<Object> getWizardControlNumber(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_WizardControlNumber) String WizardControlNumber,
			@RequestParam(value=GLOBALCONSTANT.fullfillmentCenter) String fullfillmentCenter)  throws JASCIEXCEPTION { 

		List<LOCATION> ObjLocation=null;
		OBJCOMMONSESSIONBE.setFulfillmentCenter(fullfillmentCenter);
		ObjLocation =objIlocationlabelprintdal.getWizardControlNumber(OBJCOMMONSESSIONBE, WizardControlNumber);
		List<Object> Labletype=new ArrayList<Object>();
		/*if(ObjLocation.size()>0){
			Labletype.add(ObjLocation.get(0).getLocation_Label_Type().toLowerCase());
		}*/
		if(ObjLocation.size()>GLOBALCONSTANT.INT_ZERO){
			   if(ObjLocation.get(GLOBALCONSTANT.INT_ZERO).getLocation_Label_Type()!=null)
			   Labletype.add(ObjLocation.get(GLOBALCONSTANT.INT_ZERO).getLocation_Label_Type().toLowerCase());
			   else
			    Labletype.add(GLOBALCONSTANT.BlankString);
			    
			  }
		return Labletype;


	}



	/**
	 * 
	 * @Description get Location Wizard list from table wizard location
	 * @author Rahul Kumar
	 * @Date Jan 21, 2015
	 * @Return type List<LOCATIONWIZARDBE>
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationLabelPrint_Restfull_GetWizardLocationList, method = RequestMethod.GET)
	public @ResponseBody List<LOCATIONWIZARDBE> getLocationWizardList()throws JASCIEXCEPTION  {
		List<LOCATIONWIZARD> objLocationwizards=null;
		if(GLOBALCONSTANT.Blank.equalsIgnoreCase(FulfillmentCenterID)){
			return null;
		}
		List<LOCATIONWIZARDBE> arrObjLocationwizardbe=new ArrayList<LOCATIONWIZARDBE>();
		objLocationwizards=objIlocationlabelprintdal.getLocationWizardList(OBJCOMMONSESSIONBE,FulfillmentCenterID);

		for(LOCATIONWIZARD valueObjLocationwizards:objLocationwizards){
			LOCATIONWIZARDBE objLocationwizardbe=new LOCATIONWIZARDBE();
			objLocationwizardbe.setWizardControlNumber(String.valueOf(valueObjLocationwizards.getId().getWIZARD_CONTROL_NUMBER()));
			objLocationwizardbe.setWizardId(valueObjLocationwizards.getId().getWIZARD_ID());
			objLocationwizardbe.setDescription20(valueObjLocationwizards.getDESCRIPTION20());
			objLocationwizardbe.setDescription50(valueObjLocationwizards.getDESCRIPTION50());
			arrObjLocationwizardbe.add(objLocationwizardbe);

		}

		return arrObjLocationwizardbe;
	}

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.UrlsetFulfillmentId, method = RequestMethod.POST)
	public @ResponseBody Boolean setFulfillmentCenterID(@RequestParam(value=GLOBALCONSTANT.ffid) String ffid)throws JASCIEXCEPTION  {
		Boolean isStatus=false;
		List<LOCATIONWIZARD> objLocationwizards=null;
		objLocationwizards=objIlocationlabelprintdal.getLocationWizardList(OBJCOMMONSESSIONBE,ffid);
		if(objLocationwizards.size()>GLOBALCONSTANT.INT_ZERO){
		FulfillmentCenterID=ffid;
		isStatus=true;
		}else{
			isStatus=false;
			FulfillmentCenterID=GLOBALCONSTANT.Blank;
		}
		return isStatus;
	}



	/**
	 * 
	 *@Description get Location Label print type  from table Locations 
	 *@Auther Rahul Kumar
	 *@Date Jan 29, 2015			
	 *@Return type List<LOCATION>
	 *@param WizardControlNumber
	 *@param StrLocation
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationLabelPrint_Restfull_GetLocationLabelTypePrint, method = RequestMethod.POST)
	public @ResponseBody  List<Object> getLocationLabelTypePrint(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String Area,
			@RequestParam(value=GLOBALCONSTANT.fullfillmentCenter) String fullfillmentCenter)  throws JASCIEXCEPTION { 

		List<LOCATION> ObjLocation=new ArrayList<LOCATION>();
		OBJCOMMONSESSIONBE.setFulfillmentCenter(fullfillmentCenter);
		ObjLocation =objIlocationlabelprintdal.getLocationLabelTypePrint(OBJCOMMONSESSIONBE, Area);
		List<Object> Labletype=new ArrayList<Object>();
		if(ObjLocation.size()>GLOBALCONSTANT.INT_ZERO){

			Labletype.add(ObjLocation.get(GLOBALCONSTANT.INT_ZERO).getLocation_Label_Type().toLowerCase());
		}
		else{
			Labletype.add(GLOBALCONSTANT.BlankString);
		}
		return Labletype;

	}



	/**
	 * 
	 *@Description generate barcode label by area and And Location
	 *@Auther Rahul Kumar
	 *@Date Feb 03, 2015			
	 *@Return type List<LOCATION>
	 *@param Location
	 *@param StrLocation
	 *@return
	 *@throws JASCIEXCEPTION
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationLabelPrint_Restfull_Generate_Barcode_LocationLabel_Part1, method = RequestMethod.POST)
	public @ResponseBody  List<Object> createBarcodeLabel(
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String Area,
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String Location,
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Label_Type) String LabelType,
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Barcode_Type) String BarcodeType,
			HttpServletRequest request,
			HttpServletResponse response)  throws JASCIEXCEPTION { 

		
		try{
			
		}catch(Exception obj){

		}
		return null;

	}


	
	/**
	 * 
	 *@Description get location name from table Locations 
	 *@Auther sarvendra tyagi
	 *@Date feb 23, 2015			
	 *@Return type List<LOCATION>
	 *@param Area
	
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value =GLOBALCONSTANT.UrlRestGetLocationLabelRePrintGetLocation, method = RequestMethod.POST)
	public @ResponseBody  List<LOCATION> getLocationReprint(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,
			@RequestParam(value=GLOBALCONSTANT.fullfillmentCenter) String fullfillmentCenter)  throws JASCIEXCEPTION { 

		List<LOCATION> ObjLocation=null;
		OBJCOMMONSESSIONBE.setFulfillmentCenter(fullfillmentCenter);
		ObjLocation =objIlocationlabelprintdal.getLocationReprint(OBJCOMMONSESSIONBE, StrArea);
		return ObjLocation;

	}
	
	/**
	 * 
	 *@Description generate barcode label by Wizard control number
	 *@Auther Rahul Kumar
	 *@Date Feb 03, 2015			
	 *@Return type List<LOCATION>
	 *@param Location
	 *@param StrLocation
	 *@return
	 *@throws JASCIEXCEPTION
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationLabelPrint_Restfull_Generate_Barcode_LocationLabel_Part2, method = RequestMethod.POST)
	public @ResponseBody  List<Object> createBarcodeLabelbyWizardControlNumber(
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_WizardControlNumber) String WizardControlNumber,
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Label_Type) String LabelType,
			@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Barcode_Type) String BarcodeType,
			@RequestParam(value=GLOBALCONSTANT.fullfillmentCenter) String fullfillmentCenter,
			HttpServletRequest request,
			HttpServletResponse response)  throws JASCIEXCEPTION { 

		List<LOCATION> ObjLocation=null;
		OBJCOMMONSESSIONBE.setFulfillmentCenter(fullfillmentCenter);
		ObjLocation =objIlocationlabelprintdal.getWizardControlNumber(OBJCOMMONSESSIONBE, WizardControlNumber);
		
		for(LOCATION objLocation2:ObjLocation){
			try{
				
				objLocation2.getId().getArea();
				objLocation2.getId().getLocation();
		
			}catch(Exception obj){

			}
		}
		
		

		return null;

	}

	

}
