/**

Date Developed :Dec 12 2014
Created by: Sarvendra Tyagi
Description :MENUOPTIONS Controller TO HANDLE THE REQUESTS FROM SCREENS
 */

package com.jasci.biz.AdminModule.ServicesApi;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jasci.biz.AdminModule.dao.IMENUOPTIONSDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEOPTIONS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class MENUOPTIONSSERVICEAPI {
	
	@Autowired
	IMENUOPTIONSDAO objMenuOptionDao;
	@Autowired 
	COMMONSESSIONBE OBJCOMMONSESSION;
	
	
	/**
	 * Created on:Dec 16  2014
	 * Created by:Sarvendra Tyagi
	 * Description: This service function get MenuOption based on SearchFieldvalue, FieldSearch
	 * Input parameter:String SearchFieldvalue, String FieldSearch
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	
	
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_RestFull_MenuOption_searchlookup, method = RequestMethod.POST)
	public @ResponseBody  Boolean  getList(@RequestParam(value=GLOBALCONSTANT.MenuOptions_Restfull_SearchFieldValue) String SearchFieldValue,@RequestParam(value=GLOBALCONSTANT.MenuOptions_Restfull_SearchField) String SearchField) throws JASCIEXCEPTION {	

		
		
		
		Boolean flagForSearch=false;
		List<MENUOPTIONS> ObjListMenuOptions=null;
		String strApplication=GLOBALCONSTANT.BlankString;
		String strMenuOption=GLOBALCONSTANT.BlankString;
		String strMenuType=GLOBALCONSTANT.BlankString;
		String partOfdesc20=GLOBALCONSTANT.BlankString;
		String partOfDesc50=GLOBALCONSTANT.BlankString;
		String strTenant=GLOBALCONSTANT.BlankString;
		
		try{
			
			if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblMenuOption) ||
					SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartMenuOption)){
				strMenuOption=SearchFieldValue;
				
				ObjListMenuOptions=objMenuOptionDao.getMenuOptionList(strApplication, strMenuOption, strMenuType, partOfdesc20, partOfDesc50,strTenant);	
			
				if(ObjListMenuOptions.size()>GLOBALCONSTANT.INT_ZERO){
					flagForSearch=true;
				}
				
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblApplication) ||
					SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartApplication)){
				
				strApplication=SearchFieldValue;
				
				ObjListMenuOptions=objMenuOptionDao.getMenuOptionListByApplication(strApplication);
				if(ObjListMenuOptions.size()>GLOBALCONSTANT.INT_ZERO){
					flagForSearch=true;
				}
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblMenuType)){
				
				strMenuType=SearchFieldValue;
				
				ObjListMenuOptions=objMenuOptionDao.getMenuOptionListByMenuType(strMenuType);
				if(ObjListMenuOptions.size()>GLOBALCONSTANT.INT_ZERO){
					flagForSearch=true;
				}
			
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartDescription20)||
					SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartDescription50)){
				
				partOfdesc20=SearchFieldValue;
				ObjListMenuOptions=objMenuOptionDao.getMenuOptionListByDescription(partOfdesc20);
				
				if(ObjListMenuOptions.size()>GLOBALCONSTANT.INT_ZERO){
					flagForSearch=true;
				}
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_txtTenant)){
				strTenant=SearchFieldValue;
				
						ObjListMenuOptions=objMenuOptionDao.getMenuOptionListByTenant(strTenant);
					
				if(ObjListMenuOptions.size()>GLOBALCONSTANT.INT_ZERO){
					flagForSearch=true;	
				}
				
				
			}
			
			
			
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
			
		}	
		return flagForSearch;
	}

	
	
	
	
	/** 
	 * @deprecated: This function check menu option assigned in menu profile option table or not
	 * @param: strMenuOption
	 * @return: boolean
	 * @throws: DATABASEEXCEPTION
	 * @Developer: Sarvendra Tyagi
	 * @Date	 : Dec 17 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_RestFull_Check_MenuOption_Profile, method = RequestMethod.POST)
    public @ResponseBody  Boolean  checkMenuOptionInMenuProfile(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Parameter_MenuOptionValue) String strMenuOption) throws JASCIEXCEPTION {	

		
		
		List<MENUPROFILEOPTIONS> objlistMenuProfile=null;
		Boolean flagForMenuOption=false;
		
		
		try{
			
	
			objlistMenuProfile=objMenuOptionDao.checkAssignedOption(strMenuOption);
			
			if(objlistMenuProfile.size()>GLOBALCONSTANT.INT_ZERO){
				flagForMenuOption=true;
			}
			
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
			
		
			
		}	
		return flagForMenuOption;
	}
	

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_RestFull_Check_MenuOption, method = RequestMethod.POST)
    public @ResponseBody  Boolean  checkMenuOptionInMenuOption(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Parameter_MenuOptionValue) String strMenuOption) throws JASCIEXCEPTION {	

		List<Object> objlistMenuOption=null;
		Boolean flagForMenuOption=false;
		
		
		try{
			
			objlistMenuOption=objMenuOptionDao.checkMenuOptionFromOptionTable(strMenuOption);
			
			if(objlistMenuOption.size()>GLOBALCONSTANT.INT_ZERO){
				flagForMenuOption=true;
			}
		}catch(Exception objException){
			

			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return flagForMenuOption;
	}
	
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_RestFull_DropDown, method = RequestMethod.POST)
	public @ResponseBody  List<GENERALCODES> getApplicationAndSubApplication(@RequestParam(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Parameter_valApplication) String strApplication) throws JASCIEXCEPTION{
	
		List<GENERALCODES> listObject=null;
		try{
			
			listObject=objMenuOptionDao.getMenuTypes(strApplication);
			
		}catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
	
		
		return listObject;
	}
	
}

	
	
	
	
	
	
	
		
	

