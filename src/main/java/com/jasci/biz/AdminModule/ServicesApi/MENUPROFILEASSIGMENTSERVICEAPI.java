/**

Date Developed :JAN 13 2014
Created by: sarvendra tyagi
Description :Menu Profile Assigment Service Api For RestfullService
 */


package com.jasci.biz.AdminModule.ServicesApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.MENUPROFILEASSIGNMENTPOJOBE;
import com.jasci.biz.AdminModule.dao.IMENUPROFILEASSIGMENTDAO;
import com.jasci.biz.AdminModule.model.MENUPROFILEASSIGMENTPK;
import com.jasci.biz.AdminModule.model.MENUPROFILEASSIGNMENT;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class MENUPROFILEASSIGMENTSERVICEAPI {
	
	@Autowired
	IMENUPROFILEASSIGMENTDAO objImenuprofileassigmentdao;
	@Autowired
	COMMONSESSIONBE objCommonSession;
	
	
	/** 
	 * @description: This Method is used for search list according to search field and search value
	 * @param: SearchValue
	 * @param: SearchField
	 * @return:
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 15 2014
	 */
	@Transactional
	@RequestMapping(value =GLOBALCONSTANT.RestFullMapping_MenuAssignment_MenuProfileAssignmentSearchList, method = RequestMethod.GET)
	public @ResponseBody List<MENUPROFILEASSIGNMENTPOJOBE> getList(@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_SearchValue) String SearchValue,
			@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_SearchField) String SearchField) throws JASCIEXCEPTION {	

		MENUPROFILEASSIGNMENTPOJOBE objMenuprofileassignmentpojobe=null;
		List<Object> listObject=null;
		List<MENUPROFILEASSIGNMENTPOJOBE> objMenuprofileassignmentpojobes=new ArrayList<MENUPROFILEASSIGNMENTPOJOBE>();
		
		if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberName)){
			
			listObject=objImenuprofileassigmentdao.serchByPartOfTeamMemberName(SearchValue);
			
					
		}else{
			
			listObject=objImenuprofileassigmentdao.getTeamMemberDisplayAll();
		}
		//Iterator<Object> ObjIteratorObject=null;
		Object[] ObjObject = null;

		if(!(listObject.isEmpty()))
		{

		for(Object teamMember : listObject){
			ObjObject=(Object[]) teamMember;
			
			objMenuprofileassignmentpojobe=new MENUPROFILEASSIGNMENTPOJOBE();
			try{
			objMenuprofileassignmentpojobe.setTenant_ID(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
			
			}catch(ArrayIndexOutOfBoundsException objException){
				objMenuprofileassignmentpojobe.setTenant_ID(GLOBALCONSTANT.BlankString);
				throw new JASCIEXCEPTION(objException.getMessage());
			}catch(Exception objException){
				objMenuprofileassignmentpojobe.setTenant_ID(GLOBALCONSTANT.BlankString);
				throw new JASCIEXCEPTION(objException.getMessage());
			}
			
			try{
				objMenuprofileassignmentpojobe.setTeamMemberID(ObjObject[GLOBALCONSTANT.IntOne].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setTeamMemberID(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setTeamMemberID(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			
			try{
				objMenuprofileassignmentpojobe.setFirstName(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setFirstName(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setFirstName(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			
			try{
				objMenuprofileassignmentpojobe.setLastName(ObjObject[GLOBALCONSTANT.INT_THREE].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setLastName(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setLastName(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			
			try{
				objMenuprofileassignmentpojobe.setMiddleName(ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setMiddleName(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setMiddleName(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			
			try{
				objMenuprofileassignmentpojobe.setCurrentStatus(ObjObject[GLOBALCONSTANT.INT_FIVE].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setCurrentStatus(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setCurrentStatus(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			
			try{
				objMenuprofileassignmentpojobe.setProfile(ObjObject[GLOBALCONSTANT.INT_SIX].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setProfile(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setProfile(GLOBALCONSTANT.BlankString);
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			objMenuprofileassignmentpojobes.add(objMenuprofileassignmentpojobe);
		}
		}
		
		
	return objMenuprofileassignmentpojobes;	
	
	}
	
	
	
	/** 
	 * @description: This Method is used for get assigned menu profile list
	 *
	 * @param: String teamMemberId
	 * @return:
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 15 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_AssignedMenuProfile, method = RequestMethod.POST)
	public @ResponseBody  List<MENUPROFILEASSIGNMENTPOJOBE>  getAsignedTeamMemberMenuProfile(
			@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_TeamMemberID) String teamMemberId) throws JASCIEXCEPTION {	

		MENUPROFILEASSIGNMENTPOJOBE objMenuprofileassignmentpojobe=null;
		
		List<MENUPROFILEASSIGNMENTPOJOBE> objMenuprofileassignmentpojobeList=new ArrayList<MENUPROFILEASSIGNMENTPOJOBE>();
		List<Object> objList=null;
		try{
		objList=objImenuprofileassigmentdao.getTeamMemberAssignedProfileList(teamMemberId);
		}catch(Exception objException){
			
			
		}
		
		Object[] ObjObject = null;

		if(!(objList.isEmpty()))
		{

		for(Object teamMember : objList){
			ObjObject=(Object[]) teamMember;
			
			objMenuprofileassignmentpojobe=new MENUPROFILEASSIGNMENTPOJOBE();
			
			try{
			objMenuprofileassignmentpojobe.setTenant_ID(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
			
			}catch(ArrayIndexOutOfBoundsException objException){
				objMenuprofileassignmentpojobe.setTenant_ID(GLOBALCONSTANT.BlankString);
				
			}catch(Exception objException){
				objMenuprofileassignmentpojobe.setTenant_ID(GLOBALCONSTANT.BlankString);
				
			}
			
			try{
				objMenuprofileassignmentpojobe.setProfile(ObjObject[GLOBALCONSTANT.IntOne].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setProfile(GLOBALCONSTANT.BlankString);
					
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setProfile(GLOBALCONSTANT.BlankString);
					
				}
			try{
				objMenuprofileassignmentpojobe.setMenuType(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setMenuType(GLOBALCONSTANT.BlankString);
					
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setMenuType(GLOBALCONSTANT.BlankString);
					
				}
			try{
				objMenuprofileassignmentpojobe.setDESCRIPTION20(ObjObject[GLOBALCONSTANT.INT_THREE].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setDESCRIPTION20(GLOBALCONSTANT.BlankString);
					
				}	catch(Exception objException){
					objMenuprofileassignmentpojobe.setDESCRIPTION20(GLOBALCONSTANT.BlankString);
					
				}	
			
			try{
				objMenuprofileassignmentpojobe.setDESCRIPTION50(ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setDESCRIPTION50(GLOBALCONSTANT.BlankString);
					
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setDESCRIPTION50(GLOBALCONSTANT.BlankString);
					
				}	
			
			try{
				objMenuprofileassignmentpojobe.setHELP_LINE(ObjObject[GLOBALCONSTANT.INT_SIX].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setHELP_LINE(GLOBALCONSTANT.BlankString);
					
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setHELP_LINE(GLOBALCONSTANT.BlankString);
					
				}
			
			try{
				objMenuprofileassignmentpojobe.setFirstName(ObjObject[GLOBALCONSTANT.INT_SIX].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setFirstName(GLOBALCONSTANT.BlankString);
					
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setFirstName(GLOBALCONSTANT.BlankString);
					
				}
			
			try{
				objMenuprofileassignmentpojobe.setMiddleName(ObjObject[GLOBALCONSTANT.INT_SEVEN].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setMiddleName(GLOBALCONSTANT.BlankString);
					
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setMiddleName(GLOBALCONSTANT.BlankString);
					
				}
			
			try{
				objMenuprofileassignmentpojobe.setLastName(ObjObject[GLOBALCONSTANT.INT_EIGHT].toString());
				
				}catch(ArrayIndexOutOfBoundsException objException){
					objMenuprofileassignmentpojobe.setLastName(GLOBALCONSTANT.BlankString);
					
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setLastName(GLOBALCONSTANT.BlankString);
					
				}
			
			if(objMenuprofileassignmentpojobe.getFirstName()!=null && objMenuprofileassignmentpojobe.getMiddleName() !=null
					&& objMenuprofileassignmentpojobe.getLastName()!=null){
				
				if(objMenuprofileassignmentpojobe.getMiddleName().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
					
					objMenuprofileassignmentpojobe.setName(objMenuprofileassignmentpojobe.getFirstName()+GLOBALCONSTANT.Single_Space+
							objMenuprofileassignmentpojobe.getLastName());
				}else{
					

					objMenuprofileassignmentpojobe.setName(objMenuprofileassignmentpojobe.getFirstName()+GLOBALCONSTANT.Single_Space+
							objMenuprofileassignmentpojobe.getMiddleName()+GLOBALCONSTANT.Single_Space
							+objMenuprofileassignmentpojobe.getLastName());
				}
			}
			objMenuprofileassignmentpojobeList.add(objMenuprofileassignmentpojobe);
		}
		}
		
		
		 return objMenuprofileassignmentpojobeList;
	
	}
	

	
	
	/** 
	 * @description: This Method is used for check team member in team member table
	 *
	 * @param: String teamMemberId
	 * @return:
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 15 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_MenuAssignment_RestFull_Check_TeamMemberID, method = RequestMethod.POST)
	public @ResponseBody  Boolean  checkTeamMemberID(@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_SearchValue) String SearchValue ,
			@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_SearchField) String SearchField) throws JASCIEXCEPTION {	
		
		Boolean Status=false;
		List<Object> objTeammembers=null;
		
		try{
			
			objTeammembers=objImenuprofileassigmentdao.getTeamMemberData(SearchValue);
			
			if(objTeammembers.size()>GLOBALCONSTANT.INT_ZERO){
				Status=true;
			}
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		
		return Status;
		
		
	
	}
	
	
	/** 
	 * @description: This Method is used for check Part of team member in team member table
	 *
	 * @param: String teamMemberId
	 * @return:
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 15 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_MenuAssignment_check_PartOfTeamMemberName, method = RequestMethod.POST)
	public @ResponseBody  Boolean  checkPartTeamMemberName(@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_SearchValue) String SearchValue ,
			@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_SearchField) String SearchField) throws JASCIEXCEPTION {	
		
		Boolean Status=false;
		List<Object> objTeammembers=null;
		
		try{
			
			objTeammembers=objImenuprofileassigmentdao.serchByPartOfTeamMemberName(SearchValue);
			
			if(objTeammembers.size()>GLOBALCONSTANT.INT_ZERO){
				Status=true;
			}
			
		}catch(Exception objException){
			
			
		}
		
		
		return Status;
		
		
	
	}
	
	
	/** 
	 * @description: This Method is used for get list of part of team member 
	 *
	 * @param: strPartOfTeamMemberName
	 * @return:List<Object>  
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 15 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_MenuAssignment_PartOFTeamMemberName, method = RequestMethod.GET)
	public @ResponseBody  List<Object>  getPartTeamMemberNameList(@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_PartOfTeamMemberName) String strPartOfTeamMemberName) throws JASCIEXCEPTION {	

		
		return objImenuprofileassigmentdao.serchByPartOfTeamMemberName(strPartOfTeamMemberName);
		
		
	
	}
	
	

	/** 
	 * @description: This Method is used for get list based on team member id 
	 *
	 * @param: teamMemberId
	 * @return:List<Object>  
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 15 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_MenuAssignment_TeamMemberID, method = RequestMethod.GET)
	public @ResponseBody  List<Object>  getTeamMemberIDList(@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_TeamMemberID) String teamMemberId) throws JASCIEXCEPTION {	

		
		return objImenuprofileassigmentdao.getTeamMemberData(teamMemberId);
		
		
	
	}
	
	
	
	
	/** 
	 * @description: This Method is used for get list all team member
	 *
	 * @param: 
	 * @return:List<Object>  
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 16 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_MenuAssignment_AllTeamMember, method = RequestMethod.GET)
	public @ResponseBody  List<Object>  getAllTeamMemberList() throws JASCIEXCEPTION {	

		
		return objImenuprofileassigmentdao.getTeamMemberDisplayAll();
		
		
	
	}
	
	
	/** 
	 * @description: This Method is used for get list of menu profile from menu profile header
	 *
	 * @param: MenuType
	 * @return:List<MENUPROFILEASSIGNMENTPOJOBE> 
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 16 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_MenuProfile_fromMenuHeader, method = RequestMethod.POST)
	public @ResponseBody  List<MENUPROFILEASSIGNMENTPOJOBE>  getMenuProfileList(@RequestParam(value=GLOBALCONSTANT.MENUOPTION_MenuType) String MenuType) throws JASCIEXCEPTION {	

		 List<Object> objList=null;
			MENUPROFILEASSIGNMENTPOJOBE objMenuprofileassignmentpojobe=null;
			
			List<MENUPROFILEASSIGNMENTPOJOBE> objMenuprofileassignmentpojobes=new ArrayList<MENUPROFILEASSIGNMENTPOJOBE>();
		 if(MenuType.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All)){
			 
			 objList=objImenuprofileassigmentdao.getAllMenuProfileList();
		 }else{
			 
			 objList=objImenuprofileassigmentdao.getMenuProfileList(MenuType);
		 }
		
		 Object[] ObjObject = null;

			if(!(objList.isEmpty()))
			{

			for(Object teamMember : objList){
				ObjObject=(Object[]) teamMember;
				
				objMenuprofileassignmentpojobe=new MENUPROFILEASSIGNMENTPOJOBE();
				
				try{
				objMenuprofileassignmentpojobe.setTenant_ID(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setTenant_ID(GLOBALCONSTANT.BlankString);
					
				}
				
				try{
					objMenuprofileassignmentpojobe.setProfile(ObjObject[GLOBALCONSTANT.IntOne].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setProfile(GLOBALCONSTANT.BlankString);
						
					}
				
				try{
					objMenuprofileassignmentpojobe.setMenuType(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setMenuType(GLOBALCONSTANT.BlankString);
						
					}
				
				try{
					objMenuprofileassignmentpojobe.setDESCRIPTION20(ObjObject[GLOBALCONSTANT.INT_THREE].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setDESCRIPTION20(GLOBALCONSTANT.BlankString);
						
					}	
				
				try{
					objMenuprofileassignmentpojobe.setDESCRIPTION50(ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setDESCRIPTION50(GLOBALCONSTANT.BlankString);
						
					}	
				
				try{
					objMenuprofileassignmentpojobe.setHELP_LINE(ObjObject[GLOBALCONSTANT.INT_FIVE].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setHELP_LINE(GLOBALCONSTANT.BlankString);
						
					}
				
				objMenuprofileassignmentpojobes.add(objMenuprofileassignmentpojobe);
			}
			
			}
		 
		
		return objMenuprofileassignmentpojobes;
		
		
	
	}
	
	
	/** 
	 * @description: This Method is used for get list of assigned menu profile from menu profile Assignments table
	 * @param: MenuType
	 * @param: TeamMemberId
	 * @return:List<MENUPROFILEASSIGNMENTPOJOBE> 
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 16 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_MenuProfileAssignment_fromMenuHeader, method = RequestMethod.POST)
	public @ResponseBody  List<MENUPROFILEASSIGNMENTPOJOBE>  getMenuProfileListRemoveRight(@RequestParam(value=GLOBALCONSTANT.MENUOPTION_MenuType) String MenuType,
			@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_SelectedTeamMember) String TeamMemberId) throws JASCIEXCEPTION {	

		 List<Object> objList=null;
			MENUPROFILEASSIGNMENTPOJOBE objMenuprofileassignmentpojobe=null;
			
			List<MENUPROFILEASSIGNMENTPOJOBE> objMenuprofileassignmentpojobes=new ArrayList<MENUPROFILEASSIGNMENTPOJOBE>();
		
			 if(MenuType.equalsIgnoreCase(GLOBALCONSTANT.display_all)){
				 objList=objImenuprofileassigmentdao.getAllMenuProfileListLeftMenu(TeamMemberId);
			 }else{
				 objList=objImenuprofileassigmentdao.getAllMenuProfileListMenu(TeamMemberId, MenuType);
			 }
			 
		
		
		 Object[] ObjObject = null;

			if(!(objList.isEmpty()))
			{

			for(Object teamMember : objList){
				ObjObject=(Object[]) teamMember;
				
				objMenuprofileassignmentpojobe=new MENUPROFILEASSIGNMENTPOJOBE();
				
				try{
				objMenuprofileassignmentpojobe.setTenant_ID(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setTenant_ID(GLOBALCONSTANT.BlankString);
					
				}
				
				try{
					objMenuprofileassignmentpojobe.setProfile(ObjObject[GLOBALCONSTANT.IntOne].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setProfile(GLOBALCONSTANT.BlankString);
						
					}
				
				try{
					objMenuprofileassignmentpojobe.setMenuType(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setMenuType(GLOBALCONSTANT.BlankString);
						
					}
				
				try{
					objMenuprofileassignmentpojobe.setDESCRIPTION20(ObjObject[GLOBALCONSTANT.INT_THREE].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setDESCRIPTION20(GLOBALCONSTANT.BlankString);
						
					}	
				
				try{
					objMenuprofileassignmentpojobe.setDESCRIPTION50(ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setDESCRIPTION50(GLOBALCONSTANT.BlankString);
						
					}	
				
				try{
					objMenuprofileassignmentpojobe.setHELP_LINE(ObjObject[GLOBALCONSTANT.INT_FIVE].toString());
					
					}catch(Exception objException){
						objMenuprofileassignmentpojobe.setHELP_LINE(GLOBALCONSTANT.No_Help_exist_for_menu_profile);
						
					}
				
				objMenuprofileassignmentpojobes.add(objMenuprofileassignmentpojobe);
			}
			
			}
		 
		
		return objMenuprofileassignmentpojobes;
		
		
	
	}
	
	
	
	
	/** 
	 * @description: This Method is used for delete data from menu profile assignment table
	 * @param: Tenant
	 * @param: TeamMemberId
	 * @return:Boolean 
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 18 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_MenuProfileAssignment_Delete, method = RequestMethod.POST)
	public @ResponseBody  Boolean  getMenuProfileDelete(@RequestParam(value=GLOBALCONSTANT.MenuProfile_Tenant) String Tenant,
			@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberID) String TeamMemberId) throws JASCIEXCEPTION {	

		Boolean status=false;
		
		try{
			
			
			status=objImenuprofileassigmentdao.getAllAssignedMenuProfileDelete(TeamMemberId);
		}catch(Exception objException){
			
			
		}
		
		return status;
	}
	
	
	
	/** 
	 * @description: This Method is used for save data in menu profile assignment table
	 * @param: strMenuProfileJson
	 * @param: TeamMemberId
	 * @return:Boolean 
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 18 2014
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_MenuProfileAssignment_SaveAndUpdate, method = RequestMethod.POST)
	public @ResponseBody  Boolean  SaveAndUpdate(@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_TeamMemberID) String teamMemberId,
			@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_strMenuProfileJson) String strMenuProfileJson,
			@RequestParam(value=GLOBALCONSTANT.strDate) String strDate) throws JASCIEXCEPTION {	

		Boolean status=false;
		
		MENUPROFILEASSIGNMENT objMenuprofileassignment=null;
		MENUPROFILEASSIGMENTPK objMenuprofileassigmentpk=null;
		
		try{
		
		
		
		
		
		
		List<String> lstMenuProfile=null;
		try{
			
			lstMenuProfile=getListMenuOption(strMenuProfileJson);
			
		
			
		}catch(Exception objException){
			
		}
		

		/* Date ObjDate1 = new Date();*/
	        DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Insert_DateFormat_DD_MMM_YY);
	       /* String StrCurrentDate=ObjDateFormat1.format(ObjDate1);*/
	        Date currentDate=ObjDateFormat1.parse(strDate);
		
		for(String strMenuProfile : lstMenuProfile){
			objMenuprofileassignment=new MENUPROFILEASSIGNMENT();
		objMenuprofileassigmentpk=new MENUPROFILEASSIGMENTPK();
			objMenuprofileassigmentpk.setMenuProfile(strMenuProfile);
			
			objMenuprofileassigmentpk.setTeamMember(teamMemberId);
			objMenuprofileassigmentpk.setTenant(objCommonSession.getTenant());
			
			
			
		        objMenuprofileassignment.setLastActivityDate(currentDate);
		        objMenuprofileassignment.setLastActivityTeamMember(objCommonSession.getTeam_Member());
		        objMenuprofileassignment.setId(objMenuprofileassigmentpk);
		        status=objImenuprofileassigmentdao.getSaveAndUpdate(objMenuprofileassignment);  
			
		}
		
	
		
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return status;
		
		
		
	}
	
	
	
	
	/** 
	 * @description: This Method is used for save data in menu profile assignment table
	 * @param: strMenuProfileJson
	 * @param: TeamMemberId
	 * @return:Boolean 
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 18 2014
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFullMapping_RestFull_TeamMemberName_MenuProfileAssignment, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  getTeamMemberName(@RequestParam(value=GLOBALCONSTANT.MenuProfileAssigment_TeamMemberID) String teamMemberId) throws JASCIEXCEPTION {
		
		return objImenuprofileassigmentdao.getTeamMemberName(teamMemberId);
	}
	
	
	
	
	
	

	/** 
	 * @description: This Method is used for get data from json
	 * @param: strMenuProfileJson
	 * 
	 * @return:List<String>  
	 * @throws: JASCIEXCEPTION
	 * @developed by: Sarvendra Tyagi
	 * @date		: Jan 18 2014
	 */
	// create List Of menu Option from json 
	public List<String> getListMenuOption(String strValue)
	{
			
        List<String> lstMenuProfile=new ArrayList<String>();

		JSONParser jsonParser=new JSONParser();
		try {

			Object objJson =jsonParser.parse(strValue);

			//create json array from object values
			JSONArray jsonArrayObject = (JSONArray) objJson;

			for(int subJson=GLOBALCONSTANT.IntZero;subJson<jsonArrayObject.size();subJson++){
			//fetch values from json array at first index
			JSONObject jsonObject=(JSONObject)jsonParser.parse(jsonArrayObject.get(subJson).toString());

			String menuOption=(String) jsonObject.get(GLOBALCONSTANT.profile);
			
			lstMenuProfile.add(menuOption);
			
			}
			
			
		} catch (Exception objException) {

		new JASCIEXCEPTION(objException.getMessage());
			

		} 

		return lstMenuProfile;

	}


	
}
