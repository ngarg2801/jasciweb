/**

Date Developed :Dec 12 2014
Created by: Rahul kumar
Description :MENUAPPICONMAINTENANCE Controller TO HANDLE THE REQUESTS FROM SCREENS
 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class MENUAPPICONSERVICEAPI {

	@Autowired
	private IMENUAPPICONDAL objImenuappicondal;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;

	
   
	
	/**
	 * @description set screen language
	 * @author Rahul Kumar
	 * @Date Dec 12, 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<LANGUAGES>
	 *
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_SetScreenLanguage, method = RequestMethod.GET)
	public @ResponseBody  List<LANGUAGES> setScreenLanguage(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_ScreenName) String StrScreenName,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_SetScreenLanguage) String StrLanguage) throws JASCIEXCEPTION {	

		return objImenuappicondal.setScreenLanguage(StrScreenName, StrLanguage);

	
	}
	
/**
 * 
 * @author Rahul Kumar
 * @Date Dec 14, 2014
 * @param ObjMenuappicons
 * @return
 * @throws JASCIEXCEPTION
 * @Return Boolean
 *
 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_AddOrUpdate, method = RequestMethod.GET)
	public @ResponseBody Boolean addOrUpdateMenuAppIcon(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_ObjMenuAppIcons) MENUAPPICONS ObjMenuappicons) throws JASCIEXCEPTION {	

		
		return objImenuappicondal.addOrUpdateMenuAppIcon(ObjMenuappicons);

	
	}

	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param Tenant
	 * @param Company
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<MENUAPPICONS>
	 *
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_GetMenuAppIconsList, method = RequestMethod.GET)
	public @ResponseBody  List<MENUAPPICONS> getMenuAppIconList(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_Company) String Company) throws JASCIEXCEPTION {	

		
		return objImenuappicondal.getManuAppIconList(Tenant, Company);
	
	}
	
	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014 
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<MENUAPPICONS>
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_GetMenuAppIconsListByAppIcon, method = RequestMethod.GET)
	public @ResponseBody  List<MENUAPPICONS> getMenuAppIconListByApplicationAppIcon(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppLication) String Application,@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIcon) String AppIcon) throws JASCIEXCEPTION {	

		
		return objImenuappicondal.getMenuAppIconByAppIcon(AppIcon);
	
	}
	
	
	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 15, 2014
	 * @param AppIcon
	 * @param AppIconName
	 * @param AppIconPart
	 * @param SearchBy
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<MENUAPPICONS>
	 *
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_checkIcon, method = RequestMethod.POST)
	public @ResponseBody  List<MENUAPPICONS>  checkValidAppIcon(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIcon) String AppIcon,@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIconName) String AppIconName,@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIconPart) String AppIconPart,@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_SearchBy) String SearchBy) throws JASCIEXCEPTION {	

		List<MENUAPPICONS> objMenuappicons=null; 
		
		if(SearchBy.equalsIgnoreCase(GLOBALCONSTANT.MenuAppIcons_Restfull_AppIcon)){
		
			objMenuappicons=objImenuappicondal.getMenuAppIconByAppIcon(AppIcon);
		}
		else if(SearchBy.equalsIgnoreCase(GLOBALCONSTANT.MenuAppIcons_Restfull_AppIconName)){
			
			objMenuappicons=objImenuappicondal.getMenuAppIconByAppIconName(AppIconName);		
		}
		else if(SearchBy.equalsIgnoreCase(GLOBALCONSTANT.MenuAppIcons_Restfull_AppIconPart)){
		
			objMenuappicons=objImenuappicondal.getMenuAppIconByAppIconPart(AppIconPart);
		}
		else{
			
		}	
		
		
		return objMenuappicons;
	}

	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 16, 2014 
	 * @param GeneralCodeID
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<GENERALCODES>
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_GetApplication, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODES>  getApplication(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_GeneralCodeId) String GeneralCodeID) throws JASCIEXCEPTION {	
	
		List<GENERALCODES> objGeneralcodes=null;
		String Tenant=OBJCOMMONSESSIONBE.getTenant();
		String Company=OBJCOMMONSESSIONBE.getCompany();
		objGeneralcodes=objImenuappicondal.getApplicationList(Tenant,Company,GeneralCodeID);
		return objGeneralcodes;
	}

	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014 
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * Boolean
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_Delete_Application, method = RequestMethod.GET)
	public @ResponseBody  Boolean  deleteMenuAppIcon(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_Delete_Application) String Application,@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIcon) String AppIcon) throws JASCIEXCEPTION {	
	
		
		return objImenuappicondal.deleteMenuAppIcon(Application, AppIcon);
		
	}
	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 18, 2014 
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * Boolean
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_GetMenuAppIconsListByAppIcon, method = RequestMethod.POST)
	public @ResponseBody Boolean getMenuAppIconListByAppIcon(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIcon) String AppIcon) throws JASCIEXCEPTION {	

		
		List <MENUAPPICONS> objMenuappicons=null;
		objMenuappicons=objImenuappicondal.getMenuAppIconByAppIcon(AppIcon);
		
		if(objMenuappicons.size()>GLOBALCONSTANT.INT_ZERO){
		return true;
		}
		else{
			return false;
		}
	
	}
	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 18, 2014 
	 * @param Application
	 * @return
	 * @throws JASCIEXCEPTION
	 * Boolean
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_GetMenuAppIconsListByApplication, method = RequestMethod.POST)
	public @ResponseBody  Boolean getMenuAppIconListByApplicationAppIcon(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppLication) String Application) throws JASCIEXCEPTION {	
		
		List <MENUAPPICONS> objMenuappicons=null;
		objMenuappicons=objImenuappicondal.getMenuAppIconByApplication(Application);
		
		if(objMenuappicons.size()>GLOBALCONSTANT.INT_ZERO){
		return true;
		}
		else{
			return false;
		}
		
	}
	
	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014 
	 * @param AppIconName
	 * @param AppIconPart
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<MENUAPPICONS>
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_checkAppIcon, method = RequestMethod.GET)
	public @ResponseBody  List<MENUAPPICONS>  getAppIconListByPart(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIconName) String AppIconName,@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_AppIconPart) String AppIconPart) throws JASCIEXCEPTION {	

	
		List<MENUAPPICONS> objMenuappicons=null; 
		
		 if(!GLOBALCONSTANT.BlankString.equals(AppIconName)){
			
			objMenuappicons=objImenuappicondal.getMenuAppIconByAppIconName(AppIconName);		
		}
		else if(!GLOBALCONSTANT.BlankString.equals(AppIconPart)){
		
			objMenuappicons=objImenuappicondal.getMenuAppIconByAppIconPart(AppIconPart);
		}
		else{
			
		}	
		
		
		return objMenuappicons;
	}
	
	
	
	/**
	 * @Description upload image on amazon server
	 * @param request
	 * @param response
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	
	@SuppressWarnings({ GLOBALCONSTANT.Strrawtypes, GLOBALCONSTANT.Strfinally })
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_UploadImage, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  uploadIcon(HttpServletRequest request,HttpServletResponse response) throws JASCIEXCEPTION {
		
		List<Object> obj=new ArrayList<Object>();
		
		/*Properties ObjectProperty = new Properties();
		String propFileName = GLOBALCONSTANT.ConfigFilePathWithName;
		try {
			InputStream ObjectInputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			ObjectProperty.load(ObjectInputStream);
		} catch (IOException ObjectIOException) {
		}*/
		CONFIGURATIONEFILE ObjConfiguration=new CONFIGURATIONEFILE();
		String AMAZON_ACCESS_KEY = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_AccessKey);
		String AMAZON_SECRET_KEY = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_SecrateKey);
			
		String S3_BUCKET_NAME = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_BucketName);	
		
		
		ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_ServerName);
		
		  String urlfile=GLOBALCONSTANT.BlankString;
	      
		
		try{
		// needed for cross-domain communication
        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Origin, GLOBALCONSTANT.ASTRIK);
        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Methods,GLOBALCONSTANT.POST);
        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Headers, GLOBALCONSTANT.Access_Control_Allow_Headers_Content_Type);
        response.setHeader(GLOBALCONSTANT.Access_Control_MaxAge, GLOBALCONSTANT.Access_Control_Allow_Max_Age_Value);
        
       
		
        
     
        if (!ServletFileUpload.isMultipartContent(request)) {
            //PrintWriter writer = response.getWriter();
           // writer.println("Request does not contain upload data");
           // writer.flush();
            //return GLOBALCONSTANT.BlankString;
        }
        
        
        
     // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(GLOBALCONSTANT.THRESHOLD_SIZE);
 
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(GLOBALCONSTANT.MAX_FILE_SIZE);
        upload.setSizeMax(GLOBALCONSTANT.MAX_REQUEST_SIZE);
         
        
        String uuidValue =UUID.randomUUID().toString();
        FileItem itemFile = null;
        
        try {
            // parses the request's content to extract file data
            List formItems = upload.parseRequest(request);
            Iterator iter = formItems.iterator();
 
            // iterates over form's fields to get UUID Value
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (item.isFormField()) {
                    if (item.getFieldName().equalsIgnoreCase(GLOBALCONSTANT.UUID_STRING)) {
                        uuidValue = UUID.randomUUID().toString();
                    }
                }
                // processes only fields that are not form fields
                if (!item.isFormField()) {
                    itemFile = item;
                }
            }
 
            if (itemFile != null) {
                // get item inputstream to upload file into s3 aws
 
                BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY);
 
                AmazonS3 s3client = new AmazonS3Client(awsCredentials);
                
                /*List<Bucket> buckets = s3client.listBuckets();
                for (Bucket bucket : buckets) {
                        System.out.println(bucket.getName());
                               
                }*/
                try {
 
                    ObjectMetadata om = new ObjectMetadata();
                    om.setContentLength(itemFile.getSize());
                    String ext = FilenameUtils.getExtension(itemFile.getName());
                    String keyName = uuidValue + GLOBALCONSTANT.DOT + ext;
 
                    s3client.putObject(new PutObjectRequest(S3_BUCKET_NAME, keyName, itemFile.getInputStream(), om));
                    s3client.setObjectAcl(S3_BUCKET_NAME, keyName, CannedAccessControlList.PublicRead);
                    
                    
                    
                    try{
              		  GeneratePresignedUrlRequest requestUrl = new GeneratePresignedUrlRequest(S3_BUCKET_NAME, keyName);
              		  URL url=s3client.generatePresignedUrl(requestUrl);
              		  String protocol=url.getProtocol();
              		  String host=url.getHost();
              		  String file=url.getFile();
              		  urlfile=protocol+GLOBALCONSTANT.Forward_Double_Slash+host+file;
              		  
              		  }catch(Exception e){
              			
              		  }
                    
                    
 
                } catch (AmazonServiceException ase) {
                 
 
                } catch (AmazonClientException ace) {
                	
                }
 
 
            } else {
               
            }
 
        } catch (Exception ex) {
        	
        	
        }
        
        
        
        
        
        
 
		
		}catch(Exception objException){
			//objException.printStackTrace();
		}
	
		finally
		{
			urlfile=urlfile.substring(GLOBALCONSTANT.INT_ZERO,urlfile.indexOf(GLOBALCONSTANT.QUESTION_MARK));			
			obj.add(urlfile);
			return obj;
		}
		
	}
	
	
	/**
	 * @Description get team member name
	 * @author Rahul Kumar
	 * @date 26 dec 2014
	 * @param Tenant
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.MenuAppIcons_Restfull_GetTeamMember, method = RequestMethod.GET)
	public @ResponseBody List<TEAMMEMBERS> getTeamMemberName(@RequestParam(value=GLOBALCONSTANT.MenuAppIcons_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_TeamMember) String TeamMember) throws JASCIEXCEPTION {	

		
		return objImenuappicondal.getTeamMemberName(Tenant, TeamMember);

	
	}
	
	
}
