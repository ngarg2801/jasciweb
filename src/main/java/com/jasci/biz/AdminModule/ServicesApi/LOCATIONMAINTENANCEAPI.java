/*

Description This class is used to make restfull service for LOCATION MAINTENANCE SCREENS
Created By Aakash Bishnoi
Created Date Dec 26 2014

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.dao.ILOCATIONMAINTENANCEDAO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.INVENTORY_LEVELS;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONBEAN;
import com.jasci.biz.AdminModule.model.LOCATIONPK;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILESPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class LOCATIONMAINTENANCEAPI {
	/*It is used to get the selected value of Fulfillment center dropdown*/
	String SelectedFulfillmentCenterValue=GLOBALCONSTANT.BlankString;
	@Autowired
	private ILOCATIONMAINTENANCEDAO ObjectLocationMaintenanceDao;
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 26, 2014
	 * Description:This is used to Add Record in Location Table Form LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	 * @throws ParseException 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenance_Restfull_RestLocationMaintenanceAdd, method = RequestMethod.GET)
	public @ResponseBody  String addEntry(LOCATIONBEAN ObjectLocationBean) throws JASCIEXCEPTION{	
		LOCATION ObjectLocation=new LOCATION();
		LOCATIONPK ObjectLocationpk=new LOCATIONPK();

		String Result=null;
		ObjectLocation=useGetterSetterForAdd(ObjectLocationBean,ObjectLocationpk,ObjectLocation);
		Result=ObjectLocationMaintenanceDao.addEntry(ObjectLocation);								
		return Result;
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 26, 2014
	 * Description:This is used to Update Record in Location Table Form LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	 * @throws ParseException 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenance_Restfull_RestLocationMaintenanceUpdate, method = RequestMethod.GET)
	public @ResponseBody  String updateEntry(LOCATIONBEAN ObjLocationBean) throws JASCIEXCEPTION{	
		LOCATION objLocation=new LOCATION();
		LOCATIONPK ObjLocationpk=new LOCATIONPK();

		String Result=null;
		//ObjectLocation=useGetterSetterForAdd(ObjectLocationBean,ObjectLocationpk,ObjectLocation);
		ObjLocationpk.setTenant_ID(trimValues(ObjLocationBean.getTenant_ID()));
		ObjLocationpk.setCompany_ID(trimValues(ObjLocationBean.getCompany_ID()));
		ObjLocationpk.setFullfillment_Center_ID(trimValues(ObjLocationBean.getFullfillment_Center_ID()));
		ObjLocationpk.setLocation(trimValues(ObjLocationBean.getLocation()));
		ObjLocationpk.setArea(trimValues(ObjLocationBean.getArea()));		
		objLocation.setId(ObjLocationpk);
		//
		//Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
		//String testDateString = ObjDateFormat.format(CurrentDate);		

		try {
			objLocation.setLast_Activity_Date(ObjDateFormat.parse(ObjLocationBean.getLast_Activity_Date()));
		} catch (ParseException e) {

		}
		objLocation.setLast_Activity_Team_Member(trimValues(objCommonsessionbe.getTeam_Member()));
		objLocation.setLast_Activity_Task(trimValues(ObjLocationBean.getLast_Activity_Task()));
		objLocation.setLocation_Type(trimValues(ObjLocationBean.getLocation_Type()));
		objLocation.setDescription20(trimValues(ObjLocationBean.getDescription20()));
		objLocation.setDescription50(trimValues(ObjLocationBean.getDescription50()));
		objLocation.setLocation_Profile(trimValues(ObjLocationBean.getLocation_Profile()));
		
		try{
			objLocation.setWizard_ID(trimValues(ObjLocationBean.getWizard_ID()));
			objLocation.setWizard_Control_Number(Long.parseLong(ObjLocationBean.getWizard_Control_Number()));
		}catch(Exception e)
		{
			objLocation.setWizard_Control_Number(GLOBALCONSTANT.LongZero);

		}
		//objLocation.setWizard_Control_Number(Long.parseLong(trimValues(ObjLocationBean.getWizard_Control_Number())));
		objLocation.setWork_Group_Zone(trimValues(ObjLocationBean.getWork_Group_Zone()));
		objLocation.setWork_Zone(trimValues(ObjLocationBean.getWork_Zone()));
		objLocation.setCheck_Digit(trimValues(ObjLocationBean.getCheck_Digit()));			
		objLocation.setAlternate_Location(trimValues(ObjLocationBean.getAlternate_Location()));
		try{
			objLocation.setLocation_Depth(Double.parseDouble(ObjLocationBean.getLocation_Depth()));
		}catch(Exception e)
		{
			objLocation.setLocation_Depth(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Depth(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Depth())));
		try{
			objLocation.setLocation_Height(Double.parseDouble(ObjLocationBean.getLocation_Height()));
		}catch(Exception e)
		{
			objLocation.setLocation_Height(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Height(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Height())));
		try{
			objLocation.setLocation_Width(Double.parseDouble(ObjLocationBean.getLocation_Width()));
		}catch(Exception e)
		{
			objLocation.setLocation_Width(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Width(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Width())));
		try{
			objLocation.setLocation_Height_Capacity(Double.parseDouble(ObjLocationBean.getLocation_Height_Capacity()));
		}catch(Exception e)
		{
			objLocation.setLocation_Height_Capacity(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Height_Capacity(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Height_Capacity())));
		try{
			objLocation.setLocation_Weight_Capacity(Double.parseDouble(ObjLocationBean.getLocation_Weight_Capacity()));
		}catch(Exception e)
		{
			objLocation.setLocation_Weight_Capacity(GLOBALCONSTANT.DecimalZero);

		}

		/*objLocation.setLocation_Height(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Height())));
		objLocation.setLocation_Width(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Width())));
		objLocation.setLocation_Height_Capacity(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Height_Capacity())));
		objLocation.setLocation_Weight_Capacity(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Weight_Capacity())));*/
		try{
			objLocation.setNumber_Pallets_Fit(Long.parseLong(ObjLocationBean.getNumber_Pallets_Fit()));
		}catch(Exception e)
		{
			objLocation.setNumber_Pallets_Fit(GLOBALCONSTANT.LongZero);

		}

		//objLocation.setNumber_Pallets_Fit(Long.parseLong(trimValues(ObjLocationBean.getNumber_Pallets_Fit())));
		try{
			objLocation.setNumber_Floor_Locations(Long.parseLong(ObjLocationBean.getNumber_Floor_Locations()));
		}catch(Exception e)
		{
			objLocation.setNumber_Floor_Locations(GLOBALCONSTANT.LongZero);

		}
		/*objLocation.setNumber_Pallets_Fit(Long.parseLong(trimValues(ObjLocationBean.getNumber_Pallets_Fit())));
		objLocation.setNumber_Floor_Locations(Long.parseLong(trimValues(ObjLocationBean.getNumber_Floor_Locations())));*/
		objLocation.setAllocation_Allowable(trimValues(ObjLocationBean.getAllocation_Allowable()));
		objLocation.setFree_Location_When_Zero(trimValues(ObjLocationBean.getFree_Location_When_Zero()));
		try{
			objLocation.setFree_Prime_Days(Long.parseLong(ObjLocationBean.getFree_Prime_Days()));
		}catch(Exception e)
		{
			objLocation.setFree_Prime_Days(GLOBALCONSTANT.LongZero);

		}
		//objLocation.setFree_Prime_Days(Long.parseLong(trimValues(ObjLocationBean.getFree_Prime_Days())));
		objLocation.setMultiple_Items(trimValues(ObjLocationBean.getMultiple_Items()));
		objLocation.setSlotting(trimValues(ObjLocationBean.getSlotting()));
		objLocation.setStorage_Type(trimValues(ObjLocationBean.getStorage_Type()));					
		//return objLocation;
		Result=ObjectLocationMaintenanceDao.updateEntry(objLocation);								
		return Result;
	}


	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This Restfull Service is used to get the data from GENERAL_CODES.
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrGeneralCodeID
	 *@return List<GENERALCODES>
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenanace_Restfull_RestSelectGeneralCode, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODES>  getGeneralCode(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_GeneralCodeID) String StrGeneralCodeID) throws JASCIEXCEPTION {


		List<GENERALCODES> ListGeneralCodeId=null;

		ListGeneralCodeId = ObjectLocationMaintenanceDao.getGeneralCode(StrTenant,StrCompany,StrGeneralCodeID);				

		return ListGeneralCodeId;
	}

	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrGeneralCodeID
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenanace_Restfull_RestSelectLocationProfile, method = RequestMethod.GET)
	public @ResponseBody  List<LOCATIONPROFILES>  getLocationProfile(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FullfilmentCenter) String StrFullfilmentCenter) throws JASCIEXCEPTION {


		List<LOCATIONPROFILES> ListLocationProfile=ObjectLocationMaintenanceDao.getLocationProfile(StrTenant,StrCompany,SelectedFulfillmentCenterValue);
		List<LOCATIONPROFILES> ConvertListLocationProfile=new ArrayList<LOCATIONPROFILES>();
		for(int IterateLocationProfileList=GLOBALCONSTANT.INT_ZERO;IterateLocationProfileList<ListLocationProfile.size();IterateLocationProfileList++)	{
			LOCATIONPROFILES objLocationProfile=new LOCATIONPROFILES();
			LOCATIONPROFILESPK objLocationProfilepk=new LOCATIONPROFILESPK();
			try{
				objLocationProfilepk.setProfile(ListLocationProfile.get(IterateLocationProfileList).getLocationProfileId().getProfile().replace(GLOBALCONSTANT.DoubleQuote, GLOBALCONSTANT.StringDoubleQuote));						
				objLocationProfile.setDescription20(ListLocationProfile.get(IterateLocationProfileList).getDescription20());						
				objLocationProfile.setLocationProfileId(objLocationProfilepk);
			}
			catch(Exception objException){							
			}
			ConvertListLocationProfile.add(objLocationProfile);
		}								
		return ConvertListLocationProfile;
	}
	public   List<LOCATIONPROFILES>  getLocationProfileshow(String StrTenant,String StrCompany,String StrFullfilmentCenter) throws JASCIEXCEPTION {


		List<LOCATIONPROFILES> ListLocationProfile=ObjectLocationMaintenanceDao.getLocationProfile(StrTenant,StrCompany,SelectedFulfillmentCenterValue);
		List<LOCATIONPROFILES> ConvertListLocationProfile=new ArrayList<LOCATIONPROFILES>();
		for(int IterateLocationProfileList=GLOBALCONSTANT.INT_ZERO;IterateLocationProfileList<ListLocationProfile.size();IterateLocationProfileList++)	{
			LOCATIONPROFILES objLocationProfile=new LOCATIONPROFILES();					
			LOCATIONPROFILESPK objLocationProfilepk=new LOCATIONPROFILESPK();
			try{
				objLocationProfilepk.setProfile(ListLocationProfile.get(IterateLocationProfileList).getLocationProfileId().getProfile().replace(GLOBALCONSTANT.DoubleQuote, GLOBALCONSTANT.StringDoubleQuote));						
				objLocationProfile.setDescription20(ListLocationProfile.get(IterateLocationProfileList).getDescription20());						
				objLocationProfile.setLocationProfileId(objLocationProfilepk);
			}
			catch(Exception objException){							
			}
			ConvertListLocationProfile.add(objLocationProfile);
		}								
		return ConvertListLocationProfile;
	}
	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Dec 31, 2014
	 * Description:This function is used to set the value in Location Object.
	 *@param ObjLocationBean
	 *@param ObjLocationpk
	 *@param objLocation
	 *@return LOCATION
	 * @throws ParseException 
	 */
	public LOCATION useGetterSetterForAdd(LOCATIONBEAN ObjLocationBean,LOCATIONPK ObjLocationpk,LOCATION objLocation){
		ObjLocationpk.setTenant_ID(trimValues(objCommonsessionbe.getTenant()));
		ObjLocationpk.setCompany_ID(trimValues(objCommonsessionbe.getCompany()));
		//ObjLocationpk.setFullfillment_Center_ID(trimValues(ObjLocationBean.getFullfillment_Center_ID()));
		ObjLocationpk.setFullfillment_Center_ID(trimValues(ObjLocationBean.getFullfillment_Center_ID().replace(GLOBALCONSTANT.StringDoubleQuote, GLOBALCONSTANT.DoubleQuote)));
		ObjLocationpk.setLocation(trimValues(ObjLocationBean.getLocation()));
		ObjLocationpk.setArea(trimValues(ObjLocationBean.getArea()));		
		objLocation.setId(ObjLocationpk);
		//StringEscapeUtils.escapeHtml(trimValues(ObjLocationBean
		//Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
		//String testDateString = ObjDateFormat.format(CurrentDate);		

		try {
			objLocation.setLast_Activity_Date(ObjDateFormat.parse(ObjLocationBean.getLast_Activity_Date()));
		} catch (ParseException e) {

		}
		objLocation.setLast_Activity_Team_Member(trimValues(objCommonsessionbe.getTeam_Member()));
		objLocation.setLast_Activity_Task(trimValues(ObjLocationBean.getLast_Activity_Task()));
		objLocation.setLocation_Type(trimValues(ObjLocationBean.getLocation_Type()));
		objLocation.setDescription20(trimValues(ObjLocationBean.getDescription20()));
		objLocation.setDescription50(trimValues(ObjLocationBean.getDescription50()));
		objLocation.setLocation_Profile(trimValues(ObjLocationBean.getLocation_Profile().replace(GLOBALCONSTANT.StringDoubleQuote, GLOBALCONSTANT.DoubleQuote)));
		objLocation.setWizard_ID(trimValues(ObjLocationBean.getWizard_ID()));
		try{
			objLocation.setWizard_Control_Number(Long.parseLong(ObjLocationBean.getWizard_Control_Number()));
		}catch(Exception e)
		{
			objLocation.setWizard_Control_Number(GLOBALCONSTANT.LongZero);

		}
		//objLocation.setWizard_Control_Number(Long.parseLong(trimValues(ObjLocationBean.getWizard_Control_Number())));
		objLocation.setWork_Group_Zone(trimValues(ObjLocationBean.getWork_Group_Zone()));
		objLocation.setWork_Zone(trimValues(ObjLocationBean.getWork_Zone()));
		objLocation.setCheck_Digit(trimValues(ObjLocationBean.getCheck_Digit()));		
		objLocation.setAlternate_Location(trimValues(ObjLocationBean.getAlternate_Location()));
		try{
			objLocation.setLocation_Depth(Double.parseDouble(ObjLocationBean.getLocation_Depth()));
		}catch(Exception e)
		{
			objLocation.setLocation_Depth(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Depth(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Depth())));
		try{
			objLocation.setLocation_Height(Double.parseDouble(ObjLocationBean.getLocation_Height()));
		}catch(Exception e)
		{
			objLocation.setLocation_Height(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Height(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Height())));
		try{
			objLocation.setLocation_Width(Double.parseDouble(ObjLocationBean.getLocation_Width()));
		}catch(Exception e)
		{
			objLocation.setLocation_Width(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Width(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Width())));
		try{
			objLocation.setLocation_Height_Capacity(Double.parseDouble(ObjLocationBean.getLocation_Height_Capacity()));
		}catch(Exception e)
		{
			objLocation.setLocation_Height_Capacity(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Height_Capacity(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Height_Capacity())));
		try{
			objLocation.setLocation_Weight_Capacity(Double.parseDouble(ObjLocationBean.getLocation_Weight_Capacity()));
		}catch(Exception e)
		{
			objLocation.setLocation_Weight_Capacity(GLOBALCONSTANT.DecimalZero);

		}
		//objLocation.setLocation_Weight_Capacity(Double.parseDouble(trimValues(ObjLocationBean.getLocation_Weight_Capacity())));
		try{
			objLocation.setNumber_Pallets_Fit(Long.parseLong(ObjLocationBean.getNumber_Pallets_Fit()));
		}catch(Exception e)
		{
			objLocation.setNumber_Pallets_Fit(GLOBALCONSTANT.LongZero);

		}

		//objLocation.setNumber_Pallets_Fit(Long.parseLong(trimValues(ObjLocationBean.getNumber_Pallets_Fit())));
		try{
			objLocation.setNumber_Floor_Locations(Long.parseLong(ObjLocationBean.getNumber_Floor_Locations()));
		}catch(Exception e)
		{
			objLocation.setNumber_Floor_Locations(GLOBALCONSTANT.LongZero);

		}

		//objLocation.setNumber_Floor_Locations(Long.parseLong(trimValues(ObjLocationBean.getNumber_Floor_Locations())));
		objLocation.setAllocation_Allowable(trimValues(ObjLocationBean.getAllocation_Allowable()));
		objLocation.setFree_Location_When_Zero(trimValues(ObjLocationBean.getFree_Location_When_Zero()));
		try{
			objLocation.setFree_Prime_Days(Long.parseLong(ObjLocationBean.getFree_Prime_Days()));
		}catch(Exception e)
		{
			objLocation.setFree_Prime_Days(GLOBALCONSTANT.LongZero);

		}

		//objLocation.setFree_Prime_Days(Long.parseLong(trimValues(ObjLocationBean.getFree_Prime_Days())));
		objLocation.setMultiple_Items(trimValues(ObjLocationBean.getMultiple_Items()));
		objLocation.setSlotting(trimValues(ObjLocationBean.getSlotting()));
		objLocation.setStorage_Type(trimValues(ObjLocationBean.getStorage_Type()));					
		return objLocation;
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 31, 2014
	 * Description This function is used to trim the space
	 * @param Value
	 * @return String
	 */
	public static String trimValues(String Value){
		if(Value == null){
			return null;
		}
		else{
			String CheckValue=Value.trim();
			if(CheckValue.length()<GLOBALCONSTANT.IntOne){
				return null;
			}else{						
				return CheckValue;
			}
		}
	}

	/**
	 * @Description it is used to get team member name from TeamMembers
	 * @author Aakash Bishnoi
	 * @date 31 dec 2014
	 * @param Tenant
	 * @param TeamMember
	 * @return List<TEAMMEMBERS>
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenance_Restfull_GetTeamMember, method = RequestMethod.GET)
	public @ResponseBody String getTeamMemberName(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_TeamMember) String TeamMember) throws JASCIEXCEPTION {	


		//return ObjectLocationMaintenanceDao.getTeamMemberName(Tenant, TeamMember);
		String TeamMemberName=GLOBALCONSTANT.BlankString;

		List<TEAMMEMBERS> objTeammembers= ObjectLocationMaintenanceDao.getTeamMemberName(Tenant, TeamMember);

		TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
		return TeamMemberName;


	}
	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 2, 2015
	 * Description:It is used to check uniqueness of Location in LOCATIONS TABLE before add record
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@return Boolean
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Restfull_CheckUniqueLocation, method = RequestMethod.POST)
	public @ResponseBody Boolean getLocationUnique(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FulfillmentCenter) String StrFulfillmentCenter,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String StrLocation)  throws JASCIEXCEPTION {	

		List<LOCATION> ObjLocation=null;

		ObjLocation = ObjectLocationMaintenanceDao.getLocationUnique(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrFulfillmentCenter,StrArea,StrLocation); 

		if(ObjLocation.size()>GLOBALCONSTANT.INT_ZERO){
			return true;					
		}
		else{
			return false;
		}
	}

	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 2, 2015
	 * Description:It is used to check uniqueness of Alternate Location In LOCATIONS TABLE befor Add record
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@param StrAlternateLocation
	 *@return Boolean
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	/*@RequestMapping(value = GLOBALCONSTANT.Location_Restfull_CheckUniqueAlternateLocation, method = RequestMethod.GET)
	public @ResponseBody Boolean getAlternateLocationUnique(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FulfillmentCenter) String StrFulfillmentCenter,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String StrLocation,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Alternate_Location) String StrAlternateLocation)  throws JASCIEXCEPTION {	
	 */
	@RequestMapping(value = GLOBALCONSTANT.Location_Restfull_CheckUniqueAlternateLocation, method = RequestMethod.POST)
	public @ResponseBody Boolean getAlternateLocationUnique(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FulfillmentCenter) String StrFulfillmentCenter,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Alternate_Location) String StrAlternateLocation)  throws JASCIEXCEPTION {
		List<LOCATION> ObjLocation=null;

		//ObjLocation = ObjectLocationMaintenanceDao.getAlternateLocationUnique(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrFulfillmentCenter,StrArea,StrLocation,StrAlternateLocation); 
		ObjLocation = ObjectLocationMaintenanceDao.getAlternateLocationUnique(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrFulfillmentCenter,StrAlternateLocation);
		if(ObjLocation.size()>GLOBALCONSTANT.INT_ZERO){
			return true;					
		}
		else{
			return false;
		}
	}


	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 3, 2015
	 * Description:This is used to search the list from LOCATION on Location Search Screen.
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrDescription20
	 *@param StrDescription50
	 *@param StrLocationType
	 *@return List<LOCATION> 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Restfull_Search, method = RequestMethod.GET)
	public @ResponseBody List<LOCATIONBEAN> getList(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String StrLocation,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FulfillmentCenter) String StrFulfillmentCenter,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Description) String StrDescription,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_LocationType) String StrLocationType)  throws JASCIEXCEPTION {	

		

		List<Object> ListLocationAll=ObjectLocationMaintenanceDao.getList(StrLocation,objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),objCommonsessionbe.getFulfillmentCenter(),StrArea,StrDescription,StrLocationType);
		List<LOCATIONBEAN> ReturnLocationList=new ArrayList<LOCATIONBEAN>();
		
		
		for(Iterator<Object> LocationListLength=ListLocationAll.iterator();LocationListLength.hasNext(); ){

			LOCATIONBEAN ObjectLocation=new LOCATIONBEAN();

			Object []ObjectLocObj =  (Object[]) LocationListLength.next();

			try{
				ObjectLocation.setArea(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				ObjectLocation.setArea_Description(ObjectLocObj[GLOBALCONSTANT.IntOne].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
 				ObjectLocation.setFullfillment_Center_ID(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				ObjectLocation.setFullfillment_Center_ID_Description(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				ObjectLocation.setLocation(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				ObjectLocation.setDescription50(ObjectLocObj[GLOBALCONSTANT.INT_FIVE].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				ObjectLocation.setLocation_Type_Description(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				
				String lastDate=ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString();
				 ObjectLocation.setLast_Activity_Date(lastDate);
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				ObjectLocation.setLast_Activity_Team_Member(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				ObjectLocation.setLast_Activity_Task_Description(ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}
			try{
				
				String notes=ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString();
				  StringBuilder ObjectStringBuilder = new StringBuilder();				  
				  for(int NotesStartIndex=GLOBALCONSTANT.INT_ZERO,NotesLastIndex=GLOBALCONSTANT.INT_ZERO;NotesStartIndex<notes.length();NotesStartIndex++,NotesLastIndex++)
				  {
				   if(notes.charAt(NotesStartIndex)!=GLOBALCONSTANT.BlankSpace && NotesLastIndex==GLOBALCONSTANT.INT_TWENTYFOUR)
				   {
				    ObjectStringBuilder.append(GLOBALCONSTANT.Single_Space+notes.charAt(NotesStartIndex));
				    NotesLastIndex=GLOBALCONSTANT.INT_ZERO;
				   }else if(notes.charAt(NotesStartIndex)==GLOBALCONSTANT.BlankSpace)
				   {
				    ObjectStringBuilder.append(notes.charAt(NotesStartIndex));
				    NotesLastIndex=GLOBALCONSTANT.INT_ZERO;
				   }
				    else{
				    ObjectStringBuilder.append(notes.charAt(NotesStartIndex));
				   }
				  }
				ObjectLocation.setNotes(ObjectStringBuilder.toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception obj_e)	{}

			ReturnLocationList.add(ObjectLocation);
		}
	
		return ReturnLocationList;
	}	

	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2014
	 * Description This is used to convert date format
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return String
	 */
	public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
		DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
		String StrFormattedString=dateInString ;

		try {

			Date date = formatter.parse(dateInString);
			StrFormattedString=dateFormat.format(date);

		} catch (Exception e) {

		}

		return StrFormattedString;

	}

	/**	
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description:It is used to Fetch the record form Location table on the behalf of parameters and conver it in pojo to bean
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@return LOCATIONBEAN
	 *@throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Restfull_FetchLocation, method = RequestMethod.GET)
	public @ResponseBody LOCATIONBEAN getFetchLocation(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FulfillmentCenter) String StrFulfillmentCenter,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String StrLocation)  throws JASCIEXCEPTION {
		LOCATIONBEAN ObjLocationBean=new LOCATIONBEAN(); 
		LOCATION ObjLocation = ObjectLocationMaintenanceDao.getFetchLocation(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrFulfillmentCenter,StrArea,StrLocation);

		ObjLocationBean.setAllocation_Allowable(StringEscapeUtils.escapeHtml(ObjLocation.getAllocation_Allowable()));
		ObjLocationBean.setAlternate_Location(StringEscapeUtils.escapeHtml(ObjLocation.getAlternate_Location()));
		ObjLocationBean.setArea(StringEscapeUtils.escapeHtml(ObjLocation.getId().getArea()));
		ObjLocationBean.setCheck_Digit(StringEscapeUtils.escapeHtml(ObjLocation.getCheck_Digit()));
		ObjLocationBean.setCompany_ID(StringEscapeUtils.escapeHtml(ObjLocation.getId().getCompany_ID()));
		ObjLocationBean.setDescription20(StringEscapeUtils.escapeHtml(ObjLocation.getDescription20()));
		ObjLocationBean.setDescription50(StringEscapeUtils.escapeHtml(ObjLocation.getDescription50()));
		ObjLocationBean.setFree_Location_When_Zero(StringEscapeUtils.escapeHtml(ObjLocation.getFree_Location_When_Zero()));
		if(ObjLocation.getFree_Prime_Days()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setFree_Prime_Days(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=ObjLocation.getFree_Prime_Days().toString();
			ObjLocationBean.setFree_Prime_Days(dvalue);
		}
		//ObjLocationBean.setFree_Prime_Days(ObjLocation.getFree_Prime_Days().toString());
		ObjLocationBean.setFullfillment_Center_ID(StringEscapeUtils.escapeHtml(ObjLocation.getId().getFullfillment_Center_ID()));
		String StrLast_Activity_date=ConvertDate(ObjLocation.getLast_Activity_Date().toString(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format);
		ObjLocationBean.setLast_Activity_Date(StrLast_Activity_date);
		ObjLocationBean.setLast_Activity_Task(StringEscapeUtils.escapeHtml(ObjLocation.getLast_Activity_Task()));		
		String StrTeamMember=getTeamMemberName(objCommonsessionbe.getTenant(), ObjLocation.getLast_Activity_Team_Member());
		ObjLocationBean.setLast_Activity_Team_Member(StrTeamMember);
		ObjLocationBean.setLocation(StringEscapeUtils.escapeHtml(ObjLocation.getId().getLocation()));
		if(ObjLocation.getLocation_Depth()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setLocation_Depth(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=BigDecimal.valueOf(ObjLocation.getLocation_Depth()).toPlainString();
			ObjLocationBean.setLocation_Depth(dvalue);
		}
		//ObjLocationBean.setLocation_Depth(ObjLocation.getLocation_Depth().toString());
		if(ObjLocation.getLocation_Height()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setLocation_Height(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=BigDecimal.valueOf(ObjLocation.getLocation_Height()).toPlainString();
			ObjLocationBean.setLocation_Height(dvalue);
		}
		//ObjLocationBean.setLocation_Height(ObjLocation.getLocation_Height().toString());
		if(ObjLocation.getLocation_Width()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setLocation_Width(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=BigDecimal.valueOf(ObjLocation.getLocation_Width()).toPlainString();
			ObjLocationBean.setLocation_Width(dvalue);
		}
		//ObjLocationBean.setLocation_Width(ObjLocation.getLocation_Width().toString());
		if(ObjLocation.getLocation_Height_Capacity()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setLocation_Height_Capacity(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=BigDecimal.valueOf(ObjLocation.getLocation_Height_Capacity()).toPlainString();
			ObjLocationBean.setLocation_Height_Capacity(dvalue);
		}

		//ObjLocationBean.setLocation_Height_Capacity(ObjLocation.getLocation_Height_Capacity().toString());
		ObjLocationBean.setLocation_Profile(StringEscapeUtils.escapeHtml(ObjLocation.getLocation_Profile()));
		ObjLocationBean.setLocation_Type(StringEscapeUtils.escapeHtml(ObjLocation.getLocation_Type()));
		if(ObjLocation.getLocation_Weight_Capacity()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setLocation_Weight_Capacity(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=BigDecimal.valueOf(ObjLocation.getLocation_Weight_Capacity()).toPlainString();
			ObjLocationBean.setLocation_Weight_Capacity(dvalue);
		}
		//ObjLocationBean.setLocation_Weight_Capacity(ObjLocation.getLocation_Weight_Capacity().toString());
		//ObjLocationBean.setLast_Activity_Date(StringEscapeUtils.escapeHtml(ObjLocation.getLast_Activity_Date()));
		ObjLocationBean.setMultiple_Items(StringEscapeUtils.escapeHtml(ObjLocation.getMultiple_Items()));
		if(ObjLocation.getNumber_Floor_Locations()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setNumber_Floor_Locations(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=ObjLocation.getNumber_Floor_Locations().toString();
			ObjLocationBean.setNumber_Floor_Locations(dvalue);
		}
		//ObjLocationBean.setNumber_Floor_Locations(ObjLocation.getNumber_Floor_Locations().toString());

		if(ObjLocation.getNumber_Pallets_Fit()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setNumber_Pallets_Fit(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=ObjLocation.getNumber_Pallets_Fit().toString();
			ObjLocationBean.setNumber_Pallets_Fit(dvalue);
		}
		//ObjLocationBean.setNumber_Pallets_Fit(ObjLocation.getNumber_Pallets_Fit().toString());


		ObjLocationBean.setSlotting(StringEscapeUtils.escapeHtml(ObjLocation.getSlotting()));
		ObjLocationBean.setStorage_Type(StringEscapeUtils.escapeHtml(ObjLocation.getStorage_Type()));

		if(ObjLocation.getWizard_Control_Number()==null || ObjLocation.getWizard_Control_Number()==GLOBALCONSTANT.INT_ZERO)
		{
			ObjLocationBean.setWizard_Control_Number(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=ObjLocation.getWizard_Control_Number().toString();
			ObjLocationBean.setWizard_Control_Number(dvalue);
		}
		//ObjLocationBean.setWizard_Control_Number(ObjLocation.getWizard_Control_Number().toString());
		ObjLocationBean.setWizard_ID(StringEscapeUtils.escapeHtml(ObjLocation.getWizard_ID()));
		ObjLocationBean.setWork_Group_Zone(StringEscapeUtils.escapeHtml(ObjLocation.getWork_Group_Zone()));
		ObjLocationBean.setWork_Zone(StringEscapeUtils.escapeHtml(ObjLocation.getWork_Zone()));
		ObjLocationBean.setTenant_ID(StringEscapeUtils.escapeHtml(ObjLocation.getId().getTenant_ID()));
		return ObjLocationBean;
	}
	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 6, 2015
	 * Description:it is used to check location exist in INVENTRY_LEVEL TABLE or not
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Location_Restfull_LocationExistForDelete, method = RequestMethod.GET)
	public @ResponseBody JSONObject getLocationExistForDelete(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FulfillmentCenter) String StrFulfillmentCenter,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String StrLocation)  throws JASCIEXCEPTION {
		String Result=GLOBALCONSTANT.BlankString;
		INVENTORY_LEVELS objInventory_Level=null;
		objInventory_Level=ObjectLocationMaintenanceDao.getLocationExistForDelete(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(), StrFulfillmentCenter, StrArea, StrLocation);

		if(objInventory_Level.getQuantity() == GLOBALCONSTANT.INT_ZERO) {
			Result= GLOBALCONSTANT.LocationMaintenance_Response_Zero;
		}
		else if(objInventory_Level.getQuantity() > GLOBALCONSTANT.INT_ZERO) {
			Result= GLOBALCONSTANT.LocationMaintenance_Response_One;
		}
		else if(objInventory_Level.getQuantity() == GLOBALCONSTANT.INT_MINUSONE) {
			Result= GLOBALCONSTANT.LocationMaintenance_Response_Delete;
		}

		JSONObject objJsonObject = new JSONObject();
		objJsonObject.put(GLOBALCONSTANT.Result, Result);

		return objJsonObject;
	}


	/**
	 * Created on:Jan 05  2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is used to get fulfillmentcenterId and FulfillmentCenter name.
	 * Input parameter: String
	 * Return Type :List<FULFILLMENTCENTERSPOJO>
	 * 
	 */
	@Transactional
	public List<FULFILLMENTCENTERSPOJO> getFulFillmentCenter(String tenant,String company,String PageName) throws JASCIEXCEPTION
	{
		/**call LocationMaintenanceDao class method for getting FulFilment center info */

		List<Object> objListObject=ObjectLocationMaintenanceDao.getfulFillmentCenter(tenant, company,PageName);		
		List<FULFILLMENTCENTERSPOJO> ObjListFulfillmentcenter=new ArrayList<FULFILLMENTCENTERSPOJO>();
		for (Object object : objListObject) 
		{
			Object[] object_arr = (Object[]) object;
			FULFILLMENTCENTERSPOJO FulfilmentcenterObj = new FULFILLMENTCENTERSPOJO();
			FULFILLMENTCENTERS_PK fulfillmentcenters_PK=new FULFILLMENTCENTERS_PK();

			try{
				fulfillmentcenters_PK.setFulfillmentCenter(object_arr[GLOBALCONSTANT.INT_ZERO].toString().replace(GLOBALCONSTANT.DoubleQuote, GLOBALCONSTANT.StringDoubleQuote));
				fulfillmentcenters_PK.setTenant(objCommonsessionbe.getTenant());
				FulfilmentcenterObj.setId(fulfillmentcenters_PK);

			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception e)
			{}
			try{
				FulfilmentcenterObj.setName20(object_arr[GLOBALCONSTANT.IntOne].toString());
			}catch(ArrayIndexOutOfBoundsException objException){}
			catch(Exception e)
			{ 
				FulfilmentcenterObj.setName20(GLOBALCONSTANT.BlankString);
			}
			ObjListFulfillmentcenter.add(FulfilmentcenterObj);
		}


		return ObjListFulfillmentcenter;
	}
	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 6, 2015
	 * Description:It is used to check location exist in INVENTRY_LEVEL TABLE or not
	 * @param StrMessage_ID
	 * @param StrTenant_ID
	 * @param StrCompany_ID
	 * @param StrStatus
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenance_Restfull_Delete, method = RequestMethod.POST)
	public @ResponseBody  Boolean deleteEntry(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FulfillmentCenter) String StrFulfillmentCenter,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Area) String StrArea,@RequestParam(value=GLOBALCONSTANT.Location_Restfull_Location) String StrLocation)  throws JASCIEXCEPTION {	
		Boolean Result=false;


		/**call LocationMaintenanceDao class method for deleting exists record */

		Result= ObjectLocationMaintenanceDao.deleteEntry(StrTenant,StrCompany,StrFulfillmentCenter,StrArea,StrLocation);


		return Result;
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 10 ,2015
	 * Description:It is used to get the selected fulfillment center for showing the location profile dropdown.
	 * @param StrFullfilmentCenter
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenanace_Restfull_RestSelectFulfillmentCenterValue, method = RequestMethod.GET)
	public @ResponseBody  List<LOCATIONPROFILES>  getFulfillmentValue(@RequestParam(value=GLOBALCONSTANT.Location_Restfull_FullfilmentCenter) String StrFullfilmentCenter) throws JASCIEXCEPTION {
		SelectedFulfillmentCenterValue=StrFullfilmentCenter;
		List<LOCATIONPROFILES> ConvertListLocationProfile= getLocationProfileshow(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrFullfilmentCenter);
		return ConvertListLocationProfile;
	}
}
