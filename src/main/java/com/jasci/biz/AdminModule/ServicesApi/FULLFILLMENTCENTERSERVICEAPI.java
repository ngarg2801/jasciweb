/*

Description This class is used to make restfull service for FULFILLMENT CENTER
Created By Deepak Sharma
Created Date Feb 5 2015

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jasci.biz.AdminModule.dao.FULLFILLMENTCENTERDAO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;

import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class FULLFILLMENTCENTERSERVICEAPI {

	@Autowired
	FULLFILLMENTCENTERDAO objFulfillmentCenterDAO;
	@Autowired
	COMMONSESSIONBE ObjCommonSessionBe;
	
	/**
	 * 
	 * @param StrSelectedText
	 * @param IsSearchByFullfillmentID
	 * @param IsSearchByPartOfFullfillMentCenterName
	 * @param IsSearchByPartOfFullfillment
	 * @param StrTenant
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_SearchFullfillment, method = RequestMethod.POST)
	public @ResponseBody List<FULFILLMENTCENTERSPOJO> searchForfulfillmentCenter(
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_Fullfillment) String StrSelectedText,
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_IsSearchByFullfillmentID) boolean IsSearchByFullfillmentID,
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_IsSearchByPartOfFullfillMentCenterName) boolean IsSearchByPartOfFullfillMentCenterName,
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_IsSearchByPartOfFullfillment) boolean IsSearchByPartOfFullfillment,
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_Tenant) String StrTenant) throws JASCIEXCEPTION {

		List<FULFILLMENTCENTERSPOJO> objFullfillmentCenter = null;

		if (IsSearchByFullfillmentID) {
			objFullfillmentCenter = objFulfillmentCenterDAO.getfulfillmentcenterbyfulfillment(StrSelectedText,
					StrTenant);
		} else if (IsSearchByPartOfFullfillMentCenterName) {
			objFullfillmentCenter = objFulfillmentCenterDAO.getfulfillmentcenterbyPartOffulfillmentName(
					StrSelectedText, StrTenant);
		}
		return objFullfillmentCenter;
	}

	
	/**
	 * 
	 * @param StrTenant
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_DisplayAllFulfillmentAPI, method = RequestMethod.POST)
	public @ResponseBody List<FULFILLMENTCENTERSPOJO> getAllFulfillment(
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_Tenant) String StrTenant)
			throws JASCIEXCEPTION {
		List<FULFILLMENTCENTERSPOJO> objFullfillmentCenter = null;
		objFullfillmentCenter = objFulfillmentCenterDAO.getAllFulfillment(StrTenant);
		return objFullfillmentCenter;
	}

	/**
	 * 
	 * @param StrTenant
	 * @param StrFullfillment
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value =GLOBALCONSTANT.Fullfullment_M_DeleteFullfillment, method = RequestMethod.POST)
	public @ResponseBody WEBSERVICESTATUS deleteFulfillment(
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_Tenant) String StrTenant,
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_Fullfillment) String StrFullfillment) throws JASCIEXCEPTION {
		WEBSERVICESTATUS objWEBSERVICESTATUS = new WEBSERVICESTATUS();
		int IntExecuteUpdate;
		IntExecuteUpdate = objFulfillmentCenterDAO.deleteFulfillment(StrTenant, StrFullfillment);

		if (IntExecuteUpdate == GLOBALCONSTANT.IntOne) {
			objWEBSERVICESTATUS.setBoolStatus(true);
			objWEBSERVICESTATUS.setStrStatus(GLOBALCONSTANT.Security_WebServiceSuccessStatus);

		} else if (IntExecuteUpdate == GLOBALCONSTANT.INT_ZERO) {
			objWEBSERVICESTATUS.setBoolStatus(false);
			objWEBSERVICESTATUS.setStrStatus(GLOBALCONSTANT.Security_WebServiceErrorStatus);
		}

		return objWEBSERVICESTATUS;
	}

	/**
	 * 
	 * @param StrTenant
	 * @param StrFullfillment
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_IsFulfillmentAvailable, method = RequestMethod.POST)
	public @ResponseBody WEBSERVICESTATUS IsFulfillmentAvailable(
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_Tenant) String StrTenant,
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_Fullfillment) String StrFullfillment) throws JASCIEXCEPTION {

		List<FULFILLMENTCENTERSPOJO> lstFullfillmentCenter = null;
		WEBSERVICESTATUS objWEBSERVICESTATUS = new WEBSERVICESTATUS();
		lstFullfillmentCenter = objFulfillmentCenterDAO.isFulfillmentAvailable(StrTenant, StrFullfillment);

		if (lstFullfillmentCenter.size() > GLOBALCONSTANT.INT_ZERO) {
   objWEBSERVICESTATUS.setBoolStatus(true);
   objWEBSERVICESTATUS.setStrStatus(GLOBALCONSTANT.Security_WebServiceSuccessStatus);
   if(lstFullfillmentCenter.get(GLOBALCONSTANT.INT_ZERO).getSTATUS().equalsIgnoreCase(GLOBALCONSTANT.Status_Delete))
   {
    objWEBSERVICESTATUS.setStrMessage(GLOBALCONSTANT.Status_Delete);
   }
   else
   {
    objWEBSERVICESTATUS.setStrMessage(GLOBALCONSTANT.Status_Active);
   }
  } else {
   objWEBSERVICESTATUS.setBoolStatus(false);
   objWEBSERVICESTATUS.setStrStatus(GLOBALCONSTANT.Security_WebServiceErrorStatus);
   
  }

		return objWEBSERVICESTATUS;
	}

	/**
	 * 
	 * @param objFulfillmentcenterspojo
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_addUpdateFulfillment, method = RequestMethod.POST)
	public @ResponseBody void AddUpdateFullfillmentCenter(
			@RequestParam(value = GLOBALCONSTANT.Fullfullment_R_P_FulfillmentPojo) FULFILLMENTCENTERSPOJO objFulfillmentcenterspojo)
			throws JASCIEXCEPTION {

		objFulfillmentCenterDAO.updateFulfillment(objFulfillmentcenterspojo);

		// return returnObjWEBSERVICESTATUS;

	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Name20 in FULFILLMENTCENTERS TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description20
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Fulfillment_Restfull_RestCheckUniqueFulfillmentDescriptionShort, method = RequestMethod.POST)
	public @ResponseBody Boolean getDescription20Unique(@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String StrFulFillmentCenter,@RequestParam(value=GLOBALCONSTANT.RestParam_Description20) String StrDescription20,@RequestParam(value=GLOBALCONSTANT.ScreenName) String StrScreenName)  throws JASCIEXCEPTION {	

		List<FULFILLMENTCENTERSPOJO> lstFullfillmentCenter=null;

		lstFullfillmentCenter = objFulfillmentCenterDAO.getDescription20Unique(ObjCommonSessionBe.getTenant(),StrFulFillmentCenter,StrDescription20,StrScreenName); 
		
			if(lstFullfillmentCenter.size()>GLOBALCONSTANT.INT_ZERO){
				return true;				
			}
			else{
				return false;
			}
	}
	

	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Name50 in FULFILLMENTCENTERS TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description50
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Fulfillment_Restfull_RestCheckUniqueFulfillmentDescriptionLong, method = RequestMethod.POST)
	public @ResponseBody Boolean getDescription50Unique(@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String StrFulFillmentCenter,@RequestParam(value=GLOBALCONSTANT.RestParam_Description50) String StrDescription50,@RequestParam(value=GLOBALCONSTANT.ScreenName) String StrScreenName)  throws JASCIEXCEPTION {	

		List<FULFILLMENTCENTERSPOJO> lstFullfillmentCenter=null;

		lstFullfillmentCenter = objFulfillmentCenterDAO.getDescription50Unique(ObjCommonSessionBe.getTenant(),StrFulFillmentCenter,StrDescription50,StrScreenName); 
		
			if(lstFullfillmentCenter.size()>GLOBALCONSTANT.INT_ZERO){
				return true;				
			}
			else{
				return false;
			}
		
	}	
}
