/**

Date Developed  Nov 16 2014
Created by:Diksha Gupta
Description RESTTEAMMEMBERMESSAGES is a restFullservice.
*/
package com.jasci.biz.AdminModule.ServicesApi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESBE;
import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESSCREENBE;
import com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL;
import com.jasci.biz.AdminModule.dao.ITEAMMEMBERMESSAGESDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGESPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class RESTTEAMMEMBERMESSAGES 
{
	@Autowired
	 private ITEAMMEMBERMESSAGESDAO ObjectTeamMemberMessagesDao;
	@Autowired
	COMMONSESSIONBE ObjCommonSessionBe;
	@Autowired
	LANGUANGELABELSAPI ObjLanguageLabelsApi;
	@Autowired
	IMENUAPPICONDAL ObjMenuAppIconDal;
	 
	 /**
		 *Description This function is designed to add messages into  TeamMemberMessages table
		 *Input parameter objTeamMemberMessagesbe
		 *Return Type Boolean
		 *Created By "Diksha Gupta"
		 *Created Date "nov 22 2014" 
		 * */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_AddMessage, method = RequestMethod.GET)
	public @ResponseBody Boolean InsertRow(TEAMMEMBERMESSAGESBE objTeamMemberMessagesbe,String [] StrTeammemberList) throws JASCIEXCEPTION 	
	{
	
		Boolean Status=false;

		if(objTeamMemberMessagesbe.getTicketTapeMessage()==null)
		{
			objTeamMemberMessagesbe.setTicketTapeMessage(GLOBALCONSTANT.BlankString);
		}
		if(objTeamMemberMessagesbe.getMessage1()==null)
		{
			objTeamMemberMessagesbe.setMessage1(GLOBALCONSTANT.BlankString);
		}
			if(StrTeammemberList!=null)
		     {
				
					for(int TeamMemberMessageIndex=GLOBALCONSTANT.INT_ZERO;TeamMemberMessageIndex<StrTeammemberList.length;TeamMemberMessageIndex++)
			          {
						
						if(!objTeamMemberMessagesbe.getTicketTapeMessage().equals(GLOBALCONSTANT.BlankString))
						{
							TEAMMEMBERMESSAGESPK objTeamMemberMessagesPk= new TEAMMEMBERMESSAGESPK();
						TEAMMEMBERMESSAGES objTeammemberMessages=new TEAMMEMBERMESSAGES();
						objTeamMemberMessagesPk.setTeamMember(StrTeammemberList[TeamMemberMessageIndex]);
			          //	objTeammemberMessages.setMessage(objTeamMemberMessagesbe.getMessage1());
			    	    objTeammemberMessages.setTicketTapeMessge(objTeamMemberMessagesbe.getTicketTapeMessage());
			    		objTeamMemberMessagesPk.setMesageNumber(UUID.randomUUID().toString());

		        		 objTeammemberMessages=useGetterSetter(objTeamMemberMessagesPk,objTeammemberMessages,objTeamMemberMessagesbe);
		        		 Status= ObjectTeamMemberMessagesDao.InsertRow(objTeammemberMessages);
		        		}
						
						if(!objTeamMemberMessagesbe.getMessage1().equals(GLOBALCONSTANT.BlankString))
						{
							TEAMMEMBERMESSAGESPK objTeamMemberMessagesPk= new TEAMMEMBERMESSAGESPK();
						TEAMMEMBERMESSAGES objTeammemberMessages=new TEAMMEMBERMESSAGES();
						objTeamMemberMessagesPk.setTeamMember(StrTeammemberList[TeamMemberMessageIndex]);
			          	objTeammemberMessages.setMessage(objTeamMemberMessagesbe.getMessage1());
			    	    //objTeammemberMessages.setTicketTapeMessge(objTeamMemberMessagesbe.getTicketTapeMessage());
			    		objTeamMemberMessagesPk.setMesageNumber(UUID.randomUUID().toString());

		        		 objTeammemberMessages=useGetterSetter(objTeamMemberMessagesPk,objTeammemberMessages,objTeamMemberMessagesbe);
		        		 Status= ObjectTeamMemberMessagesDao.InsertRow(objTeammemberMessages);
		        		}
			          }
					
				}
		
		else
		{		
			
			if(!objTeamMemberMessagesbe.getTicketTapeMessage().equals(GLOBALCONSTANT.BlankString))
			{
			TEAMMEMBERMESSAGESPK objTeamMemberMessagesPk= new TEAMMEMBERMESSAGESPK();
			TEAMMEMBERMESSAGES objTeammemberMessages=new TEAMMEMBERMESSAGES();
			objTeamMemberMessagesPk.setTeamMember(objTeamMemberMessagesbe.getTeamMember());
    	    objTeammemberMessages.setTicketTapeMessge(objTeamMemberMessagesbe.getTicketTapeMessage());
    		objTeamMemberMessagesPk.setMesageNumber(UUID.randomUUID().toString());

    		 objTeammemberMessages=useGetterSetter(objTeamMemberMessagesPk,objTeammemberMessages,objTeamMemberMessagesbe);
    		 Status= ObjectTeamMemberMessagesDao.InsertRow(objTeammemberMessages);
    		}
			
			if(!objTeamMemberMessagesbe.getMessage1().equals(GLOBALCONSTANT.BlankString))
			{
				TEAMMEMBERMESSAGESPK objTeamMemberMessagesPk= new TEAMMEMBERMESSAGESPK();
			TEAMMEMBERMESSAGES objTeammemberMessages=new TEAMMEMBERMESSAGES();
			objTeamMemberMessagesPk.setTeamMember(objTeamMemberMessagesbe.getTeamMember());
          	objTeammemberMessages.setMessage(objTeamMemberMessagesbe.getMessage1());
    		objTeamMemberMessagesPk.setMesageNumber(UUID.randomUUID().toString());

    		 objTeammemberMessages=useGetterSetter(objTeamMemberMessagesPk,objTeammemberMessages,objTeamMemberMessagesbe);
    		 Status= ObjectTeamMemberMessagesDao.InsertRow(objTeammemberMessages);
    		}
			
			
		}
			
		
		return Status;
	}
	
	
	
	private TEAMMEMBERMESSAGES useGetterSetter(TEAMMEMBERMESSAGESPK objTeamMemberMessagesPk, TEAMMEMBERMESSAGES objTeammemberMessages, TEAMMEMBERMESSAGESBE objTeamMemberMessagesbe) 
	{
		  Date startDate=null;
		  Date Enddate=null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
			startDate=formatter.parse(objTeamMemberMessagesbe.getStartDate());
			  Enddate=formatter.parse(objTeamMemberMessagesbe.getEndDate());
			
			Date LastActivitydate = formatter.parse(objTeamMemberMessagesbe.getLastActivityDate());
			objTeammemberMessages.setLastActivityDate(LastActivitydate);

		}catch(Exception objException){}
		
		
   		 
		objTeammemberMessages.setStartDate(startDate);
		objTeammemberMessages.setEndDate(Enddate);
		
		objTeammemberMessages.setStatus(GLOBALCONSTANT.Status_Active);
		//objTeammemberMessages.setLastActivityDate(LastActivitydate);
		objTeammemberMessages.setLastActivityTeamMember(ObjCommonSessionBe.getTeam_Member());
		//objTeamMemberMessagesPk.setMesageNumber(UUID.randomUUID().toString());
        objTeamMemberMessagesPk.setCompany(ObjCommonSessionBe.getCompany());
        objTeamMemberMessagesPk.setTenant(ObjCommonSessionBe.getTenant());
		objTeammemberMessages.setTeamMemberMessagesId(objTeamMemberMessagesPk);
		return objTeammemberMessages;
	}

	
	
	
	/**
	 *Description This function design to get TeamMember data from TeamMember table
	 *Input parameter TeamMemberName,PartOfTeamMember,StrDepartment,StrShift,Tenant
	 *Return Type List<TEAMMEMBERSBE>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 25 2014" 
	 * */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMemberList,method = RequestMethod.GET)
	public List<TEAMMEMBERSBE> getTeamMemberList(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMember)String strTeamMember, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_PartTeamMember)String strPartTeamMember, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Department)String strDepartment, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Shift)String strShift, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Tenant)String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Company) String StrCompany) throws JASCIEXCEPTION
	{
		List<TEAMMEMBERSBE> ObjListTeamMembersBe=ObjectTeamMemberMessagesDao.getTeamMemberList(strTeamMember, strPartTeamMember,strDepartment,strShift,Tenant,StrCompany);
		return ObjListTeamMembersBe;
	}
	

	/**
	 *Description This function design to get TeamMembermessages 
	 *Input parameter TeamMember
	 *Return Type List<TEAMMEMBERMESSAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMemberMessagesList,method = RequestMethod.GET)
	public List<TEAMMEMBERMESSAGES> getMessages(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMember)String StrTeamMember) throws JASCIEXCEPTION 
	{
		List<TEAMMEMBERMESSAGES> objListTeamMemberMessages=ObjectTeamMemberMessagesDao.getTeamMemberMessages(StrTeamMember);
		return objListTeamMemberMessages;
	}
	

	/**
	 *Description This function design to get TeamMembermessages 
	 *Input parameter StrTeamMember
	 *Return Type List<TEAMMEMBERMESSAGESBE>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	
	
	
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMemberMessagesList1,method = RequestMethod.GET)
	public List<TEAMMEMBERMESSAGESBE> getMessagesList(String StrTeamMember) throws JASCIEXCEPTION 
	{
		
		
		List objListTeamMemberMessages=ObjectTeamMemberMessagesDao.getTeamMemberMessagesList(StrTeamMember);
		
	
	
		// DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format);
	  
		List<TEAMMEMBERMESSAGESBE> objTeammembersMessagesbeList=new ArrayList<TEAMMEMBERMESSAGESBE>();

		
		
		for(Object ObjectValue:objListTeamMemberMessages)
		{
			Object[]  valueTeamMembersMessages = (Object[]) ObjectValue;
			
			if(valueTeamMembersMessages[GLOBALCONSTANT.INT_SIX]!=null)
			{
				TEAMMEMBERMESSAGESBE objTeammembersMessagesbe=new TEAMMEMBERMESSAGESBE();
				objTeammembersMessagesbe.setType(GLOBALCONSTANT.TicketTypeTeamMessage);
				try{objTeammembersMessagesbe.setTenant(valueTeamMembersMessages[GLOBALCONSTANT.INT_ZERO].toString());}catch(Exception e){objTeammembersMessagesbe.setTenant(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setEndDate(valueTeamMembersMessages[GLOBALCONSTANT.INT_TWO].toString());}catch(Exception e){objTeammembersMessagesbe.setEndDate(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setStartDate(valueTeamMembersMessages[GLOBALCONSTANT.IntOne].toString());}catch(Exception e){objTeammembersMessagesbe.setStartDate(GLOBALCONSTANT.BlankString);}		
				try{objTeammembersMessagesbe.setCompany(valueTeamMembersMessages[GLOBALCONSTANT.INT_THREE].toString());}catch(Exception e){objTeammembersMessagesbe.setCompany(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setMessage1(valueTeamMembersMessages[GLOBALCONSTANT.INT_FOUR].toString());}catch(Exception e){objTeammembersMessagesbe.setMessage1(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setTeamMember(valueTeamMembersMessages[GLOBALCONSTANT.INT_FIVE].toString());}catch(Exception e){objTeammembersMessagesbe.setTeamMember(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setTicketTapeMessage(valueTeamMembersMessages[GLOBALCONSTANT.INT_SIX].toString());}catch(Exception e){objTeammembersMessagesbe.setTicketTapeMessage(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setMessage(valueTeamMembersMessages[GLOBALCONSTANT.INT_SIX].toString());}catch(Exception e){objTeammembersMessagesbe.setMessage(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setMessageNumber(valueTeamMembersMessages[GLOBALCONSTANT.INT_SEVEN].toString());}catch(Exception e){objTeammembersMessagesbe.setMessageNumber(GLOBALCONSTANT.BlankString);}	           	
				objTeammembersMessagesbeList.add(objTeammembersMessagesbe);
			}
			
		
			
			if(valueTeamMembersMessages[GLOBALCONSTANT.INT_FOUR]!=null)
			{
				TEAMMEMBERMESSAGESBE objTeammembersMessagesbe=new TEAMMEMBERMESSAGESBE();
				objTeammembersMessagesbe.setType(GLOBALCONSTANT.TypeTeamMessage);
				try{objTeammembersMessagesbe.setTenant(valueTeamMembersMessages[GLOBALCONSTANT.INT_ZERO].toString());}catch(Exception e){objTeammembersMessagesbe.setTenant(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setEndDate(valueTeamMembersMessages[GLOBALCONSTANT.INT_TWO].toString());}catch(Exception e){objTeammembersMessagesbe.setEndDate(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setStartDate(valueTeamMembersMessages[GLOBALCONSTANT.IntOne].toString());}catch(Exception e){objTeammembersMessagesbe.setStartDate(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setCompany(valueTeamMembersMessages[GLOBALCONSTANT.INT_THREE].toString());}catch(Exception e){objTeammembersMessagesbe.setCompany(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setMessage(valueTeamMembersMessages[GLOBALCONSTANT.INT_FOUR].toString());}catch(Exception e){objTeammembersMessagesbe.setMessage(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setTeamMember(valueTeamMembersMessages[GLOBALCONSTANT.INT_FIVE].toString());}catch(Exception e){objTeammembersMessagesbe.setTeamMember(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setTicketTapeMessage(valueTeamMembersMessages[GLOBALCONSTANT.INT_SIX].toString());}catch(Exception e){objTeammembersMessagesbe.setTicketTapeMessage(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setMessage(valueTeamMembersMessages[GLOBALCONSTANT.INT_FOUR].toString());}catch(Exception e){objTeammembersMessagesbe.setMessage(GLOBALCONSTANT.BlankString);}
				try{objTeammembersMessagesbe.setMessageNumber(valueTeamMembersMessages[GLOBALCONSTANT.INT_SEVEN].toString());}catch(Exception e){objTeammembersMessagesbe.setMessageNumber(GLOBALCONSTANT.BlankString);}
				objTeammembersMessagesbeList.add(objTeammembersMessagesbe);
			}


		}

	
		return objTeammembersMessagesbeList;
	}

	/**
	 *Description This function design to delete TeamMembermessages 
	 *Input parameter Tenant,teamMember,company,ticketTape
	 *Return Type Boolean
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */

    @Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_DeleteMessage,method = RequestMethod.GET)
	public Boolean DeleteMessages(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Tenant)String tenant, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMember)String teamMember,@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Company) String company, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_MessageNumber)String messagenumber) throws JASCIEXCEPTION 
    {
	
    	TEAMMEMBERMESSAGESPK objTeammembersMessagesPK=new TEAMMEMBERMESSAGESPK();
    	TEAMMEMBERMESSAGES objTeammembermessages=new TEAMMEMBERMESSAGES();

		  objTeammembersMessagesPK.setTenant(tenant);
		  objTeammembersMessagesPK.setTeamMember(teamMember);
		  objTeammembersMessagesPK.setCompany(company);
		  objTeammembersMessagesPK.setMesageNumber(messagenumber);
		  objTeammembermessages.setTeamMemberMessagesId(objTeammembersMessagesPK);
		  
    	return ObjectTeamMemberMessagesDao.DeleteMessages(objTeammembermessages);
		
	}
    /**
	 *Description This function design to getMessages to edit TeamMembermessages 
	 *Input parameter Tenant,teamMember,company
	 *Return Type Boolean
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
    
    @Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_GetTeamMemberMessage,method = RequestMethod.GET)
    public List<TEAMMEMBERMESSAGESBE> getMessagesToEdit(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Tenant)String tenant, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMember)String teamMember, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Company)String company, @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_MessageNumber)String messagenumber) throws JASCIEXCEPTION 
	{		
    	List<TEAMMEMBERMESSAGES> objTeammembermessagesList= ObjectTeamMemberMessagesDao.GetMessagesToEdit(tenant,teamMember,company,messagenumber);
		 String startDate=null;
		 String Enddate=null;
		 //DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
	   DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		List<TEAMMEMBERMESSAGESBE> objTeammembersMessagesbeList=new ArrayList<TEAMMEMBERMESSAGESBE>();

		for(TEAMMEMBERMESSAGES  valueTeamMembersMessages:objTeammembermessagesList)
		{
			
			
				TEAMMEMBERMESSAGESBE objTeammembersMessagesbe=new TEAMMEMBERMESSAGESBE();
				objTeammembersMessagesbe.setTenant(valueTeamMembersMessages.getTeamMemberMessagesId().getTenant());
				try{
					startDate= ObjDateFormat1.format(valueTeamMembersMessages.getStartDate());
					Enddate= ObjDateFormat1.format(valueTeamMembersMessages.getEndDate());

				}catch(Exception e){
					
				}
				objTeammembersMessagesbe.setEndDate(Enddate);
				objTeammembersMessagesbe.setCompany(valueTeamMembersMessages.getTeamMemberMessagesId().getCompany());
				objTeammembersMessagesbe.setMessage1(StringEscapeUtils.escapeHtml(valueTeamMembersMessages.getMessage()));
				objTeammembersMessagesbe.setTeamMember(valueTeamMembersMessages.getTeamMemberMessagesId().getTeamMember());
				objTeammembersMessagesbe.setTicketTapeMessage(StringEscapeUtils.escapeHtml(valueTeamMembersMessages.getTicketTapeMessge()));
				objTeammembersMessagesbe.setStartDate(startDate);
				objTeammembersMessagesbe.setMessage(StringEscapeUtils.escapeHtml(valueTeamMembersMessages.getTicketTapeMessge()));
				objTeammembersMessagesbe.setMessageNumber(valueTeamMembersMessages.getTeamMemberMessagesId().getMesageNumber());
				objTeammembersMessagesbeList.add(objTeammembersMessagesbe);
			}
			
		
		 return objTeammembersMessagesbeList;
			
	}


	 /**
		 *Description This function design to edit TeamMembermessages 
		 *Input parameter objTeamMemberMessagesBe
		 *Return Type Boolean
		 *Created By "Diksha Gupta"
		 *Created Date "nov 19 2014" 
		 * */
    @Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_UpdateMessage,method = RequestMethod.GET)
	public Boolean UpdateMessages(TEAMMEMBERMESSAGESBE objTeamMemberMessagesBe) throws JASCIEXCEPTION 
    {
		  TEAMMEMBERMESSAGESPK objTeammembersMessagesPk=new TEAMMEMBERMESSAGESPK();
		  TEAMMEMBERMESSAGES objTeammembersMessages=new TEAMMEMBERMESSAGES();
		  objTeammembersMessagesPk.setTenant(ObjCommonSessionBe.getTenant());
		  objTeammembersMessagesPk.setTeamMember(objTeamMemberMessagesBe.getTeamMember());
		  objTeammembersMessagesPk.setCompany(ObjCommonSessionBe.getCompany());
		  objTeammembersMessagesPk.setMesageNumber(objTeamMemberMessagesBe.getMessageNumber());
		 
		  Date StartDate=null;
		  Date Enddate=null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
			StartDate=formatter.parse(objTeamMemberMessagesBe.getStartDate());
			  Enddate=formatter.parse(objTeamMemberMessagesBe.getEndDate());
			
			Date LastActivitydate = formatter.parse(objTeamMemberMessagesBe.getLastActivityDate());
			objTeammembersMessages.setLastActivityDate(LastActivitydate);

		}catch(Exception objException){}
		
		
		   
			
		  objTeammembersMessages.setTicketTapeMessge(objTeamMemberMessagesBe.getTicketTapeMessage());
		  objTeammembersMessages.setMessage(objTeamMemberMessagesBe.getMessage1());
		  objTeammembersMessages.setStatus(GLOBALCONSTANT.Status_Active);
		  objTeammembersMessages.setEndDate(Enddate);
		  objTeammembersMessages.setStartDate(StartDate);
		  objTeammembersMessages.setLastActivityTeamMember(ObjCommonSessionBe.getTeam_Member());
		  objTeammembersMessages.setTeamMemberMessagesId(objTeammembersMessagesPk);

		return ObjectTeamMemberMessagesDao.UpdateMessages(objTeammembersMessages);
	}

    
    
    /**
	 *Description This function design to get screen labels
	 *Input parameter Team_Member_Language
	 *Return Type TEAMMEMBERMESSAGESSCREENBE
	 *Created By "Diksha Gupta"
	 *Created Date "nov 28 2014" 
	 * */

    @SuppressWarnings(GLOBALCONSTANT.Strstatic_access)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_ScreenLabels,method = RequestMethod.GET)
	public TEAMMEMBERMESSAGESSCREENBE GetScreenLabels(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Language) String StrLanguage) throws JASCIEXCEPTION 
    {
    	//List<LANGUAGES> ObjListLanguages=ObjectTeamMemberMessagesDao.GetLanguage(StrLanguage);
    	List<LANGUAGES> ObjListLanguages=ObjLanguageLabelsApi.GetScreenLabels(ObjLanguageLabelsApi.strTeamMemberMessageModule,StrLanguage);

    	TEAMMEMBERMESSAGESSCREENBE ObjTeamMemberMessageScreenBe=SetTeamMemberMessagesScreenBe(ObjListLanguages);
		return ObjTeamMemberMessageScreenBe;
	}

    
    
    
    /**
	 *Description This function design to set screen labels
	 *Input parameter Team_Member_Language
	 *Return Type TEAMMEMBERMESSAGESSCREENBE
	 *Created By "Diksha Gupta"
	 *Created Date "nov 28 2014" 
	 * */

    
    
    public TEAMMEMBERMESSAGESSCREENBE SetTeamMemberMessagesScreenBe(List<LANGUAGES> ObjListLanguages)
	{
		/* Call GetLanguage() to get translation according to language.*/
			TEAMMEMBERMESSAGESSCREENBE ObjTeamMemberMessageScreenBe=new TEAMMEMBERMESSAGESSCREENBE();
		for (LANGUAGES ObjLanguages : ObjListLanguages) 
		{
			String StrKeyPhrase=ObjLanguages.getId().getKeyPhrase().trim();
			 if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Actions(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_DisplayAll(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_End_Date))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_EndDate(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Id))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_EnterTeamMember(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Message))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Message(ObjLanguages.getTranslation());
				
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Name(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_the_Team_Member_Name))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_PartOfTeamMember(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_SaveUpdate(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Search))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Search(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Shift))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Shift(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sort_First_Name))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_SortFirstName(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sort_Last_Name))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_SortLastName(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_Date))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_StartDate(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Selection))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_TeamMemberMaintenance(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Ticket_Tape_Message))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_TicketTape(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Type))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Type(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Delete(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Department))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Department(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Edit(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Department))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_InvalidDepartment(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_Team_Member_Name))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_InvalidPartOfTm(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Shift))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_InvalidShift(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Team_Member_ID))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_InvalidTM(ObjLanguages.getTranslation());
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_SaveMessage(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_UpdateMessage(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_MandatoryFieldMessage(ObjLanguages.getTranslation());
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_End_date_cannot_be_older_then_start_date))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_InvalidDateMessage(ObjLanguages.getTranslation());
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Department))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_BlankDepartment(ObjLanguages.getTranslation());
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_part_of_the_Team_Member_Name))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_BlankPartTeamMember(ObjLanguages.getTranslation());
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Shift))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_BlankShift(ObjLanguages.getTranslation());
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Team_Member_ID))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_BlankTeamMember(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_either_Ticket_Tape_Message_or_Message))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_BlankTicketTapeMessage(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_message))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_DeleteMessage(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Cancel(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_SEARCH_LOOKUP))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_SearchLookup(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Message_All_Selected))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_MessageAllSelected(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Active_Team_Member_Messages))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_ActiveTMM(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Company(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Teammember(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Addnew(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Message_Lookup))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_TMMLookUp(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Search_Lookup))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_TMLookUp(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Message_Maintenance_New_Edit))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_TMMMaintenance(ObjLanguages.getTranslation());
			}
			 
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_Date_YYYY_MM_DD))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_StartDate_YY(ObjLanguages.getTranslation());
			}
			 
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_End_Date_YYYY_MM_DD))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_EndDate_YY(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_a_team_member_to_proceed))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_MessageALLError(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Select(ObjLanguages.getTranslation());
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
			{
				ObjTeamMemberMessageScreenBe.setTeamMemberMessages_Reset(ObjLanguages.getTranslation());
			}
			 
}
		return ObjTeamMemberMessageScreenBe;
	}
    
    /**
	 * Created on:Nov 28  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get General code list based on tenant,companygeneralCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Restfull_GetGeneralCode, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODES> getGeneralCode(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Company) String Company,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_GeneralCodeId) String GeneralCodeId) throws JASCIEXCEPTION {	

		return ObjectTeamMemberMessagesDao.getGeneralCode(Tenant, Company, GeneralCodeId);

	}
    

	    /**
		 * 
		 * @author Diksha Gupta
		 * @Date Dec 18, 2014
		 * @param LANGUAGETRANSLATIONBE
		 * @throws JASCIEXCEPTION
		 * @Return Boolean
		 *
		 */ 
      @Transactional
		public String getTeamMemberName(String Teammember)
		{           
     String TeamMemberName=GLOBALCONSTANT.BlankString;
		List<TEAMMEMBERS> objTeammembers=null;
		try{
			objTeammembers=ObjMenuAppIconDal.getTeamMemberName(ObjCommonSessionBe.getTenant(),Teammember);
		}catch(Exception e){}
		try{
			TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
		}catch(Exception e){}
		
     return TeamMemberName;
		
		}
      
      /**
  	 *Description This function design to check if TeamMemberId exist TeamMember table
  	 *Input parameter TeamMemberId
  	 *Return Type Boolean
  	 *Created By "Diksha Gupta"
  	 *Created Date "nov 25 2014" 
  	 * */
  	
  	@Transactional
  	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_CheckTeamMemberId ,method = RequestMethod.POST)
  	public @ResponseBody Boolean CheckTeamMemberId(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_TeamMember)String strTeamMember) throws JASCIEXCEPTION
  	{
  		List<TEAMMEMBERS> ObjListTeamMembers=ObjectTeamMemberMessagesDao.CheckTeamMemberId(ObjCommonSessionBe.getTenant(),strTeamMember);
  		if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
			{
			return true;
			}
		else
		{
			return false;
		}
  	}
  	
  	 /**
  	 *Description This function design to check if TeamMemberName exist TeamMember table
  	 *Input parameter strPartTeamMember
  	 *Return Type Boolean
  	 *Created By "Diksha Gupta"
  	 *Created Date "nov 25 2014" 
  	 * */
  	
  	@Transactional
  	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_CheckTeamMemberName ,method = RequestMethod.POST)
  	public @ResponseBody Boolean CheckTeamMemberName( @RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_PartTeamMember)String strPartTeamMember) throws JASCIEXCEPTION
  	{
  		List<TEAMMEMBERS> ObjListTeamMembers=ObjectTeamMemberMessagesDao.CheckTeamMemberName(ObjCommonSessionBe.getTenant(),strPartTeamMember);
  		if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
			{
			return true;
			}
		else
		{
			return false;
		}
  	}
  	
  	
  	 /**
  	 *Description This function design to check if department exist TeamMember table
  	 *Input parameter Department
  	 *Return Type Boolean
  	 *Created By "Diksha Gupta"
  	 *Created Date "nov 25 2014" 
  	 * */
  	
  	@Transactional
  	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_CheckDepartment ,method = RequestMethod.POST)
  	public @ResponseBody Boolean CheckDepartment(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Department)String strDepartment) throws JASCIEXCEPTION
  	{
  		List<TEAMMEMBERS> ObjListTeamMembers=ObjectTeamMemberMessagesDao.CheckDepartment(ObjCommonSessionBe.getTenant(),strDepartment);
  		if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
			{
			return true;
			}
		else
		{
			return false;
		}
  	}
  	
  	
  	 /**
  	 *Description This function design to check if shift exist TeamMember table
  	 *Input parameter shift
  	 *Return Type Boolean
  	 *Created By "Diksha Gupta"
  	 *Created Date "nov 25 2014" 
  	 * */
  	
  	@Transactional
  	@RequestMapping(value = GLOBALCONSTANT.TeamMemmberMessages_Rest_CheckShift ,method = RequestMethod.POST)
  	public @ResponseBody Boolean CheckShift(@RequestParam(value=GLOBALCONSTANT.TeamMemmberMessages_Rest_Shift)String strShift) throws JASCIEXCEPTION
  	{
  		List<TEAMMEMBERS> ObjListTeamMembers=ObjectTeamMemberMessagesDao.CheckShift(ObjCommonSessionBe.getTenant(),strShift);
  		if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
  			{
  			return true;
  			}
  		else
  		{
  			return false;
  		}
  	}
  	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 17, 2014
	 * Description This is used to convert date format
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return String
	 */
	public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
		DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
		String StrFormattedString=dateInString ;
	 
		try {
	 
			Date date = formatter.parse(dateInString);
			StrFormattedString=dateFormat.format(date);
	 
		} catch (Exception e) {
			
		}
		
		return StrFormattedString;
		
	}

}
