/*

Description make a RestfullServices for communication between dao to services
Created By Aakash Bishnoi
Created Date Nov 22 2014

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.dao.IGENERALCODEDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.GENERALCODESBEAN;
import com.jasci.biz.AdminModule.model.LANGUAGES;

import com.jasci.biz.AdminModule.model.MENUOPTIONS;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class GENERALCODESERVICEAPI {

	@Autowired
	private IGENERALCODEDAO ObjectGeneralCodeDao;
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
	//It is used to read the list of generalcodes
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestGeneralCodeList, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODESBEAN>  getList(@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_tenant) String StrTenant) throws JASCIEXCEPTION {	

		List<GENERALCODES> GeneralCodesList= ObjectGeneralCodeDao.getList(StrTenant.toUpperCase(),StrCompany.toUpperCase());	
		
		
			
			List<GENERALCODESBEAN> GeneralCodesListBean = new ArrayList<GENERALCODESBEAN>();
			 
			   for(int Genralcodelistindex=GLOBALCONSTANT.INT_ZERO;Genralcodelistindex<GeneralCodesList.size();Genralcodelistindex++){

				    GENERALCODESBEAN objGeneralcodesbean= new GENERALCODESBEAN();

				    objGeneralcodesbean.setTenant(GeneralCodesList.get(Genralcodelistindex).getId().getTenant());
				    objGeneralcodesbean.setApplication(GeneralCodesList.get((Genralcodelistindex)).getId().getApplication());
				    objGeneralcodesbean.setCompany(GeneralCodesList.get(Genralcodelistindex).getId().getCompany());
				    objGeneralcodesbean.setGeneralCode(GeneralCodesList.get(Genralcodelistindex).getId().getGeneralCode());
				    objGeneralcodesbean.setGeneralCodeID(GeneralCodesList.get(Genralcodelistindex).getId().getGeneralCodeID());
				    objGeneralcodesbean.setDescription20(GeneralCodesList.get(Genralcodelistindex).getDescription20());
				    objGeneralcodesbean.setDescription50(GeneralCodesList.get(Genralcodelistindex).getDescription50());
				    objGeneralcodesbean.setHelpline(GeneralCodesList.get(Genralcodelistindex).getHelpline());
				    GeneralCodesListBean.add(objGeneralcodesbean);
				    
				   }
			   
			   
			   
		return GeneralCodesListBean;
	}

	//It is used to get the list of Main list of Manage GeneralCodeID screen
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestGeneralCodeListMain, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODESBEAN>  getMainGeneralCodeList(@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcode) String StrGeneralCode) throws JASCIEXCEPTION {	

		List<GENERALCODES> MainList= ObjectGeneralCodeDao.getMainGeneralCodeList(StrTenant, StrCompany, StrGeneralCode);
		
		List<GENERALCODESBEAN> GeneralCodesMainList = new ArrayList<GENERALCODESBEAN>();
		 
		   for(int MainListindex=GLOBALCONSTANT.INT_ZERO;MainListindex<MainList.size();MainListindex++){
			    GENERALCODESBEAN objGeneralcodesbean= new GENERALCODESBEAN();
			    	   
			    objGeneralcodesbean.setTenant(MainList.get(MainListindex).getId().getTenant());
			    objGeneralcodesbean.setApplication(MainList.get(MainListindex).getId().getApplication());
			    objGeneralcodesbean.setCompany(MainList.get(MainListindex).getId().getCompany());
			    objGeneralcodesbean.setGeneralCode(MainList.get(MainListindex).getId().getGeneralCode());
			    objGeneralcodesbean.setGeneralCodeID(MainList.get(MainListindex).getId().getGeneralCodeID());
			    objGeneralcodesbean.setDescription20(MainList.get(MainListindex).getDescription20());
			    objGeneralcodesbean.setDescription50(MainList.get(MainListindex).getDescription50());
			    objGeneralcodesbean.setIntKendoID(MainListindex);
		    GeneralCodesMainList.add(objGeneralcodesbean);
		    
		   }
		return GeneralCodesMainList;
	}

	//It is used to get the list of sub list of Manage GeneralCodeID screen
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestGeneralCodeListSub, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODESBEAN>  getSubGeneralCodeList(@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcodeid) String StrGeneralCodeID) throws JASCIEXCEPTION {	

		List<GENERALCODES> SubList= ObjectGeneralCodeDao.getSubGeneralCodeList(StrTenant, StrCompany, StrGeneralCodeID);
		
		List<GENERALCODESBEAN> GeneralCodesSubList = new ArrayList<GENERALCODESBEAN>();
		 
		
		   for(int SubListindex=GLOBALCONSTANT.INT_ZERO;SubListindex<SubList.size();SubListindex++){
			    //System.out.println(GeneralCodesList.get(i).getId().getTenant());
			    GENERALCODESBEAN objGeneralcodesbean= new GENERALCODESBEAN();
			    
			   
			   
			    objGeneralcodesbean.setTenant(SubList.get(SubListindex).getId().getTenant());
			    objGeneralcodesbean.setApplication(SubList.get(SubListindex).getId().getApplication());
			    objGeneralcodesbean.setCompany(SubList.get(SubListindex).getId().getCompany());
			    objGeneralcodesbean.setGeneralCode(SubList.get(SubListindex).getId().getGeneralCode());
			    objGeneralcodesbean.setGeneralCodeID(SubList.get(SubListindex).getId().getGeneralCodeID());
			    objGeneralcodesbean.setDescription20(SubList.get(SubListindex).getDescription20());
			    objGeneralcodesbean.setDescription50(SubList.get(SubListindex).getDescription50());
			    objGeneralcodesbean.setIntKendoID(SubListindex);
			    
			 
			    
			    GeneralCodesSubList.add(objGeneralcodesbean);
			    
			   }
		
		
		return GeneralCodesSubList;
	}

	//Add General Code for GeneralOCdeID=GENERALCODE on Generalcode screen
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestGeneralCodeAdd, method = RequestMethod.GET)
	public @ResponseBody  String addEntry(GENERALCODES ObjectGeneralCodes,MENUOPTIONS ObjMenuOprions) throws JASCIEXCEPTION {	
		String Result=null;

		
			Result= ObjectGeneralCodeDao.addEntry(ObjectGeneralCodes,ObjMenuOprions);
			return Result;
		
	}


	//Add Manage General Code for screen GeneralcodeID
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestManageGeneralCodeAdd, method = RequestMethod.GET)
	public @ResponseBody  String addGeneralCodeIdEntry(GENERALCODES ObjectGeneralCodes) throws JASCIEXCEPTION {	
		String Result=null;

		
			Result= ObjectGeneralCodeDao.addGeneralCodeIdEntry(ObjectGeneralCodes);
			return Result;
		
	}

	//Update General Code for screen Generalcode
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestGeneralCodeUpdate, method = RequestMethod.GET)
	public @ResponseBody  String updateEntry(GENERALCODES ObjectGeneralCodes,MENUOPTIONS ObjMenuOprions) throws JASCIEXCEPTION {	
		String Result=null;

		
			Result= ObjectGeneralCodeDao.updateEntry(ObjectGeneralCodes,ObjMenuOprions);
			
		return Result;
	}


	//Update General Code for screen GeneralcodeID
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestManageGeneralCodeUpdate, method = RequestMethod.GET)
	public @ResponseBody  String updateGeneralCodeID(GENERALCODES ObjectGeneralCodes) throws JASCIEXCEPTION {	
		String Result=null;

		
			Result= ObjectGeneralCodeDao.updateGeneralCodeID(ObjectGeneralCodes);
			
		return Result;
	}

	//Delete General Code and menu options for screen GeneralcodeID
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestGeneralCodeDelete, method = RequestMethod.GET)
	public @ResponseBody  Boolean deleteEntry(@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_application) String StrApplication,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcodeid) String StrGeneralCodeId,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcode) String StrGeneralCode) throws JASCIEXCEPTION{	
		Boolean Result=false;

		
			Result= ObjectGeneralCodeDao.deleteEntry(StrTenant,StrCompany,StrApplication,StrGeneralCodeId,StrGeneralCode);
			
		
		return Result;
	}


	//delete General Code for screen GeneralcodeID
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestManageGeneralCodeDelete, method = RequestMethod.GET)
	public @ResponseBody  Boolean  deleteGeneralCodeID(@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_application) String StrApplication,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcodeid) String StrGeneralCodeId,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcode) String StrGeneralCode){	
		Boolean Result=false;

		try {
			Result= ObjectGeneralCodeDao.deleteGeneralCodeID(StrTenant,StrCompany,StrApplication,StrGeneralCodeId,StrGeneralCode);
			return Result;
		} catch (JASCIEXCEPTION e) {

		}
		return Result;
	}

	
	//It is used to get the label of GeneralCode Management Screen(Add/Edit)
		@Transactional
		@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_RestGeneralCodeEditDeleteScreenlabel, method = RequestMethod.GET)
		public @ResponseBody  List<LANGUAGES> SetGeneralCodesBe(String StrLanguage) throws JASCIEXCEPTION {	

			
			
			List<LANGUAGES> ObjGeneralCodeBe = ObjectGeneralCodeDao.SetGeneralCodesBe(StrLanguage); 
			
			return ObjGeneralCodeBe;
		}
		
	
			
			//It is used to fetch the data for generalcode update and it is working on edit button of generalcodelist screen
			@Transactional
			@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Restfull_GeneralCodeListFetch, method = RequestMethod.GET)
			public @ResponseBody  GENERALCODES getEntry(@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_tenant) String StrTenant,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_company) String StrCompany,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_application) String StrApplication,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcodeid) String StrGeneralCodeId,@RequestParam(value=GLOBALCONSTANT.GeneralCodes_Restfull_generalcode) String StrGeneralCode)  throws JASCIEXCEPTION {	

				GENERALCODES ObjGeneralCode=null;
				
				ObjGeneralCode = ObjectGeneralCodeDao.getEntry(StrTenant,StrCompany,StrApplication,StrGeneralCodeId,StrGeneralCode); 
				
				return ObjGeneralCode;
			}

			 /**
			 * @author Aakash Bishnoi
			 * @Date Feb 16, 2015
			 * Description:It is used to check uniqueness of Description20 in GeneralCodes TABLE before add record 
			 * @param Tenant
			 * @param Company
		
			 * @param GeneralCodeID
			 * @param GeneralCode
			 * @return Boolean
			 * @throws JASCIEXCEPTION
			 */
			
			@Transactional
			@RequestMapping(value = GLOBALCONSTANT.Generalcode_Restfull_RestCheckUniqueDescriptionShort, method = RequestMethod.POST)
			public @ResponseBody Boolean getDescription20Unique(@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_Application) String StrApplicationID,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCodeID) String StrGeneralCodeID,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCode) String StrGeneralCode,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_Description20) String StrDescription20,@RequestParam(value=GLOBALCONSTANT.ScreenName) String StrScreenName)  throws JASCIEXCEPTION {	

				List<GENERALCODES> ObjGeneralCodes=null;

				ObjGeneralCodes = ObjectGeneralCodeDao.getDescription20Unique(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrApplicationID,StrGeneralCodeID,StrGeneralCode,StrDescription20,StrScreenName); 
				if(ObjGeneralCodes.size()>GLOBALCONSTANT.INT_ZERO){
					return true;				
				}
				else{
					return false;
				}
				
			}
			
			
			 /**
			 * @author Aakash Bishnoi
			 * @Date Feb 16, 2015
			 * Description:It is used to check uniqueness of Description50 in GeneralCodes TABLE before add record 
			 * @param Tenant
			 * @param Company
			 
			 * @param GeneralCodeID
			 * @param GeneralCode
			 * @return Boolean
			 * @throws JASCIEXCEPTION
			 */
			
			@Transactional
			@RequestMapping(value = GLOBALCONSTANT.Generalcode_Restfull_RestCheckUniqueDescriptionLong, method = RequestMethod.POST)
			public @ResponseBody Boolean getDescription50Unique(@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_Application) String StrApplicationID,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCodeID) String StrGeneralCodeID,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCode) String StrGeneralCode,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_Description50) String StrDescrption50,@RequestParam(value=GLOBALCONSTANT.ScreenName) String StrScreenName)  throws JASCIEXCEPTION {	

				List<GENERALCODES> ObjGeneralCodes=null;

				ObjGeneralCodes = ObjectGeneralCodeDao.getDescription50Unique(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrApplicationID,StrGeneralCodeID,StrGeneralCode,StrDescrption50,StrScreenName); 
				
					if(ObjGeneralCodes.size()>GLOBALCONSTANT.INT_ZERO){
						return true;				
					}
					else{
						return false;
					}
				
				
				
			}
			
			
			@Transactional
			@RequestMapping(value = GLOBALCONSTANT.Generalcode_Restfull_RestCheckUniqueGeneralCode, method = RequestMethod.POST)
			public @ResponseBody Boolean getGeneralCodeUnique(@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_Application) String StrApplicationID,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCodeID) String StrGeneralCodeID,@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCode) String StrGeneralCode)  throws JASCIEXCEPTION {	

				List<GENERALCODES> ObjGeneralCodes=null;

				ObjGeneralCodes = ObjectGeneralCodeDao.getGeneralCodeUnique(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),StrApplicationID,StrGeneralCodeID,StrGeneralCode); 				//
				if(ObjGeneralCodes.size()>GLOBALCONSTANT.INT_ZERO){
					return true;					
				}
				else{
					return false;
				}
			}

			
			
			@Transactional
			@RequestMapping(value = GLOBALCONSTANT.Generalcode_Restfull_RestCheckUniqueMenuName, method = RequestMethod.POST)
			public @ResponseBody Boolean getMenuOptionUnique(@RequestParam(value=GLOBALCONSTANT.DataBase_GeneralCodes_MenuOptionName) String StrMenuOption)  throws JASCIEXCEPTION {	

				
				List<MENUOPTIONS> ObjMenuOptions=null;

				ObjMenuOptions = ObjectGeneralCodeDao.getMenuNameUnique(StrMenuOption); 

				if(ObjMenuOptions.size()>GLOBALCONSTANT.INT_ZERO){
					return true;					
				}
				else{
					return false;
				}
			}
}
