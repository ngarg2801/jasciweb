/**

Date Developed :Dec 26 2014
Created by: Diksha Gupta
Description :ILOCATIONPROFILESERVICE to call the methods of ILOCATIONPROFILEDAL.
 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.LOCATIONPROFILEBE;
import com.jasci.biz.AdminModule.be.LOCATIONPROFILESCREENLABELSBE;
import com.jasci.biz.AdminModule.dao.ILOCATIONPROFILEDAL;
import com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILESPK;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.TIMECHECK;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class LOCATIONPROILESERVICEAPI 
{
	
	@Autowired
	ILOCATIONPROFILEDAL objLocationProfileDal;
	@Autowired
	COMMONSESSIONBE ObjCommonSessionBe;
	@Autowired
	IMENUAPPICONDAL ObjMenuAppIconDal;
	@Autowired
	LANGUANGELABELSAPI ObjLanguageLabelsApi;
	

	/**
	 * Created on:Dec 28  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get General code list based on tenant,companygeneralCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */

	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Locations_Profile_Restfull_GetGeneralCode, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODES> getGeneralCode(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Company) String Company,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_GeneralCodeId) String GeneralCodeId) throws JASCIEXCEPTION {	

		return objLocationProfileDal.getGeneralCode(Tenant, Company, GeneralCodeId);

	}

	

	/**
	 * Created on:Dec 30  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to add records to LocationProfiles
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */

@Transactional
	public @ResponseBody  Boolean addLocationProfile(LOCATIONPROFILEBE objLocationProfileBe) throws JASCIEXCEPTION 
	{	
     LOCATIONPROFILES objLocationprofiles= new LOCATIONPROFILES();
     LOCATIONPROFILESPK objLocationprofilespk=new LOCATIONPROFILESPK();
     objLocationprofiles=useGetterSetter(objLocationprofiles, objLocationprofilespk, objLocationProfileBe);

		return objLocationProfileDal.addLocationProfile(objLocationprofiles);

	}


/**
 * Created on:JAN 05  2015
 * Created by:"Diksha Gupta"
 * Description: This function  is to update records to LocationProfiles
 * Input parameter: String
 * Return Type :Boolean
 * 
 */

@Transactional
public @ResponseBody  Boolean editLocationProfile(LOCATIONPROFILEBE objLocationProfileBe) throws JASCIEXCEPTION 
{	
 LOCATIONPROFILES objLocationprofiles= new LOCATIONPROFILES();
 LOCATIONPROFILESPK objLocationprofilespk=new LOCATIONPROFILESPK();
 objLocationprofiles=useGetterSetter(objLocationprofiles, objLocationprofilespk, objLocationProfileBe);

	return objLocationProfileDal.editLocationProfile(objLocationprofiles);

}

	
	/**
	 * Created on:Dec 30  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to set values from LOCATIONPROFILEBE to LOCATIONPROFILES .
	 * Input parameter: LOCATIONPROFILES,LOCATIONPROFILESPK,LOCATIONPROFILEBE
	 * Return Type :LOCATIONPROFILES
	 * 
	 */

    private LOCATIONPROFILES useGetterSetter(LOCATIONPROFILES objLocationprofiles,LOCATIONPROFILESPK objLocationprofilespk,LOCATIONPROFILEBE objLocationprofilebe)
    {
    	
    	DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
    	
    	objLocationprofilespk.setCompany(ObjCommonSessionBe.getCompany());
    	objLocationprofilespk.setTenant(ObjCommonSessionBe.getTenant());
    	objLocationprofilespk.setFulfillmentCenter(objLocationprofilebe.getFulFillmentCenter().replace(GLOBALCONSTANT.StringDoubleQuote, GLOBALCONSTANT.DoubleQuote));
    	objLocationprofilespk.setProfile(objLocationprofilebe.getLocationProfile().trim());
    	//objLocationprofilespk.setProfileGroup(objLocationprofilebe.getProfileGroup());
    	objLocationprofiles.setProfileGroup(objLocationprofilebe.getProfileGroup());
    	objLocationprofiles.setAllocationAllowable(objLocationprofilebe.getAllocationAllowable());
    	try{
    		objLocationprofiles.setCCActivityPoint(Long.parseLong(objLocationprofilebe.getCCActivityPoints().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setCCActivityPoint(GLOBALCONSTANT.LongZero);
    		
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setCCActivityPoint(GLOBALCONSTANT.LongZero);
    		
    	}
    	try{
    		objLocationprofiles.setCCHighValueAmount(Long.parseLong(objLocationprofilebe.getCCAmount().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setCCHighValueAmount(GLOBALCONSTANT.LongZero);
    		
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setCCHighValueAmount(GLOBALCONSTANT.LongZero);
    		
    	}
    	try{
    		objLocationprofiles.setCCHighValueFactor(Long.parseLong(objLocationprofilebe.getCCFactor().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setCCHighValueFactor(GLOBALCONSTANT.LongZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setCCHighValueFactor(GLOBALCONSTANT.LongZero);
    	}
    	objLocationprofiles.setDescription20(objLocationprofilebe.getDescription20().trim());
    	objLocationprofiles.setDescription50(objLocationprofilebe.getDescription50().trim());
    	objLocationprofiles.setFreeLocationWhenZero(objLocationprofilebe.getFreeLocation().trim());
    	try{
    		objLocationprofiles.setFreePrimeDays(Long.parseLong(objLocationprofilebe.getPrimeDays().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setFreePrimeDays(GLOBALCONSTANT.LongZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setFreePrimeDays(GLOBALCONSTANT.LongZero);
    	}
    	/*objLocationprofiles.setLastActivityDate(StrCurrentDate);*/
    	try {
    		objLocationprofiles.setLastActivityDate(ObjDateFormat.parse(objLocationprofilebe.getLastActivityDate()));
		} catch (ParseException e) {

		}
    	objLocationprofiles.setLastActivityTeamMember(ObjCommonSessionBe.getTeam_Member());

    	objLocationprofiles.setLocationCheck(objLocationprofilebe.getLocationCheck());
    	try{
    		objLocationprofiles.setLocationDepth(Double.parseDouble(objLocationprofilebe.getLocationDepth().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setLocationDepth(GLOBALCONSTANT.DecimalZero);
    	}catch(Exception e)
    	{
    		objLocationprofiles.setLocationDepth(GLOBALCONSTANT.DecimalZero);
    	}
    	
    	try{
    		objLocationprofiles.setLocationHeight(Double.parseDouble(objLocationprofilebe.getLocationHeight().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setLocationHeight(GLOBALCONSTANT.DecimalZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setLocationHeight(GLOBALCONSTANT.DecimalZero);
    	}
    	try{
    		objLocationprofiles.setLocationHeightCapacity(Double.parseDouble(objLocationprofilebe.getLocationHeightCapacity().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setLocationHeightCapacity(GLOBALCONSTANT.DecimalZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setLocationHeightCapacity(GLOBALCONSTANT.DecimalZero);
    	}
    	try{
    		objLocationprofiles.setLocationWeightCapacity(Double.parseDouble(objLocationprofilebe.getLocationWeightCapacity().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setLocationWeightCapacity(GLOBALCONSTANT.DecimalZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setLocationWeightCapacity(GLOBALCONSTANT.DecimalZero);
    	}
    	try{
    		objLocationprofiles.setLocationWidth(Double.parseDouble(objLocationprofilebe.getLocationWidth().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setLocationWidth(GLOBALCONSTANT.DecimalZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setLocationWidth(GLOBALCONSTANT.DecimalZero);
    	}
    	objLocationprofiles.setMutipleProductsInLocation(objLocationprofilebe.getMultipleProducts().trim());
    	try{
    		objLocationprofiles.setNumberFloorLocations(Long.parseLong(objLocationprofilebe.getNumberOfFloorPallet().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setNumberFloorLocations(GLOBALCONSTANT.LongZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setNumberFloorLocations(GLOBALCONSTANT.LongZero);
    	}
    	try{
    		objLocationprofiles.setNumberPalletsFit(Long.parseLong(objLocationprofilebe.getNumberOfPallets().trim()));
    	}catch(NumberFormatException e)
    	{
    		objLocationprofiles.setNumberPalletsFit(GLOBALCONSTANT.LongZero);
    	}
    	catch(Exception e)
    	{
    		objLocationprofiles.setNumberPalletsFit(GLOBALCONSTANT.LongZero);
    	}
    	objLocationprofiles.setSlotting(objLocationprofilebe.getSlotting().trim());
    	objLocationprofiles.setStorageType(objLocationprofilebe.getStorageType());
    	objLocationprofiles.setType(objLocationprofilebe.getLocationType().trim());
    	objLocationprofiles.setLocationProfileId(objLocationprofilespk);
    	return objLocationprofiles;
    }
	

	/**
	 * Created on:Dec 29  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to check whether the LocationProfile already exists.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFull_CheckAlreadyExists, method = RequestMethod.POST) 
	public @ResponseBody Boolean CheckAlreadyExists(@RequestParam(value=GLOBALCONSTANT.RestParam_Profile) String LocationProfile,@RequestParam(value=GLOBALCONSTANT.RestParam_ProfileGroup) String ProfileGroup,@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String FulFillmentCenter)
	{
		
		List<LOCATIONPROFILES> objListLocationProfiles=null;
		try{
		objListLocationProfiles=objLocationProfileDal.CheckAlreadyExists(LocationProfile,ProfileGroup,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),FulFillmentCenter);
		}catch(JASCIEXCEPTION ObjJasciException)
		{}
		if(objListLocationProfiles.size()>GLOBALCONSTANT.INT_ZERO)
		{
			return true;
		}
		
		return false;
		
	}
	
	
	/**
	 * Created on:Dec 31  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to check whether the entered LocationProfile is valid.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFull_CheckLocationProfile, method = RequestMethod.POST) 
	public @ResponseBody Boolean CheckLocationProfile(@RequestParam(value=GLOBALCONSTANT.RestParam_Profile) String LocationProfile)
	{
		
		List<LOCATIONPROFILES> objListLocationProfiles=null;
		try{
		objListLocationProfiles=objLocationProfileDal.CheckLocationProfile(LocationProfile,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ObjCommonSessionBe.getFulfillmentCenter());
		}catch(JASCIEXCEPTION ObjJasciException)
		{}
		if(objListLocationProfiles!=null && !objListLocationProfiles.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	
	
	/**
	 * Created on:Dec 31  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to check whether the entered Profile Group is valid.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFull_CheckProfileGroup, method = RequestMethod.POST) 
	public @ResponseBody Boolean CheckProfileGroup(@RequestParam(value=GLOBALCONSTANT.RestParam_ProfileGroup) String ProfileGroup)
	{
		
		List<LOCATIONPROFILES> objListLocationProfiles=null;
		try{
		objListLocationProfiles=objLocationProfileDal.CheckProfileGroup(ProfileGroup,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ObjCommonSessionBe.getFulfillmentCenter());
		}catch(JASCIEXCEPTION ObjJasciException)
		{}
		if(objListLocationProfiles!=null && !objListLocationProfiles.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	
	
	/**
	 * Created by:"Diksha Gupta"
	 * Description: This function is to check whether the LocationProfile already exists.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFull_CheckDescription, method = RequestMethod.POST) 
	public @ResponseBody Boolean CheckDescription(@RequestParam(value=GLOBALCONSTANT.RestParam_Description) String Description)
	{
		
		List<LOCATIONPROFILES> objListLocationProfiles=null;
		try{
		objListLocationProfiles=objLocationProfileDal.CheckDescription(Description,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ObjCommonSessionBe.getFulfillmentCenter());
		}catch(JASCIEXCEPTION ObjJasciException)
		{}
		if(objListLocationProfiles!=null && !objListLocationProfiles.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	
	
	/**
	 * Created by:"Diksha Gupta"
	 * Description: This function is to check whether the LocationProfile already exists.
	 * Input parameter: String
	 * Return Type :Boolean
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFull_Delete, method = RequestMethod.POST) 
	public @ResponseBody Boolean DeleteLocationProfile(@RequestParam(value=GLOBALCONSTANT.RestParam_Profile) String LocationProfile,@RequestParam(value=GLOBALCONSTANT.RestParam_ProfileGroup) String ProfileGroup,@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String FulFillmentCenter) throws JASCIEXCEPTION
	{
		Boolean Status=false;
		LOCATIONPROFILES objLocationProfiles=new LOCATIONPROFILES();
		LOCATIONPROFILESPK objLocationprofilespk=new LOCATIONPROFILESPK();
		objLocationprofilespk.setCompany(ObjCommonSessionBe.getCompany());
    	objLocationprofilespk.setTenant(ObjCommonSessionBe.getTenant());
    	objLocationprofilespk.setFulfillmentCenter(FulFillmentCenter);
    	objLocationprofilespk.setProfile(LocationProfile);
    	//objLocationprofilespk.setProfileGroup(ProfileGroup);
    	objLocationProfiles.setLocationProfileId(objLocationprofilespk);
    	Status=objLocationProfileDal.DeleteLocationProfile(objLocationProfiles);
		
		return Status;
		
	}

	/**
	 * Created on:Jan 2  2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get location profile data.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
@Transactional
	public List<LOCATIONPROFILEBE> GetLocationProfileList(String strLocationProfile, String strProfileGroup,String strPartOfDescription) throws JASCIEXCEPTION 
	{
		List<LOCATIONPROFILEBE> objListLocationprofilebe=new ArrayList<LOCATIONPROFILEBE>();
		List<Object> ObjListLocationProfile=new ArrayList<Object>();
		if(strLocationProfile==null)
		{
			strLocationProfile=GLOBALCONSTANT.BlankString;
		}
		if(strProfileGroup==null)
		{
			strProfileGroup=GLOBALCONSTANT.BlankString;
		}
		if(strPartOfDescription==null)
		{
			strPartOfDescription=GLOBALCONSTANT.BlankString;
		}
		if(strLocationProfile!=null && !strLocationProfile.equalsIgnoreCase(GLOBALCONSTANT.BlankString))
		{
			 ObjListLocationProfile= objLocationProfileDal.CheckLocationProfile(strLocationProfile,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ObjCommonSessionBe.getFulfillmentCenter());


		}
		else if(strProfileGroup!=null && !strProfileGroup.equalsIgnoreCase(GLOBALCONSTANT.BlankString))
		{
			 ObjListLocationProfile= objLocationProfileDal.CheckProfileGroup(strProfileGroup,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ObjCommonSessionBe.getFulfillmentCenter());

		}
		else if(strPartOfDescription!=null && !strPartOfDescription.equalsIgnoreCase(GLOBALCONSTANT.BlankString))
		{
			 ObjListLocationProfile= objLocationProfileDal.CheckDescription(strPartOfDescription,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ObjCommonSessionBe.getFulfillmentCenter());

		}
		else
		{
			 ObjListLocationProfile= objLocationProfileDal.GetLocationProfileList(ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ObjCommonSessionBe.getFulfillmentCenter());

		}
				
		objListLocationprofilebe=useGetterSetterList(ObjListLocationProfile,objListLocationprofilebe);
		return objListLocationprofilebe;		
	}


/**
 * Created on:Jan 2  2015
 * Created by:"Diksha Gupta"
 * Description: This function is to get location profile data.
 * Input parameter: String
 * Return Type :List<LOCATIONPROFILEBE>
 * @throws JASCIEXCEPTION 
 * 
 */
@Transactional
public List<LOCATIONPROFILEBE> GetLocationProfileListReAssign(String ProfileGroup,String Fulfillmentcenter,String LocationProfile) throws JASCIEXCEPTION 
{
	List<LOCATIONPROFILEBE> objListLocationprofilebe=new ArrayList<LOCATIONPROFILEBE>();
	List<LOCATIONPROFILES> ObjListLocationProfile=new ArrayList<LOCATIONPROFILES>();

    ObjListLocationProfile= objLocationProfileDal.GetLocationProfileListReAssign(ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),ProfileGroup,Fulfillmentcenter,LocationProfile);

	objListLocationprofilebe=useGetterSetter(ObjListLocationProfile,objListLocationprofilebe);
	return objListLocationprofilebe;
}

/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get list of distinct LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
 * @throws JASCIEXCEPTION 
	 * 
*/
@Transactional
      public List<String> GetDistinctLocationProfile() throws JASCIEXCEPTION
      {
    	List<String> objListLocationprofile=new ArrayList<String>();
  		List<Object> objListObject=objLocationProfileDal.getDistinctLocationProfile(ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany());
  		
		for (Object object : objListObject) 
		{
			
			
			String StrLocationProfile=GLOBALCONSTANT.BlankString;

			try{
				StrLocationProfile=object.toString();
								
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			objListLocationprofile.add(StrLocationProfile);
			}
		
		return objListLocationprofile;
       }


private List<LOCATIONPROFILEBE> useGetterSetterList(List<Object> objListLocationProfile,List<LOCATIONPROFILEBE> objListLocationprofilebe) 
{
	
	
	for(Iterator<Object> LocationListLength=objListLocationProfile.iterator();LocationListLength.hasNext(); )
	{

		LOCATIONPROFILEBE ObjectLocationProfileBe=new LOCATIONPROFILEBE();
		Object[] ObjectLocObj=null;
		try{
		ObjectLocObj =  (Object[]) LocationListLength.next();
		}catch(Exception e){}
		try{
		ObjectLocationProfileBe.setProfileGroupCode(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			}catch(Exception obj_e)	{
				ObjectLocationProfileBe.setProfileGroupCode(GLOBALCONSTANT.BlankString);
			}
		try{
			ObjectLocationProfileBe.setProfileGroup(ObjectLocObj[GLOBALCONSTANT.IntOne].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setProfileGroup(GLOBALCONSTANT.BlankString);
		}
		
		try{
			ObjectLocationProfileBe.setFulFillmentCenter(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setFulFillmentCenter(GLOBALCONSTANT.BlankString);
		}
		try{
			ObjectLocationProfileBe.setFullfillment_Center_ID_Description(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setFullfillment_Center_ID_Description(GLOBALCONSTANT.BlankString);
		}
		try{
			ObjectLocationProfileBe.setNumberOfLocations(ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setNumberOfLocations(GLOBALCONSTANT.BlankString);				
		}
		try{
			ObjectLocationProfileBe.setLocationProfile(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setLocationProfile(GLOBALCONSTANT.BlankString);
		}
		try{
			ObjectLocationProfileBe.setDescription50(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setDescription50(GLOBALCONSTANT.BlankString);
		}
		try{
			ObjectLocationProfileBe.setLastUsedDate(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setLastUsedDate(GLOBALCONSTANT.BlankString);
		}
		try{
			ObjectLocationProfileBe.setLastUsedTeamMember(ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setLastUsedTeamMember(GLOBALCONSTANT.BlankString);
		}
		
		try{
			ObjectLocationProfileBe.setLastActivityDate(ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setLastActivityDate(GLOBALCONSTANT.BlankString);
		}
		try{
			ObjectLocationProfileBe.setLastActivityTeamMember(ObjectLocObj[GLOBALCONSTANT.INT_ELEVEN].toString());
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setLastActivityTeamMember(GLOBALCONSTANT.BlankString);
		}
		try{			
			String notes=ObjectLocObj[GLOBALCONSTANT.INT_TWELVE].toString();
			  StringBuilder sb = new StringBuilder();
			  for(int NotesIndex=GLOBALCONSTANT.INT_ZERO,Noteslastindex=GLOBALCONSTANT.INT_ZERO;NotesIndex<notes.length();NotesIndex++,Noteslastindex++)
			  {
			   if(notes.charAt(NotesIndex)!=GLOBALCONSTANT.BlankSpace && Noteslastindex==GLOBALCONSTANT.INT_TWENTYFOUR)
			   {
			    sb.append(GLOBALCONSTANT.Single_Space+notes.charAt(NotesIndex));
			    Noteslastindex=GLOBALCONSTANT.INT_ZERO;
			   }else if(notes.charAt(NotesIndex)==GLOBALCONSTANT.BlankSpace)
			   {
			    sb.append(notes.charAt(NotesIndex));
			    Noteslastindex=GLOBALCONSTANT.INT_ZERO;
			   }
			    else{
			    sb.append(notes.charAt(NotesIndex));
			   }
			  }
			ObjectLocationProfileBe.setNotes(sb.toString());
			
		}catch(Exception obj_e)	{
			ObjectLocationProfileBe.setNotes(GLOBALCONSTANT.BlankString);
		}

		objListLocationprofilebe.add(ObjectLocationProfileBe);
    
	}
	return objListLocationprofilebe;
}


	/**
	 * Created on:Jan 01  2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to set location profile data to LocationProfileBe.
	 * Input parameter: LANGUAGES
	 * Return Type :List<>
	 * 
	 */
	private List<LOCATIONPROFILEBE> useGetterSetter(List<LOCATIONPROFILES> objListLocationProfile,List<LOCATIONPROFILEBE> objListLocationprofilebe) 
	{
		
		
		
	    
	
		 //String StrLastActivityDate=GLOBALCONSTANT.BlankString;
		// String StrLastUsedDate=GLOBALCONSTANT.BlankString;
		 String ObjLastUsedDateValue=GLOBALCONSTANT.BlankString;
		 String ObjLastActivityDateValue=GLOBALCONSTANT.BlankString;
		 
		 DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format);
		
		 for(LOCATIONPROFILES ObjLocationProfile:objListLocationProfile)
		{
			LOCATIONPROFILEBE Objlocationprofilebe=new LOCATIONPROFILEBE();
		
			
			if(ObjLocationProfile.getLastUsedDate()!=null){
				String ObjLastUsedDate = dateFormat.format(ObjLocationProfile.getLastUsedDate());
				  ObjLastUsedDateValue=TIMECHECK.ConvertUTCToClientTimeZone(ObjLastUsedDate, GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.InputTimezoneUTC, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format, ObjCommonSessionBe.getClietnDeviceTimeZone());
			}else{
				ObjLastUsedDateValue = GLOBALCONSTANT.BlankString;
			}

			if(ObjLocationProfile.getLastActivityDate()!=null){
				 String ObjLastActivityDate = dateFormat.format(ObjLocationProfile.getLastActivityDate());
				 ObjLastActivityDateValue=TIMECHECK.ConvertUTCToClientTimeZone(ObjLastActivityDate, GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.InputTimezoneUTC, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format, ObjCommonSessionBe.getClietnDeviceTimeZone());
				
			}else{
				ObjLastActivityDateValue = GLOBALCONSTANT.BlankString;
			}
			
		
			Objlocationprofilebe.setLastActivityDate(ObjLastActivityDateValue);
			Objlocationprofilebe.setLastUsedDate(ObjLastUsedDateValue);
			Objlocationprofilebe.setLastActivityTeamMember(getTeamMemberName(ObjLocationProfile.getLastActivityTeamMember()));
			Objlocationprofilebe.setLastUsedTeamMember(getTeamMemberName(ObjLocationProfile.getLastUsedTeamMember()));
			
			Objlocationprofilebe.setDescription20(ObjLocationProfile.getDescription20());
			Objlocationprofilebe.setDescription50(ObjLocationProfile.getDescription50());
			Objlocationprofilebe.setFulFillmentCenter(ObjLocationProfile.getLocationProfileId().getFulfillmentCenter());
			List<Object> ObjListObject=null;
			try {
				ObjListObject=objLocationProfileDal.GetNumberOfLocations(ObjCommonSessionBe.getTenant(), ObjCommonSessionBe.getCompany(),ObjLocationProfile.getLocationProfileId().getFulfillmentCenter(),ObjLocationProfile.getLocationProfileId().getProfile());
			} catch (JASCIEXCEPTION e1) {
				
			}
			String NumberOfLocations=ObjListObject.get(GLOBALCONSTANT.INT_ZERO).toString();
			Objlocationprofilebe.setNumberOfLocations(NumberOfLocations);
			Objlocationprofilebe.setProfileGroupCode(ObjLocationProfile.getProfileGroup());
			List<GENERALCODES> ObjListGeneralCodes = null;
			try {
				ObjListGeneralCodes = objLocationProfileDal.getGeneralCode(ObjCommonSessionBe.getTenant(), ObjCommonSessionBe.getCompany(),GLOBALCONSTANT.GIC_LOCPFGRP);
			} catch (JASCIEXCEPTION e) {
							}
			if(ObjListGeneralCodes!=null && !ObjListGeneralCodes.isEmpty())
			{
			try{
				for(GENERALCODES objGeneralCode:ObjListGeneralCodes)
			{
				if(objGeneralCode.getId().getGeneralCode().equalsIgnoreCase(ObjLocationProfile.getProfileGroup()))
			       {
					Objlocationprofilebe.setProfileGroup(objGeneralCode.getDescription20());
			       }
				
			}
			}catch(Exception e){}	
			}
			
			
			
			List<FULFILLMENTCENTERSPOJO> FulfillmentCenterList=null;
			try {
				FulfillmentCenterList = objLocationProfileDal.getFulfillmentCenter(ObjCommonSessionBe.getTenant());
			} catch (JASCIEXCEPTION e) {
							}
			if(FulfillmentCenterList!=null && !FulfillmentCenterList.isEmpty())
			{
			try{
				for(FULFILLMENTCENTERSPOJO objFulfillmentCenter:FulfillmentCenterList)
			{
				if(objFulfillmentCenter.getId().getFulfillmentCenter().equalsIgnoreCase(ObjLocationProfile.getLocationProfileId().getFulfillmentCenter()))
			       {
					Objlocationprofilebe.setFullfillment_Center_ID_Description(objFulfillmentCenter.getName20());
			       }
				
			}
			}catch(Exception e){}	
			}
			
			Objlocationprofilebe.setLocationProfile(ObjLocationProfile.getLocationProfileId().getProfile());
			Objlocationprofilebe.setCompany(ObjLocationProfile.getLocationProfileId().getCompany());
			Objlocationprofilebe.setFulFillmentCenter(ObjLocationProfile.getLocationProfileId().getFulfillmentCenter());
			Objlocationprofilebe.setTenant(ObjLocationProfile.getLocationProfileId().getTenant());
     	
     		List<NOTES> ObjListNotes=null;
			try {
				ObjListNotes=objLocationProfileDal.GetNotes(ObjCommonSessionBe.getTenant(), ObjCommonSessionBe.getCompany(),ObjLocationProfile.getLocationProfileId().getProfile(),GLOBALCONSTANT.LocationProfile_NotesId);
			} catch (JASCIEXCEPTION objJasciexception) {}
			if(ObjListNotes!=null)
			{
				try{
					Objlocationprofilebe.setNotes(ObjListNotes.get(GLOBALCONSTANT.INT_ZERO).getNote());
				}catch(Exception e){
					//Objlocationprofilebe.setNotes(GLOBALCONSTANT.LocationProfile_NotesError);
					Objlocationprofilebe.setNotes(GLOBALCONSTANT.BlankString);
				}
			}
			objListLocationprofilebe.add(Objlocationprofilebe);
		}
		return objListLocationprofilebe;
	}

	/**
	 * Created on:Jan 05 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to get record of LocationProfile for updation.
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILEBE
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	@Transactional
	public LOCATIONPROFILEBE editLocationProfile(String locationProfile,String profileGroup, String fulfillmentCenter) throws JASCIEXCEPTION 
	{
		LOCATIONPROFILEBE objLocationProfileBe=new LOCATIONPROFILEBE();
		List<LOCATIONPROFILES> ObjListLocationProfiles=null;
		 DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);

		ObjListLocationProfiles=objLocationProfileDal.CheckAlreadyExists(locationProfile,profileGroup,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),fulfillmentCenter);
		LOCATIONPROFILES ObjLocationProfiles=ObjListLocationProfiles.get(GLOBALCONSTANT.INT_ZERO);
		
		Date ObjLastActivityDate =ObjLocationProfiles.getLastActivityDate();
		Date ObjLastUsedDate=ObjLocationProfiles.getLastUsedDate();
		 String StrLastActivityDate=GLOBALCONSTANT.BlankString;
		 String StrLastUsedDate=GLOBALCONSTANT.BlankString;
		 try{
			 StrLastActivityDate=ObjDateFormat1.format(ObjLastActivityDate);
			 
		 }catch(Exception e)
		 {}
		 try
		 {
			 StrLastUsedDate=ObjDateFormat1.format(ObjLastUsedDate);
		 }catch(Exception e){}
		objLocationProfileBe.setLastActivityDate(StrLastActivityDate);
		objLocationProfileBe.setLastUsedDate(StrLastUsedDate);
		objLocationProfileBe.setLastActivityTeamMember(getTeamMemberName(ObjLocationProfiles.getLastActivityTeamMember()));
		objLocationProfileBe.setLastUsedTeamMember(getTeamMemberName(ObjLocationProfiles.getLastUsedTeamMember()));
		objLocationProfileBe.setAllocationAllowable(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getAllocationAllowable()));
		objLocationProfileBe.setFulFillmentCenter(StringEscapeUtils.escapeHtml(fulfillmentCenter));
		objLocationProfileBe.setProfileGroup(StringEscapeUtils.escapeHtml(profileGroup));
		objLocationProfileBe.setLocationProfile(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getLocationProfileId().getProfile()));
		objLocationProfileBe.setDescription20(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getDescription20()));
		objLocationProfileBe.setDescription50(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getDescription50()));
		objLocationProfileBe.setLocationType(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getType()));
		if(ObjLocationProfiles.getLocationHeight()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setLocationHeight(GLOBALCONSTANT.BlankString);
		}
		else
		{

		String dvalue=BigDecimal.valueOf(ObjLocationProfiles.getLocationHeight()).toPlainString();
		
		
		objLocationProfileBe.setLocationHeight(dvalue);
		}
	
		if(ObjLocationProfiles.getLocationWidth()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setLocationWidth(GLOBALCONSTANT.BlankString);
		}
		else
			{
			String dvalue=BigDecimal.valueOf(ObjLocationProfiles.getLocationWidth()).toPlainString();

			objLocationProfileBe.setLocationWidth(dvalue);
			}
		if(ObjLocationProfiles.getLocationDepth()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setLocationDepth(GLOBALCONSTANT.BlankString);
		}
		else
		{
			
			String dvalue=BigDecimal.valueOf(ObjLocationProfiles.getLocationDepth()).toPlainString();
		objLocationProfileBe.setLocationDepth(dvalue);
		}
		if(ObjLocationProfiles.getLocationWeightCapacity()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setLocationWeightCapacity(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=BigDecimal.valueOf(ObjLocationProfiles.getLocationWeightCapacity()).toPlainString();
		objLocationProfileBe.setLocationWeightCapacity(dvalue);
		}
		if(ObjLocationProfiles.getLocationHeightCapacity()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setLocationHeightCapacity(GLOBALCONSTANT.BlankString);
		}
		else{
			String dvalue=BigDecimal.valueOf(ObjLocationProfiles.getLocationHeightCapacity()).toPlainString();
		objLocationProfileBe.setLocationHeightCapacity(dvalue);
		}
		if(ObjLocationProfiles.getNumberPalletsFit()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setNumberOfPallets(GLOBALCONSTANT.BlankString);
		}
		else{
		
		objLocationProfileBe.setNumberOfPallets(ObjLocationProfiles.getNumberPalletsFit().toString());
		}
		if(ObjLocationProfiles.getNumberFloorLocations()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setNumberOfFloorPallet(GLOBALCONSTANT.BlankString);
		}
		else{
			objLocationProfileBe.setNumberOfFloorPallet(ObjLocationProfiles.getNumberFloorLocations().toString());
		}
		
		objLocationProfileBe.setFreeLocation(ObjLocationProfiles.getFreeLocationWhenZero());
		if(ObjLocationProfiles.getFreePrimeDays()==GLOBALCONSTANT.INT_ZERO)
		{
		    objLocationProfileBe.setPrimeDays(GLOBALCONSTANT.BlankString);	
		}
		
		else{
			objLocationProfileBe.setPrimeDays(ObjLocationProfiles.getFreePrimeDays().toString());
		}
		objLocationProfileBe.setMultipleProducts(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getMutipleProductsInLocation()));
		objLocationProfileBe.setStorageType(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getStorageType()));
		objLocationProfileBe.setSlotting(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getSlotting()));
		objLocationProfileBe.setLocationCheck(StringEscapeUtils.escapeHtml(ObjLocationProfiles.getLocationCheck()));
		if(ObjLocationProfiles.getCCActivityPoint()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setCCActivityPoints(GLOBALCONSTANT.BlankString);
		}
		else{
			objLocationProfileBe.setCCActivityPoints(ObjLocationProfiles.getCCActivityPoint().toString());
		}
		if(ObjLocationProfiles.getCCHighValueAmount()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setCCAmount(GLOBALCONSTANT.BlankString);
		}
		else{
			objLocationProfileBe.setCCAmount(ObjLocationProfiles.getCCHighValueAmount().toString());
		}
		if(ObjLocationProfiles.getCCHighValueFactor()==GLOBALCONSTANT.INT_ZERO)
		{
			objLocationProfileBe.setCCFactor(GLOBALCONSTANT.BlankString);
		}
		else{
		objLocationProfileBe.setCCFactor(ObjLocationProfiles.getCCHighValueFactor().toString());
		}
		
		
	
		return objLocationProfileBe;
	}
	
	
	/**
	 * Created on:Jan 06 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to reassign the LocationProfile.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RestFull_ReassignLocation, method = RequestMethod.POST) 
	public @ResponseBody Boolean ReassignLocation(@RequestParam(value=GLOBALCONSTANT.RestParam_ToSelectLocation) String TolocationProfile,@RequestParam(value=GLOBALCONSTANT.RestParam_FromlocationProfile) String FromlocationProfile,@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String VarFulFillmentCenterID)
	{
		Boolean Status=false;
		try{
		Status=objLocationProfileDal.ReassignLocation(TolocationProfile,FromlocationProfile,ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),VarFulFillmentCenterID);
		}catch(JASCIEXCEPTION ObjJasciException)
		{}
		
		if(Status==true)
		{
			return true;
		}
		else
		{
			return false;

		}
		
		
	}


	/**
	 * Created on:Jan 05  2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get fulfillmentcenterId and FulfillmentCenter name.
	 * Input parameter: String
	 * Return Type :List<FULFILLMENTCENTERSPOJO>
	 * 
	 */
	@Transactional
	public List<FULFILLMENTCENTERSPOJO> getFulFillmentCenter(String tenant,String company,String PageName) throws JASCIEXCEPTION
	{
		List<Object> objListObject=objLocationProfileDal.getfulFillmentCenter(tenant, company, PageName);		
		List<FULFILLMENTCENTERSPOJO> ObjListFulfillmentcenter=new ArrayList<FULFILLMENTCENTERSPOJO>();
		for (Object object : objListObject) 
		{
			Object[] object_arr = (Object[]) object;
			FULFILLMENTCENTERSPOJO FulfilmentcenterObj = new FULFILLMENTCENTERSPOJO();
			FULFILLMENTCENTERS_PK fulfillmentcenters_PK=new FULFILLMENTCENTERS_PK();

			try{
				fulfillmentcenters_PK.setFulfillmentCenter(object_arr[GLOBALCONSTANT.INT_ZERO].toString().replace(GLOBALCONSTANT.DoubleQuote, GLOBALCONSTANT.StringDoubleQuote));
				fulfillmentcenters_PK.setTenant(ObjCommonSessionBe.getTenant());
				FulfilmentcenterObj.setId(fulfillmentcenters_PK);
				
			}
			catch(Exception e)
			{}
			try{
				FulfilmentcenterObj.setName20(object_arr[GLOBALCONSTANT.IntOne].toString());
			}
				catch(Exception e)
			    { 
					FulfilmentcenterObj.setName20(GLOBALCONSTANT.BlankString);
				}
			ObjListFulfillmentcenter.add(FulfilmentcenterObj);
			}
		
		
		return ObjListFulfillmentcenter;
	}
	
	
	 /**
		 * 
		 * @author Diksha Gupta
		 * @Date Dec 18, 2014
		 * @param LANGUAGETRANSLATIONBE
		 * @throws JASCIEXCEPTION
		 * @Return Boolean
		 *
		 */ 
      @Transactional
		public String getTeamMemberName(String Teammember)
		{           
     String TeamMemberName=GLOBALCONSTANT.BlankString;
		List<TEAMMEMBERS> objTeammembers=null;
		try{
			objTeammembers=ObjMenuAppIconDal.getTeamMemberName(ObjCommonSessionBe.getTenant(),Teammember);
		}catch(Exception e){}
		try{
			TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
		}catch(Exception e){}
		
     return TeamMemberName;
		
		}
    
		
	
	/**
	 * Created on:Dec 29  2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to set screen labels for location profile from data getting from language table
	 * Input parameter: LANGUAGES
	 * Return Type :List<>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strstatic_access)
	@Transactional
	public LOCATIONPROFILESCREENLABELSBE getScreenLabels(String StrLanguage) throws JASCIEXCEPTION
	{
		LOCATIONPROFILESCREENLABELSBE ObjLocationProfileScreenBe=new LOCATIONPROFILESCREENLABELSBE();
		/** to set LanguageTranslationScreen data from languages data from table LANGUAGES */
		List<LANGUAGES> objLanguagesList=ObjLanguageLabelsApi.GetScreenLabels(ObjLanguageLabelsApi.strLocationProfileModule,StrLanguage);
		for (LANGUAGES ObjLanguages : objLanguagesList) 
		{
			String StrKeyPhrase=ObjLanguages.getId().getKeyPhrase().trim();
			 if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
			 {
				 ObjLocationProfileScreenBe.setLocationProfiles_Actions(ObjLanguages.getTranslation());
			 }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_AddnewBtn(ObjLanguages.getTranslation());
			 }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Profile_Lookup))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LookupLabel(ObjLanguages.getTranslation());
		    	 
			 }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Profile))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationProfile(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Profile_Group))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_ProfileGroup(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_Location_Profile_Description))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_PartOfLocation(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NewBtn(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_DisplayAllBtn(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Profile_Search_Lookup))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_SearchLookupLabel(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Description(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Locations))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NumberOfLocations(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Used_Date))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastUsedDate(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Used_By))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastUsedBy(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Change_Date))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastChangeDate(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Change_By))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastChangeBy(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Actions(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_AddnewBtn(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Copy))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_CopyBtn(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Notes))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NotesBtn(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_EditBtn(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_DeleteBtn(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reassign_Locations))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_ReassignLocationBtn(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastActivityBy(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastActivityDate(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Assigned_Date_YYYY_MM_DD))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastAssignedDate(ObjLanguages.getTranslation());
		     } 
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Assigned_TeamMember))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LastAssignedTeammember(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_FulFillment_Center))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_FulfillmentcenterLabel(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_DescriptionShort(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Long))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_DescriptionLong(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Type))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationType(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationHeight(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Width))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationWidth(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Depth))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationDepth(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Weight_Capacity))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationWeightCapacity(ObjLanguages.getTranslation());
		     }
	 
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_Capacity))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationHeightCapacity(ObjLanguages.getTranslation());
		     }
	 
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Pallets_That_Fit_In_Location))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NumberOfPallets(ObjLanguages.getTranslation());
		     }
	 
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Floor_Pallet_Location))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NumberOfFloorPallet(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Allocation_Allowable))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_AllocationAllowable(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Free_Location_When_Zero))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_FreeLocationWhenZero(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Free_Prime_Location_in_99999_Days))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_FreePrimeDays(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Multiple_Products_in_The_Same_Locations))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_MultipleProducts(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Storage_Type))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_StorageTypes(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Slotting))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Slotting(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Check))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationCheck(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cycle_Count_Activity_Points))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_CCActivityPoints(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cycle_Count_High_Value_Amount))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_CCActivityHighAmount(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cycle_Count_High_Value_Factor))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_CCActivityHighFactor(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_SaveUpdate(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Cancel(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_From_Location_Profile))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_FromProfile(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_To_Location_Profile))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_ToProfile(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Apply))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Apply(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Profile_Maintenance))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_MaintenanceLabel(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Must_Reassign_Location_Profile_before_Deletion))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_ReassignBeforeDeletion(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_a_From_Profile_first_to_proceed))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_SelectFromProfile(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_a_To_Profile_first_to_proceed))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_SelectToProfile(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_There_are_no_locations_to_reassign_for_the_selected_Location_Profile))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NoLocationToReassign(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_DeleteMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Profile_Already_Exist))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_AlreadyExistsErrorMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_MandatoryFieldErrorMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Location_Profile))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_InvalidLocationProfile(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Profile_Group))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_InvalidProfileGroup(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Location_Profile))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_BlankLocationProfile(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Profile_Group))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_BlankProfileGroup(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_part_of_location_profile_description))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_InvalidPart(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_part_of_location_profile_description))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_BlankPartOfLocation(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_must_be_numeric_with_3_decimal_values_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationHeightErrorMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Width_must_be_numeric_with_3_decimal_values_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationWidthErrorMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Depth_must_be_numeric_with_3_decimal_values_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationDepthErrorMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationWeightCapacityErrorMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationHeightCapacityErrorMessage(ObjLanguages.getTranslation());
		     }
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Pallets_that_fit_in_the_Location_must_be_numeric_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NumberOfPalletErrorMessage(ObjLanguages.getTranslation());
		     } 
			 else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Floor_Pallet_Location_must_be_numeric_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_NumberOfFloorPalletErrorMessage(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Free_Prime_Location_in_99999_Days_must_be_numeric_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_FreePrimeLocationErrorMessage(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cycle_Count_Activity_Points_must_be_numeric_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_CCActivityPointsErrorMessage(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cycle_Count_High_Value_Amount_must_be_numeric_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_CCAmountErrorMessage(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cycle_Count_High_Value_Factor_must_be_numeric_only))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_CCFactorErrorMessage(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_SavedMessage(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_UpdatedMessage(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_SelectDropDown(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_There_is_no_other_location_profile_for_this_fulfillment_center))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short_already_used))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Description_Short_is_already_Used(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_long_already_used))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Description_Long_is_already_Used(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_Reset(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Weight_Capacity_must_be_less_then_or_equal_to_Location_Width))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth(ObjLanguages.getTranslation());
		     }
		     else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_Capacity_must_be_less_then_or_equal_to_Location_Height))
		     {
		    	 ObjLocationProfileScreenBe.setLocationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight(ObjLanguages.getTranslation());
		     }
	 
		}
		
		return ObjLocationProfileScreenBe;
	}
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description20 in LOCATIONPROFILEBE TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description20
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationProfile_Restfull_RestCheckUniqueLocationProfileDescriptionShort, method = RequestMethod.POST)
	public @ResponseBody Boolean getDescription20Unique(@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String StrFulFillmentCenter,@RequestParam(value=GLOBALCONSTANT.RestParam_Description20) String StrDescription20,@RequestParam(value=GLOBALCONSTANT.ScreenName) String StrScreenName,@RequestParam(value=GLOBALCONSTANT.RestParam_Profile) String LocationProfile)  throws JASCIEXCEPTION {	

		List<LOCATIONPROFILEBE> ObjectLocationProfile=null;

		ObjectLocationProfile = objLocationProfileDal.getDescription20Unique(ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),StrFulFillmentCenter,StrDescription20,StrScreenName,LocationProfile); 
		
			if(ObjectLocationProfile.size()>GLOBALCONSTANT.INT_ZERO){
				return true;				
			}
			else{
				return false;
			}
	}
	

	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description50 in LOCATIONPROFILEBE TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description50
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationProfile_Restfull_RestCheckUniqueLocationProfileDescriptionLong, method = RequestMethod.POST)
	public @ResponseBody Boolean getDescription50Unique(@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String StrFulFillmentCenter,@RequestParam(value=GLOBALCONSTANT.RestParam_Description50) String StrDescription50,@RequestParam(value=GLOBALCONSTANT.ScreenName) String StrScreenName,@RequestParam(value=GLOBALCONSTANT.RestParam_Profile) String LocationProfile)  throws JASCIEXCEPTION {	

		List<LOCATIONPROFILEBE> ObjectLocationProfile=null;

		ObjectLocationProfile = objLocationProfileDal.getDescription50Unique(ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getCompany(),StrFulFillmentCenter,StrDescription50,StrScreenName,LocationProfile); 
		
		if(ObjectLocationProfile.size()>GLOBALCONSTANT.INT_ZERO){
				return true;				
			}
			else{
				return false;
			}
		
	}	
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check no of location for reassign location profile. 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description50
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.LocationProfile_Restfull_RestGetNumberOfLocation, method = RequestMethod.POST)
	public @ResponseBody Boolean getNoofLocation(@RequestParam(value=GLOBALCONSTANT.RestParam_FulFillmentCenter) String StrFulFillmentCenter,@RequestParam(value=GLOBALCONSTANT.RestParam_Profile) String LocationProfile)  throws JASCIEXCEPTION {	

		List<Object> ObjListObject=null;
		try {
			ObjListObject=objLocationProfileDal.GetNumberOfLocations(ObjCommonSessionBe.getTenant(), ObjCommonSessionBe.getCompany(),StrFulFillmentCenter,LocationProfile);
		} catch (JASCIEXCEPTION ObjException) {
			
		}
		String NumberOfLocations=ObjListObject.get(GLOBALCONSTANT.INT_ZERO).toString();
		
		if(NumberOfLocations.equalsIgnoreCase(GLOBALCONSTANT.ZeroValue)){
				return false;				
			}
		
			else{
				return true;
			}
		
	}
	}
