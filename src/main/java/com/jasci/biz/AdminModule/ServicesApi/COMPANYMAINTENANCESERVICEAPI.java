/**

Date Developed :Dec 22 2014
Created by: Rahul kumar
Description :COMPANYMAINTENANCESERVICEAPI Controller TO HANDLE THE REQUESTS FROM Service Interface 
*/
package com.jasci.biz.AdminModule.ServicesApi;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;



@Controller
public class COMPANYMAINTENANCESERVICEAPI {
	
	
	@Autowired
	private ICOMPANYMAINTENANCEDAL objIcompanydal;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	
	
	/**
	 * @Description get All companies for specific tenant
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param Tenant
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIES>
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Companies_Restfull_GetCompanies, method = RequestMethod.GET)
	public @ResponseBody   List<COMPANIES> getAllCompanies(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant) throws JASCIEXCEPTION {	

		return objIcompanydal.getCompanies(Tenant);
		
	}


	
/**
 * @Description set screen label by team member language
 * @author Rahul Kumar
 * @Date Dec 23, 2014 
 * @param StrScreenName
 * @param StrLanguage
 * @return
 * @throws JASCIEXCEPTION
 * List<LANGUAGES>
 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_SetScreenLanguage, method = RequestMethod.GET)
	public @ResponseBody  List<LANGUAGES> setScreenLanguage(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_ScreenName) String StrScreenName,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_SetScreenLanguage) String StrLanguage) throws JASCIEXCEPTION {	

		return objIcompanydal.setScreenLanguage(StrScreenName, StrLanguage);

	
	}
	
	
	
	
	
	/**
	 * @Description get company by company id from companies table
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param CompanyID
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIES>
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_GetCompanyByID, method = RequestMethod.POST)
	public @ResponseBody  List<Object> getCompanyById(@RequestParam(value=GLOBALCONSTANT.Company_Restfull_CompanyByID) String CompanyID) throws JASCIEXCEPTION {	

		
		List<COMPANIES> objCompanies=null;
		String Tenant=OBJCOMMONSESSIONBE.getTenant();
		List<Object> compList=new ArrayList<Object>();
		
		objCompanies=objIcompanydal.getCompanyById(Tenant, CompanyID);
		
		if(objCompanies.size() > GLOBALCONSTANT.INT_ZERO){
			compList.add(objCompanies.get(GLOBALCONSTANT.INT_ZERO).getStatus().toUpperCase());
		}
		
		return compList;
	
	}
	
	
	
	/**
	 * * @Description get company by company part from companies table
	 * @author Rahul Kumar
	 * @Date Dec 24, 2014 
	 * @param CompanyPartName
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIES>
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_GetCompanyByPartName, method = RequestMethod.POST)
	public @ResponseBody  List<COMPANIES> getCompanyByPartName(@RequestParam(value=GLOBALCONSTANT.Company_Restfull_CompanyPartName) String CompanyPartName) throws JASCIEXCEPTION {	

		
		List<COMPANIES> objCompanies=null;
		String Tenant=OBJCOMMONSESSIONBE.getTenant();
		
		
		objCompanies=objIcompanydal.getCompanyByPartName(Tenant, CompanyPartName);	
		
		
		return objCompanies;
	
	}
	
	
	
	/**
	 * @Description get general code by general code id form General code table
	 * @author Rahul Kumar
	 * @Date Dec 24, 2014 
	 * @param Tenant
	 * @param Company
	 * @param GeneralCodeId
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<GENERALCODES>
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_GeneralCode, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODES> getGeneralCode(@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_Company) String Company,@RequestParam(value=GLOBALCONSTANT.TeamMemebers_Restfull_GeneralCodeId) String GeneralCodeId) throws JASCIEXCEPTION {	

		return objIcompanydal.getGeneralCode(Tenant, Company, GeneralCodeId);

	}
	
	
	
	

	/**
	 * @Description upload image on amazon server
	 * @param request
	 * @param response
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally, GLOBALCONSTANT.Strrawtypes })
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_UploadImage, method = RequestMethod.POST)
	public @ResponseBody  List<Object>  uploadIcon(HttpServletRequest request,HttpServletResponse response) throws JASCIEXCEPTION {
		
		List<Object> obj=new ArrayList<Object>();
		
		/*Properties ObjectProperty = new Properties();
		String propFileName = GLOBALCONSTANT.ConfigFilePathWithName;
		try {
			InputStream ObjectInputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			ObjectProperty.load(ObjectInputStream);
		} catch (IOException ObjectIOException) {
		}*/
		CONFIGURATIONEFILE ObjConfiguration=new CONFIGURATIONEFILE();
		String AMAZON_ACCESS_KEY = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_AccessKey);
		String AMAZON_SECRET_KEY = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_SecrateKey);
		
		String S3_BUCKET_NAME = ObjConfiguration.getConfigProperty(GLOBALCONSTANT.CompanyMaintenanceIcon_BucketName);	
		
		
		ObjConfiguration.getConfigProperty(GLOBALCONSTANT.MenuAppIcon_ServerName);
		
		  String urlfile=GLOBALCONSTANT.BlankString;
	      
		
		try{
		// needed for cross-domain communication
        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Origin, GLOBALCONSTANT.ASTRIK);
        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Methods,GLOBALCONSTANT.POST);
        response.setHeader(GLOBALCONSTANT.Access_Control_Allow_Headers, GLOBALCONSTANT.Access_Control_Allow_Headers_Content_Type);
        response.setHeader(GLOBALCONSTANT.Access_Control_MaxAge, GLOBALCONSTANT.Access_Control_Allow_Max_Age_Value);
        
       
		
        
     // checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
           // PrintWriter writer = response.getWriter();
           // writer.println("Request does not contain upload data");
           // writer.flush();
            //return GLOBALCONSTANT.BlankString;
        }
        
        
        
     // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(GLOBALCONSTANT.THRESHOLD_SIZE);
 
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(GLOBALCONSTANT.MAX_FILE_SIZE);
        upload.setSizeMax(GLOBALCONSTANT.MAX_REQUEST_SIZE);
         
        
        String uuidValue =UUID.randomUUID().toString();
        FileItem itemFile = null;
        
        try {
            // parses the request's content to extract file data
            List formItems = upload.parseRequest(request);
            Iterator iter = formItems.iterator();
 
            // iterates over form's fields to get UUID Value
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (item.isFormField()) {
                    if (item.getFieldName().equalsIgnoreCase(GLOBALCONSTANT.UUID_STRING)) {
                        uuidValue = UUID.randomUUID().toString();
                    }
                }
                // processes only fields that are not form fields
                if (!item.isFormField()) {
                    itemFile = item;
                }
            }
 
            if (itemFile != null) {
                // get item inputstream to upload file into s3 aws
 
                BasicAWSCredentials awsCredentials = new BasicAWSCredentials(AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY);
 
                AmazonS3 s3client = new AmazonS3Client(awsCredentials);
                
                /*List<Bucket> buckets = s3client.listBuckets();
                for (Bucket bucket : buckets) {
                        System.out.println(bucket.getName());
                               
                }*/
                try {
 
                    ObjectMetadata om = new ObjectMetadata();
                    om.setContentLength(itemFile.getSize());
                    String ext = FilenameUtils.getExtension(itemFile.getName());
                    String keyName = uuidValue + GLOBALCONSTANT.DOT + ext;
 
                    s3client.putObject(new PutObjectRequest(S3_BUCKET_NAME, keyName, itemFile.getInputStream(), om));
                    s3client.setObjectAcl(S3_BUCKET_NAME, keyName, CannedAccessControlList.PublicRead);
                    
                    
                    
                    try{
              		  GeneratePresignedUrlRequest requestUrl = new GeneratePresignedUrlRequest(S3_BUCKET_NAME, keyName);
              		  URL url=s3client.generatePresignedUrl(requestUrl);
              		  String protocol=url.getProtocol();
              		  String host=url.getHost();
              		  String file=url.getFile();
              		  urlfile=protocol+GLOBALCONSTANT.Forward_Double_Slash+host+file;
              		  
              		  }catch(Exception e){
              			urlfile=GLOBALCONSTANT.QUESTION_MARK;
              			 // e.printStackTrace();
              		  }
                    
                    
 
                } catch (AmazonServiceException ase) {
                	urlfile=GLOBALCONSTANT.QUESTION_MARK;
                   // LOGGER.error(uuidValue + ":error:" + ase.getMessage());
                //	ase.printStackTrace();
 
                } catch (AmazonClientException ace) {
                	urlfile=GLOBALCONSTANT.QUESTION_MARK;
                	//ace.printStackTrace();
                    //LOGGER.error(uuidValue + ":error:" + ace.getMessage());
                }
 
 
            } else {
                //LOGGER.error(uuidValue + ":error:" + "No Upload file");
            }
 
        } catch (Exception ex) {
        	urlfile=GLOBALCONSTANT.QUESTION_MARK;
        	//ex.printStackTrace();
            //LOGGER.error(uuidValue + ":" + ":error: " + ex.getMessage());
        }
        

		}catch(Exception objException){
			//objException.printStackTrace();
			urlfile=GLOBALCONSTANT.QUESTION_MARK;
		}
	
		finally
		{
			urlfile=urlfile.substring(GLOBALCONSTANT.INT_ZERO,urlfile.indexOf(GLOBALCONSTANT.QUESTION_MARK));			
			obj.add(urlfile);
			return obj;
		}
		
	}
	 
	
	

	/**
	 * 
	 * @Description add or update company into company table 
	 * @author Rahul Kumar
	 * @Date Dec 26, 2014 
	 * @param objCompanies
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type Boolean
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_AddOrUpdate, method = RequestMethod.GET)
	public Boolean addOrUpdateCompany(@RequestParam(value=GLOBALCONSTANT.Company_Restfull_Companies) COMPANIES objCompanies) throws JASCIEXCEPTION{
		
		return objIcompanydal.addOrUpdateCompany(objCompanies);
	}
	
	/**
	 * 
	 * @Description get company by its id and tenant from companies table 
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param Tenant
	 * @param CompanyiD
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type List<COMPANIESBE>
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_GetCompanyByIdTenant, method = RequestMethod.GET)
	public List<COMPANIES> getCompanyById(@RequestParam(value=GLOBALCONSTANT.Company_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.Company_Restfull_CompanyByID) String CompanyID) throws JASCIEXCEPTION {
		
		return objIcompanydal.getCompanyById(Tenant, CompanyID);
	}


	
	/**
	 * 
	 * @Description call DAL function to delete existing company records 
	 * @author Rahul Kumar
	 * @Date Dec 29, 2014 
	 * @param Tenant
	 * @param CompanyID
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type Boolean
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_DeleteCompanyByIdTenant, method = RequestMethod.GET)
	public Boolean deleteCompany(@RequestParam(value=GLOBALCONSTANT.Company_Restfull_Tenant) String Tenant,@RequestParam(value=GLOBALCONSTANT.Company_Restfull_CompanyByID) String CompanyID) throws JASCIEXCEPTION {
		
		return objIcompanydal.deleteCompany(Tenant, CompanyID);
	}
	
	
	
	/**
	 * 
	 *@Description check unique email from table companies
	 *@Auther Rahul Kumar
	 *@Date Jan 20, 2015			
	 *@Return type Boolean
	 *@param Email
	 *@return
	 * @throws JASCIEXCEPTION 
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Company_Restfull_CheckUniqueEmail, method = RequestMethod.POST)
	public @ResponseBody  Boolean checkEmail(@RequestParam(value=GLOBALCONSTANT.Company_Restfull_CompaniesEmail) String Email) throws JASCIEXCEPTION {	

		//return objIcompanydal.isEmailUnique(Email);
		return false;

	}
}



