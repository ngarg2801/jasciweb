/*

Description This class is used to make restfull service for COMMON use of all screen
Created By Deepak Sharma
Created Date Dec 26 2014

 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.be.GOJSPROPERTIESBE;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.GETINFOHELP;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.GETINFOHELPS;
import com.jasci.common.utildal.GETINFOHELPDAL;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class COMMONSERVICEAPI {

	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	
	@Autowired
	GETINFOHELP ObjGetInfoHelp;
	
	@Autowired
	private GETINFOHELPDAL objgetinfohelp;
	
	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = GLOBALCONSTANT.HEADER_M_HeaderLanguageChange, method = RequestMethod.GET)
	public @ResponseBody WEBSERVICESTATUS HeaderLanguageChange(){
		WEBSERVICESTATUS objWEBSERVICESTATUS = new WEBSERVICESTATUS();
		if(OBJCOMMONSESSIONBE.getCurrentLanguage().equalsIgnoreCase(OBJCOMMONSESSIONBE.getTenant_Language()))
				{
			OBJCOMMONSESSIONBE.setCurrentLanguage(OBJCOMMONSESSIONBE.getTeam_Member_Language());
			objWEBSERVICESTATUS.setBoolStatus(true);
				}
		else if(OBJCOMMONSESSIONBE.getCurrentLanguage().equalsIgnoreCase(OBJCOMMONSESSIONBE.getTeam_Member_Language()))
		{
			OBJCOMMONSESSIONBE.setCurrentLanguage(OBJCOMMONSESSIONBE.getTenant_Language());
			objWEBSERVICESTATUS.setBoolStatus(true);
		}
		else
		{
			objWEBSERVICESTATUS.setBoolStatus(false);
		}
		
		return objWEBSERVICESTATUS;
	}
	
	/**
	 * 
	 * @param StrInfoHelp
	 * @param StrInfoHelpType
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = GLOBALCONSTANT.HEADER_M_HeaderInfoHelp, method = RequestMethod.POST)
	public @ResponseBody WEBSERVICESTATUS HeaderInfoHelp(
			@RequestParam(value = GLOBALCONSTANT.HEADER_R_P_InfoHelp) String StrInfoHelp,
			@RequestParam(value = GLOBALCONSTANT.HEADER_R_P_InfoHelpType) String StrInfoHelpType,
			HttpServletRequest request,
			HttpServletResponse response){
		
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = new WEBSERVICESTATUS();
		
		List<GETINFOHELPS> lstGetinfohelps = new ArrayList<GETINFOHELPS>();
		try {
			lstGetinfohelps = ObjGetInfoHelp.GetInfoHelpDetails(StrInfoHelp, OBJCOMMONSESSIONBE.getCurrentLanguage(), StrInfoHelpType, request, response);
		} catch (JASCIEXCEPTION e) {
			}
		
		if(lstGetinfohelps.size()>GLOBALCONSTANT.INT_ZERO)
		{
			objWEBSERVICESTATUS.setBoolStatus(true);
			objWEBSERVICESTATUS.setStrMessage(lstGetinfohelps.get(GLOBALCONSTANT.INT_ZERO).getExecution());
			
		}
		else
		{
			/*objWEBSERVICESTATUS.setBoolStatus(false);*/
			
			try{

				//objWEBSERVICESTATUS.setBoolStatus(false);
				lstGetinfohelps = ObjGetInfoHelp.GetInfoHelpDetails(StrInfoHelp=GLOBALCONSTANT.DEFAULT, OBJCOMMONSESSIONBE.getCurrentLanguage(), StrInfoHelpType=GLOBALCONSTANT.DEFAULT, request, response);
			}catch(JASCIEXCEPTION e){}
				objWEBSERVICESTATUS.setBoolStatus(true);
				objWEBSERVICESTATUS.setStrMessage(lstGetinfohelps.get(GLOBALCONSTANT.INT_ZERO).getExecution());
			}
			return objWEBSERVICESTATUS;
	}

	
	@RequestMapping(value = GLOBALCONSTANT.COMMON_M_SetDeviceType, method = RequestMethod.POST)
	public @ResponseBody WEBSERVICESTATUS getDeviceType(
			@RequestParam(value = GLOBALCONSTANT.COMMON_R_P_DeviceType) String StrDeviceType
			){
		WEBSERVICESTATUS objWEBSERVICESTATUS = new WEBSERVICESTATUS();

		OBJCOMMONSESSIONBE.setMenuType(StrDeviceType);
		
			objWEBSERVICESTATUS.setBoolStatus(true);
		
		
		return objWEBSERVICESTATUS;
	}
	
	@RequestMapping(value = GLOBALCONSTANT.GOJS_Restfull_getGOJSConfigProp, method = RequestMethod.POST)
	public @ResponseBody GOJSPROPERTIESBE getGOJSConfigProperties() throws JASCIEXCEPTION{ 
		
		CONFIGURATIONEFILE OBJConfigurationefile = new CONFIGURATIONEFILE();
		return OBJConfigurationefile.getGoJSConfigProperty();
	}
	
	
	
}
