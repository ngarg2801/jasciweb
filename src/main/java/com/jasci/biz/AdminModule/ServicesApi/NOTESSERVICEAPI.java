/**

Date Developed :Dec 24 2014
Created by: Rakesh Pal
Description :NOTESSERVICEAPI Controller TO HANDLE THE REQUESTS FROM SCREENS
 */
package com.jasci.biz.AdminModule.ServicesApi;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jasci.biz.AdminModule.dao.INOTESDAL;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class NOTESSERVICEAPI {

	@Autowired
	private INOTESDAL objNotesDal;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;

	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 21, 2014
	 * @param StrNote_id
	 * @param StrTeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<NOTES>
	 *
	 */
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Rest_NotesScreen_Action, method = RequestMethod.GET)	
	public @ResponseBody  List<NOTES> getNotesList(@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Note_Link) String Note_Link,@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Note_Id) String Note_Id,@RequestParam(value=GLOBALCONSTANT.VarTenant_ID_getNotesList) String Tenant_ID, @RequestParam(value=GLOBALCONSTANT.VarCompnay_ID_getNotesList) String Company_ID) throws JASCIEXCEPTION {	
		
		return objNotesDal.getNotesList(Note_Link,Note_Id,Tenant_ID, Company_ID);	
		
	}
	
	
	
	/**
	 * 
	 * @author Rakesh Pal
	 * @param lastActivitydate 
	 * @Date Dec 21, 2014
	 * @param StrNote_id
	 * @param StrNote_link
	 * @param StrTeamMember_id
	 * @param StrCompany_Id
	 * @param Strtenant_Id
	 * @return 
	 * @throws JASCIEXCEPTION
	 * @Return List<NOTES>
	 *
	 */
	
	
	
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Notes_Update_Request_Action_rest, method = RequestMethod.GET)
	public @ResponseBody  List<NOTES> getnote(@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Note_Link)
	String Note_Link,@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Note_Id)
	String Note_Id,@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Tenant_Id)
	String Tenant_id,@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Company_Id)
	String Company_id,@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_ID) int ID) throws JASCIEXCEPTION {	
		
		return objNotesDal.getnote(Note_Link,Note_Id,Tenant_id,Company_id,ID);	
	}


	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 14, 2014
	 * @param ObjMenuappicons
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */

		
		@Transactional
		@RequestMapping(value = GLOBALCONSTANT.Notes_Restfull_AddOrUpdate, method = RequestMethod.GET)
		public @ResponseBody Boolean addOrUpdateNotesEntry(@RequestParam(value=GLOBALCONSTANT.notes_Restfull_Objnotes) NOTES objNotes) throws JASCIEXCEPTION {	

			return objNotesDal.addOrUpdateNotesEntry(objNotes);
		}







	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 31, 2014 
	 * @param Tenant_Id,
	 * @param Company_Id,note_id,note_link,lastupdatedDate
	 * @return
	 * @throws JASCIEXCEPTION
	 * Boolean
	 */
	@Transactional
	@RequestMapping(value = GLOBALCONSTANT.Notes_Restfull_Delete_note, method = RequestMethod.GET)
	public @ResponseBody  Boolean  deleteNote(@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Tenant_Id) String tenant_Id,@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Company_Id) String company_id,
			@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Note_Id) String note_id,
			@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_Note_Link) String note_link,
			@RequestParam(value=GLOBALCONSTANT.DataBase_Notes_ID) String ID
			) throws JASCIEXCEPTION {	

		return objNotesDal.deleteNote(tenant_Id,company_id,note_id,note_link,ID);
		
	}
	

}
