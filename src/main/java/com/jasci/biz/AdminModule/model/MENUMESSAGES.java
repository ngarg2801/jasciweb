/*

Date Developed  Sep 18 2014
Description It is used to make setter and getter for MenuMessages Record Set And Get
Created By Aakash Bishnoi
Created Date Dec 12 2014
 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_Menu_Messages)

@NamedNativeQueries({
	//Select All Data for Display All Button
	/*@NamedNativeQuery(			
	name = GLOBALCONSTANT.MenuMessage_QueryNameGetAllList,
	query = GLOBALCONSTANT.MenuMessage_QueryGetAllList,
	resultSetMapping = GLOBALCONSTANT.MenuMessage_ResultSetCompnayList
	),*/
	@NamedNativeQuery(			
	name = GLOBALCONSTANT.MenuMessage_QueryNameGetCompanyList,
	query = GLOBALCONSTANT.MenuMessage_QueryGetCompanyList,
	resultClass = COMPANIES.class
	/*resultSetMapping = GLOBALCONSTANT.MenuMessage_ResultSetCompnayList*/
	),	
	@NamedNativeQuery(			
	 name = GLOBALCONSTANT.MenuMessage_QueryNamefetchMenuMessage,
     query = GLOBALCONSTANT.MenuMessage_QueryfetchMenuMessage,
	 resultClass = MENUMESSAGES.class
			
			)
})

@SqlResultSetMapping(name =GLOBALCONSTANT.MenuMessage_ResultSetCompnayList,
columns = { 
 
		 @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnMessage_ID),
		 @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnTenant_ID),
		 @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnCompany_ID),
		 @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnStatus),
		
  @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnSTART_DATE),
  @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnEND_DATE),
  @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnTICKET_TAPE_MESSAGE),
  @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnMESSAGE),
  @ColumnResult(name = GLOBALCONSTANT.MenuMessage_ColumnName50),
 
})


public class MENUMESSAGES{

	@EmbeddedId
	private MENUMESSAGESPK Id;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessages_Ticket_Tape_Message)
	private String Ticket_Tape_Message;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessages_Message)
	private String Message;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessages_Start_Date)
	private Date Start_Date;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessages_End_Date)
	private Date End_Date;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessages_Last_Activity_Date)
	private Date Last_Activity_Date;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessages_Last_Activity_Team_Member)
	private String Last_Activity_Team_Member;
	
	public MENUMESSAGESPK getId() {
		return Id;
	}

	public void setId(MENUMESSAGESPK id) {
		Id = id;
	}
	public String getTicket_Tape_Message() {
		return Ticket_Tape_Message;
	}
	public void setTicket_Tape_Message(String ticket_Tape_Message) {
		Ticket_Tape_Message = ticket_Tape_Message;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	
	public Date getStart_Date() {
		return Start_Date;
	}
	public void setStart_Date(Date start_Date) {
		Start_Date = start_Date;
	}
	public Date getEnd_Date() {
		return End_Date;
	}
	public void setEnd_Date(Date end_Date) {
		End_Date = end_Date;
	}
	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}

	
	
}
