/*

Date Developed  Nov 20 2014
Created by: Aakash Bishnoi
Description  pojo class of Location Table except primary key 
Created On:Dec 23 2014
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_Locations)

@NamedNativeQueries({
	//Select DropDown Value Form GENERAL_CODE
	@NamedNativeQuery(			
	name = GLOBALCONSTANT.Location_QueryNameSelectGeneralCode,
	query = GLOBALCONSTANT.Location_QuerySelectGeneralCode,
        resultClass = GENERALCODES.class
	),


@NamedNativeQuery(
			
			name = GLOBALCONSTANT.Location_QueryNameUniqueWizardControlNumberCheck,
			query = GLOBALCONSTANT.Location_QueryWizardControlNumberCheck,
			resultClass = LOCATION.class
			),
@NamedNativeQuery(
			
			name = GLOBALCONSTANT.Location_QueryNameLocationLabelTypePrint,
			query = GLOBALCONSTANT.Location_QueryLocationLabelTypePrint,
			resultClass = LOCATION.class
			),
@NamedNativeQuery(
			
			name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchListLocations,
			query = GLOBALCONSTANT.Location_Wizard_QueryFetchListLocations,
			resultClass = LOCATION.class
			),
			@NamedNativeQuery(
					
					name = GLOBALCONSTANT.Location_Wizard_QueryNameUpdateLocations,
					query = GLOBALCONSTANT.Location_Wizard_QueryUpdateLocations,
					resultClass = LOCATION.class
					),
	@NamedNativeQuery(	
			//It is used to show display all functionality
			name = GLOBALCONSTANT.Location_QueryNameDisplayAll,
			query = GLOBALCONSTANT.Location_QueryDisplayAll,
		        resultClass = LOCATION.class
			),
		//Select DropDown Value Form LOCATION_PROFILES
		@NamedNativeQuery(			
		name = GLOBALCONSTANT.Location_QueryNameSelectLocationProfile,
		query = GLOBALCONSTANT.Location_QuerySelectLocationProfile,
	        resultClass = LOCATIONPROFILES.class
		),
		@NamedNativeQuery(	
				//This is used to check the uniqueness of location in LOCATION TABLE
		name = GLOBALCONSTANT.Location_QueryNameUniqueLocationCheck,
		query = GLOBALCONSTANT.Location_QueryUniqueLocationCheck,
		resultClass = LOCATION.class
		),
		@NamedNativeQuery(	
				//This is used to check the uniqueness of Alternate_location in LOCATION TABLE
		name = GLOBALCONSTANT.Location_QueryNameUniqueAlternateLocationCheck,
		query = GLOBALCONSTANT.Location_QueryUniqueAlternateLocationCheck,
		resultClass = LOCATION.class
		),
		@NamedNativeQuery(	
				//This is used to check the uniqueness of Alternate_location in LOCATION TABLE
		name = GLOBALCONSTANT.Location_QueryNameSearch,
		query = GLOBALCONSTANT.Location_QuerySearch,
		resultClass = LOCATION.class
		),
		@NamedNativeQuery(	
				//This is used to get the list from generalcode
		name = GLOBALCONSTANT.Location_QueryNameGeneralCode,
		query = GLOBALCONSTANT.Location_QueryGeneralCode,
		resultClass = GENERALCODES.class
		),
		@NamedNativeQuery(	
				//This is used to fetch list from Location table on behalf of Primary key
		name = GLOBALCONSTANT.Location_QueryNameFetchLocation,
		query = GLOBALCONSTANT.Location_QueryFetchLocation,
		resultClass = GENERALCODES.class
		),
		@NamedNativeQuery(	
				//This is used to fetch List from INVENTORY_LEVELS
		name = GLOBALCONSTANT.Location_QueryNameLocationExistForDelete,
		query = GLOBALCONSTANT.Location_QueryLocationExistForDelete,
		resultClass = INVENTORY_LEVELS.class
		),
		@NamedNativeQuery(	
				//This is used to fetch List from Fulfillmentcenter
		name = GLOBALCONSTANT.Location_QueryNameFulfillmentCenter,
		query = GLOBALCONSTANT.Location_QueryFulfillmentCenter,
		resultClass = FULFILLMENTCENTERSPOJO.class
		)
})
/*@SqlResultSetMappings({
	//Provide column name for get particular column into table
	 @SqlResultSetMapping(name = GLOBALCONSTANT.InfoHelps_ResultSetSelectInfoHelpType, columns = @ColumnResult(name = GLOBALCONSTANT.InfoHelps_ColumnResultInfohelps)),
	 @SqlResultSetMapping(name = GLOBALCONSTANT.InfoHelps_ResultSetLanguage, columns = @ColumnResult(name = GLOBALCONSTANT.InfoHelps_ColumnGeneralCodeID)),
})

*/

public class LOCATION {

	@EmbeddedId
	private LOCATIONPK Id;
	
	public LOCATIONPK getId() {
		return Id;
	}

	public void setId(LOCATIONPK id) {
		Id = id;
	}
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Location_DESCRIPTION20)
	private String Description20;
	@Column(name=GLOBALCONSTANT.DataBase_Location_DESCRIPTION50)
	private String Description50;
	@Column(name=GLOBALCONSTANT.DataBase_Location_WORK_GROUP_ZONE)
	private String Work_Group_Zone;
	@Column(name=GLOBALCONSTANT.DataBase_Location_WORK_ZONE)
	private String Work_Zone;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_TYPE)
	private String Location_Type;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_PROFILE)
	private String Location_Profile;
	@Column(name=GLOBALCONSTANT.DataBase_Location_WIZARD_ID)
	private String Wizard_ID;
	@Column(name=GLOBALCONSTANT.DataBase_Location_WIZARD_CONTROL_NUMBER)
	private Long Wizard_Control_Number;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_HEIGHT)
	private Double Location_Height;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_WIDTH)
	private Double Location_Width;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_DEPTH)
	private Double Location_Depth;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_WEIGHT_CAPACITY)
	private Double Location_Weight_Capacity;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_HEIGHT_CAPACITY)
	private Double Location_Height_Capacity;
	@Column(name=GLOBALCONSTANT.DataBase_Location_NUMBER_PALLETS_FIT)
	private Long Number_Pallets_Fit;
	@Column(name=GLOBALCONSTANT.DataBase_Location_NUMBER_FLOOR_LOCATIONS)
	private Long Number_Floor_Locations;
	@Column(name=GLOBALCONSTANT.DataBase_Location_ALLOCATION_ALLOWABLE)
	private String Allocation_Allowable;
	@Column(name=GLOBALCONSTANT.DataBase_Location_MULTIPLE_ITEMS)
	private String Multiple_Items;
	@Column(name=GLOBALCONSTANT.DataBase_Location_SLOTTING)
	private String Slotting;
	@Column(name=GLOBALCONSTANT.DataBase_Location_STORAGE_TYPE)
	private String Storage_Type;
	@Column(name=GLOBALCONSTANT.DataBase_Location_CHECK_DIGIT)
	private String Check_Digit;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_LABEL_TYPE)
	private String Location_Label_Type;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION_REPRINT)
	private String Location_Reprint;
	@Column(name=GLOBALCONSTANT.DataBase_Location_CYCLE_COUNT_POINTS)
	private String Cycle_Count_Points;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LAST_CYCLE_COUNT_DATE)
	private String Last_Cycle_Count_Date;
	@Column(name=GLOBALCONSTANT.DataBase_Location_INVENTORY_PICK_SEQUENCE)
	private String Inventory_Pick_Sequence;
	@Column(name=GLOBALCONSTANT.DataBase_Location_ALTERNATE_LOCATION)
	private String Alternate_Location;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LAST_ACTIVITY_DATE)
	private Date Last_Activity_Date;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LAST_ACTIVITY_TEAM_MEMBER)
	private String Last_Activity_Team_Member;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LAST_ACTIVITY_TASK)
	private String Last_Activity_Task;
	@Column(name=GLOBALCONSTANT.DataBase_Location_FREE_LOCATION_WHEN_ZERO)
	private String Free_Location_When_Zero;
	@Column(name=GLOBALCONSTANT.DataBase_Location_FREE_PRIME_DAYS)
	private Long Free_Prime_Days;
	
	
	
	public String getLocation_Label_Type() {
		return Location_Label_Type;
	}

	public void setLocation_Label_Type(String location_Label_Type) {
		Location_Label_Type = location_Label_Type;
	}

	public String getLocation_Reprint() {
		return Location_Reprint;
	}

	public void setLocation_Reprint(String location_Reprint) {
		Location_Reprint = location_Reprint;
	}

	public String getCycle_Count_Points() {
		return Cycle_Count_Points;
	}

	public void setCycle_Count_Points(String cycle_Count_Points) {
		Cycle_Count_Points = cycle_Count_Points;
	}

	public String getLast_Cycle_Count_Date() {
		return Last_Cycle_Count_Date;
	}

	public void setLast_Cycle_Count_Date(String last_Cycle_Count_Date) {
		Last_Cycle_Count_Date = last_Cycle_Count_Date;
	}

	public String getInventory_Pick_Sequence() {
		return Inventory_Pick_Sequence;
	}

	public void setInventory_Pick_Sequence(String inventory_Pick_Sequence) {
		Inventory_Pick_Sequence = inventory_Pick_Sequence;
	}

	public String getFree_Location_When_Zero() {
		return Free_Location_When_Zero;
	}

	public void setFree_Location_When_Zero(String free_Location_When_Zero) {
		Free_Location_When_Zero = free_Location_When_Zero;
	}

	public Long getFree_Prime_Days() {
		return Free_Prime_Days;
	}

	public void setFree_Prime_Days(Long free_Prime_Days) {
		Free_Prime_Days = free_Prime_Days;
	}



	
	public String getDescription20() {
		return Description20;
	}

	public void setDescription20(String description20) {
		Description20 = description20;
	}

	public String getDescription50() {
		return Description50;
	}

	public void setDescription50(String description50) {
		Description50 = description50;
	}

	public String getWork_Group_Zone() {
		return Work_Group_Zone;
	}

	public void setWork_Group_Zone(String work_Group_Zone) {
		Work_Group_Zone = work_Group_Zone;
	}

	public String getWork_Zone() {
		return Work_Zone;
	}

	public void setWork_Zone(String work_Zone) {
		Work_Zone = work_Zone;
	}

	public String getLocation_Type() {
		return Location_Type;
	}

	public void setLocation_Type(String location_Type) {
		Location_Type = location_Type;
	}

	public String getLocation_Profile() {
		return Location_Profile;
	}

	public void setLocation_Profile(String location_Profile) {
		Location_Profile = location_Profile;
	}

	public String getWizard_ID() {
		return Wizard_ID;
	}

	public void setWizard_ID(String wizard_ID) {
		Wizard_ID = wizard_ID;
	}

	public Long getWizard_Control_Number() {
		return Wizard_Control_Number;
	}

	public void setWizard_Control_Number(Long wizard_Control_Number) {
		Wizard_Control_Number = wizard_Control_Number;
	}

	public Double getLocation_Height() {
		return Location_Height;
	}

	public void setLocation_Height(Double location_Height) {
		Location_Height = location_Height;
	}

	public Double getLocation_Width() {
		return Location_Width;
	}

	public void setLocation_Width(Double location_Width) {
		Location_Width = location_Width;
	}

	public Double getLocation_Depth() {
		return Location_Depth;
	}

	public void setLocation_Depth(Double location_Depth) {
		Location_Depth = location_Depth;
	}

	public Double getLocation_Weight_Capacity() {
		return Location_Weight_Capacity;
	}

	public void setLocation_Weight_Capacity(Double location_Weight_Capacity) {
		Location_Weight_Capacity = location_Weight_Capacity;
	}

	public Double getLocation_Height_Capacity() {
		return Location_Height_Capacity;
	}

	public void setLocation_Height_Capacity(Double location_Height_Capacity) {
		Location_Height_Capacity = location_Height_Capacity;
	}

	public Long getNumber_Pallets_Fit() {
		return Number_Pallets_Fit;
	}

	public void setNumber_Pallets_Fit(Long number_Pallets_Fit) {
		Number_Pallets_Fit = number_Pallets_Fit;
	}

	public Long getNumber_Floor_Locations() {
		return Number_Floor_Locations;
	}

	public void setNumber_Floor_Locations(Long number_Floor_Locations) {
		Number_Floor_Locations = number_Floor_Locations;
	}

	public String getAllocation_Allowable() {
		return Allocation_Allowable;
	}

	public void setAllocation_Allowable(String allocation_Allowable) {
		Allocation_Allowable = allocation_Allowable;
	}

	public String getMultiple_Items() {
		return Multiple_Items;
	}

	public void setMultiple_Items(String multiple_Items) {
		Multiple_Items = multiple_Items;
	}

	public String getSlotting() {
		return Slotting;
	}

	public void setSlotting(String slotting) {
		Slotting = slotting;
	}

	public String getStorage_Type() {
		return Storage_Type;
	}

	public void setStorage_Type(String storage_Type) {
		Storage_Type = storage_Type;
	}

	public String getCheck_Digit() {
		return Check_Digit;
	}

	public void setCheck_Digit(String check_Digit) {
		Check_Digit = check_Digit;
	}

	/*public String getLocation_Label_Type() {
		return Location_Label_Type;
	}

	public void setLocation_Label_Type(String location_Label_Type) {
		Location_Label_Type = location_Label_Type;
	}

	public String getLocation_Reprint() {
		return Location_Reprint;
	}

	public void setLocation_Reprint(String location_Reprint) {
		Location_Reprint = location_Reprint;
	}

	public String getCycle_Count_Points() {
		return Cycle_Count_Points;
	}

	public void setCycle_Count_Points(String cycle_Count_Points) {
		Cycle_Count_Points = cycle_Count_Points;
	}

	public String getLast_Cycle_Count_Date() {
		return Last_Cycle_Count_Date;
	}

	public void setLast_Cycle_Count_Date(String last_Cycle_Count_Date) {
		Last_Cycle_Count_Date = last_Cycle_Count_Date;
	}

	public String getInventory_Pick_Sequence() {
		return Inventory_Pick_Sequence;
	}

	public void setInventory_Pick_Sequence(String inventory_Pick_Sequence) {
		Inventory_Pick_Sequence = inventory_Pick_Sequence;
	}
*/
	public String getAlternate_Location() {
		return Alternate_Location;
	}

	public void setAlternate_Location(String alternate_Location) {
		Alternate_Location = alternate_Location;
	}

	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}

	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}

	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}

	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}

	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}

	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}


	
	
	

}
