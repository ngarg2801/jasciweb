/*

Date Developed  Sep 18 2014
Description It is used to make setter and getter for MenuMessages Record Set And Get
Created By Aakash Bishnoi
Created Date Dec 12 2014
 */
package com.jasci.biz.AdminModule.model;



public class MENUMESSAGESBEAN {


	
	public int getIntKendoID() {
		return intKendoID;
	}
	public void setIntKendoID(int intKendoID) {
		this.intKendoID = intKendoID;
	}
	public String getTenant_ID() {
		return Tenant_ID;
	}
	public void setTenant_ID(String tenant_ID) {
		Tenant_ID = tenant_ID;
	}
	public String getCompany_ID() {
		return Company_ID;
	}
	public void setCompany_ID(String company_ID) {
		Company_ID = company_ID;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getTicket_Tape_Message() {
		return Ticket_Tape_Message;
	}
	public void setTicket_Tape_Message(String ticket_Tape_Message) {
		Ticket_Tape_Message = ticket_Tape_Message;
	}
	public String getMessage1() {
		return Message1;
	}
	public void setMessage1(String message1) {
		Message1 = message1;
	}
	
	public String getStart_Date() {
		return Start_Date;
	}
	public void setStart_Date(String start_Date) {
		Start_Date = start_Date;
	}
	public String getEnd_Date() {
		return End_Date;
	}
	public void setEnd_Date(String end_Date) {
		End_Date = end_Date;
	}
	public String getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(String last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}

	public String getMessage_ID() {
		return Message_ID;
	}
	public void setMessage_ID(String message_ID) {
		Message_ID = message_ID;
	}
	public String getCompany_Name() {
		return Company_Name;
	}
	public void setCompany_Name(String company_Name) {
		Company_Name = company_Name;
	}
	private String Message_ID;
	int intKendoID;
	private String Tenant_ID;
	private String Company_ID;
	private String Status;
	private String Type;	
	private String Ticket_Tape_Message;
	private String Message;	
	private String Message1;
	private String Start_Date;
	private String End_Date;	
	private String Last_Activity_Date;	
	private String Last_Activity_Team_Member;
	private String Company_Name;
	
	
	
	
	
}
