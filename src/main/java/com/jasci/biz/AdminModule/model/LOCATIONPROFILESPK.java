/**

Date Developed  Dec 26 2014
Developed by: Diksha Gupta
Description: LocationProfiles class has getter and setter to map with table Location_profiles only for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class LOCATIONPROFILESPK implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Tenant)
	private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_FulfillmentCenter)
	private String FulfillmentCenter;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Company)
	private String Company;
	
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Profile)
	private String Profile;
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	
	public String getProfile() {
		return Profile;
	}
	public void setProfile(String profile) {
		Profile = profile;
	}
	
	
}
