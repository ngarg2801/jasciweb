/**
 * 
 * @ file_name: WORKFLOWSTEPS
* @Developed by:Pradeep Kumar
*@Created Date:30 Mar 2015
*Purpose : Pojo Used for Mapping with table WORK_FLOW_STEPS
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Work_Flow_Steps)

public class WORKFLOWSTEPS {

	@EmbeddedId
	private WORKFLOWSTEPSPK id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Area)
	private String Area;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Type)
	private String Work_Type;

	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence_Name)
	private String Execution_Sequence_Name;
	
	public WORKFLOWSTEPSPK getId() {
		return id;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String area) {
		Area = area;
	}

	public String getExecution_Sequence_Name() {
		return Execution_Sequence_Name;
	}

	public void setExecution_Sequence_Name(String execution_Sequence_Name) {
		Execution_Sequence_Name = execution_Sequence_Name;
	}

	public void setId(WORKFLOWSTEPSPK id) {
		this.id = id;
	}

	

	public String getWork_Type() {
		return Work_Type;
	}

	public void setWork_Type(String work_Type) {
		Work_Type = work_Type;
	}
	
	
}
