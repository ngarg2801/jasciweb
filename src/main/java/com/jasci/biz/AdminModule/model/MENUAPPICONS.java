/**
 *Description :pojo class of MENUAPPICON in which getter and setter methods to map database table
 *@author Rahul Kumar
 *@Date Dec 12, 2014
 *
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

/**
 * @author Rahul Kumar
 *
 */
@Entity
@Table(name=GLOBALCONSTANT.TableName_MenuAppIcons)
@NamedNativeQueries({
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.MenuAppIcon_GetList_NamedQuery,
	query = GLOBALCONSTANT.MenuAppIcon_GetList_Query,
        resultClass = MENUAPPICONS.class
	),
	
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.MenuAppIcon_GetListByAppIcon_NamedQuery,
	query = GLOBALCONSTANT.MenuAppIcon_GetListByAppIcon_Query,
        resultClass = MENUAPPICONS.class
	),

	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.MenuAppIcon_GetListByAppIconName_NamedQuery,
	query = GLOBALCONSTANT.MenuAppIcon_GetListByAppIconName_Query,
        resultClass = MENUAPPICONS.class
	),
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.MenuAppIcon_Delete_NamedQuery,
	query = GLOBALCONSTANT.MenuAppIcon_Delete_Query,
        resultClass = MENUAPPICONS.class
	),
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.MenuAppIcon_GetListByApplication_NamedQuery,
	query = GLOBALCONSTANT.MenuAppIcon_GetListByApplication_Query,
        resultClass = MENUAPPICONS.class
	)
	
	
})

public class MENUAPPICONS {

	
	@Id
	@Column(name=GLOBALCONSTANT.DataBase_MenuAppIcons_APPLICATION)
	private String Application;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuAppIcons_APP_ICON)
	private String AppIcon;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuAppIcons_NAME20)
	private String DescriptionShort;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuAppIcons_NAME50)
	private String DescriptionLong;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuAppIcons_APP_ICON_ADDRESS)
	private String AppIconAddress;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuAppIcons_LAST_ACTIVITY_DATE)
	private Date LastActivityDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuAppIcons_LAST_ACTIVITY_TEAM_MEMBER)
	private String LastActivityTeamMember;

	public String getApplication() {
		return Application;
	}

	public void setApplication(String application) {
		Application = application;
	}

	public String getAppIcon() {
		return AppIcon;
	}

	public void setAppIcon(String appIcon) {
		AppIcon = appIcon;
	}

	public String getDescriptionShort() {
		return DescriptionShort;
	}

	public void setDescriptionShort(String descriptionShort) {
		DescriptionShort = descriptionShort;
	}

	public String getDescriptionLong() {
		return DescriptionLong;
	}

	public void setDescriptionLong(String descriptionLong) {
		DescriptionLong = descriptionLong;
	}

	public String getAppIconAddress() {
		return AppIconAddress;
	}

	public void setAppIconAddress(String appIconAddress) {
		AppIconAddress = appIconAddress;
	}

	public Date getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	
	
	
	
}
