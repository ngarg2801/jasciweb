
/*


Created by: Shailendra Rajput
Description Bean class of SMART_TASK_SEQ_INSTRUCTIONS in which getter and setter methods 
Created On:June 1 2015
 */
package com.jasci.biz.AdminModule.model;

public class SMARTTASKSEQINSTRUCTIONSBEAN {

	
   private String Tenant_Id;
   private String Company_Id;
   private String Execution_Sequence;
   private String Execution_Sequence_Name;
   private String Execution_Sequence_Type;
   private String Execution_Name;	
   private String Action_Name;
   private String Comments;	
   private String Goto_Tag;
   private String Message;
   private String Custom_Execution;
   private String Return_Code_Value;
   private String Execution_Sequence_Type_Description;
   
public String getTenant_Id() {
	return Tenant_Id;
}
public void setTenant_Id(String tenant_Id) {
	Tenant_Id = tenant_Id;
}
public String getCompany_Id() {
	return Company_Id;
}
public void setCompany_Id(String company_Id) {
	Company_Id = company_Id;
}
public String getExecution_Sequence() {
	return Execution_Sequence;
}
public void setExecution_Sequence(String execution_Sequence) {
	Execution_Sequence = execution_Sequence;
}
public String getExecution_Sequence_Name() {
	return Execution_Sequence_Name;
}
public void setExecution_Sequence_Name(String execution_Sequence_Name) {
	Execution_Sequence_Name = execution_Sequence_Name;
}
public String getExecution_Sequence_Type() {
	return Execution_Sequence_Type;
}
public void setExecution_Sequence_Type(String execution_Sequence_Type) {
	Execution_Sequence_Type = execution_Sequence_Type;
}
public String getExecution_Name() {
	return Execution_Name;
}
public void setExecution_Name(String execution_Name) {
	Execution_Name = execution_Name;
}
public String getAction_Name() {
	return Action_Name;
}
public void setAction_Name(String action_Name) {
	Action_Name = action_Name;
}
public String getComments() {
	return Comments;
}
public void setComments(String comments) {
	Comments = comments;
}
public String getGoto_Tag() {
	return Goto_Tag;
}
public String getCustom_Execution() {
	return Custom_Execution;
}
public void setCustom_Execution(String custom_Execution) {
	Custom_Execution = custom_Execution;
}
public String getReturn_Code_Value() {
	return Return_Code_Value;
}
public void setReturn_Code_Value(String return_Code_Value) {
	Return_Code_Value = return_Code_Value;
}
public void setGoto_Tag(String goto_Tag) {
	Goto_Tag = goto_Tag;
}
public String getMessage() {
	return Message;
}
public void setMessage(String message) {
	Message = message;
}
public String getExecution_Sequence_Type_Description() {
	return Execution_Sequence_Type_Description;
}
public void setExecution_Sequence_Type_Description(
		String execution_Sequence_Type_Description) {
	Execution_Sequence_Type_Description = execution_Sequence_Type_Description;
}	
  	
}
