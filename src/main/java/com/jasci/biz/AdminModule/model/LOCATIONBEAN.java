/*

Date Developed  Nov 20 2014
Created by: Aakash Bishnoi
Description  Bean class of Location Table where we have all of the field related to coding
Created On:Dec 24 2014
 */
package com.jasci.biz.AdminModule.model;

public class LOCATIONBEAN {

	private String Tenant_ID;
	private String Fullfillment_Center_ID;
	private String Company_ID;
	private String Area;
	private String Location;
	private String Description20;	
	private String Description50;	
	private String Work_Group_Zone;	
	private String Work_Zone;	
	private String Location_Type;	
	private String Location_Profile;	
	private String Wizard_ID;	
	private String Wizard_Control_Number;	
	private String Location_Height;
	private String Location_Width;
	private String Location_Depth;	
	private String Location_Weight_Capacity;	
	private String Location_Height_Capacity;	
	private String Number_Pallets_Fit;	
	private String Number_Floor_Locations;	
	private String Allocation_Allowable;	
	private String Multiple_Items;	
	private String Slotting;	
	private String Storage_Type;
	private String Check_Digit;	
	private String Alternate_Location;	
	private String Last_Activity_Date;	
	private String Last_Activity_Team_Member;	
	private String Last_Activity_Task;
	private String Free_Location_When_Zero;	
	private String Free_Prime_Days;
	private String Area_Description;
	private String Location_Type_Description;
	private String Last_Activity_Task_Description;
	private String Fullfillment_Center_ID_Description;
	private String Notes;
	/*private String Location_Label_Type;	
	private String Location_Reprint;
	private String Cycle_Count_Points;	
	private String Last_Cycle_Count_Date;
	private String Inventory_Pick_Sequence;	
*/	
	
	
	public String getNotes() {
		return Notes;
	}

	public void setNotes(String notes) {
		Notes = notes;
	}

	public String getFullfillment_Center_ID_Description() {
		return Fullfillment_Center_ID_Description;
	}

	public void setFullfillment_Center_ID_Description(
			String fullfillment_Center_ID_Description) {
		Fullfillment_Center_ID_Description = fullfillment_Center_ID_Description;
	}

	public String getArea_Description() {
		return Area_Description;
	}

	public void setArea_Description(String area_Description) {
		Area_Description = area_Description;
	}

	public String getLocation_Type_Description() {
		return Location_Type_Description;
	}

	public void setLocation_Type_Description(String location_Type_Description) {
		Location_Type_Description = location_Type_Description;
	}

	public String getLast_Activity_Task_Description() {
		return Last_Activity_Task_Description;
	}

	public void setLast_Activity_Task_Description(
			String last_Activity_Task_Description) {
		Last_Activity_Task_Description = last_Activity_Task_Description;
	}
	
	
	public String getFree_Location_When_Zero() {
		return Free_Location_When_Zero;
	}

	public void setFree_Location_When_Zero(String free_Location_When_Zero) {
		Free_Location_When_Zero = free_Location_When_Zero;
	}

	public String getFree_Prime_Days() {
		return Free_Prime_Days;
	}

	public void setFree_Prime_Days(String free_Prime_Days) {
		Free_Prime_Days = free_Prime_Days;
	}
	
	
	public String getTenant_ID() {
		return Tenant_ID;
	}
	public void setTenant_ID(String tenant_ID) {
		Tenant_ID = tenant_ID;
	}
	public String getFullfillment_Center_ID() {
		return Fullfillment_Center_ID;
	}
	public void setFullfillment_Center_ID(String fullfillment_Center_ID) {
		Fullfillment_Center_ID = fullfillment_Center_ID;
	}
	public String getCompany_ID() {
		return Company_ID;
	}
	public void setCompany_ID(String company_ID) {
		Company_ID = company_ID;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getWork_Group_Zone() {
		return Work_Group_Zone;
	}
	public void setWork_Group_Zone(String work_Group_Zone) {
		Work_Group_Zone = work_Group_Zone;
	}
	public String getWork_Zone() {
		return Work_Zone;
	}
	public void setWork_Zone(String work_Zone) {
		Work_Zone = work_Zone;
	}
	public String getLocation_Type() {
		return Location_Type;
	}
	public void setLocation_Type(String location_Type) {
		Location_Type = location_Type;
	}
	public String getLocation_Profile() {
		return Location_Profile;
	}
	public void setLocation_Profile(String location_Profile) {
		Location_Profile = location_Profile;
	}
	public String getWizard_ID() {
		return Wizard_ID;
	}
	public void setWizard_ID(String wizard_ID) {
		Wizard_ID = wizard_ID;
	}
	public String getWizard_Control_Number() {
		return Wizard_Control_Number;
	}
	public void setWizard_Control_Number(String wizard_Control_Number) {
		Wizard_Control_Number = wizard_Control_Number;
	}
	public String getLocation_Height() {
		return Location_Height;
	}
	public void setLocation_Height(String location_Height) {
		Location_Height = location_Height;
	}
	public String getLocation_Width() {
		return Location_Width;
	}
	public void setLocation_Width(String location_Width) {
		Location_Width = location_Width;
	}
	public String getLocation_Depth() {
		return Location_Depth;
	}
	public void setLocation_Depth(String location_Depth) {
		Location_Depth = location_Depth;
	}
	public String getLocation_Weight_Capacity() {
		return Location_Weight_Capacity;
	}
	public void setLocation_Weight_Capacity(String location_Weight_Capacity) {
		Location_Weight_Capacity = location_Weight_Capacity;
	}
	public String getLocation_Height_Capacity() {
		return Location_Height_Capacity;
	}
	public void setLocation_Height_Capacity(String location_Height_Capacity) {
		Location_Height_Capacity = location_Height_Capacity;
	}
	public String getNumber_Pallets_Fit() {
		return Number_Pallets_Fit;
	}
	public void setNumber_Pallets_Fit(String number_Pallets_Fit) {
		Number_Pallets_Fit = number_Pallets_Fit;
	}
	public String getNumber_Floor_Locations() {
		return Number_Floor_Locations;
	}
	public void setNumber_Floor_Locations(String number_Floor_Locations) {
		Number_Floor_Locations = number_Floor_Locations;
	}
	public String getAllocation_Allowable() {
		return Allocation_Allowable;
	}
	public void setAllocation_Allowable(String allocation_Allowable) {
		Allocation_Allowable = allocation_Allowable;
	}
	public String getMultiple_Items() {
		return Multiple_Items;
	}
	public void setMultiple_Items(String multiple_Items) {
		Multiple_Items = multiple_Items;
	}
	public String getSlotting() {
		return Slotting;
	}
	public void setSlotting(String slotting) {
		Slotting = slotting;
	}
	public String getStorage_Type() {
		return Storage_Type;
	}
	public void setStorage_Type(String storage_Type) {
		Storage_Type = storage_Type;
	}
	public String getCheck_Digit() {
		return Check_Digit;
	}
	public void setCheck_Digit(String check_Digit) {
		Check_Digit = check_Digit;
	}
	/*public String getLocation_Label_Type() {
		return Location_Label_Type;
	}
	public void setLocation_Label_Type(String location_Label_Type) {
		Location_Label_Type = location_Label_Type;
	}
	public String getLocation_Reprint() {
		return Location_Reprint;
	}
	public void setLocation_Reprint(String location_Reprint) {
		Location_Reprint = location_Reprint;
	}
	public String getCycle_Count_Points() {
		return Cycle_Count_Points;
	}
	public void setCycle_Count_Points(String cycle_Count_Points) {
		Cycle_Count_Points = cycle_Count_Points;
	}
	public String getLast_Cycle_Count_Date() {
		return Last_Cycle_Count_Date;
	}
	public void setLast_Cycle_Count_Date(String last_Cycle_Count_Date) {
		Last_Cycle_Count_Date = last_Cycle_Count_Date;
	}
	public String getInventory_Pick_Sequence() {
		return Inventory_Pick_Sequence;
	}
	public void setInventory_Pick_Sequence(String inventory_Pick_Sequence) {
		Inventory_Pick_Sequence = inventory_Pick_Sequence;
	}
*/	public String getAlternate_Location() {
		return Alternate_Location;
	}
	public void setAlternate_Location(String alternate_Location) {
		Alternate_Location = alternate_Location;
	}
	public String getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(String last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}
	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}

	
}
