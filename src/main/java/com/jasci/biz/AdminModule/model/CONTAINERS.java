/*
Created by: Shailendra Rajput
Description Bean class of CONTAINERS in which getter and setter methods 
Created On:Oct 12 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DATABASE_TABLENAME_CONTAINERS)
public class CONTAINERS {
	
	@EmbeddedId
	private CONTAINERSPK Id;
	
	@Column(name=GLOBALCONSTANT.DATABASE_NAME20)
	private String Name20;
	@Column(name=GLOBALCONSTANT.DATABASE_NAME50)
	private String	Name50;
	@Column(name=GLOBALCONSTANT.DATABASE_HEIGHT)
	private double	Height;
	@Column(name=GLOBALCONSTANT.DATABASE_WIDTH)
	private double	Width;
	@Column(name=GLOBALCONSTANT.DATABASE_LENGTH)
	private double	Length;
	@Column(name=GLOBALCONSTANT.DATABASE_STORAGE_CUBE)
	private double	Storage_Cube;
	@Column(name=GLOBALCONSTANT.DATABASE_STORAGE_CUBE_FACTOR)
	private double	Storage_Cube_Factor;
	@Column(name=GLOBALCONSTANT.DATABASE_STORAGE_CUBE_NET)
	private double	Storage_Cube_Net;
	@Column(name=GLOBALCONSTANT.DATABASE_WEIGHT)
	private double	Weight;
	@Column(name=GLOBALCONSTANT.DATABASE_MAXIUM_WEIGHT)
	private double	Maxium_Weight;
	@Column(name=GLOBALCONSTANT.DATABASE_MAXIUM_QTY)
	private double	Maxium_QTY;
	@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_USE_CODE)
	private String	Container_Use_Code;
	@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_MATERIAL_CODE)
	private String	Container_Material_Code;
	@Column(name=GLOBALCONSTANT.DATABASE_CREATED_BY)
	private Date Created_By;
	@Column(name=GLOBALCONSTANT.DATABASE_DATE_CREATED)
	private Date Date_Created;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_DATE)
	private Date Last_Activity_Date;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_TEAM_MEMBER)
	private String	Last_Activity_Team_Member;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_TASK)
	private String	Last_Activity_Task;
	
	public CONTAINERSPK getId() {
		return Id;
	}
	public void setId(CONTAINERSPK id) {
		Id = id;
	}
	public String getName20() {
		return Name20;
	}
	public void setName20(String name20) {
		Name20 = name20;
	}
	public String getName50() {
		return Name50;
	}
	public void setName50(String name50) {
		Name50 = name50;
	}
	public double getHeight() {
		return Height;
	}
	public void setHeight(double height) {
		Height = height;
	}
	public double getWidth() {
		return Width;
	}
	public void setWidth(double width) {
		Width = width;
	}
	public double getLength() {
		return Length;
	}
	public void setLength(double length) {
		Length = length;
	}
	public double getStorage_Cube() {
		return Storage_Cube;
	}
	public void setStorage_Cube(double storage_Cube) {
		Storage_Cube = storage_Cube;
	}
	public double getStorage_Cube_Factor() {
		return Storage_Cube_Factor;
	}
	public void setStorage_Cube_Factor(double storage_Cube_Factor) {
		Storage_Cube_Factor = storage_Cube_Factor;
	}
	public double getStorage_Cube_Net() {
		return Storage_Cube_Net;
	}
	public void setStorage_Cube_Net(double storage_Cube_Net) {
		Storage_Cube_Net = storage_Cube_Net;
	}
	public double getWeight() {
		return Weight;
	}
	public void setWeight(double weight) {
		Weight = weight;
	}
	public double getMaxium_Weight() {
		return Maxium_Weight;
	}
	public void setMaxium_Weight(double maxium_Weight) {
		Maxium_Weight = maxium_Weight;
	}
	public double getMaxium_QTY() {
		return Maxium_QTY;
	}
	public void setMaxium_QTY(double maxium_QTY) {
		Maxium_QTY = maxium_QTY;
	}
	public String getContainer_Use_Code() {
		return Container_Use_Code;
	}
	public void setContainer_Use_Code(String container_Use_Code) {
		Container_Use_Code = container_Use_Code;
	}
	public String getContainer_Material_Code() {
		return Container_Material_Code;
	}
	public void setContainer_Material_Code(String container_Material_Code) {
		Container_Material_Code = container_Material_Code;
	}
	public Date getCreated_By() {
		return Created_By;
	}
	public void setCreated_By(Date created_By) {
		Created_By = created_By;
	}
	public Date getDate_Created() {
		return Date_Created;
	}
	public void setDate_Created(Date date_Created) {
		Date_Created = date_Created;
	}
	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}
	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}
	
	

}
