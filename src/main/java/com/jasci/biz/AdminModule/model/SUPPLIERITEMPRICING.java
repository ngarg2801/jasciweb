/**
 * 
 * @ file_name: SUPPLIERITEMPRICING
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table SUPPLIER_ITEM_PRICING
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Supplier_Item_Pricing)

public class SUPPLIERITEMPRICING {

	@EmbeddedId
	private SUPPLIERITEMPRICINGPK Id;
	
	@Column (name=GLOBALCONSTANT.DataBase_Supplier_Item)
	private String SUPPLIER_ITEM;

	@Column (name=GLOBALCONSTANT.DataBase_From_Date)
	private Date FROM_DATE;

	@Column (name=GLOBALCONSTANT.DataBase_To_Date)
	private Date TO_DATE;

	@Column (name=GLOBALCONSTANT.DataBase_Break1_Price)
	private float BREAK1_PRICE;

	@Column (name=GLOBALCONSTANT.DataBase_Break1_Qty)
	private float BREAK1_QTY;

	@Column (name=GLOBALCONSTANT.DataBase_Break2_Price)
	private float BREAK2_PRICE;

	@Column (name=GLOBALCONSTANT.DataBase_Break2_Qty)
	private float BREAK2_QTY;

	@Column (name=GLOBALCONSTANT.DataBase_Break3_Price)
	private float BREAK3_PRICE;

	@Column (name=GLOBALCONSTANT.DataBase_Break3_Qty)
	private float BREAK3_QTY;

	@Column (name=GLOBALCONSTANT.DataBase_Break4_Price)
	private float BREAK4_PRICE;

	@Column (name=GLOBALCONSTANT.DataBase_Break4_Qty)
	private float BREAK4_QTY;

	@Column (name=GLOBALCONSTANT.DataBase_Break5_Price)
	private float BREAK5_PRICE;

	@Column (name=GLOBALCONSTANT.DataBase_Break5_Qty)
	private float BREAK5_QTY;

	@Column (name=GLOBALCONSTANT.DataBase_Crating_Cost)
	private float CRATING_COST;

	@Column (name=GLOBALCONSTANT.DataBase_Status)
	private char STATUS;

	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;

	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	public SUPPLIERITEMPRICINGPK getId() {
		return Id;
	}

	public void setId(SUPPLIERITEMPRICINGPK id) {
		Id = id;
	}

	public String getSUPPLIER_ITEM() {
		return SUPPLIER_ITEM;
	}

	public void setSUPPLIER_ITEM(String sUPPLIER_ITEM) {
		SUPPLIER_ITEM = sUPPLIER_ITEM;
	}

	public Date getFROM_DATE() {
		return FROM_DATE;
	}

	public void setFROM_DATE(Date fROM_DATE) {
		FROM_DATE = fROM_DATE;
	}

	public Date getTO_DATE() {
		return TO_DATE;
	}

	public void setTO_DATE(Date tO_DATE) {
		TO_DATE = tO_DATE;
	}

	public float getBREAK1_PRICE() {
		return BREAK1_PRICE;
	}

	public void setBREAK1_PRICE(float bREAK1_PRICE) {
		BREAK1_PRICE = bREAK1_PRICE;
	}

	public float getBREAK1_QTY() {
		return BREAK1_QTY;
	}

	public void setBREAK1_QTY(float bREAK1_QTY) {
		BREAK1_QTY = bREAK1_QTY;
	}

	public float getBREAK2_PRICE() {
		return BREAK2_PRICE;
	}

	public void setBREAK2_PRICE(float bREAK2_PRICE) {
		BREAK2_PRICE = bREAK2_PRICE;
	}

	public float getBREAK2_QTY() {
		return BREAK2_QTY;
	}

	public void setBREAK2_QTY(float bREAK2_QTY) {
		BREAK2_QTY = bREAK2_QTY;
	}

	public float getBREAK3_PRICE() {
		return BREAK3_PRICE;
	}

	public void setBREAK3_PRICE(float bREAK3_PRICE) {
		BREAK3_PRICE = bREAK3_PRICE;
	}

	public float getBREAK3_QTY() {
		return BREAK3_QTY;
	}

	public void setBREAK3_QTY(float bREAK3_QTY) {
		BREAK3_QTY = bREAK3_QTY;
	}

	public float getBREAK4_PRICE() {
		return BREAK4_PRICE;
	}

	public void setBREAK4_PRICE(float bREAK4_PRICE) {
		BREAK4_PRICE = bREAK4_PRICE;
	}

	public float getBREAK4_QTY() {
		return BREAK4_QTY;
	}

	public void setBREAK4_QTY(float bREAK4_QTY) {
		BREAK4_QTY = bREAK4_QTY;
	}

	public float getBREAK5_PRICE() {
		return BREAK5_PRICE;
	}

	public void setBREAK5_PRICE(float bREAK5_PRICE) {
		BREAK5_PRICE = bREAK5_PRICE;
	}

	public float getBREAK5_QTY() {
		return BREAK5_QTY;
	}

	public void setBREAK5_QTY(float bREAK5_QTY) {
		BREAK5_QTY = bREAK5_QTY;
	}

	public float getCRATING_COST() {
		return CRATING_COST;
	}

	public void setCRATING_COST(float cRATING_COST) {
		CRATING_COST = cRATING_COST;
	}

	public char getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(char sTATUS) {
		STATUS = sTATUS;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	
	
}
