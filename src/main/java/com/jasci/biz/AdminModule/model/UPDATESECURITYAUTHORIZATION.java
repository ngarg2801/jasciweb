/*

Date Developed  Oct 15 2014
Description   pojo class of Tenants in which getter and setter methods
Created By Deepak Sharma
 */
package com.jasci.biz.AdminModule.model;

public class UPDATESECURITYAUTHORIZATION {
	
	private String UserId;
	private String Password;
	private String Tenant;
	private String Company;	
	private String PasswordDate;
	private String SecurityQuestion;
	private String SecurityQuestionAnswer;
	private Integer numberOfLastAttempts;
	
	
	public String getSecurityQuestionAnswer() {
		return SecurityQuestionAnswer;
	}

	public void setSecurityQuestionAnswer(String securityQuestionAnswer) {
		SecurityQuestionAnswer = securityQuestionAnswer;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}

	public String getPasswordDate() {
		return PasswordDate;
	}

	public void setPasswordDate(String passwordDate) {
		PasswordDate = passwordDate;
	}

	public String getSecurityQuestion() {
		return SecurityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion) {
		SecurityQuestion = securityQuestion;
	}

	public Integer getNumberOfLastAttempts() {
		return numberOfLastAttempts;
	}

	public void setNumberOfLastAttempts(Integer numberOfLastAttempts) {
		this.numberOfLastAttempts = numberOfLastAttempts;
	}
	
	
	

	
}
