/**
 * 
 * @ file_name: PURCHASEORDERPRODUCTS
 * @Developed by:Pradeep kumar
 *@Created Date:Apr 1 2015
 *Purpose : Pojo Used for Mapping with table PURCHASE_ORDER_PRODUCTS
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Purchase_Order_Products)

public class PURCHASEORDERPRODUCTS {

	@EmbeddedId
	PURCHASEORDERPRODUCTSPK Id;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Product)
	 private String PRODUCT;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quality)
	 private String QUALITY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quantity)
	 private double QUANTITY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Unit_Of_Measure_Code)
	 private String UNIT_OF_MEASURE_CODE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quantity_In_Transit)
	 private String QUANTITY_IN_TRANSIT;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quantity_Received)
	 private double QUANTITY_RECEIVED;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quantity_Damaged)
	private double QUANTITY_DAMAGED;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quantity_Qc_Issue)
	 private double QUANTITY_QC_ISSUE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Price)
	 private double PRICE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Discount_Percent)
	 private double DISCOUNT_PERCENT;
	
	@Column(name=GLOBALCONSTANT.DataBase_Crating_Cost)
	 private double CRATING_COST;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quote_Reference_Number)
	 private String QUOTE_REFERENCE_NUMBER;
	
	@Column(name=GLOBALCONSTANT.DataBase_Delivered_By)
	 private String DELIVERED_BY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Expected_Delivery_Date)
	 private String EXPECTED_DELIVERY_DATE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Date_Received)
	 private Date DATE_RECEIVED;
	
	@Column(name=GLOBALCONSTANT.DataBase_Received_By)
	 private char RECEIVED_BY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Date_Created)
	 private Date DATE_CREATED;
	
	@Column(name=GLOBALCONSTANT.DataBase_Created_By)
	 private String CREATED_BY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Status)
	 private char STATUS;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	 private String LAST_ACTIVITY_DATE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	 private String LAST_ACTIVITY_TEAM_MEMBER;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Task)
	 private String LAST_ACTIVITY_TASK;

	public PURCHASEORDERPRODUCTSPK getId() {
		return Id;
	}

	public void setId(PURCHASEORDERPRODUCTSPK id) {
		Id = id;
	}

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}

	public String getQUALITY() {
		return QUALITY;
	}

	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}

	public double getQUANTITY() {
		return QUANTITY;
	}

	public void setQUANTITY(double qUANTITY) {
		QUANTITY = qUANTITY;
	}

	public String getUNIT_OF_MEASURE_CODE() {
		return UNIT_OF_MEASURE_CODE;
	}

	public void setUNIT_OF_MEASURE_CODE(String uNIT_OF_MEASURE_CODE) {
		UNIT_OF_MEASURE_CODE = uNIT_OF_MEASURE_CODE;
	}

	public String getQUANTITY_IN_TRANSIT() {
		return QUANTITY_IN_TRANSIT;
	}

	public void setQUANTITY_IN_TRANSIT(String qUANTITY_IN_TRANSIT) {
		QUANTITY_IN_TRANSIT = qUANTITY_IN_TRANSIT;
	}

	public double getQUANTITY_RECEIVED() {
		return QUANTITY_RECEIVED;
	}

	public void setQUANTITY_RECEIVED(double qUANTITY_RECEIVED) {
		QUANTITY_RECEIVED = qUANTITY_RECEIVED;
	}

	public double getQUANTITY_DAMAGED() {
		return QUANTITY_DAMAGED;
	}

	public void setQUANTITY_DAMAGED(double qUANTITY_DAMAGED) {
		QUANTITY_DAMAGED = qUANTITY_DAMAGED;
	}

	public double getQUANTITY_QC_ISSUE() {
		return QUANTITY_QC_ISSUE;
	}

	public void setQUANTITY_QC_ISSUE(double qUANTITY_QC_ISSUE) {
		QUANTITY_QC_ISSUE = qUANTITY_QC_ISSUE;
	}

	public double getPRICE() {
		return PRICE;
	}

	public void setPRICE(double pRICE) {
		PRICE = pRICE;
	}

	public double getDISCOUNT_PERCENT() {
		return DISCOUNT_PERCENT;
	}

	public void setDISCOUNT_PERCENT(double dISCOUNT_PERCENT) {
		DISCOUNT_PERCENT = dISCOUNT_PERCENT;
	}

	public double getCRATING_COST() {
		return CRATING_COST;
	}

	public void setCRATING_COST(double cRATING_COST) {
		CRATING_COST = cRATING_COST;
	}

	public String getQUOTE_REFERENCE_NUMBER() {
		return QUOTE_REFERENCE_NUMBER;
	}

	public void setQUOTE_REFERENCE_NUMBER(String qUOTE_REFERENCE_NUMBER) {
		QUOTE_REFERENCE_NUMBER = qUOTE_REFERENCE_NUMBER;
	}

	public String getDELIVERED_BY() {
		return DELIVERED_BY;
	}

	public void setDELIVERED_BY(String dELIVERED_BY) {
		DELIVERED_BY = dELIVERED_BY;
	}

	public String getEXPECTED_DELIVERY_DATE() {
		return EXPECTED_DELIVERY_DATE;
	}

	public void setEXPECTED_DELIVERY_DATE(String eXPECTED_DELIVERY_DATE) {
		EXPECTED_DELIVERY_DATE = eXPECTED_DELIVERY_DATE;
	}

	public Date getDATE_RECEIVED() {
		return DATE_RECEIVED;
	}

	public void setDATE_RECEIVED(Date dATE_RECEIVED) {
		DATE_RECEIVED = dATE_RECEIVED;
	}

	public char getRECEIVED_BY() {
		return RECEIVED_BY;
	}

	public void setRECEIVED_BY(char rECEIVED_BY) {
		RECEIVED_BY = rECEIVED_BY;
	}

	public Date getDATE_CREATED() {
		return DATE_CREATED;
	}

	public void setDATE_CREATED(Date dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}

	public String getCREATED_BY() {
		return CREATED_BY;
	}

	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}

	public char getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(char sTATUS) {
		STATUS = sTATUS;
	}

	public String getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(String lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getLAST_ACTIVITY_TASK() {
		return LAST_ACTIVITY_TASK;
	}

	public void setLAST_ACTIVITY_TASK(String lAST_ACTIVITY_TASK) {
		LAST_ACTIVITY_TASK = lAST_ACTIVITY_TASK;
	}
	
	
}
