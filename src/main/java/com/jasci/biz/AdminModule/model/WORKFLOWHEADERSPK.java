/**
 * 
 * @ file_name: WORKFLOWHEADERSPK
* @Developed by:Pradeep Kumar
*@Created Date:31 Mar 2015
*Purpose : Pojo Used for Mapping with table WORK_FLOW_HEADERS for primary key only
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class WORKFLOWHEADERSPK implements Serializable{

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key;
	
	@Column(name=GLOBALCONSTANT.DataBase_Fulfillment_Center_Id)
	private String FULFILLMENT_CENTER_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Flow_Id)
	private String WORK_FLOW_ID;

	public COMPANIESPK getForeign_Key() {
		return Foreign_Key;
	}

	public void setForeign_Key(COMPANIESPK foreign_Key) {
		Foreign_Key = foreign_Key;
	}

	public String getFULFILLMENT_CENTER_ID() {
		return FULFILLMENT_CENTER_ID;
	}

	public void setFULFILLMENT_CENTER_ID(String fULFILLMENT_CENTER_ID) {
		FULFILLMENT_CENTER_ID = fULFILLMENT_CENTER_ID;
	}

	public String getWORK_FLOW_ID() {
		return WORK_FLOW_ID;
	}

	public void setWORK_FLOW_ID(String wORK_FLOW_ID) {
		WORK_FLOW_ID = wORK_FLOW_ID;
	}

	
}
