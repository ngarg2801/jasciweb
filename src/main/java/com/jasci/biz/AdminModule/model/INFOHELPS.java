/*

Date Developed  Oct 16 2014
Description   pojo class of Infohelps in which getter and setter methods
Created By Aakash Bishnoi
 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_Infohelps)


@NamedNativeQueries({
	//Select All Data for Display All Button
	@NamedNativeQuery(			
	name = GLOBALCONSTANT.InfoHelps_QueryNameGetAllList,
	query = GLOBALCONSTANT.InfoHelps_QueryGetAllList,
        resultClass = INFOHELPS.class
	),
	
	@NamedNativeQuery(	
			//For fetch data from infohelps
			name = GLOBALCONSTANT.InfoHelps_QueryNameFetchInfoHelp,
			query = GLOBALCONSTANT.InfoHelps_QueryFetchInfoHelp,
		    resultClass = INFOHELPS.class
			),
	@NamedNativeQuery(	
			//For Search According to parameter passed
			name = GLOBALCONSTANT.InfoHelps_QueryNameSearchLookup,
			query = GLOBALCONSTANT.InfoHelps_QuerySearchLookup,
		        resultClass = INFOHELPS.class
			),
	@NamedNativeQuery(	
		//For delete data from infohelps
		name = GLOBALCONSTANT.InfoHelps_QueryNameDeleteInfoHelp,
		query = GLOBALCONSTANT.InfoHelps_QueryDeleteInfoHelp,
	     resultClass = INFOHELPS.class
		),
			
	/*@NamedNativeQuery(
			//For InfoHelpType drop down
			name = GLOBALCONSTANT.InfoHelps_QueryNameSelectInfoHelpType,
			query = GLOBALCONSTANT.InfoHelps_QuerySelectInfoHelpType,
			//resultSetMapping = GLOBALCONSTANT.InfoHelps_ResultSetSelectInfoHelpType
			 resultClass = GENERALCODES.class
	),
	@NamedNativeQuery(
			//For InfoHelpType drop down
			name = GLOBALCONSTANT.InfoHelps_QueryNameLanguage,
			query = GLOBALCONSTANT.InfoHelps_QueryLanguage,
			//resultSetMapping = GLOBALCONSTANT.InfoHelps_ResultSetLanguage
			 resultClass = GENERALCODES.class
	),*/
	@NamedNativeQuery(
			//For getting list of generalcode Table
			name = GLOBALCONSTANT.InfoHelps_QueryNameGeneralCodeList,
			query = GLOBALCONSTANT.InfoHelps_QueryGeneralCodeList,
			//resultSetMapping = GLOBALCONSTANT.InfoHelps_ResultSetLanguage
			 resultClass = GENERALCODES.class
	)
})
/*@SqlResultSetMappings({
	//Provide column name for get particular column into table
	 @SqlResultSetMapping(name = GLOBALCONSTANT.InfoHelps_ResultSetSelectInfoHelpType, columns = @ColumnResult(name = GLOBALCONSTANT.InfoHelps_ColumnResultInfohelps)),
	 @SqlResultSetMapping(name = GLOBALCONSTANT.InfoHelps_ResultSetLanguage, columns = @ColumnResult(name = GLOBALCONSTANT.InfoHelps_ColumnGeneralCodeID)),
})

*/


public class INFOHELPS 
{
	/*@Id
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_InfoHelp)
	private String InfoHelp;

	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Language)
	private String Language;
	*/
	@EmbeddedId
	private INFOHELPSPK Id;
	
	public INFOHELPSPK getId() {
		return Id;
	}

	public void setId(INFOHELPSPK id) {
		Id = id;
	}
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_InfoHelpType)
	private String InfoHelpType;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Description20)
	private String Description20;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Description50)
	private String Description50;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Execution)
	private String Execution;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_LastActivityDate)
	private Date LastActivityDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_LastActivityTeamMember)
	private String LastActivityTeamMember;

	
	
	/*public String getInfoHelp() {
		return InfoHelp;
	}

	public void setInfoHelp(String infoHelp) {
		InfoHelp = infoHelp;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}
*/
	public String getInfoHelpType() {
		return InfoHelpType;
	}

	public void setInfoHelpType(String infoHelpType) {
		InfoHelpType = infoHelpType;
	}

	public String getDescription20() {
		return Description20;
	}

	public void setDescription20(String description20) {
		Description20 = description20;
	}

	public String getDescription50() {
		return Description50;
	}

	public void setDescription50(String description50) {
		Description50 = description50;
	}

	public String getExecution() {
		return Execution;
	}

	public void setExecution(String execution) {
		Execution = execution;
	}

	public Date getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	
	
	
}
