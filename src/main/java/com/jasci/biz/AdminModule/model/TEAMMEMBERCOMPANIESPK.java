/*
Created by: Rahul Kumar
Description  pojo class of Team Member primary key in which getter and setter methods for primary keys
Created Date Oct 30 2014
 */

package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class TEAMMEMBERCOMPANIESPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersCompanies_Tenant)
	private String	Tenant;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersCompanies_TeamMember)
	private String	TeamMember ;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersCompanies_Company)
	private String	Company;

	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getTeamMember() {
		return TeamMember;
	}

	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}
	
	
	
	
}
