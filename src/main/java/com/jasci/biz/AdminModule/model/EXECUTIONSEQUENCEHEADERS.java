/**
 * 
 * @ file_name: EXECUTIONSEQUENCEHEADERS
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table EXECUTION_SEQUENCE_HEADERS
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Executions_Sequence_Headers)

public class EXECUTIONSEQUENCEHEADERS {

	@EmbeddedId
	private EXECUTIONSEQUENCEHEADERSPK Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Description20 , length=20)
	private String DESCRIPTION20;
	
	@Column(name=GLOBALCONSTANT.DataBase_Description50,length=50)
	private String DESCRIPTION50;
	
	@Column(name=GLOBALCONSTANT.DataBase_Menu_Name)
	private String MENU_NAME;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Device)
	private String EXECUTION_DEVICE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Type)
	private String EXECUTION_TYPE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;

	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	public EXECUTIONSEQUENCEHEADERSPK getId() {
		return Id;
	}

	public void setId(EXECUTIONSEQUENCEHEADERSPK id) {
		Id = id;
	}

	public String getDESCRIPTION20() {
		return DESCRIPTION20;
	}

	public void setDESCRIPTION20(String dESCRIPTION20) {
		DESCRIPTION20 = dESCRIPTION20;
	}

	public String getDESCRIPTION50() {
		return DESCRIPTION50;
	}

	public void setDESCRIPTION50(String dESCRIPTION50) {
		DESCRIPTION50 = dESCRIPTION50;
	}

	public String getMENU_NAME() {
		return MENU_NAME;
	}

	public void setMENU_NAME(String mENU_NAME) {
		MENU_NAME = mENU_NAME;
	}

	public String getEXECUTION_DEVICE() {
		return EXECUTION_DEVICE;
	}

	public void setEXECUTION_DEVICE(String eXECUTION_DEVICE) {
		EXECUTION_DEVICE = eXECUTION_DEVICE;
	}

	public String getEXECUTION_TYPE() {
		return EXECUTION_TYPE;
	}

	public void setEXECUTION_TYPE(String eXECUTION_TYPE) {
		EXECUTION_TYPE = eXECUTION_TYPE;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	
	
}
