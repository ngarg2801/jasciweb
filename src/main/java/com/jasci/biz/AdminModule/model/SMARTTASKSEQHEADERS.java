/**
 * 
 * @ file_name: SMART_TASK_SEQ_HEADERS
* @Developed by:Shailendra Rajput
*@Created Date:Mar 30 2015
*Purpose : Pojo Used for Mapping with table Smart Task Sequence header
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Smart_Task_Seq_Headers)

public class SMARTTASKSEQHEADERS {
	
	@EmbeddedId
	private SMARTTASKSEQHEADERSPK Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Application_Id)
	private String APPLICATION_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence_Group)
	private String EXECUTION_SEQUENCE_GROUP;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Description20)
	private String DESCRIPTION20;
	
	@Column(name=GLOBALCONSTANT.DataBase_Description50)
	private String DESCRIPTION50;
	
	@Column(name=GLOBALCONSTANT.DataBase_Menu_Option)
	private String MENU_OPTION;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Device)
	private String EXECUTION_DEVICE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Type)
	private String EXECUTION_TYPE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	@Column(name=GLOBALCONSTANT.DataBase_Task)
	private String TASK;
	
	public SMARTTASKSEQHEADERSPK getId() {
		return Id;
	}

	public void setId(SMARTTASKSEQHEADERSPK id) {
		Id = id;
	}

	public String getDESCRIPTION20() {
		return DESCRIPTION20;
	}

	public void setDESCRIPTION20(String dESCRIPTION20) {
		DESCRIPTION20 = dESCRIPTION20;
	}

	public String getDESCRIPTION50() {
		return DESCRIPTION50;
	}

	public void setDESCRIPTION50(String dESCRIPTION50) {
		DESCRIPTION50 = dESCRIPTION50;
	}

	
	public String getMENU_OPTION() {
		return MENU_OPTION;
	}

	public void setMENU_OPTION(String mENU_OPTION) {
		MENU_OPTION = mENU_OPTION;
	}

	public String getEXECUTION_DEVICE() {
		return EXECUTION_DEVICE;
	}

	public void setEXECUTION_DEVICE(String eXECUTION_DEVICE) {
		EXECUTION_DEVICE = eXECUTION_DEVICE;
	}

	public String getEXECUTION_TYPE() {
		return EXECUTION_TYPE;
	}

	public void setEXECUTION_TYPE(String eXECUTION_TYPE) {
		EXECUTION_TYPE = eXECUTION_TYPE;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	
	public String getTASK() {
		return TASK;
	}

	public void setTASK(String tASK) {
		TASK = tASK;
	}

	public String getAPPLICATION_ID() {
		return APPLICATION_ID;
	}

	public void setAPPLICATION_ID(String aPPLICATION_ID) {
		APPLICATION_ID = aPPLICATION_ID;
	}

	public String getEXECUTION_SEQUENCE_GROUP() {
		return EXECUTION_SEQUENCE_GROUP;
	}

	public void setEXECUTION_SEQUENCE_GROUP(String eXECUTION_SEQUENCE_GROUP) {
		EXECUTION_SEQUENCE_GROUP = eXECUTION_SEQUENCE_GROUP;
	}

	
	
}
