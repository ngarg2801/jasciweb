/*

Date Developed  Nov 14 2014
Description It is used to make composite primary key for TeammemberMessages.
Created By Diksha Gupta

 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class TEAMMEMBERMESSAGESPK implements Serializable
{
    
	
	private static final long serialVersionUID = 1L;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_Tenant)
	 private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_Company)
	 private String Company;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_TeamMember)
	 private String TeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_MessageId)
	 private String MesageId;
	
	public String getTenant() 
	{

		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getTeamMember() {
		return TeamMember;
	}
	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}
	public String getMesageNumber() {
		return MesageId;
	}
	public void setMesageNumber(String mesageNumber) {
		MesageId = mesageNumber;
	}
	
	
	
}
