/*

Date Developed  Jan 5 2015
Description  Pojo getter and setter method  of Location Wizard table
Created By:Pradeep Kumar
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity 
@Table(name=GLOBALCONSTANT.TableName_WIZARD_LOCATIONS)
@NamedNativeQueries({
@NamedNativeQuery(
			
			name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchWizardList,
			query = GLOBALCONSTANT.Location_Wizard_QueryFetchWizardList,
		        resultClass = LOCATIONWIZARD.class
			),
			
			
	@NamedNativeQuery(
			
	name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchDataForUpdate,
	query = GLOBALCONSTANT.Location_Wizard_QueryFetchDataForUpdate,
        resultClass = LOCATIONWIZARD.class
	),
	@NamedNativeQuery(
									
									name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchDataForGrid,
									query = GLOBALCONSTANT.Location_Wizard_QueryFetchDataForGrid,
								        resultClass = LOCATIONWIZARD.class
									),
									@NamedNativeQuery(
											
											name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchDataForCopy,
											query = GLOBALCONSTANT.Location_Wizard_QueryFetchDataForCopy,
										        resultClass = LOCATIONWIZARD.class
											),
										@NamedNativeQuery(
			
													name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchMaxWizardControlNumber,
													query = GLOBALCONSTANT.Location_Wizard_QueryFetchMaxWizardControlNumber,
													resultClass = LOCATIONWIZARD.class
													)
													
	
	
})

public class LOCATIONWIZARD {
	
	@EmbeddedId
	private LOCATIONWIZARDPK Id;

	public LOCATIONWIZARDPK getId() {
		return Id;
	}

	public void setId(LOCATIONWIZARDPK id) {
		Id = id;
	}


	
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_DESCRIPTION20)
	private String DESCRIPTION20;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_DESCRIPTION50)
	private String DESCRIPTION50;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_WORK_GROUP_ZONE)
	private String WORK_GROUP_ZONE;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_AREA)
	private String AREA;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_WORK_ZONE)
	private String WORK_ZONE;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_LOCATION_TYPE)
	private String LOCATION_TYPE;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_LOCATION_PROFILE)
	private String LOCATION_PROFILE;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_DATE_CREATED)
	private Date DATE_CREATED;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_LAST_ACTIVITY_DATE)
	private Date LAST_ACTIVITY_DATE;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_LAST_ACTIVITY_TEAM_MEMBER)
	private String LAST_ACTIVITY_TEAM_MEMBER;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_WIZARD_CREATED_BY_TEAM_MEMBER)
	private String CREATED_BY_TEAM_MEMBER;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_Status)
	private String STATUS;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_ROW_NUMBER_CHARACTERS)
	private String ROW_NUMBER_CHARACTERS;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_ROW_CHARACTER_SET)
	private String ROW_CHARACTER_SET;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_ROW_STARTING)
	private String ROW_STARTING;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_ROW_NUMBER_OF)
	private String ROW_NUMBER_OF;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_AISLE_NUMBER_CHARACTERS)
	private String AISLE_NUMBER_CHARACTERS;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_AISLE_CHARACTER_SET)
	private String AISLE_CHARACTER_SET;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_AISLE_STARTING)
	private String AISLE_STARTING;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_AISLE_NUMBER_OF)
	private String AISLE_NUMBER_OF;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_BAY_NUMBER_CHARACTERS)
	private String BAY_NUMBER_CHARACTERS;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_BAY_CHARACTER_SET)
	private String BAY_CHARACTER_SET;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_BAY_STARTING)
	private String BAY_STARTING;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_BAY_NUMBER_OF)
	private String BAY_NUMBER_OF;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_LEVEL_NUMBER_CHARACTERS)
	private String LEVEL_NUMBER_CHARACTERS;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_LEVEL_CHARACTER_SET)
	private String LEVEL_CHARACTER_SET;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_LEVEL_STARTING)
	private String LEVEL_STARTING;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_LEVEL_NUMBER_OF)
	private String LEVEL_NUMBER_OF;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SLOT_NUMBER_CHARACTERS)
	private String SLOT_NUMBER_CHARACTERS;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SLOT_CHARACTER_SET)
	private String SLOT_CHARACTER_SET;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SLOT_STARTING)
	private String SLOT_STARTING;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SLOT_NUMBER_OF)
	private String SLOT_NUMBER_OF;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_ROW_AISLE)
	private String SEPARATOR_ROW_AISLE;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_AISLE_BAY)
	private String SEPARATOR_AISLE_BAY;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_BAY_LEVEL)
	private String SEPARATOR_BAY_LEVEL;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_LEVEL_SLOT)
	private String SEPARATOR_LEVEL_SLOT;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_ROW_AISLE_PRINT)
	private String SEPARATOR_ROW_AISLE_PRINT;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_AISLE_BAY_PRINT)
	private String SEPARATOR_AISLE_BAY_PRINT;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_BAY_LEVEL_PRINT)
	private String SEPARATOR_BAY_LEVEL_PRINT;
	
	@Column(name = GLOBALCONSTANT.Database_Location_Wizard_SEPARATOR_LEVEL_SLOT_PRINT)
	private String SEPARATOR_LEVEL_SLOT_PRINT;
	
	

	//setter getter
	
	public String getSTATUS()
	{
		return STATUS;
	}
	
	public void setSTATUS(String sTATUS)
	{
		STATUS=sTATUS;
	}
	public String getDESCRIPTION20() {
		return DESCRIPTION20;
	}

	public void setDESCRIPTION20(String dESCRIPTION20) {
		DESCRIPTION20 = dESCRIPTION20;
	}

	public String getDESCRIPTION50() {
		return DESCRIPTION50;
	}

	public void setDESCRIPTION50(String dESCRIPTION50) {
		DESCRIPTION50 = dESCRIPTION50;
	}

	public String getWORK_GROUP_ZONE() {
		return WORK_GROUP_ZONE;
	}

	public void setWORK_GROUP_ZONE(String wORK_GROUP_ZONE) {
		WORK_GROUP_ZONE = wORK_GROUP_ZONE;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getWORK_ZONE() {
		return WORK_ZONE;
	}

	public void setWORK_ZONE(String wORK_ZONE) {
		WORK_ZONE = wORK_ZONE;
	}

	public String getLOCATION_TYPE() {
		return LOCATION_TYPE;
	}

	public void setLOCATION_TYPE(String lOCATION_TYPE) {
		LOCATION_TYPE = lOCATION_TYPE;
	}

	public String getLOCATION_PROFILE() {
		return LOCATION_PROFILE;
	}

	public void setLOCATION_PROFILE(String lOCATION_PROFILE) {
		LOCATION_PROFILE = lOCATION_PROFILE;
	}

	public Date getDATE_CREATED() {
		return DATE_CREATED;
	}

	public void setDATE_CREATED(Date dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getCREATED_BY_TEAM_MEMBER() {
		return CREATED_BY_TEAM_MEMBER;
	}

	public void setCREATED_BY_TEAM_MEMBER(String cREATED_BY_TEAM_MEMBER) {
		CREATED_BY_TEAM_MEMBER = cREATED_BY_TEAM_MEMBER;
	}

	public String getROW_NUMBER_CHARACTERS() {
		return ROW_NUMBER_CHARACTERS;
	}

	public void setROW_NUMBER_CHARACTERS(String rOW_NUMBER_CHARACTERS) {
		ROW_NUMBER_CHARACTERS = rOW_NUMBER_CHARACTERS;
	}

	public String getROW_CHARACTER_SET() {
		return ROW_CHARACTER_SET;
	}

	public void setROW_CHARACTER_SET(String rOW_CHARACTER_SET) {
		ROW_CHARACTER_SET = rOW_CHARACTER_SET;
	}

	public String getROW_STARTING() {
		return ROW_STARTING;
	}

	public void setROW_STARTING(String rOW_STARTING) {
		ROW_STARTING = rOW_STARTING;
	}

	public String getROW_NUMBER_OF() {
		return ROW_NUMBER_OF;
	}

	public void setROW_NUMBER_OF(String rOW_NUMBER_OF) {
		ROW_NUMBER_OF = rOW_NUMBER_OF;
	}

	public String getAISLE_NUMBER_CHARACTERS() {
		return AISLE_NUMBER_CHARACTERS;
	}

	public void setAISLE_NUMBER_CHARACTERS(String aISLE_NUMBER_CHARACTERS) {
		AISLE_NUMBER_CHARACTERS = aISLE_NUMBER_CHARACTERS;
	}

	public String getAISLE_CHARACTER_SET() {
		return AISLE_CHARACTER_SET;
	}

	public void setAISLE_CHARACTER_SET(String aISLE_CHARACTER_SET) {
		AISLE_CHARACTER_SET = aISLE_CHARACTER_SET;
	}

	public String getAISLE_STARTING() {
		return AISLE_STARTING;
	}

	public void setAISLE_STARTING(String aISLE_STARTING) {
		AISLE_STARTING = aISLE_STARTING;
	}

	public String getAISLE_NUMBER_OF() {
		return AISLE_NUMBER_OF;
	}

	public void setAISLE_NUMBER_OF(String aISLE_NUMBER_OF) {
		AISLE_NUMBER_OF = aISLE_NUMBER_OF;
	}

	public String getBAY_NUMBER_CHARACTERS() {
		return BAY_NUMBER_CHARACTERS;
	}

	public void setBAY_NUMBER_CHARACTERS(String bAY_NUMBER_CHARACTERS) {
		BAY_NUMBER_CHARACTERS = bAY_NUMBER_CHARACTERS;
	}

	public String getBAY_CHARACTER_SET() {
		return BAY_CHARACTER_SET;
	}

	public void setBAY_CHARACTER_SET(String bAY_CHARACTER_SET) {
		BAY_CHARACTER_SET = bAY_CHARACTER_SET;
	}

	public String getBAY_STARTING() {
		return BAY_STARTING;
	}

	public void setBAY_STARTING(String bAY_STARTING) {
		BAY_STARTING = bAY_STARTING;
	}

	public String getBAY_NUMBER_OF() {
		return BAY_NUMBER_OF;
	}

	public void setBAY_NUMBER_OF(String bAY_NUMBER_OF) {
		BAY_NUMBER_OF = bAY_NUMBER_OF;
	}

	public String getLEVEL_NUMBER_CHARACTERS() {
		return LEVEL_NUMBER_CHARACTERS;
	}

	public void setLEVEL_NUMBER_CHARACTERS(String lEVEL_NUMBER_CHARACTERS) {
		LEVEL_NUMBER_CHARACTERS = lEVEL_NUMBER_CHARACTERS;
	}

	public String getLEVEL_CHARACTER_SET() {
		return LEVEL_CHARACTER_SET;
	}

	public void setLEVEL_CHARACTER_SET(String lEVEL_CHARACTER_SET) {
		LEVEL_CHARACTER_SET = lEVEL_CHARACTER_SET;
	}

	public String getLEVEL_STARTING() {
		return LEVEL_STARTING;
	}

	public void setLEVEL_STARTING(String lEVEL_STARTING) {
		LEVEL_STARTING = lEVEL_STARTING;
	}

	public String getLEVEL_NUMBER_OF() {
		return LEVEL_NUMBER_OF;
	}

	public void setLEVEL_NUMBER_OF(String lEVEL_NUMBER_OF) {
		LEVEL_NUMBER_OF = lEVEL_NUMBER_OF;
	}

	public String getSLOT_NUMBER_CHARACTERS() {
		return SLOT_NUMBER_CHARACTERS;
	}

	public void setSLOT_NUMBER_CHARACTERS(String sLOT_NUMBER_CHARACTERS) {
		SLOT_NUMBER_CHARACTERS = sLOT_NUMBER_CHARACTERS;
	}

	public String getSLOT_CHARACTER_SET() {
		return SLOT_CHARACTER_SET;
	}

	public void setSLOT_CHARACTER_SET(String sLOT_CHARACTER_SET) {
		SLOT_CHARACTER_SET = sLOT_CHARACTER_SET;
	}

	public String getSLOT_STARTING() {
		return SLOT_STARTING;
	}

	public void setSLOT_STARTING(String sLOT_STARTING) {
		SLOT_STARTING = sLOT_STARTING;
	}

	public String getSLOT_NUMBER_OF() {
		return SLOT_NUMBER_OF;
	}

	public void setSLOT_NUMBER_OF(String sLOT_NUMBER_OF) {
		SLOT_NUMBER_OF = sLOT_NUMBER_OF;
	}

	public String getSEPARATOR_ROW_AISLE() {
		return SEPARATOR_ROW_AISLE;
	}

	public void setSEPARATOR_ROW_AISLE(String sEPARATOR_ROW_AISLE) {
		SEPARATOR_ROW_AISLE = sEPARATOR_ROW_AISLE;
	}

	public String getSEPARATOR_AISLE_BAY() {
		return SEPARATOR_AISLE_BAY;
	}

	public void setSEPARATOR_AISLE_BAY(String sEPARATOR_AISLE_BAY) {
		SEPARATOR_AISLE_BAY = sEPARATOR_AISLE_BAY;
	}

	public String getSEPARATOR_BAY_LEVEL() {
		return SEPARATOR_BAY_LEVEL;
	}

	public void setSEPARATOR_BAY_LEVEL(String sEPARATOR_BAY_LEVEL) {
		SEPARATOR_BAY_LEVEL = sEPARATOR_BAY_LEVEL;
	}

	public String getSEPARATOR_LEVEL_SLOT() {
		return SEPARATOR_LEVEL_SLOT;
	}

	public void setSEPARATOR_LEVEL_SLOT(String sEPARATOR_LEVEL_SLOT) {
		SEPARATOR_LEVEL_SLOT = sEPARATOR_LEVEL_SLOT;
	}

	public String getSEPARATOR_ROW_AISLE_PRINT() {
		return SEPARATOR_ROW_AISLE_PRINT;
	}

	public void setSEPARATOR_ROW_AISLE_PRINT(String sEPARATOR_ROW_AISLE_PRINT) {
		SEPARATOR_ROW_AISLE_PRINT = sEPARATOR_ROW_AISLE_PRINT;
	}

	public String getSEPARATOR_AISLE_BAY_PRINT() {
		return SEPARATOR_AISLE_BAY_PRINT;
	}

	public void setSEPARATOR_AISLE_BAY_PRINT(String sEPARATOR_AISLE_BAY_PRINT) {
		SEPARATOR_AISLE_BAY_PRINT = sEPARATOR_AISLE_BAY_PRINT;
	}

	public String getSEPARATOR_BAY_LEVEL_PRINT() {
		return SEPARATOR_BAY_LEVEL_PRINT;
	}

	public void setSEPARATOR_BAY_LEVEL_PRINT(String sEPARATOR_BAY_LEVEL_PRINT) {
		SEPARATOR_BAY_LEVEL_PRINT = sEPARATOR_BAY_LEVEL_PRINT;
	}

	public String getSEPARATOR_LEVEL_SLOT_PRINT() {
		return SEPARATOR_LEVEL_SLOT_PRINT;
	}

	public void setSEPARATOR_LEVEL_SLOT_PRINT(String sEPARATOR_LEVEL_SLOT_PRINT) {
		SEPARATOR_LEVEL_SLOT_PRINT = sEPARATOR_LEVEL_SLOT_PRINT;
	}


}
