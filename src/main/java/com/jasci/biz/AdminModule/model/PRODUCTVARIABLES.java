
/*

Date Developed  Nov 20 2014
Created by: Aakash Bishnoi
Description   pojo class of PRODUCT_VARIABLES in which getter and setter methods 
Created On:Mar 31 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_ProductVariables)
public class PRODUCTVARIABLES {

	@EmbeddedId
	private PRODUCTVARIABLESPK Id;

	@Column(name=GLOBALCONSTANT.DataBase_Unit_Of_Measure_Code)
	private String Unit_Of_Measure_Code;

	@Column(name=GLOBALCONSTANT.DataBase_Bar_Code)
	private String Bar_Code;

	@Column(name=GLOBALCONSTANT.DataBase_Bar_Code_Type)
	private String Bar_Code_Type;

	@Column(name=GLOBALCONSTANT.DataBase_Rfid)
	private String Rfid;

	@Column(name=GLOBALCONSTANT.DataBase_Height)
	private double Height;
	
	@Column(name=GLOBALCONSTANT.DataBase_Width)
	private double Width;

	@Column(name=GLOBALCONSTANT.DataBase_Length)
	private double Length;

	@Column(name=GLOBALCONSTANT.DataBase_Weight)
	private double Weight;

	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date Last_Activity_Date;

	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String Last_Activity_Team_Member;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Task)
	private String Last_Activity_Task;

	public PRODUCTVARIABLESPK getId() {
		return Id;
	}

	public void setId(PRODUCTVARIABLESPK id) {
		Id = id;
	}

	public String getUnit_Of_Measure_Code() {
		return Unit_Of_Measure_Code;
	}

	public void setUnit_Of_Measure_Code(String unit_Of_Measure_Code) {
		Unit_Of_Measure_Code = unit_Of_Measure_Code;
	}

	public String getBar_Code() {
		return Bar_Code;
	}

	public void setBar_Code(String bar_Code) {
		Bar_Code = bar_Code;
	}

	public String getBar_Code_Type() {
		return Bar_Code_Type;
	}

	public void setBar_Code_Type(String bar_Code_Type) {
		Bar_Code_Type = bar_Code_Type;
	}

	public String getRfid() {
		return Rfid;
	}

	public void setRfid(String rfid) {
		Rfid = rfid;
	}

	public double getHeight() {
		return Height;
	}

	public void setHeight(double height) {
		Height = height;
	}

	public double getWidth() {
		return Width;
	}

	public void setWidth(double width) {
		Width = width;
	}

	public double getLength() {
		return Length;
	}

	public void setLength(double length) {
		Length = length;
	}

	public double getWeight() {
		return Weight;
	}

	public void setWeight(double weight) {
		Weight = weight;
	}

	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}

	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}

	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}

	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}

	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}

	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}
	
	
}
