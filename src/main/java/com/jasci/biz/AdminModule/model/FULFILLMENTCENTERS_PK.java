/*

Date Developed  Oct 15 2014
Description  Pojo class of fullfillment center
Created By Deepak Sharma
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class FULFILLMENTCENTERS_PK implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public String getTenant() {
		return Tenant;
	}


	public void setTenant(String tenant) {
		Tenant = tenant;
	}


	public String getFulfillmentCenter() {
		return FulfillmentCenter;
	}


	public void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}


	@Column(name=GLOBALCONSTANT.DataBase_FulfillmentCenters_Tenant)
	private String Tenant;
	
   
	@Column(name=GLOBALCONSTANT.DataBase_FulfillmentCenters_FulfillmentCenter)
	private String FulfillmentCenter;
	
}
