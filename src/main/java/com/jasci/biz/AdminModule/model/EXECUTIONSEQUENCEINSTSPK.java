/**
 * 
 * @ file_name: EXECUTIONSEQUENCEINSTSPK
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table EXECUTION_SEQUENCE_INSTS for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class EXECUTIONSEQUENCEINSTSPK implements Serializable {

	/*@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key_Company;
	*/
	/*@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey_Tenant)
	private COMPANIESPK Foreign_Key_Tenant;*/
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence_Name)
	private String EXECUTION_SEQUENCE_NAME;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence)
	private long EXECUTION_SEQUENCE;

	/*public COMPANIESPK getForeign_Key_Company() {
		return Foreign_Key_Company;
	}

	public void setForeign_Key_Company(COMPANIESPK foreign_Key_Company) {
		Foreign_Key_Company = foreign_Key_Company;
	}
*/
	public String getEXECUTION_SEQUENCE_NAME() {
		return EXECUTION_SEQUENCE_NAME;
	}

	public void setEXECUTION_SEQUENCE_NAME(String eXECUTION_SEQUENCE_NAME) {
		EXECUTION_SEQUENCE_NAME = eXECUTION_SEQUENCE_NAME;
	}

	public long getEXECUTION_SEQUENCE() {
		return EXECUTION_SEQUENCE;
	}

	public void setEXECUTION_SEQUENCE(long eXECUTION_SEQUENCE) {
		EXECUTION_SEQUENCE = eXECUTION_SEQUENCE;
	}
	
	
}
