/*


Created by: Aakash Bishnoi
Description   pojo class of WORK_HISTORY in which getter and setter methods 
Created On:Mar 31 2015
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Work_History)
public class WORKHISTORY {

	@EmbeddedId
	private WORKHISTORYPK Id;
    
	@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
  	private String Company_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Fulfillment_Center_Id)
  	private String Fulfillment_Center_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Lpn)
  	private String Lpn;
	
	@Column(name=GLOBALCONSTANT.DataBase_Asn)
  	private String Asn;
	
	@Column(name=GLOBALCONSTANT.DataBase_Zone)
  	private String Zone;
	
	@Column(name=GLOBALCONSTANT.DataBase_Divert)
  	private String Divert;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Type)
  	private String Work_Type;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_History_Code)
  	private String Work_History_Code;
	

	
}
