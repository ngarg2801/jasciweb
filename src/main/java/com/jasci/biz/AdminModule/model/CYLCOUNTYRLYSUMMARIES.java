/**
 * 
 * @ file_name: CYLCOUNTYRLYSUMMARIES
 * @Developed by:Pradeep kumar
 *@Created Date:1 April 2015
 *Purpose : Pojo Used for Mapping with table CYL_COUNT_YRLY_SUMMARIES
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Cyl_Count_Yrly_Summaries)

public class CYLCOUNTYRLYSUMMARIES {

	@EmbeddedId CYLCOUNTYRLYSUMMARIESPK Id;
	
	@Column (name=GLOBALCONSTANT.DataBase_Number_Cycle_Counts)
	private long NUMBER_CYCLE_COUNTS;
	
	@Column (name=GLOBALCONSTANT.DataBase_Positive_Qty)
	private double POSITIVE_QTY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Negative_Qty)
	private double NEGATIVE_QTY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Cycle_Count_Date)
	private Date LAST_CYCLE_COUNT_DATE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Cycle_Count_Points)
	private long CYCLE_COUNT_POINTS;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Time)
	private String LAST_ACTIVITY_TIME;

	public CYLCOUNTYRLYSUMMARIESPK getId() {
		return Id;
	}

	public void setId(CYLCOUNTYRLYSUMMARIESPK id) {
		Id = id;
	}

	public long getNUMBER_CYCLE_COUNTS() {
		return NUMBER_CYCLE_COUNTS;
	}

	public void setNUMBER_CYCLE_COUNTS(long nUMBER_CYCLE_COUNTS) {
		NUMBER_CYCLE_COUNTS = nUMBER_CYCLE_COUNTS;
	}

	public double getPOSITIVE_QTY() {
		return POSITIVE_QTY;
	}

	public void setPOSITIVE_QTY(double pOSITIVE_QTY) {
		POSITIVE_QTY = pOSITIVE_QTY;
	}

	public double getNEGATIVE_QTY() {
		return NEGATIVE_QTY;
	}

	public void setNEGATIVE_QTY(double nEGATIVE_QTY) {
		NEGATIVE_QTY = nEGATIVE_QTY;
	}

	public Date getLAST_CYCLE_COUNT_DATE() {
		return LAST_CYCLE_COUNT_DATE;
	}

	public void setLAST_CYCLE_COUNT_DATE(Date lAST_CYCLE_COUNT_DATE) {
		LAST_CYCLE_COUNT_DATE = lAST_CYCLE_COUNT_DATE;
	}

	public long getCYCLE_COUNT_POINTS() {
		return CYCLE_COUNT_POINTS;
	}

	public void setCYCLE_COUNT_POINTS(long cYCLE_COUNT_POINTS) {
		CYCLE_COUNT_POINTS = cYCLE_COUNT_POINTS;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TIME() {
		return LAST_ACTIVITY_TIME;
	}

	public void setLAST_ACTIVITY_TIME(String lAST_ACTIVITY_TIME) {
		LAST_ACTIVITY_TIME = lAST_ACTIVITY_TIME;
	}
	
	
	
}
