/**
 * 
 * @ file_name: PURCHASEORDERHEADERS
 * @Developed by:Pradeep kumar
 *@Created Date:Apr 1 2015
 *Purpose : Pojo Used for Mapping with table PURCHASE_ORDER_HEADERS
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name = GLOBALCONSTANT.DataBase_TableName_Purchase_Order_Headers)
public class PURCHASEORDERHEADERS {

	@EmbeddedId
	PURCHASEORDERHEADERSPK Id;

	@Column(name = GLOBALCONSTANT.DataBase_Type)
	private String TYPE;
	
	@Column(name = GLOBALCONSTANT.DataBase_Buying_Terms)
	private String BUYING_TERMS;
	
	@Column(name = GLOBALCONSTANT.DataBase_Shipping_Charge)
	private String SHIPPING_CHARGE;
	
	@Column(name = GLOBALCONSTANT.DataBase_Crating_Cost)
	private String CRATING_COST;
	
	@Column(name = GLOBALCONSTANT.DataBase_Special_Instructions)
	private String SPECIAL_INSTRUCTIONS;
	
	@Column(name = GLOBALCONSTANT.DataBase_Fob)
	private String FOB;
	
	@Column(name = GLOBALCONSTANT.DataBase_Ship_Via)
	private String SHIP_VIA;
	
	@Column(name = GLOBALCONSTANT.DataBase_Quote_Reference_Number)
	private String QUOTE_REFERENCE_NUMBER;
	
	@Column(name = GLOBALCONSTANT.DataBase_Delivered_By)
	private String DELIVERED_BY;
	
	@Column(name = GLOBALCONSTANT.DataBase_Expected_Delivery_Date)
	private String EXPECTED_DELIVERY_DATE;
	
	@Column(name = GLOBALCONSTANT.DataBase_Date_Received)
	private String DATE_RECEIVED;
	
	@Column(name = GLOBALCONSTANT.DataBase_Received_By)
	private String RECEIVED_BY;
	
	@Column(name = GLOBALCONSTANT.DataBase_Buyer)
	private String BUYER;
	
	@Column(name = GLOBALCONSTANT.DataBase_Number_Of_Shipments)
	private String NUMBER_OF_SHIPMENTS;
	
	@Column(name = GLOBALCONSTANT.DataBase_Created_By)
	private String CREATED_BY;
	
	@Column(name = GLOBALCONSTANT.DataBase_Date_Created)
	private String DATE_CREATED;
	
	@Column(name = GLOBALCONSTANT.DataBase_Status)
	private String STATUS;
	
	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private String LAST_ACTIVITY_DATE;
	
	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;
	
	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Task)
	private String LAST_ACTIVITY_TASK;

	public PURCHASEORDERHEADERSPK getId() {
		return Id;
	}

	public void setId(PURCHASEORDERHEADERSPK id) {
		Id = id;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getBUYING_TERMS() {
		return BUYING_TERMS;
	}

	public void setBUYING_TERMS(String bUYING_TERMS) {
		BUYING_TERMS = bUYING_TERMS;
	}

	public String getSHIPPING_CHARGE() {
		return SHIPPING_CHARGE;
	}

	public void setSHIPPING_CHARGE(String sHIPPING_CHARGE) {
		SHIPPING_CHARGE = sHIPPING_CHARGE;
	}

	public String getCRATING_COST() {
		return CRATING_COST;
	}

	public void setCRATING_COST(String cRATING_COST) {
		CRATING_COST = cRATING_COST;
	}

	public String getSPECIAL_INSTRUCTIONS() {
		return SPECIAL_INSTRUCTIONS;
	}

	public void setSPECIAL_INSTRUCTIONS(String sPECIAL_INSTRUCTIONS) {
		SPECIAL_INSTRUCTIONS = sPECIAL_INSTRUCTIONS;
	}

	public String getFOB() {
		return FOB;
	}

	public void setFOB(String fOB) {
		FOB = fOB;
	}

	public String getSHIP_VIA() {
		return SHIP_VIA;
	}

	public void setSHIP_VIA(String sHIP_VIA) {
		SHIP_VIA = sHIP_VIA;
	}

	public String getQUOTE_REFERENCE_NUMBER() {
		return QUOTE_REFERENCE_NUMBER;
	}

	public void setQUOTE_REFERENCE_NUMBER(String qUOTE_REFERENCE_NUMBER) {
		QUOTE_REFERENCE_NUMBER = qUOTE_REFERENCE_NUMBER;
	}

	public String getDELIVERED_BY() {
		return DELIVERED_BY;
	}

	public void setDELIVERED_BY(String dELIVERED_BY) {
		DELIVERED_BY = dELIVERED_BY;
	}

	public String getEXPECTED_DELIVERY_DATE() {
		return EXPECTED_DELIVERY_DATE;
	}

	public void setEXPECTED_DELIVERY_DATE(String eXPECTED_DELIVERY_DATE) {
		EXPECTED_DELIVERY_DATE = eXPECTED_DELIVERY_DATE;
	}

	public String getDATE_RECEIVED() {
		return DATE_RECEIVED;
	}

	public void setDATE_RECEIVED(String dATE_RECEIVED) {
		DATE_RECEIVED = dATE_RECEIVED;
	}

	public String getRECEIVED_BY() {
		return RECEIVED_BY;
	}

	public void setRECEIVED_BY(String rECEIVED_BY) {
		RECEIVED_BY = rECEIVED_BY;
	}

	public String getBUYER() {
		return BUYER;
	}

	public void setBUYER(String bUYER) {
		BUYER = bUYER;
	}

	public String getNUMBER_OF_SHIPMENTS() {
		return NUMBER_OF_SHIPMENTS;
	}

	public void setNUMBER_OF_SHIPMENTS(String nUMBER_OF_SHIPMENTS) {
		NUMBER_OF_SHIPMENTS = nUMBER_OF_SHIPMENTS;
	}

	public String getCREATED_BY() {
		return CREATED_BY;
	}

	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}

	public String getDATE_CREATED() {
		return DATE_CREATED;
	}

	public void setDATE_CREATED(String dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(String lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getLAST_ACTIVITY_TASK() {
		return LAST_ACTIVITY_TASK;
	}

	public void setLAST_ACTIVITY_TASK(String lAST_ACTIVITY_TASK) {
		LAST_ACTIVITY_TASK = lAST_ACTIVITY_TASK;
	}
	
	
}
