/**
 * 
 * @ file_name: PURORDTERMSANDCONDS
 * @Developed by:Pradeep kumar
 *@Created Date:Apr 1 2015
 *Purpose : Pojo Used for Mapping with table PUR_ORD_TERMS_AND_CONDS
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class PURORDTERMSANDCONDS {

	@Column(name = GLOBALCONSTANT.DataBase_Heading)
	private String HEADING;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line01)
	private String DESCRIPTION_LINE01;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line02)
	private String DESCRIPTION_LINE02;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line03)
	private String DESCRIPTION_LINE03;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line04)
	private String DESCRIPTION_LINE04;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line05)
	private String DESCRIPTION_LINE05;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line06)
	private String DESCRIPTION_LINE06;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line07)
	private String DESCRIPTION_LINE07;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line08)
	private String DESCRIPTION_LINE08;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line09)
	private String DESCRIPTION_LINE09;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line10)
	private String DESCRIPTION_LINE10;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line11)
	private String DESCRIPTION_LINE11;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line12)
	private String DESCRIPTION_LINE12;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line13)
	private String DESCRIPTION_LINE13;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line14)
	private String DESCRIPTION_LINE14;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line15)
	private String DESCRIPTION_LINE15;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line16)
	private String DESCRIPTION_LINE16;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line17)
	private String DESCRIPTION_LINE17;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line18)
	private String DESCRIPTION_LINE18;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line19)
	private String DESCRIPTION_LINE19;
	
	@Column(name = GLOBALCONSTANT.DataBase_Description_Line20)
	private String DESCRIPTION_LINE20;
	
	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;
	
	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Task)
	private String LAST_ACTIVITY_TASK;

	public String getHEADING() {
		return HEADING;
	}

	public void setHEADING(String hEADING) {
		HEADING = hEADING;
	}

	public String getDESCRIPTION_LINE01() {
		return DESCRIPTION_LINE01;
	}

	public void setDESCRIPTION_LINE01(String dESCRIPTION_LINE01) {
		DESCRIPTION_LINE01 = dESCRIPTION_LINE01;
	}

	public String getDESCRIPTION_LINE02() {
		return DESCRIPTION_LINE02;
	}

	public void setDESCRIPTION_LINE02(String dESCRIPTION_LINE02) {
		DESCRIPTION_LINE02 = dESCRIPTION_LINE02;
	}

	public String getDESCRIPTION_LINE03() {
		return DESCRIPTION_LINE03;
	}

	public void setDESCRIPTION_LINE03(String dESCRIPTION_LINE03) {
		DESCRIPTION_LINE03 = dESCRIPTION_LINE03;
	}

	public String getDESCRIPTION_LINE04() {
		return DESCRIPTION_LINE04;
	}

	public void setDESCRIPTION_LINE04(String dESCRIPTION_LINE04) {
		DESCRIPTION_LINE04 = dESCRIPTION_LINE04;
	}

	public String getDESCRIPTION_LINE05() {
		return DESCRIPTION_LINE05;
	}

	public void setDESCRIPTION_LINE05(String dESCRIPTION_LINE05) {
		DESCRIPTION_LINE05 = dESCRIPTION_LINE05;
	}

	public String getDESCRIPTION_LINE06() {
		return DESCRIPTION_LINE06;
	}

	public void setDESCRIPTION_LINE06(String dESCRIPTION_LINE06) {
		DESCRIPTION_LINE06 = dESCRIPTION_LINE06;
	}

	public String getDESCRIPTION_LINE07() {
		return DESCRIPTION_LINE07;
	}

	public void setDESCRIPTION_LINE07(String dESCRIPTION_LINE07) {
		DESCRIPTION_LINE07 = dESCRIPTION_LINE07;
	}

	public String getDESCRIPTION_LINE08() {
		return DESCRIPTION_LINE08;
	}

	public void setDESCRIPTION_LINE08(String dESCRIPTION_LINE08) {
		DESCRIPTION_LINE08 = dESCRIPTION_LINE08;
	}

	public String getDESCRIPTION_LINE09() {
		return DESCRIPTION_LINE09;
	}

	public void setDESCRIPTION_LINE09(String dESCRIPTION_LINE09) {
		DESCRIPTION_LINE09 = dESCRIPTION_LINE09;
	}

	public String getDESCRIPTION_LINE10() {
		return DESCRIPTION_LINE10;
	}

	public void setDESCRIPTION_LINE10(String dESCRIPTION_LINE10) {
		DESCRIPTION_LINE10 = dESCRIPTION_LINE10;
	}

	public String getDESCRIPTION_LINE11() {
		return DESCRIPTION_LINE11;
	}

	public void setDESCRIPTION_LINE11(String dESCRIPTION_LINE11) {
		DESCRIPTION_LINE11 = dESCRIPTION_LINE11;
	}

	public String getDESCRIPTION_LINE12() {
		return DESCRIPTION_LINE12;
	}

	public void setDESCRIPTION_LINE12(String dESCRIPTION_LINE12) {
		DESCRIPTION_LINE12 = dESCRIPTION_LINE12;
	}

	public String getDESCRIPTION_LINE13() {
		return DESCRIPTION_LINE13;
	}

	public void setDESCRIPTION_LINE13(String dESCRIPTION_LINE13) {
		DESCRIPTION_LINE13 = dESCRIPTION_LINE13;
	}

	public String getDESCRIPTION_LINE14() {
		return DESCRIPTION_LINE14;
	}

	public void setDESCRIPTION_LINE14(String dESCRIPTION_LINE14) {
		DESCRIPTION_LINE14 = dESCRIPTION_LINE14;
	}

	public String getDESCRIPTION_LINE15() {
		return DESCRIPTION_LINE15;
	}

	public void setDESCRIPTION_LINE15(String dESCRIPTION_LINE15) {
		DESCRIPTION_LINE15 = dESCRIPTION_LINE15;
	}

	public String getDESCRIPTION_LINE16() {
		return DESCRIPTION_LINE16;
	}

	public void setDESCRIPTION_LINE16(String dESCRIPTION_LINE16) {
		DESCRIPTION_LINE16 = dESCRIPTION_LINE16;
	}

	public String getDESCRIPTION_LINE17() {
		return DESCRIPTION_LINE17;
	}

	public void setDESCRIPTION_LINE17(String dESCRIPTION_LINE17) {
		DESCRIPTION_LINE17 = dESCRIPTION_LINE17;
	}

	public String getDESCRIPTION_LINE18() {
		return DESCRIPTION_LINE18;
	}

	public void setDESCRIPTION_LINE18(String dESCRIPTION_LINE18) {
		DESCRIPTION_LINE18 = dESCRIPTION_LINE18;
	}

	public String getDESCRIPTION_LINE19() {
		return DESCRIPTION_LINE19;
	}

	public void setDESCRIPTION_LINE19(String dESCRIPTION_LINE19) {
		DESCRIPTION_LINE19 = dESCRIPTION_LINE19;
	}

	public String getDESCRIPTION_LINE20() {
		return DESCRIPTION_LINE20;
	}

	public void setDESCRIPTION_LINE20(String dESCRIPTION_LINE20) {
		DESCRIPTION_LINE20 = dESCRIPTION_LINE20;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getLAST_ACTIVITY_TASK() {
		return LAST_ACTIVITY_TASK;
	}

	public void setLAST_ACTIVITY_TASK(String lAST_ACTIVITY_TASK) {
		LAST_ACTIVITY_TASK = lAST_ACTIVITY_TASK;
	}
	
	
}
