/*

Date Developed  Sep 18 2014
Description It is used to make setter and getter for MenuMessages Record Using Primary Key Set And Get
Created By Aakash Bishnoi
Created Date Dec 12 2014
 */

package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class MENUMESSAGESPK implements Serializable{

	private static final long serialVersionUID = 1L;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessage_Tenant_ID)
	private String Tenant_ID;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessage_Company_ID)
	private String Company_ID;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessage_Status)
	private String Status;
	@Column(name = GLOBALCONSTANT.DataBase_MenuMessage_Message_ID)
	private String Message_ID;
	
	
	
	public String getMessage_ID() {
		return Message_ID;
	}
	public void setMessage_ID(String message_ID) {
		Message_ID = message_ID;
	}
	public String getTenant_ID() {
		return Tenant_ID;
	}
	public void setTenant_ID(String tenant_ID) {
		Tenant_ID = tenant_ID;
	}
	public String getCompany_ID() {
		return Company_ID;
	}
	public void setCompany_ID(String company_ID) {
		Company_ID = company_ID;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
}
