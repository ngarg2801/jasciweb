/**
 * 
 * @ file_name: SUPPLIERSPK
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table SUPPLIERS
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name = GLOBALCONSTANT.DataBase_TableName_Supplier)
public class SUPPLIER {

	@EmbeddedId
	private SUPPLIERSPK Id;

	@Column(name = GLOBALCONSTANT.DataBase_Prefered_Supplier)
	private String SUPPIER_ACCOUNT_IDENTIFICATION;

	@Column(name = GLOBALCONSTANT.DataBase_Prefered_Supplier)
	private char PREFERED_SUPPLIER;

	@Column(name = GLOBALCONSTANT.DataBase_Supplier_Rating)
	private long SUPPLIER_RATING;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Name20)
	private String SHIP_FROM_NAME20;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Name50)
	private String SHIP_FROM_NAME50;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Address_Line1)
	private String SHIP_FROM_ADDRESS_LINE1;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Address_Line2)
	private String SHIP_FROM_ADDRESS_LINE2;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Address_Line3)
	private String SHIP_FROM_ADDRESS_LINE3;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Address_Line4)
	private String SHIP_FROM_ADDRESS_LINE4;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_City)
	private String SHIP_FROM_CITY;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_State_Code)
	private String SHIP_FROM_STATE_CODE;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Country_Code)
	private String SHIP_FROM_COUNTRY_CODE;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_From_Zip_Code)
	private String SHIP_FROM_ZIP_CODE;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Name20)
	private String BILL_TO_NAME20;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Name50)
	private String BILL_TO_NAME50;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Address_Line1)
	private String BILL_TO_ADDRESS_LINE1;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Address_Line2)
	private String BILL_TO_ADDRESS_LINE2;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Address_Line3)
	private String BILL_TO_ADDRESS_LINE3;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Address_Line4)
	private String BILL_TO_ADDRESS_LINE4;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_From_City)
	private String BILL_TO_FROM_CITY;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_State_Code)
	private String BILL_TO_STATE_CODE;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Country_Code)
	private String BILL_TO_COUNTRY_CODE;

	@Column(name = GLOBALCONSTANT.DataBase_Bill_To_Zip_Code)
	private String BILL_TO_ZIP_CODE;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Name1)
	private String CONTACT_NAME1;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Phone1)
	private String CONTACT_PHONE1;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Extension1)
	private String CONTACT_EXTENSION1;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Cell1)
	private String CONTACT_CELL1;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Fax1)
	private String CONTACT_FAX1;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Name2)
	private String CONTACT_NAME2;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Phone2)
	private String CONTACT_PHONE2;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Extension2)
	private String CONTACT_EXTENSION2;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Cell2)
	private String CONTACT_CELL2;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Fax2)
	private String CONTACT_FAX2;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Name3)
	private String CONTACT_NAME3;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Phone3)
	private String CONTACT_PHONE3;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Extension3)
	private String CONTACT_EXTENSION3;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Cell3)
	private String CONTACT_CELL3;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Fax3)
	private String CONTACT_FAX3;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Name4)
	private String CONTACT_NAME4;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Phone4)
	private String CONTACT_PHONE4;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Extension4)
	private String CONTACT_EXTENSION4;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Cell4)
	private String CONTACT_CELL4;

	@Column(name = GLOBALCONSTANT.DataBase_Contact_Fax4)
	private String CONTACT_FAX4;

	@Column(name = GLOBALCONSTANT.DataBase_Main_Fax)
	private String MAIN_FAX;

	@Column(name = GLOBALCONSTANT.DataBase_Main_Email)
	private String MAIN_EMAIL;

	@Column(name = GLOBALCONSTANT.DataBase_Account_Number)
	private String ACCOUNT_NUMBER;

	@Column(name = GLOBALCONSTANT.DataBase_Currency)
	private String CURRENCY;

	@Column(name = GLOBALCONSTANT.DataBase_Payment_Terms1)
	private String PAYMENT_TERMS1;

	@Column(name = GLOBALCONSTANT.DataBase_Buying_Terms)
	private String BUYING_TERMS;

	@Column(name = GLOBALCONSTANT.DataBase_Duties_Percent)
	private float DUTIES_PERCENT;

	@Column(name = GLOBALCONSTANT.DataBase_Tax_Percent)
	private float TAX_PERCENT;

	@Column(name = GLOBALCONSTANT.DataBase_Brokers_Fees)
	private float BROKERS_FEES;

	@Column(name = GLOBALCONSTANT.DataBase_Harbor_Fees)
	private float HARBOR_FEES;

	@Column(name = GLOBALCONSTANT.DataBase_Incentivies)
	private float INCENTIVIES;

	@Column(name = GLOBALCONSTANT.DataBase_Incentivies_Days)
	private long INCENTIVIES_DAYS;

	@Column(name = GLOBALCONSTANT.DataBase_Penalties)
	private float PENALTIES;

	@Column(name = GLOBALCONSTANT.DataBase_Penalties_Days)
	private long PENALTIES_DAYS;

	@Column(name = GLOBALCONSTANT.DataBase_Transmission_Method)
	private String TRANSMISSION_METHOD;

	@Column(name = GLOBALCONSTANT.DataBase_Special_Instructions)
	private String SPECIAL_INSTRUCTIONS;

	@Column(name = GLOBALCONSTANT.DataBase_Cancellation_Notice_Days)
	private long CANCELLATION_NOTICE_DAYS;

	@Column(name = GLOBALCONSTANT.DataBase_Cancellation_Notice_Email)
	private String CANCELLATION_NOTICE_EMAIL;

	@Column(name = GLOBALCONSTANT.DataBase_Ship_Via)
	private String SHIP_VIA;

	@Column(name = GLOBALCONSTANT.DataBase_Freight_Terms)
	private String FREIGHT_TERMS;

	@Column(name = GLOBALCONSTANT.DataBase_Payment_Terms2)
	private long PAYMENT_TERMS2;

	@Column(name = GLOBALCONSTANT.DataBase_Landing_Factor_Percent)
	private float LANDING_FACTOR_PERCENT;

	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private String LAST_ACTIVITY_DATE;

	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Task)
	private String LAST_ACTIVITY_TASK;

	public SUPPLIERSPK getId() {
		return Id;
	}

	public void setId(SUPPLIERSPK id) {
		Id = id;
	}

	public String getSUPPIER_ACCOUNT_IDENTIFICATION() {
		return SUPPIER_ACCOUNT_IDENTIFICATION;
	}

	public void setSUPPIER_ACCOUNT_IDENTIFICATION(String sUPPIER_ACCOUNT_IDENTIFICATION) {
		SUPPIER_ACCOUNT_IDENTIFICATION = sUPPIER_ACCOUNT_IDENTIFICATION;
	}

	public char getPREFERED_SUPPLIER() {
		return PREFERED_SUPPLIER;
	}

	public void setPREFERED_SUPPLIER(char pREFERED_SUPPLIER) {
		PREFERED_SUPPLIER = pREFERED_SUPPLIER;
	}

	public long getSUPPLIER_RATING() {
		return SUPPLIER_RATING;
	}

	public void setSUPPLIER_RATING(long sUPPLIER_RATING) {
		SUPPLIER_RATING = sUPPLIER_RATING;
	}

	public String getSHIP_FROM_NAME20() {
		return SHIP_FROM_NAME20;
	}

	public void setSHIP_FROM_NAME20(String sHIP_FROM_NAME20) {
		SHIP_FROM_NAME20 = sHIP_FROM_NAME20;
	}

	public String getSHIP_FROM_NAME50() {
		return SHIP_FROM_NAME50;
	}

	public void setSHIP_FROM_NAME50(String sHIP_FROM_NAME50) {
		SHIP_FROM_NAME50 = sHIP_FROM_NAME50;
	}

	public String getSHIP_FROM_ADDRESS_LINE1() {
		return SHIP_FROM_ADDRESS_LINE1;
	}

	public void setSHIP_FROM_ADDRESS_LINE1(String sHIP_FROM_ADDRESS_LINE1) {
		SHIP_FROM_ADDRESS_LINE1 = sHIP_FROM_ADDRESS_LINE1;
	}

	public String getSHIP_FROM_ADDRESS_LINE2() {
		return SHIP_FROM_ADDRESS_LINE2;
	}

	public void setSHIP_FROM_ADDRESS_LINE2(String sHIP_FROM_ADDRESS_LINE2) {
		SHIP_FROM_ADDRESS_LINE2 = sHIP_FROM_ADDRESS_LINE2;
	}

	public String getSHIP_FROM_ADDRESS_LINE3() {
		return SHIP_FROM_ADDRESS_LINE3;
	}

	public void setSHIP_FROM_ADDRESS_LINE3(String sHIP_FROM_ADDRESS_LINE3) {
		SHIP_FROM_ADDRESS_LINE3 = sHIP_FROM_ADDRESS_LINE3;
	}

	public String getSHIP_FROM_ADDRESS_LINE4() {
		return SHIP_FROM_ADDRESS_LINE4;
	}

	public void setSHIP_FROM_ADDRESS_LINE4(String sHIP_FROM_ADDRESS_LINE4) {
		SHIP_FROM_ADDRESS_LINE4 = sHIP_FROM_ADDRESS_LINE4;
	}

	public String getSHIP_FROM_CITY() {
		return SHIP_FROM_CITY;
	}

	public void setSHIP_FROM_CITY(String sHIP_FROM_CITY) {
		SHIP_FROM_CITY = sHIP_FROM_CITY;
	}

	public String getSHIP_FROM_STATE_CODE() {
		return SHIP_FROM_STATE_CODE;
	}

	public void setSHIP_FROM_STATE_CODE(String sHIP_FROM_STATE_CODE) {
		SHIP_FROM_STATE_CODE = sHIP_FROM_STATE_CODE;
	}

	public String getSHIP_FROM_COUNTRY_CODE() {
		return SHIP_FROM_COUNTRY_CODE;
	}

	public void setSHIP_FROM_COUNTRY_CODE(String sHIP_FROM_COUNTRY_CODE) {
		SHIP_FROM_COUNTRY_CODE = sHIP_FROM_COUNTRY_CODE;
	}

	public String getSHIP_FROM_ZIP_CODE() {
		return SHIP_FROM_ZIP_CODE;
	}

	public void setSHIP_FROM_ZIP_CODE(String sHIP_FROM_ZIP_CODE) {
		SHIP_FROM_ZIP_CODE = sHIP_FROM_ZIP_CODE;
	}

	public String getBILL_TO_NAME20() {
		return BILL_TO_NAME20;
	}

	public void setBILL_TO_NAME20(String bILL_TO_NAME20) {
		BILL_TO_NAME20 = bILL_TO_NAME20;
	}

	public String getBILL_TO_NAME50() {
		return BILL_TO_NAME50;
	}

	public void setBILL_TO_NAME50(String bILL_TO_NAME50) {
		BILL_TO_NAME50 = bILL_TO_NAME50;
	}

	public String getBILL_TO_ADDRESS_LINE1() {
		return BILL_TO_ADDRESS_LINE1;
	}

	public void setBILL_TO_ADDRESS_LINE1(String bILL_TO_ADDRESS_LINE1) {
		BILL_TO_ADDRESS_LINE1 = bILL_TO_ADDRESS_LINE1;
	}

	public String getBILL_TO_ADDRESS_LINE2() {
		return BILL_TO_ADDRESS_LINE2;
	}

	public void setBILL_TO_ADDRESS_LINE2(String bILL_TO_ADDRESS_LINE2) {
		BILL_TO_ADDRESS_LINE2 = bILL_TO_ADDRESS_LINE2;
	}

	public String getBILL_TO_ADDRESS_LINE3() {
		return BILL_TO_ADDRESS_LINE3;
	}

	public void setBILL_TO_ADDRESS_LINE3(String bILL_TO_ADDRESS_LINE3) {
		BILL_TO_ADDRESS_LINE3 = bILL_TO_ADDRESS_LINE3;
	}

	public String getBILL_TO_ADDRESS_LINE4() {
		return BILL_TO_ADDRESS_LINE4;
	}

	public void setBILL_TO_ADDRESS_LINE4(String bILL_TO_ADDRESS_LINE4) {
		BILL_TO_ADDRESS_LINE4 = bILL_TO_ADDRESS_LINE4;
	}

	public String getBILL_TO_FROM_CITY() {
		return BILL_TO_FROM_CITY;
	}

	public void setBILL_TO_FROM_CITY(String bILL_TO_FROM_CITY) {
		BILL_TO_FROM_CITY = bILL_TO_FROM_CITY;
	}

	public String getBILL_TO_STATE_CODE() {
		return BILL_TO_STATE_CODE;
	}

	public void setBILL_TO_STATE_CODE(String bILL_TO_STATE_CODE) {
		BILL_TO_STATE_CODE = bILL_TO_STATE_CODE;
	}

	public String getBILL_TO_COUNTRY_CODE() {
		return BILL_TO_COUNTRY_CODE;
	}

	public void setBILL_TO_COUNTRY_CODE(String bILL_TO_COUNTRY_CODE) {
		BILL_TO_COUNTRY_CODE = bILL_TO_COUNTRY_CODE;
	}

	public String getBILL_TO_ZIP_CODE() {
		return BILL_TO_ZIP_CODE;
	}

	public void setBILL_TO_ZIP_CODE(String bILL_TO_ZIP_CODE) {
		BILL_TO_ZIP_CODE = bILL_TO_ZIP_CODE;
	}

	public String getCONTACT_NAME1() {
		return CONTACT_NAME1;
	}

	public void setCONTACT_NAME1(String cONTACT_NAME1) {
		CONTACT_NAME1 = cONTACT_NAME1;
	}

	public String getCONTACT_PHONE1() {
		return CONTACT_PHONE1;
	}

	public void setCONTACT_PHONE1(String cONTACT_PHONE1) {
		CONTACT_PHONE1 = cONTACT_PHONE1;
	}

	public String getCONTACT_EXTENSION1() {
		return CONTACT_EXTENSION1;
	}

	public void setCONTACT_EXTENSION1(String cONTACT_EXTENSION1) {
		CONTACT_EXTENSION1 = cONTACT_EXTENSION1;
	}

	public String getCONTACT_CELL1() {
		return CONTACT_CELL1;
	}

	public void setCONTACT_CELL1(String cONTACT_CELL1) {
		CONTACT_CELL1 = cONTACT_CELL1;
	}

	public String getCONTACT_FAX1() {
		return CONTACT_FAX1;
	}

	public void setCONTACT_FAX1(String cONTACT_FAX1) {
		CONTACT_FAX1 = cONTACT_FAX1;
	}

	public String getCONTACT_NAME2() {
		return CONTACT_NAME2;
	}

	public void setCONTACT_NAME2(String cONTACT_NAME2) {
		CONTACT_NAME2 = cONTACT_NAME2;
	}

	public String getCONTACT_PHONE2() {
		return CONTACT_PHONE2;
	}

	public void setCONTACT_PHONE2(String cONTACT_PHONE2) {
		CONTACT_PHONE2 = cONTACT_PHONE2;
	}

	public String getCONTACT_EXTENSION2() {
		return CONTACT_EXTENSION2;
	}

	public void setCONTACT_EXTENSION2(String cONTACT_EXTENSION2) {
		CONTACT_EXTENSION2 = cONTACT_EXTENSION2;
	}

	public String getCONTACT_CELL2() {
		return CONTACT_CELL2;
	}

	public void setCONTACT_CELL2(String cONTACT_CELL2) {
		CONTACT_CELL2 = cONTACT_CELL2;
	}

	public String getCONTACT_FAX2() {
		return CONTACT_FAX2;
	}

	public void setCONTACT_FAX2(String cONTACT_FAX2) {
		CONTACT_FAX2 = cONTACT_FAX2;
	}

	public String getCONTACT_NAME3() {
		return CONTACT_NAME3;
	}

	public void setCONTACT_NAME3(String cONTACT_NAME3) {
		CONTACT_NAME3 = cONTACT_NAME3;
	}

	public String getCONTACT_PHONE3() {
		return CONTACT_PHONE3;
	}

	public void setCONTACT_PHONE3(String cONTACT_PHONE3) {
		CONTACT_PHONE3 = cONTACT_PHONE3;
	}

	public String getCONTACT_EXTENSION3() {
		return CONTACT_EXTENSION3;
	}

	public void setCONTACT_EXTENSION3(String cONTACT_EXTENSION3) {
		CONTACT_EXTENSION3 = cONTACT_EXTENSION3;
	}

	public String getCONTACT_CELL3() {
		return CONTACT_CELL3;
	}

	public void setCONTACT_CELL3(String cONTACT_CELL3) {
		CONTACT_CELL3 = cONTACT_CELL3;
	}

	public String getCONTACT_FAX3() {
		return CONTACT_FAX3;
	}

	public void setCONTACT_FAX3(String cONTACT_FAX3) {
		CONTACT_FAX3 = cONTACT_FAX3;
	}

	public String getCONTACT_NAME4() {
		return CONTACT_NAME4;
	}

	public void setCONTACT_NAME4(String cONTACT_NAME4) {
		CONTACT_NAME4 = cONTACT_NAME4;
	}

	public String getCONTACT_PHONE4() {
		return CONTACT_PHONE4;
	}

	public void setCONTACT_PHONE4(String cONTACT_PHONE4) {
		CONTACT_PHONE4 = cONTACT_PHONE4;
	}

	public String getCONTACT_EXTENSION4() {
		return CONTACT_EXTENSION4;
	}

	public void setCONTACT_EXTENSION4(String cONTACT_EXTENSION4) {
		CONTACT_EXTENSION4 = cONTACT_EXTENSION4;
	}

	public String getCONTACT_CELL4() {
		return CONTACT_CELL4;
	}

	public void setCONTACT_CELL4(String cONTACT_CELL4) {
		CONTACT_CELL4 = cONTACT_CELL4;
	}

	public String getCONTACT_FAX4() {
		return CONTACT_FAX4;
	}

	public void setCONTACT_FAX4(String cONTACT_FAX4) {
		CONTACT_FAX4 = cONTACT_FAX4;
	}

	public String getMAIN_FAX() {
		return MAIN_FAX;
	}

	public void setMAIN_FAX(String mAIN_FAX) {
		MAIN_FAX = mAIN_FAX;
	}

	public String getMAIN_EMAIL() {
		return MAIN_EMAIL;
	}

	public void setMAIN_EMAIL(String mAIN_EMAIL) {
		MAIN_EMAIL = mAIN_EMAIL;
	}

	public String getACCOUNT_NUMBER() {
		return ACCOUNT_NUMBER;
	}

	public void setACCOUNT_NUMBER(String aCCOUNT_NUMBER) {
		ACCOUNT_NUMBER = aCCOUNT_NUMBER;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getPAYMENT_TERMS1() {
		return PAYMENT_TERMS1;
	}

	public void setPAYMENT_TERMS1(String pAYMENT_TERMS1) {
		PAYMENT_TERMS1 = pAYMENT_TERMS1;
	}

	public String getBUYING_TERMS() {
		return BUYING_TERMS;
	}

	public void setBUYING_TERMS(String bUYING_TERMS) {
		BUYING_TERMS = bUYING_TERMS;
	}

	public String getTRANSMISSION_METHOD() {
		return TRANSMISSION_METHOD;
	}

	public void setTRANSMISSION_METHOD(String tRANSMISSION_METHOD) {
		TRANSMISSION_METHOD = tRANSMISSION_METHOD;
	}

	public String getSPECIAL_INSTRUCTIONS() {
		return SPECIAL_INSTRUCTIONS;
	}

	public void setSPECIAL_INSTRUCTIONS(String sPECIAL_INSTRUCTIONS) {
		SPECIAL_INSTRUCTIONS = sPECIAL_INSTRUCTIONS;
	}

	public String getCANCELLATION_NOTICE_EMAIL() {
		return CANCELLATION_NOTICE_EMAIL;
	}

	public void setCANCELLATION_NOTICE_EMAIL(String cANCELLATION_NOTICE_EMAIL) {
		CANCELLATION_NOTICE_EMAIL = cANCELLATION_NOTICE_EMAIL;
	}

	public String getSHIP_VIA() {
		return SHIP_VIA;
	}

	public void setSHIP_VIA(String sHIP_VIA) {
		SHIP_VIA = sHIP_VIA;
	}

	public String getFREIGHT_TERMS() {
		return FREIGHT_TERMS;
	}

	public void setFREIGHT_TERMS(String fREIGHT_TERMS) {
		FREIGHT_TERMS = fREIGHT_TERMS;
	}

	public String getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(String lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getLAST_ACTIVITY_TASK() {
		return LAST_ACTIVITY_TASK;
	}

	public void setLAST_ACTIVITY_TASK(String lAST_ACTIVITY_TASK) {
		LAST_ACTIVITY_TASK = lAST_ACTIVITY_TASK;
	}

	public float getDUTIES_PERCENT() {
		return DUTIES_PERCENT;
	}

	public void setDUTIES_PERCENT(float dUTIES_PERCENT) {
		DUTIES_PERCENT = dUTIES_PERCENT;
	}

	public float getTAX_PERCENT() {
		return TAX_PERCENT;
	}

	public void setTAX_PERCENT(float tAX_PERCENT) {
		TAX_PERCENT = tAX_PERCENT;
	}

	public float getBROKERS_FEES() {
		return BROKERS_FEES;
	}

	public void setBROKERS_FEES(float bROKERS_FEES) {
		BROKERS_FEES = bROKERS_FEES;
	}

	public float getHARBOR_FEES() {
		return HARBOR_FEES;
	}

	public void setHARBOR_FEES(float hARBOR_FEES) {
		HARBOR_FEES = hARBOR_FEES;
	}

	public float getINCENTIVIES() {
		return INCENTIVIES;
	}

	public void setINCENTIVIES(float iNCENTIVIES) {
		INCENTIVIES = iNCENTIVIES;
	}

	public long getINCENTIVIES_DAYS() {
		return INCENTIVIES_DAYS;
	}

	public void setINCENTIVIES_DAYS(long iNCENTIVIES_DAYS) {
		INCENTIVIES_DAYS = iNCENTIVIES_DAYS;
	}

	public float getPENALTIES() {
		return PENALTIES;
	}

	public void setPENALTIES(float pENALTIES) {
		PENALTIES = pENALTIES;
	}

	public long getPENALTIES_DAYS() {
		return PENALTIES_DAYS;
	}

	public void setPENALTIES_DAYS(long pENALTIES_DAYS) {
		PENALTIES_DAYS = pENALTIES_DAYS;
	}

	public long getCANCELLATION_NOTICE_DAYS() {
		return CANCELLATION_NOTICE_DAYS;
	}

	public void setCANCELLATION_NOTICE_DAYS(long cANCELLATION_NOTICE_DAYS) {
		CANCELLATION_NOTICE_DAYS = cANCELLATION_NOTICE_DAYS;
	}

	public long getPAYMENT_TERMS2() {
		return PAYMENT_TERMS2;
	}

	public void setPAYMENT_TERMS2(long pAYMENT_TERMS2) {
		PAYMENT_TERMS2 = pAYMENT_TERMS2;
	}

	public float getLANDING_FACTOR_PERCENT() {
		return LANDING_FACTOR_PERCENT;
	}

	public void setLANDING_FACTOR_PERCENT(float lANDING_FACTOR_PERCENT) {
		LANDING_FACTOR_PERCENT = lANDING_FACTOR_PERCENT;
	}

}
