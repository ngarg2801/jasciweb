/**
 * 
 * @ file_name: SMART_TASK_SEQ_HEADERS_PK
* @Developed by:Shailendra Rajput
*@Created Date Mar 30 2015
*Purpose : Pojo Used for Mapping with table Smart Task Sequence header
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class SMARTTASKSEQHEADERSPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Tenant_Id)
	private String TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
	private String COMPANY_ID;
	
	//@ManyToOne
	/*@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key;*/
	
	

	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence_Name)
	private String EXECUTION_SEQUENCE_NAME;

	public String getTENANT_ID() {
		return TENANT_ID;
	}

	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	
	public String getEXECUTION_SEQUENCE_NAME() {
		return EXECUTION_SEQUENCE_NAME;
	}

	public void setEXECUTION_SEQUENCE_NAME(String eXECUTION_SEQUENCE_NAME) {
		EXECUTION_SEQUENCE_NAME = eXECUTION_SEQUENCE_NAME;
	}

	/*public COMPANIESPK getForeign_Key() {
		return Foreign_Key;
	}

	public void setForeign_Key(COMPANIESPK foreign_Key) {
		Foreign_Key = foreign_Key;
	}*/
	
	
}
