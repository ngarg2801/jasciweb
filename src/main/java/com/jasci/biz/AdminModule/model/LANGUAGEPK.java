/*

Date Developed  nov 03 2014
Description  pojo class of Languages in which getter and setter methods only for primary keys
Created By Diksha Gupta
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class LANGUAGEPK implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Languages_Language)
	private String Language;
	
	@Column(name=GLOBALCONSTANT.DataBase_Languages_KeyPhrase)
	private String KeyPhrase;

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getKeyPhrase() {
		return KeyPhrase;
	}

	public void setKeyPhrase(String keyPhrase) {
		KeyPhrase = keyPhrase;
	}
	

}
