/*

Date Developed  Nov 14 2014
Description It is used to make setter and getter to map with table Temmember_messages.
Created By Diksha Gupta
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_TeamMembersMessages)

/*@NamedNativeQuery(name = GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_MaxMessageNumber_QueryName , query = GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_MaxMessageNumber , resultSetMapping = 
GLOBALCONSTANT.TeamMembersMessages_ResultMaxMessageNumber)

@SqlResultSetMapping(name =GLOBALCONSTANT.TeamMembersMessages_ResultMaxMessageNumber,
columns = { 
		
  @ColumnResult(name = "MESSAGENUMBER")
 
  })*/
@NamedNativeQueries({
@NamedNativeQuery(
		//Fetch data For GeneralCode where teammember should Y for data
name = GLOBALCONSTANT.TeamMembersMessages_QueryName_SelectTeamMember_Department,
query = GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_Department_ConditionalQuery,
    resultClass = TEAMMEMBERS.class
),
@NamedNativeQuery(
		//Fetch data For GeneralCode where teammember should Y for data
name = GLOBALCONSTANT.TeamMembersMessages_QueryName_SelectTeamMember_Shift,
query = GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_Shift_ConditionalQuery,
    resultClass = TEAMMEMBERS.class
)


})
public class TEAMMEMBERMESSAGES 
{

	@EmbeddedId
	private TEAMMEMBERMESSAGESPK TeamMemberMessagesId;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_Status)
	 private String Status;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_TicketTapeMessge)
	 private String TicketTapeMessge;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_Message)
	private String Message;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_StartDate)
	 private Date StartDate ;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_EndDate )
	 private Date EndDate ;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_LastActivityDate)
	 private Date LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersMessages_LastActivityTeamMember)
	 private String LastActivityTeamMember;
	public TEAMMEMBERMESSAGESPK getTeamMemberMessagesId() {
		return TeamMemberMessagesId;
	}
	public void setTeamMemberMessagesId(TEAMMEMBERMESSAGESPK teamMemberMessagesId) {
		TeamMemberMessagesId = teamMemberMessagesId;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getTicketTapeMessge() {
		return TicketTapeMessge;
	}
	public void setTicketTapeMessge(String ticketTapeMessge) {
		TicketTapeMessge = ticketTapeMessge;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}

	public Date getStartDate() {
		return StartDate;
	}
	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}
	public Date getEndDate() {
		return EndDate;
	}
	public void setEndDate(Date endDate) {
		EndDate = endDate;
	}
	public Date getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	

	
	
}
