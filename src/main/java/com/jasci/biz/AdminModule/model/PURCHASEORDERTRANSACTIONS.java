/**
 * 
 * @ file_name: PURCHASEORDERTRANSACTIONS
 * @Developed by:Pradeep kumar
 *@Created Date:Apr 1 2015
 *Purpose : Pojo Used for Mapping with table PURCHASE_ORDER_TRANSACTIONS
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;

import com.jasci.common.constant.GLOBALCONSTANT;

public class PURCHASEORDERTRANSACTIONS {

	@EmbeddedId
	PURCHASEORDERTRANSACTIONSPK Id;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	 private String LAST_ACTIVITY_TEAM_MEMBER;
	
	@Column( name=GLOBALCONSTANT.DataBase_Last_Activity_Task )
	private String LAST_ACTIVITY_TASK;
	
	@Column( name=GLOBALCONSTANT.DataBase_Type )
	private String TYPE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Po_Terms)
	 private String PO_TERMS;
	
	@Column (name=GLOBALCONSTANT.DataBase_Shipping_Charge )
	private String SHIPPING_CHARGE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Crating_Cost1)
	 private String CRATING_COST1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Fob)
	 private String FOB;
	
	@Column (name=GLOBALCONSTANT.DataBase_Ship_Via)
	 private String SHIP_VIA;
	
	@Column (name=GLOBALCONSTANT.DataBase_Delivered_By)
	 private String DELIVERED_BY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Expected_Delivery_Date)
	 private String EXPECTED_DELIVERY_DATE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Date_Received)
	 private String DATE_RECEIVED;
	
	@Column (name=GLOBALCONSTANT.DataBase_Received_By)
	 private String RECEIVED_BY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Line_Number )
	private String LINE_NUMBER;
	
	@Column (name=GLOBALCONSTANT.DataBase_Shipment_Number)
	 private String SHIPMENT_NUMBER;
	
	@Column (name=GLOBALCONSTANT.DataBase_Product)
	 private String PRODUCT;
	
	@Column (name=GLOBALCONSTANT.DataBase_Quality)
	 private String QUALITY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Quantity)
	 private String QUANTITY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Quantity_Task)
	 private String QUANTITY_TASK;
	
	@Column (name=GLOBALCONSTANT.DataBase_Price)
	 private String PRICE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Discount_Percent)
	 private String DISCOUNT_PERCENT;
	
	@Column (name=GLOBALCONSTANT.DataBase_Crating_Cost2)
	 private String CRATING_COST2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Quote_Reference_Number )
	private String QUOTE_REFERENCE_NUMBER;
	
	@Column (name=GLOBALCONSTANT.DataBase_Shipment_Number2)
	 private String SHIPMENT_NUMBER2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Container_Number)
	 private String CONTAINER_NUMBER;
	
	@Column (name=GLOBALCONSTANT.DataBase_Boat_Name )
	private String BOAT_NAME;
	
	@Column (name=GLOBALCONSTANT.DataBase_Carrier) 
	private String CARRIER;
	
	@Column (name=GLOBALCONSTANT.DataBase_Status)
	 private String STATUS;

	public PURCHASEORDERTRANSACTIONSPK getId() {
		return Id;
	}

	public void setId(PURCHASEORDERTRANSACTIONSPK id) {
		Id = id;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getLAST_ACTIVITY_TASK() {
		return LAST_ACTIVITY_TASK;
	}

	public void setLAST_ACTIVITY_TASK(String lAST_ACTIVITY_TASK) {
		LAST_ACTIVITY_TASK = lAST_ACTIVITY_TASK;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getPO_TERMS() {
		return PO_TERMS;
	}

	public void setPO_TERMS(String pO_TERMS) {
		PO_TERMS = pO_TERMS;
	}

	public String getSHIPPING_CHARGE() {
		return SHIPPING_CHARGE;
	}

	public void setSHIPPING_CHARGE(String sHIPPING_CHARGE) {
		SHIPPING_CHARGE = sHIPPING_CHARGE;
	}

	public String getCRATING_COST1() {
		return CRATING_COST1;
	}

	public void setCRATING_COST1(String cRATING_COST1) {
		CRATING_COST1 = cRATING_COST1;
	}

	public String getFOB() {
		return FOB;
	}

	public void setFOB(String fOB) {
		FOB = fOB;
	}

	public String getSHIP_VIA() {
		return SHIP_VIA;
	}

	public void setSHIP_VIA(String sHIP_VIA) {
		SHIP_VIA = sHIP_VIA;
	}

	public String getDELIVERED_BY() {
		return DELIVERED_BY;
	}

	public void setDELIVERED_BY(String dELIVERED_BY) {
		DELIVERED_BY = dELIVERED_BY;
	}

	public String getEXPECTED_DELIVERY_DATE() {
		return EXPECTED_DELIVERY_DATE;
	}

	public void setEXPECTED_DELIVERY_DATE(String eXPECTED_DELIVERY_DATE) {
		EXPECTED_DELIVERY_DATE = eXPECTED_DELIVERY_DATE;
	}

	public String getDATE_RECEIVED() {
		return DATE_RECEIVED;
	}

	public void setDATE_RECEIVED(String dATE_RECEIVED) {
		DATE_RECEIVED = dATE_RECEIVED;
	}

	public String getRECEIVED_BY() {
		return RECEIVED_BY;
	}

	public void setRECEIVED_BY(String rECEIVED_BY) {
		RECEIVED_BY = rECEIVED_BY;
	}

	public String getLINE_NUMBER() {
		return LINE_NUMBER;
	}

	public void setLINE_NUMBER(String lINE_NUMBER) {
		LINE_NUMBER = lINE_NUMBER;
	}

	public String getSHIPMENT_NUMBER() {
		return SHIPMENT_NUMBER;
	}

	public void setSHIPMENT_NUMBER(String sHIPMENT_NUMBER) {
		SHIPMENT_NUMBER = sHIPMENT_NUMBER;
	}

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}

	public String getQUALITY() {
		return QUALITY;
	}

	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}

	public String getQUANTITY() {
		return QUANTITY;
	}

	public void setQUANTITY(String qUANTITY) {
		QUANTITY = qUANTITY;
	}

	public String getQUANTITY_TASK() {
		return QUANTITY_TASK;
	}

	public void setQUANTITY_TASK(String qUANTITY_TASK) {
		QUANTITY_TASK = qUANTITY_TASK;
	}

	public String getPRICE() {
		return PRICE;
	}

	public void setPRICE(String pRICE) {
		PRICE = pRICE;
	}

	public String getDISCOUNT_PERCENT() {
		return DISCOUNT_PERCENT;
	}

	public void setDISCOUNT_PERCENT(String dISCOUNT_PERCENT) {
		DISCOUNT_PERCENT = dISCOUNT_PERCENT;
	}

	public String getCRATING_COST2() {
		return CRATING_COST2;
	}

	public void setCRATING_COST2(String cRATING_COST2) {
		CRATING_COST2 = cRATING_COST2;
	}

	public String getQUOTE_REFERENCE_NUMBER() {
		return QUOTE_REFERENCE_NUMBER;
	}

	public void setQUOTE_REFERENCE_NUMBER(String qUOTE_REFERENCE_NUMBER) {
		QUOTE_REFERENCE_NUMBER = qUOTE_REFERENCE_NUMBER;
	}

	public String getSHIPMENT_NUMBER2() {
		return SHIPMENT_NUMBER2;
	}

	public void setSHIPMENT_NUMBER2(String sHIPMENT_NUMBER2) {
		SHIPMENT_NUMBER2 = sHIPMENT_NUMBER2;
	}

	public String getCONTAINER_NUMBER() {
		return CONTAINER_NUMBER;
	}

	public void setCONTAINER_NUMBER(String cONTAINER_NUMBER) {
		CONTAINER_NUMBER = cONTAINER_NUMBER;
	}

	public String getBOAT_NAME() {
		return BOAT_NAME;
	}

	public void setBOAT_NAME(String bOAT_NAME) {
		BOAT_NAME = bOAT_NAME;
	}

	public String getCARRIER() {
		return CARRIER;
	}

	public void setCARRIER(String cARRIER) {
		CARRIER = cARRIER;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	
}
