/*

Date Developed  Jan 5 2015
Description  Pojo primary keys getter and setter  of Location Wizard table
Created By:Pradeep Kumar
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class LOCATIONWIZARDPK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.Database_Location_Wizard_Tenant_ID)
	private String	TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.Database_Location_Wizard_Company_ID)
	private String	COMPANY_ID ;
	
	@Column(name=GLOBALCONSTANT.Database_Location_Wizard_WIZARD_ID)
	private String	WIZARD_ID ;
	
	@Column(name=GLOBALCONSTANT.Database_Location_Wizard_WIZARD_FULFILLMENT_CENTER_ID)
	private String	FULFILLMENT_CENTER_ID;
	
	@Column(name=GLOBALCONSTANT.Database_Location_Wizard_WIZARD_CONTROL_NUMBER)
	private Long WIZARD_CONTROL_NUMBER;

	public String getTENANT_ID() {
		return TENANT_ID;
	}

	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	public String getWIZARD_ID() {
		return WIZARD_ID;
	}

	public void setWIZARD_ID(String wIZARD_ID) {
		WIZARD_ID = wIZARD_ID;
	}

	public String getFULFILLMENT_CENTER_ID() {
		return FULFILLMENT_CENTER_ID;
	}

	public void setFULFILLMENT_CENTER_ID(String fULFILLMENT_CENTER_ID) {
		FULFILLMENT_CENTER_ID = fULFILLMENT_CENTER_ID;
	}

	public Long getWIZARD_CONTROL_NUMBER() {
		return WIZARD_CONTROL_NUMBER;
	}

	public void setWIZARD_CONTROL_NUMBER(Long wIZARD_CONTROL_NUMBER) {
		WIZARD_CONTROL_NUMBER = wIZARD_CONTROL_NUMBER;
	}

	
}



