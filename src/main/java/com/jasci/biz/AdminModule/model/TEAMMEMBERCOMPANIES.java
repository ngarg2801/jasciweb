/*

Date Developed  Sep 18 2014
Description pojo class of TeamMembersCompanies in which getter and setter methods and mapping with table
Created By Diksha Gupta
Created Date Oct 30 2014
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQuery;

import com.jasci.common.constant.GLOBALCONSTANT;


//add by sarvendra tyagi
@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_Team_Member_company , query=GLOBALCONSTANT.NativeQueryName_Team_member_Companies_query, 
resultSetMapping=GLOBALCONSTANT.NativeQueryName_Team_member_Companies_ResultSetMapping)

@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_Team_member_Companies_ResultSetMapping ,
columns ={@ColumnResult(name=GLOBALCONSTANT.DataBase_Companies_Company),
		@ColumnResult(name=GLOBALCONSTANT.DataBase_Tenants_Name20),
		@ColumnResult(name=GLOBALCONSTANT.ColumnName_PurchaseOrderApproval),
		@ColumnResult(name=GLOBALCONSTANT.ColumnName_CompanyLogo),
@ColumnResult(name="TMobile"),
@ColumnResult(name="TRF"),
@ColumnResult(name="TFullDisplay")})




@Entity
@Table(name=GLOBALCONSTANT.TableName_TeamMembersCompanies)
public class TEAMMEMBERCOMPANIES 
{


	@EmbeddedId
	private TEAMMEMBERCOMPANIESPK Id;

	public TEAMMEMBERCOMPANIESPK getId() {
		return Id;
	}

	public void setId(TEAMMEMBERCOMPANIESPK id) {
		Id = id;
	}

	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersCompanies_LastActivityDate)
	private Date	LastActivityDate;

	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersCompanies_LastActivityTeamMember)
	private String	LastActivityTeamMember;

	@Column(name=GLOBALCONSTANT.DataBase_TeamMembersCompanies_CurrentStatus)
	private String	CurrentStatus;



	public Date getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	public String getCurrentStatus() {
		return CurrentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		CurrentStatus = currentStatus;
	}


}

