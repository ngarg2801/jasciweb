/*
Created by: Shailendra Rajput
Description Bean class of PUTWALL_CUBES in which getter and setter methods 
Created On:Oct 12 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DATABASE_TABLENAME_PUTWALL_CUBES)
public class PUTWALL_CUBES {

	@EmbeddedId
	private PUTWALL_CUBES Id;
	@Column(name=GLOBALCONSTANT.DATABASE_PRIORITY)
	private double Priority;
	@Column(name=GLOBALCONSTANT.DATABASE_LEVEL)
	private String Level;
	@Column(name=GLOBALCONSTANT.DATABASE_CUBE_SLOT)
	private String Cube_Slot;	
	@Column(name=GLOBALCONSTANT.DATABASE_FRONT_BACK)	
	private String Front_Back;
	@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE)	
	private String Container_Type;
	@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID)	
	private String Container_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_RELATIVE_CUBE_ID)
	private String Relative_Cube_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_ALTERNATE_CUBE_ID)	
	private String Alternate_Cube_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER)
	private double Work_Control_Number;
	@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE)
	private String Work_Type;
	@Column(name=GLOBALCONSTANT.DATABASE_LPN)
	private String LPN;
	@Column(name=GLOBALCONSTANT.DATABASE_ORDER_ID)
	private String Order_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_STORE_NUMBER)
	private String Store_Number;
	@Column(name=GLOBALCONSTANT.DATABASE_TYPE)
	private String Type;
	@Column(name=GLOBALCONSTANT.DATABASE_PUT_UNITS_REQUIRED)
	private double Put_Units_Required;
	@Column(name=GLOBALCONSTANT.DATABASE_PUT_UNITS_PUT)
	private double Put_Units_Put;
	@Column(name=GLOBALCONSTANT.DATABASE_CUT_UNITS)
	private double Cut_Units;
	@Column(name=GLOBALCONSTANT.DATABASE_DATE_TIME_ASSIGNED)
	private Date Date_Time_Assigned;
	@Column(name=GLOBALCONSTANT.DATABASE_FIRST_PUT_TIME)
	private Date First_Put_Time;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_PUT_TIME)
	private Date Last_Put_Time;
	@Column(name=GLOBALCONSTANT.DATABASE_PULL_TIME)
	private Date Pull_Time;
	@Column(name=GLOBALCONSTANT.DATABASE_PUSH_TIME)
	private Date Push_Time;
	@Column(name=GLOBALCONSTANT.DATABASE_STATUS)
	private String Status;
	@Column(name=GLOBALCONSTANT.DATABASE_CREATED_BY)
	private Date Created_By;
	@Column(name=GLOBALCONSTANT.DATABASE_DATE_CREATED)
	private Date Date_Created;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_DATE)
	private Date Last_Activity_Date;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_TEAM_MEMBER)
	private String Last_Activity_Team_Member;
}
