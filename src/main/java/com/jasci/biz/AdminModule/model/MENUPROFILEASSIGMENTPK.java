/*


Date Developed  Nov 26 2014
Created by: Sarvendra Tyagi
Description  pojo class of MENUPROFILEASSIGMENT Primary key in which getter and setter methods
 */


package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.jasci.common.constant.GLOBALCONSTANT;


@Embeddable
public class MENUPROFILEASSIGMENTPK  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileAssignment_Tenant)
	private String Tenant;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileAssignment_TeamMember)
	private String TeamMember;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileAssignment_MenuProfile)
	private String MenuProfile;
	
	

	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getTeamMember() {
		return TeamMember;
	}

	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}

	public String getMenuProfile() {
		return MenuProfile;
	}

	public void setMenuProfile(String menuProfile) {
		MenuProfile = menuProfile;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
