/**
 * 
 * @ file_name: CYLCOUNTYRLYSUMMARIESPK
 * @Developed by:Pradeep kumar
 *@Created Date:1 April 2015
 *Purpose : Pojo Used for Mapping with table CYL_COUNT_YRLY_SUMMARIES for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class CYLCOUNTYRLYSUMMARIESPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key;
	
	/*@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey_Tenant)
	private COMPANIESPK Foreign_Key_Tenant;*/
	
	@Column(name=GLOBALCONSTANT.DataBase_Fulfillment_Center_Id)
	private String FULFILLMENT_CENTER_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Area)
	private String AREA;
	
	@Column(name=GLOBALCONSTANT.DataBase_Location)
	private String LOCATION;
	
	@Column(name=GLOBALCONSTANT.DataBase_Product)
	private String PRODUCT;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quality)
	private String QUALITY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Year)
	private String YEAR;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month)
	private String MONTH;

	public COMPANIESPK getForeign_Key() {
		return Foreign_Key;
	}

	public void setForeign_Key(COMPANIESPK foreign_Key) {
		Foreign_Key = foreign_Key;
	}

	public String getFULFILLMENT_CENTER_ID() {
		return FULFILLMENT_CENTER_ID;
	}

	public void setFULFILLMENT_CENTER_ID(String fULFILLMENT_CENTER_ID) {
		FULFILLMENT_CENTER_ID = fULFILLMENT_CENTER_ID;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getLOCATION() {
		return LOCATION;
	}

	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}

	public String getQUALITY() {
		return QUALITY;
	}

	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}

	public String getYEAR() {
		return YEAR;
	}

	public void setYEAR(String yEAR) {
		YEAR = yEAR;
	}

	public String getMONTH() {
		return MONTH;
	}

	public void setMONTH(String mONTH) {
		MONTH = mONTH;
	}
	
	
}
