/*
Created by: Shailendra Rajput
Description Bean class of PUTWALLS in which getter and setter methods 
Created On:Oct 12 2015
 */
package com.jasci.biz.AdminModule.model;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DATABASE_TABLENAME_PUTWALLS)
public class PUTWALLS {

	@EmbeddedId
	private PUTWALLS Id;
	@Column(name=GLOBALCONSTANT.DATABASE_NAME20)
	private String Name20;
	@Column(name=GLOBALCONSTANT.DATABASE_NAME50)
	private String Name50;
	@Column(name=GLOBALCONSTANT.DATABASE_SINGLE_MULTI_SIDED)
	private String Single_Multi_Sided;
	@Column(name=GLOBALCONSTANT.DATABASE_PUTWALL_TYPE)
	private String Putwall_Type;
	@Column(name=GLOBALCONSTANT.DATABASE_STATUS)
	private String Status;
	@Column(name=GLOBALCONSTANT.DATABASE_CREATED_BY)
	private Date Created_By;
	@Column(name=GLOBALCONSTANT.DATABASE_DATE_CREATED)
	private Date Date_Created;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_DATE)
	private Date Last_Activity_Date;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_TEAM_MEMBER)
	private String Last_Activity_Team_Member;
	public PUTWALLS getId() {
		return Id;
	}
	public void setId(PUTWALLS id) {
		Id = id;
	}
	public String getName20() {
		return Name20;
	}
	public void setName20(String name20) {
		Name20 = name20;
	}
	public String getName50() {
		return Name50;
	}
	public void setName50(String name50) {
		Name50 = name50;
	}
	public String getSingle_Multi_Sided() {
		return Single_Multi_Sided;
	}
	public void setSingle_Multi_Sided(String single_Multi_Sided) {
		Single_Multi_Sided = single_Multi_Sided;
	}
	public String getPutwall_Type() {
		return Putwall_Type;
	}
	public void setPutwall_Type(String putwall_Type) {
		Putwall_Type = putwall_Type;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public Date getCreated_By() {
		return Created_By;
	}
	public void setCreated_By(Date created_By) {
		Created_By = created_By;
	}
	public Date getDate_Created() {
		return Date_Created;
	}
	public void setDate_Created(Date date_Created) {
		Date_Created = date_Created;
	}
	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}

	
}
