/*

Date Developed  Nov 03 2014
Description  pojo class of notes in which getter and setter methods for primary key only
Created By:Rakesh Pal
 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_NOTES)

@NamedNativeQueries({
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.Notes_Delete_NamedQuery,
	query = GLOBALCONSTANT.Notes_Delete_Query,
        resultClass = NOTES.class
	)})

public class NOTES {
	
	
	@EmbeddedId
	private NOTESPK Id;
	
	
	public NOTESPK getId() {
		return Id;
	}

	public void setId(NOTESPK id) {
		Id = id;
	}

	@Column(name=GLOBALCONSTANT.DataBase_Notes_Note)
	private String Note;

	@Column(name=GLOBALCONSTANT.DataBase_Notes_Last_Acitivity_Date)
	private Date LAST_ACTIVITY_DATE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Notes_Last_Activity_Team_Member)
	private String Last_Activity_Team_Member;
	
	@Lob
	@Column(name=GLOBALCONSTANT.DataBase_Notes_CONTINUATIONS_SEQUENCE)
	private String CONTINUATIONS_SEQUENCE;
	
	public String getNote() {
		return Note;
	}

	public String getCONTINUATIONS_SEQUENCE() {
		return CONTINUATIONS_SEQUENCE;
	}

	public void setCONTINUATIONS_SEQUENCE(String cONTINUATIONS_SEQUENCE) {
		CONTINUATIONS_SEQUENCE = cONTINUATIONS_SEQUENCE;
	}

	public void setNote(String note) {
		Note = note;
	}
	
	
	public Date getLast_Acitivity_Date() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLast_Acitivity_Date(Date last_Acitivity_Date) {
		LAST_ACTIVITY_DATE = last_Acitivity_Date;
	}
	
	
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}

	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	
	
	
}
