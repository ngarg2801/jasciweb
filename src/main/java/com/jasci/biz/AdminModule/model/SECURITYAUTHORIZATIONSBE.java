/*

Date Developed  Oct 15 2014
Description   pojo class of SecurityAuthorizations in which getter and setter methods
Craeted By Deepak Sharma
 */

package com.jasci.biz.AdminModule.model;

public class SECURITYAUTHORIZATIONSBE 
{

	
	private String UserId;


	private String Password;

	
	private String PasswordDate;

	
	private String Tenant;

	
	private String Company;

	private String TeamMember;

	
	private String SecurityQuestion1;

	
	private String SecurityQuestionAnswer1;
	
	
	private String SecurityQuestion2;

	
	private String SecurityQuestionAnswer2;
	
	
	private String SecurityQuestion3;

	
	private String SecurityQuestionAnswer3;

	
	private String LastPassword1;

	
	private String LastPassword2;

	
	private String LastPassword3;

	
	private String LastPassword4;


	private String LastPassword5;

	
	private int NumberAttempts;

	
	private String InvalidAttemptDate;

	
	private String Status;


	private String LastActivityDate;

	
	private String LastActivityTeamMember;
	
	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	

	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}

	public String getTeamMember() {
		return TeamMember;
	}

	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}



	public String getLastPassword1() {
		return LastPassword1;
	}

	public void setLastPassword1(String lastPassword1) {
		LastPassword1 = lastPassword1;
	}

	public String getLastPassword2() {
		return LastPassword2;
	}

	public void setLastPassword2(String lastPassword2) {
		LastPassword2 = lastPassword2;
	}

	public String getLastPassword3() {
		return LastPassword3;
	}

	public void setLastPassword3(String lastPassword3) {
		LastPassword3 = lastPassword3;
	}

	public String getLastPassword4() {
		return LastPassword4;
	}

	public void setLastPassword4(String lastPassword4) {
		LastPassword4 = lastPassword4;
	}

	public String getLastPassword5() {
		return LastPassword5;
	}

	public void setLastPassword5(String lastPassword5) {
		LastPassword5 = lastPassword5;
	}


	public int getNumberAttempts() {
		return NumberAttempts;
	}

	public void setNumberAttempts(int numberAttempts) {
		NumberAttempts = numberAttempts;
	}

	

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	

	public String getPasswordDate() {
		return PasswordDate;
	}

	public void setPasswordDate(String passwordDate) {
		PasswordDate = passwordDate;
	}

	public String getInvalidAttemptDate() {
		return InvalidAttemptDate;
	}

	public void setInvalidAttemptDate(String invalidAttemptDate) {
		InvalidAttemptDate = invalidAttemptDate;
	}

	public String getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	
	

	public String getSecurityQuestion1() {
		return SecurityQuestion1;
	}

	public void setSecurityQuestion1(String securityQuestion1) {
		SecurityQuestion1 = securityQuestion1;
	}

	public String getSecurityQuestionAnswer1() {
		return SecurityQuestionAnswer1;
	}

	public void setSecurityQuestionAnswer1(String securityQuestionAnswer1) {
		SecurityQuestionAnswer1 = securityQuestionAnswer1;
	}

	public String getSecurityQuestion2() {
		return SecurityQuestion2;
	}

	public void setSecurityQuestion2(String securityQuestion2) {
		SecurityQuestion2 = securityQuestion2;
	}

	public String getSecurityQuestionAnswer2() {
		return SecurityQuestionAnswer2;
	}

	public void setSecurityQuestionAnswer2(String securityQuestionAnswer2) {
		SecurityQuestionAnswer2 = securityQuestionAnswer2;
	}

	public String getSecurityQuestion3() {
		return SecurityQuestion3;
	}

	public void setSecurityQuestion3(String securityQuestion3) {
		SecurityQuestion3 = securityQuestion3;
	}

	public String getSecurityQuestionAnswer3() {
		return SecurityQuestionAnswer3;
	}

	public void setSecurityQuestionAnswer3(String securityQuestionAnswer3) {
		SecurityQuestionAnswer3 = securityQuestionAnswer3;
	}

	

	public SECURITYAUTHORIZATIONSBE() {


	}



}
