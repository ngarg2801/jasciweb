/**
 * 
 * @ file_name: EXECUTIONSEQUENCEHEADERSPK
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table EXECUTION_SEQUENCE_HEADERS for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class EXECUTIONSEQUENCEHEADERSPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key_Company;
	
	/*@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey_Tenant)
	private COMPANIESPK Foreign_Key_Tenant;*/
	
	@Column(name=GLOBALCONSTANT.DataBase_Application_Id)
	private String APPLICATION_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence_Group)
	private String EXECUTION_SEQUENCE_GROUP;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence_Name)
	private String EXECUTION_SEQUENCE_NAME;

	public COMPANIESPK getForeign_Key_Company() {
		return Foreign_Key_Company;
	}

	public void setForeign_Key_Company(COMPANIESPK foreign_Key_Company) {
		Foreign_Key_Company = foreign_Key_Company;
	}

	public String getAPPLICATION_ID() {
		return APPLICATION_ID;
	}

	public void setAPPLICATION_ID(String aPPLICATION_ID) {
		APPLICATION_ID = aPPLICATION_ID;
	}

	public String getEXECUTION_SEQUENCE_GROUP() {
		return EXECUTION_SEQUENCE_GROUP;
	}

	public void setEXECUTION_SEQUENCE_GROUP(String eXECUTION_SEQUENCE_GROUP) {
		EXECUTION_SEQUENCE_GROUP = eXECUTION_SEQUENCE_GROUP;
	}

	public String getEXECUTION_SEQUENCE_NAME() {
		return EXECUTION_SEQUENCE_NAME;
	}

	public void setEXECUTION_SEQUENCE_NAME(String eXECUTION_SEQUENCE_NAME) {
		EXECUTION_SEQUENCE_NAME = eXECUTION_SEQUENCE_NAME;
	}
	
	
}
