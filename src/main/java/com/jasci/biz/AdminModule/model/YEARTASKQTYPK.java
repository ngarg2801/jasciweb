/*


Created by: Aakash Bishnoi
Description  pojo class of YEAR_TASK_QTY Table only for primary keys
Created On:Apr 1 2015
 */

package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class YEARTASKQTYPK implements Serializable{

private static final long serialVersionUID = 1L;


@Column(name=GLOBALCONSTANT.DateBase_Tenant_Id)
private String Tenant_Id;
@Column(name=GLOBALCONSTANT.DateBase_Company_Id)
private String Company_Id;
@Column(name=GLOBALCONSTANT.DataBase_Year)
private String Year;
@Column(name=GLOBALCONSTANT.DataBase_Task)
private String Task;
@Column(name=GLOBALCONSTANT.DataBase_Product)
private String Product;
@Column(name=GLOBALCONSTANT.DataBase_Quality)
private String Quality;


public String getTenant_Id() {
	return Tenant_Id;
}
public void setTenant_Id(String tenant_Id) {
	Tenant_Id = tenant_Id;
}
public String getCompany_Id() {
	return Company_Id;
}
public void setCompany_Id(String company_Id) {
	Company_Id = company_Id;
}
public String getYear() {
	return Year;
}
public void setYear(String year) {
	Year = year;
}
public String getTask() {
	return Task;
}
public void setTask(String task) {
	Task = task;
}
public String getProduct() {
	return Product;
}
public void setProduct(String product) {
	Product = product;
}
public String getQuality() {
	return Quality;
}
public void setQuality(String quality) {
	Quality = quality;
}


}
