/**
 * 
 * @ file_name: CARRIERSPK
 * @Developed by:Pradeep kumar
 *@Created Date:1 April 2015
 *Purpose : Pojo only for primary keys Used for Mapping with table CARRIERS
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ForeignKey;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class CARRIERSPK implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		//bi-directional many-to-one association to Department
		@ManyToOne
		@JoinColumn(name=GLOBALCONSTANT.DataBase_Tenant_Id)
		@ForeignKey(name=GLOBALCONSTANT.DataBase_Foreign_Key_Name_TNTS)
		private TENANTS tenant;

		//bi-directional many-to-one association to Employee
		@ManyToOne
		@JoinColumns({
			@JoinColumn(name=GLOBALCONSTANT.DataBase_Tenant_Id, referencedColumnName=GLOBALCONSTANT.DataBase_Tenant_Id),
			@JoinColumn(name=GLOBALCONSTANT.DataBase_Company_Id, referencedColumnName=GLOBALCONSTANT.DataBase_Company_Id)
			})
		@ForeignKey(name=GLOBALCONSTANT.DataBase_Foreign_Key_Name_CMPY)
		private COMPANIES company;
		
	
	@Column(name=GLOBALCONSTANT.DataBase_Carrier_Id,nullable=false)
	private String CARRIER_ID;

	
	public String getCARRIER_ID() {
		return CARRIER_ID;
	}

	public void setCARRIER_ID(String cARRIER_ID) {
		CARRIER_ID = cARRIER_ID;
	}
	
	/*@ManyToOne(targetEntity = COMPANIESPK.class,
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
@JoinTable(name = "COMPANIES",
           joinColumns = {@JoinColumn(table = "COMPANIES",
                                      name = "COMP_TENA_FK",                               
                                      referencedColumnName = "TENANT"
                                      )},
           inverseJoinColumns = @JoinColumn(table = "TENANTS",
                                            name="fk_tenant",
                                            referencedColumnName = "TENANT"))
	
	private COMPANIESPK FK_CMY;*/
}
