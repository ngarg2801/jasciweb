/*
Created by: Shailendra Rajput
Description Bean class of CARTS in which getter and setter methods 
Created On:Oct 12 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;



@Entity
@Table(name=GLOBALCONSTANT.DATABASE_TABLENAME_CARTS)

public class CARTS {

	@EmbeddedId
	private CARTSPK Id;

	public CARTSPK getId() {
		return Id;
	}

	public void setId(CARTSPK id) {
		Id = id;
	}



		@Column(name=GLOBALCONSTANT.DATABASE_CART_TYPE)
		private String	Cart_Type;
		@Column(name=GLOBALCONSTANT.DATABASE_NAME20)
		private String	Name20;
		@Column(name=GLOBALCONSTANT.DATABASE_NAME50)
		private String	Name50;
		@Column(name=GLOBALCONSTANT.DATABASE_DEPARTMENT)
		private String	Department;
		@Column(name=GLOBALCONSTANT.DATABASE_CREATED_BY)
		private Date Created_By;
		@Column(name=GLOBALCONSTANT.DATABASE_DATE_CREATED)
		private String	Date_Created;
		@Column(name=GLOBALCONSTANT.DATABASE_TEAMMEMBER_ASSIGNEDTO_CART)
		private String	TeamMember_AssignedTo_Cart;
		@Column(name=GLOBALCONSTANT.DATABASE_NUMBER_CART_SLOTS)
		private int	Number_Cart_Slots;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT01)
		private double	Cart_Slot01;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT02)
		private double	Cart_Slot02;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT03)
		private double	Cart_Slot03;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT04)
		private double	Cart_Slot04;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT05)
		private double	Cart_Slot05;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT06)
		private double	Cart_Slot06;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT07)
		private double	Cart_Slot07;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT08)
		private double	Cart_Slot08;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT09)
		private double	Cart_Slot09;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT10)
		private double	Cart_Slot10;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT11)
		private double	Cart_Slot11;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT12)
		private double	Cart_Slot12;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT13)
		private double	Cart_Slot13;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT14)
		private double	Cart_Slot14;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT15)
		private double	Cart_Slot15;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT16)
		private double	Cart_Slot16;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT17)
		private double	Cart_Slot17;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT18)
		private double	Cart_Slot18;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT19)
		private double	Cart_Slot19;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT20)
		private double	Cart_Slot20;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT21)
		private double	Cart_Slot21;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT22)
		private double	Cart_Slot22;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT23)
		private double	Cart_Slot23;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT24)
		private double	Cart_Slot24;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT25)
		private double	Cart_Slot25;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT26)
		private double	Cart_Slot26;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT27)
		private double	Cart_Slot27;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT28)
		private double	Cart_Slot28;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT29)
		private double	Cart_Slot29;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT30)
		private double	Cart_Slot30;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT31)
		private double	Cart_Slot31;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT32)
		private double	Cart_Slot32;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT33)
		private double	Cart_Slot33;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT34)
		private double	Cart_Slot34;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT35)
		private double	Cart_Slot35;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT36)
		private double	Cart_Slot36;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT37)
		private double	Cart_Slot37;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT38)
		private double	Cart_Slot38;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT39)
		private double	Cart_Slot39;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT40)
		private double	Cart_Slot40;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT41)
		private double	Cart_Slot41;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT42)
		private double	Cart_Slot42;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT43)
		private double	Cart_Slot43;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT44)
		private double	Cart_Slot44;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT45)
		private double	Cart_Slot45;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT46)
		private double	Cart_Slot46;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT47)
		private double	Cart_Slot47;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT48)
		private double	Cart_Slot48;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT49)
		private double	Cart_Slot49;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_SLOT50)
		private double	Cart_Slot50;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER01)
		private double	Work_Control_Number01;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER02)
		private double	Work_Control_Number02;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER03)
		private double	Work_Control_Number03;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER04)
		private double	Work_Control_Number04;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER05)
		private double	Work_Control_Number05;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER06)
		private double	Work_Control_Number06;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER07)
		private double	Work_Control_Number07;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER08)
		private double	Work_Control_Number08;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER09)
		private double	Work_Control_Number09;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER10)
		private double	Work_Control_Number10;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER11)
		private double	Work_Control_Number11;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER12)
		private double	Work_Control_Number12;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER13)
		private double	Work_Control_Number13;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER14)
		private double	Work_Control_Number14;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER15)
		private double	Work_Control_Number15;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER16)
		private double	Work_Control_Number16;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER17)
		private double	Work_Control_Number17;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER18)
		private double	Work_Control_Number18;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER19)
		private double	Work_Control_Number19;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER20)
		private double	Work_Control_Number20;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER21)
		private double	Work_Control_Number21;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER22)
		private double	Work_Control_Number22;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER23)
		private double	Work_Control_Number23;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER24)
		private double	Work_Control_Number24;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER25)
		private double	Work_Control_Number25;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER26)
		private double	Work_Control_Number26;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER27)
		private double	Work_Control_Number27;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER28)
		private double	Work_Control_Number28;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER29)
		private double	Work_Control_Number29;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER30)
		private double	Work_Control_Number30;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER31)
		private double	Work_Control_Number31;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER32)
		private double	Work_Control_Number32;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER33)
		private double	Work_Control_Number33;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER34)
		private double	Work_Control_Number34;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER35)
		private double	Work_Control_Number35;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER36)
		private double	Work_Control_Number36;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER37)
		private double	Work_Control_Number37;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER38)
		private double	Work_Control_Number38;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER39)
		private double	Work_Control_Number39;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER40)
		private double	Work_Control_Number40;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER41)
		private double	Work_Control_Number41;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER42)
		private double	Work_Control_Number42;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER43)
		private double	Work_Control_Number43;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER44)
		private double	Work_Control_Number44;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER45)
		private double	Work_Control_Number45;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER46)
		private double	Work_Control_Number46;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER47)
		private double	Work_Control_Number47;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER48)
		private double	Work_Control_Number48;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER49)
		private double	Work_Control_Number49;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER50)
		private double	Work_Control_Number50;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE01)
		private String	Work_Type01;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE02)
		private String	Work_Type02;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE03)
		private String	Work_Type03;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE04)
		private String	Work_Type04;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE05)
		private String	Work_Type05;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE06)
		private String	Work_Type06;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE07)
		private String	Work_Type07;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE08)
		private String	Work_Type08;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE09)
		private String	Work_Type09;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE10)
		private String	Work_Type10;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE11)
		private String	Work_Type11;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE12)
		private String	Work_Type12;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE13)
		private String	Work_Type13;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE14)
		private String	Work_Type14;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE15)
		private String	Work_Type15;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE16)
		private String	Work_Type16;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE17)
		private String	Work_Type17;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE18)
		private String	Work_Type18;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE19)
		private String	Work_Type19;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE20)
		private String	Work_Type20;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE21)
		private String	Work_Type21;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE22)
		private String	Work_Type22;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE23)
		private String	Work_Type23;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE24)
		private String	Work_Type24;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE25)
		private String	Work_Type25;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE26)
		private String	Work_Type26;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE27)
		private String	Work_Type27;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE28)
		private String	Work_Type28;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE29)
		private String	Work_Type29;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE30)
		private String	Work_Type30;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE31)
		private String	Work_Type31;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE32)
		private String	Work_Type32;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE33)
		private String	Work_Type33;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE34)
		private String	Work_Type34;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE35)
		private String	Work_Type35;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE36)
		private String	Work_Type36;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE37)
		private String	Work_Type37;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE38)
		private String	Work_Type38;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE39)
		private String	Work_Type39;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE40)
		private String	Work_Type40;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE41)
		private String	Work_Type41;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE42)
		private String	Work_Type42;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE43)
		private String	Work_Type43;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE44)
		private String	Work_Type44;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE45)
		private String	Work_Type45;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE46)
		private String	Work_Type46;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE47)
		private String	Work_Type47;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE48)
		private String	Work_Type48;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE49)
		private String	Work_Type49;
		@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE50)
		private String	Work_Type50;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN01)
		private String	LPN01;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN02)
		private String	LPN02;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN03)
		private String	LPN03;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN04)
		private String	LPN04;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN05)
		private String	LPN05;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN06)
		private String	LPN06;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN07)
		private String	LPN07;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN08)
		private String	LPN08;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN09)
		private String	LPN09;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN10)
		private String	LPN10;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN11)
		private String	LPN11;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN12)
		private String	LPN12;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN13)
		private String	LPN13;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN14)
		private String	LPN14;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN15)
		private String	LPN15;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN16)
		private String	LPN16;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN17)
		private String	LPN17;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN18)
		private String	LPN18;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN19)
		private String	LPN19;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN20)
		private String	LPN20;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN21)
		private String	LPN21;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN22)
		private String	LPN22;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN23)
		private String	LPN23;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN24)
		private String	LPN24;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN25)
		private String	LPN25;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN26)
		private String	LPN26;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN27)
		private String	LPN27;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN28)
		private String	LPN28;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN29)
		private String	LPN29;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN30)
		private String	LPN30;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN31)
		private String	LPN31;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN32)
		private String	LPN32;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN33)
		private String	LPN33;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN34)
		private String	LPN34;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN35)
		private String	LPN35;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN36)
		private String	LPN36;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN37)
		private String	LPN37;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN38)
		private String	LPN38;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN39)
		private String	LPN39;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN40)
		private String	LPN40;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN41)
		private String	LPN41;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN42)
		private String	LPN42;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN43)
		private String	LPN43;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN44)
		private String	LPN44;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN45)
		private String	LPN45;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN46)
		private String	LPN46;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN47)
		private String	LPN47;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN48)
		private String	LPN48;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN49)
		private String	LPN49;
		@Column(name=GLOBALCONSTANT.DATABASE_LPN50)
		private String	LPN50;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE01)
		private String	Container_Type01;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE02)
		private String	Container_Type02;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE03)
		private String	Container_Type03;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE04)
		private String	Container_Type04;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE05)
		private String	Container_Type05;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE06)
		private String	Container_Type06;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE07)
		private String	Container_Type07;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE08)
		private String	Container_Type08;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE09)
		private String	Container_Type09;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE10)
		private String	Container_Type10;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE11)
		private String	Container_Type11;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE12)
		private String	Container_Type12;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE13)
		private String	Container_Type13;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE14)
		private String	Container_Type14;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE15)
		private String	Container_Type15;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE16)
		private String	Container_Type16;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE17)
		private String	Container_Type17;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE18)
		private String	Container_Type18;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE19)
		private String	Container_Type19;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE20)
		private String	Container_Type20;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE21)
		private String	Container_Type21;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE22)
		private String	Container_Type22;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE23)
		private String	Container_Type23;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE24)
		private String	Container_Type24;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE25)
		private String	Container_Type25;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE26)
		private String	Container_Type26;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE27)
		private String	Container_Type27;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE28)
		private String	Container_Type28;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE29)
		private String	Container_Type29;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE30)
		private String	Container_Type30;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE31)
		private String	Container_Type31;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE32)
		private String	Container_Type32;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE33)
		private String	Container_Type33;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE34)
		private String	Container_Type34;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE35)
		private String	Container_Type35;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE36)
		private String	Container_Type36;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE37)
		private String	Container_Type37;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE38)
		private String	Container_Type38;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE39)
		private String	Container_Type39;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE40)
		private String	Container_Type40;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE41)
		private String	Container_Type41;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE42)
		private String	Container_Type42;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE43)
		private String	Container_Type43;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE44)
		private String	Container_Type44;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE45)
		private String	Container_Type45;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE46)
		private String	Container_Type46;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE47)
		private String	Container_Type47;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE48)
		private String	Container_Type48;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE49)
		private String	Container_Type49;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE50)
		private String	Container_Type50;
		
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID01)
		private String	Container_ID01;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID02)
		private String	Container_ID02;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID03)
		private String	Container_ID03;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID04)
		private String	Container_ID04;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID05)
		private String	Container_ID05;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID06)
		private String	Container_ID06;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID07)
		private String	Container_ID07;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID08)
		private String	Container_ID08;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID09)
		private String	Container_ID09;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID10)
		private String	Container_ID10;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID11)
		private String	Container_ID11;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID12)
		private String	Container_ID12;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID13)
		private String	Container_ID13;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID14)
		private String	Container_ID14;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID15)
		private String	Container_ID15;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID16)
		private String	Container_ID16;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID17)
		private String	Container_ID17;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID18)
		private String	Container_ID18;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID19)
		private String	Container_ID19;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID20)
		private String	Container_ID20;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID21)
		private String	Container_ID21;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID22)
		private String	Container_ID22;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID23)
		private String	Container_ID23;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID24)
		private String	Container_ID24;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID25)
		private String	Container_ID25;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID26)
		private String	Container_ID26;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID27)
		private String	Container_ID27;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID28)
		private String	Container_ID28;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID29)
		private String	Container_ID29;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID30)
		private String	Container_ID30;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID31)
		private String	Container_ID31;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID32)
		private String	Container_ID32;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID33)
		private String	Container_ID33;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID34)
		private String	Container_ID34;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID35)
		private String	Container_ID35;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID36)
		private String	Container_ID36;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID37)
		private String	Container_ID37;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID38)
		private String	Container_ID38;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID39)
		private String	Container_ID39;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID40)
		private String	Container_ID40;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID41)
		private String	Container_ID41;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID42)
		private String	Container_ID42;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID43)
		private String	Container_ID43;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID44)
		private String	Container_ID44;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID45)
		private String	Container_ID45;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID46)
		private String	Container_ID46;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID47)
		private String	Container_ID47;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID48)
		private String	Container_ID48;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID49)
		private String	Container_ID49;
		@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID50)
		private String	Container_ID50;
		
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS01)
		private String	Cart_Location_Status01;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS02)
		private String	Cart_Location_Status02;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS03)
		private String	Cart_Location_Status03;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS04)
		private String	Cart_Location_Status04;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS05)
		private String	Cart_Location_Status05;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS06)
		private String	Cart_Location_Status06;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS07)
		private String	Cart_Location_Status07;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS08)
		private String	Cart_Location_Status08;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS09)
		private String	Cart_Location_Status09;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS10)
		private String	Cart_Location_Status10;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS11)
		private String	Cart_Location_Status11;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS12)
		private String	Cart_Location_Status12;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS13)
		private String	Cart_Location_Status13;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS14)
		private String	Cart_Location_Status14;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS15)
		private String	Cart_Location_Status15;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS16)
		private String	Cart_Location_Status16;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS17)
		private String	Cart_Location_Status17;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS18)
		private String	Cart_Location_Status18;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS19)
		private String	Cart_Location_Status19;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS20)
		private String	Cart_Location_Status20;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS21)
		private String	Cart_Location_Status21;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS22)
		private String	Cart_Location_Status22;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS23)
		private String	Cart_Location_Status23;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS24)
		private String	Cart_Location_Status24;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS25)
		private String	Cart_Location_Status25;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS26)
		private String	Cart_Location_Status26;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS27)
		private String	Cart_Location_Status27;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS28)
		private String	Cart_Location_Status28;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS29)
		private String	Cart_Location_Status29;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS30)
		private String	Cart_Location_Status30;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS31)
		private String	Cart_Location_Status31;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS32)
		private String	Cart_Location_Status32;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS33)
		private String	Cart_Location_Status33;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS34)
		private String	Cart_Location_Status34;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS35)
		private String	Cart_Location_Status35;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS36)
		private String	Cart_Location_Status36;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS37)
		private String	Cart_Location_Status37;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS38)
		private String	Cart_Location_Status38;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS39)
		private String	Cart_Location_Status39;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS40)
		private String	Cart_Location_Status40;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS41)
		private String	Cart_Location_Status41;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS42)
		private String	Cart_Location_Status42;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS43)
		private String	Cart_Location_Status43;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS44)
		private String	Cart_Location_Status44;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS45)
		private String	Cart_Location_Status45;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS46)
		private String	Cart_Location_Status46;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS47)
		private String	Cart_Location_Status47;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS48)
		private String	Cart_Location_Status48;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS49)
		private String	Cart_Location_Status49;
		@Column(name=GLOBALCONSTANT.DATABASE_CART_LOCATION_STATUS50)
		private String	Cart_Location_Status50;
		@Column(name=GLOBALCONSTANT.DATABASE_STATUS)
		private String	Status;
		@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_DATE)
		private Date Last_Activity_Date;
		@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_TEAM_MEMBER)
		private String	Last_Activity_Team_Member;

		public String getCart_Type() {
			return Cart_Type;
		}

		public void setCart_Type(String cart_Type) {
			Cart_Type = cart_Type;
		}

		public String getName20() {
			return Name20;
		}

		public void setName20(String name20) {
			Name20 = name20;
		}

		public String getName50() {
			return Name50;
		}

		public void setName50(String name50) {
			Name50 = name50;
		}

		public String getDepartment() {
			return Department;
		}

		public void setDepartment(String department) {
			Department = department;
		}

		public Date getCreated_By() {
			return Created_By;
		}

		public void setCreated_By(Date created_By) {
			Created_By = created_By;
		}

		public String getDate_Created() {
			return Date_Created;
		}

		public void setDate_Created(String date_Created) {
			Date_Created = date_Created;
		}

		public String getTeamMember_AssignedTo_Cart() {
			return TeamMember_AssignedTo_Cart;
		}

		public void setTeamMember_AssignedTo_Cart(String teamMember_AssignedTo_Cart) {
			TeamMember_AssignedTo_Cart = teamMember_AssignedTo_Cart;
		}

		public int getNumber_Cart_Slots() {
			return Number_Cart_Slots;
		}

		public void setNumber_Cart_Slots(int number_Cart_Slots) {
			Number_Cart_Slots = number_Cart_Slots;
		}

		public double getCart_Slot01() {
			return Cart_Slot01;
		}

		public void setCart_Slot01(double cart_Slot01) {
			Cart_Slot01 = cart_Slot01;
		}

		public double getCart_Slot02() {
			return Cart_Slot02;
		}

		public void setCart_Slot02(double cart_Slot02) {
			Cart_Slot02 = cart_Slot02;
		}

		public double getCart_Slot03() {
			return Cart_Slot03;
		}

		public void setCart_Slot03(double cart_Slot03) {
			Cart_Slot03 = cart_Slot03;
		}

		public double getCart_Slot04() {
			return Cart_Slot04;
		}

		public void setCart_Slot04(double cart_Slot04) {
			Cart_Slot04 = cart_Slot04;
		}

		public double getCart_Slot05() {
			return Cart_Slot05;
		}

		public void setCart_Slot05(double cart_Slot05) {
			Cart_Slot05 = cart_Slot05;
		}

		public double getCart_Slot06() {
			return Cart_Slot06;
		}

		public void setCart_Slot06(double cart_Slot06) {
			Cart_Slot06 = cart_Slot06;
		}

		public double getCart_Slot07() {
			return Cart_Slot07;
		}

		public void setCart_Slot07(double cart_Slot07) {
			Cart_Slot07 = cart_Slot07;
		}

		public double getCart_Slot08() {
			return Cart_Slot08;
		}

		public void setCart_Slot08(double cart_Slot08) {
			Cart_Slot08 = cart_Slot08;
		}

		public double getCart_Slot09() {
			return Cart_Slot09;
		}

		public void setCart_Slot09(double cart_Slot09) {
			Cart_Slot09 = cart_Slot09;
		}

		public double getCart_Slot10() {
			return Cart_Slot10;
		}

		public void setCart_Slot10(double cart_Slot10) {
			Cart_Slot10 = cart_Slot10;
		}

		public double getCart_Slot11() {
			return Cart_Slot11;
		}

		public void setCart_Slot11(double cart_Slot11) {
			Cart_Slot11 = cart_Slot11;
		}

		public double getCart_Slot12() {
			return Cart_Slot12;
		}

		public void setCart_Slot12(double cart_Slot12) {
			Cart_Slot12 = cart_Slot12;
		}

		public double getCart_Slot13() {
			return Cart_Slot13;
		}

		public void setCart_Slot13(double cart_Slot13) {
			Cart_Slot13 = cart_Slot13;
		}

		public double getCart_Slot14() {
			return Cart_Slot14;
		}

		public void setCart_Slot14(double cart_Slot14) {
			Cart_Slot14 = cart_Slot14;
		}

		public double getCart_Slot15() {
			return Cart_Slot15;
		}

		public void setCart_Slot15(double cart_Slot15) {
			Cart_Slot15 = cart_Slot15;
		}

		public double getCart_Slot16() {
			return Cart_Slot16;
		}

		public void setCart_Slot16(double cart_Slot16) {
			Cart_Slot16 = cart_Slot16;
		}

		public double getCart_Slot17() {
			return Cart_Slot17;
		}

		public void setCart_Slot17(double cart_Slot17) {
			Cart_Slot17 = cart_Slot17;
		}

		public double getCart_Slot18() {
			return Cart_Slot18;
		}

		public void setCart_Slot18(double cart_Slot18) {
			Cart_Slot18 = cart_Slot18;
		}

		public double getCart_Slot19() {
			return Cart_Slot19;
		}

		public void setCart_Slot19(double cart_Slot19) {
			Cart_Slot19 = cart_Slot19;
		}

		public double getCart_Slot20() {
			return Cart_Slot20;
		}

		public void setCart_Slot20(double cart_Slot20) {
			Cart_Slot20 = cart_Slot20;
		}

		public double getCart_Slot21() {
			return Cart_Slot21;
		}

		public void setCart_Slot21(double cart_Slot21) {
			Cart_Slot21 = cart_Slot21;
		}

		public double getCart_Slot22() {
			return Cart_Slot22;
		}

		public void setCart_Slot22(double cart_Slot22) {
			Cart_Slot22 = cart_Slot22;
		}

		public double getCart_Slot23() {
			return Cart_Slot23;
		}

		public void setCart_Slot23(double cart_Slot23) {
			Cart_Slot23 = cart_Slot23;
		}

		public double getCart_Slot24() {
			return Cart_Slot24;
		}

		public void setCart_Slot24(double cart_Slot24) {
			Cart_Slot24 = cart_Slot24;
		}

		public double getCart_Slot25() {
			return Cart_Slot25;
		}

		public void setCart_Slot25(double cart_Slot25) {
			Cart_Slot25 = cart_Slot25;
		}

		public double getCart_Slot26() {
			return Cart_Slot26;
		}

		public void setCart_Slot26(double cart_Slot26) {
			Cart_Slot26 = cart_Slot26;
		}

		public double getCart_Slot27() {
			return Cart_Slot27;
		}

		public void setCart_Slot27(double cart_Slot27) {
			Cart_Slot27 = cart_Slot27;
		}

		public double getCart_Slot28() {
			return Cart_Slot28;
		}

		public void setCart_Slot28(double cart_Slot28) {
			Cart_Slot28 = cart_Slot28;
		}

		public double getCart_Slot29() {
			return Cart_Slot29;
		}

		public void setCart_Slot29(double cart_Slot29) {
			Cart_Slot29 = cart_Slot29;
		}

		public double getCart_Slot30() {
			return Cart_Slot30;
		}

		public void setCart_Slot30(double cart_Slot30) {
			Cart_Slot30 = cart_Slot30;
		}

		public double getCart_Slot31() {
			return Cart_Slot31;
		}

		public void setCart_Slot31(double cart_Slot31) {
			Cart_Slot31 = cart_Slot31;
		}

		public double getCart_Slot32() {
			return Cart_Slot32;
		}

		public void setCart_Slot32(double cart_Slot32) {
			Cart_Slot32 = cart_Slot32;
		}

		public double getCart_Slot33() {
			return Cart_Slot33;
		}

		public void setCart_Slot33(double cart_Slot33) {
			Cart_Slot33 = cart_Slot33;
		}

		public double getCart_Slot34() {
			return Cart_Slot34;
		}

		public void setCart_Slot34(double cart_Slot34) {
			Cart_Slot34 = cart_Slot34;
		}

		public double getCart_Slot35() {
			return Cart_Slot35;
		}

		public void setCart_Slot35(double cart_Slot35) {
			Cart_Slot35 = cart_Slot35;
		}

		public double getCart_Slot36() {
			return Cart_Slot36;
		}

		public void setCart_Slot36(double cart_Slot36) {
			Cart_Slot36 = cart_Slot36;
		}

		public double getCart_Slot37() {
			return Cart_Slot37;
		}

		public void setCart_Slot37(double cart_Slot37) {
			Cart_Slot37 = cart_Slot37;
		}

		public double getCart_Slot38() {
			return Cart_Slot38;
		}

		public void setCart_Slot38(double cart_Slot38) {
			Cart_Slot38 = cart_Slot38;
		}

		public double getCart_Slot39() {
			return Cart_Slot39;
		}

		public void setCart_Slot39(double cart_Slot39) {
			Cart_Slot39 = cart_Slot39;
		}

		public double getCart_Slot40() {
			return Cart_Slot40;
		}

		public void setCart_Slot40(double cart_Slot40) {
			Cart_Slot40 = cart_Slot40;
		}

		public double getCart_Slot41() {
			return Cart_Slot41;
		}

		public void setCart_Slot41(double cart_Slot41) {
			Cart_Slot41 = cart_Slot41;
		}

		public double getCart_Slot42() {
			return Cart_Slot42;
		}

		public void setCart_Slot42(double cart_Slot42) {
			Cart_Slot42 = cart_Slot42;
		}

		public double getCart_Slot43() {
			return Cart_Slot43;
		}

		public void setCart_Slot43(double cart_Slot43) {
			Cart_Slot43 = cart_Slot43;
		}

		public double getCart_Slot44() {
			return Cart_Slot44;
		}

		public void setCart_Slot44(double cart_Slot44) {
			Cart_Slot44 = cart_Slot44;
		}

		public double getCart_Slot45() {
			return Cart_Slot45;
		}

		public void setCart_Slot45(double cart_Slot45) {
			Cart_Slot45 = cart_Slot45;
		}

		public double getCart_Slot46() {
			return Cart_Slot46;
		}

		public void setCart_Slot46(double cart_Slot46) {
			Cart_Slot46 = cart_Slot46;
		}

		public double getCart_Slot47() {
			return Cart_Slot47;
		}

		public void setCart_Slot47(double cart_Slot47) {
			Cart_Slot47 = cart_Slot47;
		}

		public double getCart_Slot48() {
			return Cart_Slot48;
		}

		public void setCart_Slot48(double cart_Slot48) {
			Cart_Slot48 = cart_Slot48;
		}

		public double getCart_Slot49() {
			return Cart_Slot49;
		}

		public void setCart_Slot49(double cart_Slot49) {
			Cart_Slot49 = cart_Slot49;
		}

		public double getCart_Slot50() {
			return Cart_Slot50;
		}

		public void setCart_Slot50(double cart_Slot50) {
			Cart_Slot50 = cart_Slot50;
		}

		public double getWork_Control_Number01() {
			return Work_Control_Number01;
		}

		public void setWork_Control_Number01(double work_Control_Number01) {
			Work_Control_Number01 = work_Control_Number01;
		}

		public double getWork_Control_Number02() {
			return Work_Control_Number02;
		}

		public void setWork_Control_Number02(double work_Control_Number02) {
			Work_Control_Number02 = work_Control_Number02;
		}

		public double getWork_Control_Number03() {
			return Work_Control_Number03;
		}

		public void setWork_Control_Number03(double work_Control_Number03) {
			Work_Control_Number03 = work_Control_Number03;
		}

		public double getWork_Control_Number04() {
			return Work_Control_Number04;
		}

		public void setWork_Control_Number04(double work_Control_Number04) {
			Work_Control_Number04 = work_Control_Number04;
		}

		public double getWork_Control_Number05() {
			return Work_Control_Number05;
		}

		public void setWork_Control_Number05(double work_Control_Number05) {
			Work_Control_Number05 = work_Control_Number05;
		}

		public double getWork_Control_Number06() {
			return Work_Control_Number06;
		}

		public void setWork_Control_Number06(double work_Control_Number06) {
			Work_Control_Number06 = work_Control_Number06;
		}

		public double getWork_Control_Number07() {
			return Work_Control_Number07;
		}

		public void setWork_Control_Number07(double work_Control_Number07) {
			Work_Control_Number07 = work_Control_Number07;
		}

		public double getWork_Control_Number08() {
			return Work_Control_Number08;
		}

		public void setWork_Control_Number08(double work_Control_Number08) {
			Work_Control_Number08 = work_Control_Number08;
		}

		public double getWork_Control_Number09() {
			return Work_Control_Number09;
		}

		public void setWork_Control_Number09(double work_Control_Number09) {
			Work_Control_Number09 = work_Control_Number09;
		}

		public double getWork_Control_Number10() {
			return Work_Control_Number10;
		}

		public void setWork_Control_Number10(double work_Control_Number10) {
			Work_Control_Number10 = work_Control_Number10;
		}

		public double getWork_Control_Number11() {
			return Work_Control_Number11;
		}

		public void setWork_Control_Number11(double work_Control_Number11) {
			Work_Control_Number11 = work_Control_Number11;
		}

		public double getWork_Control_Number12() {
			return Work_Control_Number12;
		}

		public void setWork_Control_Number12(double work_Control_Number12) {
			Work_Control_Number12 = work_Control_Number12;
		}

		public double getWork_Control_Number13() {
			return Work_Control_Number13;
		}

		public void setWork_Control_Number13(double work_Control_Number13) {
			Work_Control_Number13 = work_Control_Number13;
		}

		public double getWork_Control_Number14() {
			return Work_Control_Number14;
		}

		public void setWork_Control_Number14(double work_Control_Number14) {
			Work_Control_Number14 = work_Control_Number14;
		}

		public double getWork_Control_Number15() {
			return Work_Control_Number15;
		}

		public void setWork_Control_Number15(double work_Control_Number15) {
			Work_Control_Number15 = work_Control_Number15;
		}

		public double getWork_Control_Number16() {
			return Work_Control_Number16;
		}

		public void setWork_Control_Number16(double work_Control_Number16) {
			Work_Control_Number16 = work_Control_Number16;
		}

		public double getWork_Control_Number17() {
			return Work_Control_Number17;
		}

		public void setWork_Control_Number17(double work_Control_Number17) {
			Work_Control_Number17 = work_Control_Number17;
		}

		public double getWork_Control_Number18() {
			return Work_Control_Number18;
		}

		public void setWork_Control_Number18(double work_Control_Number18) {
			Work_Control_Number18 = work_Control_Number18;
		}

		public double getWork_Control_Number19() {
			return Work_Control_Number19;
		}

		public void setWork_Control_Number19(double work_Control_Number19) {
			Work_Control_Number19 = work_Control_Number19;
		}

		public double getWork_Control_Number20() {
			return Work_Control_Number20;
		}

		public void setWork_Control_Number20(double work_Control_Number20) {
			Work_Control_Number20 = work_Control_Number20;
		}

		public double getWork_Control_Number21() {
			return Work_Control_Number21;
		}

		public void setWork_Control_Number21(double work_Control_Number21) {
			Work_Control_Number21 = work_Control_Number21;
		}

		public double getWork_Control_Number22() {
			return Work_Control_Number22;
		}

		public void setWork_Control_Number22(double work_Control_Number22) {
			Work_Control_Number22 = work_Control_Number22;
		}

		public double getWork_Control_Number23() {
			return Work_Control_Number23;
		}

		public void setWork_Control_Number23(double work_Control_Number23) {
			Work_Control_Number23 = work_Control_Number23;
		}

		public double getWork_Control_Number24() {
			return Work_Control_Number24;
		}

		public void setWork_Control_Number24(double work_Control_Number24) {
			Work_Control_Number24 = work_Control_Number24;
		}

		public double getWork_Control_Number25() {
			return Work_Control_Number25;
		}

		public void setWork_Control_Number25(double work_Control_Number25) {
			Work_Control_Number25 = work_Control_Number25;
		}

		public double getWork_Control_Number26() {
			return Work_Control_Number26;
		}

		public void setWork_Control_Number26(double work_Control_Number26) {
			Work_Control_Number26 = work_Control_Number26;
		}

		public double getWork_Control_Number27() {
			return Work_Control_Number27;
		}

		public void setWork_Control_Number27(double work_Control_Number27) {
			Work_Control_Number27 = work_Control_Number27;
		}

		public double getWork_Control_Number28() {
			return Work_Control_Number28;
		}

		public void setWork_Control_Number28(double work_Control_Number28) {
			Work_Control_Number28 = work_Control_Number28;
		}

		public double getWork_Control_Number29() {
			return Work_Control_Number29;
		}

		public void setWork_Control_Number29(double work_Control_Number29) {
			Work_Control_Number29 = work_Control_Number29;
		}

		public double getWork_Control_Number30() {
			return Work_Control_Number30;
		}

		public void setWork_Control_Number30(double work_Control_Number30) {
			Work_Control_Number30 = work_Control_Number30;
		}

		public double getWork_Control_Number31() {
			return Work_Control_Number31;
		}

		public void setWork_Control_Number31(double work_Control_Number31) {
			Work_Control_Number31 = work_Control_Number31;
		}

		public double getWork_Control_Number32() {
			return Work_Control_Number32;
		}

		public void setWork_Control_Number32(double work_Control_Number32) {
			Work_Control_Number32 = work_Control_Number32;
		}

		public double getWork_Control_Number33() {
			return Work_Control_Number33;
		}

		public void setWork_Control_Number33(double work_Control_Number33) {
			Work_Control_Number33 = work_Control_Number33;
		}

		public double getWork_Control_Number34() {
			return Work_Control_Number34;
		}

		public void setWork_Control_Number34(double work_Control_Number34) {
			Work_Control_Number34 = work_Control_Number34;
		}

		public double getWork_Control_Number35() {
			return Work_Control_Number35;
		}

		public void setWork_Control_Number35(double work_Control_Number35) {
			Work_Control_Number35 = work_Control_Number35;
		}

		public double getWork_Control_Number36() {
			return Work_Control_Number36;
		}

		public void setWork_Control_Number36(double work_Control_Number36) {
			Work_Control_Number36 = work_Control_Number36;
		}

		public double getWork_Control_Number37() {
			return Work_Control_Number37;
		}

		public void setWork_Control_Number37(double work_Control_Number37) {
			Work_Control_Number37 = work_Control_Number37;
		}

		public double getWork_Control_Number38() {
			return Work_Control_Number38;
		}

		public void setWork_Control_Number38(double work_Control_Number38) {
			Work_Control_Number38 = work_Control_Number38;
		}

		public double getWork_Control_Number39() {
			return Work_Control_Number39;
		}

		public void setWork_Control_Number39(double work_Control_Number39) {
			Work_Control_Number39 = work_Control_Number39;
		}

		public double getWork_Control_Number40() {
			return Work_Control_Number40;
		}

		public void setWork_Control_Number40(double work_Control_Number40) {
			Work_Control_Number40 = work_Control_Number40;
		}

		public double getWork_Control_Number41() {
			return Work_Control_Number41;
		}

		public void setWork_Control_Number41(double work_Control_Number41) {
			Work_Control_Number41 = work_Control_Number41;
		}

		public double getWork_Control_Number42() {
			return Work_Control_Number42;
		}

		public void setWork_Control_Number42(double work_Control_Number42) {
			Work_Control_Number42 = work_Control_Number42;
		}

		public double getWork_Control_Number43() {
			return Work_Control_Number43;
		}

		public void setWork_Control_Number43(double work_Control_Number43) {
			Work_Control_Number43 = work_Control_Number43;
		}

		public double getWork_Control_Number44() {
			return Work_Control_Number44;
		}

		public void setWork_Control_Number44(double work_Control_Number44) {
			Work_Control_Number44 = work_Control_Number44;
		}

		public double getWork_Control_Number45() {
			return Work_Control_Number45;
		}

		public void setWork_Control_Number45(double work_Control_Number45) {
			Work_Control_Number45 = work_Control_Number45;
		}

		public double getWork_Control_Number46() {
			return Work_Control_Number46;
		}

		public void setWork_Control_Number46(double work_Control_Number46) {
			Work_Control_Number46 = work_Control_Number46;
		}

		public double getWork_Control_Number47() {
			return Work_Control_Number47;
		}

		public void setWork_Control_Number47(double work_Control_Number47) {
			Work_Control_Number47 = work_Control_Number47;
		}

		public double getWork_Control_Number48() {
			return Work_Control_Number48;
		}

		public void setWork_Control_Number48(double work_Control_Number48) {
			Work_Control_Number48 = work_Control_Number48;
		}

		public double getWork_Control_Number49() {
			return Work_Control_Number49;
		}

		public void setWork_Control_Number49(double work_Control_Number49) {
			Work_Control_Number49 = work_Control_Number49;
		}

		public double getWork_Control_Number50() {
			return Work_Control_Number50;
		}

		public void setWork_Control_Number50(double work_Control_Number50) {
			Work_Control_Number50 = work_Control_Number50;
		}

		public String getWork_Type01() {
			return Work_Type01;
		}

		public void setWork_Type01(String work_Type01) {
			Work_Type01 = work_Type01;
		}

		public String getWork_Type02() {
			return Work_Type02;
		}

		public void setWork_Type02(String work_Type02) {
			Work_Type02 = work_Type02;
		}

		public String getWork_Type03() {
			return Work_Type03;
		}

		public void setWork_Type03(String work_Type03) {
			Work_Type03 = work_Type03;
		}

		public String getWork_Type04() {
			return Work_Type04;
		}

		public void setWork_Type04(String work_Type04) {
			Work_Type04 = work_Type04;
		}

		public String getWork_Type05() {
			return Work_Type05;
		}

		public void setWork_Type05(String work_Type05) {
			Work_Type05 = work_Type05;
		}

		public String getWork_Type06() {
			return Work_Type06;
		}

		public void setWork_Type06(String work_Type06) {
			Work_Type06 = work_Type06;
		}

		public String getWork_Type07() {
			return Work_Type07;
		}

		public void setWork_Type07(String work_Type07) {
			Work_Type07 = work_Type07;
		}

		public String getWork_Type08() {
			return Work_Type08;
		}

		public void setWork_Type08(String work_Type08) {
			Work_Type08 = work_Type08;
		}

		public String getWork_Type09() {
			return Work_Type09;
		}

		public void setWork_Type09(String work_Type09) {
			Work_Type09 = work_Type09;
		}

		public String getWork_Type10() {
			return Work_Type10;
		}

		public void setWork_Type10(String work_Type10) {
			Work_Type10 = work_Type10;
		}

		public String getWork_Type11() {
			return Work_Type11;
		}

		public void setWork_Type11(String work_Type11) {
			Work_Type11 = work_Type11;
		}

		public String getWork_Type12() {
			return Work_Type12;
		}

		public void setWork_Type12(String work_Type12) {
			Work_Type12 = work_Type12;
		}

		public String getWork_Type13() {
			return Work_Type13;
		}

		public void setWork_Type13(String work_Type13) {
			Work_Type13 = work_Type13;
		}

		public String getWork_Type14() {
			return Work_Type14;
		}

		public void setWork_Type14(String work_Type14) {
			Work_Type14 = work_Type14;
		}

		public String getWork_Type15() {
			return Work_Type15;
		}

		public void setWork_Type15(String work_Type15) {
			Work_Type15 = work_Type15;
		}

		public String getWork_Type16() {
			return Work_Type16;
		}

		public void setWork_Type16(String work_Type16) {
			Work_Type16 = work_Type16;
		}

		public String getWork_Type17() {
			return Work_Type17;
		}

		public void setWork_Type17(String work_Type17) {
			Work_Type17 = work_Type17;
		}

		public String getWork_Type18() {
			return Work_Type18;
		}

		public void setWork_Type18(String work_Type18) {
			Work_Type18 = work_Type18;
		}

		public String getWork_Type19() {
			return Work_Type19;
		}

		public void setWork_Type19(String work_Type19) {
			Work_Type19 = work_Type19;
		}

		public String getWork_Type20() {
			return Work_Type20;
		}

		public void setWork_Type20(String work_Type20) {
			Work_Type20 = work_Type20;
		}

		public String getWork_Type21() {
			return Work_Type21;
		}

		public void setWork_Type21(String work_Type21) {
			Work_Type21 = work_Type21;
		}

		public String getWork_Type22() {
			return Work_Type22;
		}

		public void setWork_Type22(String work_Type22) {
			Work_Type22 = work_Type22;
		}

		public String getWork_Type23() {
			return Work_Type23;
		}

		public void setWork_Type23(String work_Type23) {
			Work_Type23 = work_Type23;
		}

		public String getWork_Type24() {
			return Work_Type24;
		}

		public void setWork_Type24(String work_Type24) {
			Work_Type24 = work_Type24;
		}

		public String getWork_Type25() {
			return Work_Type25;
		}

		public void setWork_Type25(String work_Type25) {
			Work_Type25 = work_Type25;
		}

		public String getWork_Type26() {
			return Work_Type26;
		}

		public void setWork_Type26(String work_Type26) {
			Work_Type26 = work_Type26;
		}

		public String getWork_Type27() {
			return Work_Type27;
		}

		public void setWork_Type27(String work_Type27) {
			Work_Type27 = work_Type27;
		}

		public String getWork_Type28() {
			return Work_Type28;
		}

		public void setWork_Type28(String work_Type28) {
			Work_Type28 = work_Type28;
		}

		public String getWork_Type29() {
			return Work_Type29;
		}

		public void setWork_Type29(String work_Type29) {
			Work_Type29 = work_Type29;
		}

		public String getWork_Type30() {
			return Work_Type30;
		}

		public void setWork_Type30(String work_Type30) {
			Work_Type30 = work_Type30;
		}

		public String getWork_Type31() {
			return Work_Type31;
		}

		public void setWork_Type31(String work_Type31) {
			Work_Type31 = work_Type31;
		}

		public String getWork_Type32() {
			return Work_Type32;
		}

		public void setWork_Type32(String work_Type32) {
			Work_Type32 = work_Type32;
		}

		public String getWork_Type33() {
			return Work_Type33;
		}

		public void setWork_Type33(String work_Type33) {
			Work_Type33 = work_Type33;
		}

		public String getWork_Type34() {
			return Work_Type34;
		}

		public void setWork_Type34(String work_Type34) {
			Work_Type34 = work_Type34;
		}

		public String getWork_Type35() {
			return Work_Type35;
		}

		public void setWork_Type35(String work_Type35) {
			Work_Type35 = work_Type35;
		}

		public String getWork_Type36() {
			return Work_Type36;
		}

		public void setWork_Type36(String work_Type36) {
			Work_Type36 = work_Type36;
		}

		public String getWork_Type37() {
			return Work_Type37;
		}

		public void setWork_Type37(String work_Type37) {
			Work_Type37 = work_Type37;
		}

		public String getWork_Type38() {
			return Work_Type38;
		}

		public void setWork_Type38(String work_Type38) {
			Work_Type38 = work_Type38;
		}

		public String getWork_Type39() {
			return Work_Type39;
		}

		public void setWork_Type39(String work_Type39) {
			Work_Type39 = work_Type39;
		}

		public String getWork_Type40() {
			return Work_Type40;
		}

		public void setWork_Type40(String work_Type40) {
			Work_Type40 = work_Type40;
		}

		public String getWork_Type41() {
			return Work_Type41;
		}

		public void setWork_Type41(String work_Type41) {
			Work_Type41 = work_Type41;
		}

		public String getWork_Type42() {
			return Work_Type42;
		}

		public void setWork_Type42(String work_Type42) {
			Work_Type42 = work_Type42;
		}

		public String getWork_Type43() {
			return Work_Type43;
		}

		public void setWork_Type43(String work_Type43) {
			Work_Type43 = work_Type43;
		}

		public String getWork_Type44() {
			return Work_Type44;
		}

		public void setWork_Type44(String work_Type44) {
			Work_Type44 = work_Type44;
		}

		public String getWork_Type45() {
			return Work_Type45;
		}

		public void setWork_Type45(String work_Type45) {
			Work_Type45 = work_Type45;
		}

		public String getWork_Type46() {
			return Work_Type46;
		}

		public void setWork_Type46(String work_Type46) {
			Work_Type46 = work_Type46;
		}

		public String getWork_Type47() {
			return Work_Type47;
		}

		public void setWork_Type47(String work_Type47) {
			Work_Type47 = work_Type47;
		}

		public String getWork_Type48() {
			return Work_Type48;
		}

		public void setWork_Type48(String work_Type48) {
			Work_Type48 = work_Type48;
		}

		public String getWork_Type49() {
			return Work_Type49;
		}

		public void setWork_Type49(String work_Type49) {
			Work_Type49 = work_Type49;
		}

		public String getWork_Type50() {
			return Work_Type50;
		}

		public void setWork_Type50(String work_Type50) {
			Work_Type50 = work_Type50;
		}

		public String getLPN01() {
			return LPN01;
		}

		public void setLPN01(String lPN01) {
			LPN01 = lPN01;
		}

		public String getLPN02() {
			return LPN02;
		}

		public void setLPN02(String lPN02) {
			LPN02 = lPN02;
		}

		public String getLPN03() {
			return LPN03;
		}

		public void setLPN03(String lPN03) {
			LPN03 = lPN03;
		}

		public String getLPN04() {
			return LPN04;
		}

		public void setLPN04(String lPN04) {
			LPN04 = lPN04;
		}

		public String getLPN05() {
			return LPN05;
		}

		public void setLPN05(String lPN05) {
			LPN05 = lPN05;
		}

		public String getLPN06() {
			return LPN06;
		}

		public void setLPN06(String lPN06) {
			LPN06 = lPN06;
		}

		public String getLPN07() {
			return LPN07;
		}

		public void setLPN07(String lPN07) {
			LPN07 = lPN07;
		}

		public String getLPN08() {
			return LPN08;
		}

		public void setLPN08(String lPN08) {
			LPN08 = lPN08;
		}

		public String getLPN09() {
			return LPN09;
		}

		public void setLPN09(String lPN09) {
			LPN09 = lPN09;
		}

		public String getLPN10() {
			return LPN10;
		}

		public void setLPN10(String lPN10) {
			LPN10 = lPN10;
		}

		public String getLPN11() {
			return LPN11;
		}

		public void setLPN11(String lPN11) {
			LPN11 = lPN11;
		}

		public String getLPN12() {
			return LPN12;
		}

		public void setLPN12(String lPN12) {
			LPN12 = lPN12;
		}

		public String getLPN13() {
			return LPN13;
		}

		public void setLPN13(String lPN13) {
			LPN13 = lPN13;
		}

		public String getLPN14() {
			return LPN14;
		}

		public void setLPN14(String lPN14) {
			LPN14 = lPN14;
		}

		public String getLPN15() {
			return LPN15;
		}

		public void setLPN15(String lPN15) {
			LPN15 = lPN15;
		}

		public String getLPN16() {
			return LPN16;
		}

		public void setLPN16(String lPN16) {
			LPN16 = lPN16;
		}

		public String getLPN17() {
			return LPN17;
		}

		public void setLPN17(String lPN17) {
			LPN17 = lPN17;
		}

		public String getLPN18() {
			return LPN18;
		}

		public void setLPN18(String lPN18) {
			LPN18 = lPN18;
		}

		public String getLPN19() {
			return LPN19;
		}

		public void setLPN19(String lPN19) {
			LPN19 = lPN19;
		}

		public String getLPN20() {
			return LPN20;
		}

		public void setLPN20(String lPN20) {
			LPN20 = lPN20;
		}

		public String getLPN21() {
			return LPN21;
		}

		public void setLPN21(String lPN21) {
			LPN21 = lPN21;
		}

		public String getLPN22() {
			return LPN22;
		}

		public void setLPN22(String lPN22) {
			LPN22 = lPN22;
		}

		public String getLPN23() {
			return LPN23;
		}

		public void setLPN23(String lPN23) {
			LPN23 = lPN23;
		}

		public String getLPN24() {
			return LPN24;
		}

		public void setLPN24(String lPN24) {
			LPN24 = lPN24;
		}

		public String getLPN25() {
			return LPN25;
		}

		public void setLPN25(String lPN25) {
			LPN25 = lPN25;
		}

		public String getLPN26() {
			return LPN26;
		}

		public void setLPN26(String lPN26) {
			LPN26 = lPN26;
		}

		public String getLPN27() {
			return LPN27;
		}

		public void setLPN27(String lPN27) {
			LPN27 = lPN27;
		}

		public String getLPN28() {
			return LPN28;
		}

		public void setLPN28(String lPN28) {
			LPN28 = lPN28;
		}

		public String getLPN29() {
			return LPN29;
		}

		public void setLPN29(String lPN29) {
			LPN29 = lPN29;
		}

		public String getLPN30() {
			return LPN30;
		}

		public void setLPN30(String lPN30) {
			LPN30 = lPN30;
		}

		public String getLPN31() {
			return LPN31;
		}

		public void setLPN31(String lPN31) {
			LPN31 = lPN31;
		}

		public String getLPN32() {
			return LPN32;
		}

		public void setLPN32(String lPN32) {
			LPN32 = lPN32;
		}

		public String getLPN33() {
			return LPN33;
		}

		public void setLPN33(String lPN33) {
			LPN33 = lPN33;
		}

		public String getLPN34() {
			return LPN34;
		}

		public void setLPN34(String lPN34) {
			LPN34 = lPN34;
		}

		public String getLPN35() {
			return LPN35;
		}

		public void setLPN35(String lPN35) {
			LPN35 = lPN35;
		}

		public String getLPN36() {
			return LPN36;
		}

		public void setLPN36(String lPN36) {
			LPN36 = lPN36;
		}

		public String getLPN37() {
			return LPN37;
		}

		public void setLPN37(String lPN37) {
			LPN37 = lPN37;
		}

		public String getLPN38() {
			return LPN38;
		}

		public void setLPN38(String lPN38) {
			LPN38 = lPN38;
		}

		public String getLPN39() {
			return LPN39;
		}

		public void setLPN39(String lPN39) {
			LPN39 = lPN39;
		}

		public String getLPN40() {
			return LPN40;
		}

		public void setLPN40(String lPN40) {
			LPN40 = lPN40;
		}

		public String getLPN41() {
			return LPN41;
		}

		public void setLPN41(String lPN41) {
			LPN41 = lPN41;
		}

		public String getLPN42() {
			return LPN42;
		}

		public void setLPN42(String lPN42) {
			LPN42 = lPN42;
		}

		public String getLPN43() {
			return LPN43;
		}

		public void setLPN43(String lPN43) {
			LPN43 = lPN43;
		}

		public String getLPN44() {
			return LPN44;
		}

		public void setLPN44(String lPN44) {
			LPN44 = lPN44;
		}

		public String getLPN45() {
			return LPN45;
		}

		public void setLPN45(String lPN45) {
			LPN45 = lPN45;
		}

		public String getLPN46() {
			return LPN46;
		}

		public void setLPN46(String lPN46) {
			LPN46 = lPN46;
		}

		public String getLPN47() {
			return LPN47;
		}

		public void setLPN47(String lPN47) {
			LPN47 = lPN47;
		}

		public String getLPN48() {
			return LPN48;
		}

		public void setLPN48(String lPN48) {
			LPN48 = lPN48;
		}

		public String getLPN49() {
			return LPN49;
		}

		public void setLPN49(String lPN49) {
			LPN49 = lPN49;
		}

		public String getLPN50() {
			return LPN50;
		}

		public void setLPN50(String lPN50) {
			LPN50 = lPN50;
		}

		public String getContainer_Type01() {
			return Container_Type01;
		}

		public void setContainer_Type01(String container_Type01) {
			Container_Type01 = container_Type01;
		}

		public String getContainer_Type02() {
			return Container_Type02;
		}

		public void setContainer_Type02(String container_Type02) {
			Container_Type02 = container_Type02;
		}

		public String getContainer_Type03() {
			return Container_Type03;
		}

		public void setContainer_Type03(String container_Type03) {
			Container_Type03 = container_Type03;
		}

		public String getContainer_Type04() {
			return Container_Type04;
		}

		public void setContainer_Type04(String container_Type04) {
			Container_Type04 = container_Type04;
		}

		public String getContainer_Type05() {
			return Container_Type05;
		}

		public void setContainer_Type05(String container_Type05) {
			Container_Type05 = container_Type05;
		}

		public String getContainer_Type06() {
			return Container_Type06;
		}

		public void setContainer_Type06(String container_Type06) {
			Container_Type06 = container_Type06;
		}

		public String getContainer_Type07() {
			return Container_Type07;
		}

		public void setContainer_Type07(String container_Type07) {
			Container_Type07 = container_Type07;
		}

		public String getContainer_Type08() {
			return Container_Type08;
		}

		public void setContainer_Type08(String container_Type08) {
			Container_Type08 = container_Type08;
		}

		public String getContainer_Type09() {
			return Container_Type09;
		}

		public void setContainer_Type09(String container_Type09) {
			Container_Type09 = container_Type09;
		}

		public String getContainer_Type10() {
			return Container_Type10;
		}

		public void setContainer_Type10(String container_Type10) {
			Container_Type10 = container_Type10;
		}

		public String getContainer_Type11() {
			return Container_Type11;
		}

		public void setContainer_Type11(String container_Type11) {
			Container_Type11 = container_Type11;
		}

		public String getContainer_Type12() {
			return Container_Type12;
		}

		public void setContainer_Type12(String container_Type12) {
			Container_Type12 = container_Type12;
		}

		public String getContainer_Type13() {
			return Container_Type13;
		}

		public void setContainer_Type13(String container_Type13) {
			Container_Type13 = container_Type13;
		}

		public String getContainer_Type14() {
			return Container_Type14;
		}

		public void setContainer_Type14(String container_Type14) {
			Container_Type14 = container_Type14;
		}

		public String getContainer_Type15() {
			return Container_Type15;
		}

		public void setContainer_Type15(String container_Type15) {
			Container_Type15 = container_Type15;
		}

		public String getContainer_Type16() {
			return Container_Type16;
		}

		public void setContainer_Type16(String container_Type16) {
			Container_Type16 = container_Type16;
		}

		public String getContainer_Type17() {
			return Container_Type17;
		}

		public void setContainer_Type17(String container_Type17) {
			Container_Type17 = container_Type17;
		}

		public String getContainer_Type18() {
			return Container_Type18;
		}

		public void setContainer_Type18(String container_Type18) {
			Container_Type18 = container_Type18;
		}

		public String getContainer_Type19() {
			return Container_Type19;
		}

		public void setContainer_Type19(String container_Type19) {
			Container_Type19 = container_Type19;
		}

		public String getContainer_Type20() {
			return Container_Type20;
		}

		public void setContainer_Type20(String container_Type20) {
			Container_Type20 = container_Type20;
		}

		public String getContainer_Type21() {
			return Container_Type21;
		}

		public void setContainer_Type21(String container_Type21) {
			Container_Type21 = container_Type21;
		}

		public String getContainer_Type22() {
			return Container_Type22;
		}

		public void setContainer_Type22(String container_Type22) {
			Container_Type22 = container_Type22;
		}

		public String getContainer_Type23() {
			return Container_Type23;
		}

		public void setContainer_Type23(String container_Type23) {
			Container_Type23 = container_Type23;
		}

		public String getContainer_Type24() {
			return Container_Type24;
		}

		public void setContainer_Type24(String container_Type24) {
			Container_Type24 = container_Type24;
		}

		public String getContainer_Type25() {
			return Container_Type25;
		}

		public void setContainer_Type25(String container_Type25) {
			Container_Type25 = container_Type25;
		}

		public String getContainer_Type26() {
			return Container_Type26;
		}

		public void setContainer_Type26(String container_Type26) {
			Container_Type26 = container_Type26;
		}

		public String getContainer_Type27() {
			return Container_Type27;
		}

		public void setContainer_Type27(String container_Type27) {
			Container_Type27 = container_Type27;
		}

		public String getContainer_Type28() {
			return Container_Type28;
		}

		public void setContainer_Type28(String container_Type28) {
			Container_Type28 = container_Type28;
		}

		public String getContainer_Type29() {
			return Container_Type29;
		}

		public void setContainer_Type29(String container_Type29) {
			Container_Type29 = container_Type29;
		}

		public String getContainer_Type30() {
			return Container_Type30;
		}

		public void setContainer_Type30(String container_Type30) {
			Container_Type30 = container_Type30;
		}

		public String getContainer_Type31() {
			return Container_Type31;
		}

		public void setContainer_Type31(String container_Type31) {
			Container_Type31 = container_Type31;
		}

		public String getContainer_Type32() {
			return Container_Type32;
		}

		public void setContainer_Type32(String container_Type32) {
			Container_Type32 = container_Type32;
		}

		public String getContainer_Type33() {
			return Container_Type33;
		}

		public void setContainer_Type33(String container_Type33) {
			Container_Type33 = container_Type33;
		}

		public String getContainer_Type34() {
			return Container_Type34;
		}

		public void setContainer_Type34(String container_Type34) {
			Container_Type34 = container_Type34;
		}

		public String getContainer_Type35() {
			return Container_Type35;
		}

		public void setContainer_Type35(String container_Type35) {
			Container_Type35 = container_Type35;
		}

		public String getContainer_Type36() {
			return Container_Type36;
		}

		public void setContainer_Type36(String container_Type36) {
			Container_Type36 = container_Type36;
		}

		public String getContainer_Type37() {
			return Container_Type37;
		}

		public void setContainer_Type37(String container_Type37) {
			Container_Type37 = container_Type37;
		}

		public String getContainer_Type38() {
			return Container_Type38;
		}

		public void setContainer_Type38(String container_Type38) {
			Container_Type38 = container_Type38;
		}

		public String getContainer_Type39() {
			return Container_Type39;
		}

		public void setContainer_Type39(String container_Type39) {
			Container_Type39 = container_Type39;
		}

		public String getContainer_Type40() {
			return Container_Type40;
		}

		public void setContainer_Type40(String container_Type40) {
			Container_Type40 = container_Type40;
		}

		public String getContainer_Type41() {
			return Container_Type41;
		}

		public void setContainer_Type41(String container_Type41) {
			Container_Type41 = container_Type41;
		}

		public String getContainer_Type42() {
			return Container_Type42;
		}

		public void setContainer_Type42(String container_Type42) {
			Container_Type42 = container_Type42;
		}

		public String getContainer_Type43() {
			return Container_Type43;
		}

		public void setContainer_Type43(String container_Type43) {
			Container_Type43 = container_Type43;
		}

		public String getContainer_Type44() {
			return Container_Type44;
		}

		public void setContainer_Type44(String container_Type44) {
			Container_Type44 = container_Type44;
		}

		public String getContainer_Type45() {
			return Container_Type45;
		}

		public void setContainer_Type45(String container_Type45) {
			Container_Type45 = container_Type45;
		}

		public String getContainer_Type46() {
			return Container_Type46;
		}

		public void setContainer_Type46(String container_Type46) {
			Container_Type46 = container_Type46;
		}

		public String getContainer_Type47() {
			return Container_Type47;
		}

		public void setContainer_Type47(String container_Type47) {
			Container_Type47 = container_Type47;
		}

		public String getContainer_Type48() {
			return Container_Type48;
		}

		public void setContainer_Type48(String container_Type48) {
			Container_Type48 = container_Type48;
		}

		public String getContainer_Type49() {
			return Container_Type49;
		}

		public void setContainer_Type49(String container_Type49) {
			Container_Type49 = container_Type49;
		}

		public String getContainer_Type50() {
			return Container_Type50;
		}

		public void setContainer_Type50(String container_Type50) {
			Container_Type50 = container_Type50;
		}

		public String getContainer_ID01() {
			return Container_ID01;
		}

		public void setContainer_ID01(String container_ID01) {
			Container_ID01 = container_ID01;
		}

		public String getContainer_ID02() {
			return Container_ID02;
		}

		public void setContainer_ID02(String container_ID02) {
			Container_ID02 = container_ID02;
		}

		public String getContainer_ID03() {
			return Container_ID03;
		}

		public void setContainer_ID03(String container_ID03) {
			Container_ID03 = container_ID03;
		}

		public String getContainer_ID04() {
			return Container_ID04;
		}

		public void setContainer_ID04(String container_ID04) {
			Container_ID04 = container_ID04;
		}

		public String getContainer_ID05() {
			return Container_ID05;
		}

		public void setContainer_ID05(String container_ID05) {
			Container_ID05 = container_ID05;
		}

		public String getContainer_ID06() {
			return Container_ID06;
		}

		public void setContainer_ID06(String container_ID06) {
			Container_ID06 = container_ID06;
		}

		public String getContainer_ID07() {
			return Container_ID07;
		}

		public void setContainer_ID07(String container_ID07) {
			Container_ID07 = container_ID07;
		}

		public String getContainer_ID08() {
			return Container_ID08;
		}

		public void setContainer_ID08(String container_ID08) {
			Container_ID08 = container_ID08;
		}

		public String getContainer_ID09() {
			return Container_ID09;
		}

		public void setContainer_ID09(String container_ID09) {
			Container_ID09 = container_ID09;
		}

		public String getContainer_ID10() {
			return Container_ID10;
		}

		public void setContainer_ID10(String container_ID10) {
			Container_ID10 = container_ID10;
		}

		public String getContainer_ID11() {
			return Container_ID11;
		}

		public void setContainer_ID11(String container_ID11) {
			Container_ID11 = container_ID11;
		}

		public String getContainer_ID12() {
			return Container_ID12;
		}

		public void setContainer_ID12(String container_ID12) {
			Container_ID12 = container_ID12;
		}

		public String getContainer_ID13() {
			return Container_ID13;
		}

		public void setContainer_ID13(String container_ID13) {
			Container_ID13 = container_ID13;
		}

		public String getContainer_ID14() {
			return Container_ID14;
		}

		public void setContainer_ID14(String container_ID14) {
			Container_ID14 = container_ID14;
		}

		public String getContainer_ID15() {
			return Container_ID15;
		}

		public void setContainer_ID15(String container_ID15) {
			Container_ID15 = container_ID15;
		}

		public String getContainer_ID16() {
			return Container_ID16;
		}

		public void setContainer_ID16(String container_ID16) {
			Container_ID16 = container_ID16;
		}

		public String getContainer_ID17() {
			return Container_ID17;
		}

		public void setContainer_ID17(String container_ID17) {
			Container_ID17 = container_ID17;
		}

		public String getContainer_ID18() {
			return Container_ID18;
		}

		public void setContainer_ID18(String container_ID18) {
			Container_ID18 = container_ID18;
		}

		public String getContainer_ID19() {
			return Container_ID19;
		}

		public void setContainer_ID19(String container_ID19) {
			Container_ID19 = container_ID19;
		}

		public String getContainer_ID20() {
			return Container_ID20;
		}

		public void setContainer_ID20(String container_ID20) {
			Container_ID20 = container_ID20;
		}

		public String getContainer_ID21() {
			return Container_ID21;
		}

		public void setContainer_ID21(String container_ID21) {
			Container_ID21 = container_ID21;
		}

		public String getContainer_ID22() {
			return Container_ID22;
		}

		public void setContainer_ID22(String container_ID22) {
			Container_ID22 = container_ID22;
		}

		public String getContainer_ID23() {
			return Container_ID23;
		}

		public void setContainer_ID23(String container_ID23) {
			Container_ID23 = container_ID23;
		}

		public String getContainer_ID24() {
			return Container_ID24;
		}

		public void setContainer_ID24(String container_ID24) {
			Container_ID24 = container_ID24;
		}

		public String getContainer_ID25() {
			return Container_ID25;
		}

		public void setContainer_ID25(String container_ID25) {
			Container_ID25 = container_ID25;
		}

		public String getContainer_ID26() {
			return Container_ID26;
		}

		public void setContainer_ID26(String container_ID26) {
			Container_ID26 = container_ID26;
		}

		public String getContainer_ID27() {
			return Container_ID27;
		}

		public void setContainer_ID27(String container_ID27) {
			Container_ID27 = container_ID27;
		}

		public String getContainer_ID28() {
			return Container_ID28;
		}

		public void setContainer_ID28(String container_ID28) {
			Container_ID28 = container_ID28;
		}

		public String getContainer_ID29() {
			return Container_ID29;
		}

		public void setContainer_ID29(String container_ID29) {
			Container_ID29 = container_ID29;
		}

		public String getContainer_ID30() {
			return Container_ID30;
		}

		public void setContainer_ID30(String container_ID30) {
			Container_ID30 = container_ID30;
		}

		public String getContainer_ID31() {
			return Container_ID31;
		}

		public void setContainer_ID31(String container_ID31) {
			Container_ID31 = container_ID31;
		}

		public String getContainer_ID32() {
			return Container_ID32;
		}

		public void setContainer_ID32(String container_ID32) {
			Container_ID32 = container_ID32;
		}

		public String getContainer_ID33() {
			return Container_ID33;
		}

		public void setContainer_ID33(String container_ID33) {
			Container_ID33 = container_ID33;
		}

		public String getContainer_ID34() {
			return Container_ID34;
		}

		public void setContainer_ID34(String container_ID34) {
			Container_ID34 = container_ID34;
		}

		public String getContainer_ID35() {
			return Container_ID35;
		}

		public void setContainer_ID35(String container_ID35) {
			Container_ID35 = container_ID35;
		}

		public String getContainer_ID36() {
			return Container_ID36;
		}

		public void setContainer_ID36(String container_ID36) {
			Container_ID36 = container_ID36;
		}

		public String getContainer_ID37() {
			return Container_ID37;
		}

		public void setContainer_ID37(String container_ID37) {
			Container_ID37 = container_ID37;
		}

		public String getContainer_ID38() {
			return Container_ID38;
		}

		public void setContainer_ID38(String container_ID38) {
			Container_ID38 = container_ID38;
		}

		public String getContainer_ID39() {
			return Container_ID39;
		}

		public void setContainer_ID39(String container_ID39) {
			Container_ID39 = container_ID39;
		}

		public String getContainer_ID40() {
			return Container_ID40;
		}

		public void setContainer_ID40(String container_ID40) {
			Container_ID40 = container_ID40;
		}

		public String getContainer_ID41() {
			return Container_ID41;
		}

		public void setContainer_ID41(String container_ID41) {
			Container_ID41 = container_ID41;
		}

		public String getContainer_ID42() {
			return Container_ID42;
		}

		public void setContainer_ID42(String container_ID42) {
			Container_ID42 = container_ID42;
		}

		public String getContainer_ID43() {
			return Container_ID43;
		}

		public void setContainer_ID43(String container_ID43) {
			Container_ID43 = container_ID43;
		}

		public String getContainer_ID44() {
			return Container_ID44;
		}

		public void setContainer_ID44(String container_ID44) {
			Container_ID44 = container_ID44;
		}

		public String getContainer_ID45() {
			return Container_ID45;
		}

		public void setContainer_ID45(String container_ID45) {
			Container_ID45 = container_ID45;
		}

		public String getContainer_ID46() {
			return Container_ID46;
		}

		public void setContainer_ID46(String container_ID46) {
			Container_ID46 = container_ID46;
		}

		public String getContainer_ID47() {
			return Container_ID47;
		}

		public void setContainer_ID47(String container_ID47) {
			Container_ID47 = container_ID47;
		}

		public String getContainer_ID48() {
			return Container_ID48;
		}

		public void setContainer_ID48(String container_ID48) {
			Container_ID48 = container_ID48;
		}

		public String getContainer_ID49() {
			return Container_ID49;
		}

		public void setContainer_ID49(String container_ID49) {
			Container_ID49 = container_ID49;
		}

		public String getContainer_ID50() {
			return Container_ID50;
		}

		public void setContainer_ID50(String container_ID50) {
			Container_ID50 = container_ID50;
		}

		public String getCart_Location_Status01() {
			return Cart_Location_Status01;
		}

		public void setCart_Location_Status01(String cart_Location_Status01) {
			Cart_Location_Status01 = cart_Location_Status01;
		}

		public String getCart_Location_Status02() {
			return Cart_Location_Status02;
		}

		public void setCart_Location_Status02(String cart_Location_Status02) {
			Cart_Location_Status02 = cart_Location_Status02;
		}

		public String getCart_Location_Status03() {
			return Cart_Location_Status03;
		}

		public void setCart_Location_Status03(String cart_Location_Status03) {
			Cart_Location_Status03 = cart_Location_Status03;
		}

		public String getCart_Location_Status04() {
			return Cart_Location_Status04;
		}

		public void setCart_Location_Status04(String cart_Location_Status04) {
			Cart_Location_Status04 = cart_Location_Status04;
		}

		public String getCart_Location_Status05() {
			return Cart_Location_Status05;
		}

		public void setCart_Location_Status05(String cart_Location_Status05) {
			Cart_Location_Status05 = cart_Location_Status05;
		}

		public String getCart_Location_Status06() {
			return Cart_Location_Status06;
		}

		public void setCart_Location_Status06(String cart_Location_Status06) {
			Cart_Location_Status06 = cart_Location_Status06;
		}

		public String getCart_Location_Status07() {
			return Cart_Location_Status07;
		}

		public void setCart_Location_Status07(String cart_Location_Status07) {
			Cart_Location_Status07 = cart_Location_Status07;
		}

		public String getCart_Location_Status08() {
			return Cart_Location_Status08;
		}

		public void setCart_Location_Status08(String cart_Location_Status08) {
			Cart_Location_Status08 = cart_Location_Status08;
		}

		public String getCart_Location_Status09() {
			return Cart_Location_Status09;
		}

		public void setCart_Location_Status09(String cart_Location_Status09) {
			Cart_Location_Status09 = cart_Location_Status09;
		}

		public String getCart_Location_Status10() {
			return Cart_Location_Status10;
		}

		public void setCart_Location_Status10(String cart_Location_Status10) {
			Cart_Location_Status10 = cart_Location_Status10;
		}

		public String getCart_Location_Status11() {
			return Cart_Location_Status11;
		}

		public void setCart_Location_Status11(String cart_Location_Status11) {
			Cart_Location_Status11 = cart_Location_Status11;
		}

		public String getCart_Location_Status12() {
			return Cart_Location_Status12;
		}

		public void setCart_Location_Status12(String cart_Location_Status12) {
			Cart_Location_Status12 = cart_Location_Status12;
		}

		public String getCart_Location_Status13() {
			return Cart_Location_Status13;
		}

		public void setCart_Location_Status13(String cart_Location_Status13) {
			Cart_Location_Status13 = cart_Location_Status13;
		}

		public String getCart_Location_Status14() {
			return Cart_Location_Status14;
		}

		public void setCart_Location_Status14(String cart_Location_Status14) {
			Cart_Location_Status14 = cart_Location_Status14;
		}

		public String getCart_Location_Status15() {
			return Cart_Location_Status15;
		}

		public void setCart_Location_Status15(String cart_Location_Status15) {
			Cart_Location_Status15 = cart_Location_Status15;
		}

		public String getCart_Location_Status16() {
			return Cart_Location_Status16;
		}

		public void setCart_Location_Status16(String cart_Location_Status16) {
			Cart_Location_Status16 = cart_Location_Status16;
		}

		public String getCart_Location_Status17() {
			return Cart_Location_Status17;
		}

		public void setCart_Location_Status17(String cart_Location_Status17) {
			Cart_Location_Status17 = cart_Location_Status17;
		}

		public String getCart_Location_Status18() {
			return Cart_Location_Status18;
		}

		public void setCart_Location_Status18(String cart_Location_Status18) {
			Cart_Location_Status18 = cart_Location_Status18;
		}

		public String getCart_Location_Status19() {
			return Cart_Location_Status19;
		}

		public void setCart_Location_Status19(String cart_Location_Status19) {
			Cart_Location_Status19 = cart_Location_Status19;
		}

		public String getCart_Location_Status20() {
			return Cart_Location_Status20;
		}

		public void setCart_Location_Status20(String cart_Location_Status20) {
			Cart_Location_Status20 = cart_Location_Status20;
		}

		public String getCart_Location_Status21() {
			return Cart_Location_Status21;
		}

		public void setCart_Location_Status21(String cart_Location_Status21) {
			Cart_Location_Status21 = cart_Location_Status21;
		}

		public String getCart_Location_Status22() {
			return Cart_Location_Status22;
		}

		public void setCart_Location_Status22(String cart_Location_Status22) {
			Cart_Location_Status22 = cart_Location_Status22;
		}

		public String getCart_Location_Status23() {
			return Cart_Location_Status23;
		}

		public void setCart_Location_Status23(String cart_Location_Status23) {
			Cart_Location_Status23 = cart_Location_Status23;
		}

		public String getCart_Location_Status24() {
			return Cart_Location_Status24;
		}

		public void setCart_Location_Status24(String cart_Location_Status24) {
			Cart_Location_Status24 = cart_Location_Status24;
		}

		public String getCart_Location_Status25() {
			return Cart_Location_Status25;
		}

		public void setCart_Location_Status25(String cart_Location_Status25) {
			Cart_Location_Status25 = cart_Location_Status25;
		}

		public String getCart_Location_Status26() {
			return Cart_Location_Status26;
		}

		public void setCart_Location_Status26(String cart_Location_Status26) {
			Cart_Location_Status26 = cart_Location_Status26;
		}

		public String getCart_Location_Status27() {
			return Cart_Location_Status27;
		}

		public void setCart_Location_Status27(String cart_Location_Status27) {
			Cart_Location_Status27 = cart_Location_Status27;
		}

		public String getCart_Location_Status28() {
			return Cart_Location_Status28;
		}

		public void setCart_Location_Status28(String cart_Location_Status28) {
			Cart_Location_Status28 = cart_Location_Status28;
		}

		public String getCart_Location_Status29() {
			return Cart_Location_Status29;
		}

		public void setCart_Location_Status29(String cart_Location_Status29) {
			Cart_Location_Status29 = cart_Location_Status29;
		}

		public String getCart_Location_Status30() {
			return Cart_Location_Status30;
		}

		public void setCart_Location_Status30(String cart_Location_Status30) {
			Cart_Location_Status30 = cart_Location_Status30;
		}

		public String getCart_Location_Status31() {
			return Cart_Location_Status31;
		}

		public void setCart_Location_Status31(String cart_Location_Status31) {
			Cart_Location_Status31 = cart_Location_Status31;
		}

		public String getCart_Location_Status32() {
			return Cart_Location_Status32;
		}

		public void setCart_Location_Status32(String cart_Location_Status32) {
			Cart_Location_Status32 = cart_Location_Status32;
		}

		public String getCart_Location_Status33() {
			return Cart_Location_Status33;
		}

		public void setCart_Location_Status33(String cart_Location_Status33) {
			Cart_Location_Status33 = cart_Location_Status33;
		}

		public String getCart_Location_Status34() {
			return Cart_Location_Status34;
		}

		public void setCart_Location_Status34(String cart_Location_Status34) {
			Cart_Location_Status34 = cart_Location_Status34;
		}

		public String getCart_Location_Status35() {
			return Cart_Location_Status35;
		}

		public void setCart_Location_Status35(String cart_Location_Status35) {
			Cart_Location_Status35 = cart_Location_Status35;
		}

		public String getCart_Location_Status36() {
			return Cart_Location_Status36;
		}

		public void setCart_Location_Status36(String cart_Location_Status36) {
			Cart_Location_Status36 = cart_Location_Status36;
		}

		public String getCart_Location_Status37() {
			return Cart_Location_Status37;
		}

		public void setCart_Location_Status37(String cart_Location_Status37) {
			Cart_Location_Status37 = cart_Location_Status37;
		}

		public String getCart_Location_Status38() {
			return Cart_Location_Status38;
		}

		public void setCart_Location_Status38(String cart_Location_Status38) {
			Cart_Location_Status38 = cart_Location_Status38;
		}

		public String getCart_Location_Status39() {
			return Cart_Location_Status39;
		}

		public void setCart_Location_Status39(String cart_Location_Status39) {
			Cart_Location_Status39 = cart_Location_Status39;
		}

		public String getCart_Location_Status40() {
			return Cart_Location_Status40;
		}

		public void setCart_Location_Status40(String cart_Location_Status40) {
			Cart_Location_Status40 = cart_Location_Status40;
		}

		public String getCart_Location_Status41() {
			return Cart_Location_Status41;
		}

		public void setCart_Location_Status41(String cart_Location_Status41) {
			Cart_Location_Status41 = cart_Location_Status41;
		}

		public String getCart_Location_Status42() {
			return Cart_Location_Status42;
		}

		public void setCart_Location_Status42(String cart_Location_Status42) {
			Cart_Location_Status42 = cart_Location_Status42;
		}

		public String getCart_Location_Status43() {
			return Cart_Location_Status43;
		}

		public void setCart_Location_Status43(String cart_Location_Status43) {
			Cart_Location_Status43 = cart_Location_Status43;
		}

		public String getCart_Location_Status44() {
			return Cart_Location_Status44;
		}

		public void setCart_Location_Status44(String cart_Location_Status44) {
			Cart_Location_Status44 = cart_Location_Status44;
		}

		public String getCart_Location_Status45() {
			return Cart_Location_Status45;
		}

		public void setCart_Location_Status45(String cart_Location_Status45) {
			Cart_Location_Status45 = cart_Location_Status45;
		}

		public String getCart_Location_Status46() {
			return Cart_Location_Status46;
		}

		public void setCart_Location_Status46(String cart_Location_Status46) {
			Cart_Location_Status46 = cart_Location_Status46;
		}

		public String getCart_Location_Status47() {
			return Cart_Location_Status47;
		}

		public void setCart_Location_Status47(String cart_Location_Status47) {
			Cart_Location_Status47 = cart_Location_Status47;
		}

		public String getCart_Location_Status48() {
			return Cart_Location_Status48;
		}

		public void setCart_Location_Status48(String cart_Location_Status48) {
			Cart_Location_Status48 = cart_Location_Status48;
		}

		public String getCart_Location_Status49() {
			return Cart_Location_Status49;
		}

		public void setCart_Location_Status49(String cart_Location_Status49) {
			Cart_Location_Status49 = cart_Location_Status49;
		}

		public String getCart_Location_Status50() {
			return Cart_Location_Status50;
		}

		public void setCart_Location_Status50(String cart_Location_Status50) {
			Cart_Location_Status50 = cart_Location_Status50;
		}

		public String getStatus() {
			return Status;
		}

		public void setStatus(String status) {
			Status = status;
		}

		public Date getLast_Activity_Date() {
			return Last_Activity_Date;
		}

		public void setLast_Activity_Date(Date last_Activity_Date) {
			Last_Activity_Date = last_Activity_Date;
		}

		public String getLast_Activity_Team_Member() {
			return Last_Activity_Team_Member;
		}

		public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
			Last_Activity_Team_Member = last_Activity_Team_Member;
		}

		

	
}
