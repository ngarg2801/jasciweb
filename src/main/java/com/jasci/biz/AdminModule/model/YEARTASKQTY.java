/*


Created by: Aakash Bishnoi
Description   pojo class of YEAR_TASK_QTY in which getter and setter methods 
Created On:Apr 1 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Year_Task_Qty)
public class YEARTASKQTY {

	
	
	@Column(name=GLOBALCONSTANT.DataBase_Month01Qty)
	private double Month01Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month02Qty)
	private double Month02Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month03Qty)
	private double Month03Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month04Qty)
	private double Month04Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month05Qty)
	private double Month05Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month06Qty)
	private double Month06Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month07Qty)
	private double Month07Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month08Qty)
	private double Month08Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month09Qty)
	private double Month09Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month10Qty)
	private double Month10Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month11Qty)
	private double Month11Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Month12Qty)
	private double Month12Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week01Qty)
	private double Week01Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week02Qty)
	private double Week02Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week03Qty)
	private double Week03Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week04Qty)
	private double Week04Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week05Qty)
	private double Week05Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week06Qty)
	private double Week06Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week07Qty)
	private double Week07Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week08Qty)
	private double Week08Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week09Qty)
	private double Week09Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week10Qty)
	private double Week10Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week11Qty)
	private double Week11Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week12Qty)
	private double Week12Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week13Qty)
	private double Week13Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week14Qty)
	private double Week14Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week15Qty)
	private double Week15Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week16Qty)
	private double Week16Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week17Qty)
	private double Week17Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week18Qty)
	private double Week18Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week19Qty)
	private double Week19Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week20Qty)
	private double Week20Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week21Qty)
	private double Week21Qty;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Week22Qty)
	private double Week22Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week23Qty)
	private double Week23Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week24Qty)
	private double Week24Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week25Qty)
	private double Week25Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week26Qty)
	private double Week26Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week27Qty)
	private double Week27Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week28Qty)
	private double Week28Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week29Qty)
	private double Week29Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week30Qty)
	private double Week30Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week31Qty)
	private double Week31Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week32Qty)
	private double Week32Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week33Qty)
	private double Week33Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week34Qty)
	private double Week34Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week35Qty)
	private double Week35Qty;
		
	@Column(name=GLOBALCONSTANT.DataBase_Week36Qty)
	private double Week36Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week37Qty)
	private double Week37Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week38Qty)
	private double Week38Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week39Qty)
	private double Week39Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week40Qty)
	private double Week40Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week41Qty)
	private double Week41Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week42Qty)
	private double Week42Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week43Qty)
	private double Week43Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week44Qty)
	private double Week44Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week45Qty)
	private double Week45Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week46Qty)
	private double Week46Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week47Qty)
	private double Week47Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week48Qty)
	private double Week48Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week49Qty)
	private double Week49Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week50Qty)
	private double Week50Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week51Qty)
	private double Week51Qty;
	
	@Column(name=GLOBALCONSTANT.DataBase_Week52Qty)
	private double Week52Qty;

	@Column(name=GLOBALCONSTANT.DataBase_LAST_ACTIVITY_DATE)
	private Date Last_Activity_Date;
	
	@Column(name=GLOBALCONSTANT.DataBase_LAST_ACTIVITY_TEAM_MEMBER)
	private String Last_Activity_Team_Member;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Task)
	private String Last_Activity_Task;

	public double getMonth01Qty() {
		return Month01Qty;
	}

	public void setMonth01Qty(double month01Qty) {
		Month01Qty = month01Qty;
	}

	public double getMonth02Qty() {
		return Month02Qty;
	}

	public void setMonth02Qty(double month02Qty) {
		Month02Qty = month02Qty;
	}

	public double getMonth03Qty() {
		return Month03Qty;
	}

	public void setMonth03Qty(double month03Qty) {
		Month03Qty = month03Qty;
	}

	public double getMonth04Qty() {
		return Month04Qty;
	}

	public void setMonth04Qty(double month04Qty) {
		Month04Qty = month04Qty;
	}

	public double getMonth05Qty() {
		return Month05Qty;
	}

	public void setMonth05Qty(double month05Qty) {
		Month05Qty = month05Qty;
	}

	public double getMonth06Qty() {
		return Month06Qty;
	}

	public void setMonth06Qty(double month06Qty) {
		Month06Qty = month06Qty;
	}

	public double getMonth07Qty() {
		return Month07Qty;
	}

	public void setMonth07Qty(double month07Qty) {
		Month07Qty = month07Qty;
	}

	public double getMonth08Qty() {
		return Month08Qty;
	}

	public void setMonth08Qty(double month08Qty) {
		Month08Qty = month08Qty;
	}

	public double getMonth09Qty() {
		return Month09Qty;
	}

	public void setMonth09Qty(double month09Qty) {
		Month09Qty = month09Qty;
	}

	public double getMonth10Qty() {
		return Month10Qty;
	}

	public void setMonth10Qty(double month10Qty) {
		Month10Qty = month10Qty;
	}

	public double getMonth11Qty() {
		return Month11Qty;
	}

	public void setMonth11Qty(double month11Qty) {
		Month11Qty = month11Qty;
	}

	public double getMonth12Qty() {
		return Month12Qty;
	}

	public void setMonth12Qty(double month12Qty) {
		Month12Qty = month12Qty;
	}

	public double getWeek01Qty() {
		return Week01Qty;
	}

	public void setWeek01Qty(double week01Qty) {
		Week01Qty = week01Qty;
	}

	public double getWeek02Qty() {
		return Week02Qty;
	}

	public void setWeek02Qty(double week02Qty) {
		Week02Qty = week02Qty;
	}

	public double getWeek03Qty() {
		return Week03Qty;
	}

	public void setWeek03Qty(double week03Qty) {
		Week03Qty = week03Qty;
	}

	public double getWeek04Qty() {
		return Week04Qty;
	}

	public void setWeek04Qty(double week04Qty) {
		Week04Qty = week04Qty;
	}

	public double getWeek05Qty() {
		return Week05Qty;
	}

	public void setWeek05Qty(double week05Qty) {
		Week05Qty = week05Qty;
	}

	public double getWeek06Qty() {
		return Week06Qty;
	}

	public void setWeek06Qty(double week06Qty) {
		Week06Qty = week06Qty;
	}

	public double getWeek07Qty() {
		return Week07Qty;
	}

	public void setWeek07Qty(double week07Qty) {
		Week07Qty = week07Qty;
	}

	public double getWeek08Qty() {
		return Week08Qty;
	}

	public void setWeek08Qty(double week08Qty) {
		Week08Qty = week08Qty;
	}

	public double getWeek09Qty() {
		return Week09Qty;
	}

	public void setWeek09Qty(double week09Qty) {
		Week09Qty = week09Qty;
	}

	public double getWeek10Qty() {
		return Week10Qty;
	}

	public void setWeek10Qty(double week10Qty) {
		Week10Qty = week10Qty;
	}

	public double getWeek11Qty() {
		return Week11Qty;
	}

	public void setWeek11Qty(double week11Qty) {
		Week11Qty = week11Qty;
	}

	public double getWeek12Qty() {
		return Week12Qty;
	}

	public void setWeek12Qty(double week12Qty) {
		Week12Qty = week12Qty;
	}

	public double getWeek13Qty() {
		return Week13Qty;
	}

	public void setWeek13Qty(double week13Qty) {
		Week13Qty = week13Qty;
	}

	public double getWeek14Qty() {
		return Week14Qty;
	}

	public void setWeek14Qty(double week14Qty) {
		Week14Qty = week14Qty;
	}

	public double getWeek15Qty() {
		return Week15Qty;
	}

	public void setWeek15Qty(double week15Qty) {
		Week15Qty = week15Qty;
	}

	public double getWeek16Qty() {
		return Week16Qty;
	}

	public void setWeek16Qty(double week16Qty) {
		Week16Qty = week16Qty;
	}

	public double getWeek17Qty() {
		return Week17Qty;
	}

	public void setWeek17Qty(double week17Qty) {
		Week17Qty = week17Qty;
	}

	public double getWeek18Qty() {
		return Week18Qty;
	}

	public void setWeek18Qty(double week18Qty) {
		Week18Qty = week18Qty;
	}

	public double getWeek19Qty() {
		return Week19Qty;
	}

	public void setWeek19Qty(double week19Qty) {
		Week19Qty = week19Qty;
	}

	public double getWeek20Qty() {
		return Week20Qty;
	}

	public void setWeek20Qty(double week20Qty) {
		Week20Qty = week20Qty;
	}

	public double getWeek21Qty() {
		return Week21Qty;
	}

	public void setWeek21Qty(double week21Qty) {
		Week21Qty = week21Qty;
	}

	public double getWeek22Qty() {
		return Week22Qty;
	}

	public void setWeek22Qty(double week22Qty) {
		Week22Qty = week22Qty;
	}

	public double getWeek23Qty() {
		return Week23Qty;
	}

	public void setWeek23Qty(double week23Qty) {
		Week23Qty = week23Qty;
	}

	public double getWeek24Qty() {
		return Week24Qty;
	}

	public void setWeek24Qty(double week24Qty) {
		Week24Qty = week24Qty;
	}

	public double getWeek25Qty() {
		return Week25Qty;
	}

	public void setWeek25Qty(double week25Qty) {
		Week25Qty = week25Qty;
	}

	public double getWeek26Qty() {
		return Week26Qty;
	}

	public void setWeek26Qty(double week26Qty) {
		Week26Qty = week26Qty;
	}

	public double getWeek27Qty() {
		return Week27Qty;
	}

	public void setWeek27Qty(double week27Qty) {
		Week27Qty = week27Qty;
	}

	public double getWeek28Qty() {
		return Week28Qty;
	}

	public void setWeek28Qty(double week28Qty) {
		Week28Qty = week28Qty;
	}

	public double getWeek29Qty() {
		return Week29Qty;
	}

	public void setWeek29Qty(double week29Qty) {
		Week29Qty = week29Qty;
	}

	public double getWeek30Qty() {
		return Week30Qty;
	}

	public void setWeek30Qty(double week30Qty) {
		Week30Qty = week30Qty;
	}

	public double getWeek31Qty() {
		return Week31Qty;
	}

	public void setWeek31Qty(double week31Qty) {
		Week31Qty = week31Qty;
	}

	public double getWeek32Qty() {
		return Week32Qty;
	}

	public void setWeek32Qty(double week32Qty) {
		Week32Qty = week32Qty;
	}

	public double getWeek33Qty() {
		return Week33Qty;
	}

	public void setWeek33Qty(double week33Qty) {
		Week33Qty = week33Qty;
	}

	public double getWeek34Qty() {
		return Week34Qty;
	}

	public void setWeek34Qty(double week34Qty) {
		Week34Qty = week34Qty;
	}

	public double getWeek35Qty() {
		return Week35Qty;
	}

	public void setWeek35Qty(double week35Qty) {
		Week35Qty = week35Qty;
	}

	public double getWeek36Qty() {
		return Week36Qty;
	}

	public void setWeek36Qty(double week36Qty) {
		Week36Qty = week36Qty;
	}

	public double getWeek37Qty() {
		return Week37Qty;
	}

	public void setWeek37Qty(double week37Qty) {
		Week37Qty = week37Qty;
	}

	public double getWeek38Qty() {
		return Week38Qty;
	}

	public void setWeek38Qty(double week38Qty) {
		Week38Qty = week38Qty;
	}

	public double getWeek39Qty() {
		return Week39Qty;
	}

	public void setWeek39Qty(double week39Qty) {
		Week39Qty = week39Qty;
	}

	public double getWeek40Qty() {
		return Week40Qty;
	}

	public void setWeek40Qty(double week40Qty) {
		Week40Qty = week40Qty;
	}

	public double getWeek41Qty() {
		return Week41Qty;
	}

	public void setWeek41Qty(double week41Qty) {
		Week41Qty = week41Qty;
	}

	public double getWeek42Qty() {
		return Week42Qty;
	}

	public void setWeek42Qty(double week42Qty) {
		Week42Qty = week42Qty;
	}

	public double getWeek43Qty() {
		return Week43Qty;
	}

	public void setWeek43Qty(double week43Qty) {
		Week43Qty = week43Qty;
	}

	public double getWeek44Qty() {
		return Week44Qty;
	}

	public void setWeek44Qty(double week44Qty) {
		Week44Qty = week44Qty;
	}

	public double getWeek45Qty() {
		return Week45Qty;
	}

	public void setWeek45Qty(double week45Qty) {
		Week45Qty = week45Qty;
	}

	public double getWeek46Qty() {
		return Week46Qty;
	}

	public void setWeek46Qty(double week46Qty) {
		Week46Qty = week46Qty;
	}

	public double getWeek47Qty() {
		return Week47Qty;
	}

	public void setWeek47Qty(double week47Qty) {
		Week47Qty = week47Qty;
	}

	public double getWeek48Qty() {
		return Week48Qty;
	}

	public void setWeek48Qty(double week48Qty) {
		Week48Qty = week48Qty;
	}

	public double getWeek49Qty() {
		return Week49Qty;
	}

	public void setWeek49Qty(double week49Qty) {
		Week49Qty = week49Qty;
	}

	public double getWeek50Qty() {
		return Week50Qty;
	}

	public void setWeek50Qty(double week50Qty) {
		Week50Qty = week50Qty;
	}

	public double getWeek51Qty() {
		return Week51Qty;
	}

	public void setWeek51Qty(double week51Qty) {
		Week51Qty = week51Qty;
	}

	public double getWeek52Qty() {
		return Week52Qty;
	}

	public void setWeek52Qty(double week52Qty) {
		Week52Qty = week52Qty;
	}

	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}

	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}

	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}

	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}

	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}

	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}
	
	
}
