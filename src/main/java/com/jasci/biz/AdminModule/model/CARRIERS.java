/**
 * 
 * @ file_name: CARRIERS
 * @Developed by:Pradeep kumar
 *@Created Date:1 April 2015
 *Purpose : Pojo Used for Mapping with table CARRIERS
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Carriers)

public class CARRIERS {

	@EmbeddedId
	private CARRIERSPK Id;
	
	@Column (name=GLOBALCONSTANT.DataBase_Carrier_Process)
	private String CARRIER_PROCESS;
	
	@Column (name=GLOBALCONSTANT.DataBase_Type_Of_Carrier)
	 private String TYPE_OF_CARRIER;
	
	@Column (name=GLOBALCONSTANT.DataBase_Name20 )
	private String NAME20;
	
	@Column (name=GLOBALCONSTANT.DataBase_Name50)
	 private String NAME50;
	
	@Column (name=GLOBALCONSTANT.DataBase_Address_Line1)
	 private String ADDRESS_LINE1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Address_Line2)
	 private String ADDRESS_LINE2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Address_Line3)
	 private String ADDRESS_LINE3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Address_Line4)
	 private String ADDRESS_LINE4;
	
	@Column (name=GLOBALCONSTANT.DataBase_City)
	 private String CITY;
	
	@Column (name=GLOBALCONSTANT.DataBase_State_Code)
	 private String STATE_CODE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Country_Code)
	 private String COUNTRY_CODE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Zip_Code)
	 private String ZIP_CODE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Name1)
	 private String CONTACT_NAME1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Phone1)
	 private String CONTACT_PHONE1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Extension1)
	 private String CONTACT_EXTENSION1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Cell1)
	 private String CONTACT_CELL1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Fax1)
	 private String CONTACT_FAX1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Email1)
	 private String CONTACT_EMAIL1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Name2)
	 private String CONTACT_NAME2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Phone2)
	 private String CONTACT_PHONE2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Extension2)
	 private String CONTACT_EXTENSION2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Cell2)
	 private String CONTACT_CELL2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Fax2)
	 private String CONTACT_FAX2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Email2)
	 private String CONTACT_EMAIL2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Name3)
	 private String CONTACT_NAME3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Phone3)
	 private String CONTACT_PHONE3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Extension3)
	 private String CONTACT_EXTENSION3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Cell3)
	 private String CONTACT_CELL3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Fax3)
	 private String CONTACT_FAX3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Email3)
	 private String CONTACT_EMAIL3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Name4)
	 private String CONTACT_NAME4;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Phone4)
	 private String CONTACT_PHONE4;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Extension4)
	 private String CONTACT_EXTENSION4;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Cell4)
	 private String CONTACT_CELL4;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Fax4)
	 private String CONTACT_FAX4;
	
	@Column (name=GLOBALCONSTANT.DataBase_Contact_Email4)
	 private String CONTACT_EMAIL4;
	
	@Column (name=GLOBALCONSTANT.DataBase_Main_Fax)
	 private String MAIN_FAX;
	
	@Column (name=GLOBALCONSTANT.DataBase_Main_Email)
	 private String MAIN_EMAIL;
	
	@Column (name=GLOBALCONSTANT.DataBase_Handling_Charge_Percent)
	 private double HANDLING_CHARGE_PERCENT;
	
	@Column (name=GLOBALCONSTANT.DataBase_Discount_Percent)
	 private double DISCOUNT_PERCENT;
	
	@Column (name=GLOBALCONSTANT.DataBase_Minimal_Charge )
	private double MINIMAL_CHARGE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Broker_Name)
	 private String BROKER_NAME;
	
	@Column (name=GLOBALCONSTANT.DataBase_Tractable)
	 private char TRACTABLE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Ship_To_Type )
	private String SHIP_TO_TYPE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Ship_To_Pobox )
	private char SHIP_TO_POBOX;
	
	@Column (name=GLOBALCONSTANT.DataBase_Returns_Carrier)
	 private char RETURNS_CARRIER;
	
	@Column (name=GLOBALCONSTANT.DataBase_Ship_To_Agent )
	private char SHIP_TO_AGENT;
	
	@Column (name=GLOBALCONSTANT.DataBase_Pro_Number_Start) 
	private long PRO_NUMBER_START 
	;
	@Column (name=GLOBALCONSTANT.DataBase_Pro_Number_End )
	private long PRO_NUMBER_END ;
	
	@Column (name=GLOBALCONSTANT.DataBase_Pro_Number_Last_Used )
	private long PRO_NUMBER_LAST_USED;
	
	@Column (name=GLOBALCONSTANT.DataBase_Pro_Number_Remaining_Warring )
	private long PRO_NUMBER_REMAINING_WARRING;
	
	@Column (name=GLOBALCONSTANT.DataBase_Pro_Num_Remaining_Warn_Email )
	private String PRO_NUM_REMAINING_WARN_EMAIL;
	
	@Column (name=GLOBALCONSTANT.DataBase_Pro_Number_Prefix )
	private long PRO_NUMBER_PREFIX;
	
	@Column (name=GLOBALCONSTANT.DataBase_Pro_Number_Check_Digit_Type)
	 private String PRO_NUMBER_CHECK_DIGIT_TYPE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Accepts_Electronic_Manifies)
	 private char ACCEPTS_ELECTRONIC_MANIFIES;
	
	@Column (name=GLOBALCONSTANT.DataBase_Carrer_Interface)
	 private String CARRER_INTERFACE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Name20 )
	private String PAYMENT_NAME20;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Name50)
	 private String PAYMENT_NAME50;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Address_Line1) 
	private String PAYMENT_ADDRESS_LINE1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Address_Line2)
	 private String PAYMENT_ADDRESS_LINE2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Address_Line3)
	 private String PAYMENT_ADDRESS_LINE3;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Address_Line4)
	 private String PAYMENT_ADDRESS_LINE4;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_City)
	 private String PAYMENT_CITY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_State_Code )
	private String PAYMENT_STATE_CODE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Country_Code)
	 private String PAYMENT_COUNTRY_CODE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Zip_Code )
	private String PAYMENT_ZIP_CODE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Name1)
	 private String PAYMENT_CONTACT_NAME1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Phone1)
	 private String PAYMENT_CONTACT_PHONE1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Extension1)
	 private String PAYMENT_CONTACT_EXTENSION1;
	
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Cell1)
	 private String PAYMENT_CONTACT_CELL1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Fax1)
	 private String PAYMENT_CONTACT_FAX1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Email1)
	 private String PAYMENT_CONTACT_EMAIL1;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Name2)
	 private String PAYMENT_CONTACT_NAME2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Phone2)
	 private String PAYMENT_CONTACT_PHONE2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Extension2)
	 private String PAYMENT_CONTACT_EXTENSION2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Cell2)
	 private String PAYMENT_CONTACT_CELL2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Contact_Fax2)
	 private String PAYMENT_CONTACT_FAX2;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Routing)
	 private String PAYMENT_ROUTING;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Type)
	 private String PAYMENT_TYPE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Payment_Terms)
	 private long PAYMENT_TERMS;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	 private Date LAST_ACTIVITY_DATE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	 private String LAST_ACTIVITY_TEAM_MEMBER;

	public CARRIERSPK getId() {
		return Id;
	}

	public void setId(CARRIERSPK id) {
		Id = id;
	}

	public String getCARRIER_PROCESS() {
		return CARRIER_PROCESS;
	}

	public void setCARRIER_PROCESS(String cARRIER_PROCESS) {
		CARRIER_PROCESS = cARRIER_PROCESS;
	}

	public String getTYPE_OF_CARRIER() {
		return TYPE_OF_CARRIER;
	}

	public void setTYPE_OF_CARRIER(String tYPE_OF_CARRIER) {
		TYPE_OF_CARRIER = tYPE_OF_CARRIER;
	}

	public String getNAME20() {
		return NAME20;
	}

	public void setNAME20(String nAME20) {
		NAME20 = nAME20;
	}

	public String getNAME50() {
		return NAME50;
	}

	public void setNAME50(String nAME50) {
		NAME50 = nAME50;
	}

	public String getADDRESS_LINE1() {
		return ADDRESS_LINE1;
	}

	public void setADDRESS_LINE1(String aDDRESS_LINE1) {
		ADDRESS_LINE1 = aDDRESS_LINE1;
	}

	public String getADDRESS_LINE2() {
		return ADDRESS_LINE2;
	}

	public void setADDRESS_LINE2(String aDDRESS_LINE2) {
		ADDRESS_LINE2 = aDDRESS_LINE2;
	}

	public String getADDRESS_LINE3() {
		return ADDRESS_LINE3;
	}

	public void setADDRESS_LINE3(String aDDRESS_LINE3) {
		ADDRESS_LINE3 = aDDRESS_LINE3;
	}

	public String getADDRESS_LINE4() {
		return ADDRESS_LINE4;
	}

	public void setADDRESS_LINE4(String aDDRESS_LINE4) {
		ADDRESS_LINE4 = aDDRESS_LINE4;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String cITY) {
		CITY = cITY;
	}

	public String getSTATE_CODE() {
		return STATE_CODE;
	}

	public void setSTATE_CODE(String sTATE_CODE) {
		STATE_CODE = sTATE_CODE;
	}

	public String getCOUNTRY_CODE() {
		return COUNTRY_CODE;
	}

	public void setCOUNTRY_CODE(String cOUNTRY_CODE) {
		COUNTRY_CODE = cOUNTRY_CODE;
	}

	public String getZIP_CODE() {
		return ZIP_CODE;
	}

	public void setZIP_CODE(String zIP_CODE) {
		ZIP_CODE = zIP_CODE;
	}

	public String getCONTACT_NAME1() {
		return CONTACT_NAME1;
	}

	public void setCONTACT_NAME1(String cONTACT_NAME1) {
		CONTACT_NAME1 = cONTACT_NAME1;
	}

	public String getCONTACT_PHONE1() {
		return CONTACT_PHONE1;
	}

	public void setCONTACT_PHONE1(String cONTACT_PHONE1) {
		CONTACT_PHONE1 = cONTACT_PHONE1;
	}

	public String getCONTACT_EXTENSION1() {
		return CONTACT_EXTENSION1;
	}

	public void setCONTACT_EXTENSION1(String cONTACT_EXTENSION1) {
		CONTACT_EXTENSION1 = cONTACT_EXTENSION1;
	}

	public String getCONTACT_CELL1() {
		return CONTACT_CELL1;
	}

	public void setCONTACT_CELL1(String cONTACT_CELL1) {
		CONTACT_CELL1 = cONTACT_CELL1;
	}

	public String getCONTACT_FAX1() {
		return CONTACT_FAX1;
	}

	public void setCONTACT_FAX1(String cONTACT_FAX1) {
		CONTACT_FAX1 = cONTACT_FAX1;
	}

	public String getCONTACT_EMAIL1() {
		return CONTACT_EMAIL1;
	}

	public void setCONTACT_EMAIL1(String cONTACT_EMAIL1) {
		CONTACT_EMAIL1 = cONTACT_EMAIL1;
	}

	public String getCONTACT_NAME2() {
		return CONTACT_NAME2;
	}

	public void setCONTACT_NAME2(String cONTACT_NAME2) {
		CONTACT_NAME2 = cONTACT_NAME2;
	}

	public String getCONTACT_PHONE2() {
		return CONTACT_PHONE2;
	}

	public void setCONTACT_PHONE2(String cONTACT_PHONE2) {
		CONTACT_PHONE2 = cONTACT_PHONE2;
	}

	public String getCONTACT_EXTENSION2() {
		return CONTACT_EXTENSION2;
	}

	public void setCONTACT_EXTENSION2(String cONTACT_EXTENSION2) {
		CONTACT_EXTENSION2 = cONTACT_EXTENSION2;
	}

	public String getCONTACT_CELL2() {
		return CONTACT_CELL2;
	}

	public void setCONTACT_CELL2(String cONTACT_CELL2) {
		CONTACT_CELL2 = cONTACT_CELL2;
	}

	public String getCONTACT_FAX2() {
		return CONTACT_FAX2;
	}

	public void setCONTACT_FAX2(String cONTACT_FAX2) {
		CONTACT_FAX2 = cONTACT_FAX2;
	}

	public String getCONTACT_EMAIL2() {
		return CONTACT_EMAIL2;
	}

	public void setCONTACT_EMAIL2(String cONTACT_EMAIL2) {
		CONTACT_EMAIL2 = cONTACT_EMAIL2;
	}

	public String getCONTACT_NAME3() {
		return CONTACT_NAME3;
	}

	public void setCONTACT_NAME3(String cONTACT_NAME3) {
		CONTACT_NAME3 = cONTACT_NAME3;
	}

	public String getCONTACT_PHONE3() {
		return CONTACT_PHONE3;
	}

	public void setCONTACT_PHONE3(String cONTACT_PHONE3) {
		CONTACT_PHONE3 = cONTACT_PHONE3;
	}

	public String getCONTACT_EXTENSION3() {
		return CONTACT_EXTENSION3;
	}

	public void setCONTACT_EXTENSION3(String cONTACT_EXTENSION3) {
		CONTACT_EXTENSION3 = cONTACT_EXTENSION3;
	}

	public String getCONTACT_CELL3() {
		return CONTACT_CELL3;
	}

	public void setCONTACT_CELL3(String cONTACT_CELL3) {
		CONTACT_CELL3 = cONTACT_CELL3;
	}

	public String getCONTACT_FAX3() {
		return CONTACT_FAX3;
	}

	public void setCONTACT_FAX3(String cONTACT_FAX3) {
		CONTACT_FAX3 = cONTACT_FAX3;
	}

	public String getCONTACT_EMAIL3() {
		return CONTACT_EMAIL3;
	}

	public void setCONTACT_EMAIL3(String cONTACT_EMAIL3) {
		CONTACT_EMAIL3 = cONTACT_EMAIL3;
	}

	public String getCONTACT_NAME4() {
		return CONTACT_NAME4;
	}

	public void setCONTACT_NAME4(String cONTACT_NAME4) {
		CONTACT_NAME4 = cONTACT_NAME4;
	}

	public String getCONTACT_PHONE4() {
		return CONTACT_PHONE4;
	}

	public void setCONTACT_PHONE4(String cONTACT_PHONE4) {
		CONTACT_PHONE4 = cONTACT_PHONE4;
	}

	public String getCONTACT_EXTENSION4() {
		return CONTACT_EXTENSION4;
	}

	public void setCONTACT_EXTENSION4(String cONTACT_EXTENSION4) {
		CONTACT_EXTENSION4 = cONTACT_EXTENSION4;
	}

	public String getCONTACT_CELL4() {
		return CONTACT_CELL4;
	}

	public void setCONTACT_CELL4(String cONTACT_CELL4) {
		CONTACT_CELL4 = cONTACT_CELL4;
	}

	public String getCONTACT_FAX4() {
		return CONTACT_FAX4;
	}

	public void setCONTACT_FAX4(String cONTACT_FAX4) {
		CONTACT_FAX4 = cONTACT_FAX4;
	}

	public String getCONTACT_EMAIL4() {
		return CONTACT_EMAIL4;
	}

	public void setCONTACT_EMAIL4(String cONTACT_EMAIL4) {
		CONTACT_EMAIL4 = cONTACT_EMAIL4;
	}

	public String getMAIN_FAX() {
		return MAIN_FAX;
	}

	public void setMAIN_FAX(String mAIN_FAX) {
		MAIN_FAX = mAIN_FAX;
	}

	public String getMAIN_EMAIL() {
		return MAIN_EMAIL;
	}

	public void setMAIN_EMAIL(String mAIN_EMAIL) {
		MAIN_EMAIL = mAIN_EMAIL;
	}

	public double getHANDLING_CHARGE_PERCENT() {
		return HANDLING_CHARGE_PERCENT;
	}

	public void setHANDLING_CHARGE_PERCENT(double hANDLING_CHARGE_PERCENT) {
		HANDLING_CHARGE_PERCENT = hANDLING_CHARGE_PERCENT;
	}

	public double getDISCOUNT_PERCENT() {
		return DISCOUNT_PERCENT;
	}

	public void setDISCOUNT_PERCENT(double dISCOUNT_PERCENT) {
		DISCOUNT_PERCENT = dISCOUNT_PERCENT;
	}

	public double getMINIMAL_CHARGE() {
		return MINIMAL_CHARGE;
	}

	public void setMINIMAL_CHARGE(double mINIMAL_CHARGE) {
		MINIMAL_CHARGE = mINIMAL_CHARGE;
	}

	public String getBROKER_NAME() {
		return BROKER_NAME;
	}

	public void setBROKER_NAME(String bROKER_NAME) {
		BROKER_NAME = bROKER_NAME;
	}

	public char getTRACTABLE() {
		return TRACTABLE;
	}

	public void setTRACTABLE(char tRACTABLE) {
		TRACTABLE = tRACTABLE;
	}

	public String getSHIP_TO_TYPE() {
		return SHIP_TO_TYPE;
	}

	public void setSHIP_TO_TYPE(String sHIP_TO_TYPE) {
		SHIP_TO_TYPE = sHIP_TO_TYPE;
	}

	public char getSHIP_TO_POBOX() {
		return SHIP_TO_POBOX;
	}

	public void setSHIP_TO_POBOX(char sHIP_TO_POBOX) {
		SHIP_TO_POBOX = sHIP_TO_POBOX;
	}

	public char getRETURNS_CARRIER() {
		return RETURNS_CARRIER;
	}

	public void setRETURNS_CARRIER(char rETURNS_CARRIER) {
		RETURNS_CARRIER = rETURNS_CARRIER;
	}

	public char getSHIP_TO_AGENT() {
		return SHIP_TO_AGENT;
	}

	public void setSHIP_TO_AGENT(char sHIP_TO_AGENT) {
		SHIP_TO_AGENT = sHIP_TO_AGENT;
	}

	public long getPRO_NUMBER_START() {
		return PRO_NUMBER_START;
	}

	public void setPRO_NUMBER_START(long pRO_NUMBER_START) {
		PRO_NUMBER_START = pRO_NUMBER_START;
	}

	public long getPRO_NUMBER_END() {
		return PRO_NUMBER_END;
	}

	public void setPRO_NUMBER_END(long pRO_NUMBER_END) {
		PRO_NUMBER_END = pRO_NUMBER_END;
	}

	public long getPRO_NUMBER_LAST_USED() {
		return PRO_NUMBER_LAST_USED;
	}

	public void setPRO_NUMBER_LAST_USED(long pRO_NUMBER_LAST_USED) {
		PRO_NUMBER_LAST_USED = pRO_NUMBER_LAST_USED;
	}

	public long getPRO_NUMBER_REMAINING_WARRING() {
		return PRO_NUMBER_REMAINING_WARRING;
	}

	public void setPRO_NUMBER_REMAINING_WARRING(long pRO_NUMBER_REMAINING_WARRING) {
		PRO_NUMBER_REMAINING_WARRING = pRO_NUMBER_REMAINING_WARRING;
	}

	public String getPRO_NUM_REMAINING_WARN_EMAIL() {
		return PRO_NUM_REMAINING_WARN_EMAIL;
	}

	public void setPRO_NUM_REMAINING_WARN_EMAIL(String pRO_NUM_REMAINING_WARN_EMAIL) {
		PRO_NUM_REMAINING_WARN_EMAIL = pRO_NUM_REMAINING_WARN_EMAIL;
	}

	public long getPRO_NUMBER_PREFIX() {
		return PRO_NUMBER_PREFIX;
	}

	public void setPRO_NUMBER_PREFIX(long pRO_NUMBER_PREFIX) {
		PRO_NUMBER_PREFIX = pRO_NUMBER_PREFIX;
	}

	public String getPRO_NUMBER_CHECK_DIGIT_TYPE() {
		return PRO_NUMBER_CHECK_DIGIT_TYPE;
	}

	public void setPRO_NUMBER_CHECK_DIGIT_TYPE(String pRO_NUMBER_CHECK_DIGIT_TYPE) {
		PRO_NUMBER_CHECK_DIGIT_TYPE = pRO_NUMBER_CHECK_DIGIT_TYPE;
	}

	public char getACCEPTS_ELECTRONIC_MANIFIES() {
		return ACCEPTS_ELECTRONIC_MANIFIES;
	}

	public void setACCEPTS_ELECTRONIC_MANIFIES(char aCCEPTS_ELECTRONIC_MANIFIES) {
		ACCEPTS_ELECTRONIC_MANIFIES = aCCEPTS_ELECTRONIC_MANIFIES;
	}

	public String getCARRER_INTERFACE() {
		return CARRER_INTERFACE;
	}

	public void setCARRER_INTERFACE(String cARRER_INTERFACE) {
		CARRER_INTERFACE = cARRER_INTERFACE;
	}

	public String getPAYMENT_NAME20() {
		return PAYMENT_NAME20;
	}

	public void setPAYMENT_NAME20(String pAYMENT_NAME20) {
		PAYMENT_NAME20 = pAYMENT_NAME20;
	}

	public String getPAYMENT_NAME50() {
		return PAYMENT_NAME50;
	}

	public void setPAYMENT_NAME50(String pAYMENT_NAME50) {
		PAYMENT_NAME50 = pAYMENT_NAME50;
	}

	public String getPAYMENT_ADDRESS_LINE1() {
		return PAYMENT_ADDRESS_LINE1;
	}

	public void setPAYMENT_ADDRESS_LINE1(String pAYMENT_ADDRESS_LINE1) {
		PAYMENT_ADDRESS_LINE1 = pAYMENT_ADDRESS_LINE1;
	}

	public String getPAYMENT_ADDRESS_LINE2() {
		return PAYMENT_ADDRESS_LINE2;
	}

	public void setPAYMENT_ADDRESS_LINE2(String pAYMENT_ADDRESS_LINE2) {
		PAYMENT_ADDRESS_LINE2 = pAYMENT_ADDRESS_LINE2;
	}

	public String getPAYMENT_ADDRESS_LINE3() {
		return PAYMENT_ADDRESS_LINE3;
	}

	public void setPAYMENT_ADDRESS_LINE3(String pAYMENT_ADDRESS_LINE3) {
		PAYMENT_ADDRESS_LINE3 = pAYMENT_ADDRESS_LINE3;
	}

	public String getPAYMENT_ADDRESS_LINE4() {
		return PAYMENT_ADDRESS_LINE4;
	}

	public void setPAYMENT_ADDRESS_LINE4(String pAYMENT_ADDRESS_LINE4) {
		PAYMENT_ADDRESS_LINE4 = pAYMENT_ADDRESS_LINE4;
	}

	public String getPAYMENT_CITY() {
		return PAYMENT_CITY;
	}

	public void setPAYMENT_CITY(String pAYMENT_CITY) {
		PAYMENT_CITY = pAYMENT_CITY;
	}

	public String getPAYMENT_STATE_CODE() {
		return PAYMENT_STATE_CODE;
	}

	public void setPAYMENT_STATE_CODE(String pAYMENT_STATE_CODE) {
		PAYMENT_STATE_CODE = pAYMENT_STATE_CODE;
	}

	public String getPAYMENT_COUNTRY_CODE() {
		return PAYMENT_COUNTRY_CODE;
	}

	public void setPAYMENT_COUNTRY_CODE(String pAYMENT_COUNTRY_CODE) {
		PAYMENT_COUNTRY_CODE = pAYMENT_COUNTRY_CODE;
	}

	public String getPAYMENT_ZIP_CODE() {
		return PAYMENT_ZIP_CODE;
	}

	public void setPAYMENT_ZIP_CODE(String pAYMENT_ZIP_CODE) {
		PAYMENT_ZIP_CODE = pAYMENT_ZIP_CODE;
	}

	public String getPAYMENT_CONTACT_NAME1() {
		return PAYMENT_CONTACT_NAME1;
	}

	public void setPAYMENT_CONTACT_NAME1(String pAYMENT_CONTACT_NAME1) {
		PAYMENT_CONTACT_NAME1 = pAYMENT_CONTACT_NAME1;
	}

	public String getPAYMENT_CONTACT_PHONE1() {
		return PAYMENT_CONTACT_PHONE1;
	}

	public void setPAYMENT_CONTACT_PHONE1(String pAYMENT_CONTACT_PHONE1) {
		PAYMENT_CONTACT_PHONE1 = pAYMENT_CONTACT_PHONE1;
	}

	public String getPAYMENT_CONTACT_EXTENSION1() {
		return PAYMENT_CONTACT_EXTENSION1;
	}

	public void setPAYMENT_CONTACT_EXTENSION1(String pAYMENT_CONTACT_EXTENSION1) {
		PAYMENT_CONTACT_EXTENSION1 = pAYMENT_CONTACT_EXTENSION1;
	}

	public String getPAYMENT_CONTACT_CELL1() {
		return PAYMENT_CONTACT_CELL1;
	}

	public void setPAYMENT_CONTACT_CELL1(String pAYMENT_CONTACT_CELL1) {
		PAYMENT_CONTACT_CELL1 = pAYMENT_CONTACT_CELL1;
	}

	public String getPAYMENT_CONTACT_FAX1() {
		return PAYMENT_CONTACT_FAX1;
	}

	public void setPAYMENT_CONTACT_FAX1(String pAYMENT_CONTACT_FAX1) {
		PAYMENT_CONTACT_FAX1 = pAYMENT_CONTACT_FAX1;
	}

	public String getPAYMENT_CONTACT_EMAIL1() {
		return PAYMENT_CONTACT_EMAIL1;
	}

	public void setPAYMENT_CONTACT_EMAIL1(String pAYMENT_CONTACT_EMAIL1) {
		PAYMENT_CONTACT_EMAIL1 = pAYMENT_CONTACT_EMAIL1;
	}

	public String getPAYMENT_CONTACT_NAME2() {
		return PAYMENT_CONTACT_NAME2;
	}

	public void setPAYMENT_CONTACT_NAME2(String pAYMENT_CONTACT_NAME2) {
		PAYMENT_CONTACT_NAME2 = pAYMENT_CONTACT_NAME2;
	}

	public String getPAYMENT_CONTACT_PHONE2() {
		return PAYMENT_CONTACT_PHONE2;
	}

	public void setPAYMENT_CONTACT_PHONE2(String pAYMENT_CONTACT_PHONE2) {
		PAYMENT_CONTACT_PHONE2 = pAYMENT_CONTACT_PHONE2;
	}

	public String getPAYMENT_CONTACT_EXTENSION2() {
		return PAYMENT_CONTACT_EXTENSION2;
	}

	public void setPAYMENT_CONTACT_EXTENSION2(String pAYMENT_CONTACT_EXTENSION2) {
		PAYMENT_CONTACT_EXTENSION2 = pAYMENT_CONTACT_EXTENSION2;
	}

	public String getPAYMENT_CONTACT_CELL2() {
		return PAYMENT_CONTACT_CELL2;
	}

	public void setPAYMENT_CONTACT_CELL2(String pAYMENT_CONTACT_CELL2) {
		PAYMENT_CONTACT_CELL2 = pAYMENT_CONTACT_CELL2;
	}

	public String getPAYMENT_CONTACT_FAX2() {
		return PAYMENT_CONTACT_FAX2;
	}

	public void setPAYMENT_CONTACT_FAX2(String pAYMENT_CONTACT_FAX2) {
		PAYMENT_CONTACT_FAX2 = pAYMENT_CONTACT_FAX2;
	}

	public String getPAYMENT_ROUTING() {
		return PAYMENT_ROUTING;
	}

	public void setPAYMENT_ROUTING(String pAYMENT_ROUTING) {
		PAYMENT_ROUTING = pAYMENT_ROUTING;
	}

	public String getPAYMENT_TYPE() {
		return PAYMENT_TYPE;
	}

	public void setPAYMENT_TYPE(String pAYMENT_TYPE) {
		PAYMENT_TYPE = pAYMENT_TYPE;
	}

	public long getPAYMENT_TERMS() {
		return PAYMENT_TERMS;
	}

	public void setPAYMENT_TERMS(long pAYMENT_TERMS) {
		PAYMENT_TERMS = pAYMENT_TERMS;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	
}
