/*

Date Developed  Sep 18 2014
Description pojo class of General_Codes in which getter and setter methods only for primary composite key and mapping with table
Created By Aakash Bishnoi
Created Date Oct 8 2014
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class GENERALCODESPK implements Serializable{

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		@Column(name = GLOBALCONSTANT.DataBase_GeneralCodes_Tenant)
		public String Tenant;
		
		@Column(name = GLOBALCONSTANT.DataBase_GeneralCodes_Company)
		public String Company;
		
		@Column(name = GLOBALCONSTANT.DataBase_GeneralCodes_Application)
		public String Application;
		
		@Column(name = GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCodeID)
		public String GeneralCodeID;
		
		@Column(name = GLOBALCONSTANT.DataBase_GeneralCodes_GeneralCode)
		public String GeneralCode;
		
		public String getTenant() {
			return Tenant;
		}
		public void setTenant(String tenant) {
			Tenant = tenant;
		}
		public String getCompany() {
			return Company;
		}
		public void setCompany(String company) {
			Company = company;
		}
		public String getApplication() {
			return Application;
		}
		public void setApplication(String application) {
			Application = application;
		}
		public String getGeneralCodeID() {
			return GeneralCodeID;
		}
		public void setGeneralCodeID(String generalCodeID) {
			GeneralCodeID = generalCodeID;
		}
		public String getGeneralCode() {
			return GeneralCode;
		}
		public void setGeneralCode(String generalCode) {
			GeneralCode = generalCode;
		}
}

