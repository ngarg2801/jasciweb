/*

Date Developed  Nov 20 2014
Created by: Aakash Bishnoi
Description  pojo class of PRODUCT_VARIABLES Table only for primary keys
Created On:Mar 31 2015
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;

import com.jasci.common.constant.GLOBALCONSTANT;

public class PRODUCTVARIABLESPK implements Serializable{

private static final long serialVersionUID = 1L;



public String getTenant_Id() {
	return Tenant_Id;
}

public void setTenant_Id(String tenant_Id) {
	Tenant_Id = tenant_Id;
}

public String getCompany_Id() {
	return Company_Id;
}

public void setCompany_Id(String company_Id) {
	Company_Id = company_Id;
}

public String getProduct() {
	return Product;
}

public void setProduct(String product) {
	Product = product;
}

public String getQuality() {
	return Quality;
}

public void setQuality(String quality) {
	Quality = quality;
}

public long getUnit_Of_Measure_Qty() {
	return Unit_Of_Measure_Qty;
}

public void setUnit_Of_Measure_Qty(long unit_Of_Measure_Qty) {
	Unit_Of_Measure_Qty = unit_Of_Measure_Qty;
}

@Column(name=GLOBALCONSTANT.DateBase_Tenant_Id)
private String Tenant_Id;

@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
private String Company_Id;

@Column(name=GLOBALCONSTANT.DataBase_Product)
private String Product;

@Column(name=GLOBALCONSTANT.DataBase_Quality)
private String Quality;

@Column(name=GLOBALCONSTANT.DataBase_Unit_Of_Measure_Qty)
private long Unit_Of_Measure_Qty;

}
