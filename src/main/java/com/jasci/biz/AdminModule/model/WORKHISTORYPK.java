/*


Created by: Aakash Bishnoi
Description  pojo class of WORK_HISTORY Table only for primary keys
Created On:Apr 1 2015
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

import com.jasci.common.constant.GLOBALCONSTANT;

public class WORKHISTORYPK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DateBase_Tenant_Id)
	private String Tenant_Id;
	@Column(name=GLOBALCONSTANT.DataBase_Work_Control_Number)
	private String Work_Control_Number;
	@Column(name=GLOBALCONSTANT.DataBase_Task)
	private String Task;
	@Column(name=GLOBALCONSTANT.DataBase_Team_Member)
	private String Team_Member;
	@Column(name=GLOBALCONSTANT.DataBase_Dates)
	private Date Dates;
	
	public String getTenant_Id() {
		return Tenant_Id;
	}
	public void setTenant_Id(String tenant_Id) {
		Tenant_Id = tenant_Id;
	}
	public String getWork_Control_Number() {
		return Work_Control_Number;
	}
	public void setWork_Control_Number(String work_Control_Number) {
		Work_Control_Number = work_Control_Number;
	}
	public String getTask() {
		return Task;
	}
	public void setTask(String task) {
		Task = task;
	}
	public String getTeam_Member() {
		return Team_Member;
	}
	public void setTeam_Member(String team_Member) {
		Team_Member = team_Member;
	}
	public Date getDates() {
		return Dates;
	}
	public void setDates(Date dates) {
		Dates = dates;
	}
	
	
	
}
