/*


Date Developed  Nov 17 2014
Created by: Rahul Kumar
Description  pojo class of Languages in which getter and setter methods for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class TEAMMEMBERSPK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_TeamMember )	
	private String TeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Tenant)	
	private String Tenant;	
	
	
	public String getTeamMember() {
		return TeamMember;
	}
	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	
	
	
	
	

}
