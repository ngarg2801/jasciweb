/*


Created by: Shailendra Rajput
Description  pojo class of SMART_TASK_EXECUTIONS Table only for primary keys
Created On:Mar 30 2015
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class SMARTTASKEXECUTIONSPK implements Serializable{

private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DateBase_Tenant_Id)
	 private String Tenant_Id;
	@Column(name=GLOBALCONSTANT.DateBase_Company_Id)
    private String Company_Id;
	@Column(name=GLOBALCONSTANT.DateBase_Execution_Name)
	private String Execution_Name;
	
	public String getTenant_Id() {
		return Tenant_Id;
	}
	public void setTenant_Id(String tenant_Id) {
		Tenant_Id = tenant_Id;
	}
	public String getCompany_Id() {
		return Company_Id;
	}
	public void setCompany_Id(String company_Id) {
		Company_Id = company_Id;
	}
	public String getExecution_Name() {
		return Execution_Name;
	}
	public void setExecution_Name(String execution_Name) {
		Execution_Name = execution_Name;
	}
	
	
}
