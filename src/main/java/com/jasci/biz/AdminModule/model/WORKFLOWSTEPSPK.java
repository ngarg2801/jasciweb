/**
 * 
 * @ file_name: WORKFLOWSTEPSPK
* @Developed by:Pradeep Kumar
*@Created Date:30 Mar 2015
*Purpose : Pojo Used for Mapping with table WORK_FLOW_STEPS for primary keys only
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class WORKFLOWSTEPSPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Tenant_Id)
	private String Tenant_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
	private String Company_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Flow_Id)
	private String Work_Flow_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Flow_Step)
	private String Work_Flow_Step;

	public String getTenant_Id() {
		return Tenant_Id;
	}

	public void setTenant_Id(String tenant_Id) {
		Tenant_Id = tenant_Id;
	}

	public String getCompany_Id() {
		return Company_Id;
	}

	public void setCompany_Id(String company_Id) {
		Company_Id = company_Id;
	}

	public String getWork_Flow_Id() {
		return Work_Flow_Id;
	}

	public void setWork_Flow_Id(String work_Flow_Id) {
		Work_Flow_Id = work_Flow_Id;
	}

	public String getWork_Flow_Step() {
		return Work_Flow_Step;
	}

	public void setWork_Flow_Step(String work_Flow_Step) {
		Work_Flow_Step = work_Flow_Step;
	}

	
	
	
}
