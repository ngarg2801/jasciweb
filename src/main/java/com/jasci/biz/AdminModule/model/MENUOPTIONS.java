/**

Date Developed  Sep 18 2014
Description pojo class of Menu_Options in which getter and setter methods and mapping with table
Created By Aakash Bishnoi
Created Date Oct 27 2014
 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.ColumnResult;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;

import com.jasci.common.constant.GLOBALCONSTANT;


@NamedNativeQueries
({
@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfileOptions , query=GLOBALCONSTANT.NativeQueryName_MenuOption_query, 
resultSetMapping=GLOBALCONSTANT.NativeQueryName_MenuProfileOptions_ResultSetMapping),
@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions , query=GLOBALCONSTANT.NativeQueryName_MenuOptionProfile_query, 
resultSetMapping=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions_ResultSetMapping),

@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuOptions_MenuOptionQueryName_CheckOne, query=GLOBALCONSTANT.NativeQueryName_MenuOptions_MenuOptionQuery_CheckOne,
resultClass =MENUOPTIONS.class),

@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuOptions_MenuOptionQueryName_CheckTwo, query=GLOBALCONSTANT.NativeQueryName_MenuOptions_MenuOptionQuery_CheckTwo,
resultClass =MENUOPTIONS.class),
@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuOption_delete_From_MenuProfileOption, query=GLOBALCONSTANT.NativeQuery_MenuOption_delete_From_MenuProfileOption,
resultClass =MENUPROFILEOPTIONS.class),


/*@NamedNativeQuery(name="ApplicationGeneralCode", query="Select GENERAL_CODE_ID As APPLICATION,GENERAL_CODE as SUBAPPLICATION from GENERAL_CODES where GENERAL_CODE_ID in "
		+ "(select GENERAL_CODE from GENERAL_CODES where Upper(GENERAL_CODE_ID)=:valApplication)",
		resultSetMapping="ApplicationAndSubApplication"),*/
		
@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuOptions_GeneralCode_MenuTypeQueryName, query=GLOBALCONSTANT.NativeQueryName_MenuOptions_GeneralCode_MenuTypeQuery,
resultClass=GENERALCODES.class),
				

@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuOptions_DisplayAllQueryNameOne, query=GLOBALCONSTANT.NativeQueryName_MenuOptions_DisplayAllQueryOne,
resultClass =MENUOPTIONS.class),


@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuOptions_DisplayAllQueryNameTwo, query=GLOBALCONSTANT.NativeQueryName_MenuOptions_DisplayAllQueryTwo,
resultClass =MENUOPTIONS.class)



/*@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions , query=GLOBALCONSTANT.NativeQueryName_MenuOptionProfile_query, 
resultSetMapping=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions_ResultSetMapping),

*/


})

@SqlResultSetMappings({
@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_MenuProfileOptions_ResultSetMapping ,
columns ={@ColumnResult(name=GLOBALCONSTANT.DataBase_MenuOptions_MenuOption),
		@ColumnResult(name=GLOBALCONSTANT.DataBase_MenuOptions_Application),
		@ColumnResult(name=GLOBALCONSTANT.DataBase_MenuOptions_ApplicationSub),
		@ColumnResult(name=GLOBALCONSTANT.DataBase_MenuOptions_Execution),
		@ColumnResult(name=GLOBALCONSTANT.ColumnName_Help)}),
		

@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions_ResultSetMapping ,
columns ={@ColumnResult(name=GLOBALCONSTANT.DataBase_MenuOptions_MenuOption)}),


/*@SqlResultSetMapping(name="ApplicationAndSubApplication",
columns={@ColumnResult(name="APPLICATION"),
		@ColumnResult(name="SUBAPPLICATION")
}),*/

/*@SqlResultSetMapping(name=GLOBALCONSTANT.ResultSetMapping_MenuOptions_GeneralCode_MenuType,
columns={@ColumnResult(name=GLOBALCONSTANT.DataBase_MenuOptions_MenuType)
		})
*/



})


@Entity
@Table(name=GLOBALCONSTANT.TableName_Menu_Options)
public class MENUOPTIONS {

	@Id
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_MenuOption)
	private String MenuOption;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_Tenant)
	private String Tenant;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_Application)
	private String Application;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_ApplicationSub)
	private String ApplicationSub;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_MenuType)
	private String MenuType;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_Description20)
	private String Description20;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_Description50)
	private String Description50;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_Helpline)
	private String Helpline;
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_Execution)
	private String Execution;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_LastActivityDate)
	private Date LastActivityDate;
	 
	 @Column(name = GLOBALCONSTANT.DataBase_MenuOptions_LastActivityTeamMember)
	private String LastActivityTeamMember;
	
	
	public String getMenuOption() {
		return MenuOption;
	}
	public void setMenuOption(String menuOption) {
		MenuOption = menuOption;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getApplication() {
		return Application;
	}
	public void setApplication(String application) {
		Application = application;
	}
	public String getApplicationSub() {
		return ApplicationSub;
	}
	public void setApplicationSub(String applicationSub) {
		ApplicationSub = applicationSub;
	}
	public String getMenuType() {
		return MenuType;
	}
	public void setMenuType(String menuType) {
		MenuType = menuType;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getHelpline() {
		return Helpline;
	}
	public void setHelpline(String helpline) {
		Helpline = helpline;
	}

	public String getExecution() {
		return Execution;
	}
	public void setExecution(String execution) {
		Execution = execution;
	}
	public Date getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
}
