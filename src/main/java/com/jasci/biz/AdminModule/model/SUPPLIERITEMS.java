/**
 * 
 * @ file_name: SUPPLIERSITEMS
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table SUPPLIER_ITEMS
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Supplier_Item)

public class SUPPLIERITEMS {

	@EmbeddedId
	private SUPPLIERITEMSPK Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Supplier_Item)
	private String SUPPLIER_ITEM;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Description20 , length=20)
	private String DESCRIPTION20;
	
	@Column(name=GLOBALCONSTANT.DataBase_Description50,length=50)
	private String DESCRIPTION50;
	
	@Column(name=GLOBALCONSTANT.DataBase_Order_Instructions)
	private String ORDER_INSTRUCTIONS;
	
	@Column(name=GLOBALCONSTANT.DataBase_Order_By_Qty)
	private char ORDER_BY_QTY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Minimum_Order_Qty)
	private long MINIMUM_ORDER_QTY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Returns_Allowed)
	private char RETURNS_ALLOWED;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Order_Date)
	private Date LAST_ORDER_DATE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Order_Quanitity)
	private double LAST_ORDER_QUANITITY;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Order_Price)
	private double LAST_ORDER_PRICE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Team_Member_Id)
	private String Team_Member_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Supplier_Bar_Code)
	private String SUPPLIER_BAR_CODE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Supplier_Bar_Code_Type)
	private String SUPPLIER_BAR_CODE_TYPE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Status)
	private char STATUS;

	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;

	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	public SUPPLIERITEMSPK getId() {
		return Id;
	}

	public void setId(SUPPLIERITEMSPK id) {
		Id = id;
	}

	public String getSUPPLIER_ITEM() {
		return SUPPLIER_ITEM;
	}

	public void setSUPPLIER_ITEM(String sUPPLIER_ITEM) {
		SUPPLIER_ITEM = sUPPLIER_ITEM;
	}

	public String getDESCRIPTION20() {
		return DESCRIPTION20;
	}

	public void setDESCRIPTION20(String dESCRIPTION20) {
		DESCRIPTION20 = dESCRIPTION20;
	}

	public String getDESCRIPTION50() {
		return DESCRIPTION50;
	}

	public void setDESCRIPTION50(String dESCRIPTION50) {
		DESCRIPTION50 = dESCRIPTION50;
	}

	public String getORDER_INSTRUCTIONS() {
		return ORDER_INSTRUCTIONS;
	}

	public void setORDER_INSTRUCTIONS(String oRDER_INSTRUCTIONS) {
		ORDER_INSTRUCTIONS = oRDER_INSTRUCTIONS;
	}

	public char getORDER_BY_QTY() {
		return ORDER_BY_QTY;
	}

	public void setORDER_BY_QTY(char oRDER_BY_QTY) {
		ORDER_BY_QTY = oRDER_BY_QTY;
	}

	public long getMINIMUM_ORDER_QTY() {
		return MINIMUM_ORDER_QTY;
	}

	public void setMINIMUM_ORDER_QTY(long mINIMUM_ORDER_QTY) {
		MINIMUM_ORDER_QTY = mINIMUM_ORDER_QTY;
	}

	public char getRETURNS_ALLOWED() {
		return RETURNS_ALLOWED;
	}

	public void setRETURNS_ALLOWED(char rETURNS_ALLOWED) {
		RETURNS_ALLOWED = rETURNS_ALLOWED;
	}

	public Date getLAST_ORDER_DATE() {
		return LAST_ORDER_DATE;
	}

	public void setLAST_ORDER_DATE(Date lAST_ORDER_DATE) {
		LAST_ORDER_DATE = lAST_ORDER_DATE;
	}

	public double getLAST_ORDER_QUANITITY() {
		return LAST_ORDER_QUANITITY;
	}

	public void setLAST_ORDER_QUANITITY(double lAST_ORDER_QUANITITY) {
		LAST_ORDER_QUANITITY = lAST_ORDER_QUANITITY;
	}

	public double getLAST_ORDER_PRICE() {
		return LAST_ORDER_PRICE;
	}

	public void setLAST_ORDER_PRICE(double lAST_ORDER_PRICE) {
		LAST_ORDER_PRICE = lAST_ORDER_PRICE;
	}

	public String getTeam_Member_Id() {
		return Team_Member_Id;
	}

	public void setTeam_Member_Id(String team_Member_Id) {
		Team_Member_Id = team_Member_Id;
	}

	public String getSUPPLIER_BAR_CODE() {
		return SUPPLIER_BAR_CODE;
	}

	public void setSUPPLIER_BAR_CODE(String sUPPLIER_BAR_CODE) {
		SUPPLIER_BAR_CODE = sUPPLIER_BAR_CODE;
	}

	public String getSUPPLIER_BAR_CODE_TYPE() {
		return SUPPLIER_BAR_CODE_TYPE;
	}

	public void setSUPPLIER_BAR_CODE_TYPE(String sUPPLIER_BAR_CODE_TYPE) {
		SUPPLIER_BAR_CODE_TYPE = sUPPLIER_BAR_CODE_TYPE;
	}

	public char getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(char sTATUS) {
		STATUS = sTATUS;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	
	
}
