/**
 * 
 * @ file_name: PRODUCTREORDERLEVELS
 * @Developed by:Pradeep kumar
 *@Created Date:Apr 1 2015
 *Purpose : Pojo Used for Mapping with table PRODUCT_REORDER_LEVELS
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Product_Reorder_Levels)

public class PRODUCTREORDERLEVELS {

	@EmbeddedId
	private PRODUCTREORDERLEVELSPK Id;
	
	@Column (name=GLOBALCONSTANT.DataBase_Minimum_Days_Suppy)
	private long MINIMUM_DAYS_SUPPLY;
	
	@Column (name=GLOBALCONSTANT.DataBase_Minimum_Inventory_Level)
	private long MINIMUM_INVENTORY_LEVEL;
	
	@Column (name=GLOBALCONSTANT.DataBase_Notes)
	private String NOTES;
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;

	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	public PRODUCTREORDERLEVELSPK getId() {
		return Id;
	}

	public void setId(PRODUCTREORDERLEVELSPK id) {
		Id = id;
	}

	public long getMINIMUM_DAYS_SUPPLY() {
		return MINIMUM_DAYS_SUPPLY;
	}

	public void setMINIMUM_DAYS_SUPPLY(long mINIMUM_DAYS_SUPPLY) {
		MINIMUM_DAYS_SUPPLY = mINIMUM_DAYS_SUPPLY;
	}

	public long getMINIMUM_INVENTORY_LEVEL() {
		return MINIMUM_INVENTORY_LEVEL;
	}

	public void setMINIMUM_INVENTORY_LEVEL(long mINIMUM_INVENTORY_LEVEL) {
		MINIMUM_INVENTORY_LEVEL = mINIMUM_INVENTORY_LEVEL;
	}

	public String getNOTES() {
		return NOTES;
	}

	public void setNOTES(String nOTES) {
		NOTES = nOTES;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	
	
}
