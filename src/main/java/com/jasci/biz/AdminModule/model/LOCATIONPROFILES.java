/**

Date Developed  Dec 26 2014
Developed by: Diksha Gupta
Description: LocationProfiles class has getter and setter to map with table Location_profiles
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_LOCATION_PROFILES)
@NamedNativeQueries({
@NamedNativeQuery(name = GLOBALCONSTANT.LocationProfile_Count_NumberOfLocations_QueryName , query = GLOBALCONSTANT.LocationProfile_Count_NumberOfLocations_Query , resultSetMapping = 
GLOBALCONSTANT.LocationProfile_Count_NumberOfLocations_ResultMapping),

@NamedNativeQuery(
		
		name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchLocationProfile,
		query = GLOBALCONSTANT.Location_Wizard_Select_Location_Profile_Query,
	        resultClass = LOCATIONPROFILES.class
		),

@NamedNativeQuery(name = GLOBALCONSTANT.LocationProfile_FulFillmentCenter_QueryName , query = GLOBALCONSTANT.LocationProfile_FulFillmentCenter_Query, resultClass=
FULFILLMENTCENTERSPOJO.class)
})
@SqlResultSetMapping(name =GLOBALCONSTANT.LocationProfile_Count_NumberOfLocations_ResultMapping,
columns = { 
		
  @ColumnResult(name = GLOBALCONSTANT.NumberOfLocations)
 
  })
public class LOCATIONPROFILES {
	
	@EmbeddedId
	private LOCATIONPROFILESPK LocationProfileId;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_ProfileGroup)
	private String ProfileGroup;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Description20)
	private String Description20;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Description50)
	private String Description50;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Type)
	private String Type;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationHeight)
	private Double LocationHeight;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationWidth)
	private Double LocationWidth;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationDepth)
	private Double LocationDepth;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationWeightCapacity)
	private Double LocationWeightCapacity;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationHeightCapacity)
	private Double LocationHeightCapacity;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_NumberPalletsFit)
	private Long NumberPalletsFit;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_NumberFloorLocations)
	private Long NumberFloorLocations;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_AllocationAllowable)
	private String AllocationAllowable;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LocationCheck )
	private String LocationCheck ;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_FreeLocationWhenZero)
	private String FreeLocationWhenZero;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_FreePrimeDays)
	private Long FreePrimeDays;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_MutipleProductsInLocation)
	private String MutipleProductsInLocation;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_StorageType)
	private String StorageType;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_Slotting )
	private String Slotting ;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_CCActivityPoint)
	private Long CCActivityPoint;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_CCHighValueAmount)
	private Long CCHighValueAmount;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_CCHighValueFactor)
	private Long CCHighValueFactor;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastUsedDate)
	private Date LastUsedDate;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastUsedTeamMember)
	private String LastUsedTeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastActivityDate)
	private Date LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION_PROFILES_LastActivityTeamMember)
	private String LastActivityTeamMember;
	

	public LOCATIONPROFILESPK getLocationProfileId() {
		return LocationProfileId;
	}
	public void setLocationProfileId(LOCATIONPROFILESPK locationProfileId) {
		LocationProfileId = locationProfileId;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	
	
	public Double getLocationHeight() {
		return LocationHeight;
	}
	public void setLocationHeight(Double locationHeight) {
		LocationHeight = locationHeight;
	}
	public Double getLocationWidth() {
		return LocationWidth;
	}
	public void setLocationWidth(Double locationWidth) {
		LocationWidth = locationWidth;
	}
	public Double getLocationDepth() {
		return LocationDepth;
	}
	public void setLocationDepth(Double locationDepth) {
		LocationDepth = locationDepth;
	}
	public Double getLocationWeightCapacity() {
		return LocationWeightCapacity;
	}
	public void setLocationWeightCapacity(Double locationWeightCapacity) {
		LocationWeightCapacity = locationWeightCapacity;
	}
	public Double getLocationHeightCapacity() {
		return LocationHeightCapacity;
	}
	public void setLocationHeightCapacity(Double locationHeightCapacity) {
		LocationHeightCapacity = locationHeightCapacity;
	}
	public Long getNumberPalletsFit() {
		return NumberPalletsFit;
	}
	public void setNumberPalletsFit(Long numberPalletsFit) {
		NumberPalletsFit = numberPalletsFit;
	}
	public Long getNumberFloorLocations() {
		return NumberFloorLocations;
	}
	public void setNumberFloorLocations(Long numberFloorLocations) {
		NumberFloorLocations = numberFloorLocations;
	}
	public String getAllocationAllowable() {
		return AllocationAllowable;
	}
	public void setAllocationAllowable(String allocationAllowable) {
		AllocationAllowable = allocationAllowable;
	}
	public String getLocationCheck() {
		return LocationCheck;
	}
	public void setLocationCheck(String locationCheck) {
		LocationCheck = locationCheck;
	}
	public String getFreeLocationWhenZero() {
		return FreeLocationWhenZero;
	}
	public void setFreeLocationWhenZero(String freeLocationWhenZero) {
		FreeLocationWhenZero = freeLocationWhenZero;
	}
	public Long getFreePrimeDays() {
		return FreePrimeDays;
	}
	public void setFreePrimeDays(Long freePrimeDays) {
		FreePrimeDays = freePrimeDays;
	}
	public String getMutipleProductsInLocation() {
		return MutipleProductsInLocation;
	}
	public void setMutipleProductsInLocation(String mutipleProductsInLocation) {
		MutipleProductsInLocation = mutipleProductsInLocation;
	}
	public String getStorageType() {
		return StorageType;
	}
	public void setStorageType(String storageType) {
		StorageType = storageType;
	}
	public String getSlotting() {
		return Slotting;
	}
	public void setSlotting(String slotting) {
		Slotting = slotting;
	}
	public Long getCCActivityPoint() {
		return CCActivityPoint;
	}
	public void setCCActivityPoint(Long cCActivityPoint) {
		CCActivityPoint = cCActivityPoint;
	}
	public Long getCCHighValueAmount() {
		return CCHighValueAmount;
	}
	public void setCCHighValueAmount(Long cCHighValueAmount) {
		CCHighValueAmount = cCHighValueAmount;
	}
	public Long getCCHighValueFactor() {
		return CCHighValueFactor;
	}
	public void setCCHighValueFactor(Long cCHighValueFactor) {
		CCHighValueFactor = cCHighValueFactor;
	}
	public Date getLastUsedDate() {
		return LastUsedDate;
	}
	public void setLastUsedDate(Date lastUsedDate) {
		LastUsedDate = lastUsedDate;
	}
	public Date getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastUsedTeamMember() {
		return LastUsedTeamMember;
	}
	public void setLastUsedTeamMember(String lastUsedTeamMember) {
		LastUsedTeamMember = lastUsedTeamMember;
	}
	
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getProfileGroup() {
		return ProfileGroup;
	}
	public void setProfileGroup(String profileGroup) {
		ProfileGroup = profileGroup;
	}

	
}
