/*

Date Developed  Oct 16 2014
Description   pojo only for primary keys class of Infohelps in which getter and setter methods
Created By Aakash Bishnoi
 */

package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;



import com.jasci.common.constant.GLOBALCONSTANT;


@Embeddable
public class INFOHELPSPK implements Serializable{	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_InfoHelp)
	private String InfoHelp;

	@Column(name=GLOBALCONSTANT.DataBase_InfoHelps_Language)
	private String Language;
	

	public String getInfoHelp() {
		return InfoHelp;
	}

	public void setInfoHelp(String infoHelp) {
		InfoHelp = infoHelp;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	
	
	
	
}
