/**
 * 
 * @ file_name: WORKFLOWHEADERS
* @Developed by:Pradeep Kumar
*@Created Date:31 Mar 2015
*Purpose : Pojo Used for Mapping with table WORK_FLOW_HEADERS
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Work_Flow_Headers)
public class WORKFLOWHEADERS {

	@EmbeddedId
	private WORKFLOWHEADERSPK Id;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Description20 , length=36)
	private String DESCRIPTION20;
	
	@Column(name=GLOBALCONSTANT.DataBase_Description50,length=36)
	private String DESCRIPTION50;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Type)
	private String WORK_TYPE;

	public WORKFLOWHEADERSPK getId() {
		return Id;
	}

	public void setId(WORKFLOWHEADERSPK id) {
		Id = id;
	}

	public String getDESCRIPTION20() {
		return DESCRIPTION20;
	}

	public void setDESCRIPTION20(String dESCRIPTION20) {
		DESCRIPTION20 = dESCRIPTION20;
	}

	public String getDESCRIPTION50() {
		return DESCRIPTION50;
	}

	public void setDESCRIPTION50(String dESCRIPTION50) {
		DESCRIPTION50 = dESCRIPTION50;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getWORK_TYPE() {
		return WORK_TYPE;
	}

	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	
	
	
}
