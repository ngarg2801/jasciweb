/*

Date Developed  Oct 15 2014
Description create getter and setter method for the class INVENTORY_LEVELS except primary fields
Created By Pradeep Kumar
 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_INVENTORY_LEVELS)

@NamedNativeQueries({
	@NamedNativeQuery(
			
	name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchListInventoryLevel,
	query = GLOBALCONSTANT.Location_Wizard_QueryFetchListInventoryLevel,
        resultClass = INVENTORY_LEVELS.class
	),
	@NamedNativeQuery(
			
			name = GLOBALCONSTANT.Location_Wizard_QueryNameFetchListALLInventoryLevel,
			query = GLOBALCONSTANT.Location_Wizard_QueryFetchListAllInventoryLevel,
		        resultClass = INVENTORY_LEVELS.class
			)
})

public class INVENTORY_LEVELS {
	
	@EmbeddedId
	public INVENTORY_LEVELSPK Id;

	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Quantity )
	private double Quantity;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_UnitofMeasureCode)
	private String UnitofMeasureCode;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_UnitofMeasureQty)
	private int UnitofMeasureQty;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_DateControl)
	private int DateControl;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_ProductValue)
	private double ProductValue;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_DateReceived)
	private Date DateReceived;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_LastActivityDate)
	private Date LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_LastActivityEmployee)
	private String LastActivityEmployee;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_LastActivityTask)
	private String LastActivityTask;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_HoldCode)
	private String HoldCode;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_SystemCode)
	private String SystemCode;


	public INVENTORY_LEVELSPK getId() {
		return Id;
	}
	public void setId(INVENTORY_LEVELSPK id) {
		Id = id;
	}
	public double getQuantity() {
		return Quantity;
	}
	public void setQuantity(double quantity) {
		Quantity = quantity;
	}
	public String getUnitofMeasureCode() {
		return UnitofMeasureCode;
	}
	public void setUnitofMeasureCode(String unitofMeasureCode) {
		UnitofMeasureCode = unitofMeasureCode;
	}
	public int getUnitofMeasureQty() {
		return UnitofMeasureQty;
	}
	public void setUnitofMeasureQty(int unitofMeasureQty) {
		UnitofMeasureQty = unitofMeasureQty;
	}
	public int getDateControl() {
		return DateControl;
	}
	public void setDateControl(int dateControl) {
		DateControl = dateControl;
	}
	public double getProductValue() {
		return ProductValue;
	}
	public void setProductValue(double productValue) {
		ProductValue = productValue;
	}
	public Date getDateReceived() {
		return DateReceived;
	}
	public void setDateReceived(Date dateReceived) {
		DateReceived = dateReceived;
	}
	public Date getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityEmployee() {
		return LastActivityEmployee;
	}
	public void setLastActivityEmployee(String lastActivityEmployee) {
		LastActivityEmployee = lastActivityEmployee;
	}
	public String getLastActivityTask() {
		return LastActivityTask;
	}
	public void setLastActivityTask(String lastActivityTask) {
		LastActivityTask = lastActivityTask;
	}
	public String getHoldCode() {
		return HoldCode;
	}
	public void setHoldCode(String holdCode) {
		HoldCode = holdCode;
	}
	public String getSystemCode() {
		return SystemCode;
	}
	public void setSystemCode(String systemCode) {
		SystemCode = systemCode;
	}

	
}
