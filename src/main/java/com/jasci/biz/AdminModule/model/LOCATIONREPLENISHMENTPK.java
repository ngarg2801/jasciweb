/*


Created by: Pradeep Kumar
Description  pojo class of LOCATION_REPLENISHMENT Table in which getter and setter with primary keys
Created On:Jan 16 2015
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;



@Embeddable
public class LOCATIONREPLENISHMENTPK implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_TENANT_ID)
	private String TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_FULFILLMENT_CENTER_ID)
	private String FULFILLMENT_CENTER_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_COMPANY_ID)
	private String COMPANY_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_AREA)
	private String AREA;
	
	@Column(name=GLOBALCONSTANT.DataBase_LOCATION)
	private String LOCATION;
	
	@Column(name=GLOBALCONSTANT.DataBase_PRODUCT)
	private String PRODUCT;
	
	@Column(name=GLOBALCONSTANT.DataBase_QUALITY)
	private String QUALITY;

	
	
	public String getTENANT_ID() {
		return TENANT_ID;
	}

	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}

	public String getFULFILLMENT_CENTER_ID() {
		return FULFILLMENT_CENTER_ID;
	}

	public void setFULFILLMENT_CENTER_ID(String fULFILLMENT_CENTER_ID) {
		FULFILLMENT_CENTER_ID = fULFILLMENT_CENTER_ID;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getLOCATION() {
		return LOCATION;
	}

	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}

	public String getQUALITY() {
		return QUALITY;
	}

	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}
	
	

}
