/**
 * 
 * @ file_name: SUPPLIERITEMPRICINGPK
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table SUPPLIER_ITEM_PRICING for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class SUPPLIERITEMPRICINGPK implements Serializable {


	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Tenant_Id)
	private String TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
	private String COMPANY_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Supplier)
	private String SUPPLIER;
	
	@Column(name=GLOBALCONSTANT.DataBase_Product)
	private String PRODUCT;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quality)
	private String QUALITY;

	public String getTENANT_ID() {
		return TENANT_ID;
	}

	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	public String getSUPPLIER() {
		return SUPPLIER;
	}

	public void setSUPPLIER(String sUPPLIER) {
		SUPPLIER = sUPPLIER;
	}

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}

	public String getQUALITY() {
		return QUALITY;
	}

	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}
	
	
}
