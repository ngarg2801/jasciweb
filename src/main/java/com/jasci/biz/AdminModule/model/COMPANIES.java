/*

Date Developed  Nov 20 2014
Description pojo class of Companies in which getter and setter methods and mapping with table
Created By Rahul Kumar

 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.*;

@Entity
@Table(name=GLOBALCONSTANT.TabelName_Companies)

@NamedNativeQueries({
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.Companies_GetCompanyById_NamedQuery,
	query = GLOBALCONSTANT.Companies_GetCompanyById_Query,
        resultClass = COMPANIES.class
	),
	
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.Companies_Delete_NamedQuery,
	query = GLOBALCONSTANT.Companies_Delete_Query,
        resultClass = COMPANIES.class
	),
	
	
	
	@NamedNativeQuery(
			//Fetch data For GeneralCode where teammember should Y for data
	name = GLOBALCONSTANT.Companies_GetCompanyByPartName_NamedQuery,
	query = GLOBALCONSTANT.Companies_GetCompanyByPartName_Query,
        resultClass = COMPANIES.class
	),
})
public class COMPANIES {
	
	
	@EmbeddedId
	private COMPANIESPK Id;
	
	public COMPANIESPK getId() {
		return Id;
	}

	public void setId(COMPANIESPK id) {
		Id = id;
	}
	
	
	
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_Name20)
	private String Name20;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_Name50)
	private String Name50;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_AddressLine1)
	private String AddressLine1;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_AddressLine2)
	private String AddressLine2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_AddressLine3)
	private String AddressLine3;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_AddressLine4)
	private String AddressLine4;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_City)
	private String City;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_StateCode)
	private String StateCode;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_CountryCode)
	private String CountryCode;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ZipCode)
	private String ZipCode;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToName20)
	private String BillToName20;
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToName50)
	private String BillToName50;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToAddressLine1)
	private String BillToAddressLine1;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToAddressLine2)
	private String BillToAddressLine2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToAddressLine3)
	private String BillToAddressLine3;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToAddressLine4)
	private String BillToAddressLine4;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToFromCity)
	private String BillToFromCity;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToStateCode)
	private String BillToStateCode;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToCountryCode)
	private String BillToCountryCode;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactName1)
	private String ContactName1;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactPhone1)
	private String ContactPhone1;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactExtension1)
	private String ContactExtension1;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactCell1)
	private String ContactCell1;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactFax1)
	private String ContactFax1;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactEmail1)
	private String ContactEmail1;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactName2)
	private String ContactName2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactPhone2)
	private String ContactPhone2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactCell2)
	private String ContactCell2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactExtension2)
	private String ContactExtension2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactFax2)
	private String ContactFax2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactEmail2)
	private String ContactEmail2;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactName3)
	private String ContactName3;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactPhone3)
	private String ContactPhone3;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactCell3)
	private String ContactCell3;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactExtension3)
	private String ContactExtension3;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactFax3)
	private String ContactFax3;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactEmail3)
	private String ContactEmail3;
	
	

	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactName4)
	private String ContactName4;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactPhone4)
	private String ContactPhone4;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactCell4)
	private String ContactCell4;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactFax4)
	private String ContactFax4;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactExtension4)
	private String ContactExtension4;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ContactEmail4)
	private String ContactEmail4;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_MainFax)
	private String MainFax;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_MainEmail)
	private String MainEmail;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillToZipCode)
	private String BillToZipCode;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_BillingControlNumber,updatable=false)
	private Long BillingControlNumber;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ThemeMobile)
	private String ThemeMobile;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ThemeRF)
	private String ThemeRF;

	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_ThemeFullDisplay)
	private String ThemeFullDisplay;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_PurchageOrdersRequireApproval)
	private String PurchageOrdersRequireApproval;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_Logo)
	private String Logo;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_SetUpSelected)
	private String SetUpSelected;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_SetUpDate,updatable=false)
	private Date SetUpDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_SetUpBy,updatable=false)
	private String SetUpBy;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_LastActivityDate)
	private Date LastActivityDate;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_LastActivityTeamMember)
	private String LastActivityTeamMember;

	@Column(name=GLOBALCONSTANT.DataBase_Companies_TimeZone)
	private String TimeZone;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_Status)
	private String Status;
	
	
	

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getTimeZone() {
		return TimeZone;
	}

	public void setTimeZone(String timeZone) {
		TimeZone = timeZone;
	}

	public String getAddressLine4() {
		return AddressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		AddressLine4 = addressLine4;
	}

	public String getBillToAddressLine4() {
		return BillToAddressLine4;
	}

	public void setBillToAddressLine4(String billToAddressLine4) {
		BillToAddressLine4 = billToAddressLine4;
	}

	public String getContactName1() {
		return ContactName1;
	}

	public void setContactName1(String contactName1) {
		ContactName1 = contactName1;
	}

	public String getContactExtension2() {
		return ContactExtension2;
	}

	public void setContactExtension2(String contactExtension2) {
		ContactExtension2 = contactExtension2;
	}

	public String getContactExtension3() {
		return ContactExtension3;
	}

	public void setContactExtension3(String contactExtension3) {
		ContactExtension3 = contactExtension3;
	}

	public String getContactExtension4() {
		return ContactExtension4;
	}

	public void setContactExtension4(String contactExtension4) {
		ContactExtension4 = contactExtension4;
	}

	public String getBillToZipCode() {
		return BillToZipCode;
	}

	public void setBillToZipCode(String billToZipCode) {
		BillToZipCode = billToZipCode;
	}

	public String getName20() {
		return Name20;
	}


	public void setName20(String name20) {
		Name20 = name20;
	}


	public String getName50() {
		return Name50;
	}


	public void setName50(String name50) {
		Name50 = name50;
	}


	public String getAddressLine1() {
		return AddressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}


	public String getAddressLine2() {
		return AddressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}


	public String getAddressLine3() {
		return AddressLine3;
	}


	public void setAddressLine3(String addressLine3) {
		AddressLine3 = addressLine3;
	}


	public String getCity() {
		return City;
	}


	public void setCity(String city) {
		City = city;
	}


	public String getStateCode() {
		return StateCode;
	}


	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}


	public String getCountryCode() {
		return CountryCode;
	}


	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}


	public String getZipCode() {
		return ZipCode;
	}


	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}


	public String getBillToName20() {
		return BillToName20;
	}


	public void setBillToName20(String billToName20) {
		BillToName20 = billToName20;
	}


	public String getBillToName50() {
		return BillToName50;
	}


	public void setBillToName50(String billToName50) {
		BillToName50 = billToName50;
	}


	public String getBillToAddressLine1() {
		return BillToAddressLine1;
	}


	public void setBillToAddressLine1(String billToAddressLine1) {
		BillToAddressLine1 = billToAddressLine1;
	}


	public String getBillToAddressLine2() {
		return BillToAddressLine2;
	}


	public void setBillToAddressLine2(String billToAddressLine2) {
		BillToAddressLine2 = billToAddressLine2;
	}


	public String getBillToAddressLine3() {
		return BillToAddressLine3;
	}


	public void setBillToAddressLine3(String billToAddressLine3) {
		BillToAddressLine3 = billToAddressLine3;
	}


	public String getBillToFromCity() {
		return BillToFromCity;
	}


	public void setBillToFromCity(String billToFromCity) {
		BillToFromCity = billToFromCity;
	}


	public String getBillToStateCode() {
		return BillToStateCode;
	}


	public void setBillToStateCode(String billToStateCode) {
		BillToStateCode = billToStateCode;
	}


	public String getBillToCountryCode() {
		return BillToCountryCode;
	}


	public void setBillToCountryCode(String billToCountryCode) {
		BillToCountryCode = billToCountryCode;
	}


	public String getContactPhone1() {
		return ContactPhone1;
	}


	public void setContactPhone1(String contactPhone1) {
		ContactPhone1 = contactPhone1;
	}


	public String getContactExtension1() {
		return ContactExtension1;
	}


	public void setContactExtension1(String contactExtension1) {
		ContactExtension1 = contactExtension1;
	}


	public String getContactCell1() {
		return ContactCell1;
	}


	public void setContactCell1(String contactCell1) {
		ContactCell1 = contactCell1;
	}


	public String getContactFax1() {
		return ContactFax1;
	}


	public void setContactFax1(String contactFax1) {
		ContactFax1 = contactFax1;
	}


	public String getContactEmail1() {
		return ContactEmail1;
	}


	public void setContactEmail1(String contactEmail1) {
		ContactEmail1 = contactEmail1;
	}


	public String getContactName2() {
		return ContactName2;
	}


	public void setContactName2(String contactName2) {
		ContactName2 = contactName2;
	}


	public String getContactPhone2() {
		return ContactPhone2;
	}


	public void setContactPhone2(String contactPhone2) {
		ContactPhone2 = contactPhone2;
	}


	public String getContactCell2() {
		return ContactCell2;
	}


	public void setContactCell2(String contactCell2) {
		ContactCell2 = contactCell2;
	}


	public String getContactFax2() {
		return ContactFax2;
	}


	public void setContactFax2(String contactFax2) {
		ContactFax2 = contactFax2;
	}


	public String getContactEmail2() {
		return ContactEmail2;
	}


	public void setContactEmail2(String contactEmail2) {
		ContactEmail2 = contactEmail2;
	}


	public String getContactName3() {
		return ContactName3;
	}


	public void setContactName3(String contactName3) {
		ContactName3 = contactName3;
	}


	public String getContactPhone3() {
		return ContactPhone3;
	}


	public void setContactPhone3(String contactPhone3) {
		ContactPhone3 = contactPhone3;
	}


	public String getContactCell3() {
		return ContactCell3;
	}


	public void setContactCell3(String contactCell3) {
		ContactCell3 = contactCell3;
	}


	public String getContactFax3() {
		return ContactFax3;
	}


	public void setContactFax3(String contactFax3) {
		ContactFax3 = contactFax3;
	}


	public String getContactEmail3() {
		return ContactEmail3;
	}


	public void setContactEmail3(String contactEmail3) {
		ContactEmail3 = contactEmail3;
	}


	public String getContactName4() {
		return ContactName4;
	}


	public void setContactName4(String contactName4) {
		ContactName4 = contactName4;
	}


	public String getContactPhone4() {
		return ContactPhone4;
	}


	public void setContactPhone4(String contactPhone4) {
		ContactPhone4 = contactPhone4;
	}


	public String getContactCell4() {
		return ContactCell4;
	}


	public void setContactCell4(String contactCell4) {
		ContactCell4 = contactCell4;
	}


	public String getContactFax4() {
		return ContactFax4;
	}


	public void setContactFax4(String contactFax4) {
		ContactFax4 = contactFax4;
	}


	public String getContactEmail4() {
		return ContactEmail4;
	}


	public void setContactEmail4(String contactEmail4) {
		ContactEmail4 = contactEmail4;
	}


	public String getMainFax() {
		return MainFax;
	}


	public void setMainFax(String mainFax) {
		MainFax = mainFax;
	}


	public String getMainEmail() {
		return MainEmail;
	}


	public void setMainEmail(String mainEmail) {
		MainEmail = mainEmail;
	}


	public Long getBillingControlNumber() {
		return BillingControlNumber;
	}


	public void setBillingControlNumber(Long billingControlNumber) {
		BillingControlNumber = billingControlNumber;
	}


	public String getThemeMobile() {
		return ThemeMobile;
	}


	public void setThemeMobile(String themeMobile) {
		ThemeMobile = themeMobile;
	}


	public String getThemeRF() {
		return ThemeRF;
	}


	public void setThemeRF(String themeRF) {
		ThemeRF = themeRF;
	}


	public String getThemeFullDisplay() {
		return ThemeFullDisplay;
	}


	public void setThemeFullDisplay(String themeFullDisplay) {
		ThemeFullDisplay = themeFullDisplay;
	}


	public String getPurchageOrdersRequireApproval() {
		return PurchageOrdersRequireApproval;
	}


	public void setPurchageOrdersRequireApproval(
			String purchageOrdersRequireApproval) {
		PurchageOrdersRequireApproval = purchageOrdersRequireApproval;
	}


	public String getLogo() {
		return Logo;
	}


	public void setLogo(String logo) {
		Logo = logo;
	}


	public String getSetUpSelected() {
		return SetUpSelected;
	}


	public void setSetUpSelected(String setUpSelected) {
		SetUpSelected = setUpSelected;
	}


	public Date getSetUpDate() {
		return SetUpDate;
	}


	public void setSetUpDate(Date setUpDate) {
		SetUpDate = setUpDate;
	}


	public String getSetUpBy() {
		return SetUpBy;
	}


	public void setSetUpBy(String setUpBy) {
		SetUpBy = setUpBy;
	}


	public Date getLastActivityDate() {
		return LastActivityDate;
	}


	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}


	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}


	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	
	
	
	
}
