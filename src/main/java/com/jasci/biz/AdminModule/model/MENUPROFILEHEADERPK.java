/**


Date Developed  Nov 26 2014
Created by: Sarvendra Tyagi
Description  pojo  key class of Menu profile header  in which getter and setter methods
 */


package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;


@Embeddable
public class MENUPROFILEHEADERPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_Tenant)
	private String Tenant;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_MenuProfile)
	private String MenuProfile;

	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getMenuProfile() {
		return MenuProfile;
	}

	public void setMenuProfile(String menuProfile) {
		MenuProfile = menuProfile;
	}

}
