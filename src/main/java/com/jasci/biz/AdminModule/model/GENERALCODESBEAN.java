/*

Date Developed  Sep 18 2014
Description bean class of General_Codes in which getter and setter methods for all feilds include primary keys and others
Created By Aakash Bishnoi
Created Date Oct 8 2014
 */
package com.jasci.biz.AdminModule.model;
public class GENERALCODESBEAN  {
	
	int intKendoID;
	
	public int getIntKendoID() {
		return intKendoID;
	}
	public void setIntKendoID(int intKendoID) {
		this.intKendoID = intKendoID;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getApplication() {
		return Application;
	}
	public void setApplication(String application) {
		Application = application;
	}
	public String getGeneralCodeID() {
		return GeneralCodeID;
	}
	public void setGeneralCodeID(String generalCodeID) {
		GeneralCodeID = generalCodeID;
	}
	public String getGeneralCode() {
		return GeneralCode;
	}
	public void setGeneralCode(String generalCode) {
		GeneralCode = generalCode;
	}
	public String getSystemUse() {
		return SystemUse;
	}
	public void setSystemUse(String systemUse) {
		SystemUse = systemUse;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getMenuOptionName() {
		return MenuOptionName;
	}
	public void setMenuOptionName(String menuOptionName) {
		MenuOptionName = menuOptionName;
	}

	public String getControl01Description() {
		return Control01Description;
	}
	public void setControl01Description(String control01Description) {
		Control01Description = control01Description;
	}
	public String getControl01Value() {
		return Control01Value;
	}
	public void setControl01Value(String control01Value) {
		Control01Value = control01Value;
	}
	public String getControl02Description() {
		return Control02Description;
	}
	public void setControl02Description(String control02Description) {
		Control02Description = control02Description;
	}
	public String getControl02Value() {
		return Control02Value;
	}
	public void setControl02Value(String control02Value) {
		Control02Value = control02Value;
	}
	public String getControl03Description() {
		return Control03Description;
	}
	public void setControl03Description(String control03Description) {
		Control03Description = control03Description;
	}
	public String getControl03Value() {
		return Control03Value;
	}
	public void setControl03Value(String control03Value) {
		Control03Value = control03Value;
	}
	public String getControl04Description() {
		return Control04Description;
	}
	public void setControl04Description(String control04Description) {
		Control04Description = control04Description;
	}
	public String getControl04Value() {
		return Control04Value;
	}
	public void setControl04Value(String control04Value) {
		Control04Value = control04Value;
	}
	public String getControl05Description() {
		return Control05Description;
	}
	public void setControl05Description(String control05Description) {
		Control05Description = control05Description;
	}
	public String getControl05Value() {
		return Control05Value;
	}
	public void setControl05Value(String control05Value) {
		Control05Value = control05Value;
	}
	public String getControl06Description() {
		return Control06Description;
	}
	public void setControl06Description(String control06Description) {
		Control06Description = control06Description;
	}
	public String getControl06Value() {
		return Control06Value;
	}
	public void setControl06Value(String control06Value) {
		Control06Value = control06Value;
	}
	public String getControl07Description() {
		return Control07Description;
	}
	public void setControl07Description(String control07Description) {
		Control07Description = control07Description;
	}
	public String getControl07Value() {
		return Control07Value;
	}
	public void setControl07Value(String control07Value) {
		Control07Value = control07Value;
	}
	public String getControl08Description() {
		return Control08Description;
	}
	public void setControl08Description(String control08Description) {
		Control08Description = control08Description;
	}
	public String getControl08Value() {
		return Control08Value;
	}
	public void setControl08Value(String control08Value) {
		Control08Value = control08Value;
	}
	public String getControl09Description() {
		return Control09Description;
	}
	public void setControl09Description(String control09Description) {
		Control09Description = control09Description;
	}
	public String getControl09Value() {
		return Control09Value;
	}
	public void setControl09Value(String control09Value) {
		Control09Value = control09Value;
	}
	public String getControl10Description() {
		return Control10Description;
	}
	public void setControl10Description(String control10Description) {
		Control10Description = control10Description;
	}
	public String getControl10Value() {
		return Control10Value;
	}
	public void setControl10Value(String control10Value) {
		Control10Value = control10Value;
	}
	public String getHelpline() {
		return Helpline;
	}
	public void setHelpline(String helpline) {
		Helpline = helpline;
	}
	/*public String getHelpline2() {
		return Helpline2;
	}
	public void setHelpline2(String helpline2) {
		Helpline2 = helpline2;
	}
	public String getHelpline3() {
		return Helpline3;
	}
	public void setHelpline3(String helpline3) {
		Helpline3 = helpline3;
	}
	public String getHelpline4() {
		return Helpline4;
	}
	public void setHelpline4(String helpline4) {
		Helpline4 = helpline4;
	}
	public String getHelpline5() {
		return Helpline5;
	}
	public void setHelpline5(String helpline5) {
		Helpline5 = helpline5;
	}*/
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	
	
	public String Tenant;
	public String Company;	
	public String Application;
	public String GeneralCodeID;
	public String GeneralCode;
	
	private String SystemUse;
	
	
	private String Description20;
	
	
	private String Description50;
	
	
	private String MenuOptionName;
	
	
	private int ControlNumber01;
	
	
	private int ControlNumber02;
	
	
	private int ControlNumber03;
	
	
	private int ControlNumber04;
	
	
	private int ControlNumber05;
	
	
	private int ControlNumber06;
	
	
	private int ControlNumber07;
	
	
	private int ControlNumber08;
	
	
	private int ControlNumber09;
	
	
	private int ControlNumber10;
	
	
	private String Control01Description;
	
	
	public int getControlNumber01() {
		return ControlNumber01;
	}
	public void setControlNumber01(int controlNumber01) {
		ControlNumber01 = controlNumber01;
	}
	public int getControlNumber02() {
		return ControlNumber02;
	}
	public void setControlNumber02(int controlNumber02) {
		ControlNumber02 = controlNumber02;
	}
	public int getControlNumber03() {
		return ControlNumber03;
	}
	public void setControlNumber03(int controlNumber03) {
		ControlNumber03 = controlNumber03;
	}
	public int getControlNumber04() {
		return ControlNumber04;
	}
	public void setControlNumber04(int controlNumber04) {
		ControlNumber04 = controlNumber04;
	}
	public int getControlNumber05() {
		return ControlNumber05;
	}
	public void setControlNumber05(int controlNumber05) {
		ControlNumber05 = controlNumber05;
	}
	public int getControlNumber06() {
		return ControlNumber06;
	}
	public void setControlNumber06(int controlNumber06) {
		ControlNumber06 = controlNumber06;
	}
	public int getControlNumber07() {
		return ControlNumber07;
	}
	public void setControlNumber07(int controlNumber07) {
		ControlNumber07 = controlNumber07;
	}
	public int getControlNumber08() {
		return ControlNumber08;
	}
	public void setControlNumber08(int controlNumber08) {
		ControlNumber08 = controlNumber08;
	}
	public int getControlNumber09() {
		return ControlNumber09;
	}
	public void setControlNumber09(int controlNumber09) {
		ControlNumber09 = controlNumber09;
	}
	public int getControlNumber10() {
		return ControlNumber10;
	}
	public void setControlNumber10(int controlNumber10) {
		ControlNumber10 = controlNumber10;
	}
	private String Control01Value;
	
	
	private String Control02Description;
	
	
	private String Control02Value;
	
	
	private String Control03Description;
	
	
	private String Control03Value;
	
	
	private String Control04Description;
	
	
	private String Control04Value;
	
	
	private String Control05Description;
	
	
	private String Control05Value;
	
	
	private String Control06Description;
	
	
	private String Control06Value;
	
	
	private String Control07Description;
	
	
	private String Control07Value;
	
	
	private String Control08Description;
	
	
	private String Control08Value;
	
	
	private String Control09Description;
	
	
	private String Control09Value;
	

	private String Control10Description;
	
	
	private String Control10Value;
	
	
	private String Helpline ;
	
	
	
	/*private String Helpline2 ;
	
	
	private String Helpline3 ;

	private String Helpline4 ;
	private String Helpline5 ;*/
	private String LastActivityDate;	
	private String LastActivityTeamMember;
}

