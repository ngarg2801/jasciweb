/*
Created by: Shailendra Rajput
Description Bean class of LPNSPK in which getter and setter methods only for primary keys.
Created On:Oct 12 2015
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class LPNSPK implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DATABASE_TENANT_ID)
	private String Tenant_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_COMPANY_ID)
	private String Company_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_FULFILLMENT_CENTER_ID)
	private String	Fulfillment_Center_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_LPN)
	private String LPN;
	
	public String getTenant_ID() {
		return Tenant_ID;
	}
	public void setTenant_ID(String tenant_ID) {
		Tenant_ID = tenant_ID;
	}
	public String getCompany_ID() {
		return Company_ID;
	}
	public void setCompany_ID(String company_ID) {
		Company_ID = company_ID;
	}
	public String getFulfillment_Center_ID() {
		return Fulfillment_Center_ID;
	}
	public void setFulfillment_Center_ID(String fulfillment_Center_ID) {
		Fulfillment_Center_ID = fulfillment_Center_ID;
	}
	public String getLPN() {
		return LPN;
	}
	public void setLPN(String lPN) {
		LPN = lPN;
	}
	
	
	
}
