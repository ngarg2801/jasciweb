/*


Date Developed  Nov 26 2014
Created by: Sarvendra Tyagi
Description  pojo  key class of Menu profile Assigment  in which getter and setter methods
 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import org.hibernate.annotations.NamedNativeQuery;
import com.jasci.common.constant.GLOBALCONSTANT;

@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfileAssigment , query=GLOBALCONSTANT.NativeQueryName_MenuProfileAssigment_query, 
resultSetMapping=GLOBALCONSTANT.NativeQueryName_MenuProfileAssigment_ResultSetMapping)
	
	@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_MenuProfileAssigment_ResultSetMapping ,
	columns ={@ColumnResult(name=GLOBALCONSTANT.ColumnName_Profile),
			@ColumnResult(name=GLOBALCONSTANT.ColumnName_APPICON),
			@ColumnResult(name=GLOBALCONSTANT.ColumnName_APPICONADDRESS),
			@ColumnResult(name=GLOBALCONSTANT.ColumnName_Help)})

@Entity
@Table(name=GLOBALCONSTANT.TabelName_MenuProfileAssignment)
public class MENUPROFILEASSIGNMENT {
	
	@EmbeddedId
	private MENUPROFILEASSIGMENTPK Id;
	
	public MENUPROFILEASSIGMENTPK getId() {
		return Id;
	}

	public void setId(MENUPROFILEASSIGMENTPK id) {
		Id = id;
	}
	

	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileAssignment_LastActivityDate)
	private Date LastActivityDate;

	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileAssignment_LastActivityTeamMember)
	private String LastActivityTeamMember;

	

	
	public Date getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

}
