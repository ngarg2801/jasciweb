/*

Date Developed  Oct 15 2014
Description create getter and setter method for the class INVENTORY_LEVELS only primary fields
Created By Pradeep Kumar
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

/**
 * @author HP-USA-004
 *
 */
@Embeddable
public class INVENTORY_LEVELSPK  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Tenant)
	private String Tenant;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_FulfillmentCenter)
	private String FulfillmentCenter;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Company)
	private String Company;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Area)
	private String Area;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Location)
	private String Location;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Product)
	private String Product;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Quality)
	private String Quality;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Lot)
	private String Lot;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial01)
	private String Serial01;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial02)
	private String Serial02;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial03)
	private String Serial03;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial04)
	private String Serial04;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial05)
	private String Serial05;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial06)
	private String Serial06;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial07)
	private String Serial07;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial08)
	private String Serial08;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial09)
	private String Serial09;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial10)
	private String Serial10;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial11)
	private String Serial11;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_Serial12)
	private String Serial12;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_LPN)
	private String LPN;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl01)
	private String InventroryControl01;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl02)
	private String InventroryControl02;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl03)
	private String InventroryControl03;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl04)
	private String InventroryControl04;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl05)
	private String InventroryControl05;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl06)
	private String InventroryControl06;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl07)
	private String InventroryControl07;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl08)
	private String InventroryControl08;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl09)
	private String InventroryControl09;
	@Column(name=GLOBALCONSTANT.DataBase_INVENTORY_LEVELS_InventroryControl10)
	private String InventroryControl10;
	
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getProduct() {
		return Product;
	}
	public void setProduct(String product) {
		Product = product;
	}
	public String getQuality() {
		return Quality;
	}
	public void setQuality(String quality) {
		Quality = quality;
	}
	public String getLot() {
		return Lot;
	}
	public void setLot(String lot) {
		Lot = lot;
	}
	public String getSerial01() {
		return Serial01;
	}
	public void setSerial01(String serial01) {
		Serial01 = serial01;
	}
	public String getSerial02() {
		return Serial02;
	}
	public void setSerial02(String serial02) {
		Serial02 = serial02;
	}
	public String getSerial03() {
		return Serial03;
	}
	public void setSerial03(String serial03) {
		Serial03 = serial03;
	}
	public String getSerial04() {
		return Serial04;
	}
	public void setSerial04(String serial04) {
		Serial04 = serial04;
	}
	public String getSerial05() {
		return Serial05;
	}
	public void setSerial05(String serial05) {
		Serial05 = serial05;
	}
	public String getSerial06() {
		return Serial06;
	}
	public void setSerial06(String serial06) {
		Serial06 = serial06;
	}
	public String getSerial07() {
		return Serial07;
	}
	public void setSerial07(String serial07) {
		Serial07 = serial07;
	}
	public String getSerial08() {
		return Serial08;
	}
	public void setSerial08(String serial08) {
		Serial08 = serial08;
	}
	public String getSerial09() {
		return Serial09;
	}
	public void setSerial09(String serial09) {
		Serial09 = serial09;
	}
	public String getSerial10() {
		return Serial10;
	}
	public void setSerial10(String serial10) {
		Serial10 = serial10;
	}
	public String getSerial11() {
		return Serial11;
	}
	public void setSerial11(String serial11) {
		Serial11 = serial11;
	}
	public String getSerial12() {
		return Serial12;
	}
	public void setSerial12(String serial12) {
		Serial12 = serial12;
	}
	public String getLPN() {
		return LPN;
	}
	public void setLPN(String lPN) {
		LPN = lPN;
	}
	public String getInventroryControl01() {
		return InventroryControl01;
	}
	public void setInventroryControl01(String inventroryControl01) {
		InventroryControl01 = inventroryControl01;
	}
	public String getInventroryControl02() {
		return InventroryControl02;
	}
	public void setInventroryControl02(String inventroryControl02) {
		InventroryControl02 = inventroryControl02;
	}
	public String getInventroryControl03() {
		return InventroryControl03;
	}
	public void setInventroryControl03(String inventroryControl03) {
		InventroryControl03 = inventroryControl03;
	}
	public String getInventroryControl04() {
		return InventroryControl04;
	}
	public void setInventroryControl04(String inventroryControl04) {
		InventroryControl04 = inventroryControl04;
	}
	public String getInventroryControl05() {
		return InventroryControl05;
	}
	public void setInventroryControl05(String inventroryControl05) {
		InventroryControl05 = inventroryControl05;
	}
	public String getInventroryControl06() {
		return InventroryControl06;
	}
	public void setInventroryControl06(String inventroryControl06) {
		InventroryControl06 = inventroryControl06;
	}
	public String getInventroryControl07() {
		return InventroryControl07;
	}
	public void setInventroryControl07(String inventroryControl07) {
		InventroryControl07 = inventroryControl07;
	}
	public String getInventroryControl08() {
		return InventroryControl08;
	}
	public void setInventroryControl08(String inventroryControl08) {
		InventroryControl08 = inventroryControl08;
	}
	public String getInventroryControl09() {
		return InventroryControl09;
	}
	public void setInventroryControl09(String inventroryControl09) {
		InventroryControl09 = inventroryControl09;
	}
	public String getInventroryControl10() {
		return InventroryControl10;
	}
	public void setInventroryControl10(String inventroryControl10) {
		InventroryControl10 = inventroryControl10;
	}
	
}
