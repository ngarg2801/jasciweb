/**
 * 
 * @ file_name: EXECUTIONSEQUENCEINSTSPK
 * @Developed by:Pradeep kumar
 *@Created Date:31March 2015
 *Purpose : Pojo Used for Mapping with table EXECUTION_SEQUENCE_INSTS
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Executions_Sequence_Insts)

public class EXECUTIONSEQUENCEINSTS {

	@EmbeddedId
	private EXECUTIONSEQUENCEINSTSPK Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Sequence_Type)
	private String EXECUTION_SEQUENCE_TYPE;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Name)
	private String EXECUTION_NAME;
	
	@Column(name=GLOBALCONSTANT.DataBase_Action_Name)
	private String ACTION_NAME;
	
	@Column(name=GLOBALCONSTANT.DataBase_Comment)
	private String COMMENT;
	
	@Column(name=GLOBALCONSTANT.DataBase_Goto_Sequence_Number)
	private long GOTO_SEQUENCE_NUMBER;
	
	@Column(name=GLOBALCONSTANT.DataBase_Message)
	private String MESSAGE;

	public EXECUTIONSEQUENCEINSTSPK getId() {
		return Id;
	}

	public void setId(EXECUTIONSEQUENCEINSTSPK id) {
		Id = id;
	}

	public String getEXECUTION_SEQUENCE_TYPE() {
		return EXECUTION_SEQUENCE_TYPE;
	}

	public void setEXECUTION_SEQUENCE_TYPE(String eXECUTION_SEQUENCE_TYPE) {
		EXECUTION_SEQUENCE_TYPE = eXECUTION_SEQUENCE_TYPE;
	}

	public String getEXECUTION_NAME() {
		return EXECUTION_NAME;
	}

	public void setEXECUTION_NAME(String eXECUTION_NAME) {
		EXECUTION_NAME = eXECUTION_NAME;
	}

	public String getACTION_NAME() {
		return ACTION_NAME;
	}

	public void setACTION_NAME(String aCTION_NAME) {
		ACTION_NAME = aCTION_NAME;
	}

	public String getCOMMENT() {
		return COMMENT;
	}

	public void setCOMMENT(String cOMMENT) {
		COMMENT = cOMMENT;
	}

	public long getGOTO_SEQUENCE_NUMBER() {
		return GOTO_SEQUENCE_NUMBER;
	}

	public void setGOTO_SEQUENCE_NUMBER(long gOTO_SEQUENCE_NUMBER) {
		GOTO_SEQUENCE_NUMBER = gOTO_SEQUENCE_NUMBER;
	}

	public String getMESSAGE() {
		return MESSAGE;
	}

	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	
	
}
