/**
 * 
 * @ file_name: EXCHANGERATES
 * @Developed by:Pradeep kumar
 *@Created Date:1 April 2015
 *Purpose : Pojo Used for Mapping with table EXCHANGE_RATES
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Exchange_Rates)

public class EXCHANGERATES {
	

	@EmbeddedId
	private EXCHANGERATESPK Id;
	
	@Column (name=GLOBALCONSTANT.DataBase_Status)
	private Date TO_DATE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Status)
	private String EXCHANGE_RATE;
	
	@Column (name=GLOBALCONSTANT.DataBase_Status)
	private char STATUS;
	
	
	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;

	@Column (name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	public EXCHANGERATESPK getId() {
		return Id;
	}

	public void setId(EXCHANGERATESPK id) {
		Id = id;
	}

	public Date getTO_DATE() {
		return TO_DATE;
	}

	public void setTO_DATE(Date tO_DATE) {
		TO_DATE = tO_DATE;
	}

	public String getEXCHANGE_RATE() {
		return EXCHANGE_RATE;
	}

	public void setEXCHANGE_RATE(String eXCHANGE_RATE) {
		EXCHANGE_RATE = eXCHANGE_RATE;
	}

	public char getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(char sTATUS) {
		STATUS = sTATUS;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	
	
}
