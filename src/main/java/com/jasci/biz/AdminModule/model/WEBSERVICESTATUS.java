/*

Date Developed  Oct 15 2014
Description   create the getter setter of WEBSERVICESTATUS class
Created By Deepak Sharma
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

public class WEBSERVICESTATUS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String StrStatus,StrMessage;
	boolean BoolStatus;
	public String getStrStatus() {
		return StrStatus;
	}
	public void setStrStatus(String strStatus) {
		StrStatus = strStatus;
	}
	public String getStrMessage() {
		return StrMessage;
	}
	public void setStrMessage(String strMessage) {
		StrMessage = strMessage;
	}
	public boolean isBoolStatus() {
		return BoolStatus;
	}
	public void setBoolStatus(boolean boolStatus) {
		BoolStatus = boolStatus;
	} 
	
}
