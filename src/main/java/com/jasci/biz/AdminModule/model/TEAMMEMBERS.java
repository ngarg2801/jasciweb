/*

Date Developed  Sep 18 2014
Description pojo class of TeamMembers in which getter and setter methods and mapping with table
Created By Diksha Gupta
Created Date Oct 30 2014
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.TableName_TeamMembers)

@NamedNativeQueries({
	@NamedNativeQuery(
			//fetch all team member based on tenant
	name = GLOBALCONSTANT.TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_ConditionalQuery,
	query = GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_ConditionalQuery,
        resultClass = TEAMMEMBERS.class
	),
	
	@NamedNativeQuery(
			//fetch all team member based on its part of name
	name = GLOBALCONSTANT.TeamMembers_QueryNamed_SelectTeamMember_PartOfTeamMember_ConditionalQuery,
	query = GLOBALCONSTANT.TeamMembers_SelectTeamMember_PartOfTeamMember_ConditionalQuery,
        resultClass = TEAMMEMBERS.class
	),
	
	@NamedNativeQuery(
			//fetch all team member ordered by last activity date
	name = GLOBALCONSTANT.TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_OrderByActivityDateConditionalQuery,
	query = GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_OrderByActivityDateConditionalQuery,
        resultClass = TEAMMEMBERS.class
	),
	
	@NamedNativeQuery(
			//fetch all team member order by last name
	name = GLOBALCONSTANT.TeamMembers_QueryNamed_SelectTeamMember_ALLPartOfTeamMember_LastNameConditionalQuery,
	query = GLOBALCONSTANT.TeamMembers_SelectTeamMember_ALLPartOfTeamMember_LastNameConditionalQuery,
        resultClass = TEAMMEMBERS.class
	),

	@NamedNativeQuery(
			//fetch all team member  based on its name of part order by last name
	name = GLOBALCONSTANT.TeamMembers_QueryNamed_SelectTeamMember_AllTeamMember_OrderByLastNameConditionalQuery,
	query = GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_OrderByLastNameConditionalQuery,
        resultClass = TEAMMEMBERS.class
	),
	
	@NamedNativeQuery(
			//fetch email id from teammembers tables 
	name = GLOBALCONSTANT.TeamMembers_QueryNamed_Select_Email_Query,
	query = GLOBALCONSTANT.TeamMembers_Select_Email_Query,
        resultClass = TEAMMEMBERS.class
	),
	
	@NamedNativeQuery(
			//check team member availability
	name = GLOBALCONSTANT.TeamMembers_QueryNamed_Availability_Query,
	query = GLOBALCONSTANT.TeamMembers_Availability_Query,
        resultClass = TEAMMEMBERS.class
	),
	
	})

public class TEAMMEMBERS 
{
	@EmbeddedId
	private TEAMMEMBERSPK Id;
	
	public TEAMMEMBERSPK getId() {
		return Id;
	}

	public void setId(TEAMMEMBERSPK id) {
		Id = id;
	}
	

	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_FULFILLMENT_CENTER_ID)	
	private String FulfillmentCenterId;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Department)	
	private String Department;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_LastName)	
	private String LastName;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_FirstName)	
	private String FirstName;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_MiddleName )	
	private String MiddleName;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_AddressLine1)	
	private String AddressLine1;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_AddressLine2)	
	private String AddressLine2;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_AddressLine3)	
	private String AddressLine3;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_AddressLine4)	
	private String AddressLine4;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_City)
	private String City;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_StateCode)	
	private String StateCode;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_CountryCode)
	private String CountryCode;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_ZipCode)
	private String ZipCode;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_WorkPhone)
	private String WorkPhone;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_WorkExtension)
	private String WorkExtension;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_WorkCell)
	private String WorkCell;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_WorkFax)
	private String WorkFax;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_HomePhone)
	private String HomePhone;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Cell)
	private String Cell;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_EmergencyContactName)
	private String EmergencyContactName;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_EmergencyContactHomePhone)
	private String EmergencyContactHomePhone;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_EmergencyContactCell)
	private String EmergencyContactCell;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_ShiftCode)
	private String ShiftCode;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_SystemUse)
	private String SystemUse;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_AuthorityProfile)
	private String AuthorityProfile;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_TaskProfile)
	private String TaskProfile;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Menu_Profile_Glass)
	private String Menu_Profile_Glass;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Menu_Profile_Tablet)
	private String Menu_Profile_Tablet;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Menu_Profile_Mobile)
	private String Menu_Profile_Mobile;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Menu_Profile_Station)
	private String Menu_Profile_Station;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Menu_Profile_RF)
	private String Menu_Profile_RF;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_EquipmentCertification)
	private String EquipmentCertification;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_Language)
	private String Language;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_StartDate)
	private Date StartDate;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_LastDate)
	private Date LastDate;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_SetupDate,updatable=false)
	private Date SetupDate;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_SetupBy,updatable=false)
	private String SetupBy;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_LastActivityDate)
	private Date LastActivityDate;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_LastActivityTeamMember)
	private String LastActivityTeamMember;
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_CurrentStatus)
	private String CurrentStatus;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_PersonalEmailAddress)
	private String PersonalEmailAddress;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_WORKEMAIL)
	private String WorkEmailAddress;
	
	@Column(name=GLOBALCONSTANT.DataBase_TeamMembers_EMERGENCYEMAIL)
	private String EmergencyEmailAddress;
	
	
	
	
	
	public String getFulfillmentCenterId() {
		return FulfillmentCenterId;
	}

	public void setFulfillmentCenterId(String fulfillmentCenterId) {
		FulfillmentCenterId = fulfillmentCenterId;
	}

	public String getWorkEmailAddress() {
		return WorkEmailAddress;
	}

	public void setWorkEmailAddress(String workEmailAddress) {
		WorkEmailAddress = workEmailAddress;
	}

	public String getEmergencyEmailAddress() {
		return EmergencyEmailAddress;
	}

	public void setEmergencyEmailAddress(String emergencyEmailAddress) {
		EmergencyEmailAddress = emergencyEmailAddress;
	}

	public String getDepartment() {
		return Department;
	}
	public void setDepartment(String department) {
		Department = department;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}
	public String getAddressLine1() {
		return AddressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return AddressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return AddressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		AddressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return AddressLine4;
	}
	public void setAddressLine4(String addressLine4) {
		AddressLine4 = addressLine4;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getStateCode() {
		return StateCode;
	}
	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getZipCode() {
		return ZipCode;
	}
	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}
	public String getWorkPhone() {
		return WorkPhone;
	}
	public void setWorkPhone(String workPhone) {
		WorkPhone = workPhone;
	}
	public String getWorkExtension() {
		return WorkExtension;
	}
	public void setWorkExtension(String workExtension) {
		WorkExtension = workExtension;
	}
	public String getWorkCell() {
		return WorkCell;
	}
	public void setWorkCell(String workCell) {
		WorkCell = workCell;
	}
	public String getWorkFax() {
		return WorkFax;
	}
	public void setWorkFax(String workFax) {
		WorkFax = workFax;
	}
	public String getHomePhone() {
		return HomePhone;
	}
	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}
	public String getCell() {
		return Cell;
	}
	public void setCell(String cell) {
		Cell = cell;
	}
	public String getEmergencyContactName() {
		return EmergencyContactName;
	}
	public void setEmergencyContactName(String emergencyContactName) {
		EmergencyContactName = emergencyContactName;
	}
	public String getEmergencyContactHomePhone() {
		return EmergencyContactHomePhone;
	}
	public void setEmergencyContactHomePhone(String emergencyContactHomePhone) {
		EmergencyContactHomePhone = emergencyContactHomePhone;
	}
	public String getEmergencyContactCell() {
		return EmergencyContactCell;
	}
	public void setEmergencyContactCell(String emergencyContactCell) {
		EmergencyContactCell = emergencyContactCell;
	}
	public String getShiftCode() {
		return ShiftCode;
	}
	public void setShiftCode(String shiftCode) {
		ShiftCode = shiftCode;
	}
	public String getSystemUse() {
		return SystemUse;
	}
	public void setSystemUse(String systemUse) {
		SystemUse = systemUse;
	}
	public String getAuthorityProfile() {
		return AuthorityProfile;
	}
	public void setAuthorityProfile(String authorityProfile) {
		AuthorityProfile = authorityProfile;
	}
	public String getTaskProfile() {
		return TaskProfile;
	}
	public void setTaskProfile(String taskProfile) {
		TaskProfile = taskProfile;
	}
	public String getMenu_Profile_Glass() {
		return Menu_Profile_Glass;
	}
	public void setMenu_Profile_Glass(String menu_Profile_Glass) {
		Menu_Profile_Glass = menu_Profile_Glass;
	}
	public String getMenu_Profile_Tablet() {
		return Menu_Profile_Tablet;
	}
	public void setMenu_Profile_Tablet(String menu_Profile_Tablet) {
		Menu_Profile_Tablet = menu_Profile_Tablet;
	}
	public String getMenu_Profile_Mobile() {
		return Menu_Profile_Mobile;
	}
	public void setMenu_Profile_Mobile(String menu_Profile_Mobile) {
		Menu_Profile_Mobile = menu_Profile_Mobile;
	}
	public String getMenu_Profile_Station() {
		return Menu_Profile_Station;
	}
	public void setMenu_Profile_Station(String menu_Profile_Station) {
		Menu_Profile_Station = menu_Profile_Station;
	}
	public String getMenu_Profile_RF() {
		return Menu_Profile_RF;
	}
	public void setMenu_Profile_RF(String menu_Profile_RF) {
		Menu_Profile_RF = menu_Profile_RF;
	}
	public String getEquipmentCertification() {
		return EquipmentCertification;
	}
	public void setEquipmentCertification(String equipmentCertification) {
		EquipmentCertification = equipmentCertification;
	}
	public String getLanguage() {
		return Language;
	}
	public void setLanguage(String language) {
		Language = language;
	}
	public Date getStartDate() {
		return StartDate;
	}
	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}
	public Date getLastDate() {
		return LastDate;
	}
	public void setLastDate(Date lastDate) {
		LastDate = lastDate;
	}
	public Date getSetupDate() {
		return SetupDate;
	}
	public void setSetupDate(Date setupDate) {
		SetupDate = setupDate;
	}
	public String getSetupBy() {
		return SetupBy;
	}
	public void setSetupBy(String setupBy) {
		SetupBy = setupBy;
	}
	public Date getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getCurrentStatus() {
		return CurrentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		CurrentStatus = currentStatus;
	}
	public String getPersonalEmailAddress() {
		return PersonalEmailAddress;
	}
	public void setPersonalEmailAddress(String personalEmailAddress) {
		PersonalEmailAddress = personalEmailAddress;
	}

	

}
