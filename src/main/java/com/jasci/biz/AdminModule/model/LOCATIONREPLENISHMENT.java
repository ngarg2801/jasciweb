/*


Created by: Pradeep Kumar
Description  pojo class of LOCATION_REPLENISHMENT Table in which getter and setter
Created On:Jan 16 2015
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity 
@Table(name=GLOBALCONSTANT.TableName_LOCATION_REPLENISHMENTS)
public class LOCATIONREPLENISHMENT {
	
	@EmbeddedId
	private LOCATIONREPLENISHMENTPK Id;

	
	
	@Column(name=GLOBALCONSTANT.DataBase_MINIMUM_INVENTORY_LEVEL)
	private String MINIMUM_INVENTORY_LEVEL;
	@Column(name=GLOBALCONSTANT.DataBase_MAXIMUM_QTY)
	private String MAXIMUM_QTY;
	@Column(name=GLOBALCONSTANT.DataBase_MAXIMUM_NUMBER_OF_CASES)
	private String MAXIMUM_NUMBER_OF_CASES;
	@Column(name=GLOBALCONSTANT.DataBase_MAXIMUM_NUMBER_OF_PALLETS)
	private String MAXIMUM_NUMBER_OF_PALLETS;
	@Column(name=GLOBALCONSTANT.DataBase_RE_STOCK_TYPE)
	private String RE_STOCK_TYPE;
	@Column(name=GLOBALCONSTANT.DataBase_RE_STOCK_MODEL)
	private String RE_STOCK_MODEL;
	@Column(name=GLOBALCONSTANT.DataBase_GENERATED_TYPE)
	private String GENERATED_TYPE;
	@Column(name=GLOBALCONSTANT.DataBase_LAST_ACTIVITY_DATE)
	private String LAST_ACTIVITY_DATE;
	@Column(name=GLOBALCONSTANT.DataBase_LAST_ACTIVITY_TEAM_MEMBER)
	private String LAST_ACTIVITY_TEAM_MEMBER;
	
	
	public LOCATIONREPLENISHMENTPK getId() {
		return Id;
	}
	public void setId(LOCATIONREPLENISHMENTPK id) {
		Id = id;
	}
	public String getMINIMUM_INVENTORY_LEVEL() {
		return MINIMUM_INVENTORY_LEVEL;
	}
	public void setMINIMUM_INVENTORY_LEVEL(String mINIMUM_INVENTORY_LEVEL) {
		MINIMUM_INVENTORY_LEVEL = mINIMUM_INVENTORY_LEVEL;
	}
	public String getMAXIMUM_QTY() {
		return MAXIMUM_QTY;
	}
	public void setMAXIMUM_QTY(String mAXIMUM_QTY) {
		MAXIMUM_QTY = mAXIMUM_QTY;
	}
	public String getMAXIMUM_NUMBER_OF_CASES() {
		return MAXIMUM_NUMBER_OF_CASES;
	}
	public void setMAXIMUM_NUMBER_OF_CASES(String mAXIMUM_NUMBER_OF_CASES) {
		MAXIMUM_NUMBER_OF_CASES = mAXIMUM_NUMBER_OF_CASES;
	}
	public String getMAXIMUM_NUMBER_OF_PALLETS() {
		return MAXIMUM_NUMBER_OF_PALLETS;
	}
	public void setMAXIMUM_NUMBER_OF_PALLETS(String mAXIMUM_NUMBER_OF_PALLETS) {
		MAXIMUM_NUMBER_OF_PALLETS = mAXIMUM_NUMBER_OF_PALLETS;
	}
	public String getRE_STOCK_TYPE() {
		return RE_STOCK_TYPE;
	}
	public void setRE_STOCK_TYPE(String rE_STOCK_TYPE) {
		RE_STOCK_TYPE = rE_STOCK_TYPE;
	}
	public String getRE_STOCK_MODEL() {
		return RE_STOCK_MODEL;
	}
	public void setRE_STOCK_MODEL(String rE_STOCK_MODEL) {
		RE_STOCK_MODEL = rE_STOCK_MODEL;
	}
	public String getGENERATED_TYPE() {
		return GENERATED_TYPE;
	}
	public void setGENERATED_TYPE(String gENERATED_TYPE) {
		GENERATED_TYPE = gENERATED_TYPE;
	}
	public String getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}
	public void setLAST_ACTIVITY_DATE(String lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}
	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}
	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	
	

}
