
/**


Date Developed  Nov 26 2014
Created by: Sarvendra Tyagi
Description  pojo  key class of Menu profile header  in which getter and setter methods
 */


package com.jasci.biz.AdminModule.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.jasci.common.constant.GLOBALCONSTANT;


/*
@NamedNativeQueries({
@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions , query=GLOBALCONSTANT.NativeQueryName_MenuOptionProfile_query, 
resultSetMapping=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions_ResultSetMapping),

@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptionsList , query=GLOBALCONSTANT.NativeQueryName_MenuOptionProfile_MEnuProfile_query, 
resultSetMapping=GLOBALCONSTANT.NativeQueryName_CheckingMenu_Profile_Option_ResultSet)


})	


@SqlResultSetMappings({
	@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions_ResultSetMapping ,
	columns ={@ColumnResult(name=GLOBALCONSTANT.DataBase_MenuOptions_MenuOption)
			}),
			
			@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_CheckingMenu_Profile_Option_ResultSet ,
			columns ={@ColumnResult(name=GLOBALCONSTANT.DataBase_MENU_PROFILE_COLUMN)
					})	

})*/
@Entity
@Table(name=GLOBALCONSTANT.TableName_MenuProfileOption)
public class MENUPROFILEOPTIONS {
	
	@EmbeddedId
	private MENUPROFILEOPTIONPK Id;
	
	public MENUPROFILEOPTIONPK getId() {
		return Id;
	}

	public void setId(MENUPROFILEOPTIONPK id) {
		Id = id;
	}
	
	
	
	
	

}
