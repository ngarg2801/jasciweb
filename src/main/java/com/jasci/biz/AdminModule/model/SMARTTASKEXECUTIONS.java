
/**


Created by: Shailendra Rajput
Description   pojo class of SMART_TASK_EXECUTIONS in which getter and setter methods 
Created On:Mar 30 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;



import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_Smart_Task_Executions)

/*@NamedNativeQueries({
	//It is used to check Esecution Name is Assign or not into SMART_TASK_SEQ_INSTRUCTIONS
	@NamedNativeQuery(			
	name = GLOBALCONSTANT.SmartTaskExecutions_QueryNameSelectExecutionNameAssign,
	query = GLOBALCONSTANT.SmartTaskExecutions_QuerySelectExecutionNameAssign,
        resultClass = SMARTTASKSEQINSTRUCTIONS.class
	),*/
	@NamedNativeQueries({
		//It is used to fetch data from SMART_TASK_EXECUTIONS TABLE behalf of primary keys
		@NamedNativeQuery(			
		name = GLOBALCONSTANT.SmartTaskExecutions_QueryNameFetchExecution,
		query = GLOBALCONSTANT.SmartTaskExecutions_QueryFetchExecution,
	        resultClass = SMARTTASKEXECUTIONS.class
		)
})
public class SMARTTASKEXECUTIONS {
	
	  
	@EmbeddedId
	private SMARTTASKEXECUTIONSPK Id;
	
 	@Column(name=GLOBALCONSTANT.DataBase_Application_Id)
	 private String Application_Id;
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Group)
	 private String Execution_Group;
	@Column(name=GLOBALCONSTANT.DataBase_Description20)
	 private String Description20;
	@Column(name=GLOBALCONSTANT.DataBase_Description50)
	 private String Description50;
	@Column(name=GLOBALCONSTANT.DataBase_Help_Line)
	 private String Help_Line;
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Type)
	 private String Execution_Type;
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Device)
	 private String Execution_Device;
	@Column(name=GLOBALCONSTANT.DataBase_Button)
	 private String Button;
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Path)
	 private String Execution;
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
	 private Date Last_Activity_Date;
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	 private String Last_Activity_Team_Member;
	@Column(name=GLOBALCONSTANT.DataBase_Button_Name)
	 private String Button_Name;
	//CHANGES ACCORDING TO NEW REQUIREMENT
	@Column(name=GLOBALCONSTANT.DATABASE_PARAMETERS_SYSTEM_KEY)
	 private double Parameters_System_Key;
	
	public SMARTTASKEXECUTIONSPK getId() {
		return Id;
	}
	public void setId(SMARTTASKEXECUTIONSPK id) {
		Id = id;
	}
	public String getApplication_Id() {
		return Application_Id;
	}
	public void setApplication_Id(String application_Id) {
		Application_Id = application_Id;
	}
	public String getExecution_Group() {
		return Execution_Group;
	}
	public void setExecution_Group(String execution_Group) {
		Execution_Group = execution_Group;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getHelp_Line() {
		return Help_Line;
	}
	public void setHelp_Line(String help_Line) {
		Help_Line = help_Line;
	}
	public String getExecution_Type() {
		return Execution_Type;
	}
	public void setExecution_Type(String execution_Type) {
		Execution_Type = execution_Type;
	}
	public String getExecution_Device() {
		return Execution_Device;
	}
	public void setExecution_Device(String execution_Device) {
		Execution_Device = execution_Device;
	}
	public String getButton() {
		return Button;
	}
	public void setButton(String button) {
		Button = button;
	}
	public String getExecution() {
		return Execution;
	}
	public void setExecution(String execution) {
		Execution = execution;
	}
	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	public String getButton_Name() {
		return Button_Name;
	}
	public void setButton_Name(String button_Name) {
		Button_Name = button_Name;
	}
	public double getParameters_System_Key() {
		return Parameters_System_Key;
	}
	public void setParameters_System_Key(double parameters_System_Key) {
		Parameters_System_Key = parameters_System_Key;
	}
	
	
	
}
