/**
 * 
 * @ file_name: PURORDTERMSANDCONDSPK
 * @Developed by:Pradeep kumar
 *@Created Date:Apr 1 2015
 *Purpose : Pojo Used for Mapping with table PUR_ORD_TERMS_AND_CONDS
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class PURORDTERMSANDCONDSPK implements Serializable {


	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Tenant_Id)
	private String TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
	private String COMPANY_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Sequence_Number)
	private long SEQUENCE_NUMBER;
	
	@Column(name=GLOBALCONSTANT.DataBase_Side)
	private String SIDE;

	public String getTENANT_ID() {
		return TENANT_ID;
	}

	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	public long getSEQUENCE_NUMBER() {
		return SEQUENCE_NUMBER;
	}

	public void setSEQUENCE_NUMBER(long sEQUENCE_NUMBER) {
		SEQUENCE_NUMBER = sEQUENCE_NUMBER;
	}

	public String getSIDE() {
		return SIDE;
	}

	public void setSIDE(String sIDE) {
		SIDE = sIDE;
	}
	
	
}
