/*

Date Developed  Nov 20 2014
Created by: Aakash Bishnoi
Description  pojo class of Location Table only for primary keys
Created On:Dec 23 2014
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class LOCATIONPK  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name=GLOBALCONSTANT.DataBase_Location_TENANT_ID)
	private String Tenant_ID;
	@Column(name=GLOBALCONSTANT.DataBase_Location_FULFILLMENT_CENTER_ID)
	private String Fullfillment_Center_ID;
	@Column(name=GLOBALCONSTANT.DataBase_Location_COMPANY_ID)
	private String Company_ID;
	@Column(name=GLOBALCONSTANT.DataBase_Location_AREA)
	private String Area;
	@Column(name=GLOBALCONSTANT.DataBase_Location_LOCATION)
	private String Location;

	public String getTenant_ID() {
		return Tenant_ID;
	}
	public void setTenant_ID(String tenant_ID) {
		Tenant_ID = tenant_ID;
	}
	public String getFullfillment_Center_ID() {
		return Fullfillment_Center_ID;
	}
	public void setFullfillment_Center_ID(String fullfillment_Center_ID) {
		Fullfillment_Center_ID = fullfillment_Center_ID;
	}
	public String getCompany_ID() {
		return Company_ID;
	}
	public void setCompany_ID(String company_ID) {
		Company_ID = company_ID;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	
	
	
	

	
	
}
