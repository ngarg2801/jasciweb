/**
 * 
 * @ file_name: SUPPLIERSPK
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table SUPPLIERS for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class SUPPLIERSPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Tenant_Id)
	private String TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
	private String COMPANY_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Supplier)
	private String SUPPLIER;

	public String getTENANT_ID() {
		return TENANT_ID;
	}

	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	public String getSUPPLIER() {
		return SUPPLIER;
	}

	public void setSUPPLIER(String sUPPLIER) {
		SUPPLIER = sUPPLIER;
	}
	
	
}
