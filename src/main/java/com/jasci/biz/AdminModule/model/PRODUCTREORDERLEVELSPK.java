/**
 * 
 * @ file_name: PRODUCTREORDERLEVELSPK
 * @Developed by:Pradeep kumar
 *@Created Date:Apr 1 2015 
 *Purpose : Pojo Used for Mapping with table PRODUCT_REORDER_LEVELS for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class PRODUCTREORDERLEVELSPK implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Tenant_Id)
	private String TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Company_Id)
	private String COMPANY_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Fulfillment_Center_Id)
	private String FULFILLMENT_CENTER_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Product)
	private String PRODUCT;
	
	@Column(name=GLOBALCONSTANT.DataBase_Quality)
	private String QUALITY;

	public String getTENANT_ID() {
		return TENANT_ID;
	}

	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}

	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}

	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}

	public String getFULFILLMENT_CENTER_ID() {
		return FULFILLMENT_CENTER_ID;
	}

	public void setFULFILLMENT_CENTER_ID(String fULFILLMENT_CENTER_ID) {
		FULFILLMENT_CENTER_ID = fULFILLMENT_CENTER_ID;
	}

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}

	public String getQUALITY() {
		return QUALITY;
	}

	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}
	
	
}
