
/*

Date Developed  Nov 20 2014
Created by: Aakash Bishnoi
Description   pojo class of PRODUCTS in which getter and setter methods 
Created On:Mar 31 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Products)
public class PRODUCTS { 

	@EmbeddedId
	private PRODUCTSPK Id;
	
    @Column(name=GLOBALCONSTANT.DataBase_Description20)
    private String Description20;
    
    @Column(name=GLOBALCONSTANT.DataBase_Description50)
    private String Description50;
    
    @Column(name=GLOBALCONSTANT.DataBase_Measurement_Type)
    private String Measurement_Type;
    
    @Column(name=GLOBALCONSTANT.DataBase_Style_Code)
    private String Style_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Size_Code)
    private String Size_Code;
     
    @Column(name=GLOBALCONSTANT.DataBase_Color_Code)
    private String Color_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Product_Group_Code)
    private String Product_Group_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Product_Sub_Group_Code)
    private String Product_Sub_Group_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Accounting_Code)
    private String Accounting_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Country_Of_Origin)
    private String Country_Of_Origin;
    
    @Column(name=GLOBALCONSTANT.DataBase_Harmonized_Tariff_Code)
    private String Harmonized_Tariff_Code;
 
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type01)
    private String Serial_Type01;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type02)
    private String Serial_Type02;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type03)
    private String Serial_Type03;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type04)
    private String Serial_Type04;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type05)
    private String Serial_Type05;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type06)
    private String Serial_Type06;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type07)
    private String Serial_Type07;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type08)
    private String Serial_Type08;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type09)
    private String Serial_Type09;
    
    @Column(name=GLOBALCONSTANT.DataBase_Serial_Type10)
    private String Serial_Type10;
 
    @Column(name=GLOBALCONSTANT.DataBase_Lot_Control_Product)
    private String Lot_Control_Product;
    
    @Column(name=GLOBALCONSTANT.DataBase_Number_Of_Days_To_Expiration)
    private long Number_Of_Days_To_Expiration;
    
    @Column(name=GLOBALCONSTANT.DataBase_Ship_As_Is)
    private String Ship_As_Is;
    
    @Column(name=GLOBALCONSTANT.DataBase_Drop_Ship_Item_Flag)
    private String Drop_Ship_Item_Flag;
    
    @Column(name=GLOBALCONSTANT.DataBase_Division)
    private String Division;
    
    @Column(name=GLOBALCONSTANT.DataBase_Product_Abc_Codes)
    private String Product_Abc_Codes;
    
    @Column(name=GLOBALCONSTANT.DataBase_Product_Value)
    private double Product_Value;

    @Column(name=GLOBALCONSTANT.DataBase_Date_Sequence)
    private String Date_Sequence;

    @Column(name=GLOBALCONSTANT.DataBase_Returns_Allowed)
    private String Returns_Allowed;
    
    @Column(name=GLOBALCONSTANT.DataBase_Season_Code)
    private String Season_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Requires_Cube)
    private String Requires_Cube;
    
    @Column(name=GLOBALCONSTANT.DataBase_Handling_Code)
    private String Handling_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_First_Received_Date)
    private Date First_Received_Date;
    
    @Column(name=GLOBALCONSTANT.DataBase_Hazmat_Code)
    private String Hazmat_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Secured_Level)
    private String Secured_Level;
    
    @Column(name=GLOBALCONSTANT.DataBase_Non_Stock_Item)
    private String Non_Stock_Item;
    
    @Column(name=GLOBALCONSTANT.DataBase_Case_Pickable)
    private String Case_Pickable;
    
    @Column(name=GLOBALCONSTANT.DataBase_Special_Handling_Code)
    private String Special_Handling_Code;
    
    @Column(name=GLOBALCONSTANT.DataBase_Pallet_Block)
    private long Pallet_Block;
    
    @Column(name=GLOBALCONSTANT.DataBase_Pallet_Tier)
    private long Pallet_Tier;
    
    @Column(name=GLOBALCONSTANT.DataBase_Max_Number_Pallet_Stacked)
    private long Max_Number_Pallet_Stacked;
    
    @Column(name=GLOBALCONSTANT.DataBase_Product_Image_High)
    private String Product_Image_High;
    
    @Column(name=GLOBALCONSTANT.DataBase_Product_Image_Low)
    private String Product_Image_Low;
    
    @Column(name=GLOBALCONSTANT.DataBase_Last_Landed_Cost)
    private double Last_Landed_Cost;
    
    @Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
    private Date Last_Activity_Date;
    
    @Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
    private String Last_Activity_Team_Member;
    
    @Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Task)
    private String Last_Activity_Task;

	public PRODUCTSPK getId() {
		return Id;
	}

	public void setId(PRODUCTSPK id) {
		Id = id;
	}

	public String getDescription20() {
		return Description20;
	}

	public void setDescription20(String description20) {
		Description20 = description20;
	}

	public String getDescription50() {
		return Description50;
	}

	public void setDescription50(String description50) {
		Description50 = description50;
	}

	public String getMeasurement_Type() {
		return Measurement_Type;
	}

	public void setMeasurement_Type(String measurement_Type) {
		Measurement_Type = measurement_Type;
	}

	public String getStyle_Code() {
		return Style_Code;
	}

	public void setStyle_Code(String style_Code) {
		Style_Code = style_Code;
	}

	public String getSize_Code() {
		return Size_Code;
	}

	public void setSize_Code(String size_Code) {
		Size_Code = size_Code;
	}

	public String getColor_Code() {
		return Color_Code;
	}

	public void setColor_Code(String color_Code) {
		Color_Code = color_Code;
	}

	public String getProduct_Group_Code() {
		return Product_Group_Code;
	}

	public void setProduct_Group_Code(String product_Group_Code) {
		Product_Group_Code = product_Group_Code;
	}

	public String getProduct_Sub_Group_Code() {
		return Product_Sub_Group_Code;
	}

	public void setProduct_Sub_Group_Code(String product_Sub_Group_Code) {
		Product_Sub_Group_Code = product_Sub_Group_Code;
	}

	public String getAccounting_Code() {
		return Accounting_Code;
	}

	public void setAccounting_Code(String accounting_Code) {
		Accounting_Code = accounting_Code;
	}

	public String getCountry_Of_Origin() {
		return Country_Of_Origin;
	}

	public void setCountry_Of_Origin(String country_Of_Origin) {
		Country_Of_Origin = country_Of_Origin;
	}

	public String getHarmonized_Tariff_Code() {
		return Harmonized_Tariff_Code;
	}

	public void setHarmonized_Tariff_Code(String harmonized_Tariff_Code) {
		Harmonized_Tariff_Code = harmonized_Tariff_Code;
	}

	public String getSerial_Type01() {
		return Serial_Type01;
	}

	public void setSerial_Type01(String serial_Type01) {
		Serial_Type01 = serial_Type01;
	}

	public String getSerial_Type02() {
		return Serial_Type02;
	}

	public void setSerial_Type02(String serial_Type02) {
		Serial_Type02 = serial_Type02;
	}

	public String getSerial_Type03() {
		return Serial_Type03;
	}

	public void setSerial_Type03(String serial_Type03) {
		Serial_Type03 = serial_Type03;
	}

	public String getSerial_Type04() {
		return Serial_Type04;
	}

	public void setSerial_Type04(String serial_Type04) {
		Serial_Type04 = serial_Type04;
	}

	public String getSerial_Type05() {
		return Serial_Type05;
	}

	public void setSerial_Type05(String serial_Type05) {
		Serial_Type05 = serial_Type05;
	}

	public String getSerial_Type06() {
		return Serial_Type06;
	}

	public void setSerial_Type06(String serial_Type06) {
		Serial_Type06 = serial_Type06;
	}

	public String getSerial_Type07() {
		return Serial_Type07;
	}

	public void setSerial_Type07(String serial_Type07) {
		Serial_Type07 = serial_Type07;
	}

	public String getSerial_Type08() {
		return Serial_Type08;
	}

	public void setSerial_Type08(String serial_Type08) {
		Serial_Type08 = serial_Type08;
	}

	public String getSerial_Type09() {
		return Serial_Type09;
	}

	public void setSerial_Type09(String serial_Type09) {
		Serial_Type09 = serial_Type09;
	}

	public String getSerial_Type10() {
		return Serial_Type10;
	}

	public void setSerial_Type10(String serial_Type10) {
		Serial_Type10 = serial_Type10;
	}

	public String getLot_Control_Product() {
		return Lot_Control_Product;
	}

	public void setLot_Control_Product(String lot_Control_Product) {
		Lot_Control_Product = lot_Control_Product;
	}

	public long getNumber_Of_Days_To_Expiration() {
		return Number_Of_Days_To_Expiration;
	}

	public void setNumber_Of_Days_To_Expiration(long number_Of_Days_To_Expiration) {
		Number_Of_Days_To_Expiration = number_Of_Days_To_Expiration;
	}

	public String getShip_As_Is() {
		return Ship_As_Is;
	}

	public void setShip_As_Is(String ship_As_Is) {
		Ship_As_Is = ship_As_Is;
	}

	public String getDrop_Ship_Item_Flag() {
		return Drop_Ship_Item_Flag;
	}

	public void setDrop_Ship_Item_Flag(String drop_Ship_Item_Flag) {
		Drop_Ship_Item_Flag = drop_Ship_Item_Flag;
	}

	public String getDivision() {
		return Division;
	}

	public void setDivision(String division) {
		Division = division;
	}

	public String getProduct_Abc_Codes() {
		return Product_Abc_Codes;
	}

	public void setProduct_Abc_Codes(String product_Abc_Codes) {
		Product_Abc_Codes = product_Abc_Codes;
	}

	public double getProduct_Value() {
		return Product_Value;
	}

	public void setProduct_Value(double product_Value) {
		Product_Value = product_Value;
	}

	public String getDate_Sequence() {
		return Date_Sequence;
	}

	public void setDate_Sequence(String date_Sequence) {
		Date_Sequence = date_Sequence;
	}

	public String getReturns_Allowed() {
		return Returns_Allowed;
	}

	public void setReturns_Allowed(String returns_Allowed) {
		Returns_Allowed = returns_Allowed;
	}

	public String getSeason_Code() {
		return Season_Code;
	}

	public void setSeason_Code(String season_Code) {
		Season_Code = season_Code;
	}

	public String getRequires_Cube() {
		return Requires_Cube;
	}

	public void setRequires_Cube(String requires_Cube) {
		Requires_Cube = requires_Cube;
	}

	public String getHandling_Code() {
		return Handling_Code;
	}

	public void setHandling_Code(String handling_Code) {
		Handling_Code = handling_Code;
	}

	public Date getFirst_Received_Date() {
		return First_Received_Date;
	}

	public void setFirst_Received_Date(Date first_Received_Date) {
		First_Received_Date = first_Received_Date;
	}

	public String getHazmat_Code() {
		return Hazmat_Code;
	}

	public void setHazmat_Code(String hazmat_Code) {
		Hazmat_Code = hazmat_Code;
	}

	public String getSecured_Level() {
		return Secured_Level;
	}

	public void setSecured_Level(String secured_Level) {
		Secured_Level = secured_Level;
	}

	public String getNon_Stock_Item() {
		return Non_Stock_Item;
	}

	public void setNon_Stock_Item(String non_Stock_Item) {
		Non_Stock_Item = non_Stock_Item;
	}

	public String getCase_Pickable() {
		return Case_Pickable;
	}

	public void setCase_Pickable(String case_Pickable) {
		Case_Pickable = case_Pickable;
	}

	public String getSpecial_Handling_Code() {
		return Special_Handling_Code;
	}

	public void setSpecial_Handling_Code(String special_Handling_Code) {
		Special_Handling_Code = special_Handling_Code;
	}

	public long getPallet_Block() {
		return Pallet_Block;
	}

	public void setPallet_Block(long pallet_Block) {
		Pallet_Block = pallet_Block;
	}

	public long getPallet_Tier() {
		return Pallet_Tier;
	}

	public void setPallet_Tier(long pallet_Tier) {
		Pallet_Tier = pallet_Tier;
	}

	public long getMax_Number_Pallet_Stacked() {
		return Max_Number_Pallet_Stacked;
	}

	public void setMax_Number_Pallet_Stacked(long max_Number_Pallet_Stacked) {
		Max_Number_Pallet_Stacked = max_Number_Pallet_Stacked;
	}

	public String getProduct_Image_High() {
		return Product_Image_High;
	}

	public void setProduct_Image_High(String product_Image_High) {
		Product_Image_High = product_Image_High;
	}

	public String getProduct_Image_Low() {
		return Product_Image_Low;
	}

	public void setProduct_Image_Low(String product_Image_Low) {
		Product_Image_Low = product_Image_Low;
	}

	public double getLast_Landed_Cost() {
		return Last_Landed_Cost;
	}

	public void setLast_Landed_Cost(double last_Landed_Cost) {
		Last_Landed_Cost = last_Landed_Cost;
	}

	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}

	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}

	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}

	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}

	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}

	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}
    
    
}