/**
 * 
 * @ file_name: WORKFLOWSTEPS
* @Developed by:Pradeep Kumar
*@Created Date:31 Mar 2015
*Purpose : Pojo Used for Mapping with table WORK_TYPE_LINES
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name = GLOBALCONSTANT.DataBase_TableName_Work_Type_Lines)
public class WORKTYPELINES {

	@EmbeddedId
	private WORKTYPELINESPK Id;

	@Column(name = GLOBALCONSTANT.DataBase_Work_Group_Zone)
	private String WORK_GROUP_ZONE;

	@Column(name = GLOBALCONSTANT.DataBase_Work_Zone)
	private String WORK_ZONE;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Pick_Sequence)
	private String INVENTORY_PICK_SEQUENCE;

	@Column(name = GLOBALCONSTANT.DataBase_Quantity_Requied)
	private long QUANTITY_REQUIRED;

	@Column(name = GLOBALCONSTANT.DataBase_Quantity_Processed)
	private long QUANTITY_PROCESSED;

	@Column(name = GLOBALCONSTANT.DataBase_Quantity_Could_Not_Process)
	private long QUANTITY_COULD_NOT_PROCESS;

	@Column(name = GLOBALCONSTANT.DataBase_To_Area)
	private String TO_AREA;

	@Column(name = GLOBALCONSTANT.DataBase_To_Location)
	private String TO_LOCATION;

	@Column(name = GLOBALCONSTANT.DataBase_Status)
	private String STATUS;

	@Column(name = GLOBALCONSTANT.DataBase_Last_Sequence_Executed)
	private String LAST_SEQUENCE_EXECUTED;

	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Date)
	private Date LAST_ACTIVITY_DATE;

	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
	private String LAST_ACTIVITY_TEAM_MEMBER;

	@Column(name = GLOBALCONSTANT.DataBase_Last_Activity_Task)
	private String LAST_ACTIVITY_TASK;

	public WORKTYPELINESPK getId() {
		return Id;
	}

	public void setId(WORKTYPELINESPK id) {
		Id = id;
	}

	public String getWORK_GROUP_ZONE() {
		return WORK_GROUP_ZONE;
	}

	public void setWORK_GROUP_ZONE(String wORK_GROUP_ZONE) {
		WORK_GROUP_ZONE = wORK_GROUP_ZONE;
	}

	public String getWORK_ZONE() {
		return WORK_ZONE;
	}

	public void setWORK_ZONE(String wORK_ZONE) {
		WORK_ZONE = wORK_ZONE;
	}

	public String getINVENTORY_PICK_SEQUENCE() {
		return INVENTORY_PICK_SEQUENCE;
	}

	public void setINVENTORY_PICK_SEQUENCE(String iNVENTORY_PICK_SEQUENCE) {
		INVENTORY_PICK_SEQUENCE = iNVENTORY_PICK_SEQUENCE;
	}

	public long getQUANTITY_REQUIRED() {
		return QUANTITY_REQUIRED;
	}

	public void setQUANTITY_REQUIRED(long qUANTITY_REQUIRED) {
		QUANTITY_REQUIRED = qUANTITY_REQUIRED;
	}

	public long getQUANTITY_PROCESSED() {
		return QUANTITY_PROCESSED;
	}

	public void setQUANTITY_PROCESSED(long qUANTITY_PROCESSED) {
		QUANTITY_PROCESSED = qUANTITY_PROCESSED;
	}

	public long getQUANTITY_COULD_NOT_PROCESS() {
		return QUANTITY_COULD_NOT_PROCESS;
	}

	public void setQUANTITY_COULD_NOT_PROCESS(long qUANTITY_COULD_NOT_PROCESS) {
		QUANTITY_COULD_NOT_PROCESS = qUANTITY_COULD_NOT_PROCESS;
	}

	public String getTO_AREA() {
		return TO_AREA;
	}

	public void setTO_AREA(String tO_AREA) {
		TO_AREA = tO_AREA;
	}

	public String getTO_LOCATION() {
		return TO_LOCATION;
	}

	public void setTO_LOCATION(String tO_LOCATION) {
		TO_LOCATION = tO_LOCATION;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getLAST_SEQUENCE_EXECUTED() {
		return LAST_SEQUENCE_EXECUTED;
	}

	public void setLAST_SEQUENCE_EXECUTED(String lAST_SEQUENCE_EXECUTED) {
		LAST_SEQUENCE_EXECUTED = lAST_SEQUENCE_EXECUTED;
	}

	public Date getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}

	public void setLAST_ACTIVITY_DATE(Date lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}

	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}

	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}

	public String getLAST_ACTIVITY_TASK() {
		return LAST_ACTIVITY_TASK;
	}

	public void setLAST_ACTIVITY_TASK(String lAST_ACTIVITY_TASK) {
		LAST_ACTIVITY_TASK = lAST_ACTIVITY_TASK;
	}
	
	
}
