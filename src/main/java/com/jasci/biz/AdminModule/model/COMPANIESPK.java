/*


Date Developed  Nov 20 2014
Created by: Rahul Kumar
Description  pojo class of Companies in which getter and setter methods for primary keys
 */
package com.jasci.biz.AdminModule.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;


@Embeddable
public class COMPANIESPK implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_Tenant)
	private String Tenant;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Companies_Company)
	private String Company;


	public String getTenant() {
		return Tenant;
	}


	public void setTenant(String tenant) {
		Tenant = tenant;
	}


	public String getCompany() {
		return Company;
	}


	public void setCompany(String company) {
		Company = company;
	}
	
	
	

	
}
