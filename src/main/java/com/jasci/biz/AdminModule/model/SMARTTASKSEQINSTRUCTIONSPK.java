/*


Created by: Shailendra Rajput
Description  pojo class of SMART_TASK_SEQ_INSTRUCTIONS Table only for primary keys
Created On:Mar 30 2015
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class SMARTTASKSEQINSTRUCTIONSPK implements Serializable{

private static final long serialVersionUID = 1L;
	

	public String getTenant_Id() {
	return Tenant_Id;
}
public void setTenant_Id(String tenant_Id) {
	Tenant_Id = tenant_Id;
}
public String getCompany_Id() {
	return Company_Id;
}
public void setCompany_Id(String company_Id) {
	Company_Id = company_Id;
}
public long getExecution_Sequence() {
	return Execution_Sequence;
}
public void setExecution_Sequence(long execution_Sequence) {
	Execution_Sequence = execution_Sequence;
}
public String getExecution_Sequence_Name() {
	return Execution_Sequence_Name;
}
public void setExecution_Sequence_Name(String execution_Sequence_Name) {
	Execution_Sequence_Name = execution_Sequence_Name;
}

	@Column(name=GLOBALCONSTANT.DateBase_Tenant_Id)
	 private String Tenant_Id;
	@Column(name=GLOBALCONSTANT.DateBase_Company_Id)
    private String Company_Id;
	@Column(name=GLOBALCONSTANT.DateBase_Execution_Sequence)
    private long Execution_Sequence;
	@Column(name=GLOBALCONSTANT.DateBase_Execution_Sequence_Name)
    private String Execution_Sequence_Name;
	
	
}
