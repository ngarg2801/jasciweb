/**
 * 
 * @ file_name: EXECUTIONS
 * @Developed by:Pradeep kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table EXECUTIONS,in it only primary key mapped
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class EXECUTIONSPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key_Company;
	
	/*@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey_Tenant)
	private COMPANIESPK Foreign_Key_Tenant;*/
	
	@Column(name=GLOBALCONSTANT.DataBase_Application_Id)
	private String APPLICATION_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Group)
	private String EXECUTION_GROUP;
	
	@Column(name=GLOBALCONSTANT.DataBase_Execution_Name)
	private String EXECUTION_NAME;

	public COMPANIESPK getForeign_Key_Company() {
		return Foreign_Key_Company;
	}

	public void setForeign_Key_Company(COMPANIESPK foreign_Key_Company) {
		Foreign_Key_Company = foreign_Key_Company;
	}

	public String getAPPLICATION_ID() {
		return APPLICATION_ID;
	}

	public void setAPPLICATION_ID(String aPPLICATION_ID) {
		APPLICATION_ID = aPPLICATION_ID;
	}

	public String getEXECUTION_GROUP() {
		return EXECUTION_GROUP;
	}

	public void setEXECUTION_GROUP(String eXECUTION_GROUP) {
		EXECUTION_GROUP = eXECUTION_GROUP;
	}

	public String getEXECUTION_NAME() {
		return EXECUTION_NAME;
	}

	public void setEXECUTION_NAME(String eXECUTION_NAME) {
		EXECUTION_NAME = eXECUTION_NAME;
	}
	
	
}
