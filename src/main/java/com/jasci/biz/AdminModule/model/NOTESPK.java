/*

Date Developed  Nov 03 2014
Description  primarykey pojo class of notes in which getter and setter methods
Created By Rakesh pal
 */
package com.jasci.biz.AdminModule.model;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class NOTESPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name=GLOBALCONSTANT.DataBase_Notes_Tenant_Id)
	private String TENANT_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Notes_Company_Id)
	private String COMPANY_ID;

	
	@Column(name=GLOBALCONSTANT.DataBase_Notes_Note_Id)
	private String NOTE_ID;
	
	@Column(name=GLOBALCONSTANT.DataBase_Notes_Note_Link)
	private String NOTE_LINK;
	
	@GeneratedValue(generator = GLOBALCONSTANT.SEC_ODON, strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(sequenceName=GLOBALCONSTANT.NOTES_SEQ, name = GLOBALCONSTANT.SEC_ODON)
	@Column(name=GLOBALCONSTANT.DataBase_Notes_ID)
	private int ID;
	
	
	
	
	public String getTenant_Id() {
		return TENANT_ID;
	}

	public void setTenant_Id(String tenant_Id) {
		TENANT_ID = tenant_Id;
	}
	
	public String getCompany_Id() {
		return COMPANY_ID;
	}

	public void setCompany_Id(String Company_id) {
		COMPANY_ID = Company_id;
	}
	
	public String getNote_Id() {
		return NOTE_ID;
	}

	public void setNote_Id(String note_Id) {
		NOTE_ID = note_Id;
	}
	

	public String getNote_Link() {
		return NOTE_LINK;
	}

	public void setNote_Link(String note_Link) {
		NOTE_LINK = note_Link;
	}
	
	
	public int getID() {
		return ID;
	}

	public void setID(int id) {
		ID = id;
	}
	

}
