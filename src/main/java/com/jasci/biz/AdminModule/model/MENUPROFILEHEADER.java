/**

Date Developed  Oct 16 2014
Description  pojo class of Menu profile header in which getter and setter methods
developed by Sarvendra tyagi
Date : Dec 11 2014
 */




package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@NamedNativeQueries({
	
	@NamedNativeQuery(name=GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_Display_All_QueryName,
			query=GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_Display_All_Query
			,resultClass =MENUPROFILEHEADER.class),
	
	
	
	@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfile_ProfileList_QueryName,query=GLOBALCONSTANT.NativeQuery_MenuProfile_ProfileList_QueryName
			,resultClass =MENUPROFILEHEADER.class),
			
			@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfile_ProfileDisplayAll_QueryName,query=GLOBALCONSTANT.NativeQuery_MenuProfile_ProfileDisplayAll_Query
			,resultClass =MENUPROFILEHEADER.class),
	
			@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfile_ProfileLike_QueryName,query=GLOBALCONSTANT.NativeQuery_MenuProfile_ProfileLike_Query
			,resultClass =MENUPROFILEHEADER.class),
	
			
			@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptionsList , query=GLOBALCONSTANT.NativeQueryName_MenuOptionProfile_MEnuProfile_query, 
			resultSetMapping=GLOBALCONSTANT.NativeQueryName_CheckingMenu_Profile_Option_ResultSet),

			@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfile_Delete_QueryName,query=GLOBALCONSTANT.NativeQueryName_MenuProfileHEADER_Delete_Query
			,resultClass =MENUPROFILEHEADER.class),
	
			@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfileOPTION_Delete_QueryName,query=GLOBALCONSTANT.NativeQueryName_MenuProfileOPTION_Delete_Query
			,resultClass =MENUPROFILEOPTIONS.class),
	
			@NamedNativeQuery(name=GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_QueryName,query=GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_Query
			,resultClass =MENUOPTIONS.class),
	
			@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfile_Assigment_Delete_QueryName,query=GLOBALCONSTANT.NativeQueryName_MenuProfileHEADER_FromAssigment_Delete_Query
			,resultClass =MENUPROFILEASSIGNMENT.class),
	
			
			@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuProfiles_GeneralCode_MenuTypeQueryName,
			query=GLOBALCONSTANT.NativeQueryName_MenuProfiles_GeneralCode_MenuTypeQuery
			,resultClass =GENERALCODES.class)
			
	
})

@SqlResultSetMappings({
	
	@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_CheckingMenu_Profile_Option_ResultSet ,
			columns ={@ColumnResult(name=GLOBALCONSTANT.DataBase_MENU_PROFILE_COLUMN)
					})	

})



@Entity
@Table(name=GLOBALCONSTANT.TabelName_MenuProfileHeader)
public class MENUPROFILEHEADER {
	
	
	@EmbeddedId
	private MENUPROFILEHEADERPK Id;
	
	public MENUPROFILEHEADERPK getId() {
		return Id;
	}

	public void setId(MENUPROFILEHEADERPK id) {
		Id = id;
	}
	
	
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_MenuType)
	private String MenuType;
	
	
	
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_Description20)
	private String Description20;
	
	
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_Description50)
	private String Description50;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_AppIcon)
	private String AppIcon;
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_HelpLine)
	private String HelpLine;
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_LastActivityDate)
	private Date LastActivityDate;
	
	
	@Column(name=GLOBALCONSTANT.DataBase_MenuProfileHeader_LastActivityTeamMember)
	private String LastActivityTeamMember;




	public String getMenuType() {
		return MenuType;
	}


	public void setMenuType(String menuType) {
		MenuType = menuType;
	}


	


	public String getDescription20() {
		return Description20;
	}


	public void setDescription20(String description20) {
		Description20 = description20;
	}


	public String getDescription50() {
		return Description50;
	}


	public void setDescription50(String description50) {
		Description50 = description50;
	}


	public String getAppIcon() {
		return AppIcon;
	}


	public void setAppIcon(String appIcon) {
		AppIcon = appIcon;
	}


	public String getHelpLine() {
		return HelpLine;
	}


	public void setHelpLine(String helpLine) {
		HelpLine = helpLine;
	}


	public Date getLastActivityDate() {
		return LastActivityDate;
	}


	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}


	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}


	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	
	

}
