/**
 * 
 * @ file_name: WORKFLOWSTEPSPK
 * @Developed by:Pradeep Kumar
 *@Created Date:31 Mar 2015
 *Purpose : Pojo Used for Mapping with table WORK_TYPE_LINES
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class WORKTYPELINESPK implements Serializable {


	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key;

	@Column(name = GLOBALCONSTANT.DataBase_Fulfillment_Center_Id)
	private String FULFILLMENT_CENTER_ID;

	@Column(name = GLOBALCONSTANT.DataBase_Work_Type)
	private String WORK_TYPE;

	@Column(name = GLOBALCONSTANT.DataBase_Work_Control_Number)
	private String WORK_CONTROL_NUMBER;

	@Column(name = GLOBALCONSTANT.DataBase_Line_Number)
	private long LINE_NUMBER;

	@Column(name = GLOBALCONSTANT.DataBase_Area)
	private String AREA;

	@Column(name = GLOBALCONSTANT.DataBase_Location)
	private String LOCATION;

	@Column(name = GLOBALCONSTANT.DataBase_Product)
	private String PRODUCT;

	@Column(name = GLOBALCONSTANT.DataBase_Quality)
	private String QUALITY;

	@Column(name = GLOBALCONSTANT.DataBase_Lot)
	private String LOT;

	@Column(name = GLOBALCONSTANT.DataBase_Serial01)
	private String SERIAL01;

	@Column(name = GLOBALCONSTANT.DataBase_Serial02)
	private String SERIAL02;

	@Column(name = GLOBALCONSTANT.DataBase_Serial03)
	private String SERIAL03;

	@Column(name = GLOBALCONSTANT.DataBase_Serial04)
	private String SERIAL04;

	@Column(name = GLOBALCONSTANT.DataBase_Serial05)
	private String SERIAL05;

	@Column(name = GLOBALCONSTANT.DataBase_Serial06)
	private String SERIAL06;

	@Column(name = GLOBALCONSTANT.DataBase_Serial07)
	private String SERIAL07;

	@Column(name = GLOBALCONSTANT.DataBase_Serial08)
	private String SERIAL08;

	@Column(name = GLOBALCONSTANT.DataBase_Serial09)
	private String SERIAL09;

	@Column(name = GLOBALCONSTANT.DataBase_Serial10)
	private String SERIAL10;

	@Column(name = GLOBALCONSTANT.DataBase_Lpn)
	private String LPN;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control01)
	private String INVENTORY_CONTROL01;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control02)
	private String INVENTORY_CONTROL02;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control03)
	private String INVENTORY_CONTROL03;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control04)
	private String INVENTORY_CONTROL04;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control05)
	private String INVENTORY_CONTROL05;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control06)
	private String INVENTORY_CONTROL06;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control07)
	private String INVENTORY_CONTROL07;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control08)
	private String INVENTORY_CONTROL08;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control09)
	private String INVENTORY_CONTROL09;

	@Column(name = GLOBALCONSTANT.DataBase_Inventory_Control10)
	private String INVENTORY_CONTROL10;

	public COMPANIESPK getForeign_Key() {
		return Foreign_Key;
	}

	public void setForeign_Key(COMPANIESPK foreign_Key) {
		Foreign_Key = foreign_Key;
	}

	public String getFULFILLMENT_CENTER_ID() {
		return FULFILLMENT_CENTER_ID;
	}

	public void setFULFILLMENT_CENTER_ID(String fULFILLMENT_CENTER_ID) {
		FULFILLMENT_CENTER_ID = fULFILLMENT_CENTER_ID;
	}

	public String getWORK_TYPE() {
		return WORK_TYPE;
	}

	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}

	public String getWORK_CONTROL_NUMBER() {
		return WORK_CONTROL_NUMBER;
	}

	public void setWORK_CONTROL_NUMBER(String wORK_CONTROL_NUMBER) {
		WORK_CONTROL_NUMBER = wORK_CONTROL_NUMBER;
	}

	public long getLINE_NUMBER() {
		return LINE_NUMBER;
	}

	public void setLINE_NUMBER(long lINE_NUMBER) {
		LINE_NUMBER = lINE_NUMBER;
	}

	public String getAREA() {
		return AREA;
	}

	public void setAREA(String aREA) {
		AREA = aREA;
	}

	public String getLOCATION() {
		return LOCATION;
	}

	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}

	public String getPRODUCT() {
		return PRODUCT;
	}

	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}

	public String getQUALITY() {
		return QUALITY;
	}

	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}

	public String getLOT() {
		return LOT;
	}

	public void setLOT(String lOT) {
		LOT = lOT;
	}

	public String getSERIAL01() {
		return SERIAL01;
	}

	public void setSERIAL01(String sERIAL01) {
		SERIAL01 = sERIAL01;
	}

	public String getSERIAL02() {
		return SERIAL02;
	}

	public void setSERIAL02(String sERIAL02) {
		SERIAL02 = sERIAL02;
	}

	public String getSERIAL03() {
		return SERIAL03;
	}

	public void setSERIAL03(String sERIAL03) {
		SERIAL03 = sERIAL03;
	}

	public String getSERIAL04() {
		return SERIAL04;
	}

	public void setSERIAL04(String sERIAL04) {
		SERIAL04 = sERIAL04;
	}

	public String getSERIAL05() {
		return SERIAL05;
	}

	public void setSERIAL05(String sERIAL05) {
		SERIAL05 = sERIAL05;
	}

	public String getSERIAL06() {
		return SERIAL06;
	}

	public void setSERIAL06(String sERIAL06) {
		SERIAL06 = sERIAL06;
	}

	public String getSERIAL07() {
		return SERIAL07;
	}

	public void setSERIAL07(String sERIAL07) {
		SERIAL07 = sERIAL07;
	}

	public String getSERIAL08() {
		return SERIAL08;
	}

	public void setSERIAL08(String sERIAL08) {
		SERIAL08 = sERIAL08;
	}

	public String getSERIAL09() {
		return SERIAL09;
	}

	public void setSERIAL09(String sERIAL09) {
		SERIAL09 = sERIAL09;
	}

	public String getSERIAL10() {
		return SERIAL10;
	}

	public void setSERIAL10(String sERIAL10) {
		SERIAL10 = sERIAL10;
	}

	public String getLPN() {
		return LPN;
	}

	public void setLPN(String lPN) {
		LPN = lPN;
	}

	public String getINVENTORY_CONTROL01() {
		return INVENTORY_CONTROL01;
	}

	public void setINVENTORY_CONTROL01(String iNVENTORY_CONTROL01) {
		INVENTORY_CONTROL01 = iNVENTORY_CONTROL01;
	}

	public String getINVENTORY_CONTROL02() {
		return INVENTORY_CONTROL02;
	}

	public void setINVENTORY_CONTROL02(String iNVENTORY_CONTROL02) {
		INVENTORY_CONTROL02 = iNVENTORY_CONTROL02;
	}

	public String getINVENTORY_CONTROL03() {
		return INVENTORY_CONTROL03;
	}

	public void setINVENTORY_CONTROL03(String iNVENTORY_CONTROL03) {
		INVENTORY_CONTROL03 = iNVENTORY_CONTROL03;
	}

	public String getINVENTORY_CONTROL04() {
		return INVENTORY_CONTROL04;
	}

	public void setINVENTORY_CONTROL04(String iNVENTORY_CONTROL04) {
		INVENTORY_CONTROL04 = iNVENTORY_CONTROL04;
	}

	public String getINVENTORY_CONTROL05() {
		return INVENTORY_CONTROL05;
	}

	public void setINVENTORY_CONTROL05(String iNVENTORY_CONTROL05) {
		INVENTORY_CONTROL05 = iNVENTORY_CONTROL05;
	}

	public String getINVENTORY_CONTROL06() {
		return INVENTORY_CONTROL06;
	}

	public void setINVENTORY_CONTROL06(String iNVENTORY_CONTROL06) {
		INVENTORY_CONTROL06 = iNVENTORY_CONTROL06;
	}

	public String getINVENTORY_CONTROL07() {
		return INVENTORY_CONTROL07;
	}

	public void setINVENTORY_CONTROL07(String iNVENTORY_CONTROL07) {
		INVENTORY_CONTROL07 = iNVENTORY_CONTROL07;
	}

	public String getINVENTORY_CONTROL08() {
		return INVENTORY_CONTROL08;
	}

	public void setINVENTORY_CONTROL08(String iNVENTORY_CONTROL08) {
		INVENTORY_CONTROL08 = iNVENTORY_CONTROL08;
	}

	public String getINVENTORY_CONTROL09() {
		return INVENTORY_CONTROL09;
	}

	public void setINVENTORY_CONTROL09(String iNVENTORY_CONTROL09) {
		INVENTORY_CONTROL09 = iNVENTORY_CONTROL09;
	}

	public String getINVENTORY_CONTROL10() {
		return INVENTORY_CONTROL10;
	}

	public void setINVENTORY_CONTROL10(String iNVENTORY_CONTROL10) {
		INVENTORY_CONTROL10 = iNVENTORY_CONTROL10;
	}

}
