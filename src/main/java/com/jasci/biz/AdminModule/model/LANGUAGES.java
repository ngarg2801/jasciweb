/*

Date Developed  Oct 16 2014
Description  pojo class of Languages in which getter and setter methods
Created By Diksha Gupta
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.EmbeddedId;
import javax.persistence.NamedNativeQueries;
import javax.persistence.Entity;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import com.jasci.common.constant.GLOBALCONSTANT;

@NamedNativeQueries({
	@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuExecutionLabel , query=GLOBALCONSTANT.NativeQueryName_MenuExecutionLabel_query, 
			resultSetMapping=GLOBALCONSTANT.NativeQueryName_MenuExecutionLabel_ResultSetMapping),
				
	@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuOptions_LabelQueryName,query=GLOBALCONSTANT.NativeQueryName_MenuOptions_LabelQuery
			,resultClass =LANGUAGES.class),
	@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_MenuExecution_LabelQueryName,query=GLOBALCONSTANT.NativeQueryName_MenuExecutionLabel_LabelQuery
			,resultClass =LANGUAGES.class)
	,
	@NamedNativeQuery(name=GLOBALCONSTANT.NativeQueryName_CommonLanguageTranslation,query=GLOBALCONSTANT.NativeQuery_CommonLanguageTranslation
			,resultClass =LANGUAGES.class)
})

//select LANGUAGE,KEY_PHRASE,nvl2(TRANSLATION,TRANSLATION,KEY_PHRASE) as TRANSLATION,OBJECT_CODE,OBJECT_FIELD_CODE,LAST_ACTIVITY_DATE,LAST_ACTIVITY_TEAM_MEMBER from languages

@SqlResultSetMapping(name=GLOBALCONSTANT.NativeQueryName_MenuExecutionLabel_ResultSetMapping ,
columns ={@ColumnResult(name=GLOBALCONSTANT.ColumnName_TRANSLATION)})




@Entity
@Table(name=GLOBALCONSTANT.TableName_Languages)
public class LANGUAGES 
{
	@EmbeddedId
	private LANGUAGEPK Id;
	
	public LANGUAGEPK getId() {
		return Id;
	}

	public void setId(LANGUAGEPK id) {
		Id = id;
	}

	@Column(name=GLOBALCONSTANT.DataBase_Languages_Translation)
	private String Translation;
	
	@Column(name=GLOBALCONSTANT.DataBase_Languages_LastActivityDate)
	private Date LastActivityDate;
	
	@Column(name=GLOBALCONSTANT.DataBase_Languages_LastActivityTeamMember)
	private String LastActivityTeamMember;
	
	
	/*private String ObjectFieldCode;
	
	
	private String ObjectCode;
	
	

	public String getObjectFieldCode() {
		return ObjectFieldCode;
	}

	public void setObjectFieldCode(String objectFieldCode) {
		ObjectFieldCode = objectFieldCode;
	}

	public String getObjectCode() {
		return ObjectCode;
	}

	public void setObjectCode(String objectCode) {
		ObjectCode = objectCode;
	}
*/
	
	public String getTranslation() {
		return Translation;
	}

	public void setTranslation(String translation) {
		Translation = translation;
	}

	

	public Date getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	
	
	
}
