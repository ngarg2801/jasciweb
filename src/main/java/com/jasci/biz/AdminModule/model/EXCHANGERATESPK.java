/**
 * 
 * @ file_name: EXCHANGERATESPK
 * @Developed by:Pradeep kumar
 *@Created Date:1 April 2015 
 *Purpose : Pojo Used for Mapping with table EXCHANGE_RATES for primary keys
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class EXCHANGERATESPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey)
	private COMPANIESPK Foreign_Key;
	
	/*@ManyToOne
	@JoinColumn(name=GLOBALCONSTANT.DataBase_ForeignKey_Tenant)
	private COMPANIESPK Foreign_Key_Tenant;*/
	
	
	@Column(name=GLOBALCONSTANT.DataBase_Currence_Code)
	private String CURRENCE_CODE;
	
	@Column(name=GLOBALCONSTANT.DataBase_From_Date)
	private String FROM_DATE;

	public COMPANIESPK getForeign_Key() {
		return Foreign_Key;
	}

	public void setForeign_Key(COMPANIESPK foreign_Key) {
		Foreign_Key = foreign_Key;
	}

	public String getCURRENCE_CODE() {
		return CURRENCE_CODE;
	}

	public void setCURRENCE_CODE(String cURRENCE_CODE) {
		CURRENCE_CODE = cURRENCE_CODE;
	}

	public String getFROM_DATE() {
		return FROM_DATE;
	}

	public void setFROM_DATE(String fROM_DATE) {
		FROM_DATE = fROM_DATE;
	}
	
	
}
