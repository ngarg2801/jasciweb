/*


Created by: Aakash Bishnoi
Description  pojo class of WORK_TYPE_HEADERS Table only for primary keys
Created On:Mar 31 2015
 */
package com.jasci.biz.AdminModule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.jasci.common.constant.GLOBALCONSTANT;

@Embeddable
public class WORKTYPEHEADERSPK implements Serializable{

private static final long serialVersionUID = 1L;

@Column(name=GLOBALCONSTANT.DateBase_Tenant_Id)
private String Tenant_Id;
@Column(name=GLOBALCONSTANT.DateBase_Company_Id)
private String Company_Id;
@Column(name=GLOBALCONSTANT.DataBase_FulFillment_Center_Id)
private String FulFillment_Center_Id;
@Column(name=GLOBALCONSTANT.DataBase_Work_Control_Number)
private long Work_Control_Number;

public String getTenant_Id() {
	return Tenant_Id;
}
public void setTenant_Id(String tenant_Id) {
	Tenant_Id = tenant_Id;
}
public String getCompany_Id() {
	return Company_Id;
}
public void setCompany_Id(String company_Id) {
	Company_Id = company_Id;
}
public String getFulFillment_Center_Id() {
	return FulFillment_Center_Id;
}
public void setFulFillment_Center_Id(String fulFillment_Center_Id) {
	FulFillment_Center_Id = fulFillment_Center_Id;
}
public long getWork_Control_Number() {
	return Work_Control_Number;
}
public void setWork_Control_Number(long work_Control_Number) {
	Work_Control_Number = work_Control_Number;
}


}
