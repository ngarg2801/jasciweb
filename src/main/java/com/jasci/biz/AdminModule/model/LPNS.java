/*
Created by: Shailendra Rajput
Description Bean class of LPNS in which getter and setter methods.
Created On:Oct 12 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DATABASE_TABLENAME_LPNS)
public class LPNS {

	@EmbeddedId
	private LPNSPK Id;
	@Column(name=GLOBALCONSTANT.DATABASE_WORK_CONTROL_NUMBER)
	private double Work_Control_Number;
	@Column(name=GLOBALCONSTANT.DATABASE_WORK_TYPE)
	private String Work_Type;
	@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_TYPE)
	private String Container_Type;
	@Column(name=GLOBALCONSTANT.DATABASE_CONTAINER_ID)
	private String Container_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_CART_ID)
	private String Cart_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_CART_TYPE)
	private String Cart_Type;
	@Column(name=GLOBALCONSTANT.DATABASE_PUT_WALL_ID)
	private String Put_wall_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_PUT_WALL_CUBE_ID)
	private String Put_wall_Cube_ID;
	@Column(name=GLOBALCONSTANT.DATABASE_STATUS)
	private String Status;
	@Column(name=GLOBALCONSTANT.DATABASE_CREATED_BY)
	private Date Created_By;
	@Column(name=GLOBALCONSTANT.DATABASE_DATE_CREATED)
	private Date Date_Created;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_DATE)
	private Date Last_Activity_Date;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_TEAM_MEMBER)
	private String Last_Activity_Team_Member;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_ACTIVITY_TASK)
	private String Last_Activity_Task;
	@Column(name=GLOBALCONSTANT.DATABASE_LAST_DIVERT)
	private String Last_Divert;
	public LPNSPK getId() {
		return Id;
	}
	public void setId(LPNSPK id) {
		Id = id;
	}
	public double getWork_Control_Number() {
		return Work_Control_Number;
	}
	public void setWork_Control_Number(double work_Control_Number) {
		Work_Control_Number = work_Control_Number;
	}
	public String getWork_Type() {
		return Work_Type;
	}
	public void setWork_Type(String work_Type) {
		Work_Type = work_Type;
	}
	public String getContainer_Type() {
		return Container_Type;
	}
	public void setContainer_Type(String container_Type) {
		Container_Type = container_Type;
	}
	public String getContainer_ID() {
		return Container_ID;
	}
	public void setContainer_ID(String container_ID) {
		Container_ID = container_ID;
	}
	public String getCart_ID() {
		return Cart_ID;
	}
	public void setCart_ID(String cart_ID) {
		Cart_ID = cart_ID;
	}
	public String getCart_Type() {
		return Cart_Type;
	}
	public void setCart_Type(String cart_Type) {
		Cart_Type = cart_Type;
	}
	public String getPut_wall_ID() {
		return Put_wall_ID;
	}
	public void setPut_wall_ID(String put_wall_ID) {
		Put_wall_ID = put_wall_ID;
	}
	public String getPut_wall_Cube_ID() {
		return Put_wall_Cube_ID;
	}
	public void setPut_wall_Cube_ID(String put_wall_Cube_ID) {
		Put_wall_Cube_ID = put_wall_Cube_ID;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public Date getCreated_By() {
		return Created_By;
	}
	public void setCreated_By(Date created_By) {
		Created_By = created_By;
	}
	public Date getDate_Created() {
		return Date_Created;
	}
	public void setDate_Created(Date date_Created) {
		Date_Created = date_Created;
	}
	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}
	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}
	public String getLast_Divert() {
		return Last_Divert;
	}
	public void setLast_Divert(String last_Divert) {
		Last_Divert = last_Divert;
	}
	
	
}
