/*

Date Developed  Oct 15 2014
Description   bean class of SetNewpassword in which getter and setter methods
Created By:Diksha gupta
 */

package com.jasci.biz.AdminModule.model;

public class SETNEWPASSWORD 
{

	private String email;
	private String password;
	private String struserId;
	private String UserId;
	
	

	

	public String getStruserId() {
		return struserId;
	}

	public void setStruserId(String struserId) {
		this.struserId = struserId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		this.UserId = userId;
	}
	
	
	
	
	
	
}
