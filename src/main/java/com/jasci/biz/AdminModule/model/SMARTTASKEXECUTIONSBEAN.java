/**


Created by: Shailendra Rajput
Description  Bean class of SMART_TASK_EXECUTIONS in which All fields getter and setter methods.
Created On:Apr 13 2015
 */
package com.jasci.biz.AdminModule.model;



public class SMARTTASKEXECUTIONSBEAN {

	     private String Tenant_Id;
		
	     private String Company_Id;
		
		 private String Execution_Name;
		
		 private String Application_Id;
			
		 private String Execution_Group;
	
		 private String Description20;
		
		 private String Description50;
		
		 private String Help_Line;
		
		 private String Execution_Type;
	
		 private String Execution_Device;
	
		 private String Button;
	
		 private String Execution_Path;
		 
		 private String Button_Name;
		
		 private String Last_Activity_Date;
		
		 private String Last_Activity_Team_Member;
		 
		 private String Execution_Type_Description;
			
		 private String Execution_Device_Description;
		 
		 private String Execution_Group_Description;
		 
		 private String Application_Id_Description; 
		 
		 private String Notes;
		 
		 
		 public String getApplication_Id_Description() {
			return Application_Id_Description;
		}

		public void setApplication_Id_Description(String application_Id_Description) {
			Application_Id_Description = application_Id_Description;
		}

		public String getNotes() {
			return Notes;
		}

		public void setNotes(String notes) {
			Notes = notes;
		}

		public String getExecution_Type_Description() {
			return Execution_Type_Description;
		}

		public void setExecution_Type_Description(String execution_Type_Description) {
			Execution_Type_Description = execution_Type_Description;
		}

		public String getExecution_Device_Description() {
			return Execution_Device_Description;
		}

		public void setExecution_Device_Description(String execution_Device_Description) {
			Execution_Device_Description = execution_Device_Description;
		}

		public String getExecution_Group_Description() {
			return Execution_Group_Description;
		}

		public void setExecution_Group_Description(String execution_Group_Description) {
			Execution_Group_Description = execution_Group_Description;
		}

		
		 
		 public String getTenant_Id() {
			return Tenant_Id;
		}

		public void setTenant_Id(String tenant_Id) {
			Tenant_Id = tenant_Id;
		}

		public String getCompany_Id() {
			return Company_Id;
		}

		public void setCompany_Id(String company_Id) {
			Company_Id = company_Id;
		}

		public String getExecution_Name() {
			return Execution_Name;
		}

		public void setExecution_Name(String execution_Name) {
			Execution_Name = execution_Name;
		}

		public String getApplication_Id() {
			return Application_Id;
		}

		public void setApplication_Id(String application_Id) {
			Application_Id = application_Id;
		}

		public String getExecution_Group() {
			return Execution_Group;
		}

		public void setExecution_Group(String execution_Group) {
			Execution_Group = execution_Group;
		}

		public String getDescription20() {
			return Description20;
		}

		public void setDescription20(String description20) {
			Description20 = description20;
		}

		public String getDescription50() {
			return Description50;
		}

		public void setDescription50(String description50) {
			Description50 = description50;
		}

		public String getHelp_Line() {
			return Help_Line;
		}

		public void setHelp_Line(String help_Line) {
			Help_Line = help_Line;
		}

		public String getExecution_Type() {
			return Execution_Type;
		}

		public void setExecution_Type(String execution_Type) {
			Execution_Type = execution_Type;
		}

		public String getExecution_Device() {
			return Execution_Device;
		}

		public void setExecution_Device(String execution_Device) {
			Execution_Device = execution_Device;
		}

		public String getButton() {
			return Button;
		}

		public void setButton(String button) {
			Button = button;
		}

		public String getLast_Activity_Date() {
			return Last_Activity_Date;
		}

		public void setLast_Activity_Date(String last_Activity_Date) {
			Last_Activity_Date = last_Activity_Date;
		}

		public String getLast_Activity_Team_Member() {
			return Last_Activity_Team_Member;
		}

		public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
			Last_Activity_Team_Member = last_Activity_Team_Member;
		}

		public String getExecution_Path() {
			return Execution_Path;
		}

		public void setExecution_Path(String execution_Path) {
			Execution_Path = execution_Path;
		}

		public String getButton_Name() {
			return Button_Name;
		}

		public void setButton_Name(String button_Name) {
			Button_Name = button_Name;
		}

		
		 
}
