/*

Date Developed  Oct 15 2014
Description   pojo class of SecurityAuthorizations in which getter and setter methods
Created By:Deepak Sharma
 */

package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;
@Entity
@Table(name=GLOBALCONSTANT.TableName_SecurityAuthorizations)


//@NamedNativeQuery(name = "GETSECURITY" , query = GLOBALCONSTANT.SecurityAuthorizations_SelectConditionalQuery, resultClass = SECURITYAUTHORIZATIONS.class)
/**Define named native query*/
@NamedNativeQuery(name = GLOBALCONSTANT.GETCOMMONSESSIONBE , query = GLOBALCONSTANT.SecurityAuthorizations_SelectConditionalQuery , resultSetMapping = 
GLOBALCONSTANT.RESULTSET_COMMONSESSIONBE)

/**Define result set request mapping*/
@SqlResultSetMapping(name =GLOBALCONSTANT.RESULTSET_COMMONSESSIONBE,
columns = { @ColumnResult(name = GLOBALCONSTANT.TENANT),
		@ColumnResult(name = GLOBALCONSTANT.TEAMMEMBER),
		@ColumnResult(name = GLOBALCONSTANT.COMPANY),
		@ColumnResult(name = GLOBALCONSTANT.PASSWORDDATE),
		@ColumnResult(name = GLOBALCONSTANT.STATUS),
		@ColumnResult(name = GLOBALCONSTANT.CNAME20),
		@ColumnResult(name = GLOBALCONSTANT.CNAME50),
		@ColumnResult(name = GLOBALCONSTANT.PURCHASEORDERSREQUIREAPPROVAL),
		@ColumnResult(name = GLOBALCONSTANT.FULFILLMENTCENTER),
		@ColumnResult(name = GLOBALCONSTANT.FCNAME20),
		@ColumnResult(name = GLOBALCONSTANT.FCNAME50),
		@ColumnResult(name = GLOBALCONSTANT.TLANGUAGE),
		@ColumnResult(name = GLOBALCONSTANT.TMLANGUAGE),
		@ColumnResult(name = GLOBALCONSTANT.GIC_THEMERF),
		@ColumnResult(name = GLOBALCONSTANT.GIC_THEMEFULLDISPLAY),
		@ColumnResult(name = GLOBALCONSTANT.PASSWORDEXPIRATIONDAYS),
		@ColumnResult(name = GLOBALCONSTANT.TEAMMEMBERNAME),
		@ColumnResult(name = GLOBALCONSTANT.AUTHORITYPROFILE),
		@ColumnResult(name = GLOBALCONSTANT.MENU_PROFILE_TABLET),
		@ColumnResult(name = GLOBALCONSTANT.MENU_PROFILE_STATION),
		@ColumnResult(name = GLOBALCONSTANT.MENU_PROFILE_RF),
		@ColumnResult(name = GLOBALCONSTANT.MENU_PROFILE_GLASS),
		@ColumnResult(name = GLOBALCONSTANT.EQUIPMENTCERTIFICATION),
		@ColumnResult(name = GLOBALCONSTANT.SYSTEMUSE),
		@ColumnResult(name = GLOBALCONSTANT.PASSWORD),
		@ColumnResult(name=  GLOBALCONSTANT.INVALIDATTEMPTDATE),
		@ColumnResult(name=  GLOBALCONSTANT.NUMBERATTEMPTS),
		@ColumnResult(name=  GLOBALCONSTANT.TNAME20),
		@ColumnResult(name=  GLOBALCONSTANT.TNAME50),
		@ColumnResult(name=  GLOBALCONSTANT.COMPANYSTATUS)})
public class SECURITYAUTHORIZATIONS 
{

	@Id
	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_UserId)
	private String UserId;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_Password)
	private String Password;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_PasswordDate)
	private Date PasswordDate;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant)
	private String Tenant;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_Company)
	private String Company;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_TeamMember)
	private String TeamMember;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_SecurityQuestion1)
	private String SecurityQuestion1;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_SecurityQuestionAnswer1)
	private String SecurityQuestionAnswer1;
	
	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_SecurityQuestion2)
	private String SecurityQuestion2;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_SecurityQuestionAnswer2)
	private String SecurityQuestionAnswer2;
	
	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_SecurityQuestion3)
	private String SecurityQuestion3;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_SecurityQuestionAnswer3)
	private String SecurityQuestionAnswer3;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastPassword1)
	private String LastPassword1;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastPassword2)
	private String LastPassword2;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastPassword3)
	private String LastPassword3;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastPassword4)
	private String LastPassword4;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastPassword5)
	private String LastPassword5;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_NumberAttempts)
	private int NumberAttempts;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_InvalidAttemptDate)
	private Date InvalidAttemptDate;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_Status)
	private String Status;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastActivityDate)
	private Date LastActivityDate;

	@Column(name=GLOBALCONSTANT.DataBase_SecurityAuthorizations_LastActivityTeamMember)
	private String LastActivityTeamMember;
	
	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	

	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getCompany() {
		return Company;
	}

	public void setCompany(String company) {
		Company = company;
	}

	public String getTeamMember() {
		return TeamMember;
	}

	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}



	public String getLastPassword1() {
		return LastPassword1;
	}

	public void setLastPassword1(String lastPassword1) {
		LastPassword1 = lastPassword1;
	}

	public String getLastPassword2() {
		return LastPassword2;
	}

	public void setLastPassword2(String lastPassword2) {
		LastPassword2 = lastPassword2;
	}

	public String getLastPassword3() {
		return LastPassword3;
	}

	public void setLastPassword3(String lastPassword3) {
		LastPassword3 = lastPassword3;
	}

	public String getLastPassword4() {
		return LastPassword4;
	}

	public void setLastPassword4(String lastPassword4) {
		LastPassword4 = lastPassword4;
	}

	public String getLastPassword5() {
		return LastPassword5;
	}

	public void setLastPassword5(String lastPassword5) {
		LastPassword5 = lastPassword5;
	}


	public int getNumberAttempts() {
		return NumberAttempts;
	}

	public void setNumberAttempts(int numberAttempts) {
		NumberAttempts = numberAttempts;
	}

	

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	

	public Date getPasswordDate() {
		return PasswordDate;
	}

	public void setPasswordDate(Date passwordDate) {
		PasswordDate = passwordDate;
	}

	public Date getInvalidAttemptDate() {
		return InvalidAttemptDate;
	}

	public void setInvalidAttemptDate(Date invalidAttemptDate) {
		InvalidAttemptDate = invalidAttemptDate;
	}

	public Date getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	
	

	public String getSecurityQuestion1() {
		return SecurityQuestion1;
	}

	public void setSecurityQuestion1(String securityQuestion1) {
		SecurityQuestion1 = securityQuestion1;
	}

	public String getSecurityQuestionAnswer1() {
		return SecurityQuestionAnswer1;
	}

	public void setSecurityQuestionAnswer1(String securityQuestionAnswer1) {
		SecurityQuestionAnswer1 = securityQuestionAnswer1;
	}

	public String getSecurityQuestion2() {
		return SecurityQuestion2;
	}

	public void setSecurityQuestion2(String securityQuestion2) {
		SecurityQuestion2 = securityQuestion2;
	}

	public String getSecurityQuestionAnswer2() {
		return SecurityQuestionAnswer2;
	}

	public void setSecurityQuestionAnswer2(String securityQuestionAnswer2) {
		SecurityQuestionAnswer2 = securityQuestionAnswer2;
	}

	public String getSecurityQuestion3() {
		return SecurityQuestion3;
	}

	public void setSecurityQuestion3(String securityQuestion3) {
		SecurityQuestion3 = securityQuestion3;
	}

	public String getSecurityQuestionAnswer3() {
		return SecurityQuestionAnswer3;
	}

	public void setSecurityQuestionAnswer3(String securityQuestionAnswer3) {
		SecurityQuestionAnswer3 = securityQuestionAnswer3;
	}

	

	public SECURITYAUTHORIZATIONS() {


	}



}
