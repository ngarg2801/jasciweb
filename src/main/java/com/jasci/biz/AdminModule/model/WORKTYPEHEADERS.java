
/*


Created by: Aakash Bishnoi
Description   pojo class of WORK_TYPE_HEADERS in which getter and setter methods 
Created On:Mar 31 2015
 */
package com.jasci.biz.AdminModule.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;

@Entity
@Table(name=GLOBALCONSTANT.DataBase_TableName_Work_Type_Headers)
public class WORKTYPEHEADERS {

     
	
	@EmbeddedId
	private WORKTYPEHEADERSPK Id;
      
	@Column(name=GLOBALCONSTANT.DataBase_Work_Type)
  	private String Work_Type;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Flow_Id)
  	private String Work_Flow_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Flow_Step)
  	private String Work_Flow_Step;
	
	@Column(name=GLOBALCONSTANT.DataBase_Priority)
  	private String Priority;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Group_Zone)
  	private String Work_Group_Zone;
	
	@Column(name=GLOBALCONSTANT.DataBase_Work_Zone)
  	private String Work_Zone;
	
	@Column(name=GLOBALCONSTANT.DataBase_Sales_Order_Id)
  	private String Sales_Order_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Purchase_order_number)
  	private String Purchase_order_number;
	
	@Column(name=GLOBALCONSTANT.DataBase_Order_Id)
  	private String Order_Id;
	
	@Column(name=GLOBALCONSTANT.DataBase_Accountion_Code)
  	private String Accountion_Code;
	
	@Column(name=GLOBALCONSTANT.DataBase_Created_By)
  	private String Created_By;
	
	@Column(name=GLOBALCONSTANT.DataBase_Date_Created)
  	private Date Date_Created;
	
	@Column(name=GLOBALCONSTANT.DataBase_Status)
  	private String Status;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Date)
  	private Date Last_Activity_Date;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Team_Member)
  	private String Last_Activity_Team_Member;
	
	@Column(name=GLOBALCONSTANT.DataBase_Last_Activity_Task)
  	private String Last_Activity_Task;

	@Column(name=GLOBALCONSTANT.DATABASE_WAVE_ID)
  	private String Wave_ID;
	
	

	public String getWave_ID() {
		return Wave_ID;
	}

	public void setWave_ID(String wave_ID) {
		Wave_ID = wave_ID;
	}

	public WORKTYPEHEADERSPK getId() {
		return Id;
	}

	public void setId(WORKTYPEHEADERSPK id) {
		Id = id;
	}

	public String getWork_Type() {
		return Work_Type;
	}

	public void setWork_Type(String work_Type) {
		Work_Type = work_Type;
	}

	public String getWork_Flow_Id() {
		return Work_Flow_Id;
	}

	public void setWork_Flow_Id(String work_Flow_Id) {
		Work_Flow_Id = work_Flow_Id;
	}

	public String getWork_Flow_Step() {
		return Work_Flow_Step;
	}

	public void setWork_Flow_Step(String work_Flow_Step) {
		Work_Flow_Step = work_Flow_Step;
	}

	public String getPriority() {
		return Priority;
	}

	public void setPriority(String priority) {
		Priority = priority;
	}

	public String getWork_Group_Zone() {
		return Work_Group_Zone;
	}

	public void setWork_Group_Zone(String work_Group_Zone) {
		Work_Group_Zone = work_Group_Zone;
	}

	public String getWork_Zone() {
		return Work_Zone;
	}

	public void setWork_Zone(String work_Zone) {
		Work_Zone = work_Zone;
	}

	public String getSales_Order_Id() {
		return Sales_Order_Id;
	}

	public void setSales_Order_Id(String sales_Order_Id) {
		Sales_Order_Id = sales_Order_Id;
	}

	public String getPurchase_order_number() {
		return Purchase_order_number;
	}

	public void setPurchase_order_number(String purchase_order_number) {
		Purchase_order_number = purchase_order_number;
	}

	public String getOrder_Id() {
		return Order_Id;
	}

	public void setOrder_Id(String order_Id) {
		Order_Id = order_Id;
	}

	public String getAccountion_Code() {
		return Accountion_Code;
	}

	public void setAccountion_Code(String accountion_Code) {
		Accountion_Code = accountion_Code;
	}

	public String getCreated_By() {
		return Created_By;
	}

	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}

	public Date getDate_Created() {
		return Date_Created;
	}

	public void setDate_Created(Date date_Created) {
		Date_Created = date_Created;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}

	public void setLast_Activity_Date(Date last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}

	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}

	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}

	public String getLast_Activity_Task() {
		return Last_Activity_Task;
	}

	public void setLast_Activity_Task(String last_Activity_Task) {
		Last_Activity_Task = last_Activity_Task;
	}

}
