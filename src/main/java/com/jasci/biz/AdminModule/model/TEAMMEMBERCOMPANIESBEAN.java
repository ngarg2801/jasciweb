/*

Date Developed  Sep 18 2014
Description pojo class of TeamMembersCompanies in which getter and setter methods
Created By Diksha Gupta
Created Date Oct 30 2014
 */
package com.jasci.biz.AdminModule.model;



public class TEAMMEMBERCOMPANIESBEAN {
	
	
	
	
	public String Company;
	public String Name20;
	public String Name50;
	
	
	int intKendoID;
	
	public String ComapnyLogo;
	public String PurchaseOrderApproval;
	public String TMobile;
	public String TRF;
	public String TFullDisplay;
	
	
	 public String getTMobile() {
		return TMobile;
	}
	public void setTMobile(String tMobile) {
		TMobile = tMobile;
	}
	public String getTRF() {
		return TRF;
	}
	public void setTRF(String tRF) {
		TRF = tRF;
	}
	public String getTFullDisplay() {
		return TFullDisplay;
	}
	public void setTFullDisplay(String tFullDisplay) {
		TFullDisplay = tFullDisplay;
	}
	public String getComapnyLogo() {
		return ComapnyLogo;
	}
	public void setComapnyLogo(String comapnyLogo) {
		ComapnyLogo = comapnyLogo;
	}
	public String getPurchaseOrderApproval() {
		return PurchaseOrderApproval;
	}
	public void setPurchaseOrderApproval(String purchaseOrderApproval) {
		PurchaseOrderApproval = purchaseOrderApproval;
	}
	public int getIntKendoID() {
	  return intKendoID;
	 }
	 public void setIntKendoID(int intKendoID) {
	  this.intKendoID = intKendoID;
	 }
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getName20() {
		return Name20;
	}
	public void setName20(String name20) {
		Name20 = name20;
	}
	public String getName50() {
		return Name50;
	}
	public void setName50(String name50) {
		Name50 = name50;
	}

}
