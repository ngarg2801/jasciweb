/*

Date Developed  Sep 18 2014
Description pojo class of TeamMembers in which getter and setter methods
Created By Diksha Gupta
Created Date Oct 30 2014
 */
package com.jasci.biz.AdminModule.model;

public class TEAMMEMBERBEAN {
	
	int intKendoID;
	String FirstName;
	String LastName;
	String Tenant;
	String TeamMember;
	String FullName;
	String SeqStatus;
	String TeamMemberStatus;
	String MiddleName;
	
	public String getSeqStatus() {
		return SeqStatus;
	}
	public void setSeqStatus(String seqStatus) {
		SeqStatus = seqStatus;
	}
	public String getTeamMemberStatus() {
		return TeamMemberStatus;
	}
	public void setTeamMemberStatus(String teamMemberStatus) {
		TeamMemberStatus = teamMemberStatus;
	}
	public String getMiddleName() {
		return MiddleName;
	}
	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}
	public String getFullName() {
		return FullName;
	}
	public void setFullName(String fullName) {
		FullName = fullName;
	}
	public int getIntKendoID() {
		return intKendoID;
	}
	public void setIntKendoID(int intKendoID) {
		this.intKendoID = intKendoID;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getTeamMember() {
		return TeamMember;
	}
	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}
	
	

}
