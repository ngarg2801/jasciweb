
/*


Created by: Shailendra Rajput
Description   pojo class of SMART_TASK_SEQ_INSTRUCTIONS in which getter and setter methods 
Created On:Mar 30 2015
 */
package com.jasci.biz.AdminModule.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.jasci.common.constant.GLOBALCONSTANT;


@Entity
@Table(name=GLOBALCONSTANT.TableName_Smart_Task_Seq_Instruction)

public class SMARTTASKSEQINSTRUCTIONS {

	@EmbeddedId
	private SMARTTASKSEQINSTRUCTIONSPK Id;
      
	@Column(name=GLOBALCONSTANT.DateBase_Execution_Sequence_Type)
  	private String Execution_Sequence_Type;
  	
	@Column(name=GLOBALCONSTANT.DateBase_Execution_Name)
	private String Execution_Name;
	
	@Column(name=GLOBALCONSTANT.DateBase_Action_Name)
	private String Action_Name;
	
	@Column(name=GLOBALCONSTANT.DateBase_Comments)
	private String Comments;
	
	@Column(name=GLOBALCONSTANT.DateBase_Goto_Tag)
	private String Goto_Tag;
	
	@Column(name=GLOBALCONSTANT.DateBase_Message)
	private String Message;
	
	@Column(name=GLOBALCONSTANT.DateBase_Custom_Execution)
	 private String Custom_Execution;
	
	@Column(name=GLOBALCONSTANT.DateBase_Return_Code_Value)
    private String Return_Code_Value;
	   
	//For change according to new requirement
	@Column(name=GLOBALCONSTANT.DATABASE_COMPARISON_OPERATOR)
	private String Comparison_Operator;
	@Column(name=GLOBALCONSTANT.DATABASE_COMPARISON_TYPE)
	private String Comparison_Type;
	@Column(name=GLOBALCONSTANT.DATABASE_PARAMETERS_SYSTEM_KEY)
	private double Parameters_System_Key;
	@Column(name=GLOBALCONSTANT.DATABASE_SCREEN_SECTION_ID)
	private String Screen_Section_ID;

	
  	public String getComparison_Operator() {
		return Comparison_Operator;
	}

	public void setComparison_Operator(String comparison_Operator) {
		Comparison_Operator = comparison_Operator;
	}

	public String getComparison_Type() {
		return Comparison_Type;
	}

	public void setComparison_Type(String comparison_Type) {
		Comparison_Type = comparison_Type;
	}

	public double getParameters_System_Key() {
		return Parameters_System_Key;
	}

	public void setParameters_System_Key(double parameters_System_Key) {
		Parameters_System_Key = parameters_System_Key;
	}

	public String getScreen_Section_ID() {
		return Screen_Section_ID;
	}

	public void setScreen_Section_ID(String screen_Section_ID) {
		Screen_Section_ID = screen_Section_ID;
	}

	public String getCustom_Execution() {
		return Custom_Execution;
	}

	public void setCustom_Execution(String custom_Execution) {
		Custom_Execution = custom_Execution;
	}

	public String getReturn_Code_Value() {
		return Return_Code_Value;
	}

	public void setReturn_Code_Value(String return_Code_Value) {
		Return_Code_Value = return_Code_Value;
	}

	public void setComments(String comments) {
		Comments = comments;
	}

	public SMARTTASKSEQINSTRUCTIONSPK getId() {
		return Id;
	}

	public void setId(SMARTTASKSEQINSTRUCTIONSPK id) {
		Id = id;
	}

	public String getExecution_Sequence_Type() {
		return Execution_Sequence_Type;
	}

	public void setExecution_Sequence_Type(String execution_Sequence_Type) {
		Execution_Sequence_Type = execution_Sequence_Type;
	}

	public String getExecution_Name() {
		return Execution_Name;
	}

	public void setExecution_Name(String execution_Name) {
		Execution_Name = execution_Name;
	}

	public String getAction_Name() {
		return Action_Name;
	}

	public void setAction_Name(String action_Name) {
		Action_Name = action_Name;
	}

	public String getComments() {
		return Comments;
	}

	public void setComment(String comments) {
		Comments = comments;
	}

	public String getGoto_Tag() {
		return Goto_Tag;
	}

	public void setGoto_Tag(String goto_Tag) {
		Goto_Tag = goto_Tag;
	}

	public String getMessage() {
		return Message;
	}

	public void setMessage(String message) {
		Message = message;
	}
}
