/*

Date Developed  Oct 16 2014
Description   pojo class of Infohelps in which getter and setter methods with primary keys and others
Created By Aakash Bishnoi
 */

package com.jasci.biz.AdminModule.model;




public class INFOHELPSBEAN 
{
	
	private String InfoHelp;

	
	private String Language;
	
	
	private String InfoHelpType;
	
	
	private String Description20;
	
	
	private String Description50;
	
	
	private String Execution;
	
	
	private String LastActivityDate;
	
	
	private String LastActivityTeamMember;

	private String LanguageCode;
	private String InfoHelpTypeValue;
	
	public String getLanguageCode() {
		return LanguageCode;
	}

	public void setLanguageCode(String languageCode) {
		LanguageCode = languageCode;
	}

	public String getInfoHelpTypeValue() {
		return InfoHelpTypeValue;
	}

	public void setInfoHelpTypeValue(String infoHelpTypeValue) {
		InfoHelpTypeValue = infoHelpTypeValue;
	}

	
	public String getInfoHelp() {
		return InfoHelp;
	}

	public void setInfoHelp(String infoHelp) {
		InfoHelp = infoHelp;
	}

	public String getLanguage() {
		return Language;
	}

	public void setLanguage(String language) {
		Language = language;
	}

	public String getInfoHelpType() {
		return InfoHelpType;
	}

	public void setInfoHelpType(String infoHelpType) {
		InfoHelpType = infoHelpType;
	}

	public String getDescription20() {
		return Description20;
	}

	public void setDescription20(String description20) {
		Description20 = description20;
	}

	public String getDescription50() {
		return Description50;
	}

	public void setDescription50(String description50) {
		Description50 = description50;
	}

	public String getExecution() {
		return Execution;
	}

	public void setExecution(String execution) {
		Execution = execution;
	}

	public String getLastActivityDate() {
		return LastActivityDate;
	}

	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	
	
	
}
