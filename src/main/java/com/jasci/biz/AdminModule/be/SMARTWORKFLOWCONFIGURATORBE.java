/**
Description This class is used for set bean variable
Created By Sarvendra Tyagi 
Created Date APR 27 2015
 */

package com.jasci.biz.AdminModule.be;


public class SMARTWORKFLOWCONFIGURATORBE {
	
	private String tenant;
	private String fulfillmentCenter;
	private String workType;
	private String companyId;
	private String workFlowId;
	private String description20;
	private String description50;
	private String lastActivityDate;
	private String lastTeamMember;
	private String workTypeDesc;
	private String fulfillmentCenterName20;
	private String notes;
	
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getTenant() {
		return tenant;
	}
	public void setTenant(String tenant) {
		this.tenant = tenant;
	}
	public String getFulfillmentCenter() {
		return fulfillmentCenter;
	}
	public void setFulfillmentCenter(String fulfillmentCenter) {
		this.fulfillmentCenter = fulfillmentCenter;
	}
	public String getWorkType() {
		return workType;
	}
	public void setWorkType(String workType) {
		this.workType = workType;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getWorkFlowId() {
		return workFlowId;
	}
	public void setWorkFlowId(String workFlowId) {
		this.workFlowId = workFlowId;
	}
	public String getDescription20() {
		return description20;
	}
	public void setDescription20(String description20) {
		this.description20 = description20;
	}
	public String getDescription50() {
		return description50;
	}
	public void setDescription50(String description50) {
		this.description50 = description50;
	}
	public String getLastActivityDate() {
		return lastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}
	public String getLastTeamMember() {
		return lastTeamMember;
	}
	public void setLastTeamMember(String lastTeamMember) {
		this.lastTeamMember = lastTeamMember;
	}
	public String getWorkTypeDesc() {
		return workTypeDesc;
	}
	public void setWorkTypeDesc(String workTypeDesc) {
		this.workTypeDesc = workTypeDesc;
	}
	public String getFulfillmentCenterName20() {
		return fulfillmentCenterName20;
	}
	public void setFulfillmentCenterName20(String fulfillmentCenterName20) {
		this.fulfillmentCenterName20 = fulfillmentCenterName20;
	}
}
