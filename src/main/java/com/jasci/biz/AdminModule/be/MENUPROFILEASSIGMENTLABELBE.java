/**
 
Date Developed :JAN 13 2014
Created by: sarvendra tyagi
Description :Menu Profile Assigment BE For Set Label of all screen.
 */


package com.jasci.biz.AdminModule.be;

public class MENUPROFILEASSIGMENTLABELBE {
	
	
	private String   lbl_Part_of_the_Team_Member_Name;
	private String   lbl_Team_Member_ID;
	private String   lbl_Available_Menu_Profiles;
	private String   lbl_Menu_Profiles_Assigned;
	private String   lbl_Mandatory_field_cannot_be_left_blank;
	private String   lbl_Please_enter_a_Team_Member_ID;
	private String   lbl_Please_enter_part_of_Team_Member_Name;
	private String   lbl_Invalid_Team_Member_ID;
	private String   lbl_Invalid_part_of_Team_Member_Name;
	private String   lbl_Delete;
	private String   lbl_Add_New;
	private String   lbl_Actions;
	private String   lbl_Cancel;
	private String   lbl_Save_Update;
	private String   lbl_Display_All;
	private String   lbl_Your_record_has_been_saved_successfully;
	private String   lbl_Your_record_has_been_updated_successfully;
	private String   lbl_Name;
	private String   lbl_Select_Menu_Type;
	private String   lbl_Menu_profile;
	private String   lbl_MenuType;
	private String   lbl_Select;
	private String   lbl_Edit;
	private String   lbl_Menu_Profiles_Assignment_Lookup;
	private String   lbl_Menu_Profiles_Assignment_SEARCHLOOKUP;
	
	
	
	private String   lbl_Menu_Profiles_Assignment_Maintenance;
	private String 	 lbl_Description;
	private String 	 lbl_Company;
	private String   lbl_Team_Member;
	private String 	 lbl_Are_you_sure_you_want_to_delete_row;
	private String   lbl_No_menu_profile_has_been_assigned_to_the_TeamMember;
	private String lblReset;
	
	private String lblSort_First_Name;
	private String lblSort_Last_Name;

	public String getLblSort_First_Name() {
		return lblSort_First_Name;
	}
	public void setLblSort_First_Name(String lblSort_First_Name) {
		this.lblSort_First_Name = lblSort_First_Name;
	}
	public String getLblSort_Last_Name() {
		return lblSort_Last_Name;
	}
	public void setLblSort_Last_Name(String lblSort_Last_Name) {
		this.lblSort_Last_Name = lblSort_Last_Name;
	}
	public String getLblReset() {
		return lblReset;
	}
	public void setLblReset(String lblReset) {
		this.lblReset = lblReset;
	}
	
	public String getLbl_No_menu_profile_has_been_assigned_to_the_TeamMember() {
		return lbl_No_menu_profile_has_been_assigned_to_the_TeamMember;
	}
	public void setLbl_No_menu_profile_has_been_assigned_to_the_TeamMember(
			String lbl_No_menu_profile_has_been_assigned_to_the_TeamMember) {
		this.lbl_No_menu_profile_has_been_assigned_to_the_TeamMember = lbl_No_menu_profile_has_been_assigned_to_the_TeamMember;
	}
	public String getLbl_Are_you_sure_you_want_to_delete_row() {
		return lbl_Are_you_sure_you_want_to_delete_row;
	}
	public void setLbl_Are_you_sure_you_want_to_delete_row(String lbl_Are_you_sure_you_want_to_delete_row) {
		this.lbl_Are_you_sure_you_want_to_delete_row = lbl_Are_you_sure_you_want_to_delete_row;
	}
	public String getLbl_Team_Member() {
		return lbl_Team_Member;
	}
	public void setLbl_Team_Member(String lbl_Team_Member) {
		this.lbl_Team_Member = lbl_Team_Member;
	}
	public String getLbl_Company() {
		return lbl_Company;
	}
	public void setLbl_Company(String lbl_Company) {
		this.lbl_Company = lbl_Company;
	}
	public String getLbl_Description() {
		return lbl_Description;
	}
	public void setLbl_Description(String lbl_Description) {
		this.lbl_Description = lbl_Description;
	}
	public String getLbl_Menu_Profiles_Assignment_Lookup() {
		return lbl_Menu_Profiles_Assignment_Lookup;
	}
	public void setLbl_Menu_Profiles_Assignment_Lookup(String lbl_Menu_Profiles_Assignment_Lookup) {
		this.lbl_Menu_Profiles_Assignment_Lookup = lbl_Menu_Profiles_Assignment_Lookup;
	}
	public String getLbl_Menu_Profiles_Assignment_SEARCHLOOKUP() {
		return lbl_Menu_Profiles_Assignment_SEARCHLOOKUP;
	}
	public void setLbl_Menu_Profiles_Assignment_SEARCHLOOKUP(String lbl_Menu_Profiles_Assignment_SEARCHLOOKUP) {
		this.lbl_Menu_Profiles_Assignment_SEARCHLOOKUP = lbl_Menu_Profiles_Assignment_SEARCHLOOKUP;
	}
	public String getLbl_Menu_Profiles_Assignment_Maintenance() {
		return lbl_Menu_Profiles_Assignment_Maintenance;
	}
	public void setLbl_Menu_Profiles_Assignment_Maintenance(String lbl_Menu_Profiles_Assignment_Maintenance) {
		this.lbl_Menu_Profiles_Assignment_Maintenance = lbl_Menu_Profiles_Assignment_Maintenance;
	}
	public String getLbl_Edit() {
		return lbl_Edit;
	}
	public void setLbl_Edit(String lbl_Edit) {
		this.lbl_Edit = lbl_Edit;
	}
	public String getLbl_Select() {
		return lbl_Select;
	}
	public void setLbl_Select(String lbl_Select) {
		this.lbl_Select = lbl_Select;
	}
	public String getLbl_Part_of_the_Team_Member_Name() {
		return lbl_Part_of_the_Team_Member_Name;
	}
	public void setLbl_Part_of_the_Team_Member_Name(String lbl_Part_of_the_Team_Member_Name) {
		this.lbl_Part_of_the_Team_Member_Name = lbl_Part_of_the_Team_Member_Name;
	}
	public String getLbl_Team_Member_ID() {
		return lbl_Team_Member_ID;
	}
	public void setLbl_Team_Member_ID(String lbl_Team_Member_ID) {
		this.lbl_Team_Member_ID = lbl_Team_Member_ID;
	}
	public String getLbl_Available_Menu_Profiles() {
		return lbl_Available_Menu_Profiles;
	}
	public void setLbl_Available_Menu_Profiles(String lbl_Available_Menu_Profiles) {
		this.lbl_Available_Menu_Profiles = lbl_Available_Menu_Profiles;
	}
	public String getLbl_Menu_Profiles_Assigned() {
		return lbl_Menu_Profiles_Assigned;
	}
	public void setLbl_Menu_Profiles_Assigned(String lbl_Menu_Profiles_Assigned) {
		this.lbl_Menu_Profiles_Assigned = lbl_Menu_Profiles_Assigned;
	}
	public String getLbl_Mandatory_field_cannot_be_left_blank() {
		return lbl_Mandatory_field_cannot_be_left_blank;
	}
	public void setLbl_Mandatory_field_cannot_be_left_blank(String lbl_Mandatory_field_cannot_be_left_blank) {
		this.lbl_Mandatory_field_cannot_be_left_blank = lbl_Mandatory_field_cannot_be_left_blank;
	}
	public String getLbl_Please_enter_a_Team_Member_ID() {
		return lbl_Please_enter_a_Team_Member_ID;
	}
	public void setLbl_Please_enter_a_Team_Member_ID(String lbl_Please_enter_a_Team_Member_ID) {
		this.lbl_Please_enter_a_Team_Member_ID = lbl_Please_enter_a_Team_Member_ID;
	}
	public String getLbl_Please_enter_part_of_Team_Member_Name() {
		return lbl_Please_enter_part_of_Team_Member_Name;
	}
	public void setLbl_Please_enter_part_of_Team_Member_Name(String lbl_Please_enter_part_of_Team_Member_Name) {
		this.lbl_Please_enter_part_of_Team_Member_Name = lbl_Please_enter_part_of_Team_Member_Name;
	}
	public String getLbl_Invalid_Team_Member_ID() {
		return lbl_Invalid_Team_Member_ID;
	}
	public void setLbl_Invalid_Team_Member_ID(String lbl_Invalid_Team_Member_ID) {
		this.lbl_Invalid_Team_Member_ID = lbl_Invalid_Team_Member_ID;
	}
	public String getLbl_Invalid_part_of_Team_Member_Name() {
		return lbl_Invalid_part_of_Team_Member_Name;
	}
	public void setLbl_Invalid_part_of_Team_Member_Name(String lbl_Invalid_part_of_Team_Member_Name) {
		this.lbl_Invalid_part_of_Team_Member_Name = lbl_Invalid_part_of_Team_Member_Name;
	}
	public String getLbl_Delete() {
		return lbl_Delete;
	}
	public void setLbl_Delete(String lbl_Delete) {
		this.lbl_Delete = lbl_Delete;
	}
	public String getLbl_Add_New() {
		return lbl_Add_New;
	}
	public void setLbl_Add_New(String lbl_Add_New) {
		this.lbl_Add_New = lbl_Add_New;
	}
	public String getLbl_Actions() {
		return lbl_Actions;
	}
	public void setLbl_Actions(String lbl_Actions) {
		this.lbl_Actions = lbl_Actions;
	}
	public String getLbl_Cancel() {
		return lbl_Cancel;
	}
	public void setLbl_Cancel(String lbl_Cancel) {
		this.lbl_Cancel = lbl_Cancel;
	}
	public String getLbl_Save_Update() {
		return lbl_Save_Update;
	}
	public void setLbl_Save_Update(String lbl_Save_Update) {
		this.lbl_Save_Update = lbl_Save_Update;
	}
	public String getLbl_Display_All() {
		return lbl_Display_All;
	}
	public void setLbl_Display_All(String lbl_Display_All) {
		this.lbl_Display_All = lbl_Display_All;
	}
	public String getLbl_Your_record_has_been_saved_successfully() {
		return lbl_Your_record_has_been_saved_successfully;
	}
	public void setLbl_Your_record_has_been_saved_successfully(String lbl_Your_record_has_been_saved_successfully) {
		this.lbl_Your_record_has_been_saved_successfully = lbl_Your_record_has_been_saved_successfully;
	}
	public String getLbl_Your_record_has_been_updated_successfully() {
		return lbl_Your_record_has_been_updated_successfully;
	}
	public void setLbl_Your_record_has_been_updated_successfully(String lbl_Your_record_has_been_updated_successfully) {
		this.lbl_Your_record_has_been_updated_successfully = lbl_Your_record_has_been_updated_successfully;
	}
	public String getLbl_Name() {
		return lbl_Name;
	}
	public void setLbl_Name(String lbl_Name) {
		this.lbl_Name = lbl_Name;
	}
	public String getLbl_Select_Menu_Type() {
		return lbl_Select_Menu_Type;
	}
	public void setLbl_Select_Menu_Type(String lbl_Select_Menu_Type) {
		this.lbl_Select_Menu_Type = lbl_Select_Menu_Type;
	}
	public String getLbl_Menu_profile() {
		return lbl_Menu_profile;
	}
	public void setLbl_Menu_profile(String lbl_Menu_profile) {
		this.lbl_Menu_profile = lbl_Menu_profile;
	}
	public String getLbl_MenuType() {
		return lbl_MenuType;
	}
	public void setLbl_MenuType(String lbl_MenuType) {
		this.lbl_MenuType = lbl_MenuType;
	}

	
}
