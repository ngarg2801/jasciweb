/*

Date Developed  Oct 10 2015
Description pojo class is used to make getter and setter 
Created By Shailendar kumar

 */
package com.jasci.biz.AdminModule.be;

import com.jasci.common.constant.GLOBALCONSTANT;

@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
public class EXCECUTIONHISTORY  implements Comparable
{
    public String ExcecutionName;

    public Integer Position;

    public EXCECUTIONHISTORYVALUES[] EXCECUTIONHISTORYVALUES;

    public String getExcecutionName ()
    {
        return ExcecutionName;
    }

    public void setExcecutionName (String ExcecutionName)
    {
        this.ExcecutionName = ExcecutionName;
    }

    public Integer getPosition ()
    {
        return Position;
    }

    public void setPosition (Integer Position)
    {
        this.Position = Position;
    }

    public EXCECUTIONHISTORYVALUES[] getEXCECUTIONHISTORYVALUES ()
    {
        return EXCECUTIONHISTORYVALUES;
    }

    public void setEXCECUTIONHISTORYVALUES (EXCECUTIONHISTORYVALUES[] EXCECUTIONHISTORYVALUES)
    {
        this.EXCECUTIONHISTORYVALUES = EXCECUTIONHISTORYVALUES;
    }

   
	public int compareTo(Object object) {
		
		EXCECUTIONHISTORY objExcecutionhistory = (EXCECUTIONHISTORY)object;
		return objExcecutionhistory.Position.compareTo(this.Position);
	//	return 0;
	}
}