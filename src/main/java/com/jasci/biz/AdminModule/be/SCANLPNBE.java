/**
 * @author Pradeep Kumar
 * @Date Modified :June 22july, 2015
 * @Description:LPN Bean.
 * **/
package com.jasci.biz.AdminModule.be;

import java.io.Serializable;

public class SCANLPNBE implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String TENANT_ID;
	private String FULFILLMENT_CENTER_ID;
	private String COMPANY_ID;
	private String WORK_CONTROL_NUMBER;
	private String WORK_CONTROL_NUMBER_PRIMARY;
	private String LPN;
	private String ASN;
	private String WORK_TYPE;
	private String TASK;
	private String WORK_FLOW_ID;
	private String WORK_FLOW_STEP;
	private String WORK_GROUP_ZONE;
	private String WORK_ZONE;
	private String PRIORITY;
	private String PRIORITYCODE;
	private String SALES_ORDER_ID;
	private String PURCHASE_ORDER_NUMBER;
	private String ORDER_ID;
	private String ACCOUNTING_CODE;
	private String CREATED_BY;
	private String DATE_CREATED;
	private String STATUS;
	private String LAST_ACTIVITY_DATE;
	private String LAST_ACTIVITY_TEAM_MEMBER;
	private String LAST_ACTIVITY_TASK;
	private String AREA;
	private String LOCATION;
	
	
	public String getAREA() {
		return AREA;
	}
	public void setAREA(String aREA) {
		AREA = aREA;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getTENANT_ID() {
		return TENANT_ID;
	}
	public void setTENANT_ID(String tENANT_ID) {
		TENANT_ID = tENANT_ID;
	}
	public String getFULFILLMENT_CENTER_ID() {
		return FULFILLMENT_CENTER_ID;
	}
	public void setFULFILLMENT_CENTER_ID(String fULFILLMENT_CENTER_ID) {
		FULFILLMENT_CENTER_ID = fULFILLMENT_CENTER_ID;
	}
	public String getCOMPANY_ID() {
		return COMPANY_ID;
	}
	public void setCOMPANY_ID(String cOMPANY_ID) {
		COMPANY_ID = cOMPANY_ID;
	}
	public String getWORK_CONTROL_NUMBER() {
		return WORK_CONTROL_NUMBER;
	}
	public void setWORK_CONTROL_NUMBER(String wORK_CONTROL_NUMBER) {
		WORK_CONTROL_NUMBER = wORK_CONTROL_NUMBER;
	}
	public String getWORK_CONTROL_NUMBER_PRIMARY() {
		return WORK_CONTROL_NUMBER_PRIMARY;
	}
	public void setWORK_CONTROL_NUMBER_PRIMARY(String wORK_CONTROL_NUMBER_PRIMARY) {
		WORK_CONTROL_NUMBER_PRIMARY = wORK_CONTROL_NUMBER_PRIMARY;
	}
	public String getLPN() {
		return LPN;
	}
	public void setLPN(String lPN) {
		LPN = lPN;
	}
	public String getASN() {
		return ASN;
	}
	public void setASN(String aSN) {
		ASN = aSN;
	}
	public String getWORK_TYPE() {
		return WORK_TYPE;
	}
	public void setWORK_TYPE(String wORK_TYPE) {
		WORK_TYPE = wORK_TYPE;
	}
	public String getTASK() {
		return TASK;
	}
	public void setTASK(String tASK) {
		TASK = tASK;
	}
	public String getWORK_FLOW_ID() {
		return WORK_FLOW_ID;
	}
	public void setWORK_FLOW_ID(String wORK_FLOW_ID) {
		WORK_FLOW_ID = wORK_FLOW_ID;
	}
	public String getWORK_FLOW_STEP() {
		return WORK_FLOW_STEP;
	}
	public void setWORK_FLOW_STEP(String wORK_FLOW_STEP) {
		WORK_FLOW_STEP = wORK_FLOW_STEP;
	}
	public String getWORK_GROUP_ZONE() {
		return WORK_GROUP_ZONE;
	}
	public void setWORK_GROUP_ZONE(String wORK_GROUP_ZONE) {
		WORK_GROUP_ZONE = wORK_GROUP_ZONE;
	}
	public String getWORK_ZONE() {
		return WORK_ZONE;
	}
	public void setWORK_ZONE(String wORK_ZONE) {
		WORK_ZONE = wORK_ZONE;
	}
	public String getPRIORITY() {
		return PRIORITY;
	}
	public void setPRIORITY(String pRIORITY) {
		PRIORITY = pRIORITY;
	}
	public String getPRIORITYCODE() {
		return PRIORITYCODE;
	}
	public void setPRIORITYCODE(String pRIORITYCODE) {
		PRIORITYCODE = pRIORITYCODE;
	}
	public String getSALES_ORDER_ID() {
		return SALES_ORDER_ID;
	}
	public void setSALES_ORDER_ID(String sALES_ORDER_ID) {
		SALES_ORDER_ID = sALES_ORDER_ID;
	}
	public String getPURCHASE_ORDER_NUMBER() {
		return PURCHASE_ORDER_NUMBER;
	}
	public void setPURCHASE_ORDER_NUMBER(String pURCHASE_ORDER_NUMBER) {
		PURCHASE_ORDER_NUMBER = pURCHASE_ORDER_NUMBER;
	}
	public String getORDER_ID() {
		return ORDER_ID;
	}
	public void setORDER_ID(String oRDER_ID) {
		ORDER_ID = oRDER_ID;
	}
	public String getACCOUNTING_CODE() {
		return ACCOUNTING_CODE;
	}
	public void setACCOUNTING_CODE(String aCCOUNTING_CODE) {
		ACCOUNTING_CODE = aCCOUNTING_CODE;
	}
	public String getCREATED_BY() {
		return CREATED_BY;
	}
	public void setCREATED_BY(String cREATED_BY) {
		CREATED_BY = cREATED_BY;
	}
	public String getDATE_CREATED() {
		return DATE_CREATED;
	}
	public void setDATE_CREATED(String dATE_CREATED) {
		DATE_CREATED = dATE_CREATED;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getLAST_ACTIVITY_DATE() {
		return LAST_ACTIVITY_DATE;
	}
	public void setLAST_ACTIVITY_DATE(String lAST_ACTIVITY_DATE) {
		LAST_ACTIVITY_DATE = lAST_ACTIVITY_DATE;
	}
	public String getLAST_ACTIVITY_TEAM_MEMBER() {
		return LAST_ACTIVITY_TEAM_MEMBER;
	}
	public void setLAST_ACTIVITY_TEAM_MEMBER(String lAST_ACTIVITY_TEAM_MEMBER) {
		LAST_ACTIVITY_TEAM_MEMBER = lAST_ACTIVITY_TEAM_MEMBER;
	}
	public String getLAST_ACTIVITY_TASK() {
		return LAST_ACTIVITY_TASK;
	}
	public void setLAST_ACTIVITY_TASK(String lAST_ACTIVITY_TASK) {
		LAST_ACTIVITY_TASK = lAST_ACTIVITY_TASK;
	}
	
	
	

}
