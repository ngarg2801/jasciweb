/*
 
Date Developed  Nov 14 2014
Description It is used to make setter and getter to get data from form and to set in TeamMemberMessage class to insert into database
Created By Diksha Gupta
*/
package com.jasci.biz.AdminModule.be;

public class TEAMMEMBERMESSAGESBE 
{

	private String Type;
	private String StartDate;
	private String EndDate;
	private String TicketTapeMessage;
	private String Message1;
	private String Message2;
	private String Message3;
	private String Message4;
	private String Message5;
	private String TeamMember;
	private String Tenant;
	private String Company;
	private String Status;
	private String ResponseRequired;
	private String LastActivityDate;
	private String LastActivityTeamMember;
	private String Message;
	private String MessageNumber;
	
	
	
		
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getTeamMember() {
		return TeamMember;
	}
	public void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}
	public String getStartDate() {
		return StartDate;
	}
	public void setStartDate(String startDate) {
		StartDate = startDate;
	}
	public String getEndDate() {
		return EndDate;
	}
	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
	public String getTicketTapeMessage() {
		return TicketTapeMessage;
	}
	public void setTicketTapeMessage(String ticketTapeMessage) {
		TicketTapeMessage = ticketTapeMessage;
	}
	public String getMessage1() {
		return Message1;
	}
	public void setMessage1(String message1) {
		Message1 = message1;
	}
	public String getMessage2() {
		return Message2;
	}
	public void setMessage2(String message2) {
		Message2 = message2;
	}
	public String getMessage3() {
		return Message3;
	}
	public void setMessage3(String message3) {
		Message3 = message3;
	}
	public String getMessage4() {
		return Message4;
	}
	public void setMessage4(String message4) {
		Message4 = message4;
	}
	public String getMessage5() {
		return Message5;
	}
	public void setMessage5(String message5) {
		Message5 = message5;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getResponseRequired() {
		return ResponseRequired;
	}
	public void setResponseRequired(String responseRequired) {
		ResponseRequired = responseRequired;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getMessageNumber() {
		return MessageNumber;
	}
	public void setMessageNumber(String messageNumber) {
		MessageNumber = messageNumber;
	}
	
	
	
	
}
