/**
 * @Description: This file contains value of smart task execution table data
 * @Developer: Pradeep Kumar
 * @Date: July 16 2015
 * 
 */

package com.jasci.biz.AdminModule.be;

public class EXECUTIONBE {
	
	private String strTENANT_ID;
	private String strCOMPANY_ID;
	private String strEXECUTION_NAME;
	private String strAPPLICATION_ID;
	private String strEXECUTION_GROUP;
	private String strDESCRIPTION20;
	private String strDESCRIPTION50;
	private String strHELP_LINE;
	private String strEXECUTION_TYPE;
	
	public String getStrTENANT_ID() {
		return strTENANT_ID;
	}
	public void setStrTENANT_ID(String strTENANT_ID) {
		this.strTENANT_ID = strTENANT_ID;
	}
	public String getStrCOMPANY_ID() {
		return strCOMPANY_ID;
	}
	public void setStrCOMPANY_ID(String strCOMPANY_ID) {
		this.strCOMPANY_ID = strCOMPANY_ID;
	}
	public String getStrEXECUTION_NAME() {
		return strEXECUTION_NAME;
	}
	public void setStrEXECUTION_NAME(String strEXECUTION_NAME) {
		this.strEXECUTION_NAME = strEXECUTION_NAME;
	}
	public String getStrAPPLICATION_ID() {
		return strAPPLICATION_ID;
	}
	public void setStrAPPLICATION_ID(String strAPPLICATION_ID) {
		this.strAPPLICATION_ID = strAPPLICATION_ID;
	}
	public String getStrEXECUTION_GROUP() {
		return strEXECUTION_GROUP;
	}
	public void setStrEXECUTION_GROUP(String strEXECUTION_GROUP) {
		this.strEXECUTION_GROUP = strEXECUTION_GROUP;
	}
	public String getStrDESCRIPTION20() {
		return strDESCRIPTION20;
	}
	public void setStrDESCRIPTION20(String strDESCRIPTION20) {
		this.strDESCRIPTION20 = strDESCRIPTION20;
	}
	public String getStrDESCRIPTION50() {
		return strDESCRIPTION50;
	}
	public void setStrDESCRIPTION50(String strDESCRIPTION50) {
		this.strDESCRIPTION50 = strDESCRIPTION50;
	}
	public String getStrHELP_LINE() {
		return strHELP_LINE;
	}
	public void setStrHELP_LINE(String strHELP_LINE) {
		this.strHELP_LINE = strHELP_LINE;
	}
	public String getStrEXECUTION_TYPE() {
		return strEXECUTION_TYPE;
	}
	public void setStrEXECUTION_TYPE(String strEXECUTION_TYPE) {
		this.strEXECUTION_TYPE = strEXECUTION_TYPE;
	}
	public String getStrEXECUTION_DEVICE() {
		return strEXECUTION_DEVICE;
	}
	public void setStrEXECUTION_DEVICE(String strEXECUTION_DEVICE) {
		this.strEXECUTION_DEVICE = strEXECUTION_DEVICE;
	}
	public String getStrBUTTON() {
		return strBUTTON;
	}
	public void setStrBUTTON(String strBUTTON) {
		this.strBUTTON = strBUTTON;
	}
	public String getStrEXECUTION() {
		return strEXECUTION;
	}
	public void setStrEXECUTION(String strEXECUTION) {
		this.strEXECUTION = strEXECUTION;
	}
	public String getStrLAST_ACTIVITY_DATE() {
		return strLAST_ACTIVITY_DATE;
	}
	public void setStrLAST_ACTIVITY_DATE(String strLAST_ACTIVITY_DATE) {
		this.strLAST_ACTIVITY_DATE = strLAST_ACTIVITY_DATE;
	}
	public String getStrLAST_ACTIVITY_TEAM_MEMBER() {
		return strLAST_ACTIVITY_TEAM_MEMBER;
	}
	public void setStrLAST_ACTIVITY_TEAM_MEMBER(String strLAST_ACTIVITY_TEAM_MEMBER) {
		this.strLAST_ACTIVITY_TEAM_MEMBER = strLAST_ACTIVITY_TEAM_MEMBER;
	}
	public String getStrBUTTON_NAME() {
		return strBUTTON_NAME;
	}
	public void setStrBUTTON_NAME(String strBUTTON_NAME) {
		this.strBUTTON_NAME = strBUTTON_NAME;
	}
	private String strEXECUTION_DEVICE;
	private String strBUTTON;
	private String strEXECUTION;
	private String strLAST_ACTIVITY_DATE;
	private String strLAST_ACTIVITY_TEAM_MEMBER;
	private String strBUTTON_NAME;

}
