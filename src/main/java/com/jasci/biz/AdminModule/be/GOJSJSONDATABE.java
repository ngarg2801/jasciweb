/**
 
Date Developed  July 10 2014
Description pojo class of GOJS JSON data in which have getter and setter methods.
Created By Shailendra Rajput
 */
package com.jasci.biz.AdminModule.be;

import java.util.List;

public class GOJSJSONDATABE {
	
	
	private String Class ;
	private String LinkFromPortIdProperty ;
	private String LinkToPortIdProperty ;
	private List<GOJSNODEDATABE> NodeDataArray ;
	private List<GOJSLINKDATABE> linkDataArray;
	public String getclass() {
		return Class;
	}
	public void setclass(String class1) {
		Class = class1;
	}
	public String getLinkFromPortIdProperty() {
		return LinkFromPortIdProperty;
	}
	public void setLinkFromPortIdProperty(String linkFromPortIdProperty) {
		LinkFromPortIdProperty = linkFromPortIdProperty;
	}
	public String getLinkToPortIdProperty() {
		return LinkToPortIdProperty;
	}
	public void setLinkToPortIdProperty(String linkToPortIdProperty) {
		LinkToPortIdProperty = linkToPortIdProperty;
	}
	public List<GOJSNODEDATABE> getNodeDataArray() {
		return NodeDataArray;
	}
	public void setNodeDataArray(List<GOJSNODEDATABE> nodeDataArray) {
		NodeDataArray = nodeDataArray;
	}
	public List<GOJSLINKDATABE> getLinkDataArray() {
		return linkDataArray;
	}
	public void setLinkDataArray(List<GOJSLINKDATABE> linkDataArray) {
		this.linkDataArray = linkDataArray;
	}	

}
