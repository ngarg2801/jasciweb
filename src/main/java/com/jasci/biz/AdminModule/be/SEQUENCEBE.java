/**
Date Developed: May 13 2015
Description: This bean class is used to  provide setter and getter method of Task details
Created By: Sarvendra Tyagi
 */
package com.jasci.biz.AdminModule.be;

public class SEQUENCEBE {

	
	private String strMESSAGE;
	private String strEXECUTIONNAME;
	private String strEXECUTIONSEQUENCETYPE;
	private String strEXECUTIONSEQUENCENAME;
	private String strACTIONNAME;
	private String strCOMMENT;
	private String strGOTO_TAG;
	private int strEXECUTIONSEQUENCE;
	private String strTaskInstanceID;
	
	
	/** (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + strEXECUTIONSEQUENCE;
		return result;
	}
	/** (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SEQUENCEBE other = (SEQUENCEBE) obj;
		if (strEXECUTIONSEQUENCE != other.strEXECUTIONSEQUENCE)
			return false;
		return true;
	}
	private String strTenantID;
	private String strCompanyID;
	private String strMessage;
	private String strReturnValue;
	private String strCustomTag;
	private String strFulfillmentCenter;
	private String strTeamMemberID;
	
	
	
	public String getStrFulfillmentCenter() {
		return strFulfillmentCenter;
	}
	public void setStrFulfillmentCenter(String strFulfillmentCenter) {
		this.strFulfillmentCenter = strFulfillmentCenter;
	}
	public String getStrTeamMemberID() {
		return strTeamMemberID;
	}
	public void setStrTeamMemberID(String strTeamMemberID) {
		this.strTeamMemberID = strTeamMemberID;
	}
	public String getStrMessage() {
		return strMessage;
	}
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	public String getStrReturnValue() {
		return strReturnValue;
	}
	public void setStrReturnValue(String strReturnValue) {
		this.strReturnValue = strReturnValue;
	}
	public String getStrCustomTag() {
		return strCustomTag;
	}
	public void setStrCustomTag(String strCustomTag) {
		this.strCustomTag = strCustomTag;
	}
	public String getStrTenantID() {
		return strTenantID;
	}
	public void setStrTenantID(String strTenantID) {
		this.strTenantID = strTenantID;
	}
	public String getStrCompanyID() {
		return strCompanyID;
	}
	public void setStrCompanyID(String strCompanyID) {
		this.strCompanyID = strCompanyID;
	}
	
	
	
	public String getStrTaskInstanceID() {
		return strTaskInstanceID;
	}
	public void setStrTaskInstanceID(String strTaskInstanceID) {
		this.strTaskInstanceID = strTaskInstanceID;
	}
	public String getStrMESSAGE() {
		return strMESSAGE;
	}
	public void setStrMESSAGE(String strMESSAGE) {
		this.strMESSAGE = strMESSAGE;
	}
	public String getStrEXECUTIONNAME() {
		return strEXECUTIONNAME;
	}
	public void setStrEXECUTIONNAME(String strEXECUTIONNAME) {
		this.strEXECUTIONNAME = strEXECUTIONNAME;
	}
	public String getStrEXECUTIONSEQUENCETYPE() {
		return strEXECUTIONSEQUENCETYPE;
	}
	public void setStrEXECUTIONSEQUENCETYPE(String strEXECUTIONSEQUENCETYPE) {
		this.strEXECUTIONSEQUENCETYPE = strEXECUTIONSEQUENCETYPE;
	}
	public String getStrEXECUTIONSEQUENCENAME() {
		return strEXECUTIONSEQUENCENAME;
	}
	public void setStrEXECUTIONSEQUENCENAME(String strEXECUTIONSEQUENCENAME) {
		this.strEXECUTIONSEQUENCENAME = strEXECUTIONSEQUENCENAME;
	}
	public String getStrACTIONNAME() {
		return strACTIONNAME;
	}
	public void setStrACTIONNAME(String strACTIONNAME) {
		this.strACTIONNAME = strACTIONNAME;
	}
	public String getStrCOMMENT() {
		return strCOMMENT;
	}
	public void setStrCOMMENT(String strCOMMENT) {
		this.strCOMMENT = strCOMMENT;
	}
	
	public String getStrGOTO_TAG() {
		return strGOTO_TAG;
	}
	public void setStrGOTO_TAG(String strGOTO_TAG) {
		this.strGOTO_TAG = strGOTO_TAG;
	}
	public int getStrEXECUTIONSEQUENCE() {
		return strEXECUTIONSEQUENCE;
	}
	public void setStrEXECUTIONSEQUENCE(int strEXECUTIONSEQUENCE) {
		this.strEXECUTIONSEQUENCE = strEXECUTIONSEQUENCE;
	}
	
	
}
