/*
 
Date Developed  Nov 20 2014
Description pojo class of fulfillmentcenter in which getter and setter methods and mapping with table
Created By Deepak Sharma

 */
package com.jasci.biz.AdminModule.be;

import java.util.Date;



public class FULLFILLMENTCENTERBE {

	

	private String SetUpByName;
	
	private String Tenant,Fullfillment,Name20,Name50;
	
	
	private String AddressLine1;
	
	
	private String AddressLine2;
	
	
	private String AddressLine3;
	
	
	private String AddressLine4;
	
	
    private String City;
	
	
	private String StateCode;
	
	
   private String CountryCode;
	
	
	private String ZipCode;
	
	
    private String ContactName1;
	
	
	private String ContactPhone1;

	
	private String ContactExtension1;

	
	private String ContactCell1;

	
	private String ContactFax1;

	
	private String ContactEmail1;

	
	private String ContactName2;

	
	private String ContactPhone2;

	
	private String ContactExtension2;

	
	private String ContactCell2;

	
    private String ContactFax2;
	
	
	private String ContactEmail2;

	
	private String ContactName3;

	
	private String ContactPhone3;

	
	private String ContactExtension3;

	
	private String ContactCell3;

	
    private String ContactFax3;
	
	
	private String ContactEmail3;

	
	private String ContactName4;

	
	private String ContactPhone4;

	
	private String ContactExtension4;

	
	private String ContactCell4;


	private String ContactFax4;

	
	private String ContactEmail4;

	
	private String MainFax;

	
	private String MainEmail;
	
	
	private String SetUpSelected;

	
	private String SetUpDate;
	

	
	private String SetUpBy;
	

	
	private String LastActivityDateString;
	
	private Date LastActivityDate;
	
	

	
	private String LastActivityTeamMember;
	
	private String IsNew;
	
	public String getTenant() {
		return Tenant;
	}

	public void setTenant(String tenant) {
		Tenant = tenant;
	}

	public String getFullfillment() {
		return Fullfillment;
	}

	public void setFullfillment(String fullfillment) {
		Fullfillment = fullfillment;
	}

	public String getName20() {
		return Name20;
	}

	public void setName20(String name20) {
		Name20 = name20;
	}

	public String getName50() {
		return Name50;
	}

	public void setName50(String name50) {
		Name50 = name50;
	}
	
	public String getAddressLine1() {
		return AddressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return AddressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return AddressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		AddressLine3 = addressLine3;
	}

	public String getAddressLine4() {
		return AddressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		AddressLine4 = addressLine4;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getStateCode() {
		return StateCode;
	}

	public void setStateCode(String stateCode) {
		StateCode = stateCode;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getContactName1() {
		return ContactName1;
	}

	public void setContactName1(String contactName1) {
		ContactName1 = contactName1;
	}

	public String getContactPhone1() {
		return ContactPhone1;
	}

	public void setContactPhone1(String contactPhone1) {
		ContactPhone1 = contactPhone1;
	}

	public String getContactExtension1() {
		return ContactExtension1;
	}

	public void setContactExtension1(String contactExtension1) {
		ContactExtension1 = contactExtension1;
	}

	public String getContactCell1() {
		return ContactCell1;
	}

	public void setContactCell1(String contactCell1) {
		ContactCell1 = contactCell1;
	}

	public String getContactFax1() {
		return ContactFax1;
	}

	public void setContactFax1(String contactFax1) {
		ContactFax1 = contactFax1;
	}

	public String getContactEmail1() {
		return ContactEmail1;
	}

	public void setContactEmail1(String contactEmail1) {
		ContactEmail1 = contactEmail1;
	}

	public String getContactName2() {
		return ContactName2;
	}

	public void setContactName2(String contactName2) {
		ContactName2 = contactName2;
	}

	public String getContactPhone2() {
		return ContactPhone2;
	}

	public void setContactPhone2(String contactPhone2) {
		ContactPhone2 = contactPhone2;
	}

	public String getContactExtension2() {
		return ContactExtension2;
	}

	public void setContactExtension2(String contactExtension2) {
		ContactExtension2 = contactExtension2;
	}

	public String getContactCell2() {
		return ContactCell2;
	}

	public void setContactCell2(String contactCell2) {
		ContactCell2 = contactCell2;
	}

	public String getContactFax2() {
		return ContactFax2;
	}

	public void setContactFax2(String contactFax2) {
		ContactFax2 = contactFax2;
	}

	public String getContactEmail2() {
		return ContactEmail2;
	}

	public void setContactEmail2(String contactEmail2) {
		ContactEmail2 = contactEmail2;
	}

	public String getContactName3() {
		return ContactName3;
	}

	public void setContactName3(String contactName3) {
		ContactName3 = contactName3;
	}

	public String getContactPhone3() {
		return ContactPhone3;
	}

	public void setContactPhone3(String contactPhone3) {
		ContactPhone3 = contactPhone3;
	}

	public String getContactExtension3() {
		return ContactExtension3;
	}

	public void setContactExtension3(String contactExtension3) {
		ContactExtension3 = contactExtension3;
	}

	public String getContactCell3() {
		return ContactCell3;
	}

	public void setContactCell3(String contactCell3) {
		ContactCell3 = contactCell3;
	}

	public String getContactFax3() {
		return ContactFax3;
	}

	public void setContactFax3(String contactFax3) {
		ContactFax3 = contactFax3;
	}

	public String getContactEmail3() {
		return ContactEmail3;
	}

	public void setContactEmail3(String contactEmail3) {
		ContactEmail3 = contactEmail3;
	}

	public String getContactName4() {
		return ContactName4;
	}

	public void setContactName4(String contactName4) {
		ContactName4 = contactName4;
	}

	public String getContactPhone4() {
		return ContactPhone4;
	}

	public void setContactPhone4(String contactPhone4) {
		ContactPhone4 = contactPhone4;
	}

	public String getContactExtension4() {
		return ContactExtension4;
	}

	public void setContactExtension4(String contactExtension4) {
		ContactExtension4 = contactExtension4;
	}

	public String getContactCell4() {
		return ContactCell4;
	}

	public void setContactCell4(String contactCell4) {
		ContactCell4 = contactCell4;
	}

	public String getContactFax4() {
		return ContactFax4;
	}

	public void setContactFax4(String contactFax4) {
		ContactFax4 = contactFax4;
	}

	public String getContactEmail4() {
		return ContactEmail4;
	}

	public void setContactEmail4(String contactEmail4) {
		ContactEmail4 = contactEmail4;
	}

	public String getMainFax() {
		return MainFax;
	}

	public void setMainFax(String mainFax) {
		MainFax = mainFax;
	}

	public String getMainEmail() {
		return MainEmail;
	}

	public void setMainEmail(String mainEmail) {
		MainEmail = mainEmail;
	}

	public String getSetUpSelected() {
		return SetUpSelected;
	}

	public void setSetUpSelected(String setUpSelected) {
		SetUpSelected = setUpSelected;
	}

	public String getSetUpDate() {
		return SetUpDate;
	}

	public void setSetUpDate(String setUpDate) {
		SetUpDate = setUpDate;
	}

	public String getSetUpBy() {
		return SetUpBy;
	}

	public void setSetUpBy(String setUpBy) {
		SetUpBy = setUpBy;
	}

	public String getLastActivityDateString() {
		return LastActivityDateString;
	}

	public void setLastActivityDateString(String lastActivityDate) {
		LastActivityDateString = lastActivityDate;
	}

	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}

	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}

	public String getLastActivityTask() {
		return LastActivityTask;
	}

	public void setLastActivityTask(String lastActivityTask) {
		LastActivityTask = lastActivityTask;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	/**
	 * @return the isNew
	 */
	public String getIsNew() {
		return IsNew;
	}

	/**
	 * @param isNew the isNew to set
	 */
	public void setIsNew(String isNew) {
		IsNew = isNew;
	}

	/**
	 * @return the setUpByName
	 */
	public String getSetUpByName() {
		return SetUpByName;
	}

	/**
	 * @param setUpByName the setUpByName to set
	 */
	public void setSetUpByName(String setUpByName) {
		SetUpByName = setUpByName;
	}

	/**
	 * @return the lastActivityDate
	 */
	public Date getLastActivityDate() {
		return LastActivityDate;
	}

	/**
	 * @param lastActivityDate the lastActivityDate to set
	 */
	public void setLastActivityDate(Date lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}

	private String LastActivityTask;
	
	
	private String STATUS;
	


	
}
