/*
 
Date Developed  Nov 02 2014
Description  Declaration of constants
Created By Diksha Gupta
 */

package com.jasci.biz.AdminModule.be;

public class PASSWORDEXPIRYBE 
{
	private String StrCreateNewPassword;
	private String StrNewPassword;
	private String StrConfirmPassword;
	private String StrSubmitButton;
	private String StrCancelButton;
	private String StrPasswordExpiredMessage;
	private String StrPasswordNotMatchedMessage;
	private String StrPasswordUsedMessage;
	private String StrPasswordPatternMessage;
	private String StrTemporaryPasswordMessage;
	
	
	
	public String getStrTemporaryPasswordMessage() {
		return StrTemporaryPasswordMessage;
	}
	public void setStrTemporaryPasswordMessage(String strTemporaryPasswordMessage) {
		StrTemporaryPasswordMessage = strTemporaryPasswordMessage;
	}
	public String getStrPasswordExpiredMessage() {
		return StrPasswordExpiredMessage;
	}
	public void setStrPasswordExpiredMessage(String strPasswordExpiredMessage) {
		StrPasswordExpiredMessage = strPasswordExpiredMessage;
	}
	public String getStrPasswordNotMatchedMessage() {
		return StrPasswordNotMatchedMessage;
	}
	public void setStrPasswordNotMatchedMessage(String strPasswordNotMatchedMessage) {
		StrPasswordNotMatchedMessage = strPasswordNotMatchedMessage;
	}
	public String getStrPasswordUsedMessage() {
		return StrPasswordUsedMessage;
	}
	public void setStrPasswordUsedMessage(String strPasswordUsedMessage) {
		StrPasswordUsedMessage = strPasswordUsedMessage;
	}
	public String getStrPasswordPatternMessage() {
		return StrPasswordPatternMessage;
	}
	public void setStrPasswordPatternMessage(String strPasswordPatternMessage) {
		StrPasswordPatternMessage = strPasswordPatternMessage;
	}
	public String getStrCreateNewPassword() {
		return StrCreateNewPassword;
	}
	public void setStrCreateNewPassword(String strCreateNewPassword) {
		StrCreateNewPassword = strCreateNewPassword;
	}
	public String getStrNewPassword() {
		return StrNewPassword;
	}
	public void setStrNewPassword(String strNewPassword) {
		StrNewPassword = strNewPassword;
	}
	public String getStrConfirmPassword() {
		return StrConfirmPassword;
	}
	public void setStrConfirmPassword(String strConfirmPassword) {
		StrConfirmPassword = strConfirmPassword;
	}
	public String getStrSubmitButton() {
		return StrSubmitButton;
	}
	public void setStrSubmitButton(String strSubmitButton) {
		StrSubmitButton = strSubmitButton;
	}
	public String getStrCancelButton() {
		return StrCancelButton;
	}
	public void setStrCancelButton(String strCancelButton) {
		StrCancelButton = strCancelButton;
	}
	
	
 
}
