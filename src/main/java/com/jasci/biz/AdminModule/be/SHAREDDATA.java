/**
Date Developed: May 13 2015
Description: This bean class is used to provide common data getter setter.
Created By: Sarvendra Tyagi
 */

package com.jasci.biz.AdminModule.be;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;



public class SHAREDDATA {

	EXECUTIONHISTORYLIST onjExecutionhistorylist;
	private JSONObject ObjJson;
	private String ErrorCode;
	private boolean IsExecute=true;
	private boolean IsSucess;
	private String strTaskName;
	private String ISRETURN;
	private Object objectView;	
	private Boolean RESETSCREEN;
	private Boolean REDISPLAY=true;
	private String IFERROR="N";
	private String IFEXIST="N";
	private String IFRETURN;
	private String COMMENT;
	private String DISPLAY;
	private String CUSTOMEXECUTION;
	private int currentPositionTag;
	private String strProcessUUID;
	private Date taskStartDate;
	private String strTaskUUID;
	private SEQUENCEBE objSequenceBE;
	private boolean MESSAGECHECK;
	private int intNumberWorkPerformed;
	private int intNumberLines;
	private int intNumberQty;
	private int intNumberPallets;
	private int intNumberLocation;
	private int intNumberRows;
	private int intNumberAlias;
	private int intNumberLevels;
	private String strNumberEmptyReturns;
	private String strWorkType;
	private int intWorkControlNumber;
	private int intNumberCuts;
	private String strWorkHistoryCode;
	private int intNumberCases;
	private String strLPN;
	private String strASN;
	private String strZONE;
	private String strDIVERT;
	private String strTASK;
	private Date dateWorkDate;
	private String strTenantID;
	private String strCompanyID;
	private int intNumberBays;
	private String strFulfillmentCenter;
	private boolean pause;
	private int intCounterMAX=10;
	private int intTestCounter;
	private String strExeReturnValue="";
	private String strDisplayActionMessage;
	private String strArea;
	private String strLocation;
	private boolean comment;
	private String strCommentMessage;
	private String strPriorityCode;
	private String strPriority;
	private boolean isReset;
	private String strWorkGroupZone;
	private String strGroupZone;
	private String strProduct;
	private String strQuality;



	public EXECUTIONHISTORYLIST getOnjExecutionhistorylist() {
		return onjExecutionhistorylist;
	}
	public void setOnjExecutionhistorylist(EXECUTIONHISTORYLIST onjExecutionhistorylist) {
		this.onjExecutionhistorylist = onjExecutionhistorylist;
	}
	public String getStrWorkGroupZone() {
		return strWorkGroupZone;
	}
	public void setStrWorkGroupZone(String strWorkGroupZone) {
		this.strWorkGroupZone = strWorkGroupZone;
	}
	public String getStrGroupZone() {
		return strGroupZone;
	}
	public void setStrGroupZone(String strGroupZone) {
		this.strGroupZone = strGroupZone;
	}
	public String getStrProduct() {
		return strProduct;
	}
	public void setStrProduct(String strProduct) {
		this.strProduct = strProduct;
	}
	public String getStrQuality() {
		return strQuality;
	}
	public void setStrQuality(String strQuality) {
		this.strQuality = strQuality;
	}
	public boolean isReset() {
		return isReset;
	}
	public void setReset(boolean isReset) {
		this.isReset = isReset;
	}
	public String getStrPriorityCode() {
		return strPriorityCode;
	}
	public void setStrPriorityCode(String strPriorityCode) {
		this.strPriorityCode = strPriorityCode;
	}
	public String getStrPriority() {
		return strPriority;
	}
	public void setStrPriority(String strPriority) {
		this.strPriority = strPriority;
	}
	public boolean isComment() {
		return comment;
	}
	public void setComment(boolean comment) {
		this.comment = comment;
	}
	public String getStrCommentMessage() {
		return strCommentMessage;
	}
	public void setStrCommentMessage(String strCommentMessage) {
		this.strCommentMessage = strCommentMessage;
	}
	public String getStrArea() {
		return strArea;
	}
	public void setStrArea(String strArea) {
		this.strArea = strArea;
	}
	public String getStrLocation() {
		return strLocation;
	}
	public void setStrLocation(String strLocation) {
		this.strLocation = strLocation;
	}
	private boolean displayAction;
	public boolean isDisplayAction() {
		return displayAction;
	}
	public void setDisplayAction(boolean displayAction) {
		this.displayAction = displayAction;
	}
	public String getStrDisplayActionMessage() {
		return strDisplayActionMessage;
	}
	public void setStrDisplayActionMessage(String strDisplayActionMessage) {
		this.strDisplayActionMessage = strDisplayActionMessage;
	}
	private String strButtonName;
	public List<String> getListTeamMemberMessages() {
		return listTeamMemberMessages;
	}
	public void setListTeamMemberMessages(List<String> listTeamMemberMessages) {
		this.listTeamMemberMessages = listTeamMemberMessages;
	}
	private List<String> listTeamMemberMessages;

	private String strErrorMessage;

	public String getStrErrorMessage() {
		return strErrorMessage;
	}
	public void setStrErrorMessage(String strErrorMessage) {
		this.strErrorMessage = strErrorMessage;
	}


	public String getStrButtonName() {
		return strButtonName;
	}
	public void setStrButtonName(String strButtonName) {
		this.strButtonName = strButtonName;
	}
	private Map< String, EXECUTIONBE> mapExecution;

	public Map<String, EXECUTIONBE> getMapExecution() {
		return mapExecution;
	}
	public void setMapExecution(Map<String, EXECUTIONBE> mapExecution) {
		this.mapExecution = mapExecution;
	}
	public String getStrExeReturnValue() {
		return strExeReturnValue;
	}
	public void setStrExeReturnValue(String strExeReturnValue) {
		this.strExeReturnValue = strExeReturnValue;
	}
	//private WORKFLOWSERVICE objWorkFlowService;
	private List<String> arrListBottomBarText=new ArrayList<String>();
	private List<String> arrListBottomBarTextButton=new ArrayList<String>();

	public List<String> getArrListBottomBarText() {
		return arrListBottomBarText;
	}
	public void setArrListBottomBarText(List<String> arrListBottomBarText) {
		this.arrListBottomBarText = arrListBottomBarText;
	}
	public List<String> getArrListBottomBarTextButton() {
		return arrListBottomBarTextButton;
	}
	public void setArrListBottomBarTextButton(List<String> arrListBottomBarTextButton) {
		this.arrListBottomBarTextButton = arrListBottomBarTextButton;
	}
	/*public WORKFLOWSERVICE getObjWorkFlowService() {
		return objWorkFlowService;
	}
	public void setObjWorkFlowService(WORKFLOWSERVICE objWorkFlowService) {
		this.objWorkFlowService = objWorkFlowService;
	}*/
	public int getIntCounterMAX() {
		return intCounterMAX;
	}
	public void setIntCounterMAX(int intCounterMAX) {
		this.intCounterMAX = intCounterMAX;
	}
	public int getIntTestCounter() {
		return intTestCounter;
	}
	public void setIntTestCounter(int intTestCounter) {
		this.intTestCounter = intTestCounter;
	}
	public boolean isPause() {
		return pause;
	}
	public void setPause(boolean pause) {
		this.pause = pause;
	}
	public String getStrFulfillmentCenter() {
		return strFulfillmentCenter;
	}
	public void setStrFulfillmentCenter(String strFulfillmentCenter) {
		this.strFulfillmentCenter = strFulfillmentCenter;
	}
	public int getIntNumberBays() {
		return intNumberBays;
	}
	public void setIntNumberBays(int intNumberBays) {
		this.intNumberBays = intNumberBays;
	}
	public String getStrTeamMember() {
		return strTeamMember;
	}
	public void setStrTeamMember(String strTeamMember) {
		this.strTeamMember = strTeamMember;
	}
	private String strTeamMember;






	public String getStrTenantID() {
		return strTenantID;
	}
	public void setStrTenantID(String strTenantID) {
		this.strTenantID = strTenantID;
	}
	public String getStrCompanyID() {
		return strCompanyID;
	}
	public void setStrCompanyID(String strCompanyID) {
		this.strCompanyID = strCompanyID;
	}
	public int getIntNumberWorkPerformed() {
		return intNumberWorkPerformed;
	}
	public void setIntNumberWorkPerformed(int intNumberWorkPerformed) {
		this.intNumberWorkPerformed = intNumberWorkPerformed;
	}
	public int getIntNumberLines() {
		return intNumberLines;
	}
	public void setIntNumberLines(int intNumberLines) {
		this.intNumberLines = intNumberLines;
	}
	public int getIntNumberQty() {
		return intNumberQty;
	}
	public void setIntNumberQty(int intNumberQty) {
		this.intNumberQty = intNumberQty;
	}
	public int getIntNumberPallets() {
		return intNumberPallets;
	}
	public void setIntNumberPallets(int intNumberPallets) {
		this.intNumberPallets = intNumberPallets;
	}
	public int getIntNumberLocation() {
		return intNumberLocation;
	}
	public void setIntNumberLocation(int intNumberLocation) {
		this.intNumberLocation = intNumberLocation;
	}
	public int getIntNumberRows() {
		return intNumberRows;
	}
	public void setIntNumberRows(int intNumberRows) {
		this.intNumberRows = intNumberRows;
	}
	public int getIntNumberAlias() {
		return intNumberAlias;
	}
	public void setIntNumberAlias(int intNumberAlias) {
		this.intNumberAlias = intNumberAlias;
	}
	public int getIntNumberLevels() {
		return intNumberLevels;
	}
	public void setIntNumberLevels(int intNumberLevels) {
		this.intNumberLevels = intNumberLevels;
	}
	public String getStrNumberEmptyReturns() {
		return strNumberEmptyReturns;
	}
	public void setStrNumberEmptyReturns(String strNumberEmptyReturns) {
		this.strNumberEmptyReturns = strNumberEmptyReturns;
	}
	public String getStrWorkType() {
		return strWorkType;
	}
	public void setStrWorkType(String strWorkType) {
		this.strWorkType = strWorkType;
	}
	public int getIntWorkControlNumber() {
		return intWorkControlNumber;
	}
	public void setIntWorkControlNumber(int intWorkControlNumber) {
		this.intWorkControlNumber = intWorkControlNumber;
	}
	public int getIntNumberCuts() {
		return intNumberCuts;
	}
	public void setIntNumberCuts(int intNumberCuts) {
		this.intNumberCuts = intNumberCuts;
	}
	public String getStrWorkHistoryCode() {
		return strWorkHistoryCode;
	}
	public void setStrWorkHistoryCode(String strWorkHistoryCode) {
		this.strWorkHistoryCode = strWorkHistoryCode;
	}
	public int getIntNumberCases() {
		return intNumberCases;
	}
	public void setIntNumberCases(int intNumberCases) {
		this.intNumberCases = intNumberCases;
	}
	public String getStrLPN() {
		return strLPN;
	}
	public void setStrLPN(String strLPN) {
		this.strLPN = strLPN;
	}
	public String getStrASN() {
		return strASN;
	}
	public void setStrASN(String strASN) {
		this.strASN = strASN;
	}
	public String getStrZONE() {
		return strZONE;
	}
	public void setStrZONE(String strZONE) {
		this.strZONE = strZONE;
	}
	public String getStrDIVERT() {
		return strDIVERT;
	}
	public void setStrDIVERT(String strDIVERT) {
		this.strDIVERT = strDIVERT;
	}
	public String getStrTASK() {
		return strTASK;
	}
	public void setStrTASK(String strTASK) {
		this.strTASK = strTASK;
	}
	public Date getDateWorkDate() {
		return dateWorkDate;
	}
	public void setDateWorkDate(Date dateWorkDate) {
		this.dateWorkDate = dateWorkDate;
	}
	public boolean isMESSAGECHECK() {
		return MESSAGECHECK;
	}
	public void setMESSAGECHECK(boolean mESSAGECHECK) {
		MESSAGECHECK = mESSAGECHECK;
	}
	public SEQUENCEBE getObjSequenceBE() {
		return objSequenceBE;
	}
	public void setObjSequenceBE(SEQUENCEBE objSequenceBE) {
		this.objSequenceBE = objSequenceBE;
	}
	public Date getTaskStartDate() {
		return taskStartDate;
	}
	public void setTaskStartDate(Date taskStartDate) {
		this.taskStartDate = taskStartDate;
	}
	public String getStrTaskUUID() {
		return strTaskUUID;
	}
	public void setStrTaskUUID(String strTaskUUID) {
		this.strTaskUUID = strTaskUUID;
	}
	public String getStrProcessUUID() {
		return strProcessUUID;
	}
	public void setStrProcessUUID(String strProcessUUID) {
		this.strProcessUUID = strProcessUUID;
	}
	public int getCurrentPositionTag() {
		return currentPositionTag;
	}
	public void setCurrentPositionTag(int currentPositionTag) {
		this.currentPositionTag = currentPositionTag;
	}
	public String getDISPLAY() {
		return DISPLAY;
	}
	public void setDISPLAY(String dISPLAY) {
		DISPLAY = dISPLAY;
	}
	public String getCUSTOMEXECUTION() {
		return CUSTOMEXECUTION;
	}
	public void setCUSTOMEXECUTION(String cUSTOMEXECUTION) {
		CUSTOMEXECUTION = cUSTOMEXECUTION;
	}

	public Boolean getRESETSCREEN() {
		return RESETSCREEN;
	}
	public void setRESETSCREEN(Boolean rESETSCREEN) {
		RESETSCREEN = rESETSCREEN;
	}
	public Boolean getREDISPLAY() {
		return REDISPLAY;
	}
	public void setREDISPLAY(Boolean rEDISPLAY) {
		REDISPLAY = rEDISPLAY;
	}
	public String getIFERROR() {
		return IFERROR;
	}
	public void setIFERROR(String iFERROR) {
		IFERROR = iFERROR;
	}
	public String getIFEXIST() {
		return IFEXIST;
	}
	public void setIFEXIST(String iFEXIST) {
		IFEXIST = iFEXIST;
	}
	public String getIFRETURN() {
		return IFRETURN;
	}
	public void setIFRETURN(String iFRETURN) {
		IFRETURN = iFRETURN;
	}
	public String getCOMMENT() {
		return COMMENT;
	}
	public void setCOMMENT(String cOMMENT) {
		COMMENT = cOMMENT;
	}
	public Object getObjectView() {
		return objectView;
	}
	public void setObjectView(Object objectView) {
		this.objectView = objectView;
	}
	public String getISRETURN() {
		return ISRETURN;
	}
	public void setISRETURN(String iSRETURN) {
		ISRETURN = iSRETURN;
	}
	public String getStrTaskName() {
		return strTaskName;
	}
	public void setStrTaskName(String strTaskName) {
		this.strTaskName = strTaskName;
	}
	public JSONObject getObjJson() {
		return ObjJson;
	}
	public void setObjJson(JSONObject objJson) {
		ObjJson = objJson;
	}
	public String getErrorCode() {
		return ErrorCode;
	}
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	public boolean isIsExecute() {
		return IsExecute;
	}
	public void setIsExecute(boolean isExecute) {
		IsExecute = isExecute;
	}
	public boolean isIsSucess() {
		return IsSucess;
	}
	public void setIsSucess(boolean isSucess) {
		IsSucess = isSucess;
	}




}
