/**
Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :This class has getter setter for the variables that are to be showthe screen labels from languages table. */


package com.jasci.biz.AdminModule.be;

public class LANGUAGETRANSLATIONSCREENLABELSBE 
{

	private String 	LanguageTranslation_Language	;
	private String 	LanguageTranslation_LTLookup	;
	private String 	LanguageTranslation_EnglishKeyPhrase	;
	private String 	LanguageTranslation_DisplayAll	;
	private String 	LanguageTranslation_New	;
	private String 	LanguageTranslation_KeyPhrase	;
	private String 	LanguageTranslation_Translation	;
	private String 	LanguageTranslation_Action	;
	private String 	LanguageTranslation_AddNew	;
	private String 	LanguageTranslation_LTSearchLookup	;
	private String 	LanguageTranslation_LTMaintenance	;
	private String 	LanguageTranslation_LastActivityDate	;
	private String 	LanguageTranslation_LastActivityBy	;
	private String 	LanguageTranslation_LTMkeyPhrase	;
	private String 	LanguageTranslation_SaveUpdate	;
	private String 	LanguageTranslation_Cancel	;
	private String 	LanguageTranslation_SaveMessage	;
	private String 	LanguageTranslation_UpdateMessage	;
	private String 	LanguageTranslation_DeleteMessage	;
	private String 	LanguageTranslation_InValidLanguageMessage	;
	private String 	LanguageTranslation_InValidKeyPhraseMessage	;
	private String 	LanguageTranslation_BlankLanguageMessage	;
	private String 	LanguageTranslation_BlankKeyphraseMessage	;
	private String 	LanguageTranslation_KeyPhraseAlreadyExist	;
	private String 	LanguageTranslation_MandatoryFieldMessage	;
	private String 	LanguageTranslation_Edit	;
	private String 	LanguageTranslation_Delete	;
	private String LanguageTranslation_Select;
	private String LanguageTranslation_Reset;
	
	public String getLanguageTranslation_Reset() {
		return LanguageTranslation_Reset;
	}
	public void setLanguageTranslation_Reset(String languageTranslation_Reset) {
		LanguageTranslation_Reset = languageTranslation_Reset;
	}
	public String getLanguageTranslation_Select() {
		return LanguageTranslation_Select;
	}
	public void setLanguageTranslation_Select(String languageTranslation_Select) {
		LanguageTranslation_Select = languageTranslation_Select;
	}
	public String getLanguageTranslation_Edit() {
		return LanguageTranslation_Edit;
	}
	public void setLanguageTranslation_Edit(String languageTranslation_Edit) {
		LanguageTranslation_Edit = languageTranslation_Edit;
	}
	public String getLanguageTranslation_Delete() {
		return LanguageTranslation_Delete;
	}
	public void setLanguageTranslation_Delete(String languageTranslation_Delete) {
		LanguageTranslation_Delete = languageTranslation_Delete;
	}
	public String getLanguageTranslation_Language() {
		
		return LanguageTranslation_Language;
	}
	public void setLanguageTranslation_Language(String languageTranslation_Language) {
		LanguageTranslation_Language = languageTranslation_Language;
	}
	public String getLanguageTranslation_LTLookup() {
		return LanguageTranslation_LTLookup;
	}
	public void setLanguageTranslation_LTLookup(String languageTranslation_LTLookup) {
		LanguageTranslation_LTLookup = languageTranslation_LTLookup;
	}
	public String getLanguageTranslation_EnglishKeyPhrase() {
		return LanguageTranslation_EnglishKeyPhrase;
	}
	public void setLanguageTranslation_EnglishKeyPhrase(
			String languageTranslation_EnglishKeyPhrase) {
		LanguageTranslation_EnglishKeyPhrase = languageTranslation_EnglishKeyPhrase;
	}
	public String getLanguageTranslation_DisplayAll() {
		return LanguageTranslation_DisplayAll;
	}
	public void setLanguageTranslation_DisplayAll(
			String languageTranslation_DisplayAll) {
		LanguageTranslation_DisplayAll = languageTranslation_DisplayAll;
	}
	public String getLanguageTranslation_New() {
		return LanguageTranslation_New;
	}
	public void setLanguageTranslation_New(String languageTranslation_New) {
		LanguageTranslation_New = languageTranslation_New;
	}
	public String getLanguageTranslation_KeyPhrase() {
		return LanguageTranslation_KeyPhrase;
	}
	public void setLanguageTranslation_KeyPhrase(
			String languageTranslation_KeyPhrase) {
		LanguageTranslation_KeyPhrase = languageTranslation_KeyPhrase;
	}
	public String getLanguageTranslation_Translation() {
		return LanguageTranslation_Translation;
	}
	public void setLanguageTranslation_Translation(
			String languageTranslation_Translation) {
		LanguageTranslation_Translation = languageTranslation_Translation;
	}
	public String getLanguageTranslation_Action() {
		return LanguageTranslation_Action;
	}
	public void setLanguageTranslation_Action(String languageTranslation_Action) {
		LanguageTranslation_Action = languageTranslation_Action;
	}
	public String getLanguageTranslation_AddNew() {
		return LanguageTranslation_AddNew;
	}
	public void setLanguageTranslation_AddNew(String languageTranslation_AddNew) {
		LanguageTranslation_AddNew = languageTranslation_AddNew;
	}
	public String getLanguageTranslation_LTSearchLookup() {
		return LanguageTranslation_LTSearchLookup;
	}
	public void setLanguageTranslation_LTSearchLookup(
			String languageTranslation_LTSearchLookup) {
		LanguageTranslation_LTSearchLookup = languageTranslation_LTSearchLookup;
	}
	public String getLanguageTranslation_LTMaintenance() {
		return LanguageTranslation_LTMaintenance;
	}
	public void setLanguageTranslation_LTMaintenance(
			String languageTranslation_LTMaintenance) {
		LanguageTranslation_LTMaintenance = languageTranslation_LTMaintenance;
	}
	public String getLanguageTranslation_LastActivityDate() {
		return LanguageTranslation_LastActivityDate;
	}
	public void setLanguageTranslation_LastActivityDate(
			String languageTranslation_LastActivityDate) {
		LanguageTranslation_LastActivityDate = languageTranslation_LastActivityDate;
	}
	public String getLanguageTranslation_LastActivityBy() {
		return LanguageTranslation_LastActivityBy;
	}
	public void setLanguageTranslation_LastActivityBy(
			String languageTranslation_LastActivityBy) {
		LanguageTranslation_LastActivityBy = languageTranslation_LastActivityBy;
	}
	public String getLanguageTranslation_LTMkeyPhrase() {
		return LanguageTranslation_LTMkeyPhrase;
	}
	public void setLanguageTranslation_LTMkeyPhrase(
			String languageTranslation_LTMkeyPhrase) {
		LanguageTranslation_LTMkeyPhrase = languageTranslation_LTMkeyPhrase;
	}
	public String getLanguageTranslation_SaveUpdate() {
		return LanguageTranslation_SaveUpdate;
	}
	public void setLanguageTranslation_SaveUpdate(
			String languageTranslation_SaveUpdate) {
		LanguageTranslation_SaveUpdate = languageTranslation_SaveUpdate;
	}
	public String getLanguageTranslation_Cancel() {
		return LanguageTranslation_Cancel;
	}
	public void setLanguageTranslation_Cancel(String languageTranslation_Cancel) {
		LanguageTranslation_Cancel = languageTranslation_Cancel;
	}
	public String getLanguageTranslation_SaveMessage() {
		return LanguageTranslation_SaveMessage;
	}
	public void setLanguageTranslation_SaveMessage(
			String languageTranslation_SaveMessage) {
		LanguageTranslation_SaveMessage = languageTranslation_SaveMessage;
	}
	public String getLanguageTranslation_UpdateMessage() {
		return LanguageTranslation_UpdateMessage;
	}
	public void setLanguageTranslation_UpdateMessage(
			String languageTranslation_UpdateMessage) {
		LanguageTranslation_UpdateMessage = languageTranslation_UpdateMessage;
	}
	public String getLanguageTranslation_DeleteMessage() {
		return LanguageTranslation_DeleteMessage;
	}
	public void setLanguageTranslation_DeleteMessage(
			String languageTranslation_DeleteMessage) {
		LanguageTranslation_DeleteMessage = languageTranslation_DeleteMessage;
	}
	public String getLanguageTranslation_InValidLanguageMessage() {
		return LanguageTranslation_InValidLanguageMessage;
	}
	public void setLanguageTranslation_InValidLanguageMessage(
			String languageTranslation_InValidLanguageMessage) {
		LanguageTranslation_InValidLanguageMessage = languageTranslation_InValidLanguageMessage;
	}
	public String getLanguageTranslation_InValidKeyPhraseMessage() {
		return LanguageTranslation_InValidKeyPhraseMessage;
	}
	public void setLanguageTranslation_InValidKeyPhraseMessage(
			String languageTranslation_InValidKeyPhraseMessage) {
		LanguageTranslation_InValidKeyPhraseMessage = languageTranslation_InValidKeyPhraseMessage;
	}
	public String getLanguageTranslation_BlankLanguageMessage() {
		return LanguageTranslation_BlankLanguageMessage;
	}
	public void setLanguageTranslation_BlankLanguageMessage(
			String languageTranslation_BlankLanguageMessage) {
		LanguageTranslation_BlankLanguageMessage = languageTranslation_BlankLanguageMessage;
	}
	public String getLanguageTranslation_BlankKeyphraseMessage() {
		return LanguageTranslation_BlankKeyphraseMessage;
	}
	public void setLanguageTranslation_BlankKeyphraseMessage(
			String languageTranslation_BlankKeyphraseMessage) {
		LanguageTranslation_BlankKeyphraseMessage = languageTranslation_BlankKeyphraseMessage;
	}
	public String getLanguageTranslation_KeyPhraseAlreadyExist() {
		return LanguageTranslation_KeyPhraseAlreadyExist;
	}
	public void setLanguageTranslation_KeyPhraseAlreadyExist(
			String languageTranslation_KeyPhraseAlreadyExist) {
		LanguageTranslation_KeyPhraseAlreadyExist = languageTranslation_KeyPhraseAlreadyExist;
	}
	public String getLanguageTranslation_MandatoryFieldMessage() {
		return LanguageTranslation_MandatoryFieldMessage;
	}
	public void setLanguageTranslation_MandatoryFieldMessage(
			String languageTranslation_MandatoryFieldMessage) {
		LanguageTranslation_MandatoryFieldMessage = languageTranslation_MandatoryFieldMessage;
	}

	
}
