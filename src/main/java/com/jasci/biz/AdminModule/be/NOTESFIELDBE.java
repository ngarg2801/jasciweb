/*
 
Date Developed :Dec 12 2014
Created by: Rakesh Pal
Description :NOTESFIELDBE IS USED AS TO SET/GET THE NOTES FIELD VALUES
 */
package com.jasci.biz.AdminModule.be;

public class NOTESFIELDBE {
	
	private String Tenant_Id;
	private String Company_Id;
	private String Note_Id;
	private String Note_Link;
	public String getNotesExtra() {
		return NotesExtra;
	}
	public void setNotesExtra(String notesExtra) {
		NotesExtra = notesExtra;
	}
	private String AUTO_ID;
	
	private String Last_Activity_Date;
	private String Last_Activity_Team_Member;
	private String Note;
	private String Formatted_date;
	private String NotesExtra;
	

	public String getFormatted_date() {
		return Formatted_date;
	}
	public void setFormatted_date(String formatted_date) {
		Formatted_date = formatted_date;
	}
	public String getAUTO_ID() {
		return AUTO_ID;
	}
	public void setAUTO_ID(String aUTO_ID) {
		AUTO_ID = aUTO_ID;
	}

	public String getTenant_Id() {
		return Tenant_Id;
	}
	public void setTenant_Id(String tenant_Id) {
		Tenant_Id = tenant_Id;
	}
	public String getCompany_Id() {
		return Company_Id;
	}
	public void setCompany_Id(String company_Id) {
		Company_Id = company_Id;
	}
	public String getNote_Id() {
		return Note_Id;
	}
	public void setNote_Id(String note_Id) {
		Note_Id = note_Id;
	}

	
	public String getNote_Link() {
		return Note_Link;
	}
	public void setNote_Link(String note_Link) {
		Note_Link = note_Link;
	}
	public String getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(String last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	public String getNote() {
		return Note;
	}
	public void setNote(String note) {
		Note = note;
	}

}
