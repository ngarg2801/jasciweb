/**
 *   
   
  
 * @author Shailendra Rajput
 * @Date created date :oct 1,2015
 * @Description: ProductBE Class
 * **/
package com.jasci.biz.AdminModule.be;

import java.util.Date;

public class PRODUCTSBE {

	private String tenant_ID;
	private String company_ID;

	private String product;

	private String quality;

	private String description20;

	private String description50;

	private String measurment_type;

	private String style_code;

	private String size_code;

	private String color_code;

	private String product_group_code;

	private String product_sub_group_code;

	private String accounting_code;

	private String country_of_origin;

	private String harmonized_tariff_code;

	private String serial_type01;

	private String serial_type02;

	private String serial_type03;

	private String serial_type04;

	private String serial_type05;
	private String serial_type06;

	private String serial_type07;

	private String serial_type08;

	private String serial_type09;
	private String serial_type10;
	
	
	private String lot_control_product;

	private long number_of_days_to_expiration;

	private String ship_as_is;

	private String drop_ship_item_flag;

	private String division;

	private String product_abc_codes;
	private long product_value;

	private String data_sequence;

	private String returns_allowed;

	private String season_code;

	private String requires_cube;

	private String handling_code;

	private Date first_received_date;

	private String hazmat_code;

	private String secured_level;

	private String non_stock_item;

	private String case_pickable;

	private String special_handling_code;

	private long pallet_block;

	private long pallet_tier;

	private long max_number_pallet_stacked;
	
	private String product_image_high;

	public String getQuantity() {
		return Quantity;
	}

	public void setQuantity(String quantity) {
		Quantity = quantity;
	}

	private String product_image_low;

	private long last_landed_cost;

	private Date last_activity_date;

	private String last_activity_team_member;

	private String last_activity_task;
	
	private String Quantity;
	

	public String getTenant_ID() {
		return tenant_ID;
	}

	public void setTenant_ID(String tenantT_ID) {
		this.tenant_ID = tenantT_ID;
	}

	public String getCompany_ID() {
		return company_ID;
	}

	public void setCompany_ID(String company_ID) {
		this.company_ID = company_ID;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public String getDescription20() {
		return description20;
	}

	public void setDescription20(String description20) {
		this.description20 = description20;
	}

	public String getDescription50() {
		return description50;
	}

	public void setDescription50(String description50) {
		this.description50 = description50;
	}

	public String getMeasurment_type() {
		return measurment_type;
	}

	public void setMeasurment_type(String measurment_type) {
		this.measurment_type = measurment_type;
	}

	public String getStyle_code() {
		return style_code;
	}

	public void setStyle_code(String style_code) {
		this.style_code = style_code;
	}

	public String getSize_code() {
		return size_code;
	}

	public void setSize_code(String size_code) {
		this.size_code = size_code;
	}

	public String getColor_code() {
		return color_code;
	}

	public void setColor_code(String color_code) {
		this.color_code = color_code;
	}

	public String getProduct_group_code() {
		return product_group_code;
	}

	public void setProduct_group_code(String product_group_code) {
		this.product_group_code = product_group_code;
	}

	public String getProduct_sub_group_code() {
		return product_sub_group_code;
	}

	public void setProduct_sub_group_code(String product_sub_group_code) {
		this.product_sub_group_code = product_sub_group_code;
	}

	public String getAccounting_code() {
		return accounting_code;
	}

	public void setAccounting_code(String accounting_code) {
		this.accounting_code = accounting_code;
	}

	public String getCountry_of_origin() {
		return country_of_origin;
	}

	public void setCountry_of_origin(String country_of_origin) {
		this.country_of_origin = country_of_origin;
	}

	public String getHarmonized_tariff_code() {
		return harmonized_tariff_code;
	}

	public void setHarmonized_tariff_code(String harmonized_tariff_code) {
		this.harmonized_tariff_code = harmonized_tariff_code;
	}

	public String getSerial_type01() {
		return serial_type01;
	}

	public void setSerial_type01(String serial_type01) {
		this.serial_type01 = serial_type01;
	}

	public String getSerial_type02() {
		return serial_type02;
	}

	public void setSerial_type02(String serial_type02) {
		this.serial_type02 = serial_type02;
	}

	public String getSerial_type03() {
		return serial_type03;
	}

	public void setSerial_type03(String serial_type03) {
		this.serial_type03 = serial_type03;
	}

	public String getSerial_type04() {
		return serial_type04;
	}

	public void setSerial_type04(String serial_type04) {
		this.serial_type04 = serial_type04;
	}

	public String getSerial_type05() {
		return serial_type05;
	}

	public void setSerial_type05(String serial_type05) {
		this.serial_type05 = serial_type05;
	}

	public String getSerial_type06() {
		return serial_type06;
	}

	public void setSerial_type06(String serial_type06) {
		this.serial_type06 = serial_type06;
	}

	public String getSerial_type07() {
		return serial_type07;
	}

	public void setSerial_type07(String serial_type07) {
		this.serial_type07 = serial_type07;
	}

	public String getSerial_type08() {
		return serial_type08;
	}

	public void setSerial_type08(String serial_type08) {
		this.serial_type08 = serial_type08;
	}

	public String getSerial_type09() {
		return serial_type09;
	}

	public void setSerial_type09(String serial_type09) {
		this.serial_type09 = serial_type09;
	}

	public String getSerial_type10() {
		return serial_type10;
	}

	public void setSerial_type10(String serial_type10) {
		this.serial_type10 = serial_type10;
	}

	public String getLot_control_product() {
		return lot_control_product;
	}

	public void setLot_control_product(String lot_control_product) {
		this.lot_control_product = lot_control_product;
	}

	public long getNumber_of_days_to_expiration() {
		return number_of_days_to_expiration;
	}

	public void setNumber_of_days_to_expiration(long number_of_days_to_expiration) {
		this.number_of_days_to_expiration = number_of_days_to_expiration;
	}

	public String getShip_as_is() {
		return ship_as_is;
	}

	public void setShip_as_is(String ship_as_is) {
		this.ship_as_is = ship_as_is;
	}

	public String getDrop_ship_item_flag() {
		return drop_ship_item_flag;
	}

	public void setDrop_ship_item_flag(String drop_ship_item_flag) {
		this.drop_ship_item_flag = drop_ship_item_flag;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getProduct_abc_codes() {
		return product_abc_codes;
	}

	public void setProduct_abc_codes(String product_abc_codes) {
		this.product_abc_codes = product_abc_codes;
	}

	public long getProduct_value() {
		return product_value;
	}

	public void setProduct_value(long product_value) {
		this.product_value = product_value;
	}

	public String getData_sequence() {
		return data_sequence;
	}

	public void setData_sequence(String data_sequence) {
		this.data_sequence = data_sequence;
	}

	public String getReturns_allowed() {
		return returns_allowed;
	}

	public void setReturns_allowed(String returns_allowed) {
		this.returns_allowed = returns_allowed;
	}

	public String getSeason_code() {
		return season_code;
	}

	public void setSeason_code(String season_code) {
		this.season_code = season_code;
	}

	public String getRequires_cube() {
		return requires_cube;
	}

	public void setRequires_cube(String requires_cube) {
		this.requires_cube = requires_cube;
	}

	public String getHandling_code() {
		return handling_code;
	}

	public void setHandling_code(String handling_code) {
		this.handling_code = handling_code;
	}

	public Date getFirst_received_date() {
		return first_received_date;
	}

	public void setFirst_received_date(Date first_received_date) {
		this.first_received_date = first_received_date;
	}

	public String getHazmat_code() {
		return hazmat_code;
	}

	public void setHazmat_code(String hazmat_code) {
		this.hazmat_code = hazmat_code;
	}

	public String getSecured_level() {
		return secured_level;
	}

	public void setSecured_level(String secured_level) {
		this.secured_level = secured_level;
	}

	public String getNon_stock_item() {
		return non_stock_item;
	}

	public void setNon_stock_item(String non_stock_item) {
		this.non_stock_item = non_stock_item;
	}

	public String getCase_pickable() {
		return case_pickable;
	}

	public void setCase_pickable(String case_pickable) {
		this.case_pickable = case_pickable;
	}

	public String getSpecial_handling_code() {
		return special_handling_code;
	}

	public void setSpecial_handling_code(String special_handling_code) {
		this.special_handling_code = special_handling_code;
	}

	public long getPallet_block() {
		return pallet_block;
	}

	public void setPallet_block(long pallet_block) {
		this.pallet_block = pallet_block;
	}

	public long getPallet_tier() {
		return pallet_tier;
	}

	public void setPallet_tier(long pallet_tier) {
		this.pallet_tier = pallet_tier;
	}

	public long getMax_number_pallet_stacked() {
		return max_number_pallet_stacked;
	}

	public void setMax_number_pallet_stacked(long max_number_pallet_stacked) {
		this.max_number_pallet_stacked = max_number_pallet_stacked;
	}

	public String getProduct_image_high() {
		return product_image_high;
	}

	public void setProduct_image_high(String product_image_high) {
		this.product_image_high = product_image_high;
	}

	public String getProduct_image_low() {
		return product_image_low;
	}

	public void setProduct_image_low(String product_image_low) {
		this.product_image_low = product_image_low;
	}

	public long getLast_landed_cost() {
		return last_landed_cost;
	}

	public void setLast_landed_cost(long last_landed_cost) {
		this.last_landed_cost = last_landed_cost;
	}

	public Date getLast_activity_date() {
		return last_activity_date;
	}

	public void setLast_activity_date(Date last_activity_date) {
		this.last_activity_date = last_activity_date;
	}

	public String getLast_activity_team_member() {
		return last_activity_team_member;
	}

	public void setLast_activity_team_member(String last_activity_team_member) {
		this.last_activity_team_member = last_activity_team_member;
	}

	public String getLast_activity_task() {
		return last_activity_task;
	}

	public void setLast_activity_task(String last_activity_task) {
		this.last_activity_task = last_activity_task;
	}


	

	

}
