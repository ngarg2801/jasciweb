/**

Description It is used to make setter and getter for changing text of screen in another language
Created By Sarvendra Tyagi
Created Date Nov 2 2014
 */

package com.jasci.biz.AdminModule.be;

public class MENUOPTIONLABELBE {
	

private String keyPharseMenuOptionSearchLookUp="MenuOptionSearchLookUp";
private String keyPharseTenant="tenant";
private String keyPharseMenuType="menutype";
private String keyPharseMenuOption="menuoption";
private String keyPharseDescription="description";
private String keyPharseApplication="application";
private String keyPharseApplicationSub="applicationSub";
private String keyPharseAction="Actions";
private String keyPharseCancel="Cancel";
private String keyPharseDelete="Delete";
private String keyPharseEdit="Edit";
private String keyPharseAddnew="Add New"; 
private String keyPharseNew="new";
private String keyPharsepartOfApplication="partOfApplication";
private String keyPharsePartOfMenuOption="PartOfMenuOption";
private String keyPharsePartOFtheMenuOptionDescription="PartOFtheMenuOptionDescription";
private String keyPharseTenantID="tenantID";
private String keyPharseLastActivityDate="LastActivityDate";
private String keyPharseLastActivityBy="lastactivityby";
private String keyPharseHelpLine="helpline";
private String keyPharseExecutionPath="ExecutionPath";
private String keyPharseMenuOptionMantenance="MenuOptionMantenance";
private String keyPharseMenuOptionMaintenanceLookUp="MenuOptionMaintenanceLookUp";
private String keyPharseCompany="Company";
private String keyPharseUpdate_Save="Save/Update";

private String keyPharsedescriptionlong="descriptionlong";
private String keyPharsedescriptionshrot="descriptionshrot";
private String keyPharseON_UPDATE_SUCCESSFULLY="ON_UPDATE_SUCCESSFULLY";
private String keyPharseON_SAVE_SUCCESSFULLY="ON_SAVE_SUCCESSFULLY";
private String keyPharseERR_MANDATORY_FIELD_LEFT_BLANK="ERR_MANDATORY FIELD_LEFT BLANK";
private String keyPharseERR_MENU_OPTION_ALREADY_USED="ERR_MENU_OPTION_ALREADY_USED";
private String keyPharseERR_WHILE_DELETING_MENU_OPTION="ERR_WHILE_DELETING_MENU_OPTION";
private String keyPharseERR_WHILE_DELETING_MENU_PROFILE="ERR_WHILE_DELETING_MENU_PROFILE";
private String keyPharseERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK="ERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK";
private String keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION="ERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION";
private String keyPharseERR_MENU_TYPE_LEFT_BLANK="ERR_MENU_TYPE_LEFT_BLANK";
private String keyPharseERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK="ERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK";
private String keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION="ERR_INVALID_PART_OF_THE_MENU_OPTION";
private String keyPharseERR_MENU_OPTION_LEFT_BLANK="ERR_MENU_OPTION_LEFT_BLANK";
private String keyPharseERR_INVALID_PART_OF_THE_APPLICATION_NAME="ERR_INVALID_PART_OF_THE_APPLICATION_NAME";
private String keyPharseERR_INVALID_MENU_OPTION="ERR_INVALID_MENU_OPTION";
private String keyPharseERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK="ERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK";
private String keyPharseERR_APPLICATION_LEFT_BLANK="ERR_APPLICATION_LEFT_BLANK";
private String keyPharseERR_INVALID_APPLICATION="ERR_INVALID_APPLICATION";
private String keyPharsedisplayall="displayall";
private String keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK="ERR_INVALID_MENU_TYPE_LEFT_BLANK";
private String keyPharseErr_INVALID_TENANTID="Err_INVALID_TENANTID";
private String keyPharseErr_INVALID_TENANT_ID_LEFT_BLANK="Err_INVALID_TENANT_ID_LEFT_BLANK";

private String lblMenu_Execution_Path_Not_Valid;

public String getLblMenu_Execution_Path_Not_Valid() {
	return lblMenu_Execution_Path_Not_Valid;
}
public void setLblMenu_Execution_Path_Not_Valid(String lblMenu_Execution_Path_Not_Valid) {
	this.lblMenu_Execution_Path_Not_Valid = lblMenu_Execution_Path_Not_Valid;
}
private String lblReset;

public String getLblReset() {
	return lblReset;
}
public void setLblReset(String lblReset) {
	this.lblReset = lblReset;
}
private String keyPharse_SELECT_MENU_TYPE="SELECT_MENU_TYPE";
public String getKeyPharse_SELECT_MENU_TYPE() {
	return keyPharse_SELECT_MENU_TYPE;
}
public void setKeyPharse_SELECT_MENU_TYPE(String keyPharse_SELECT_MENU_TYPE) {
	this.keyPharse_SELECT_MENU_TYPE = keyPharse_SELECT_MENU_TYPE;
}
public String getLblSELECT_MENU_TYPE() {
	return lblSELECT_MENU_TYPE;
}
public void setLblSELECT_MENU_TYPE(String lblSELECT_MENU_TYPE) {
	this.lblSELECT_MENU_TYPE = lblSELECT_MENU_TYPE;
}
private String lblSELECT_MENU_TYPE;


private String lblErr_INVALID_TENANT_ID_LEFT_BLANK;
public String getLblErr_INVALID_TENANT_ID_LEFT_BLANK() {
	return lblErr_INVALID_TENANT_ID_LEFT_BLANK;
}
public void setLblErr_INVALID_TENANT_ID_LEFT_BLANK(String lblErr_INVALID_TENANT_ID_LEFT_BLANK) {
	this.lblErr_INVALID_TENANT_ID_LEFT_BLANK = lblErr_INVALID_TENANT_ID_LEFT_BLANK;
}
private String lblErr_INVALID_TENANTID;

public String getKeyPharseErr_INVALID_TENANTID() {
	return keyPharseErr_INVALID_TENANTID;
}
public void setKeyPharseErr_INVALID_TENANTID(String keyPharseErr_INVALID_TENANTID) {
	this.keyPharseErr_INVALID_TENANTID = keyPharseErr_INVALID_TENANTID;
}
public String getKeyPharseErr_INVALID_TENANT_ID_LEFT_BLANK() {
	return keyPharseErr_INVALID_TENANT_ID_LEFT_BLANK;
}
public void setKeyPharseErr_INVALID_TENANT_ID_LEFT_BLANK(String keyPharseErr_INVALID_TENANT_ID_LEFT_BLANK) {
	this.keyPharseErr_INVALID_TENANT_ID_LEFT_BLANK = keyPharseErr_INVALID_TENANT_ID_LEFT_BLANK;
}
public String getLblErr_INVALID_TENANTID() {
	return lblErr_INVALID_TENANTID;
}
public void setLblErr_INVALID_TENANTID(String lblErr_INVALID_TENANTID) {
	this.lblErr_INVALID_TENANTID = lblErr_INVALID_TENANTID;
}
public String getKeyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK() {
	return keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK;
}
public void setKeyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK(String keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK) {
	this.keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK = keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK;
}
private String lblCompany;
private String lblUpdate_Save;

private String lbldescriptionlong;
private String lbldescriptionshrot;
private String lblERR_INVALID_MENU_TYPE_LEFT_BLANK;
private String keyPharseGeneralCodeMenuTypes="MenuTypes";
private String keyPharseGeneralCodeApplication="Applications";
private String keyPharseSelect="Select";


public String getKeyPharseSelect() {
	return keyPharseSelect;
}
public void setKeyPharseSelect(String keyPharseSelect) {
	this.keyPharseSelect = keyPharseSelect;
}
public String getLblSelect() {
	return lblSelect;
}
public void setLblSelect(String lblSelect) {
	this.lblSelect = lblSelect;
}
private String lblSelect;

private String lblGeneralCodeMenuTypes;
private String lblGeneralCodeApplication;


public String getKeyPharseGeneralCodeMenuTypes() {
	return keyPharseGeneralCodeMenuTypes;
}
public void setKeyPharseGeneralCodeMenuTypes(String keyPharseGeneralCodeMenuTypes) {
	this.keyPharseGeneralCodeMenuTypes = keyPharseGeneralCodeMenuTypes;
}
public String getKeyPharseGeneralCodeApplication() {
	return keyPharseGeneralCodeApplication;
}
public void setKeyPharseGeneralCodeApplication(String keyPharseGeneralCodeApplication) {
	this.keyPharseGeneralCodeApplication = keyPharseGeneralCodeApplication;
}
public String getLblGeneralCodeMenuTypes() {
	return lblGeneralCodeMenuTypes;
}
public void setLblGeneralCodeMenuTypes(String lblGeneralCodeMenuTypes) {
	this.lblGeneralCodeMenuTypes = lblGeneralCodeMenuTypes;
}
public String getLblGeneralCodeApplication() {
	return lblGeneralCodeApplication;
}
public void setLblGeneralCodeApplication(String lblGeneralCodeApplication) {
	this.lblGeneralCodeApplication = lblGeneralCodeApplication;
}
public String getLblERR_INVALID_MENU_TYPE_LEFT_BLANK() {
	return lblERR_INVALID_MENU_TYPE_LEFT_BLANK;
}
public void setLblERR_INVALID_MENU_TYPE_LEFT_BLANK(String lblERR_INVALID_MENU_TYPE_LEFT_BLANK) {
	this.lblERR_INVALID_MENU_TYPE_LEFT_BLANK = lblERR_INVALID_MENU_TYPE_LEFT_BLANK;
}
public String getLblCompany() {
	return lblCompany;
}
public void setLblCompany(String lblCompany) {
	this.lblCompany = lblCompany;
}
public String getLblUpdate_Save() {
	return lblUpdate_Save;
}
public void setLblUpdate_Save(String lblUpdate_Save) {
	this.lblUpdate_Save = lblUpdate_Save;
}
public String getLbldescriptionlong() {
	return lbldescriptionlong;
}
public void setLbldescriptionlong(String lbldescriptionlong) {
	this.lbldescriptionlong = lbldescriptionlong;
}
public String getLbldescriptionshrot() {
	return lbldescriptionshrot;
}
public void setLbldescriptionshrot(String lbldescriptionshrot) {
	this.lbldescriptionshrot = lbldescriptionshrot;
}
public String getLblON_UPDATE_SUCCESSFULLY() {
	return lblON_UPDATE_SUCCESSFULLY;
}
public void setLblON_UPDATE_SUCCESSFULLY(String lblON_UPDATE_SUCCESSFULLY) {
	this.lblON_UPDATE_SUCCESSFULLY = lblON_UPDATE_SUCCESSFULLY;
}
public String getLblON_SAVE_SUCCESSFULLY() {
	return lblON_SAVE_SUCCESSFULLY;
}
public void setLblON_SAVE_SUCCESSFULLY(String lblON_SAVE_SUCCESSFULLY) {
	this.lblON_SAVE_SUCCESSFULLY = lblON_SAVE_SUCCESSFULLY;
}
public String getLblERR_MANDATORY_FIELD_LEFT_BLANK() {
	return lblERR_MANDATORY_FIELD_LEFT_BLANK;
}
public void setLblERR_MANDATORY_FIELD_LEFT_BLANK(String lblERR_MANDATORY_FIELD_LEFT_BLANK) {
	this.lblERR_MANDATORY_FIELD_LEFT_BLANK = lblERR_MANDATORY_FIELD_LEFT_BLANK;
}
public String getLblERR_MENU_OPTION_ALREADY_USED() {
	return lblERR_MENU_OPTION_ALREADY_USED;
}
public void setLblERR_MENU_OPTION_ALREADY_USED(String lblERR_MENU_OPTION_ALREADY_USED) {
	this.lblERR_MENU_OPTION_ALREADY_USED = lblERR_MENU_OPTION_ALREADY_USED;
}
public String getLblERR_WHILE_DELETING_MENU_OPTION() {
	return lblERR_WHILE_DELETING_MENU_OPTION;
}
public void setLblERR_WHILE_DELETING_MENU_OPTION(String lblERR_WHILE_DELETING_MENU_OPTION) {
	this.lblERR_WHILE_DELETING_MENU_OPTION = lblERR_WHILE_DELETING_MENU_OPTION;
}
public String getLblERR_WHILE_DELETING_MENU_PROFILE() {
	return lblERR_WHILE_DELETING_MENU_PROFILE;
}
public void setLblERR_WHILE_DELETING_MENU_PROFILE(String lblERR_WHILE_DELETING_MENU_PROFILE) {
	this.lblERR_WHILE_DELETING_MENU_PROFILE = lblERR_WHILE_DELETING_MENU_PROFILE;
}
public String getLblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK() {
	return lblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK;
}
public void setLblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK(
		String lblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK) {
	this.lblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK = lblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK;
}
public String getLblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION() {
	return lblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION;
}
public void setLblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION(
		String lblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION) {
	this.lblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION = lblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION;
}
public String getLblERR_MENU_TYPE_LEFT_BLANK() {
	return lblERR_MENU_TYPE_LEFT_BLANK;
}
public void setLblERR_MENU_TYPE_LEFT_BLANK(String lblERR_MENU_TYPE_LEFT_BLANK) {
	this.lblERR_MENU_TYPE_LEFT_BLANK = lblERR_MENU_TYPE_LEFT_BLANK;
}
public String getLblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK() {
	return lblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK;
}
public void setLblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK(String lblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK) {
	this.lblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK = lblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK;
}
public String getLblERR_INVALID_PART_OF_THE_MENU_OPTION() {
	return lblERR_INVALID_PART_OF_THE_MENU_OPTION;
}
public void setLblERR_INVALID_PART_OF_THE_MENU_OPTION(String lblERR_INVALID_PART_OF_THE_MENU_OPTION) {
	this.lblERR_INVALID_PART_OF_THE_MENU_OPTION = lblERR_INVALID_PART_OF_THE_MENU_OPTION;
}
public String getLblERR_MENU_OPTION_LEFT_BLANK() {
	return lblERR_MENU_OPTION_LEFT_BLANK;
}
public void setLblERR_MENU_OPTION_LEFT_BLANK(String lblERR_MENU_OPTION_LEFT_BLANK) {
	this.lblERR_MENU_OPTION_LEFT_BLANK = lblERR_MENU_OPTION_LEFT_BLANK;
}
public String getLblERR_INVALID_PART_OF_THE_APPLICATION_NAME() {
	return lblERR_INVALID_PART_OF_THE_APPLICATION_NAME;
}
public void setLblERR_INVALID_PART_OF_THE_APPLICATION_NAME(String lblERR_INVALID_PART_OF_THE_APPLICATION_NAME) {
	this.lblERR_INVALID_PART_OF_THE_APPLICATION_NAME = lblERR_INVALID_PART_OF_THE_APPLICATION_NAME;
}
public String getLblERR_INVALID_MENU_OPTION() {
	return lblERR_INVALID_MENU_OPTION;
}
public void setLblERR_INVALID_MENU_OPTION(String lblERR_INVALID_MENU_OPTION) {
	this.lblERR_INVALID_MENU_OPTION = lblERR_INVALID_MENU_OPTION;
}
public String getLblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK() {
	return lblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK;
}
public void setLblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK(String lblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK) {
	this.lblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK = lblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK;
}
public String getLblERR_APPLICATION_LEFT_BLANK() {
	return lblERR_APPLICATION_LEFT_BLANK;
}
public void setLblERR_APPLICATION_LEFT_BLANK(String lblERR_APPLICATION_LEFT_BLANK) {
	this.lblERR_APPLICATION_LEFT_BLANK = lblERR_APPLICATION_LEFT_BLANK;
}
public String getLblERR_INVALID_APPLICATION() {
	return lblERR_INVALID_APPLICATION;
}
public void setLblERR_INVALID_APPLICATION(String lblERR_INVALID_APPLICATION) {
	this.lblERR_INVALID_APPLICATION = lblERR_INVALID_APPLICATION;
}
public String getLbldisplayall() {
	return lbldisplayall;
}
public void setLbldisplayall(String lbldisplayall) {
	this.lbldisplayall = lbldisplayall;
}
private String lblON_UPDATE_SUCCESSFULLY;
private String lblON_SAVE_SUCCESSFULLY;
private String lblERR_MANDATORY_FIELD_LEFT_BLANK;
private String lblERR_MENU_OPTION_ALREADY_USED;
private String lblERR_WHILE_DELETING_MENU_OPTION;
private String lblERR_WHILE_DELETING_MENU_PROFILE;
private String lblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK;
private String lblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION;
private String lblERR_MENU_TYPE_LEFT_BLANK;
private String lblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK;
private String lblERR_INVALID_PART_OF_THE_MENU_OPTION;
private String lblERR_MENU_OPTION_LEFT_BLANK;
private String lblERR_INVALID_PART_OF_THE_APPLICATION_NAME;
private String lblERR_INVALID_MENU_OPTION;
private String lblERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK;
private String lblERR_APPLICATION_LEFT_BLANK;
private String lblERR_INVALID_APPLICATION;
private String lbldisplayall;


public String getKeyPharseMenuOptionSearchLookUp() {
	return keyPharseMenuOptionSearchLookUp;
}
public void setKeyPharseMenuOptionSearchLookUp(String keyPharseMenuOptionSearchLookUp) {
	this.keyPharseMenuOptionSearchLookUp = keyPharseMenuOptionSearchLookUp;
}
public String getKeyPharseTenant() {
	return keyPharseTenant;
}
public void setKeyPharseTenant(String keyPharseTenant) {
	this.keyPharseTenant = keyPharseTenant;
}
public String getKeyPharseMenuType() {
	return keyPharseMenuType;
}
public void setKeyPharseMenuType(String keyPharseMenuType) {
	this.keyPharseMenuType = keyPharseMenuType;
}
public String getKeyPharseMenuOption() {
	return keyPharseMenuOption;
}
public void setKeyPharseMenuOption(String keyPharseMenuOption) {
	this.keyPharseMenuOption = keyPharseMenuOption;
}
public String getKeyPharseDescription() {
	return keyPharseDescription;
}
public void setKeyPharseDescription(String keyPharseDescription) {
	this.keyPharseDescription = keyPharseDescription;
}
public String getKeyPharseApplication() {
	return keyPharseApplication;
}
public void setKeyPharseApplication(String keyPharseApplication) {
	this.keyPharseApplication = keyPharseApplication;
}
public String getKeyPharseApplicationSub() {
	return keyPharseApplicationSub;
}
public void setKeyPharseApplicationSub(String keyPharseApplicationSub) {
	this.keyPharseApplicationSub = keyPharseApplicationSub;
}
public String getKeyPharseAction() {
	return keyPharseAction;
}
public void setKeyPharseAction(String keyPharseAction) {
	this.keyPharseAction = keyPharseAction;
}
public String getKeyPharseCancel() {
	return keyPharseCancel;
}
public void setKeyPharseCancel(String keyPharseCancel) {
	this.keyPharseCancel = keyPharseCancel;
}
public String getKeyPharseDelete() {
	return keyPharseDelete;
}
public void setKeyPharseDelete(String keyPharseDelete) {
	this.keyPharseDelete = keyPharseDelete;
}
public String getKeyPharseEdit() {
	return keyPharseEdit;
}
public void setKeyPharseEdit(String keyPharseEdit) {
	this.keyPharseEdit = keyPharseEdit;
}
public String getKeyPharseAddnew() {
	return keyPharseAddnew;
}
public void setKeyPharseAddnew(String keyPharseAddnew) {
	this.keyPharseAddnew = keyPharseAddnew;
}
public String getKeyPharsedescriptionlong() {
	return keyPharsedescriptionlong;
}
public void setKeyPharsedescriptionlong(String keyPharsedescriptionlong) {
	this.keyPharsedescriptionlong = keyPharsedescriptionlong;
}
public String getKeyPharsedescriptionshrot() {
	return keyPharsedescriptionshrot;
}
public void setKeyPharsedescriptionshrot(String keyPharsedescriptionshrot) {
	this.keyPharsedescriptionshrot = keyPharsedescriptionshrot;
}
public String getKeyPharseON_UPDATE_SUCCESSFULLY() {
	return keyPharseON_UPDATE_SUCCESSFULLY;
}
public void setKeyPharseON_UPDATE_SUCCESSFULLY(String keyPharseON_UPDATE_SUCCESSFULLY) {
	this.keyPharseON_UPDATE_SUCCESSFULLY = keyPharseON_UPDATE_SUCCESSFULLY;
}
public String getKeyPharseON_SAVE_SUCCESSFULLY() {
	return keyPharseON_SAVE_SUCCESSFULLY;
}
public void setKeyPharseON_SAVE_SUCCESSFULLY(String keyPharseON_SAVE_SUCCESSFULLY) {
	this.keyPharseON_SAVE_SUCCESSFULLY = keyPharseON_SAVE_SUCCESSFULLY;
}
public String getKeyPharseERR_MANDATORY_FIELD_LEFT_BLANK() {
	return keyPharseERR_MANDATORY_FIELD_LEFT_BLANK;
}
public void setKeyPharseERR_MANDATORY_FIELD_LEFT_BLANK(String keyPharseERR_MANDATORY_FIELD_LEFT_BLANK) {
	this.keyPharseERR_MANDATORY_FIELD_LEFT_BLANK = keyPharseERR_MANDATORY_FIELD_LEFT_BLANK;
}
public String getKeyPharseERR_MENU_OPTION_ALREADY_USED() {
	return keyPharseERR_MENU_OPTION_ALREADY_USED;
}
public void setKeyPharseERR_MENU_OPTION_ALREADY_USED(String keyPharseERR_MENU_OPTION_ALREADY_USED) {
	this.keyPharseERR_MENU_OPTION_ALREADY_USED = keyPharseERR_MENU_OPTION_ALREADY_USED;
}
public String getKeyPharseERR_WHILE_DELETING_MENU_OPTION() {
	return keyPharseERR_WHILE_DELETING_MENU_OPTION;
}
public void setKeyPharseERR_WHILE_DELETING_MENU_OPTION(String keyPharseERR_WHILE_DELETING_MENU_OPTION) {
	this.keyPharseERR_WHILE_DELETING_MENU_OPTION = keyPharseERR_WHILE_DELETING_MENU_OPTION;
}
public String getKeyPharseERR_WHILE_DELETING_MENU_PROFILE() {
	return keyPharseERR_WHILE_DELETING_MENU_PROFILE;
}
public void setKeyPharseERR_WHILE_DELETING_MENU_PROFILE(String keyPharseERR_WHILE_DELETING_MENU_PROFILE) {
	this.keyPharseERR_WHILE_DELETING_MENU_PROFILE = keyPharseERR_WHILE_DELETING_MENU_PROFILE;
}
public String getKeyPharseERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK() {
	return keyPharseERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK;
}
public void setKeyPharseERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK(
		String keyPharseERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK) {
	this.keyPharseERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK = keyPharseERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK;
}
public String getKeyPharseERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION() {
	return keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION;
}
public void setKeyPharseERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION(
		String keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION) {
	this.keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION = keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION;
}
public String getKeyPharseERR_MENU_TYPE_LEFT_BLANK() {
	return keyPharseERR_MENU_TYPE_LEFT_BLANK;
}
public void setKeyPharseERR_MENU_TYPE_LEFT_BLANK(String keyPharseERR_MENU_TYPE_LEFT_BLANK) {
	this.keyPharseERR_MENU_TYPE_LEFT_BLANK = keyPharseERR_MENU_TYPE_LEFT_BLANK;
}
public String getKeyPharseERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK() {
	return keyPharseERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK;
}
public void setKeyPharseERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK(String keyPharseERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK) {
	this.keyPharseERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK = keyPharseERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK;
}
public String getKeyPharseERR_INVALID_PART_OF_THE_MENU_OPTION() {
	return keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION;
}
public void setKeyPharseERR_INVALID_PART_OF_THE_MENU_OPTION(String keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION) {
	this.keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION = keyPharseERR_INVALID_PART_OF_THE_MENU_OPTION;
}
public String getKeyPharseERR_MENU_OPTION_LEFT_BLANK() {
	return keyPharseERR_MENU_OPTION_LEFT_BLANK;
}
public void setKeyPharseERR_MENU_OPTION_LEFT_BLANK(String keyPharseERR_MENU_OPTION_LEFT_BLANK) {
	this.keyPharseERR_MENU_OPTION_LEFT_BLANK = keyPharseERR_MENU_OPTION_LEFT_BLANK;
}
public String getKeyPharseERR_INVALID_PART_OF_THE_APPLICATION_NAME() {
	return keyPharseERR_INVALID_PART_OF_THE_APPLICATION_NAME;
}
public void setKeyPharseERR_INVALID_PART_OF_THE_APPLICATION_NAME(
		String keyPharseERR_INVALID_PART_OF_THE_APPLICATION_NAME) {
	this.keyPharseERR_INVALID_PART_OF_THE_APPLICATION_NAME = keyPharseERR_INVALID_PART_OF_THE_APPLICATION_NAME;
}
public String getKeyPharseERR_INVALID_MENU_OPTION() {
	return keyPharseERR_INVALID_MENU_OPTION;
}
public void setKeyPharseERR_INVALID_MENU_OPTION(String keyPharseERR_INVALID_MENU_OPTION) {
	this.keyPharseERR_INVALID_MENU_OPTION = keyPharseERR_INVALID_MENU_OPTION;
}
public String getKeyPharseERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK() {
	return keyPharseERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK;
}
public void setKeyPharseERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK(
		String keyPharseERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK) {
	this.keyPharseERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK = keyPharseERR_PART_OF_THE_APPLICATION_NAME_LEFT_BLANK;
}
public String getKeyPharseERR_APPLICATION_LEFT_BLANK() {
	return keyPharseERR_APPLICATION_LEFT_BLANK;
}
public void setKeyPharseERR_APPLICATION_LEFT_BLANK(String keyPharseERR_APPLICATION_LEFT_BLANK) {
	this.keyPharseERR_APPLICATION_LEFT_BLANK = keyPharseERR_APPLICATION_LEFT_BLANK;
}
public String getKeyPharseERR_INVALID_APPLICATION() {
	return keyPharseERR_INVALID_APPLICATION;
}
public void setKeyPharseERR_INVALID_APPLICATION(String keyPharseERR_INVALID_APPLICATION) {
	this.keyPharseERR_INVALID_APPLICATION = keyPharseERR_INVALID_APPLICATION;
}
public String getKeyPharsedisplayall() {
	return keyPharsedisplayall;
}
public void setKeyPharsedisplayall(String keyPharsedisplayall) {
	this.keyPharsedisplayall = keyPharsedisplayall;
}
public String getKeyPharseCompany() {
	return keyPharseCompany;
}
public void setKeyPharseCompany(String keyPharseCompany) {
	this.keyPharseCompany = keyPharseCompany;
}
public String getKeyPharseUpdate_Save() {
	return keyPharseUpdate_Save;
}
public void setKeyPharseUpdate_Save(String keyPharseUpdate_Save) {
	this.keyPharseUpdate_Save = keyPharseUpdate_Save;
}
private String lblMenuOptionSearchLookUp;
private String lblTenant;
private String lblMenuType;
private String lblMenuOption;
private String lblDescription;
private String lblApplication;
private String lblApplicationSub;
private String lblAction;
private String lblCancel;
private String lblDelete;
private String lblEdit;
private String lblAddnew;
private String lblNew;
private String lblpartOfApplication;
private String lblPartOfMenuOption;
private String lblPartOFtheMenuOptionDescription;
private String lblTenantID;
private String lblLastActivityDate;
private String lblLastActivityBy;
private String lblHelpLine;
private String lblExecutionPath;
private String lblMenuOptionMantenance;
private String lblMenuOptionMaintenanceLookUp;



public String getKeyPharesMenuOptionSearchLookUp() {
	return keyPharseMenuOptionSearchLookUp;
}
public void setKeyPharesMenuOptionSearchLookUp(String keyPharesMenuOptionSearchLookUp) {
	this.keyPharseMenuOptionSearchLookUp = keyPharesMenuOptionSearchLookUp;
}
public String getKeyPharesTenant() {
	return keyPharseTenant;
}
public void setKeyPharesTenant(String keyPharesTenant) {
	this.keyPharseTenant = keyPharesTenant;
}
public String getKeyPharesMenuType() {
	return keyPharseMenuType;
}
public void setKeyPharesMenuType(String keyPharesMenuType) {
	this.keyPharseMenuType = keyPharesMenuType;
}
public String getKeyPharesMenuOption() {
	return keyPharseMenuOption;
}
public void setKeyPharesMenuOption(String keyPharesMenuOption) {
	this.keyPharseMenuOption = keyPharesMenuOption;
}
public String getKeyPharesDescription() {
	return keyPharseDescription;
}
public void setKeyPharesDescription(String keyPharesDescription) {
	this.keyPharseDescription = keyPharesDescription;
}
public String getKeyPharesApplication() {
	return keyPharseApplication;
}
public void setKeyPharesApplication(String keyPharesApplication) {
	this.keyPharseApplication = keyPharesApplication;
}
public String getKeyPharesApplicationSub() {
	return keyPharseApplicationSub;
}
public void setKeyPharesApplicationSub(String keyPharesApplicationSub) {
	this.keyPharseApplicationSub = keyPharesApplicationSub;
}
public String getKeyPharesAction() {
	return keyPharseAction;
}
public void setKeyPharesAction(String keyPharesAction) {
	this.keyPharseAction = keyPharesAction;
}
public String getKeyPharesCancel() {
	return keyPharseCancel;
}
public void setKeyPharesCancel(String keyPharesCancel) {
	this.keyPharseCancel = keyPharesCancel;
}
public String getKeyPharesDelete() {
	return keyPharseDelete;
}
public void setKeyPharesDelete(String keyPharesDelete) {
	this.keyPharseDelete = keyPharesDelete;
}
public String getKeyPharesEdit() {
	return keyPharseEdit;
}
public void setKeyPharesEdit(String keyPharesEdit) {
	this.keyPharseEdit = keyPharesEdit;
}
public String getKeyPharesAddnew() {
	return keyPharseAddnew;
}
public void setKeyPharesAddnew(String keyPharesAddnew) {
	this.keyPharseAddnew = keyPharesAddnew;
}
public String getKeyPharseNew() {
	return keyPharseNew;
}
public void setKeyPharseNew(String keyPharseNew) {
	this.keyPharseNew = keyPharseNew;
}
public String getKeyPharsepartOfApplication() {
	return keyPharsepartOfApplication;
}
public void setKeyPharsepartOfApplication(String keyPharsepartOfApplication) {
	this.keyPharsepartOfApplication = keyPharsepartOfApplication;
}
public String getKeyPharsePartOfMenuOption() {
	return keyPharsePartOfMenuOption;
}
public void setKeyPharsePartOfMenuOption(String keyPharsePartOfMenuOption) {
	this.keyPharsePartOfMenuOption = keyPharsePartOfMenuOption;
}
public String getKeyPharsePartOFtheMenuOptionDescription() {
	return keyPharsePartOFtheMenuOptionDescription;
}
public void setKeyPharsePartOFtheMenuOptionDescription(String keyPharsePartOFtheMenuOptionDescription) {
	this.keyPharsePartOFtheMenuOptionDescription = keyPharsePartOFtheMenuOptionDescription;
}
public String getKeyPharseTenantID() {
	return keyPharseTenantID;
}
public void setKeyPharseTenantID(String keyPharseTenantID) {
	this.keyPharseTenantID = keyPharseTenantID;
}
public String getKeyPharseLastActivityDate() {
	return keyPharseLastActivityDate;
}
public void setKeyPharseLastActivityDate(String keyPharseLastActivityDate) {
	this.keyPharseLastActivityDate = keyPharseLastActivityDate;
}
public String getKeyPharseLastActivityBy() {
	return keyPharseLastActivityBy;
}
public void setKeyPharseLastActivityBy(String keyPharseLastActivityBy) {
	this.keyPharseLastActivityBy = keyPharseLastActivityBy;
}
public String getKeyPharseHelpLine() {
	return keyPharseHelpLine;
}
public void setKeyPharseHelpLine(String keyPharseHelpLine) {
	this.keyPharseHelpLine = keyPharseHelpLine;
}
public String getKeyPharseExecutionPath() {
	return keyPharseExecutionPath;
}
public void setKeyPharseExecutionPath(String keyPharseExecutionPath) {
	this.keyPharseExecutionPath = keyPharseExecutionPath;
}
public String getKeyPharseMenuOptionMantenance() {
	return keyPharseMenuOptionMantenance;
}
public void setKeyPharseMenuOptionMantenance(String keyPharseMenuOptionMantenance) {
	this.keyPharseMenuOptionMantenance = keyPharseMenuOptionMantenance;
}
public String getKeyPharseMenuOptionMaintenanceLookUp() {
	return keyPharseMenuOptionMaintenanceLookUp;
}
public void setKeyPharseMenuOptionMaintenanceLookUp(String keyPharseMenuOptionMaintenanceLookUp) {
	this.keyPharseMenuOptionMaintenanceLookUp = keyPharseMenuOptionMaintenanceLookUp;
}
public String getLblMenuOptionSearchLookUp() {
	return lblMenuOptionSearchLookUp;
}
public void setLblMenuOptionSearchLookUp(String lblMenuOptionSearchLookUp) {
	this.lblMenuOptionSearchLookUp = lblMenuOptionSearchLookUp;
}
public String getLblTenant() {
	return lblTenant;
}
public void setLblTenant(String lblTenant) {
	this.lblTenant = lblTenant;
}
public String getLblMenuType() {
	return lblMenuType;
}
public void setLblMenuType(String lblMenuType) {
	this.lblMenuType = lblMenuType;
}
public String getLblMenuOption() {
	return lblMenuOption;
}
public void setLblMenuOption(String lblMenuOption) {
	this.lblMenuOption = lblMenuOption;
}
public String getLblDescription() {
	return lblDescription;
}
public void setLblDescription(String lblDescription) {
	this.lblDescription = lblDescription;
}
public String getLblApplication() {
	return lblApplication;
}
public void setLblApplication(String lblApplication) {
	this.lblApplication = lblApplication;
}
public String getLblApplicationSub() {
	return lblApplicationSub;
}
public void setLblApplicationSub(String lblApplicationSub) {
	this.lblApplicationSub = lblApplicationSub;
}
public String getLblAction() {
	return lblAction;
}
public void setLblAction(String lblAction) {
	this.lblAction = lblAction;
}
public String getLblCancel() {
	return lblCancel;
}
public void setLblCancel(String lblCancel) {
	this.lblCancel = lblCancel;
}
public String getLblDelete() {
	return lblDelete;
}
public void setLblDelete(String lblDelete) {
	this.lblDelete = lblDelete;
}
public String getLblEdit() {
	return lblEdit;
}
public void setLblEdit(String lblEdit) {
	this.lblEdit = lblEdit;
}
public String getLblAddnew() {
	return lblAddnew;
}
public void setLblAddnew(String lblAddnew) {
	this.lblAddnew = lblAddnew;
}
public String getLblNew() {
	return lblNew;
}
public void setLblNew(String lblNew) {
	this.lblNew = lblNew;
}
public String getLblpartOfApplication() {
	return lblpartOfApplication;
}
public void setLblpartOfApplication(String lblpartOfApplication) {
	this.lblpartOfApplication = lblpartOfApplication;
}
public String getLblPartOfMenuOption() {
	return lblPartOfMenuOption;
}
public void setLblPartOfMenuOption(String lblPartOfMenuOption) {
	this.lblPartOfMenuOption = lblPartOfMenuOption;
}
public String getLblPartOFtheMenuOptionDescription() {
	return lblPartOFtheMenuOptionDescription;
}
public void setLblPartOFtheMenuOptionDescription(String lblPartOFtheMenuOptionDescription) {
	this.lblPartOFtheMenuOptionDescription = lblPartOFtheMenuOptionDescription;
}
public String getLblTenantID() {
	return lblTenantID;
}
public void setLblTenantID(String lblTenantID) {
	this.lblTenantID = lblTenantID;
}
public String getLblLastActivityDate() {
	return lblLastActivityDate;
}
public void setLblLastActivityDate(String lblLastActivityDate) {
	this.lblLastActivityDate = lblLastActivityDate;
}
public String getLblLastActivityBy() {
	return lblLastActivityBy;
}
public void setLblLastActivityBy(String lblLastActivityBy) {
	this.lblLastActivityBy = lblLastActivityBy;
}
public String getLblHelpLine() {
	return lblHelpLine;
}
public void setLblHelpLine(String lblHelpLine) {
	this.lblHelpLine = lblHelpLine;
}
public String getLblExecutionPath() {
	return lblExecutionPath;
}
public void setLblExecutionPath(String lblExecutionPath) {
	this.lblExecutionPath = lblExecutionPath;
}
public String getLblMenuOptionMantenance() {
	return lblMenuOptionMantenance;
}
public void setLblMenuOptionMantenance(String lblMenuOptionMantenance) {
	this.lblMenuOptionMantenance = lblMenuOptionMantenance;
}
public String getLblMenuOptionMaintenanceLookUp() {
	return lblMenuOptionMaintenanceLookUp;
}
public void setLblMenuOptionMaintenanceLookUp(String lblMenuOptionMaintenanceLookUp) {
	this.lblMenuOptionMaintenanceLookUp = lblMenuOptionMaintenanceLookUp;
}





	
	

}
