
/**
Description this class contains all getter setter for screen Label
Created By Sarvendra Tyagi 
Created Date APR 28 2015
 */
package com.jasci.biz.AdminModule.be;

public class SMARTWORKFLOWLABELBE {
	private String Fulfillment_Center;
	private String Work_Flow_ID;
	private String Work_Flow_Type;
	private String Any_Part_of_the_Work_Flow_Description;
	private String New;
	private String Display_All;
	private String Work_Flow_Lookup;
	private String Flow_ID;
	private String Description;
	private String Notes;
	private String Actions;
	private String Edit;
	private String Copy;
	private String Note;
	private String Delete;
	private String Cancel;
	private String Add_New;
	private String Work_Flow_Search_Lookup;
	private String Work_Flow_Maintenance;
	private String MANDATORY_FIELD_LEFT_BLANK;
	
	private String Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button;
	private String Your_record_has_been_saved_successfully;
	private String Your_record_has_been_updated_successfully;
	
	
	private String Add;
	
	private String Please_enter_Fulfillment_center;
	private String Please_enter_Work_Flow_ID;
	private String Please_select_Work_Flow_Type;
	private String Please_enter_Any_Part_of_the_Work_Flow_Description;
	private String Invalid_Work_Flow_Type;
	private String Invalid_Any_Part_of_the_Work_Flow_Description; 
	private String Invalid_Work_Flow_ID;
	private String Invalid_Fulfillment_center;
	private String Select;
	private String Work_Type;
	
	
	
	public String getWork_Type() {
		return Work_Type;
	}
	public void setWork_Type(String work_Type) {
		Work_Type = work_Type;
	}
	public String getInvalid_Work_Flow_Type() {
		return Invalid_Work_Flow_Type;
	}
	public String getSelect() {
		return Select;
	}
	public void setSelect(String select) {
		Select = select;
	}
	public void setInvalid_Work_Flow_Type(String invalid_Work_Flow_Type) {
		Invalid_Work_Flow_Type = invalid_Work_Flow_Type;
	}
	public String getInvalid_Any_Part_of_the_Work_Flow_Description() {
		return Invalid_Any_Part_of_the_Work_Flow_Description;
	}
	public void setInvalid_Any_Part_of_the_Work_Flow_Description(String invalid_Any_Part_of_the_Work_Flow_Description) {
		Invalid_Any_Part_of_the_Work_Flow_Description = invalid_Any_Part_of_the_Work_Flow_Description;
	}
	public String getInvalid_Work_Flow_ID() {
		return Invalid_Work_Flow_ID;
	}
	public void setInvalid_Work_Flow_ID(String invalid_Work_Flow_ID) {
		Invalid_Work_Flow_ID = invalid_Work_Flow_ID;
	}
	public String getInvalid_Fulfillment_center() {
		return Invalid_Fulfillment_center;
	}
	public void setInvalid_Fulfillment_center(String invalid_Fulfillment_center) {
		Invalid_Fulfillment_center = invalid_Fulfillment_center;
	}
	public String getAdd() {
		return Add;
	}
	public String getPlease_enter_Fulfillment_center() {
		return Please_enter_Fulfillment_center;
	}
	public void setPlease_enter_Fulfillment_center(String please_enter_Fulfillment_center) {
		Please_enter_Fulfillment_center = please_enter_Fulfillment_center;
	}
	public String getPlease_enter_Work_Flow_ID() {
		return Please_enter_Work_Flow_ID;
	}
	public void setPlease_enter_Work_Flow_ID(String please_enter_Work_Flow_ID) {
		Please_enter_Work_Flow_ID = please_enter_Work_Flow_ID;
	}
	public String getPlease_select_Work_Flow_Type() {
		return Please_select_Work_Flow_Type;
	}
	public void setPlease_select_Work_Flow_Type(String please_select_Work_Flow_Type) {
		Please_select_Work_Flow_Type = please_select_Work_Flow_Type;
	}
	public String getPlease_enter_Any_Part_of_the_Work_Flow_Description() {
		return Please_enter_Any_Part_of_the_Work_Flow_Description;
	}
	public void setPlease_enter_Any_Part_of_the_Work_Flow_Description(
			String please_enter_Any_Part_of_the_Work_Flow_Description) {
		Please_enter_Any_Part_of_the_Work_Flow_Description = please_enter_Any_Part_of_the_Work_Flow_Description;
	}
	public void setAdd(String add) {
		Add = add;
	}
	public String getAre_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button() {
		return Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button;
	}
	public void setAre_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button(
			String are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button) {
		Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button = are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button;
	}
	public String getYour_record_has_been_saved_successfully() {
		return Your_record_has_been_saved_successfully;
	}
	public void setYour_record_has_been_saved_successfully(String your_record_has_been_saved_successfully) {
		Your_record_has_been_saved_successfully = your_record_has_been_saved_successfully;
	}
	public String getYour_record_has_been_updated_successfully() {
		return Your_record_has_been_updated_successfully;
	}
	public void setYour_record_has_been_updated_successfully(String your_record_has_been_updated_successfully) {
		Your_record_has_been_updated_successfully = your_record_has_been_updated_successfully;
	}
	public String getMANDATORY_FIELD_LEFT_BLANK() {
		return MANDATORY_FIELD_LEFT_BLANK;
	}
	public void setMANDATORY_FIELD_LEFT_BLANK(String mANDATORY_FIELD_LEFT_BLANK) {
		MANDATORY_FIELD_LEFT_BLANK = mANDATORY_FIELD_LEFT_BLANK;
	}
	public String getFulfillment_Center() {
		return Fulfillment_Center;
	}
	public void setFulfillment_Center(String fulfillment_Center) {
		Fulfillment_Center = fulfillment_Center;
	}
	public String getWork_Flow_ID() {
		return Work_Flow_ID;
	}
	public void setWork_Flow_ID(String work_Flow_ID) {
		Work_Flow_ID = work_Flow_ID;
	}
	public String getWork_Flow_Type() {
		return Work_Flow_Type;
	}
	public void setWork_Flow_Type(String work_Flow_Type) {
		Work_Flow_Type = work_Flow_Type;
	}
	public String getAny_Part_of_the_Work_Flow_Description() {
		return Any_Part_of_the_Work_Flow_Description;
	}
	public void setAny_Part_of_the_Work_Flow_Description(String any_Part_of_the_Work_Flow_Description) {
		Any_Part_of_the_Work_Flow_Description = any_Part_of_the_Work_Flow_Description;
	}
	public String getNew() {
		return New;
	}
	public void setNew(String new1) {
		New = new1;
	}
	public String getDisplay_All() {
		return Display_All;
	}
	public void setDisplay_All(String display_All) {
		Display_All = display_All;
	}
	public String getWork_Flow_Lookup() {
		return Work_Flow_Lookup;
	}
	public void setWork_Flow_Lookup(String work_Flow_Lookup) {
		Work_Flow_Lookup = work_Flow_Lookup;
	}
	public String getFlow_ID() {
		return Flow_ID;
	}
	public void setFlow_ID(String flow_ID) {
		Flow_ID = flow_ID;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getNotes() {
		return Notes;
	}
	public void setNotes(String notes) {
		Notes = notes;
	}
	public String getActions() {
		return Actions;
	}
	public void setActions(String actions) {
		Actions = actions;
	}
	public String getEdit() {
		return Edit;
	}
	public void setEdit(String edit) {
		Edit = edit;
	}
	public String getCopy() {
		return Copy;
	}
	public void setCopy(String copy) {
		Copy = copy;
	}
	public String getNote() {
		return Note;
	}
	public void setNote(String note) {
		Note = note;
	}
	public String getDelete() {
		return Delete;
	}
	public void setDelete(String delete) {
		Delete = delete;
	}
	public String getCancel() {
		return Cancel;
	}
	public void setCancel(String cancel) {
		Cancel = cancel;
	}
	public String getAdd_New() {
		return Add_New;
	}
	public void setAdd_New(String add_New) {
		Add_New = add_New;
	}
	public String getWork_Flow_Search_Lookup() {
		return Work_Flow_Search_Lookup;
	}
	public void setWork_Flow_Search_Lookup(String work_Flow_Search_Lookup) {
		Work_Flow_Search_Lookup = work_Flow_Search_Lookup;
	}
	public String getWork_Flow_Maintenance() {
		return Work_Flow_Maintenance;
	}
	public void setWork_Flow_Maintenance(String work_Flow_Maintenance) {
		Work_Flow_Maintenance = work_Flow_Maintenance;
	}
	public String getSave_Update() {
		return Save_Update;
	}
	public void setSave_Update(String save_Update) {
		Save_Update = save_Update;
	}
	public String getReset() {
		return Reset;
	}
	public void setReset(String reset) {
		Reset = reset;
	}
	private String Save_Update;
	private String Reset;

}
