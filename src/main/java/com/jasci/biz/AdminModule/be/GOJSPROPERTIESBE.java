/**
 
Date Developed  July 10 2014
Description pojo class of GOJS config proferties file data in which have getter and setter methods.
Created By Shailendra Rajput
 */
package com.jasci.biz.AdminModule.be;


public class GOJSPROPERTIESBE {
	
	
	private int RectangleWidth;
	private int RectangleHeight;
	private int RectangleTextWidth;
	private int RectangleTextHeight;
	private int DiamondWidth;
	private int DiamondHeight;
	private int DiamondTextWidth;
	private int DiamondTextHeight;
	private int DisplayWidth;
	private int DisplayHeight;
	private int DisplayTextWidth;
	private int DisplayTextHeight;
	private int CircleWidth;
	private int CircleHeight;
	private int CircleTextWidth;
	private int CircleTextHeight;
	private int VerticalDistance;
	private int DiamondVerticalDistance;
	private int CircleVerticalDistance;
	private int SquareVerticalDistance;
	private int HorizontalDistance;
	private int MaxToolTipWidth ;
	private int MaxToolTipHeight ;
	private int MinToolTipWidth;
	private int MinToolTipHeight ;
	private int MinTextWidth;
	private int MinTextHeight ;
	private int MarginTop;
	private int MarginLeft ;

	private int SquareWidth ;
	private int SquareHeight ;
	private int SquareTextWidth ;
	private int SquareTextHeight ;
	private boolean IsSequenceViewGrid ;
	
	private String TextFontSizeForActionPanel ;
	private String TextFontSizeForExecutionPanel;
	private String TextFontSizeForFlowChartPanel;
	
	
	
	public String getTextFontSizeForActionPanel() {
		return TextFontSizeForActionPanel;
	}
	public void setTextFontSizeForActionPanel(String textFontSizeForActionPanel) {
		TextFontSizeForActionPanel = textFontSizeForActionPanel;
	}
	public String getTextFontSizeForExecutionPanel() {
		return TextFontSizeForExecutionPanel;
	}
	public void setTextFontSizeForExecutionPanel(String textFontSizeForExecutionPanel) {
		TextFontSizeForExecutionPanel = textFontSizeForExecutionPanel;
	}
	public String getTextFontSizeForFlowChartPanel() {
		return TextFontSizeForFlowChartPanel;
	}
	public void setTextFontSizeForFlowChartPanel(String textFontSizeForFlowChartPanel) {
		TextFontSizeForFlowChartPanel = textFontSizeForFlowChartPanel;
	}
	public boolean isIsSequenceViewGrid() {
		return IsSequenceViewGrid;
	}
	public void setIsSequenceViewGrid(boolean isSequenceViewGrid) {
		IsSequenceViewGrid = isSequenceViewGrid;
	}
	public int getSquareWidth() {
		return SquareWidth;
	}
	public void setSquareWidth(int squareWidth) {
		SquareWidth = squareWidth;
	}
	public int getSquareHeight() {
		return SquareHeight;
	}
	public void setSquareHeight(int squareHeight) {
		SquareHeight = squareHeight;
	}
	public int getSquareTextWidth() {
		return SquareTextWidth;
	}
	public void setSquareTextWidth(int squareTextWidth) {
		SquareTextWidth = squareTextWidth;
	}
	public int getSquareTextHeight() {
		return SquareTextHeight;
	}
	public void setSquareTextHeight(int squareTextHeight) {
		SquareTextHeight = squareTextHeight;
	}
	public int getRectangleWidth() {
		return RectangleWidth;
	}
	public void setRectangleWidth(int rectangleWidth) {
		RectangleWidth = rectangleWidth;
	}
	public int getRectangleHeight() {
		return RectangleHeight;
	}
	public void setRectangleHeight(int rectangleHeight) {
		RectangleHeight = rectangleHeight;
	}
	public int getRectangleTextWidth() {
		return RectangleTextWidth;
	}
	public void setRectangleTextWidth(int rectangleTextWidth) {
		RectangleTextWidth = rectangleTextWidth;
	}
	public int getRectangleTextHeight() {
		return RectangleTextHeight;
	}
	public void setRectangleTextHeight(int rectangleTextHeight) {
		RectangleTextHeight = rectangleTextHeight;
	}
	public int getDiamondWidth() {
		return DiamondWidth;
	}
	public void setDiamondWidth(int diamondWidth) {
		DiamondWidth = diamondWidth;
	}
	public int getDiamondHeight() {
		return DiamondHeight;
	}
	public void setDiamondHeight(int diamondHeight) {
		DiamondHeight = diamondHeight;
	}
	public int getDiamondTextWidth() {
		return DiamondTextWidth;
	}
	public void setDiamondTextWidth(int diamondTextWidth) {
		DiamondTextWidth = diamondTextWidth;
	}
	public int getDiamondTextHeight() {
		return DiamondTextHeight;
	}
	public void setDiamondTextHeight(int diamondTextHeight) {
		DiamondTextHeight = diamondTextHeight;
	}
	public int getDisplayWidth() {
		return DisplayWidth;
	}
	public void setDisplayWidth(int displayWidth) {
		DisplayWidth = displayWidth;
	}
	public int getDisplayHeight() {
		return DisplayHeight;
	}
	public void setDisplayHeight(int displayHeight) {
		DisplayHeight = displayHeight;
	}
	public int getDisplayTextWidth() {
		return DisplayTextWidth;
	}
	public void setDisplayTextWidth(int displayTextWidth) {
		DisplayTextWidth = displayTextWidth;
	}
	public int getDisplayTextHeight() {
		return DisplayTextHeight;
	}
	public void setDisplayTextHeight(int displayTextHeight) {
		DisplayTextHeight = displayTextHeight;
	}
	public int getCircleWidth() {
		return CircleWidth;
	}
	public void setCircleWidth(int circleWidth) {
		CircleWidth = circleWidth;
	}
	public int getCircleHeight() {
		return CircleHeight;
	}
	public void setCircleHeight(int circleHeight) {
		CircleHeight = circleHeight;
	}
	public int getCircleTextWidth() {
		return CircleTextWidth;
	}
	public void setCircleTextWidth(int circleTextWidth) {
		CircleTextWidth = circleTextWidth;
	}
	public int getCircleTextHeight() {
		return CircleTextHeight;
	}
	public void setCircleTextHeight(int circleTextHeight) {
		CircleTextHeight = circleTextHeight;
	}
	public int getVerticalDistance() {
		return VerticalDistance;
	}
	public void setVerticalDistance(int verticalDistance) {
		VerticalDistance = verticalDistance;
	}
	public int getHorizontalDistance() {
		return HorizontalDistance;
	}
	public void setHorizontalDistance(int horizontalDistance) {
		HorizontalDistance = horizontalDistance;
	}
	public int getMaxToolTipWidth() {
		return MaxToolTipWidth;
	}
	public void setMaxToolTipWidth(int maxToolTipWidth) {
		MaxToolTipWidth = maxToolTipWidth;
	}
	public int getMaxToolTipHeight() {
		return MaxToolTipHeight;
	}
	public void setMaxToolTipHeight(int maxToolTipHeight) {
		MaxToolTipHeight = maxToolTipHeight;
	}
	public int getMinToolTipWidth() {
		return MinToolTipWidth;
	}
	public void setMinToolTipWidth(int minToolTipWidth) {
		MinToolTipWidth = minToolTipWidth;
	}
	public int getMinToolTipHeight() {
		return MinToolTipHeight;
	}
	public void setMinToolTipHeight(int minToolTipHeight) {
		MinToolTipHeight = minToolTipHeight;
	}
	public int getMinTextWidth() {
		return MinTextWidth;
	}
	public void setMinTextWidth(int minTextWidth) {
		MinTextWidth = minTextWidth;
	}
	public int getMinTextHeight() {
		return MinTextHeight;
	}
	public void setMinTextHeight(int minTextHeight) {
		MinTextHeight = minTextHeight;
	}
	public int getMarginTop() {
		return MarginTop;
	}
	public void setMarginTop(int marginTop) {
		MarginTop = marginTop;
	}
	public int getMarginLeft() {
		return MarginLeft;
	}
	public void setMarginLeft(int marginLeft) {
		MarginLeft = marginLeft;
	}
	public int getDiamondVerticalDistance() {
		return DiamondVerticalDistance;
	}
	public void setDiamondVerticalDistance(int diamondVerticalDistance) {
		DiamondVerticalDistance = diamondVerticalDistance;
	}
	public int getCircleVerticalDistance() {
		return CircleVerticalDistance;
	}
	public void setCircleVerticalDistance(int circleVerticalDistance) {
		CircleVerticalDistance = circleVerticalDistance;
	}
	public int getSquareVerticalDistance() {
		return SquareVerticalDistance;
	}
	public void setSquareVerticalDistance(int squareVerticalDistance) {
		SquareVerticalDistance = squareVerticalDistance;
	}
	
}
