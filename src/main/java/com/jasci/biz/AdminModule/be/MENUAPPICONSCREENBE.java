/**
 
Description It is used to make setter and getter for changing text of screen in another language
Created By :Rahul Kumar
Created Date Dec 12 2014
 */
package com.jasci.biz.AdminModule.be;

public class MENUAPPICONSCREENBE {
	
	private String	MenuAppIconMaintenance_ButtonResetText;
	private String	MenuAppIconMaintenance_MenuAppIconLookUpLabel;
	private String	MenuAppIconMaintenance_MenuAppIconSearckLookUp;
	private String	MenuAppIconMaintenance_MenuAppIconMaintenance;
	private String	MenuAppIconMaintenance_EnterAppIconLabel;
	private String	MenuAppIconMaintenance_PartOfTheAppIconNameLabel;
	private String	MenuAppIconMaintenance_PartOfTheAppIconLabel;
	private String	MenuAppIconMaintenance_ButtonNewText;
	private String	MenuAppIconMaintenance_ButtonDisplayAllText;
	private String	MenuAppIconMaintenance_ButtonEditText;
	private String	MenuAppIconMaintenance_ButtonDeleteText;
	private String	MenuAppIconMaintenance_ButtonBrowseText;
	private String	MenuAppIconMaintenance_ButtonSaveUpdateText;
	private String	MenuAppIconMaintenance_ButtonCancelText;
	private String	MenuAppIconMaintenance_PartOfTheDescription;
	private String	MenuAppIconMaintenance_AppIconGrid;
	private String	MenuAppIconMaintenance_DescriptionGrid;
	private String	MenuAppIconMaintenance_LastActivityDate;
	private String	MenuAppIconMaintenance_LastActivityBy;
	private String	MenuAppIconMaintenance_DescriptionShortLabel;
	private String	MenuAppIconMaintenance_DescriptionLongLabel;
	private String	MenuAppIconMaintenance_PreferredApplicationLabel;
	private String	MenuAppIconMaintenance_AppIconAddressLabel;
	private String	MenuAppIconMaintenance_YY_MM_DD_Label;
	private String	MenuAppIconMaintenance_ERR_INVALID_APP_ICON;
	private String	MenuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON;
	private String	MenuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED;
	private String	MenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	private String	MenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON;
	private String	MenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME;
	private String	MenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON;
	private String	MenuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS;
	private String	MenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON;
	private String	MenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME;
	private String	MenuAppIconMaintenance_ON_SAVE_SUCCESSFULLY;
	private String	MenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY;
	private String MenuAppIconMaintenance_Select;
	private String MenuAppIconMaintenance_GeneralCodeId_Application;	
	private String MenuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED;
	private String MenuAppIconMaintenance_ERR_SUPPRTED_FORMAT;
	private String MenuAppIconMaintenance_ButtonAddNew;
	private String MenuAppIconMaintenance_Action;
	private String MenuAppIconMaintenance_Text_Loading;
	
	
	
	
	
	
	public String getMenuAppIconMaintenance_ButtonResetText() {
		return MenuAppIconMaintenance_ButtonResetText;
	}
	public void setMenuAppIconMaintenance_ButtonResetText(String menuAppIconMaintenance_ButtonResetText) {
		MenuAppIconMaintenance_ButtonResetText = menuAppIconMaintenance_ButtonResetText;
	}
	public String getMenuAppIconMaintenance_ERR_SUPPRTED_FORMAT() {
		return MenuAppIconMaintenance_ERR_SUPPRTED_FORMAT;
	}
	public void setMenuAppIconMaintenance_ERR_SUPPRTED_FORMAT(String menuAppIconMaintenance_ERR_SUPPRTED_FORMAT) {
		MenuAppIconMaintenance_ERR_SUPPRTED_FORMAT = menuAppIconMaintenance_ERR_SUPPRTED_FORMAT;
	}
	public String getMenuAppIconMaintenance_Text_Loading() {
		return MenuAppIconMaintenance_Text_Loading;
	}
	public void setMenuAppIconMaintenance_Text_Loading(String menuAppIconMaintenance_Text_Loading) {
		MenuAppIconMaintenance_Text_Loading = menuAppIconMaintenance_Text_Loading;
	}
	public String getMenuAppIconMaintenance_Action() {
		return MenuAppIconMaintenance_Action;
	}
	public void setMenuAppIconMaintenance_Action(String MenuAppIconMaintenance_Action) {
		this.MenuAppIconMaintenance_Action = MenuAppIconMaintenance_Action;
	}
	/**
	 * @return the menuAppIconMaintenance_ButtonAddNew
	 */
	public String getMenuAppIconMaintenance_ButtonAddNew() {
		return MenuAppIconMaintenance_ButtonAddNew;
	}
	/**
	 * @param menuAppIconMaintenance_ButtonAddNew the menuAppIconMaintenance_ButtonAddNew to set
	 */
	public void setMenuAppIconMaintenance_ButtonAddNew(String menuAppIconMaintenance_ButtonAddNew) {
		MenuAppIconMaintenance_ButtonAddNew = menuAppIconMaintenance_ButtonAddNew;
	}
	/**
	 * @return the menuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED
	 */
	public String getMenuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED() {
		return MenuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED;
	}
	/**
	 * @param menuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED the menuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED to set
	 */
	public void setMenuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED(
			String menuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED) {
		MenuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED = menuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED;
	}
	/**
	 * @return the menuAppIconMaintenance_GeneralCodeId_Application
	 */
	public String getMenuAppIconMaintenance_GeneralCodeId_Application() {
		return MenuAppIconMaintenance_GeneralCodeId_Application;
	}
	/**
	 * @param menuAppIconMaintenance_GeneralCodeId_Application the menuAppIconMaintenance_GeneralCodeId_Application to set
	 */
	public void setMenuAppIconMaintenance_GeneralCodeId_Application(String menuAppIconMaintenance_GeneralCodeId_Application) {
		MenuAppIconMaintenance_GeneralCodeId_Application = menuAppIconMaintenance_GeneralCodeId_Application;
	}
	public String getMenuAppIconMaintenance_Select() {
		return MenuAppIconMaintenance_Select;
	}
	public void setMenuAppIconMaintenance_Select(String menuAppIconMaintenance_Select) {
		MenuAppIconMaintenance_Select = menuAppIconMaintenance_Select;
	}
	public String getMenuAppIconMaintenance_MenuAppIconLookUpLabel() {
		return MenuAppIconMaintenance_MenuAppIconLookUpLabel;
	}
	public void setMenuAppIconMaintenance_MenuAppIconLookUpLabel(String menuAppIconMaintenance_MenuAppIconLookUpLabel) {
		MenuAppIconMaintenance_MenuAppIconLookUpLabel = menuAppIconMaintenance_MenuAppIconLookUpLabel;
	}
	public String getMenuAppIconMaintenance_MenuAppIconSearckLookUp() {
		return MenuAppIconMaintenance_MenuAppIconSearckLookUp;
	}
	public void setMenuAppIconMaintenance_MenuAppIconSearckLookUp(String menuAppIconMaintenance_MenuAppIconSearckLookUp) {
		MenuAppIconMaintenance_MenuAppIconSearckLookUp = menuAppIconMaintenance_MenuAppIconSearckLookUp;
	}
	public String getMenuAppIconMaintenance_MenuAppIconMaintenance() {
		return MenuAppIconMaintenance_MenuAppIconMaintenance;
	}
	public void setMenuAppIconMaintenance_MenuAppIconMaintenance(String menuAppIconMaintenance_MenuAppIconMaintenance) {
		MenuAppIconMaintenance_MenuAppIconMaintenance = menuAppIconMaintenance_MenuAppIconMaintenance;
	}
	public String getMenuAppIconMaintenance_EnterAppIconLabel() {
		return MenuAppIconMaintenance_EnterAppIconLabel;
	}
	public void setMenuAppIconMaintenance_EnterAppIconLabel(String menuAppIconMaintenance_EnterAppIconLabel) {
		MenuAppIconMaintenance_EnterAppIconLabel = menuAppIconMaintenance_EnterAppIconLabel;
	}
	public String getMenuAppIconMaintenance_PartOfTheAppIconNameLabel() {
		return MenuAppIconMaintenance_PartOfTheAppIconNameLabel;
	}
	public void setMenuAppIconMaintenance_PartOfTheAppIconNameLabel(String menuAppIconMaintenance_PartOfTheAppIconNameLabel) {
		MenuAppIconMaintenance_PartOfTheAppIconNameLabel = menuAppIconMaintenance_PartOfTheAppIconNameLabel;
	}
	public String getMenuAppIconMaintenance_PartOfTheAppIconLabel() {
		return MenuAppIconMaintenance_PartOfTheAppIconLabel;
	}
	public void setMenuAppIconMaintenance_PartOfTheAppIconLabel(String menuAppIconMaintenance_PartOfTheAppIconLabel) {
		MenuAppIconMaintenance_PartOfTheAppIconLabel = menuAppIconMaintenance_PartOfTheAppIconLabel;
	}
	public String getMenuAppIconMaintenance_ButtonNewText() {
		return MenuAppIconMaintenance_ButtonNewText;
	}
	public void setMenuAppIconMaintenance_ButtonNewText(String menuAppIconMaintenance_ButtonNewText) {
		MenuAppIconMaintenance_ButtonNewText = menuAppIconMaintenance_ButtonNewText;
	}
	public String getMenuAppIconMaintenance_ButtonDisplayAllText() {
		return MenuAppIconMaintenance_ButtonDisplayAllText;
	}
	public void setMenuAppIconMaintenance_ButtonDisplayAllText(String menuAppIconMaintenance_ButtonDisplayAllText) {
		MenuAppIconMaintenance_ButtonDisplayAllText = menuAppIconMaintenance_ButtonDisplayAllText;
	}
	public String getMenuAppIconMaintenance_ButtonEditText() {
		return MenuAppIconMaintenance_ButtonEditText;
	}
	public void setMenuAppIconMaintenance_ButtonEditText(String menuAppIconMaintenance_ButtonEditText) {
		MenuAppIconMaintenance_ButtonEditText = menuAppIconMaintenance_ButtonEditText;
	}
	public String getMenuAppIconMaintenance_ButtonDeleteText() {
		return MenuAppIconMaintenance_ButtonDeleteText;
	}
	public void setMenuAppIconMaintenance_ButtonDeleteText(String menuAppIconMaintenance_ButtonDeleteText) {
		MenuAppIconMaintenance_ButtonDeleteText = menuAppIconMaintenance_ButtonDeleteText;
	}
	public String getMenuAppIconMaintenance_ButtonBrowseText() {
		return MenuAppIconMaintenance_ButtonBrowseText;
	}
	public void setMenuAppIconMaintenance_ButtonBrowseText(String menuAppIconMaintenance_ButtonBrowseText) {
		MenuAppIconMaintenance_ButtonBrowseText = menuAppIconMaintenance_ButtonBrowseText;
	}
	public String getMenuAppIconMaintenance_ButtonSaveUpdateText() {
		return MenuAppIconMaintenance_ButtonSaveUpdateText;
	}
	public void setMenuAppIconMaintenance_ButtonSaveUpdateText(String menuAppIconMaintenance_ButtonSaveUpdateText) {
		MenuAppIconMaintenance_ButtonSaveUpdateText = menuAppIconMaintenance_ButtonSaveUpdateText;
	}
	public String getMenuAppIconMaintenance_ButtonCancelText() {
		return MenuAppIconMaintenance_ButtonCancelText;
	}
	public void setMenuAppIconMaintenance_ButtonCancelText(String menuAppIconMaintenance_ButtonCancelText) {
		MenuAppIconMaintenance_ButtonCancelText = menuAppIconMaintenance_ButtonCancelText;
	}
	public String getMenuAppIconMaintenance_PartOfTheDescription() {
		return MenuAppIconMaintenance_PartOfTheDescription;
	}
	public void setMenuAppIconMaintenance_PartOfTheDescription(String menuAppIconMaintenance_PartOfTheDescription) {
		MenuAppIconMaintenance_PartOfTheDescription = menuAppIconMaintenance_PartOfTheDescription;
	}
	public String getMenuAppIconMaintenance_AppIconGrid() {
		return MenuAppIconMaintenance_AppIconGrid;
	}
	public void setMenuAppIconMaintenance_AppIconGrid(String menuAppIconMaintenance_AppIconGrid) {
		MenuAppIconMaintenance_AppIconGrid = menuAppIconMaintenance_AppIconGrid;
	}
	public String getMenuAppIconMaintenance_DescriptionGrid() {
		return MenuAppIconMaintenance_DescriptionGrid;
	}
	public void setMenuAppIconMaintenance_DescriptionGrid(String menuAppIconMaintenance_DescriptionGrid) {
		MenuAppIconMaintenance_DescriptionGrid = menuAppIconMaintenance_DescriptionGrid;
	}
	public String getMenuAppIconMaintenance_LastActivityDate() {
		return MenuAppIconMaintenance_LastActivityDate;
	}
	public void setMenuAppIconMaintenance_LastActivityDate(String menuAppIconMaintenance_LastActivityDate) {
		MenuAppIconMaintenance_LastActivityDate = menuAppIconMaintenance_LastActivityDate;
	}
	public String getMenuAppIconMaintenance_LastActivityBy() {
		return MenuAppIconMaintenance_LastActivityBy;
	}
	public void setMenuAppIconMaintenance_LastActivityBy(String menuAppIconMaintenance_LastActivityBy) {
		MenuAppIconMaintenance_LastActivityBy = menuAppIconMaintenance_LastActivityBy;
	}
	public String getMenuAppIconMaintenance_DescriptionShortLabel() {
		return MenuAppIconMaintenance_DescriptionShortLabel;
	}
	public void setMenuAppIconMaintenance_DescriptionShortLabel(String menuAppIconMaintenance_DescriptionShortLabel) {
		MenuAppIconMaintenance_DescriptionShortLabel = menuAppIconMaintenance_DescriptionShortLabel;
	}
	public String getMenuAppIconMaintenance_DescriptionLongLabel() {
		return MenuAppIconMaintenance_DescriptionLongLabel;
	}
	public void setMenuAppIconMaintenance_DescriptionLongLabel(String menuAppIconMaintenance_DescriptionLongLabel) {
		MenuAppIconMaintenance_DescriptionLongLabel = menuAppIconMaintenance_DescriptionLongLabel;
	}
	public String getMenuAppIconMaintenance_PreferredApplicationLabel() {
		return MenuAppIconMaintenance_PreferredApplicationLabel;
	}
	public void setMenuAppIconMaintenance_PreferredApplicationLabel(String menuAppIconMaintenance_PreferredApplicationLabel) {
		MenuAppIconMaintenance_PreferredApplicationLabel = menuAppIconMaintenance_PreferredApplicationLabel;
	}
	public String getMenuAppIconMaintenance_AppIconAddressLabel() {
		return MenuAppIconMaintenance_AppIconAddressLabel;
	}
	public void setMenuAppIconMaintenance_AppIconAddressLabel(String menuAppIconMaintenance_AppIconAddressLabel) {
		MenuAppIconMaintenance_AppIconAddressLabel = menuAppIconMaintenance_AppIconAddressLabel;
	}
	public String getMenuAppIconMaintenance_YY_MM_DD_Label() {
		return MenuAppIconMaintenance_YY_MM_DD_Label;
	}
	public void setMenuAppIconMaintenance_YY_MM_DD_Label(String menuAppIconMaintenance_YY_MM_DD_Label) {
		MenuAppIconMaintenance_YY_MM_DD_Label = menuAppIconMaintenance_YY_MM_DD_Label;
	}
	public String getMenuAppIconMaintenance_ERR_INVALID_APP_ICON() {
		return MenuAppIconMaintenance_ERR_INVALID_APP_ICON;
	}
	public void setMenuAppIconMaintenance_ERR_INVALID_APP_ICON(String menuAppIconMaintenance_ERR_INVALID_APP_ICON) {
		MenuAppIconMaintenance_ERR_INVALID_APP_ICON = menuAppIconMaintenance_ERR_INVALID_APP_ICON;
	}
	public String getMenuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON() {
		return MenuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON;
	}
	public void setMenuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON(
			String menuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON) {
		MenuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON = menuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON;
	}
	public String getMenuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED() {
		return MenuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED;
	}
	public void setMenuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED(String menuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED) {
		MenuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED = menuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED;
	}
	public String getMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK() {
		return MenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public void setMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK(
			String menuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK) {
		MenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK = menuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public String getMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON() {
		return MenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON;
	}
	public void setMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON(
			String menuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON) {
		MenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON = menuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON;
	}
	public String getMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME() {
		return MenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME;
	}
	public void setMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME(
			String menuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME) {
		MenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME = menuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME;
	}
	public String getMenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON() {
		return MenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON;
	}
	public void setMenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON(String menuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON) {
		MenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON = menuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON;
	}
	public String getMenuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS() {
		return MenuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS;
	}
	public void setMenuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS(
			String menuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS) {
		MenuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS = menuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS;
	}
	public String getMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON() {
		return MenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON;
	}
	public void setMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON(
			String menuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON) {
		MenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON = menuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON;
	}
	public String getMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME() {
		return MenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME;
	}
	public void setMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME(
			String menuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME) {
		MenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME = menuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME;
	}
	public String getMenuAppIconMaintenance_ON_SAVE_SUCCESSFULLY() {
		return MenuAppIconMaintenance_ON_SAVE_SUCCESSFULLY;
	}
	public void setMenuAppIconMaintenance_ON_SAVE_SUCCESSFULLY(String menuAppIconMaintenance_ON_SAVE_SUCCESSFULLY) {
		MenuAppIconMaintenance_ON_SAVE_SUCCESSFULLY = menuAppIconMaintenance_ON_SAVE_SUCCESSFULLY;
	}
	public String getMenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY() {
		return MenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY;
	}
	public void setMenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY(String menuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY) {
		MenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY = menuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY;
	}

	
	
	
	

}
