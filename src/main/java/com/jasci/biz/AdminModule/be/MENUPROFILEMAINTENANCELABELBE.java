/**

Description It is used to make setter and getter  for changing text of menu profile maintenace screen in another language
Created By Sarvendra Tyagi
Created Date Dec 26 2014
 */


package com.jasci.biz.AdminModule.be;

public class MENUPROFILEMAINTENANCELABELBE {
	
	
	//for menu profile maintenance lookup variable
	private String keyPharseMenuType="menutype";
	private String keyPharseMenuProfile="MenuProfile";
	private String keyPharseMenuProfilesMaintenanceLookup="MenuProfilesMaintenanceLookup";
	private String keyPharsePartoftheMenuProfileDescription="PartoftheMenuProfileDescription";
	private String keyPharseERR_INVALID_MENU_PROFILE="ERR_INVALID_MENU_PROFILE";
	private String keyPharseERR_MENU_PROFILE_LEFT_BLANK="ERR_MENU_PROFILE_LEFT_BLANK";
	private String keyPharseERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION="ERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION";
	private String keyPharseERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK="ERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK";
	private String keyPharseERR_MENU_TYPE_LEFT_BLANK="ERR_MENU_TYPE_LEFT_BLANK";
	
	private String KP_ERR_WHILE_DELETING_MENU_PROFILE="ERR_WHILE_DELETING_MENU_PROFILE";
	private String lbl_ERR_WHILE_DELETING_MENU_PROFILE;
	private String KP_ERR_WHILE_DELETING_MENU_OPTION="ERR_WHILE_DELETING_MENU_OPTION";
	private String lbl_ERR_WHILE_DELETING_MENU_OPTION;
	private String lblReset;

	public String getLblReset() {
		return lblReset;
	}
	public void setLblReset(String lblReset) {
		this.lblReset = lblReset;
	}
    public String getKP_ERR_WHILE_DELETING_MENU_PROFILE() {
		return KP_ERR_WHILE_DELETING_MENU_PROFILE;
	}
	public void setKP_ERR_WHILE_DELETING_MENU_PROFILE(String kP_ERR_WHILE_DELETING_MENU_PROFILE) {
		KP_ERR_WHILE_DELETING_MENU_PROFILE = kP_ERR_WHILE_DELETING_MENU_PROFILE;
	}
	public String getLbl_ERR_WHILE_DELETING_MENU_PROFILE() {
		return lbl_ERR_WHILE_DELETING_MENU_PROFILE;
	}
	public void setLbl_ERR_WHILE_DELETING_MENU_PROFILE(String lbl_ERR_WHILE_DELETING_MENU_PROFILE) {
		this.lbl_ERR_WHILE_DELETING_MENU_PROFILE = lbl_ERR_WHILE_DELETING_MENU_PROFILE;
	}
	public String getKP_ERR_WHILE_DELETING_MENU_OPTION() {
		return KP_ERR_WHILE_DELETING_MENU_OPTION;
	}
	public void setKP_ERR_WHILE_DELETING_MENU_OPTION(String kP_ERR_WHILE_DELETING_MENU_OPTION) {
		KP_ERR_WHILE_DELETING_MENU_OPTION = kP_ERR_WHILE_DELETING_MENU_OPTION;
	}
	public String getLbl_ERR_WHILE_DELETING_MENU_OPTION() {
		return lbl_ERR_WHILE_DELETING_MENU_OPTION;
	}
	public void setLbl_ERR_WHILE_DELETING_MENU_OPTION(String lbl_ERR_WHILE_DELETING_MENU_OPTION) {
		this.lbl_ERR_WHILE_DELETING_MENU_OPTION = lbl_ERR_WHILE_DELETING_MENU_OPTION;
	}
	private String lblMenuType;
    private String lblMenuProfile;
	private String lblMenuProfilesMaintenanceLookup;
	private String lblPartoftheMenuProfileDescription;
	private String lblERR_INVALID_MENU_PROFILE;
	private String lblERR_MENU_PROFILE_LEFT_BLANK;
	private String lblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION;
	private String lblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK;
	private String lblERR_MENU_TYPE_LEFT_BLANK;
	
	
	
	private String keyPharseGeneralCodeMenuTypes="MenuTypes";
	private String keyPharseGeneralCodeApplication="Applications";
	
	private String lblON_UPDATE_SUCCESSFULLY;
	private String lblON_SAVE_SUCCESSFULLY;
	private String keyPharseON_UPDATE_SUCCESSFULLY="ON_UPDATE_SUCCESSFULLY";
	private String keyPharseON_SAVE_SUCCESSFULLY="ON_SAVE_SUCCESSFULLY";
	private String lblNo_menu_option_has_been_assigned_to_the_profile;

public String getLblNo_menu_option_has_been_assigned_to_the_profile() {
		return lblNo_menu_option_has_been_assigned_to_the_profile;
	}
	public void setLblNo_menu_option_has_been_assigned_to_the_profile(
			String lblNo_menu_option_has_been_assigned_to_the_profile) {
		this.lblNo_menu_option_has_been_assigned_to_the_profile = lblNo_menu_option_has_been_assigned_to_the_profile;
	}
public String getLblON_UPDATE_SUCCESSFULLY() {
		return lblON_UPDATE_SUCCESSFULLY;
	}
	public void setLblON_UPDATE_SUCCESSFULLY(String lblON_UPDATE_SUCCESSFULLY) {
		this.lblON_UPDATE_SUCCESSFULLY = lblON_UPDATE_SUCCESSFULLY;
	}
	public String getLblON_SAVE_SUCCESSFULLY() {
		return lblON_SAVE_SUCCESSFULLY;
	}
	public void setLblON_SAVE_SUCCESSFULLY(String lblON_SAVE_SUCCESSFULLY) {
		this.lblON_SAVE_SUCCESSFULLY = lblON_SAVE_SUCCESSFULLY;
	}
	public String getKeyPharseON_UPDATE_SUCCESSFULLY() {
		return keyPharseON_UPDATE_SUCCESSFULLY;
	}
	public void setKeyPharseON_UPDATE_SUCCESSFULLY(String keyPharseON_UPDATE_SUCCESSFULLY) {
		this.keyPharseON_UPDATE_SUCCESSFULLY = keyPharseON_UPDATE_SUCCESSFULLY;
	}
	public String getKeyPharseON_SAVE_SUCCESSFULLY() {
		return keyPharseON_SAVE_SUCCESSFULLY;
	}
	public void setKeyPharseON_SAVE_SUCCESSFULLY(String keyPharseON_SAVE_SUCCESSFULLY) {
		this.keyPharseON_SAVE_SUCCESSFULLY = keyPharseON_SAVE_SUCCESSFULLY;
	}
private String lblGeneralCodeMenuTypes;
private String lblGeneralCodeApplication;

	public String getKeyPharseGeneralCodeMenuTypes() {
	return keyPharseGeneralCodeMenuTypes;
}
public void setKeyPharseGeneralCodeMenuTypes(String keyPharseGeneralCodeMenuTypes) {
	this.keyPharseGeneralCodeMenuTypes = keyPharseGeneralCodeMenuTypes;
}
public String getKeyPharseGeneralCodeApplication() {
	return keyPharseGeneralCodeApplication;
}
public void setKeyPharseGeneralCodeApplication(String keyPharseGeneralCodeApplication) {
	this.keyPharseGeneralCodeApplication = keyPharseGeneralCodeApplication;
}
public String getLblGeneralCodeMenuTypes() {
	return lblGeneralCodeMenuTypes;
}
public void setLblGeneralCodeMenuTypes(String lblGeneralCodeMenuTypes) {
	this.lblGeneralCodeMenuTypes = lblGeneralCodeMenuTypes;
}
public String getLblGeneralCodeApplication() {
	return lblGeneralCodeApplication;
}
public void setLblGeneralCodeApplication(String lblGeneralCodeApplication) {
	this.lblGeneralCodeApplication = lblGeneralCodeApplication;
}
	// Menu Profile maintenance search lookup screen
	private String keyPharseMenuProfilesMaintenanceSearchLookup="MenuProfilesMaintenanceSearchLookup";
	

	private String lblMenuProfilesMaintenanceSearchLookup;
	
	

	public String getLblMenuProfilesMaintenanceSearchLookup() {
		return lblMenuProfilesMaintenanceSearchLookup;
	}
	public void setLblMenuProfilesMaintenanceSearchLookup(String lblMenuProfilesMaintenanceSearchLookup) {
		this.lblMenuProfilesMaintenanceSearchLookup = lblMenuProfilesMaintenanceSearchLookup;
	}
	//Menu Profile Maintenance 
	private String keyPharseDescription="description";
	private String keyPharsedescriptionlong="descriptionlong";
	private String keyPharsedescriptionshrot="descriptionshrot";
	
	private String keyPharseAvailableMenuOptions="AvailableMenuOptions";
	
	
	private String keyPharseMenuProfilesOptions="MenuProfilesOptions";
	private String keyPharseSubApplication="SubApplication";
	private String keyPharseMenuOption="menuoption";
	private String keyPharseOR="OR";
	private String keyPharseSelectApplication="SelectApplication";
	private String keyPharseMenuProfilesMaintenance="MenuProfilesMaintenance";
	private String keyPharseERR_MENU_PROFILE_ALREADY_USED="ERR_MENU_PROFILE_ALREADY_USED";

	private String keyPharseSelect="Select";


	public String getKeyPharseSelect() {
		return keyPharseSelect;
	}
	public void setKeyPharseSelect(String keyPharseSelect) {
		this.keyPharseSelect = keyPharseSelect;
	}
	public String getLblSelect() {
		return lblSelect;
	}
	public void setLblSelect(String lblSelect) {
		this.lblSelect = lblSelect;
	}
	private String lblSelect;

	

	private String lblDescription;
	private String lbldescriptionlong;
	private String lbldescriptionshrot;
	
	private String lblAvailableMenuOptions;
	
	
	private String lblMenuProfilesOptions;
	private String lblSubApplication;
	private String lblMenuOption;
	private String lblOR;
	private String lblSelectApplication;
	private String lblMenuProfilesMaintenance;
	private String lblERR_MENU_PROFILE_ALREADY_USED;

	
	public String getKeyPharseERR_MENU_PROFILE_ALREADY_USED() {
		return keyPharseERR_MENU_PROFILE_ALREADY_USED;
	}
	public void setKeyPharseERR_MENU_PROFILE_ALREADY_USED(String keyPharseERR_MENU_PROFILE_ALREADY_USED) {
		this.keyPharseERR_MENU_PROFILE_ALREADY_USED = keyPharseERR_MENU_PROFILE_ALREADY_USED;
	}
	public String getKeyPharseERR_MANDATORY_FIELD_LEFT_BLANK() {
		return keyPharseERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public void setKeyPharseERR_MANDATORY_FIELD_LEFT_BLANK(String keyPharseERR_MANDATORY_FIELD_LEFT_BLANK) {
		this.keyPharseERR_MANDATORY_FIELD_LEFT_BLANK = keyPharseERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	//common vaiable
	private String keyPharseAction="Actions";
	private String keyPharseCancel="Cancel";
	private String keyPharseDelete="Delete";
	private String keyPharseEdit="Edit";
	private String keyPharseAddnew="Add New"; 
	private String keyPharseNew="new";
	private String keyPharseUpdate_Save="Save/Update";
	
	private String keyPharseLastActivityDate="LastActivityDate";
	private String keyPharseLastActivityBy="lastactivityby";
	private String keyPharseHelpLine="helpline";
	
	private String keyPharseERR_MANDATORY_FIELD_LEFT_BLANK="ERR_MANDATORY FIELD_LEFT BLANK";
	
	private String keyPharseMenu_App_Icon="Menu_App_Icon";
	private String lblMenu_App_Icon;
	public String getKeyPharseMenu_App_Icon() {
		return keyPharseMenu_App_Icon;
	}
	public void setKeyPharseMenu_App_Icon(String keyPharseMenu_App_Icon) {
		this.keyPharseMenu_App_Icon = keyPharseMenu_App_Icon;
	}
	public String getLblMenu_App_Icon() {
		return lblMenu_App_Icon;
	}
	public void setLblMenu_App_Icon(String lblMenu_App_Icon) {
		this.lblMenu_App_Icon = lblMenu_App_Icon;
	}
	private String keyPharsedisplayall="displayall";
	public String getKeyPharsedisplayall() {
		return keyPharsedisplayall;
	}
	public void setKeyPharsedisplayall(String keyPharsedisplayall) {
		this.keyPharsedisplayall = keyPharsedisplayall;
	}
	
	private String keyPharseApplication="application";
	private String lblApplication;
	
	public String getKeyPharseApplication() {
		return keyPharseApplication;
	}
	public void setKeyPharseApplication(String keyPharseApplication) {
		this.keyPharseApplication = keyPharseApplication;
	}
	public String getLblApplication() {
		return lblApplication;
	}
	public void setLblApplication(String lblApplication) {
		this.lblApplication = lblApplication;
	}
	private String lblAction;
	private String lblCancel;
	private String lblDelete;
	private String lblEdit;
	private String lblAddnew; 
	private String lblNew;
	private String lblUpdate_Save;
	
	private String lblLastActivityDate;
	private String lblLastActivityBy;
	private String lblHelpLine;
	
	private String lblERR_MANDATORY_FIELD_LEFT_BLANK;
	private String lbldisplayall;
	
	private String lblERR_INVALID_MENU_TYPE_LEFT_BLANK;
	private String keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK="ERR_INVALID_MENU_TYPE_LEFT_BLANK";
	
	public String getLblERR_INVALID_MENU_TYPE_LEFT_BLANK() {
		return lblERR_INVALID_MENU_TYPE_LEFT_BLANK;
	}
	public void setLblERR_INVALID_MENU_TYPE_LEFT_BLANK(String lblERR_INVALID_MENU_TYPE_LEFT_BLANK) {
		this.lblERR_INVALID_MENU_TYPE_LEFT_BLANK = lblERR_INVALID_MENU_TYPE_LEFT_BLANK;
	}
	public String getKeyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK() {
		return keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK;
	}
	public void setKeyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK(String keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK) {
		this.keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK = keyPharseERR_INVALID_MENU_TYPE_LEFT_BLANK;
	}
	public String getLbldisplayall() {
		return lbldisplayall;
	}
	public void setLbldisplayall(String lbldisplayall) {
		this.lbldisplayall = lbldisplayall;
	}
	
	public String getLblDescription() {
		return lblDescription;
	}
	public void setLblDescription(String lblDescription) {
		this.lblDescription = lblDescription;
	}
	public String getLbldescriptionlong() {
		return lbldescriptionlong;
	}
	public void setLbldescriptionlong(String lbldescriptionlong) {
		this.lbldescriptionlong = lbldescriptionlong;
	}
	public String getLbldescriptionshrot() {
		return lbldescriptionshrot;
	}
	public void setLbldescriptionshrot(String lbldescriptionshrot) {
		this.lbldescriptionshrot = lbldescriptionshrot;
	}
	public String getLblAvailableMenuOptions() {
		return lblAvailableMenuOptions;
	}
	public void setLblAvailableMenuOptions(String lblAvailableMenuOptions) {
		this.lblAvailableMenuOptions = lblAvailableMenuOptions;
	}
	public String getLblMenuProfilesOptions() {
		return lblMenuProfilesOptions;
	}
	public void setLblMenuProfilesOptions(String lblMenuProfilesOptions) {
		this.lblMenuProfilesOptions = lblMenuProfilesOptions;
	}
	public String getLblSubApplication() {
		return lblSubApplication;
	}
	public void setLblSubApplication(String lblSubApplication) {
		this.lblSubApplication = lblSubApplication;
	}
	public String getLblMenuOption() {
		return lblMenuOption;
	}
	public void setLblMenuOption(String lblMenuOption) {
		this.lblMenuOption = lblMenuOption;
	}
	public String getLblOR() {
		return lblOR;
	}
	public void setLblOR(String lblOR) {
		this.lblOR = lblOR;
	}
	public String getLblSelectApplication() {
		return lblSelectApplication;
	}
	public void setLblSelectApplication(String lblSelectApplication) {
		this.lblSelectApplication = lblSelectApplication;
	}
	public String getLblMenuProfilesMaintenance() {
		return lblMenuProfilesMaintenance;
	}
	public void setLblMenuProfilesMaintenance(String lblMenuProfilesMaintenance) {
		this.lblMenuProfilesMaintenance = lblMenuProfilesMaintenance;
	}
	public String getLblERR_MENU_PROFILE_ALREADY_USED() {
		return lblERR_MENU_PROFILE_ALREADY_USED;
	}
	public void setLblERR_MENU_PROFILE_ALREADY_USED(String lblERR_MENU_PROFILE_ALREADY_USED) {
		this.lblERR_MENU_PROFILE_ALREADY_USED = lblERR_MENU_PROFILE_ALREADY_USED;
	}
	public String getLblAction() {
		return lblAction;
	}
	public void setLblAction(String lblAction) {
		this.lblAction = lblAction;
	}
	public String getLblCancel() {
		return lblCancel;
	}
	public void setLblCancel(String lblCancel) {
		this.lblCancel = lblCancel;
	}
	public String getLblDelete() {
		return lblDelete;
	}
	public void setLblDelete(String lblDelete) {
		this.lblDelete = lblDelete;
	}
	public String getLblEdit() {
		return lblEdit;
	}
	public void setLblEdit(String lblEdit) {
		this.lblEdit = lblEdit;
	}
	public String getLblAddnew() {
		return lblAddnew;
	}
	public void setLblAddnew(String lblAddnew) {
		this.lblAddnew = lblAddnew;
	}
	public String getLblNew() {
		return lblNew;
	}
	public void setLblNew(String lblNew) {
		this.lblNew = lblNew;
	}
	public String getLblUpdate_Save() {
		return lblUpdate_Save;
	}
	public void setLblUpdate_Save(String lblUpdate_Save) {
		this.lblUpdate_Save = lblUpdate_Save;
	}
	public String getLblLastActivityDate() {
		return lblLastActivityDate;
	}
	public void setLblLastActivityDate(String lblLastActivityDate) {
		this.lblLastActivityDate = lblLastActivityDate;
	}
	public String getLblLastActivityBy() {
		return lblLastActivityBy;
	}
	public void setLblLastActivityBy(String lblLastActivityBy) {
		this.lblLastActivityBy = lblLastActivityBy;
	}
	public String getLblHelpLine() {
		return lblHelpLine;
	}
	public void setLblHelpLine(String lblHelpLine) {
		this.lblHelpLine = lblHelpLine;
	}
	public String getLblERR_MANDATORY_FIELD_LEFT_BLANK() {
		return lblERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public void setLblERR_MANDATORY_FIELD_LEFT_BLANK(String lblERR_MANDATORY_FIELD_LEFT_BLANK) {
		this.lblERR_MANDATORY_FIELD_LEFT_BLANK = lblERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public String getKeyPharseMenuType() {
		return keyPharseMenuType;
	}
	public void setKeyPharseMenuType(String keyPharseMenuType) {
		this.keyPharseMenuType = keyPharseMenuType;
	}
	public String getKeyPharseMenuProfile() {
		return keyPharseMenuProfile;
	}
	public void setKeyPharseMenuProfile(String keyPharseMenuProfile) {
		this.keyPharseMenuProfile = keyPharseMenuProfile;
	}
	public String getKeyPharseMenuProfilesMaintenanceLookup() {
		return keyPharseMenuProfilesMaintenanceLookup;
	}
	public void setKeyPharseMenuProfilesMaintenanceLookup(String keyPharseMenuProfilesMaintenanceLookup) {
		this.keyPharseMenuProfilesMaintenanceLookup = keyPharseMenuProfilesMaintenanceLookup;
	}
	public String getKeyPharsePartoftheMenuProfileDescription() {
		return keyPharsePartoftheMenuProfileDescription;
	}
	public void setKeyPharsePartoftheMenuProfileDescription(String keyPharsePartoftheMenuProfileDescription) {
		this.keyPharsePartoftheMenuProfileDescription = keyPharsePartoftheMenuProfileDescription;
	}
	public String getKeyPharseERR_INVALID_MENU_PROFILE() {
		return keyPharseERR_INVALID_MENU_PROFILE;
	}
	public void setKeyPharseERR_INVALID_MENU_PROFILE(String keyPharseERR_INVALID_MENU_PROFILE) {
		this.keyPharseERR_INVALID_MENU_PROFILE = keyPharseERR_INVALID_MENU_PROFILE;
	}
	public String getKeyPharseERR_MENU_PROFILE_LEFT_BLANK() {
		return keyPharseERR_MENU_PROFILE_LEFT_BLANK;
	}
	public void setKeyPharseERR_MENU_PROFILE_LEFT_BLANK(String keyPharseERR_MENU_PROFILE_LEFT_BLANK) {
		this.keyPharseERR_MENU_PROFILE_LEFT_BLANK = keyPharseERR_MENU_PROFILE_LEFT_BLANK;
	}
	public String getKeyPharseERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION() {
		return keyPharseERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION;
	}
	public void setKeyPharseERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION(
			String keyPharseERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION) {
		this.keyPharseERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION = keyPharseERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION;
	}
	public String getKeyPharseERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK() {
		return keyPharseERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK;
	}
	public void setKeyPharseERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK(
			String keyPharseERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK) {
		this.keyPharseERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK = keyPharseERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK;
	}
	public String getKeyPharseERR_MENU_TYPE_LEFT_BLANK() {
		return keyPharseERR_MENU_TYPE_LEFT_BLANK;
	}
	public void setKeyPharseERR_MENU_TYPE_LEFT_BLANK(String keyPharseERR_MENU_TYPE_LEFT_BLANK) {
		this.keyPharseERR_MENU_TYPE_LEFT_BLANK = keyPharseERR_MENU_TYPE_LEFT_BLANK;
	}
	public String getLblMenuType() {
		return lblMenuType;
	}
	public void setLblMenuType(String lblMenuType) {
		this.lblMenuType = lblMenuType;
	}
	public String getLblMenuProfile() {
		return lblMenuProfile;
	}
	public void setLblMenuProfile(String lblMenuProfile) {
		this.lblMenuProfile = lblMenuProfile;
	}
	public String getLblMenuProfilesMaintenanceLookup() {
		return lblMenuProfilesMaintenanceLookup;
	}
	public void setLblMenuProfilesMaintenanceLookup(String lblMenuProfilesMaintenanceLookup) {
		this.lblMenuProfilesMaintenanceLookup = lblMenuProfilesMaintenanceLookup;
	}
	public String getLblPartoftheMenuProfileDescription() {
		return lblPartoftheMenuProfileDescription;
	}
	public void setLblPartoftheMenuProfileDescription(String lblPartoftheMenuProfileDescription) {
		this.lblPartoftheMenuProfileDescription = lblPartoftheMenuProfileDescription;
	}
	public String getLblERR_INVALID_MENU_PROFILE() {
		return lblERR_INVALID_MENU_PROFILE;
	}
	public void setLblERR_INVALID_MENU_PROFILE(String lblERR_INVALID_MENU_PROFILE) {
		this.lblERR_INVALID_MENU_PROFILE = lblERR_INVALID_MENU_PROFILE;
	}
	public String getLblERR_MENU_PROFILE_LEFT_BLANK() {
		return lblERR_MENU_PROFILE_LEFT_BLANK;
	}
	public void setLblERR_MENU_PROFILE_LEFT_BLANK(String lblERR_MENU_PROFILE_LEFT_BLANK) {
		this.lblERR_MENU_PROFILE_LEFT_BLANK = lblERR_MENU_PROFILE_LEFT_BLANK;
	}
	public String getLblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION() {
		return lblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION;
	}
	public void setLblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION(
			String lblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION) {
		this.lblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION = lblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION;
	}
	public String getLblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK() {
		return lblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK;
	}
	public void setLblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK(
			String lblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK) {
		this.lblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK = lblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK;
	}
	public String getLblERR_MENU_TYPE_LEFT_BLANK() {
		return lblERR_MENU_TYPE_LEFT_BLANK;
	}
	public void setLblERR_MENU_TYPE_LEFT_BLANK(String lblERR_MENU_TYPE_LEFT_BLANK) {
		this.lblERR_MENU_TYPE_LEFT_BLANK = lblERR_MENU_TYPE_LEFT_BLANK;
	}
	public String getKeyPharseMenuProfilesMaintenanceSearchLookup() {
		return keyPharseMenuProfilesMaintenanceSearchLookup;
	}
	public void setKeyPharseMenuProfilesMaintenanceSearchLookup(String keyPharseMenuProfilesMaintenanceSearchLookup) {
		this.keyPharseMenuProfilesMaintenanceSearchLookup = keyPharseMenuProfilesMaintenanceSearchLookup;
	}
	public String getKeyPharseDescription() {
		return keyPharseDescription;
	}
	public void setKeyPharseDescription(String keyPharseDescription) {
		this.keyPharseDescription = keyPharseDescription;
	}
	public String getKeyPharsedescriptionlong() {
		return keyPharsedescriptionlong;
	}
	public void setKeyPharsedescriptionlong(String keyPharsedescriptionlong) {
		this.keyPharsedescriptionlong = keyPharsedescriptionlong;
	}
	public String getKeyPharsedescriptionshrot() {
		return keyPharsedescriptionshrot;
	}
	public void setKeyPharsedescriptionshrot(String keyPharsedescriptionshrot) {
		this.keyPharsedescriptionshrot = keyPharsedescriptionshrot;
	}
	public String getKeyPharseAvailableMenuOptions() {
		return keyPharseAvailableMenuOptions;
	}
	public void setKeyPharseAvailableMenuOptions(String keyPharseAvailableMenuOptions) {
		this.keyPharseAvailableMenuOptions = keyPharseAvailableMenuOptions;
	}
	public String getKeyPharseMenuProfilesOptions() {
		return keyPharseMenuProfilesOptions;
	}
	public void setKeyPharseMenuProfilesOptions(String keyPharseMenuProfilesOptions) {
		this.keyPharseMenuProfilesOptions = keyPharseMenuProfilesOptions;
	}
	public String getKeyPharseSubApplication() {
		return keyPharseSubApplication;
	}
	public void setKeyPharseSubApplication(String keyPharseSubApplication) {
		this.keyPharseSubApplication = keyPharseSubApplication;
	}
	public String getKeyPharseMenuOption() {
		return keyPharseMenuOption;
	}
	public void setKeyPharseMenuOption(String keyPharseMenuOption) {
		this.keyPharseMenuOption = keyPharseMenuOption;
	}
	public String getKeyPharseOR() {
		return keyPharseOR;
	}
	public void setKeyPharseOR(String keyPharseOR) {
		this.keyPharseOR = keyPharseOR;
	}
	public String getKeyPharseSelectApplication() {
		return keyPharseSelectApplication;
	}
	public void setKeyPharseSelectApplication(String keyPharseSelectApplication) {
		this.keyPharseSelectApplication = keyPharseSelectApplication;
	}
	public String getKeyPharseMenuProfilesMaintenance() {
		return keyPharseMenuProfilesMaintenance;
	}
	public void setKeyPharseMenuProfilesMaintenance(String keyPharseMenuProfilesMaintenance) {
		this.keyPharseMenuProfilesMaintenance = keyPharseMenuProfilesMaintenance;
	}
	public String getKeyPharseAction() {
		return keyPharseAction;
	}
	public void setKeyPharseAction(String keyPharseAction) {
		this.keyPharseAction = keyPharseAction;
	}
	public String getKeyPharseCancel() {
		return keyPharseCancel;
	}
	public void setKeyPharseCancel(String keyPharseCancel) {
		this.keyPharseCancel = keyPharseCancel;
	}
	public String getKeyPharseDelete() {
		return keyPharseDelete;
	}
	public void setKeyPharseDelete(String keyPharseDelete) {
		this.keyPharseDelete = keyPharseDelete;
	}
	public String getKeyPharseEdit() {
		return keyPharseEdit;
	}
	public void setKeyPharseEdit(String keyPharseEdit) {
		this.keyPharseEdit = keyPharseEdit;
	}
	public String getKeyPharseAddnew() {
		return keyPharseAddnew;
	}
	public void setKeyPharseAddnew(String keyPharseAddnew) {
		this.keyPharseAddnew = keyPharseAddnew;
	}
	public String getKeyPharseNew() {
		return keyPharseNew;
	}
	public void setKeyPharseNew(String keyPharseNew) {
		this.keyPharseNew = keyPharseNew;
	}
	public String getKeyPharseUpdate_Save() {
		return keyPharseUpdate_Save;
	}
	public void setKeyPharseUpdate_Save(String keyPharseUpdate_Save) {
		this.keyPharseUpdate_Save = keyPharseUpdate_Save;
	}
	public String getKeyPharseLastActivityDate() {
		return keyPharseLastActivityDate;
	}
	public void setKeyPharseLastActivityDate(String keyPharseLastActivityDate) {
		this.keyPharseLastActivityDate = keyPharseLastActivityDate;
	}
	public String getKeyPharseLastActivityBy() {
		return keyPharseLastActivityBy;
	}
	public void setKeyPharseLastActivityBy(String keyPharseLastActivityBy) {
		this.keyPharseLastActivityBy = keyPharseLastActivityBy;
	}
	public String getKeyPharseHelpLine() {
		return keyPharseHelpLine;
	}
	public void setKeyPharseHelpLine(String keyPharseHelpLine) {
		this.keyPharseHelpLine = keyPharseHelpLine;
	}
	

}
