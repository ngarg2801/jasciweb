package com.jasci.biz.AdminModule.be;


/**
 *   
    Copyright 2014� World Wide 
    @Company Developed: NextGen Invent Corporation
 *  @authorFile purpose:Used for Global constants   
	@CreatedBy: Pradeep Kumar
	@Created On:04-Aug-2015
 *
 */
public class LABELTEXTBE {




	public static String SCAN="SCAN";	
	public static String PRODUCTS="PRODUCTS";
	public static String QUANTITY="QUANTITY";
	public static String LINES="LINES";
	public static String PRIORITY="PRIORITY";
	public static String PICK="Pick";
	public static String LOCATION="LOCATION";	
	public static String LOCATION_PRIME="PRIME";
	public static String TEXT_OK="OK";
	public static String DESCRIPTION="DESCRIPTION";
	public static String PRODUCT="PRODUCT";	
	public static String QUALITY="QUALITY";
	public static String MESSAGE_CHECK=" Message ";
	public static String LPN="LPN";
	public static String ITEM ="ITEM";	
	public static String SCAN_LPN="SCAN LPN";
	public static String SCAN_ITEM ="SCAN ITEM";	
	//Error msg
	public static String ERR_MSG_INVALID_SCAN_LOCATION="Invalid Location.";
	public static String ERR_MSG_LPN_ALREADY_ASSIGNED = "LPN Already Assigned";	
	public static String ERR_MSG_INVALID_WORK_TYPE = "Invalid Work Type.";	
	public static String ERR_MSG_INVALIDE_TASK_FOR_SEQUENCE = "Invalid Task for Sequence.";
	public static String ERR_MSG_INVALIDE_LPN = "Invalid LPN.";
	public static String ERR_MSG_INVALIDE_EXECUTION = "Invalid Execution.";
	public static String ERR_MSG_WORK_TYPE_NOT_FOUND = "Work Type not found.";
	public static String ERR_MSG_INTERNET_CONNECTION="Please check internet connection.";
	public static String ERR_MSG_INVALIDE_AREA_LOCATION = "Invalid area and location.";
	public static String ERR_MSG_SERVER_NOT_RESPOND = "Server not responding.";
	public static String STRLOADING="Loading...";
	public static String DIRECTION_FORWARD="FORWARD";
	public static String DIRECTION_LEFT="LEFT";
	public static String DIRECTION_RIGHT="RIGHT";
	
	
	
	public static String APK_UPDATE_MSG="Jasci app updated version available, Do you want to install updated version ?";
	public static String APK_UPDATE_YES="Yes";
	public static String APK_UPDATE_NO="No";
	public static String END="End";
	public static String EXIT="Exit";
	public static String START="Start";

	public static String[] GlassKeyPhraseArray={END,START,EXIT,STRLOADING,SCAN,PRODUCTS,QUANTITY,LINES,PRIORITY,PICK,LOCATION,LOCATION_PRIME,TEXT_OK,DESCRIPTION,PRODUCT,QUALITY,MESSAGE_CHECK,LPN,ITEM,SCAN_LPN,SCAN_ITEM,ERR_MSG_INVALID_SCAN_LOCATION,ERR_MSG_LPN_ALREADY_ASSIGNED,ERR_MSG_INVALID_WORK_TYPE,ERR_MSG_INVALIDE_TASK_FOR_SEQUENCE,ERR_MSG_INVALIDE_LPN,ERR_MSG_INVALIDE_EXECUTION,ERR_MSG_WORK_TYPE_NOT_FOUND,ERR_MSG_INTERNET_CONNECTION,ERR_MSG_INVALIDE_AREA_LOCATION,ERR_MSG_SERVER_NOT_RESPOND,APK_UPDATE_MSG,APK_UPDATE_YES,APK_UPDATE_NO,DIRECTION_FORWARD,DIRECTION_LEFT,DIRECTION_RIGHT};


	public static String getEND() {
		return END;
	}
	public static void setEND(String eND) {
		END = eND;
	}
	public static String getEXIT() {
		return EXIT;
	}
	public static void setEXIT(String eXIT) {
		EXIT = eXIT;
	}
	public static String getSTART() {
		return START;
	}
	public static void setSTART(String sTART) {
		START = sTART;
	}
	public static String getDIRECTION_FORWARD() {
		return DIRECTION_FORWARD;
	}
	public static void setDIRECTION_FORWARD(String dIRECTION_FORWARD) {
		DIRECTION_FORWARD = dIRECTION_FORWARD;
	}
	public static String getDIRECTION_LEFT() {
		return DIRECTION_LEFT;
	}
	public static void setDIRECTION_LEFT(String dIRECTION_LEFT) {
		DIRECTION_LEFT = dIRECTION_LEFT;
	}
	public static String getDIRECTION_RIGHT() {
		return DIRECTION_RIGHT;
	}
	public static void setDIRECTION_RIGHT(String dIRECTION_RIGHT) {
		DIRECTION_RIGHT = dIRECTION_RIGHT;
	}
	public static String getAPK_UPDATE_MSG() {
		return APK_UPDATE_MSG;
	}
	public static void setAPK_UPDATE_MSG(String aPK_UPDATE_MSG) {
		APK_UPDATE_MSG = aPK_UPDATE_MSG;
	}
	public static String getAPK_UPDATE_YES() {
		return APK_UPDATE_YES;
	}
	public static void setAPK_UPDATE_YES(String aPK_UPDATE_YES) {
		APK_UPDATE_YES = aPK_UPDATE_YES;
	}
	public static String getAPK_UPDATE_NO() {
		return APK_UPDATE_NO;
	}
	public static void setAPK_UPDATE_NO(String aPK_UPDATE_NO) {
		APK_UPDATE_NO = aPK_UPDATE_NO;
	}
	public static String getSTRLOADING() {
		return STRLOADING;
	}
	public static void setSTRLOADING(String sTRLOADING) {
		STRLOADING = sTRLOADING;
	}
	public static String getSCAN() {
		return SCAN;
	}
	public static void setSCAN(String sCAN) {
		SCAN = sCAN;
	}
	public static String getPRODUCTS() {
		return PRODUCTS;
	}
	public static void setPRODUCTS(String pRODUCTS) {
		PRODUCTS = pRODUCTS;
	}
	public static String getQUANTITY() {
		return QUANTITY;
	}
	public static void setQUANTITY(String qUANTITY) {
		QUANTITY = qUANTITY;
	}
	public static String getLINES() {
		return LINES;
	}
	public static void setLINES(String lINES) {
		LINES = lINES;
	}
	public static String getPRIORITY() {
		return PRIORITY;
	}
	public static void setPRIORITY(String pRIORITY) {
		PRIORITY = pRIORITY;
	}
	public static String getPICK() {
		return PICK;
	}
	public static void setPICK(String pICK) {
		PICK = pICK;
	}
	public static String getLOCATION() {
		return LOCATION;
	}
	public static void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public static String getLOCATION_PRIME() {
		return LOCATION_PRIME;
	}
	public static void setLOCATION_PRIME(String lOCATION_PRIME) {
		LOCATION_PRIME = lOCATION_PRIME;
	}
	public static String getTEXT_OK() {
		return TEXT_OK;
	}
	public static void setTEXT_OK(String tEXT_OK) {
		TEXT_OK = tEXT_OK;
	}
	public static String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public static void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public static String getPRODUCT() {
		return PRODUCT;
	}
	public static void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}
	public static String getQUALITY() {
		return QUALITY;
	}
	public static void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}
	public static String getMESSAGE_CHECK() {
		return MESSAGE_CHECK;
	}
	public static void setMESSAGE_CHECK(String mESSAGE_CHECK) {
		MESSAGE_CHECK = mESSAGE_CHECK;
	}
	public static String getLPN() {
		return LPN;
	}
	public static void setLPN(String lPN) {
		LPN = lPN;
	}
	public static String getITEM() {
		return ITEM;
	}
	public static void setITEM(String iTEM) {
		ITEM = iTEM;
	}
	public static String getSCAN_LPN() {
		return SCAN_LPN;
	}
	public static void setSCAN_LPN(String sCAN_LPN) {
		SCAN_LPN = sCAN_LPN;
	}
	public static String getSCAN_ITEM() {
		return SCAN_ITEM;
	}
	public static void setSCAN_ITEM(String sCAN_ITEM) {
		SCAN_ITEM = sCAN_ITEM;
	}
	public static String getERR_MSG_INVALID_SCAN_LOCATION() {
		return ERR_MSG_INVALID_SCAN_LOCATION;
	}
	public static void setERR_MSG_INVALID_SCAN_LOCATION(
			String eRR_MSG_INVALID_SCAN_LOCATION) {
		ERR_MSG_INVALID_SCAN_LOCATION = eRR_MSG_INVALID_SCAN_LOCATION;
	}
	public static String getERR_MSG_LPN_ALREADY_ASSIGNED() {
		return ERR_MSG_LPN_ALREADY_ASSIGNED;
	}
	public static void setERR_MSG_LPN_ALREADY_ASSIGNED(
			String eRR_MSG_LPN_ALREADY_ASSIGNED) {
		ERR_MSG_LPN_ALREADY_ASSIGNED = eRR_MSG_LPN_ALREADY_ASSIGNED;
	}
	public static String getERR_MSG_INVALID_WORK_TYPE() {
		return ERR_MSG_INVALID_WORK_TYPE;
	}
	public static void setERR_MSG_INVALID_WORK_TYPE(String eRR_MSG_INVALID_WORK_TYPE) {
		ERR_MSG_INVALID_WORK_TYPE = eRR_MSG_INVALID_WORK_TYPE;
	}
	public static String getERR_MSG_INVALIDE_TASK_FOR_SEQUENCE() {
		return ERR_MSG_INVALIDE_TASK_FOR_SEQUENCE;
	}
	public static void setERR_MSG_INVALIDE_TASK_FOR_SEQUENCE(
			String eRR_MSG_INVALIDE_TASK_FOR_SEQUENCE) {
		ERR_MSG_INVALIDE_TASK_FOR_SEQUENCE = eRR_MSG_INVALIDE_TASK_FOR_SEQUENCE;
	}
	public static String getERR_MSG_INVALIDE_LPN() {
		return ERR_MSG_INVALIDE_LPN;
	}
	public static void setERR_MSG_INVALIDE_LPN(String eRR_MSG_INVALIDE_LPN) {
		ERR_MSG_INVALIDE_LPN = eRR_MSG_INVALIDE_LPN;
	}
	public static String getERR_MSG_INVALIDE_EXECUTION() {
		return ERR_MSG_INVALIDE_EXECUTION;
	}
	public static void setERR_MSG_INVALIDE_EXECUTION(
			String eRR_MSG_INVALIDE_EXECUTION) {
		ERR_MSG_INVALIDE_EXECUTION = eRR_MSG_INVALIDE_EXECUTION;
	}
	public static String getERR_MSG_WORK_TYPE_NOT_FOUND() {
		return ERR_MSG_WORK_TYPE_NOT_FOUND;
	}
	public static void setERR_MSG_WORK_TYPE_NOT_FOUND(
			String eRR_MSG_WORK_TYPE_NOT_FOUND) {
		ERR_MSG_WORK_TYPE_NOT_FOUND = eRR_MSG_WORK_TYPE_NOT_FOUND;
	}
	public static String getERR_MSG_INTERNET_CONNECTION() {
		return ERR_MSG_INTERNET_CONNECTION;
	}
	public static void setERR_MSG_INTERNET_CONNECTION(
			String eRR_MSG_INTERNET_CONNECTION) {
		ERR_MSG_INTERNET_CONNECTION = eRR_MSG_INTERNET_CONNECTION;
	}
	public static String getERR_MSG_INVALIDE_AREA_LOCATION() {
		return ERR_MSG_INVALIDE_AREA_LOCATION;
	}
	public static void setERR_MSG_INVALIDE_AREA_LOCATION(
			String eRR_MSG_INVALIDE_AREA_LOCATION) {
		ERR_MSG_INVALIDE_AREA_LOCATION = eRR_MSG_INVALIDE_AREA_LOCATION;
	}
	public static String getERR_MSG_SERVER_NOT_RESPOND() {
		return ERR_MSG_SERVER_NOT_RESPOND;
	}
	public static void setERR_MSG_SERVER_NOT_RESPOND(
			String eRR_MSG_SERVER_NOT_RESPOND) {
		ERR_MSG_SERVER_NOT_RESPOND = eRR_MSG_SERVER_NOT_RESPOND;
	}








}
