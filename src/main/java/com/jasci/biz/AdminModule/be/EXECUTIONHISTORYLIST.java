package com.jasci.biz.AdminModule.be;

/*

Date Developed  Oct 10 2015
Description pojo class is used to make getter and setter 
Created By Shailendar kumar

 */

public class EXECUTIONHISTORYLIST {
	
	 public EXCECUTIONHISTORY[] EXCECUTIONHISTORY;

	    public EXCECUTIONHISTORY[] getEXCECUTIONHISTORY ()
	    {
	        return EXCECUTIONHISTORY;
	    }

	    public void setEXCECUTIONHISTORY (EXCECUTIONHISTORY[] EXCECUTIONHISTORY)
	    {
	        this.EXCECUTIONHISTORY = EXCECUTIONHISTORY;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [EXCECUTIONHISTORY = "+EXCECUTIONHISTORY+"]";
	    }

}
