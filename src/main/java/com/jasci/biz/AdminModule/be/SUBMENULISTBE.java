/*
 
Date Developed  Nov 02 2014
Description  Created a getter and setter of Headers
Created By Sarvendra Tyagi
 */
package com.jasci.biz.AdminModule.be;

import java.util.HashMap;
import java.util.Map;


public class SUBMENULISTBE {
	
	private Map<String, Map<String, String>> hmListSubMenu=new HashMap<String, Map<String,String>>();

	public Map<String, Map<String, String>> getHmListSubMenu() {
		return hmListSubMenu;
	}

	public void setHmListSubMenu(Map<String, Map<String, String>> hmListSubMenu) {
		this.hmListSubMenu = hmListSubMenu;
	}

}
