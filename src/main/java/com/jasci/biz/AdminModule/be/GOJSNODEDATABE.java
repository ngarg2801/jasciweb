/**
 
Date Developed  July 10 2014
Description pojo class of GOJS NODE data in which have getter and setter methods.
Created By Shailendra Rajput
 */
package com.jasci.biz.AdminModule.be;

public class GOJSNODEDATABE {
	
	private String Text;
	private String ExecutionName;
	private String Key;
	private String ToolTipText;
	private String Category;
	private String Loc;
	public String getText() {
		return Text;
	}
	public void setText(String text) {
		Text = text;
	}
	
	public String getExecutionName() {
		return ExecutionName;
	}
	public void setExecutionName(String executionName) {
		ExecutionName = executionName;
	}
	public String getKey() {
		return Key;
	}
	public void setKey(String key) {
		Key = key;
	}
	public String getToolTipText() {
		return ToolTipText;
	}
	public void setToolTipText(String toolTipText) {
		ToolTipText = toolTipText;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getLoc() {
		return Loc;
	}
	public void setLoc(String loc) {
		Loc = loc;
	}
	

}
