package com.jasci.biz.AdminModule.be;
/**
 *   
      
    
 * @author Pradeep Kumar
 * @Date Modified :Aug 1 2015
 * @Description: Bean class for Work history
 * **/
public class SCANLPNHISTORYBE {
	
	public  String getTenant() {
		return Tenant;
	}
	public  void setTenant(String tenant) {
		Tenant = tenant;
	}
	public  String getWorkControlNumber() {
		return WorkControlNumber;
	}
	public  void setWorkControlNumber(String workControlNumber) {
		WorkControlNumber = workControlNumber;
	}
	public  String getTask() {
		return Task;
	}
	public  void setTask(String task) {
		Task = task;
	}
	public  String getTeamMember() {
		return TeamMember;
	}
	public  void setTeamMember(String teamMember) {
		TeamMember = teamMember;
	}
	public  String getDate() {
		return Date;
	}
	public  void setDate(String date) {
		Date = date;
	}
	public  String getCompany() {
		return Company;
	}
	public  void setCompany(String company) {
		Company = company;
	}
	public  String getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public  void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public  String getLPN() {
		return LPN;
	}
	public  void setLPN(String lPN) {
		LPN = lPN;
	}
	public  String getASN() {
		return ASN;
	}
	public void setASN(String aSN) {
		ASN = aSN;
	}
	public  String getZONE() {
		return ZONE;
	}
	public  void setZONE(String zONE) {
		ZONE = zONE;
	}
	public  String getDIVERT() {
		return DIVERT;
	}
	public  void setDIVERT(String dIVERT) {
		DIVERT = dIVERT;
	}
	public  String getWorkType() {
		return WorkType;
	}
	public  void setWorkType(String workType) {
		WorkType = workType;
	}
	public  String getWorkHistoryCode() {
		return WorkHistoryCode;
	}
	public  void setWorkHistoryCode(String workHistoryCode) {
		WorkHistoryCode = workHistoryCode;
	}
	public  String Tenant;
	public  String WorkControlNumber;
	public  String Task;
	public  String TeamMember;
	public  String Date;
	public  String Company;
	public  String FulfillmentCenter;
	public  String LPN;
	public  String ASN;
	public  String ZONE;
	public  String DIVERT;
	public  String WorkType;
	public  String WorkHistoryCode;


}
