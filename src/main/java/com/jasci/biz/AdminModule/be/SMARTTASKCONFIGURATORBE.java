/**
Description It is used to make setter and getter for changing text of screen in another language
Created By Shailendra Rajput
Created Date May 15 2015
 */
package com.jasci.biz.AdminModule.be;

public class SMARTTASKCONFIGURATORBE {

	
	public static String SmartTaskConfigurator_Select;
	public static String SmartTaskConfigurator_Task;
	public static String SmartTaskConfigurator_Application;
	public static String SmartTaskConfigurator_Execution_Group;
	public static String SmartTaskConfigurator_Execution_Type;
	public static String SmartTaskConfigurator_Execution_Device;
	public static String SmartTaskConfigurator_Execution_Sequence;
	
	public static String SmartTaskConfigurator_Group;
	public static String SmartTaskConfigurator_Type;
	public static String SmartTaskConfigurator_Device;
	public static String SmartTaskConfigurator_Sequence;	
	public static String SmartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted;
	
	public static String SmartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description;
	public static String SmartTaskConfigurator_Tenant;
	public static String SmartTaskConfigurator_Menu_Name;
	public static String SmartTaskConfigurator_New;
	public static String SmartTaskConfigurator_Display_All;
	public static String SmartTaskConfigurator_Search_All;
	public static String SmartTaskConfigurator_Smart_Task_Configurator_Lookup;
	
	public static String SmartTaskConfigurator_Please_select_Application;
	public static String SmartTaskConfigurator_Please_select_Execution_Group;
	public static String SmartTaskConfigurator_Please_select_Task;
	public static String SmartTaskConfigurator_Please_select_Execution_Type;
	public static String SmartTaskConfigurator_Please_select_Execution_Device;
	public static String SmartTaskConfigurator_Please_enter_Execution_Sequence;
	public static String SmartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description;
	public static String SmartTaskConfigurator_Please_enter_Tenant;
	public static String SmartTaskConfigurator_Please_enter_Menu_Name;
	
	public static String SmartTaskConfigurator_Invalid_Application;
	public static String SmartTaskConfigurator_Invalid_Execution_Group;
	public static String SmartTaskConfigurator_Invalid_Task;
	public static String SmartTaskConfigurator_Invalid_Execution_Type;
	public static String SmartTaskConfigurator_Invalid_Execution_Device;
	public static String SmartTaskConfigurator_Invalid_Execution_Sequence;
	public static String SmartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description;
	public static String SmartTaskConfigurator_Invalid_Tenant;
	public static String SmartTaskConfigurator_Invalid_Menu_Name;
	
	public static String SmartTaskConfigurator_Smart_Task_Configurator_Search_Lookup;
	public static String SmartTaskConfigurator_Add_New;
	public static String SmartTaskConfigurator_Edit;
	public static String SmartTaskConfigurator_Notes;
	public static String SmartTaskConfigurator_Delete;
	public static String SmartTaskConfigurator_Actions;
	public static String SmartTaskConfigurator_Description;
	public static String SmartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record;
	
	
	
	public static String SmartTaskConfigurator_Smart_Task_Configurator_Maintenance;
	public static String SmartTaskConfigurator_Last_Activity_Date;
	public static String SmartTaskConfigurator_Last_Activity_By;
	public static String SmartTaskConfigurator_Description_Short;
	public static String SmartTaskConfigurator_Description_Long;
	public static String SmartTaskConfigurator_Help_Line;
	public static String SmartTaskConfigurator_Save_Update;
	public static String SmartTaskConfigurator_Reset;
	public static String SmartTaskConfigurator_Cancel;
	public static String SmartTaskConfigurator_mandatory_field_can_not_be_left_blank;
	public static String SmartTaskConfigurator_Your_record_has_been_saved_successfully;
	public static String SmartTaskConfigurator_Your_record_has_been_updated_successfully;
	
	public static String SmartTaskConfigurator_Company;
	
	public static String SmartTaskConfigurator_Execution_Sequence_Name;
	public static String SmartTaskConfigurator_Configurator;
	public static String SmartTaskConfigurator_Execution;
	public static String SmartTaskConfigurator_Action;
	public static String SmartTaskConfigurator_Sequence_View;
	public static String SmartTaskConfigurator_Smart_Task_Congigurator_Maintenance;
	
	public static String SmartTaskConfigurator_Execution_Sequence_Type;
	public static String SmartTaskConfigurator_Go_To_Tag;
	public static String SmartTaskConfigurator_Message;
	
	public static String SmartTaskConfigurator_TeamMemberName;
	public static String SmartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist;

	public static String SmartTaskConfigurator_Please_first_of_all_connect_every_node_via_link; 
	public static String SmartTaskConfigurator_IF_EXIT;
	public static String SmartTaskConfigurator_GoTo_Tag;
	public static String SmartTaskConfigurator_Save;
	public static String SmartTaskConfigurator_Display_Text;
	public static String SmartTaskConfigurator_IF_ERROR;
	public static String SmartTaskConfigurator_GENERAL_TAG_NAME;
	public static String SmartTaskConfigurator_CUSTOM_EXECUTION_PATH; 
	public static String SmartTaskConfigurator_Return_Code;
	public static String SmartTaskConfigurator_IF_RETURN_CODE;
	public static String SmartTaskConfigurator_COMMENT;
	public static String SmartTaskConfigurator_JASCICONTEXT;

	
	
	public static String SmartTaskConfigurator_Start_node_already_exist;
	public static String SmartTaskConfigurator_End_node_already_exist;
	public static String SmartTaskConfigurator_You_can_not_delete_Start_node;
	public static String SmartTaskConfigurator_You_can_not_delete_End_node;
	public static String SmartTaskConfigurator_You_cannot_delete_selected_link;
	
	public static String SmartTaskConfigurator_Please_select_GoTo_Tag_first;
	public static String SmartTaskConfigurator_All_fields_are_mandatory;
	public static String SmartTaskConfigurator_Please_enter_display_text;
	public static String SmartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel;
	public static String SmartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link;
	public static String SmartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one;
	public static String SmartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking;
	public static String SmartTaskConfigurator_General_Tag_already_exist_with;
	public static String SmartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name;
	public static String SmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid;
	public static String SmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid;
	public static String SmartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid;
	public static String SmartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data;
	public static String SmartTaskConfigurator_Please_connect_every_node_via_link_before_align_data;
	public static String SmartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel;
	public static String SmartTaskConfigurator_Align;
	public static String SmartTaskConfigurator_Print;
	public static String SmartTaskConfigurator_Back;
	public static String SmartTaskConfigurator_Reload;
    public static String SmartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking;
    public static String SmartTaskConfigurator_Sequence_Name;
	public static String SmartTaskConfigurator_Execution_TagName;
	public static String SmartTaskConfigurator_Copy;
	
	 public static String SmartTaskConfigurator_Refresh;
	 public static String SmartTaskConfigurator_Close;
	 public static String SmartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	 public static String SmartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	 public static String SmartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel;
	 public static String SmartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	 public static String SmartTaskConfigurator_other_nodes_not_connected;
     public static String SmartTaskConfigurator_AND;
     public static String SmartTaskConfigurator_Please_enter_Display_text;
     public static String SmartTaskConfigurator_Please_enter_Comment;
     public static String SmartTaskConfigurator_Please_enter_General_Tag_Name;
     public static String SmartTaskConfigurator_Please_enter_Custom_Execution_Path;
     public static String SmartTaskConfigurator_Custom_Execution_Path_not_valid;
     public static String SmartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION;
     public static String SmartTaskConfigurator_Please_enter_different_value_except_COMMENT;
     public static String SmartTaskConfigurator_Please_enter_different_value_except_DISPLAY;
     public static String SmartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG;
     public static String SmartTaskConfigurator_Please_do_not_left_any_node_blank;
     
	
     public static String SmartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data;
	 
     
	 public  String getSmartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data() {
		return SmartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data;
	}
	public  void setSmartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data(
			String smartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data) {
		SmartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data = smartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data;
	}
	
	public String getSmartTaskConfigurator_Please_enter_Display_text() {
		return SmartTaskConfigurator_Please_enter_Display_text;
	}
	public void setSmartTaskConfigurator_Please_enter_Display_text(String smartTaskConfigurator_Please_enter_Display_text) {
		SmartTaskConfigurator_Please_enter_Display_text = smartTaskConfigurator_Please_enter_Display_text;
	}
	public String getSmartTaskConfigurator_Please_enter_Comment() {
		return SmartTaskConfigurator_Please_enter_Comment;
	}
	public void setSmartTaskConfigurator_Please_enter_Comment(String smartTaskConfigurator_Please_enter_Comment) {
		SmartTaskConfigurator_Please_enter_Comment = smartTaskConfigurator_Please_enter_Comment;
	}
	public String getSmartTaskConfigurator_Please_enter_General_Tag_Name() {
		return SmartTaskConfigurator_Please_enter_General_Tag_Name;
	}
	public void setSmartTaskConfigurator_Please_enter_General_Tag_Name(
			String smartTaskConfigurator_Please_enter_General_Tag_Name) {
		SmartTaskConfigurator_Please_enter_General_Tag_Name = smartTaskConfigurator_Please_enter_General_Tag_Name;
	}
	public String getSmartTaskConfigurator_Please_enter_Custom_Execution_Path() {
		return SmartTaskConfigurator_Please_enter_Custom_Execution_Path;
	}
	public void setSmartTaskConfigurator_Please_enter_Custom_Execution_Path(
			String smartTaskConfigurator_Please_enter_Custom_Execution_Path) {
		SmartTaskConfigurator_Please_enter_Custom_Execution_Path = smartTaskConfigurator_Please_enter_Custom_Execution_Path;
	}
	public String getSmartTaskConfigurator_Custom_Execution_Path_not_valid() {
		return SmartTaskConfigurator_Custom_Execution_Path_not_valid;
	}
	public void setSmartTaskConfigurator_Custom_Execution_Path_not_valid(
			String smartTaskConfigurator_Custom_Execution_Path_not_valid) {
		SmartTaskConfigurator_Custom_Execution_Path_not_valid = smartTaskConfigurator_Custom_Execution_Path_not_valid;
	}
	public String getSmartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION() {
		return SmartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION;
	}
	public void setSmartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION(
			String smartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION) {
		SmartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION = smartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION;
	}
	public String getSmartTaskConfigurator_Please_enter_different_value_except_COMMENT() {
		return SmartTaskConfigurator_Please_enter_different_value_except_COMMENT;
	}
	public void setSmartTaskConfigurator_Please_enter_different_value_except_COMMENT(
			String smartTaskConfigurator_Please_enter_different_value_except_COMMENT) {
		SmartTaskConfigurator_Please_enter_different_value_except_COMMENT = smartTaskConfigurator_Please_enter_different_value_except_COMMENT;
	}
	public String getSmartTaskConfigurator_Please_enter_different_value_except_DISPLAY() {
		return SmartTaskConfigurator_Please_enter_different_value_except_DISPLAY;
	}
	public void setSmartTaskConfigurator_Please_enter_different_value_except_DISPLAY(
			String smartTaskConfigurator_Please_enter_different_value_except_DISPLAY) {
		SmartTaskConfigurator_Please_enter_different_value_except_DISPLAY = smartTaskConfigurator_Please_enter_different_value_except_DISPLAY;
	}
	public String getSmartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG() {
		return SmartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG;
	}
	public void setSmartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG(
			String smartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG) {
		SmartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG = smartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG;
	}
	public String getSmartTaskConfigurator_Please_do_not_left_any_node_blank() {
		return SmartTaskConfigurator_Please_do_not_left_any_node_blank;
	}
	public void setSmartTaskConfigurator_Please_do_not_left_any_node_blank(
			String smartTaskConfigurator_Please_do_not_left_any_node_blank) {
		SmartTaskConfigurator_Please_do_not_left_any_node_blank = smartTaskConfigurator_Please_do_not_left_any_node_blank;
	}
	public  String getSmartTaskConfigurator_other_nodes_not_connected() {
		return SmartTaskConfigurator_other_nodes_not_connected;
	}
	public  void setSmartTaskConfigurator_other_nodes_not_connected(
			String smartTaskConfigurator_other_nodes_not_connected) {
		SmartTaskConfigurator_other_nodes_not_connected = smartTaskConfigurator_other_nodes_not_connected;
	}
	public  String getSmartTaskConfigurator_AND() {
		return SmartTaskConfigurator_AND;
	}
	public  void setSmartTaskConfigurator_AND(String smartTaskConfigurator_AND) {
		SmartTaskConfigurator_AND = smartTaskConfigurator_AND;
	}
	public  String getSmartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG() {
		return SmartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	}
	public  void setSmartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG(
			String smartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG) {
		SmartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG = smartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	}
	 public  String getSmartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel() {
		return SmartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel;
	}
	public void setSmartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel(
			String smartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel) {
		SmartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel = smartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel;
	}
	public  String getSmartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG() {
		return SmartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	}
	public  void setSmartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG(
			String smartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG) {
		SmartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG = smartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	}
	public  String getSmartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG() {
		return SmartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	}
	public  void setSmartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG(
			String smartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG) {
		SmartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG = smartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG;
	}
	public  String getSmartTaskConfigurator_Refresh() {
	  return SmartTaskConfigurator_Refresh;
	 }
	 public  void setSmartTaskConfigurator_Refresh(
	   String smartTaskConfigurator_Refresh) {
	  SmartTaskConfigurator_Refresh = smartTaskConfigurator_Refresh;
	 }
	 public  String getSmartTaskConfigurator_Close() {
	  return SmartTaskConfigurator_Close;
	 }
	 public  void setSmartTaskConfigurator_Close(
	   String smartTaskConfigurator_Close) {
	  SmartTaskConfigurator_Close = smartTaskConfigurator_Close;
	 }

	public  String getSmartTaskConfigurator_Copy() {
		return SmartTaskConfigurator_Copy;
	}
	public  void setSmartTaskConfigurator_Copy(
			String smartTaskConfigurator_Copy) {
		SmartTaskConfigurator_Copy = smartTaskConfigurator_Copy;
	}
	public  String getSmartTaskConfigurator_Sequence_Name() {
		return SmartTaskConfigurator_Sequence_Name;
	}
	public  void setSmartTaskConfigurator_Sequence_Name(
			String smartTaskConfigurator_Sequence_Name) {
		SmartTaskConfigurator_Sequence_Name = smartTaskConfigurator_Sequence_Name;
	}
	public  String getSmartTaskConfigurator_Execution_TagName() {
		return SmartTaskConfigurator_Execution_TagName;
	}
	public  void setSmartTaskConfigurator_Execution_TagName(
			String smartTaskConfigurator_Execution_TagName) {
		SmartTaskConfigurator_Execution_TagName = smartTaskConfigurator_Execution_TagName;
	}
		
	
	public  String getSmartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking() {
		return SmartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking;
	}
	public  void setSmartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking(
			String smartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking) {
		SmartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking = smartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking;
	}
	
	public  String getSmartTaskConfigurator_Align() {
		return SmartTaskConfigurator_Align;
	}
	public  void setSmartTaskConfigurator_Align(
			String smartTaskConfigurator_Align) {
		SmartTaskConfigurator_Align = smartTaskConfigurator_Align;
	}
	public  String getSmartTaskConfigurator_Print() {
		return SmartTaskConfigurator_Print;
	}
	public  void setSmartTaskConfigurator_Print(
			String smartTaskConfigurator_Print) {
		SmartTaskConfigurator_Print = smartTaskConfigurator_Print;
	}
	public  String getSmartTaskConfigurator_Back() {
		return SmartTaskConfigurator_Back;
	}
	public  void setSmartTaskConfigurator_Back(
			String smartTaskConfigurator_Back) {
		SmartTaskConfigurator_Back = smartTaskConfigurator_Back;
	}
	public  String getSmartTaskConfigurator_Reload() {
		return SmartTaskConfigurator_Reload;
	}
	public  void setSmartTaskConfigurator_Reload(
			String smartTaskConfigurator_Reload) {
		SmartTaskConfigurator_Reload = smartTaskConfigurator_Reload;
	}
		
	public String getSmartTaskConfigurator_Please_connect_every_node_via_link_before_align_data() {
		return SmartTaskConfigurator_Please_connect_every_node_via_link_before_align_data;
	}
	public void setSmartTaskConfigurator_Please_connect_every_node_via_link_before_align_data(
			String smartTaskConfigurator_Please_connect_every_node_via_link_before_align_data) {
		SmartTaskConfigurator_Please_connect_every_node_via_link_before_align_data = smartTaskConfigurator_Please_connect_every_node_via_link_before_align_data;
	}
	public String getSmartTaskConfigurator_JASCICONTEXT() {
		return SmartTaskConfigurator_JASCICONTEXT;
	}
	public void setSmartTaskConfigurator_JASCICONTEXT(String smartTaskConfigurator_JASCICONTEXT) {
		SmartTaskConfigurator_JASCICONTEXT = smartTaskConfigurator_JASCICONTEXT;
	}
	public  String getSmartTaskConfigurator_Start_node_already_exist() {
		return SmartTaskConfigurator_Start_node_already_exist;
	}
	public  void setSmartTaskConfigurator_Start_node_already_exist(
			String smartTaskConfigurator_Start_node_already_exist) {
		SmartTaskConfigurator_Start_node_already_exist = smartTaskConfigurator_Start_node_already_exist;
	}
	public  String getSmartTaskConfigurator_End_node_already_exist() {
		return SmartTaskConfigurator_End_node_already_exist;
	}
	public  void setSmartTaskConfigurator_End_node_already_exist(
			String smartTaskConfigurator_End_node_already_exist) {
		SmartTaskConfigurator_End_node_already_exist = smartTaskConfigurator_End_node_already_exist;
	}
	public  String getSmartTaskConfigurator_You_can_not_delete_Start_node() {
		return SmartTaskConfigurator_You_can_not_delete_Start_node;
	}
	public  void setSmartTaskConfigurator_You_can_not_delete_Start_node(
			String smartTaskConfigurator_You_can_not_delete_Start_node) {
		SmartTaskConfigurator_You_can_not_delete_Start_node = smartTaskConfigurator_You_can_not_delete_Start_node;
	}
	public  String getSmartTaskConfigurator_You_can_not_delete_End_node() {
		return SmartTaskConfigurator_You_can_not_delete_End_node;
	}
	public  void setSmartTaskConfigurator_You_can_not_delete_End_node(
			String smartTaskConfigurator_You_can_not_delete_End_node) {
		SmartTaskConfigurator_You_can_not_delete_End_node = smartTaskConfigurator_You_can_not_delete_End_node;
	}
	public  String getSmartTaskConfigurator_You_cannot_delete_selected_link() {
		return SmartTaskConfigurator_You_cannot_delete_selected_link;
	}
	public  void setSmartTaskConfigurator_You_cannot_delete_selected_link(
			String smartTaskConfigurator_You_cannot_delete_selected_link) {
		SmartTaskConfigurator_You_cannot_delete_selected_link = smartTaskConfigurator_You_cannot_delete_selected_link;
	}
	public  String getSmartTaskConfigurator_Please_first_of_all_connect_every_node_via_link() {
		return SmartTaskConfigurator_Please_first_of_all_connect_every_node_via_link;
	}
	public  void setSmartTaskConfigurator_Please_first_of_all_connect_every_node_via_link(
			String smartTaskConfigurator_Please_first_of_all_connect_every_node_via_link) {
		SmartTaskConfigurator_Please_first_of_all_connect_every_node_via_link = smartTaskConfigurator_Please_first_of_all_connect_every_node_via_link;
	}
	public  String getSmartTaskConfigurator_IF_EXIT() {
		return SmartTaskConfigurator_IF_EXIT;
	}
	public  void setSmartTaskConfigurator_IF_EXIT(
			String smartTaskConfigurator_IF_EXIT) {
		SmartTaskConfigurator_IF_EXIT = smartTaskConfigurator_IF_EXIT;
	}
	public  String getSmartTaskConfigurator_GoTo_Tag() {
		return SmartTaskConfigurator_GoTo_Tag;
	}
	public  void setSmartTaskConfigurator_GoTo_Tag(
			String smartTaskConfigurator_GoTo_Tag) {
		SmartTaskConfigurator_GoTo_Tag = smartTaskConfigurator_GoTo_Tag;
	}
	public  String getSmartTaskConfigurator_Save() {
		return SmartTaskConfigurator_Save;
	}
	public  void setSmartTaskConfigurator_Save(
			String smartTaskConfigurator_Save) {
		SmartTaskConfigurator_Save = smartTaskConfigurator_Save;
	}
	public  String getSmartTaskConfigurator_Display_Text() {
		return SmartTaskConfigurator_Display_Text;
	}
	public  void setSmartTaskConfigurator_Display_Text(
			String smartTaskConfigurator_Display_Text) {
		SmartTaskConfigurator_Display_Text = smartTaskConfigurator_Display_Text;
	}
	public  String getSmartTaskConfigurator_IF_ERROR() {
		return SmartTaskConfigurator_IF_ERROR;
	}
	public  void setSmartTaskConfigurator_IF_ERROR(
			String smartTaskConfigurator_IF_ERROR) {
		SmartTaskConfigurator_IF_ERROR = smartTaskConfigurator_IF_ERROR;
	}
	public  String getSmartTaskConfigurator_GENERAL_TAG_NAME() {
		return SmartTaskConfigurator_GENERAL_TAG_NAME;
	}
	public  void setSmartTaskConfigurator_GENERAL_TAG_NAME(
			String smartTaskConfigurator_GENERAL_TAG_NAME) {
		SmartTaskConfigurator_GENERAL_TAG_NAME = smartTaskConfigurator_GENERAL_TAG_NAME;
	}
	public  String getSmartTaskConfigurator_CUSTOM_EXECUTION_PATH() {
		return SmartTaskConfigurator_CUSTOM_EXECUTION_PATH;
	}
	public  void setSmartTaskConfigurator_CUSTOM_EXECUTION_PATH(
			String smartTaskConfigurator_CUSTOM_EXECUTION_PATH) {
		SmartTaskConfigurator_CUSTOM_EXECUTION_PATH = smartTaskConfigurator_CUSTOM_EXECUTION_PATH;
	}
	public  String getSmartTaskConfigurator_Return_Code() {
		return SmartTaskConfigurator_Return_Code;
	}
	public  void setSmartTaskConfigurator_Return_Code(
			String smartTaskConfigurator_Return_Code) {
		SmartTaskConfigurator_Return_Code = smartTaskConfigurator_Return_Code;
	}
	public  String getSmartTaskConfigurator_IF_RETURN_CODE() {
		return SmartTaskConfigurator_IF_RETURN_CODE;
	}
	public  void setSmartTaskConfigurator_IF_RETURN_CODE(
			String smartTaskConfigurator_IF_RETURN_CODE) {
		SmartTaskConfigurator_IF_RETURN_CODE = smartTaskConfigurator_IF_RETURN_CODE;
	}
	public  String getSmartTaskConfigurator_COMMENT() {
		return SmartTaskConfigurator_COMMENT;
	}
	public  void setSmartTaskConfigurator_COMMENT(
			String smartTaskConfigurator_COMMENT) {
		SmartTaskConfigurator_COMMENT = smartTaskConfigurator_COMMENT;
	}
	
	
	
	public  String getSmartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist() {
		return SmartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist;
	}
	public  void setSmartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist(
			String smartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist) {
		SmartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist = smartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist;
	}
	public  String getSmartTaskConfigurator_Execution_Sequence_Type() {
		return SmartTaskConfigurator_Execution_Sequence_Type;
	}
	public  void setSmartTaskConfigurator_Execution_Sequence_Type(
			String smartTaskConfigurator_Execution_Sequence_Type) {
		SmartTaskConfigurator_Execution_Sequence_Type = smartTaskConfigurator_Execution_Sequence_Type;
	}
	public  String getSmartTaskConfigurator_Go_To_Tag() {
		return SmartTaskConfigurator_Go_To_Tag;
	}
	public  void setSmartTaskConfigurator_Go_To_Tag(
			String smartTaskConfigurator_Go_To_Tag) {
		SmartTaskConfigurator_Go_To_Tag = smartTaskConfigurator_Go_To_Tag;
	}
	public  String getSmartTaskConfigurator_Message() {
		return SmartTaskConfigurator_Message;
	}
	public  void setSmartTaskConfigurator_Message(
			String smartTaskConfigurator_Message) {
		SmartTaskConfigurator_Message = smartTaskConfigurator_Message;
	}
	
	public 	 String getSmartTaskConfigurator_TeamMemberName() {
		return SmartTaskConfigurator_TeamMemberName;
	}
	public   void setSmartTaskConfigurator_TeamMemberName(
			String smartTaskConfigurator_TeamMemberName) {
		SmartTaskConfigurator_TeamMemberName = smartTaskConfigurator_TeamMemberName;
	}
	
	public  String getSmartTaskConfigurator_Execution_Sequence_Name() {
		return SmartTaskConfigurator_Execution_Sequence_Name;
	}
	public  void setSmartTaskConfigurator_Execution_Sequence_Name(
			String smartTaskConfigurator_Execution_Sequence_Name) {
		SmartTaskConfigurator_Execution_Sequence_Name = smartTaskConfigurator_Execution_Sequence_Name;
	}
	public  String getSmartTaskConfigurator_Configurator() {
		return SmartTaskConfigurator_Configurator;
	}
	public  void setSmartTaskConfigurator_Configurator(
			String smartTaskConfigurator_Configurator) {
		SmartTaskConfigurator_Configurator = smartTaskConfigurator_Configurator;
	}
	public  String getSmartTaskConfigurator_Execution() {
		return SmartTaskConfigurator_Execution;
	}
	public  void setSmartTaskConfigurator_Execution(
			String smartTaskConfigurator_Execution) {
		SmartTaskConfigurator_Execution = smartTaskConfigurator_Execution;
	}
	public  String getSmartTaskConfigurator_Action() {
		return SmartTaskConfigurator_Action;
	}
	public  void setSmartTaskConfigurator_Action(
			String smartTaskConfigurator_Action) {
		SmartTaskConfigurator_Action = smartTaskConfigurator_Action;
	}
	public  String getSmartTaskConfigurator_Sequence_View() {
		return SmartTaskConfigurator_Sequence_View;
	}
	public  void setSmartTaskConfigurator_Sequence_View(
			String smartTaskConfigurator_Sequence_View) {
		SmartTaskConfigurator_Sequence_View = smartTaskConfigurator_Sequence_View;
	}
	public  String getSmartTaskConfigurator_Smart_Task_Congigurator_Maintenance() {
		return SmartTaskConfigurator_Smart_Task_Congigurator_Maintenance;
	}
	public  void setSmartTaskConfigurator_Smart_Task_Congigurator_Maintenance(
			String smartTaskConfigurator_Smart_Task_Congigurator_Maintenance) {
		SmartTaskConfigurator_Smart_Task_Congigurator_Maintenance = smartTaskConfigurator_Smart_Task_Congigurator_Maintenance;
	}
	public  String getSmartTaskConfigurator_Search_All() {
		return SmartTaskConfigurator_Search_All;
	}
	public  void setSmartTaskConfigurator_Search_All(
			String smartTaskConfigurator_Search_All) {
		SmartTaskConfigurator_Search_All = smartTaskConfigurator_Search_All;
	}
	public  String getSmartTaskConfigurator_Company() {
		return SmartTaskConfigurator_Company;
	}
	public  void setSmartTaskConfigurator_Company(
			String smartTaskConfigurator_Company) {
		SmartTaskConfigurator_Company = smartTaskConfigurator_Company;
	}
	public  String getSmartTaskConfigurator_Group() {
		return SmartTaskConfigurator_Group;
	}
	public  void setSmartTaskConfigurator_Group(
			String smartTaskConfigurator_Group) {
		SmartTaskConfigurator_Group = smartTaskConfigurator_Group;
	}
	public  String getSmartTaskConfigurator_Type() {
		return SmartTaskConfigurator_Type;
	}
	public  void setSmartTaskConfigurator_Type(
			String smartTaskConfigurator_Type) {
		SmartTaskConfigurator_Type = smartTaskConfigurator_Type;
	}
	public  String getSmartTaskConfigurator_Device() {
		return SmartTaskConfigurator_Device;
	}
	public  void setSmartTaskConfigurator_Device(
			String smartTaskConfigurator_Device) {
		SmartTaskConfigurator_Device = smartTaskConfigurator_Device;
	}
	public  String getSmartTaskConfigurator_Sequence() {
		return SmartTaskConfigurator_Sequence;
	}
	public  void setSmartTaskConfigurator_Sequence(
			String smartTaskConfigurator_Sequence) {
		SmartTaskConfigurator_Sequence = smartTaskConfigurator_Sequence;
	}
	public  String getSmartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted() {
		return SmartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted;
	}
	public  void setSmartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted(
			String smartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted) {
		SmartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted = smartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted;
	}
	public  String getSmartTaskConfigurator_Invalid_Task() {
		return SmartTaskConfigurator_Invalid_Task;
	}
	public  void setSmartTaskConfigurator_Invalid_Task(
			String smartTaskConfigurator_Invalid_Task) {
		SmartTaskConfigurator_Invalid_Task = smartTaskConfigurator_Invalid_Task;
	}
	public  String getSmartTaskConfigurator_Select() {
		return SmartTaskConfigurator_Select;
	}
	public  void setSmartTaskConfigurator_Select(
			String smartTaskConfigurator_Select) {
		SmartTaskConfigurator_Select = smartTaskConfigurator_Select;
	}
	public  String getSmartTaskConfigurator_Task() {
		return SmartTaskConfigurator_Task;
	}
	public  void setSmartTaskConfigurator_Task(
			String smartTaskConfigurator_Task) {
		SmartTaskConfigurator_Task = smartTaskConfigurator_Task;
	}
	public  String getSmartTaskConfigurator_Application() {
		return SmartTaskConfigurator_Application;
	}
	public  void setSmartTaskConfigurator_Application(
			String smartTaskConfigurator_Application) {
		SmartTaskConfigurator_Application = smartTaskConfigurator_Application;
	}
	public  String getSmartTaskConfigurator_Execution_Group() {
		return SmartTaskConfigurator_Execution_Group;
	}
	public  void setSmartTaskConfigurator_Execution_Group(
			String smartTaskConfigurator_Execution_Group) {
		SmartTaskConfigurator_Execution_Group = smartTaskConfigurator_Execution_Group;
	}
	public  String getSmartTaskConfigurator_Execution_Type() {
		return SmartTaskConfigurator_Execution_Type;
	}
	public  void setSmartTaskConfigurator_Execution_Type(
			String smartTaskConfigurator_Execution_Type) {
		SmartTaskConfigurator_Execution_Type = smartTaskConfigurator_Execution_Type;
	}
	public  String getSmartTaskConfigurator_Execution_Device() {
		return SmartTaskConfigurator_Execution_Device;
	}
	public  void setSmartTaskConfigurator_Execution_Device(
			String smartTaskConfigurator_Execution_Device) {
		SmartTaskConfigurator_Execution_Device = smartTaskConfigurator_Execution_Device;
	}
	public  String getSmartTaskConfigurator_Execution_Sequence() {
		return SmartTaskConfigurator_Execution_Sequence;
	}
	public  void setSmartTaskConfigurator_Execution_Sequence(
			String smartTaskConfigurator_Execution_Sequence) {
		SmartTaskConfigurator_Execution_Sequence = smartTaskConfigurator_Execution_Sequence;
	}
	public  String getSmartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description() {
		return SmartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description;
	}
	public  void setSmartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description(
			String smartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description) {
		SmartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description = smartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description;
	}
	public  String getSmartTaskConfigurator_Tenant() {
		return SmartTaskConfigurator_Tenant;
	}
	public  void setSmartTaskConfigurator_Tenant(
			String smartTaskConfigurator_Tenant) {
		SmartTaskConfigurator_Tenant = smartTaskConfigurator_Tenant;
	}
	public  String getSmartTaskConfigurator_Menu_Name() {
		return SmartTaskConfigurator_Menu_Name;
	}
	public  void setSmartTaskConfigurator_Menu_Name(
			String smartTaskConfigurator_Menu_Name) {
		SmartTaskConfigurator_Menu_Name = smartTaskConfigurator_Menu_Name;
	}
	public  String getSmartTaskConfigurator_New() {
		return SmartTaskConfigurator_New;
	}
	public  void setSmartTaskConfigurator_New(String smartTaskConfigurator_New) {
		SmartTaskConfigurator_New = smartTaskConfigurator_New;
	}
	public  String getSmartTaskConfigurator_Display_All() {
		return SmartTaskConfigurator_Display_All;
	}
	public  void setSmartTaskConfigurator_Display_All(
			String smartTaskConfigurator_Display_All) {
		SmartTaskConfigurator_Display_All = smartTaskConfigurator_Display_All;
	}
	public  String getSmartTaskConfigurator_Smart_Task_Configurator_Lookup() {
		return SmartTaskConfigurator_Smart_Task_Configurator_Lookup;
	}
	public  void setSmartTaskConfigurator_Smart_Task_Configurator_Lookup(
			String smartTaskConfigurator_Smart_Task_Configurator_Lookup) {
		SmartTaskConfigurator_Smart_Task_Configurator_Lookup = smartTaskConfigurator_Smart_Task_Configurator_Lookup;
	}
	public  String getSmartTaskConfigurator_Please_select_Application() {
		return SmartTaskConfigurator_Please_select_Application;
	}
	public  void setSmartTaskConfigurator_Please_select_Application(
			String smartTaskConfigurator_Please_select_Application) {
		SmartTaskConfigurator_Please_select_Application = smartTaskConfigurator_Please_select_Application;
	}
	public  String getSmartTaskConfigurator_Please_select_Execution_Group() {
		return SmartTaskConfigurator_Please_select_Execution_Group;
	}
	public  void setSmartTaskConfigurator_Please_select_Execution_Group(
			String smartTaskConfigurator_Please_select_Execution_Group) {
		SmartTaskConfigurator_Please_select_Execution_Group = smartTaskConfigurator_Please_select_Execution_Group;
	}
	public  String getSmartTaskConfigurator_Please_select_Task() {
		return SmartTaskConfigurator_Please_select_Task;
	}
	public  void setSmartTaskConfigurator_Please_select_Task(
			String smartTaskConfigurator_Please_select_Task) {
		SmartTaskConfigurator_Please_select_Task = smartTaskConfigurator_Please_select_Task;
	}
	public  String getSmartTaskConfigurator_Please_select_Execution_Type() {
		return SmartTaskConfigurator_Please_select_Execution_Type;
	}
	public  void setSmartTaskConfigurator_Please_select_Execution_Type(
			String smartTaskConfigurator_Please_select_Execution_Type) {
		SmartTaskConfigurator_Please_select_Execution_Type = smartTaskConfigurator_Please_select_Execution_Type;
	}
	public  String getSmartTaskConfigurator_Please_select_Execution_Device() {
		return SmartTaskConfigurator_Please_select_Execution_Device;
	}
	public  void setSmartTaskConfigurator_Please_select_Execution_Device(
			String smartTaskConfigurator_Please_select_Execution_Device) {
		SmartTaskConfigurator_Please_select_Execution_Device = smartTaskConfigurator_Please_select_Execution_Device;
	}
	public  String getSmartTaskConfigurator_Please_enter_Execution_Sequence() {
		return SmartTaskConfigurator_Please_enter_Execution_Sequence;
	}
	public  void setSmartTaskConfigurator_Please_enter_Execution_Sequence(
			String smartTaskConfigurator_Please_enter_Execution_Sequence) {
		SmartTaskConfigurator_Please_enter_Execution_Sequence = smartTaskConfigurator_Please_enter_Execution_Sequence;
	}
	public  String getSmartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description() {
		return SmartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description;
	}
	public  void setSmartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description(
			String smartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description) {
		SmartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description = smartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description;
	}
	public  String getSmartTaskConfigurator_Please_enter_Tenant() {
		return SmartTaskConfigurator_Please_enter_Tenant;
	}
	public  void setSmartTaskConfigurator_Please_enter_Tenant(
			String smartTaskConfigurator_Please_enter_Tenant) {
		SmartTaskConfigurator_Please_enter_Tenant = smartTaskConfigurator_Please_enter_Tenant;
	}
	public  String getSmartTaskConfigurator_Please_enter_Menu_Name() {
		return SmartTaskConfigurator_Please_enter_Menu_Name;
	}
	public  void setSmartTaskConfigurator_Please_enter_Menu_Name(
			String smartTaskConfigurator_Please_enter_Menu_Name) {
		SmartTaskConfigurator_Please_enter_Menu_Name = smartTaskConfigurator_Please_enter_Menu_Name;
	}
	public  String getSmartTaskConfigurator_Invalid_Application() {
		return SmartTaskConfigurator_Invalid_Application;
	}
	public  void setSmartTaskConfigurator_Invalid_Application(
			String smartTaskConfigurator_Invalid_Application) {
		SmartTaskConfigurator_Invalid_Application = smartTaskConfigurator_Invalid_Application;
	}
	public  String getSmartTaskConfigurator_Invalid_Execution_Group() {
		return SmartTaskConfigurator_Invalid_Execution_Group;
	}
	public  void setSmartTaskConfigurator_Invalid_Execution_Group(
			String smartTaskConfigurator_Invalid_Execution_Group) {
		SmartTaskConfigurator_Invalid_Execution_Group = smartTaskConfigurator_Invalid_Execution_Group;
	}
	public  String getSmartTaskConfigurator_Invalid_Execution_Type() {
		return SmartTaskConfigurator_Invalid_Execution_Type;
	}
	public  void setSmartTaskConfigurator_Invalid_Execution_Type(
			String smartTaskConfigurator_Invalid_Execution_Type) {
		SmartTaskConfigurator_Invalid_Execution_Type = smartTaskConfigurator_Invalid_Execution_Type;
	}
	public  String getSmartTaskConfigurator_Invalid_Execution_Device() {
		return SmartTaskConfigurator_Invalid_Execution_Device;
	}
	public  void setSmartTaskConfigurator_Invalid_Execution_Device(
			String smartTaskConfigurator_Invalid_Execution_Device) {
		SmartTaskConfigurator_Invalid_Execution_Device = smartTaskConfigurator_Invalid_Execution_Device;
	}
	public  String getSmartTaskConfigurator_Invalid_Execution_Sequence() {
		return SmartTaskConfigurator_Invalid_Execution_Sequence;
	}
	public  void setSmartTaskConfigurator_Invalid_Execution_Sequence(
			String smartTaskConfigurator_Invalid_Execution_Sequence) {
		SmartTaskConfigurator_Invalid_Execution_Sequence = smartTaskConfigurator_Invalid_Execution_Sequence;
	}
	public  String getSmartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description() {
		return SmartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description;
	}
	public  void setSmartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description(
			String smartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description) {
		SmartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description = smartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description;
	}
	public  String getSmartTaskConfigurator_Invalid_Tenant() {
		return SmartTaskConfigurator_Invalid_Tenant;
	}
	public  void setSmartTaskConfigurator_Invalid_Tenant(
			String smartTaskConfigurator_Invalid_Tenant) {
		SmartTaskConfigurator_Invalid_Tenant = smartTaskConfigurator_Invalid_Tenant;
	}
	public  String getSmartTaskConfigurator_Invalid_Menu_Name() {
		return SmartTaskConfigurator_Invalid_Menu_Name;
	}
	public  void setSmartTaskConfigurator_Invalid_Menu_Name(
			String smartTaskConfigurator_Invalid_Menu_Name) {
		SmartTaskConfigurator_Invalid_Menu_Name = smartTaskConfigurator_Invalid_Menu_Name;
	}
	public  String getSmartTaskConfigurator_Smart_Task_Configurator_Search_Lookup() {
		return SmartTaskConfigurator_Smart_Task_Configurator_Search_Lookup;
	}
	public  void setSmartTaskConfigurator_Smart_Task_Configurator_Search_Lookup(
			String smartTaskConfigurator_Smart_Task_Configurator_Search_Lookup) {
		SmartTaskConfigurator_Smart_Task_Configurator_Search_Lookup = smartTaskConfigurator_Smart_Task_Configurator_Search_Lookup;
	}
	public  String getSmartTaskConfigurator_Add_New() {
		return SmartTaskConfigurator_Add_New;
	}
	public  void setSmartTaskConfigurator_Add_New(
			String smartTaskConfigurator_Add_New) {
		SmartTaskConfigurator_Add_New = smartTaskConfigurator_Add_New;
	}
	public  String getSmartTaskConfigurator_Edit() {
		return SmartTaskConfigurator_Edit;
	}
	public  void setSmartTaskConfigurator_Edit(
			String smartTaskConfigurator_Edit) {
		SmartTaskConfigurator_Edit = smartTaskConfigurator_Edit;
	}
	public  String getSmartTaskConfigurator_Notes() {
		return SmartTaskConfigurator_Notes;
	}
	public  void setSmartTaskConfigurator_Notes(
			String smartTaskConfigurator_Notes) {
		SmartTaskConfigurator_Notes = smartTaskConfigurator_Notes;
	}
	public  String getSmartTaskConfigurator_Delete() {
		return SmartTaskConfigurator_Delete;
	}
	public  void setSmartTaskConfigurator_Delete(
			String smartTaskConfigurator_Delete) {
		SmartTaskConfigurator_Delete = smartTaskConfigurator_Delete;
	}
	public  String getSmartTaskConfigurator_Actions() {
		return SmartTaskConfigurator_Actions;
	}
	public  void setSmartTaskConfigurator_Actions(
			String smartTaskConfigurator_Actions) {
		SmartTaskConfigurator_Actions = smartTaskConfigurator_Actions;
	}
	public  String getSmartTaskConfigurator_Description() {
		return SmartTaskConfigurator_Description;
	}
	public  void setSmartTaskConfigurator_Description(
			String smartTaskConfigurator_Description) {
		SmartTaskConfigurator_Description = smartTaskConfigurator_Description;
	}
	public  String getSmartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record() {
		return SmartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record;
	}
	public  void setSmartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record(
			String smartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record) {
		SmartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record = smartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record;
	}
	public  String getSmartTaskConfigurator_Smart_Task_Configurator_Maintenance() {
		return SmartTaskConfigurator_Smart_Task_Configurator_Maintenance;
	}
	public  void setSmartTaskConfigurator_Smart_Task_Configurator_Maintenance(
			String smartTaskConfigurator_Smart_Task_Configurator_Maintenance) {
		SmartTaskConfigurator_Smart_Task_Configurator_Maintenance = smartTaskConfigurator_Smart_Task_Configurator_Maintenance;
	}
	public  String getSmartTaskConfigurator_Last_Activity_Date() {
		return SmartTaskConfigurator_Last_Activity_Date;
	}
	public  void setSmartTaskConfigurator_Last_Activity_Date(
			String smartTaskConfigurator_Last_Activity_Date) {
		SmartTaskConfigurator_Last_Activity_Date = smartTaskConfigurator_Last_Activity_Date;
	}
	public  String getSmartTaskConfigurator_Last_Activity_By() {
		return SmartTaskConfigurator_Last_Activity_By;
	}
	public  void setSmartTaskConfigurator_Last_Activity_By(
			String smartTaskConfigurator_Last_Activity_By) {
		SmartTaskConfigurator_Last_Activity_By = smartTaskConfigurator_Last_Activity_By;
	}
	public  String getSmartTaskConfigurator_Description_Short() {
		return SmartTaskConfigurator_Description_Short;
	}
	public  void setSmartTaskConfigurator_Description_Short(
			String smartTaskConfigurator_Description_Short) {
		SmartTaskConfigurator_Description_Short = smartTaskConfigurator_Description_Short;
	}
	public  String getSmartTaskConfigurator_Description_Long() {
		return SmartTaskConfigurator_Description_Long;
	}
	public  void setSmartTaskConfigurator_Description_Long(
			String smartTaskConfigurator_Description_Long) {
		SmartTaskConfigurator_Description_Long = smartTaskConfigurator_Description_Long;
	}
	public  String getSmartTaskConfigurator_Help_Line() {
		return SmartTaskConfigurator_Help_Line;
	}
	public  void setSmartTaskConfigurator_Help_Line(
			String smartTaskConfigurator_Help_Line) {
		SmartTaskConfigurator_Help_Line = smartTaskConfigurator_Help_Line;
	}
	public  String getSmartTaskConfigurator_Save_Update() {
		return SmartTaskConfigurator_Save_Update;
	}
	public  void setSmartTaskConfigurator_Save_Update(
			String smartTaskConfigurator_Save_Update) {
		SmartTaskConfigurator_Save_Update = smartTaskConfigurator_Save_Update;
	}
	public  String getSmartTaskConfigurator_Reset() {
		return SmartTaskConfigurator_Reset;
	}
	public  void setSmartTaskConfigurator_Reset(
			String smartTaskConfigurator_Reset) {
		SmartTaskConfigurator_Reset = smartTaskConfigurator_Reset;
	}
	public  String getSmartTaskConfigurator_Cancel() {
		return SmartTaskConfigurator_Cancel;
	}
	public  void setSmartTaskConfigurator_Cancel(
			String smartTaskConfigurator_Cancel) {
		SmartTaskConfigurator_Cancel = smartTaskConfigurator_Cancel;
	}
	public  String getSmartTaskConfigurator_Your_record_has_been_saved_successfully() {
		return SmartTaskConfigurator_Your_record_has_been_saved_successfully;
	}
	public  void setSmartTaskConfigurator_Your_record_has_been_saved_successfully(
			String smartTaskConfigurator_Your_record_has_been_saved_successfully) {
		SmartTaskConfigurator_Your_record_has_been_saved_successfully = smartTaskConfigurator_Your_record_has_been_saved_successfully;
	}
	public  String getSmartTaskConfigurator_Your_record_has_been_updated_successfully() {
		return SmartTaskConfigurator_Your_record_has_been_updated_successfully;
	}
	public  void setSmartTaskConfigurator_Your_record_has_been_updated_successfully(
			String smartTaskConfigurator_Your_record_has_been_updated_successfully) {
		SmartTaskConfigurator_Your_record_has_been_updated_successfully = smartTaskConfigurator_Your_record_has_been_updated_successfully;
	}
	public String getSmartTaskConfigurator_mandatory_field_can_not_be_left_blank() {
		return SmartTaskConfigurator_mandatory_field_can_not_be_left_blank;
	}
	public void setSmartTaskConfigurator_mandatory_field_can_not_be_left_blank(
			String smartTaskConfigurator_mandatory_field_can_not_be_left_blank) {
		SmartTaskConfigurator_mandatory_field_can_not_be_left_blank = smartTaskConfigurator_mandatory_field_can_not_be_left_blank;
	}
	
	public  String getSmartTaskConfigurator_Please_select_GoTo_Tag_first() {
		return SmartTaskConfigurator_Please_select_GoTo_Tag_first;
	}
	public  void setSmartTaskConfigurator_Please_select_GoTo_Tag_first(
			String smartTaskConfigurator_Please_select_GoTo_Tag_first) {
		SmartTaskConfigurator_Please_select_GoTo_Tag_first = smartTaskConfigurator_Please_select_GoTo_Tag_first;
	}
	public  String getSmartTaskConfigurator_All_fields_are_mandatory() {
		return SmartTaskConfigurator_All_fields_are_mandatory;
	}
	public  void setSmartTaskConfigurator_All_fields_are_mandatory(
			String smartTaskConfigurator_All_fields_are_mandatory) {
		SmartTaskConfigurator_All_fields_are_mandatory = smartTaskConfigurator_All_fields_are_mandatory;
	}
	public  String getSmartTaskConfigurator_Please_enter_display_text() {
		return SmartTaskConfigurator_Please_enter_display_text;
	}
	public  void setSmartTaskConfigurator_Please_enter_display_text(
			String smartTaskConfigurator_Please_enter_display_text) {
		SmartTaskConfigurator_Please_enter_display_text = smartTaskConfigurator_Please_enter_display_text;
	}
	public  String getSmartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel() {
		return SmartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel;
	}
	public  void setSmartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel(
			String smartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel) {
		SmartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel = smartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel;
	}
	public  String getSmartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link() {
		return SmartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link;
	}
	public  void setSmartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link(
			String smartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link) {
		SmartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link = smartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link;
	}
	public  String getSmartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one() {
		return SmartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one;
	}
	public  void setSmartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one(
			String smartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one) {
		SmartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one = smartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one;
	}
	public  String getSmartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking() {
		return SmartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking;
	}
	public  void setSmartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking(
			String smartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking) {
		SmartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking = smartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking;
	}
	public  String getSmartTaskConfigurator_General_Tag_already_exist_with() {
		return SmartTaskConfigurator_General_Tag_already_exist_with;
	}
	public  void setSmartTaskConfigurator_General_Tag_already_exist_with(
			String smartTaskConfigurator_General_Tag_already_exist_with) {
		SmartTaskConfigurator_General_Tag_already_exist_with = smartTaskConfigurator_General_Tag_already_exist_with;
	}
	public  String getSmartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name() {
		return SmartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name;
	}
	public  void setSmartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name(
			String smartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name) {
		SmartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name = smartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name;
	}
	public  String getSmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid() {
		return SmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid;
	}
	public  void setSmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid(
			String smartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid) {
		SmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid = smartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid;
	}
	
	
	public String getSmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid() {
		return SmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid;
	}
	public void setSmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid(
			String smartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid) {
		SmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid = smartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid;
	}
	public  String getSmartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid() {
		return SmartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid;
	}
	public  void setSmartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid(
			String smartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid) {
		SmartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid = smartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid;
	}
	public  String getSmartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data() {
		return SmartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data;
	}
	public  void setSmartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data(
			String smartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data) {
		SmartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data = smartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data;
	}
	public  String getSmartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel() {
		return SmartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel;
	}
	public  void setSmartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel(
			String smartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel) {
		SmartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel = smartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel;
	}
	
}
