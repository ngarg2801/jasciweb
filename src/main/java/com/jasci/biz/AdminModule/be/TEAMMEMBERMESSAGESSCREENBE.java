/*
 
Date Developed  Nov 16 2014
Created By "Diksha Gupta"
Description It is used to create getter and setter to shoe=w screen labels getting values from database.
*/

package com.jasci.biz.AdminModule.be;

public class TEAMMEMBERMESSAGESSCREENBE
{
	

	
	
	private String TeamMemberMessages_EnterTeamMember;
	private String TeamMemberMessages_PartOfTeamMember;
	private String TeamMemberMessages_Department;
	private String TeamMemberMessages_Shift;
	private String TeamMemberMessages_Search;
	private String TeamMemberMessages_DisplayAll;
	private String TeamMemberMessages_TeamMemberMaintenance;
	private String TeamMemberMessages_StartDate;
	private String TeamMemberMessages_EndDate;
	private String TeamMemberMessages_TicketTape;
	private String TeamMemberMessages_Message1;
	private String TeamMemberMessages_Message2;
	private String TeamMemberMessages_Message3;
	private String TeamMemberMessages_Message4;
	private String TeamMemberMessages_Message5;
	private String TeamMemberMessages_SaveUpdate;
	private String TeamMemberMessages_Name;
	private String TeamMemberMessages_SortFirstName;
	private String TeamMemberMessages_SortLastName;
	private String TeamMemberMessages_MessageAllSelected;
	private String TeamMemberMessages_Type;
	private String TeamMemberMessages_Message;
	private String TeamMemberMessages_Actions;
	private String TeamMemberMessages_Edit;
	private String TeamMemberMessages_Delete;
    private   String TeamMemberMessages_InvalidTM;
	  private   String TeamMemberMessages_InvalidPartOfTm;
	  private   String TeamMemberMessages_InvalidDepartment;
	  private   String TeamMemberMessages_InvalidShift;
	  private   String TeamMemberMessages_NoRecordFound;
	  private   String TeamMemberMessages_SaveMessage;
	  private   String TeamMemberMessages_UpdateMessage;
      private   String TeamMemberMessages_Cancel;
	  private   String TeamMemberMessages_BlankTicketTapeMessage;
	  private   String TeamMemberMessages_MandatoryFieldMessage;
	  private   String TeamMemberMessages_BlankEndDateMessage;
	  private   String TeamMemberMessages_InvalidDateMessage;
	  private String TeamMemberMessages_BlankTeamMember;
	  private String TeamMemberMessages_BlankPartTeamMember;
	  private String TeamMemberMessages_BlankDepartment;
	  private String TeamMemberMessages_BlankShift;
	  private   String TeamMemberMessages_DeleteMessage;
	  private String TeamMemberMessages_SearchLookup;
	  private String TeamMemberMessages_ActiveTMM; // active team member messages
	 private String TeamMemberMessages_Company; // company
	 private String TeamMemberMessages_Teammember;// Team member
	 private String TeamMemberMessages_Addnew; //add new
	 private String TeamMemberMessages_TMMLookUp;// team member messages lookup
	 private String TeamMemberMessages_TMLookUp;
	 
	 private String TeamMemberMessages_TMMMaintenance; //Menu Messages Maintenance New/Edit
	 private String TeamMemberMessages_StartDate_YY;
		private String TeamMemberMessages_EndDate_YY;
		private String TeamMemberMessages_Select;
		private String TeamMemberMessages_MessageALLError;
		private String TeamMemberMessages_Reset;
	
	public String getTeamMemberMessages_Reset() {
			return TeamMemberMessages_Reset;
		}
		public void setTeamMemberMessages_Reset(String teamMemberMessages_Reset) {
			TeamMemberMessages_Reset = teamMemberMessages_Reset;
		}
		
		
	public String getTeamMemberMessages_Select() {
			return TeamMemberMessages_Select;
		}
		public void setTeamMemberMessages_Select(String teamMemberMessages_Select) {
			TeamMemberMessages_Select = teamMemberMessages_Select;
		}
		public String getTeamMemberMessages_MessageALLError() {
			return TeamMemberMessages_MessageALLError;
		}
		public void setTeamMemberMessages_MessageALLError(
				String teamMemberMessages_MessageALLError) {
			TeamMemberMessages_MessageALLError = teamMemberMessages_MessageALLError;
		}
	public String getTeamMemberMessages_StartDate_YY() {
			return TeamMemberMessages_StartDate_YY;
		}
		public void setTeamMemberMessages_StartDate_YY(
				String teamMemberMessages_StartDate_YY) {
			TeamMemberMessages_StartDate_YY = teamMemberMessages_StartDate_YY;
		}
		public String getTeamMemberMessages_EndDate_YY() {
			return TeamMemberMessages_EndDate_YY;
		}
		public void setTeamMemberMessages_EndDate_YY(
				String teamMemberMessages_EndDate_YY) {
			TeamMemberMessages_EndDate_YY = teamMemberMessages_EndDate_YY;
		}
	public String getTeamMemberMessages_TMMMaintenance() {
		return TeamMemberMessages_TMMMaintenance;
	}
	public void setTeamMemberMessages_TMMMaintenance(
			String teamMemberMessages_TMMMaintenance) {
		TeamMemberMessages_TMMMaintenance = teamMemberMessages_TMMMaintenance;
	}
	public String getTeamMemberMessages_TMLookUp() {
		return TeamMemberMessages_TMLookUp;
	}
	public void setTeamMemberMessages_TMLookUp(String teamMemberMessages_TMLookUp) {
		TeamMemberMessages_TMLookUp = teamMemberMessages_TMLookUp;
	}
	public String getTeamMemberMessages_TMMLookUp() {
		return TeamMemberMessages_TMMLookUp;
	}
	public void setTeamMemberMessages_TMMLookUp(String teamMemberMessages_TMMLookUp) {
		TeamMemberMessages_TMMLookUp = teamMemberMessages_TMMLookUp;
	}
	public String getTeamMemberMessages_Addnew() {
		return TeamMemberMessages_Addnew;
	}
	public void setTeamMemberMessages_Addnew(String teamMemberMessages_Addnew) {
		TeamMemberMessages_Addnew = teamMemberMessages_Addnew;
	}
	public String getTeamMemberMessages_Company() {
		return TeamMemberMessages_Company;
	}
	public void setTeamMemberMessages_Company(String teamMemberMessages_Company) {
		TeamMemberMessages_Company = teamMemberMessages_Company;
	}
	public String getTeamMemberMessages_Teammember() {
		return TeamMemberMessages_Teammember;
	}
	public void setTeamMemberMessages_Teammember(
			String teamMemberMessages_Teammember) {
		TeamMemberMessages_Teammember = teamMemberMessages_Teammember;
	}
	public String getTeamMemberMessages_ActiveTMM() {
		return TeamMemberMessages_ActiveTMM;
	}
	public void setTeamMemberMessages_ActiveTMM(String teamMemberMessages_ActiveTMM) {
		TeamMemberMessages_ActiveTMM = teamMemberMessages_ActiveTMM;
	}
	public String getTeamMemberMessages_SearchLookup() {
		return TeamMemberMessages_SearchLookup;
	}
	public void setTeamMemberMessages_SearchLookup(
			String teamMemberMessages_SearchLookup) {
		TeamMemberMessages_SearchLookup = teamMemberMessages_SearchLookup;
	}
	public String getTeamMemberMessages_InvalidTM() {
		return TeamMemberMessages_InvalidTM;
	}
	public void setTeamMemberMessages_InvalidTM(String teamMemberMessages_InvalidTM) {
		TeamMemberMessages_InvalidTM = teamMemberMessages_InvalidTM;
	}
	public String getTeamMemberMessages_InvalidPartOfTm() {
		return TeamMemberMessages_InvalidPartOfTm;
	}
	public void setTeamMemberMessages_InvalidPartOfTm(
			String teamMemberMessages_InvalidPartOfTm) {
		TeamMemberMessages_InvalidPartOfTm = teamMemberMessages_InvalidPartOfTm;
	}
	public String getTeamMemberMessages_InvalidDepartment() {
		return TeamMemberMessages_InvalidDepartment;
	}
	public void setTeamMemberMessages_InvalidDepartment(
			String teamMemberMessages_InvalidDepartment) {
		TeamMemberMessages_InvalidDepartment = teamMemberMessages_InvalidDepartment;
	}
	public String getTeamMemberMessages_InvalidShift() {
		return TeamMemberMessages_InvalidShift;
	}
	public void setTeamMemberMessages_InvalidShift(
			String teamMemberMessages_InvalidShift) {
		TeamMemberMessages_InvalidShift = teamMemberMessages_InvalidShift;
	}
	public String getTeamMemberMessages_NoRecordFound() {
		return TeamMemberMessages_NoRecordFound;
	}
	public void setTeamMemberMessages_NoRecordFound(
			String teamMemberMessages_NoRecordFound) {
		TeamMemberMessages_NoRecordFound = teamMemberMessages_NoRecordFound;
	}
	public String getTeamMemberMessages_SaveMessage() {
		return TeamMemberMessages_SaveMessage;
	}
	public void setTeamMemberMessages_SaveMessage(
			String teamMemberMessages_SaveMessage) {
		TeamMemberMessages_SaveMessage = teamMemberMessages_SaveMessage;
	}
	public String getTeamMemberMessages_UpdateMessage() {
		return TeamMemberMessages_UpdateMessage;
	}
	public void setTeamMemberMessages_UpdateMessage(
			String teamMemberMessages_UpdateMessage) {
		TeamMemberMessages_UpdateMessage = teamMemberMessages_UpdateMessage;
	}
	public String getTeamMemberMessages_EnterTeamMember() {
		return TeamMemberMessages_EnterTeamMember;
	}
	public void setTeamMemberMessages_EnterTeamMember(
			String teamMemberMessages_EnterTeamMember) {
		TeamMemberMessages_EnterTeamMember = teamMemberMessages_EnterTeamMember;
	}
	public String getTeamMemberMessages_PartOfTeamMember() {
		return TeamMemberMessages_PartOfTeamMember;
	}
	public void setTeamMemberMessages_PartOfTeamMember(
			String teamMemberMessages_PartOfTeamMember) {
		TeamMemberMessages_PartOfTeamMember = teamMemberMessages_PartOfTeamMember;
	}
	public String getTeamMemberMessages_Department() {
		return TeamMemberMessages_Department;
	}
	public void setTeamMemberMessages_Department(
			String teamMemberMessages_Department) {
		TeamMemberMessages_Department = teamMemberMessages_Department;
	}
	public String getTeamMemberMessages_Shift() {
		return TeamMemberMessages_Shift;
	}
	public void setTeamMemberMessages_Shift(String teamMemberMessages_Shift) {
		TeamMemberMessages_Shift = teamMemberMessages_Shift;
	}
	public String getTeamMemberMessages_Search() {
		return TeamMemberMessages_Search;
	}
	public void setTeamMemberMessages_Search(String teamMemberMessages_Search) {
		TeamMemberMessages_Search = teamMemberMessages_Search;
	}
	public String getTeamMemberMessages_DisplayAll() {
		return TeamMemberMessages_DisplayAll;
	}
	public void setTeamMemberMessages_DisplayAll(
			String teamMemberMessages_DisplayAll) {
		TeamMemberMessages_DisplayAll = teamMemberMessages_DisplayAll;
	}
	public String getTeamMemberMessages_TeamMemberMaintenance() {
		return TeamMemberMessages_TeamMemberMaintenance;
	}
	public void setTeamMemberMessages_TeamMemberMaintenance(
			String teamMemberMessages_TeamMemberMaintenance) {
		TeamMemberMessages_TeamMemberMaintenance = teamMemberMessages_TeamMemberMaintenance;
	}
	public String getTeamMemberMessages_StartDate() {
		return TeamMemberMessages_StartDate;
	}
	public void setTeamMemberMessages_StartDate(String teamMemberMessages_StartDate) {
		TeamMemberMessages_StartDate = teamMemberMessages_StartDate;
	}
	public String getTeamMemberMessages_EndDate() {
		return TeamMemberMessages_EndDate;
	}
	public void setTeamMemberMessages_EndDate(String teamMemberMessages_EndDate) {
		TeamMemberMessages_EndDate = teamMemberMessages_EndDate;
	}
	public String getTeamMemberMessages_TicketTape() {
		return TeamMemberMessages_TicketTape;
	}
	public void setTeamMemberMessages_TicketTape(
			String teamMemberMessages_TicketTape) {
		TeamMemberMessages_TicketTape = teamMemberMessages_TicketTape;
	}
	public String getTeamMemberMessages_Message1() {
		return TeamMemberMessages_Message1;
	}
	public void setTeamMemberMessages_Message1(String teamMemberMessages_Message1) {
		TeamMemberMessages_Message1 = teamMemberMessages_Message1;
	}
	public String getTeamMemberMessages_Message2() {
		return TeamMemberMessages_Message2;
	}
	public void setTeamMemberMessages_Message2(String teamMemberMessages_Message2) {
		TeamMemberMessages_Message2 = teamMemberMessages_Message2;
	}
	public String getTeamMemberMessages_Message3() {
		return TeamMemberMessages_Message3;
	}
	public void setTeamMemberMessages_Message3(String teamMemberMessages_Message3) {
		TeamMemberMessages_Message3 = teamMemberMessages_Message3;
	}
	public String getTeamMemberMessages_Message4() {
		return TeamMemberMessages_Message4;
	}
	public void setTeamMemberMessages_Message4(String teamMemberMessages_Message4) {
		TeamMemberMessages_Message4 = teamMemberMessages_Message4;
	}
	public String getTeamMemberMessages_Message5() {
		return TeamMemberMessages_Message5;
	}
	public void setTeamMemberMessages_Message5(String teamMemberMessages_Message5) {
		TeamMemberMessages_Message5 = teamMemberMessages_Message5;
	}
	public String getTeamMemberMessages_SaveUpdate() {
		return TeamMemberMessages_SaveUpdate;
	}
	public void setTeamMemberMessages_SaveUpdate(
			String teamMemberMessages_SaveUpdate) {
		TeamMemberMessages_SaveUpdate = teamMemberMessages_SaveUpdate;
	}
	
	
	public String getTeamMemberMessages_Cancel() {
		return TeamMemberMessages_Cancel;
	}
	public void setTeamMemberMessages_Cancel(String teamMemberMessages_Cancel) {
		TeamMemberMessages_Cancel = teamMemberMessages_Cancel;
	}
	public String getTeamMemberMessages_Name() {
		return TeamMemberMessages_Name;
	}
	public void setTeamMemberMessages_Name(String teamMemberMessages_Name) {
		TeamMemberMessages_Name = teamMemberMessages_Name;
	}
	public String getTeamMemberMessages_SortFirstName() {
		return TeamMemberMessages_SortFirstName;
	}
	public void setTeamMemberMessages_SortFirstName(
			String teamMemberMessages_SortFirstName) {
		TeamMemberMessages_SortFirstName = teamMemberMessages_SortFirstName;
	}
	public String getTeamMemberMessages_SortLastName() {
		return TeamMemberMessages_SortLastName;
	}
	public void setTeamMemberMessages_SortLastName(
			String teamMemberMessages_SortLastName) {
		TeamMemberMessages_SortLastName = teamMemberMessages_SortLastName;
	}
	public String getTeamMemberMessages_MessageAllSelected() {
		return TeamMemberMessages_MessageAllSelected;
	}
	public void setTeamMemberMessages_MessageAllSelected(
			String teamMemberMessages_MessageAllSelected) {
		TeamMemberMessages_MessageAllSelected = teamMemberMessages_MessageAllSelected;
	}
	public String getTeamMemberMessages_Type() {
		return TeamMemberMessages_Type;
	}
	public void setTeamMemberMessages_Type(String teamMemberMessages_Type) {
		TeamMemberMessages_Type = teamMemberMessages_Type;
	}
	public String getTeamMemberMessages_Message() {
		return TeamMemberMessages_Message;
	}
	public void setTeamMemberMessages_Message(String teamMemberMessages_Message) {
		TeamMemberMessages_Message = teamMemberMessages_Message;
	}
	public String getTeamMemberMessages_Actions() {
		return TeamMemberMessages_Actions;
	}
	public void setTeamMemberMessages_Actions(String teamMemberMessages_Actions) {
		TeamMemberMessages_Actions = teamMemberMessages_Actions;
	}
	public String getTeamMemberMessages_Edit() {
		return TeamMemberMessages_Edit;
	}
	public void setTeamMemberMessages_Edit(String teamMemberMessages_Edit) {
		TeamMemberMessages_Edit = teamMemberMessages_Edit;
	}
	public String getTeamMemberMessages_Delete() {
		return TeamMemberMessages_Delete;
	}
	public void setTeamMemberMessages_Delete(String teamMemberMessages_Delete) {
		TeamMemberMessages_Delete = teamMemberMessages_Delete;
	}
	public String getTeamMemberMessages_BlankTicketTapeMessage() {
		return TeamMemberMessages_BlankTicketTapeMessage;
	}
	public void setTeamMemberMessages_BlankTicketTapeMessage(
			String teamMemberMessages_BlankTicketTapeMessage) {
		TeamMemberMessages_BlankTicketTapeMessage = teamMemberMessages_BlankTicketTapeMessage;
	}
	public String getTeamMemberMessages_MandatoryFieldMessage() {
		return TeamMemberMessages_MandatoryFieldMessage;
	}
	public void setTeamMemberMessages_MandatoryFieldMessage(
			String teamMemberMessages_MandatoryFieldMessage) {
		TeamMemberMessages_MandatoryFieldMessage = teamMemberMessages_MandatoryFieldMessage;
	}
	public String getTeamMemberMessages_BlankEndDateMessage() {
		return TeamMemberMessages_BlankEndDateMessage;
	}
	public void setTeamMemberMessages_BlankEndDateMessage(
			String teamMemberMessages_BlankEndDateMessage) {
		TeamMemberMessages_BlankEndDateMessage = teamMemberMessages_BlankEndDateMessage;
	}
	public String getTeamMemberMessages_InvalidDateMessage() {
		return TeamMemberMessages_InvalidDateMessage;
	}
	public void setTeamMemberMessages_InvalidDateMessage(
			String teamMemberMessages_InvalidDateMessage) {
		TeamMemberMessages_InvalidDateMessage = teamMemberMessages_InvalidDateMessage;
	}
	public String getTeamMemberMessages_BlankTeamMember() {
		return TeamMemberMessages_BlankTeamMember;
	}
	public void setTeamMemberMessages_BlankTeamMember(
			String teamMemberMessages_BlankTeamMember) {
		TeamMemberMessages_BlankTeamMember = teamMemberMessages_BlankTeamMember;
	}
	public String getTeamMemberMessages_BlankPartTeamMember() {
		return TeamMemberMessages_BlankPartTeamMember;
	}
	public void setTeamMemberMessages_BlankPartTeamMember(
			String teamMemberMessages_BlankPartTeamMember) {
		TeamMemberMessages_BlankPartTeamMember = teamMemberMessages_BlankPartTeamMember;
	}
	public String getTeamMemberMessages_BlankDepartment() {
		return TeamMemberMessages_BlankDepartment;
	}
	public void setTeamMemberMessages_BlankDepartment(
			String teamMemberMessages_BlankDepartment) {
		TeamMemberMessages_BlankDepartment = teamMemberMessages_BlankDepartment;
	}
	public String getTeamMemberMessages_BlankShift() {
		return TeamMemberMessages_BlankShift;
	}
	public void setTeamMemberMessages_BlankShift(
			String teamMemberMessages_BlankShift) {
		TeamMemberMessages_BlankShift = teamMemberMessages_BlankShift;
	}
	public String getTeamMemberMessages_DeleteMessage() {
		return TeamMemberMessages_DeleteMessage;
	}
	public void setTeamMemberMessages_DeleteMessage(
			String teamMemberMessages_DeleteMessage) {
		TeamMemberMessages_DeleteMessage = teamMemberMessages_DeleteMessage;
	}  
}
