/*
Description It is used to make setter and getter for changing text of screen in another language
Created By Aakash Bishnoi
Created Date Dec 12 2014
 */
package com.jasci.biz.AdminModule.be;

public class MENUMESSAGEBE {
	private String MenuMessage_Menu_Messages_Search_Lookup;
	private String MenuMessage_Start_Date_YYMMDD;;
	private String MenuMessage_End_Date;
	private String MenuMessage_Type;
	private String MenuMessage_Message;
	private String MenuMessage_Actions;
	private String MenuMessage_Edit;
	private String MenuMessage_Delete;
	private String MenuMessage_Add_New;
	private String MenuMessage_ERR_Start_Date;
	private String MenuMessage_ERR_End_Date;
	private String MenuMessage_ERR_Confirm_Delete;
	private String MenuMessage_ERR_Atleast_One;
	private String MenuMessage_ERR_Message_Already_Used;
	private String MenuMessage_Mandatory_Fields;
	private String MenuMessage_Menu_Messages_Maintenance;
	private String MenuMessage_Ticket_Tape_Message;
	private String MenuMessage_Message1;	
	private String MenuMessage_Save_Update;
	private String MenuMessage_Companies;
	private String MenuMessage_Cancel;
	private String MenuMessage_End_Date_YYMMDD;
	private String MenuMessage_Start_Date;
	
	private String MenuMessage_Confirm_Save;
	private String MenuMessage_Confirm_Update;
	private String MenuMessage_Company_Name;
	private String MenuMessage_Company_ID;
	private String MenuMessage_ERR_SELECT_ONE_COMPANY;
	private String MenuMessage_Reset;
	 
	 public String getMenuMessage_Reset() {
	  return MenuMessage_Reset;
	 }
	 public void setMenuMessage_Reset(String menuMessage_Reset) {
	  MenuMessage_Reset = menuMessage_Reset;
	 }
	
	
	
	public String getMenuMessage_Confirm_Save() {
		return MenuMessage_Confirm_Save;
	}
	public void setMenuMessage_Confirm_Save(String menuMessage_Confirm_Save) {
		MenuMessage_Confirm_Save = menuMessage_Confirm_Save;
	}
	public String getMenuMessage_Confirm_Update() {
		return MenuMessage_Confirm_Update;
	}
	public void setMenuMessage_Confirm_Update(String menuMessage_Confirm_Update) {
		MenuMessage_Confirm_Update = menuMessage_Confirm_Update;
	}
	public String getMenuMessage_Company_Name() {
		return MenuMessage_Company_Name;
	}
	public void setMenuMessage_Company_Name(String menuMessage_Company_Name) {
		MenuMessage_Company_Name = menuMessage_Company_Name;
	}
	public String getMenuMessage_Company_ID() {
		return MenuMessage_Company_ID;
	}
	public void setMenuMessage_Company_ID(String menuMessage_Company_ID) {
		MenuMessage_Company_ID = menuMessage_Company_ID;
	}
	public String getMenuMessage_ERR_SELECT_ONE_COMPANY() {
		return MenuMessage_ERR_SELECT_ONE_COMPANY;
	}
	public void setMenuMessage_ERR_SELECT_ONE_COMPANY(
			String menuMessage_ERR_SELECT_ONE_COMPANY) {
		MenuMessage_ERR_SELECT_ONE_COMPANY = menuMessage_ERR_SELECT_ONE_COMPANY;
	}
	
	
	
	public String getMenuMessage_Menu_Messages_Search_Lookup() {
		return MenuMessage_Menu_Messages_Search_Lookup;
	}
	public void setMenuMessage_Menu_Messages_Search_Lookup(
			String menuMessage_Menu_Messages_Search_Lookup) {
		MenuMessage_Menu_Messages_Search_Lookup = menuMessage_Menu_Messages_Search_Lookup;
	}
	public String getMenuMessage_Start_Date_YYMMDD() {
		return MenuMessage_Start_Date_YYMMDD;
	}
	public void setMenuMessage_Start_Date_YYMMDD(
			String menuMessage_Start_Date_YYMMDD) {
		MenuMessage_Start_Date_YYMMDD = menuMessage_Start_Date_YYMMDD;
	}
	public String getMenuMessage_End_Date() {
		return MenuMessage_End_Date;
	}
	public void setMenuMessage_End_Date(String menuMessage_End_Date) {
		MenuMessage_End_Date = menuMessage_End_Date;
	}
	public String getMenuMessage_Type() {
		return MenuMessage_Type;
	}
	public void setMenuMessage_Type(String menuMessage_Type) {
		MenuMessage_Type = menuMessage_Type;
	}
	public String getMenuMessage_Message() {
		return MenuMessage_Message;
	}
	public void setMenuMessage_Message(String menuMessage_Message) {
		MenuMessage_Message = menuMessage_Message;
	}
	public String getMenuMessage_Actions() {
		return MenuMessage_Actions;
	}
	public void setMenuMessage_Actions(String menuMessage_Actions) {
		MenuMessage_Actions = menuMessage_Actions;
	}
	public String getMenuMessage_Edit() {
		return MenuMessage_Edit;
	}
	public void setMenuMessage_Edit(String menuMessage_Edit) {
		MenuMessage_Edit = menuMessage_Edit;
	}
	public String getMenuMessage_Delete() {
		return MenuMessage_Delete;
	}
	public void setMenuMessage_Delete(String menuMessage_Delete) {
		MenuMessage_Delete = menuMessage_Delete;
	}
	public String getMenuMessage_Add_New() {
		return MenuMessage_Add_New;
	}
	public void setMenuMessage_Add_New(String menuMessage_Add_New) {
		MenuMessage_Add_New = menuMessage_Add_New;
	}
	public String getMenuMessage_ERR_Start_Date() {
		return MenuMessage_ERR_Start_Date;
	}
	public void setMenuMessage_ERR_Start_Date(String menuMessage_ERR_Start_Date) {
		MenuMessage_ERR_Start_Date = menuMessage_ERR_Start_Date;
	}
	public String getMenuMessage_ERR_End_Date() {
		return MenuMessage_ERR_End_Date;
	}
	public void setMenuMessage_ERR_End_Date(String menuMessage_ERR_End_Date) {
		MenuMessage_ERR_End_Date = menuMessage_ERR_End_Date;
	}
	public String getMenuMessage_ERR_Confirm_Delete() {
		return MenuMessage_ERR_Confirm_Delete;
	}
	public void setMenuMessage_ERR_Confirm_Delete(
			String menuMessage_ERR_Confirm_Delete) {
		MenuMessage_ERR_Confirm_Delete = menuMessage_ERR_Confirm_Delete;
	}
	public String getMenuMessage_ERR_Atleast_One() {
		return MenuMessage_ERR_Atleast_One;
	}
	public void setMenuMessage_ERR_Atleast_One(String menuMessage_ERR_Atleast_One) {
		MenuMessage_ERR_Atleast_One = menuMessage_ERR_Atleast_One;
	}
	public String getMenuMessage_ERR_Message_Already_Used() {
		return MenuMessage_ERR_Message_Already_Used;
	}
	public void setMenuMessage_ERR_Message_Already_Used(
			String menuMessage_ERR_Message_Already_Used) {
		MenuMessage_ERR_Message_Already_Used = menuMessage_ERR_Message_Already_Used;
	}
	public String getMenuMessage_Mandatory_Fields() {
		return MenuMessage_Mandatory_Fields;
	}
	public void setMenuMessage_Mandatory_Fields(String menuMessage_Mandatory_Fields) {
		MenuMessage_Mandatory_Fields = menuMessage_Mandatory_Fields;
	}
	public String getMenuMessage_Menu_Messages_Maintenance() {
		return MenuMessage_Menu_Messages_Maintenance;
	}
	public void setMenuMessage_Menu_Messages_Maintenance(
			String menuMessage_Menu_Messages_Maintenance) {
		MenuMessage_Menu_Messages_Maintenance = menuMessage_Menu_Messages_Maintenance;
	}
	public String getMenuMessage_Ticket_Tape_Message() {
		return MenuMessage_Ticket_Tape_Message;
	}
	public void setMenuMessage_Ticket_Tape_Message(
			String menuMessage_Ticket_Tape_Message) {
		MenuMessage_Ticket_Tape_Message = menuMessage_Ticket_Tape_Message;
	}
	public String getMenuMessage_Message1() {
		return MenuMessage_Message1;
	}
	public void setMenuMessage_Message1(String menuMessage_Message1) {
		MenuMessage_Message1 = menuMessage_Message1;
	}
	
	public String getMenuMessage_Save_Update() {
		return MenuMessage_Save_Update;
	}
	public void setMenuMessage_Save_Update(String menuMessage_Save_Update) {
		MenuMessage_Save_Update = menuMessage_Save_Update;
	}
	public String getMenuMessage_Companies() {
		return MenuMessage_Companies;
	}
	public void setMenuMessage_Companies(String menuMessage_Companies) {
		MenuMessage_Companies = menuMessage_Companies;
	}
	public String getMenuMessage_Cancel() {
		return MenuMessage_Cancel;
	}
	public void setMenuMessage_Cancel(String menuMessage_Cancel) {
		MenuMessage_Cancel = menuMessage_Cancel;
	}
	public String getMenuMessage_End_Date_YYMMDD() {
		return MenuMessage_End_Date_YYMMDD;
	}
	public void setMenuMessage_End_Date_YYMMDD(String menuMessage_End_Date_YYMMDD) {
		MenuMessage_End_Date_YYMMDD = menuMessage_End_Date_YYMMDD;
	}
	public String getMenuMessage_Start_Date() {
		return MenuMessage_Start_Date;
	}
	public void setMenuMessage_Start_Date(String menuMessage_Start_Date) {
		MenuMessage_Start_Date = menuMessage_Start_Date;
	}
	
}
