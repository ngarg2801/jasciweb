/**
Description : Be class for language labels change
Created By : Shailendra Rajput
Created Date : Oct 19 2015
*/
package com.jasci.biz.AdminModule.be;

public class EXECUTIONSBE {

	public  String SCAN="SCAN";	
	public  String PRODUCTS="PRODUCTS";
	public  String QUANTITY="QUANTITY";
	public  String LINES="LINES";
	public  String PRIORITY="PRIORITY";
	public  String PICK="Pick";
	public  String LOCATION="LOCATION";	
	public  String LOCATION_PRIME="PRIME";
	public  String TEXT_OK="OK";
	public  String DESCRIPTION="DESCRIPTION";
	public  String PRODUCT="PRODUCT";	
	public  String QUALITY="QUALITY";
	public  String MESSAGE_CHECK=" Message ";
	public  String LPN="LPN";
	public  String ITEM ="ITEM";	
	public  String SCAN_LPN="SCAN LPN";
	public  String SCAN_ITEM ="SCAN ITEM";	
	//Error msg
	public  String ERR_MSG_INVALID_SCAN_LOCATION="Invalid Location.";
	public  String ERR_MSG_LPN_ALREADY_ASSIGNED = "LPN Already Assigned";	
	public  String ERR_MSG_INVALID_WORK_TYPE = "Invalid Work Type.";	
	public  String ERR_MSG_INVALIDE_TASK_FOR_SEQUENCE = "Invalid Task for Sequence.";
	public  String ERR_MSG_INVALIDE_LPN = "Invalid LPN.";
	public  String ERR_MSG_INVALIDE_EXECUTION = "Invalid Execution.";
	public  String ERR_MSG_WORK_TYPE_NOT_FOUND = "Work Type not found.";
	public  String ERR_MSG_INTERNET_CONNECTION="Please check internet connection.";
	public  String ERR_MSG_INVALIDE_AREA_LOCATION = "Invalid area and location.";
	public  String ERR_MSG_SERVER_NOT_RESPOND = "Server not responding.";
	public  String STRLOADING="Loading...";
	public  String DIRECTION_FORWARD="FORWARD";
	public  String DIRECTION_LEFT="LEFT";
	public  String DIRECTION_RIGHT="RIGHT";
	
	
	
	public  String APK_UPDATE_MSG="Jasci app updated version available, Do you want to install updated version ?";
	public  String APK_UPDATE_YES="Yes";
	public  String APK_UPDATE_NO="No";
	public  String END="End";
	public  String EXIT="Exit";
	public  String START="Start";
	public  String SCANWORKZONE="SCAN WORK ZONE";
	public  String ERR_INVALIDWORKZONE="Invalid Work Zone";
	public String CONTINUE="Continue";
	
	
	
	public String getCONTINUE() {
		return CONTINUE;
	}
	public void setCONTINUE(String cONTINUE) {
		CONTINUE = cONTINUE;
	}
	public String getSCAN() {
		return SCAN;
	}
	public void setSCAN(String sCAN) {
		SCAN = sCAN;
	}
	public String getPRODUCTS() {
		return PRODUCTS;
	}
	public void setPRODUCTS(String pRODUCTS) {
		PRODUCTS = pRODUCTS;
	}
	public String getQUANTITY() {
		return QUANTITY;
	}
	public void setQUANTITY(String qUANTITY) {
		QUANTITY = qUANTITY;
	}
	public String getLINES() {
		return LINES;
	}
	public void setLINES(String lINES) {
		LINES = lINES;
	}
	public String getPRIORITY() {
		return PRIORITY;
	}
	public void setPRIORITY(String pRIORITY) {
		PRIORITY = pRIORITY;
	}
	public String getPICK() {
		return PICK;
	}
	public void setPICK(String pICK) {
		PICK = pICK;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getLOCATION_PRIME() {
		return LOCATION_PRIME;
	}
	public void setLOCATION_PRIME(String lOCATION_PRIME) {
		LOCATION_PRIME = lOCATION_PRIME;
	}
	public String getTEXT_OK() {
		return TEXT_OK;
	}
	public void setTEXT_OK(String tEXT_OK) {
		TEXT_OK = tEXT_OK;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getPRODUCT() {
		return PRODUCT;
	}
	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}
	public String getQUALITY() {
		return QUALITY;
	}
	public void setQUALITY(String qUALITY) {
		QUALITY = qUALITY;
	}
	public String getMESSAGE_CHECK() {
		return MESSAGE_CHECK;
	}
	public void setMESSAGE_CHECK(String mESSAGE_CHECK) {
		MESSAGE_CHECK = mESSAGE_CHECK;
	}
	public String getLPN() {
		return LPN;
	}
	public void setLPN(String lPN) {
		LPN = lPN;
	}
	public String getITEM() {
		return ITEM;
	}
	public void setITEM(String iTEM) {
		ITEM = iTEM;
	}
	public String getSCAN_LPN() {
		return SCAN_LPN;
	}
	public void setSCAN_LPN(String sCAN_LPN) {
		SCAN_LPN = sCAN_LPN;
	}
	public String getSCAN_ITEM() {
		return SCAN_ITEM;
	}
	public void setSCAN_ITEM(String sCAN_ITEM) {
		SCAN_ITEM = sCAN_ITEM;
	}
	public String getERR_MSG_INVALID_SCAN_LOCATION() {
		return ERR_MSG_INVALID_SCAN_LOCATION;
	}
	public void setERR_MSG_INVALID_SCAN_LOCATION(String eRR_MSG_INVALID_SCAN_LOCATION) {
		ERR_MSG_INVALID_SCAN_LOCATION = eRR_MSG_INVALID_SCAN_LOCATION;
	}
	public String getERR_MSG_LPN_ALREADY_ASSIGNED() {
		return ERR_MSG_LPN_ALREADY_ASSIGNED;
	}
	public void setERR_MSG_LPN_ALREADY_ASSIGNED(String eRR_MSG_LPN_ALREADY_ASSIGNED) {
		ERR_MSG_LPN_ALREADY_ASSIGNED = eRR_MSG_LPN_ALREADY_ASSIGNED;
	}
	public String getERR_MSG_INVALID_WORK_TYPE() {
		return ERR_MSG_INVALID_WORK_TYPE;
	}
	public void setERR_MSG_INVALID_WORK_TYPE(String eRR_MSG_INVALID_WORK_TYPE) {
		ERR_MSG_INVALID_WORK_TYPE = eRR_MSG_INVALID_WORK_TYPE;
	}
	public String getERR_MSG_INVALIDE_TASK_FOR_SEQUENCE() {
		return ERR_MSG_INVALIDE_TASK_FOR_SEQUENCE;
	}
	public void setERR_MSG_INVALIDE_TASK_FOR_SEQUENCE(String eRR_MSG_INVALIDE_TASK_FOR_SEQUENCE) {
		ERR_MSG_INVALIDE_TASK_FOR_SEQUENCE = eRR_MSG_INVALIDE_TASK_FOR_SEQUENCE;
	}
	public String getERR_MSG_INVALIDE_LPN() {
		return ERR_MSG_INVALIDE_LPN;
	}
	public void setERR_MSG_INVALIDE_LPN(String eRR_MSG_INVALIDE_LPN) {
		ERR_MSG_INVALIDE_LPN = eRR_MSG_INVALIDE_LPN;
	}
	public String getERR_MSG_INVALIDE_EXECUTION() {
		return ERR_MSG_INVALIDE_EXECUTION;
	}
	public void setERR_MSG_INVALIDE_EXECUTION(String eRR_MSG_INVALIDE_EXECUTION) {
		ERR_MSG_INVALIDE_EXECUTION = eRR_MSG_INVALIDE_EXECUTION;
	}
	public String getERR_MSG_WORK_TYPE_NOT_FOUND() {
		return ERR_MSG_WORK_TYPE_NOT_FOUND;
	}
	public void setERR_MSG_WORK_TYPE_NOT_FOUND(String eRR_MSG_WORK_TYPE_NOT_FOUND) {
		ERR_MSG_WORK_TYPE_NOT_FOUND = eRR_MSG_WORK_TYPE_NOT_FOUND;
	}
	public String getERR_MSG_INTERNET_CONNECTION() {
		return ERR_MSG_INTERNET_CONNECTION;
	}
	public void setERR_MSG_INTERNET_CONNECTION(String eRR_MSG_INTERNET_CONNECTION) {
		ERR_MSG_INTERNET_CONNECTION = eRR_MSG_INTERNET_CONNECTION;
	}
	public String getERR_MSG_INVALIDE_AREA_LOCATION() {
		return ERR_MSG_INVALIDE_AREA_LOCATION;
	}
	public void setERR_MSG_INVALIDE_AREA_LOCATION(String eRR_MSG_INVALIDE_AREA_LOCATION) {
		ERR_MSG_INVALIDE_AREA_LOCATION = eRR_MSG_INVALIDE_AREA_LOCATION;
	}
	public String getERR_MSG_SERVER_NOT_RESPOND() {
		return ERR_MSG_SERVER_NOT_RESPOND;
	}
	public void setERR_MSG_SERVER_NOT_RESPOND(String eRR_MSG_SERVER_NOT_RESPOND) {
		ERR_MSG_SERVER_NOT_RESPOND = eRR_MSG_SERVER_NOT_RESPOND;
	}
	public String getSTRLOADING() {
		return STRLOADING;
	}
	public void setSTRLOADING(String sTRLOADING) {
		STRLOADING = sTRLOADING;
	}
	public String getDIRECTION_FORWARD() {
		return DIRECTION_FORWARD;
	}
	public void setDIRECTION_FORWARD(String dIRECTION_FORWARD) {
		DIRECTION_FORWARD = dIRECTION_FORWARD;
	}
	public String getDIRECTION_LEFT() {
		return DIRECTION_LEFT;
	}
	public void setDIRECTION_LEFT(String dIRECTION_LEFT) {
		DIRECTION_LEFT = dIRECTION_LEFT;
	}
	public String getDIRECTION_RIGHT() {
		return DIRECTION_RIGHT;
	}
	public void setDIRECTION_RIGHT(String dIRECTION_RIGHT) {
		DIRECTION_RIGHT = dIRECTION_RIGHT;
	}
	public String getAPK_UPDATE_MSG() {
		return APK_UPDATE_MSG;
	}
	public void setAPK_UPDATE_MSG(String aPK_UPDATE_MSG) {
		APK_UPDATE_MSG = aPK_UPDATE_MSG;
	}
	public String getAPK_UPDATE_YES() {
		return APK_UPDATE_YES;
	}
	public void setAPK_UPDATE_YES(String aPK_UPDATE_YES) {
		APK_UPDATE_YES = aPK_UPDATE_YES;
	}
	public String getAPK_UPDATE_NO() {
		return APK_UPDATE_NO;
	}
	public void setAPK_UPDATE_NO(String aPK_UPDATE_NO) {
		APK_UPDATE_NO = aPK_UPDATE_NO;
	}
	public String getEND() {
		return END;
	}
	public void setEND(String eND) {
		END = eND;
	}
	public String getEXIT() {
		return EXIT;
	}
	public void setEXIT(String eXIT) {
		EXIT = eXIT;
	}
	public String getSTART() {
		return START;
	}
	public void setSTART(String sTART) {
		START = sTART;
	}
	public String getSCANWORKZONE() {
		return SCANWORKZONE;
	}
	public void setSCANWORKZONE(String sCANWORKZONE) {
		SCANWORKZONE = sCANWORKZONE;
	}
	public String getERR_INVALIDWORKZONE() {
		return ERR_INVALIDWORKZONE;
	}
	public void setERR_INVALIDWORKZONE(String eRR_INVALIDWORKZONE) {
		ERR_INVALIDWORKZONE = eRR_INVALIDWORKZONE;
	}
	
	
	
	
	
	
}
