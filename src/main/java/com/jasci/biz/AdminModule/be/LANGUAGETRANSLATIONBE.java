/**
Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :This class has getter setter for the variables that are to be shown on grid to show tha data of languages table. */

package com.jasci.biz.AdminModule.be;

public class LANGUAGETRANSLATIONBE 
{

	int intKendoID;
	private String LastActivityDate;
	private String LastActivityBy;
	private String KeyPhrase;
	private String Translation;
	private String Language;
	private String Description20;
	private String ModuleName;

	
	public String getModuleName() {
		return ModuleName;
	}
	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityBy() {
		return LastActivityBy;
	}
	public void setLastActivityBy(String lastActivityBy) {
		LastActivityBy = lastActivityBy;
	}
	public String getKeyPhrase() {
		return KeyPhrase;
	}
	public void setKeyPhrase(String keyPhrase) {
		KeyPhrase = keyPhrase;
	}
	public String getTranslation() {
		return Translation;
	}
	public void setTranslation(String translation) {
		Translation = translation;
	}
	public String getLanguage() {
		return Language;
	}
	public void setLanguage(String language) {
		Language = language;
	}
	public int getIntKendoID() {
		return intKendoID;
	}
	public void setIntKendoID(int intKendoID) {
		this.intKendoID = intKendoID;
	}
	
	
}
