/**
Description It is used to make setter and getter for changing text of screen in another language
Created By Sarvendra Tyagi
Created Date DEC 26 2014
 */
package com.jasci.biz.AdminModule.be;

public class MENUEXECUTIONLABELBE {

	
	private String KeyPharesTeam_Member="Team_Member";
	private String KeyPharesCompany_ID="CompanyID";
	private String keyName="Name";
	private String KeyPharesMESSAGES="MESSAGES";
	private String KeyPharesAPPLICATIONS="APPLICATIONS";
	
	private String lblAPPLICATIONS;
	
	public String getLblAPPLICATIONS() {
		return lblAPPLICATIONS;
	}
	public void setLblAPPLICATIONS(String lblAPPLICATIONS) {
		this.lblAPPLICATIONS = lblAPPLICATIONS;
	}
	public String getKeyPharesTeam_Member() {
		return KeyPharesTeam_Member;
	}
	public void setKeyPharesTeam_Member(String keyPharesTeam_Member) {
		KeyPharesTeam_Member = keyPharesTeam_Member;
	}
	public String getKeyPharesCompany_ID() {
		return KeyPharesCompany_ID;
	}
	public void setKeyPharesCompany_ID(String keyPharesCompany_ID) {
		KeyPharesCompany_ID = keyPharesCompany_ID;
	}
	public String getKeyName() {
		return keyName;
	}
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	public String getKeyPharesMESSAGES() {
		return KeyPharesMESSAGES;
	}
	public void setKeyPharesMESSAGES(String keyPharesMESSAGES) {
		KeyPharesMESSAGES = keyPharesMESSAGES;
	}
	public String getKeyPharesAPPLICATIONS() {
		return KeyPharesAPPLICATIONS;
	}
	public void setKeyPharesAPPLICATIONS(String keyPharesAPPLICATIONS) {
		KeyPharesAPPLICATIONS = keyPharesAPPLICATIONS;
	}
	public String getLblMESSAGES() {
		return lblMESSAGES;
	}
	public void setLblMESSAGES(String lblMESSAGES) {
		this.lblMESSAGES = lblMESSAGES;
	}
	public String getLblName() {
		return lblName;
	}
	public void setLblName(String lblName) {
		this.lblName = lblName;
	}
	public String getLblCompany_ID() {
		return lblCompany_ID;
	}
	public void setLblCompany_ID(String lblCompany_ID) {
		this.lblCompany_ID = lblCompany_ID;
	}
	public String getLblTeam_Member() {
		return lblTeam_Member;
	}
	public void setLblTeam_Member(String lblTeam_Member) {
		this.lblTeam_Member = lblTeam_Member;
	}
	private String lblMESSAGES;
	private String lblName;
	private String lblCompany_ID;
	private String lblTeam_Member;
	
	private String lblCompaySelectWorkWith;

	public String getLblCompaySelectWorkWith() {
		return lblCompaySelectWorkWith;
	}
	public void setLblCompaySelectWorkWith(String lblCompaySelectWorkWith) {
		this.lblCompaySelectWorkWith = lblCompaySelectWorkWith;
	}
	
}
