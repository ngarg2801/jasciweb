/* 
Description It is used to make setter and getter for changing text of screen in another language
Created By :Rahul Kumar
Created Date Dec 22 2014
 */
package com.jasci.biz.AdminModule.be;

public class COMPANYSCREENBE {
	
	private String CompanyMaintenance_ButtonResetText;
	private String CompanyMaintenance_PartOfTheCompanyNameLabel;
	private String CompanyMaintenance_CompanyidLabel;
	private String CompanyMaintenance_Name20Label;
	private String CompanyMaintenance_Name50Label;
	private String CompanyMaintenance_Addressline1Label;
	private String CompanyMaintenance_Addressline2Label;
	private String CompanyMaintenance_Addressline3Label;
	private String CompanyMaintenance_Addressline4Label;
	private String CompanyMaintenance_CityLabel;
	private String CompanyMaintenance_StatecodeLabel;
	private String CompanyMaintenance_CountrycodeLabel;
	private String CompanyMaintenance_ZipcodeLabel;
	private String CompanyMaintenance_Billtoname20Label;
	private String CompanyMaintenance_Billtoname50Label;
	private String CompanyMaintenance_Billtoaddressline1Label;
	private String CompanyMaintenance_Billtoaddressline2Label;
	private String CompanyMaintenance_Billtoaddressline3Label;
	private String CompanyMaintenance_Billtoaddressline4Label;
	private String CompanyMaintenance_BilltofromcityLabel;
	private String CompanyMaintenance_BilltostatecodeLabel;
	private String CompanyMaintenance_BilltocountrycodeLabel;
	private String CompanyMaintenance_BilltozipcodeLabel;
	private String CompanyMaintenance_Contactname1Label;
	private String CompanyMaintenance_Contactphone1Label;
	private String CompanyMaintenance_Contactextension1Label;
	private String CompanyMaintenance_Contactcell1Label;
	private String CompanyMaintenance_Contactfax1Label;
	private String CompanyMaintenance_Contactemail1Label;
	private String CompanyMaintenance_Contactname2Label;
	private String CompanyMaintenance_Contactphone2Label;
	private String CompanyMaintenance_Contactextension2Label;
	private String CompanyMaintenance_Contactcell2Label;
	private String CompanyMaintenance_Contactfax2Label;
	private String CompanyMaintenance_Contactemail2Label;
	private String CompanyMaintenance_Contactname3Label;
	private String CompanyMaintenance_Contactphone3Label;
	private String CompanyMaintenance_Contactextension3Label;
	private String CompanyMaintenance_Contactcell3Label;
	private String CompanyMaintenance_Contactfax3Label;
	private String CompanyMaintenance_Contactemail3Label;
	private String CompanyMaintenance_Contactname4Label;
	private String CompanyMaintenance_Contactphone4Label;
	private String CompanyMaintenance_Contactextension4Label;
	private String CompanyMaintenance_Contactcell4Label;
	private String CompanyMaintenance_Contactfax4Label;
	private String CompanyMaintenance_Contactemail4Label;
	private String CompanyMaintenance_MainfaxLabel;
	private String CompanyMaintenance_MainemailLabel;
	private String CompanyMaintenance_BillingcontrolnumberLabel;
	private String CompanyMaintenance_ThememobileLabel;
	private String CompanyMaintenance_ThemerfLabel;
	private String CompanyMaintenance_ThemefulldisplayLabel;
	private String CompanyMaintenance_LogoLabel;
	private String CompanyMaintenance_PurchaseorderreqapprovalLabel;
	private String CompanyMaintenance_StatusLabel;
	private String CompanyMaintenance_SetupselectedLabel;
	private String CompanyMaintenance_SetupdateLabel;
	private String CompanyMaintenance_SetupbyLabel;
	private String CompanyMaintenance_TimezoneLabel;
	private String CompanyMaintenance_LastactivitydateLabel;
	private String CompanyMaintenance_LastactivityteammemberLabel;
	private String CompanyMaintenance_ButtonAddNewText;
	private String CompanyMaintenance_ButtonCancelText;
	private String CompanyMaintenance_ButtonActionText;
	private String CompanyMaintenance_ButtonDeleteText;
	private String CompanyMaintenance_ButtonEditText;
	private String CompanyMaintenance_ButtonDisplayAllText;
	private String CompanyMaintenance_LookupLabel;
	private String CompanyMaintenance_MaintenanceLabel;
	private String CompanyMaintenance_SearchLookupLabel;
	private String CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	private String CompanyMaintenance_ERR_ALL_FIELDS_LEFT_BLANK;
	private String CompanyMaintenance_ERR_IF_INVALID_COMPANY;
	private String CompanyMaintenance_ERR_IF_ADDRESSLINE1_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_ADDRESSLINE2_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_ADDRESSLINE3_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_ADDRESSLINE4_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_CITY_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE1_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE2_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE3_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE4_EXCEEDED;
	private String CompanyMaintenance_ERR_IF_BillTo_CITY_EXCEEDED;
	private String CompanyMaintenance_ERR_INVALID_PART_OF_COMPANY;
	private String CompanyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME;
	private String CompanyMaintenance_ERR_WHILE_DELETING_COMPANY_IF_USED_IN_OTHER_TABLE;
	private String CompanyMaintenance_ON_SAVE_SUCCESSFULLY;
	private String CompanyMaintenance_ON_UPDATE_SUCCESSFULLY;
	private String CompanyMaintenance_ERR_WHILE_DELETING_COMPANY;
	private String CompanyMaintenance_ERR_COMPANY_ALREADY_USED;
	private String CompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID;
	private String CompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME;
	private String CompanyMaintenance_CompanyNameLabel;
	private String CompanyMaintenance_SetupSelectedWizardLabel;
	private String CompanyMaintenance_SaveUpdateText;
	private String CompanyMaintenance_CompanyLabel;
	private String CompanyMaintenance_Select;
	private String CompanyMaintenance_Yes;
	private String CompanyMaintenance_No;
	private String CompanyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE;
	private String CompanyMaintenance_ERR_INVALID_ZIP_CODE;
	private String CompanyMaintenance_ERR_INVALID_BILL_TO_ADDRESS;
	private String CompanyMaintenance_ERR_INVALID_ADDRESS;
	private String CompanyMaintenance_ERR_INVALID_CONTACT_NUMBER;
	private String CompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS;
	private String CompanyMaintenance_BrowseText;	
	private String CompanyMaintenance_SupportedFormatsLabel;
	private String CompanyMaintenance_ERR_INVALID_SUPPORTED_FORMATS;
	private String CompanyMaintenance_Contact;
	private String CompanyMaintenance_Text_Loading;
	private String CompanyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED;
	private String CompanyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status;	
	private String	CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
	private String	CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;   
	
	
	
	
	public String getCompanyMaintenance_ButtonResetText() {
		return CompanyMaintenance_ButtonResetText;
	}
	public void setCompanyMaintenance_ButtonResetText(String companyMaintenance_ButtonResetText) {
		CompanyMaintenance_ButtonResetText = companyMaintenance_ButtonResetText;
	}
	public String getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS() {
		return CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;
	}
	public void setCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS(
			String companyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS) {
		CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS = companyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;
	}
	public String getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY() {
		return CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
	}
	public void setCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY(
			String companyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY) {
		CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY = companyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
	}
	public String getCompanyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status() {
		return CompanyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status;
	}
	public void setCompanyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status(
			String companyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status) {
		CompanyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status = companyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status;
	}
	/**
	 * @return the companyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED
	 */
	public String getCompanyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED() {
		return CompanyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED;
	}
	/**
	 * @param companyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED the companyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED to set
	 */
	public void setCompanyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED(
			String companyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED) {
		CompanyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED = companyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED;
	}
	public String getCompanyMaintenance_Text_Loading() {
		return CompanyMaintenance_Text_Loading;
	}
	public void setCompanyMaintenance_Text_Loading(String companyMaintenance_Text_Loading) {
		CompanyMaintenance_Text_Loading = companyMaintenance_Text_Loading;
	}
	public String getCompanyMaintenance_Contact() {
		return CompanyMaintenance_Contact;
	}
	public void setCompanyMaintenance_Contact(String companyMaintenance_Contact) {
		CompanyMaintenance_Contact = companyMaintenance_Contact;
	}
	public String getCompanyMaintenance_SupportedFormatsLabel() {
		return CompanyMaintenance_SupportedFormatsLabel;
	}
	public void setCompanyMaintenance_SupportedFormatsLabel(String companyMaintenance_SupportedFormatsLabel) {
		CompanyMaintenance_SupportedFormatsLabel = companyMaintenance_SupportedFormatsLabel;
	}
	public String getCompanyMaintenance_ERR_INVALID_SUPPORTED_FORMATS() {
		return CompanyMaintenance_ERR_INVALID_SUPPORTED_FORMATS;
	}
	public void setCompanyMaintenance_ERR_INVALID_SUPPORTED_FORMATS(String companyMaintenance_ERR_INVALID_SUPPORTED_FORMATS) {
		CompanyMaintenance_ERR_INVALID_SUPPORTED_FORMATS = companyMaintenance_ERR_INVALID_SUPPORTED_FORMATS;
	}
	public String getCompanyMaintenance_BrowseText() {
		return CompanyMaintenance_BrowseText;
	}
	public void setCompanyMaintenance_BrowseText(String companyMaintenance_BrowseText) {
		CompanyMaintenance_BrowseText = companyMaintenance_BrowseText;
	}
	public String getCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER() {
		return CompanyMaintenance_ERR_INVALID_CONTACT_NUMBER;
	}
	public void setCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER(String companyMaintenance_ERR_INVALID_CONTACT_NUMBER) {
		CompanyMaintenance_ERR_INVALID_CONTACT_NUMBER = companyMaintenance_ERR_INVALID_CONTACT_NUMBER;
	}
	public String getCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS() {
		return CompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS;
	}
	public void setCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS(String companyMaintenance_ERR_INVALID_EMAIL_ADDRESS) {
		CompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS = companyMaintenance_ERR_INVALID_EMAIL_ADDRESS;
	}
	public String getCompanyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE() {
		return CompanyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE;
	}
	public void setCompanyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE(String companyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE) {
		CompanyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE = companyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE;
	}
	public String getCompanyMaintenance_ERR_INVALID_ZIP_CODE() {
		return CompanyMaintenance_ERR_INVALID_ZIP_CODE;
	}
	public void setCompanyMaintenance_ERR_INVALID_ZIP_CODE(String companyMaintenance_ERR_INVALID_ZIP_CODE) {
		CompanyMaintenance_ERR_INVALID_ZIP_CODE = companyMaintenance_ERR_INVALID_ZIP_CODE;
	}
	public String getCompanyMaintenance_ERR_INVALID_BILL_TO_ADDRESS() {
		return CompanyMaintenance_ERR_INVALID_BILL_TO_ADDRESS;
	}
	public void setCompanyMaintenance_ERR_INVALID_BILL_TO_ADDRESS(String companyMaintenance_ERR_INVALID_BILL_TO_ADDRESS) {
		CompanyMaintenance_ERR_INVALID_BILL_TO_ADDRESS = companyMaintenance_ERR_INVALID_BILL_TO_ADDRESS;
	}
	public String getCompanyMaintenance_ERR_INVALID_ADDRESS() {
		return CompanyMaintenance_ERR_INVALID_ADDRESS;
	}
	public void setCompanyMaintenance_ERR_INVALID_ADDRESS(String companyMaintenance_ERR_INVALID_ADDRESS) {
		CompanyMaintenance_ERR_INVALID_ADDRESS = companyMaintenance_ERR_INVALID_ADDRESS;
	}
	public String getCompanyMaintenance_Yes() {
		return CompanyMaintenance_Yes;
	}
	public void setCompanyMaintenance_Yes(String companyMaintenance_Yes) {
		CompanyMaintenance_Yes = companyMaintenance_Yes;
	}
	public String getCompanyMaintenance_No() {
		return CompanyMaintenance_No;
	}
	public void setCompanyMaintenance_No(String companyMaintenance_No) {
		CompanyMaintenance_No = companyMaintenance_No;
	}
	public String getCompanyMaintenance_Select() {
		return CompanyMaintenance_Select;
	}
	public void setCompanyMaintenance_Select(String companyMaintenance_Select) {
		CompanyMaintenance_Select = companyMaintenance_Select;
	}
	public String getCompanyMaintenance_CompanyLabel() {
		return CompanyMaintenance_CompanyLabel;
	}
	public void setCompanyMaintenance_CompanyLabel(String companyMaintenance_CompanyLabel) {
		CompanyMaintenance_CompanyLabel = companyMaintenance_CompanyLabel;
	}
	public String getCompanyMaintenance_SaveUpdateText() {
		return CompanyMaintenance_SaveUpdateText;
	}
	public void setCompanyMaintenance_SaveUpdateText(String companyMaintenance_SaveUpdateText) {
		CompanyMaintenance_SaveUpdateText = companyMaintenance_SaveUpdateText;
	}
	public String getCompanyMaintenance_SetupSelectedWizardLabel() {
		return CompanyMaintenance_SetupSelectedWizardLabel;
	}
	public void setCompanyMaintenance_SetupSelectedWizardLabel(String companyMaintenance_SetupSelectedWizardLabel) {
		CompanyMaintenance_SetupSelectedWizardLabel = companyMaintenance_SetupSelectedWizardLabel;
	}
	public String getCompanyMaintenance_CompanyNameLabel() {
		return CompanyMaintenance_CompanyNameLabel;
	}
	public void setCompanyMaintenance_CompanyNameLabel(String companyMaintenance_CompanyNameLabel) {
		CompanyMaintenance_CompanyNameLabel = companyMaintenance_CompanyNameLabel;
	}
	public String getCompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID() {
		return CompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID;
	}
	public void setCompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID(
			String companyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID) {
		CompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID = companyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID;
	}
	public String getCompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME() {
		return CompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME;
	}
	public void setCompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME(
			String companyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME) {
		CompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME = companyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME;
	}
	private String CompanyMaintenance_ButtonNewText;
	
	
	
	public String getCompanyMaintenance_ButtonNewText() {
		return CompanyMaintenance_ButtonNewText;
	}
	public void setCompanyMaintenance_ButtonNewText(String companyMaintenance_ButtonNewText) {
		CompanyMaintenance_ButtonNewText = companyMaintenance_ButtonNewText;
	}
	public String getCompanyMaintenance_PartOfTheCompanyNameLabel() {
		return CompanyMaintenance_PartOfTheCompanyNameLabel;
	}
	public void setCompanyMaintenance_PartOfTheCompanyNameLabel(String companyMaintenance_PartOfTheCompanyNameLabel) {
		CompanyMaintenance_PartOfTheCompanyNameLabel = companyMaintenance_PartOfTheCompanyNameLabel;
	}
	public String getCompanyMaintenance_CompanyidLabel() {
		return CompanyMaintenance_CompanyidLabel;
	}
	public void setCompanyMaintenance_CompanyidLabel(String companyMaintenance_CompanyidLabel) {
		CompanyMaintenance_CompanyidLabel = companyMaintenance_CompanyidLabel;
	}
	public String getCompanyMaintenance_Name20Label() {
		return CompanyMaintenance_Name20Label;
	}
	public void setCompanyMaintenance_Name20Label(String companyMaintenance_Name20Label) {
		CompanyMaintenance_Name20Label = companyMaintenance_Name20Label;
	}
	public String getCompanyMaintenance_Name50Label() {
		return CompanyMaintenance_Name50Label;
	}
	public void setCompanyMaintenance_Name50Label(String companyMaintenance_Name50Label) {
		CompanyMaintenance_Name50Label = companyMaintenance_Name50Label;
	}
	public String getCompanyMaintenance_Addressline1Label() {
		return CompanyMaintenance_Addressline1Label;
	}
	public void setCompanyMaintenance_Addressline1Label(String companyMaintenance_Addressline1Label) {
		CompanyMaintenance_Addressline1Label = companyMaintenance_Addressline1Label;
	}
	public String getCompanyMaintenance_Addressline2Label() {
		return CompanyMaintenance_Addressline2Label;
	}
	public void setCompanyMaintenance_Addressline2Label(String companyMaintenance_Addressline2Label) {
		CompanyMaintenance_Addressline2Label = companyMaintenance_Addressline2Label;
	}
	public String getCompanyMaintenance_Addressline3Label() {
		return CompanyMaintenance_Addressline3Label;
	}
	public void setCompanyMaintenance_Addressline3Label(String companyMaintenance_Addressline3Label) {
		CompanyMaintenance_Addressline3Label = companyMaintenance_Addressline3Label;
	}
	public String getCompanyMaintenance_Addressline4Label() {
		return CompanyMaintenance_Addressline4Label;
	}
	public void setCompanyMaintenance_Addressline4Label(String companyMaintenance_Addressline4Label) {
		CompanyMaintenance_Addressline4Label = companyMaintenance_Addressline4Label;
	}
	public String getCompanyMaintenance_CityLabel() {
		return CompanyMaintenance_CityLabel;
	}
	public void setCompanyMaintenance_CityLabel(String companyMaintenance_CityLabel) {
		CompanyMaintenance_CityLabel = companyMaintenance_CityLabel;
	}
	public String getCompanyMaintenance_StatecodeLabel() {
		return CompanyMaintenance_StatecodeLabel;
	}
	public void setCompanyMaintenance_StatecodeLabel(String companyMaintenance_StatecodeLabel) {
		CompanyMaintenance_StatecodeLabel = companyMaintenance_StatecodeLabel;
	}
	public String getCompanyMaintenance_CountrycodeLabel() {
		return CompanyMaintenance_CountrycodeLabel;
	}
	public void setCompanyMaintenance_CountrycodeLabel(String companyMaintenance_CountrycodeLabel) {
		CompanyMaintenance_CountrycodeLabel = companyMaintenance_CountrycodeLabel;
	}
	public String getCompanyMaintenance_ZipcodeLabel() {
		return CompanyMaintenance_ZipcodeLabel;
	}
	public void setCompanyMaintenance_ZipcodeLabel(String companyMaintenance_ZipcodeLabel) {
		CompanyMaintenance_ZipcodeLabel = companyMaintenance_ZipcodeLabel;
	}
	public String getCompanyMaintenance_Billtoname20Label() {
		return CompanyMaintenance_Billtoname20Label;
	}
	public void setCompanyMaintenance_Billtoname20Label(String companyMaintenance_Billtoname20Label) {
		CompanyMaintenance_Billtoname20Label = companyMaintenance_Billtoname20Label;
	}
	public String getCompanyMaintenance_Billtoname50Label() {
		return CompanyMaintenance_Billtoname50Label;
	}
	public void setCompanyMaintenance_Billtoname50Label(String companyMaintenance_Billtoname50Label) {
		CompanyMaintenance_Billtoname50Label = companyMaintenance_Billtoname50Label;
	}
	public String getCompanyMaintenance_Billtoaddressline1Label() {
		return CompanyMaintenance_Billtoaddressline1Label;
	}
	public void setCompanyMaintenance_Billtoaddressline1Label(String companyMaintenance_Billtoaddressline1Label) {
		CompanyMaintenance_Billtoaddressline1Label = companyMaintenance_Billtoaddressline1Label;
	}
	public String getCompanyMaintenance_Billtoaddressline2Label() {
		return CompanyMaintenance_Billtoaddressline2Label;
	}
	public void setCompanyMaintenance_Billtoaddressline2Label(String companyMaintenance_Billtoaddressline2Label) {
		CompanyMaintenance_Billtoaddressline2Label = companyMaintenance_Billtoaddressline2Label;
	}
	public String getCompanyMaintenance_Billtoaddressline3Label() {
		return CompanyMaintenance_Billtoaddressline3Label;
	}
	public void setCompanyMaintenance_Billtoaddressline3Label(String companyMaintenance_Billtoaddressline3Label) {
		CompanyMaintenance_Billtoaddressline3Label = companyMaintenance_Billtoaddressline3Label;
	}
	public String getCompanyMaintenance_Billtoaddressline4Label() {
		return CompanyMaintenance_Billtoaddressline4Label;
	}
	public void setCompanyMaintenance_Billtoaddressline4Label(String companyMaintenance_Billtoaddressline4Label) {
		CompanyMaintenance_Billtoaddressline4Label = companyMaintenance_Billtoaddressline4Label;
	}
	public String getCompanyMaintenance_BilltofromcityLabel() {
		return CompanyMaintenance_BilltofromcityLabel;
	}
	public void setCompanyMaintenance_BilltofromcityLabel(String companyMaintenance_BilltofromcityLabel) {
		CompanyMaintenance_BilltofromcityLabel = companyMaintenance_BilltofromcityLabel;
	}
	public String getCompanyMaintenance_BilltostatecodeLabel() {
		return CompanyMaintenance_BilltostatecodeLabel;
	}
	public void setCompanyMaintenance_BilltostatecodeLabel(String companyMaintenance_BilltostatecodeLabel) {
		CompanyMaintenance_BilltostatecodeLabel = companyMaintenance_BilltostatecodeLabel;
	}
	public String getCompanyMaintenance_BilltocountrycodeLabel() {
		return CompanyMaintenance_BilltocountrycodeLabel;
	}
	public void setCompanyMaintenance_BilltocountrycodeLabel(String companyMaintenance_BilltocountrycodeLabel) {
		CompanyMaintenance_BilltocountrycodeLabel = companyMaintenance_BilltocountrycodeLabel;
	}
	public String getCompanyMaintenance_BilltozipcodeLabel() {
		return CompanyMaintenance_BilltozipcodeLabel;
	}
	public void setCompanyMaintenance_BilltozipcodeLabel(String companyMaintenance_BilltozipcodeLabel) {
		CompanyMaintenance_BilltozipcodeLabel = companyMaintenance_BilltozipcodeLabel;
	}
	public String getCompanyMaintenance_Contactname1Label() {
		return CompanyMaintenance_Contactname1Label;
	}
	public void setCompanyMaintenance_Contactname1Label(String companyMaintenance_Contactname1Label) {
		CompanyMaintenance_Contactname1Label = companyMaintenance_Contactname1Label;
	}
	public String getCompanyMaintenance_Contactphone1Label() {
		return CompanyMaintenance_Contactphone1Label;
	}
	public void setCompanyMaintenance_Contactphone1Label(String companyMaintenance_Contactphone1Label) {
		CompanyMaintenance_Contactphone1Label = companyMaintenance_Contactphone1Label;
	}
	public String getCompanyMaintenance_Contactextension1Label() {
		return CompanyMaintenance_Contactextension1Label;
	}
	public void setCompanyMaintenance_Contactextension1Label(String companyMaintenance_Contactextension1Label) {
		CompanyMaintenance_Contactextension1Label = companyMaintenance_Contactextension1Label;
	}
	public String getCompanyMaintenance_Contactcell1Label() {
		return CompanyMaintenance_Contactcell1Label;
	}
	public void setCompanyMaintenance_Contactcell1Label(String companyMaintenance_Contactcell1Label) {
		CompanyMaintenance_Contactcell1Label = companyMaintenance_Contactcell1Label;
	}
	public String getCompanyMaintenance_Contactfax1Label() {
		return CompanyMaintenance_Contactfax1Label;
	}
	public void setCompanyMaintenance_Contactfax1Label(String companyMaintenance_Contactfax1Label) {
		CompanyMaintenance_Contactfax1Label = companyMaintenance_Contactfax1Label;
	}
	public String getCompanyMaintenance_Contactemail1Label() {
		return CompanyMaintenance_Contactemail1Label;
	}
	public void setCompanyMaintenance_Contactemail1Label(String companyMaintenance_Contactemail1Label) {
		CompanyMaintenance_Contactemail1Label = companyMaintenance_Contactemail1Label;
	}
	public String getCompanyMaintenance_Contactname2Label() {
		return CompanyMaintenance_Contactname2Label;
	}
	public void setCompanyMaintenance_Contactname2Label(String companyMaintenance_Contactname2Label) {
		CompanyMaintenance_Contactname2Label = companyMaintenance_Contactname2Label;
	}
	public String getCompanyMaintenance_Contactphone2Label() {
		return CompanyMaintenance_Contactphone2Label;
	}
	public void setCompanyMaintenance_Contactphone2Label(String companyMaintenance_Contactphone2Label) {
		CompanyMaintenance_Contactphone2Label = companyMaintenance_Contactphone2Label;
	}
	public String getCompanyMaintenance_Contactextension2Label() {
		return CompanyMaintenance_Contactextension2Label;
	}
	public void setCompanyMaintenance_Contactextension2Label(String companyMaintenance_Contactextension2Label) {
		CompanyMaintenance_Contactextension2Label = companyMaintenance_Contactextension2Label;
	}
	public String getCompanyMaintenance_Contactcell2Label() {
		return CompanyMaintenance_Contactcell2Label;
	}
	public void setCompanyMaintenance_Contactcell2Label(String companyMaintenance_Contactcell2Label) {
		CompanyMaintenance_Contactcell2Label = companyMaintenance_Contactcell2Label;
	}
	public String getCompanyMaintenance_Contactfax2Label() {
		return CompanyMaintenance_Contactfax2Label;
	}
	public void setCompanyMaintenance_Contactfax2Label(String companyMaintenance_Contactfax2Label) {
		CompanyMaintenance_Contactfax2Label = companyMaintenance_Contactfax2Label;
	}
	public String getCompanyMaintenance_Contactemail2Label() {
		return CompanyMaintenance_Contactemail2Label;
	}
	public void setCompanyMaintenance_Contactemail2Label(String companyMaintenance_Contactemail2Label) {
		CompanyMaintenance_Contactemail2Label = companyMaintenance_Contactemail2Label;
	}
	public String getCompanyMaintenance_Contactname3Label() {
		return CompanyMaintenance_Contactname3Label;
	}
	public void setCompanyMaintenance_Contactname3Label(String companyMaintenance_Contactname3Label) {
		CompanyMaintenance_Contactname3Label = companyMaintenance_Contactname3Label;
	}
	public String getCompanyMaintenance_Contactphone3Label() {
		return CompanyMaintenance_Contactphone3Label;
	}
	public void setCompanyMaintenance_Contactphone3Label(String companyMaintenance_Contactphone3Label) {
		CompanyMaintenance_Contactphone3Label = companyMaintenance_Contactphone3Label;
	}
	public String getCompanyMaintenance_Contactextension3Label() {
		return CompanyMaintenance_Contactextension3Label;
	}
	public void setCompanyMaintenance_Contactextension3Label(String companyMaintenance_Contactextension3Label) {
		CompanyMaintenance_Contactextension3Label = companyMaintenance_Contactextension3Label;
	}
	public String getCompanyMaintenance_Contactcell3Label() {
		return CompanyMaintenance_Contactcell3Label;
	}
	public void setCompanyMaintenance_Contactcell3Label(String companyMaintenance_Contactcell3Label) {
		CompanyMaintenance_Contactcell3Label = companyMaintenance_Contactcell3Label;
	}
	public String getCompanyMaintenance_Contactfax3Label() {
		return CompanyMaintenance_Contactfax3Label;
	}
	public void setCompanyMaintenance_Contactfax3Label(String companyMaintenance_Contactfax3Label) {
		CompanyMaintenance_Contactfax3Label = companyMaintenance_Contactfax3Label;
	}
	public String getCompanyMaintenance_Contactemail3Label() {
		return CompanyMaintenance_Contactemail3Label;
	}
	public void setCompanyMaintenance_Contactemail3Label(String companyMaintenance_Contactemail3Label) {
		CompanyMaintenance_Contactemail3Label = companyMaintenance_Contactemail3Label;
	}
	public String getCompanyMaintenance_Contactname4Label() {
		return CompanyMaintenance_Contactname4Label;
	}
	public void setCompanyMaintenance_Contactname4Label(String companyMaintenance_Contactname4Label) {
		CompanyMaintenance_Contactname4Label = companyMaintenance_Contactname4Label;
	}
	public String getCompanyMaintenance_Contactphone4Label() {
		return CompanyMaintenance_Contactphone4Label;
	}
	public void setCompanyMaintenance_Contactphone4Label(String companyMaintenance_Contactphone4Label) {
		CompanyMaintenance_Contactphone4Label = companyMaintenance_Contactphone4Label;
	}
	public String getCompanyMaintenance_Contactextension4Label() {
		return CompanyMaintenance_Contactextension4Label;
	}
	public void setCompanyMaintenance_Contactextension4Label(String companyMaintenance_Contactextension4Label) {
		CompanyMaintenance_Contactextension4Label = companyMaintenance_Contactextension4Label;
	}
	public String getCompanyMaintenance_Contactcell4Label() {
		return CompanyMaintenance_Contactcell4Label;
	}
	public void setCompanyMaintenance_Contactcell4Label(String companyMaintenance_Contactcell4Label) {
		CompanyMaintenance_Contactcell4Label = companyMaintenance_Contactcell4Label;
	}
	public String getCompanyMaintenance_Contactfax4Label() {
		return CompanyMaintenance_Contactfax4Label;
	}
	public void setCompanyMaintenance_Contactfax4Label(String companyMaintenance_Contactfax4Label) {
		CompanyMaintenance_Contactfax4Label = companyMaintenance_Contactfax4Label;
	}
	public String getCompanyMaintenance_Contactemail4Label() {
		return CompanyMaintenance_Contactemail4Label;
	}
	public void setCompanyMaintenance_Contactemail4Label(String companyMaintenance_Contactemail4Label) {
		CompanyMaintenance_Contactemail4Label = companyMaintenance_Contactemail4Label;
	}
	public String getCompanyMaintenance_MainfaxLabel() {
		return CompanyMaintenance_MainfaxLabel;
	}
	public void setCompanyMaintenance_MainfaxLabel(String companyMaintenance_MainfaxLabel) {
		CompanyMaintenance_MainfaxLabel = companyMaintenance_MainfaxLabel;
	}
	public String getCompanyMaintenance_MainemailLabel() {
		return CompanyMaintenance_MainemailLabel;
	}
	public void setCompanyMaintenance_MainemailLabel(String companyMaintenance_MainemailLabel) {
		CompanyMaintenance_MainemailLabel = companyMaintenance_MainemailLabel;
	}
	public String getCompanyMaintenance_BillingcontrolnumberLabel() {
		return CompanyMaintenance_BillingcontrolnumberLabel;
	}
	public void setCompanyMaintenance_BillingcontrolnumberLabel(String companyMaintenance_BillingcontrolnumberLabel) {
		CompanyMaintenance_BillingcontrolnumberLabel = companyMaintenance_BillingcontrolnumberLabel;
	}
	public String getCompanyMaintenance_ThememobileLabel() {
		return CompanyMaintenance_ThememobileLabel;
	}
	public void setCompanyMaintenance_ThememobileLabel(String companyMaintenance_ThememobileLabel) {
		CompanyMaintenance_ThememobileLabel = companyMaintenance_ThememobileLabel;
	}
	public String getCompanyMaintenance_ThemerfLabel() {
		return CompanyMaintenance_ThemerfLabel;
	}
	public void setCompanyMaintenance_ThemerfLabel(String companyMaintenance_ThemerfLabel) {
		CompanyMaintenance_ThemerfLabel = companyMaintenance_ThemerfLabel;
	}
	public String getCompanyMaintenance_ThemefulldisplayLabel() {
		return CompanyMaintenance_ThemefulldisplayLabel;
	}
	public void setCompanyMaintenance_ThemefulldisplayLabel(String companyMaintenance_ThemefulldisplayLabel) {
		CompanyMaintenance_ThemefulldisplayLabel = companyMaintenance_ThemefulldisplayLabel;
	}
	public String getCompanyMaintenance_LogoLabel() {
		return CompanyMaintenance_LogoLabel;
	}
	public void setCompanyMaintenance_LogoLabel(String companyMaintenance_LogoLabel) {
		CompanyMaintenance_LogoLabel = companyMaintenance_LogoLabel;
	}
	public String getCompanyMaintenance_PurchaseorderreqapprovalLabel() {
		return CompanyMaintenance_PurchaseorderreqapprovalLabel;
	}
	public void setCompanyMaintenance_PurchaseorderreqapprovalLabel(String companyMaintenance_PurchaseorderreqapprovalLabel) {
		CompanyMaintenance_PurchaseorderreqapprovalLabel = companyMaintenance_PurchaseorderreqapprovalLabel;
	}
	public String getCompanyMaintenance_StatusLabel() {
		return CompanyMaintenance_StatusLabel;
	}
	public void setCompanyMaintenance_StatusLabel(String companyMaintenance_StatusLabel) {
		CompanyMaintenance_StatusLabel = companyMaintenance_StatusLabel;
	}
	public String getCompanyMaintenance_SetupselectedLabel() {
		return CompanyMaintenance_SetupselectedLabel;
	}
	public void setCompanyMaintenance_SetupselectedLabel(String companyMaintenance_SetupselectedLabel) {
		CompanyMaintenance_SetupselectedLabel = companyMaintenance_SetupselectedLabel;
	}
	public String getCompanyMaintenance_SetupdateLabel() {
		return CompanyMaintenance_SetupdateLabel;
	}
	public void setCompanyMaintenance_SetupdateLabel(String companyMaintenance_SetupdateLabel) {
		CompanyMaintenance_SetupdateLabel = companyMaintenance_SetupdateLabel;
	}
	public String getCompanyMaintenance_SetupbyLabel() {
		return CompanyMaintenance_SetupbyLabel;
	}
	public void setCompanyMaintenance_SetupbyLabel(String companyMaintenance_SetupbyLabel) {
		CompanyMaintenance_SetupbyLabel = companyMaintenance_SetupbyLabel;
	}
	public String getCompanyMaintenance_TimezoneLabel() {
		return CompanyMaintenance_TimezoneLabel;
	}
	public void setCompanyMaintenance_TimezoneLabel(String companyMaintenance_TimezoneLabel) {
		CompanyMaintenance_TimezoneLabel = companyMaintenance_TimezoneLabel;
	}
	public String getCompanyMaintenance_LastactivitydateLabel() {
		return CompanyMaintenance_LastactivitydateLabel;
	}
	public void setCompanyMaintenance_LastactivitydateLabel(String companyMaintenance_LastactivitydateLabel) {
		CompanyMaintenance_LastactivitydateLabel = companyMaintenance_LastactivitydateLabel;
	}
	public String getCompanyMaintenance_LastactivityteammemberLabel() {
		return CompanyMaintenance_LastactivityteammemberLabel;
	}
	public void setCompanyMaintenance_LastactivityteammemberLabel(String companyMaintenance_LastactivityteammemberLabel) {
		CompanyMaintenance_LastactivityteammemberLabel = companyMaintenance_LastactivityteammemberLabel;
	}
	public String getCompanyMaintenance_ButtonAddNewText() {
		return CompanyMaintenance_ButtonAddNewText;
	}
	public void setCompanyMaintenance_ButtonAddNewText(String companyMaintenance_ButtonAddNewText) {
		CompanyMaintenance_ButtonAddNewText = companyMaintenance_ButtonAddNewText;
	}
	public String getCompanyMaintenance_ButtonCancelText() {
		return CompanyMaintenance_ButtonCancelText;
	}
	public void setCompanyMaintenance_ButtonCancelText(String companyMaintenance_ButtonCancelText) {
		CompanyMaintenance_ButtonCancelText = companyMaintenance_ButtonCancelText;
	}
	public String getCompanyMaintenance_ButtonActionText() {
		return CompanyMaintenance_ButtonActionText;
	}
	public void setCompanyMaintenance_ButtonActionText(String companyMaintenance_ButtonActionText) {
		CompanyMaintenance_ButtonActionText = companyMaintenance_ButtonActionText;
	}
	public String getCompanyMaintenance_ButtonDeleteText() {
		return CompanyMaintenance_ButtonDeleteText;
	}
	public void setCompanyMaintenance_ButtonDeleteText(String companyMaintenance_ButtonDeleteText) {
		CompanyMaintenance_ButtonDeleteText = companyMaintenance_ButtonDeleteText;
	}
	public String getCompanyMaintenance_ButtonEditText() {
		return CompanyMaintenance_ButtonEditText;
	}
	public void setCompanyMaintenance_ButtonEditText(String companyMaintenance_ButtonEditText) {
		CompanyMaintenance_ButtonEditText = companyMaintenance_ButtonEditText;
	}
	public String getCompanyMaintenance_ButtonDisplayAllText() {
		return CompanyMaintenance_ButtonDisplayAllText;
	}
	public void setCompanyMaintenance_ButtonDisplayAllText(String companyMaintenance_ButtonDisplayAllText) {
		CompanyMaintenance_ButtonDisplayAllText = companyMaintenance_ButtonDisplayAllText;
	}
	public String getCompanyMaintenance_LookupLabel() {
		return CompanyMaintenance_LookupLabel;
	}
	public void setCompanyMaintenance_LookupLabel(String companyMaintenance_LookupLabel) {
		CompanyMaintenance_LookupLabel = companyMaintenance_LookupLabel;
	}
	public String getCompanyMaintenance_MaintenanceLabel() {
		return CompanyMaintenance_MaintenanceLabel;
	}
	public void setCompanyMaintenance_MaintenanceLabel(String companyMaintenance_MaintenanceLabel) {
		CompanyMaintenance_MaintenanceLabel = companyMaintenance_MaintenanceLabel;
	}
	public String getCompanyMaintenance_SearchLookupLabel() {
		return CompanyMaintenance_SearchLookupLabel;
	}
	public void setCompanyMaintenance_SearchLookupLabel(String companyMaintenance_SearchLookupLabel) {
		CompanyMaintenance_SearchLookupLabel = companyMaintenance_SearchLookupLabel;
	}
	public String getCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK() {
		return CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public void setCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK(
			String companyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK) {
		CompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK = companyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public String getCompanyMaintenance_ERR_ALL_FIELDS_LEFT_BLANK() {
		return CompanyMaintenance_ERR_ALL_FIELDS_LEFT_BLANK;
	}
	public void setCompanyMaintenance_ERR_ALL_FIELDS_LEFT_BLANK(String companyMaintenance_ERR_ALL_FIELDS_LEFT_BLANK) {
		CompanyMaintenance_ERR_ALL_FIELDS_LEFT_BLANK = companyMaintenance_ERR_ALL_FIELDS_LEFT_BLANK;
	}
	public String getCompanyMaintenance_ERR_IF_INVALID_COMPANY() {
		return CompanyMaintenance_ERR_IF_INVALID_COMPANY;
	}
	public void setCompanyMaintenance_ERR_IF_INVALID_COMPANY(String companyMaintenance_ERR_IF_INVALID_COMPANY) {
		CompanyMaintenance_ERR_IF_INVALID_COMPANY = companyMaintenance_ERR_IF_INVALID_COMPANY;
	}
	public String getCompanyMaintenance_ERR_IF_ADDRESSLINE1_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_ADDRESSLINE1_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_ADDRESSLINE1_EXCEEDED(String companyMaintenance_ERR_IF_ADDRESSLINE1_EXCEEDED) {
		CompanyMaintenance_ERR_IF_ADDRESSLINE1_EXCEEDED = companyMaintenance_ERR_IF_ADDRESSLINE1_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_ADDRESSLINE2_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_ADDRESSLINE2_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_ADDRESSLINE2_EXCEEDED(String companyMaintenance_ERR_IF_ADDRESSLINE2_EXCEEDED) {
		CompanyMaintenance_ERR_IF_ADDRESSLINE2_EXCEEDED = companyMaintenance_ERR_IF_ADDRESSLINE2_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_ADDRESSLINE3_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_ADDRESSLINE3_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_ADDRESSLINE3_EXCEEDED(String companyMaintenance_ERR_IF_ADDRESSLINE3_EXCEEDED) {
		CompanyMaintenance_ERR_IF_ADDRESSLINE3_EXCEEDED = companyMaintenance_ERR_IF_ADDRESSLINE3_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_ADDRESSLINE4_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_ADDRESSLINE4_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_ADDRESSLINE4_EXCEEDED(String companyMaintenance_ERR_IF_ADDRESSLINE4_EXCEEDED) {
		CompanyMaintenance_ERR_IF_ADDRESSLINE4_EXCEEDED = companyMaintenance_ERR_IF_ADDRESSLINE4_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_CITY_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_CITY_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_CITY_EXCEEDED(String companyMaintenance_ERR_IF_CITY_EXCEEDED) {
		CompanyMaintenance_ERR_IF_CITY_EXCEEDED = companyMaintenance_ERR_IF_CITY_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE1_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE1_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE1_EXCEEDED(
			String companyMaintenance_ERR_IF_BillTo_ADDRESSLINE1_EXCEEDED) {
		CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE1_EXCEEDED = companyMaintenance_ERR_IF_BillTo_ADDRESSLINE1_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE2_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE2_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE2_EXCEEDED(
			String companyMaintenance_ERR_IF_BillTo_ADDRESSLINE2_EXCEEDED) {
		CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE2_EXCEEDED = companyMaintenance_ERR_IF_BillTo_ADDRESSLINE2_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE3_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE3_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE3_EXCEEDED(
			String companyMaintenance_ERR_IF_BillTo_ADDRESSLINE3_EXCEEDED) {
		CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE3_EXCEEDED = companyMaintenance_ERR_IF_BillTo_ADDRESSLINE3_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE4_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE4_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE4_EXCEEDED(
			String companyMaintenance_ERR_IF_BillTo_ADDRESSLINE4_EXCEEDED) {
		CompanyMaintenance_ERR_IF_BillTo_ADDRESSLINE4_EXCEEDED = companyMaintenance_ERR_IF_BillTo_ADDRESSLINE4_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_IF_BillTo_CITY_EXCEEDED() {
		return CompanyMaintenance_ERR_IF_BillTo_CITY_EXCEEDED;
	}
	public void setCompanyMaintenance_ERR_IF_BillTo_CITY_EXCEEDED(String companyMaintenance_ERR_IF_BillTo_CITY_EXCEEDED) {
		CompanyMaintenance_ERR_IF_BillTo_CITY_EXCEEDED = companyMaintenance_ERR_IF_BillTo_CITY_EXCEEDED;
	}
	public String getCompanyMaintenance_ERR_INVALID_PART_OF_COMPANY() {
		return CompanyMaintenance_ERR_INVALID_PART_OF_COMPANY;
	}
	public void setCompanyMaintenance_ERR_INVALID_PART_OF_COMPANY(String companyMaintenance_ERR_INVALID_PART_OF_COMPANY) {
		CompanyMaintenance_ERR_INVALID_PART_OF_COMPANY = companyMaintenance_ERR_INVALID_PART_OF_COMPANY;
	}
	public String getCompanyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME() {
		return CompanyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME;
	}
	public void setCompanyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME(
			String companyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME) {
		CompanyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME = companyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME;
	}
	public String getCompanyMaintenance_ERR_WHILE_DELETING_COMPANY_IF_USED_IN_OTHER_TABLE() {
		return CompanyMaintenance_ERR_WHILE_DELETING_COMPANY_IF_USED_IN_OTHER_TABLE;
	}
	public void setCompanyMaintenance_ERR_WHILE_DELETING_COMPANY_IF_USED_IN_OTHER_TABLE(
			String companyMaintenance_ERR_WHILE_DELETING_COMPANY_IF_USED_IN_OTHER_TABLE) {
		CompanyMaintenance_ERR_WHILE_DELETING_COMPANY_IF_USED_IN_OTHER_TABLE = companyMaintenance_ERR_WHILE_DELETING_COMPANY_IF_USED_IN_OTHER_TABLE;
	}
	public String getCompanyMaintenance_ON_SAVE_SUCCESSFULLY() {
		return CompanyMaintenance_ON_SAVE_SUCCESSFULLY;
	}
	public void setCompanyMaintenance_ON_SAVE_SUCCESSFULLY(String companyMaintenance_ON_SAVE_SUCCESSFULLY) {
		CompanyMaintenance_ON_SAVE_SUCCESSFULLY = companyMaintenance_ON_SAVE_SUCCESSFULLY;
	}
	public String getCompanyMaintenance_ON_UPDATE_SUCCESSFULLY() {
		return CompanyMaintenance_ON_UPDATE_SUCCESSFULLY;
	}
	public void setCompanyMaintenance_ON_UPDATE_SUCCESSFULLY(String companyMaintenance_ON_UPDATE_SUCCESSFULLY) {
		CompanyMaintenance_ON_UPDATE_SUCCESSFULLY = companyMaintenance_ON_UPDATE_SUCCESSFULLY;
	}
	public String getCompanyMaintenance_ERR_WHILE_DELETING_COMPANY() {
		return CompanyMaintenance_ERR_WHILE_DELETING_COMPANY;
	}
	public void setCompanyMaintenance_ERR_WHILE_DELETING_COMPANY(String companyMaintenance_ERR_WHILE_DELETING_COMPANY) {
		CompanyMaintenance_ERR_WHILE_DELETING_COMPANY = companyMaintenance_ERR_WHILE_DELETING_COMPANY;
	}
	public String getCompanyMaintenance_ERR_COMPANY_ALREADY_USED() {
		return CompanyMaintenance_ERR_COMPANY_ALREADY_USED;
	}
	public void setCompanyMaintenance_ERR_COMPANY_ALREADY_USED(String companyMaintenance_ERR_COMPANY_ALREADY_USED) {
		CompanyMaintenance_ERR_COMPANY_ALREADY_USED = companyMaintenance_ERR_COMPANY_ALREADY_USED;
	}
	
	
	
	
	
	
	
	


}
