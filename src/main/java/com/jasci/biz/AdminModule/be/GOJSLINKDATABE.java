/**
 
Date Developed  July 10 2014
Description pojo class of GOJS LINK data in which have getter and setter methods.
Created By Shailendra Rajput
 */
package com.jasci.biz.AdminModule.be;

public class GOJSLINKDATABE {
	
	
	private String From;
	private String To;
	private String FromPort;
	private String ToPort;
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getFromPort() {
		return FromPort;
	}
	public void setFromPort(String fromPort) {
		FromPort = fromPort;
	}
	public String getToPort() {
		return ToPort;
	}
	public void setToPort(String toPort) {
		ToPort = toPort;
	}

}
