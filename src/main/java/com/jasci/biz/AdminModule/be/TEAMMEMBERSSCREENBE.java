/*
Description It is used to make setter and getter for changing text of screen in another language
Created By :Rahul Kumar
Created Date Nov 24 2014
 */

package com.jasci.biz.AdminModule.be;

public class TEAMMEMBERSSCREENBE {

	private String	TeamMemberMaintenance_ButtonResetText;
	private String	TeamMemberMaintenance_TeamMemberMaintenance;
	private String	TeamMemberMaintenance_EnterTeamMember;
	private String	TeamMemberMaintenance_PartOfTheTeamMemberName;
	private String	TeamMemberMaintenance_New;
	private String	TeamMemberMaintenance_Search;
	private String	TeamMemberMaintenance_DisplayAll;
	private String	TeamMemberMaintenance_TeamMemberMaintenanceNew;
	private String	TeamMemberMaintenance_TeamMemberId;
	private String	TeamMemberMaintenance_SetUpDate;
	private String	TeamMemberMaintenance_SetupBy;
	private String	TeamMemberMaintenance_LastActivityDate;
	private String	TeamMemberMaintenance_LastActivityBy;
	private String	TeamMemberMaintenance_CurrentStatus;
	private String	TeamMemberMaintenance_FirstName;
	private String	TeamMemberMaintenance_LastName;
	private String	TeamMemberMaintenance_MiddleName;
	private String	TeamMemberMaintenance_AddressLine1;
	private String	TeamMemberMaintenance_AddressLine2;
	private String	TeamMemberMaintenance_AddressLine3;
	private String	TeamMemberMaintenance_AddressLine4;
	private String	TeamMemberMaintenance_City;
	private String	TeamMemberMaintenance_CountryCode;
	private String	TeamMemberMaintenance_State;
	private String	TeamMemberMaintenance_ZipCode;
	private String	TeamMemberMaintenance_WorkPhone;
	private String	TeamMemberMaintenance_WorkExtension;
	private String	TeamMemberMaintenance_WorkCell;
	private String	TeamMemberMaintenance_WorkFax;
	private String	TeamMemberMaintenance_WorkEmail;
	private String	TeamMemberMaintenance_HomePhone;
	private String	TeamMemberMaintenance_Cell;
	private String	TeamMemberMaintenance_Email;
	private String	TeamMemberMaintenance_EmergencyContactName;
	private String	TeamMemberMaintenance_EmergencyHomePhone;
	private String	TeamMemberMaintenance_EmergencyCell;
	private String	TeamMemberMaintenance_EmergencyEmial;
	private String	TeamMemberMaintenance_DepartMent;
	private String	TeamMemberMaintenance_ShiftCode;
	private String	TeamMemberMaintenance_Language;
	private String	TeamMemberMaintenance_StartDate;
	private String	TeamMemberMaintenance_LastDate;
	private String	TeamMemberMaintenance_Company;
	private String	TeamMemberMaintenance_CompanyName;
	private String	TeamMemberMaintenance_Save;
	private String	TeamMemberMaintenance_Notes;
	private String	TeamMemberMaintenance_SearchLooup;
	private String	TeamMemberMaintenance_Name;
	private String	TeamMemberMaintenance_TeamMember;
	private String	TeamMemberMaintenance_Action;
	private String	TeamMemberMaintenance_Delete;
	private String	TeamMemberMaintenance_SortFirstName;
	private String	TeamMemberMaintenance_SortLastName;
	private String	TeamMemberMaintenance_AddNew;
	private String	TeamMemberMaintenance_Select;
	private String	TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	private String	TeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST;
	private String	TeamMemberMaintenance_ERR_INVALID_WORK_PHONE;
	private String	TeamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST;
	private String	TeamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST;
	private String	TeamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST;
	private String	TeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE;
	private String	TeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE;
	private String	TeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT;
	private String	TeamMemberMaintenance_ERR_INVALID_WORK_CELL;
	private String	TeamMemberMaintenance_ERR_INVALID_WORK_FAX;
	private String	TeamMemberMaintenance_ERR_INVALID_CELL;
	private String	TeamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE;
	private String	TeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL;
	private String	TeamMemberMaintenance_ON_SAVE_SUCCESSFULLY;
	private String	TeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY;
	private String	TeamMemberMaintenance_ERR_NO_COMPANY_SELECTED;
	private String	TeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION;
    private String	TeamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR;
    private String	TeamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER;	
    private String	TeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER;
    private	String	TeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME;
    private String	TeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD;    
    private String  TeamMemberMaintenance_Edit;
    private String	TeamMemberMaintenance_SystemUse;
    private String  TeamMemberMaintenance_Cancel;
    private String  TeamMemberMaintenance_ERR_INVALID_HOME_PHONE;
    private String  TeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS;
    private String  TeamMemberMaintenance_ERR_INVALID_ZIP_CODE;
    private String  TeamMemberMaintenance_ERR_INVALID_ADDRESS;
    private String  TeamMemberMaintenance_SystemUse_Y;
    private String  TeamMemberMaintenance_SystemUse_N;
    private String	TeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE;
    private String	TeamMemberMaintenance_Status;
    private String	TeamMemberMaintenance_YY_MM_DD;
   
    private String  TeamMemberMaintenance_LeavOfAbsence;
    private String  TeamMemberMaintenance_NotActive;
    private String  TeamMemberMaintenance_Active;
    private String  TeamMemberMaintenance_StatusDelete;
    
    private String TeamMemberMaintenance_GeneralCodeId_Department;
    private String TeamMemberMaintenance_GeneralCodeId_Shift;
    private String TeamMemberMaintenance_GeneralCodeId_State;
    private String TeamMemberMaintenance_GeneralCodeId_Language;
    private String TeamMemberMaintenance_GeneralCodeId_CountryCode;
    
    private String	TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
    private String	TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;   
	
          
	
	public String getTeamMemberMaintenance_ButtonResetText() {
		return TeamMemberMaintenance_ButtonResetText;
	}
	public void setTeamMemberMaintenance_ButtonResetText(String teamMemberMaintenance_ButtonResetText) {
		TeamMemberMaintenance_ButtonResetText = teamMemberMaintenance_ButtonResetText;
	}
	public String getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS() {
		return TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;
	}
	public void setTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS(
			String teamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS) {
		TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS = teamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;
	}
	public String getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY() {
		return TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
	}
	public void setTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY(
			String teamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY) {
		TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY = teamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
	}
	public String getTeamMemberMaintenance_StatusDelete() {
		return TeamMemberMaintenance_StatusDelete;
	}
	public void setTeamMemberMaintenance_StatusDelete(String teamMemberMaintenance_StatusDelete) {
		TeamMemberMaintenance_StatusDelete = teamMemberMaintenance_StatusDelete;
	}
	public String getTeamMemberMaintenance_GeneralCodeId_Department() {
		return TeamMemberMaintenance_GeneralCodeId_Department;
	}
	public void setTeamMemberMaintenance_GeneralCodeId_Department(String teamMemberMaintenance_GeneralCodeId_Department) {
		TeamMemberMaintenance_GeneralCodeId_Department = teamMemberMaintenance_GeneralCodeId_Department;
	}
	public String getTeamMemberMaintenance_GeneralCodeId_Shift() {
		return TeamMemberMaintenance_GeneralCodeId_Shift;
	}
	public void setTeamMemberMaintenance_GeneralCodeId_Shift(String teamMemberMaintenance_GeneralCodeId_Shift) {
		TeamMemberMaintenance_GeneralCodeId_Shift = teamMemberMaintenance_GeneralCodeId_Shift;
	}
	public String getTeamMemberMaintenance_GeneralCodeId_State() {
		return TeamMemberMaintenance_GeneralCodeId_State;
	}
	public void setTeamMemberMaintenance_GeneralCodeId_State(String teamMemberMaintenance_GeneralCodeId_State) {
		TeamMemberMaintenance_GeneralCodeId_State = teamMemberMaintenance_GeneralCodeId_State;
	}
	public String getTeamMemberMaintenance_GeneralCodeId_Language() {
		return TeamMemberMaintenance_GeneralCodeId_Language;
	}
	public void setTeamMemberMaintenance_GeneralCodeId_Language(String teamMemberMaintenance_GeneralCodeId_Language) {
		TeamMemberMaintenance_GeneralCodeId_Language = teamMemberMaintenance_GeneralCodeId_Language;
	}
	public String getTeamMemberMaintenance_GeneralCodeId_CountryCode() {
		return TeamMemberMaintenance_GeneralCodeId_CountryCode;
	}
	public void setTeamMemberMaintenance_GeneralCodeId_CountryCode(String teamMemberMaintenance_GeneralCodeId_CountryCode) {
		TeamMemberMaintenance_GeneralCodeId_CountryCode = teamMemberMaintenance_GeneralCodeId_CountryCode;
	}
	public String getTeamMemberMaintenance_LeavOfAbsence() {
		return TeamMemberMaintenance_LeavOfAbsence;
	}
	public void setTeamMemberMaintenance_LeavOfAbsence(String teamMemberMaintenance_LeavOfAbsence) {
		TeamMemberMaintenance_LeavOfAbsence = teamMemberMaintenance_LeavOfAbsence;
	}
	public String getTeamMemberMaintenance_NotActive() {
		return TeamMemberMaintenance_NotActive;
	}
	public void setTeamMemberMaintenance_NotActive(String teamMemberMaintenance_NotActive) {
		TeamMemberMaintenance_NotActive = teamMemberMaintenance_NotActive;
	}
	public String getTeamMemberMaintenance_Active() {
		return TeamMemberMaintenance_Active;
	}
	public void setTeamMemberMaintenance_Active(String teamMemberMaintenance_Active) {
		TeamMemberMaintenance_Active = teamMemberMaintenance_Active;
	}
	public String getTeamMemberMaintenance_Status() {
		return TeamMemberMaintenance_Status;
	}
	public void setTeamMemberMaintenance_Status(String teamMemberMaintenance_Status) {
		TeamMemberMaintenance_Status = teamMemberMaintenance_Status;
	}
	public String getTeamMemberMaintenance_YY_MM_DD() {
		return TeamMemberMaintenance_YY_MM_DD;
	}
	public void setTeamMemberMaintenance_YY_MM_DD(String teamMemberMaintenance_YY_MM_DD) {
		TeamMemberMaintenance_YY_MM_DD = teamMemberMaintenance_YY_MM_DD;
	}
	public String getTeamMemberMaintenance_SystemUse_Y() {
		return TeamMemberMaintenance_SystemUse_Y;
	}
	public void setTeamMemberMaintenance_SystemUse_Y(String teamMemberMaintenance_SystemUse_Y) {
		TeamMemberMaintenance_SystemUse_Y = teamMemberMaintenance_SystemUse_Y;
	}
	public String getTeamMemberMaintenance_SystemUse_N() {
		return TeamMemberMaintenance_SystemUse_N;
	}
	public void setTeamMemberMaintenance_SystemUse_N(String teamMemberMaintenance_SystemUse_N) {
		TeamMemberMaintenance_SystemUse_N = teamMemberMaintenance_SystemUse_N;
	}
	public String getTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE() {
		return TeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE;
	}
	public void setTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE(
			String teamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE) {
		TeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE = teamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_ZIP_CODE() {
		return TeamMemberMaintenance_ERR_INVALID_ZIP_CODE;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_ZIP_CODE(String teamMemberMaintenance_ERR_INVALID_ZIP_CODE) {
		TeamMemberMaintenance_ERR_INVALID_ZIP_CODE = teamMemberMaintenance_ERR_INVALID_ZIP_CODE;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_ADDRESS() {
		return TeamMemberMaintenance_ERR_INVALID_ADDRESS;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_ADDRESS(String teamMemberMaintenance_ERR_INVALID_ADDRESS) {
		TeamMemberMaintenance_ERR_INVALID_ADDRESS = teamMemberMaintenance_ERR_INVALID_ADDRESS;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_HOME_PHONE() {
		return TeamMemberMaintenance_ERR_INVALID_HOME_PHONE;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_HOME_PHONE(String teamMemberMaintenance_ERR_INVALID_HOME_PHONE) {
		TeamMemberMaintenance_ERR_INVALID_HOME_PHONE = teamMemberMaintenance_ERR_INVALID_HOME_PHONE;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS() {
		return TeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS(String teamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS) {
		TeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS = teamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS;
	}
	public String getTeamMemberMaintenance_Cancel() {
		return TeamMemberMaintenance_Cancel;
	}
	public void setTeamMemberMaintenance_Cancel(String teamMemberMaintenance_Cancel) {
		TeamMemberMaintenance_Cancel = teamMemberMaintenance_Cancel;
	}
	public String getTeamMemberMaintenance_SystemUse() {
		return TeamMemberMaintenance_SystemUse;
	}
	public void setTeamMemberMaintenance_SystemUse(String teamMemberMaintenance_SystemUse) {
		TeamMemberMaintenance_SystemUse = teamMemberMaintenance_SystemUse;
	}
	public String getTeamMemberMaintenance_Edit() {
		return TeamMemberMaintenance_Edit;
	}
	public void setTeamMemberMaintenance_Edit(String teamMemberMaintenance_Edit) {
		TeamMemberMaintenance_Edit = teamMemberMaintenance_Edit;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER() {
		return TeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER(String teamMemberMaintenance_ERR_INVALID_TEAM_MEMBER) {
		TeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER = teamMemberMaintenance_ERR_INVALID_TEAM_MEMBER;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME() {
		return TeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME(
			String teamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME) {
		TeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME = teamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME;
	}
	public String getTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD() {
		return TeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD;
	}
	public void setTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD(
			String teamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD) {
		TeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD = teamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD;
	}
	public String getTeamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR() {
		return TeamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR;
	}
	public void setTeamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR(String teamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR) {
		TeamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR = teamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR;
	}
	public String getTeamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER() {
		return TeamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER;
	}
	public void setTeamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER(
			String teamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER) {
		TeamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER = teamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER;
	}
	public String getTeamMemberMaintenance_TeamMemberMaintenance() {
		return TeamMemberMaintenance_TeamMemberMaintenance;
	}
	public void setTeamMemberMaintenance_TeamMemberMaintenance(String teamMemberMaintenance_TeamMemberMaintenance) {
		TeamMemberMaintenance_TeamMemberMaintenance = teamMemberMaintenance_TeamMemberMaintenance;
	}
	public String getTeamMemberMaintenance_EnterTeamMember() {
		return TeamMemberMaintenance_EnterTeamMember;
	}
	public void setTeamMemberMaintenance_EnterTeamMember(String teamMemberMaintenance_EnterTeamMember) {
		TeamMemberMaintenance_EnterTeamMember = teamMemberMaintenance_EnterTeamMember;
	}
	public String getTeamMemberMaintenance_PartOfTheTeamMemberName() {
		return TeamMemberMaintenance_PartOfTheTeamMemberName;
	}
	public void setTeamMemberMaintenance_PartOfTheTeamMemberName(String teamMemberMaintenance_PartOfTheTeamMemberName) {
		TeamMemberMaintenance_PartOfTheTeamMemberName = teamMemberMaintenance_PartOfTheTeamMemberName;
	}
	public String getTeamMemberMaintenance_New() {
		return TeamMemberMaintenance_New;
	}
	public void setTeamMemberMaintenance_New(String teamMemberMaintenance_New) {
		TeamMemberMaintenance_New = teamMemberMaintenance_New;
	}
	public String getTeamMemberMaintenance_Search() {
		return TeamMemberMaintenance_Search;
	}
	public void setTeamMemberMaintenance_Search(String teamMemberMaintenance_Search) {
		TeamMemberMaintenance_Search = teamMemberMaintenance_Search;
	}
	public String getTeamMemberMaintenance_DisplayAll() {
		return TeamMemberMaintenance_DisplayAll;
	}
	public void setTeamMemberMaintenance_DisplayAll(String teamMemberMaintenance_DisplayAll) {
		TeamMemberMaintenance_DisplayAll = teamMemberMaintenance_DisplayAll;
	}
	public String getTeamMemberMaintenance_TeamMemberMaintenanceNew() {
		return TeamMemberMaintenance_TeamMemberMaintenanceNew;
	}
	public void setTeamMemberMaintenance_TeamMemberMaintenanceNew(String teamMemberMaintenance_TeamMemberMaintenanceNew) {
		TeamMemberMaintenance_TeamMemberMaintenanceNew = teamMemberMaintenance_TeamMemberMaintenanceNew;
	}
	public String getTeamMemberMaintenance_TeamMemberId() {
		return TeamMemberMaintenance_TeamMemberId;
	}
	public void setTeamMemberMaintenance_TeamMemberId(String teamMemberMaintenance_TeamMemberId) {
		TeamMemberMaintenance_TeamMemberId = teamMemberMaintenance_TeamMemberId;
	}
	public String getTeamMemberMaintenance_SetUpDate() {
		return TeamMemberMaintenance_SetUpDate;
	}
	public void setTeamMemberMaintenance_SetUpDate(String teamMemberMaintenance_SetUpDate) {
		TeamMemberMaintenance_SetUpDate = teamMemberMaintenance_SetUpDate;
	}
	public String getTeamMemberMaintenance_SetupBy() {
		return TeamMemberMaintenance_SetupBy;
	}
	public void setTeamMemberMaintenance_SetupBy(String teamMemberMaintenance_SetupBy) {
		TeamMemberMaintenance_SetupBy = teamMemberMaintenance_SetupBy;
	}
	public String getTeamMemberMaintenance_LastActivityDate() {
		return TeamMemberMaintenance_LastActivityDate;
	}
	public void setTeamMemberMaintenance_LastActivityDate(String teamMemberMaintenance_LastActivityDate) {
		TeamMemberMaintenance_LastActivityDate = teamMemberMaintenance_LastActivityDate;
	}
	public String getTeamMemberMaintenance_LastActivityBy() {
		return TeamMemberMaintenance_LastActivityBy;
	}
	public void setTeamMemberMaintenance_LastActivityBy(String teamMemberMaintenance_LastActivityBy) {
		TeamMemberMaintenance_LastActivityBy = teamMemberMaintenance_LastActivityBy;
	}
	public String getTeamMemberMaintenance_CurrentStatus() {
		return TeamMemberMaintenance_CurrentStatus;
	}
	public void setTeamMemberMaintenance_CurrentStatus(String teamMemberMaintenance_CurrentStatus) {
		TeamMemberMaintenance_CurrentStatus = teamMemberMaintenance_CurrentStatus;
	}
	public String getTeamMemberMaintenance_FirstName() {
		return TeamMemberMaintenance_FirstName;
	}
	public void setTeamMemberMaintenance_FirstName(String teamMemberMaintenance_FirstName) {
		TeamMemberMaintenance_FirstName = teamMemberMaintenance_FirstName;
	}
	public String getTeamMemberMaintenance_LastName() {
		return TeamMemberMaintenance_LastName;
	}
	public void setTeamMemberMaintenance_LastName(String teamMemberMaintenance_LastName) {
		TeamMemberMaintenance_LastName = teamMemberMaintenance_LastName;
	}
	public String getTeamMemberMaintenance_MiddleName() {
		return TeamMemberMaintenance_MiddleName;
	}
	public void setTeamMemberMaintenance_MiddleName(String teamMemberMaintenance_MiddleName) {
		TeamMemberMaintenance_MiddleName = teamMemberMaintenance_MiddleName;
	}
	public String getTeamMemberMaintenance_AddressLine1() {
		return TeamMemberMaintenance_AddressLine1;
	}
	public void setTeamMemberMaintenance_AddressLine1(String teamMemberMaintenance_AddressLine1) {
		TeamMemberMaintenance_AddressLine1 = teamMemberMaintenance_AddressLine1;
	}
	public String getTeamMemberMaintenance_AddressLine2() {
		return TeamMemberMaintenance_AddressLine2;
	}
	public void setTeamMemberMaintenance_AddressLine2(String teamMemberMaintenance_AddressLine2) {
		TeamMemberMaintenance_AddressLine2 = teamMemberMaintenance_AddressLine2;
	}
	public String getTeamMemberMaintenance_AddressLine3() {
		return TeamMemberMaintenance_AddressLine3;
	}
	public void setTeamMemberMaintenance_AddressLine3(String teamMemberMaintenance_AddressLine3) {
		TeamMemberMaintenance_AddressLine3 = teamMemberMaintenance_AddressLine3;
	}
	public String getTeamMemberMaintenance_AddressLine4() {
		return TeamMemberMaintenance_AddressLine4;
	}
	public void setTeamMemberMaintenance_AddressLine4(String teamMemberMaintenance_AddressLine4) {
		TeamMemberMaintenance_AddressLine4 = teamMemberMaintenance_AddressLine4;
	}
	public String getTeamMemberMaintenance_City() {
		return TeamMemberMaintenance_City;
	}
	public void setTeamMemberMaintenance_City(String teamMemberMaintenance_City) {
		TeamMemberMaintenance_City = teamMemberMaintenance_City;
	}
	public String getTeamMemberMaintenance_CountryCode() {
		return TeamMemberMaintenance_CountryCode;
	}
	public void setTeamMemberMaintenance_CountryCode(String teamMemberMaintenance_CountryCode) {
		TeamMemberMaintenance_CountryCode = teamMemberMaintenance_CountryCode;
	}
	public String getTeamMemberMaintenance_State() {
		return TeamMemberMaintenance_State;
	}
	public void setTeamMemberMaintenance_State(String teamMemberMaintenance_State) {
		TeamMemberMaintenance_State = teamMemberMaintenance_State;
	}
	public String getTeamMemberMaintenance_ZipCode() {
		return TeamMemberMaintenance_ZipCode;
	}
	public void setTeamMemberMaintenance_ZipCode(String teamMemberMaintenance_ZipCode) {
		TeamMemberMaintenance_ZipCode = teamMemberMaintenance_ZipCode;
	}
	public String getTeamMemberMaintenance_WorkPhone() {
		return TeamMemberMaintenance_WorkPhone;
	}
	public void setTeamMemberMaintenance_WorkPhone(String teamMemberMaintenance_WorkPhone) {
		TeamMemberMaintenance_WorkPhone = teamMemberMaintenance_WorkPhone;
	}
	public String getTeamMemberMaintenance_WorkExtension() {
		return TeamMemberMaintenance_WorkExtension;
	}
	public void setTeamMemberMaintenance_WorkExtension(String teamMemberMaintenance_WorkExtension) {
		TeamMemberMaintenance_WorkExtension = teamMemberMaintenance_WorkExtension;
	}
	public String getTeamMemberMaintenance_WorkCell() {
		return TeamMemberMaintenance_WorkCell;
	}
	public void setTeamMemberMaintenance_WorkCell(String teamMemberMaintenance_WorkCell) {
		TeamMemberMaintenance_WorkCell = teamMemberMaintenance_WorkCell;
	}
	public String getTeamMemberMaintenance_WorkFax() {
		return TeamMemberMaintenance_WorkFax;
	}
	public void setTeamMemberMaintenance_WorkFax(String teamMemberMaintenance_WorkFax) {
		TeamMemberMaintenance_WorkFax = teamMemberMaintenance_WorkFax;
	}
	public String getTeamMemberMaintenance_WorkEmail() {
		return TeamMemberMaintenance_WorkEmail;
	}
	public void setTeamMemberMaintenance_WorkEmail(String teamMemberMaintenance_WorkEmail) {
		TeamMemberMaintenance_WorkEmail = teamMemberMaintenance_WorkEmail;
	}
	public String getTeamMemberMaintenance_HomePhone() {
		return TeamMemberMaintenance_HomePhone;
	}
	public void setTeamMemberMaintenance_HomePhone(String teamMemberMaintenance_HomePhone) {
		TeamMemberMaintenance_HomePhone = teamMemberMaintenance_HomePhone;
	}
	public String getTeamMemberMaintenance_Cell() {
		return TeamMemberMaintenance_Cell;
	}
	public void setTeamMemberMaintenance_Cell(String teamMemberMaintenance_Cell) {
		TeamMemberMaintenance_Cell = teamMemberMaintenance_Cell;
	}
	public String getTeamMemberMaintenance_Email() {
		return TeamMemberMaintenance_Email;
	}
	public void setTeamMemberMaintenance_Email(String teamMemberMaintenance_Email) {
		TeamMemberMaintenance_Email = teamMemberMaintenance_Email;
	}
	public String getTeamMemberMaintenance_EmergencyContactName() {
		return TeamMemberMaintenance_EmergencyContactName;
	}
	public void setTeamMemberMaintenance_EmergencyContactName(String teamMemberMaintenance_EmergencyContactName) {
		TeamMemberMaintenance_EmergencyContactName = teamMemberMaintenance_EmergencyContactName;
	}
	public String getTeamMemberMaintenance_EmergencyHomePhone() {
		return TeamMemberMaintenance_EmergencyHomePhone;
	}
	public void setTeamMemberMaintenance_EmergencyHomePhone(String teamMemberMaintenance_EmergencyHomePhone) {
		TeamMemberMaintenance_EmergencyHomePhone = teamMemberMaintenance_EmergencyHomePhone;
	}
	public String getTeamMemberMaintenance_EmergencyCell() {
		return TeamMemberMaintenance_EmergencyCell;
	}
	public void setTeamMemberMaintenance_EmergencyCell(String teamMemberMaintenance_EmergencyCell) {
		TeamMemberMaintenance_EmergencyCell = teamMemberMaintenance_EmergencyCell;
	}
	public String getTeamMemberMaintenance_EmergencyEmial() {
		return TeamMemberMaintenance_EmergencyEmial;
	}
	public void setTeamMemberMaintenance_EmergencyEmial(String teamMemberMaintenance_EmergencyEmial) {
		TeamMemberMaintenance_EmergencyEmial = teamMemberMaintenance_EmergencyEmial;
	}
	public String getTeamMemberMaintenance_DepartMent() {
		return TeamMemberMaintenance_DepartMent;
	}
	public void setTeamMemberMaintenance_DepartMent(String teamMemberMaintenance_DepartMent) {
		TeamMemberMaintenance_DepartMent = teamMemberMaintenance_DepartMent;
	}
	public String getTeamMemberMaintenance_ShiftCode() {
		return TeamMemberMaintenance_ShiftCode;
	}
	public void setTeamMemberMaintenance_ShiftCode(String teamMemberMaintenance_ShiftCode) {
		TeamMemberMaintenance_ShiftCode = teamMemberMaintenance_ShiftCode;
	}
	public String getTeamMemberMaintenance_Language() {
		return TeamMemberMaintenance_Language;
	}
	public void setTeamMemberMaintenance_Language(String teamMemberMaintenance_Language) {
		TeamMemberMaintenance_Language = teamMemberMaintenance_Language;
	}
	public String getTeamMemberMaintenance_StartDate() {
		return TeamMemberMaintenance_StartDate;
	}
	public void setTeamMemberMaintenance_StartDate(String teamMemberMaintenance_StartDate) {
		TeamMemberMaintenance_StartDate = teamMemberMaintenance_StartDate;
	}
	public String getTeamMemberMaintenance_LastDate() {
		return TeamMemberMaintenance_LastDate;
	}
	public void setTeamMemberMaintenance_LastDate(String teamMemberMaintenance_LastDate) {
		TeamMemberMaintenance_LastDate = teamMemberMaintenance_LastDate;
	}
	public String getTeamMemberMaintenance_Company() {
		return TeamMemberMaintenance_Company;
	}
	public void setTeamMemberMaintenance_Company(String teamMemberMaintenance_Company) {
		TeamMemberMaintenance_Company = teamMemberMaintenance_Company;
	}
	public String getTeamMemberMaintenance_CompanyName() {
		return TeamMemberMaintenance_CompanyName;
	}
	public void setTeamMemberMaintenance_CompanyName(String teamMemberMaintenance_CompanyName) {
		TeamMemberMaintenance_CompanyName = teamMemberMaintenance_CompanyName;
	}
	public String getTeamMemberMaintenance_Save() {
		return TeamMemberMaintenance_Save;
	}
	public void setTeamMemberMaintenance_Save(String teamMemberMaintenance_Save) {
		TeamMemberMaintenance_Save = teamMemberMaintenance_Save;
	}
	public String getTeamMemberMaintenance_Notes() {
		return TeamMemberMaintenance_Notes;
	}
	public void setTeamMemberMaintenance_Notes(String teamMemberMaintenance_Notes) {
		TeamMemberMaintenance_Notes = teamMemberMaintenance_Notes;
	}
	public String getTeamMemberMaintenance_SearchLooup() {
		return TeamMemberMaintenance_SearchLooup;
	}
	public void setTeamMemberMaintenance_SearchLooup(String teamMemberMaintenance_SearchLooup) {
		TeamMemberMaintenance_SearchLooup = teamMemberMaintenance_SearchLooup;
	}
	public String getTeamMemberMaintenance_Name() {
		return TeamMemberMaintenance_Name;
	}
	public void setTeamMemberMaintenance_Name(String teamMemberMaintenance_Name) {
		TeamMemberMaintenance_Name = teamMemberMaintenance_Name;
	}
	public String getTeamMemberMaintenance_TeamMember() {
		return TeamMemberMaintenance_TeamMember;
	}
	public void setTeamMemberMaintenance_TeamMember(String teamMemberMaintenance_TeamMember) {
		TeamMemberMaintenance_TeamMember = teamMemberMaintenance_TeamMember;
	}
	public String getTeamMemberMaintenance_Action() {
		return TeamMemberMaintenance_Action;
	}
	public void setTeamMemberMaintenance_Action(String teamMemberMaintenance_Action) {
		TeamMemberMaintenance_Action = teamMemberMaintenance_Action;
	}
	public String getTeamMemberMaintenance_Delete() {
		return TeamMemberMaintenance_Delete;
	}
	public void setTeamMemberMaintenance_Delete(String teamMemberMaintenance_Delete) {
		TeamMemberMaintenance_Delete = teamMemberMaintenance_Delete;
	}
	public String getTeamMemberMaintenance_SortFirstName() {
		return TeamMemberMaintenance_SortFirstName;
	}
	public void setTeamMemberMaintenance_SortFirstName(String teamMemberMaintenance_SortFirstName) {
		TeamMemberMaintenance_SortFirstName = teamMemberMaintenance_SortFirstName;
	}
	public String getTeamMemberMaintenance_SortLastName() {
		return TeamMemberMaintenance_SortLastName;
	}
	public void setTeamMemberMaintenance_SortLastName(String teamMemberMaintenance_SortLastName) {
		TeamMemberMaintenance_SortLastName = teamMemberMaintenance_SortLastName;
	}
	public String getTeamMemberMaintenance_AddNew() {
		return TeamMemberMaintenance_AddNew;
	}
	public void setTeamMemberMaintenance_AddNew(String teamMemberMaintenance_AddNew) {
		TeamMemberMaintenance_AddNew = teamMemberMaintenance_AddNew;
	}
	
	
	public String getTeamMemberMaintenance_Select() {
		return TeamMemberMaintenance_Select;
	}
	public void setTeamMemberMaintenance_Select(String teamMemberMaintenance_Select) {
		TeamMemberMaintenance_Select = teamMemberMaintenance_Select;
	}
	public String getTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK() {
		return TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public void setTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK(
			String teamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK) {
		TeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK = teamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public String getTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST() {
		return TeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST;
	}
	public void setTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST(
			String teamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST) {
		TeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST = teamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_WORK_PHONE() {
		return TeamMemberMaintenance_ERR_INVALID_WORK_PHONE;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_WORK_PHONE(String teamMemberMaintenance_ERR_INVALID_WORK_PHONE) {
		TeamMemberMaintenance_ERR_INVALID_WORK_PHONE = teamMemberMaintenance_ERR_INVALID_WORK_PHONE;
	}
	public String getTeamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST() {
		return TeamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST;
	}
	public void setTeamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST(
			String teamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST) {
		TeamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST = teamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST;
	}
	public String getTeamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST() {
		return TeamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST;
	}
	public void setTeamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST(
			String teamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST) {
		TeamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST = teamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST;
	}
	public String getTeamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST() {
		return TeamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST;
	}
	public void setTeamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST(
			String teamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST) {
		TeamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST = teamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST;
	}
	public String getTeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE() {
		return TeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE;
	}
	public void setTeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE(
			String teamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE) {
		TeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE = teamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE;
	}
	public String getTeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE() {
		return TeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE;
	}
	public void setTeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE(
			String teamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE) {
		TeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE = teamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE;
	}
	public String getTeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT() {
		return TeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT;
	}
	public void setTeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT(String teamMemberMaintenance_ERR_EMAILID_NOT_CORRECT) {
		TeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT = teamMemberMaintenance_ERR_EMAILID_NOT_CORRECT;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_WORK_CELL() {
		return TeamMemberMaintenance_ERR_INVALID_WORK_CELL;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_WORK_CELL(String teamMemberMaintenance_ERR_INVALID_WORK_CELL) {
		TeamMemberMaintenance_ERR_INVALID_WORK_CELL = teamMemberMaintenance_ERR_INVALID_WORK_CELL;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_WORK_FAX() {
		return TeamMemberMaintenance_ERR_INVALID_WORK_FAX;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_WORK_FAX(String teamMemberMaintenance_ERR_INVALID_WORK_FAX) {
		TeamMemberMaintenance_ERR_INVALID_WORK_FAX = teamMemberMaintenance_ERR_INVALID_WORK_FAX;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_CELL() {
		return TeamMemberMaintenance_ERR_INVALID_CELL;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_CELL(String teamMemberMaintenance_ERR_INVALID_CELL) {
		TeamMemberMaintenance_ERR_INVALID_CELL = teamMemberMaintenance_ERR_INVALID_CELL;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE() {
		return TeamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE(
			String teamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE) {
		TeamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE = teamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL() {
		return TeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL(String teamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL) {
		TeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL = teamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL;
	}
	public String getTeamMemberMaintenance_ON_SAVE_SUCCESSFULLY() {
		return TeamMemberMaintenance_ON_SAVE_SUCCESSFULLY;
	}
	public void setTeamMemberMaintenance_ON_SAVE_SUCCESSFULLY(String teamMemberMaintenance_ON_SAVE_SUCCESSFULLY) {
		TeamMemberMaintenance_ON_SAVE_SUCCESSFULLY = teamMemberMaintenance_ON_SAVE_SUCCESSFULLY;
	}
	public String getTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY() {
		return TeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY;
	}
	public void setTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY(String teamMemberMaintenance_ON_UPDATE_SUCCESSFULLY) {
		TeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY = teamMemberMaintenance_ON_UPDATE_SUCCESSFULLY;
	}
	public String getTeamMemberMaintenance_ERR_NO_COMPANY_SELECTED() {
		return TeamMemberMaintenance_ERR_NO_COMPANY_SELECTED;
	}
	public void setTeamMemberMaintenance_ERR_NO_COMPANY_SELECTED(String teamMemberMaintenance_ERR_NO_COMPANY_SELECTED) {
		TeamMemberMaintenance_ERR_NO_COMPANY_SELECTED = teamMemberMaintenance_ERR_NO_COMPANY_SELECTED;
	}
	public String getTeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION() {
		return TeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION;
	}
	public void setTeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION(String teamMemberMaintenance_ERR_INVALID_WORK_EXTENSION) {
		TeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION = teamMemberMaintenance_ERR_INVALID_WORK_EXTENSION;
	}
	
	
	
	
	
	
	
	
}
