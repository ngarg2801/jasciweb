/**
 
Date Developed  Jan 15 2015
Description pojo class of Team Member and Assigned Profile in which getter and setter methods and mapping with table
Created By Sarvendra Tyagi

 */

package com.jasci.biz.AdminModule.be;

public class MENUPROFILEASSIGNMENTPOJOBE {
	
	private String Tenant_ID;
	private String TeamMemberID;
	private String lastName;
	private String firstName;
	private String middleName;
	private String currentStatus;
	private String profile;
	private String Name;
	private String DESCRIPTION20;
	private String DESCRIPTION50;
	private String HELP_LINE;
	private String MenuType;
	
	public String getMenuType() {
		return MenuType;
	}
	public void setMenuType(String menuType) {
		MenuType = menuType;
	}
	public String getDESCRIPTION20() {
		return DESCRIPTION20;
	}
	public void setDESCRIPTION20(String dESCRIPTION20) {
		DESCRIPTION20 = dESCRIPTION20;
	}
	public String getDESCRIPTION50() {
		return DESCRIPTION50;
	}
	public void setDESCRIPTION50(String dESCRIPTION50) {
		DESCRIPTION50 = dESCRIPTION50;
	}
	public String getHELP_LINE() {
		return HELP_LINE;
	}
	public void setHELP_LINE(String hELP_LINE) {
		HELP_LINE = hELP_LINE;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getTenant_ID() {
		return Tenant_ID;
	}
	public void setTenant_ID(String tenant_ID) {
		Tenant_ID = tenant_ID;
	}
	public String getTeamMemberID() {
		return TeamMemberID;
	}
	public void setTeamMemberID(String teamMemberID) {
		TeamMemberID = teamMemberID;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}

}
