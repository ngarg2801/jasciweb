/*
Description It is used to make setter and getter for changing text of screen in another language
Created By Aakash Bishnoi
Created Date Nov 2 2014
 */
package com.jasci.biz.AdminModule.be;
public class GENERALCODESBE {
	
	private String GeneralCodes_Select;
	private String GeneralCodes_GENERALCODES;
	private String GeneralCodes_YES;
	private String GeneralCodes_NO;
	
	private String GeneralCodes_ErrorMsg_GeneralCode_MenuOption;
	private String GeneralCodes_ErrorMsg_GeneralCode;
	private String GeneralCodes_ErrorMsg_MandatoryFields;
	private String GeneralCodes_Confirm_Delete;
	private String GeneralCodes_Confirm_Save;
	private String GeneralCodes_Confirm_Update;
	private String GeneralCodes_SYSTEM;
	public String GeneralCodes_Description_Long_is_already_Used;
	public String GeneralCodes_Description_Short_is_already_Used;
	public String GeneralCodes_Menu_Option_already_used;
	
	
	private String GeneralCodes_Reset;
	 public String getGeneralCodes_Reset() {
	  return GeneralCodes_Reset;
	 }
	 public void setGeneralCodes_Reset(String generalCodes_Reset) {
	  GeneralCodes_Reset = generalCodes_Reset;
	 }
	
	public String getGeneralCodes_Menu_Option_already_used() {
		return GeneralCodes_Menu_Option_already_used;
	}
	public void setGeneralCodes_Menu_Option_already_used(
			String generalCodes_Menu_Option_already_used) {
		GeneralCodes_Menu_Option_already_used = generalCodes_Menu_Option_already_used;
	}
	public String getGeneralCodes_Description_Short_is_already_Used() {
		return GeneralCodes_Description_Short_is_already_Used;
	}
	public void setGeneralCodes_Description_Short_is_already_Used(
			String generalCodes_Description_Short_is_already_Used) {
		GeneralCodes_Description_Short_is_already_Used = generalCodes_Description_Short_is_already_Used;
	}
	public String getGeneralCodes_Description_Long_is_already_Used() {
		return GeneralCodes_Description_Long_is_already_Used;
	}
	public void setGeneralCodes_Description_Long_is_already_Used(
			String generalCodes_Description_Long_is_already_Used) {
		GeneralCodes_Description_Long_is_already_Used = generalCodes_Description_Long_is_already_Used;
	}

	
	public String getGeneralCodes_SYSTEM() {
		return GeneralCodes_SYSTEM;
	}
	public void setGeneralCodes_SYSTEM(String generalCodes_SYSTEM) {
		GeneralCodes_SYSTEM = generalCodes_SYSTEM;
	}
	public String getGeneralCodes_Confirm_Delete() {
		return GeneralCodes_Confirm_Delete;
	}
	public void setGeneralCodes_Confirm_Delete(String generalCodes_Confirm_Delete) {
		GeneralCodes_Confirm_Delete = generalCodes_Confirm_Delete;
	}
	public String getGeneralCodes_Confirm_Save() {
		return GeneralCodes_Confirm_Save;
	}
	public void setGeneralCodes_Confirm_Save(String generalCodes_Confirm_Save) {
		GeneralCodes_Confirm_Save = generalCodes_Confirm_Save;
	}
	public String getGeneralCodes_Confirm_Update() {
		return GeneralCodes_Confirm_Update;
	}
	public void setGeneralCodes_Confirm_Update(String generalCodes_Confirm_Update) {
		GeneralCodes_Confirm_Update = generalCodes_Confirm_Update;
	}
	
	
	public String getGeneralCodes_ErrorMsg_GeneralCode_MenuOption() {
		return GeneralCodes_ErrorMsg_GeneralCode_MenuOption;
	}
	public void setGeneralCodes_ErrorMsg_GeneralCode_MenuOption(
			String generalCodes_ErrorMsg_GeneralCode_MenuOption) {
		GeneralCodes_ErrorMsg_GeneralCode_MenuOption = generalCodes_ErrorMsg_GeneralCode_MenuOption;
	}
	public String getGeneralCodes_ErrorMsg_GeneralCode() {
		return GeneralCodes_ErrorMsg_GeneralCode;
	}
	public void setGeneralCodes_ErrorMsg_GeneralCode(
			String generalCodes_ErrorMsg_GeneralCode) {
		GeneralCodes_ErrorMsg_GeneralCode = generalCodes_ErrorMsg_GeneralCode;
	}
	public String getGeneralCodes_ErrorMsg_MandatoryFields() {
		return GeneralCodes_ErrorMsg_MandatoryFields;
	}
	public void setGeneralCodes_ErrorMsg_MandatoryFields(
			String generalCodes_ErrorMsg_MandatoryFields) {
		GeneralCodes_ErrorMsg_MandatoryFields = generalCodes_ErrorMsg_MandatoryFields;
	}
	
	
	
	public String getGeneralCodes_YES() {
		return GeneralCodes_YES;
	}
	public void setGeneralCodes_YES(String generalCodes_YES) {
		GeneralCodes_YES = generalCodes_YES;
	}
	public String getGeneralCodes_NO() {
		return GeneralCodes_NO;
	}
	public void setGeneralCodes_NO(String generalCodes_NO) {
		GeneralCodes_NO = generalCodes_NO;
	}
	
	
	public String getGeneralCodes_GENERALCODES() {
		return GeneralCodes_GENERALCODES;
	}
	public void setGeneralCodes_GENERALCODES(String generalCodes_GENERALCODES) {
		GeneralCodes_GENERALCODES = generalCodes_GENERALCODES;
	}
	public String getGeneralCodes_Select() {
		return GeneralCodes_Select;
	}
	public void setGeneralCodes_Select(String generalCodes_Select) {
		GeneralCodes_Select = generalCodes_Select;
	}
	private String GeneralCodes_Home;
	public String getGeneralCodes_Home() {
		return GeneralCodes_Home;
	}
	public void setGeneralCodes_Home(String generalCodes_Home) {
		GeneralCodes_Home = generalCodes_Home;
	}
	
	public String getGeneralCodes_GeneralCode_NewEdit() {
		return GeneralCodes_GeneralCode_NewEdit;
	}
	public void setGeneralCodes_GeneralCode_NewEdit(
			String generalCodes_GeneralCode_NewEdit) {
		GeneralCodes_GeneralCode_NewEdit = generalCodes_GeneralCode_NewEdit;
	}
	public String getGeneralCodes_GeneralCode_SaveUpdate() {
		return GeneralCodes_SaveUpdate;
	}
	public void setGeneralCodes_SaveUpdate(
			String generalCodes_SaveUpdate) {
		GeneralCodes_SaveUpdate = generalCodes_SaveUpdate;
	}
	public String getGeneralCodes_Cancels() {
		return GeneralCodes_Cancels;
	}
	public void setGeneralCodes_Cancels(
			String generalCodes_Cancels) {
		GeneralCodes_Cancels = generalCodes_Cancels;
	}
	public String getGeneralCodes_Code_Identification() {
		return GeneralCodes_Code_Identification;
	}
	public void setGeneralCodes_Code_Identification(
			String generalCodes_Code_Identification) {
		GeneralCodes_Code_Identification = generalCodes_Code_Identification;
	}
	public String getGeneralCodes_Description() {
		return GeneralCodes_Description;
	}
	public void setGeneralCodes_Description(String generalCodes_Description) {
		GeneralCodes_Description = generalCodes_Description;
	}
	public String getGeneralCodes_Actions() {
		return GeneralCodes_Actions;
	}
	public void setGeneralCodes_Actions(String generalCodes_Actions) {
		GeneralCodes_Actions = generalCodes_Actions;
	}
	public String getGeneralCodes_Edit() {
		return GeneralCodes_Edit;
	}
	public void setGeneralCodes_Edit(String generalCodes_Edit) {
		GeneralCodes_Edit = generalCodes_Edit;
	}
	public String getGeneralCodes_Delete() {
		return GeneralCodes_Delete;
	}
	public void setGeneralCodes_Delete(String generalCodes_Delete) {
		GeneralCodes_Delete = generalCodes_Delete;
	}
	private String GeneralCodes_eCommerce;	
	public String getGeneralCodes_eCommerce() {
		return GeneralCodes_eCommerce;
	}
	public void setGeneralCodes_eCommerce(String generalCodes_eCommerce) {
		GeneralCodes_eCommerce = generalCodes_eCommerce;
	}
	//private String GeneralCodes_Admin;
	private String GeneralCodes_Code_Identification;
	private String GeneralCodes_Description;
	private String GeneralCodes_Actions;
	private String GeneralCodes_Edit;
	private String GeneralCodes_Delete;	
	//private String GeneralCodes_Add;
	//private String GeneralCodes_New;
	private String GeneralCodes_Company;
	private String GeneralCodes_AddNewBtn;
	//private String GeneralCodes_Tools;
	private String GeneralCodes_Application;
	private String GeneralCodes_GeneralCodeID;
	private String GeneralCodes_GeneralCode;
	private String GeneralCodes_SystemUse;
	private String GeneralCodes_Description20;
	private String GeneralCodes_Description50;
	private String GeneralCodes_MenuOptionName;
	private String GeneralCodes_Control01Description;
	private String GeneralCodes_Control01Value;
	private String GeneralCodes_Control02Description;
	private String GeneralCodes_Control02Value;
	private String GeneralCodes_Control03Description;
	private String GeneralCodes_Control03Value;
	private String GeneralCodes_Control04Description;
	private String GeneralCodes_Control04Value;
	private String GeneralCodes_Control05Description;
	private String GeneralCodes_Control05Value;
	private String GeneralCodes_Control06Description;
	private String GeneralCodes_Control06Value;
	private String GeneralCodes_Control07Description;
	private String GeneralCodes_Control07Value;
	private String GeneralCodes_Control08Description;
	private String GeneralCodes_Control08Value;
	private String GeneralCodes_Control09Description;
	private String GeneralCodes_Control09Value;
	private String GeneralCodes_Control10Description;
	private String GeneralCodes_Control10Value;
	private String GeneralCodes_Helpline;
	/*private String GeneralCodes_Helpline2;
	private String GeneralCodes_Helpline3;
	private String GeneralCodes_Helpline4;
	private String GeneralCodes_Helpline5;
*/	private String GeneralCodes_General_Code;
	private String GeneralCodes_GeneralCode_NewEdit;
	private String GeneralCodes_SaveUpdate;
	private String GeneralCodes_Cancels;
	

	public String getGeneralCodes_Company() {
		return GeneralCodes_Company;
	}
	public void setGeneralCodes_Company(String generalCodes_Company) {
		GeneralCodes_Company = generalCodes_Company;
	}
	public String getGeneralCodes_AddNewBtn() {
		return GeneralCodes_AddNewBtn;
	}
	public void setGeneralCodes_AddNewBtn(String generalCodes_AddNewBtn) {
		GeneralCodes_AddNewBtn = generalCodes_AddNewBtn;
	}
	
	public String getGeneralCodes_Application() {
		return GeneralCodes_Application;
	}
	public void setGeneralCodes_Application(String generalCodes_Application) {
		GeneralCodes_Application = generalCodes_Application;
	}
	public String getGeneralCodes_GeneralCodeID() {
		return GeneralCodes_GeneralCodeID;
	}
	public void setGeneralCodes_GeneralCodeID(String generalCodes_GeneralCodeID) {
		GeneralCodes_GeneralCodeID = generalCodes_GeneralCodeID;
	}
	public String getGeneralCodes_GeneralCode() {
		return GeneralCodes_GeneralCode;
	}
	public void setGeneralCodes_GeneralCode(String generalCodes_GeneralCode) {
		GeneralCodes_GeneralCode = generalCodes_GeneralCode;
	}
	public String getGeneralCodes_General_Code() {
		return GeneralCodes_General_Code;
	}
	public void setGeneralCodes_General_Code(String generalCodes_General_Code) {
		GeneralCodes_General_Code = generalCodes_General_Code;
	}
	public String getGeneralCodes_SystemUse() {
		return GeneralCodes_SystemUse;
	}
	public void setGeneralCodes_SystemUse(String generalCodes_SystemUse) {
		GeneralCodes_SystemUse = generalCodes_SystemUse;
	}
	public String getGeneralCodes_Description20() {
		return GeneralCodes_Description20;
	}
	public void setGeneralCodes_Description20(String generalCodes_Description20) {
		GeneralCodes_Description20 = generalCodes_Description20;
	}
	public String getGeneralCodes_Description50() {
		return GeneralCodes_Description50;
	}
	public void setGeneralCodes_Description50(String generalCodes_Description50) {
		GeneralCodes_Description50 = generalCodes_Description50;
	}
	public String getGeneralCodes_MenuOptionName() {
		return GeneralCodes_MenuOptionName;
	}
	public void setGeneralCodes_MenuOptionName(String generalCodes_MenuOptionName) {
		GeneralCodes_MenuOptionName = generalCodes_MenuOptionName;
	}
	public String getGeneralCodes_Control01Description() {
		return GeneralCodes_Control01Description;
	}
	public void setGeneralCodes_Control01Description(
			String generalCodes_Control01Description) {
		GeneralCodes_Control01Description = generalCodes_Control01Description;
	}
	public String getGeneralCodes_Control01Value() {
		return GeneralCodes_Control01Value;
	}
	public void setGeneralCodes_Control01Value(String generalCodes_Control01Value) {
		GeneralCodes_Control01Value = generalCodes_Control01Value;
	}
	public String getGeneralCodes_Control02Description() {
		return GeneralCodes_Control02Description;
	}
	public void setGeneralCodes_Control02Description(
			String generalCodes_Control02Description) {
		GeneralCodes_Control02Description = generalCodes_Control02Description;
	}
	public String getGeneralCodes_Control02Value() {
		return GeneralCodes_Control02Value;
	}
	public void setGeneralCodes_Control02Value(String generalCodes_Control02Value) {
		GeneralCodes_Control02Value = generalCodes_Control02Value;
	}
	public String getGeneralCodes_Control03Description() {
		return GeneralCodes_Control03Description;
	}
	public void setGeneralCodes_Control03Description(
			String generalCodes_Control03Description) {
		GeneralCodes_Control03Description = generalCodes_Control03Description;
	}
	public String getGeneralCodes_Control03Value() {
		return GeneralCodes_Control03Value;
	}
	public void setGeneralCodes_Control03Value(String generalCodes_Control03Value) {
		GeneralCodes_Control03Value = generalCodes_Control03Value;
	}
	public String getGeneralCodes_Control04Description() {
		return GeneralCodes_Control04Description;
	}
	public void setGeneralCodes_Control04Description(
			String generalCodes_Control04Description) {
		GeneralCodes_Control04Description = generalCodes_Control04Description;
	}
	public String getGeneralCodes_Control04Value() {
		return GeneralCodes_Control04Value;
	}
	public void setGeneralCodes_Control04Value(String generalCodes_Control04Value) {
		GeneralCodes_Control04Value = generalCodes_Control04Value;
	}
	public String getGeneralCodes_Control05Description() {
		return GeneralCodes_Control05Description;
	}
	public void setGeneralCodes_Control05Description(
			String generalCodes_Control05Description) {
		GeneralCodes_Control05Description = generalCodes_Control05Description;
	}
	public String getGeneralCodes_Control05Value() {
		return GeneralCodes_Control05Value;
	}
	public void setGeneralCodes_Control05Value(String generalCodes_Control05Value) {
		GeneralCodes_Control05Value = generalCodes_Control05Value;
	}
	public String getGeneralCodes_Control06Description() {
		return GeneralCodes_Control06Description;
	}
	public void setGeneralCodes_Control06Description(
			String generalCodes_Control06Description) {
		GeneralCodes_Control06Description = generalCodes_Control06Description;
	}
	public String getGeneralCodes_Control06Value() {
		return GeneralCodes_Control06Value;
	}
	public void setGeneralCodes_Control06Value(String generalCodes_Control06Value) {
		GeneralCodes_Control06Value = generalCodes_Control06Value;
	}
	public String getGeneralCodes_Control07Description() {
		return GeneralCodes_Control07Description;
	}
	public void setGeneralCodes_Control07Description(
			String generalCodes_Control07Description) {
		GeneralCodes_Control07Description = generalCodes_Control07Description;
	}
	public String getGeneralCodes_Control07Value() {
		return GeneralCodes_Control07Value;
	}
	public void setGeneralCodes_Control07Value(String generalCodes_Control07Value) {
		GeneralCodes_Control07Value = generalCodes_Control07Value;
	}
	public String getGeneralCodes_Control08Description() {
		return GeneralCodes_Control08Description;
	}
	public void setGeneralCodes_Control08Description(
			String generalCodes_Control08Description) {
		GeneralCodes_Control08Description = generalCodes_Control08Description;
	}
	public String getGeneralCodes_Control08Value() {
		return GeneralCodes_Control08Value;
	}
	public void setGeneralCodes_Control08Value(String generalCodes_Control08Value) {
		GeneralCodes_Control08Value = generalCodes_Control08Value;
	}
	public String getGeneralCodes_Control09Description() {
		return GeneralCodes_Control09Description;
	}
	public void setGeneralCodes_Control09Description(
			String generalCodes_Control09Description) {
		GeneralCodes_Control09Description = generalCodes_Control09Description;
	}
	public String getGeneralCodes_Control09Value() {
		return GeneralCodes_Control09Value;
	}
	public void setGeneralCodes_Control09Value(String generalCodes_Control09Value) {
		GeneralCodes_Control09Value = generalCodes_Control09Value;
	}
	public String getGeneralCodes_Control10Description() {
		return GeneralCodes_Control10Description;
	}
	public void setGeneralCodes_Control10Description(
			String generalCodes_Control10Description) {
		GeneralCodes_Control10Description = generalCodes_Control10Description;
	}
	public String getGeneralCodes_Control10Value() {
		return GeneralCodes_Control10Value;
	}
	public void setGeneralCodes_Control10Value(String generalCodes_Control10Value) {
		GeneralCodes_Control10Value = generalCodes_Control10Value;
	}
	public String getGeneralCodes_Helpline() {
		return GeneralCodes_Helpline;
	}
	public void setGeneralCodes_Helpline(String generalCodes_Helpline) {
		GeneralCodes_Helpline = generalCodes_Helpline;
	}
	/*public String getGeneralCodes_Helpline2() {
		return GeneralCodes_Helpline2;
	}
	public void setGeneralCodes_Helpline2(String generalCodes_Helpline2) {
		GeneralCodes_Helpline2 = generalCodes_Helpline2;
	}
	public String getGeneralCodes_Helpline3() {
		return GeneralCodes_Helpline3;
	}
	public void setGeneralCodes_Helpline3(String generalCodes_Helpline3) {
		GeneralCodes_Helpline3 = generalCodes_Helpline3;
	}
	public String getGeneralCodes_Helpline4() {
		return GeneralCodes_Helpline4;
	}
	public void setGeneralCodes_Helpline4(String generalCodes_Helpline4) {
		GeneralCodes_Helpline4 = generalCodes_Helpline4;
	}
	public String getGeneralCodes_Helpline5() {
		return GeneralCodes_Helpline5;
	}
	public void setGeneralCodes_Helpline5(String generalCodes_Helpline5) {
		GeneralCodes_Helpline5 = generalCodes_Helpline5;
	}
	*/
}
