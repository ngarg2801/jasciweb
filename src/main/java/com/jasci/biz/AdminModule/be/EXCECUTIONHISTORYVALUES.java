/*

Date Developed  Oct 10 2015
Description pojo class is used to make getter and setter 
Created By Shailendar kumar

 */
package com.jasci.biz.AdminModule.be;

public class EXCECUTIONHISTORYVALUES
{
	public String Name;

	public String Value;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getValue ()
    {
        return Value;
    }

    public void setValue (String Value)
    {
        this.Value = Value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Name = "+Name+", Value = "+Value+"]";
    }
}
			
		