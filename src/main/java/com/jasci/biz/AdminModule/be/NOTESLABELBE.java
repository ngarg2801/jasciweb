/*
 
Date Developed  Jan 06 2015
Description   Notes_Label
Created By Rakesh Pal
 */
package com.jasci.biz.AdminModule.be;

public class NOTESLABELBE {

	private String  Lbl_SearchLookup_title;
	private String  Lbl_Add_New_noteslookup;
	private String  Lbl_Select_noteslookup;
	private String  Lbl_Edit_noteslookup;
	private String  Lbl_Delete_noteslookup;
	private String  Lbl_Date_noteslookup;
	private String  Lbl_By_noteslookup;
	private String  Lbl_Note_noteslookup;
	private String  Lbl_NotesMaintenance_title;
	private String  Lbl_NoteID_NotesMaintenance;
	private String  Lbl_NoteLink_NotesMaintenance;
	private String  Lbl_Notes_Date_YY_MM_DD_NotesMaintenance;
	private String  Lbl_Last_Activity_By_NotesMaintenance;
	private String  Lbl_Note_NotesMaintenance;
	private String  Lbl_Save_Update_NotesMaintenance;
	private String  Lbl_Cancel_NotesMaintenance;
	private String  Lbl_note_delete_msg;
	private String  Lbl_note_save_msg;
	private String  Lbl_note_update_msg;
	private String  Lbl_note_blank_field_error_msg;
	private String  Lbl_Reset_NotesMaintenance;
	private String  Lbl_CONTINUATIONS_SEQUENCE;
	private String  Lbl_Add;
	


	
	
	public String getLbl_Add() {
		return Lbl_Add;
	}
	public void setLbl_Add(String lbl_Add) {
		Lbl_Add = lbl_Add;
	}
	public String getLbl_CONTINUATIONS_SEQUENCE() {
		return Lbl_CONTINUATIONS_SEQUENCE;
	}
	public void setLbl_CONTINUATIONS_SEQUENCE(String lbl_CONTINUATIONS_SEQUENCE) {
		Lbl_CONTINUATIONS_SEQUENCE = lbl_CONTINUATIONS_SEQUENCE;
	}
	public String getLbl_Reset_NotesMaintenance() {
		return Lbl_Reset_NotesMaintenance;
	}
	public void setLbl_Reset_NotesMaintenance(String lbl_Reset_NotesMaintenance) {
		Lbl_Reset_NotesMaintenance = lbl_Reset_NotesMaintenance;
	}
	//-------------------
	//public final String KP_SearchLookup_title="Notes";
	//public final String KP_Add_New_noteslookup="Add New";
	//public final String KP_Select_noteslookup="Actions";
	//public final String KP_Edit_noteslookup="Edit";
	//public final String KP_Delete_noteslookup="Delete";
	//public final String KP_Date_noteslookup="Last Activity Date";
	//public final String KP_By_noteslookup="Last Activity Team Member";
	//public final String KP_Note_noteslookup="Note";
	//public final String KP_NotesMaintenance_title="Notes Maintenance";
	//public final String KP_NoteID_NotesMaintenance="Note Id";
	//public final String KP_NoteLink_NotesMaintenance="Note Link";
	//public final String KP_Notes_Date_YY_MM_DD_NotesMaintenance="Last Activity Date (YYYY-MM-DD)";
	//public final String KP_Last_Activity_By_NotesMaintenance="Last Activity By";
	//public final String KP_Note_NotesMaintenance="Note";
	//public final String KP_Save_Update_NotesMaintenance="Save/Update";
	//public final String KP_Cancel_NotesMaintenance="Cancel";
	//public final String KP_Note_delete_msg="Are you sure you want to delete this record?";
	//public final String KP_Note_save_msg="Your record has been saved successfully.";
	//public final String KP_Note_update_msg="Your record has been updated successfully.";
	//public static String KP_Note_blank_field_error_msg="Note cannot be left blank.";
	//public final String KP_Reset_NotesMaintenance="Reset";


	
	
	//----------------------------
	
	public String getLbl_SearchLookup_title() {
		return Lbl_SearchLookup_title;
	}
	public void setLbl_SearchLookup_title(String lbl_SearchLookup_title) {
		Lbl_SearchLookup_title = lbl_SearchLookup_title;
	}
	public String getLbl_Add_New_noteslookup() {
		return Lbl_Add_New_noteslookup;
	}
	public void setLbl_Add_New_noteslookup(String lbl_Add_New_noteslookup) {
		Lbl_Add_New_noteslookup = lbl_Add_New_noteslookup;
	}
	public String getLbl_Select_noteslookup() {
		return Lbl_Select_noteslookup;
	}
	public void setLbl_Select_noteslookup(String lbl_Select_noteslookup) {
		Lbl_Select_noteslookup = lbl_Select_noteslookup;
	}
	public String getLbl_Edit_noteslookup() {
		return Lbl_Edit_noteslookup;
	}
	public void setLbl_Edit_noteslookup(String lbl_Edit_noteslookup) {
		Lbl_Edit_noteslookup = lbl_Edit_noteslookup;
	}
	public String getLbl_Delete_noteslookup() {
		return Lbl_Delete_noteslookup;
	}
	public void setLbl_Delete_noteslookup(String lbl_Delete_noteslookup) {
		Lbl_Delete_noteslookup = lbl_Delete_noteslookup;
	}
	public String getLbl_Date_noteslookup() {
		return Lbl_Date_noteslookup;
	}
	public void setLbl_Date_noteslookup(String lbl_Date_noteslookup) {
		Lbl_Date_noteslookup = lbl_Date_noteslookup;
	}
	public String getLbl_By_noteslookup() {
		return Lbl_By_noteslookup;
	}
	public void setLbl_By_noteslookup(String lbl_By_noteslookup) {
		Lbl_By_noteslookup = lbl_By_noteslookup;
	}
	public String getLbl_Note_noteslookup() {
		return Lbl_Note_noteslookup;
	}
	public void setLbl_Note_noteslookup(String lbl_Note_noteslookup) {
		Lbl_Note_noteslookup = lbl_Note_noteslookup;
	}
	public String getLbl_NotesMaintenance_title() {
		return Lbl_NotesMaintenance_title;
	}
	public void setLbl_NotesMaintenance_title(String lbl_NotesMaintenance_title) {
		Lbl_NotesMaintenance_title = lbl_NotesMaintenance_title;
	}
	public String getLbl_NoteID_NotesMaintenance() {
		return Lbl_NoteID_NotesMaintenance;
	}
	public void setLbl_NoteID_NotesMaintenance(String lbl_NoteID_NotesMaintenance) {
		Lbl_NoteID_NotesMaintenance = lbl_NoteID_NotesMaintenance;
	}
	public String getLbl_NoteLink_NotesMaintenance() {
		return Lbl_NoteLink_NotesMaintenance;
	}
	public void setLbl_NoteLink_NotesMaintenance(
			String lbl_NoteLink_NotesMaintenance) {
		Lbl_NoteLink_NotesMaintenance = lbl_NoteLink_NotesMaintenance;
	}
	public String getLbl_Notes_Date_YY_MM_DD_NotesMaintenance() {
		return Lbl_Notes_Date_YY_MM_DD_NotesMaintenance;
	}
	public void setLbl_Notes_Date_YY_MM_DD_NotesMaintenance(
			String lbl_Notes_Date_YY_MM_DD_NotesMaintenance) {
		Lbl_Notes_Date_YY_MM_DD_NotesMaintenance = lbl_Notes_Date_YY_MM_DD_NotesMaintenance;
	}
	public String getLbl_Last_Activity_By_NotesMaintenance() {
		return Lbl_Last_Activity_By_NotesMaintenance;
	}
	public void setLbl_Last_Activity_By_NotesMaintenance(
			String lbl_Last_Activity_By_NotesMaintenance) {
		Lbl_Last_Activity_By_NotesMaintenance = lbl_Last_Activity_By_NotesMaintenance;
	}
	public String getLbl_Note_NotesMaintenance() {
		return Lbl_Note_NotesMaintenance;
	}
	public void setLbl_Note_NotesMaintenance(String lbl_Note_NotesMaintenance) {
		Lbl_Note_NotesMaintenance = lbl_Note_NotesMaintenance;
	}
	public String getLbl_Save_Update_NotesMaintenance() {
		return Lbl_Save_Update_NotesMaintenance;
	}
	public void setLbl_Save_Update_NotesMaintenance(
			String lbl_Save_Update_NotesMaintenance) {
		Lbl_Save_Update_NotesMaintenance = lbl_Save_Update_NotesMaintenance;
	}
	public String getLbl_Cancel_NotesMaintenance() {
		return Lbl_Cancel_NotesMaintenance;
	}
	public void setLbl_Cancel_NotesMaintenance(String lbl_Cancel_NotesMaintenance) {
		Lbl_Cancel_NotesMaintenance = lbl_Cancel_NotesMaintenance;
	}
	public String getLbl_note_delete_msg() {
		return Lbl_note_delete_msg;
	}
	public void setLbl_note_delete_msg(String lbl_note_delete_msg) {
		Lbl_note_delete_msg = lbl_note_delete_msg;
	}
	public String getLbl_note_save_msg() {
		return Lbl_note_save_msg;
	}
	public void setLbl_note_save_msg(String lbl_note_save_msg) {
		Lbl_note_save_msg = lbl_note_save_msg;
	}
	public String getLbl_note_update_msg() {
		return Lbl_note_update_msg;
	}
	public void setLbl_note_update_msg(String lbl_note_update_msg) {
		Lbl_note_update_msg = lbl_note_update_msg;
	}
	
	public String getLbl_note_blank_field_error_msg() {
		return Lbl_note_blank_field_error_msg;
	}
	public void setLbl_note_blank_field_error_msg(
			String lbl_note_blank_field_error_msg) {
		Lbl_note_blank_field_error_msg = lbl_note_blank_field_error_msg;
	}

}
