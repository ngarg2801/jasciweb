/**
 
Date Developed :Dec 26 2014
Created by: Diksha Gupta
Description :This class has getter and setter for the values of location profile to show and get data to and from view respectively .
 */
package com.jasci.biz.AdminModule.be;

public class LOCATIONPROFILEBE 
{

	private String Tenant ;
	private String FulFillmentCenter ;
	private String Company ;
	private String ProfileGroup ;
	private String ProfileGroupCode; 
	private String LocationProfile ;
	private String Description20 ;
	private String Description50 ;
	private String LocationType ;
	private String LocationHeight ;
	private String LocationWidth ;
	private String LocationDepth ;
	private String LocationWeightCapacity ;
	private String LocationHeightCapacity ;
	private String NumberOfPallets ;
	private String NumberOfFloorPallet ;
	private String AllocationAllowable ;
	private String LocationCheck  ;
	private String FreeLocation ;
	private String PrimeDays ;
	private String MultipleProducts ;
	private String StorageType;
	private String Slotting ;
	private String CCActivityPoints ;
	private String CCAmount ;
	private String CCFactor;
	private String LastUsedDate ;
	private String LastUsedTeamMember ;
	private String LastActivityDate;
	private String LastActivityTeamMember ; 
	private String NumberOfLocations;
	private String Notes;
	private String Fullfillment_Center_ID_Description;
	
	public String getFullfillment_Center_ID_Description() {
		return Fullfillment_Center_ID_Description;
	}
	public void setFullfillment_Center_ID_Description(
			String fullfillment_Center_ID_Description) {
		Fullfillment_Center_ID_Description = fullfillment_Center_ID_Description;
	}
	
	public String getNotes() {
		return Notes;
	}
	public void setNotes(String notes) {
		Notes = notes;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getFulFillmentCenter() {
		return FulFillmentCenter;
	}
	public void setFulFillmentCenter(String fulFillmentCenter) {
		FulFillmentCenter = fulFillmentCenter;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getProfileGroup() {
		return ProfileGroup;
	}
	public void setProfileGroup(String profileGroup) {
		ProfileGroup = profileGroup;
	}
	public String getProfileGroupCode() {
		return ProfileGroupCode;
	}
	public void setProfileGroupCode(String profileGroupCode) {
		ProfileGroupCode = profileGroupCode;
	}
	public String getLocationProfile() {
		return LocationProfile;
	}
	public void setLocationProfile(String locationProfile) {
		LocationProfile = locationProfile;
	}
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getDescription50() {
		return Description50;
	}
	public void setDescription50(String description50) {
		Description50 = description50;
	}
	public String getLocationType() {
		return LocationType;
	}
	public void setLocationType(String locationType) {
		LocationType = locationType;
	}
	public String getLocationHeight() {
		return LocationHeight;
	}
	public void setLocationHeight(String locationHeight) {
		LocationHeight = locationHeight;
	}
	public String getLocationWidth() {
		return LocationWidth;
	}
	public void setLocationWidth(String locationWidth) {
		LocationWidth = locationWidth;
	}
	public String getLocationDepth() {
		return LocationDepth;
	}
	public void setLocationDepth(String locationDepth) {
		LocationDepth = locationDepth;
	}
	public String getLocationWeightCapacity() {
		return LocationWeightCapacity;
	}
	public void setLocationWeightCapacity(String locationWeightCapacity) {
		LocationWeightCapacity = locationWeightCapacity;
	}
	public String getLocationHeightCapacity() {
		return LocationHeightCapacity;
	}
	public void setLocationHeightCapacity(String locationHeightCapacity) {
		LocationHeightCapacity = locationHeightCapacity;
	}
	public String getNumberOfPallets() {
		return NumberOfPallets;
	}
	public void setNumberOfPallets(String numberOfPallets) {
		NumberOfPallets = numberOfPallets;
	}
	public String getNumberOfFloorPallet() {
		return NumberOfFloorPallet;
	}
	public void setNumberOfFloorPallet(String numberOfFloorPallet) {
		NumberOfFloorPallet = numberOfFloorPallet;
	}
	public String getAllocationAllowable() {
		return AllocationAllowable;
	}
	public void setAllocationAllowable(String allocationAllowable) {
		AllocationAllowable = allocationAllowable;
	}
	public String getLocationCheck() {
		return LocationCheck;
	}
	public void setLocationCheck(String locationCheck) {
		LocationCheck = locationCheck;
	}
	public String getFreeLocation() {
		return FreeLocation;
	}
	public void setFreeLocation(String freeLocation) {
		FreeLocation = freeLocation;
	}
	public String getPrimeDays() {
		return PrimeDays;
	}
	public void setPrimeDays(String primeDays) {
		PrimeDays = primeDays;
	}
	public String getMultipleProducts() {
		return MultipleProducts;
	}
	public void setMultipleProducts(String multipleProducts) {
		MultipleProducts = multipleProducts;
	}
	public String getStorageType() {
		return StorageType;
	}
	public void setStorageType(String storageType) {
		StorageType = storageType;
	}
	public String getSlotting() {
		return Slotting;
	}
	public void setSlotting(String slotting) {
		Slotting = slotting;
	}
	public String getCCActivityPoints() {
		return CCActivityPoints;
	}
	public void setCCActivityPoints(String cCActivityPoints) {
		CCActivityPoints = cCActivityPoints;
	}
	public String getCCAmount() {
		return CCAmount;
	}
	public void setCCAmount(String cCAmount) {
		CCAmount = cCAmount;
	}
	public String getCCFactor() {
		return CCFactor;
	}
	public void setCCFactor(String cCFactor) {
		CCFactor = cCFactor;
	}
	public String getLastUsedDate() {
		return LastUsedDate;
	}
	public void setLastUsedDate(String lastUsedDate) {
		LastUsedDate = lastUsedDate;
	}
	public String getLastUsedTeamMember() {
		return LastUsedTeamMember;
	}
	public void setLastUsedTeamMember(String lastUsedTeamMember) {
		LastUsedTeamMember = lastUsedTeamMember;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}
	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}
	public String getNumberOfLocations() {
		return NumberOfLocations;
	}
	public void setNumberOfLocations(String numberOfLocations) {
		NumberOfLocations = numberOfLocations;
	}
	
	
	
	
	
}
