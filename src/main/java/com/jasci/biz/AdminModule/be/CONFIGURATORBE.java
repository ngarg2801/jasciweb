/**
 
Description It is used to make setter and getter for set the value in common session due to cache issues
Created By : Shailendra Kumar
Created Date Aug 22 2015
 *
 */
package com.jasci.biz.AdminModule.be;
public class CONFIGURATORBE {

	private String StrTenant_ID_S;
	private String StrCompany_ID_S;
	private String StrExecutionSequenceName_S;
	private String StrActionName_S;
	private String StrPageName_S;
	
	public String getStrPageName_S() {
		return StrPageName_S;
	}
	public void setStrPageName_S(String strPageName_S) {
		StrPageName_S = strPageName_S;
	}
	
	public String getStrTenant_ID_S() {
		return StrTenant_ID_S;
	}
	public void setStrTenant_ID_S(String strTenant_ID_S) {
		StrTenant_ID_S = strTenant_ID_S;
	}
	public String getStrCompany_ID_S() {
		return StrCompany_ID_S;
	}
	public void setStrCompany_ID_S(String strCompany_ID_S) {
		StrCompany_ID_S = strCompany_ID_S;
	}
	public String getStrExecutionSequenceName_S() {
		return StrExecutionSequenceName_S;
	}
	public void setStrExecutionSequenceName_S(String strExecutionSequenceName_S) {
		StrExecutionSequenceName_S = strExecutionSequenceName_S;
	}
	public String getStrActionName_S() {
		return StrActionName_S;
	}
	public void setStrActionName_S(String strActionName_S) {
		StrActionName_S = strActionName_S;
	}
	
	
	
}
