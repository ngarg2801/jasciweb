/*
 
Date Developed  Nov 20 2014
Description pojo class is used to make getter and setter of Email
Created By Diksha Gupta

 */
package com.jasci.biz.AdminModule.be;



public class EMAILBE {

	private String Subject;
	private String CC;
	private String BCC;
	private String EventMessage;
	private String UserEmail;
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getCC() {
		return CC;
	}
	public void setCC(String cC) {
		CC = cC;
	}
	public String getBCC() {
		return BCC;
	}
	public void setBCC(String bCC) {
		BCC = bCC;
	}
	public String getEventMessage() {
		return EventMessage;
	}
	public void setEventMessage(String eventMessage) {
		EventMessage = eventMessage;
	}
	public String getUserEmail() {
		return UserEmail;
	}
	public void setUserEmail(String userEmail) {
		UserEmail = userEmail;
	}

	
	

}
