/*
 
Date Developed  Nov 20 2014
Description pojo class of fulfillmentcenter in which getter and setter methods and mapping with table
Created By Deepak Sharma

 */
package com.jasci.biz.AdminModule.be;

public class FULFILLMENTCENTERSCREENBE {
	  String lbl_Enter_Fulfillment;
	  String lbl_Part_of_the_Fulfillment_Center_Name;
	  String lbl_New;
	  String lbl_Display_All;
	  String lbl_Fullfillment_Centers_Lookup;
	  String lbl_Fullfillment_Centers_Search_Lookup;

	  String lbl_Fullfillment_Center;

	  String lbl_Name;

	  String lbl_Edit;
	  String lbl_Delete;
		  String lbl_Add_New;
	  String lbl_Fulfillment_Center_Maintenance;
	  String lbl_Fulfillment_Center;

	  String lbl_Set_up_Selected;
	  String lbl_Set_up_Date;
	  String lbl_Setup_By;
	  String lbl_Last_Activity_Date;
	  String lbl_Last_Activity_By;
	
	String lbl_Name_Short;

	  String lbl_Name_Long;
	  String lbl_Address_Line_1;
	  String lbl_Address_Line_2;
	  String lbl_Address_Line_3;
	  String lbl_Address_Line_4;
	  String lbl_City;
	  String lbl_Country_Code;
	  String lbl_State;
	  String lbl_Zip_Code;
	  String lbl_Contact_Name;
	  String lbl_Contact_Phone;
	
	  String lbl_Contact_Extension;
	  String lbl_Contact_Cell;
	  String lbl_Contact_Fax;
	  String lbl_Contact_Email;
	  String lbl_Contact_Name2;
	  String lbl_Contact_Phone2;
	  String lbl_Contact_Extension2;
	  String lbl_Contact_Cell2;
	  String lbl_Contact_Fax2;
	  String lbl_Contact_Email2;
	  String lbl_Contact_Name3;
	  String lbl_Contact_Phone3;
	  String lbl_Contact_Extension3;
	  String lbl_Contact_Cell3;
	  String lbl_Contact_Fax3;
	  String lbl_Contact_Email3;
	  String lbl_Contact_Name4;
	  String lbl_Contact_Phone4;
	  String lbl_Actions;
	  String lbl_Contact;
	  String lbl_Contact_Extension4;
	  String lbl_Contact_Cell4;
	  String lbl_Contact_Fax4;
	  String lbl_Contact_Email4;



	  String lbl_Main_Fax;
	  String lbl_Main_Email;




	  String lbl_Save_Update;
	  String lbl_Cancel;
	  String ERR_Mandatory_field_cannot_be_left_blank;
	  String ERR_Fulfillment_center_already_used;
	  String ERR_Your_record_has_been_saved_successfully;
	  String ERR_Your_record_has_been_updated_successfully;
	   String ERR_Please_enter_Fulfillment_center_ID;

		String ERR_Please_enter_Part_of_Fulfillment_Center_Name;
		String ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
		String ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;   
		String lbl_Button_ResetText;   
		
		String ERR_Invalid_ZipCode;
		String ERR_Invalid_Address;
		String ERR_Fulfillment_Center_ID_already_used;
		
		
		//add code
		String lbl_Name_Short_already_used;
		String lbl_Name_Long_already_used;
		
		
		
		String lbl_Select;
		   public String getLbl_Select() {
		  return lbl_Select;
		 }

		 public void setLbl_Select(String lbl_Select) {
		  this.lbl_Select = lbl_Select;
		 }
		
		public String getLbl_Button_ResetText() {
			return lbl_Button_ResetText;
		}
		
		public void setLbl_Button_ResetText(String lbl_Button_ResetText) {
			this.lbl_Button_ResetText = lbl_Button_ResetText;
		}


		public static String KP_Name_Short_already_used = "Name Short already used.";
		public static String KP_Name_Long_already_used = "Name Long already used.";
		
		
		
		public String getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS() {
			return ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;
		}
		public void setERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS(
				String eRR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS) {
			ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS = eRR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS;
		}
		public String getERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY() {
			return ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
		}
		public void setERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY(
				String eRR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY) {
			ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY = eRR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY;
		}
		public String getLbl_Name_Short_already_used() {
			return lbl_Name_Short_already_used;
		}
		public void setLbl_Name_Short_already_used(String lbl_Name_Short_already_used) {
			this.lbl_Name_Short_already_used = lbl_Name_Short_already_used;
		}
		public String getLbl_Name_Long_already_used() {
			return lbl_Name_Long_already_used;
		}
		public void setLbl_Name_Long_already_used(String lbl_Name_Long_already_used) {
			this.lbl_Name_Long_already_used = lbl_Name_Long_already_used;
		}


		
		
		
		
	 
	public String getERR_Fulfillment_Center_ID_already_used() {
			return ERR_Fulfillment_Center_ID_already_used;
		}
		public void setERR_Fulfillment_Center_ID_already_used(
				String eRR_Fulfillment_Center_ID_already_used) {
			ERR_Fulfillment_Center_ID_already_used = eRR_Fulfillment_Center_ID_already_used;
		}
	public String getERR_Invalid_ZipCode() {
			return ERR_Invalid_ZipCode;
		}
		public void setERR_Invalid_ZipCode(String eRR_Invalid_ZipCode) {
			ERR_Invalid_ZipCode = eRR_Invalid_ZipCode;
		}
		public String getERR_Invalid_Address() {
			return ERR_Invalid_Address;
		}
		public void setERR_Invalid_Address(String eRR_Invalid_Address) {
			ERR_Invalid_Address = eRR_Invalid_Address;
		}


	String lbl_Not_a_valid_number;
	  String lbl_Not_a_valid_email_id;
	
	  String ERR_Are_you_sure_you_want_to_delete_the_selected_row;
	  String ERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table;

	  String ERR_Selected_fulfillment_center_deleted_successfully;


	  String ERR_Invalid_Fulfillment_Center;
	  String ERR_Invalid_Part_of_the_fulfillment;
	  
	public String getLbl_Actions() {
		return lbl_Actions;
	}
	public void setLbl_Actions(String lbl_Actions) {
		this.lbl_Actions = lbl_Actions;
	}
	public String getLbl_Contact() {
		return lbl_Contact;
	}
	public void setLbl_Contact(String lbl_Contact) {
		this.lbl_Contact = lbl_Contact;
	}
	public String getLbl_Contact_Phone() {
		return lbl_Contact_Phone;
	}
	public void setLbl_Contact_Phone(String lbl_Contact_Phone) {
		this.lbl_Contact_Phone = lbl_Contact_Phone;
	}
	public String getLbl_Contact_Phone2() {
		return lbl_Contact_Phone2;
	}
	public void setLbl_Contact_Phone2(String lbl_Contact_Phone2) {
		this.lbl_Contact_Phone2 = lbl_Contact_Phone2;
	}
	public String getLbl_Contact_Phone3() {
		return lbl_Contact_Phone3;
	}
	public void setLbl_Contact_Phone3(String lbl_Contact_Phone3) {
		this.lbl_Contact_Phone3 = lbl_Contact_Phone3;
	}
	public String getLbl_Contact_Phone4() {
		return lbl_Contact_Phone4;
	}
	public void setLbl_Contact_Phone4(String lbl_Contact_Phone4) {
		this.lbl_Contact_Phone4 = lbl_Contact_Phone4;
	}
	
	  
	  public String getLbl_Last_Activity_By() {
		return lbl_Last_Activity_By;
	}
	public void setLbl_Last_Activity_By(String lbl_Last_Activity_By) {
		this.lbl_Last_Activity_By = lbl_Last_Activity_By;
	}
	
	 
		public String getERR_Please_enter_Fulfillment_center_ID() {
		return ERR_Please_enter_Fulfillment_center_ID;
	}
	public void setERR_Please_enter_Fulfillment_center_ID(String eRR_Please_enter_Fulfillment_center_ID) {
		ERR_Please_enter_Fulfillment_center_ID = eRR_Please_enter_Fulfillment_center_ID;
	}
	public String getERR_Please_enter_Part_of_Fulfillment_Center_Name() {
		return ERR_Please_enter_Part_of_Fulfillment_Center_Name;
	}
	public void setERR_Please_enter_Part_of_Fulfillment_Center_Name(String eRR_Please_enter_Part_of_Fulfillment_Center_Name) {
		ERR_Please_enter_Part_of_Fulfillment_Center_Name = eRR_Please_enter_Part_of_Fulfillment_Center_Name;
	}

	
	  public	  String KP_Enter_Fulfillment = "Enter Fulfillment";
	  public String KP_Part_of_the_Fulfillment_Center_Name = "Part of the Fulfillment Center Name";
	  public String KP_New = "New";
	  public String KP_Display_All = "Display All";
	 /* public String KP_Fullfillment_Centers_Lookup = "Fullfillment Centers Lookup";
	  public String KP_Fullfillment_Centers_Search_Lookup = "Fullfillment Centers Search Lookup";*/

	  public static String KP_Fullfillment_Centers_Lookup = "Fulfillment Centers Lookup";
		public static String KP_Fullfillment_Centers_Search_Lookup = "Fulfillment Centers Search Lookup";
	 // public String KP_Fullfillment_Center = "Fulfillment Center ID";
	  public static String KP_Fullfillment_Center = "Fulfillment Center ID";
	  public String KP_Contact_Phone = "Contact Phone";
	  public String KP_Contact_Phone2 = "Contact Phone 2";
	  public String KP_Contact_Phone3 = "Contact Phone 3";
	  public String KP_Contact_Phone4 = "Contact Phone 4";
	  public static String KP_Fulfillment_Center_ID_already_used="Fulfillment Center ID already used.";
	  public String KP_Name = "Name";

	  public String KP_Edit = "Edit";
	  public String KP_Delete = "Delete";
	  
		// DS 01282015
		public static String KP_Are_you_sure_you_want_to_delete_the_selected_row = "Are you sure you want to delete this record?";
		public static String KP_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table = "Selected Fulfillment Center ID cannot be deleted as it is used in another table.";

		public static String KP_Selected_fulfillment_center_deleted_successfully = "Selected Fulfillment Center ID has been deleted successfully.";

		 /* public String KP_Are_you_sure_you_want_to_delete_the_selected_row = "Are you sure you want to delete the selected row.";
		  public String KP_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table = "Selected fulfillment center cannot be deleted as it is used in another table.";

		  public String KP_Selected_fulfillment_center_deleted_successfully = "Selected fulfillment center deleted successfully.";
*/
		// END DS 01282015
		
	
	  //START DS 01222015
	 // public String KP_Invalid_Fulfillment_Center = "Invalid Fulfillment Center.";
	//  public String KP_Invalid_Part_of_the_fulfillment = "Invalid Part of the fulfillment";
	  
	public static String KP_Invalid_Part_of_the_fulfillment = "Invalid Part of Fulfillment Center Name.";
	  
	  public static String KP_Invalid_Fulfillment_Center = "Invalid Fulfillment Center ID.";
	  
		public static String KP_Invalid_Zip_Code="Invalid Zip Code.";
		public static String KP_Invalid_Address="Invalid Address.";
	  //END
	 
	  public String KP_Add_New = "Add New";
	  public static String KP_Fulfillment_Center_Maintenance = "Fulfillment Centers Maintenance";
	  public String KP_Fulfillment_Center = "Fulfillment Center ID";
      public static String KP_Fulfillment_Center_aready_exist_with_inactive_status="Fulfillment Center already exist with inactive status.";
	  public static String KP_Set_up_Selected = "Setup Selected Wizard";
	  public static String KP_Set_up_Date = "Setup Date (YYYY-MM-DD)";
	  public String KP_Setup_By = "Setup By";
	  public static String KP_Last_Activity_Date = "Last Activity Date (YYYY-MM-DD)";
	  public String KP_Last_Activity_By = "Last Activity By";
	  public String KP_Name_Short = "Name Short";

	  public String KP_Name_Long = "Name Long";
	  public String KP_Address_Line_1 = "Address Line 1";
	  public String KP_Address_Line_2 = "Address Line 2";
	  public String KP_Address_Line_3 = "Address Line 3";
	  public String KP_Address_Line_4 = "Address Line 4";
	  public String KP_City = "City";
	  public String KP_Country_Code = "Country Code";
	  public String KP_State = "State";
	  public String KP_Zip_Code = "Zip Code";
	  public String KP_Contact_Name = "Contact Name";
	  public String KP_Contact_Extension = "Contact Extension";
	  public String KP_Contact_Cell = "Contact Cell";
	  public String KP_Contact_Fax = "Contact Fax";
	  public String KP_Contact_Email = "Contact Email";
	  public String KP_Contact_Name2 = "Contact Name 2";
	  public String KP_Contact_Extension2 = "Contact Extension 2";
	  public String KP_Contact_Cell2 = "Contact Cell 2";
	  public String KP_Contact_Fax2 = "Contact Fax 2";
	  public String KP_Contact_Email2 = "Contact Email 2";
	  public String KP_Contact_Name3 = "Contact Name 3";
	  public String KP_Contact_Extension3 = "Contact Extension 3";
	  public String KP_Contact_Cell3 = "Contact Cell 3";
	  public String KP_Contact_Fax3 = "Contact Fax 3";
	  public String KP_Contact_Email3 = "Contact Email 3";
	  public String KP_Contact_Name4 = "Contact Name 4";
	  public String KP_Contact_Extension4 = "Contact Extension 4";
	  public String KP_Contact_Cell4 = "Contact Cell 4";
	  public String KP_Contact_Fax4 = "Contact Fax 4";
	  public String KP_Contact_Email4 = "Contact Email 4";

	  public String KP_Main_Fax = "Main Fax";
	  public String KP_Main_Email = "Main Email";

	  public String KP_Save_Update = "Save/Update";
	  public String KP_Cancel = "Cancel";
	  public String KP_Mandatory_field_cannot_be_left_blank = "Mandatory field cannot be left blank.";
	  public String KP_Fulfillment_center_already_used = "Fulfillment center already used.";
	  public String KP_Your_record_has_been_saved_successfully = "Your record has been saved successfully.";
	  public static String KP_Actions="Actions";
	  public static String KP_Contact="Contact";
	  public String KP_Not_a_valid_number = "Not a valid number";
	  public String KP_Not_a_valid_email_id = "Not a valid email id";	
	  public static String KP_Please_enter_only_numeric_values="Please enter only numeric values.";
	  
	  public String KP_Your_record_has_been_updated_successfully="Your record has been updated successfully.";
	  public static String KP_Please_enter_valid_Email_Address="Please enter valid Email Address.";
	  public static String KP_Please_enter_Fulfillment_center_ID="Please enter Fulfillment center ID.";
		public static String KP_Please_enter_Part_of_Fulfillment_Center_Name="Please enter Part of Fulfillment Center Name.";
	
		

String ERR_Fulfillment_Center_aready_exist_with_inactive_status;
   public String getERR_Fulfillment_Center_aready_exist_with_inactive_status() {
 return ERR_Fulfillment_Center_aready_exist_with_inactive_status;
}
public void setERR_Fulfillment_Center_aready_exist_with_inactive_status(
  String eRR_Fulfillment_Center_aready_exist_with_inactive_status) {
 ERR_Fulfillment_Center_aready_exist_with_inactive_status = eRR_Fulfillment_Center_aready_exist_with_inactive_status;
}	
	
	public String getLbl_Enter_Fulfillment() {
		return lbl_Enter_Fulfillment;
	}
	public void setLbl_Enter_Fulfillment(String lbl_Enter_Fulfillment) {
		this.lbl_Enter_Fulfillment = lbl_Enter_Fulfillment;
	}
	public String getLbl_Part_of_the_Fulfillment_Center_Name() {
		return lbl_Part_of_the_Fulfillment_Center_Name;
	}
	public void setLbl_Part_of_the_Fulfillment_Center_Name(String lbl_Part_of_the_Fulfillment_Center_Name) {
		this.lbl_Part_of_the_Fulfillment_Center_Name = lbl_Part_of_the_Fulfillment_Center_Name;
	}
	public String getLbl_New() {
		return lbl_New;
	}
	public void setLbl_New(String lbl_New) {
		this.lbl_New = lbl_New;
	}
	public String getLbl_Display_All() {
		return lbl_Display_All;
	}
	public void setLbl_Display_All(String lbl_Display_All) {
		this.lbl_Display_All = lbl_Display_All;
	}
	public String getLbl_Fullfillment_Centers_Lookup() {
		return lbl_Fullfillment_Centers_Lookup;
	}
	public void setLbl_Fullfillment_Centers_Lookup(String lbl_Fullfillment_Centers_Lookup) {
		this.lbl_Fullfillment_Centers_Lookup = lbl_Fullfillment_Centers_Lookup;
	}
	public String getLbl_Fullfillment_Centers_Search_Lookup() {
		return lbl_Fullfillment_Centers_Search_Lookup;
	}
	public void setLbl_Fullfillment_Centers_Search_Lookup(String lbl_Fullfillment_Centers_Search_Lookup) {
		this.lbl_Fullfillment_Centers_Search_Lookup = lbl_Fullfillment_Centers_Search_Lookup;
	}
	public String getLbl_Fullfillment_Center() {
		return lbl_Fullfillment_Center;
	}
	public void setLbl_Fullfillment_Center(String lbl_Fullfillment_Center) {
		this.lbl_Fullfillment_Center = lbl_Fullfillment_Center;
	}
	public String getLbl_Name() {
		return lbl_Name;
	}
	public void setLbl_Name(String lbl_Name) {
		this.lbl_Name = lbl_Name;
	}
	public String getLbl_Edit() {
		return lbl_Edit;
	}
	public void setLbl_Edit(String lbl_Edit) {
		this.lbl_Edit = lbl_Edit;
	}
	public String getLbl_Delete() {
		return lbl_Delete;
	}
	public void setLbl_Delete(String lbl_Delete) {
		this.lbl_Delete = lbl_Delete;
	}
	public String getLbl_Add_New() {
		return lbl_Add_New;
	}
	public void setLbl_Add_New(String lbl_Add_New) {
		this.lbl_Add_New = lbl_Add_New;
	}
	public String getLbl_Fulfillment_Center_Maintenance() {
		return lbl_Fulfillment_Center_Maintenance;
	}
	public void setLbl_Fulfillment_Center_Maintenance(String lbl_Fulfillment_Center_Maintenance) {
		this.lbl_Fulfillment_Center_Maintenance = lbl_Fulfillment_Center_Maintenance;
	}
	public String getLbl_Fulfillment_Center() {
		return lbl_Fulfillment_Center;
	}
	public void setLbl_Fulfillment_Center(String lbl_Fulfillment_Center) {
		this.lbl_Fulfillment_Center = lbl_Fulfillment_Center;
	}
	public String getLbl_Set_up_Selected() {
		return lbl_Set_up_Selected;
	}
	public void setLbl_Set_up_Selected(String lbl_Set_up_Selected) {
		this.lbl_Set_up_Selected = lbl_Set_up_Selected;
	}
	public String getLbl_Set_up_Date() {
		return lbl_Set_up_Date;
	}
	public void setLbl_Set_up_Date(String lbl_Set_up_Date) {
		this.lbl_Set_up_Date = lbl_Set_up_Date;
	}
	public String getLbl_Setup_By() {
		return lbl_Setup_By;
	}
	public void setLbl_Setup_By(String lbl_Setup_By) {
		this.lbl_Setup_By = lbl_Setup_By;
	}
	public String getLbl_Last_Activity_Date() {
		return lbl_Last_Activity_Date;
	}
	public void setLbl_Last_Activity_Date(String lbl_Last_Activity_Date) {
		this.lbl_Last_Activity_Date = lbl_Last_Activity_Date;
	}
	public String getLbl_Name_Short() {
		return lbl_Name_Short;
	}
	public void setLbl_Name_Short(String lbl_Name_Short) {
		this.lbl_Name_Short = lbl_Name_Short;
	}
	public String getLbl_Name_Long() {
		return lbl_Name_Long;
	}
	public void setLbl_Name_Long(String lbl_Name_Long) {
		this.lbl_Name_Long = lbl_Name_Long;
	}
	public String getLbl_Address_Line_1() {
		return lbl_Address_Line_1;
	}
	public void setLbl_Address_Line_1(String lbl_Address_Line_1) {
		this.lbl_Address_Line_1 = lbl_Address_Line_1;
	}
	public String getLbl_Address_Line_2() {
		return lbl_Address_Line_2;
	}
	public void setLbl_Address_Line_2(String lbl_Address_Line_2) {
		this.lbl_Address_Line_2 = lbl_Address_Line_2;
	}
	public String getLbl_Address_Line_3() {
		return lbl_Address_Line_3;
	}
	public void setLbl_Address_Line_3(String lbl_Address_Line_3) {
		this.lbl_Address_Line_3 = lbl_Address_Line_3;
	}
	public String getLbl_Address_Line_4() {
		return lbl_Address_Line_4;
	}
	public void setLbl_Address_Line_4(String lbl_Address_Line_4) {
		this.lbl_Address_Line_4 = lbl_Address_Line_4;
	}
	public String getLbl_City() {
		return lbl_City;
	}
	public void setLbl_City(String lbl_City) {
		this.lbl_City = lbl_City;
	}
	public String getLbl_Country_Code() {
		return lbl_Country_Code;
	}
	public void setLbl_Country_Code(String lbl_Country_Code) {
		this.lbl_Country_Code = lbl_Country_Code;
	}
	public String getLbl_State() {
		return lbl_State;
	}
	public void setLbl_State(String lbl_State) {
		this.lbl_State = lbl_State;
	}
	public String getLbl_Zip_Code() {
		return lbl_Zip_Code;
	}
	public void setLbl_Zip_Code(String lbl_Zip_Code) {
		this.lbl_Zip_Code = lbl_Zip_Code;
	}
	public String getLbl_Contact_Name() {
		return lbl_Contact_Name;
	}
	public void setLbl_Contact_Name(String lbl_Contact_Name) {
		this.lbl_Contact_Name = lbl_Contact_Name;
	}
	public String getLbl_Contact_Extension() {
		return lbl_Contact_Extension;
	}
	public void setLbl_Contact_Extension(String lbl_Contact_Extension) {
		this.lbl_Contact_Extension = lbl_Contact_Extension;
	}
	public String getLbl_Contact_Cell() {
		return lbl_Contact_Cell;
	}
	public void setLbl_Contact_Cell(String lbl_Contact_Cell) {
		this.lbl_Contact_Cell = lbl_Contact_Cell;
	}
	public String getLbl_Contact_Fax() {
		return lbl_Contact_Fax;
	}
	public void setLbl_Contact_Fax(String lbl_Contact_Fax) {
		this.lbl_Contact_Fax = lbl_Contact_Fax;
	}
	public String getLbl_Contact_Email() {
		return lbl_Contact_Email;
	}
	public void setLbl_Contact_Email(String lbl_Contact_Email) {
		this.lbl_Contact_Email = lbl_Contact_Email;
	}
	public String getLbl_Contact_Name2() {
		return lbl_Contact_Name2;
	}
	public void setLbl_Contact_Name2(String lbl_Contact_Name2) {
		this.lbl_Contact_Name2 = lbl_Contact_Name2;
	}
	public String getLbl_Contact_Extension2() {
		return lbl_Contact_Extension2;
	}
	public void setLbl_Contact_Extension2(String lbl_Contact_Extension2) {
		this.lbl_Contact_Extension2 = lbl_Contact_Extension2;
	}
	public String getLbl_Contact_Cell2() {
		return lbl_Contact_Cell2;
	}
	public void setLbl_Contact_Cell2(String lbl_Contact_Cell2) {
		this.lbl_Contact_Cell2 = lbl_Contact_Cell2;
	}
	public String getLbl_Contact_Fax2() {
		return lbl_Contact_Fax2;
	}
	public void setLbl_Contact_Fax2(String lbl_Contact_Fax2) {
		this.lbl_Contact_Fax2 = lbl_Contact_Fax2;
	}
	public String getLbl_Contact_Email2() {
		return lbl_Contact_Email2;
	}
	public void setLbl_Contact_Email2(String lbl_Contact_Email2) {
		this.lbl_Contact_Email2 = lbl_Contact_Email2;
	}
	public String getLbl_Contact_Name3() {
		return lbl_Contact_Name3;
	}
	public void setLbl_Contact_Name3(String lbl_Contact_Name3) {
		this.lbl_Contact_Name3 = lbl_Contact_Name3;
	}
	public String getLbl_Contact_Extension3() {
		return lbl_Contact_Extension3;
	}
	public void setLbl_Contact_Extension3(String lbl_Contact_Extension3) {
		this.lbl_Contact_Extension3 = lbl_Contact_Extension3;
	}
	public String getLbl_Contact_Cell3() {
		return lbl_Contact_Cell3;
	}
	public void setLbl_Contact_Cell3(String lbl_Contact_Cell3) {
		this.lbl_Contact_Cell3 = lbl_Contact_Cell3;
	}
	public String getLbl_Contact_Fax3() {
		return lbl_Contact_Fax3;
	}
	public void setLbl_Contact_Fax3(String lbl_Contact_Fax3) {
		this.lbl_Contact_Fax3 = lbl_Contact_Fax3;
	}
	public String getLbl_Contact_Email3() {
		return lbl_Contact_Email3;
	}
	public void setLbl_Contact_Email3(String lbl_Contact_Email3) {
		this.lbl_Contact_Email3 = lbl_Contact_Email3;
	}
	public String getLbl_Contact_Name4() {
		return lbl_Contact_Name4;
	}
	public void setLbl_Contact_Name4(String lbl_Contact_Name4) {
		this.lbl_Contact_Name4 = lbl_Contact_Name4;
	}
	public String getLbl_Contact_Extension4() {
		return lbl_Contact_Extension4;
	}
	public void setLbl_Contact_Extension4(String lbl_Contact_Extension4) {
		this.lbl_Contact_Extension4 = lbl_Contact_Extension4;
	}
	public String getLbl_Contact_Cell4() {
		return lbl_Contact_Cell4;
	}
	public void setLbl_Contact_Cell4(String lbl_Contact_Cell4) {
		this.lbl_Contact_Cell4 = lbl_Contact_Cell4;
	}
	public String getLbl_Contact_Fax4() {
		return lbl_Contact_Fax4;
	}
	public void setLbl_Contact_Fax4(String lbl_Contact_Fax4) {
		this.lbl_Contact_Fax4 = lbl_Contact_Fax4;
	}
	public String getLbl_Contact_Email4() {
		return lbl_Contact_Email4;
	}
	public void setLbl_Contact_Email4(String lbl_Contact_Email4) {
		this.lbl_Contact_Email4 = lbl_Contact_Email4;
	}
	public String getLbl_Main_Fax() {
		return lbl_Main_Fax;
	}
	public void setLbl_Main_Fax(String lbl_Main_Fax) {
		this.lbl_Main_Fax = lbl_Main_Fax;
	}
	public String getLbl_Main_Email() {
		return lbl_Main_Email;
	}
	public void setLbl_Main_Email(String lbl_Main_Email) {
		this.lbl_Main_Email = lbl_Main_Email;
	}
	public String getLbl_Save_Update() {
		return lbl_Save_Update;
	}
	public void setLbl_Save_Update(String lbl_Save_Update) {
		this.lbl_Save_Update = lbl_Save_Update;
	}
	public String getLbl_Cancel() {
		return lbl_Cancel;
	}
	public void setLbl_Cancel(String lbl_Cancel) {
		this.lbl_Cancel = lbl_Cancel;
	}
	public String getERR_Mandatory_field_cannot_be_left_blank() {
		return ERR_Mandatory_field_cannot_be_left_blank;
	}
	public void setERR_Mandatory_field_cannot_be_left_blank(String eRR_Mandatory_field_cannot_be_left_blank) {
		ERR_Mandatory_field_cannot_be_left_blank = eRR_Mandatory_field_cannot_be_left_blank;
	}
	public String getERR_Fulfillment_center_already_used() {
		return ERR_Fulfillment_center_already_used;
	}
	public void setERR_Fulfillment_center_already_used(String eRR_Fulfillment_center_already_used) {
		ERR_Fulfillment_center_already_used = eRR_Fulfillment_center_already_used;
	}
	public String getERR_Your_record_has_been_saved_successfully() {
		return ERR_Your_record_has_been_saved_successfully;
	}
	public void setERR_Your_record_has_been_saved_successfully(String eRR_Your_record_has_been_saved_successfully) {
		ERR_Your_record_has_been_saved_successfully = eRR_Your_record_has_been_saved_successfully;
	}
	public String getLbl_Not_a_valid_number() {
		return lbl_Not_a_valid_number;
	}
	public void setLbl_Not_a_valid_number(String lbl_Not_a_valid_number) {
		this.lbl_Not_a_valid_number = lbl_Not_a_valid_number;
	}
	public String getLbl_Not_a_valid_email_id() {
		return lbl_Not_a_valid_email_id;
	}
	public void setLbl_Not_a_valid_email_id(String lbl_Not_a_valid_email_id) {
		this.lbl_Not_a_valid_email_id = lbl_Not_a_valid_email_id;
	}
	public String getERR_Are_you_sure_you_want_to_delete_the_selected_row() {
		return ERR_Are_you_sure_you_want_to_delete_the_selected_row;
	}
	public void setERR_Are_you_sure_you_want_to_delete_the_selected_row(
			String eRR_Are_you_sure_you_want_to_delete_the_selected_row) {
		ERR_Are_you_sure_you_want_to_delete_the_selected_row = eRR_Are_you_sure_you_want_to_delete_the_selected_row;
	}
	public String getERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table() {
		return ERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table;
	}
	public void setERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table(
			String eRR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table) {
		ERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table = eRR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table;
	}
	public String getERR_Selected_fulfillment_center_deleted_successfully() {
		return ERR_Selected_fulfillment_center_deleted_successfully;
	}
	public void setERR_Selected_fulfillment_center_deleted_successfully(
			String eRR_Selected_fulfillment_center_deleted_successfully) {
		ERR_Selected_fulfillment_center_deleted_successfully = eRR_Selected_fulfillment_center_deleted_successfully;
	}
	public String getERR_Invalid_Fulfillment_Center() {
		return ERR_Invalid_Fulfillment_Center;
	}
	public void setERR_Invalid_Fulfillment_Center(String eRR_Invalid_Fulfillment_Center) {
		ERR_Invalid_Fulfillment_Center = eRR_Invalid_Fulfillment_Center;
	}
	public String getERR_Invalid_Part_of_the_fulfillment() {
		return ERR_Invalid_Part_of_the_fulfillment;
	}
	public void setERR_Invalid_Part_of_the_fulfillment(String eRR_Invalid_Part_of_the_fulfillment) {
		ERR_Invalid_Part_of_the_fulfillment = eRR_Invalid_Part_of_the_fulfillment;
	}
	 public String getERR_Your_record_has_been_updated_successfully() {
			return ERR_Your_record_has_been_updated_successfully;
		}
		public void setERR_Your_record_has_been_updated_successfully(String eRR_Your_record_has_been_updated_successfully) {
			ERR_Your_record_has_been_updated_successfully = eRR_Your_record_has_been_updated_successfully;
		}
	
	
}
