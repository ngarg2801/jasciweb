/**
 
Description This interface use for BE  for Location Wizard screen 
Created By Pradeep Kumar  
Created Date Jan 05 2015
 */
package com.jasci.biz.AdminModule.be;


public class LOCATIONWIZARDLABELSBE {
	
	private String KP_Equality_Check_Error_Msg;
	private String KP_Separator_Character_Location;
	private String KP_Level_Number_of_Characters;
	private String KP_Separator_Character_Location_Print_only;
	private String KP_Are_you_sure_you_want_to_delete_this_record;
	// Location Wizard Maintenance
	private String KP_Row;
	private String KP_Aisle;
	private String KP_Bay;
	private String KP_Level;
	private String KP_Slot;

	private String KP_You_are_not_allowed_to_create_locations_in_reverse_order;
	private String KP_Number_of_characters_must_be_numeric_and_between_1_to_5_only;
	private String KP_Ok;
	private String KP_Download;
	private String KP_Location_is_greater;
	private String KP_There_is_no_location_profile_for_selected_fulfillment_center;
	private String KP_Numeric_Allowed_Only_Character_Set;
	private String KP_Only_Alphabets_Allowed;
	private String KP_ERR_Please_Enter_Row_character_set;
	private String KP_Numeric_value_can_not_less_than_Zero_or_eual;
	private String KP_Numeric_Allowed_Only;
	private String KP_Save_Update;
	private String KP_Mandatory_field_cannot_be_left_blank;
	private String KP_Cancel;
	private String KP_Select;
	private String KP_Location_Profile;
	private String KP_Location_Type;
	private String KP_Work_Zone;
	private String KP_WorkGroup_Zone;
	private String KP_Area;
	private String KP_Description_Long;
	private String KP_Description_Short;
	private String KP_Wizard_Control_Number;
	private String KP_Wizard_ID;
	private String KP_Applied_By;
	private String KP_Fulfillment_Center;
	private String KP_Status;
	private String KP_Created_Date;
	private String KP_Created_By;
	private String KP_Applied_Date;
	private String KP_Apply;
	private String KP_Notes;
	private String KP_Location_Wizard_Maintenance;
	private String	KP_Row_Number_of_Characters;
	private String	KP_Row_Characters_Set;
	private String	KP_Row_Starting_at;
	private String	KP_Number_of_Row;
	private String	KP_Type;
	private String	 KP_Record_has_been_saved_successfully;
	private String	 KP_Location_WIzard_is_about_to_be_deleted;
	private String	 KP_Deletion_will_remove_the_Wizard_from_the_locations;
	private String	KP_Location_WIzard_and_its_Locations_are_about_to_be_deleted;
	private String	KP_Deletion_will_not_remove_these;
	private String	KP_Locations_and_Inventory_Records;
	
	
	// Location Wizard Lookup
	private String KP_ERR_INVALID_WIZARD_CONTROL_NUMBER;
	private String KP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK;
	private String KP_ERR_INVALID_AREA;
	private String KP_ERR_AREA_LEFT_BLANK;
	private String KP_ERR_INVALID_LOCATION_TYPE;
	private String KP_ERR_LOCATION_TYPE_LEFT_BLANK;
	private String KP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION;
	private String KP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK;
	private String KP_ERR_MANDATORY_FIELD_LEFT_BLANK;
	private String KP_Any_Part_of_Wizard_Description;
	private String KP_Location_Wizard_Lookup;
	private String KP_New;
	private String KP_Display_All;
	
	// Location Wizard Search lookup
	
	private String KP_Description;
	private String KP_Number_of_Locations;
	private String KP_Copy;
	private String KP_Fulfillment_Name;
	private String  KP_Wizard_Name;
	private String KP_Delete_Wizard_Only;
	private String KP_Delete_Wizard_And_Location;
	private String KP_Location_Wizard_Search_Lookup;
	private String KP_Add_New;
	private String KP_Edit;
	private String KP_Actions;
	private String	KP_ERR_Please_Enter_Seperator_Level_Slot_Print;
	private String	KP_ERR_Please_Enter_Seperator_Level_Slot;
	private String	KP_ERR_Please_Enter_Slot_number_of;
	private String	KP_ERR_Please_Enter_Slot_starting;
	private String	KP_ERR_Please_Enter_Slot_character_set;
	private String	KP_ERR_Please_Enter_Seperator_Bay_Level_Print;
	private String	KP_ERR_Please_Enter_Seperator_Bay_Level;
	private String	KP_ERR_Please_Enter_LEVEL_number_of;
	private String	KP_ERR_Please_Enter_LEVEL_starting;
	private String	KP_ERR_Please_Enter_LEVEL_character_set;
	private String	KP_ERR_Please_Enter_Seperator_Aisle_Bay_Print;
	private String	KP_ERR_Please_Enter_Seperator_Aisle_Bay;
	private String	KP_ERR_Please_Enter_Bay_number_of;
	private String	KP_ERR_Please_Enter_Bay_starting;
	private String	KP_ERR_Please_Enter_Bay_character_set;
	private String	KP_ERR_Please_Enter_Seperator_Row_Aisle_Print;
	private String	KP_ERR_Please_Enter_Seperator_Row_Aisle;
	private String	KP_ERR_Please_Enter_Aisle_number_of;
	private String	KP_ERR_Please_Enter_Aisle_starting;
	private String	KP_ERR_Please_Enter_Aisle_character_set;
	private String	KP_ERR_Please_Enter_Row_number_of;
	private String	KP_ERR_Please_Enter_Row_starting;
	private String	KP_Number_of_locations_created;
	private String	KP_Number_of_duplicate_locations;
	private String	KP_Print;
	

	private String	KP_Reset;
	public String getKP_Reset() {
		return KP_Reset;
	}
	public void setKP_Reset(String kP_Reset) {
		KP_Reset = kP_Reset;
	}
	public String getKP_Are_you_sure_you_want_to_delete_this_record() {
		return KP_Are_you_sure_you_want_to_delete_this_record;
	}
	public void setKP_Are_you_sure_you_want_to_delete_this_record(String kP_Are_you_sure_you_want_to_delete_this_record) {
		KP_Are_you_sure_you_want_to_delete_this_record = kP_Are_you_sure_you_want_to_delete_this_record;
	}
	public String getKP_Numeric_Allowed_Only() {
		return KP_Numeric_Allowed_Only;
	}
	public void setKP_Numeric_Allowed_Only(String kP_Numeric_Allowed_Only) {
		KP_Numeric_Allowed_Only = kP_Numeric_Allowed_Only;
	}
	public String getKP_Numeric_value_can_not_less_than_Zero_or_eual() {
		return KP_Numeric_value_can_not_less_than_Zero_or_eual;
	}
	public void setKP_Numeric_value_can_not_less_than_Zero_or_eual(String kP_Numeric_value_can_not_less_than_Zero_or_eual) {
		KP_Numeric_value_can_not_less_than_Zero_or_eual = kP_Numeric_value_can_not_less_than_Zero_or_eual;
	}
	public String getKP_ERR_Please_Enter_Row_character_set() {
		return KP_ERR_Please_Enter_Row_character_set;
	}
	public void setKP_ERR_Please_Enter_Row_character_set(String kP_ERR_Please_Enter_Row_character_set) {
		KP_ERR_Please_Enter_Row_character_set = kP_ERR_Please_Enter_Row_character_set;
	}
	public String getKP_ERR_Please_Enter_Seperator_Level_Slot_Print() {
		return KP_ERR_Please_Enter_Seperator_Level_Slot_Print;
	}
	public void setKP_ERR_Please_Enter_Seperator_Level_Slot_Print(String kP_ERR_Please_Enter_Seperator_Level_Slot_Print) {
		KP_ERR_Please_Enter_Seperator_Level_Slot_Print = kP_ERR_Please_Enter_Seperator_Level_Slot_Print;
	}
	public String getKP_ERR_Please_Enter_Seperator_Level_Slot() {
		return KP_ERR_Please_Enter_Seperator_Level_Slot;
	}
	public void setKP_ERR_Please_Enter_Seperator_Level_Slot(String kP_ERR_Please_Enter_Seperator_Level_Slot) {
		KP_ERR_Please_Enter_Seperator_Level_Slot = kP_ERR_Please_Enter_Seperator_Level_Slot;
	}
	public String getKP_ERR_Please_Enter_Slot_number_of() {
		return KP_ERR_Please_Enter_Slot_number_of;
	}
	public void setKP_ERR_Please_Enter_Slot_number_of(String kP_ERR_Please_Enter_Slot_number_of) {
		KP_ERR_Please_Enter_Slot_number_of = kP_ERR_Please_Enter_Slot_number_of;
	}
	public String getKP_ERR_Please_Enter_Slot_starting() {
		return KP_ERR_Please_Enter_Slot_starting;
	}
	public void setKP_ERR_Please_Enter_Slot_starting(String kP_ERR_Please_Enter_Slot_starting) {
		KP_ERR_Please_Enter_Slot_starting = kP_ERR_Please_Enter_Slot_starting;
	}
	public String getKP_ERR_Please_Enter_Slot_character_set() {
		return KP_ERR_Please_Enter_Slot_character_set;
	}
	public void setKP_ERR_Please_Enter_Slot_character_set(String kP_ERR_Please_Enter_Slot_character_set) {
		KP_ERR_Please_Enter_Slot_character_set = kP_ERR_Please_Enter_Slot_character_set;
	}
	public String getKP_ERR_Please_Enter_Seperator_Bay_Level_Print() {
		return KP_ERR_Please_Enter_Seperator_Bay_Level_Print;
	}
	public void setKP_ERR_Please_Enter_Seperator_Bay_Level_Print(String kP_ERR_Please_Enter_Seperator_Bay_Level_Print) {
		KP_ERR_Please_Enter_Seperator_Bay_Level_Print = kP_ERR_Please_Enter_Seperator_Bay_Level_Print;
	}
	public String getKP_ERR_Please_Enter_Seperator_Bay_Level() {
		return KP_ERR_Please_Enter_Seperator_Bay_Level;
	}
	public void setKP_ERR_Please_Enter_Seperator_Bay_Level(String kP_ERR_Please_Enter_Seperator_Bay_Level) {
		KP_ERR_Please_Enter_Seperator_Bay_Level = kP_ERR_Please_Enter_Seperator_Bay_Level;
	}
	public String getKP_ERR_Please_Enter_LEVEL_number_of() {
		return KP_ERR_Please_Enter_LEVEL_number_of;
	}
	public void setKP_ERR_Please_Enter_LEVEL_number_of(String kP_ERR_Please_Enter_LEVEL_number_of) {
		KP_ERR_Please_Enter_LEVEL_number_of = kP_ERR_Please_Enter_LEVEL_number_of;
	}
	public String getKP_ERR_Please_Enter_LEVEL_starting() {
		return KP_ERR_Please_Enter_LEVEL_starting;
	}
	public void setKP_ERR_Please_Enter_LEVEL_starting(String kP_ERR_Please_Enter_LEVEL_starting) {
		KP_ERR_Please_Enter_LEVEL_starting = kP_ERR_Please_Enter_LEVEL_starting;
	}
	public String getKP_ERR_Please_Enter_LEVEL_character_set() {
		return KP_ERR_Please_Enter_LEVEL_character_set;
	}
	public void setKP_ERR_Please_Enter_LEVEL_character_set(String kP_ERR_Please_Enter_LEVEL_character_set) {
		KP_ERR_Please_Enter_LEVEL_character_set = kP_ERR_Please_Enter_LEVEL_character_set;
	}
	public String getKP_ERR_Please_Enter_Seperator_Aisle_Bay_Print() {
		return KP_ERR_Please_Enter_Seperator_Aisle_Bay_Print;
	}
	public void setKP_ERR_Please_Enter_Seperator_Aisle_Bay_Print(String kP_ERR_Please_Enter_Seperator_Aisle_Bay_Print) {
		KP_ERR_Please_Enter_Seperator_Aisle_Bay_Print = kP_ERR_Please_Enter_Seperator_Aisle_Bay_Print;
	}
	public String getKP_ERR_Please_Enter_Seperator_Aisle_Bay() {
		return KP_ERR_Please_Enter_Seperator_Aisle_Bay;
	}
	public void setKP_ERR_Please_Enter_Seperator_Aisle_Bay(String kP_ERR_Please_Enter_Seperator_Aisle_Bay) {
		KP_ERR_Please_Enter_Seperator_Aisle_Bay = kP_ERR_Please_Enter_Seperator_Aisle_Bay;
	}
	public String getKP_ERR_Please_Enter_Bay_number_of() {
		return KP_ERR_Please_Enter_Bay_number_of;
	}
	public void setKP_ERR_Please_Enter_Bay_number_of(String kP_ERR_Please_Enter_Bay_number_of) {
		KP_ERR_Please_Enter_Bay_number_of = kP_ERR_Please_Enter_Bay_number_of;
	}
	public String getKP_ERR_Please_Enter_Bay_starting() {
		return KP_ERR_Please_Enter_Bay_starting;
	}
	public void setKP_ERR_Please_Enter_Bay_starting(String kP_ERR_Please_Enter_Bay_starting) {
		KP_ERR_Please_Enter_Bay_starting = kP_ERR_Please_Enter_Bay_starting;
	}
	public String getKP_ERR_Please_Enter_Bay_character_set() {
		return KP_ERR_Please_Enter_Bay_character_set;
	}
	public void setKP_ERR_Please_Enter_Bay_character_set(String kP_ERR_Please_Enter_Bay_character_set) {
		KP_ERR_Please_Enter_Bay_character_set = kP_ERR_Please_Enter_Bay_character_set;
	}
	public String getKP_ERR_Please_Enter_Seperator_Row_Aisle_Print() {
		return KP_ERR_Please_Enter_Seperator_Row_Aisle_Print;
	}
	public void setKP_ERR_Please_Enter_Seperator_Row_Aisle_Print(String kP_ERR_Please_Enter_Seperator_Row_Aisle_Print) {
		KP_ERR_Please_Enter_Seperator_Row_Aisle_Print = kP_ERR_Please_Enter_Seperator_Row_Aisle_Print;
	}
	public String getKP_ERR_Please_Enter_Seperator_Row_Aisle() {
		return KP_ERR_Please_Enter_Seperator_Row_Aisle;
	}
	public void setKP_ERR_Please_Enter_Seperator_Row_Aisle(String kP_ERR_Please_Enter_Seperator_Row_Aisle) {
		KP_ERR_Please_Enter_Seperator_Row_Aisle = kP_ERR_Please_Enter_Seperator_Row_Aisle;
	}
	public String getKP_ERR_Please_Enter_Aisle_number_of() {
		return KP_ERR_Please_Enter_Aisle_number_of;
	}
	public void setKP_ERR_Please_Enter_Aisle_number_of(String kP_ERR_Please_Enter_Aisle_number_of) {
		KP_ERR_Please_Enter_Aisle_number_of = kP_ERR_Please_Enter_Aisle_number_of;
	}
	public String getKP_ERR_Please_Enter_Aisle_starting() {
		return KP_ERR_Please_Enter_Aisle_starting;
	}
	public void setKP_ERR_Please_Enter_Aisle_starting(String kP_ERR_Please_Enter_Aisle_starting) {
		KP_ERR_Please_Enter_Aisle_starting = kP_ERR_Please_Enter_Aisle_starting;
	}
	public String getKP_ERR_Please_Enter_Aisle_character_set() {
		return KP_ERR_Please_Enter_Aisle_character_set;
	}
	public void setKP_ERR_Please_Enter_Aisle_character_set(String kP_ERR_Please_Enter_Aisle_character_set) {
		KP_ERR_Please_Enter_Aisle_character_set = kP_ERR_Please_Enter_Aisle_character_set;
	}
	public String getKP_ERR_Please_Enter_Row_number_of() {
		return KP_ERR_Please_Enter_Row_number_of;
	}
	public void setKP_ERR_Please_Enter_Row_number_of(String kP_ERR_Please_Enter_Row_number_of) {
		KP_ERR_Please_Enter_Row_number_of = kP_ERR_Please_Enter_Row_number_of;
	}
	public String getKP_ERR_Please_Enter_Row_starting() {
		return KP_ERR_Please_Enter_Row_starting;
	}
	public void setKP_ERR_Please_Enter_Row_starting(String kP_ERR_Please_Enter_Row_starting) {
		KP_ERR_Please_Enter_Row_starting = kP_ERR_Please_Enter_Row_starting;
	}
	
	public String getKP_Select() {
		return KP_Select;
	}
	public void setKP_Select(String kP_Select) {
		KP_Select = kP_Select;
	}
	public String getKP_Edit() {
		return KP_Edit;
	}
	public void setKP_Edit(String kP_Edit) {
		KP_Edit = kP_Edit;
	}
	
	// levels getter setter
	public String getKP_Location_Type() {
		return KP_Location_Type;
	}
	public void setKP_Location_Type(String kP_Location_Type) {
		KP_Location_Type = kP_Location_Type;
	}
	public String getKP_Work_Zone() {
		return KP_Work_Zone;
	}
	public void setKP_Work_Zone(String kP_Work_Zone) {
		KP_Work_Zone = kP_Work_Zone;
	}
	public String getKP_WorkGroup_Zone() {
		return KP_WorkGroup_Zone;
	}
	public void setKP_WorkGroup_Zone(String kP_WorkGroup_Zone) {
		KP_WorkGroup_Zone = kP_WorkGroup_Zone;
	}
	public String getKP_Area() {
		return KP_Area;
	}
	public void setKP_Area(String kP_Area) {
		KP_Area = kP_Area;
	}
	public String getKP_Description_Long() {
		return KP_Description_Long;
	}
	public void setKP_Description_Long(String kP_Description_Long) {
		KP_Description_Long = kP_Description_Long;
	}
	public String getKP_Description_Short() {
		return KP_Description_Short;
	}
	public void setKP_Description_Short(String kP_Description_Short) {
		KP_Description_Short = kP_Description_Short;
	}
	public String getKP_Wizard_Control_Number() {
		return KP_Wizard_Control_Number;
	}
	public void setKP_Wizard_Control_Number(String kP_Wizard_Control_Number) {
		KP_Wizard_Control_Number = kP_Wizard_Control_Number;
	}
	public String getKP_Wizard_ID() {
		return KP_Wizard_ID;
	}
	public void setKP_Wizard_ID(String kP_Wizard_ID) {
		KP_Wizard_ID = kP_Wizard_ID;
	}
	public String getKP_Applied_By() {
		return KP_Applied_By;
	}
	public void setKP_Applied_By(String kP_Applied_By) {
		KP_Applied_By = kP_Applied_By;
	}
	public String getKP_Fulfillment_Center() {
		return KP_Fulfillment_Center;
	}
	public void setKP_Fulfillment_Center(String kP_Fulfillment_Center) {
		KP_Fulfillment_Center = kP_Fulfillment_Center;
	}
	public String getKP_Status() {
		return KP_Status;
	}
	public void setKP_Status(String kP_Status) {
		KP_Status = kP_Status;
	}
	public String getKP_Created_Date() {
		return KP_Created_Date;
	}
	public void setKP_Created_Date(String kP_Created_Date) {
		KP_Created_Date = kP_Created_Date;
	}
	public String getKP_Created_By() {
		return KP_Created_By;
	}
	public void setKP_Created_By(String kP_Created_By) {
		KP_Created_By = kP_Created_By;
	}
	public String getKP_Applied_Date() {
		return KP_Applied_Date;
	}
	public void setKP_Applied_Date(String kP_Applied_Date) {
		KP_Applied_Date = kP_Applied_Date;
	}
	public String getKP_Location_Profile() {
		return KP_Location_Profile;
	}
	public void setKP_Location_Profile(String kP_Location_Profile) {
		KP_Location_Profile = kP_Location_Profile;
	}
	public String getKP_Apply() {
		return KP_Apply;
	}
	public void setKP_Apply(String kP_Apply) {
		KP_Apply = kP_Apply;
	}
	public String getKP_Notes() {
		return KP_Notes;
	}
	public void setKP_Notes(String kP_Notes) {
		KP_Notes = kP_Notes;
	}
	public String getKP_Location_Wizard_Maintenance() {
		return KP_Location_Wizard_Maintenance;
	}
	public void setKP_Location_Wizard_Maintenance(String kP_Location_Wizard_Maintenance) {
		KP_Location_Wizard_Maintenance = kP_Location_Wizard_Maintenance;
	}
	
	// Location wizard Lookup
	
	public String getKP_Any_Part_of_Wizard_Description() {
		return KP_Any_Part_of_Wizard_Description;
	}
	public void setKP_Any_Part_of_Wizard_Description(String kP_Any_Part_of_Wizard_Description) {
		KP_Any_Part_of_Wizard_Description = kP_Any_Part_of_Wizard_Description;
	}
	public String getKP_Location_Wizard_Lookup() {
		return KP_Location_Wizard_Lookup;
	}
	public void setKP_Location_Wizard_Lookup(String kP_Location_Wizard_Lookup) {
		KP_Location_Wizard_Lookup = kP_Location_Wizard_Lookup;
	}
	public String getKP_New() {
		return KP_New;
	}
	public void setKP_New(String kP_New) {
		KP_New = kP_New;
	}
	public String getKP_Display_All() {
		return KP_Display_All;
	}
	public void setKP_Display_All(String kP_Display_All) {
		KP_Display_All = kP_Display_All;
	}
	public String getKP_Description() {
		return KP_Description;
	}
	public void setKP_Description(String kP_Description) {
		KP_Description = kP_Description;
	}
	public String getKP_Number_of_Locations() {
		return KP_Number_of_Locations;
	}
	public void setKP_Number_of_Locations(String kP_Number_of_Locations) {
		KP_Number_of_Locations = kP_Number_of_Locations;
	}
	
	// Location wizard search lookup
	
	public String getKP_Copy() {
		return KP_Copy;
	}
	public void setKP_Copy(String kP_Copy) {
		KP_Copy = kP_Copy;
	}
	public String getKP_Delete_Wizard_Only() {
		return KP_Delete_Wizard_Only;
	}
	public void setKP_Delete_Wizard_Only(String kP_Delete_Wizard_Only) {
		KP_Delete_Wizard_Only = kP_Delete_Wizard_Only;
	}
	public String getKP_Delete_Wizard_And_Location() {
		return KP_Delete_Wizard_And_Location;
	}
	public void setKP_Delete_Wizard_And_Location(String kP_Delete_Wizard_And_Location) {
		KP_Delete_Wizard_And_Location = kP_Delete_Wizard_And_Location;
	}
	public String getKP_Location_Wizard_Search_Lookup() {
		return KP_Location_Wizard_Search_Lookup;
	}
	public void setKP_Location_Wizard_Search_Lookup(String kP_Location_Wizard_Search_Lookup) {
		KP_Location_Wizard_Search_Lookup = kP_Location_Wizard_Search_Lookup;
	}
	public String getKP_Add_New() {
		return KP_Add_New;
	}
	public void setKP_Add_New(String kP_Add_New) {
		KP_Add_New = kP_Add_New;
	}
	public String getKP_Actions() {
		return KP_Actions;
	}
	public void setKP_Actions(String kP_Actions) {
		KP_Actions = kP_Actions;
	}
	public String getKP_ERR_INVALID_WIZARD_CONTROL_NUMBER() {
		return KP_ERR_INVALID_WIZARD_CONTROL_NUMBER;
	}
	public void setKP_ERR_INVALID_WIZARD_CONTROL_NUMBER(String kP_ERR_INVALID_WIZARD_CONTROL_NUMBER) {
		KP_ERR_INVALID_WIZARD_CONTROL_NUMBER = kP_ERR_INVALID_WIZARD_CONTROL_NUMBER;
	}
	public String getKP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK() {
		return KP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK;
	}
	public void setKP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK(String kP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK) {
		KP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK = kP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK;
	}
	public String getKP_ERR_INVALID_AREA() {
		return KP_ERR_INVALID_AREA;
	}
	public void setKP_ERR_INVALID_AREA(String kP_ERR_INVALID_AREA) {
		KP_ERR_INVALID_AREA = kP_ERR_INVALID_AREA;
	}
	public String getKP_ERR_AREA_LEFT_BLANK() {
		return KP_ERR_AREA_LEFT_BLANK;
	}
	public void setKP_ERR_AREA_LEFT_BLANK(String kP_ERR_AREA_LEFT_BLANK) {
		KP_ERR_AREA_LEFT_BLANK = kP_ERR_AREA_LEFT_BLANK;
	}
	public String getKP_ERR_INVALID_LOCATION_TYPE() {
		return KP_ERR_INVALID_LOCATION_TYPE;
	}
	public void setKP_ERR_INVALID_LOCATION_TYPE(String kP_ERR_INVALID_LOCATION_TYPE) {
		KP_ERR_INVALID_LOCATION_TYPE = kP_ERR_INVALID_LOCATION_TYPE;
	}
	public String getKP_ERR_LOCATION_TYPE_LEFT_BLANK() {
		return KP_ERR_LOCATION_TYPE_LEFT_BLANK;
	}
	public void setKP_ERR_LOCATION_TYPE_LEFT_BLANK(String kP_ERR_LOCATION_TYPE_LEFT_BLANK) {
		KP_ERR_LOCATION_TYPE_LEFT_BLANK = kP_ERR_LOCATION_TYPE_LEFT_BLANK;
	}
	public String getKP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION() {
		return KP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION;
	}
	public void setKP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION(String kP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION) {
		KP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION = kP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION;
	}
	public String getKP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK() {
		return KP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK;
	}
	public void setKP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK(String kP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK) {
		KP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK = kP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK;
	}
	public String getKP_ERR_MANDATORY_FIELD_LEFT_BLANK() {
		return KP_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public void setKP_ERR_MANDATORY_FIELD_LEFT_BLANK(String kP_ERR_MANDATORY_FIELD_LEFT_BLANK) {
		KP_ERR_MANDATORY_FIELD_LEFT_BLANK = kP_ERR_MANDATORY_FIELD_LEFT_BLANK;
	}
	public String getKP_Row_Number_of_Characters() {
		return KP_Row_Number_of_Characters;
	}
	public void setKP_Row_Number_of_Characters(String kP_Row_Number_of_Characters) {
		KP_Row_Number_of_Characters = kP_Row_Number_of_Characters;
	}
	public String getKP_Row_Characters_Set() {
		return KP_Row_Characters_Set;
	}
	public void setKP_Row_Characters_Set(String kP_Row_Characters_Set) {
		KP_Row_Characters_Set = kP_Row_Characters_Set;
	}
	public String getKP_Row_Starting_at() {
		return KP_Row_Starting_at;
	}
	public void setKP_Row_Starting_at(String kP_Row_Starting_at) {
		KP_Row_Starting_at = kP_Row_Starting_at;
	}
	public String getKP_Number_of_Row() {
		return KP_Number_of_Row;
	}
	public void setKP_Number_of_Row(String kP_Number_of_Row) {
		KP_Number_of_Row = kP_Number_of_Row;
	}
	
	public String getKP_Save_Update() {
		return KP_Save_Update;
	}
	public void setKP_Save_Update(String kP_Save_Update) {
		KP_Save_Update = kP_Save_Update;
	}
	public String getKP_Cancel() {
		return KP_Cancel;
	}
	public void setKP_Cancel(String kP_Cancel) {
		KP_Cancel = kP_Cancel;
	}
	
	public String getKP_Mandatory_field_cannot_be_left_blank() {
		return KP_Mandatory_field_cannot_be_left_blank;
	}
	public void setKP_Mandatory_field_cannot_be_left_blank(String kP_Mandatory_field_cannot_be_left_blank) {
		KP_Mandatory_field_cannot_be_left_blank = kP_Mandatory_field_cannot_be_left_blank;
	}
	
	public String getKP_Separator_Character_Location() {
		return KP_Separator_Character_Location;
	}
	public void setKP_Separator_Character_Location(String kP_Separator_Character_Location) {
		KP_Separator_Character_Location = kP_Separator_Character_Location;
	}
	public String getKP_Separator_Character_Location_Print_only() {
		return KP_Separator_Character_Location_Print_only;
	}
	public void setKP_Separator_Character_Location_Print_only(String kP_Separator_Character_Location_Print_only) {
		KP_Separator_Character_Location_Print_only = kP_Separator_Character_Location_Print_only;
	}
	public String getKP_Type() {
		return KP_Type;
	}
	public void setKP_Type(String kP_Type) {
		KP_Type = kP_Type;
	}
	
	public String getKP_Level_Number_of_Characters() {
		return KP_Level_Number_of_Characters;
	}
	public void setKP_Level_Number_of_Characters(String kP_Level_Number_of_Characters) {
		KP_Level_Number_of_Characters = kP_Level_Number_of_Characters;
	}
	public String getKP_Record_has_been_saved_successfully() {
		return KP_Record_has_been_saved_successfully;
	}
	public void setKP_Record_has_been_saved_successfully(String kP_Record_has_been_saved_successfully) {
		KP_Record_has_been_saved_successfully = kP_Record_has_been_saved_successfully;
	}
	public String getKP_Location_WIzard_is_about_to_be_deleted() {
		return KP_Location_WIzard_is_about_to_be_deleted;
	}
	public void setKP_Location_WIzard_is_about_to_be_deleted(String kP_Location_WIzard_is_about_to_be_deleted) {
		KP_Location_WIzard_is_about_to_be_deleted = kP_Location_WIzard_is_about_to_be_deleted;
	}
	public String getKP_Deletion_will_remove_the_Wizard_from_the_locations() {
		return KP_Deletion_will_remove_the_Wizard_from_the_locations;
	}
	public void setKP_Deletion_will_remove_the_Wizard_from_the_locations(
			String kP_Deletion_will_remove_the_Wizard_from_the_locations) {
		KP_Deletion_will_remove_the_Wizard_from_the_locations = kP_Deletion_will_remove_the_Wizard_from_the_locations;
	}
	public String getKP_Location_WIzard_and_its_Locations_are_about_to_be_deleted() {
		return KP_Location_WIzard_and_its_Locations_are_about_to_be_deleted;
	}
	public void setKP_Location_WIzard_and_its_Locations_are_about_to_be_deleted(
			String kP_Location_WIzard_and_its_Locations_are_about_to_be_deleted) {
		KP_Location_WIzard_and_its_Locations_are_about_to_be_deleted = kP_Location_WIzard_and_its_Locations_are_about_to_be_deleted;
	}
	public String getKP_Deletion_will_not_remove_these() {
		return KP_Deletion_will_not_remove_these;
	}
	public void setKP_Deletion_will_not_remove_these(String kP_Deletion_will_not_remove_these) {
		KP_Deletion_will_not_remove_these = kP_Deletion_will_not_remove_these;
	}
	public String getKP_Locations_and_Inventory_Records() {
		return KP_Locations_and_Inventory_Records;
	}
	public void setKP_Locations_and_Inventory_Records(String kP_Locations_and_Inventory_Records) {
		KP_Locations_and_Inventory_Records = kP_Locations_and_Inventory_Records;
	}
	public String getKP_Fulfillment_Name() {
		return KP_Fulfillment_Name;
	}
	public void setKP_Fulfillment_Name(String kP_Fulfillment_Name) {
		KP_Fulfillment_Name = kP_Fulfillment_Name;
	}
	public String getKP_Wizard_Name() {
		return KP_Wizard_Name;
	}
	public void setKP_Wizard_Name(String kP_Wizard_Name) {
		KP_Wizard_Name = kP_Wizard_Name;
	}
	public String getKP_Only_Alphabets_Allowed() {
		return KP_Only_Alphabets_Allowed;
	}
	public void setKP_Only_Alphabets_Allowed(String kP_Only_Alphabets_Allowed) {
		KP_Only_Alphabets_Allowed = kP_Only_Alphabets_Allowed;
	}
	public String getKP_Numeric_Allowed_Only_Character_Set() {
		return KP_Numeric_Allowed_Only_Character_Set;
	}
	public void setKP_Numeric_Allowed_Only_Character_Set(String kP_Numeric_Allowed_Only_Character_Set) {
		KP_Numeric_Allowed_Only_Character_Set = kP_Numeric_Allowed_Only_Character_Set;
	}
	public String getKP_Number_of_locations_created() {
		return KP_Number_of_locations_created;
	}
	public void setKP_Number_of_locations_created(String kP_Number_of_locations_created) {
		KP_Number_of_locations_created = kP_Number_of_locations_created;
	}
	public String getKP_Number_of_duplicate_locations() {
		return KP_Number_of_duplicate_locations;
	}
	public void setKP_Number_of_duplicate_locations(String kP_Number_of_duplicate_locations) {
		KP_Number_of_duplicate_locations = kP_Number_of_duplicate_locations;
	}
	public String getKP_Print() {
		return KP_Print;
	}
	public void setKP_Print(String kP_Print) {
		KP_Print = kP_Print;
	}
	public String getKP_There_is_no_location_profile_for_selected_fulfillment_center() {
		return KP_There_is_no_location_profile_for_selected_fulfillment_center;
	}
	public void setKP_There_is_no_location_profile_for_selected_fulfillment_center(
			String kP_There_is_no_location_profile_for_selected_fulfillment_center) {
		KP_There_is_no_location_profile_for_selected_fulfillment_center = kP_There_is_no_location_profile_for_selected_fulfillment_center;
	}
	public String getKP_Ok() {
		return KP_Ok;
	}
	public void setKP_Ok(String kP_Ok) {
		KP_Ok = kP_Ok;
	}
	public String getKP_Download() {
		return KP_Download;
	}
	public void setKP_Download(String kP_Download) {
		KP_Download = kP_Download;
	}
	public String getKP_Location_is_greater() {
		return KP_Location_is_greater;
	}
	public void setKP_Location_is_greater(String kP_Location_is_greater) {
		KP_Location_is_greater = kP_Location_is_greater;
	}
	public String getKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only() {
		return KP_Number_of_characters_must_be_numeric_and_between_1_to_5_only;
	}
	public void setKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only(
			String kP_Number_of_characters_must_be_numeric_and_between_1_to_5_only) {
		KP_Number_of_characters_must_be_numeric_and_between_1_to_5_only = kP_Number_of_characters_must_be_numeric_and_between_1_to_5_only;
	}
	public String getKP_You_are_not_allowed_to_create_locations_in_reverse_order() {
		return KP_You_are_not_allowed_to_create_locations_in_reverse_order;
	}
	public void setKP_You_are_not_allowed_to_create_locations_in_reverse_order(
			String kP_You_are_not_allowed_to_create_locations_in_reverse_order) {
		KP_You_are_not_allowed_to_create_locations_in_reverse_order = kP_You_are_not_allowed_to_create_locations_in_reverse_order;
	}
	public String getKP_Row() {
		return KP_Row;
	}
	public void setKP_Row(String kP_Row) {
		KP_Row = kP_Row;
	}
	public String getKP_Aisle() {
		return KP_Aisle;
	}
	public void setKP_Aisle(String kP_Aisle) {
		KP_Aisle = kP_Aisle;
	}
	public String getKP_Bay() {
		return KP_Bay;
	}
	public void setKP_Bay(String kP_Bay) {
		KP_Bay = kP_Bay;
	}
	public String getKP_Level() {
		return KP_Level;
	}
	public void setKP_Level(String kP_Level) {
		KP_Level = kP_Level;
	}
	public String getKP_Slot() {
		return KP_Slot;
	}
	public void setKP_Slot(String kP_Slot) {
		KP_Slot = kP_Slot;
	}
	public String getKP_Equality_Check_Error_Msg() {
		return KP_Equality_Check_Error_Msg;
	}
	public void setKP_Equality_Check_Error_Msg(String kP_Equality_Check_Error_Msg) {
		KP_Equality_Check_Error_Msg = kP_Equality_Check_Error_Msg;
	}
	
	
	

}
