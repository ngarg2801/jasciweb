/*
 
Date Developed  23 Dec 2014
Developed by Rakesh Pal
Description   Notesfield bean class is used to set the value recieved from NOTES pojo class
 */
package com.jasci.biz.AdminModule.be;

import java.util.Date;

public class NOTESADDFIELDBE {
	
	private String Tenant_Id;
	private String Company_Id;
	private String Note_Id;
	private String Note_Link;
	private Date Last_Activity_Date;
	private String Last_Activity_Team_Member;
	private String Note;
	
	public String getTenant_Id() {
		return Tenant_Id;
	}
	public void setTenant_Id(String tenant_Id) {
		Tenant_Id = tenant_Id;
	}
	public String getCompany_Id() {
		return Company_Id;
	}
	public void setCompany_Id(String company_Id) {
		Company_Id = company_Id;
	}
	public String getNote_Id() {
		return Note_Id;
	}
	public void setNote_Id(String note_Id) {
		Note_Id = note_Id;
	}
	public String getNote_Link() {
		return Note_Link;
	}
	public void setNote_Link(String note_Link) {
		Note_Link = note_Link;
	}
	public Date getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(Date date) {
		Last_Activity_Date = date;
	}
	public String getLast_Activity_Team_Member() {
		return Last_Activity_Team_Member;
	}
	public void setLast_Activity_Team_Member(String last_Activity_Team_Member) {
		Last_Activity_Team_Member = last_Activity_Team_Member;
	}
	public String getNote() {
		return Note;
	}
	public void setNote(String note) {
		Note = note;
	}


}
