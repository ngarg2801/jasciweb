/**
 
Description It is used to make setter and getter for changing text of screen in another language
Created By Shailendra Rajput
Created Date Apr 14 2015
 */
package com.jasci.biz.AdminModule.be;

public class SMARTTASKEXECUTIONBE {

	
	public static String SmartTaskExecutions_Select;
	public static String SmartTaskExecutions_Yes;
	public static String SmartTaskExecutions_No;
	public static String SmartTaskExecutions_Application;
	public static String SmartTaskExecutions_Execution_Group;
	public static String SmartTaskExecutions_Execution_Type;
	public static String SmartTaskExecutions_Execution_Device;
	public static String SmartTaskExecutions_Execution_Name;
	public static String SmartTaskExecutions_Any_Part_of_the_Execution_Description;
	public static String SmartTaskExecutions_Tenant;
	public static String SmartTaskExecutions_Buttons;
	public static String SmartTaskExecutions_New;
	public static String SmartTaskExecutions_Display_All;
	
	public static String SmartTaskExecutions_Executions_Lookup;
	
	public static String SmartTaskExecutions_Please_select_Application;
	public static String SmartTaskExecutions_Please_select_Execution_Group;
	public static String SmartTaskExecutions_Please_select_Execution_Type;
	public static String SmartTaskExecutions_Please_select_Execution_Device;
	public static String SmartTaskExecutions_Please_enter_Execution;
	public static String SmartTaskExecutions_Please_enter_any_part_of_the_Execution_Description;
	public static String SmartTaskExecutions_Please_enter_Tenant;
	public static String SmartTaskExecutions_Please_select_Buttons;
	public static String SmartTaskExecutions_Invalid_Application;
	public static String SmartTaskExecutions_Invalid_Execution_Group;
	public static String SmartTaskExecutions_Invalid_Execution_Type;
	public static String SmartTaskExecutions_Invalid_Execution_Device;
	public static String SmartTaskExecutions_Invalid_Execution;
	public static String SmartTaskExecutions_Invalid_any_part_of_the_Execution_Description;
	public static String SmartTaskExecutions_Invalid_Tenant;
	public static String SmartTaskExecutions_Invalid_Buttons;
	
	public static String SmartTaskExecutions_Executions_Search_Lookup;
	public static String SmartTaskExecutions_Add_New;
	public static String SmartTaskExecutions_Edit;
	public static String SmartTaskExecutions_Notes;
	public static String SmartTaskExecutions_Delete;
	public static String SmartTaskExecutions_Actions;
	public static String SmartTaskExecutions_Description;
	public static String SmartTaskExecutions_Are_you_sure_you_want_to_delete_this_record;
	public static String SmartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted;
	
	
	public static String SmartTaskExecutions_Executions_Maintenance;
	public static String SmartTaskExecutions_Last_Activity_Date;
	public static String SmartTaskExecutions_Last_Activity_By;
	public static String SmartTaskExecutions_Description_Short;
	public static String SmartTaskExecutions_Description_Long;
	public static String SmartTaskExecutions_Help_Line;
	public static String SmartTaskExecutions_Execution_Path;
	public static String SmartTaskExecutions_Save_Update;
	public static String SmartTaskExecutions_Reset;
	public static String SmartTaskExecutions_Cancel;
	public static String SmartTaskExecutions_Maindatory_field_can_not_be_left_blank;
	public static String SmartTaskExecutions_Your_record_has_been_saved_successfully;
	public static String SmartTaskExecutions_Execution_Name_already_Used;
	public static String SmartTaskExecutions_Your_record_has_been_updated_successfully;
	public static String SmartTaskExecutions_Please_enter_valid_Execution_Path;
	
	public  String getSmartTaskExecutions_Please_enter_valid_Execution_Path() {
		return SmartTaskExecutions_Please_enter_valid_Execution_Path;
	}

	public  void setSmartTaskExecutions_Please_enter_valid_Execution_Path(
			String smartTaskExecutions_Please_enter_valid_Execution_Path) {
		SmartTaskExecutions_Please_enter_valid_Execution_Path = smartTaskExecutions_Please_enter_valid_Execution_Path;
	}

	public  String getSmartTaskExecutions_Your_record_has_been_saved_successfully() {
		return SmartTaskExecutions_Your_record_has_been_saved_successfully;
	}

	public  void setSmartTaskExecutions_Your_record_has_been_saved_successfully(
			String smartTaskExecutions_Your_record_has_been_saved_successfully) {
		SmartTaskExecutions_Your_record_has_been_saved_successfully = smartTaskExecutions_Your_record_has_been_saved_successfully;
	}

	public  String getSmartTaskExecutions_Execution_Name_already_Used() {
		return SmartTaskExecutions_Execution_Name_already_Used;
	}

	public  void setSmartTaskExecutions_Execution_Name_already_Used(
			String smartTaskExecutions_Execution_Name_already_Used) {
		SmartTaskExecutions_Execution_Name_already_Used = smartTaskExecutions_Execution_Name_already_Used;
	}

	public  String getSmartTaskExecutions_Your_record_has_been_updated_successfully() {
		return SmartTaskExecutions_Your_record_has_been_updated_successfully;
	}

	public  void setSmartTaskExecutions_Your_record_has_been_updated_successfully(
			String smartTaskExecutions_Your_record_has_been_updated_successfully) {
		SmartTaskExecutions_Your_record_has_been_updated_successfully = smartTaskExecutions_Your_record_has_been_updated_successfully;
	}

	public  String getSmartTaskExecutions_Maindatory_field_can_not_be_left_blank() {
		return SmartTaskExecutions_Maindatory_field_can_not_be_left_blank;
	}

	public  void setSmartTaskExecutions_Maindatory_field_can_not_be_left_blank(
			String smartTaskExecutions_Maindatory_field_can_not_be_left_blank) {
		SmartTaskExecutions_Maindatory_field_can_not_be_left_blank = smartTaskExecutions_Maindatory_field_can_not_be_left_blank;
	}

	public  String getSmartTaskExecutions_Executions_Maintenance() {
		return SmartTaskExecutions_Executions_Maintenance;
	}

	public  void setSmartTaskExecutions_Executions_Maintenance(
			String smartTaskExecutions_Executions_Maintenance) {
		SmartTaskExecutions_Executions_Maintenance = smartTaskExecutions_Executions_Maintenance;
	}

	public  String getSmartTaskExecutions_Last_Activity_Date() {
		return SmartTaskExecutions_Last_Activity_Date;
	}

	public  void setSmartTaskExecutions_Last_Activity_Date(
			String smartTaskExecutions_Last_Activity_Date) {
		SmartTaskExecutions_Last_Activity_Date = smartTaskExecutions_Last_Activity_Date;
	}

	public  String getSmartTaskExecutions_Last_Activity_Team_Member() {
		return SmartTaskExecutions_Last_Activity_By;
	}

	public  void setSmartTaskExecutions_Last_Activity_Team_Member(
			String smartTaskExecutions_Last_Activity_Team_Member) {
		SmartTaskExecutions_Last_Activity_By = smartTaskExecutions_Last_Activity_Team_Member;
	}

	public  String getSmartTaskExecutions_Description_Short() {
		return SmartTaskExecutions_Description_Short;
	}

	public  void setSmartTaskExecutions_Description_Short(
			String smartTaskExecutions_Description_Short) {
		SmartTaskExecutions_Description_Short = smartTaskExecutions_Description_Short;
	}

	public  String getSmartTaskExecutions_Description_Long() {
		return SmartTaskExecutions_Description_Long;
	}

	public  void setSmartTaskExecutions_Description_Long(
			String smartTaskExecutions_Description_Long) {
		SmartTaskExecutions_Description_Long = smartTaskExecutions_Description_Long;
	}

	public  String getSmartTaskExecutions_Help_Line() {
		return SmartTaskExecutions_Help_Line;
	}

	public  void setSmartTaskExecutions_Help_Line(
			String smartTaskExecutions_Help_Line) {
		SmartTaskExecutions_Help_Line = smartTaskExecutions_Help_Line;
	}

	public  String getSmartTaskExecutions_Execution_Path() {
		return SmartTaskExecutions_Execution_Path;
	}

	public  void setSmartTaskExecutions_Execution_Path(
			String smartTaskExecutions_Execution_Path) {
		SmartTaskExecutions_Execution_Path = smartTaskExecutions_Execution_Path;
	}

	public  String getSmartTaskExecutions_Save_Update() {
		return SmartTaskExecutions_Save_Update;
	}

	public  void setSmartTaskExecutions_Save_Update(
			String smartTaskExecutions_Save_Update) {
		SmartTaskExecutions_Save_Update = smartTaskExecutions_Save_Update;
	}

	public  String getSmartTaskExecutions_Reset() {
		return SmartTaskExecutions_Reset;
	}

	public  void setSmartTaskExecutions_Reset(String smartTaskExecutions_Reset) {
		SmartTaskExecutions_Reset = smartTaskExecutions_Reset;
	}

	public  String getSmartTaskExecutions_Cancel() {
		return SmartTaskExecutions_Cancel;
	}

	public  void setSmartTaskExecutions_Cancel(
			String smartTaskExecutions_Cancel) {
		SmartTaskExecutions_Cancel = smartTaskExecutions_Cancel;
	}

	public  String getSmartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted() {
		return SmartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted;
	}

	public  void setSmartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted(
			String smartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted) {
		SmartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted = smartTaskExecutions_Execution_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted;
	}

	public  String getSmartTaskExecutions_Are_you_sure_you_want_to_delete_this_record() {
		return SmartTaskExecutions_Are_you_sure_you_want_to_delete_this_record;
	}

	public  void setSmartTaskExecutions_Are_you_sure_you_want_to_delete_this_record(
			String smartTaskExecutions_Are_you_sure_you_want_to_delete_this_record) {
		SmartTaskExecutions_Are_you_sure_you_want_to_delete_this_record = smartTaskExecutions_Are_you_sure_you_want_to_delete_this_record;
	}
	
	public  String getSmartTaskExecutions_Description() {
		return SmartTaskExecutions_Description;
	}

	public  void setSmartTaskExecutions_Description(
			String smartTaskExecutions_Description) {
		SmartTaskExecutions_Description = smartTaskExecutions_Description;
	}

	public  String getSmartTaskExecutions_Actions() {
		return SmartTaskExecutions_Actions;
	}
	
	public  void setSmartTaskExecutions_Actions(
			String smartTaskExecutions_Actions) {
		SmartTaskExecutions_Actions = smartTaskExecutions_Actions;
	}
	public  String getSmartTaskExecutions_Executions_Search_Lookup() {
		return SmartTaskExecutions_Executions_Search_Lookup;
	}
	public  void setSmartTaskExecutions_Executions_Search_Lookup(
			String smartTaskExecutions_Executions_Search_Lookup) {
		SmartTaskExecutions_Executions_Search_Lookup = smartTaskExecutions_Executions_Search_Lookup;
	}
	public  String getSmartTaskExecutions_Add_New() {
		return SmartTaskExecutions_Add_New;
	}
	public  void setSmartTaskExecutions_Add_New(
			String smartTaskExecutions_Add_New) {
		SmartTaskExecutions_Add_New = smartTaskExecutions_Add_New;
	}
	public  String getSmartTaskExecutions_Edit() {
		return SmartTaskExecutions_Edit;
	}
	public  void setSmartTaskExecutions_Edit(String smartTaskExecutions_Edit) {
		SmartTaskExecutions_Edit = smartTaskExecutions_Edit;
	}
	public  String getSmartTaskExecutions_Notes() {
		return SmartTaskExecutions_Notes;
	}
	public  void setSmartTaskExecutions_Notes(String smartTaskExecutions_Notes) {
		SmartTaskExecutions_Notes = smartTaskExecutions_Notes;
	}
	public  String getSmartTaskExecutions_Delete() {
		return SmartTaskExecutions_Delete;
	}
	public  void setSmartTaskExecutions_Delete(
			String smartTaskExecutions_Delete) {
		SmartTaskExecutions_Delete = smartTaskExecutions_Delete;
	}
	public   String getSmartTaskExecutions_Select() {
		return SmartTaskExecutions_Select;
	}
	public   void setSmartTaskExecutions_Select(
			String smartTaskExecutions_Select) {
		SmartTaskExecutions_Select = smartTaskExecutions_Select;
	}
	public   String getSmartTaskExecutions_Yes() {
		return SmartTaskExecutions_Yes;
	}
	public   void setSmartTaskExecutions_Yes(String smartTaskExecutions_Yes) {
		SmartTaskExecutions_Yes = smartTaskExecutions_Yes;
	}
	public   String getSmartTaskExecutions_No() {
		return SmartTaskExecutions_No;
	}
	public   void setSmartTaskExecutions_No(String smartTaskExecutions_No) {
		SmartTaskExecutions_No = smartTaskExecutions_No;
	}
	public   String getSmartTaskExecutions_Application() {
		return SmartTaskExecutions_Application;
	}
	public   void setSmartTaskExecutions_Application(
			String smartTaskExecutions_Application) {
		SmartTaskExecutions_Application = smartTaskExecutions_Application;
	}
	public   String getSmartTaskExecutions_Execution_Group() {
		return SmartTaskExecutions_Execution_Group;
	}
	public   void setSmartTaskExecutions_Execution_Group(
			String smartTaskExecutions_Execution_Group) {
		SmartTaskExecutions_Execution_Group = smartTaskExecutions_Execution_Group;
	}
	public   String getSmartTaskExecutions_Execution_Type() {
		return SmartTaskExecutions_Execution_Type;
	}
	public   void setSmartTaskExecutions_Execution_Type(
			String smartTaskExecutions_Execution_Type) {
		SmartTaskExecutions_Execution_Type = smartTaskExecutions_Execution_Type;
	}
	public   String getSmartTaskExecutions_Execution_Device() {
		return SmartTaskExecutions_Execution_Device;
	}
	public   void setSmartTaskExecutions_Execution_Device(
			String smartTaskExecutions_Execution_Device) {
		SmartTaskExecutions_Execution_Device = smartTaskExecutions_Execution_Device;
	}
	public   String getSmartTaskExecutions_Execution() {
		return SmartTaskExecutions_Execution_Name;
	}
	public   void setSmartTaskExecutions_Execution(
			String smartTaskExecutions_Execution) {
		SmartTaskExecutions_Execution_Name = smartTaskExecutions_Execution;
	}
	public   String getSmartTaskExecutions_Any_Part_of_the_Execution_Description() {
		return SmartTaskExecutions_Any_Part_of_the_Execution_Description;
	}
	public   void setSmartTaskExecutions_Any_Part_of_the_Execution_Description(
			String smartTaskExecutions_Any_Part_of_the_Execution_Description) {
		SmartTaskExecutions_Any_Part_of_the_Execution_Description = smartTaskExecutions_Any_Part_of_the_Execution_Description;
	}
	public   String getSmartTaskExecutions_Tenant() {
		return SmartTaskExecutions_Tenant;
	}
	public   void setSmartTaskExecutions_Tenant(
			String smartTaskExecutions_Tenant) {
		SmartTaskExecutions_Tenant = smartTaskExecutions_Tenant;
	}
	public   String getSmartTaskExecutions_Buttons() {
		return SmartTaskExecutions_Buttons;
	}
	public   void setSmartTaskExecutions_Buttons(
			String smartTaskExecutions_Buttons) {
		SmartTaskExecutions_Buttons = smartTaskExecutions_Buttons;
	}
	public   String getSmartTaskExecutions_New() {
		return SmartTaskExecutions_New;
	}
	public   void setSmartTaskExecutions_New(String smartTaskExecutions_New) {
		SmartTaskExecutions_New = smartTaskExecutions_New;
	}
	public   String getSmartTaskExecutions_Display_All() {
		return SmartTaskExecutions_Display_All;
	}
	public   void setSmartTaskExecutions_Display_All(
			String smartTaskExecutions_Display_All) {
		SmartTaskExecutions_Display_All = smartTaskExecutions_Display_All;
	}
	public   String getSmartTaskExecutions_Executions_Lookup() {
		return SmartTaskExecutions_Executions_Lookup;
	}
	public   void setSmartTaskExecutions_Executions_Lookup(
			String smartTaskExecutions_Executions_Lookup) {
		SmartTaskExecutions_Executions_Lookup = smartTaskExecutions_Executions_Lookup;
	}
	public  String getSmartTaskExecutions_Please_select_Application() {
		return SmartTaskExecutions_Please_select_Application;
	}
	public  void setSmartTaskExecutions_Please_select_Application(
			String smartTaskExecutions_Please_select_Application) {
		SmartTaskExecutions_Please_select_Application = smartTaskExecutions_Please_select_Application;
	}
	public  String getSmartTaskExecutions_Please_select_Execution_Group() {
		return SmartTaskExecutions_Please_select_Execution_Group;
	}
	public  void setSmartTaskExecutions_Please_select_Execution_Group(
			String smartTaskExecutions_Please_select_Execution_Group) {
		SmartTaskExecutions_Please_select_Execution_Group = smartTaskExecutions_Please_select_Execution_Group;
	}
	public  String getSmartTaskExecutions_Please_select_Execution_Type() {
		return SmartTaskExecutions_Please_select_Execution_Type;
	}
	public  void setSmartTaskExecutions_Please_select_Execution_Type(
			String smartTaskExecutions_Please_select_Execution_Type) {
		SmartTaskExecutions_Please_select_Execution_Type = smartTaskExecutions_Please_select_Execution_Type;
	}
	public  String getSmartTaskExecutions_Please_select_Execution_Device() {
		return SmartTaskExecutions_Please_select_Execution_Device;
	}
	public  void setSmartTaskExecutions_Please_select_Execution_Device(
			String smartTaskExecutions_Please_select_Execution_Device) {
		SmartTaskExecutions_Please_select_Execution_Device = smartTaskExecutions_Please_select_Execution_Device;
	}
	public  String getSmartTaskExecutions_Please_enter_Execution() {
		return SmartTaskExecutions_Please_enter_Execution;
	}
	public  void setSmartTaskExecutions_Please_enter_Execution(
			String smartTaskExecutions_Please_enter_Execution) {
		SmartTaskExecutions_Please_enter_Execution = smartTaskExecutions_Please_enter_Execution;
	}
	public  String getSmartTaskExecutions_Please_enter_any_part_of_the_Execution_Description() {
		return SmartTaskExecutions_Please_enter_any_part_of_the_Execution_Description;
	}
	public  void setSmartTaskExecutions_Please_enter_any_part_of_the_Execution_Description(
			String smartTaskExecutions_Please_enter_any_part_of_the_Execution_Description) {
		SmartTaskExecutions_Please_enter_any_part_of_the_Execution_Description = smartTaskExecutions_Please_enter_any_part_of_the_Execution_Description;
	}
	public  String getSmartTaskExecutions_Please_enter_Tenant() {
		return SmartTaskExecutions_Please_enter_Tenant;
	}
	public  void setSmartTaskExecutions_Please_enter_Tenant(
			String smartTaskExecutions_Please_enter_Tenant) {
		SmartTaskExecutions_Please_enter_Tenant = smartTaskExecutions_Please_enter_Tenant;
	}
	public  String getSmartTaskExecutions_Please_select_Buttons() {
		return SmartTaskExecutions_Please_select_Buttons;
	}
	public  void setSmartTaskExecutions_Please_select_Buttons(
			String smartTaskExecutions_Please_select_Buttons) {
		SmartTaskExecutions_Please_select_Buttons = smartTaskExecutions_Please_select_Buttons;
	}
	public  String getSmartTaskExecutions_Invalid_Application() {
		return SmartTaskExecutions_Invalid_Application;
	}
	public  void setSmartTaskExecutions_Invalid_Application(
			String smartTaskExecutions_Invalid_Application) {
		SmartTaskExecutions_Invalid_Application = smartTaskExecutions_Invalid_Application;
	}
	public  String getSmartTaskExecutions_Invalid_Execution_Group() {
		return SmartTaskExecutions_Invalid_Execution_Group;
	}
	public  void setSmartTaskExecutions_Invalid_Execution_Group(
			String smartTaskExecutions_Invalid_Execution_Group) {
		SmartTaskExecutions_Invalid_Execution_Group = smartTaskExecutions_Invalid_Execution_Group;
	}
	public  String getSmartTaskExecutions_Invalid_Execution_Type() {
		return SmartTaskExecutions_Invalid_Execution_Type;
	}
	public  void setSmartTaskExecutions_Invalid_Execution_Type(
			String smartTaskExecutions_Invalid_Execution_Type) {
		SmartTaskExecutions_Invalid_Execution_Type = smartTaskExecutions_Invalid_Execution_Type;
	}
	public  String getSmartTaskExecutions_Invalid_Execution_Device() {
		return SmartTaskExecutions_Invalid_Execution_Device;
	}
	public  void setSmartTaskExecutions_Invalid_Execution_Device(
			String smartTaskExecutions_Invalid_Execution_Device) {
		SmartTaskExecutions_Invalid_Execution_Device = smartTaskExecutions_Invalid_Execution_Device;
	}
	public  String getSmartTaskExecutions_Invalid_Execution() {
		return SmartTaskExecutions_Invalid_Execution;
	}
	public  void setSmartTaskExecutions_Invalid_Execution(
			String smartTaskExecutions_Invalid_Execution) {
		SmartTaskExecutions_Invalid_Execution = smartTaskExecutions_Invalid_Execution;
	}
	public  String getSmartTaskExecutions_Invalid_any_part_of_the_Execution_Description() {
		return SmartTaskExecutions_Invalid_any_part_of_the_Execution_Description;
	}
	public  void setSmartTaskExecutions_Invalid_any_part_of_the_Execution_Description(
			String smartTaskExecutions_Invalid_any_part_of_the_Execution_Description) {
		SmartTaskExecutions_Invalid_any_part_of_the_Execution_Description = smartTaskExecutions_Invalid_any_part_of_the_Execution_Description;
	}
	public  String getSmartTaskExecutions_Invalid_Tenant() {
		return SmartTaskExecutions_Invalid_Tenant;
	}
	public  void setSmartTaskExecutions_Invalid_Tenant(
			String smartTaskExecutions_Invalid_Tenant) {
		SmartTaskExecutions_Invalid_Tenant = smartTaskExecutions_Invalid_Tenant;
	}
	public  String getSmartTaskExecutions_Invalid_Buttons() {
		return SmartTaskExecutions_Invalid_Buttons;
	}
	public  void setSmartTaskExecutions_Invalid_Buttons(
			String smartTaskExecutions_Invalid_Buttons) {
		SmartTaskExecutions_Invalid_Buttons = smartTaskExecutions_Invalid_Buttons;
	}
	
	
}
