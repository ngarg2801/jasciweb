/*
 
Date Developed  Nov 02 2014
Description  Created a getter and setter of SECURITYAUTHORIZATION
Created By Deepak Sharma
 */
package com.jasci.biz.AdminModule.be;

public class SECURITYAUTHORIZATIONSCREENBE {


	
	public  static String Security_labels_TeamMemberName_name="Team Member Name";
	public  static String Security_labels_lblLookUPScreenTitle="lblLookUPScreenTitle";
	public  static String Security_labels_lblButtonReset="lblButtonReset";
	public  static String Security_labels_lblLookUpEnterTeamMember="lblLookUpEnterTeamMember";
	public  static String Security_labels_lblLookUpPartOfTeamMember="lblLookUpPartOfTeamMember";
	public  static String Security_labels_lblLookUpDisplayAll="lblLookUpDisplayAll";
	public  static String Security_labels_ErrLookUpNotAvalidTeamMember="ErrLookUpNotAvalidTeamMember";
	public  static String Security_labels_lblTeamMemberSelectionTitle="lblTeamMemberSelectionTitle";
	public  static String Security_labels_lblSelectionPartOfTeamMember="lblSelectionPartOfTeamMember";
	public  static String Security_labels_lblSelectionGridName="lblSelectionGridName";
	public  static String Security_labels_lblSelectionGridTeamMember="lblSelectionGridTeamMember";
	public  static String Security_labels_lblSelectionGridActions="lblSelectionGridActions";
	public  static String Security_labels_lblSelectionGridEdit="lblSelectionGridEdit";
	public  static String Security_labels_lblSelectionGridDelete="lblSelectionGridDelete";
	public  static String Security_labels_lblSelectionSortFirstName="lblSelectionSortFirstName";
	public  static String Security_labels_lblSelectionSortLastName="lblSelectionSortLastName";
	public  static String Security_labels_ErrPartOfTeamMemberNotLeftBlank="ErrPartOfTeamMemberNotLeftBlank";
	public  static String Security_labels_ErrTeamMemberNotLeftBlank="ErrTeamMemberNotLeftBlank";
	public  static String Security_labels_ErrSelectionConfirmDelete="ErrSelectionConfirmDelete";
	public  static String Security_labels_ErrSelectionUserIsAlreadyInActive="ErrSelectionUserIsAlreadyInActive";
	public  static String Security_labels_ErrSelectionUserNotExists="ErrSelectionUserNotExists";
	public  static String Security_labels_ErrSelectionStatusUpdated="ErrSelectionStatusUpdated";
	public  static String Security_labels_lblSecurityTitle="lblSecurityTitle";
	public  static String Security_labels_lblSecurityLAstActivityDate="lblSecurityLAstActivityDate";
	public  static String Security_labels_lblSecurityLastActivityBy="lblSecurityLastActivityBy";
	public  static String Security_labels_lblSecurityUserID="lblSecurityUserID";
	public  static String Security_labels_lblSecurityTenant="lblSecurityTenant";
	public  static String Security_labels_lblSecurityCompany="lblSecurityCompany";
	public  static String Security_labels_lblSecurityCurrentPwd="lblSecurityCurrentPwd";
	public  static String Security_labels_lblSecuritySeqQues1="lblSecuritySeqQues1";
	public  static String Security_labels_lblSecuritySeqAns1="lblSecuritySeqAns1";
	public  static String Security_labels_lblSecuritySeqQues2="lblSecuritySeqQues2";
	public  static String Security_labels_lblSecuritySeqAns2="lblSecuritySeqAns2";
	public  static String Security_labels_lblSecuritySeqQues3="lblSecuritySeqQues3";
	public  static String Security_labels_lblSecuritySeqAns3="lblSecuritySeqAns3";
	public  static String Security_labels_lblSecurityLstPwd1="lblSecurityLstPwd1";
	public  static String Security_labels_lblSecurityLstPwd2="lblSecurityLstPwd2";
	public  static String Security_labels_lblSecurityLstPwd3="lblSecurityLstPwd3";
	public  static String Security_labels_lblSecurityLstPwd4="lblSecurityLstPwd4";
	public  static String Security_labels_lblSecurityLstPwd5="lblSecurityLstPwd5";
	public  static String Security_labels_lblSecurityNoOfAtmp="lblSecurityNoOfAtmp";
	public  static String Security_labels_lblSecurityLstInvalidDate="lblSecurityLstInvalidDate";
	public  static String Security_labels_lblSequrityUpdate="lblSequrityUpdate";
	public  static String Security_labels_lblSequrityDelete="lblSequrityDelete";
	public  static String Security_labels_ErrSecurityUserNotLeftBlank="ErrSecurityUserNotLeftBlank";
	public  static String Security_labels_ErrSecurityUserValid="ErrSecurityUserValid";
	public  static String Security_labels_ErrSecurityPasswordBlank="ErrSecurityPasswordBlank";
	public  static String Security_labels_ErrSecurityPasswordValid="ErrSecurityPasswordValid";
	public  static String Security_labels_ErrSecurityUserNameNotAvailable="ErrSecurityUserNameNotAvailable";
	public  static String Security_labels_ErrSecuritySeqQuesNotValid="ErrSecuritySeqQuesNotValid";
	public  static String Security_labels_ErrSecuritySeqAns1NotLeftBlank="ErrSecuritySeqAns1NotLeftBlank";
	public  static String Security_labels_ErrSecuritySeqAns2NotLeftBlank="ErrSecuritySeqAns2NotLeftBlank";
	public  static String Security_labels_ErrSecuritySeqAns3NotLeftBlank="ErrSecuritySeqAns3NotLeftBlank";
	public  static String Security_labels_InfoStatusUpdatedSuccessFully="InfoStatusUpdatedSuccessFully";
	public  static String Security_labels_InfoStatusAddedSuccessFully="InfoStatusAddedSuccessFully";
	public  static String Security_labels_lblSelectionGridStatus="lblSelectionGridStatus";
	public  static String Security_labels_ErrSelectionNotAbleToEdit="ErrSelectionNotAbleToEdit";
	public  static String Security_labels_ErrLookUpNotAvalidPartOfTeamMember="ErrLookUpNotAvalidPartOfTeamMember";
	
	private String lblLookUPScreenTitle,lblLookUpEnterTeamMember,lblLookUpPartOfTeamMember,lblLookUpDisplayAll;

	private String ErrLookUpNotAvalidTeamMember,ErrLookUpNotAvalidPartOfTeamMember;
	private String lblTeamMemberSelectionTitle,lblSelectionPartOfTeamMember,lblSelectionGridName,lblSelectionGridTeamMember,lblSelectionGridActions,lblSelectionGridEdit,lblSelectionGridDelete,lblSelectionGridStatus,lblSelectionSortFirstName,lblSelectionSortLastName;
	
	private String ErrPartOfTeamMemberNotLeftBlank,ErrTeamMemberNotLeftBlank,ErrSelectionConfirmDelete,ErrSelectionUserIsAlreadyInActive,ErrSelectionUserNotExists,ErrSelectionStatusUpdated,ErrSelectionNotAbleToEdit;
	
	private String lblSecurityTitle,lblSecurityLAstActivityDate,lblSecurityLastActivityBy,lblSecurityUserID,lblSecurityTenant,lblSecurityCompany,lblSecurityCurrentPwd,lblSecuritySeqQues1,
	lblSecuritySeqAns1,lblSecuritySeqQues2,lblSecuritySeqAns2,lblSecuritySeqQues3,lblSecuritySeqAns3,lblSecurityLstPwd1,lblSecurityLstPwd2,lblSecurityLstPwd3,lblSecurityLstPwd4,lblSecurityLstPwd5,
	lblSecurityNoOfAtmp,lblSecurityLstInvalidDate,lblSequrityUpdate,lblSequrityDelete,Security_labels_TeamMemberName;
	
	private String ErrSecurityUserNotLeftBlank,ErrSecurityUserValid,ErrSecurityPasswordBlank,ErrSecurityPasswordValid,ErrSecurityUserNameNotAvailable,
	ErrSecuritySeqQuesNotValid,ErrSecuritySeqAns1NotLeftBlank,ErrSecuritySeqAns2NotLeftBlank,ErrSecuritySeqAns3NotLeftBlank;

	private String lblSecurity_ButtonResetText;
	
	
	public String getLblSecurity_ButtonResetText() {
		return lblSecurity_ButtonResetText;
	}

	public void setLblSecurity_ButtonResetText(String lblSecurity_ButtonResetText) {
		this.lblSecurity_ButtonResetText = lblSecurity_ButtonResetText;
	}

	private String InfoStatusUpdatedSuccessFully,InfoStatusAddedSuccessFully;
	public String getInfoStatusUpdatedSuccessFully() {
		return InfoStatusUpdatedSuccessFully;
	}

	public void setInfoStatusUpdatedSuccessFully(String infoStatusUpdatedSuccessFully) {
		InfoStatusUpdatedSuccessFully = infoStatusUpdatedSuccessFully;
	}

	public String getInfoStatusAddedSuccessFully() {
		return InfoStatusAddedSuccessFully;
	}

	public void setInfoStatusAddedSuccessFully(String infoStatusAddedSuccessFully) {
		InfoStatusAddedSuccessFully = infoStatusAddedSuccessFully;
	}

	public String getErrSecuritySeqAns1NotLeftBlank() {
		return ErrSecuritySeqAns1NotLeftBlank;
	}

	public void setErrSecuritySeqAns1NotLeftBlank(String errSecuritySeqAns1NotLeftBlank) {
		ErrSecuritySeqAns1NotLeftBlank = errSecuritySeqAns1NotLeftBlank;
	}

	public String getErrSecuritySeqAns2NotLeftBlank() {
		return ErrSecuritySeqAns2NotLeftBlank;
	}

	public void setErrSecuritySeqAns2NotLeftBlank(String errSecuritySeqAns2NotLeftBlank) {
		ErrSecuritySeqAns2NotLeftBlank = errSecuritySeqAns2NotLeftBlank;
	}

	public String getErrSecuritySeqAns3NotLeftBlank() {
		return ErrSecuritySeqAns3NotLeftBlank;
	}

	public void setErrSecuritySeqAns3NotLeftBlank(String errSecuritySeqAns3NotLeftBlank) {
		ErrSecuritySeqAns3NotLeftBlank = errSecuritySeqAns3NotLeftBlank;
	}

	public String getLblLookUPScreenTitle() {
		return lblLookUPScreenTitle;
	}

	public void setLblLookUPScreenTitle(String lblLookUPScreenTitle) {
		this.lblLookUPScreenTitle = lblLookUPScreenTitle;
	}

	public String getLblLookUpEnterTeamMember() {
		return lblLookUpEnterTeamMember;
	}

	public void setLblLookUpEnterTeamMember(String lblLookUpEnterTeamMember) {
		this.lblLookUpEnterTeamMember = lblLookUpEnterTeamMember;
	}

	public String getLblLookUpPartOfTeamMember() {
		return lblLookUpPartOfTeamMember;
	}

	public void setLblLookUpPartOfTeamMember(String lblLookUpPartOfTeamMember) {
		this.lblLookUpPartOfTeamMember = lblLookUpPartOfTeamMember;
	}

	public String getLblLookUpDisplayAll() {
		return lblLookUpDisplayAll;
	}

	public void setLblLookUpDisplayAll(String lblLookUpDisplayAll) {
		this.lblLookUpDisplayAll = lblLookUpDisplayAll;
	}

	public String getErrLookUpNotAvalidTeamMember() {
		return ErrLookUpNotAvalidTeamMember;
	}

	public void setErrLookUpNotAvalidTeamMember(String errLookUpNotAvalidTeamMember) {
		ErrLookUpNotAvalidTeamMember = errLookUpNotAvalidTeamMember;
	}

	public String getLblSelectionPartOfTeamMember() {
		return lblSelectionPartOfTeamMember;
	}

	public void setLblSelectionPartOfTeamMember(String lblSelectionPartOfTeamMember) {
		this.lblSelectionPartOfTeamMember = lblSelectionPartOfTeamMember;
	}

	public String getLblSelectionGridName() {
		return lblSelectionGridName;
	}

	public void setLblSelectionGridName(String lblSelectionGridName) {
		this.lblSelectionGridName = lblSelectionGridName;
	}

	public String getLblSelectionGridTeamMember() {
		return lblSelectionGridTeamMember;
	}

	public void setLblSelectionGridTeamMember(String lblSelectionGridTeamMember) {
		this.lblSelectionGridTeamMember = lblSelectionGridTeamMember;
	}

	public String getLblSelectionGridActions() {
		return lblSelectionGridActions;
	}

	public void setLblSelectionGridActions(String lblSelectionGridActions) {
		this.lblSelectionGridActions = lblSelectionGridActions;
	}

	public String getLblSelectionGridEdit() {
		return lblSelectionGridEdit;
	}

	public void setLblSelectionGridEdit(String lblSelectionGridEdit) {
		this.lblSelectionGridEdit = lblSelectionGridEdit;
	}

	public String getLblSelectionGridDelete() {
		return lblSelectionGridDelete;
	}

	public void setLblSelectionGridDelete(String lblSelectionGridDelete) {
		this.lblSelectionGridDelete = lblSelectionGridDelete;
	}

	public String getErrSelectionConfirmDelete() {
		return ErrSelectionConfirmDelete;
	}

	public void setErrSelectionConfirmDelete(String errSelectionConfirmDelete) {
		ErrSelectionConfirmDelete = errSelectionConfirmDelete;
	}

	public String getErrSelectionUserIsAlreadyInActive() {
		return ErrSelectionUserIsAlreadyInActive;
	}

	public void setErrSelectionUserIsAlreadyInActive(String errSelectionUserIsAlreadyInActive) {
		ErrSelectionUserIsAlreadyInActive = errSelectionUserIsAlreadyInActive;
	}

	public String getErrSelectionUserNotExists() {
		return ErrSelectionUserNotExists;
	}

	public void setErrSelectionUserNotExists(String errSelectionUserNotExists) {
		ErrSelectionUserNotExists = errSelectionUserNotExists;
	}

	public String getErrSelectionStatusUpdated() {
		return ErrSelectionStatusUpdated;
	}

	public void setErrSelectionStatusUpdated(String errSelectionStatusUpdated) {
		ErrSelectionStatusUpdated = errSelectionStatusUpdated;
	}

	public String getLblSecurityTitle() {
		return lblSecurityTitle;
	}

	public void setLblSecurityTitle(String lblSecurityTitle) {
		this.lblSecurityTitle = lblSecurityTitle;
	}

	public String getLblSecurityLAstActivityDate() {
		return lblSecurityLAstActivityDate;
	}

	public void setLblSecurityLAstActivityDate(String lblSecurityLAstActivityDate) {
		this.lblSecurityLAstActivityDate = lblSecurityLAstActivityDate;
	}

	public String getLblSecurityLastActivityBy() {
		return lblSecurityLastActivityBy;
	}

	public void setLblSecurityLastActivityBy(String lblSecurityLastActivityBy) {
		this.lblSecurityLastActivityBy = lblSecurityLastActivityBy;
	}

	public String getLblSecurityUserID() {
		return lblSecurityUserID;
	}

	public void setLblSecurityUserID(String lblSecurityUserID) {
		this.lblSecurityUserID = lblSecurityUserID;
	}

	public String getLblSecurityTenant() {
		return lblSecurityTenant;
	}

	public void setLblSecurityTenant(String lblSecurityTenant) {
		this.lblSecurityTenant = lblSecurityTenant;
	}

	public String getLblSecurityCompany() {
		return lblSecurityCompany;
	}

	public void setLblSecurityCompany(String lblSecurityCompany) {
		this.lblSecurityCompany = lblSecurityCompany;
	}

	public String getLblSecurityCurrentPwd() {
		return lblSecurityCurrentPwd;
	}

	public void setLblSecurityCurrentPwd(String lblSecurityCurrentPwd) {
		this.lblSecurityCurrentPwd = lblSecurityCurrentPwd;
	}

	public String getLblSecuritySeqQues1() {
		return lblSecuritySeqQues1;
	}

	public void setLblSecuritySeqQues1(String lblSecuritySeqQues1) {
		this.lblSecuritySeqQues1 = lblSecuritySeqQues1;
	}

	public String getLblSecuritySeqAns1() {
		return lblSecuritySeqAns1;
	}

	public void setLblSecuritySeqAns1(String lblSecuritySeqAns1) {
		this.lblSecuritySeqAns1 = lblSecuritySeqAns1;
	}

	public String getLblSecuritySeqQues2() {
		return lblSecuritySeqQues2;
	}

	public void setLblSecuritySeqQues2(String lblSecuritySeqQues2) {
		this.lblSecuritySeqQues2 = lblSecuritySeqQues2;
	}

	public String getLblSecuritySeqAns2() {
		return lblSecuritySeqAns2;
	}

	public void setLblSecuritySeqAns2(String lblSecuritySeqAns2) {
		this.lblSecuritySeqAns2 = lblSecuritySeqAns2;
	}

	public String getLblSecuritySeqQues3() {
		return lblSecuritySeqQues3;
	}

	public void setLblSecuritySeqQues3(String lblSecuritySeqQues3) {
		this.lblSecuritySeqQues3 = lblSecuritySeqQues3;
	}

	public String getLblSecuritySeqAns3() {
		return lblSecuritySeqAns3;
	}

	public void setLblSecuritySeqAns3(String lblSecuritySeqAns3) {
		this.lblSecuritySeqAns3 = lblSecuritySeqAns3;
	}

	public String getLblSecurityLstPwd1() {
		return lblSecurityLstPwd1;
	}

	public void setLblSecurityLstPwd1(String lblSecurityLstPwd1) {
		this.lblSecurityLstPwd1 = lblSecurityLstPwd1;
	}

	public String getLblSecurityLstPwd2() {
		return lblSecurityLstPwd2;
	}

	public void setLblSecurityLstPwd2(String lblSecurityLstPwd2) {
		this.lblSecurityLstPwd2 = lblSecurityLstPwd2;
	}

	public String getLblSecurityLstPwd3() {
		return lblSecurityLstPwd3;
	}

	public void setLblSecurityLstPwd3(String lblSecurityLstPwd3) {
		this.lblSecurityLstPwd3 = lblSecurityLstPwd3;
	}

	public String getLblSecurityLstPwd4() {
		return lblSecurityLstPwd4;
	}

	public void setLblSecurityLstPwd4(String lblSecurityLstPwd4) {
		this.lblSecurityLstPwd4 = lblSecurityLstPwd4;
	}

	public String getLblSecurityLstPwd5() {
		return lblSecurityLstPwd5;
	}

	public void setLblSecurityLstPwd5(String lblSecurityLstPwd5) {
		this.lblSecurityLstPwd5 = lblSecurityLstPwd5;
	}

	public String getLblSecurityNoOfAtmp() {
		return lblSecurityNoOfAtmp;
	}

	public void setLblSecurityNoOfAtmp(String lblSecurityNoOfAtmp) {
		this.lblSecurityNoOfAtmp = lblSecurityNoOfAtmp;
	}

	public String getLblSecurityLstInvalidDate() {
		return lblSecurityLstInvalidDate;
	}

	public void setLblSecurityLstInvalidDate(String lblSecurityLstInvalidDate) {
		this.lblSecurityLstInvalidDate = lblSecurityLstInvalidDate;
	}

	public String getErrSecurityUserNotLeftBlank() {
		return ErrSecurityUserNotLeftBlank;
	}

	public void setErrSecurityUserNotLeftBlank(String errSecurityUserNotLeftBlank) {
		ErrSecurityUserNotLeftBlank = errSecurityUserNotLeftBlank;
	}

	public String getErrSecurityUserValid() {
		return ErrSecurityUserValid;
	}

	public void setErrSecurityUserValid(String errSecurityUserValid) {
		ErrSecurityUserValid = errSecurityUserValid;
	}

	public String getErrSecurityPasswordBlank() {
		return ErrSecurityPasswordBlank;
	}

	public void setErrSecurityPasswordBlank(String errSecurityPasswordBlank) {
		ErrSecurityPasswordBlank = errSecurityPasswordBlank;
	}

	public String getErrSecurityPasswordValid() {
		return ErrSecurityPasswordValid;
	}

	public void setErrSecurityPasswordValid(String errSecurityPasswordValid) {
		ErrSecurityPasswordValid = errSecurityPasswordValid;
	}

	public String getErrSecurityUserNameNotAvailable() {
		return ErrSecurityUserNameNotAvailable;
	}

	public void setErrSecurityUserNameNotAvailable(String errSecurityUserNameNotAvailable) {
		ErrSecurityUserNameNotAvailable = errSecurityUserNameNotAvailable;
	}

	public String getErrSecuritySeqQuesNotValid() {
		return ErrSecuritySeqQuesNotValid;
	}

	public void setErrSecuritySeqQuesNotValid(String errSecuritySeqQuesNotValid) {
		ErrSecuritySeqQuesNotValid = errSecuritySeqQuesNotValid;
	}

	public String getLblTeamMemberSelectionTitle() {
		return lblTeamMemberSelectionTitle;
	}

	public void setLblTeamMemberSelectionTitle(String lblTeamMemberSelectionTitle) {
		this.lblTeamMemberSelectionTitle = lblTeamMemberSelectionTitle;
	}

	public String getErrPartOfTeamMemberNotLeftBlank() {
		return ErrPartOfTeamMemberNotLeftBlank;
	}

	public void setErrPartOfTeamMemberNotLeftBlank(String errPartOfTeamMemberNotLeftBlank) {
		ErrPartOfTeamMemberNotLeftBlank = errPartOfTeamMemberNotLeftBlank;
	}

	public String getErrTeamMemberNotLeftBlank() {
		return ErrTeamMemberNotLeftBlank;
	}

	public void setErrTeamMemberNotLeftBlank(String errTeamMemberNotLeftBlank) {
		ErrTeamMemberNotLeftBlank = errTeamMemberNotLeftBlank;
	}

	public String getLblSelectionSortFirstName() {
		return lblSelectionSortFirstName;
	}

	public void setLblSelectionSortFirstName(String lblSelectionSortFirstName) {
		this.lblSelectionSortFirstName = lblSelectionSortFirstName;
	}

	public String getLblSelectionSortLastName() {
		return lblSelectionSortLastName;
	}

	public void setLblSelectionSortLastName(String lblSelectionSortLastName) {
		this.lblSelectionSortLastName = lblSelectionSortLastName;
	}

	public String getLblSequrityUpdate() {
		return lblSequrityUpdate;
	}

	public void setLblSequrityUpdate(String lblSequrityUpdate) {
		this.lblSequrityUpdate = lblSequrityUpdate;
	}

	public String getLblSequrityDelete() {
		return lblSequrityDelete;
	}

	public void setLblSequrityDelete(String lblSequrityDelete) {
		this.lblSequrityDelete = lblSequrityDelete;
	}

	/**
	 * @return the lblSelectionGridStatus
	 */
	public String getLblSelectionGridStatus() {
		return lblSelectionGridStatus;
	}

	/**
	 * @param lblSelectionGridStatus the lblSelectionGridStatus to set
	 */
	public void setLblSelectionGridStatus(String lblSelectionGridStatus) {
		this.lblSelectionGridStatus = lblSelectionGridStatus;
	}

	/**
	 * @return the errSelectionNotAbleToEdit
	 */
	public String getErrSelectionNotAbleToEdit() {
		return ErrSelectionNotAbleToEdit;
	}

	/**
	 * @param errSelectionNotAbleToEdit the errSelectionNotAbleToEdit to set
	 */
	public void setErrSelectionNotAbleToEdit(String errSelectionNotAbleToEdit) {
		ErrSelectionNotAbleToEdit = errSelectionNotAbleToEdit;
	}

	/**
	 * @return the errLookUpNotAvalidPartOfTeamMember
	 */
	public String getErrLookUpNotAvalidPartOfTeamMember() {
		return ErrLookUpNotAvalidPartOfTeamMember;
	}

	/**
	 * @param errLookUpNotAvalidPartOfTeamMember the errLookUpNotAvalidPartOfTeamMember to set
	 */
	public void setErrLookUpNotAvalidPartOfTeamMember(String errLookUpNotAvalidPartOfTeamMember) {
		ErrLookUpNotAvalidPartOfTeamMember = errLookUpNotAvalidPartOfTeamMember;
	}

	public String getSecurity_labels_TeamMemberName() {
		return Security_labels_TeamMemberName;
	}

	public void setSecurity_labels_TeamMemberName(String security_labels_TeamMemberName) {
		Security_labels_TeamMemberName = security_labels_TeamMemberName;
	}

	public String Security_labels_Select;
	 
	 public  String getSecurity_labels_Select() {
	  return Security_labels_Select;
	 }

	 public void setSecurity_labels_Select(String security_labels_Select) {
	  Security_labels_Select = security_labels_Select;
	 }
	
}
