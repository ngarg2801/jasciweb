/**
 
Date Developed  Dec 30 2014
Description  pojo class of Menu profile header in which getter and setter methods
developed by Sarvendra tyagi

 */

package com.jasci.biz.AdminModule.be;


public class MENUPROFILEBEANPOJOBE {
	


	
	
	public String getMenuType() {
		return MenuType;
	}



	public void setMenuType(String menuType) {
		MenuType = menuType;
	}



	public String getTenant() {
		return Tenant;
	}



	public void setTenant(String tenant) {
		Tenant = tenant;
	}



	public String getMenuProfile() {
		return MenuProfile;
	}



	public void setMenuProfile(String menuProfile) {
		MenuProfile = menuProfile;
	}



	public String getDescription20() {
		return Description20;
	}



	public void setDescription20(String description20) {
		Description20 = description20;
	}



	public String getDescription50() {
		return Description50;
	}



	public void setDescription50(String description50) {
		Description50 = description50;
	}



	public String getAppIcon() {
		return AppIcon;
	}



	public void setAppIcon(String appIcon) {
		AppIcon = appIcon;
	}



	public String getHelpLine() {
		return HelpLine;
	}



	public void setHelpLine(String helpLine) {
		HelpLine = helpLine;
	}



	public String getLastActivityDate() {
		return LastActivityDate;
	}



	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}



	public String getLastActivityTeamMember() {
		return LastActivityTeamMember;
	}



	public void setLastActivityTeamMember(String lastActivityTeamMember) {
		LastActivityTeamMember = lastActivityTeamMember;
	}



	private String MenuType;
	
	
	private String Tenant;
	
	
	private String MenuProfile;
	
	
	
	
	private String Description20;
	
	
	
	
	private String Description50;
	
	
	private String AppIcon;
	
	
	private String HelpLine;
	
	private String LastActivityDate;
	
	
	
	private String LastActivityTeamMember;



}
