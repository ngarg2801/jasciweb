/*
 
Date Developed  nov 01 2014
Created By Diskha Gupta
Description   Login bean class to set the screen labels on login screen
 */
package com.jasci.biz.AdminModule.be;

public class LOGINBE 
{
	private String StrMessageBoard;
	private String StrLoginToAccount;
	private String StrErrorMessageInvalidUser;
	private String StrErrorMessageNumberAttempts;

	private String StrUserId;
	private String StrPassword;
	private String StrRememberMe;
	private String StrLoginButton;
	private String StrForgotPassword;
	private String StrNoWorriesClick;
	private String StrHereLink;
	private String StrResetPassword;
	private String StrForgetPasswordOnNewPassword;
	private String StrEnterEmailMessage;
	private String StrBackButton;
	private String StrSubmitButton;
	private String StrErrorMessageLoginNotAllowedCompanyDeleted;
	
	
	public String getStrErrorMessageLoginNotAllowedCompanyDeleted() {
		return StrErrorMessageLoginNotAllowedCompanyDeleted;
	}
	public void setStrErrorMessageLoginNotAllowedCompanyDeleted(String strErrorMessageLoginNotAllowedCompanyDeleted) {
		StrErrorMessageLoginNotAllowedCompanyDeleted = strErrorMessageLoginNotAllowedCompanyDeleted;
	}
	public String getStrErrorMessageInvalidUser() {
		return StrErrorMessageInvalidUser;
	}
	public void setStrErrorMessageInvalidUser(String strErrorMessageInvalidUser) {
		StrErrorMessageInvalidUser = strErrorMessageInvalidUser;
	}
	public String getStrErrorMessageNumberAttempts() {
		return StrErrorMessageNumberAttempts;
	}
	public void setStrErrorMessageNumberAttempts(
			String strErrorMessageNumberAttempts) {
		StrErrorMessageNumberAttempts = strErrorMessageNumberAttempts;
	}
	public String getStrForgetPasswordOnNewPassword() {
		return StrForgetPasswordOnNewPassword;
	}
	public void setStrForgetPasswordOnNewPassword(
			String strForgetPasswordOnNewPassword) {
		StrForgetPasswordOnNewPassword = strForgetPasswordOnNewPassword;
	}
	public String getStrMessageBoard() {
		return StrMessageBoard;
	}
	public void setStrMessageBoard(String strMessageBoard) {
		StrMessageBoard = strMessageBoard;
	}
	public String getStrLoginToAccount() {
		return StrLoginToAccount;
	}
	public void setStrLoginToAccount(String strLoginToAccount) {
		StrLoginToAccount = strLoginToAccount;
	}
	
	public String getStrUserId() {
		return StrUserId;
	}
	public void setStrUserId(String strUserId) {
		StrUserId = strUserId;
	}
	public String getStrPassword() {
		return StrPassword;
	}
	public void setStrPassword(String strPassword) {
		StrPassword = strPassword;
	}
	public String getStrRememberMe() {
		return StrRememberMe;
	}
	public void setStrRememberMe(String strRememberMe) {
		StrRememberMe = strRememberMe;
	}
	public String getStrLoginButton() {
		return StrLoginButton;
	}
	public void setStrLoginButton(String strLoginButton) {
		StrLoginButton = strLoginButton;
	}
	public String getStrForgotPassword() {
		return StrForgotPassword;
	}
	public void setStrForgotPassword(String strForgotPassword) {
		StrForgotPassword = strForgotPassword;
	}
	public String getStrNoWorriesClick() {
		return StrNoWorriesClick;
	}
	public void setStrNoWorriesClick(String strNoWorriesClick) {
		StrNoWorriesClick = strNoWorriesClick;
	}
	public String getStrHereLink() {
		return StrHereLink;
	}
	public void setStrHereLink(String strHereLink) {
		StrHereLink = strHereLink;
	}
	public String getStrResetPassword() {
		return StrResetPassword;
	}
	public void setStrResetPassword(String strResetPassword) {
		StrResetPassword = strResetPassword;
	}
	
	public String getStrEnterEmailMessage() {
		return StrEnterEmailMessage;
	}
	public void setStrEnterEmailMessage(String strEnterEmailMessage) {
		StrEnterEmailMessage = strEnterEmailMessage;
	}
	public String getStrBackButton() {
		return StrBackButton;
	}
	public void setStrBackButton(String strBackButton) {
		StrBackButton = strBackButton;
	}
	public String getStrSubmitButton() {
		return StrSubmitButton;
	}
	public void setStrSubmitButton(String strSubmitButton) {
		StrSubmitButton = strSubmitButton;
	}
	
	
	
	
	
}
