/*
Description It is used to make setter and getter for changing text of screen in another language
Created By Aakash Bishnoi
Created Date Dec 2 2014
 */
package com.jasci.biz.AdminModule.be;

public class INFOHELPSBE {

	//For InfoHelp Assignment Lookup screen label and Error Message
		private String InfoHelps_Please_enter_Info_Help;
		private String InfoHelps_Please_select_Info_Help_Type;
		private String InfoHelps_Invalid_Info_Help_Type;
		private String InfoHelps_Invalid_Info_Help;
		private String InfoHelps_Please_enter_Part_of_the_Description;
		private String InfoHelps_Invalid_Description;
		private String Infohelps_Enter_Info_Help;
		private String Infohelp_Part_of_the_Description;
		private String Infohelp_Info_Help_Type;
		private String Infohelp_Info_Help_Assignment_Search;
		private String Infohelp_Select;
		private String Infohelp_New;
		private String Infohelp_DisplayAll;
		
		private String  InfoHelpassignmentmaintenance_Reset;
		  
		 public String getInfoHelpassignmentmaintenance_Reset() {
		   return InfoHelpassignmentmaintenance_Reset;
		  }
		  public void setInfoHelpassignmentmaintenance_Reset(
		    String infoHelpassignmentmaintenance_Reset) {
		   InfoHelpassignmentmaintenance_Reset = infoHelpassignmentmaintenance_Reset;
		  }
		
	public String getInfohelps_Enter_Info_Help() {
		return Infohelps_Enter_Info_Help;
	}
	public void setInfohelps_Enter_Info_Help(String infohelps_Enter_Info_Help) {
		Infohelps_Enter_Info_Help = infohelps_Enter_Info_Help;
	}
	
	public String getInfohelp_Part_of_the_Description() {
		return Infohelp_Part_of_the_Description;
	}
	public void setInfohelp_Part_of_the_Description(
			String infohelp_Part_of_the_Description) {
		Infohelp_Part_of_the_Description = infohelp_Part_of_the_Description;
	}
	
	public String getInfohelp_Info_Help_Type() {
		return Infohelp_Info_Help_Type;
	}
	public void setInfohelp_Info_Help_Type(String infohelp_Info_Help_Type) {
		Infohelp_Info_Help_Type = infohelp_Info_Help_Type;
	}
	
	public String getInfohelp_Info_Help_Assignment_Search() {
		return Infohelp_Info_Help_Assignment_Search;
	}
	public void setInfohelp_Info_Help_Assignment_Search(
			String infohelp_Info_Help_Assignment_Search) {
		Infohelp_Info_Help_Assignment_Search = infohelp_Info_Help_Assignment_Search;
	}
	
	public String getInfohelp_Select() {
		return Infohelp_Select;
	}
	public void setInfohelp_Select(String infohelp_Select) {
		Infohelp_Select = infohelp_Select;
	}
	
	public String getInfohelp_New() {
		return Infohelp_New;
	}
	public void setInfohelp_New(String infohelp_New) {
		Infohelp_New = infohelp_New;
	}
	
	public String getInfohelp_DisplayAll() {
		return Infohelp_DisplayAll;
	}
	public void setInfohelp_DisplayAll(String infohelp_DisplayAll) {
		Infohelp_DisplayAll = infohelp_DisplayAll;
	}
	
	public String getInfoHelps_Invalid_Description() {
		return InfoHelps_Invalid_Description;
	}
	public void setInfoHelps_Invalid_Description(
			String infoHelps_Invalid_Description) {
		InfoHelps_Invalid_Description = infoHelps_Invalid_Description;
	}
	public String getInfoHelps_Please_enter_Info_Help() {
		return InfoHelps_Please_enter_Info_Help;
	}
	public void setInfoHelps_Please_enter_Info_Help(
			String infoHelps_Please_enter_Info_Help) {
		InfoHelps_Please_enter_Info_Help = infoHelps_Please_enter_Info_Help;
	}
	public String getInfoHelps_Please_select_Info_Help_Type() {
		return InfoHelps_Please_select_Info_Help_Type;
	}
	public void setInfoHelps_Please_select_Info_Help_Type(
			String infoHelps_Please_select_Info_Help_Type) {
		InfoHelps_Please_select_Info_Help_Type = infoHelps_Please_select_Info_Help_Type;
	}
	public String getInfoHelps_Invalid_Info_Help_Type() {
		return InfoHelps_Invalid_Info_Help_Type;
	}
	public void setInfoHelps_Invalid_Info_Help_Type(
			String infoHelps_Invalid_Info_Help_Type) {
		InfoHelps_Invalid_Info_Help_Type = infoHelps_Invalid_Info_Help_Type;
	}
	public String getInfoHelps_Invalid_Info_Help() {
		return InfoHelps_Invalid_Info_Help;
	}
	public void setInfoHelps_Invalid_Info_Help(String infoHelps_Invalid_Info_Help) {
		InfoHelps_Invalid_Info_Help = infoHelps_Invalid_Info_Help;
	}
	public String getInfoHelps_Please_enter_Part_of_the_Description() {
		return InfoHelps_Please_enter_Part_of_the_Description;
	}
	public void setInfoHelps_Please_enter_Part_of_the_Description(
			String infoHelps_Please_enter_Part_of_the_Description) {
		InfoHelps_Please_enter_Part_of_the_Description = infoHelps_Please_enter_Part_of_the_Description;
	}

	
	
	//For InfoHelp Assignment Maintenance screen label and Error Message
	private String InfoHelpassignmentmaintenance_Confirm_Success;
	private String InfoHelpassignmentmaintenance_Confirm_Update;
	private String InfoHelpassignmentmaintenance_Infohelps_already_used;
	private String InfoHelpassignmentmaintenance_Mandotry_field_can_not_blank;
	private String InfoHelpassignmentmaintenance_Execution_Path;
	private String InfoHelpassignmentmaintenance_Description_Long;
	private String InfoHelpassignmentmaintenance_Description_Short;
	private String InfoHelpassignmentmaintenance_Language;
	private String InfoHelpassignmentmaintenance_Info_Help;
	private String InfoHelpassignmentmaintenance_Info_Help_Type;
	private String InfoHelpassignmentmaintenance_Last_Activity_By;
	private String InfoHelpassignmentmaintenance_Last_Activity_Date;
	private String InfoHelpassignmentmaintenance_Internal_Error;
	private String InfoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance;
	private String InfoHelpassignmentmaintenance_Select;
	private String InfoHelpassignmentmaintenance_SaveUpdate;
	private String InfoHelpassignmentmaintenance_Cancel;
	
	private String InfoHelpassignmentmaintenance_Choose_File;
	private String InfoHelpassignmentmaintenance_Loading;
	private String InfoHelpassignmentmaintenance_Supported_formats_is_pdf;
	
	
	public String getInfoHelpassignmentmaintenance_Choose_File() {
		return InfoHelpassignmentmaintenance_Choose_File;
	}
	public void setInfoHelpassignmentmaintenance_Choose_File(
			String infoHelpassignmentmaintenance_Choose_File) {
		InfoHelpassignmentmaintenance_Choose_File = infoHelpassignmentmaintenance_Choose_File;
	}
	public String getInfoHelpassignmentmaintenance_Loading() {
		return InfoHelpassignmentmaintenance_Loading;
	}
	public void setInfoHelpassignmentmaintenance_Loading(
			String infoHelpassignmentmaintenance_Loading) {
		InfoHelpassignmentmaintenance_Loading = infoHelpassignmentmaintenance_Loading;
	}
	public String getInfoHelpassignmentmaintenance_Supported_formats_is_pdf() {
		return InfoHelpassignmentmaintenance_Supported_formats_is_pdf;
	}
	public void setInfoHelpassignmentmaintenance_Supported_formats_is_pdf(
			String infoHelpassignmentmaintenance_Supported_formats_is_pdf) {
		InfoHelpassignmentmaintenance_Supported_formats_is_pdf = infoHelpassignmentmaintenance_Supported_formats_is_pdf;
	}



	
	
	
	public String getInfoHelpassignmentmaintenance_Confirg_Update() {
		return InInfoHelpassignmentmaintenance_Confirg_Update;
	}
	public void setInfoHelpassignmentmaintenance_Confirg_Update(
			String inInfoHelpassignmentmaintenance_Confirg_Update) {
		InInfoHelpassignmentmaintenance_Confirg_Update = inInfoHelpassignmentmaintenance_Confirg_Update;
	}
	public String getInfoHelpassignmentmaintenance_Confirm_Success() {
		return InfoHelpassignmentmaintenance_Confirm_Success;
	}
	public void setInfoHelpassignmentmaintenance_Confirm_Success(
			String infoHelpassignmentmaintenance_Confirm_Success) {
		InfoHelpassignmentmaintenance_Confirm_Success = infoHelpassignmentmaintenance_Confirm_Success;
	}
	public String getInfoHelpassignmentmaintenance_Infohelps_already_used() {
		return InfoHelpassignmentmaintenance_Infohelps_already_used;
	}
	public void setInfoHelpassignmentmaintenance_Infohelps_already_used(
			String infoHelpassignmentmaintenance_Infohelps_already_used) {
		InfoHelpassignmentmaintenance_Infohelps_already_used = infoHelpassignmentmaintenance_Infohelps_already_used;
	}
	public String getInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank() {
		return InfoHelpassignmentmaintenance_Mandotry_field_can_not_blank;
	}
	public void setInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank(
			String infoHelpassignmentmaintenance_Mandotry_field_can_not_blank) {
		InfoHelpassignmentmaintenance_Mandotry_field_can_not_blank = infoHelpassignmentmaintenance_Mandotry_field_can_not_blank;
	}
	public String getInfoHelpassignmentmaintenance_Execution_Path() {
		return InfoHelpassignmentmaintenance_Execution_Path;
	}
	public void setInfoHelpassignmentmaintenance_Execution_Path(
			String infoHelpassignmentmaintenance_Execution_Path) {
		InfoHelpassignmentmaintenance_Execution_Path = infoHelpassignmentmaintenance_Execution_Path;
	}
	public String getInfoHelpassignmentmaintenance_Description_Long() {
		return InfoHelpassignmentmaintenance_Description_Long;
	}
	public void setInfoHelpassignmentmaintenance_Description_Long(
			String infoHelpassignmentmaintenance_Description_Long) {
		InfoHelpassignmentmaintenance_Description_Long = infoHelpassignmentmaintenance_Description_Long;
	}
	public String getInfoHelpassignmentmaintenance_Description_Short() {
		return InfoHelpassignmentmaintenance_Description_Short;
	}
	public void setInfoHelpassignmentmaintenance_Description_Short(
			String infoHelpassignmentmaintenance_Description_Short) {
		InfoHelpassignmentmaintenance_Description_Short = infoHelpassignmentmaintenance_Description_Short;
	}
	public String getInfoHelpassignmentmaintenance_Language() {
		return InfoHelpassignmentmaintenance_Language;
	}
	public void setInfoHelpassignmentmaintenance_Language(
			String infoHelpassignmentmaintenance_Language) {
		InfoHelpassignmentmaintenance_Language = infoHelpassignmentmaintenance_Language;
	}
	public String getInfoHelpassignmentmaintenance_Info_Help() {
		return InfoHelpassignmentmaintenance_Info_Help;
	}
	public void setInfoHelpassignmentmaintenance_Info_Help(
			String infoHelpassignmentmaintenance_Info_Help) {
		InfoHelpassignmentmaintenance_Info_Help = infoHelpassignmentmaintenance_Info_Help;
	}
	public String getInfoHelpassignmentmaintenance_Info_Help_Type() {
		return InfoHelpassignmentmaintenance_Info_Help_Type;
	}
	public void setInfoHelpassignmentmaintenance_Info_Help_Type(
			String infoHelpassignmentmaintenance_Info_Help_Type) {
		InfoHelpassignmentmaintenance_Info_Help_Type = infoHelpassignmentmaintenance_Info_Help_Type;
	}
	public String getInfoHelpassignmentmaintenance_Last_Activity_By() {
		return InfoHelpassignmentmaintenance_Last_Activity_By;
	}
	public void setInfoHelpassignmentmaintenance_Last_Activity_By(
			String infoHelpassignmentmaintenance_Last_Activity_By) {
		InfoHelpassignmentmaintenance_Last_Activity_By = infoHelpassignmentmaintenance_Last_Activity_By;
	}
	public String getInfoHelpassignmentmaintenance_Last_Activity_Date() {
		return InfoHelpassignmentmaintenance_Last_Activity_Date;
	}
	public void setInfoHelpassignmentmaintenance_Last_Activity_Date(
			String infoHelpassignmentmaintenance_Last_Activity_Date) {
		InfoHelpassignmentmaintenance_Last_Activity_Date = infoHelpassignmentmaintenance_Last_Activity_Date;
	}
	public String getInfoHelpassignmentmaintenance_Internal_Error() {
		return InfoHelpassignmentmaintenance_Internal_Error;
	}
	public void setInfoHelpassignmentmaintenance_Internal_Error(
			String infoHelpassignmentmaintenance_Internal_Error) {
		InfoHelpassignmentmaintenance_Internal_Error = infoHelpassignmentmaintenance_Internal_Error;
	}
	public String getInfoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance() {
		return InfoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance;
	}
	public void setInfoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance(
			String infoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance) {
		InfoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance = infoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance;
	}

	public String getInfoHelpassignmentmaintenance_Select() {
		return InfoHelpassignmentmaintenance_Select;
	}
	public void setInfoHelpassignmentmaintenance_Select(
			String infoHelpassignmentmaintenance_Select) {
		InfoHelpassignmentmaintenance_Select = infoHelpassignmentmaintenance_Select;
	}
	
	public String getInfoHelpassignmentmaintenance_SaveUpdate() {
		return InfoHelpassignmentmaintenance_SaveUpdate;
	}
	public void setInfoHelpassignmentmaintenance_SaveUpdate(
			String infoHelpassignmentmaintenance_SaveUpdate) {
		InfoHelpassignmentmaintenance_SaveUpdate = infoHelpassignmentmaintenance_SaveUpdate;
	}
	public String getInfoHelpassignmentmaintenance_Cancel() {
		return InfoHelpassignmentmaintenance_Cancel;
	}
	public void setInfoHelpassignmentmaintenance_Cancel(
			String infoHelpassignmentmaintenance_Cancel) {
		InfoHelpassignmentmaintenance_Cancel = infoHelpassignmentmaintenance_Cancel;
	}
	public String getInfoHelpassignmentmaintenance_Confirm_Update() {
		return InfoHelpassignmentmaintenance_Confirm_Update;
	}
	public void setInfoHelpassignmentmaintenance_Confirm_Update(
			String infoHelpassignmentmaintenance_Confirm_Update) {
		InfoHelpassignmentmaintenance_Confirm_Update = infoHelpassignmentmaintenance_Confirm_Update;
	}
	

	
	//For InfoHelp Assignment Search Lookup screen label and Error Message
		private String InfoHelpSearchlookup_Edit;
		private String InfoHelpSearchlookup_Delete;
		private String InfoHelpSearchlookup_Add_New;
		private String InfoHelpSearchlookup_Action;
		private String InfoHelpSearchlookup_Discription;
		private String InfoHelpSearchlookup_Language;
		private String InfoHelpSearchlookup_Info_Help;
		private String InfoHelpSearchlookup_Info_Help_Type;
		private String InfoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup;
		private String InfoHelpSearchlookup_Confirm_Delete;
		private String InInfoHelpassignmentmaintenance_Confirg_Update;

	public String getInfoHelpSearchlookup_Edit() {
		return InfoHelpSearchlookup_Edit;
	}
	public void setInfoHelpSearchlookup_Edit(String infoHelpSearchlookup_Edit) {
		InfoHelpSearchlookup_Edit = infoHelpSearchlookup_Edit;
	}
	public String getInfoHelpSearchlookup_Delete() {
		return InfoHelpSearchlookup_Delete;
	}
	public void setInfoHelpSearchlookup_Delete(String infoHelpSearchlookup_Delete) {
		InfoHelpSearchlookup_Delete = infoHelpSearchlookup_Delete;
	}
	public String getInfoHelpSearchlookup_Add_New() {
		return InfoHelpSearchlookup_Add_New;
	}
	public void setInfoHelpSearchlookup_Add_New(String infoHelpSearchlookup_Add_New) {
		InfoHelpSearchlookup_Add_New = infoHelpSearchlookup_Add_New;
	}
	public String getInfoHelpSearchlookup_Action() {
		return InfoHelpSearchlookup_Action;
	}
	public void setInfoHelpSearchlookup_Action(String infoHelpSearchlookup_Action) {
		InfoHelpSearchlookup_Action = infoHelpSearchlookup_Action;
	}
	public String getInfoHelpSearchlookup_Discription() {
		return InfoHelpSearchlookup_Discription;
	}
	public void setInfoHelpSearchlookup_Discription(
			String infoHelpSearchlookup_Discription) {
		InfoHelpSearchlookup_Discription = infoHelpSearchlookup_Discription;
	}
	public String getInfoHelpSearchlookup_Language() {
		return InfoHelpSearchlookup_Language;
	}
	public void setInfoHelpSearchlookup_Language(
			String infoHelpSearchlookup_Language) {
		InfoHelpSearchlookup_Language = infoHelpSearchlookup_Language;
	}
	public String getInfoHelpSearchlookup_Info_Help() {
		return InfoHelpSearchlookup_Info_Help;
	}
	public void setInfoHelpSearchlookup_Info_Help(
			String infoHelpSearchlookup_Info_Help) {
		InfoHelpSearchlookup_Info_Help = infoHelpSearchlookup_Info_Help;
	}
	public String getInfoHelpSearchlookup_Info_Help_Type() {
		return InfoHelpSearchlookup_Info_Help_Type;
	}
	public void setInfoHelpSearchlookup_Info_Help_Type(
			String infoHelpSearchlookup_Info_Help_Type) {
		InfoHelpSearchlookup_Info_Help_Type = infoHelpSearchlookup_Info_Help_Type;
	}
	public String getInfoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup() {
		return InfoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup;
	}
	public void setInfoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup(
			String infoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup) {
		InfoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup = infoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup;
	}
	public String getInfoHelpSearchlookup_Confirm_Delete() {
		return InfoHelpSearchlookup_Confirm_Delete;
	}
	public void setInfoHelpSearchlookup_Confirm_Delete(
			String infoHelpSearchlookup_Confirm_Delete) {
		InfoHelpSearchlookup_Confirm_Delete = infoHelpSearchlookup_Confirm_Delete;
	}
	

	
	
	
}
