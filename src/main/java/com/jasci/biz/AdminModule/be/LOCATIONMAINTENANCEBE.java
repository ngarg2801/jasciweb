/*
Description It is used to make setter and getter for changing text of screen in another language
Created By Aakash Bishnoi
Created Date Dec 27 2014
 */
package com.jasci.biz.AdminModule.be;

public class LOCATIONMAINTENANCEBE {
	
	public   String getLocationMaintenace_Location_Search_Lookup() {
		return LocationMaintenace_Location_Search_Lookup;
	}
	public   void setLocationMaintenace_Location_Search_Lookup(
			String locationMaintenace_Location_Search_Lookup) {
		LocationMaintenace_Location_Search_Lookup = locationMaintenace_Location_Search_Lookup;
	}
	public   String getLocationMaintenace_Area() {
		return LocationMaintenace_Area;
	}
	public   void setLocationMaintenace_Area(String locationMaintenace_Area) {
		LocationMaintenace_Area = locationMaintenace_Area;
	}
	public   String getLocationMaintenace_FulFillment_Center() {
		return LocationMaintenace_FulFillment_Center;
	}
	public   void setLocationMaintenace_FulFillment_Center(
			String locationMaintenace_FulFillment_Center) {
		LocationMaintenace_FulFillment_Center = locationMaintenace_FulFillment_Center;
	}
	public   String getLocationMaintenace_Location() {
		return LocationMaintenace_Location;
	}
	public   void setLocationMaintenace_Location(
			String locationMaintenace_Location) {
		LocationMaintenace_Location = locationMaintenace_Location;
	}
	public   String getLocationMaintenace_Description() {
		return LocationMaintenace_Description;
	}
	public   void setLocationMaintenace_Description(
			String locationMaintenace_Description) {
		LocationMaintenace_Description = locationMaintenace_Description;
	}
	public   String getLocationMaintenace_Location_Type() {
		return LocationMaintenace_Location_Type;
	}
	public   void setLocationMaintenace_Location_Type(
			String locationMaintenace_Location_Type) {
		LocationMaintenace_Location_Type = locationMaintenace_Location_Type;
	}
	public   String getLocationMaintenace_Last_Activity_Task() {
		return LocationMaintenace_Last_Activity_Task;
	}
	public   void setLocationMaintenace_Last_Activity_Task(
			String locationMaintenace_Last_Activity_Task) {
		LocationMaintenace_Last_Activity_Task = locationMaintenace_Last_Activity_Task;
	}
	public   String getLocationMaintenace_Copy() {
		return LocationMaintenace_Copy;
	}
	public   void setLocationMaintenace_Copy(String locationMaintenace_Copy) {
		LocationMaintenace_Copy = locationMaintenace_Copy;
	}
	public   String getLocationMaintenace_Notes() {
		return LocationMaintenace_Notes;
	}
	public   void setLocationMaintenace_Notes(String locationMaintenace_Notes) {
		LocationMaintenace_Notes = locationMaintenace_Notes;
	}
	public   String getLocationMaintenace_Any_Part_of_Location_Description() {
		return LocationMaintenace_Any_Part_of_Location_Description;
	}
	public   void setLocationMaintenace_Any_Part_of_Location_Description(
			String locationMaintenace_Any_Part_of_Location_Description) {
		LocationMaintenace_Any_Part_of_Location_Description = locationMaintenace_Any_Part_of_Location_Description;
	}
	public   String getLocationMaintenace_Location_Lookup() {
		return LocationMaintenace_Location_Lookup;
	}
	public   void setLocationMaintenace_Location_Lookup(
			String locationMaintenace_Location_Lookup) {
		LocationMaintenace_Location_Lookup = locationMaintenace_Location_Lookup;
	}
	public   String getLocationMaintenace_Location_Maintenance() {
		return LocationMaintenace_Location_Maintenance;
	}
	public   void setLocationMaintenace_Location_Maintenance(
			String locationMaintenace_Location_Maintenance) {
		LocationMaintenace_Location_Maintenance = locationMaintenace_Location_Maintenance;
	}
	public   String getLocationMaintenace_Location_Short() {
		return LocationMaintenace_Location_Short;
	}
	public   void setLocationMaintenace_Location_Short(
			String locationMaintenace_Location_Short) {
		LocationMaintenace_Location_Short = locationMaintenace_Location_Short;
	}
	public   String getLocationMaintenace_Location_Long() {
		return LocationMaintenace_Location_Long;
	}
	public   void setLocationMaintenace_Location_Long(
			String locationMaintenace_Location_Long) {
		LocationMaintenace_Location_Long = locationMaintenace_Location_Long;
	}
	public   String getLocationMaintenace_Location_Profile() {
		return LocationMaintenace_Location_Profile;
	}
	public   void setLocationMaintenace_Location_Profile(
			String locationMaintenace_Location_Profile) {
		LocationMaintenace_Location_Profile = locationMaintenace_Location_Profile;
	}
	public   String getLocationMaintenace_Wizard_ID() {
		return LocationMaintenace_Wizard_ID;
	}
	public   void setLocationMaintenace_Wizard_ID(
			String locationMaintenace_Wizard_ID) {
		LocationMaintenace_Wizard_ID = locationMaintenace_Wizard_ID;
	}
	public   String getLocationMaintenace_Wizard_Control_Number() {
		return LocationMaintenace_Wizard_Control_Number;
	}
	public   void setLocationMaintenace_Wizard_Control_Number(
			String locationMaintenace_Wizard_Control_Number) {
		LocationMaintenace_Wizard_Control_Number = locationMaintenace_Wizard_Control_Number;
	}
	public   String getLocationMaintenace_Work_Group_Zone() {
		return LocationMaintenace_Work_Group_Zone;
	}
	public   void setLocationMaintenace_Work_Group_Zone(
			String locationMaintenace_Work_Group_Zone) {
		LocationMaintenace_Work_Group_Zone = locationMaintenace_Work_Group_Zone;
	}
	public   String getLocationMaintenace_Work_Zone() {
		return LocationMaintenace_Work_Zone;
	}
	public   void setLocationMaintenace_Work_Zone(
			String locationMaintenace_Work_Zone) {
		LocationMaintenace_Work_Zone = locationMaintenace_Work_Zone;
	}
	public   String getLocationMaintenace_Check_Digit() {
		return LocationMaintenace_Check_Digit;
	}
	public   void setLocationMaintenace_Check_Digit(
			String locationMaintenace_Check_Digit) {
		LocationMaintenace_Check_Digit = locationMaintenace_Check_Digit;
	}
	public   String getLocationMaintenace_Alternate_Location() {
		return LocationMaintenace_Alternate_Location;
	}
	public   void setLocationMaintenace_Alternate_Location(
			String locationMaintenace_Alternate_Location) {
		LocationMaintenace_Alternate_Location = locationMaintenace_Alternate_Location;
	}
	public   String getLocationMaintenace_Location_Height() {
		return LocationMaintenace_Location_Height;
	}
	public   void setLocationMaintenace_Location_Height(
			String locationMaintenace_Location_Height) {
		LocationMaintenace_Location_Height = locationMaintenace_Location_Height;
	}
	public   String getLocationMaintenace_Location_Width() {
		return LocationMaintenace_Location_Width;
	}
	public   void setLocationMaintenace_Location_Width(
			String locationMaintenace_Location_Width) {
		LocationMaintenace_Location_Width = locationMaintenace_Location_Width;
	}
	public   String getLocationMaintenace_Location_Depth() {
		return LocationMaintenace_Location_Depth;
	}
	public   void setLocationMaintenace_Location_Depth(
			String locationMaintenace_Location_Depth) {
		LocationMaintenace_Location_Depth = locationMaintenace_Location_Depth;
	}
	public   String getLocationMaintenace_Location_Weight_Capacity() {
		return LocationMaintenace_Location_Weight_Capacity;
	}
	public   void setLocationMaintenace_Location_Weight_Capacity(
			String locationMaintenace_Location_Weight_Capacity) {
		LocationMaintenace_Location_Weight_Capacity = locationMaintenace_Location_Weight_Capacity;
	}
	public   String getLocationMaintenace_Location_Height_Capacity() {
		return LocationMaintenace_Location_Height_Capacity;
	}
	public   void setLocationMaintenace_Location_Height_Capacity(
			String locationMaintenace_Location_Height_Capacity) {
		LocationMaintenace_Location_Height_Capacity = locationMaintenace_Location_Height_Capacity;
	}
	public   String getLocationMaintenace_Number_of_Pallets() {
		return LocationMaintenace_Number_of_Pallets;
	}
	public   void setLocationMaintenace_Number_of_Pallets(
			String locationMaintenace_Number_of_Pallets) {
		LocationMaintenace_Number_of_Pallets = locationMaintenace_Number_of_Pallets;
	}
	public   String getLocationMaintenace_Number_of_Floor_Pallet_Location() {
		return LocationMaintenace_Number_of_Floor_Pallet_Location;
	}
	public   void setLocationMaintenace_Number_of_Floor_Pallet_Location(
			String locationMaintenace_Number_of_Floor_Pallet_Location) {
		LocationMaintenace_Number_of_Floor_Pallet_Location = locationMaintenace_Number_of_Floor_Pallet_Location;
	}
	public   String getLocationMaintenace_Allocation_Allowable() {
		return LocationMaintenace_Allocation_Allowable;
	}
	public   void setLocationMaintenace_Allocation_Allowable(
			String locationMaintenace_Allocation_Allowable) {
		LocationMaintenace_Allocation_Allowable = locationMaintenace_Allocation_Allowable;
	}
	public   String getLocationMaintenace_Free_Location_When_Zero() {
		return LocationMaintenace_Free_Location_When_Zero;
	}
	public   void setLocationMaintenace_Free_Location_When_Zero(
			String locationMaintenace_Free_Location_When_Zero) {
		LocationMaintenace_Free_Location_When_Zero = locationMaintenace_Free_Location_When_Zero;
	}
	public   String getLocationMaintenace_Free_Prime_Location_in_99999_Days() {
		return LocationMaintenace_Free_Prime_Location_in_99999_Days;
	}
	public   void setLocationMaintenace_Free_Prime_Location_in_99999_Days(
			String locationMaintenace_Free_Prime_Location_in_99999_Days) {
		LocationMaintenace_Free_Prime_Location_in_99999_Days = locationMaintenace_Free_Prime_Location_in_99999_Days;
	}
	public   String getLocationMaintenace_Multiple_Products_In_The_Same_Location() {
		return LocationMaintenace_Multiple_Products_In_The_Same_Location;
	}
	public   void setLocationMaintenace_Multiple_Products_In_The_Same_Location(
			String locationMaintenace_Multiple_Products_In_The_Same_Location) {
		LocationMaintenace_Multiple_Products_In_The_Same_Location = locationMaintenace_Multiple_Products_In_The_Same_Location;
	}
	public   String getLocationMaintenace_Storage_Type() {
		return LocationMaintenace_Storage_Type;
	}
	public   void setLocationMaintenace_Storage_Type(
			String locationMaintenace_Storage_Type) {
		LocationMaintenace_Storage_Type = locationMaintenace_Storage_Type;
	}
	public   String getLocationMaintenace_Slotting() {
		return LocationMaintenace_Slotting;
	}
	public   void setLocationMaintenace_Slotting(
			String locationMaintenace_Slotting) {
		LocationMaintenace_Slotting = locationMaintenace_Slotting;
	}
	public   String getLocationMaintenace_Manual_Assigned_Location() {
		return LocationMaintenace_Manual_Assigned_Location;
	}
	public   void setLocationMaintenace_Manual_Assigned_Location(
			String locationMaintenace_Manual_Assigned_Location) {
		LocationMaintenace_Manual_Assigned_Location = locationMaintenace_Manual_Assigned_Location;
	}
	public   String getLocationMaintenace_Used_In_Slotting() {
		return LocationMaintenace_Used_In_Slotting;
	}
	public   void setLocationMaintenace_Used_In_Slotting(
			String locationMaintenace_Used_In_Slotting) {
		LocationMaintenace_Used_In_Slotting = locationMaintenace_Used_In_Slotting;
	}
	public   String getLocationMaintenace_Select() {
		return LocationMaintenace_Select;
	}
	public   void setLocationMaintenace_Select(String locationMaintenace_Select) {
		LocationMaintenace_Select = locationMaintenace_Select;
	}
	public   String getLocationMaintenace_Yes() {
		return LocationMaintenace_Yes;
	}
	public   void setLocationMaintenace_Yes(String locationMaintenace_Yes) {
		LocationMaintenace_Yes = locationMaintenace_Yes;
	}
	public   String getLocationMaintenace_No() {
		return LocationMaintenace_No;
	}
	public   void setLocationMaintenace_No(String locationMaintenace_No) {
		LocationMaintenace_No = locationMaintenace_No;
	}
	public   String getLocationMaintenace_Invalid_Location() {
		return LocationMaintenace_Invalid_Location;
	}
	public   void setLocationMaintenace_Invalid_Location(
			String locationMaintenace_Invalid_Location) {
		LocationMaintenace_Invalid_Location = locationMaintenace_Invalid_Location;
	}
	public   String getLocationMaintenace_Please_select_a_Area_first() {
		return LocationMaintenace_Please_select_a_Area_first;
	}
	public   void setLocationMaintenace_Please_select_a_Area_first(
			String locationMaintenace_Please_select_a_Area_first) {
		LocationMaintenace_Please_select_a_Area_first = locationMaintenace_Please_select_a_Area_first;
	}
	public   String getLocationMaintenace_Please_select_a_Location_Type_first() {
		return LocationMaintenace_Please_select_a_Location_Type_first;
	}
	public   void setLocationMaintenace_Please_select_a_Location_Type_first(
			String locationMaintenace_Please_select_a_Location_Type_first) {
		LocationMaintenace_Please_select_a_Location_Type_first = locationMaintenace_Please_select_a_Location_Type_first;
	}
	public   String getLocationMaintenace_Invalid_Location_Description() {
		return LocationMaintenace_Invalid_Location_Description;
	}
	public   void setLocationMaintenace_Invalid_Location_Description(
			String locationMaintenace_Invalid_Location_Description) {
		LocationMaintenace_Invalid_Location_Description = locationMaintenace_Invalid_Location_Description;
	}
	public   String getLocationMaintenace_Please_enter_Location() {
		return LocationMaintenace_Please_enter_Location;
	}
	public   void setLocationMaintenace_Please_enter_Location(
			String locationMaintenace_Please_enter_Location) {
		LocationMaintenace_Please_enter_Location = locationMaintenace_Please_enter_Location;
	}
	public   String getLocationMaintenace_Please_enter_Part_Of_Loctaion_Description() {
		return LocationMaintenace_Please_enter_Part_Of_Loctaion_Description;
	}
	public   void setLocationMaintenace_Please_enter_Part_Of_Loctaion_Description(
			String locationMaintenace_Please_enter_Part_Of_Loctaion_Description) {
		LocationMaintenace_Please_enter_Part_Of_Loctaion_Description = locationMaintenace_Please_enter_Part_Of_Loctaion_Description;
	}
	public   String getLocationMaintenace_Location_is_about_to_be_deleted() {
		return LocationMaintenace_Location_is_about_to_be_deleted;
	}
	public   void setLocationMaintenace_Location_is_about_to_be_deleted(
			String locationMaintenace_Location_is_about_to_be_deleted) {
		LocationMaintenace_Location_is_about_to_be_deleted = locationMaintenace_Location_is_about_to_be_deleted;
	}
	public   String getLocationMaintenace_Location_has_Inventory_and_can_not_be_deleted() {
		return LocationMaintenace_Location_has_Inventory_and_can_not_be_deleted;
	}
	public   void setLocationMaintenace_Location_has_Inventory_and_can_not_be_deleted(
			String locationMaintenace_Location_has_Inventory_and_can_not_be_deleted) {
		LocationMaintenace_Location_has_Inventory_and_can_not_be_deleted = locationMaintenace_Location_has_Inventory_and_can_not_be_deleted;
	}
	public   String getLocationMaintenace_Location_is_still_assigned_and_can_not_be_deleted() {
		return LocationMaintenace_Location_is_still_assigned_and_can_not_be_deleted;
	}
	public   void setLocationMaintenace_Location_is_still_assigned_and_can_not_be_deleted(
			String locationMaintenace_Location_is_still_assigned_and_can_not_be_deleted) {
		LocationMaintenace_Location_is_still_assigned_and_can_not_be_deleted = locationMaintenace_Location_is_still_assigned_and_can_not_be_deleted;
	}
	public   String getLocationMaintenace_Locaton_already_Used() {
		return LocationMaintenace_Locaton_already_Used;
	}
	public   void setLocationMaintenace_Locaton_already_Used(
			String locationMaintenace_Locaton_already_Used) {
		LocationMaintenace_Locaton_already_Used = locationMaintenace_Locaton_already_Used;
	}
	public   String getLocationMaintenace_Alternate_Locaton_already_Used() {
		return LocationMaintenace_Alternate_Locaton_already_Used;
	}
	public   void setLocationMaintenace_Alternate_Locaton_already_Used(
			String locationMaintenace_Alternate_Locaton_already_Used) {
		LocationMaintenace_Alternate_Locaton_already_Used = locationMaintenace_Alternate_Locaton_already_Used;
	}
	public   String getLocationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only() {
		return LocationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only;
	}
	public   void setLocationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only(
			String locationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only) {
		LocationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only = locationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only;
	}
	public   String getLocationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only() {
		return LocationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only;
	}
	public   void setLocationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only(
			String locationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only) {
		LocationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only = locationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only;
	}
	public   String getLocationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only() {
		return LocationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only;
	}
	public   void setLocationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only(
			String locationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only) {
		LocationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only = locationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only;
	}
	public   String getLocationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only() {
		return LocationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only;
	}
	public   void setLocationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only(
			String locationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only) {
		LocationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only = locationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only;
	}
	public   String getLocationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only() {
		return LocationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only;
	}
	public   void setLocationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only(
			String locationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only) {
		LocationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only = locationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only;
	}
	public   String getLocationMaintenace_Wizard_Control_Number_must_be_numeric_only() {
		return LocationMaintenace_Wizard_Control_Number_must_be_numeric_only;
	}
	public   void setLocationMaintenace_Wizard_Control_Number_must_be_numeric_only(
			String locationMaintenace_Wizard_Control_Number_must_be_numeric_only) {
		LocationMaintenace_Wizard_Control_Number_must_be_numeric_only = locationMaintenace_Wizard_Control_Number_must_be_numeric_only;
	}
	public   String getLocationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only() {
		return LocationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only;
	}
	public   void setLocationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only(
			String locationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only) {
		LocationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only = locationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only;
	}
	public   String getLocationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only() {
		return LocationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only;
	}
	public   void setLocationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only(
			String locationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only) {
		LocationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only = locationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only;
	}
	public   String getLocationMaintenace_Number_of_Pallets_must_be_numeric_only() {
		return LocationMaintenace_Number_of_Pallets_must_be_numeric_only;
	}
	public   void setLocationMaintenace_Number_of_Pallets_must_be_numeric_only(
			String locationMaintenace_Number_of_Pallets_must_be_numeric_only) {
		LocationMaintenace_Number_of_Pallets_must_be_numeric_only = locationMaintenace_Number_of_Pallets_must_be_numeric_only;
	}
	public String getLocationMaintenace_New() {
		return LocationMaintenace_New;
	}
	public void setLocationMaintenace_New(String locationMaintenace_New) {
		LocationMaintenace_New = locationMaintenace_New;
	}
	public   String getLocationMaintenace_Maindatory_field_can_not_be_left_blank() {
		return LocationMaintenace_Maindatory_field_can_not_be_left_blank;
	}
	public   void setLocationMaintenace_Maindatory_field_can_not_be_left_blank(
			String locationMaintenace_Maindatory_field_can_not_be_left_blank) {
		LocationMaintenace_Maindatory_field_can_not_be_left_blank = locationMaintenace_Maindatory_field_can_not_be_left_blank;
	}
	
	public  String getLocationMaintenace_Display_All() {
		return LocationMaintenace_Display_All;
	}
	public  void setLocationMaintenace_Display_All(
			String locationMaintenace_Display_All) {
		LocationMaintenace_Display_All = locationMaintenace_Display_All;
	}

	
	
		public String getLocationMaintenace_Add_New() {
		return LocationMaintenace_Add_New;
	}
	public void setLocationMaintenace_Add_New(String locationMaintenace_Add_New) {
		LocationMaintenace_Add_New = locationMaintenace_Add_New;
	}
	public String getLocationMaintenace_Edit() {
		return LocationMaintenace_Edit;
	}
	public void setLocationMaintenace_Edit(String locationMaintenace_Edit) {
		LocationMaintenace_Edit = locationMaintenace_Edit;
	}
	public String getLocationMaintenace_Cancel() {
		return LocationMaintenace_Cancel;
	}
	public void setLocationMaintenace_Cancel(String locationMaintenace_Cancel) {
		LocationMaintenace_Cancel = locationMaintenace_Cancel;
	}
	public String getLocationMaintenace_Delete() {
		return LocationMaintenace_Delete;
	}
	public void setLocationMaintenace_Delete(String locationMaintenace_Delete) {
		LocationMaintenace_Delete = locationMaintenace_Delete;
	}
	public String getLocationMaintenace_Save_Update() {
		return LocationMaintenace_Save_Update;
	}
	public void setLocationMaintenace_Save_Update(
			String locationMaintenace_Save_Update) {
		LocationMaintenace_Save_Update = locationMaintenace_Save_Update;
	}

	public  String getLocationMaintenace_Actions() {
		return LocationMaintenace_Actions;
	}
	public  void setLocationMaintenace_Actions(
			String locationMaintenace_Actions) {
		LocationMaintenace_Actions = locationMaintenace_Actions;
	}
	
	public  String getLocationMaintenace_Last_Activity_Date_YYYY_MM_DD() {
		return LocationMaintenace_Last_Activity_Date_YYYY_MM_DD;
	}
	public  void setLocationMaintenace_Last_Activity_Date_YYYY_MM_DD(
			String locationMaintenace_Last_Activity_Date_YYYY_MM_DD) {
		LocationMaintenace_Last_Activity_Date_YYYY_MM_DD = locationMaintenace_Last_Activity_Date_YYYY_MM_DD;
	}
	public  String getLocationMaintenace_Last_ActivityDate() {
		return LocationMaintenace_Last_ActivityDate;
	}
	public  void setLocationMaintenace_Last_ActivityDate(
			String locationMaintenace_Last_ActivityDate) {
		LocationMaintenace_Last_ActivityDate = locationMaintenace_Last_ActivityDate;
	}
	public  String getLocationMaintenace_Last_Activity_By() {
		return LocationMaintenace_Last_Activity_By;
	}
	public  void setLocationMaintenace_Last_Activity_By(
			String locationMaintenace_Last_Activity_By) {
		LocationMaintenace_Last_Activity_By = locationMaintenace_Last_Activity_By;
	}
	public  String getLocationMaintenace_Your_record_has_been_saved_successfully() {
		return LocationMaintenace_Your_record_has_been_saved_successfully;
	}
	public  void setLocationMaintenace_Your_record_has_been_saved_successfully(
			String locationMaintenace_Your_record_has_been_saved_successfully) {
		LocationMaintenace_Your_record_has_been_saved_successfully = locationMaintenace_Your_record_has_been_saved_successfully;
	}
	public  String getLocationMaintenace_Your_record_has_been_updated_successfully() {
		return LocationMaintenace_Your_record_has_been_updated_successfully;
	}
	public  void setLocationMaintenace_Your_record_has_been_updated_successfully(
			String locationMaintenace_Your_record_has_been_updated_successfully) {
		LocationMaintenace_Your_record_has_been_updated_successfully = locationMaintenace_Your_record_has_been_updated_successfully;
	}
	public  String getLocationMaintenace_Are_you_sure_you_want_to_delete_this_record() {
		return LocationMaintenace_Are_you_sure_you_want_to_delete_this_record;
	}
	public  void setLocationMaintenace_Are_you_sure_you_want_to_delete_this_record(
			String locationMaintenace_Are_you_sure_you_want_to_delete_this_record) {
		LocationMaintenace_Are_you_sure_you_want_to_delete_this_record = locationMaintenace_Are_you_sure_you_want_to_delete_this_record;
	}

	public  String getLocationMaintenace_Loaction_and_Alternate_Location_cannot_be_same() {
		return LocationMaintenace_Loaction_and_Alternate_Location_cannot_be_same;
	}
	public  void setLocationMaintenace_Loaction_and_Alternate_Location_cannot_be_same(
			String locationMaintenace_Loaction_and_Alternate_Location_cannot_be_same) {
		LocationMaintenace_Loaction_and_Alternate_Location_cannot_be_same = locationMaintenace_Loaction_and_Alternate_Location_cannot_be_same;
	}
	

	public  String getLocationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center() {
		return LocationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center;
	}
	public  void setLocationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center(
			String locationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center) {
		LocationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center = locationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center;
	}
	
	public  String getLocationMaintenace_Reset() {
		return LocationMaintenace_Reset;
	}
	public  void setLocationMaintenace_Reset(String locationMaintenace_Reset) {
		LocationMaintenace_Reset = locationMaintenace_Reset;
	}

	public static String LocationMaintenace_Reset;
	public static String LocationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center;
	public static String LocationMaintenace_Loaction_and_Alternate_Location_cannot_be_same;
	public static String LocationMaintenace_Actions;
	public static String LocationMaintenace_Last_Activity_Date_YYYY_MM_DD;
	public static String LocationMaintenace_Last_ActivityDate;
	public static String LocationMaintenace_Last_Activity_By;
	public static String LocationMaintenace_Your_record_has_been_saved_successfully;
	public static String LocationMaintenace_Your_record_has_been_updated_successfully;
	public static String LocationMaintenace_Are_you_sure_you_want_to_delete_this_record;
	
	public static String LocationMaintenace_Cancel;
	public static String LocationMaintenace_Delete;
	public static String LocationMaintenace_Save_Update;
	public static String LocationMaintenace_Display_All;
	public static String LocationMaintenace_Add_New;
	public static String LocationMaintenace_Edit;

	public static String LocationMaintenace_New;
	public static String LocationMaintenace_Location_Search_Lookup;
	public static String LocationMaintenace_Area;
	public static String LocationMaintenace_FulFillment_Center;
	public static String LocationMaintenace_Location;
	public static String LocationMaintenace_Description;
	public static String LocationMaintenace_Location_Type;
	public static String LocationMaintenace_Last_Activity_Task;
	public static String LocationMaintenace_Copy;
	public static String LocationMaintenace_Notes;
	public static String LocationMaintenace_Any_Part_of_Location_Description;
	public static String LocationMaintenace_Location_Lookup;
	public static String LocationMaintenace_Location_Maintenance;
	public static String LocationMaintenace_Location_Short;
	public static String LocationMaintenace_Location_Long;
	public static String LocationMaintenace_Location_Profile;
	public static String LocationMaintenace_Wizard_ID;
	public static String LocationMaintenace_Wizard_Control_Number;
	public static String LocationMaintenace_Work_Group_Zone;
	public static String LocationMaintenace_Work_Zone;
	public static String LocationMaintenace_Check_Digit;
	public static String LocationMaintenace_Alternate_Location;
	public static String LocationMaintenace_Location_Height;
	public static String LocationMaintenace_Location_Width;
	public static String LocationMaintenace_Location_Depth;
	public static String LocationMaintenace_Location_Weight_Capacity;
	public static String LocationMaintenace_Location_Height_Capacity;
	public static String LocationMaintenace_Number_of_Pallets;
	public static String LocationMaintenace_Number_of_Floor_Pallet_Location;
	public static String LocationMaintenace_Allocation_Allowable;
	public static String LocationMaintenace_Free_Location_When_Zero;
	public static String LocationMaintenace_Free_Prime_Location_in_99999_Days;
	public static String LocationMaintenace_Multiple_Products_In_The_Same_Location;
	public static String LocationMaintenace_Storage_Type;
	public static String LocationMaintenace_Slotting;
	public static String LocationMaintenace_Manual_Assigned_Location;
	public static String LocationMaintenace_Used_In_Slotting;
	public static String LocationMaintenace_Select;
	public static String LocationMaintenace_Yes;
	public static String LocationMaintenace_No;
	public static String LocationMaintenace_Invalid_Location;
	public static String LocationMaintenace_Please_select_a_Area_first;
	public static String LocationMaintenace_Please_select_a_Location_Type_first;
	public static String LocationMaintenace_Invalid_Location_Description;
	public static String LocationMaintenace_Please_enter_Location;
	public static String LocationMaintenace_Please_enter_Part_Of_Loctaion_Description;
	public static String LocationMaintenace_Location_is_about_to_be_deleted;
	public static String LocationMaintenace_Location_has_Inventory_and_can_not_be_deleted;
	public static String LocationMaintenace_Location_is_still_assigned_and_can_not_be_deleted;
	public static String LocationMaintenace_Locaton_already_Used;
	public static String LocationMaintenace_Alternate_Locaton_already_Used;
	public static String LocationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only; 
	public static String LocationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only; 	
	public static String LocationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only;
	public static String LocationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only;
	public static String LocationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only;
	public static String LocationMaintenace_Wizard_Control_Number_must_be_numeric_only;
	public static String LocationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only;
	public static String LocationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only;
	public static String LocationMaintenace_Number_of_Pallets_must_be_numeric_only;
	public static String LocationMaintenace_Maindatory_field_can_not_be_left_blank;
	public static String LocationMaintenace_Invalid_Area;
	public static String LocationMaintenace_Invalid_Location_Type;
	public static String LocationMaintenace_Please_Select_Location_Type;
	
	public static String LocationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height; 
	public static String LocationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width;
	
	public  String getLocationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height() {
		return LocationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height;
	}
	public  void setLocationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height(
			String locationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height) {
		LocationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height = locationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height;
	}
	public 	 String getLocationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width() {
		return LocationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width;
	}
	public  void setLocationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width(
			String locationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width) {
		LocationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width = locationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width;
	}

	
	public  String getLocationMaintenace_Invalid_Area() {
		return LocationMaintenace_Invalid_Area;
	}
	public  void setLocationMaintenace_Invalid_Area(
			String locationMaintenace_Invalid_Area) {
		LocationMaintenace_Invalid_Area = locationMaintenace_Invalid_Area;
	}
	public  String getLocationMaintenace_Invalid_Location_Type() {
		return LocationMaintenace_Invalid_Location_Type;
	}
	public  void setLocationMaintenace_Invalid_Location_Type(
			String locationMaintenace_Invalid_Location_Type) {
		LocationMaintenace_Invalid_Location_Type = locationMaintenace_Invalid_Location_Type;
	}
	public  String getLocationMaintenace_Please_Select_Location_Type() {
		return LocationMaintenace_Please_Select_Location_Type;
	}
	public void setLocationMaintenace_Please_Select_Location_Type(
			String locationMaintenace_Please_Select_Location_Type) {
		LocationMaintenace_Please_Select_Location_Type = locationMaintenace_Please_Select_Location_Type;
	}




}