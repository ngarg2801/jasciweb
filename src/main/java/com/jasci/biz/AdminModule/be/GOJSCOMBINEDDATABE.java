/**
 
Date Developed  July 10 2014
Description pojo class of GOJS Combined data in which have getter and setter methods.
Created By Shailendra Rajput
 */
package com.jasci.biz.AdminModule.be;

import java.util.List;

public class GOJSCOMBINEDDATABE {
	
	private List<GOJSNODEDATABE> NodeDataArray;
	private List<GOJSLINKDATABE> LinkDataArray;

	public List<GOJSNODEDATABE> getNodeDataArray() {
		return NodeDataArray;
	}
	public void setNodeDataArray(List<GOJSNODEDATABE> nodeDataArray) {
		NodeDataArray = nodeDataArray;
	}
	public List<GOJSLINKDATABE> getLinkDataArray() {
		return LinkDataArray;
	}
	public void setLinkDataArray(List<GOJSLINKDATABE> linkDataArray) {
		LinkDataArray = linkDataArray;
	}

}
