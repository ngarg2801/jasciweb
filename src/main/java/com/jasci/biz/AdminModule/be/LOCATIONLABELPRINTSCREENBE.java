/**
Description It is used to make setter and getter for changing text of screen in another language
Created By :Rahul Kumar
Created Date Jan 07 2015
 */
package com.jasci.biz.AdminModule.be;


public class LOCATIONLABELPRINTSCREENBE {
	
	private String Location_Label_Print_LabelSelect;
	private String Location_Label_Print_Select;
	private String Location_Label_Print_LocationLabelSelection;
	private String Location_Label_Print_ReprintFlaggedLocations;
	private String Location_Label_Print_Area;
	private String Location_Label_Print_WizardControlNumber;
	private String Location_Label_Print_FromLocation;
	private String Location_Label_Print_AndLocation;
	private String Location_Label_Print_FromArea;
	private String Location_Label_Print_ToLocation;
	private String Location_Label_Print_LabelType;
	private String Location_Label_Print_BarcodeType3of9;
	private String Location_Label_Print_BarcodeType128;
	private String Location_Label_Print_Barcode_Type;
	private String Location_Label_Print_PrintLabels;
	private String Location_Label_Print_LookupAllwithSelection;
	private String Location_Label_Print_Or;
	private String ERR_Please_enter_Area;
	private String ERR_Please_enter_And_Location;
	private String ERR_Please_enter_Wizard_Control_Number;
	private String ERR_Please_enter_numeric_Wizard_Control_Number;
	private String ERR_Please_enter_from_Area_and_Location;
	private String ERR_Please_enter_To_Location;//ERR_Please_enter_From_Location
	private String ERR_Please_enter_From_Location;
	
	private String ERR_Please_select_Area;
	private String ERR_Please_select_Label_Type;
	private String ERR_Please_select_Barcode_Type;
	private String Location_Label_Print_Wizard_Location;
	private String KP_Please_Select_Fullfillment_Center;
	
	private String Wizard_Control_Number_not_found_for_selected_Fulfillment_Center;
	
	public String getWizard_Control_Number_not_found_for_selected_Fulfillment_Center() {
		return Wizard_Control_Number_not_found_for_selected_Fulfillment_Center;
	}
	public void setWizard_Control_Number_not_found_for_selected_Fulfillment_Center(
			String wizard_Control_Number_not_found_for_selected_Fulfillment_Center) {
		Wizard_Control_Number_not_found_for_selected_Fulfillment_Center = wizard_Control_Number_not_found_for_selected_Fulfillment_Center;
	}
	public String getKP_Please_Select_Fullfillment_Center() {
		return KP_Please_Select_Fullfillment_Center;
	}
	public void setKP_Please_Select_Fullfillment_Center(String kP_Please_Select_Fullfillment_Center) {
		KP_Please_Select_Fullfillment_Center = kP_Please_Select_Fullfillment_Center;
	}
	private String Fullfillment_center;
	

	public String getFullfillment_center() {
		return Fullfillment_center;
	}
	public void setFullfillment_center(String fullfillment_center) {
		Fullfillment_center = fullfillment_center;
	}
	private String ERR_LocationLabel_Reprint_Area;
	 


	 
	 public String getERR_LocationLabel_Reprint_Area() {
	  return ERR_LocationLabel_Reprint_Area;
	 }
	 public void setERR_LocationLabel_Reprint_Area(String eRR_LocationLabel_Reprint_Area) {
	  ERR_LocationLabel_Reprint_Area = eRR_LocationLabel_Reprint_Area;
	 }
	
	public String getLocation_Label_Print_Barcode_Type() {
		return Location_Label_Print_Barcode_Type;
	}
	public void setLocation_Label_Print_Barcode_Type(String location_Label_Print_Barcode_Type) {
		Location_Label_Print_Barcode_Type = location_Label_Print_Barcode_Type;
	}
	/**
	 * @return the location_Label_Print_Wizard_Location
	 */
	public String getLocation_Label_Print_Wizard_Location() {
		return Location_Label_Print_Wizard_Location;
	}
	/**
	 * @param location_Label_Print_Wizard_Location the location_Label_Print_Wizard_Location to set
	 */
	public void setLocation_Label_Print_Wizard_Location(String location_Label_Print_Wizard_Location) {
		Location_Label_Print_Wizard_Location = location_Label_Print_Wizard_Location;
	}
	private String Location_Label_Print_wizard_ID;
	private String Location_Label_Print_Description;
	private String Location_Label_Print_Submit; 
	private String Location_Label_Print_Cancel;
	private String ERR_Please_select_Wizard_Control_Number;
	private String ERR_Invalid_To_Location;
	private String ERR_Invalid_And_Location;
	private String ERR_Invalid_From_Location;
	private String ERR_Invalid_Wizard_Control_Number;

	
	
	
	
	/**
	 * @return the location_Label_Print_wizard_ID
	 */
	public String getLocation_Label_Print_wizard_ID() {
		return Location_Label_Print_wizard_ID;
	}
	/**
	 * @param location_Label_Print_wizard_ID the location_Label_Print_wizard_ID to set
	 */
	public void setLocation_Label_Print_wizard_ID(String location_Label_Print_wizard_ID) {
		Location_Label_Print_wizard_ID = location_Label_Print_wizard_ID;
	}
	/**
	 * @return the location_Label_Print_Description
	 */
	public String getLocation_Label_Print_Description() {
		return Location_Label_Print_Description;
	}
	/**
	 * @param location_Label_Print_Description the location_Label_Print_Description to set
	 */
	public void setLocation_Label_Print_Description(String location_Label_Print_Description) {
		Location_Label_Print_Description = location_Label_Print_Description;
	}
	/**
	 * @return the location_Label_Print_Submit
	 */
	public String getLocation_Label_Print_Submit() {
		return Location_Label_Print_Submit;
	}
	/**
	 * @param location_Label_Print_Submit the location_Label_Print_Submit to set
	 */
	public void setLocation_Label_Print_Submit(String location_Label_Print_Submit) {
		Location_Label_Print_Submit = location_Label_Print_Submit;
	}
	/**
	 * @return the location_Label_Print_Cancel
	 */
	public String getLocation_Label_Print_Cancel() {
		return Location_Label_Print_Cancel;
	}
	/**
	 * @param location_Label_Print_Cancel the location_Label_Print_Cancel to set
	 */
	public void setLocation_Label_Print_Cancel(String location_Label_Print_Cancel) {
		Location_Label_Print_Cancel = location_Label_Print_Cancel;
	}
	/**
	 * @return the eRR_Please_select_Wizard_Control_Number
	 */
	public String getERR_Please_select_Wizard_Control_Number() {
		return ERR_Please_select_Wizard_Control_Number;
	}
	/**
	 * @param eRR_Please_select_Wizard_Control_Number the eRR_Please_select_Wizard_Control_Number to set
	 */
	public void setERR_Please_select_Wizard_Control_Number(String eRR_Please_select_Wizard_Control_Number) {
		ERR_Please_select_Wizard_Control_Number = eRR_Please_select_Wizard_Control_Number;
	}
	/**
	 * @return the eRR_Invalid_To_Location
	 */
	public String getERR_Invalid_To_Location() {
		return ERR_Invalid_To_Location;
	}
	/**
	 * @param eRR_Invalid_To_Location the eRR_Invalid_To_Location to set
	 */
	public void setERR_Invalid_To_Location(String eRR_Invalid_To_Location) {
		ERR_Invalid_To_Location = eRR_Invalid_To_Location;
	}
	/**
	 * @return the eRR_Invalid_And_Location
	 */
	public String getERR_Invalid_And_Location() {
		return ERR_Invalid_And_Location;
	}
	/**
	 * @param eRR_Invalid_And_Location the eRR_Invalid_And_Location to set
	 */
	public void setERR_Invalid_And_Location(String eRR_Invalid_And_Location) {
		ERR_Invalid_And_Location = eRR_Invalid_And_Location;
	}
	/**
	 * @return the eRR_Invalid_From_Location
	 */
	public String getERR_Invalid_From_Location() {
		return ERR_Invalid_From_Location;
	}
	/**
	 * @param eRR_Invalid_From_Location the eRR_Invalid_From_Location to set
	 */
	public void setERR_Invalid_From_Location(String eRR_Invalid_From_Location) {
		ERR_Invalid_From_Location = eRR_Invalid_From_Location;
	}
	/**
	 * @return the eRR_Invalid_Wizard_Control_Number
	 */
	public String getERR_Invalid_Wizard_Control_Number() {
		return ERR_Invalid_Wizard_Control_Number;
	}
	/**
	 * @param eRR_Invalid_Wizard_Control_Number the eRR_Invalid_Wizard_Control_Number to set
	 */
	public void setERR_Invalid_Wizard_Control_Number(String eRR_Invalid_Wizard_Control_Number) {
		ERR_Invalid_Wizard_Control_Number = eRR_Invalid_Wizard_Control_Number;
	}
	/**
	 * @return the location_Label_Print_AndLocation
	 */
	public String getLocation_Label_Print_AndLocation() {
		return Location_Label_Print_AndLocation;
	}
	/**
	 * @param location_Label_Print_AndLocation the location_Label_Print_AndLocation to set
	 */
	public void setLocation_Label_Print_AndLocation(String location_Label_Print_AndLocation) {
		Location_Label_Print_AndLocation = location_Label_Print_AndLocation;
	}
	public String getERR_Please_select_Barcode_Type() {
		return ERR_Please_select_Barcode_Type;
	}
	public void setERR_Please_select_Barcode_Type(String eRR_Please_select_Barcode_Type) {
		ERR_Please_select_Barcode_Type = eRR_Please_select_Barcode_Type;
	}
	public String getERR_Please_enter_numeric_Wizard_Control_Number() {
		return ERR_Please_enter_numeric_Wizard_Control_Number;
	}
	public void setERR_Please_enter_numeric_Wizard_Control_Number(String eRR_Please_enter_numeric_Wizard_Control_Number) {
		ERR_Please_enter_numeric_Wizard_Control_Number = eRR_Please_enter_numeric_Wizard_Control_Number;
	}
	public String getERR_Please_enter_Area() {
		return ERR_Please_enter_Area;
	}
	public void setERR_Please_enter_Area(String eRR_Please_enter_Area) {
		ERR_Please_enter_Area = eRR_Please_enter_Area;
	}
	public String getERR_Please_enter_And_Location() {
		return ERR_Please_enter_And_Location;
	}
	public void setERR_Please_enter_And_Location(String eRR_Please_enter_And_Location) {
		ERR_Please_enter_And_Location = eRR_Please_enter_And_Location;
	}
	public String getERR_Please_enter_Wizard_Control_Number() {
		return ERR_Please_enter_Wizard_Control_Number;
	}
	public void setERR_Please_enter_Wizard_Control_Number(String eRR_Please_enter_Wizard_Control_Number) {
		ERR_Please_enter_Wizard_Control_Number = eRR_Please_enter_Wizard_Control_Number;
	}
	public String getERR_Please_enter_from_Area_and_Location() {
		return ERR_Please_enter_from_Area_and_Location;
	}
	public void setERR_Please_enter_from_Area_and_Location(String eRR_Please_enter_from_Area_and_Location) {
		ERR_Please_enter_from_Area_and_Location = eRR_Please_enter_from_Area_and_Location;
	}
	public String getERR_Please_enter_To_Location() {
		return ERR_Please_enter_To_Location;
	}
	public void setERR_Please_enter_To_Location(String eRR_Please_enter_To_Location) {
		ERR_Please_enter_To_Location = eRR_Please_enter_To_Location;
	}
	public String getERR_Please_enter_From_Location() {
		return ERR_Please_enter_From_Location;
	}
	public void setERR_Please_enter_From_Location(String eRR_Please_enter_From_Location) {
		ERR_Please_enter_From_Location = eRR_Please_enter_From_Location;
	}
	public String getERR_Please_select_Area() {
		return ERR_Please_select_Area;
	}
	public void setERR_Please_select_Area(String eRR_Please_select_Area) {
		ERR_Please_select_Area = eRR_Please_select_Area;
	}
	public String getERR_Please_select_Label_Type() {
		return ERR_Please_select_Label_Type;
	}
	public void setERR_Please_select_Label_Type(String eRR_Please_select_Label_Type) {
		ERR_Please_select_Label_Type = eRR_Please_select_Label_Type;
	}
	
	
	public String getLocation_Label_Print_Or() {
		return Location_Label_Print_Or;
	}
	public void setLocation_Label_Print_Or(String location_Label_Print_Or) {
		Location_Label_Print_Or = location_Label_Print_Or;
	}
	/**
	 * @return the location_Label_Print_LabelSelect
	 */
	public String getLocation_Label_Print_LabelSelect() {
		return Location_Label_Print_LabelSelect;
	}
	/**
	 * @param location_Label_Print_LabelSelect the location_Label_Print_LabelSelect to set
	 */
	public void setLocation_Label_Print_LabelSelect(String location_Label_Print_LabelSelect) {
		Location_Label_Print_LabelSelect = location_Label_Print_LabelSelect;
	}
	/**
	 * @return the location_Label_Print_Select
	 */
	public String getLocation_Label_Print_Select() {
		return Location_Label_Print_Select;
	}
	/**
	 * @param location_Label_Print_Select the location_Label_Print_Select to set
	 */
	public void setLocation_Label_Print_Select(String location_Label_Print_Select) {
		Location_Label_Print_Select = location_Label_Print_Select;
	}
	/**
	 * @return the location_Label_Print_LocationLabelSelection
	 */
	public String getLocation_Label_Print_LocationLabelSelection() {
		return Location_Label_Print_LocationLabelSelection;
	}
	/**
	 * @param location_Label_Print_LocationLabelSelection the location_Label_Print_LocationLabelSelection to set
	 */
	public void setLocation_Label_Print_LocationLabelSelection(String location_Label_Print_LocationLabelSelection) {
		Location_Label_Print_LocationLabelSelection = location_Label_Print_LocationLabelSelection;
	}
	/**
	 * @return the location_Label_Print_ReprintFlaggedLocations
	 */
	public String getLocation_Label_Print_ReprintFlaggedLocations() {
		return Location_Label_Print_ReprintFlaggedLocations;
	}
	/**
	 * @param location_Label_Print_ReprintFlaggedLocations the location_Label_Print_ReprintFlaggedLocations to set
	 */
	public void setLocation_Label_Print_ReprintFlaggedLocations(String location_Label_Print_ReprintFlaggedLocations) {
		Location_Label_Print_ReprintFlaggedLocations = location_Label_Print_ReprintFlaggedLocations;
	}
	/**
	 * @return the location_Label_Print_Area
	 */
	public String getLocation_Label_Print_Area() {
		return Location_Label_Print_Area;
	}
	/**
	 * @param location_Label_Print_Area the location_Label_Print_Area to set
	 */
	public void setLocation_Label_Print_Area(String location_Label_Print_Area) {
		Location_Label_Print_Area = location_Label_Print_Area;
	}
	/**
	 * @return the location_Label_Print_WizardControlNumber
	 */
	public String getLocation_Label_Print_WizardControlNumber() {
		return Location_Label_Print_WizardControlNumber;
	}
	/**
	 * @param location_Label_Print_WizardControlNumber the location_Label_Print_WizardControlNumber to set
	 */
	public void setLocation_Label_Print_WizardControlNumber(String location_Label_Print_WizardControlNumber) {
		Location_Label_Print_WizardControlNumber = location_Label_Print_WizardControlNumber;
	}
	/**
	 * @return the location_Label_Print_AndLocation
	 */
	public String getLocation_Label_Print_FromLocation() {
		return Location_Label_Print_FromLocation;
	}
	/**
	 * @param location_Label_Print_AndLocation the location_Label_Print_AndLocation to set
	 */
	public void setLocation_Label_Print_FromLocation(String location_Label_Print_FromLocation) {
		Location_Label_Print_FromLocation = location_Label_Print_FromLocation;
	}
	/**
	 * @return the location_Label_Print_FromAreaandLocation
	 */
	public String getLocation_Label_Print_FromArea() {
		return Location_Label_Print_FromArea;
	}
	/**
	 * @param location_Label_Print_FromAreaandLocation the location_Label_Print_FromAreaandLocation to set
	 */
	public void setLocation_Label_Print_FromArea(String location_Label_Print_FromArea) {
		Location_Label_Print_FromArea = location_Label_Print_FromArea;
	}
	/**
	 * @return the location_Label_Print_ToLocation
	 */
	public String getLocation_Label_Print_ToLocation() {
		return Location_Label_Print_ToLocation;
	}
	/**
	 * @param location_Label_Print_ToLocation the location_Label_Print_ToLocation to set
	 */
	public void setLocation_Label_Print_ToLocation(String location_Label_Print_ToLocation) {
		Location_Label_Print_ToLocation = location_Label_Print_ToLocation;
	}
	/**
	 * @return the location_Label_Print_LabelType
	 */
	public String getLocation_Label_Print_LabelType() {
		return Location_Label_Print_LabelType;
	}
	/**
	 * @param location_Label_Print_LabelType the location_Label_Print_LabelType to set
	 */
	public void setLocation_Label_Print_LabelType(String location_Label_Print_LabelType) {
		Location_Label_Print_LabelType = location_Label_Print_LabelType;
	}
	/**
	 * @return the location_Label_Print_BarcodeType3of9
	 */
	public String getLocation_Label_Print_BarcodeType3of9() {
		return Location_Label_Print_BarcodeType3of9;
	}
	/**
	 * @param location_Label_Print_BarcodeType3of9 the location_Label_Print_BarcodeType3of9 to set
	 */
	public void setLocation_Label_Print_BarcodeType3of9(String location_Label_Print_BarcodeType3of9) {
		Location_Label_Print_BarcodeType3of9 = location_Label_Print_BarcodeType3of9;
	}
	/**
	 * @return the location_Label_Print_OrBarcodeType128
	 */
	public String getLocation_Label_Print_BarcodeType128() {
		return Location_Label_Print_BarcodeType128;
	}
	/**
	 * @param location_Label_Print_OrBarcodeType128 the location_Label_Print_OrBarcodeType128 to set
	 */
	public void setLocation_Label_Print_BarcodeType128(String location_Label_Print_BarcodeType128) {
		Location_Label_Print_BarcodeType128 = location_Label_Print_BarcodeType128;
	}
	/**
	 * @return the location_Label_Print_PrintLabels
	 */
	public String getLocation_Label_Print_PrintLabels() {
		return Location_Label_Print_PrintLabels;
	}
	/**
	 * @param location_Label_Print_PrintLabels the location_Label_Print_PrintLabels to set
	 */
	public void setLocation_Label_Print_PrintLabels(String location_Label_Print_PrintLabels) {
		Location_Label_Print_PrintLabels = location_Label_Print_PrintLabels;
	}
	/**
	 * @return the location_Label_Print_LookupAllwithSelection
	 */
	public String getLocation_Label_Print_LookupAllwithSelection() {
		return Location_Label_Print_LookupAllwithSelection;
	}
	/**
	 * @param location_Label_Print_LookupAllwithSelection the location_Label_Print_LookupAllwithSelection to set
	 */
	public void setLocation_Label_Print_LookupAllwithSelection(String location_Label_Print_LookupAllwithSelection) {
		Location_Label_Print_LookupAllwithSelection = location_Label_Print_LookupAllwithSelection;
	}
	
	
	
	
	

}
