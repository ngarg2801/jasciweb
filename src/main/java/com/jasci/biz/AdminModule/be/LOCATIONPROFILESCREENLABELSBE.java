/**
 
Date Developed :Dec 26 2014
Created by: Diksha Gupta
Description :This class has getter setter for the variables that are to be show the screen labels from languages table. 
*/



package com.jasci.biz.AdminModule.be;

public class LOCATIONPROFILESCREENLABELSBE 
{
	private String  LocationProfiles_LocationProfile ;    	
	private String  LocationProfiles_ProfileGroup ;    	
	private String  LocationProfiles_PartOfLocation ;    	
	private String  LocationProfiles_NewBtn ;    	
	private String  LocationProfiles_DisplayAllBtn ;    	
	private String  LocationProfiles_SelectDropDown ;    	
	private String  LocationProfiles_LookupLabel ;    	
	private String  LocationProfiles_BlankLocationProfile ;    	
	private String  LocationProfiles_BlankProfileGroup ;    	
	private String  LocationProfiles_BlankPartOfLocation ;    	
	private String  LocationProfiles_InvalidLocationProfile ;    	
	private String  LocationProfiles_InvalidProfileGroup ;    	
	private String  LocationProfiles_InvalidPart ;    	
	private String  LocationProfiles_CopyBtn ;    	
	private String  LocationProfiles_EditBtn ;    	
	private String  LocationProfiles_DeleteBtn ;    	
	private String  LocationProfiles_AddnewBtn ;    	
	private String  LocationProfiles_ReassignLocationBtn ;    	
	private String  LocationProfiles_NotesBtn ;    	
	private String  LocationProfiles_SearchLookupLabel ;    	
	private String  LocationProfiles_FulfillmentcenterLabel ;    	
	private String  LocationProfiles_Description ;    	
	private String  LocationProfiles_NumberOfLocations ;    	
	private String  LocationProfiles_LastUsedDate ;    	
	private String  LocationProfiles_LastUsedBy ;    	
	private String  LocationProfiles_LastChangeDate ;    	
	private String  LocationProfiles_LastChangeBy ;    	
	private String  LocationProfiles_Actions ;    	
	private String  LocationProfiles_NumberZeroReassignErrorMessage ;    	
	private String  LocationProfiles_DeleteWarningMessage ;    	
	private String  LocationProfiles_DeleteErrorMessageNumberNotZero ;    	
	private String  LocationProfiles_MaintenanceLabel ;    	
	private String  LocationProfiles_LastActivityBy ;   
	private String  LocationProfiles_LastActivityDate ; 
	private String  LocationProfiles_LastAssignedDate ;    	
	private String  LocationProfiles_LastAssignedTeammember ;    	
	private String  LocationProfiles_DescriptionShort ;    	
	private String  LocationProfiles_DescriptionLong ;    	
	private String  LocationProfiles_LocationType ;    	
	private String  LocationProfiles_LocationHeight ;    	
	private String  LocationProfiles_LocationWidth ;    	
	private String  LocationProfiles_LocationDepth ;    	
	private String  LocationProfiles_LocationWeightCapacity ;    	
	private String  LocationProfiles_LocationHeightCapacity ;    	
	private String  LocationProfiles_NumberOfPallets ;    	
	private String  LocationProfiles_NumberOfFloorPallet ;    	
	private String  LocationProfiles_AllocationAllowable ;    	
	private String  LocationProfiles_FreeLocationWhenZero ;    	
	private String  LocationProfiles_FreePrimeDays ;    	
	private String  LocationProfiles_MultipleProducts ;    	
	private String  LocationProfiles_StorageTypes ;    	
	private String  LocationProfiles_Slotting ;    	
	private String  LocationProfiles_LocationCheck ;    	
	private String  LocationProfiles_CCActivityPoints ;    	
	private String  LocationProfiles_CCActivityHighAmount ;    	
	private String  LocationProfiles_CCActivityHighFactor ;    	
	private String  LocationProfiles_SaveUpdate ;    	
	private String  LocationProfiles_Cancel ;    	
	private String  LocationProfiles_MandatoryFieldErrorMessage ;    	
	private String  LocationProfiles_AlreadyExistsErrorMessage ;    	
	private String  LocationProfiles_LocationHeightErrorMessage ;
	private String  LocationProfiles_LocationWidthErrorMessage;
	private String  LocationProfiles_LocationDepthErrorMessage;
	private String  LocationProfiles_LocationWeightCapacityErrorMessage;
	private String  LocationProfiles_LocationHeightCapacityErrorMessage;
	private String  LocationProfiles_NumberOfPalletErrorMessage;
	private String  LocationProfiles_NumberOfFloorPalletErrorMessage;
	private String  LocationProfiles_FreePrimeLocationErrorMessage;
	private String  LocationProfiles_CCActivityPointsErrorMessage;
	private String  LocationProfiles_CCAmountErrorMessage;
	private String  LocationProfiles_CCFactorErrorMessage;
	private String  LocationProfiles_SavedMessage;
	private String  LocationProfiles_UpdatedMessage;
    private String  LocationProfiles_ReassignBeforeDeletion;
	private String  LocationProfiles_SelectToProfile;
    private String  LocationProfiles_SelectFromProfile;
	private String  LocationProfiles_NoLocationToReassign;
	private String  LocationProfiles_DeleteMessage;
	private String  LocationProfiles_FromProfile;
	private String  LocationProfiles_ToProfile;
	private String  LocationProfiles_Apply;
	private String  LocationProfiles_Reset;
	
	public String getLocationProfiles_Reset() {
		return LocationProfiles_Reset;
	}
	public void setLocationProfiles_Reset(String locationProfiles_Reset) {
		LocationProfiles_Reset = locationProfiles_Reset;
	}
	
	/*private String  LocationProfiles_DeleteMessage;
	private String  LocationProfiles_DeleteMessage;
	*/

	public String LocationProfiles_Description_Short_is_already_Used;
	public String LocationProfiles_Description_Long_is_already_Used;
	private String  LocationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center;
	
	private String  LocationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth;
	
	private String  LocationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight;
	
	public String getLocationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth() {
		return LocationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth;
	}
	public void setLocationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth(
			String locationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth) {
		LocationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth = locationProfiles_LocationWeightCapacityErrorMessageLessThanOrEqualToLocationWidth;
	}
	public String getLocationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight() {
		return LocationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight;
	}
	public void setLocationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight(
			String locationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight) {
		LocationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight = locationProfiles_LocationHeightCapacityErrorMessageLessThanOrEqualToLocationHeight;
	}

	public String getLocationProfiles_Description_Long_is_already_Used() {
		return LocationProfiles_Description_Long_is_already_Used;
	}
	public void setLocationProfiles_Description_Long_is_already_Used(
			String LocationProfiles_description_Long_is_already_Used) {
		LocationProfiles_Description_Long_is_already_Used = LocationProfiles_description_Long_is_already_Used;
	}
	public String getLocationProfiles_Description_Short_is_already_Used() {
		return LocationProfiles_Description_Short_is_already_Used;
	}
	public void setLocationProfiles_Description_Short_is_already_Used(
			String LocationProfiles_description_Short_is_already_Used) {
		LocationProfiles_Description_Short_is_already_Used = LocationProfiles_description_Short_is_already_Used;
	}
	
	public String getLocationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center() {
		return LocationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center;
	}
	public void setLocationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center(
			String locationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center) {
		LocationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center = locationProfiles_There_is_no_other_location_profile_for_this_fulfillment_center;
	}
	public String getLocationProfiles_LocationProfile() {
		return LocationProfiles_LocationProfile;
	}
	public String getLocationProfiles_FromProfile() {
		return LocationProfiles_FromProfile;
	}
	public void setLocationProfiles_FromProfile(String locationProfiles_FromProfile) {
		LocationProfiles_FromProfile = locationProfiles_FromProfile;
	}
	public String getLocationProfiles_ToProfile() {
		return LocationProfiles_ToProfile;
	}
	public void setLocationProfiles_ToProfile(String locationProfiles_ToProfile) {
		LocationProfiles_ToProfile = locationProfiles_ToProfile;
	}
	public String getLocationProfiles_Apply() {
		return LocationProfiles_Apply;
	}
	public void setLocationProfiles_Apply(String locationProfiles_Apply) {
		LocationProfiles_Apply = locationProfiles_Apply;
	}
	public void setLocationProfiles_LocationProfile(
			String locationProfiles_LocationProfile) {
		LocationProfiles_LocationProfile = locationProfiles_LocationProfile;
	}
	public String getLocationProfiles_ProfileGroup() {
		return LocationProfiles_ProfileGroup;
	}
	public void setLocationProfiles_ProfileGroup(
			String locationProfiles_ProfileGroup) {
		LocationProfiles_ProfileGroup = locationProfiles_ProfileGroup;
	}
	public String getLocationProfiles_PartOfLocation() {
		return LocationProfiles_PartOfLocation;
	}
	public void setLocationProfiles_PartOfLocation(
			String locationProfiles_PartOfLocation) {
		LocationProfiles_PartOfLocation = locationProfiles_PartOfLocation;
	}
	public String getLocationProfiles_NewBtn() {
		return LocationProfiles_NewBtn;
	}
	public void setLocationProfiles_NewBtn(String locationProfiles_NewBtn) {
		LocationProfiles_NewBtn = locationProfiles_NewBtn;
	}
	
	public String getLocationProfiles_SelectDropDown() {
		return LocationProfiles_SelectDropDown;
	}
	public void setLocationProfiles_SelectDropDown(
			String locationProfiles_SelectDropDown) {
		LocationProfiles_SelectDropDown = locationProfiles_SelectDropDown;
	}
	public String getLocationProfiles_LookupLabel() {
		return LocationProfiles_LookupLabel;
	}
	public void setLocationProfiles_LookupLabel(String locationProfiles_LookupLabel) {
		LocationProfiles_LookupLabel = locationProfiles_LookupLabel;
	}
	public String getLocationProfiles_BlankLocationProfile() {
		return LocationProfiles_BlankLocationProfile;
	}
	public void setLocationProfiles_BlankLocationProfile(
			String locationProfiles_BlankLocationProfile) {
		LocationProfiles_BlankLocationProfile = locationProfiles_BlankLocationProfile;
	}
	public String getLocationProfiles_BlankProfileGroup() {
		return LocationProfiles_BlankProfileGroup;
	}
	public void setLocationProfiles_BlankProfileGroup(
			String locationProfiles_BlankProfileGroup) {
		LocationProfiles_BlankProfileGroup = locationProfiles_BlankProfileGroup;
	}
	public String getLocationProfiles_BlankPartOfLocation() {
		return LocationProfiles_BlankPartOfLocation;
	}
	public void setLocationProfiles_BlankPartOfLocation(
			String locationProfiles_BlankPartOfLocation) {
		LocationProfiles_BlankPartOfLocation = locationProfiles_BlankPartOfLocation;
	}
	public String getLocationProfiles_InvalidLocationProfile() {
		return LocationProfiles_InvalidLocationProfile;
	}
	public void setLocationProfiles_InvalidLocationProfile(
			String locationProfiles_InvalidLocationProfile) {
		LocationProfiles_InvalidLocationProfile = locationProfiles_InvalidLocationProfile;
	}
	
	public String getLocationProfiles_InvalidPart() {
		return LocationProfiles_InvalidPart;
	}
	public void setLocationProfiles_InvalidPart(String locationProfiles_InvalidPart) {
		LocationProfiles_InvalidPart = locationProfiles_InvalidPart;
	}
	public String getLocationProfiles_CopyBtn() {
		return LocationProfiles_CopyBtn;
	}
	public void setLocationProfiles_CopyBtn(String locationProfiles_CopyBtn) {
		LocationProfiles_CopyBtn = locationProfiles_CopyBtn;
	}
	public String getLocationProfiles_EditBtn() {
		return LocationProfiles_EditBtn;
	}
	public void setLocationProfiles_EditBtn(String locationProfiles_EditBtn) {
		LocationProfiles_EditBtn = locationProfiles_EditBtn;
	}
	public String getLocationProfiles_DeleteBtn() {
		return LocationProfiles_DeleteBtn;
	}
	public void setLocationProfiles_DeleteBtn(String locationProfiles_DeleteBtn) {
		LocationProfiles_DeleteBtn = locationProfiles_DeleteBtn;
	}
	public String getLocationProfiles_AddnewBtn() {
		return LocationProfiles_AddnewBtn;
	}
	public void setLocationProfiles_AddnewBtn(String locationProfiles_AddnewBtn) {
		LocationProfiles_AddnewBtn = locationProfiles_AddnewBtn;
	}
	public String getLocationProfiles_ReassignLocationBtn() {
		return LocationProfiles_ReassignLocationBtn;
	}
	public void setLocationProfiles_ReassignLocationBtn(
			String locationProfiles_ReassignLocationBtn) {
		LocationProfiles_ReassignLocationBtn = locationProfiles_ReassignLocationBtn;
	}
	public String getLocationProfiles_NotesBtn() {
		return LocationProfiles_NotesBtn;
	}
	public void setLocationProfiles_NotesBtn(String locationProfiles_NotesBtn) {
		LocationProfiles_NotesBtn = locationProfiles_NotesBtn;
	}
	public String getLocationProfiles_SearchLookupLabel() {
		return LocationProfiles_SearchLookupLabel;
	}
	public void setLocationProfiles_SearchLookupLabel(
			String locationProfiles_SearchLookupLabel) {
		LocationProfiles_SearchLookupLabel = locationProfiles_SearchLookupLabel;
	}
	public String getLocationProfiles_FulfillmentcenterLabel() {
		return LocationProfiles_FulfillmentcenterLabel;
	}
	public void setLocationProfiles_FulfillmentcenterLabel(
			String locationProfiles_FulfillmentcenterLabel) {
		LocationProfiles_FulfillmentcenterLabel = locationProfiles_FulfillmentcenterLabel;
	}
	public String getLocationProfiles_Description() {
		return LocationProfiles_Description;
	}
	public void setLocationProfiles_Description(String locationProfiles_Description) {
		LocationProfiles_Description = locationProfiles_Description;
	}
	public String getLocationProfiles_NumberOfLocations() {
		return LocationProfiles_NumberOfLocations;
	}
	public void setLocationProfiles_NumberOfLocations(
			String locationProfiles_NumberOfLocations) {
		LocationProfiles_NumberOfLocations = locationProfiles_NumberOfLocations;
	}
	public String getLocationProfiles_LastUsedDate() {
		return LocationProfiles_LastUsedDate;
	}
	public void setLocationProfiles_LastUsedDate(
			String locationProfiles_LastUsedDate) {
		LocationProfiles_LastUsedDate = locationProfiles_LastUsedDate;
	}
	public String getLocationProfiles_LastUsedBy() {
		return LocationProfiles_LastUsedBy;
	}
	public void setLocationProfiles_LastUsedBy(String locationProfiles_LastUsedBy) {
		LocationProfiles_LastUsedBy = locationProfiles_LastUsedBy;
	}
	public String getLocationProfiles_LastChangeDate() {
		return LocationProfiles_LastChangeDate;
	}
	public void setLocationProfiles_LastChangeDate(
			String locationProfiles_LastChangeDate) {
		LocationProfiles_LastChangeDate = locationProfiles_LastChangeDate;
	}
	public String getLocationProfiles_LastChangeBy() {
		return LocationProfiles_LastChangeBy;
	}
	public void setLocationProfiles_LastChangeBy(
			String locationProfiles_LastChangeBy) {
		LocationProfiles_LastChangeBy = locationProfiles_LastChangeBy;
	}
	public String getLocationProfiles_Actions() {
		return LocationProfiles_Actions;
	}
	public void setLocationProfiles_Actions(String locationProfiles_Actions) {
		LocationProfiles_Actions = locationProfiles_Actions;
	}
	public String getLocationProfiles_NumberZeroReassignErrorMessage() {
		return LocationProfiles_NumberZeroReassignErrorMessage;
	}
	public void setLocationProfiles_NumberZeroReassignErrorMessage(
			String locationProfiles_NumberZeroReassignErrorMessage) {
		LocationProfiles_NumberZeroReassignErrorMessage = locationProfiles_NumberZeroReassignErrorMessage;
	}
	public String getLocationProfiles_DeleteWarningMessage() {
		return LocationProfiles_DeleteWarningMessage;
	}
	public void setLocationProfiles_DeleteWarningMessage(
			String locationProfiles_DeleteWarningMessage) {
		LocationProfiles_DeleteWarningMessage = locationProfiles_DeleteWarningMessage;
	}
	public String getLocationProfiles_DeleteErrorMessageNumberNotZero() {
		return LocationProfiles_DeleteErrorMessageNumberNotZero;
	}
	public void setLocationProfiles_DeleteErrorMessageNumberNotZero(
			String locationProfiles_DeleteErrorMessageNumberNotZero) {
		LocationProfiles_DeleteErrorMessageNumberNotZero = locationProfiles_DeleteErrorMessageNumberNotZero;
	}
	public String getLocationProfiles_MaintenanceLabel() {
		return LocationProfiles_MaintenanceLabel;
	}
	public void setLocationProfiles_MaintenanceLabel(
			String locationProfiles_MaintenanceLabel) {
		LocationProfiles_MaintenanceLabel = locationProfiles_MaintenanceLabel;
	}
	public String getLocationProfiles_LastActivityBy() {
		return LocationProfiles_LastActivityBy;
	}
	public void setLocationProfiles_LastActivityBy(
			String locationProfiles_LastActivityBy) {
		LocationProfiles_LastActivityBy = locationProfiles_LastActivityBy;
	}
	public String getLocationProfiles_LastActivityDate() {
		return LocationProfiles_LastActivityDate;
	}
	public void setLocationProfiles_LastActivityDate(
			String locationProfiles_LastActivityDate) {
		LocationProfiles_LastActivityDate = locationProfiles_LastActivityDate;
	}
	public String getLocationProfiles_DescriptionShort() {
		return LocationProfiles_DescriptionShort;
	}
	public void setLocationProfiles_DescriptionShort(
			String locationProfiles_DescriptionShort) {
		LocationProfiles_DescriptionShort = locationProfiles_DescriptionShort;
	}
	public String getLocationProfiles_DescriptionLong() {
		return LocationProfiles_DescriptionLong;
	}
	public void setLocationProfiles_DescriptionLong(
			String locationProfiles_DescriptionLong) {
		LocationProfiles_DescriptionLong = locationProfiles_DescriptionLong;
	}
	public String getLocationProfiles_LocationType() {
		return LocationProfiles_LocationType;
	}
	public void setLocationProfiles_LocationType(
			String locationProfiles_LocationType) {
		LocationProfiles_LocationType = locationProfiles_LocationType;
	}
	public String getLocationProfiles_LocationHeight() {
		return LocationProfiles_LocationHeight;
	}
	public void setLocationProfiles_LocationHeight(
			String locationProfiles_LocationHeight) {
		LocationProfiles_LocationHeight = locationProfiles_LocationHeight;
	}
	public String getLocationProfiles_LocationWidth() {
		return LocationProfiles_LocationWidth;
	}
	public void setLocationProfiles_LocationWidth(
			String locationProfiles_LocationWidth) {
		LocationProfiles_LocationWidth = locationProfiles_LocationWidth;
	}
	public String getLocationProfiles_LocationDepth() {
		return LocationProfiles_LocationDepth;
	}
	public void setLocationProfiles_LocationDepth(
			String locationProfiles_LocationDepth) {
		LocationProfiles_LocationDepth = locationProfiles_LocationDepth;
	}
	public String getLocationProfiles_LocationWeightCapacity() {
		return LocationProfiles_LocationWeightCapacity;
	}
	public void setLocationProfiles_LocationWeightCapacity(
			String locationProfiles_LocationWeightCapacity) {
		LocationProfiles_LocationWeightCapacity = locationProfiles_LocationWeightCapacity;
	}
	public String getLocationProfiles_LocationHeightCapacity() {
		return LocationProfiles_LocationHeightCapacity;
	}
	public void setLocationProfiles_LocationHeightCapacity(
			String locationProfiles_LocationHeightCapacity) {
		LocationProfiles_LocationHeightCapacity = locationProfiles_LocationHeightCapacity;
	}
	public String getLocationProfiles_NumberOfPallets() {
		return LocationProfiles_NumberOfPallets;
	}
	public void setLocationProfiles_NumberOfPallets(
			String locationProfiles_NumberOfPallets) {
		LocationProfiles_NumberOfPallets = locationProfiles_NumberOfPallets;
	}
	public String getLocationProfiles_NumberOfFloorPallet() {
		return LocationProfiles_NumberOfFloorPallet;
	}
	public void setLocationProfiles_NumberOfFloorPallet(
			String locationProfiles_NumberOfFloorPallet) {
		LocationProfiles_NumberOfFloorPallet = locationProfiles_NumberOfFloorPallet;
	}
	public String getLocationProfiles_AllocationAllowable() {
		return LocationProfiles_AllocationAllowable;
	}
	public void setLocationProfiles_AllocationAllowable(
			String locationProfiles_AllocationAllowable) {
		LocationProfiles_AllocationAllowable = locationProfiles_AllocationAllowable;
	}
	public String getLocationProfiles_FreeLocationWhenZero() {
		return LocationProfiles_FreeLocationWhenZero;
	}
	public void setLocationProfiles_FreeLocationWhenZero(
			String locationProfiles_FreeLocationWhenZero) {
		LocationProfiles_FreeLocationWhenZero = locationProfiles_FreeLocationWhenZero;
	}
	public String getLocationProfiles_FreePrimeDays() {
		return LocationProfiles_FreePrimeDays;
	}
	public void setLocationProfiles_FreePrimeDays(
			String locationProfiles_FreePrimeDays) {
		LocationProfiles_FreePrimeDays = locationProfiles_FreePrimeDays;
	}
	public String getLocationProfiles_MultipleProducts() {
		return LocationProfiles_MultipleProducts;
	}
	public void setLocationProfiles_MultipleProducts(
			String locationProfiles_MultipleProducts) {
		LocationProfiles_MultipleProducts = locationProfiles_MultipleProducts;
	}
	public String getLocationProfiles_StorageTypes() {
		return LocationProfiles_StorageTypes;
	}
	public void setLocationProfiles_StorageTypes(
			String locationProfiles_StorageTypes) {
		LocationProfiles_StorageTypes = locationProfiles_StorageTypes;
	}
	public String getLocationProfiles_Slotting() {
		return LocationProfiles_Slotting;
	}
	public void setLocationProfiles_Slotting(String locationProfiles_Slotting) {
		LocationProfiles_Slotting = locationProfiles_Slotting;
	}
	public String getLocationProfiles_LocationCheck() {
		return LocationProfiles_LocationCheck;
	}
	public void setLocationProfiles_LocationCheck(
			String locationProfiles_LocationCheck) {
		LocationProfiles_LocationCheck = locationProfiles_LocationCheck;
	}
	public String getLocationProfiles_CCActivityPoints() {
		return LocationProfiles_CCActivityPoints;
	}
	public void setLocationProfiles_CCActivityPoints(
			String locationProfiles_CCActivityPoints) {
		LocationProfiles_CCActivityPoints = locationProfiles_CCActivityPoints;
	}
	public String getLocationProfiles_CCActivityHighAmount() {
		return LocationProfiles_CCActivityHighAmount;
	}
	public void setLocationProfiles_CCActivityHighAmount(
			String locationProfiles_CCActivityHighAmount) {
		LocationProfiles_CCActivityHighAmount = locationProfiles_CCActivityHighAmount;
	}
	public String getLocationProfiles_CCActivityHighFactor() {
		return LocationProfiles_CCActivityHighFactor;
	}
	public void setLocationProfiles_CCActivityHighFactor(
			String locationProfiles_CCActivityHighFactor) {
		LocationProfiles_CCActivityHighFactor = locationProfiles_CCActivityHighFactor;
	}
	public String getLocationProfiles_SaveUpdate() {
		return LocationProfiles_SaveUpdate;
	}
	public void setLocationProfiles_SaveUpdate(String locationProfiles_SaveUpdate) {
		LocationProfiles_SaveUpdate = locationProfiles_SaveUpdate;
	}
	public String getLocationProfiles_Cancel() {
		return LocationProfiles_Cancel;
	}
	public void setLocationProfiles_Cancel(String locationProfiles_Cancel) {
		LocationProfiles_Cancel = locationProfiles_Cancel;
	}
	public String getLocationProfiles_MandatoryFieldErrorMessage() {
		return LocationProfiles_MandatoryFieldErrorMessage;
	}
	public void setLocationProfiles_MandatoryFieldErrorMessage(
			String locationProfiles_MandatoryFieldErrorMessage) {
		LocationProfiles_MandatoryFieldErrorMessage = locationProfiles_MandatoryFieldErrorMessage;
	}
	public String getLocationProfiles_AlreadyExistsErrorMessage() {
		return LocationProfiles_AlreadyExistsErrorMessage;
	}
	public void setLocationProfiles_AlreadyExistsErrorMessage(
			String locationProfiles_AlreadyExistsErrorMessage) {
		LocationProfiles_AlreadyExistsErrorMessage = locationProfiles_AlreadyExistsErrorMessage;
	}
	public String getLocationProfiles_DisplayAllBtn() {
		return LocationProfiles_DisplayAllBtn;
	}
	public void setLocationProfiles_DisplayAllBtn(
			String locationProfiles_DisplayAllBtn) {
		LocationProfiles_DisplayAllBtn = locationProfiles_DisplayAllBtn;
	}
	public String getLocationProfiles_InvalidProfileGroup() {
		return LocationProfiles_InvalidProfileGroup;
	}
	public void setLocationProfiles_InvalidProfileGroup(
			String locationProfiles_InvalidProfileGroup) {
		LocationProfiles_InvalidProfileGroup = locationProfiles_InvalidProfileGroup;
	}
	public String getLocationProfiles_LocationHeightErrorMessage() {
		return LocationProfiles_LocationHeightErrorMessage;
	}
	public void setLocationProfiles_LocationHeightErrorMessage(
			String locationProfiles_LocationHeightErrorMessage) {
		LocationProfiles_LocationHeightErrorMessage = locationProfiles_LocationHeightErrorMessage;
	}
	public String getLocationProfiles_LocationWidthErrorMessage() {
		return LocationProfiles_LocationWidthErrorMessage;
	}
	public void setLocationProfiles_LocationWidthErrorMessage(
			String locationProfiles_LocationWidthErrorMessage) {
		LocationProfiles_LocationWidthErrorMessage = locationProfiles_LocationWidthErrorMessage;
	}
	public String getLocationProfiles_LocationDepthErrorMessage() {
		return LocationProfiles_LocationDepthErrorMessage;
	}
	public void setLocationProfiles_LocationDepthErrorMessage(
			String locationProfiles_LocationDepthErrorMessage) {
		LocationProfiles_LocationDepthErrorMessage = locationProfiles_LocationDepthErrorMessage;
	}
	public String getLocationProfiles_LocationWeightCapacityErrorMessage() {
		return LocationProfiles_LocationWeightCapacityErrorMessage;
	}
	public void setLocationProfiles_LocationWeightCapacityErrorMessage(
			String locationProfiles_LocationWeightCapacityErrorMessage) {
		LocationProfiles_LocationWeightCapacityErrorMessage = locationProfiles_LocationWeightCapacityErrorMessage;
	}
	public String getLocationProfiles_LocationHeightCapacityErrorMessage() {
		return LocationProfiles_LocationHeightCapacityErrorMessage;
	}
	public void setLocationProfiles_LocationHeightCapacityErrorMessage(
			String locationProfiles_LocationHeightCapacityErrorMessage) {
		LocationProfiles_LocationHeightCapacityErrorMessage = locationProfiles_LocationHeightCapacityErrorMessage;
	}
	public String getLocationProfiles_NumberOfPalletErrorMessage() {
		return LocationProfiles_NumberOfPalletErrorMessage;
	}
	public void setLocationProfiles_NumberOfPalletErrorMessage(
			String locationProfiles_NumberOfPalletErrorMessage) {
		LocationProfiles_NumberOfPalletErrorMessage = locationProfiles_NumberOfPalletErrorMessage;
	}
	public String getLocationProfiles_NumberOfFloorPalletErrorMessage() {
		return LocationProfiles_NumberOfFloorPalletErrorMessage;
	}
	public void setLocationProfiles_NumberOfFloorPalletErrorMessage(
			String locationProfiles_NumberOfFloorPalletErrorMessage) {
		LocationProfiles_NumberOfFloorPalletErrorMessage = locationProfiles_NumberOfFloorPalletErrorMessage;
	}
	public String getLocationProfiles_FreePrimeLocationErrorMessage() {
		return LocationProfiles_FreePrimeLocationErrorMessage;
	}
	public void setLocationProfiles_FreePrimeLocationErrorMessage(
			String locationProfiles_FreePrimeLocationErrorMessage) {
		LocationProfiles_FreePrimeLocationErrorMessage = locationProfiles_FreePrimeLocationErrorMessage;
	}
	public String getLocationProfiles_CCActivityPointsErrorMessage() {
		return LocationProfiles_CCActivityPointsErrorMessage;
	}
	public void setLocationProfiles_CCActivityPointsErrorMessage(
			String locationProfiles_CCActivityPointsErrorMessage) {
		LocationProfiles_CCActivityPointsErrorMessage = locationProfiles_CCActivityPointsErrorMessage;
	}
	public String getLocationProfiles_CCAmountErrorMessage() {
		return LocationProfiles_CCAmountErrorMessage;
	}
	public void setLocationProfiles_CCAmountErrorMessage(
			String locationProfiles_CCAmountErrorMessage) {
		LocationProfiles_CCAmountErrorMessage = locationProfiles_CCAmountErrorMessage;
	}
	public String getLocationProfiles_CCFactorErrorMessage() {
		return LocationProfiles_CCFactorErrorMessage;
	}
	public void setLocationProfiles_CCFactorErrorMessage(
			String locationProfiles_CCFactorErrorMessage) {
		LocationProfiles_CCFactorErrorMessage = locationProfiles_CCFactorErrorMessage;
	}
	public String getLocationProfiles_SavedMessage() {
		return LocationProfiles_SavedMessage;
	}
	public void setLocationProfiles_SavedMessage(
			String locationProfiles_SavedMessage) {
		LocationProfiles_SavedMessage = locationProfiles_SavedMessage;
	}
	public String getLocationProfiles_UpdatedMessage() {
		return LocationProfiles_UpdatedMessage;
	}
	public void setLocationProfiles_UpdatedMessage(
			String locationProfiles_UpdatedMessage) {
		LocationProfiles_UpdatedMessage = locationProfiles_UpdatedMessage;
	}
	public String getLocationProfiles_ReassignBeforeDeletion() {
		return LocationProfiles_ReassignBeforeDeletion;
	}
	public void setLocationProfiles_ReassignBeforeDeletion(
			String locationProfiles_ReassignBeforeDeletion) {
		LocationProfiles_ReassignBeforeDeletion = locationProfiles_ReassignBeforeDeletion;
	}
	public String getLocationProfiles_SelectToProfile() {
		return LocationProfiles_SelectToProfile;
	}
	public void setLocationProfiles_SelectToProfile(
			String locationProfiles_SelectToProfile) {
		LocationProfiles_SelectToProfile = locationProfiles_SelectToProfile;
	}
	public String getLocationProfiles_SelectFromProfile() {
		return LocationProfiles_SelectFromProfile;
	}
	public void setLocationProfiles_SelectFromProfile(
			String locationProfiles_SelectFromProfile) {
		LocationProfiles_SelectFromProfile = locationProfiles_SelectFromProfile;
	}
	public String getLocationProfiles_NoLocationToReassign() {
		return LocationProfiles_NoLocationToReassign;
	}
	public void setLocationProfiles_NoLocationToReassign(
			String locationProfiles_NoLocationToReassign) {
		LocationProfiles_NoLocationToReassign = locationProfiles_NoLocationToReassign;
	}
	public String getLocationProfiles_DeleteMessage() {
		return LocationProfiles_DeleteMessage;
	}
	public void setLocationProfiles_DeleteMessage(
			String locationProfiles_DeleteMessage) {
		LocationProfiles_DeleteMessage = locationProfiles_DeleteMessage;
	}
	public String getLocationProfiles_LastAssignedDate() {
		return LocationProfiles_LastAssignedDate;
	}
	public void setLocationProfiles_LastAssignedDate(
			String locationProfiles_LastAssignedDate) {
		LocationProfiles_LastAssignedDate = locationProfiles_LastAssignedDate;
	}
	public String getLocationProfiles_LastAssignedTeammember() {
		return LocationProfiles_LastAssignedTeammember;
	}
	public void setLocationProfiles_LastAssignedTeammember(
			String locationProfiles_LastAssignedTeammember) {
		LocationProfiles_LastAssignedTeammember = locationProfiles_LastAssignedTeammember;
	}
	  	
            
            
            

}
