/*
Description It is used to make setter and getter for changing text of screen in another language
Created By Sarvendra Tyagi
Created Date Nov 26 2014
 */


package com.jasci.biz.AdminModule.be;

public class MENUEXECUTIONLISTBE {
	
	
	private String menuProfile;
	private String menuAppIcon;
	private String menuAppIconAddress;
	private String toolTipValue;
	
	
	public String getToolTipValue() {
		return toolTipValue;
	}
	public void setToolTipValue(String toolTipValue) {
		this.toolTipValue = toolTipValue;
	}
	public String getMenuAppIconAddress() {
		return menuAppIconAddress;
	}
	public void setMenuAppIconAddress(String menuAppIconAddress) {
		this.menuAppIconAddress = menuAppIconAddress;
	}
	public String getMenuProfile() {
		return menuProfile;
	}
	public void setMenuProfile(String menuProfile) {
		this.menuProfile = menuProfile;
	}
	public String getMenuAppIcon() {
		return menuAppIcon;
	}
	public void setMenuAppIcon(String menuAppIcon) {
		this.menuAppIcon = menuAppIcon;
	}

}
