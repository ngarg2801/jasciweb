/**
 *   
      
     
 *  @File purpose:Used for  Product In Location BE Class   
	@CreatedBy: Shailendra Rajput
	@Created On:1 Oct 2015
 *
 */
package com.jasci.biz.AdminModule.be;

public class PRODUCTINLOCATIONBE {

	private String Tenant;
	private String FulfillmentCenter;
	private String Company;
	private String Area;
	private String Location;
	private String Product;
	private String Quality;
	private String Description;
	private String Quantity;
	private String PRODUCT_IMAGE_HIGH;
	
	private String Lot;
	private String Serial;
	private String LPN;
	private String InventrolControl01;
	private String InventrolControl02;
	private String InventrolControl03;
	private String InventrolControl04;
	private String InventrolControl05;
	private String InventrolControl06;
	private String InventrolControl07;
	private String InventrolControl08;
	private String InventrolControl09;
	private String InventrolControl10;
	private String InventoryValue;
	private String Daterecieved;
	private String DateControl;
	private String LastActivityDate;
	private String LastActivityByName;
	private String LastActivityTask;
	private String Description20;
	
	
	
	
	public String getDescription20() {
		return Description20;
	}
	public void setDescription20(String description20) {
		Description20 = description20;
	}
	public String getLot() {
		return Lot;
	}
	public void setLot(String lot) {
		Lot = lot;
	}
	public String getSerial() {
		return Serial;
	}
	public void setSerial(String serial) {
		Serial = serial;
	}
	public String getLPN() {
		return LPN;
	}
	public void setLPN(String lPN) {
		LPN = lPN;
	}
	public String getInventrolControl01() {
		return InventrolControl01;
	}
	public void setInventrolControl01(String inventrolControl01) {
		InventrolControl01 = inventrolControl01;
	}
	public String getInventrolControl02() {
		return InventrolControl02;
	}
	public void setInventrolControl02(String inventrolControl02) {
		InventrolControl02 = inventrolControl02;
	}
	public String getInventrolControl03() {
		return InventrolControl03;
	}
	public void setInventrolControl03(String inventrolControl03) {
		InventrolControl03 = inventrolControl03;
	}
	public String getInventrolControl04() {
		return InventrolControl04;
	}
	public void setInventrolControl04(String inventrolControl04) {
		InventrolControl04 = inventrolControl04;
	}
	public String getInventrolControl05() {
		return InventrolControl05;
	}
	public void setInventrolControl05(String inventrolControl05) {
		InventrolControl05 = inventrolControl05;
	}
	public String getInventrolControl06() {
		return InventrolControl06;
	}
	public void setInventrolControl06(String inventrolControl06) {
		InventrolControl06 = inventrolControl06;
	}
	public String getInventrolControl07() {
		return InventrolControl07;
	}
	public void setInventrolControl07(String inventrolControl07) {
		InventrolControl07 = inventrolControl07;
	}
	public String getInventrolControl08() {
		return InventrolControl08;
	}
	public void setInventrolControl08(String inventrolControl08) {
		InventrolControl08 = inventrolControl08;
	}
	public String getInventrolControl09() {
		return InventrolControl09;
	}
	public void setInventrolControl09(String inventrolControl09) {
		InventrolControl09 = inventrolControl09;
	}
	public String getInventrolControl10() {
		return InventrolControl10;
	}
	public void setInventrolControl10(String inventrolControl10) {
		InventrolControl10 = inventrolControl10;
	}
	public String getInventoryValue() {
		return InventoryValue;
	}
	public void setInventoryValue(String inventoryValue) {
		InventoryValue = inventoryValue;
	}
	public String getDaterecieved() {
		return Daterecieved;
	}
	public void setDaterecieved(String daterecieved) {
		Daterecieved = daterecieved;
	}
	public String getDateControl() {
		return DateControl;
	}
	public void setDateControl(String dateControl) {
		DateControl = dateControl;
	}
	public String getLastActivityDate() {
		return LastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		LastActivityDate = lastActivityDate;
	}
	public String getLastActivityByName() {
		return LastActivityByName;
	}
	public void setLastActivityByName(String lastActivityByName) {
		LastActivityByName = lastActivityByName;
	}
	public String getLastActivityTask() {
		return LastActivityTask;
	}
	public void setLastActivityTask(String lastActivityTask) {
		LastActivityTask = lastActivityTask;
	}
	public String getPRODUCT_IMAGE_HIGH() {
		return PRODUCT_IMAGE_HIGH;
	}
	public void setPRODUCT_IMAGE_HIGH(String pRODUCT_IMAGE_HIGH) {
		PRODUCT_IMAGE_HIGH = pRODUCT_IMAGE_HIGH;
	}
	public String getTenant() {
		return Tenant;
	}
	public void setTenant(String tenant) {
		Tenant = tenant;
	}
	public String getFulfillmentCenter() {
		return FulfillmentCenter;
	}
	public void setFulfillmentCenter(String fulfillmentCenter) {
		FulfillmentCenter = fulfillmentCenter;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getArea() {
		return Area;
	}
	public void setArea(String area) {
		Area = area;
	}
	public String getLocation() {
		return Location;
	}
	public void setLocation(String location) {
		Location = location;
	}
	public String getProduct() {
		return Product;
	}
	public void setProduct(String product) {
		Product = product;
	}
	public String getQuality() {
		return Quality;
	}
	public void setQuality(String quality) {
		Quality = quality;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getQuantity() {
		return Quantity;
	}
	public void setQuantity(String quantity) {
		Quantity = quantity;
	}
	
	
}
