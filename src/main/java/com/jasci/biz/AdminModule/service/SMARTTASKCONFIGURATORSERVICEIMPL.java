/**

Description This class used to make relation with restfull services means call the restfull service functions from dao and jsp pages.
Created By Shailendra Rajput  
Created Date May 14 2015
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.SMARTTASKCONFIGURATORSERVICEAPI;
import com.jasci.biz.AdminModule.be.SMARTTASKCONFIGURATORBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class SMARTTASKCONFIGURATORSERVICEIMPL implements ISMARTTASKCONFIGURATORSERVICE{

	/** Make the instance of LANGUANGELABELSAPI for get the KP and the labels on the screen from database */
	@Autowired
	private LANGUANGELABELSAPI ObjLanguageLabelApi;
	
	@Autowired
	private SMARTTASKCONFIGURATORSERVICEAPI ObjRestServiceClient;
	/** It is used to get the value form Common Session**/
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
	/**
	 * @author Shailendra Rajput
	 * @Date May 14, 2015
	 * @Description:This is used to set screen labels and error messages of SmartTaskConfigurator's Sreen
	 * @param ObjectLocation
	 * @return SMARTTASKCONFIGURATORBE
	 * @throws JASCIEXCEPTION
	 */
	 @Transactional
	 public SMARTTASKCONFIGURATORBE getSmartTaskConfiguratorLabels(String StrLanguage) throws JASCIEXCEPTION{
		
		 	/**Create The instance List<LANGUAGES>*/
			List<LANGUAGES> ObjListLanguages=ObjLanguageLabelApi.GetScreenLabels(LANGUANGELABELSAPI.StrSmartTaskConfiguratorModule, StrLanguage);
			/**Craete the instance of SMARTTASKCONFIGURATORBE*/
			SMARTTASKCONFIGURATORBE ObjSmartTaskConfiguratorBe=new SMARTTASKCONFIGURATORBE();  
			String StrObjectFieldCode=null;
		
			/**Iterate List<LANGUAGES> to LANGUAGES */
				for (LANGUAGES ObjLanguages : ObjListLanguages) 
				{
					try{
					StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
					//String TemaMenberName=ObjRestServiceClient.getTeamMemberName(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member());
					//ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_TeamMemberName(TemaMenberName);
					}catch(Exception ObjectException){
						throw new JASCIEXCEPTION(ObjectException.getMessage());			
					}	
					
					if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Select(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Task))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Task(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Application))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Application(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Group))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_Group(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Type))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Device))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_Device(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Sequence))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_Sequence(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Group))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Group(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Type))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Device))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Device(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sequence))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Sequence(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Smart_Task_is_currently_assigned_to_Execution_Sequences_this_can_not_be_deleted(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Any_Part_of_the_Execution_Sequence_Description))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Any_Part_of_the_Execution_Sequence_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Tenant))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Tenant(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Menu_Name))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Menu_Name(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_New(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Display_All(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Search_All))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Search_All(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Smart_Task_Configurator_Lookup))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Smart_Task_Configurator_Lookup(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Application))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_Application(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Execution_Group))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_Execution_Group(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Task))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_Task(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Execution_Type))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_Execution_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Execution_Device))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_Execution_Device(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Execution_Sequence))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_Execution_Sequence(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_any_part_of_the_Execution_Sequence_Description))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_any_part_of_the_Execution_Sequence_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Tenant))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_Tenant(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Menu_Name))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_Menu_Name(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Application))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Application(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Execution_Group))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Execution_Group(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Task))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Task(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Execution_Type))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Execution_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Execution_Device))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Execution_Device(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Execution_Sequence))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Execution_Sequence(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_any_part_of_the_Execution_Sequence_Description))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_any_part_of_the_Execution_Sequence_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Tenant))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Tenant(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Menu_Name))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Invalid_Menu_Name(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Add_New(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Edit(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Copy))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Copy(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Notes))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Notes(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Delete(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Actions(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Are_you_sure_you_want_to_delete_this_record(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Last_Activity_Date(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Last_Activity_By(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Description_Short(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Long))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Description_Long(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Help_Line))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Help_Line(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Save_Update(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Reset(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Cancel(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_mandatory_field_can_not_be_left_blank(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Your_record_has_been_saved_successfully(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Your_record_has_been_updated_successfully(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Smart_Task_Configurator_Search_Lookup))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Smart_Task_Configurator_Search_Lookup(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Company(ObjLanguages.getTranslation());
					}
					
					/********/
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Last_Activity_Date(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Last_Activity_By(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Sequence_Name))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_Sequence_Name(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Configurator))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Configurator(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Action))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Action(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sequence_View))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Sequence_View(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Smart_Task_Configurator_Maintenance))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Smart_Task_Congigurator_Maintenance(ObjLanguages.getTranslation());
					}
					
					
					
				
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Sequence_Type))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_Sequence_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Go_To_Tag))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Go_To_Tag(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Message))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Message(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Sequence_Name_or_Menu_Name_already_exist))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_Sequence_Name_or_Menu_Name_already_exist(ObjLanguages.getTranslation());
					}
					
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_node_already_exist))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Start_node_already_exist(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_End_node_already_exist))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_End_node_already_exist(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_can_not_delete_Start_node))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_You_can_not_delete_Start_node(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_can_not_delete_End_node))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_You_can_not_delete_End_node(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_cannot_delete_selected_link))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_You_cannot_delete_selected_link(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_first_of_all_connect_every_node_via_link))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_first_of_all_connect_every_node_via_link(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_IF_EXIT))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_IF_EXIT(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_GoTo_Tag))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_GoTo_Tag(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Save(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_Text))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Display_Text(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_IF_ERROR))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_IF_ERROR(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_GENERAL_TAG_NAME))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_GENERAL_TAG_NAME(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_CUSTOM_EXECUTION_PATH))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_CUSTOM_EXECUTION_PATH(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Return_Code))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Return_Code(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_IF_RETURN_CODE))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_IF_RETURN_CODE(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_COMMENT))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_COMMENT(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_GoTo_Tag_first))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_GoTo_Tag_first(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_All_fields_are_mandatory))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_All_fields_are_mandatory(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_display_text))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_display_text(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_must_not_connect_Start_node_to_End_node_via_link))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_You_must_not_connect_Start_node_to_End_node_via_link(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_General_tag_already_exist_with_same_name_Please_rename_the_existing_tag_first_to_add_another_one(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_execution_data_filter_first_in_task_panel))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_execution_data_filter_first_in_task_panel(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_connect_every_node_via_link_before_saving_data))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_connect_every_node_via_link_before_saving_data(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_connect_every_node_via_link_before_align_data))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_connect_every_node_via_link_before_align_data(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_check_all_node_is_connect_via_sequential_linking_except_goto_linking(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_Tag_already_exist_with))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_General_Tag_already_exist_with(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_name_So_Please_provide_another_uniq_general_tag_name))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_name_So_Please_provide_another_uniq_general_tag_name(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_saveupdate_and_create_sequence_grid(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_add_minimum_1_execution_node_is_mandatory_except_start_and_end_node_before_create_sequence_grid(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_connect_every_node_via_link_before_see_sequence_grid))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_connect_every_node_via_link_before_see_sequence_grid(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_fill_all_mandatory_fields_value_in_task_panel))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_fill_all_mandatory_fields_value_in_task_panel(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Align))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Align(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Print))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Print(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Back))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Back(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reload))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Reload(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_You_are_not_allowed_to_create_loop_between_two_nodes_Please_change_your_linking(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sequence_Name))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Sequence_Name(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_TagName))
					{
						ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_TagName(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Refresh))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Refresh(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Close))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Close(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Goto_linking_must_before_saving_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Goto_linking_must_before_align_flowchat_data_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_select_execution_filters_type_device_group_in_task_panel_before_opening_execution_panel(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Goto_linking_must_before_see_sequence_view_grid_for_given_nodes_IF_ERROR_IF_EXIT_IF_RETURN_and_GOTO_TAG(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_AND))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_AND(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_other_nodes_not_connected))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_other_nodes_not_connected(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Display_text))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_Display_text(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Comment))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_Comment(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_General_Tag_Name))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_General_Tag_Name(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Custom_Execution_Path))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_Custom_Execution_Path(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Custom_Execution_Path_not_valid))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Custom_Execution_Path_not_valid(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_different_value_except_CUSTOM_EXECUTION))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_different_value_except_CUSTOM_EXECUTION(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_different_value_except_DISPLAY))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_different_value_except_DISPLAY(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_different_value_except_COMMENT))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_different_value_except_COMMENT(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_different_value_except_GENERAL_TAG))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_enter_different_value_except_GENERAL_TAG(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_do_not_left_any_node_blank))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Please_do_not_left_any_node_blank(ObjLanguages.getTranslation());
				     }
				     else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data))
				     {
				      ObjSmartTaskConfiguratorBe.setSmartTaskConfigurator_Execution_does_not_exist_in_Execution_Panel_filter_data_So_you_must_not_add_this_execution_in_flow_chart_panel_Please_remove_this_execution_before_save_flow_chart_data(ObjLanguages.getTranslation());
				     }
				}
				return ObjSmartTaskConfiguratorBe;	
	 }	
	 /**
		 * @author Shailendra Rajput
		 * @Date May 18, 2015
		 * @Description:This is used to search the list from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
		 *@param StrTenant
		 *@param StrTask
		 *@param StrApplication
		 *@param StrExecutionSequenceGroup
		 *@param StrExecutionType
		 *@param StrExecutionDevice
		 *@param StrExecutionSequenceName
		 *@param StrDescription
		 *@param StrMenuName
		 *@return List 
		 */
	
			@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
			public List getList(String StrTenant,String StrTask,String StrApplication,String StrExecutionSequenceGroup,String StrExecutionType,String StrExecutionDevice,String StrExecutionSequenceName,String StrDescription,String StrMenuName) throws JASCIEXCEPTION{
				/** Call the Restfull service method for get the list behalf of search conditions*/
				return ObjRestServiceClient.getList(StrTenant,StrTask,StrApplication,StrExecutionSequenceGroup,StrExecutionType,StrExecutionDevice,StrExecutionSequenceName,StrDescription,StrMenuName);
			}
		
			/**
			 * @author Shailendra Rajput
			 * @Date May 18, 2015
			 * @Description:This is used to show All data(DisplayAll) from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
			 *@param StrTenant
			 *@param StrCompany
			 *@return List<Object> 
			 */
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)	
		public List getDisplayAll() throws JASCIEXCEPTION{
			/** Call the Restfull service method for get the list of DisplayAll data*/
			return ObjRestServiceClient.getDisplayAll();
		}

		/**
		 * 
		 * @author Shailendra Rajput
		 * @Date May 19, 2015
		 * @Description:It is declaration of function,which is used to delete the record from SMART_TASK_SEQUENCE_HEADERS,SMART_TASK_SEQUENCE_INSTRUCTIONS, MENU_OPTIONS and MENU_PROFILE table
		 * @param StrTenant_Id
		 * @param Company_Id
		 * @param StrExecution_Sequence_Name
		 * @param StrMenu_Name
		 * @return boolean
		 * @throws JASCIEXCEPTION
		 */
		public boolean deleteEntry(String StrTenant_Id,String Company_Id,String StrExecution_Sequence_Name,String StrMenu_Name)throws JASCIEXCEPTION{
			/** call restfull service method for delete the record */
			return ObjRestServiceClient.deleteEntry(StrTenant_Id,Company_Id,StrExecution_Sequence_Name,StrMenu_Name);
		}


		/**
		 * @author Shailendra Rajput
		 * @Date Jun 30, 2015
		 * Description:This is used to Add Record in SMART_TASK_SEQ_HEADERS,SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
		 * @param ObjectSTSInstructionsBean
		 * @param ObjectSTSHeadersBean
		 * @return String
		 * @throws JASCIEXCEPTION
		 */
		/*public String addEntry(SMARTTASKSEQINSTRUCTIONSBEAN ObjectSTSInstructionsBean,SMARTTASKSEQHEADERSBEAN ObjectSTSHeadersBean) throws JASCIEXCEPTION{
			return ObjRestServiceClient.addEntry(ObjectSTSInstructionsBean, ObjectSTSHeadersBean);
		}*/
		public String addEntry(String Tenant_ID,String Company_ID,String Execution_Sequence_Name,String Application_Id,String Execution_Sequence_Group,String Description20,String Description50,String Menu_Option,String Execution_Device,String Execution_Type,String Task) throws JASCIEXCEPTION{
			return ObjRestServiceClient.addEntry(Execution_Sequence_Name,Application_Id,Execution_Sequence_Group,Description20,Description50,Menu_Option,Execution_Device,Execution_Type,Task);
		}
		
		/**
		 * @author Shailendra Rajput
		 * @Date Jun 30, 2015
		 * Description:This is used to Update Record in SMART_TASK_SEQ_HEADERS,SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
		 * @param ObjectSTSInstructionsBean
		 * @param ObjectSTSHeadersBean
		 * @return String
		 * @throws JASCIEXCEPTION
		 */
		public String updateEntry(String Tenant_ID,String Company_ID,String Execution_Sequence_Name,String Application_Id,String Execution_Sequence_Group,String Description20,String Description50,String Menu_Option,String Execution_Device,String Execution_Type,String Last_Activity_Date,String Last_Activity_Team_Member,String Task) throws JASCIEXCEPTION{
			return ObjRestServiceClient.updateEntry(Tenant_ID,Company_ID,Execution_Sequence_Name,Application_Id,Execution_Sequence_Group,Description20,Description50,Menu_Option,Execution_Device,Execution_Type,Last_Activity_Date,Last_Activity_Team_Member,Task);
		}
		
		/**
		 * @author Shailendra Rajput
		 * @Date Julu 2, 2015
		 * Description:This is  used to get the record from Smart_task_Seq_Headers table at the time of edit
		 * @param StrTenant_ID
		 * @param Company_ID
		 * @param StrExecutionSequenceName
		 * @return void
		 * @throws JASCIEXCEPTION
		 */
		public void fetchEntry(String StrTenant_ID,String StrCompany_ID,String StrExecutionSequenceName,String ActionName)  throws JASCIEXCEPTION{
			
				ObjRestServiceClient.SetFetchParameter(StrTenant_ID,StrCompany_ID,StrExecutionSequenceName,ActionName);	
			
			
		}
		

		/**
		 * @author Shailendra Rajput
		 * @Date Aug 12, 2015
		 * Description:This is  used to set the page name for using back button navigation
		 * @param StrPageName
		 * @return void
		 * @throws JASCIEXCEPTION
		 */
		public void setPageName(String StrPageName)  throws JASCIEXCEPTION{
			
				ObjRestServiceClient.setPageName(StrPageName);	
			
			
		}
}

