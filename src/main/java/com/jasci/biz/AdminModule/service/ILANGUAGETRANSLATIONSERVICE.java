/**

Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :ILANGUAGETRANSLATIONSERVICE interface.
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;
import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONBE;
import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONSCREENLABELSBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILANGUAGETRANSLATIONSERVICE 
{
	/** To get LanguageList based on Tenant, Company, GeneralCodeId */
	public List<LANGUAGETRANSLATIONBE> getLanguageList(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION ;
	
	/** To Check if the entered Value of language id=s valid or not 
	 * @throws JASCIEXCEPTION */
	public  Boolean CheckLanguage( String Language) throws JASCIEXCEPTION;
	/** To Check if The entered value of keyphrase is valid or not 
	 * @throws JASCIEXCEPTION */
	public Boolean CheckKeyPhrase(String KeyPhrase) throws JASCIEXCEPTION; 

    /** to add a record of language .
     * @throws JASCIEXCEPTION */
	public Boolean addLanguageData(LANGUAGETRANSLATIONBE objLanguageBe) throws JASCIEXCEPTION;

	/** to get language data based on entered Language
	 * @throws JASCIEXCEPTION */
	public List<LANGUAGETRANSLATIONBE> GetLanguageDataOnLanguage(String strSelectedLanguage, String strTenant, String strCompany, String strGeneralCodeId) throws JASCIEXCEPTION;
	
	/** to get language data based on entered KeyPhrase
	 * @throws JASCIEXCEPTION */

	public List<LANGUAGETRANSLATIONBE> GetLanguageDataOnKeyPhrase(String strKeyPhrase, String strTenant, String strCompany, String strGeneralCodeId) throws JASCIEXCEPTION;
	/** To delete a record from languages
	 * @throws JASCIEXCEPTION */

	public Boolean DeleteLanguage(String language,String keyPhrase) throws JASCIEXCEPTION;
	/** To get Language data
	 * @throws JASCIEXCEPTION */ 
	public LANGUAGETRANSLATIONBE getLanguageData(String strLanguageEdit,
			String strKeyPhrase, String strDescription) throws JASCIEXCEPTION;
	/** To edit the language data
	 * @throws JASCIEXCEPTION */
	public Boolean editLanguageData(LANGUAGETRANSLATIONBE objLanguageBe) throws JASCIEXCEPTION;
	/** to get the screen labels from Languages table
	 * @throws JASCIEXCEPTION */
	public LANGUAGETRANSLATIONSCREENLABELSBE GetScreenLabels(
			String team_Member_Language) throws JASCIEXCEPTION;
	

}
