/**
Description This interface use for service  for Location Wizard screen 
Created By Pradeep Kumar  
Created Date Jan 05 2015
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.LOCATIONWIZARDLABELSBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.common.utilbe.LOCATIONWIZARDBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILOCATIONWIZARDSERVICE {
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return LOCATIONWIZARDBE
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 5, 2015
		 * @Description :
	 */
	public List<LOCATIONWIZARDBE> getLocationWizardSearch(String wizardID, String searchVale,String Searchbased) throws JASCIEXCEPTION;
	
/*	*//**
	 * 
	 * @param wizardID
	 * @param wizardControlNumber
	 * 
	 * @return
	 * @throws JASCIEXCEPTION
	 *//*
	public LOCATIONWIZARDBE getLocationWizardSearchWizardControl(String wizardID, String wizardControlNumber) throws JASCIEXCEPTION;
	*/
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return LOCATIONWIZARDBE
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 7, 2015
		 * @Description :
	 */
	public LOCATIONWIZARDLABELSBE getLocationWizardLevel(String moduleName, String languageCode) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<GENERALCODESBEAN>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 7, 2015
		 * @Description :Area,workzone,workzonegroup,location type
	 */
	
	public List<GENERALCODES> getDropDownList(String GeneralCodeID) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<FULFILLMENTCENTERSPOJO>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 8, 2015
		 * @Description :fulfillment dropdown
	 */
	public List<FULFILLMENTCENTERSPOJO> getDropDownValuesFulfillmentList() throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<LOCATIONWIZARD>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 12, 2015
		 * @Description :get Grid DAta
	 */
	
	public List<LOCATIONWIZARDBE> getLocationWizardData(String wizardID) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 13, 2015
		 * @Description :save and update
	 */
	
	public List<Object> saveAndUpdate(LOCATIONWIZARDBE objLocationwizard) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<LOCATIONPROFILES>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 13, 2015
		 * @Description :get locationprofile drop down
	 */
	public List<LOCATIONPROFILES> getDropDownValueLocationProfile(String fulfilmentCenter) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return LOCATIONWIZARDBE
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 14, 2015
		 * @Description :used for copy
	 */
	public LOCATIONWIZARDBE  getLocationWizardCopyEdit(String wizardid,String wizardControlNo, String fulfillmentId,String copyOrEdit) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :void
			 * @throws ILOCATIONWIZARDSERVICE
			 * @Date : Jan 15, 2015
			 * @Description :deleteWizardOnly
	 */
	public void  deleteWizardOnly(String wizardid,String wizardControlNo, String fulfillmentId) throws JASCIEXCEPTION;

	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :void
			 * @throws ILOCATIONWIZARDSERVICE
			 * @Date : Jan 16, 2015
			 * @Description :deleteWizardAndLocation
	 */
	public void  deleteWizardAndLocation(String wizardid,String wizardControlNo, String fulfillmentId,String Data) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :String
			 * @throws ILOCATIONWIZARDSERVICE
			 * @Date : Jan 21, 2015
			 * @Description :convert date
	 */
	public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat);
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :long
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 21, 2015
			 * @Description : give max no and increase one
	 */
	
	public long getMaxWizardControlNumber() throws JASCIEXCEPTION;
	
	/**
     * 
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
    public String getTeamMemberName(String Tenant,String TeamMember)throws JASCIEXCEPTION;
    
}
