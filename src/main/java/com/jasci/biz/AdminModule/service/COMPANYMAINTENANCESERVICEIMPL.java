/**
 Date Developed  Dec 22 2014
 Created By "Rahul Kumar"
 Description  Provide service layer between  controller and Rest Full service  layer
 */


package com.jasci.biz.AdminModule.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jasci.biz.AdminModule.ServicesApi.COMPANYMAINTENANCESERVICEAPI;
import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.COMPANYSCREENBE;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.COMPANIESPK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.NEXTBILLINGCONTROLNUMBER;
import com.jasci.common.utilbe.COMPANIESBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class COMPANYMAINTENANCESERVICEIMPL implements ICOMPANYMAINTENANCESERVICE {


	@Autowired
	COMPANYMAINTENANCESERVICEAPI objCompanyserviceapi;
	@Autowired
	LANGUANGELABELSAPI objLanguangelabelsapi;
	@Autowired
	NEXTBILLINGCONTROLNUMBER objNextbillingcontrolnumber;

	/**
	 * @Description set screen label as team member language
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#setScreenLanguage(java.lang.String, java.lang.String)
	 */
	public COMPANYSCREENBE setScreenLanguage(String StrScreenName, String StrLanguage) throws JASCIEXCEPTION {
		
		//List<LANGUAGES> ObjListLanguages=objCompanyserviceapi.setScreenLanguage(StrScreenName,StrLanguage);
		List<LANGUAGES> ObjListLanguages=objLanguangelabelsapi.GetScreenLabels(LANGUANGELABELSAPI.strCompanyMaintenanceModule, StrLanguage);
		COMPANYSCREENBE objCompanyscreenbe=new COMPANYSCREENBE();
	   for (LANGUAGES ObjLanguages : ObjListLanguages) 
		{
			String StrKeyPhrase=ObjLanguages.getId().getKeyPhrase().trim();
			String StrTranslation=ObjLanguages.getTranslation().trim();



			// lookup screen
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_the_Company_Name)){
				objCompanyscreenbe.setCompanyMaintenance_PartOfTheCompanyNameLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_ID)){
				objCompanyscreenbe.setCompanyMaintenance_CompanyidLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_Lookup)){
				objCompanyscreenbe.setCompanyMaintenance_LookupLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonNewText(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonDisplayAllText(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_the_Company)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_PART_OF_COMPANY(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Company_Id)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_IF_INVALID_COMPANY(StrTranslation);

			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_the_Company_Name)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_PART_OF_COMPANY_NAME(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_company_Id)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_ID(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_part_of_the_company_name)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_PLEASE_ENTER_PART_OF_COMPANY_NAME(StrTranslation);

			}
			//Search Lookup
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonEditText(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonDeleteText(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonActionText(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonAddNewText(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_Search_Lookup)){
				objCompanyscreenbe.setCompanyMaintenance_SearchLookupLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_Name)){
				objCompanyscreenbe.setCompanyMaintenance_CompanyNameLabel(StrTranslation);

			}


			//maintenance

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Setup_Selected_Wizard)){
				objCompanyscreenbe.setCompanyMaintenance_SetupSelectedWizardLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name_Short)){
				objCompanyscreenbe.setCompanyMaintenance_Name20Label(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name_Long)){
				objCompanyscreenbe.setCompanyMaintenance_Name50Label(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_1)){
				objCompanyscreenbe.setCompanyMaintenance_Addressline1Label(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_2)){
				objCompanyscreenbe.setCompanyMaintenance_Addressline2Label(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_3)){
				objCompanyscreenbe.setCompanyMaintenance_Addressline3Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_4)){
				objCompanyscreenbe.setCompanyMaintenance_Addressline4Label(StrTranslation);

			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_City)){
				objCompanyscreenbe.setCompanyMaintenance_CityLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_State)){
				objCompanyscreenbe.setCompanyMaintenance_StatecodeLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Country_Code)){
				objCompanyscreenbe.setCompanyMaintenance_CountrycodeLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Zip_Code)){
				objCompanyscreenbe.setCompanyMaintenance_ZipcodeLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Name20)){
				objCompanyscreenbe.setCompanyMaintenance_Billtoname20Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Name50)){
				objCompanyscreenbe.setCompanyMaintenance_Billtoname50Label(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Address_Line_1)){
				objCompanyscreenbe.setCompanyMaintenance_Billtoaddressline1Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Address_Line_2)){
				objCompanyscreenbe.setCompanyMaintenance_Billtoaddressline2Label(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Address_Line_3)){
				objCompanyscreenbe.setCompanyMaintenance_Billtoaddressline3Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Address_Line_4)){
				objCompanyscreenbe.setCompanyMaintenance_Billtoaddressline4Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_City)){
				objCompanyscreenbe.setCompanyMaintenance_BilltofromcityLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_State_Code)){
				objCompanyscreenbe.setCompanyMaintenance_BilltostatecodeLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Country_Code)){
				objCompanyscreenbe.setCompanyMaintenance_BilltocountrycodeLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bill_To_Zip_Code)){
				objCompanyscreenbe.setCompanyMaintenance_BilltozipcodeLabel(StrTranslation);

			}

			//group 1
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name)){
				objCompanyscreenbe.setCompanyMaintenance_Contactname1Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone)){
				objCompanyscreenbe.setCompanyMaintenance_Contactphone1Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension)){
				objCompanyscreenbe.setCompanyMaintenance_Contactextension1Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell)){
				objCompanyscreenbe.setCompanyMaintenance_Contactcell1Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax)){
				objCompanyscreenbe.setCompanyMaintenance_Contactfax1Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email)){
				objCompanyscreenbe.setCompanyMaintenance_Contactemail1Label(StrTranslation);

			}

			//group 2
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name_2)){
				objCompanyscreenbe.setCompanyMaintenance_Contactname2Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone_2)){
				objCompanyscreenbe.setCompanyMaintenance_Contactphone2Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension_2)){
				objCompanyscreenbe.setCompanyMaintenance_Contactextension2Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell_2)){
				objCompanyscreenbe.setCompanyMaintenance_Contactcell2Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax_2)){
				objCompanyscreenbe.setCompanyMaintenance_Contactfax2Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email_2)){
				objCompanyscreenbe.setCompanyMaintenance_Contactemail2Label(StrTranslation);

			}
			//group 3
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name_3)){
				objCompanyscreenbe.setCompanyMaintenance_Contactname3Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone_3)){
				objCompanyscreenbe.setCompanyMaintenance_Contactphone3Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension_3)){
				objCompanyscreenbe.setCompanyMaintenance_Contactextension3Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell_3)){
				objCompanyscreenbe.setCompanyMaintenance_Contactcell3Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax_3)){
				objCompanyscreenbe.setCompanyMaintenance_Contactfax3Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email_3)){
				objCompanyscreenbe.setCompanyMaintenance_Contactemail3Label(StrTranslation);

			}
			//group 4
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name_4)){
				objCompanyscreenbe.setCompanyMaintenance_Contactname4Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone_4)){
				objCompanyscreenbe.setCompanyMaintenance_Contactphone4Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension_4)){
				objCompanyscreenbe.setCompanyMaintenance_Contactextension4Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell_4)){
				objCompanyscreenbe.setCompanyMaintenance_Contactcell4Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax_4)){
				objCompanyscreenbe.setCompanyMaintenance_Contactfax4Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email_4)){
				objCompanyscreenbe.setCompanyMaintenance_Contactemail4Label(StrTranslation);
			}
			//group end

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Main_Fax)){
				objCompanyscreenbe.setCompanyMaintenance_MainfaxLabel(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Main_Email)){
				objCompanyscreenbe.setCompanyMaintenance_MainemailLabel(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Billing_Control_Number)){
				objCompanyscreenbe.setCompanyMaintenance_BillingcontrolnumberLabel(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Theme_Mobile)){
				objCompanyscreenbe.setCompanyMaintenance_ThememobileLabel(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Theme_RF)){
				objCompanyscreenbe.setCompanyMaintenance_ThemerfLabel(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Theme_Full_Display)){
				objCompanyscreenbe.setCompanyMaintenance_ThemefulldisplayLabel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Logo)){
				objCompanyscreenbe.setCompanyMaintenance_LogoLabel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Purchase_Order_Require_Approval)){
				objCompanyscreenbe.setCompanyMaintenance_PurchaseorderreqapprovalLabel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Status)){
				objCompanyscreenbe.setCompanyMaintenance_StatusLabel(StrTranslation);
			}
			/*else if(StrKeyPhrase.equalsIgnoreCase(GLOBALCONSTANT.CompanyMaintenance_SetupselectedLabel)){
				objCompanyscreenbe.setCompanyMaintenance_SetupselectedLabel(StrTranslation);
			}*/
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Set_up_Date)){
				objCompanyscreenbe.setCompanyMaintenance_SetupdateLabel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Setup_By)){
				objCompanyscreenbe.setCompanyMaintenance_SetupbyLabel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Time_Zone)){
				objCompanyscreenbe.setCompanyMaintenance_TimezoneLabel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date)){
				objCompanyscreenbe.setCompanyMaintenance_LastactivitydateLabel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By)){
				objCompanyscreenbe.setCompanyMaintenance_LastactivityteammemberLabel(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company)){
				objCompanyscreenbe.setCompanyMaintenance_CompanyLabel(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonCancelText(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select)){
				objCompanyscreenbe.setCompanyMaintenance_Select(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_YES)){
				objCompanyscreenbe.setCompanyMaintenance_Yes(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_NO)){
				objCompanyscreenbe.setCompanyMaintenance_No(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Choose_File)){
				objCompanyscreenbe.setCompanyMaintenance_BrowseText(StrTranslation);
			}


			//error masg


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update)){
				objCompanyscreenbe.setCompanyMaintenance_SaveUpdateText(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_already_used)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_COMPANY_ALREADY_USED(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_bill_to_zip_code)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_BILL_TO_ZIP_CODE(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Zip_Code)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_ZIP_CODE(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_bill_to_address)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_BILL_TO_ADDRESS(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Address)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_ADDRESS(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Only_numeric__value_allowed)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_CONTACT_NUMBER(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_email_address)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_EMAIL_ADDRESS(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_WHILE_DELETING_COMPANY(StrTranslation);
			}

			
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully)){
				objCompanyscreenbe.setCompanyMaintenance_ON_SAVE_SUCCESSFULLY(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully)){
				objCompanyscreenbe.setCompanyMaintenance_ON_UPDATE_SUCCESSFULLY(StrTranslation);
			}
			
			
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Supported_formats_are_Jpeg_jpg_png_gif)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_INVALID_SUPPORTED_FORMATS(StrTranslation);
				objCompanyscreenbe.setCompanyMaintenance_SupportedFormatsLabel(StrTranslation);
				
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_Maintenance)){
				objCompanyscreenbe.setCompanyMaintenance_MaintenanceLabel(StrTranslation);
			}
			
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact)){
				objCompanyscreenbe.setCompanyMaintenance_Contact(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Loading)){
				objCompanyscreenbe.setCompanyMaintenance_Text_Loading(StrTranslation);
			}
			
				
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email_address_already_exist)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_CONTACT_EMAIL_ADDRESS_ALREADY_USED(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_already_exist_with_inactive_status)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_Company_Already_Exist_With_Inactive_Status(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank_and_must_be_numeric_only)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank_and_must_be_valid_email_address)){
				objCompanyscreenbe.setCompanyMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset)){
				objCompanyscreenbe.setCompanyMaintenance_ButtonResetText(StrTranslation);
			}
		}	

	   

		
		
		 if(objCompanyscreenbe.getCompanyMaintenance_Yes()==null){
			objCompanyscreenbe.setCompanyMaintenance_Yes(LANGUANGELABELSAPI.KP_YES);
	    	}

		 if(objCompanyscreenbe.getCompanyMaintenance_No()==null){
				objCompanyscreenbe.setCompanyMaintenance_Yes(LANGUANGELABELSAPI.KP_NO);
			}


		return objCompanyscreenbe;









	}


	/**
	 * * @Description get all companies based on tenant from companies table
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014
	 * @param Tenant
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#getCompanies(java.lang.String)
	 */
	public List<COMPANIESBE> getAllCompanies(String Tenant) throws JASCIEXCEPTION {
		
		List<COMPANIES>objCompanies= objCompanyserviceapi.getAllCompanies(Tenant);
		List<COMPANIESBE> obCompaniesbeList=new ArrayList<COMPANIESBE>();

		for(COMPANIES  valueCompanies:objCompanies ){

			COMPANIESBE objCompaniesbe=new COMPANIESBE();

			objCompaniesbe.setTenantId(valueCompanies.getId().getTenant());
			objCompaniesbe.setCompanyId(valueCompanies.getId().getCompany());
			objCompaniesbe.setName20(valueCompanies.getName20());
			objCompaniesbe.setName50(valueCompanies.getName50());
			//
			obCompaniesbeList.add(objCompaniesbe);
		}

		return obCompaniesbeList;


	}


	/**
	 * @Description get company by company part name from companies table
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014
	 * @param CompanyPartName
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#getCompanyByPartName(java.lang.String)
	 */
	public List<COMPANIESBE> getCompanyByPartName(String CompanyPartName) throws JASCIEXCEPTION {
		
		List<COMPANIESBE> obCompaniesbeList=new ArrayList<COMPANIESBE>();

		List<COMPANIES>objCompanies= objCompanyserviceapi.getCompanyByPartName(CompanyPartName);
		for(COMPANIES  valueCompanies:objCompanies ){

			COMPANIESBE objCompaniesbe=new COMPANIESBE();

			objCompaniesbe.setTenantId(valueCompanies.getId().getTenant());
			objCompaniesbe.setCompanyId(valueCompanies.getId().getCompany());
			objCompaniesbe.setName20(valueCompanies.getName20());
			objCompaniesbe.setName50(valueCompanies.getName50());
			//
			obCompaniesbeList.add(objCompaniesbe);
		}

		return obCompaniesbeList;
	}


	/**
	 * @Description get general code based on general code id from general code table
	 * @author Rahul Kumar
	 * @Date Dec 24, 2014
	 * @param Tenant
	 * @param Company
	 * @param GeneralCodeId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#getGeneralCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION {
		
		return objCompanyserviceapi.getGeneralCode(Tenant, Company, GeneralCodeId);

	}


	/**
	 * Description: get country code from config file
	 * @author Rahul Kumar
	 * @Date Dec 25, 2014
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#getCountryCode()
	 */
	public String getCountryCode() throws JASCIEXCEPTION {
		
		CONFIGURATIONEFILE objConfigurationefile = new CONFIGURATIONEFILE();
		String CountryCode = objConfigurationefile.getConfigProperty(GLOBALCONSTANT.CountryCode);
		return CountryCode;
	}


	/**
	 * 
	 * @Description add or update company into companies table 
	 * @author Rahul Kumar
	 * @Date Dec 26, 2014 
	 * @param objCompaniesbe
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#addOrUpdateCompany(com.jasci.common.utilbe.COMPANIESBE)
	 */
	public Boolean addOrUpdateCompany(COMPANIESBE objCompaniesbe) throws JASCIEXCEPTION {
		
		COMPANIES objCompanies=new COMPANIES();

		COMPANIESPK objCompaniespk=new COMPANIESPK();
		objCompaniespk.setCompany(trimValues(objCompaniesbe.getCompanyId()));
		objCompaniespk.setTenant(objCompaniesbe.getTenantId());
		objCompanies.setId(objCompaniespk);

		objCompanies.setSetUpSelected(trimValues(objCompaniesbe.getSetUpSelected()));
		
		Date StrCurrentDate=null;
		Date LastActivitydate=null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
			SimpleDateFormat formatterSetUp = new SimpleDateFormat(GLOBALCONSTANT.yyyyMMdd_Format_v1);
			 StrCurrentDate = formatterSetUp.parse(objCompaniesbe.getSetUpDate());
			 LastActivitydate = formatter.parse(objCompaniesbe.getLastActivityDate());
			
		}catch(Exception objException){
			objException.getMessage();
		}
		
		objCompanies.setSetUpDate(StrCurrentDate);
		objCompanies.setSetUpBy(trimValues(objCompaniesbe.getSetUpBy()));
		
		objCompanies.setStatus(GLOBALCONSTANT.Status_Active);
		objCompanies.setLastActivityDate(LastActivitydate);
		objCompanies.setLastActivityTeamMember(trimValues(objCompaniesbe.getLastActivityTeamMember()));


		objCompanies.setName20(trimValues(objCompaniesbe.getName20()));
		objCompanies.setName50(trimValues(objCompaniesbe.getName50()));
		objCompanies.setAddressLine1(trimValues(objCompaniesbe.getAddressLine1()));
		objCompanies.setAddressLine2(trimValues(objCompaniesbe.getAddressLine2()));
		objCompanies.setAddressLine3(trimValues(objCompaniesbe.getAddressLine3()));
		objCompanies.setAddressLine4(trimValues(objCompaniesbe.getAddressLine4()));
		objCompanies.setCountryCode(trimValues(objCompaniesbe.getCountryCode()));
		objCompanies.setCity(trimValues(objCompaniesbe.getCity()));
		objCompanies.setStateCode(trimValues(objCompaniesbe.getStateCode()));
		objCompanies.setZipCode(trimValues(objCompaniesbe.getZipCode()));



		objCompanies.setBillToName20(trimValues(objCompaniesbe.getBillToName20()));
		objCompanies.setBillToName50(trimValues(objCompaniesbe.getBillToName50()));
		objCompanies.setBillToAddressLine1(trimValues(objCompaniesbe.getBillToAddressLine1()));
		objCompanies.setBillToAddressLine2(trimValues(objCompaniesbe.getBillToAddressLine2()));
		objCompanies.setBillToAddressLine3(trimValues(objCompaniesbe.getBillToAddressLine3()));
		objCompanies.setBillToAddressLine4(trimValues(objCompaniesbe.getBillToAddressLine4()));
		objCompanies.setBillToCountryCode(trimValues(objCompaniesbe.getBillToCountryCode()));
		objCompanies.setBillToFromCity(trimValues(objCompaniesbe.getBillToFromCity()));
		objCompanies.setBillToStateCode(trimValues(objCompaniesbe.getBillToStateCode()));
		objCompanies.setBillToZipCode(trimValues(objCompaniesbe.getBillToZipCode()));



		objCompanies.setContactName1(trimValues(objCompaniesbe.getContactName1()));
		objCompanies.setContactName2(trimValues(objCompaniesbe.getContactName2()));
		objCompanies.setContactName3(trimValues(objCompaniesbe.getContactName3()));
		objCompanies.setContactName4(trimValues(objCompaniesbe.getContactName4()));

		objCompanies.setContactPhone1(trimValues(objCompaniesbe.getContactPhone1()));
		objCompanies.setContactPhone2(trimValues(objCompaniesbe.getContactPhone2()));
		objCompanies.setContactPhone3(trimValues(objCompaniesbe.getContactPhone3()));
		objCompanies.setContactPhone4(trimValues(objCompaniesbe.getContactPhone4()));

		objCompanies.setContactCell1(trimValues(objCompaniesbe.getContactCell1()));
		objCompanies.setContactCell2(trimValues(objCompaniesbe.getContactCell2()));
		objCompanies.setContactCell3(trimValues(objCompaniesbe.getContactCell3()));
		objCompanies.setContactCell4(trimValues(objCompaniesbe.getContactCell4()));

		objCompanies.setContactExtension1(trimValues(objCompaniesbe.getContactExtension1()));
		objCompanies.setContactExtension2(trimValues(objCompaniesbe.getContactExtension2()));
		objCompanies.setContactExtension3(trimValues(objCompaniesbe.getContactExtension3()));
		objCompanies.setContactExtension4(trimValues(objCompaniesbe.getContactExtension4()));

		objCompanies.setContactFax1(trimValues(objCompaniesbe.getContactFax1()));
		objCompanies.setContactFax2(trimValues(objCompaniesbe.getContactFax2()));
		objCompanies.setContactFax3(trimValues(objCompaniesbe.getContactFax3()));
		objCompanies.setContactFax4(trimValues(objCompaniesbe.getContactFax4()));

		objCompanies.setContactEmail1(trimValues(objCompaniesbe.getContactEmail1()));
		objCompanies.setContactEmail2(trimValues(objCompaniesbe.getContactEmail2()));
		objCompanies.setContactEmail3(trimValues(objCompaniesbe.getContactEmail3()));
		objCompanies.setContactEmail4(trimValues(objCompaniesbe.getContactEmail4()));


		objCompanies.setMainEmail(trimValues(objCompaniesbe.getMainEmail()));
		objCompanies.setMainFax(trimValues(objCompaniesbe.getMainFax()));
		
		objCompanies.setBillingControlNumber(objNextbillingcontrolnumber.getNextBillingControlNumber());
		
		objCompanies.setThemeFullDisplay(trimValues(objCompaniesbe.getThemeFullDisplay()));
		objCompanies.setThemeMobile(trimValues(objCompaniesbe.getThemeMobile()));
		objCompanies.setThemeRF(trimValues(objCompaniesbe.getThemeRF()));
		objCompanies.setPurchageOrdersRequireApproval(trimValues(objCompaniesbe.getPurchageOrdersRequireApproval()));
		objCompanies.setTimeZone(trimValues(objCompaniesbe.getTimeZone()));
		objCompanies.setLogo(trimValues(objCompaniesbe.getLogo()));







		return objCompanyserviceapi.addOrUpdateCompany(objCompanies);
		//return true;
	}





	/**
	 * @Description trim string values
	 * @param Value
	 * @return
	 * * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 */
	//This Function is used to Trim the white space
	public static String trimValues(String Value){
		if(Value == null){
			return null;
		}
		else{
			String CheckValue=Value.trim();
			if(CheckValue.length()<1){
				return null;
			}else{      
				return CheckValue;
			}
		}
	}



	/**
	 * 
	 * @Description get company by its id and tenant from companies table  
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#getCompanyById(java.lang.String, java.lang.String)
	 */
	public COMPANIESBE getCompanyById(String Tenant,String CompanyId) throws JASCIEXCEPTION {
		
		List<COMPANIES>objCompaniesList= objCompanyserviceapi.getCompanyById(Tenant, CompanyId);
		COMPANIES objCompanies=objCompaniesList.get(0);
		
		COMPANIESBE objCompaniesbe=new COMPANIESBE();
		
		String LastActivityDate=ConvertDate(objCompanies.getLastActivityDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
		String SetupDate=ConvertDate(objCompanies.getSetUpDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);

		objCompaniesbe.setSetUpDate(SetupDate);
		objCompaniesbe.setStatus(objCompanies.getStatus());
		objCompaniesbe.setLastActivityDate(LastActivityDate);	
			
		objCompaniesbe.setCompanyId(StringEscapeUtils.escapeHtml(objCompanies.getId().getCompany()));
		objCompaniesbe.setTenantId(StringEscapeUtils.escapeHtml(objCompanies.getId().getTenant()));
		objCompaniesbe.setSetUpSelected(StringEscapeUtils.escapeHtml(objCompanies.getSetUpSelected()));
		objCompaniesbe.setSetUpBy(StringEscapeUtils.escapeHtml(objCompanies.getSetUpBy()));	
		objCompaniesbe.setLastActivityTeamMember(StringEscapeUtils.escapeHtml(objCompanies.getLastActivityTeamMember()));

		objCompaniesbe.setName20(StringEscapeUtils.escapeHtml(objCompanies.getName20()));
		objCompaniesbe.setName50(StringEscapeUtils.escapeHtml(objCompanies.getName50()));
		objCompaniesbe.setAddressLine1(StringEscapeUtils.escapeHtml(objCompanies.getAddressLine1()));
		objCompaniesbe.setAddressLine2(StringEscapeUtils.escapeHtml(objCompanies.getAddressLine2()));
		objCompaniesbe.setAddressLine3(StringEscapeUtils.escapeHtml(objCompanies.getAddressLine3()));
		objCompaniesbe.setAddressLine4(StringEscapeUtils.escapeHtml(objCompanies.getAddressLine4()));
		objCompaniesbe.setCountryCode(StringEscapeUtils.escapeHtml(objCompanies.getCountryCode()));
		objCompaniesbe.setCity(StringEscapeUtils.escapeHtml(objCompanies.getCity()));
		objCompaniesbe.setStateCode(StringEscapeUtils.escapeHtml(objCompanies.getStateCode()));
		objCompaniesbe.setZipCode(StringEscapeUtils.escapeHtml(objCompanies.getZipCode()));

		objCompaniesbe.setBillToName20(StringEscapeUtils.escapeHtml(objCompanies.getBillToName20()));
		objCompaniesbe.setBillToName50(StringEscapeUtils.escapeHtml(objCompanies.getBillToName50()));
		objCompaniesbe.setBillToAddressLine1(StringEscapeUtils.escapeHtml(objCompanies.getBillToAddressLine1()));
		objCompaniesbe.setBillToAddressLine2(StringEscapeUtils.escapeHtml(objCompanies.getBillToAddressLine2()));
		objCompaniesbe.setBillToAddressLine3(StringEscapeUtils.escapeHtml(objCompanies.getBillToAddressLine3()));
		objCompaniesbe.setBillToAddressLine4(StringEscapeUtils.escapeHtml(objCompanies.getBillToAddressLine4()));
		objCompaniesbe.setBillToCountryCode(StringEscapeUtils.escapeHtml(objCompanies.getBillToCountryCode()));
		objCompaniesbe.setBillToFromCity(StringEscapeUtils.escapeHtml(objCompanies.getBillToFromCity()));
		objCompaniesbe.setBillToStateCode(StringEscapeUtils.escapeHtml(objCompanies.getBillToStateCode()));
		objCompaniesbe.setBillToZipCode(StringEscapeUtils.escapeHtml(objCompanies.getBillToZipCode()));

		objCompaniesbe.setContactName1(StringEscapeUtils.escapeHtml(objCompanies.getContactName1()));
		objCompaniesbe.setContactName2(StringEscapeUtils.escapeHtml(objCompanies.getContactName2()));
		objCompaniesbe.setContactName3(StringEscapeUtils.escapeHtml(objCompanies.getContactName3()));
		objCompaniesbe.setContactName4(StringEscapeUtils.escapeHtml(objCompanies.getContactName4()));

		objCompaniesbe.setContactPhone1(StringEscapeUtils.escapeHtml(objCompanies.getContactPhone1()));
		objCompaniesbe.setContactPhone2(StringEscapeUtils.escapeHtml(objCompanies.getContactPhone2()));
		objCompaniesbe.setContactPhone3(StringEscapeUtils.escapeHtml(objCompanies.getContactPhone3()));
		objCompaniesbe.setContactPhone4(StringEscapeUtils.escapeHtml(objCompanies.getContactPhone4()));

		objCompaniesbe.setContactCell1(StringEscapeUtils.escapeHtml(objCompanies.getContactCell1()));
		objCompaniesbe.setContactCell2(StringEscapeUtils.escapeHtml(objCompanies.getContactCell2()));
		objCompaniesbe.setContactCell3(StringEscapeUtils.escapeHtml(objCompanies.getContactCell3()));
		objCompaniesbe.setContactCell4(StringEscapeUtils.escapeHtml(objCompanies.getContactCell4()));

		objCompaniesbe.setContactExtension1(StringEscapeUtils.escapeHtml(objCompanies.getContactExtension1()));
		objCompaniesbe.setContactExtension2(StringEscapeUtils.escapeHtml(objCompanies.getContactExtension2()));
		objCompaniesbe.setContactExtension3(StringEscapeUtils.escapeHtml(objCompanies.getContactExtension3()));
		objCompaniesbe.setContactExtension4(StringEscapeUtils.escapeHtml(objCompanies.getContactExtension4()));

		objCompaniesbe.setContactFax1(StringEscapeUtils.escapeHtml(objCompanies.getContactFax1()));
		objCompaniesbe.setContactFax2(StringEscapeUtils.escapeHtml(objCompanies.getContactFax2()));
		objCompaniesbe.setContactFax3(StringEscapeUtils.escapeHtml(objCompanies.getContactFax3()));
		objCompaniesbe.setContactFax4(StringEscapeUtils.escapeHtml(objCompanies.getContactFax4()));

		objCompaniesbe.setContactEmail1(StringEscapeUtils.escapeHtml(objCompanies.getContactEmail1()));
		objCompaniesbe.setContactEmail2(StringEscapeUtils.escapeHtml(objCompanies.getContactEmail2()));
		objCompaniesbe.setContactEmail3(StringEscapeUtils.escapeHtml(objCompanies.getContactEmail3()));
		objCompaniesbe.setContactEmail4(StringEscapeUtils.escapeHtml(objCompanies.getContactEmail4()));

		objCompaniesbe.setMainEmail(StringEscapeUtils.escapeHtml(objCompanies.getMainEmail()));
		objCompaniesbe.setMainFax(StringEscapeUtils.escapeHtml(objCompanies.getMainFax()));
		objCompaniesbe.setBillingControlNumber(String.valueOf(objCompanies.getBillingControlNumber()));
		objCompaniesbe.setThemeFullDisplay(StringEscapeUtils.escapeHtml(objCompanies.getThemeFullDisplay()));
		objCompaniesbe.setThemeMobile(StringEscapeUtils.escapeHtml(objCompanies.getThemeMobile()));
		objCompaniesbe.setThemeRF(StringEscapeUtils.escapeHtml(objCompanies.getThemeRF()));
		objCompaniesbe.setPurchageOrdersRequireApproval(StringEscapeUtils.escapeHtml(objCompanies.getPurchageOrdersRequireApproval()));
		objCompaniesbe.setTimeZone(StringEscapeUtils.escapeHtml(objCompanies.getTimeZone()));
		objCompaniesbe.setLogo(StringEscapeUtils.escapeHtml(objCompanies.getLogo()));
		
		
		return objCompaniesbe;
	}

	
	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014 
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return
	 * String
	 */
	public String ConvertDate(Date dateInString,String StrInFormat,String StrOutFormat)
	{
		DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		String StrFormattedString=GLOBALCONSTANT.BlankString ;
		try {

			Date date = dateInString;
			StrFormattedString=dateFormat.format(date);

		} catch (Exception e) {}

		return StrFormattedString;

	}


	/**
	 * 
	 * @Description Delete existing company from table companies  
	 * @author Rahul Kumar
	 * @Date Dec 29, 2014 
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE#deleteCompany(java.lang.String, java.lang.String)
	 */
	public Boolean deleteCompany(String Tenant, String CompanyId) throws JASCIEXCEPTION {
		
		return objCompanyserviceapi.deleteCompany(Tenant, CompanyId);
		
	}
	

	


}
