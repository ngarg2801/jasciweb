/*

Date Developed  dec 23 2014
Created By Rakesh Pal
Description  Provide service layer between controller and Rest full service  layer.
 */


package com.jasci.biz.AdminModule.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.NOTESSERVICEAPI;
import com.jasci.biz.AdminModule.be.NOTESFIELDBE;
import com.jasci.biz.AdminModule.be.NOTESLABELBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.biz.AdminModule.model.NOTESPK;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.TIMECHECK;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.NOTESBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class NOTESSERVICESIMP implements INOTESSERVICES {

	
	@Autowired		
	NOTESSERVICEAPI objNotesServiceapi;
	@Autowired
	private SessionFactory sessionFactory ;
	@Autowired
	LANGUANGELABELSAPI ObjLanguageLabelsAPI;
	@Autowired
	COMMONSESSIONBE  objCommonsessionbe;
	
	//static Logger log = Logger.getLogger(NOTESCONTROLLER.class.getName());
	 NOTESFIELDBE objNotesFieldBe;
	// List<NOTES> objNotes;
	
	
	/**
	 * @author Rakesh Pal
	 * @Date Dec 24, 2014 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param Team_Member
	 * @return notesList
	 * @throws JASCIEXCEPTION
	 */

	
	public List<NOTESFIELDBE> getNotesList(String Notes_Link,String Notes_Id,String Tenant_ID, String Company_ID){
		
		List<NOTES> objNotes=null;
		
		try
		{
			objNotes= objNotesServiceapi.getNotesList(Notes_Link, Notes_Id,Tenant_ID,Company_ID);
		}
	
		catch(JASCIEXCEPTION objJasciexception){
			
//			log.error(objJasciexception.getMessage());
		}
		List<NOTESFIELDBE> objNotesfieldbes=new ArrayList<NOTESFIELDBE>();
		if(!objNotes.isEmpty()){
		
		for(NOTES valueNotes:objNotes ){
		
		String Formatted_date=ConvertDate(valueNotes.getLast_Acitivity_Date(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format,GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format);	
		Formatted_date=TIMECHECK.ConvertUTCToClientTimeZone(Formatted_date, GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.InputTimezoneUTC, GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, objCommonsessionbe.getClietnDeviceTimeZone());
		NOTESFIELDBE objNotesFieldBe=new NOTESFIELDBE();

		objNotesFieldBe.setTenant_Id(valueNotes.getId().getTenant_Id());
		
		objNotesFieldBe.setCompany_Id(valueNotes.getId().getCompany_Id());
		
		objNotesFieldBe.setNote_Id(valueNotes.getId().getNote_Id());
		
		objNotesFieldBe.setNote_Link(valueNotes.getId().getNote_Link());
		
		objNotesFieldBe.setAUTO_ID(String.valueOf(valueNotes.getId().getID()));
		
		objNotesFieldBe.setLast_Activity_Date(Formatted_date);
		
		objNotesFieldBe.setFormatted_date(Formatted_date);
		
		objNotesFieldBe.setLast_Activity_Team_Member(valueNotes.getLast_Activity_Team_Member());
		
		objNotesFieldBe.setNotesExtra(valueNotes.getCONTINUATIONS_SEQUENCE());
		String notes=valueNotes.getNote();
		StringBuilder sb = new StringBuilder();
		for(int NotesNumber=GLOBALCONSTANT.INT_ZERO,notescount=GLOBALCONSTANT.INT_ZERO;NotesNumber<notes.length();NotesNumber++,notescount++)
		{
			if(notes.charAt(NotesNumber)!=GLOBALCONSTANT.BlankSpace && notescount==GLOBALCONSTANT.INT_FOURTYEIGHT)
			{
				sb.append(GLOBALCONSTANT.Single_Space+notes.charAt(NotesNumber));
				notescount=GLOBALCONSTANT.INT_ZERO;
			}else if(notes.charAt(NotesNumber)==GLOBALCONSTANT.BlankSpace)
			{
				sb.append(notes.charAt(NotesNumber));
				notescount=GLOBALCONSTANT.INT_ZERO;
			}
				else{
				sb.append(notes.charAt(NotesNumber));
			}
		}
		objNotesFieldBe.setNote(sb.toString());
		
			
		objNotesfieldbes.add(objNotesFieldBe);
		}
		
		}
		return objNotesfieldbes;	
	}
	
	/**
	 * @author Rakesh Pal
	 * @Date Dec 24, 2014 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param Team_Member
	 * @param companyId
	 * @param TenantId
	 * @return update note
	 * @throws JASCIEXCEPTION
	 */
	
	

	public List<NOTESFIELDBE> getnote(String Notes_Link, String Notes_Id ,String Tenant_id , String Company_id,int ID)throws JASCIEXCEPTION {
		
		List<NOTES> objNotes=null;
		
		//Date lastdate=convertStrToDate(LastActivitydate, GLOBALCONSTANT.yyyy_MM_dd_Format,GLOBALCONSTANT.Insert_DateFormat_DD_MMM_YY);
		

		
		try
		{
			objNotes= objNotesServiceapi.getnote(Notes_Link, Notes_Id, Tenant_id,Company_id,ID);
			
		}
		
          catch(JASCIEXCEPTION objJasciexception){
			
			//log.error(objJasciexception.getMessage());
		}
		
		List<NOTESFIELDBE> objNotesfieldbelist=new ArrayList<NOTESFIELDBE>();	
		
		for(NOTES  valueNotes:objNotes){
			
			String Formatted_date=ConvertDate(valueNotes.getLast_Acitivity_Date(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
	//		Formatted_date=TimeCheck.ConvertUTCToClientTimeZone(Formatted_date, GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.InputTimezoneUTC, GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, objCommonsessionbe.getClietnDeviceTimeZone());
			NOTESFIELDBE objNotesFieldBe=new NOTESFIELDBE();
		
			objNotesFieldBe.setTenant_Id(valueNotes.getId().getTenant_Id());
			objNotesFieldBe.setCompany_Id(valueNotes.getId().getCompany_Id());
			objNotesFieldBe.setNote_Id(valueNotes.getId().getNote_Id());
			objNotesFieldBe.setNote_Link(StringEscapeUtils.escapeHtml(valueNotes.getId().getNote_Link()));
			objNotesFieldBe.setAUTO_ID(String.valueOf(valueNotes.getId().getID()));
			objNotesFieldBe.setLast_Activity_Date(Formatted_date);
			objNotesFieldBe.setLast_Activity_Team_Member(valueNotes.getLast_Activity_Team_Member());
			objNotesFieldBe.setNote(valueNotes.getNote());
			objNotesFieldBe.setNotesExtra(valueNotes.getCONTINUATIONS_SEQUENCE());

			objNotesfieldbelist.add(objNotesFieldBe);
		
		}

		return objNotesfieldbelist;
	}
	
	public NOTESLABELBE getNotesLabels(String StrModuleName,String StrLanguageCode)
	
	{
		NOTESLABELBE objNoteslabelbe=null;
		List<LANGUAGES> lstLanguages = ObjLanguageLabelsAPI.GetScreenLabels(StrModuleName, StrLanguageCode);
		
		objNoteslabelbe=convertLanguageListToBEanLabels(lstLanguages);
		
		return objNoteslabelbe;
		
	}
	
	
	public Boolean addOrUpdateNotes(NOTESBE objNotesbe) throws JASCIEXCEPTION {

		return null;
	}


	
	
	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 25, 2014 
	 * @param dateInString
	 * @param objNotefield
	 * @param StrOutFormat
	 * @return
	 * String
	 * @throws ParseException 
	 */
	
	
	
	public Boolean addOrUpdateNotesEntry(NOTESFIELDBE objNotesFieldbe)
			throws JASCIEXCEPTION, ParseException {

		NOTES objnotes=new NOTES();
		NOTESPK objnotesPK=new NOTESPK();
		
try
{
	
	 	String LastupdatedDate=objNotesFieldbe.getLast_Activity_Date();
	 	//Date date = null;
		objnotesPK.setTenant_Id(objNotesFieldbe.getTenant_Id());
		objnotesPK.setCompany_Id(objNotesFieldbe.getCompany_Id());
		objnotesPK.setNote_Id(objNotesFieldbe.getNote_Id());
		objnotesPK.setNote_Link(objNotesFieldbe.getNote_Link());
		objnotesPK.setID(Integer.parseInt(objNotesFieldbe.getAUTO_ID()));
		SimpleDateFormat newformat= new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		Date lastupdate=newformat.parse(LastupdatedDate);	
        objnotes.setLast_Acitivity_Date(lastupdate);
        if(objNotesFieldbe.getNotesExtra()==null){
        	objnotes.setCONTINUATIONS_SEQUENCE(GLOBALCONSTANT.BlankString);
        }else{
        objnotes.setCONTINUATIONS_SEQUENCE(objNotesFieldbe.getNotesExtra());
        }
		objnotes.setLast_Activity_Team_Member(objNotesFieldbe.getLast_Activity_Team_Member());
		objnotes.setNote(objNotesFieldbe.getNote());
		objnotes.setId(objnotesPK);
}

catch(Exception objJasciexception){
	
		throw new JASCIEXCEPTION(objJasciexception.getMessage());
	}

		objNotesServiceapi.addOrUpdateNotesEntry(objnotes);
		return true;
	
	}

	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 30, 2014 
	 * @param Tenant_Id
	 * @param Company_id
	 * @param Note_id
	 * @return Status
	 * 
	 */
	
	public Boolean deleteNote(String Tenant_Id, String Company_id,
			String Note_id, String Note_link, String ID)
			throws JASCIEXCEPTION {
		return objNotesServiceapi.deleteNote(Tenant_Id,Company_id,Note_id,Note_link,ID);
		
	}
	

	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 30, 2014 
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return
	 * String
	 */
	public String ConvertDate(Date dateInString,String StrInFormat,String StrOutFormat)
	 {
	  
	  DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
	  String StrFormattedString=GLOBALCONSTANT.BlankString ;
	   
	 
	  try {
	  
	   Date date = dateInString;
	   StrFormattedString=dateFormat.format(date);
	 	  
	  } catch (Exception e) {}
	  
	  return StrFormattedString;
	  
	 }

	
	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 30, 2014 
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return Date
	 * 
	 */
	public Date convertStrToDate(String dateInString,String StrInFormat,String StrOutFormat)
	 {
	  
	  Date date=null; 
	  Date lastupdatedate=null;

	  try {
			DateFormat df = new SimpleDateFormat(StrInFormat);
			date = (Date) df.parse(dateInString);
			SimpleDateFormat newformat= new SimpleDateFormat(StrOutFormat);
			String LastUpdateDate=newformat.format(date);
			lastupdatedate=newformat.parse(LastUpdateDate);
	 	  
	  } catch (Exception e) { }
	  
	  return lastupdatedate;
	  
	 }
	
	
public NOTESLABELBE convertLanguageListToBEanLabels(List<LANGUAGES> lstLanguages)
{
	NOTESLABELBE objNoteslabelbe= new NOTESLABELBE();
	for (LANGUAGES languages : lstLanguages) {
		
		if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
		{
			objNoteslabelbe.setLbl_Add_New_noteslookup(languages.getTranslation());
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_By_noteslookup))
		{
			objNoteslabelbe.setLbl_By_noteslookup(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
		{
			objNoteslabelbe.setLbl_Cancel_NotesMaintenance(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_ActivityDate))
		{
			objNoteslabelbe.setLbl_Date_noteslookup(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
		{
			objNoteslabelbe.setLbl_Delete_noteslookup(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
		{
			objNoteslabelbe.setLbl_Edit_noteslookup(languages.getTranslation());
			
		}
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
		{
			objNoteslabelbe.setLbl_Last_Activity_By_NotesMaintenance(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
		{
			objNoteslabelbe.setLbl_note_delete_msg(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Note))
		{
			objNoteslabelbe.setLbl_Note_noteslookup(languages.getTranslation());
			objNoteslabelbe.setLbl_Note_NotesMaintenance(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
		{
			objNoteslabelbe.setLbl_note_save_msg(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
		{
			objNoteslabelbe.setLbl_note_update_msg(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_NoteID_NotesMaintenance))
		{
			objNoteslabelbe.setLbl_NoteID_NotesMaintenance(languages.getTranslation());
			
		}
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_NoteLink_NotesMaintenance))
		{
			objNoteslabelbe.setLbl_NoteLink_NotesMaintenance(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
		{
			objNoteslabelbe.setLbl_Notes_Date_YY_MM_DD_NotesMaintenance(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_NotesMaintenance_title))
		{
			objNoteslabelbe.setLbl_NotesMaintenance_title(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
		{
			objNoteslabelbe.setLbl_Save_Update_NotesMaintenance(languages.getTranslation());
			
		}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Notes))
		{
			objNoteslabelbe.setLbl_SearchLookup_title(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
		{
			objNoteslabelbe.setLbl_Select_noteslookup(languages.getTranslation());
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Note_blank_field_error_msg))
		{
			objNoteslabelbe.setLbl_note_blank_field_error_msg(languages.getTranslation());
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
		{
			objNoteslabelbe.setLbl_Reset_NotesMaintenance(languages.getTranslation());
			
		}else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_CONTINUATIONS_SEQUENCE))
		{
			objNoteslabelbe.setLbl_CONTINUATIONS_SEQUENCE(languages.getTranslation());
			
		}else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add))
		{
			objNoteslabelbe.setLbl_Add(languages.getTranslation());
			
		}
		
	}
	
	return objNoteslabelbe;
}

	
}







