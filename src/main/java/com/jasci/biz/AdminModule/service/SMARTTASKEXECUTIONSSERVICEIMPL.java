/**
Description This class used to make relation with restfull services means call the restfull service functions from dao and jsp pages.
Created By	:  Toe-On Chia  
Created Date :  Step 24 2015
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.SMARTTASKEXECUTIONSSERVICEAPI;
import com.jasci.biz.AdminModule.be.SMARTTASKEXECUTIONSBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONSBEAN;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class SMARTTASKEXECUTIONSSERVICEIMPL implements ISMARTTASKEXECUTIONSSERVICE {

	/** Make the instance of LANGUANGELABELSAPI for get the KP and the labels on the screen from database */
	@Autowired
	private LANGUANGELABELSAPI ObjLanguageLabelApi;
	
	@Autowired
	private SMARTTASKEXECUTIONSSERVICEAPI ObjRestServiceClient;
	/** It is used to get the value form Common Session**/
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;

	 @Transactional
	 public SMARTTASKEXECUTIONSBE getSmartTaskExecutionsLabels(String StrLanguage) throws JASCIEXCEPTION {
		
			List<LANGUAGES> ObjListLanguages=ObjLanguageLabelApi.GetScreenLabels(LANGUANGELABELSAPI.StrSmartTaskExecutionsModule, StrLanguage);
			
			SMARTTASKEXECUTIONSBE smartTaskExecutionsBean=new SMARTTASKEXECUTIONSBE();  
			String StrObjectFieldCode=null;
		
				for (LANGUAGES ObjLanguages : ObjListLanguages) {
			
					try {
							StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
					} catch(Exception ObjectException){
						throw new JASCIEXCEPTION(ObjectException.getMessage());			
					}	
					
					if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Application)) {
							smartTaskExecutionsBean.setSmartTaskExecutions_Application(ObjLanguages.getTranslation());
					}
					else if  (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Group)) {
							smartTaskExecutionsBean.setSmartTaskExecutions_Execution_Group(ObjLanguages.getTranslation());
					}
					else if  (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Type)) {
							smartTaskExecutionsBean.setSmartTaskExecutions_Execution_Type(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Device))  {
							smartTaskExecutionsBean.setSmartTaskExecutions_Execution_Device(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Name))  {
						smartTaskExecutionsBean.setSmartTaskExecutions_Execution_Name(ObjLanguages.getTranslation());
				    }
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Path))  {
						smartTaskExecutionsBean.setSmartTaskExecutions_Execution_Path(ObjLanguages.getTranslation());
				    }
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Description_Short(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Long))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Description_Long(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Any_Part_of_the_Execution_Description)) 	{
						smartTaskExecutionsBean.setSmartTaskExecutions_Any_Part_of_the_Execution_Description(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Tenant)) {
						smartTaskExecutionsBean.setSmartTaskExecutions_Tenant(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Buttons)) {
						smartTaskExecutionsBean.setSmartTaskExecutions_Buttons(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Button_Name))	{
						smartTaskExecutionsBean.setSmartTaskExecutions_Buttons_Name(ObjLanguages.getTranslation());
				    }
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select)) {
						smartTaskExecutionsBean.setSmartTaskExecutions_Select(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New)) {
						smartTaskExecutionsBean.setSmartTaskExecutions_New(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))  {
						smartTaskExecutionsBean.setSmartTaskExecutions_Display_All(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Search))	{
						smartTaskExecutionsBean.setSmartTaskExecutions_Search(ObjLanguages.getTranslation());
					}		
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Smart_Task_Executions_Lookup))	{
							smartTaskExecutionsBean.setSmartTaskExecutions_Executions_Lookup(ObjLanguages.getTranslation());
					} 
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_YES))	{
						smartTaskExecutionsBean.setSmartTaskExecutions_Yes(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Y))	{
						smartTaskExecutionsBean.setSmartTaskExecutions_Y(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Type)) {
						smartTaskExecutionsBean.setSmartTaskExecutions_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Group))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Group(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Device))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Device(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution))  {
						smartTaskExecutionsBean.setSmartTaskExecutions_Execution(ObjLanguages.getTranslation());
				    }
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Smart_Task_Executions_Search_Lookup))	{
						smartTaskExecutionsBean.setSmartTaskExecutions_Search_Lookup(ObjLanguages.getTranslation());
				   }
				  else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Smart_Task_Execution_Maintenance))	{
						smartTaskExecutionsBean.setSmartTaskExecutions_Executions_Maintenance(ObjLanguages.getTranslation());
				   }
				  else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Last_Activity_Date(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Last_Activity_By(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Your_record_has_been_saved_successfully(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Adding_Record_Failed))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Adding_record_failed(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Company(ObjLanguages.getTranslation());
					}
					else if (StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_N))	{
						smartTaskExecutionsBean.setSmartTaskExecutions_N(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Application))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Please_select_Application(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Execution_Group))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Please_select_Execution_Group(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Execution_Type))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Please_select_Execution_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Execution_Device))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Please_select_Execution_Device(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Tenant))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Please_enter_Tenant(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Application))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Invalid_Application(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Execution_Group))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Invalid_Execution_Group(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Execution_Type))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Invalid_Execution_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Execution_Device))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Invalid_Execution_Device(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Tenant))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Invalid_Tenant(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Add_New(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Edit(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Notes))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Notes(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Delete(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Actions(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Are_you_sure_you_want_to_delete_this_record(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Last_Activity_Date(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Last_Activity_By(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Help_Line))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Help_Line(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Save_Update(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Reset(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Cancel(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Your_record_has_been_updated_successfully(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution))
					{
						smartTaskExecutionsBean.setSmartTaskExecutions_Execution(ObjLanguages.getTranslation());
					}
				   
				}
				return smartTaskExecutionsBean;	
	  }	
	
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List getList(String StrTenant, String StrApplication,String StrExecutionSequenceGroup,String StrExecutionType,String StrExecutionDevice,String StrExecutionName,String StrDescription,String StrButton) throws JASCIEXCEPTION{
				return ObjRestServiceClient.getList(StrApplication, StrExecutionSequenceGroup, StrExecutionType, StrExecutionDevice, StrExecutionName, StrDescription, StrTenant, StrButton);
	}

	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)	
	public List getDisplayAll() throws JASCIEXCEPTION{
			return ObjRestServiceClient.getDisplayAll();
	}
	
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public void setPageName(String StrPageName)  throws JASCIEXCEPTION {
			ObjRestServiceClient.setPageName(StrPageName);	
	}
	
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public Boolean addExecutions(SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean) throws JASCIEXCEPTION {
			Boolean status = ObjRestServiceClient.addExecutions(smartTaskExecutionsBean);
			return status;
	}
		
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public boolean deleteExecutions(String StrTenant_Id, String StrCompany_Id, String StrExecution_Name) throws JASCIEXCEPTION{
			return ObjRestServiceClient.deleteExecutions(StrTenant_Id, StrCompany_Id, StrExecution_Name);
	}

	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List getExecutions(String StrTenant_Id, String StrCompany_Id, String StrExecution_Name)	throws JASCIEXCEPTION {
			return ObjRestServiceClient.getExecutions(StrTenant_Id, StrCompany_Id, StrExecution_Name);
	}
	
}


