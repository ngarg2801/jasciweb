/**
 Date Developed :Jan 07 2015
 Created By "Rahul Kumar"
 Description  Provide service layer between  controller and Rest Full service  layer
 */



package com.jasci.biz.AdminModule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.LOCATIONLABELPRINTSERVICEAPI;
import com.jasci.biz.AdminModule.be.LOCATIONLABELPRINTSCREENBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.exception.JASCIEXCEPTION;



@Service
public class LOCATIONLABELPRINTSERVICEIMPL implements ILOCATIONLABELPRINTSERVICE {

	@Autowired
	LANGUANGELABELSAPI objLanguangelabelsapi;
	@Autowired
	LOCATIONLABELPRINTSERVICEAPI objLocationlabelprintserviceapi;
	
	
	/**
	  *
	  @Description set screen label language
	  @author Rahul Kumar
	  @Date Jan 7, 2015
	  @param StrScreenName
	  @param StrLanguage
	  @return
	  @throws JASCIEXCEPTION
	  @see com.jasci.biz.AdminModule.service.ILOCATIONLABELPRINTSERVICE#setScreenLanguage(java.lang.String, java.lang.String)
	 */

	public LOCATIONLABELPRINTSCREENBE setScreenLanguage(String StrScreenName, String StrLanguage) throws JASCIEXCEPTION {
		
		List<LANGUAGES> listLanguages=objLanguangelabelsapi.GetScreenLabels(StrScreenName, StrLanguage);
		LOCATIONLABELPRINTSCREENBE objLocationlabelprintscreenbe=new LOCATIONLABELPRINTSCREENBE();
		
		for (LANGUAGES ObjLanguages : listLanguages) 
		{
			String StrKeyPhrase=ObjLanguages.getId().getKeyPhrase();
			String StrTranslation=ObjLanguages.getTranslation();
			
			
			
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Label_Select)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_LabelSelect(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Select(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Label_Selection)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_LocationLabelSelection(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reprint_Flagged_Locations)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_ReprintFlaggedLocations(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Area)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Area(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Control_Number)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_WizardControlNumber(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_From_Location)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_FromLocation(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_And_Location)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_AndLocation(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_From_Area)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_FromArea(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_To_Location)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_ToLocation(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Label_Type)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_LabelType(StrTranslation);

			}
			
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Barcode_Type)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Barcode_Type(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Barcode_Type3of9)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_BarcodeType3of9(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Barcode_Type128)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_BarcodeType128(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Print_Labels)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_PrintLabels(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Lookup_All_with_Selection)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_LookupAllwithSelection(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_OR)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Or(StrTranslation);

			}
			
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_an_Area)){
				objLocationlabelprintscreenbe.setERR_Please_enter_Area(StrTranslation);
				objLocationlabelprintscreenbe.setERR_Please_select_Area(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_And_Location)){
				objLocationlabelprintscreenbe.setERR_Please_enter_And_Location(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Wizard_Control_Number)){
				objLocationlabelprintscreenbe.setERR_Please_enter_Wizard_Control_Number(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_from_Area)){
				objLocationlabelprintscreenbe.setERR_Please_enter_from_Area_and_Location(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_To_Location)){
				objLocationlabelprintscreenbe.setERR_Please_enter_To_Location(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_From_Location)){
				objLocationlabelprintscreenbe.setERR_Please_enter_From_Location(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_an_Area)){
				objLocationlabelprintscreenbe.setERR_Please_select_Area(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Label_Type)){
				objLocationlabelprintscreenbe.setERR_Please_select_Label_Type(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Barcode_Type)){
				objLocationlabelprintscreenbe.setERR_Please_select_Barcode_Type(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Control_Number_must_be_numeric_only)){
				objLocationlabelprintscreenbe.setERR_Please_enter_numeric_Wizard_Control_Number(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_ID)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_wizard_ID(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Description(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Submit)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Submit(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Cancel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Wizard_Control_Number)){
				objLocationlabelprintscreenbe.setERR_Please_select_Wizard_Control_Number(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_To_Location)){
				objLocationlabelprintscreenbe.setERR_Invalid_To_Location(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_And_Location)){
				objLocationlabelprintscreenbe.setERR_Invalid_And_Location(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_From_Location)){
				objLocationlabelprintscreenbe.setERR_Invalid_From_Location(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_WIZARD_CONTROL_NUMBER)){
				objLocationlabelprintscreenbe.setERR_Invalid_Wizard_Control_Number(StrTranslation);

			}			

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Location)){
				objLocationlabelprintscreenbe.setLocation_Label_Print_Wizard_Location(StrTranslation);

			}else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Either_no_locations_assigned_or_no_locations_flagged_to_reprint_for_the_selected_area)){
			    objLocationlabelprintscreenbe.setERR_LocationLabel_Reprint_Area(StrTranslation);

			   }else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_FulFillment_Center)){
				   objLocationlabelprintscreenbe.setFullfillment_center(ObjLanguages.getTranslation());
				   
			   }else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_Select_Fullfillment_Center)){
				   objLocationlabelprintscreenbe.setKP_Please_Select_Fullfillment_Center(ObjLanguages.getTranslation());
				   
			   }else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Control_Number_not_found_for_selected_Fulfillment_Center)){
				   
				   objLocationlabelprintscreenbe.setWizard_Control_Number_not_found_for_selected_Fulfillment_Center(ObjLanguages.getTranslation());
			   }
			
			
			
		
		}
		return objLocationlabelprintscreenbe;
		
		
	}

	/**
	 *@Description get general code from general code table base on general code id
	 *@Auther Rahul Kumar
	 *@Date Jan 15, 2015	
	 *@param Tenant
	 *@param Company
	 *@param GeneralCodeId
	 *@return
	 *@throws JASCIEXCEPTION
	 *@see com.jasci.biz.AdminModule.service.ILOCATIONLABELPRINTSERVICE#getGeneralCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION {
		
		return objLocationlabelprintserviceapi.getGeneralCode(Tenant, Company, GeneralCodeId);
	}

	
	

}
