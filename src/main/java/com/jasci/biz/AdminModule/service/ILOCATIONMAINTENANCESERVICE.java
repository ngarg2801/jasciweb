/**
Description This Interface used to maintain the standard for implementing functions of LOCATIONMAINTENANCESERVICEIMPL.
Created By Aakash Bishnoi  
Created Date Dec 26 2014
 */

package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.LOCATIONMAINTENANCEBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATIONBEAN;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILOCATIONMAINTENANCESERVICE {


	/**
	  * @author Aakash Bishnoi
	  * @Date Dec 30, 2014
	  * Description:This is used to Add Record in Location Table Form LocationMaintenance Sreen
	  *@param ObjectLocation
	  *@return String
	  *@throws JASCIEXCEPTION
	 
	 */
	public String addEntry(LOCATIONBEAN ObjectLocation) throws JASCIEXCEPTION;
	
	/**
	  * @author Aakash Bishnoi
	  * @Date Jan 6, 2015
	  * Description:This is used to Update Record in Location Table Form LocationMaintenance Sreen
	  *@param ObjectLocation
	  *@return String
	  *@throws JASCIEXCEPTION
	 
	 */
	public String updateEntry(LOCATIONBEAN ObjectLocation) throws JASCIEXCEPTION;
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This Function is used to get the list of dropdown from GeneralCode where GENERAL_CODE_ID will be change
	 *@param StrTenant
	 *@param StrCompany
	 *@param GeneralCodeID
	 *@return  List<GENERALCODES>	  
	 */
	public List<GENERALCODES> getGeneralCode(String StrTenant,String StrCompany,String GeneralCodeID) throws JASCIEXCEPTION;

	/**	 
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCESERVICEIMPL to get the list of dropdown from LOCATION_PROFILES
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFullfilmentCenter
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATIONPROFILES> getLocationProfile(String StrTenant,String StrCompany,String StrFullfilmentCenter) throws JASCIEXCEPTION;

	/**
     * 
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
    public String getTeamMemberName(String Tenant,String TeamMember)throws JASCIEXCEPTION;
    
    
    /**
     * @author Aakash Bishnoi
     * @Date Jan 3, 2015
     * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCESERVICEIMPL for search the list from LOCATION on Location Search Screen.
     *@param StrFulfillmentCenter
     *@param StrArea
     *@param StrDescription
     *@param StrLocationType
     *@param StrLocation
     *@return
    */
   public List<LOCATIONBEAN> getList(String StrLocation,String StrFulfillmentCenter,String StrArea,String StrDescription,String StrLocationType) throws JASCIEXCEPTION;

   /**
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCESERVICEIMPL for fetch data from Location table on the behalf of parameters(Primary Keys)
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@return List<LOCATIONBEAN>
	 */
	public LOCATIONBEAN getFetchLocation(String StrFulfillmentCenter,String StrArea,String StrLocation) throws JASCIEXCEPTION;
	
	/**
	 * Created on:Dec 28 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get FulfillmentCenter list based on tenant.
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * @param string 
	 * 
	 */
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentCenter(String tenant, String string,String PageName) throws  JASCIEXCEPTION;
	
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description: This function is used to delete record form Location table.
	 * @param StrTenant_ID
	 * @param StrCompany_ID
	 * @param StrFetchFulfillment_Center
	 * @param StrFetchArea
	 * @param StrFetchLocation
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	 public Boolean deleteEntry(String StrTenant_ID,String StrCompany_ID,String StrFetchFulfillment_Center,String StrFetchArea,String StrFetchLocation) throws JASCIEXCEPTION;
	
	 /**
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description: This function is used to get the screen label for all locations screen 
	 * @param StrLanguage
	 * @return LOCATIONMAINTENANCEBE
	 * @throws JASCIEXCEPTION
	 */
	 public LOCATIONMAINTENANCEBE getLocationMaintenanceLabels(String StrLanguage) throws JASCIEXCEPTION;
}
