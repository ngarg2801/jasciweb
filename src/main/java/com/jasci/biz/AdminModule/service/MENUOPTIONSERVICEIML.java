/**

Date Developed: Dec 15 2014
Created By: Sarvendra Tyagi
Description: Menu Option Provide service layer between controller and dao layer.
 */

package com.jasci.biz.AdminModule.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.MENUOPTIONLABELBE;
import com.jasci.biz.AdminModule.dao.IMENUOPTIONSDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class MENUOPTIONSERVICEIML implements IMENUOPTIONSERVICE {

	@Autowired
	IMENUOPTIONSDAO objMenuOptionDao;
	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSION;
	
	@Autowired
	LANGUANGELABELSAPI objLanguageLabelAPI;
	
	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Team member list based on given team member name
	 * Input parameter: String SearchFieldValue,String SearchField
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	
	@Transactional
	public List<MENUOPTIONS> getMenuOptionsList(String SearchFieldValue, String SearchField) throws JASCIEXCEPTION {
		
		List<MENUOPTIONS> objMenuoptions=null;
		
		String strApplication=GLOBALCONSTANT.BlankString;
		String strMenuOption=GLOBALCONSTANT.BlankString;
		String strMenuType=GLOBALCONSTANT.BlankString;
		String partOfdesc20=GLOBALCONSTANT.BlankString;
		String partOfDesc50=GLOBALCONSTANT.BlankString;
		String strTenant   =GLOBALCONSTANT.BlankString;
		
		try{
			
			if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblMenuOption) ||
					SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartMenuOption)){
				strMenuOption=SearchFieldValue;
				
				objMenuoptions=objMenuOptionDao.getMenuOptionList(strApplication, strMenuOption, strMenuType, partOfdesc20, partOfDesc50,strTenant);	
			
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblApplication) ||
					SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartApplication)){
				
				strApplication=SearchFieldValue;
					
				objMenuoptions=objMenuOptionDao.getMenuOptionListByApplication(strApplication);
				
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblMenuType)){
				
				strMenuType=SearchFieldValue;
				
				objMenuoptions=objMenuOptionDao.getMenuOptionListByMenuType(strMenuType);
			
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartDescription20)){
				
				partOfdesc20=SearchFieldValue;
				objMenuoptions=objMenuOptionDao.getMenuOptionListByDescription(partOfdesc20);
				
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblPartDescription20)){
				
				partOfDesc50=SearchFieldValue;
				objMenuoptions=objMenuOptionDao.getMenuOptionListByDescription(partOfDesc50);
				
			}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_txtTenant)){
				
				strTenant=SearchFieldValue;
				
						objMenuoptions=objMenuOptionDao.getMenuOptionListByTenant(strTenant);
					
				
				
			
			}
			
			
			
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
			
		}
		
		return objMenuoptions;
	}



	
	/**
	 * @description:This function is used for delete the menu option from menu option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 18 2014
	 */
	@Transactional
	public Boolean deleteMenuOption(String strMenuOption) throws JASCIEXCEPTION {
		 
		Boolean status=false;
		try{
			
			status=objMenuOptionDao.deleteMenuOptionFromMenuProfileOption(strMenuOption);
			
			if(status){
			
			status=objMenuOptionDao.deleteMenuOption(strMenuOption);
			}
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
			
		}
		
		return status;
	}



	/**
	 * @description:This function is used for get  menu option from menu option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 18 2014
	 */
	@Transactional
	public List<MENUOPTIONS> getMenuOptionData(String strMenuOption) throws JASCIEXCEPTION {
	List<MENUOPTIONS> objMenuoptions=null;
		try{
			
			
			objMenuoptions=objMenuOptionDao.getMenuOptionData(strMenuOption);
			
		}catch(JASCIEXCEPTION objException){
			throw new JASCIEXCEPTION(objException.getMessage());	
		}
		return objMenuoptions;
	}



	/** 
	 *  @description :This function to get the list of menu type from General code table
	 * @param        :strMenuTypes
	 * @return       :List<Object>
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 19 2014
	 */
	
	@Transactional
	public List<GENERALCODES> getMenuType(String strMenuTypes)throws JASCIEXCEPTION{
		
		
		List<GENERALCODES> objListMenutype=null;
		try{
			
			
			objListMenutype=objMenuOptionDao.getMenuTypes(strMenuTypes);
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
			
		}
		
		return objListMenutype;
	}
 
	
	
	@Transactional
	public Map<String, List<String>> getApplicationAndSubApplication(String strApplication) throws JASCIEXCEPTION {
		
		
		Map<String, List<String>> objApplicationMap=null;
		//List<String> objsubApplication=null;
		//List<GENERALCODES> listObject=null;
		/*
		try{
			
			listObject=objMenuOptionDao.getApplicationAndSubApplication(strApplication);
			Iterator<Object> ObjIteratorObject=null;
			objApplicationMap=new HashMap<String, List<String>>();
			
			
			Object[] ObjObject = null;
			if(!(listObject.isEmpty()))
			{
				ObjIteratorObject=listObject.iterator();
				for(int applicationNo=0;applicationNo<listObject.size();applicationNo++){
					
					ObjObject=(Object[]) ObjIteratorObject.next();
					
					if(!objApplicationMap.containsKey(ObjObject[0])){
						objsubApplication=new ArrayList<String>();
						
						objsubApplication.add(ObjObject[1].toString());
						objApplicationMap.put(ObjObject[0].toString(), objsubApplication);
						
					}else{
						
						objsubApplication=objApplicationMap.get(ObjObject[0].toString());
						objsubApplication.add(ObjObject[1].toString());
						objApplicationMap.put(ObjObject[0].toString(), objsubApplication);
					}
					
				}
					
				}
			
			
		}catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}*/
		
		return objApplicationMap;
	}


	/** 
	 *  @description :This function is used for updating table row of menu option
	 * @param        :strMenuTypes
	 * @return       :objMenuOption
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Dec 20 2014
	 */
	
	@Transactional
	public MENUOPTIONS update(MENUOPTIONS objMenuoptions) throws JASCIEXCEPTION {
		
		MENUOPTIONS objMenuoptionsBE=null;
		try{
			
             objMenuoptions.setLastActivityTeamMember(OBJCOMMONSESSION.getTeam_Member());
			
			objMenuoptionsBE=objMenuOptionDao.update(objMenuoptions);
			
		}catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objMenuoptionsBE;
	}



	/** 
	 * @Description:This function is getting all data from menuoption table 
	 * @param      : strMenuOption
	 * @return     :List<MENUOPTIONS>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 20 2014
	 */
	@Transactional
	public List<MENUOPTIONS> getMenuOptionDisplayAll() throws JASCIEXCEPTION {
	
		List<MENUOPTIONS> objMenuoptions=null;
		try{
			
			objMenuoptions=objMenuOptionDao.getMenuOptionDisplayAll();
			
			
		}catch(Exception objException){
			
			
		}
		
		return objMenuoptions;
	}
	
	


	/** 
	 *  @description :This function is used for getting label of screen
	 * @param        :strMenuTypes
	 * @return       :objMENUOPTIONLABELBE 
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 23 2014
	 */

	@Transactional
	public MENUOPTIONLABELBE getScreenLabel(String language) throws JASCIEXCEPTION {
		
		MENUOPTIONLABELBE objMenuoptionlabelbe=null;
		
		
		List<LANGUAGES> objLanguagesList=null;
		LANGUAGES objLanguages=null;
		try{
			objMenuoptionlabelbe=new MENUOPTIONLABELBE();
			
			objLanguagesList=objLanguageLabelAPI.GetScreenLabels(LANGUANGELABELSAPI.StrMenuOptionModule,language );
			for(int lblIndex=GLOBALCONSTANT.INT_ZERO;lblIndex<objLanguagesList.size();lblIndex++){
				objLanguages=objLanguagesList.get(lblIndex);
				
				if(LANGUANGELABELSAPI.KP_Actions.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblAction(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_APPLICATIONS.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblGeneralCodeApplication(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Select.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblSelect(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Application.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblApplication(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Add_New.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblAddnew(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Sub_Application.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblApplicationSub(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Cancel.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblCancel(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Delete.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblDelete(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblDescription(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Edit.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblEdit(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Option.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblMenuOption(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Option_Search_LookUp.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblMenuOptionSearchLookUp(objLanguages.getTranslation());;
					
				}else if(LANGUANGELABELSAPI.KP_MenuType.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblMenuType(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Tenant.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblTenant(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Save_Update.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblUpdate_Save(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Tenant_ID.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblTenantID(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Part_of_the_Menu_Option_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblPartOFtheMenuOptionDescription(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblON_UPDATE_SUCCESSFULLY(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_New.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblNew(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Option_Mantenance.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblMenuOptionMantenance(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Option_Maintenance_LookUp.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblMenuOptionMaintenanceLookUp(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Last_Activity_Date.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblLastActivityDate(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Last_Activity_By.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblLastActivityBy(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Help_Line.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblHelpLine(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Execution_Path.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblExecutionPath(objLanguages.getTranslation());
				
				}else if(LANGUANGELABELSAPI.KP_Menu_Option_is_currently_assigned_to_Menu_Profiles_if_Deleted_this_option_will_be_removed_from_all_Menu_Profiles.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_WHILE_DELETING_MENU_PROFILE(objLanguages.getTranslation());
					
					
				}else if(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_WHILE_DELETING_MENU_OPTION(objLanguages.getTranslation());
					
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_Part_Of_the_Menu_Option.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_PART_OF_THE_MENU_OPTION_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_Part_Of_The_Menu_Option_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_PART_OF_THE_MENU_OPTION_DESCRIPTION_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_Menu_Option.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_MENU_OPTION_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Display_All.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLbldisplayall(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Description_Short.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					objMenuoptionlabelbe.setLbldescriptionshrot(objLanguages.getTranslation());
					
					
				}else if(LANGUANGELABELSAPI.KP_Description_Long.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLbldescriptionlong(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Company.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblCompany(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblON_SAVE_SUCCESSFULLY(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_MANDATORY_FIELD_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Option_already_used.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_MENU_OPTION_ALREADY_USED(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Menu_Option.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_INVALID_MENU_OPTION(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_Application.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_APPLICATION_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Application.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_INVALID_APPLICATION(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_select_Menu_Type.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_MENU_TYPE_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Menu_Type.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_INVALID_MENU_TYPE_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Part_Of_The_Menu_Option_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblERR_INVALID_PART_OF_THE_MENU_OPTION_DESCRIPTION(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Tenant_ID.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
				
					objMenuoptionlabelbe.setLblErr_INVALID_TENANTID(objLanguages.getTranslation());
				
				
				}else if(LANGUANGELABELSAPI.KP_Please_enter_Tenant_ID.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					objMenuoptionlabelbe.setLblErr_INVALID_TENANT_ID_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Reset.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					objMenuoptionlabelbe.setLblReset(objLanguages.getTranslation());
				}
				else if(LANGUANGELABELSAPI.KP_Please_enter_valid_Execution_Path.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuoptionlabelbe.setLblMenu_Execution_Path_Not_Valid(objLanguages.getTranslation());
					
				}
				
			
			}
			
		}catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objMenuoptionlabelbe;
	}
	
	

	
}
