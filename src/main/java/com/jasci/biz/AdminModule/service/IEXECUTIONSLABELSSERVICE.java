package com.jasci.biz.AdminModule.service;
/**
Description This interface use for service  for SCAN LPN screen 
Created By Shailendra Rajput 
Created Date Oct 19 2015
 */
import com.jasci.biz.AdminModule.be.EXECUTIONSBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface IEXECUTIONSLABELSSERVICE {

	
	
	/**
	 * 
		 * Created By: Shailendra Rajput
		 * @return EXECUTIONSBE
		 * @throws JASCIEXCEPTION
		 * @Developed Created Date Oct 19 2015
		 * @Description :
	 */
	public EXECUTIONSBE getScanLPNLabels(String Modulename,String defaultLanguage) throws JASCIEXCEPTION;
}
