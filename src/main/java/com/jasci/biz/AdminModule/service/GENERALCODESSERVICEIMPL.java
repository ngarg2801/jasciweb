/*
Description implementation of GeneralCodeService interface in which we call the GeneralCodeDao functionality
Created By Aakash Bishnoi  
Created Date Oct 20 2014
 */
package com.jasci.biz.AdminModule.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jasci.biz.AdminModule.ServicesApi.GENERALCODESERVICEAPI;
import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.GENERALCODESBE;
import com.jasci.biz.AdminModule.dao.IGENERALCODEDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.GENERALCODESBEAN;
import com.jasci.biz.AdminModule.model.GENERALCODESPK;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Service
public class GENERALCODESSERVICEIMPL implements IGENERALCODESSERVICE {

	@Autowired
	GENERALCODESERVICEAPI ObjRestServiceClient;
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
	 @Autowired
	 LANGUANGELABELSAPI ObjLanguageLabelApi;
	 
 @Autowired

 private IGENERALCODEDAO ObjectGeneralCodeDao;

 //Used to call RestfullService for Add GeneralCode And Menuoption
 public String addEntry(GENERALCODESBEAN ObjectGeneralCodes) throws JASCIEXCEPTION  {
	 	GENERALCODESPK obj_GENERALCODESPK = new GENERALCODESPK();
		GENERALCODES obj_GENERALCODES=new GENERALCODES();
		MENUOPTIONS Obj_MENUOPTIONS=new MENUOPTIONS();
		obj_GENERALCODES=useGetterSetter(obj_GENERALCODESPK,obj_GENERALCODES,ObjectGeneralCodes,Obj_MENUOPTIONS);
		return ObjRestServiceClient.addEntry(obj_GENERALCODES,Obj_MENUOPTIONS);
		
 }

//Used to call RestfullService for Manage Add GeneralCodeID
 public String addGeneralCodeIdEntry(GENERALCODESBEAN ObjectGeneralCodes)  throws JASCIEXCEPTION {
		GENERALCODESPK obj_GENERALCODESPK = new GENERALCODESPK();
		GENERALCODES obj_GENERALCODES=new GENERALCODES();
		obj_GENERALCODES=useGetterSetterGeneralCodeID(obj_GENERALCODESPK,obj_GENERALCODES,ObjectGeneralCodes);
		return ObjRestServiceClient.addGeneralCodeIdEntry(obj_GENERALCODES);
 }
 
 
//Used to call RestfullService for get the List GeneralCode where GENERALCODEID="GENERALCODES"
 public List<GENERALCODESBEAN> getList(COMMONSESSIONBE objCommonsessionbe) throws JASCIEXCEPTION {	
	 return ObjRestServiceClient.getList(objCommonsessionbe.getCompany(),objCommonsessionbe.getTenant());
 }

 //This is used to fetch data from General_Codes for Generalcode/GeneralCodeId screen
 public GENERALCODES getEntry(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode) throws JASCIEXCEPTION {
  return ObjRestServiceClient.getEntry(Tenant,Company,Application,GeneralCodeID,GeneralCode);
 }


//Used to call RestfullService for Update GeneralCode and menuoption using generalcode screen
 public String updateEntry(GENERALCODESBEAN ObjectGeneralCodes) throws JASCIEXCEPTION {
	
	 GENERALCODESPK obj_GENERALCODESPK = new GENERALCODESPK();
		GENERALCODES obj_GENERALCODES=new GENERALCODES();
		MENUOPTIONS Obj_MENUOPTIONS=new MENUOPTIONS();
		obj_GENERALCODES=useGetterSetter(obj_GENERALCODESPK,obj_GENERALCODES,ObjectGeneralCodes,Obj_MENUOPTIONS);
		return ObjRestServiceClient.updateEntry(obj_GENERALCODES,Obj_MENUOPTIONS);
		
 }

//Used to call RestfullService for Manage Update GeneralCodeID using generalcode screen
 public String updateGeneralCodeID(GENERALCODESBEAN ObjectGeneralCodes) throws JASCIEXCEPTION {
	 	GENERALCODESPK obj_GENERALCODESPK = new GENERALCODESPK();
		GENERALCODES obj_GENERALCODES=new GENERALCODES();
		
	 obj_GENERALCODES=useGetterSetterGeneralCodeID(obj_GENERALCODESPK,obj_GENERALCODES,ObjectGeneralCodes);
	 return ObjRestServiceClient.updateGeneralCodeID(obj_GENERALCODES);
 } 
 
//Used to show GeneralCOdeEdit/Delete Screen labels
 public GENERALCODESBE SetGeneralCodesBe(String StrLanguage) throws JASCIEXCEPTION {
	
	 List<LANGUAGES> ObjListLanguages= ObjLanguageLabelApi.GetScreenLabels(LANGUANGELABELSAPI.StrGeneralCodes, StrLanguage);
    	GENERALCODESBE ObjGeneralCodesBe=new GENERALCODESBE();  
	
			for (LANGUAGES ObjLanguages : ObjListLanguages) 
			{
				String StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
				if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_Code))
				{
					ObjGeneralCodesBe.setGeneralCodes_GeneralCode(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company))
				{
					ObjGeneralCodesBe.setGeneralCodes_Company(ObjLanguages.getTranslation());
				}				
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
				{
					ObjGeneralCodesBe.setGeneralCodes_AddNewBtn(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Application))
				{
					ObjGeneralCodesBe.setGeneralCodes_Application(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Code_Identification))
				{
					ObjGeneralCodesBe.setGeneralCodes_Code_Identification(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
				{
					ObjGeneralCodesBe.setGeneralCodes_Actions(ObjLanguages.getTranslation());
				}
				
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_Code_Id))
				{
					ObjGeneralCodesBe.setGeneralCodes_GeneralCodeID(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_Code))
				{
					ObjGeneralCodesBe.setGeneralCodes_GeneralCode(ObjLanguages.getTranslation());
				}
				
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_System_Use))
				{
					ObjGeneralCodesBe.setGeneralCodes_SystemUse(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short))
				{
					ObjGeneralCodesBe.setGeneralCodes_Description20(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Long))
				{
					ObjGeneralCodesBe.setGeneralCodes_Description50(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_MenuOptionName))
				{
					ObjGeneralCodesBe.setGeneralCodes_MenuOptionName(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control01_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control01Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control02_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control02Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control03_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control03Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control04_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control04Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control05_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control05Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control06_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control06Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control07_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control07Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control08_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control08Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control09_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control09Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control10_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control10Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control01_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control01Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control02_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control02Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control03_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control03Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control04_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control04Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control05_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control05Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control06_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control06Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control07_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control07Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control08_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control08Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control09_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control09Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Control10_Value))
				{
					ObjGeneralCodesBe.setGeneralCodes_Control10Value(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Help))
				{
					ObjGeneralCodesBe.setGeneralCodes_Helpline(ObjLanguages.getTranslation());
				}
				
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
				{
					ObjGeneralCodesBe.setGeneralCodes_Edit(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_Codes_MainTenance))
				{
					ObjGeneralCodesBe.setGeneralCodes_GeneralCode_NewEdit(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
				{
					ObjGeneralCodesBe.setGeneralCodes_Cancels(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
				{
					ObjGeneralCodesBe.setGeneralCodes_SaveUpdate(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
				{
					ObjGeneralCodesBe.setGeneralCodes_Select(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_GENERALCODES))
				{
					ObjGeneralCodesBe.setGeneralCodes_GENERALCODES(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_YES))
				{
					ObjGeneralCodesBe.setGeneralCodes_YES(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_NO))
				{
					ObjGeneralCodesBe.setGeneralCodes_NO(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_code_or_Menu_name_already_used))
				{
					ObjGeneralCodesBe.setGeneralCodes_ErrorMsg_GeneralCode_MenuOption(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_code_already_used))
				{
					ObjGeneralCodesBe.setGeneralCodes_ErrorMsg_GeneralCode(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
				{
					ObjGeneralCodesBe.setGeneralCodes_ErrorMsg_MandatoryFields(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
				{
					ObjGeneralCodesBe.setGeneralCodes_Confirm_Save(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
				{
					ObjGeneralCodesBe.setGeneralCodes_Confirm_Update(ObjLanguages.getTranslation());
				}
								
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
				{
					ObjGeneralCodesBe.setGeneralCodes_Description(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
				{
					ObjGeneralCodesBe.setGeneralCodes_Actions(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
				{
					ObjGeneralCodesBe.setGeneralCodes_Edit(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
				{
					ObjGeneralCodesBe.setGeneralCodes_Delete(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_General_Codes))
				{
					ObjGeneralCodesBe.setGeneralCodes_General_Code(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
				{
					ObjGeneralCodesBe.setGeneralCodes_Confirm_Delete(ObjLanguages.getTranslation());
				}
				
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short_already_used))
				{
					ObjGeneralCodesBe.setGeneralCodes_Description_Short_is_already_Used(ObjLanguages.getTranslation());
				}

				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_long_already_used))
				{
					ObjGeneralCodesBe.setGeneralCodes_Description_Long_is_already_Used(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Menu_Option_already_used))
				{
					ObjGeneralCodesBe.setGeneralCodes_Menu_Option_already_used(ObjLanguages.getTranslation());
				}
				else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
			    {
			     ObjGeneralCodesBe.setGeneralCodes_Reset(ObjLanguages.getTranslation());
			    }
				
			}	   	   
			return ObjGeneralCodesBe;
	 
 }
 
 //This is for create on generalcode main list
public List<GENERALCODESBEAN> getMainGeneralCodeList(String Tenant,String Company,String GeneralCode)throws JASCIEXCEPTION{	
   return	ObjRestServiceClient.getMainGeneralCodeList(Tenant,Company,GeneralCode);	
}

 //get the sub list of general code screen behalf og generalcodeid
 public List<GENERALCODESBEAN> getSubGeneralCodeList(String Tenant,String Company,String GeneralCodeID)throws JASCIEXCEPTION{
	 return	ObjRestServiceClient.getSubGeneralCodeList(Tenant,Company,GeneralCodeID);
 }
 
 
//It is used to delete generalcode related All entry and menuoption
 public Boolean deleteEntry(String Tenant,String Company,String Application,String GeneralCodeId,String GeneralCode) throws JASCIEXCEPTION{
	 return ObjRestServiceClient.deleteEntry(Tenant,Company,Application,GeneralCodeId,GeneralCode);
 }
 

//It is used to delete manage generalcode id
public Boolean deleteGeneralCodeID(String Tenant,String Company,String Application,String GeneralCodeId,String GeneralCode) throws JASCIEXCEPTION {
	 return ObjRestServiceClient.deleteGeneralCodeID(Tenant,Company,Application,GeneralCodeId,GeneralCode);
 }


//Set the value of screen for composite key use Getter Setter for get and set value of generalcodes and menu options table on screen General Codes
public GENERALCODES useGetterSetter(GENERALCODESPK obj_GENERALCODESPK,GENERALCODES obj_GENERALCODES,GENERALCODESBEAN ObjectGeneralCodes,MENUOPTIONS Obj_MENUOPTIONS){

	//set menuoption data
	
	Obj_MENUOPTIONS.setTenant(trimValues(objCommonsessionbe.getTenant()));
	Obj_MENUOPTIONS.setApplication(trimValues(ObjectGeneralCodes.getApplication()));
	Obj_MENUOPTIONS.setMenuOption(trimValues(ObjectGeneralCodes.getMenuOptionName()));
	Obj_MENUOPTIONS.setDescription20(trimValues(ObjectGeneralCodes.getDescription20()));
	Obj_MENUOPTIONS.setDescription50(trimValues(ObjectGeneralCodes.getDescription50()));
	
	//Obj_MENUOPTIONS.setHelpline(trimValues(ObjectGeneralCodes.getHelpline()));
	//Obj_MENUOPTIONS.setExecution(GLOBALCONSTANT.GeneralCode_MenuExceutionpath+trimValues(ObjectGeneralCodes.getGeneralCode()));
	Obj_MENUOPTIONS.setMenuType(trimValues(objCommonsessionbe.getMenuType()));
	Obj_MENUOPTIONS.setApplicationSub(trimValues(ObjectGeneralCodes.getApplication()));
	Obj_MENUOPTIONS.setHelpline(trimValues(ObjectGeneralCodes.getMenuOptionName()));
	Obj_MENUOPTIONS.setExecution(GLOBALCONSTANT.GeneralCode_MenuExceutionpath+trimValues(ObjectGeneralCodes.getMenuOptionName()));
	Date StrCurrentDate=null;
	  // Date ObjDate1 = new Date();
	   DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format);
	   try {
		StrCurrentDate=ObjDateFormat1.parse(ObjectGeneralCodes.getLastActivityDate());
	} catch (ParseException e) {}
	   
	  
	Obj_MENUOPTIONS.setLastActivityDate(StrCurrentDate);
	Obj_MENUOPTIONS.setLastActivityTeamMember(trimValues(objCommonsessionbe.getTeam_Member()));
	
	// Set GENERALCODESPK values

	obj_GENERALCODESPK.setTenant(trimValues(objCommonsessionbe.getTenant()));
	obj_GENERALCODESPK.setApplication(trimValues(ObjectGeneralCodes.getApplication()));
	obj_GENERALCODESPK.setCompany(trimValues(objCommonsessionbe.getCompany()));
	obj_GENERALCODESPK.setGeneralCode(trimValues(ObjectGeneralCodes.getGeneralCode()));
	obj_GENERALCODESPK.setGeneralCodeID(trimValues(ObjectGeneralCodes.getGeneralCodeID()));

	//Set GENERALCODES values

	obj_GENERALCODES.setControl01Description(trimValues(ObjectGeneralCodes.getControl01Description()));
	obj_GENERALCODES.setControl02Description(trimValues(ObjectGeneralCodes.getControl02Description()));
	obj_GENERALCODES.setControl03Description(trimValues(ObjectGeneralCodes.getControl03Description()));
	obj_GENERALCODES.setControl04Description(trimValues(ObjectGeneralCodes.getControl04Description()));
	obj_GENERALCODES.setControl05Description(trimValues(ObjectGeneralCodes.getControl05Description()));
	obj_GENERALCODES.setControl06Description(trimValues(ObjectGeneralCodes.getControl06Description()));
	obj_GENERALCODES.setControl07Description(trimValues(ObjectGeneralCodes.getControl07Description()));
	obj_GENERALCODES.setControl08Description(trimValues(ObjectGeneralCodes.getControl08Description()));
	obj_GENERALCODES.setControl09Description(trimValues(ObjectGeneralCodes.getControl09Description()));
	obj_GENERALCODES.setControl10Description(trimValues(ObjectGeneralCodes.getControl10Description()));

	obj_GENERALCODES.setControl01Value(trimValues(ObjectGeneralCodes.getControl01Value()));
	obj_GENERALCODES.setControl02Value(trimValues(ObjectGeneralCodes.getControl02Value()));
	obj_GENERALCODES.setControl03Value(trimValues(ObjectGeneralCodes.getControl03Value()));
	obj_GENERALCODES.setControl04Value(trimValues(ObjectGeneralCodes.getControl04Value()));
	obj_GENERALCODES.setControl05Value(trimValues(ObjectGeneralCodes.getControl05Value()));
	obj_GENERALCODES.setControl06Value(trimValues(ObjectGeneralCodes.getControl06Value()));
	obj_GENERALCODES.setControl07Value(trimValues(ObjectGeneralCodes.getControl07Value()));
	obj_GENERALCODES.setControl08Value(trimValues(ObjectGeneralCodes.getControl08Value()));
	obj_GENERALCODES.setControl09Value(trimValues(ObjectGeneralCodes.getControl09Value()));
	obj_GENERALCODES.setControl10Value(trimValues(ObjectGeneralCodes.getControl10Value()));

	obj_GENERALCODES.setDescription20(trimValues(ObjectGeneralCodes.getDescription20()));
	obj_GENERALCODES.setDescription50(trimValues(ObjectGeneralCodes.getDescription50()));

	obj_GENERALCODES.setMenuOptionName(trimValues(ObjectGeneralCodes.getMenuOptionName()));
	
	obj_GENERALCODES.setSystemUse(trimValues(ObjectGeneralCodes.getSystemUse()));

	obj_GENERALCODES.setHelpline(trimValues(ObjectGeneralCodes.getHelpline()));
		
	//This block is used to Set Current Date
		
	   
	obj_GENERALCODES.setLastActivityDate(StrCurrentDate);
	obj_GENERALCODES.setLastActivityTeamMember(trimValues(objCommonsessionbe.getTeam_Member()));
	obj_GENERALCODES.setId(obj_GENERALCODESPK);
	
	return obj_GENERALCODES;
}

	//This function is used to onlu set getter and setter of generalcodes for GeneralCodeId screen
	public GENERALCODES useGetterSetterGeneralCodeID(GENERALCODESPK obj_GENERALCODESPK,GENERALCODES obj_GENERALCODES,GENERALCODESBEAN ObjectGeneralCodes){
			
		// Set GENERALCODESPK values

		obj_GENERALCODESPK.setTenant(trimValues(objCommonsessionbe.getTenant()));
		obj_GENERALCODESPK.setApplication(trimValues(ObjectGeneralCodes.getApplication()));
		obj_GENERALCODESPK.setCompany(trimValues(objCommonsessionbe.getCompany()));
		obj_GENERALCODESPK.setGeneralCode(trimValues(ObjectGeneralCodes.getGeneralCode()));
		obj_GENERALCODESPK.setGeneralCodeID(trimValues(ObjectGeneralCodes.getGeneralCodeID()));

		//Set GENERALCODES values

		obj_GENERALCODES.setControl01Description(trimValues(ObjectGeneralCodes.getControl01Description()));
		obj_GENERALCODES.setControl02Description(trimValues(ObjectGeneralCodes.getControl02Description()));
		obj_GENERALCODES.setControl03Description(trimValues(ObjectGeneralCodes.getControl03Description()));
		obj_GENERALCODES.setControl04Description(trimValues(ObjectGeneralCodes.getControl04Description()));
		obj_GENERALCODES.setControl05Description(trimValues(ObjectGeneralCodes.getControl05Description()));
		obj_GENERALCODES.setControl06Description(trimValues(ObjectGeneralCodes.getControl06Description()));
		obj_GENERALCODES.setControl07Description(trimValues(ObjectGeneralCodes.getControl07Description()));
		obj_GENERALCODES.setControl08Description(trimValues(ObjectGeneralCodes.getControl08Description()));
		obj_GENERALCODES.setControl09Description(trimValues(ObjectGeneralCodes.getControl09Description()));
		obj_GENERALCODES.setControl10Description(trimValues(ObjectGeneralCodes.getControl10Description()));

		obj_GENERALCODES.setControl01Value(trimValues(ObjectGeneralCodes.getControl01Value()));
		obj_GENERALCODES.setControl02Value(trimValues(ObjectGeneralCodes.getControl02Value()));
		obj_GENERALCODES.setControl03Value(trimValues(ObjectGeneralCodes.getControl03Value()));
		obj_GENERALCODES.setControl04Value(trimValues(ObjectGeneralCodes.getControl04Value()));
		obj_GENERALCODES.setControl05Value(trimValues(ObjectGeneralCodes.getControl05Value()));
		obj_GENERALCODES.setControl06Value(trimValues(ObjectGeneralCodes.getControl06Value()));
		obj_GENERALCODES.setControl07Value(trimValues(ObjectGeneralCodes.getControl07Value()));
		obj_GENERALCODES.setControl08Value(trimValues(ObjectGeneralCodes.getControl08Value()));
		obj_GENERALCODES.setControl09Value(trimValues(ObjectGeneralCodes.getControl09Value()));
		obj_GENERALCODES.setControl10Value(trimValues(ObjectGeneralCodes.getControl10Value()));
	
		obj_GENERALCODES.setDescription20(trimValues(ObjectGeneralCodes.getDescription20()));
		obj_GENERALCODES.setDescription50(trimValues(ObjectGeneralCodes.getDescription50()));

		obj_GENERALCODES.setMenuOptionName(trimValues(ObjectGeneralCodes.getMenuOptionName()));
		
		obj_GENERALCODES.setSystemUse(trimValues(ObjectGeneralCodes.getSystemUse()));

		obj_GENERALCODES.setHelpline(trimValues(ObjectGeneralCodes.getHelpline()));
		//This block is used to Set Current Date
				Date StrCurrentDate=null;
			   DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format);
			   try {
				StrCurrentDate=ObjDateFormat1.parse(ObjectGeneralCodes.getLastActivityDate());
			} catch (ParseException e) {}
			   
		obj_GENERALCODES.setLastActivityDate(StrCurrentDate);
		obj_GENERALCODES.setLastActivityTeamMember(trimValues(objCommonsessionbe.getTeam_Member()));
		obj_GENERALCODES.setId(obj_GENERALCODESPK);
		
		return obj_GENERALCODES;
	}
	//It is used to trim the space
	public static String trimValues(String Value){
		if(Value == null){
			return null;
		}
		else{
			String CheckValue=Value.trim();
			if(CheckValue.length()<GLOBALCONSTANT.IntOne){
				return null;
			}else{						
			return CheckValue;
			}
		}
	}
}

