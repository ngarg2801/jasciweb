/**
Date Developed :Dec 26 2014
Created by: Diksha Gupta
Description :ILOCATIONPROFILESERVICE to call the methods of ILOCATIONPROFILEDAL.
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.LOCATIONPROFILEBE;
import com.jasci.biz.AdminModule.be.LOCATIONPROFILESCREENLABELSBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILOCATIONPROFILESERVICE 
{

	/**
	 * Created on:Dec 28 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get General code list based on tenant,company,application,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
/*	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION;
*/
	/**
	 * Created on:Dec 28 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get FulfillmentCenter list based on tenant.
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * @param string 
	 * 
	 */
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentCenter(String tenant, String Company,String PageName) throws  JASCIEXCEPTION;
	
	/**
	 * Created on:Dec 30 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to add record of LocationProfile.
	 * Input parameter: String
	 * Return Type :Boolean
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	public Boolean addLocationProfile(LOCATIONPROFILEBE objLocationProfileBe) throws JASCIEXCEPTION;
	

	/**
	 * Created on:Dec 30 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to edit a record for LocationProfile.
	 * Input parameter: LOCATIONPROFILEBE
	 * Return Type :Boolean
	 * 
	 */
	public Boolean editLocationProfile(LOCATIONPROFILEBE objLocationProfileBe) throws JASCIEXCEPTION;

	/**
	 * Created on:Dec 30 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to delete record of LocationProfile.
	 * Input parameter: String
	 * Return Type :Boolean
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	public Boolean DeleteLocationProfile(String locationProfile,
			String profileGroup, String fulfillmentCenter) throws JASCIEXCEPTION;

	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to get records of LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	public List<LOCATIONPROFILEBE> GetLocationProfileList(
			String strLocationProfile, String strProfileGroup,
			String strPartOfDescription) throws JASCIEXCEPTION;
	
	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to get records of LocationProfileReassign.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	public List<LOCATIONPROFILEBE> GetLocationProfileListReAssign(
			 String strProfileGroup,
			String strFulfillmentcenter,String strLocationProfile) throws JASCIEXCEPTION;
	/**
	 * Created on:Jan 05 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to get record of LocationProfile for updation.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	public LOCATIONPROFILEBE editLocationProfile(String locationProfile,String profileGroup, String fulfillmentCenter)throws JASCIEXCEPTION;
	
	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get list of distinct LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * 
	 */
	public List<String> GetDistinctLocationProfile() throws JASCIEXCEPTION ;
	/**
	 * Created on:Jan 5 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get screen labels.
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILESCREENLABELSBE
	 * 
	 */

	public LOCATIONPROFILESCREENLABELSBE getScreenLabels(String currentLanguage)throws JASCIEXCEPTION ;
	
	
}
