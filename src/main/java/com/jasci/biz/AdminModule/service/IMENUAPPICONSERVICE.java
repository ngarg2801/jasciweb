
/**

Date Developed  dec 14 2014
Created By "Rahul Kumar"
Description  Provide service layer between controller and Rest full service  layer.
 */

package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.MENUAPPICONSCREENBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.common.utilbe.MENUAPPICONBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface IMENUAPPICONSERVICE {
	
	
	
	/**
	 * @Description set screen label language
	 * @author Rahul Kumar
	 * @Date Dec 12, 2014 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public MENUAPPICONSCREENBE setScreenLanguage( String StrScreenName,String StrLanguage)throws JASCIEXCEPTION;

	
	/**
	 * @Description add and update an app icon
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param objMenuAppiconsbe
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */
	public Boolean addOrUpdateMenuAppIcon(MENUAPPICONBE objMenuAppiconsbe) throws JASCIEXCEPTION;

	
	/**
	 * @Description get app icon list
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param Tenant
	 * @param Company
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<MENUAPPICONS>
	 *
	 */
	public List<MENUAPPICONS> getManuAppIconList(String Tenant,String Company)throws JASCIEXCEPTION;
	
	
	/**
	 * @Description get application list from general code
	 * @author Rahul Kumar
	 * @Date Dec 16, 2014 
	 * @param GeneralCodeID
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<GENERALCODES>
	 */
	public List<GENERALCODES> getApplicationList(String GeneralCodeID)throws JASCIEXCEPTION;

	
	
	/**
	 * @Description get app icon list by app icon name
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014 
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<MENUAPPICONS>
	 */
	public List<MENUAPPICONS> getMenuAppIconListByApplicationAppIcon(String Application,String AppIcon)throws JASCIEXCEPTION;
	
	
	/**
	 * @Description delete menu app icon 
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014 
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * Boolean
	 */
    public Boolean deleteMenuAppIcon(String Application,String AppIcon) throws JASCIEXCEPTION;
    
    
    /**
     * @Description get app icon list by its part name
     * @author Rahul Kumar
     * @Date Dec 17, 2014 
     * @param AppIconName
     * @param AppIconPart
     * @return
     * @throws JASCIEXCEPTION
     * List<MENUAPPICONS>
     */
    public List<MENUAPPICONS> getAppIconListByPart(String AppIconName,String AppIconPart)throws JASCIEXCEPTION;
    
    
    /**
	 * @Description get team member name
	 * @author Rahul Kumar
	 * @date 26 dec 2014
	 * @param Tenant
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
		
	public String getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION ;	

		
}
