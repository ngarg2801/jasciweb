/**

Date Developed  dec 22 2014
Created By "Rahul Kumar"
Description  Provide service layer between controller and Rest full service  layer.
 */


package com.jasci.biz.AdminModule.service;

import java.util.List;
import com.jasci.biz.AdminModule.be.COMPANYSCREENBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.common.utilbe.COMPANIESBE;
import com.jasci.exception.JASCIEXCEPTION;


public interface ICOMPANYMAINTENANCESERVICE {
	
	
	
	/**
	 * @Description set screen label as team member language 
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * COMPANYSCREENBE
	 */
	public COMPANYSCREENBE setScreenLanguage( String StrScreenName,String StrLanguage)throws JASCIEXCEPTION;
	
	
	/**
	 * @Description get all companies based on tenant from companies table 
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param Tenant
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIESBE>
	 */
	List<COMPANIESBE> getAllCompanies(String Tenant) throws JASCIEXCEPTION;

	

	/**
	 * @Description get company by company part name from companies table
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param CompanyPartName
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIESBE>
	 */
	List<COMPANIESBE> getCompanyByPartName(String CompanyPartName) throws JASCIEXCEPTION;
	
	
/**
 * @Description get general code by general code id from general code table
 * @author Rahul Kumar
 * @Date Dec 24, 2014 
 * @param Tenant
 * @param Company
 * @param GeneralCodeId
 * @return
 * @throws JASCIEXCEPTION
 * List<GENERALCODES>
 */
	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws JASCIEXCEPTION;

	
	/**
	 * 
	 * @Description : get country code for check code 840
	 * @author Rahul Kumar
	 * @Date Dec 25, 2014 
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type String
	 */
	public String getCountryCode() throws JASCIEXCEPTION;
	
	
	
	/**
	 * 
	 * @Description add or update company into companies table 
	 * @author Rahul Kumar
	 * @Date Dec 26, 2014 
	 * @param objCompaniesbe
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type Boolean
	 */
	public Boolean addOrUpdateCompany(COMPANIESBE objCompaniesbe) throws JASCIEXCEPTION;

	
	/**
	 * 
	 * @Description get company by its id and tenant from companies table 
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type COMPANIESBE
	 */
	public COMPANIESBE getCompanyById(String Tenant,String CompanyId) throws JASCIEXCEPTION;
	
	
	/**
	 * 
	 * @Description  Delete existing company from companies table
	 * @author Rahul Kumar
	 * @Date Dec 29, 2014 
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type Boolean
	 */
	public Boolean deleteCompany(String Tenant,String CompanyId) throws JASCIEXCEPTION;
	
}
