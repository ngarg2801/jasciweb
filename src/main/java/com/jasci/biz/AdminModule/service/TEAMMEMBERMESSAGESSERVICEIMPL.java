/**

Date Developed  Nov 16 2014
Created by:Diksha Gupta
Description TEAMMEMBERMESSAGESSERVICEIMPL implementation of ITEAMMEMBERMESSAGESSERVICE
*/

package com.jasci.biz.AdminModule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.RESTTEAMMEMBERMESSAGES;
import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESBE;
import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESSCREENBE;
import com.jasci.biz.AdminModule.dao.ITEAMMEMBERMESSAGESDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class TEAMMEMBERMESSAGESSERVICEIMPL implements ITEAMMEMBERMESSAGESSERVICE
{
	 @Autowired
	 private ITEAMMEMBERMESSAGESDAO ObjectTeamMemberMessagesDao;
	 @Autowired
	 private RESTTEAMMEMBERMESSAGES ObjectRestTeamMemberMessages;
	 
	 
	 
	 /**
		 *Description This function is designed to add messages into  TeamMemberMessages table
		 *Input parameter objTeamMemberMessagesbe
		 *Return Type Boolean
		 *Created By "Diksha Gupta"
		 *Created Date "nov 22 2014" 
		 * */
	public Boolean InsertRow(TEAMMEMBERMESSAGESBE objTeamMemberMessagesbe, String [] StrTeammemberList) throws JASCIEXCEPTION 
	{
		return ObjectRestTeamMemberMessages.InsertRow(objTeamMemberMessagesbe,StrTeammemberList);

		
	}
	
	
	
	
	/**
	 *Description This function design to get TeamMember data from TeamMember table
	 *Input parameter TeamMemberName,PartOfTeamMember,StrDepartment,StrShift,Tenant
	 *Return Type List<TEAMMEMBERSBE>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 25 2014" 
	 * */
	
	
	public List<TEAMMEMBERSBE> getTeamMemberList(String strTeamMember, String strPartTeamMember, String strDepartment, String strShift, String Tenant,String StrCompany) throws JASCIEXCEPTION
	{
		
		
		List<TEAMMEMBERSBE> ObjListTeamMembersBe=ObjectRestTeamMemberMessages.getTeamMemberList(strTeamMember, strPartTeamMember,strDepartment,strShift,Tenant,StrCompany);

		
		return ObjListTeamMembersBe;
	}


	/**
	 *Description This function design to get TeamMembermessages 
	 *Input parameter TeamMember
	 *Return Type List<TEAMMEMBERMESSAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	

	public List<TEAMMEMBERMESSAGES> getMessages(String StrTeamMember) throws JASCIEXCEPTION 
	{
		List<TEAMMEMBERMESSAGES> objListTeamMemberMessages=ObjectRestTeamMemberMessages.getMessages(StrTeamMember);

		
		return objListTeamMemberMessages;
	}

	

	/**
	 *Description This function design to get TeamMembermessages 
	 *Input parameter StrTeamMember
	 *Return Type List<TEAMMEMBERMESSAGESBE>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	
	public List<TEAMMEMBERMESSAGESBE> getMessagesList(String StrTeamMember) throws JASCIEXCEPTION 
	{
		List<TEAMMEMBERMESSAGESBE> objListTeamMemberMessagesBe=ObjectRestTeamMemberMessages.getMessagesList(StrTeamMember);

		
		return objListTeamMemberMessagesBe;
	}

	
	/**
	 *Description This function design to delete TeamMembermessages 
	 *Input parameter Tenant,teamMember,company,ticketTape
	 *Return Type Boolean
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	
	public Boolean DeleteMessages(String tenant, String teamMember, String company,String MessageNumber) throws JASCIEXCEPTION 
    {
		 return ObjectRestTeamMemberMessages.DeleteMessages(tenant,teamMember,company,MessageNumber);


		
	}


    /**
	 *Description This function design to getMessages to edit TeamMembermessages 
	 *Input parameter Tenant,teamMember,company
	 *Return Type Boolean
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	
	public List<TEAMMEMBERMESSAGESBE> getMessagesToEdit(String tenant, String teamMember, String company, String messagenumber) throws JASCIEXCEPTION 
	{		
		
		 List<TEAMMEMBERMESSAGESBE> ObjListTeamMemeberMessages= ObjectRestTeamMemberMessages.getMessagesToEdit(tenant,teamMember,company,messagenumber);
		return ObjListTeamMemeberMessages;
		
		
	}


	
	 /**
		 *Description This function design to edit TeamMembermessages 
		 *Input parameter objTeamMemberMessagesBe
		 *Return Type Boolean
		 *Created By "Diksha Gupta"
		 *Created Date "nov 19 2014" 
		 * */
	
	public Boolean UpdateMessages(TEAMMEMBERMESSAGESBE objTeamMemberMessagesBe) throws JASCIEXCEPTION 
	{
		return ObjectRestTeamMemberMessages.UpdateMessages(objTeamMemberMessagesBe);

			
	}

	 
	
	/**
		 *Description This function design to get screen labels
		 *Input parameter Team_Member_Language
		 *Return Type TEAMMEMBERMESSAGESSCREENBE
		 *Created By "Diksha Gupta"
		 *Created Date "nov 28 2014" 
		 * */
	
	

	public TEAMMEMBERMESSAGESSCREENBE getScreenLabels(String Team_Member_Language) throws JASCIEXCEPTION 
	{
		return ObjectRestTeamMemberMessages.GetScreenLabels(Team_Member_Language);
	}
	
	/**
	 * Created on:Nov 28 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get General code list based on tenant,company,application,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	@Transactional
	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION {


		return ObjectRestTeamMemberMessages.getGeneralCode(Tenant, Company, GeneralCodeId);
	}
	
	/**
	 * 
	 * @author Diksha Gupta
	 * @Date Dec 18, 2014
	 * @param LANGUAGETRANSLATIONBE
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */ 
  @Transactional
	public String getTeamMemberName(String Teammember)
	{           
   
	
 return ObjectRestTeamMemberMessages.getTeamMemberName(Teammember);
	
	}
	

}
	 
	
	