
/*

Description It is an Interface used to declare the function of implemented into MENUMESSAGESERVICEIMPL.
Created By Aakash Bishnoi 
Created On Dec 15, 2014
*/
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.MENUMESSAGEBE;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.MENUMESSAGESBEAN;

import com.jasci.exception.JASCIEXCEPTION;

public interface IMENUMESSAGESERVICE {

	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Dec 15, 2014
	 * Discription This is used to get the list of MenuMessages on the behalf of Type from MenuMessage Table
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrStatus
	 * @throws JASCIEXCEPTION
	 * @Return List<MENUMESSAGESBEAN>
	 */
	 public List<MENUMESSAGESBEAN> getList(String StrTenant,String StrCompany,String StrStatus)throws JASCIEXCEPTION;
	 
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 17, 2014
	  * Description this is used to get the List of Team Member Company
	  * @param StrTenant
	  * @param StrTeamMember
	  * @return List<TEAMMEMBERCOMPANIESBEAN>
	  * @throws JASCIEXCEPTION
	  */
	 public List<COMPANIES>  getCompanyList(String StrTenant,String StrTeamMember) throws JASCIEXCEPTION;
		 
	 
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 16, 2014
	  * Description This is used to fetch the list of MenuMessages on the behalf of Input Parameter from MenuMessage Table
	  * @param StrTenant_ID
	  * @param StrCompany_ID
	  * @param StrStatus
	  * @return MENUMESSAGES
	  * @throws JASCIEXCEPTION
	  */
	 public MENUMESSAGESBEAN fetchMenuMessages(String StrTenant_ID,String StrCompany_ID,String StrStatus,String Strtype,String StrMessage_ID) throws JASCIEXCEPTION;
	 
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 16, 2014
	  * Description This is used to update menu messages
	  * @param ObjectMenuMessage
	  * @throws JASCIEXCEPTION
	  */
	 public String updateEntry(MENUMESSAGESBEAN ObjectMenuMessage) throws JASCIEXCEPTION ;
	 
	 
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 17, 2014
	  * Description This is used to Add menu messages
	  * @param ObjectMenuMessage
	  * @throws JASCIEXCEPTION
	  */
	 public String addEntry(MENUMESSAGESBEAN ObjectMenuMessage,String[] CompanyList) throws JASCIEXCEPTION ;
	 
	 
	 
	 
		
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 19, 2014
	  * Description this is used to delete the selected menu messages
	  * @param model
	  * @return Boolean
	  * @throws JASCIEXCEPTION
	  */
	 public Boolean deleteEntry(String Message_ID,String Tenant_ID,String Company_ID,String Status) throws JASCIEXCEPTION;



		/**
		 * @author Aakash Bishnoi
		 * @Date Dec 19, 2014
		 * Description this is used to set language and get screen labels
		 * @param StrLanguage
		 * @return MENUMESSAGEBE
		 * @throws JASCIEXCEPTION
		 */
	 public  MENUMESSAGEBE getMenuMessagesLabels(String StrLanguage) throws JASCIEXCEPTION;
}
