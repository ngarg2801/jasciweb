/**

Date Developed  Oct 15 2014
Description : Business Logic Unimplemented Method
Developed by: sarvendra tyagi
 */

package com.jasci.biz.AdminModule.service;


import java.util.List;

import com.jasci.biz.AdminModule.be.MENUPROFILEBEANPOJOBE;
import com.jasci.biz.AdminModule.be.MENUPROFILEMAINTENANCELABELBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEHEADER;
import com.jasci.exception.JASCIEXCEPTION;

public interface IMENUPROFILEMAINTENANCESERVICE {
	
	/** 
	 *  @description :This function is used for getting label of Menu Profile maintenance screen
	 * @param        :strMenuTypes
	 * @return       :objMENUPROFILEMAINTENANCELABELBE 
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Dec 26 2014
	 */
	public MENUPROFILEMAINTENANCELABELBE getLanguageLabel(String language)throws JASCIEXCEPTION;
	
	

	/** 
	 * @Description:This function is getting data from menu Profile table for edit or update
	 * @param      : strMenuProfile
	 * @return     :MENUPROFILEHEADER
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 27 2014
	 */
	public MENUPROFILEBEANPOJOBE getMenuProfileList(String strMenuProfile)throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description :This function is getting menu types from general code list
	 * @param 		:strMenuType
	 * @return      :List<GENERALCODES>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	public List<GENERALCODES> getMenuTypes(String strMenuType)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description: This Method is used for get list of menu profile from menu profile header table
	 * @param: searchFieldValue
	 * @param :serchField
	 * @return:List<MENUPROFILEHEADER>
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date  : Dec 28 2014
	 */
	public List<MENUPROFILEHEADER> getProfileHeaderList(String searchFieldValue,String serchField)throws JASCIEXCEPTION;
	
	/** 
	 * @Description :This function is getting menu App Icon from MenuApp Icon table
	 * @param 		:
	 * @return      :List<MENUAPPICONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	public List<MENUAPPICONS> getMenuAppIconList()throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This function is getting Menu Options from Menu Options table
	 * @param 		:
	 * @return      :List<MENUOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
//	public List<MENUOPTIONS> getMenuOptionList(String strApplication,MENUPROFILEMAINTENANCELABELBE objMenuprofilemaintenancelabelbe)throws JASCIEXCEPTION;
	
		
	

	/** 
	 * @Description :This Method is used for save or update Menu Profile List
	 * @param 		:MENUPROFILEBEANPOJOBE objMenuprofileheader String strJsonAssignOption
	 * @return      :List<MENUPROFILEHEADER>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	public Boolean SaveOrUpdate(MENUPROFILEBEANPOJOBE objMenuprofileheader, String strJsonAssignOption)throws JASCIEXCEPTION;

	
	/** 
	 * @Description :This Method is used for delete menu profile 
	 * @param 		:strMenuProfile
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 30 2014
	 */
	public Boolean deleteMenuProfile(String strMenuProfile,String strTenant)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This Method is used for get Team Member Name
	 * @param 		:tenant and TeammemberId
	 * @return      :strTeamMemberName
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 30 2014
	 */

	public String getTeamMemberName(String strTenant,String strTeamMember)throws JASCIEXCEPTION;
	
	
	
	
	/** 
	 * @Description :This Method is used for get Last Date and Last Team Member
	 * @param 		:tenant and TeammemberId
	 * @return      :strTeamMemberName
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby :Sarvendra tyagi
	 * @Date	    :Dec 30 2014
	 */
	public MENUPROFILEBEANPOJOBE getDateAndTeamMember(String strTenant,String strTeamMember)throws JASCIEXCEPTION;
	
	
	
	

	 /** 
		 * @Description :This Method is used for get assigned menu option in menuprofile Option table 
		 * @param 		:String Tenant, String strMenuProfile
		 * @return      :List<MENUOPTIONS>
		 * @throws 		:JASCIEXCEPTION
		 * @Developedby :Sarvendra tyagi
		 * @Date	    :Dec 31 2014
		 */
	public List<MENUOPTIONS> getAssignedMenurofile(String Tenant, String strMenuProfile) throws JASCIEXCEPTION;

	 
	 
	
	
}
