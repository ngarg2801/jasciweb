/**

Date Developed  Oct 15 2014
Description : Business Logic implemented Method for Security Authorization
Developed by: Deepak Sharma
 */

package com.jasci.biz.AdminModule.service;

import java.util.ArrayList;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.SECURITYAUTHORIZATIONSERVICESAPI;
import com.jasci.biz.AdminModule.be.SECURITYAUTHORIZATIONSCREENBE;
import com.jasci.biz.AdminModule.dao.SECUTITYAUTHORIZATIONDAO;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONS;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONSBE;
import com.jasci.biz.AdminModule.model.TEAMMEMBERBEAN;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.MAILCONFIGURATION;
import com.jasci.common.util.SENDEMAIL;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class SECURITYAUTHORIZATIONSERVICE {

	@Autowired
	private SECUTITYAUTHORIZATIONDAO ObjSecurityAuthorizationDAO;
	@Autowired
	private SECURITYAUTHORIZATIONSERVICESAPI ObjSequrityAuthorizationServicesAPI;

	/**
	 * 
	 * @param TeamMemberName
	 * @param PartOfTeamMember
	 * @return
	 * @throws DATABASENOTFOUNDEXCEPTION
	 * @throws INTERNETCONNECTIONERROREXCEPTION
	 * @throws DATABASEEXCEPTION
	 * Purpose:to get the team member
	 * CreatedBy:Vikas 
	 * CretaedOn:2014-11-25
	 * 
	 */
	@Transactional
	public List<TEAMMEMBERBEAN> getTeamMemberByName(String TeamMemberName,String PartOfTeamMember,String StrTenant,String StrFullFillmentCenter) throws JASCIEXCEPTION
	
	{


		// call getTeamMemberByName(TeamMemberName) to get Team Member from Team Members Table

		List<Object> ObjListTeamMembers=ObjSequrityAuthorizationServicesAPI.getTeamMemberByName(TeamMemberName,PartOfTeamMember,StrTenant,StrFullFillmentCenter);

		List<TEAMMEMBERBEAN> lstOfViewBean=convertPojoToViewBean(ObjListTeamMembers);



		return lstOfViewBean;

	}


	public List<TEAMMEMBERBEAN> convertPojoToViewBean(List<Object> subList)
	{
		List<TEAMMEMBERBEAN> teamMemberSubList = new ArrayList<TEAMMEMBERBEAN>();
		for (Object object : subList) {
			Object[] object_arr = (Object[]) object;
			TEAMMEMBERBEAN teamMemberBeanObj = new TEAMMEMBERBEAN();

			try{teamMemberBeanObj.setTenant(object_arr[GLOBALCONSTANT.INT_ZERO].toString());}catch(Exception e){teamMemberBeanObj.setTenant(GLOBALCONSTANT.BlankString);}
			try{teamMemberBeanObj.setTeamMember(object_arr[GLOBALCONSTANT.IntOne].toString());}catch(Exception e){teamMemberBeanObj.setTeamMember(GLOBALCONSTANT.BlankString);}


			try{teamMemberBeanObj.setLastName(object_arr[GLOBALCONSTANT.INT_TWO].toString());}catch(Exception e){teamMemberBeanObj.setLastName(GLOBALCONSTANT.BlankString);}
			try{teamMemberBeanObj.setFirstName(object_arr[GLOBALCONSTANT.INT_THREE].toString());}catch(Exception e){teamMemberBeanObj.setFirstName(GLOBALCONSTANT.BlankString);}

			try{teamMemberBeanObj.setMiddleName(object_arr[GLOBALCONSTANT.INT_FOUR].toString());}catch(Exception e){teamMemberBeanObj.setMiddleName(GLOBALCONSTANT.BlankString);}
			try{teamMemberBeanObj.setSeqStatus(object_arr[GLOBALCONSTANT.INT_SIX].toString());}catch(Exception e){teamMemberBeanObj.setSeqStatus(GLOBALCONSTANT.BlankString);}
			try{teamMemberBeanObj.setTeamMemberStatus(object_arr[GLOBALCONSTANT.INT_FIVE].toString());}catch(Exception e){teamMemberBeanObj.setTeamMemberStatus(GLOBALCONSTANT.BlankString);}
			try{
				if(!teamMemberBeanObj.getTeamMemberStatus().equalsIgnoreCase(GLOBALCONSTANT.BlankString))
				{
					if(teamMemberBeanObj.getTeamMemberStatus().trim().equalsIgnoreCase(GLOBALCONSTANT.Security_TeamMember_Active_Status))
					{
						teamMemberBeanObj.setTeamMemberStatus(GLOBALCONSTANT.Security_TeamMember_Active_Status_Text);
					}
					else if(teamMemberBeanObj.getTeamMemberStatus().trim().equalsIgnoreCase(GLOBALCONSTANT.Security_TeamMember_InActive_Status))
					{
						teamMemberBeanObj.setTeamMemberStatus(GLOBALCONSTANT.Security_TeamMember_InActive_Status_Text);
					}
					else if(teamMemberBeanObj.getTeamMemberStatus().trim().equalsIgnoreCase(GLOBALCONSTANT.Security_TeamMember_LeaveOfAbsence_Status))
					{
						teamMemberBeanObj.setTeamMemberStatus(GLOBALCONSTANT.Security_TeamMember_LeaveOfAbsence_Text);
					}
					else if(teamMemberBeanObj.getTeamMemberStatus().trim().equalsIgnoreCase(GLOBALCONSTANT.Security_TeamMember_Delete_Status))
					{
						teamMemberBeanObj.setTeamMemberStatus(GLOBALCONSTANT.Security_TeamMember_Delete_Status_Text);
					}
				}

			}catch (Exception e) {

			}

			if(teamMemberBeanObj.getMiddleName()!=null)
			{
				if(!(teamMemberBeanObj.getMiddleName().equalsIgnoreCase(GLOBALCONSTANT.BlankString)))
				{
					teamMemberBeanObj.setFullName(teamMemberBeanObj.getFirstName()+GLOBALCONSTANT.Single_Space+teamMemberBeanObj.getMiddleName()+GLOBALCONSTANT.Single_Space+teamMemberBeanObj.getLastName());
				}
				else
				{
					teamMemberBeanObj.setFullName(teamMemberBeanObj.getFirstName()+GLOBALCONSTANT.Single_Space+teamMemberBeanObj.getLastName());
				}
			}
			else
			{
				teamMemberBeanObj.setFullName(teamMemberBeanObj.getFirstName()+GLOBALCONSTANT.Single_Space+teamMemberBeanObj.getLastName());
			}

			teamMemberSubList.add(teamMemberBeanObj); 


		}


		return teamMemberSubList;



	}

	/**
	 * 
	 * @param StrTenant
	 * @param StrFullfillmentCenter
	 * @return
	 * @throws DATABASENOTFOUNDEXCEPTION
	 * @throws INTERNETCONNECTIONERROREXCEPTION
	 * @throws DATABASEEXCEPTION
	 * Purpose:to get all team members
	 * CreatedBy:Deepak 
	 * CretaedOn:2014-11-28
	 */
	public List<TEAMMEMBERBEAN> getAllTeamMemberData(String StrTenant,String StrFullfillmentCenter) throws JASCIEXCEPTION 
	{

		// call getAllTeamMember to get Team Member from Team Members Table

		List<Object> ObjListTeamMembers=ObjSequrityAuthorizationServicesAPI.getAllTeamMembers(StrTenant,StrFullfillmentCenter);
		List<TEAMMEMBERBEAN> lstOfViewBean=convertPojoToViewBean(ObjListTeamMembers);

		return lstOfViewBean;		
	}

	/**
	 * 
	 * @param StrTeamMember
	 * @param StrTenant
	 * @return
	 * @throws DATABASENOTFOUNDEXCEPTION
	 * @throws INTERNETCONNECTIONERROREXCEPTION
	 * @throws DATABASEEXCEPTION
	 * Purpose:to check the login user details
	 * CreatedBy:Deepak 
	 * CretaedOn:2014-11-29 
	 */
	public List<SECURITYAUTHORIZATIONS> getSecurityAuthorizationDataByTeamMember(String StrTeamMember,String StrTenant) throws JASCIEXCEPTION
	{
		// call getSecurityAuthorizationDataByTeamMeamberName to get SecurityAuthorization data based on a team member name

		List<SECURITYAUTHORIZATIONS> ObjListSecurityAuthorizations=ObjSequrityAuthorizationServicesAPI.getSecurityAuthorizationData(StrTenant, StrTeamMember);

		return ObjListSecurityAuthorizations;
	}

	/**
	 * 
	 * @param strCompany 
	 * @param strCommTenant 
	 * @return
	 * @throws DATABASENOTFOUNDEXCEPTION
	 * @throws INTERNETCONNECTIONERROREXCEPTION
	 * @throws DATABASEEXCEPTION
	 * Purpose:to get the security questions
	 * CreatedBy:Deepak
	 * CretaedOn:2014-11-29
	 * 
	 */
	public List<Object> getGeneralCodeForSecurityQuestion(String strCommTenant, String strCompany) throws JASCIEXCEPTION
	{
		// call getSecurityAuthorizationDataByTeamMeamberName to get SecurityAuthorization data based on a team member name

		List<Object> listGeneralCodes =ObjSequrityAuthorizationServicesAPI.getSequrityQuestions(GLOBALCONSTANT.GIC_SECURITYQUESTION,strCommTenant,strCompany);

		return listGeneralCodes;
	}	

	/**
	 * 
	 * @param StrTeamMemberID
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public String getTeamMemberName(String StrTeamMemberID) throws JASCIEXCEPTION
	{
		String TeamMemberName=GLOBALCONSTANT.BlankString;
		try{
		if(StrTeamMemberID.trim().length()>GLOBALCONSTANT.INT_ZERO)
		{
		List<Object> lstTeamMemberEmail=ObjSequrityAuthorizationServicesAPI.getTeamMemberEmailID(StrTeamMemberID);
		if(lstTeamMemberEmail.get(GLOBALCONSTANT.INT_ZERO)!=null)
		{
			Object []Team_Member_Fields = (Object[]) lstTeamMemberEmail.get(GLOBALCONSTANT.INT_ZERO);
			
			String TeamMemberFirstName=Team_Member_Fields[GLOBALCONSTANT.IntOne].toString();
			String TeamMemberLastName=Team_Member_Fields[GLOBALCONSTANT.INT_TWO].toString();
			TeamMemberName = TeamMemberFirstName+ GLOBALCONSTANT.Single_Space+TeamMemberLastName;
			
		}
		}
		}
		catch(Exception objException)
		{
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return TeamMemberName;
	}
	
	
	/**
	 * 
	 * @param objUpdateSecurityAuth
	 * @return
	 * @throws INTERNETCONNECTIONERROREXCEPTION
	 * @throws DATABASENOTFOUNDEXCEPTION
	 * @throws DATABASEEXCEPTION
	 * Purpose:to add or update team member in security authorization
	 * CreatedBy:Deepak
	 * CretaedOn:2014-11-30
	 * 
	 */

	public String addOrUpdateSequrityAuthorization(SECURITYAUTHORIZATIONSBE objUpdateSecurityAuth) throws JASCIEXCEPTION{
		String StrStatus=GLOBALCONSTANT.BlankString;
		
		StrStatus=ObjSequrityAuthorizationServicesAPI.addOrUpdateSequrityAuthorization(objUpdateSecurityAuth);
		if(StrStatus.equalsIgnoreCase(GLOBALCONSTANT.Security_WebServiceSuccessStatus))
		{
		
		List<Object> lstTeamMemberEmail=ObjSequrityAuthorizationServicesAPI.getTeamMemberEmailID(objUpdateSecurityAuth.getTeamMember());
		if(lstTeamMemberEmail.get(GLOBALCONSTANT.INT_ZERO)!=null)
		{
			Object []Team_Member_Fields = (Object[]) lstTeamMemberEmail.get(GLOBALCONSTANT.INT_ZERO);
			
			String EmailID=Team_Member_Fields[GLOBALCONSTANT.INT_ZERO].toString();
			String TeamMemberFirstName=Team_Member_Fields[GLOBALCONSTANT.IntOne].toString();
			String TeamMemberLastName=Team_Member_Fields[GLOBALCONSTANT.INT_TWO].toString();
			String TeamMemberName = TeamMemberFirstName+ GLOBALCONSTANT.Single_Space+TeamMemberLastName;
			
			if(EmailID!=null)
			{
				String SecurityOfficerContactNumber=GLOBALCONSTANT.BlankString;
				String SecurityOfficerEmailId=GLOBALCONSTANT.BlankString;  
				
				CONFIGURATIONEFILE ObjConfigurationFile=new CONFIGURATIONEFILE();
				SecurityOfficerContactNumber = ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.SecurityOfficerContactNumber);
				SecurityOfficerEmailId = ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.SecurityOfficerEmailId);
				
				String StrMessage=GLOBALCONSTANT.Security_MailPart1+TeamMemberName+GLOBALCONSTANT.Security_MailPart2+objUpdateSecurityAuth.getUserId()+GLOBALCONSTANT.Security_MailPart3+objUpdateSecurityAuth.getPassword()+GLOBALCONSTANT.Security_MailPart4+SecurityOfficerContactNumber+GLOBALCONSTANT.Security_MailPart5+SecurityOfficerEmailId+GLOBALCONSTANT.Security_MailPart6;
				//System.out.println("starting sending mail");
				SENDEMAIL.sendEmail(EmailID.trim(),null, null, GLOBALCONSTANT.Security_MailSubject, StrMessage,MAILCONFIGURATION.FROM );
				// System.out.println("Mail Sent");
			}
		}
		}
		return StrStatus;
	}


	/**
	 * 
	 * @param StrLanguageCode
	 * @return
	 * @throws INTERNETCONNECTIONERROREXCEPTION
	 * @throws DATABASENOTFOUNDEXCEPTION
	 * @throws DATABASEEXCEPTION
	 */
	public SECURITYAUTHORIZATIONSCREENBE getSecuritySetUpScreenLabel(String StrLanguageCode) throws JASCIEXCEPTION{
		SECURITYAUTHORIZATIONSCREENBE objLabels=ObjSequrityAuthorizationServicesAPI.getSecuritySetUpScreenLabel(StrLanguageCode);		
		return objLabels;
	}
}
