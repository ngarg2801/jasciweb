/**

Date Developed: Dec 15 2014
Created By: Sarvendra Tyagi
Description: Provide service layer between controller and dao layer.
 */


package com.jasci.biz.AdminModule.service;

import java.util.List;
import java.util.Map;


import com.jasci.biz.AdminModule.be.MENUOPTIONLABELBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.exception.JASCIEXCEPTION;



public interface IMENUOPTIONSERVICE {
	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Team member list based on given team member name
	 * Input parameter: String SearchFieldValue,String SearchField
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	public List<MENUOPTIONS> getMenuOptionsList(String SearchFieldValue,String SearchField) throws JASCIEXCEPTION;
	
	
	/**
	 * @description:This function is used for delete the menu option from menu option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 18 2014
	 */
	public Boolean deleteMenuOption(String strMenuOption)throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description:This function is getting all data from menuoption table 
	 * @param      : strMenuOption
	 * @return     :List<MENUOPTIONS>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 18 2014
	 */
	public List<MENUOPTIONS> getMenuOptionDisplayAll() throws JASCIEXCEPTION;
	

	
	
	/**
	 * @description:This function is used for delete the menu option from menu option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 18 2014
	 */
	
	public List<MENUOPTIONS> getMenuOptionData(String strMenuOption) throws JASCIEXCEPTION;
	
	
	/** 
	 * @description : This function is used for getting value of 
	 * @param		:strApplication
	 * @return		:Map<String,List<String>>
	 * @throws		:JASCIEXCEPTION
	 *@Developed by : Sarvendra Tyagi
	 * @Date		: Dec 19 2014
	 */
	public Map<String,List<String>> getApplicationAndSubApplication(String strApplication)throws JASCIEXCEPTION;
	
	
	/** 
	 *  @description :This function to get the list of menu type from General code table
	 * @param        :strMenuTypes
	 * @return       :List<Object>
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 19 2014
	 */
	public List<GENERALCODES> getMenuType(String strMenuTypes)throws JASCIEXCEPTION;
	
	
	/** 
	 *  @description :This function is used for updating table row of menu option
	 * @param        :strMenuTypes
	 * @return       :objMenuOption
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 20 2014
	 */
	public MENUOPTIONS update(MENUOPTIONS objMenuoptions)throws JASCIEXCEPTION;
	
	
	/** 
	 *  @description :This function is used for getting label of screen
	 * @param        :strMenuTypes
	 * @return       :objMENUOPTIONLABELBE 
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 23 2014
	 */
	public MENUOPTIONLABELBE getScreenLabel(String language)throws JASCIEXCEPTION;
	


}
