/**
 *     
 * @author Shailendra Rajput

 * @Date 8 October,2015 
 * @Description Its a interface where we define the function that implement in class of DisplayProductLocationServiceImplement
 */
package com.jasci.biz.AdminModule.service;

import com.jasci.biz.AdminModule.be.PRODUCTSBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface IDISPLAYPRODUCTLOCATIONSERVICE {
	public  PRODUCTSBE getDataFromProductsWithPicture(String Tenant_ID,String Company_ID,String Product,String Quality) throws JASCIEXCEPTION ;
	
}
