/**

Date Developed  Nov 16 2014
Created by: Diksha Gupta
Description ITEAMMEMBERMESSAGESSERVICE
*/

package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESBE;
import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESSCREENBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface ITEAMMEMBERMESSAGESSERVICE 
{
    /** To add a record to  Teammember Messages.*/
	Boolean InsertRow(TEAMMEMBERMESSAGESBE objTeamMemberMessagesbe,String [] StrTeamMemberList) throws JASCIEXCEPTION;

	/** To get Teammember Messages.*/
	List<TEAMMEMBERMESSAGES> getMessages(String teamMember) throws JASCIEXCEPTION;
	/** To delete a  record to  Teammember Messages.*/
 Boolean DeleteMessages(String tenant, String teamMember, String company,
			String ticketTape) throws JASCIEXCEPTION;

 /** To get a record from  Teammember Messages to edit.*/
List<TEAMMEMBERMESSAGESBE> getMessagesToEdit(String tenant, String teamMember, String company, String MessageNumber) throws JASCIEXCEPTION;

/** To update the record to  Teammember Messages.*/
Boolean UpdateMessages(TEAMMEMBERMESSAGESBE objTeamMemberMessagesBe) throws JASCIEXCEPTION;
/** To get the list of Teammembers.*/
List<TEAMMEMBERSBE> getTeamMemberList(String strTeamMember,
		String strPartTeamMember, String strDepartment, String strShift,
		String tenant, String strCompany) throws JASCIEXCEPTION;
/** To get list of  Teammember Messages.*/
 List<TEAMMEMBERMESSAGESBE> getMessagesList(String StrTeamMember)  throws JASCIEXCEPTION;
 /** To get screen labels for screens Teammember Messages.*/
TEAMMEMBERMESSAGESSCREENBE getScreenLabels(String team_Member_Language) throws JASCIEXCEPTION;
/**  This function is get General code list based on tenant,company,generalCodeId*/
List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws JASCIEXCEPTION;

/**
 * 
 * @author Diksha Gupta
 * @Date Dec 18, 2014
 * @param LANGUAGETRANSLATIONBE
 * @throws JASCIEXCEPTION
 * @Return Boolean
 *
 */ 

public String getTeamMemberName(String Teammember);

	

}
