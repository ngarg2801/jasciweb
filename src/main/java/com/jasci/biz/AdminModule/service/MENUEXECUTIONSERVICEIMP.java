/*

Date Developed  Nov 24 2014
Description  Provide service layer between MenuExecutionController and menuExecutiondao
Created By Sarvendra Tyagi
 */


package com.jasci.biz.AdminModule.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.MENUEXECUTIONLABELBE;
import com.jasci.biz.AdminModule.be.MENUEXECUTIONLISTBE;
import com.jasci.biz.AdminModule.be.SUBMENULISTBE;
import com.jasci.biz.AdminModule.dao.MENUEXECUTIONDAOIMPL;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIESBEAN;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Service
public class MENUEXECUTIONSERVICEIMP{

	
	@Autowired
	private MENUEXECUTIONDAOIMPL objMenuExecutionDAOIMPL;
	
	@Autowired
	LANGUANGELABELSAPI objLanguageServiceApi;

	/** 
	 * Description: This method is used for getting company list  
	 * @param: ObjCommonSession
	 * @return: List<TEAMMEMBERCOMPANIESBEAN>
	 * @throws: JASCIEXCEPTION
	 * developed by: Sarvendra Tyagi
	 * Date: Nov/22/2014
	 */

	@Transactional
	public List<TEAMMEMBERCOMPANIESBEAN> getListCompanies(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION{



		List<TEAMMEMBERCOMPANIESBEAN> objListTeammembercompanies=null;
		try{
			
			objListTeammembercompanies=objMenuExecutionDAOIMPL.GetDataFromTeamMemberCompanies(ObjCommonSession);
			
		}catch(JASCIEXCEPTION objJasciexception){
			
			objJasciexception.getMessage();
		}

		return objListTeammembercompanies;

	}



	/**  
	 * Description: This method is used for getting assigned menuprofile and return MENUEXECUTIONLISTBE
	 * developed by: Sarcvendra Tyagi
	 * @param: ObjCommonSession
	 * @return: List<MENUEXECUTIONLISTBE>
	 * @throws: JASCIEXCEPTION
	 * Date: nov 28 2014
	 */
	@Transactional
	public List<MENUEXECUTIONLISTBE> getAssignedMenuProfileList(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION
			 {
		

		List<MENUEXECUTIONLISTBE> objListMenuExecution=null;
		try{
			
			objListMenuExecution=objMenuExecutionDAOIMPL.GetAssignedMenuProfileList(ObjCommonSession);
			
		}catch(JASCIEXCEPTION objJasciexception){
			
			objJasciexception.getMessage();
		}



		return objListMenuExecution;
	}
	
	
	
	/**
	 *  Description: This function is used to getting sub menu list
	 * developed by : Sarvendra Tyagi
	 * @param ObjCommonSession
	 * @return: objSubMEnuListBE
	 * @throws JASCIEXCEPTION
	 * Date: Dec 2 2014
	 */
	
	@Transactional
	public SUBMENULISTBE getSubMenus(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION{
		
		SUBMENULISTBE objSubmenulistbe=null;
		
		try{
		
			
			objSubmenulistbe=objMenuExecutionDAOIMPL.GetSubMenuList(ObjCommonSession);
		
			getMenu(ObjCommonSession);
			
			
		}catch(JASCIEXCEPTION objJasciexception){
			
			objJasciexception.getMessage();
		}
		return objSubmenulistbe;
	}


	
	@Transactional
	public MENUEXECUTIONLABELBE getLanguageLabel(String language) throws JASCIEXCEPTION {
		List<LANGUAGES> objLanguagesList=null;
		
		MENUEXECUTIONLABELBE objMenuexecutionlabelbe=null;
		
		try{
			
			objLanguagesList=objLanguageServiceApi.GetScreenLabels(LANGUANGELABELSAPI.strMenuExecutionModule, language);
			
			objMenuexecutionlabelbe=new MENUEXECUTIONLABELBE();
			for(LANGUAGES objLanguage:objLanguagesList){
				
				if(LANGUANGELABELSAPI.KP_Name.equalsIgnoreCase(objLanguage.getId().getKeyPhrase())){
					
					objMenuexecutionlabelbe.setLblName(objLanguage.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_APPLICATIONS.equalsIgnoreCase(objLanguage.getId().getKeyPhrase())){
					
					objMenuexecutionlabelbe.setLblAPPLICATIONS(objLanguage.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_MESSAGES.equalsIgnoreCase(objLanguage.getId().getKeyPhrase())){
					
					objMenuexecutionlabelbe.setLblMESSAGES(objLanguage.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Team_Member.equalsIgnoreCase(objLanguage.getId().getKeyPhrase())){
					
					objMenuexecutionlabelbe.setLblTeam_Member(objLanguage.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Company_ID.equalsIgnoreCase(objLanguage.getId().getKeyPhrase())){
					
					objMenuexecutionlabelbe.setLblCompany_ID(objLanguage.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Select_Company_to_Work_With.equalsIgnoreCase(objLanguage.getId().getKeyPhrase())){
					
					objMenuexecutionlabelbe.setLblCompaySelectWorkWith(objLanguage.getTranslation());
					
				}
				
			}
			
		}catch(Exception objException){
			
			new JASCIEXCEPTION(objException.getMessage());
		}
		return objMenuexecutionlabelbe;
	}
	

	@Transactional
	public Map<String,Map<String,Map<String,String>>> getMenu(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION
			 {
			
		Map<String,Map<String,Map<String,String>>> setMenu=new HashMap<String,Map<String,Map<String,String>>>();
		List<MENUEXECUTIONLISTBE> objListMenuExecution=null;
		try{
			
			objListMenuExecution=objMenuExecutionDAOIMPL.GetAssignedMenuProfileList(ObjCommonSession);
			for(int ProfileIndex=0;ProfileIndex<objListMenuExecution.size();ProfileIndex++){
			
				ObjCommonSession.setAuthority_Profile(objListMenuExecution.get(ProfileIndex).getMenuProfile());
				SUBMENULISTBE objSubmenulistbe=objMenuExecutionDAOIMPL.GetSubMenuList(ObjCommonSession);
				try{
				setMenu.put(objListMenuExecution.get(ProfileIndex).getMenuProfile(), objSubmenulistbe.getHmListSubMenu());
				}catch(NullPointerException obj_ex){}
				catch(Exception obj_ex){}
			}
		}catch(JASCIEXCEPTION objJasciexception){
			
			objJasciexception.getMessage();
		}

		return setMenu;
	}
	

}
