/*

Created By Aakash Bishnoi
Created Date Oct 29 2014
Description   InfoHelps service implementation
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.INFOHELPSSERVICEAPI;
import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.INFOHELPSBE;
import com.jasci.biz.AdminModule.dao.IINFOHELPSDAO;
import com.jasci.biz.AdminModule.model.INFOHELPS;
import com.jasci.biz.AdminModule.model.INFOHELPSBEAN;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.exception.JASCIEXCEPTION;


@Service
public class INFOHELPSSERVICEIMPL implements IINFOHELPSSERVICE{

	
	 @Autowired
	 private INFOHELPSSERVICEAPI ObjectRestInfoHelp;
	 @Autowired
	 LANGUANGELABELSAPI ObjLanguageLabelApi;
	 
	 @Autowired
	 private IINFOHELPSDAO ObjectInfohelpDao;

	 //Calling restfull service InfoHelpListAll 
	 @Transactional
	 public List<INFOHELPSBEAN> getInfoHelpList(String StrInfoHelp,String StrPartoftheDescription,String StrInfoHelpType) throws JASCIEXCEPTION{
		 return ObjectRestInfoHelp.getInfoHelpList(StrInfoHelp,StrPartoftheDescription,StrInfoHelpType);
	 }
	 
	/* //used to Show drop down of InfoHelp Type form GeneralCode
	 @Transactional
	 public List<GENERALCODES> SelectInfoHelpType(String StrTenant,String StrCompany,String GeneralCodeID) throws JASCIEXCEPTION{
		 
		 return ObjectInfohelpDao.SelectInfoHelpType(StrTenant,StrCompany,GeneralCodeID);
	 }
	 
	 //used to Show drop down of Language form GeneralCode
	 @Transactional
	 public List<GENERALCODES> SelectLanguage(String StrTenant,String StrCompany,String GeneralCodeID) throws JASCIEXCEPTION{
		 return ObjectInfohelpDao.SelectLanguage(StrTenant,StrCompany,GeneralCodeID);
	 }*/
	 
	//Used to insert data into InfoHelp table
	 @Transactional
	 public String insertInfoHelps(INFOHELPS ObjectInfohelps) throws JASCIEXCEPTION{
		 return ObjectInfohelpDao.insertInfoHelps(ObjectInfohelps);
	 }
	 
	 
	//get the record from infoHelp on behalf of InfoHelp and Language
	 @Transactional
	 public INFOHELPSBEAN fetchInfoHelp(String InfoHelp,String Language) throws JASCIEXCEPTION{
		 return ObjectRestInfoHelp.fetchInfoHelp(InfoHelp,Language);
	 }
	 
	 
	 //Used to Update InfoHelp record
	 @Transactional
	 public String updateInfoHelps(INFOHELPS ObjectInfohelps) throws JASCIEXCEPTION {
		 return ObjectInfohelpDao.updateInfoHelps(ObjectInfohelps);
	 }
	 
	//Used to Delete Record On behalf of Language and InfoHelp
	 @Transactional
	 public Boolean deleteInfoHelps(String InfoHelp,String Language) throws JASCIEXCEPTION{
		 return ObjectRestInfoHelp.deleteInfoHelps(InfoHelp,Language);
	 }
	 
	 //It is used to Get the label Of InfoHelps's All Screen and Error Message 
	 @Transactional
	 public INFOHELPSBE getInfoHelpAssignmentSearchLabels(String StrLanguage) throws JASCIEXCEPTION{
		
			List<LANGUAGES> ObjListLanguages=ObjLanguageLabelApi.GetScreenLabels(LANGUANGELABELSAPI.StrInfoHelps, StrLanguage);
			//List<LANGUAGES> ObjListLanguages=ObjectRestInfoHelp.getInfoHelpAssignmentSearchLabels(StrLanguage);
			INFOHELPSBE ObjInfoHelpsBe=new INFOHELPSBE();  
		
				for (LANGUAGES ObjLanguages : ObjListLanguages) 
				{
				String StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
					
					if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Info_Help))
					{
						ObjInfoHelpsBe.setInfohelps_Enter_Info_Help(ObjLanguages.getTranslation());
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Info_Help(ObjLanguages.getTranslation());
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Info_Help(ObjLanguages.getTranslation());
					}
					if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Language))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Language(ObjLanguages.getTranslation());
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Language(ObjLanguages.getTranslation());
					}
					
					if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Info_Help_Type))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Info_Help_Type(ObjLanguages.getTranslation());
						ObjInfoHelpsBe.setInfohelp_Info_Help_Type(ObjLanguages.getTranslation());
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Info_Help_Type(ObjLanguages.getTranslation());
						ObjInfoHelpsBe.setInfohelp_Info_Help_Type(ObjLanguages.getTranslation());
					}
					if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Select(ObjLanguages.getTranslation());
						ObjInfoHelpsBe.setInfohelp_Select(ObjLanguages.getTranslation());
					}
					
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_the_Description))
					{
						ObjInfoHelpsBe.setInfohelp_Part_of_the_Description(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Info_Help_Assignment_Search))
					{
						ObjInfoHelpsBe.setInfohelp_Info_Help_Assignment_Search(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
					{
						ObjInfoHelpsBe.setInfohelp_New(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
					{
						ObjInfoHelpsBe.setInfohelp_DisplayAll(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Description))
					{
						ObjInfoHelpsBe.setInfoHelps_Invalid_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Info_Help))
					{
						ObjInfoHelpsBe.setInfoHelps_Please_enter_Info_Help(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_Info_Help_Type))
					{
						ObjInfoHelpsBe.setInfoHelps_Please_select_Info_Help_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Info_Help_Type))
					{
						ObjInfoHelpsBe.setInfoHelps_Invalid_Info_Help_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Info_Help))
					{
						ObjInfoHelpsBe.setInfoHelps_Invalid_Info_Help(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Part_of_the_Description))
					{
						ObjInfoHelpsBe.setInfoHelps_Please_enter_Part_of_the_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Confirg_Update(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Confirm_Success(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Infohelps_already_used))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Infohelps_already_used(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Mandotry_field_can_not_blank(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Execution_Path))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Execution_Path(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Long))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Description_Long(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Description_Short(ObjLanguages.getTranslation());
					}
					
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Last_Activity_By(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Last_Activity_Date(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Internal_Error))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Internal_Error(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_InfoHelp_Assignment_Maintenance))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_InfoHelp_Assignment_Maintenance(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_SaveUpdate(ObjLanguages.getTranslation());
					}							
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Cancel(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
					{
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Delete(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
					{
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Add_New(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
					{
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Action(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
					{
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Discription(ObjLanguages.getTranslation());
					}
					
										
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_InfoHelp_Assignment_Search_Lookup))
					{
						ObjInfoHelpsBe.setInfoHelpSearchlookup_InfoHelp_Assignment_Search_Lookup(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
					{
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Confirm_Delete(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
					{
						ObjInfoHelpsBe.setInfoHelpSearchlookup_Edit(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Choose_File))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Choose_File(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Loading))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Loading(ObjLanguages.getTranslation());
					}	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Supported_formats_is_pdf))
					{
						ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Supported_formats_is_pdf(ObjLanguages.getTranslation());
					}else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
				     {
					      ObjInfoHelpsBe.setInfoHelpassignmentmaintenance_Reset(ObjLanguages.getTranslation());
					     }
	
	
				}
				return ObjInfoHelpsBe;	
	 }	
	 
	 /**
	     * 
	     * @Description get team member name form team members table based on team member id
	     * @param Tenant
	     * @param Company
	     * @param TeamMember
	     * @return
	     * @throws JASCIEXCEPTION
	     */
		public String getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
			
			return ObjectRestInfoHelp.getTeamMemberName(Tenant, TeamMember);
					
		}
		
}
