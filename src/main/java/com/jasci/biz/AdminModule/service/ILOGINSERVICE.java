/*

Date Developed  Oct 14 2014
Description  Provide service layer between controller and dao layer.
Created By Deepak Sharma
 */
package com.jasci.biz.AdminModule.service;

public interface ILOGINSERVICE 
{
 public boolean ValidateUser(String strUsername, String strPassword);
}
