
/*

Description It is used to Call Restfull Service Function.
Created By Aakash Bishnoi 
Created On Dec 15, 2014
*/
package com.jasci.biz.AdminModule.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.MENUMESSAGESSERVICEAPI;
import com.jasci.biz.AdminModule.be.MENUMESSAGEBE;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGESBEAN;
import com.jasci.biz.AdminModule.model.MENUMESSAGESPK;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;
@Service
public class MENUMESSAGESERVICEIMPL implements IMENUMESSAGESERVICE{
	
	@Autowired
	MENUMESSAGESSERVICEAPI ObjRestServiceClient;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	@Autowired
	LANGUANGELABELSAPI ObjLanguageLabelApi;
	
	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Dec 15, 2014
	 * Discription This is used to get the list of MenuMessages on the behalf of Type from MenuMessage Table
	 * @param StrTenant_ID
	 * @param StrCompany_ID
	 * @param StrStatus
	 * @throws JASCIEXCEPTION
	 * @Return List<MENUMESSAGESBEAN>
	 */
	 public List<MENUMESSAGESBEAN> getList(String StrTenant_ID,String StrCompany_ID,String StrStatus)throws JASCIEXCEPTION{
		 return ObjRestServiceClient.getList(StrTenant_ID,StrCompany_ID,StrStatus);
	 }
	 
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 17, 2014
	  * Description this is used to get the List of Team Member Company
	  * @param StrTenant
	  * @param StrTeamMember
	  * @return List<TEAMMEMBERCOMPANIESBEAN>
	  * @throws JASCIEXCEPTION
	  */
	 public List<COMPANIES>  getCompanyList(String StrTenant,String StrTeamMember) throws JASCIEXCEPTION{
		 return ObjRestServiceClient.getCompanyList(StrTenant,StrTeamMember);
	 }
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 16, 2014
	  * Description This is used to fetch the list of MenuMessages on the behalf of Input Parameter from MenuMessage Table
	  * @param StrTenant_ID
	  * @param StrCompany_ID
	  * @param StrStatus
	  * @return MENUMESSAGESBEAN
	  * @throws JASCIEXCEPTION
	  */
	 public MENUMESSAGESBEAN fetchMenuMessages(String StrTenant_ID,String StrCompany_ID,String StrStatus,String Strtype,String StrFetchMessage_ID) throws JASCIEXCEPTION{
		 return ObjRestServiceClient.fetchMenuMessages(StrTenant_ID,StrCompany_ID,StrStatus,Strtype,StrFetchMessage_ID);
	 }
	
	
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 17, 2014
	  * Description Used to call RestfullService for Update MenuMessage
	  * @param ObjectMenuMessage
	  * @return String
	  * @throws JASCIEXCEPTION
	  */
	 public String updateEntry(MENUMESSAGESBEAN ObjectMenuMessage) throws JASCIEXCEPTION {
		
		 MENUMESSAGESPK obj_MENUMESSAGESPK = new MENUMESSAGESPK();
		 MENUMESSAGES obj_MENUMESSAGES=new MENUMESSAGES();
			
		 obj_MENUMESSAGES=useGetterSetter(obj_MENUMESSAGESPK,obj_MENUMESSAGES,ObjectMenuMessage);
			return ObjRestServiceClient.updateEntry(obj_MENUMESSAGES);
			
	 }

	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 17, 2014
	  * Description Used to call RestfullService for Add MenuMessages
	  * @param ObjectMenuMessage
	  * @return String
	  * @throws JASCIEXCEPTION
	  */
	 public String addEntry(MENUMESSAGESBEAN ObjectMenuMessage,String[] CompanyList) throws JASCIEXCEPTION {
		
		
			
		 //obj_MENUMESSAGES=useGetterSetter(obj_MENUMESSAGESPK,obj_MENUMESSAGES,ObjectMenuMessage);
		 for(int index=0;index<CompanyList.length;index++)
		 {
			 MENUMESSAGESPK obj_MENUMESSAGESPK = new MENUMESSAGESPK();
			 MENUMESSAGES obj_MENUMESSAGES=new MENUMESSAGES();
			if(ObjectMenuMessage.getTicket_Tape_Message()!=GLOBALCONSTANT.BlankString){
				
				obj_MENUMESSAGESPK.setMessage_ID(UUID.randomUUID().toString());
				obj_MENUMESSAGESPK.setTenant_ID(trimValues(OBJCOMMONSESSIONBE.getTenant()));
				obj_MENUMESSAGESPK.setCompany_ID(trimValues(CompanyList[index]));
				obj_MENUMESSAGESPK.setStatus(GLOBALCONSTANT.MenuMessages_Status);
				obj_MENUMESSAGES.setId(obj_MENUMESSAGESPK);
				obj_MENUMESSAGES=useGetterSetterForAdd(obj_MENUMESSAGES,ObjectMenuMessage);

				obj_MENUMESSAGES.setMessage(GLOBALCONSTANT.BlankString);
				obj_MENUMESSAGES.setTicket_Tape_Message(trimValues(ObjectMenuMessage.getTicket_Tape_Message()));								
				ObjRestServiceClient.addEntry(obj_MENUMESSAGES,CompanyList);
			
			 }
			 if(ObjectMenuMessage.getMessage1()!=GLOBALCONSTANT.BlankString){
				
				 obj_MENUMESSAGESPK.setMessage_ID(UUID.randomUUID().toString());
				 obj_MENUMESSAGESPK.setTenant_ID(trimValues(OBJCOMMONSESSIONBE.getTenant()));
				 obj_MENUMESSAGESPK.setCompany_ID(trimValues(CompanyList[index]));
				 obj_MENUMESSAGESPK.setStatus(GLOBALCONSTANT.MenuMessages_Status);
				 obj_MENUMESSAGES.setId(obj_MENUMESSAGESPK);
				 obj_MENUMESSAGES=useGetterSetterForAdd(obj_MENUMESSAGES,ObjectMenuMessage);

				 obj_MENUMESSAGES.setMessage(trimValues(ObjectMenuMessage.getMessage1()));
				 obj_MENUMESSAGES.setTicket_Tape_Message(GLOBALCONSTANT.BlankString);			
				 ObjRestServiceClient.addEntry(obj_MENUMESSAGES,CompanyList);
				
			 }
			 
		 }
		 
		 return "sucess";
	 }
	 /**
	  * @author Aakash Bishnoi
	  * @Date Dec 17, 2014
	  * Description This function is used to set the value for update record
	  * @param MENUMESSAGESPK
	  * @param MENUMESSAGES
	  * @param MENUMESSAGESBEAN
	  * @return MENUMESSAGES
	  * @throws JASCIEXCEPTION
	  */
		public MENUMESSAGES useGetterSetter(MENUMESSAGESPK obj_MENUMESSAGESPK,MENUMESSAGES obj_MENUMESSAGES,MENUMESSAGESBEAN ObjectMenuMessages) throws JASCIEXCEPTION{
				
			// Set GENERALCODESPK values
			obj_MENUMESSAGESPK.setMessage_ID(trimValues(ObjectMenuMessages.getMessage_ID()));
			obj_MENUMESSAGESPK.setTenant_ID(trimValues(ObjectMenuMessages.getTenant_ID()));
			obj_MENUMESSAGESPK.setCompany_ID(trimValues(ObjectMenuMessages.getCompany_ID()));
			obj_MENUMESSAGESPK.setStatus(GLOBALCONSTANT.MenuMessages_Status);
		
			/**Set GENERALCODES values*/
			obj_MENUMESSAGES.setTicket_Tape_Message(trimValues(ObjectMenuMessages.getTicket_Tape_Message()));
		
			obj_MENUMESSAGES.setMessage(trimValues(ObjectMenuMessages.getMessage()));
			
			/**This block is used to Set Current Date*/
			//Date CurrentDate=new Date();
			DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
			
			try {
				   
				   obj_MENUMESSAGES.setEnd_Date(ObjDateFormat.parse(ObjectMenuMessages.getEnd_Date()));
				   obj_MENUMESSAGES.setStart_Date(ObjDateFormat.parse(ObjectMenuMessages.getStart_Date()));
				   obj_MENUMESSAGES.setLast_Activity_Date(ObjDateFormat.parse(ObjectMenuMessages.getLast_Activity_Date()));
				   obj_MENUMESSAGES.setLast_Activity_Team_Member(trimValues(OBJCOMMONSESSIONBE.getTeam_Member()));
				  }catch(Exception objException){}
		    
		    obj_MENUMESSAGES.setId(obj_MENUMESSAGESPK);
			
			return obj_MENUMESSAGES;
		}
		/**
		  * @author Aakash Bishnoi
		  * @Date Dec 18, 2014
		  * Description This function is used to set the value for add record
		  * @param MENUMESSAGESPK
		  * @param MENUMESSAGES
		  * @param MENUMESSAGESBEAN
		  * @return MENUMESSAGES
		  * @throws JASCIEXCEPTION
		  */
			public MENUMESSAGES useGetterSetterForAdd(MENUMESSAGES obj_MENUMESSAGES,MENUMESSAGESBEAN ObjectMenuMessages) throws JASCIEXCEPTION{
					
							
				
				
				//This block is used to Set Current Date
			
				DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
			
				try{
			    obj_MENUMESSAGES.setEnd_Date(ObjDateFormat.parse(ObjectMenuMessages.getEnd_Date()));
				obj_MENUMESSAGES.setStart_Date(ObjDateFormat.parse(ObjectMenuMessages.getStart_Date()));
			    obj_MENUMESSAGES.setLast_Activity_Date(ObjDateFormat.parse(ObjectMenuMessages.getLast_Activity_Date()));
			    obj_MENUMESSAGES.setLast_Activity_Team_Member(trimValues(OBJCOMMONSESSIONBE.getTeam_Member()));
				}catch(Exception objException)
				{
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			    
				
				return obj_MENUMESSAGES;
			}
			
			/**
			 * @author Aakash Bishnoi
			  * @Date Dec 19, 2014
			  * Description this is used to delete the selected menu messages
			 * @param model
			 * @return Boolean
			 * @throws JASCIEXCEPTION
			 */
			public Boolean deleteEntry(String Message_ID,String Tenant_ID,String Company_ID,String Status) throws JASCIEXCEPTION{
				 return ObjRestServiceClient.deleteEntry(Message_ID,Tenant_ID,Company_ID,Status);
			 }
			 
		//It is used to trim the space
		/**
		 * @author Aakash Bishnoi
		 * @Date Dec 17, 2014
		 * Description This function is used to trim the space
		 * @param Value
		 * @return
		 */
		public static String trimValues(String Value){
			if(Value == null){
				return null;
			}
			else{
				String CheckValue=Value.trim();
				if(CheckValue.length()<GLOBALCONSTANT.IntOne){
					return null;
				}else{						
				return CheckValue;
				}
			}
		}
		
		
		//It is used to convert date Into particular format 
		/**
		 * @author Aakash Bishnoi
		 * @Date Dec 17, 2014
		 * Description This function is used to Convert date into other Date Format
		 * @param dateInString
		 * @param StrInFormat
		 * @param StrOutFormat
		 * @return
		 */
		public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
		{
			SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
			DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
			String StrFormattedString=dateInString ;
		 
			try {
		 
				Date date = formatter.parse(StrFormattedString);
				StrFormattedString=dateFormat.format(date);
		 
			} catch (Exception e) {
				
			}
			
			return StrFormattedString;
			
		}
		
		
		
		 //It is used to Get the label Of InfoHelps's All Screen and Error Message 
		 @Transactional
		 public MENUMESSAGEBE getMenuMessagesLabels(String StrLanguage) throws JASCIEXCEPTION{
			
				List<LANGUAGES> ObjListLanguages=ObjLanguageLabelApi.GetScreenLabels(LANGUANGELABELSAPI.StrMenuMessage, StrLanguage);
				MENUMESSAGEBE ObjMenuMessageBe=new MENUMESSAGEBE();  
			
					for (LANGUAGES ObjLanguages : ObjListLanguages) 
					{
						String StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
						
						if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Menu_Messages_Search_Lookup))
						{
							ObjMenuMessageBe.setMenuMessage_Menu_Messages_Search_Lookup(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_Date))
						{
							ObjMenuMessageBe.setMenuMessage_Start_Date(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_End_Date))
						{
							ObjMenuMessageBe.setMenuMessage_End_Date(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Type))
						{
							ObjMenuMessageBe.setMenuMessage_Type(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Message))
						{
							ObjMenuMessageBe.setMenuMessage_Message(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
						{
							ObjMenuMessageBe.setMenuMessage_Actions(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
						{
							ObjMenuMessageBe.setMenuMessage_Edit(ObjLanguages.getTranslation());
						}
						
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
						{
							ObjMenuMessageBe.setMenuMessage_Delete(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
						{
							ObjMenuMessageBe.setMenuMessage_Add_New(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_date_cannot_be_older_then_current_date))
						{
							ObjMenuMessageBe.setMenuMessage_ERR_Start_Date(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_End_date_cannot_be_older_then_start_date))
						{
							ObjMenuMessageBe.setMenuMessage_ERR_End_Date(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
						{
							ObjMenuMessageBe.setMenuMessage_ERR_Confirm_Delete(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_either_Ticket_Tape_Message_or_Message))
						{
							ObjMenuMessageBe.setMenuMessage_ERR_Atleast_One(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Message_Already_Used))
						{
							ObjMenuMessageBe.setMenuMessage_ERR_Message_Already_Used(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
						{
							ObjMenuMessageBe.setMenuMessage_Mandatory_Fields(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Menu_Messages_Maintenance))
						{
							ObjMenuMessageBe.setMenuMessage_Menu_Messages_Maintenance(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Ticket_Tape_Message))
						{
							ObjMenuMessageBe.setMenuMessage_Ticket_Tape_Message(ObjLanguages.getTranslation());
						}
						/*else if(StrObjectFieldCode.equalsIgnoreCase(GLOBALCONSTANT.MenuMessage_Message1))
						{
							ObjMenuMessageBe.setMenuMessage_Message1(ObjLanguages.getTranslation());
						}
*/						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
						{
							ObjMenuMessageBe.setMenuMessage_Save_Update(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Companies))
						{
							ObjMenuMessageBe.setMenuMessage_Companies(ObjLanguages.getTranslation());
						}
						
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
						{
							ObjMenuMessageBe.setMenuMessage_Cancel(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_End_Date_YYYY_MM_DD))
						{
							ObjMenuMessageBe.setMenuMessage_End_Date_YYMMDD(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_Date_YYYY_MM_DD))
						{
							ObjMenuMessageBe.setMenuMessage_Start_Date_YYMMDD(ObjLanguages.getTranslation());
						}
						
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
						{
							ObjMenuMessageBe.setMenuMessage_Confirm_Save(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
						{
							ObjMenuMessageBe.setMenuMessage_Confirm_Update(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_Name))
						{
							ObjMenuMessageBe.setMenuMessage_Company_Name(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_ID))
						{
							ObjMenuMessageBe.setMenuMessage_Company_ID(ObjLanguages.getTranslation());
						}
						else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_atleast_one_company_to_proceed))
						{
							ObjMenuMessageBe.setMenuMessage_ERR_SELECT_ONE_COMPANY(ObjLanguages.getTranslation());
						}else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
					      {
						       ObjMenuMessageBe.setMenuMessage_Reset(ObjLanguages.getTranslation());
						      }
						
								
					}
					return ObjMenuMessageBe;	
		 }	
}
