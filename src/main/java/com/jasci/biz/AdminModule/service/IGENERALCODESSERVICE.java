/**
Description its a interface where we define the function that implement in class of GeneralCodeServiceImplement
Created By Aakash Bishnoi  
Created Date Oct 20 2014
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.GENERALCODESBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.GENERALCODESBEAN;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;




public interface IGENERALCODESSERVICE {
	
//Declare the function of GENERALCODESSERVICEIMPL
public List<GENERALCODESBEAN> getList(COMMONSESSIONBE objCommonsessionbe) throws JASCIEXCEPTION;

//For add data in General_Codes and Menu_Option table using GeneralCode screen
 public String addEntry(GENERALCODESBEAN ObjectGeneralCodes) throws JASCIEXCEPTION;

 //For add data in General_Codes table using GeneralCodeID screen
 public String addGeneralCodeIdEntry(GENERALCODESBEAN ObjectGeneralCodes)  throws JASCIEXCEPTION;
 
 //Method for fetch data from general_codes
 public GENERALCODES getEntry(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode) throws JASCIEXCEPTION;
 
 //Method for record in genral_codes and menu option
 public String updateEntry(GENERALCODESBEAN objectGeneralCodes) throws JASCIEXCEPTION;
 
	//It is used for Update data in GENERAL_CODES table for the screen GeneralCOdeID (it is used in updateEntry function on controller 
 public String updateGeneralCodeID(GENERALCODESBEAN ObjectGeneralCodes) throws JASCIEXCEPTION;
 
  //It is used to set the label of General Code Screens
 public GENERALCODESBE SetGeneralCodesBe(String StrLanguage) throws JASCIEXCEPTION;
 
/* //It is used to get the value of Application Combo Box
 public List SelectApplication() throws JASCIEXCEPTION;
 
 //It is used to get the value of GeneralCode ComboBox
 public List SelectGeneralCode() throws JASCIEXCEPTION;*/
 
//get the first list of GeneralCode screen
public List<GENERALCODESBEAN> getMainGeneralCodeList(String Tenant,String Company,String GeneralCode)throws JASCIEXCEPTION;

//get the sub list of general code screen behalf og generalcodeid
public List<GENERALCODESBEAN> getSubGeneralCodeList(String Tenant,String Company,String GeneralCodeID)throws JASCIEXCEPTION;

//delete using kendo ui
public Boolean deleteEntry(String Tenant,String Company,String Application,String GeneralCodeId,String GeneralCode) throws JASCIEXCEPTION;

//delete using kendo ui GeneralCodeID
public Boolean deleteGeneralCodeID(String tenant,String company,String application,String generalcodeid,String generalcode) throws JASCIEXCEPTION;
}
