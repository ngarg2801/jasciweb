/**

Date Developed: Jan 07 2015
Created By: Rahul Kumar
Description: Provide service layer between controller and dao layer.
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.LOCATIONLABELPRINTSCREENBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.exception.JASCIEXCEPTION;


public interface ILOCATIONLABELPRINTSERVICE {
	
	
	/**
	 * @Description set screen label language
	 * @author Rahul Kumar
	 * @Date Jan 07, 2015 
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public LOCATIONLABELPRINTSCREENBE setScreenLanguage( String StrScreenName,String StrLanguage)throws JASCIEXCEPTION;


	
	/**
	 * 
	 *@Description get general code from general code table base on general code id
	 *@Auther Rahul Kumar
	 *@Date Jan 15, 2015			
	 *@Return type List<GENERALCODES>
	 *@param Tenant
	 *@param Company
	 *@param GeneralCodeId
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws JASCIEXCEPTION;

}
