
/*

Date Developed  Nov 17 2014
Created By "Rahul Kumar"
Description  Provide service layer between controller and dao layer.
 */
package com.jasci.biz.AdminModule.service;

import java.io.Serializable;
import java.util.List;

import com.jasci.biz.AdminModule.be.TEAMMEMBERSSCREENBE;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface ITEAMMEMBERSSERVICE {


	/*
	 * Created on:Nov 17 2014
	 * Created by:Rahul Kumar
	 * Description: This function register new TeamMember 
	 * Input parameter:TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList
	 * Return Type :Boolean
	 * 
	 */
	public Serializable addEntry(TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList,String strLastActivityBy) throws JASCIEXCEPTION;

	/*
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function get TeamMember based on TeamMemeber or PartOfTeamMember
	 * Input parameter: String TeamMemberName,String PartOfTeamMember,String TeamMemberName,String PartOfTeamMember,String Tenant
	 * Return Type :List<TEAMMEMBERS>
	 * 
	 */
	public List<TEAMMEMBERS> getList(String TeamMemberName,String PartOfTeamMember,String Tenant) throws  JASCIEXCEPTION;

	
	
	/*
	 * Created on:Nov 25 2014
	 * Created by:Rahul Kumar
	 * Description: This function get TeamMember based on TeamMemeber or PartOfTeamMember
	 * Input parameter: String TeamMemberName,String PartOfTeamMember,String TeamMemberName,String PartOfTeamMember,String Tenant,
	 * 	 * Return Type :List<TEAMMEMBERSBE>
	 * 
	 */
	public List<TEAMMEMBERSBE> getTeamMemberList(String SortOrder,String StrPartOfTeamMember,String Tenant)throws JASCIEXCEPTION;

	/*
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function delete TeamMember based on tenant,TeamMemeber 
	 * Input parameter: String
	 * Return Type :List<COMPANIES>
	 * 
	 */
	public TEAMMEMBERS deleteEntry(String Tenant,String TeamMember,String Company) throws JASCIEXCEPTION;


	/*
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function  get Companies list based on tenant 
	 * Input parameter: String
	 * Return Type :List<COMPANIES>
	 * 
	 */
	public List<COMPANIES> getCompanies(String Tenant) throws  JASCIEXCEPTION;

	/*
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function  get General code list based on tenant,company,application,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws JASCIEXCEPTION;



	/*
	 * Created on:Nov 22 2014
	 * Created by:Rahul Kumar
	 * Description: This function get Companies of TeamMember based on tenant an team member
	 * Input parameter: String Tenant,String TeamMember
	 * Return Type :List<TEAMMEMBERCOMPANIES>
	 * 
	 */

	public List<TEAMMEMBERCOMPANIES> getTeamMemberCompanies(String Tenant,String TeamMember) throws  JASCIEXCEPTION; 


	/*
	 * Created on:Nov 23 2014
	 * Created by:Rahul Kumar
	 * Description: This function update existing   TeamMember 
	 * Input parameter:TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList
	 * Return Type :Boolean
	 * 
	 */
	public TEAMMEMBERS updateEntry(TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList,String TeamMemberoldId,String Company,String strLastActivityBy)throws JASCIEXCEPTION;



	/*
	 *Description This function design to get languages from LANGUAGES table
	 *Input parameter none
	 *Return Type List<LANGUAGES>
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 24 2014" 
	 * */

	public TEAMMEMBERSSCREENBE setScreenLanguage( String StrScreenName,String StrLanguage)throws JASCIEXCEPTION;
}



