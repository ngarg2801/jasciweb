package com.jasci.biz.AdminModule.service;

/**
 Description This class for service  for SCAN LPN screen 
 Created By  Shailendra Rajput 
 Created Date Oct 19 2015
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.EXECUTIONSBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class EXECUTIONSLABELSSERVICEIMPL implements IEXECUTIONSLABELSSERVICE {

	@Autowired
	LANGUANGELABELSAPI objLanguageLabelsAPI;

	/**
	 * 
		 * Created By: Shailendra Rajput
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Oct 19 2015
		 * @Description : Set Level in getter Setters for Executions levels
	 */
	
	@Transactional
	public EXECUTIONSBE getScanLPNLabels(String Modulename, String defaultLanguage) throws JASCIEXCEPTION {

		List<LANGUAGES> objLanguagesLabelsExecution = objLanguageLabelsAPI.GetScreenLabels(Modulename, defaultLanguage);

		EXECUTIONSBE objExecutionsbe = new EXECUTIONSBE();

		for (LANGUAGES objLanguages : objLanguagesLabelsExecution) {

			String StrKeyPhrase = objLanguages.getId().getKeyPhrase();
			String StrTranslation = objLanguages.getTranslation();

			if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPSCAN)) {
				objExecutionsbe.setSCAN(StrTranslation);
			}
			if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPPRODUCTS)) {

				objExecutionsbe.setPRODUCTS(StrTranslation);
			} else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPQUANTITY)) {
				objExecutionsbe.setQUANTITY(StrTranslation);
			}

			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPLINES)) {
				objExecutionsbe.setLINES(StrTranslation);
			}
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPPRIORITY))

			{
				objExecutionsbe.setPRIORITY(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPPICK)) 
			{
				objExecutionsbe.setPICK(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPLOCATION)) 
			{
				objExecutionsbe.setLOCATION(StrTranslation);
			}
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPLOCATION_PRIME)) 
			{
				objExecutionsbe.setLOCATION_PRIME(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPTEXT_OK)) 
			{
				objExecutionsbe.setTEXT_OK(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPDESCRIPTION)) 
			{
				objExecutionsbe.setDESCRIPTION(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPPRODUCT)) 
			{
				objExecutionsbe.setPRODUCT(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPQUALITY)) 
			{
				objExecutionsbe.setQUALITY(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPMESSAGE_CHECK)) 
			{
				objExecutionsbe.setMESSAGE_CHECK(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPLPN))
			{
				objExecutionsbe.setLPN(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPITEM)) 
			{
				objExecutionsbe.setITEM(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPSCAN_LPN)) 
			{
				objExecutionsbe.setSCAN_LPN(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPSCAN_ITEM))
			{
				objExecutionsbe.setSCAN_ITEM(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_INVALID_SCAN_LOCATION)) 
			{
				objExecutionsbe.setERR_MSG_INVALID_SCAN_LOCATION(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_LPN_ALREADY_ASSIGNED))
			{
				objExecutionsbe.setERR_MSG_LPN_ALREADY_ASSIGNED(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_INVALID_WORK_TYPE)) 
			{
				objExecutionsbe.setERR_MSG_INVALID_WORK_TYPE(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_INVALIDE_TASK_FOR_SEQUENCE)) 
			{
				objExecutionsbe.setERR_MSG_INVALIDE_TASK_FOR_SEQUENCE(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_INVALIDE_LPN)) 
			{
				objExecutionsbe.setERR_MSG_INVALIDE_LPN(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_INVALIDE_EXECUTION))
			{
				objExecutionsbe.setERR_MSG_INVALIDE_EXECUTION(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_WORK_TYPE_NOT_FOUND)) 
			{
				objExecutionsbe.setERR_MSG_WORK_TYPE_NOT_FOUND(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_INTERNET_CONNECTION))
			{
				objExecutionsbe.setERR_MSG_INTERNET_CONNECTION(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_INVALIDE_AREA_LOCATION)) 
			{
				objExecutionsbe.setERR_MSG_INVALIDE_AREA_LOCATION(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_MSG_SERVER_NOT_RESPOND)) 
			{
				objExecutionsbe.setERR_MSG_SERVER_NOT_RESPOND(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPSTRLOADING)) 
			{
				objExecutionsbe.setSTRLOADING(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPDIRECTION_FORWARD)) 
			{
				objExecutionsbe.setDIRECTION_FORWARD(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPDIRECTION_LEFT)) 
			{
				objExecutionsbe.setDIRECTION_LEFT(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPDIRECTION_RIGHT)) 
			{
				objExecutionsbe.setDIRECTION_RIGHT(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPAPK_UPDATE_MSG)) 
			{
				objExecutionsbe.setAPK_UPDATE_MSG(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPAPK_UPDATE_YES)) 
			{
				objExecutionsbe.setAPK_UPDATE_YES(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPAPK_UPDATE_NO)) 
			{
				objExecutionsbe.setAPK_UPDATE_NO(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPEND)) 
			{
				objExecutionsbe.setEND(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPEXIT)) 
			{
				objExecutionsbe.setEXIT(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPSTART)) 
			{
				objExecutionsbe.setSTART(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPSCANWORKZONE)) 
			{
				objExecutionsbe.setSCANWORKZONE(StrTranslation);
			} 
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPERR_INVALIDWORKZONE))
			{
				objExecutionsbe.setERR_INVALIDWORKZONE(StrTranslation);
			}
			else if (StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KPCONTINUE))
			{
				objExecutionsbe.setCONTINUE(StrTranslation);
			}
			
		}

		return objExecutionsbe;
	}
}
