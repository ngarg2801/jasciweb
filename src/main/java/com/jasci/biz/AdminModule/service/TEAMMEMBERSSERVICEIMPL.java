/**

Date Developed  Nov 17 2014
Created By Rahul Kumar
Description  Provide service layer between team members controller and team members DAO
 */

package com.jasci.biz.AdminModule.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.TEAMMEMBERSERVICEAPI;
import com.jasci.biz.AdminModule.be.TEAMMEMBERSSCREENBE;
import com.jasci.biz.AdminModule.dao.ITEAMMEMBERSDAO;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;



@Service
public class TEAMMEMBERSSERVICEIMPL implements ITEAMMEMBERSSERVICE {

	@Autowired
	TEAMMEMBERSERVICEAPI objRestteammembers;
	@Autowired
	LANGUANGELABELSAPI objLanguangelabelsapi;
	@Autowired
	private ITEAMMEMBERSDAO ObjTeamMemberDao;
	COMMONSESSIONBE ObjCommonSessionBe = new COMMONSESSIONBE();


	/**
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function get TeamMember based on TeamMemeber or PartOfTeamMember and Tenant 
	 * Input parameter:String TeamMemberName,String PartOfTeamMember,String Tenant,Long FulfillmentCenter
	 * Return Type :List<TEAMMEMBERS>
	 * 
	 */

	@Transactional
	public List<TEAMMEMBERS> getList(String TeamMemberName,String PartOfTeamMember,String Tenant) throws  JASCIEXCEPTION 
	{
		List<TEAMMEMBERS> ObjListTeamMembers=null;
		ObjListTeamMembers=	objRestteammembers.getList(TeamMemberName, PartOfTeamMember, Tenant);
		return ObjListTeamMembers;

	}

	/**
	 * Created on:Nov 17 2014
	 * Created by:Rahul Kumar
	 * Description: This function register new TeamMember 
	 * Input parameter:TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList
	 * Return Type :Boolean
	 * 
	 */

	@Transactional
	public Serializable addEntry(TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList,String strLastActivityBy)throws  JASCIEXCEPTION
	{


		Serializable objSerializable=null;
		objSerializable=objRestteammembers.addEntry(ObjectTeamMembersBe,CompaniesList,strLastActivityBy);
		return objSerializable;

	}


	/**
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function delete TeamMember based on tenant,TeamMemeber 
	 * Input parameter: String
	 * Return Type :List<COMPANIES>
	 * 
	 */
	@Transactional
	public TEAMMEMBERS deleteEntry(String Tenant,String TeamMember,String Company) throws JASCIEXCEPTION{


		return objRestteammembers.deleteEntry(Tenant,TeamMember,Company);


	}


	/**
	 * Created on:Nov 23 2014
	 * Created by:Rahul Kumar
	 * Description: This function update existing  TeamMember 
	 * Input parameter:TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	public TEAMMEMBERS updateEntry(TEAMMEMBERSBE ObjectTeamMembersBe, List<String> CompaniesList,String TeamMemberoldId,String Company,String  strLastActivityBy) throws JASCIEXCEPTION{
		return objRestteammembers.updateEntry(ObjectTeamMembersBe, CompaniesList,TeamMemberoldId,Company,strLastActivityBy);
	}



	/**
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function  get Companies list based on tenant 
	 * Input parameter: String
	 * Return Type :List<COMPANIES>
	 * 
	 */
	@Transactional
	public List<COMPANIES> getCompanies(String Tenant) throws JASCIEXCEPTION {
		return objRestteammembers.getCompanies(Tenant);

	}


	/**
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function  get General code list based on tenant,company,application,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	@Transactional
	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION {


		return objRestteammembers.getGeneralCode(Tenant, Company, GeneralCodeId);
	}


	/**
	 * Created on:Nov 22 2014
	 * Created by:Rahul Kumar
	 * Description: This function get Companies of TeamMember based on tenant an team member
	 * Input parameter: String Tenant,String TeamMember
	 * Return Type :List<TEAMMEMBERCOMPANIES>
	 * 
	 */
	@Transactional
	public List<TEAMMEMBERCOMPANIES> getTeamMemberCompanies(String Tenant, String TeamMember)throws  JASCIEXCEPTION {


		return objRestteammembers.getTeamMemberCompanies(Tenant, TeamMember);
	}




	/**
	 *Description This function design to get languages from LANGUAGES table
	 *Input parameter :String StrScreenName, String StrLanguage
	 *Return Type List<LANGUAGES>
	 *Created By Rahul Kumar
	 *Created Date Nov 24 2014 
	 *
	 */


	@Transactional
	public TEAMMEMBERSSCREENBE setScreenLanguage(String StrScreenName, String StrLanguage) throws  JASCIEXCEPTION {

		TEAMMEMBERSSCREENBE objTeammembersscreenbe=new TEAMMEMBERSSCREENBE();
		List<LANGUAGES> ObjListLanguages=objLanguangelabelsapi.GetScreenLabels(LANGUANGELABELSAPI.strTeamMemberModule,StrLanguage);

		for (LANGUAGES ObjLanguages : ObjListLanguages) 
		{

			String StrKeyPhrase=ObjLanguages.getId().getKeyPhrase();
			String StrTranslation=ObjLanguages.getTranslation();




			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Members_Lookup)){
				objTeammembersscreenbe.setTeamMemberMaintenance_TeamMemberMaintenance(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Member_Id)){
				objTeammembersscreenbe.setTeamMemberMaintenance_EnterTeamMember(StrTranslation);				
				objTeammembersscreenbe.setTeamMemberMaintenance_TeamMember(StrTranslation);
				objTeammembersscreenbe.setTeamMemberMaintenance_TeamMemberId(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_Team_Member_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_PartOfTheTeamMemberName(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New)){
				objTeammembersscreenbe.setTeamMemberMaintenance_New(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Search)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Search(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All)){
				objTeammembersscreenbe.setTeamMemberMaintenance_DisplayAll(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Members_Maintenance)){
				objTeammembersscreenbe.setTeamMemberMaintenance_TeamMemberMaintenanceNew(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Set_up_Date)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SetUpDate(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Setup_By)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SetupBy(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date)){
				objTeammembersscreenbe.setTeamMemberMaintenance_LastActivityDate(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By)){
				objTeammembersscreenbe.setTeamMemberMaintenance_LastActivityBy(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Currrent_Status)){
				objTeammembersscreenbe.setTeamMemberMaintenance_CurrentStatus(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_First_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_FirstName(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_LastName(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Middle_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_MiddleName(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_1)){
				objTeammembersscreenbe.setTeamMemberMaintenance_AddressLine1(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_2)){
				objTeammembersscreenbe.setTeamMemberMaintenance_AddressLine2(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_3)){
				objTeammembersscreenbe.setTeamMemberMaintenance_AddressLine3(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_4)){
				objTeammembersscreenbe.setTeamMemberMaintenance_AddressLine4(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_City)){
				objTeammembersscreenbe.setTeamMemberMaintenance_City(StrTranslation);
			}			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Country_Code)){
				objTeammembersscreenbe.setTeamMemberMaintenance_CountryCode(StrTranslation);
			}			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_State)){
				objTeammembersscreenbe.setTeamMemberMaintenance_State(StrTranslation);
			}			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Zip_Code)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ZipCode(StrTranslation);
			}			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Phone)){
				objTeammembersscreenbe.setTeamMemberMaintenance_WorkPhone(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Extension)){
				objTeammembersscreenbe.setTeamMemberMaintenance_WorkExtension(StrTranslation);
			}			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Cell)){
				objTeammembersscreenbe.setTeamMemberMaintenance_WorkCell(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Fax)){
				objTeammembersscreenbe.setTeamMemberMaintenance_WorkFax(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Email)){
				objTeammembersscreenbe.setTeamMemberMaintenance_WorkEmail(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Home_Phone)){
				objTeammembersscreenbe.setTeamMemberMaintenance_HomePhone(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cell)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Cell(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Email)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Email(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Emergency_Contact_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_EmergencyContactName(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Emergency_Home_Phone)){
				objTeammembersscreenbe.setTeamMemberMaintenance_EmergencyHomePhone(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Emergency_Cell)){
				objTeammembersscreenbe.setTeamMemberMaintenance_EmergencyCell(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Emergency_Email)){
				objTeammembersscreenbe.setTeamMemberMaintenance_EmergencyEmial(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Department)){
				objTeammembersscreenbe.setTeamMemberMaintenance_DepartMent(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Shift_Code)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ShiftCode(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Language)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Language(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_Date_YYYY_MM_DD)){
				objTeammembersscreenbe.setTeamMemberMaintenance_StartDate(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Date_YYYY_MM_DD)){
				objTeammembersscreenbe.setTeamMemberMaintenance_LastDate(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Company(StrTranslation);
			}			

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Company_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_CompanyName(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Save(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Notes)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Notes(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_Members_Search_Lookup)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SearchLooup(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Name(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Action(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Delete(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sort_First_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SortFirstName(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Sort_Last_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SortLastName(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New)){
				objTeammembersscreenbe.setTeamMemberMaintenance_AddNew(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Select(StrTranslation);
			}



			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Team_member_already_Used)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Phone_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_WORK_PHONE(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Department_does_not_exist)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_ENTERED_DEPARTMENT_DOES_NOT_EXIST(StrTranslation);
			}



			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ShiftCode_does_not_exist)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_ENTERED_SHIFTCODE_DOES_NOT_EXIST(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Language_does_not_exist)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_ENTERED_LANGUAGE_DOES_NOT_EXIST(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Start_date_must_be_before_End_date)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_START_DATE_IS_AFTER_END_DATE(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_date_must_be_after_Start_date)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_LAST_DATE_IS_BEFORE_START_DATE(StrTranslation);
			}



			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_valid_Email_Address)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_EMAILID_NOT_CORRECT(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Cell_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_WORK_CELL(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Fax_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_WORK_FAX(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cell_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_CELL(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Emergency_Home_Phone_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_EMERGENCY_HOME_PHONE(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Emergency_Cell_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_EMERGENCY_CELL(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ON_SAVE_SUCCESSFULLY(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_atleast_one_company_to_proceed)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_NO_COMPANY_SELECTED(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Extension_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_WORK_EXTENSION(StrTranslation);
			}



			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_a_Team_Member_ID)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_PLZ_ENTER_TEAMMEMEBR(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_part_of_the_Team_Member_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_PLZ_ENTER_PARTOFTEAMMEMBER(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Team_Member_ID)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_Team_Member_Name)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_want_to_delete)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Edit(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_System_Use)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SystemUse(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Cancel(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Home_Phone_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_HOME_PHONE(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Email_address_already_exist)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Zip_Code)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_ZIP_CODE(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Address)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_INVALID_ADDRESS(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_YES)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SystemUse_Y(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_NO)){
				objTeammembersscreenbe.setTeamMemberMaintenance_SystemUse_N(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_delete_as_this_team_member_is_already_Inactive)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_WHILE_DELETING_ANY_RECORD_INACTIVE(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Status)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Status(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_YY_MM_DD)){
				objTeammembersscreenbe.setTeamMemberMaintenance_YY_MM_DD(StrTranslation);
			}


			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Leave_of_Absence)){
				objTeammembersscreenbe.setTeamMemberMaintenance_LeavOfAbsence(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Not_Active)){
				objTeammembersscreenbe.setTeamMemberMaintenance_NotActive(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Active)){
				objTeammembersscreenbe.setTeamMemberMaintenance_Active(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Deleted)){
				objTeammembersscreenbe.setTeamMemberMaintenance_StatusDelete(StrTranslation);
			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank_and_must_be_numeric_only)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank_and_must_be_valid_email_address)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset)){
				objTeammembersscreenbe.setTeamMemberMaintenance_ButtonResetText(StrTranslation);
			}
		}

		
		if(objTeammembersscreenbe.getTeamMemberMaintenance_Select()==null){
			objTeammembersscreenbe.setTeamMemberMaintenance_Select(LANGUANGELABELSAPI.KP_Select);
		}
		if(objTeammembersscreenbe.getTeamMemberMaintenance_Company()==null){
			objTeammembersscreenbe.setTeamMemberMaintenance_Company(LANGUANGELABELSAPI.KP_Company_ID);
		}
		if(objTeammembersscreenbe.getTeamMemberMaintenance_CompanyName()==null){
			objTeammembersscreenbe.setTeamMemberMaintenance_CompanyName(LANGUANGELABELSAPI.KP_Company_Name);
		}
		
		
		if(objTeammembersscreenbe.getTeamMemberMaintenance_SystemUse_Y()==null){
			objTeammembersscreenbe.setTeamMemberMaintenance_SystemUse_Y(LANGUANGELABELSAPI.KP_YES);
		}
		
		if(objTeammembersscreenbe.getTeamMemberMaintenance_SystemUse_N()==null){
			objTeammembersscreenbe.setTeamMemberMaintenance_SystemUse_N(LANGUANGELABELSAPI.KP_NO);
		}
	   
		
	

		return objTeammembersscreenbe;

	}

	/**
	 * Created on:Nov 25 2014
	 * Created by:Rahul Kumar
	 * Description: This function get TeamMember based on TeamMemeber or PartOfTeamMember
	 * Input parameter: String TeamMemberName,String PartOfTeamMember,String TeamMemberName,String PartOfTeamMember,String Tenant,Long FulfillmentCenter
	 * Return Type :List<TEAMMEMBERSBE>
	 * 
	 */
	@Transactional
	public List<TEAMMEMBERSBE> getTeamMemberList(String SortOrder, String StrPartOfTeamMember,String Tenant) throws 	JASCIEXCEPTION {

		return objRestteammembers.getTeamMemberList(SortOrder,StrPartOfTeamMember,Tenant);


	}




}
