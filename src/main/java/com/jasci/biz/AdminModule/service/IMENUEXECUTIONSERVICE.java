/*

Date Developed  Oct 15 2014
Description  pojo class of  FulfillmentCenters in which getter and setter methods
Created By Sarvendra Tyagi
 */



package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.MENUEXECUTIONLABELBE;
import com.jasci.biz.AdminModule.be.MENUEXECUTIONLISTBE;
import com.jasci.biz.AdminModule.be.SUBMENULISTBE;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIESBEAN;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface IMENUEXECUTIONSERVICE {
	
	
	//get company list based on team member and tenant
	public List<TEAMMEMBERCOMPANIESBEAN> getListCompanies(COMMONSESSIONBE ObjCommonSession) throws JASCIEXCEPTION;
	

	// get assigned profile list
	public List<MENUEXECUTIONLISTBE> getAssignedMenuProfileList(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION;
	
	//get object of sub Menu
	public SUBMENULISTBE getSubMenus(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description: This function is used for getting label of menuExecution all screen
	 * @param  :language
	 * @return :objMENUEXECUTIONLABELBE
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Sarvendra Tyagi
	 * @Date :dec 26 2014
	 */
		
	public MENUEXECUTIONLABELBE getLanguageLabel(String language)throws JASCIEXCEPTION;
	
	
	
}
