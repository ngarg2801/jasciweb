/**
Description    : Interface for SMARTTASKEXECUTIONSSERVICEIMPL.
Created By 	: Toe-On Chia  
Created Date : Step 24 2015
 */

package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONBE;
import com.jasci.biz.AdminModule.be.SMARTTASKEXECUTIONSBE;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONSBEAN;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public interface  ISMARTTASKEXECUTIONSSERVICE {
		public SMARTTASKEXECUTIONSBE getSmartTaskExecutionsLabels(String StrLanguage) throws JASCIEXCEPTION;
		
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)	
		public List getDisplayAll() throws JASCIEXCEPTION;
		
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
		public void setPageName(String StrPageName)  throws JASCIEXCEPTION;
		
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
		public List getList(String StrTenant_Id, String StrApplication_Id, String StrExecution_Sequence_Group, String StrExecution_Type, String StrExecution_Device, String StrExecution_Sequence_Name,String StrDescription20, String StrButton) throws JASCIEXCEPTION;
		
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
		public Boolean addExecutions(SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean) throws JASCIEXCEPTION;
		
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
		public boolean deleteExecutions(String StrTenant_Id, String StrCompany_Id, String StrExecution_Name) throws JASCIEXCEPTION;
		
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
		public List getExecutions(String StrTenant_Id, String StrCompany_Id, String StrExecution_Name) throws JASCIEXCEPTION;
}
