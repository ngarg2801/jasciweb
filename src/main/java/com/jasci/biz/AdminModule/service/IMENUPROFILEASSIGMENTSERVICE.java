/**

Date Developed :JAN 13 2014
Created by: sarvendra tyagi
Description :Menu Profile Assigment service Interface class for Business logic Unimplemented method
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.MENUPROFILEASSIGMENTLABELBE;
import com.jasci.biz.AdminModule.be.MENUPROFILEASSIGNMENTPOJOBE;
import com.jasci.exception.JASCIEXCEPTION;
public interface IMENUPROFILEASSIGMENTSERVICE {
	

	
	/** 
	 * @Description  : This function is used for getting Team Member list
	 * @param        : String TeamMemberID,String searchField
	 * @return       : TEAMMEMBERS
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	
	
	public List<MENUPROFILEASSIGNMENTPOJOBE> getTeamMemberList(String SearchValue,String SearchField)throws JASCIEXCEPTION;
	
	
	
	
	/** 
	 * @Description  : This function is used for getting labels of screen
	 * @param        : String language
	 * @return       : objLANGUAGES
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 15 2015
	 */
	
	
	public MENUPROFILEASSIGMENTLABELBE getScreenLabel(String language)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description  : This function is used for getting MenuOption
	 * @param        : String menuType
	 * @return       : objMENUPROFILEASSIGNMENTPOJOBEList
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	
	
	public List<MENUPROFILEASSIGNMENTPOJOBE> getMenuProfileList(String menuType)throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description  : This Method is used for assigned menu profile from menu profile Assignmenttable
	 * @param        :String strTeamMemberID
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public Boolean getAllAssignedMenuProfileDelete(String strTeamMemberID,String tenant)throws JASCIEXCEPTION;
	
	/** 
	 * @Description  : This Method is used for assigned menu profile saved records in Menu Profile assignment table
	 * @param        :objMenuprofileassignment
	 * @return       : Boolean
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public Boolean SaveAndUpdate(String teamMemberId,String strMenuProfileJson,String strDate)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description  : This Method is used for get team member name
	 * @param        :objMenuprofileassignment
	 * @return       : Boolean
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public MENUPROFILEASSIGNMENTPOJOBE getTeamMemberName(String teamMemberId)throws JASCIEXCEPTION;
	
	
}
