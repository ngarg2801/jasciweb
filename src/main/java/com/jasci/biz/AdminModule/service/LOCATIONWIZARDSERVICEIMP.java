/**
Description This class use for implementation of all the methods of ILOCATIONWIZARDSERVICE interface for Location Wizard screen 
Created By Pradeep Kumar  
Created Date Jan 05 2015
 */
package com.jasci.biz.AdminModule.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.LOCATIONWIZARDSERVICEAPI;
import com.jasci.biz.AdminModule.be.LOCATIONWIZARDLABELSBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARD;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARDPK;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.LOCATIONWIZARDBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class LOCATIONWIZARDSERVICEIMP implements ILOCATIONWIZARDSERVICE {

	
	@Autowired
	private COMMONSESSIONBE objCommonsessionbe;
	@Autowired
	private LOCATIONWIZARDSERVICEAPI objLocationwizardserviceapi;
	@Autowired
	private LANGUANGELABELSAPI objLanguageLevelsAPI;
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#getLocationWizard()
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 5, 2015
		 * @Description : get list for Particular Search 
	 */
	@Transactional
	public List<LOCATIONWIZARDBE> getLocationWizardSearch(String wizardID, String SearchValue,String Searchbased) throws JASCIEXCEPTION {
	
		
		//LOCATIONWIZARDBE objLocationWizardBE = new LOCATIONWIZARDBE();
		
		List<Object> ListLocationWIzard=null;
		
		if(Searchbased.equalsIgnoreCase(GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER))
			{
				ListLocationWIzard =objLocationwizardserviceapi.getSearchByWizardControlNumber(wizardID, SearchValue);
			}
		else if(Searchbased.equalsIgnoreCase(GLOBALCONSTANT.Location_Wizard_AREA))
		{
			ListLocationWIzard =objLocationwizardserviceapi.getSearchByArea(wizardID, SearchValue);
		}
		else if(Searchbased.equalsIgnoreCase(GLOBALCONSTANT.Location_Wizard_Location_Type))
			{
				ListLocationWIzard =objLocationwizardserviceapi.getSearchByLocationType(wizardID, SearchValue);
			}
		else if(Searchbased.equalsIgnoreCase(GLOBALCONSTANT.Location_Wizard_AnyPartofWizardDiscription))
		{
			ListLocationWIzard =objLocationwizardserviceapi.getSearchByAnyPartofWizardDiscription(wizardID, SearchValue);
		}
		
		List<LOCATIONWIZARDBE> listGridData=new ArrayList<LOCATIONWIZARDBE>();
		
		for(Iterator<Object> LocationListLength=ListLocationWIzard.iterator();LocationListLength.hasNext(); ){

			LOCATIONWIZARDBE ObjectLocationWizard=new LOCATIONWIZARDBE();

			Object []ObjectLocObj =  (Object[]) LocationListLength.next();

			try{
				ObjectLocationWizard.setWizardId(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setFulfillmentCenterId(ObjectLocObj[GLOBALCONSTANT.IntOne].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setFulfillmentDesc50(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setWizardControlNumber(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setDescription50(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setNoOfLocations(Long.parseLong(ObjectLocObj[GLOBALCONSTANT.INT_FIVE].toString()));
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setLocationType(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setLocationProfile(ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setCreatedByTeamMember(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
			}catch(Exception obj_e)	{}
			try{
				String Date_Created=ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString();
			    ObjectLocationWizard.setDateCreated(Date_Created);
				
			}catch(Exception obj_e)	{}
			try{
				String lastDate=ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString();
			    ObjectLocationWizard.setLastActivityDate(lastDate);
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setLastActivityTeamMember(ObjectLocObj[GLOBALCONSTANT.INT_ELEVEN].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setStatus(ObjectLocObj[GLOBALCONSTANT.INT_TWELVE].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setTeamMemberId(ObjectLocObj[GLOBALCONSTANT.INT_THIRTEEN].toString());
			}catch(Exception obj_e)	{}
			try{
				String notes=ObjectLocObj[GLOBALCONSTANT.INT_FOURTEEN].toString();
				  StringBuilder sb = new StringBuilder();
				  for(int notesnumber=0,notescount=0;notesnumber<notes.length();notesnumber++,notescount++)
				  {
				   if(notes.charAt(notesnumber)!=GLOBALCONSTANT.BlankSpace && notescount==GLOBALCONSTANT.INT_FOURTYEIGHT)
				   {
				    sb.append(" "+notes.charAt(notesnumber));
				    notescount=0;
				   }else if(notes.charAt(notesnumber)==GLOBALCONSTANT.BlankSpace)
				   {
				    sb.append(notes.charAt(notesnumber));
				    notescount=0;
				   }
				    else{
				    sb.append(notes.charAt(notesnumber));
				   }
				  }
				  ObjectLocationWizard.setNotes(sb.toString());
				
			}
			catch(Exception obj_e){}
			
		listGridData.add(ObjectLocationWizard);
		}
	
		return listGridData;
	}

	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#getLocationWizardLevel(java.lang.String, java.lang.String)
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 7, 2015
		 * @Description : Set Level in getter Setters for populate levels
	 */
	
	@Transactional
	public LOCATIONWIZARDLABELSBE getLocationWizardLevel(String moduleName, String languageCode) throws JASCIEXCEPTION {
		
		List<LANGUAGES> languageLevelList=null;
		LOCATIONWIZARDLABELSBE objLocationWizardLebels = new LOCATIONWIZARDLABELSBE();
		languageLevelList=objLanguageLevelsAPI.GetScreenLabels(moduleName, languageCode);
		
		for (LANGUAGES objLanguages : languageLevelList) 
		{

			String StrKeyPhrase=objLanguages.getId().getKeyPhrase();
			String StrTranslation=objLanguages.getTranslation();
		
		
		
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Status))
			{
				objLocationWizardLebels.setKP_Status(StrTranslation);
			}
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			{
				
				objLocationWizardLebels.setKP_Mandatory_field_cannot_be_left_blank(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Type))
			{
				objLocationWizardLebels.setKP_Location_Type(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Zone))
			{
				objLocationWizardLebels.setKP_Work_Zone(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Group_Zone))
			{
				objLocationWizardLebels.setKP_WorkGroup_Zone(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Area))
			{
				objLocationWizardLebels.setKP_Area(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Long))
			{
				objLocationWizardLebels.setKP_Description_Long(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short))
			{
				objLocationWizardLebels.setKP_Description_Short(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Control_Number))
			{
				objLocationWizardLebels.setKP_Wizard_Control_Number(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_ID))
			{
				objLocationWizardLebels.setKP_Wizard_ID(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Applied_By))
			{
				objLocationWizardLebels.setKP_Applied_By(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_Center))
			{
				objLocationWizardLebels.setKP_Fulfillment_Center(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Created_Date))
			{
				objLocationWizardLebels.setKP_Created_Date(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Created_By))
			{
				objLocationWizardLebels.setKP_Created_By(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Applied_Date))
			{
				objLocationWizardLebels.setKP_Applied_Date(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Profile))
			{
				objLocationWizardLebels.setKP_Location_Profile(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Apply))
			{
				objLocationWizardLebels.setKP_Apply(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Notes))
			{
				objLocationWizardLebels.setKP_Notes(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Wizard_Maintenance))
			{
				objLocationWizardLebels.setKP_Location_Wizard_Maintenance(StrTranslation);
			}
		    
			// Location Wizard Lookup
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Type))
			{
				objLocationWizardLebels.setKP_Location_Type(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Any_Part_of_Wizard_Description))
			{
				objLocationWizardLebels.setKP_Any_Part_of_Wizard_Description(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Wizard_Lookup))
			{
				objLocationWizardLebels.setKP_Location_Wizard_Lookup(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
			{
				objLocationWizardLebels.setKP_New(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
			{
				objLocationWizardLebels.setKP_Display_All(StrTranslation);
			}
			// Location Wizard Search Lookup
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
			{
				objLocationWizardLebels.setKP_Description(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Locations))
			{
				objLocationWizardLebels.setKP_Number_of_Locations(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Copy))
			{
				objLocationWizardLebels.setKP_Copy(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete_Wizard_Only))
			{
				objLocationWizardLebels.setKP_Delete_Wizard_Only(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete_Wizard_And_Location))
			{
				objLocationWizardLebels.setKP_Delete_Wizard_And_Location(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Wizard_Search_Lookup))
			{
				objLocationWizardLebels.setKP_Location_Wizard_Search_Lookup(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
			{
				objLocationWizardLebels.setKP_Add_New(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
			{
				objLocationWizardLebels.setKP_Edit(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
			{
				objLocationWizardLebels.setKP_Actions(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
			{
				objLocationWizardLebels.setKP_Select(StrTranslation);
			}
			
			// Err Msg
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_WIZARD_CONTROL_NUMBER))
			{
				objLocationWizardLebels.setKP_ERR_INVALID_WIZARD_CONTROL_NUMBER(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Wizard_Control_Number))
			{
				objLocationWizardLebels.setKP_ERR_WIZARD_CONTROL_NUMBER_LEFT_BLANK(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_AREA))
			{
				objLocationWizardLebels.setKP_ERR_INVALID_AREA(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_AREA_LEFT_BLANK))
			{
				objLocationWizardLebels.setKP_ERR_AREA_LEFT_BLANK(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_LOCATION_TYPE))
			{
				objLocationWizardLebels.setKP_ERR_INVALID_LOCATION_TYPE(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_Select_Location_Type))
			{
				objLocationWizardLebels.setKP_ERR_LOCATION_TYPE_LEFT_BLANK(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION))
			{
				objLocationWizardLebels.setKP_ERR_INVALID_PART_OF_WIZARD_DESCRIPTION(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK))
			{
				objLocationWizardLebels.setKP_ERR_PART_OF_WIZARD_DESCRIPTION_LEFT_BLANK(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			{
				objLocationWizardLebels.setKP_ERR_MANDATORY_FIELD_LEFT_BLANK(StrTranslation);
			}
			
			//levels Row seperator
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Row_Number_of_Characters))
			{
				objLocationWizardLebels.setKP_Row_Number_of_Characters(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Row_Characters_Set))
			{
				objLocationWizardLebels.setKP_Row_Characters_Set(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Row_Starting_at))
			{
				objLocationWizardLebels.setKP_Row_Starting_at(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Row))
			{
				objLocationWizardLebels.setKP_Number_of_Row(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Level_Number_of_Characters))
			{
				objLocationWizardLebels.setKP_Level_Number_of_Characters(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Type))
			{
				objLocationWizardLebels.setKP_Type(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Separator_Character_Location))
			{
				objLocationWizardLebels.setKP_Separator_Character_Location(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Separator_Character_Location_Print_only))
			{
				objLocationWizardLebels.setKP_Separator_Character_Location_Print_only(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
			{
				objLocationWizardLebels.setKP_Save_Update(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
			{
				objLocationWizardLebels.setKP_Cancel(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Row_character_set))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Row_character_set(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Row_starting))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Row_starting(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Row_number_of))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Row_number_of(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Aisle_character_set))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Aisle_character_set(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Aisle_starting))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Aisle_starting(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Aisle_number_of))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Aisle_number_of(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Row_Aisle))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Row_Aisle(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Row_Aisle_Print))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Row_Aisle_Print(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Bay_character_set))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Bay_character_set(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Bay_starting))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Bay_starting(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Bay_number_of))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Bay_number_of(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Aisle_Bay))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Aisle_Bay(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Aisle_Bay_Print))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Aisle_Bay_Print(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_LEVEL_character_set))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_LEVEL_character_set(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_LEVEL_starting))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_LEVEL_starting(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_LEVEL_number_of))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_LEVEL_number_of(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Bay_Level))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Bay_Level(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Bay_Level_Print))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Bay_Level_Print(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Slot_character_set))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Slot_character_set(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Slot_starting))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Slot_starting(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Slot_number_of))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Slot_number_of(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Level_Slot))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Level_Slot(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_Please_Enter_Seperator_Level_Slot_Print))
			{
				objLocationWizardLebels.setKP_ERR_Please_Enter_Seperator_Level_Slot_Print(StrTranslation);
			}
			////
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_WIzard_is_about_to_be_deleted))
			{
				objLocationWizardLebels.setKP_Location_WIzard_is_about_to_be_deleted(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Deletion_will_remove_the_Wizard_from_the_locations))
			{
				objLocationWizardLebels.setKP_Deletion_will_remove_the_Wizard_from_the_locations(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_WIzard_and_its_Locations_are_about_to_be_deleted))
			{
				objLocationWizardLebels.setKP_Location_WIzard_and_its_Locations_are_about_to_be_deleted(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Deletion_will_not_remove_these))
			{
				objLocationWizardLebels.setKP_Deletion_will_not_remove_these(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Locations_and_Inventory_Records))
			{
				objLocationWizardLebels.setKP_Locations_and_Inventory_Records(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
			{
				objLocationWizardLebels.setKP_Record_has_been_saved_successfully(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Numeric_value_can_not_less_than_Zero_or_eual))
			{
				objLocationWizardLebels.setKP_Numeric_value_can_not_less_than_Zero_or_eual(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Numeric_Allowed_Only))
			{
				objLocationWizardLebels.setKP_Numeric_Allowed_Only(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
			{
				objLocationWizardLebels.setKP_Are_you_sure_you_want_to_delete_this_record(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Name))
			{
				objLocationWizardLebels.setKP_Wizard_Name(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_Name))
			{
				objLocationWizardLebels.setKP_Fulfillment_Name(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Only_Alphabets_Allowed))
			{
				objLocationWizardLebels.setKP_Only_Alphabets_Allowed(StrTranslation);
			}
			//
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_locations_created))
			{
				objLocationWizardLebels.setKP_Number_of_locations_created(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_duplicate_locations))
			{
				objLocationWizardLebels.setKP_Number_of_duplicate_locations(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Print))
			{
				objLocationWizardLebels.setKP_Print(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Numeric_Allowed_Only_Character_Set))
			{
				objLocationWizardLebels.setKP_Numeric_Allowed_Only_Character_Set(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_There_is_no_location_profile_for_selected_fulfillment_center))
			{
				objLocationWizardLebels.setKP_There_is_no_location_profile_for_selected_fulfillment_center(StrTranslation);
			}
			//
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Ok))
			{
				objLocationWizardLebels.setKP_Ok(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Download))
			{
				objLocationWizardLebels.setKP_Download(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_is_greater))
			{
				objLocationWizardLebels.setKP_Location_is_greater(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_characters_must_be_numeric_and_between_1_to_5_only))
			{
				objLocationWizardLebels.setKP_Number_of_characters_must_be_numeric_and_between_1_to_5_only(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_create_locations_in_reverse_order))
			{
				objLocationWizardLebels.setKP_You_are_not_allowed_to_create_locations_in_reverse_order(StrTranslation);
			}else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
			{
				objLocationWizardLebels.setKP_Reset(StrTranslation);
			}
			
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Row))
			{
				objLocationWizardLebels.setKP_Row(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Aisle))
			{
				objLocationWizardLebels.setKP_Aisle(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Bay))
			{
				objLocationWizardLebels.setKP_Bay(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Level))
			{
				objLocationWizardLebels.setKP_Level(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Slot))
			{
				objLocationWizardLebels.setKP_Slot(StrTranslation);
			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Equality_Check_Error_Msg))
			   {
			    objLocationWizardLebels.setKP_Equality_Check_Error_Msg(StrTranslation);
			   }
			
			
			

		}
		
		return objLocationWizardLebels;
	}
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#getDropDownList(java.lang.String)
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 9, 2015
		 * @Description : Return dropDown List
	 */
	
	@Transactional
	public List<GENERALCODES> getDropDownList(String GeneralCodeID) throws JASCIEXCEPTION {
		
		
		return objLocationwizardserviceapi.getDropDownValuesList(GeneralCodeID);
	}

	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#getDropDownValuesFulfillmentList()
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 9, 2015
		 * @Description :get list for fulfillment Dropdown
	 */
	
	@Transactional
	public List<FULFILLMENTCENTERSPOJO> getDropDownValuesFulfillmentList() throws JASCIEXCEPTION {
		
		return objLocationwizardserviceapi.getDropDownValuesFulfillmentList();
	}
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#getLocationWizardData(java.lang.String)
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 13, 2015     
		 * @Description :given grid data
	 */
	@Transactional
	public List<LOCATIONWIZARDBE> getLocationWizardData(String wizardID) throws JASCIEXCEPTION {
		
		List<Object> objL=null;
		List<LOCATIONWIZARDBE> listGridData=null;
		try
		{
		objL=objLocationwizardserviceapi.getLocationWizardData(wizardID);
		listGridData=new ArrayList<LOCATIONWIZARDBE>();
		
		for(Iterator<Object> LocationListLength=objL.iterator();LocationListLength.hasNext(); ){

			LOCATIONWIZARDBE ObjectLocationWizard=new LOCATIONWIZARDBE();

			Object []ObjectLocObj =  (Object[]) LocationListLength.next();

			try{
				ObjectLocationWizard.setWizardId(ObjectLocObj[GLOBALCONSTANT.INT_ZERO].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setFulfillmentCenterId(ObjectLocObj[GLOBALCONSTANT.IntOne].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setFulfillmentDesc50(ObjectLocObj[GLOBALCONSTANT.INT_TWO].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setWizardControlNumber(ObjectLocObj[GLOBALCONSTANT.INT_THREE].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setDescription50(ObjectLocObj[GLOBALCONSTANT.INT_FOUR].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setNoOfLocations(Long.parseLong(ObjectLocObj[GLOBALCONSTANT.INT_FIVE].toString()));
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setLocationType(ObjectLocObj[GLOBALCONSTANT.INT_SIX].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setLocationProfile(ObjectLocObj[GLOBALCONSTANT.INT_SEVEN].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setCreatedByTeamMember(ObjectLocObj[GLOBALCONSTANT.INT_EIGHT].toString());
			}catch(Exception obj_e)	{}
			try{
				String createdDate=ObjectLocObj[GLOBALCONSTANT.INT_NINE].toString();
				ObjectLocationWizard.setDateCreated(createdDate);
			}catch(Exception obj_e)	
			{
				obj_e.printStackTrace();
			}
			try{
				String lastDate=ObjectLocObj[GLOBALCONSTANT.INT_TEN].toString();
				ObjectLocationWizard.setLastActivityDate(lastDate);
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setLastActivityTeamMember(ObjectLocObj[GLOBALCONSTANT.INT_ELEVEN].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setStatus(ObjectLocObj[GLOBALCONSTANT.INT_TWELVE].toString());
			}catch(Exception obj_e)	{}
			try{
				ObjectLocationWizard.setTeamMemberId(ObjectLocObj[GLOBALCONSTANT.INT_THIRTEEN].toString());
			}catch(Exception obj_e)	{}
			try{
				String notes=ObjectLocObj[GLOBALCONSTANT.INT_FOURTEEN].toString();
				  StringBuilder sb = new StringBuilder();
				  for(int notesnumber=0,notescount=0;notesnumber<notes.length();notesnumber++,notescount++)
				  {
				   if(notes.charAt(notesnumber)!=GLOBALCONSTANT.BlankSpace && notescount==GLOBALCONSTANT.INT_FOURTYEIGHT)
				   {
				    sb.append(GLOBALCONSTANT.Single_Space+notes.charAt(notesnumber));
				    notescount=0;
				   }else if(notes.charAt(notesnumber)==GLOBALCONSTANT.BlankSpace)
				   {
				    sb.append(notes.charAt(notesnumber));
				    notescount=0;
				   }
				    else{
				    sb.append(notes.charAt(notesnumber));
				   }
				  }
				  ObjectLocationWizard.setNotes(sb.toString());
				
			}
			catch(Exception obj_e){}
		listGridData.add(ObjectLocationWizard);
		}
		
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		return listGridData;
	}
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#saveAndUpdate(com.jasci.biz.AdminModule.model.LOCATIONWIZARD)
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 13, 2015
		 * @Description :used for save and update
	 */
	
	@Transactional
	public List<Object> saveAndUpdate(LOCATIONWIZARDBE objLocationwizardBE) throws JASCIEXCEPTION {
		
		String StrTenant=objCommonsessionbe.getTenant();
		String StrCompany=objCommonsessionbe.getCompany();
		Date CurrentDate=null;
		try
		{
		
		  DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelp_DateFormat);
		  CurrentDate = ObjDateFormat.parse(objLocationwizardBE.getDateCreated());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		  
		LOCATIONWIZARD objLocationwizard=new LOCATIONWIZARD();
		LOCATIONWIZARDPK objLocationwizardpk=new LOCATIONWIZARDPK();
		objLocationwizardpk.setCOMPANY_ID(StrCompany);
		objLocationwizardpk.setFULFILLMENT_CENTER_ID(objLocationwizardBE.getFulfillmentCenterId());
		objLocationwizardpk.setTENANT_ID(StrTenant);
		objLocationwizardpk.setWIZARD_CONTROL_NUMBER(Long.parseLong(objLocationwizardBE.getWizardControlNumber()));
		objLocationwizardpk.setWIZARD_ID(objLocationwizardBE.getWizardId());
		objLocationwizard.setId(objLocationwizardpk);
		objLocationwizard.setDESCRIPTION20(objLocationwizardBE.getDescription20());
		objLocationwizard.setDESCRIPTION50(objLocationwizardBE.getDescription50());
		objLocationwizard.setAREA(objLocationwizardBE.getArea());
		objLocationwizard.setWORK_GROUP_ZONE(objLocationwizardBE.getWorkGroupZone());
		objLocationwizard.setWORK_ZONE(objLocationwizardBE.getWorkZone());
		objLocationwizard.setLOCATION_TYPE(objLocationwizardBE.getLocationType());
		objLocationwizard.setLOCATION_PROFILE(objLocationwizardBE.getLocationProfile());
		objLocationwizard.setROW_NUMBER_CHARACTERS(objLocationwizardBE.getRowNumberCharacters());
		objLocationwizard.setROW_CHARACTER_SET(objLocationwizardBE.getRowCharacterSet());
		objLocationwizard.setROW_NUMBER_OF(objLocationwizardBE.getRowNumberOf());
		objLocationwizard.setROW_STARTING(objLocationwizardBE.getRowStarting());
		objLocationwizard.setAISLE_NUMBER_CHARACTERS(objLocationwizardBE.getAisleNumberCharacters());
		objLocationwizard.setAISLE_CHARACTER_SET(objLocationwizardBE.getAisleCharacterSet());
		objLocationwizard.setAISLE_STARTING(objLocationwizardBE.getAisleStarting());
		objLocationwizard.setAISLE_NUMBER_OF(objLocationwizardBE.getAisleNumberOf());
		objLocationwizard.setBAY_NUMBER_CHARACTERS(objLocationwizardBE.getBayNumberCharacters());
		objLocationwizard.setBAY_CHARACTER_SET(objLocationwizardBE.getBayCharacterSet());
		objLocationwizard.setBAY_STARTING(objLocationwizardBE.getBayStarting());
		objLocationwizard.setBAY_NUMBER_OF(objLocationwizardBE.getBayNumberOf());
		objLocationwizard.setLEVEL_NUMBER_CHARACTERS(objLocationwizardBE.getLevelNumberCharacters());
		objLocationwizard.setLEVEL_CHARACTER_SET(objLocationwizardBE.getLevelCharacterSet());
		objLocationwizard.setLEVEL_STARTING(objLocationwizardBE.getLevelStarting());
		objLocationwizard.setLEVEL_NUMBER_OF(objLocationwizardBE.getLevelNumberOf());
		objLocationwizard.setSLOT_NUMBER_CHARACTERS(objLocationwizardBE.getSlotNumberCharacters());
		objLocationwizard.setSLOT_CHARACTER_SET(objLocationwizardBE.getSlotCharacterSet());
		objLocationwizard.setSLOT_STARTING(objLocationwizardBE.getSlotStarting());
		objLocationwizard.setSLOT_NUMBER_OF(objLocationwizardBE.getSlotNumberOf());
		objLocationwizard.setSEPARATOR_ROW_AISLE(objLocationwizardBE.getSeparatorRowAisle());
		objLocationwizard.setSEPARATOR_ROW_AISLE_PRINT(objLocationwizardBE.getSeparatorRowAislePrint());
		objLocationwizard.setSEPARATOR_AISLE_BAY(objLocationwizardBE.getSeparatorAisleBay());
		objLocationwizard.setSEPARATOR_AISLE_BAY_PRINT(objLocationwizardBE.getSeparatorAislebayPrint());
		objLocationwizard.setSEPARATOR_BAY_LEVEL(objLocationwizardBE.getSeparatorBayLevel());
		objLocationwizard.setSEPARATOR_BAY_LEVEL_PRINT(objLocationwizardBE.getSeparatorBaylevelPrint());
		objLocationwizard.setSEPARATOR_LEVEL_SLOT(objLocationwizardBE.getSeparatorLevelSlot());
		objLocationwizard.setSEPARATOR_LEVEL_SLOT_PRINT(objLocationwizardBE.getSeparatorLevelSlotPrint());
		objLocationwizard.setLAST_ACTIVITY_DATE(CurrentDate);
		objLocationwizard.setLAST_ACTIVITY_TEAM_MEMBER(objCommonsessionbe.getTeam_Member());

		objLocationwizard.setCREATED_BY_TEAM_MEMBER(objCommonsessionbe.getTeam_Member());
		objLocationwizard.setDATE_CREATED(CurrentDate);

		

		
		List<Object> combineRecord=objLocationwizardserviceapi.saveAndUpdate(objLocationwizard);
		List<Object> recordList=new ArrayList<Object>();
		if(combineRecord.size()>GLOBALCONSTANT.IntOne)
		{
		if(!combineRecord.isEmpty())
			
		{
		long savedList= (Long) combineRecord.get(GLOBALCONSTANT.INT_ZERO);
		long duplicateList= (Long) combineRecord.get(GLOBALCONSTANT.IntOne);
		String DuplicateLocFileName  = GLOBALCONSTANT.BlankString;
		try{
		DuplicateLocFileName =combineRecord.get(combineRecord.size()-GLOBALCONSTANT.IntOne).toString();
		}catch(Exception obj_ex){
			DuplicateLocFileName =GLOBALCONSTANT.BlankString;
		}
		
		
		recordList.add(GLOBALCONSTANT.INT_ZERO,duplicateList);

		if(savedList==GLOBALCONSTANT.INT_ZERO)
		{
			recordList.add(GLOBALCONSTANT.IntOne,GLOBALCONSTANT.INT_ZERO);
		}
		else
		{
			recordList.add(GLOBALCONSTANT.IntOne,savedList);
		}
		if(duplicateList==GLOBALCONSTANT.INT_ZERO)
		{
			recordList.add(GLOBALCONSTANT.INT_TWO,GLOBALCONSTANT.INT_ZERO);
			recordList.add(GLOBALCONSTANT.INT_THREE,GLOBALCONSTANT.INT_ZERO);
		}
		else
		{
			recordList.add(GLOBALCONSTANT.INT_TWO,duplicateList);
			recordList.add(GLOBALCONSTANT.INT_THREE,DuplicateLocFileName);
		}
		}
		return recordList;
		}
		else
		{
			return combineRecord;
		}
	}

	/**
	 * 
		 * 
		 * @author: Pradeep Kumar
		 * @Date :Jan 20, 2015
		 * @Description :get Location profile dwopdown values
		 * @see :@see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#getDropDownValueLocationProfile(java.lang.String)
	 */
	public List<LOCATIONPROFILES> getDropDownValueLocationProfile(String fulfilmentCenter) throws JASCIEXCEPTION {
		
		return objLocationwizardserviceapi.getDropDownValueLocationProfile(fulfilmentCenter);
	}
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#getLocationWizardCopy(java.lang.String, java.lang.String, java.lang.String)
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 14, 2015
		 * @Description : set data on be for copy on maintenance screen
	 */
	public LOCATIONWIZARDBE getLocationWizardCopyEdit(String wizardid, String wizardControlNo, String fulfillmentId,String copyOrEdit)
			throws JASCIEXCEPTION {
		
		LOCATIONWIZARDBE objLocationWizardBE = new LOCATIONWIZARDBE();
	List<LOCATIONWIZARD> ListLocationWIzard=null;
		
	String StrTenant=objCommonsessionbe.getTenant();
	String StrCompany=objCommonsessionbe.getCompany();
		
			
		ListLocationWIzard =objLocationwizardserviceapi.getLocationWizardCopyEdit(wizardid, wizardControlNo,fulfillmentId,StrTenant,StrCompany,copyOrEdit);
			
		
		
		for (LOCATIONWIZARD ObjLocationWizard : ListLocationWIzard) 
		{
			String StrDate_created=ConvertDate(ObjLocationWizard.getDATE_CREATED().toString(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format);
			if(ObjLocationWizard.getLAST_ACTIVITY_DATE()!=null)
			{
			String StrLast_Activity_date=ConvertDate(ObjLocationWizard.getLAST_ACTIVITY_DATE().toString(), GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.MenuMessage_yyyyMMdd_Format);
			objLocationWizardBE.setLastActivityDate(StrLast_Activity_date);
			String StrLastActiveTeamMember=getTeamMemberName(objCommonsessionbe.getTenant(), ObjLocationWizard.getLAST_ACTIVITY_TEAM_MEMBER()).toString();
			objLocationWizardBE.setLastActivityTeamMember(StrLastActiveTeamMember);
			}
			objLocationWizardBE.setDateCreated(StrDate_created);
			
			objLocationWizardBE.setWizardId(ObjLocationWizard.getId().getWIZARD_ID());
			objLocationWizardBE.setDescription20(StringEscapeUtils.escapeHtml(ObjLocationWizard.getDESCRIPTION20()));
			
			objLocationWizardBE.setDescription50(StringEscapeUtils.escapeHtml(ObjLocationWizard.getDESCRIPTION50()));
			
			objLocationWizardBE.setArea(StringEscapeUtils.escapeHtml(ObjLocationWizard.getAREA()));
			objLocationWizardBE.setFulfillmentCenterId(StringEscapeUtils.escapeHtml(ObjLocationWizard.getId().getFULFILLMENT_CENTER_ID()));
			objLocationWizardBE.setWizardId(ObjLocationWizard.getId().getWIZARD_ID());
			objLocationWizardBE.setWorkZone(StringEscapeUtils.escapeHtml(ObjLocationWizard.getWORK_ZONE()));
			objLocationWizardBE.setWorkGroupZone(StringEscapeUtils.escapeHtml(ObjLocationWizard.getWORK_GROUP_ZONE()));
			objLocationWizardBE.setLocationType(StringEscapeUtils.escapeHtml(ObjLocationWizard.getLOCATION_TYPE()));
			objLocationWizardBE.setLocationProfile(StringEscapeUtils.escapeHtml(ObjLocationWizard.getLOCATION_PROFILE()));
			String StrcreatedTeamMember=getTeamMemberName(objCommonsessionbe.getTenant(), ObjLocationWizard.getCREATED_BY_TEAM_MEMBER()).toString();
			objLocationWizardBE.setCreatedByTeamMember(StrcreatedTeamMember);
			
			
			objLocationWizardBE.setRowNumberCharacters(ObjLocationWizard.getROW_NUMBER_CHARACTERS());
			objLocationWizardBE.setRowCharacterSet(ObjLocationWizard.getROW_CHARACTER_SET());
			objLocationWizardBE.setRowStarting(ObjLocationWizard.getROW_STARTING());
			objLocationWizardBE.setRowNumberOf(ObjLocationWizard.getROW_NUMBER_OF());
			objLocationWizardBE.setAisleNumberCharacters(ObjLocationWizard.getAISLE_NUMBER_CHARACTERS());
			objLocationWizardBE.setAisleCharacterSet(ObjLocationWizard.getAISLE_CHARACTER_SET());
			objLocationWizardBE.setAisleStarting(ObjLocationWizard.getAISLE_STARTING());
			objLocationWizardBE.setAisleNumberOf(ObjLocationWizard.getAISLE_NUMBER_OF());
			objLocationWizardBE.setBayNumberCharacters(ObjLocationWizard.getBAY_NUMBER_CHARACTERS());
			objLocationWizardBE.setBayCharacterSet(ObjLocationWizard.getBAY_CHARACTER_SET());
			objLocationWizardBE.setBayStarting(ObjLocationWizard.getBAY_STARTING());
			objLocationWizardBE.setBayNumberOf(ObjLocationWizard.getBAY_NUMBER_OF());
			objLocationWizardBE.setLevelNumberCharacters(ObjLocationWizard.getLEVEL_NUMBER_CHARACTERS());
			objLocationWizardBE.setLevelCharacterSet(ObjLocationWizard.getLEVEL_CHARACTER_SET());
			objLocationWizardBE.setLevelStarting(ObjLocationWizard.getLEVEL_STARTING());
			objLocationWizardBE.setLevelNumberOf(ObjLocationWizard.getLEVEL_NUMBER_OF());
			objLocationWizardBE.setSlotNumberCharacters(ObjLocationWizard.getSLOT_NUMBER_CHARACTERS());
			objLocationWizardBE.setSlotCharacterSet(ObjLocationWizard.getSLOT_CHARACTER_SET());
			objLocationWizardBE.setSlotStarting(ObjLocationWizard.getSLOT_STARTING());
			objLocationWizardBE.setSlotNumberOf(ObjLocationWizard.getSLOT_NUMBER_OF());
			objLocationWizardBE.setSeparatorRowAisle(ObjLocationWizard.getSEPARATOR_ROW_AISLE());
			objLocationWizardBE.setSeparatorAisleBay(ObjLocationWizard.getSEPARATOR_AISLE_BAY());
			objLocationWizardBE.setSeparatorBayLevel(ObjLocationWizard.getSEPARATOR_BAY_LEVEL());
			objLocationWizardBE.setSeparatorLevelSlot(ObjLocationWizard.getSEPARATOR_LEVEL_SLOT());
			objLocationWizardBE.setSeparatorRowAislePrint(ObjLocationWizard.getSEPARATOR_ROW_AISLE_PRINT());
			objLocationWizardBE.setSeparatorAislebayPrint(ObjLocationWizard.getSEPARATOR_AISLE_BAY_PRINT());
			objLocationWizardBE.setSeparatorBaylevelPrint(ObjLocationWizard.getSEPARATOR_BAY_LEVEL_PRINT());
			objLocationWizardBE.setSeparatorLevelSlotPrint(ObjLocationWizard.getSEPARATOR_LEVEL_SLOT_PRINT());
			objLocationWizardBE.setStatus(ObjLocationWizard.getSTATUS());
			objLocationWizardBE.setWizardControlNumber(String.valueOf(ObjLocationWizard.getId().getWIZARD_CONTROL_NUMBER()));
			
		}
		return objLocationWizardBE;
	}

	/**
	 * 
		  * @author: Pradeep Kumar
		  * @Date :Jan 20, 2015
		  * Description :delete wizard Only
		  * @param 
		  * @description :call deletion only for wizard method
		  * @see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#deleteWizardOnly(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void deleteWizardOnly(String wizardID, String wizardControlNumber, String fulfillmentID)
			throws JASCIEXCEPTION {
		String StrTenant=objCommonsessionbe.getTenant();
		String StrCompany=objCommonsessionbe.getCompany();
		
		objLocationwizardserviceapi.deleteWizardOnly(wizardID, wizardControlNumber, fulfillmentID, StrTenant, StrCompany);
	}

	/**
	 * 
		 * 
		 * @author: Pradeep Kumar
		 * @Date :Mar 20, 2015
		 * @Description :used deletion for wizard and location also
		 * @see :@see com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE#deleteWizardAndLocation(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void deleteWizardAndLocation(String wizardid, String wizardControlNo, String fulfillmentId, String Data)
			throws JASCIEXCEPTION {
		String StrTenant=objCommonsessionbe.getTenant();
		String StrCompany=objCommonsessionbe.getCompany();
		
		objLocationwizardserviceapi.deleteWizardAndLocation(wizardid, wizardControlNo, fulfillmentId, StrTenant, StrCompany);
		
	}
	
	/**
	  * @author Pradeep Kumar
	  * @Date Jan 20, 2015
	  * Description This is used to convert date format
	  * @param dateInString
	  * @param StrInFormat
	  * @param StrOutFormat
	  * @return String
	  */
	 public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
	 {
		 Date date=null;
	  SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
	  DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
	  String StrFormattedString=dateInString ;
	  
	  try {
	  
	   date = formatter.parse(dateInString);
	   StrFormattedString=dateFormat.format(date);
	  } catch (Exception e) {
	   
	  }
	  
	  return StrFormattedString;
	  
	 }

	 /**
	  * 
	 	 * 
	 		 * Created By:Pradeep Kumar
	 		 * @return :long
	 		 * @throws JASCIEXCEPTION
	 		 * @Date : Jan 21, 2015
	 		 * @Description : Max wizard  Control no adding 1 
	  */
	public long getMaxWizardControlNumber() throws JASCIEXCEPTION {
		
		
		LOCATIONWIZARD objlocationWizard=objLocationwizardserviceapi.getMaxWizardControlNumber();
		
		long maxWizardControlNo=(objlocationWizard.getId().getWIZARD_CONTROL_NUMBER());
				
		return maxWizardControlNo;
				
	}
	

	/**
     * 
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public String getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
		
		return objLocationwizardserviceapi.getTeamMemberName(Tenant, TeamMember);
				
	}
	
}
