/**

Date Developed :JAN 13 2014
Created by: sarvendra tyagi
Description :Menu Profile Assigment service Interface class for Business logic Unimplemented method
 */
package com.jasci.biz.AdminModule.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.MENUPROFILEASSIGMENTSERVICEAPI;
import com.jasci.biz.AdminModule.be.MENUPROFILEASSIGMENTLABELBE;
import com.jasci.biz.AdminModule.be.MENUPROFILEASSIGNMENTPOJOBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class MENUPROFILEASSIGMENTSERVICEIMPL implements IMENUPROFILEASSIGMENTSERVICE{

	
	@Autowired
	MENUPROFILEASSIGMENTSERVICEAPI objMenuprofileassigmentserviceapi;
	
	@Autowired
	LANGUANGELABELSAPI objLanguageApi;
	
	@Autowired
	COMMONSESSIONBE objCommonSession;
		
		
		
	/** 
	 * @Description  : This function is used for getting Team Member list
	 * @param        : String TeamMemberID,String searchField
	 * @return       : TEAMMEMBERS
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	
	@Transactional
	public List<MENUPROFILEASSIGNMENTPOJOBE> getTeamMemberList(String SearchValue,String SearchField)throws JASCIEXCEPTION{
		
		
		List<MENUPROFILEASSIGNMENTPOJOBE> objMenuprofileassignmentpojobes=null;
		MENUPROFILEASSIGNMENTPOJOBE objMenuprofileassignmentpojobe=null;
		List<Object> listObject=null;
		try{
		objMenuprofileassignmentpojobes=new ArrayList<MENUPROFILEASSIGNMENTPOJOBE>();
		
		if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberName) && !(SearchValue.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){
			
			listObject=objMenuprofileassigmentserviceapi.getPartTeamMemberNameList(SearchValue);
			
					
		}else if(SearchField.equalsIgnoreCase(GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberID)  && !(SearchValue.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){
			
			listObject=objMenuprofileassigmentserviceapi.getTeamMemberIDList(SearchValue);
		
		}else{
			
			listObject=objMenuprofileassigmentserviceapi.getAllTeamMemberList();
		}
		
		
		Object[] ObjObject = null;

		if(!(listObject.isEmpty()))
		{

		for(Object teamMember : listObject){
			ObjObject=(Object[]) teamMember;
			
			objMenuprofileassignmentpojobe=new MENUPROFILEASSIGNMENTPOJOBE();
			try{
				
			objMenuprofileassignmentpojobe.setTenant_ID(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
			
			}catch(Exception objException){
				objMenuprofileassignmentpojobe.setTenant_ID(GLOBALCONSTANT.BlankString);
			}
			
			try{
				objMenuprofileassignmentpojobe.setTeamMemberID(ObjObject[GLOBALCONSTANT.IntOne].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setTeamMemberID(GLOBALCONSTANT.BlankString);
				}
			
			try{
				objMenuprofileassignmentpojobe.setFirstName(ObjObject[GLOBALCONSTANT.INT_THREE ].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setFirstName(GLOBALCONSTANT.BlankString);
				}
			
			try{
				objMenuprofileassignmentpojobe.setLastName(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setLastName(GLOBALCONSTANT.BlankString);
				}
			
			try{
				objMenuprofileassignmentpojobe.setMiddleName(ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setMiddleName(GLOBALCONSTANT.BlankString);
				}
			
			try{
				objMenuprofileassignmentpojobe.setCurrentStatus(ObjObject[GLOBALCONSTANT.INT_FIVE].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setCurrentStatus(GLOBALCONSTANT.BlankString);
				}
			
			try{
				objMenuprofileassignmentpojobe.setProfile(ObjObject[GLOBALCONSTANT.INT_SIX].toString());
				
				}catch(Exception objException){
					objMenuprofileassignmentpojobe.setProfile(GLOBALCONSTANT.BlankString);
				}
			
			if(objMenuprofileassignmentpojobe.getFirstName()!=null && objMenuprofileassignmentpojobe.getMiddleName() !=null
					&& objMenuprofileassignmentpojobe.getLastName()!=null){
				
				if(objMenuprofileassignmentpojobe.getMiddleName().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
					
					objMenuprofileassignmentpojobe.setName(objMenuprofileassignmentpojobe.getFirstName()+GLOBALCONSTANT.Single_Space+
							objMenuprofileassignmentpojobe.getLastName());
				}else{
					

					objMenuprofileassignmentpojobe.setName(objMenuprofileassignmentpojobe.getFirstName()+GLOBALCONSTANT.Single_Space+
							objMenuprofileassignmentpojobe.getMiddleName()+GLOBALCONSTANT.Single_Space
							+objMenuprofileassignmentpojobe.getLastName());
				}
			}
				
			objMenuprofileassignmentpojobes.add(objMenuprofileassignmentpojobe);
		}
		}
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return objMenuprofileassignmentpojobes;
	}

	
	
	
	
	/** 
	 * @Description  : This function is used for getting MenuOption
	 * @param        : String menuType
	 * @return       : objMENUPROFILEASSIGNMENTPOJOBEList
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	
	
	public List<MENUPROFILEASSIGNMENTPOJOBE> getMenuProfileList(String menuType)throws JASCIEXCEPTION{
		
		return objMenuprofileassigmentserviceapi.getMenuProfileList(menuType);
	}
	
	
	
	
	/** 
	 * @Description  : This Method is used for assigned menu profile from menu profile Assignmenttable
	 * @param        :String strTeamMemberID,String tenant
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public Boolean getAllAssignedMenuProfileDelete(String strTeamMemberID,String tenant)throws JASCIEXCEPTION{
		
		return objMenuprofileassigmentserviceapi.getMenuProfileDelete(tenant, strTeamMemberID);
	}
	
	
	
	/** 
	 * @Description  : This function is used for getting labels of screen
	 * @param        : String TeamMemberID,String searchField
	 * @return       : objLANGUAGES
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 15 2015
	 */
	@Transactional
	public MENUPROFILEASSIGMENTLABELBE getScreenLabel(String language) throws JASCIEXCEPTION {
		
		MENUPROFILEASSIGMENTLABELBE objMenuprofileassigmentlabelbe=null;
		List<LANGUAGES> objLanguagesList=null;
		try{
			
			objMenuprofileassigmentlabelbe=new MENUPROFILEASSIGMENTLABELBE();
			objLanguagesList=objLanguageApi.GetScreenLabels(LANGUANGELABELSAPI.StrMenuProfileAssignmentModule, language);
			for(LANGUAGES objLanguages:objLanguagesList){
				
				if(LANGUANGELABELSAPI.KP_Actions.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Actions(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Edit.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Edit(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Add_New.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Add_New(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Delete.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Delete(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Cancel.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Cancel(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Save_Update.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Save_Update(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Select.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					
					objMenuprofileassigmentlabelbe.setLbl_Select(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Select_Menu_Type.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Select_Menu_Type(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Mandatory_field_cannot_be_left_blank(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Team_Member_ID.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Invalid_Team_Member_ID(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Part_of_Team_Member_Name.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Invalid_part_of_Team_Member_Name(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_a_Team_Member_ID.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Please_enter_a_Team_Member_ID(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_part_of_the_Team_Member_Name.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					
					objMenuprofileassigmentlabelbe.setLbl_Please_enter_part_of_Team_Member_Name(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Display_All.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Display_All(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Available_Menu_Profiles.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Available_Menu_Profiles(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profiles_Assigned.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Menu_Profiles_Assigned(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profiles_Assignment_Lookup.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Menu_Profiles_Assignment_Lookup(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profiles_Assignment_Maintenance.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Menu_Profiles_Assignment_Maintenance(objLanguages.getTranslation());
				
				}else if(LANGUANGELABELSAPI.KP_Team_Member_Menu_Profile_Assignment_SEARCH_LOOKUP.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Menu_Profiles_Assignment_SEARCHLOOKUP(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Team_Member_Id.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Team_Member_ID(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Part_of_the_Team_Member_Name.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Part_of_the_Team_Member_Name(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Name.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Name(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Description(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Menu_Profile.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Menu_profile(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_MenuType.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_MenuType(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Team_Member.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Team_Member(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Company.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Company(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Are_you_sure_you_want_to_delete_row(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Your_record_has_been_updated_successfully(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Save_Update.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_Save_Update(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_No_menu_profile_has_been_assigned_to_the_TeamMember.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLbl_No_menu_profile_has_been_assigned_to_the_TeamMember(objLanguages.getTranslation());	
				
				}else if(LANGUANGELABELSAPI.KP_Reset.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					objMenuprofileassigmentlabelbe.setLblReset(objLanguages.getTranslation());
				}else if(LANGUANGELABELSAPI.KP_Sort_First_Name.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLblSort_First_Name(objLanguages.getTranslation());
				}else if(LANGUANGELABELSAPI.KP_Sort_Last_Name.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofileassigmentlabelbe.setLblSort_Last_Name(objLanguages.getTranslation());
				}
				
				
				
			}
			
			
			
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return objMenuprofileassigmentlabelbe;
	}
	
	

	
	
	/** 
	 * @Description  : This Method is used for assigned menu profile saved records in Menu Profile assignment table
	 * @param        :objMenuprofileassignment
	 * @return       : Boolean
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	@Transactional
	public Boolean SaveAndUpdate(String TeamMemberID,String strMenuProfileJson,String strDate)throws JASCIEXCEPTION{
		
		Boolean status=false;
		getAllAssignedMenuProfileDelete(TeamMemberID,objCommonSession.getTenant());
		
		status=objMenuprofileassigmentserviceapi.SaveAndUpdate(TeamMemberID, strMenuProfileJson,strDate);
		
		return status;
	}
	
	
	
	
	
	/** 
	 * @Description  : This Method is used for get team member name
	 * @param        :objMenuprofileassignment
	 * @return       : objMenuprofileassignmentpojobe
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	@Transactional
	public MENUPROFILEASSIGNMENTPOJOBE getTeamMemberName(String TeamMemberId)throws JASCIEXCEPTION{
		
		MENUPROFILEASSIGNMENTPOJOBE objMenuprofileassignmentpojobe=null;
		List<Object> objtTeammemberList=null;
		
		try{
			
			objMenuprofileassignmentpojobe=new MENUPROFILEASSIGNMENTPOJOBE();
			
			objtTeammemberList=objMenuprofileassigmentserviceapi.getTeamMemberName(TeamMemberId);	
			Object[] ObjObject = null;

			if(!(objtTeammemberList.isEmpty()))
			{

			for(Object teamMember : objtTeammemberList){
				ObjObject=(Object[]) teamMember;
			

			if(ObjObject[GLOBALCONSTANT.INT_ZERO]!=null && ObjObject[GLOBALCONSTANT.IntOne] !=null
					&& ObjObject[GLOBALCONSTANT.INT_TWO]!=null){
				
				if(ObjObject[GLOBALCONSTANT.IntOne].toString().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
					
					objMenuprofileassignmentpojobe.setName(ObjObject[GLOBALCONSTANT.INT_ZERO].toString()+GLOBALCONSTANT.Single_Space+
							ObjObject[GLOBALCONSTANT.INT_TWO].toString());
				}else{
					

					objMenuprofileassignmentpojobe.setName(ObjObject[GLOBALCONSTANT.INT_ZERO].toString()+GLOBALCONSTANT.Single_Space+
							ObjObject[GLOBALCONSTANT.IntOne].toString()+GLOBALCONSTANT.Single_Space
							+ObjObject[GLOBALCONSTANT.INT_TWO].toString());
				}
			}else if(ObjObject[GLOBALCONSTANT.INT_ZERO]!=null && ObjObject[GLOBALCONSTANT.IntOne] ==null
					&& ObjObject[GLOBALCONSTANT.INT_TWO]!=null){
				objMenuprofileassignmentpojobe.setName(ObjObject[GLOBALCONSTANT.INT_ZERO].toString()+GLOBALCONSTANT.Single_Space+
						ObjObject[GLOBALCONSTANT.INT_TWO].toString());
			}
			}
			}
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		
		return objMenuprofileassignmentpojobe;
	}
	
		
	
}
