/**

Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :LANGUAGE TRANSLATION ServiceImpl is implementation of ILANGUAGETRANSLATIONSERVICE interface.
 */
package com.jasci.biz.AdminModule.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUAGETRANSLATIONSERVICEAPI;
import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONBE;
import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONSCREENLABELSBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class LANGUAGETRANSLATIONSERVICEIMPL implements ILANGUAGETRANSLATIONSERVICE
{
	
	@Autowired
	LANGUAGETRANSLATIONSERVICEAPI objRestLanguageTranslation;
	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to get language list  from General code based on tenant,company,generalCodeId
	 * Input parameter: String
	 * Return Type :List<LANGUAGETRANSLATIONBE>
	 * 
	 */
	@Transactional
	public List<LANGUAGETRANSLATIONBE> getLanguageList(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION
	{
			return objRestLanguageTranslation.getLanguageList(Tenant, Company,GeneralCodeId);	
	}



	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to add a record to languages table.
	 * Input parameter: LANGUAGETRANSLATIONBE
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	public Boolean addLanguageData(LANGUAGETRANSLATIONBE objLanguageBe) throws  JASCIEXCEPTION
	{
		Boolean Status =objRestLanguageTranslation.addLanguageData(objLanguageBe);
		return Status;
		
	}


	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to get language data on the basis of Language from languages table.
	 * Input parameter: String
	 * Return Type :LANGUAGETRANSLATIONBE
	 * 
	 */
	@Transactional
	public List<LANGUAGETRANSLATIONBE> GetLanguageDataOnLanguage(String strSelectedLanguage,String Tenant, String Company,String GeneralCodeId) throws  JASCIEXCEPTION
	{
		return objRestLanguageTranslation.GetLanguageData(strSelectedLanguage,Tenant,Company,GeneralCodeId);
	}


	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to get language data on the basis of KeyPhrase from languages table.
	 * Input parameter: String
	 * Return Type :LANGUAGETRANSLATIONBE
	 * 
	 */
	@Transactional
	public List<LANGUAGETRANSLATIONBE> GetLanguageDataOnKeyPhrase(String strKeyPhrase,String Tenant, String Company,String GeneralCodeId) throws  JASCIEXCEPTION
	{
		return objRestLanguageTranslation.GetKeyPhrase(strKeyPhrase,Tenant,Company,GeneralCodeId);
	}


	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to delete a record from languages table.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	
	@Transactional
	public Boolean DeleteLanguage(String language, String keyPhrase) throws  JASCIEXCEPTION
	{
	    Boolean Status=false;
	    Status=objRestLanguageTranslation.DeleteLanguage(language,keyPhrase);
		return Status;
	}
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to check if the Language is valid.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	
	@Transactional
	public  Boolean CheckLanguage( String Language) throws  JASCIEXCEPTION
	{	
       
        Boolean Result=objRestLanguageTranslation.CheckLanguage(Language);
		return Result;
			
		
	
	}
	
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to Check if the keyPhrase is valid .
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	
	@Transactional

	public Boolean CheckKeyPhrase(String KeyPhrase) throws  JASCIEXCEPTION
	{	
      
        Boolean Result=objRestLanguageTranslation.CheckKeyPhrase(KeyPhrase);
		return Result;
	
	}

	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to get language data languages table.
	 * Input parameter: String
	 * Return Type :LANGUAGETRANSLATIONBE
	 * 
	 */

	@Transactional

	public LANGUAGETRANSLATIONBE getLanguageData(String strLanguageEdit,String strKeyPhrase, String StrDescription) throws  JASCIEXCEPTION
	{
		
		List<LANGUAGES> objListLanguage=objRestLanguageTranslation.GetLanguage(strLanguageEdit,strKeyPhrase);
		LANGUAGETRANSLATIONBE objLanguagetranslationbe=new LANGUAGETRANSLATIONBE();
		for(LANGUAGES ObjLanguage:objListLanguage)
		{
			objLanguagetranslationbe.setDescription20(StrDescription);
	        objLanguagetranslationbe.setLanguage(ObjLanguage.getId().getLanguage());
	        objLanguagetranslationbe.setKeyPhrase(StringEscapeUtils.escapeHtml(ObjLanguage.getId().getKeyPhrase()));
	        
	        objLanguagetranslationbe.setLastActivityBy(StringEscapeUtils.escapeHtml(objRestLanguageTranslation.getTeamMemberName(ObjLanguage.getLastActivityTeamMember())));
	        DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
	        Date ObjDate =ObjLanguage.getLastActivityDate();
			 String StrLastActivityDate=GLOBALCONSTANT.BlankString;
			 try{
				 StrLastActivityDate=ObjDateFormat1.format(ObjDate);
			 }catch(Exception e)
			 {}
	        objLanguagetranslationbe.setLastActivityDate(StrLastActivityDate);
   	        objLanguagetranslationbe.setTranslation(StringEscapeUtils.escapeHtml(ObjLanguage.getTranslation()));
	
			
		}
		
		
		return objLanguagetranslationbe;
	}


	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to update a record to languages table.
	 * Input parameter: LANGUAGETRANSLATIONBE
	 * Return Type :Boolean
	 * 
	 */
	@Transactional
	public Boolean editLanguageData(LANGUAGETRANSLATIONBE objLanguageBe) throws  JASCIEXCEPTION
	{
		return objRestLanguageTranslation.editLanguageData(objLanguageBe);
		
	}



	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to get screen labels languages table.
	 * Input parameter: String
	 * Return Type :LANGUAGETRANSLATIONSCREENLABELSBE
	 * 
	 */

	@Transactional
	public LANGUAGETRANSLATIONSCREENLABELSBE GetScreenLabels(
			String StrLanguage) throws  JASCIEXCEPTION
	{
    return objRestLanguageTranslation.GetScreenLabels(StrLanguage);

	}



	

	
	

}
