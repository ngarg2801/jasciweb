/*
Description This class used to make relation with restfull services means call the restfull service functions.
Created By Aakash Bishnoi  
Created Date Dec 26 2014
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.LOCATIONMAINTENANCEAPI;
import com.jasci.biz.AdminModule.be.LOCATIONMAINTENANCEBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.LOCATIONBEAN;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class LOCATIONMAINTENANCESERVICEIMPL implements ILOCATIONMAINTENANCESERVICE {

	@Autowired
	LOCATIONMAINTENANCEAPI ObjRestServiceClient;
	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
	@Autowired
	LANGUANGELABELSAPI ObjLanguageLabelApi;
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is used to Add Record in Location Table Form LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	
	 */
	public String addEntry(LOCATIONBEAN ObjectLocation) throws JASCIEXCEPTION{
		String Result=ObjRestServiceClient.addEntry(ObjectLocation);
		return Result;
	}
	

	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 6, 2015
	 * Description:This is used to Update Record in Location Table Form LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	
	 */
	public String updateEntry(LOCATIONBEAN ObjectLocation) throws JASCIEXCEPTION{
		String Result=ObjRestServiceClient.updateEntry(ObjectLocation);
		return Result;
	}
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This Function is used to call MENUMESSAGESSERVICEAPI function for populated DropDown Value from GENERAL_CODE
	 *@param StrTenant
	 *@param StrCompany
	 *@param GeneralCodeID
	 *@return  List<GENERALCODES>	  
	 */
	public List<GENERALCODES> getGeneralCode(String StrTenant,String StrCompany,String GeneralCodeID) throws JASCIEXCEPTION{
		return  ObjRestServiceClient.getGeneralCode(StrTenant,StrCompany,GeneralCodeID);
	}
	
	
	/**	 
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This Function is used to call MENUMESSAGESSERVICEAPI function for populated DropDown Value from LOCATION_PROFILES
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFullfilmentCenter
	 *@return List<LOCATIONPROFILES>
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATIONPROFILES> getLocationProfile(String StrTenant,String StrCompany,String StrFullfilmentCenter) throws JASCIEXCEPTION{
		return ObjRestServiceClient.getLocationProfile(StrTenant,StrCompany,StrFullfilmentCenter);
	}
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is used to set screen labels and error messages of Location's Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	 */
	 @Transactional
	 public LOCATIONMAINTENANCEBE getLocationMaintenanceLabels(String StrLanguage) throws JASCIEXCEPTION{
		
			List<LANGUAGES> ObjListLanguages=ObjLanguageLabelApi.GetScreenLabels(LANGUANGELABELSAPI.StrLocationMaintenance, StrLanguage);
			LOCATIONMAINTENANCEBE ObjLocationBe=new LOCATIONMAINTENANCEBE();  
			String StrObjectFieldCode=null;
		
				for (LANGUAGES ObjLanguages : ObjListLanguages) 
				{
					try{
					StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
					}catch(Exception ObjectException){
						throw new JASCIEXCEPTION(ObjectException.getMessage());			
					}	
					
					if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Search_Lookup))
					{
						ObjLocationBe.setLocationMaintenace_Location_Search_Lookup(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Area))
					{
						ObjLocationBe.setLocationMaintenace_Area(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_FulFillment_Center))
					{
						ObjLocationBe.setLocationMaintenace_FulFillment_Center(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location))
					{
						ObjLocationBe.setLocationMaintenace_Location(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description))
					{
						ObjLocationBe.setLocationMaintenace_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Type))
					{
						ObjLocationBe.setLocationMaintenace_Location_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Task))
					{
						ObjLocationBe.setLocationMaintenace_Last_Activity_Task(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Copy))
					{
						ObjLocationBe.setLocationMaintenace_Copy(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Notes))
					{
						ObjLocationBe.setLocationMaintenace_Notes(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Any_Part_of_Location_Description))
					{
						ObjLocationBe.setLocationMaintenace_Any_Part_of_Location_Description(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Lookup))
					{
						ObjLocationBe.setLocationMaintenace_Location_Lookup(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Maintenance))
					{
						ObjLocationBe.setLocationMaintenace_Location_Maintenance(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Short))
					{
						ObjLocationBe.setLocationMaintenace_Location_Short(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Long))
					{
						ObjLocationBe.setLocationMaintenace_Location_Long(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Profile))
					{
						ObjLocationBe.setLocationMaintenace_Location_Profile(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_ID))
					{
						ObjLocationBe.setLocationMaintenace_Wizard_ID(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Control_Number))
					{
						ObjLocationBe.setLocationMaintenace_Wizard_Control_Number(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Group_Zone))
					{
						ObjLocationBe.setLocationMaintenace_Work_Group_Zone(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Work_Zone))
					{
						ObjLocationBe.setLocationMaintenace_Work_Zone(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Check_Digit))
					{
						ObjLocationBe.setLocationMaintenace_Check_Digit(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Alternate_Location))
					{
						ObjLocationBe.setLocationMaintenace_Alternate_Location(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height))
					{
						ObjLocationBe.setLocationMaintenace_Location_Height(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Width))
					{
						ObjLocationBe.setLocationMaintenace_Location_Width(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Depth))
					{
						ObjLocationBe.setLocationMaintenace_Location_Depth(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Weight_Capacity))
					{
						ObjLocationBe.setLocationMaintenace_Location_Weight_Capacity(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_Capacity))
					{
						ObjLocationBe.setLocationMaintenace_Location_Height_Capacity(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Pallets))
					{
						ObjLocationBe.setLocationMaintenace_Number_of_Pallets(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Floor_Pallet_Location))
					{
						ObjLocationBe.setLocationMaintenace_Number_of_Floor_Pallet_Location(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Allocation_Allowable))
					{
						ObjLocationBe.setLocationMaintenace_Allocation_Allowable(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Free_Location_When_Zero))
					{
						ObjLocationBe.setLocationMaintenace_Free_Location_When_Zero(ObjLanguages.getTranslation());
					}							
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Free_Prime_Location_in_99999_Days))
					{
						ObjLocationBe.setLocationMaintenace_Free_Prime_Location_in_99999_Days(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Multiple_Products_In_The_Same_Location))
					{
						ObjLocationBe.setLocationMaintenace_Multiple_Products_In_The_Same_Location(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Storage_Type))
					{
						ObjLocationBe.setLocationMaintenace_Storage_Type(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Slotting))
					{
						ObjLocationBe.setLocationMaintenace_Slotting(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Manual_Assigned_Location))
					{
						ObjLocationBe.setLocationMaintenace_Manual_Assigned_Location(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Used_In_Slotting))
					{
						ObjLocationBe.setLocationMaintenace_Used_In_Slotting(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
					{
						ObjLocationBe.setLocationMaintenace_Select(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_YES))
					{
						ObjLocationBe.setLocationMaintenace_Yes(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_NO))
					{
						ObjLocationBe.setLocationMaintenace_No(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Location))
					{
						ObjLocationBe.setLocationMaintenace_Invalid_Location(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_an_Area))
					{
						ObjLocationBe.setLocationMaintenace_Please_select_a_Area_first(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_select_a_Location_Type_first))
					{
						ObjLocationBe.setLocationMaintenace_Please_select_a_Location_Type_first(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Any_Part_of_Location_Description))
					{
						ObjLocationBe.setLocationMaintenace_Invalid_Location_Description(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Location))
					{
						ObjLocationBe.setLocationMaintenace_Please_enter_Location(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Any_Part_of_Location_Description))
					{
						ObjLocationBe.setLocationMaintenace_Please_enter_Part_Of_Loctaion_Description(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_is_about_to_be_deleted))
					{
						ObjLocationBe.setLocationMaintenace_Location_is_about_to_be_deleted(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_has_Inventory_and_can_not_be_deleted))
					{
						ObjLocationBe.setLocationMaintenace_Location_has_Inventory_and_can_not_be_deleted(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_is_still_assigned_and_can_not_be_deleted))
					{
						ObjLocationBe.setLocationMaintenace_Location_is_still_assigned_and_can_not_be_deleted(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Locaton_already_Used))
					{
						ObjLocationBe.setLocationMaintenace_Locaton_already_Used(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Alternate_Locaton_already_Used))
					{
						ObjLocationBe.setLocationMaintenace_Alternate_Locaton_already_Used(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only))
					{
						ObjLocationBe.setLocationMaintenace_Location_Height_Capacity_must_be_numeric_with_3_decimal_values_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only))
					{
						ObjLocationBe.setLocationMaintenace_Location_Weight_Capacity_must_be_numeric_with_3_decimal_values_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Depth_must_be_numeric_with_3_decimal_values_only))
					{
						ObjLocationBe.setLocationMaintenace_Location_Depth_must_be_numeric_with_3_decimal_values_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Width_must_be_numeric_with_3_decimal_values_only))
					{
						ObjLocationBe.setLocationMaintenace_Location_Width_must_be_numeric_with_3_decimal_values_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_must_be_numeric_with_3_decimal_values_only))
					{
						ObjLocationBe.setLocationMaintenace_Location_Height_must_be_numeric_with_3_decimal_values_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Wizard_Control_Number_must_be_numeric_only))
					{
						ObjLocationBe.setLocationMaintenace_Wizard_Control_Number_must_be_numeric_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Floor_Pallet_Location_must_be_numeric_only))
					{
						ObjLocationBe.setLocationMaintenace_Number_of_Floor_Pallet_Location_must_be_numeric_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Free_Prime_Location_in_99999_Days_must_be_numeric_only))
					{
						ObjLocationBe.setLocationMaintenace_Free_Prime_Location_in_99999_Days_must_be_numeric_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Number_of_Pallets_must_be_numeric_only))
					{
						ObjLocationBe.setLocationMaintenace_Number_of_Pallets_must_be_numeric_only(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
					{
						ObjLocationBe.setLocationMaintenace_Maindatory_field_can_not_be_left_blank(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
					{
						ObjLocationBe.setLocationMaintenace_Cancel(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
					{
						ObjLocationBe.setLocationMaintenace_Delete(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
					{
						ObjLocationBe.setLocationMaintenace_Save_Update(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
					{
						ObjLocationBe.setLocationMaintenace_Display_All(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
					{
						ObjLocationBe.setLocationMaintenace_Add_New(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
					{
						ObjLocationBe.setLocationMaintenace_Edit(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
					{
						ObjLocationBe.setLocationMaintenace_New(ObjLanguages.getTranslation());
					}
						
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_AREA))
					{
						ObjLocationBe.setLocationMaintenace_Invalid_Area(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_LOCATION_TYPE))
					{
						ObjLocationBe.setLocationMaintenace_Invalid_Location_Type(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_Select_Location_Type))
					{
						ObjLocationBe.setLocationMaintenace_Please_Select_Location_Type(ObjLanguages.getTranslation());
					}

					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
					{
						ObjLocationBe.setLocationMaintenace_Edit(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
					{
						ObjLocationBe.setLocationMaintenace_New(ObjLanguages.getTranslation());
					}
						
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_AREA))
					{
						ObjLocationBe.setLocationMaintenace_Invalid_Area(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_ERR_INVALID_LOCATION_TYPE))
					{
						ObjLocationBe.setLocationMaintenace_Invalid_Location_Type(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_Select_Location_Type))
					{
						ObjLocationBe.setLocationMaintenace_Please_Select_Location_Type(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
					{
						ObjLocationBe.setLocationMaintenace_Actions(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
					{
						ObjLocationBe.setLocationMaintenace_Last_Activity_Date_YYYY_MM_DD(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_ActivityDate))
					{
						ObjLocationBe.setLocationMaintenace_Last_ActivityDate(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
					{
						ObjLocationBe.setLocationMaintenace_Last_Activity_By(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
					{
						ObjLocationBe.setLocationMaintenace_Your_record_has_been_saved_successfully(ObjLanguages.getTranslation());
					}
					
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
					{
						ObjLocationBe.setLocationMaintenace_Your_record_has_been_updated_successfully(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
					{
						ObjLocationBe.setLocationMaintenace_Are_you_sure_you_want_to_delete_this_record(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Loaction_and_Alternate_Location_can_not_be_same))
					{
						ObjLocationBe.setLocationMaintenace_Loaction_and_Alternate_Location_cannot_be_same(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_There_is_no_location_profile_for_selected_fulfillment_center))
					{
						ObjLocationBe.setLocationMaintenace_There_is_no_location_profile_for_selected_fulfillment_center(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
					{
						ObjLocationBe.setLocationMaintenace_Reset(ObjLanguages.getTranslation());
					}
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Height_Capacity_must_be_less_then_or_equal_to_Location_Height))
					{
						ObjLocationBe.setLocationMaintenace_Location_Height_Capacity_must_be_less_Than_or_equalto_Location_Height(ObjLanguages.getTranslation());
					}
	
					else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Location_Weight_Capacity_must_be_less_then_or_equal_to_Location_Width))
					{
						ObjLocationBe.setLocationMaintenace_Location_Weight_Capacity_must_be_less_Than_or_equalto_Location_Width(ObjLanguages.getTranslation());
					}
				}
				return ObjLocationBe;	
	 }	
	
	/**
     * 
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public String getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
		
		return ObjRestServiceClient.getTeamMemberName(Tenant, TeamMember);
				
	}
	
	 
    /**
     * @author Aakash Bishnoi
     * @Date Jan 3, 2015
     * Description:This function is used to call restfull service for search the list from LOCATION on Location Search Screen.
     *@param StrTenant
     *@param StrCompany
     *@param StrFulfillmentCenter
     *@param StrArea
     *@param StrDescription    
     *@param StrLocationType
     *@return
    */
   public List<LOCATIONBEAN> getList(String StrLocation,String StrFulfillmentCenter,String StrArea,String StrDescription,String StrLocationType) throws JASCIEXCEPTION{
	   return ObjRestServiceClient.getList(StrLocation,StrFulfillmentCenter,StrArea,StrDescription,StrLocationType);
   }


   /**
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description:This function is used to call restfull service for fetch data from Location table on the behalf of parameters(Primary Keys)
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@return LOCATIONBEAN
	 */
	public LOCATIONBEAN getFetchLocation(String StrFulfillmentCenter,String StrArea,String StrLocation) throws JASCIEXCEPTION{
		 return ObjRestServiceClient.getFetchLocation(StrFulfillmentCenter,StrArea,StrLocation);
	}

	/**
	 * Created on:Dec 28 2014
	 * Created by:Diksha Gupta
	 * Description: This function  get FulfillmentCenter based on tenant.
	 * Input parameter: String
	 * Return Type :List<FULFILLMENTCENTERSPOJO>
	 * 
	 */
	
	 
	 
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentCenter(String tenant,String Company,String PageName) throws JASCIEXCEPTION
	{
	
		return ObjRestServiceClient.getFulFillmentCenter(tenant,Company,PageName);
	}
	

	   /**
		 * @author Aakash Bishnoi
		 * @Date Jan 5, 2015
		 * Description:This function is used to call restfull service for fetch for delete selected data from location table
		 *@param StrTenant
		 *@param StrCompany
		 *@param StrFulfillmentCenter
		 *@param StrArea
		 *@param StrLocation
		 *@return LOCATIONBEAN
		 */
	public Boolean deleteEntry(String StrTenant_ID,String StrCompany_ID,String StrFetchFulfillment_Center,String StrFetchArea,String StrFetchLocation) throws JASCIEXCEPTION{
		 return ObjRestServiceClient.deleteEntry(StrTenant_ID,StrCompany_ID,StrFetchFulfillment_Center,StrFetchArea,StrFetchLocation);
	 }
	
}
