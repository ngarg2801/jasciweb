/**

Description This Interface used to maintain the standard for implementing functions of SMARTTASKCONFIGURATORSERVICEIMPL.
Created By Shailendra Rajput  
Created Date May 14 2015
 */

package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.SMARTTASKCONFIGURATORBE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public interface ISMARTTASKCONFIGURATORSERVICE {

		 /**
		 * @author Shailendra Rajput
		 * @Date May 14, 2015
		 * @Description: This function is used to get the screen label for all SmartTaskConfigurator screen 
		 * @param StrLanguage
		 * @return SMARTTASKCONFIGURATORBE
		 * @throws JASCIEXCEPTION
		 */
		 public SMARTTASKCONFIGURATORBE getSmartTaskConfiguratorLabels(String StrLanguage) throws JASCIEXCEPTION;
		 

		 	/**
			 * @author Shailendra Rajput
			 * @Date May 18, 2015
			 * @Description:This is used to search the list from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
			 *@param StrTenant
			 *@param StrTask
			 *@param StrApplication
			 *@param StrExecutionSequenceGroup
			 *@param StrExecutionType
			 *@param StrExecutionDevice
			 *@param StrExecutionSequenceName
			 *@param StrDescription
			 *@param StrMenuName
			 *@return List 
			 */
			
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
		public List getList(String StrTenant,String StrTask,String StrApplication,String StrExecutionSequenceGroup,String StrExecutionType,String StrExecutionDevice,String StrExecutionSequenceName,String StrDescription,String StrMenuName) throws JASCIEXCEPTION;
	
		/**
		 * @author Shailendra Rajput
		 * @Date May 18, 2015
		 * @Description:This is used to show All data(DisplayAll) from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
		 *@return List 
		 */
		@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)	
		public List getDisplayAll() throws JASCIEXCEPTION;

		/**
		 * 
		 * @author Shailendra Rajput
		 * @Date May 19, 2015
		 * @Description:It is declaration of function,which is used to delete the record from SMART_TASK_SEQUENCE_HEADERS,SMART_TASK_SEQUENCE_INSTRUCTIONS, MENU_OPTIONS and MENU_PROFILE table
		 * @param StrTenant_Id
		 * @param Company_Id
		 * @param StrApplication_Id
		 * @param StrExecution_Sequence_Group
		 * @param StrExecution_Sequence_Name
		 * @param StrMenu_Name
		 * @return boolean
		 * @throws JASCIEXCEPTION
		 */
		public boolean deleteEntry(String StrTenant_Id, String strCompany,String StrExecution_Sequence_Name,String StrMenu_Name)throws JASCIEXCEPTION;

		/**
		 * @author Shailendra Rajput
		 * @Date Jun 30, 2015
		 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORSERVICEIMPL is used to Add Record in SMART_TASK_SEQ_HEADERS,SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
		 * @param ObjectSTSInstructionsBean
		 * @param ObjectSTSHeadersBean
		 * @return String
		 * @throws JASCIEXCEPTION
		 */
		/*public String addEntry(SMARTTASKSEQINSTRUCTIONSBEAN ObjectSTSInstructionsBean,SMARTTASKSEQHEADERSBEAN ObjectSTSHeadersBean) throws JASCIEXCEPTION;
*/		public String addEntry(String Tenant_ID,String Company_ID,String Execution_Sequence_Name,String Application_Id,String Execution_Sequence_Group,String Description20,String Description50,String Menu_Option,String Execution_Device,String Execution_Type,String Task) throws JASCIEXCEPTION;

		
		
		/**
		 * @author Shailendra Rajput
		 * @Date Jun 30, 2015
		 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORSERVICEIMPL is used to Update Record in SMART_TASK_SEQ_HEADERS,SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
		 * @param ObjectSTSInstructionsBean
		 * @param ObjectSTSHeadersBean
		 * @return String
		 * @throws JASCIEXCEPTION
		 */
		public String updateEntry(String Tenant_ID,String Company_ID,String Execution_Sequence_Name,String Application_Id,String Execution_Sequence_Group,String Description20,String Description50,String Menu_Option,String Execution_Device,String Execution_Type,String Last_Activity_Date,String Last_Activity_Team_Member,String Task) throws JASCIEXCEPTION;

		

		/**
		 * @author Shailendra Rajput
		 * @Date July 2, 2015
		 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORSERVICEIMPL is used to get the record from Smart_task_Seq_Headers table at the time of edit
		 * @param StrTenant_ID
		 * @param Company_ID
		 * @param StrExecutionSequenceName
		 * @return List<SMARTTASKSEQHEADERS>
		 * @throws JASCIEXCEPTION
		 */
		public void fetchEntry(String StrTenant_ID,String Company_ID,String StrExecutionSequenceName,String ActionName)  throws JASCIEXCEPTION;
		
		/**
		 * @author Shailendra Rajput
		 * @Date Aug 12, 2015
		 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORSERVICEIMPL is used to set the page name for using back button 
		 * @param StrPageName
		 * @return void
		 * @throws JASCIEXCEPTION
		 */
		public void setPageName(String StrPageName)  throws JASCIEXCEPTION;	
}