/**

Date Developed :Dec 26 2014
Created by: Diksha Gupta
Description : LOCATIONPROFILESERVICEIMPL implementation of ILOCATIONPROFILESERVICE to call the methods of ILOCATIONPROFILEDAL.
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LOCATIONPROILESERVICEAPI;
import com.jasci.biz.AdminModule.be.LOCATIONPROFILEBE;
import com.jasci.biz.AdminModule.be.LOCATIONPROFILESCREENLABELSBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class LOCATIONPROFILESERVICEIMPL implements ILOCATIONPROFILESERVICE
{
 
	@Autowired
	LOCATIONPROILESERVICEAPI ObjLocationProfileServiceApi;
	/**
	 * Created on:Dec 28 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get General code list based on tenant,company,application,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	@Transactional
	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION 
	{
     	return ObjLocationProfileServiceApi.getGeneralCode(Tenant, Company, GeneralCodeId);
	}
	
	/**
	 * Created on:Dec 28 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function  get FulfillmentCenter based on tenant.
	 * Input parameter: String
	 * Return Type :List<FULFILLMENTCENTERSPOJO>
	 * 
	 */
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentCenter(String tenant,String Company,String PageName) throws JASCIEXCEPTION
	{
	
		return ObjLocationProfileServiceApi.getFulFillmentCenter(tenant,Company,PageName);
	}
	
	/**
	 * Created on:Dec 30 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to add a record for LocationProfile.
	 * Input parameter: LOCATIONPROFILEBE
	 * Return Type :Boolean
	 * 
	 */
	public Boolean addLocationProfile(LOCATIONPROFILEBE objLocationProfileBe) throws JASCIEXCEPTION
	{
		
		return ObjLocationProfileServiceApi.addLocationProfile(objLocationProfileBe);
	}
	
	
	/**
	 * Created on:Dec 30 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to edit a record for LocationProfile.
	 * Input parameter: LOCATIONPROFILEBE
	 * Return Type :Boolean
	 * 
	 */
	public Boolean editLocationProfile(LOCATIONPROFILEBE objLocationProfileBe) throws JASCIEXCEPTION
	{
		
		return ObjLocationProfileServiceApi.editLocationProfile(objLocationProfileBe);
	}

	
	/**
	 * Created on:Dec 30 2014
	 * Created by:"Diksha Gupta"
	 * Description: This function is to delete a record for LocationProfile.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	public Boolean DeleteLocationProfile(String locationProfile,
			String profileGroup, String fulfillmentCenter)throws JASCIEXCEPTION
	{
	return ObjLocationProfileServiceApi.DeleteLocationProfile(locationProfile, profileGroup, fulfillmentCenter);
	}

	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get records for LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * 
	 */
	public List<LOCATIONPROFILEBE> GetLocationProfileList(String strLocationProfile, String strProfileGroup,String strPartOfDescription) throws JASCIEXCEPTION 
	{
	
		return ObjLocationProfileServiceApi.GetLocationProfileList(strLocationProfile,strProfileGroup,strPartOfDescription);
		 
	}
	
	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get records for LocationProfileReassign.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * 
	 */
	public List<LOCATIONPROFILEBE> GetLocationProfileListReAssign(
			 String strProfileGroup,
			String strFulfillmentcenter,String strLocationProfile) throws JASCIEXCEPTION
			{
					return ObjLocationProfileServiceApi.GetLocationProfileListReAssign(strProfileGroup,strFulfillmentcenter,strLocationProfile);
			}
	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get list of distinct LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * 
	 */
	public List<String> GetDistinctLocationProfile() throws JASCIEXCEPTION 
	{
	
		return ObjLocationProfileServiceApi.GetDistinctLocationProfile();
		 
	}
	
	
	/**
	 * Created on:Jan 05 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to get record of LocationProfile for updation.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	public LOCATIONPROFILEBE editLocationProfile(String locationProfile, String profileGroup, String fulfillmentCenter)throws JASCIEXCEPTION
	{
		return ObjLocationProfileServiceApi.editLocationProfile( locationProfile, profileGroup, fulfillmentCenter);
	}

	/**
	 * Created on:Jan 05 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function  is to get screen labels.
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILESCREENLABELSBE
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	public LOCATIONPROFILESCREENLABELSBE getScreenLabels(String currentLanguage)
			throws JASCIEXCEPTION {
		return ObjLocationProfileServiceApi.getScreenLabels(currentLanguage);
	}



	
	
	
	
}
