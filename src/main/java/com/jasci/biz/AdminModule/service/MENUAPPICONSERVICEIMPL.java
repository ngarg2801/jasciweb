/**
 Date Developed  Dec 12 2014
 Created By "Rahul Kumar"
 Description  Provide service layer between  controller and Rest Full service  layer
 */

package com.jasci.biz.AdminModule.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.MENUAPPICONSERVICEAPI;
import com.jasci.biz.AdminModule.be.MENUAPPICONSCREENBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.MENUAPPICONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class MENUAPPICONSERVICEIMPL implements IMENUAPPICONSERVICE {

	@Autowired
	MENUAPPICONSERVICEAPI objMenuappiconserviceapi;
	@Autowired
	LANGUANGELABELSAPI objLanguangelabelsapi;
	
	
	/**
	 * @Description set screen label language
	 * @author Rahul Kumar
	 * @Date Dec 12, 2014 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	public MENUAPPICONSCREENBE setScreenLanguage(String StrScreenName, String StrLanguage) throws JASCIEXCEPTION {

		MENUAPPICONSCREENBE objMenuappiconscreenbe=new MENUAPPICONSCREENBE();
		
		List<LANGUAGES> ObjListLanguages=objLanguangelabelsapi.GetScreenLabels(LANGUANGELABELSAPI.strMenuAppIconModule,StrLanguage);			
		

		for (LANGUAGES ObjLanguages : ObjListLanguages) 
		{
				String StrKeyPhrase=ObjLanguages.getId().getKeyPhrase();
			String StrTranslation=ObjLanguages.getTranslation();
			
			
			
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_Select(StrTranslation);

			}
			
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Menu_App_Icon_LooKup)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_MenuAppIconLookUpLabel(StrTranslation);

			}
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Menu_App_Icon_Search_LooKup)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_MenuAppIconSearckLookUp(StrTranslation);

			}
			if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Menu_App_Icon_Maintenance)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_MenuAppIconMaintenance(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_a_App_Icon)){
			
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON(StrTranslation);
			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_App_Icon)){
				
				objMenuappiconscreenbe.setMenuAppIconMaintenance_AppIconGrid(StrTranslation);
				objMenuappiconscreenbe.setMenuAppIconMaintenance_EnterAppIconLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_the_App_Icon_Name)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_PartOfTheAppIconNameLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Any_Part_of_the_App_Icon)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_PartOfTheAppIconLabel(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_New)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonNewText(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonDisplayAllText(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonEditText(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonDeleteText(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Choose_File)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonBrowseText(StrTranslation);

			}	
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonSaveUpdateText(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonCancelText(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_the_Description)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_PartOfTheDescription(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_DescriptionGrid(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_LastActivityDate(StrTranslation);

			}	
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_LastActivityBy(StrTranslation);

			}	
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Short)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_DescriptionShortLabel(StrTranslation);

			}	
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Description_Long)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_DescriptionLongLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Preferred_Application)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_PreferredApplicationLabel(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_App_Icon_Address)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_AppIconAddressLabel(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_YY_MM_DD)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_YY_MM_DD_Label(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_App_Icon)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_INVALID_APP_ICON(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_sure_they_want_to_delete_this_App_Icon)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_WHILE_DELETING_APP_ICON(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_App_Icon_already_used)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_APP_ICON_ALREADY_USED(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_MANDATORY_FIELD_LEFT_BLANK(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_the_App_Icon)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_the_App_Icon_Name)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_INVALID_PART_OF_APP_ICON_NAME(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_a_App_Icon)){
					objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_PLZ_ENTER_APP_ICON(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_App_Icon_Address)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_INVALID_APP_ICON_ADDRESS(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_part_of_the_App_Icon)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_part_of_the_App_Icon_Name)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_PLZ_ENTER_PART_OF_THE_APP_ICON_NAME(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ON_SAVE_SUCCESSFULLY(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ON_UPDATE_SUCCESSFULLY(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_APPLICATIONS)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_GeneralCodeId_Application(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Application_already_used)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_APPLICATION_ALREADY_USED(StrTranslation);

			}
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonAddNew(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_Action(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Loading)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_Text_Loading(StrTranslation);

			}

			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Supported_formats_are_Jpeg_jpg_png_gif)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ERR_SUPPRTED_FORMAT(StrTranslation);

			}
			
			else if(StrKeyPhrase.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset)){
				objMenuappiconscreenbe.setMenuAppIconMaintenance_ButtonResetText(StrTranslation);

			}
			
		
		}
		return objMenuappiconscreenbe;
	}

	/**
	 * @Description add or update an app icon
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param objMenuappiconbe
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#addOrUpdateMenuAppIcon(com.jasci.biz.AdminModule.model.MENUAPPICONS)
	 */
	public Boolean addOrUpdateMenuAppIcon(MENUAPPICONBE objMenuappiconbe) throws JASCIEXCEPTION {
		
		MENUAPPICONS objMenuappicons=new MENUAPPICONS();
		objMenuappicons.setAppIcon(trimValues(objMenuappiconbe.getAppIcon()));
		objMenuappicons.setAppIconAddress(trimValues(objMenuappiconbe.getAppIconAddress()));
		objMenuappicons.setApplication(trimValues(objMenuappiconbe.getApplication()));
		objMenuappicons.setDescriptionLong(trimValues(objMenuappiconbe.getDescriptionLong()));
		objMenuappicons.setDescriptionShort(trimValues(objMenuappiconbe.getDescriptionShort()));
		Date LastActivitydate=null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

			 LastActivitydate = formatter.parse(objMenuappiconbe.getLastActivityDate());
			
		}catch(Exception objException){}
		objMenuappicons.setLastActivityDate(LastActivitydate);
		objMenuappicons.setLastActivityTeamMember(objMenuappiconbe.getLastActivityTeamMember());
		objMenuappiconserviceapi.addOrUpdateMenuAppIcon(objMenuappicons);
		return null;
	}

	/**
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param Tenant
	 * @param Company
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#getManuAppIconList(java.lang.String, java.lang.String)
	 */
	@Transactional
	public List<MENUAPPICONS> getManuAppIconList(String Tenant, String Company) throws JASCIEXCEPTION {

		
		return objMenuappiconserviceapi.getMenuAppIconList(Tenant, Company);
		
	}

	/**
	 * @Description get application list from general code
	 * @author Rahul Kumar
	 * @Date Dec 16, 2014
	 * @param GeneralCodeID
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#getApplicationList(java.lang.String)
	 */
	public List<GENERALCODES> getApplicationList(String GeneralCodeID) throws JASCIEXCEPTION {

		
		return objMenuappiconserviceapi.getApplication(GeneralCodeID);
	}

	/**
	 * @Description get app icon list by app icon name
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#getMenuAppIconListByApplicationAppIcon(java.lang.String, java.lang.String)
	 */
	public List<MENUAPPICONS> getMenuAppIconListByApplicationAppIcon(String Application, String AppIcon)
			throws JASCIEXCEPTION {

		return  objMenuappiconserviceapi.getMenuAppIconListByApplicationAppIcon(Application, AppIcon);
	}

	/**
	 * @Description delete app icon
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#deleteMenuAppIcon(java.lang.String, java.lang.String)
	 */
	public Boolean deleteMenuAppIcon(String Application, String AppIcon) throws JASCIEXCEPTION {

		return  objMenuappiconserviceapi.deleteMenuAppIcon(Application, AppIcon);
	}

	/**
	 * @Description get app icon by its part name
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 * @param AppIconName
	 * @param AppIconPart
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#getAppIconListByPart(java.lang.String, java.lang.String)
	 */
	public List<MENUAPPICONS> getAppIconListByPart(String AppIconName, String AppIconPart) throws JASCIEXCEPTION {

		return  objMenuappiconserviceapi.getAppIconListByPart(AppIconName, AppIconPart);
	}

	

	/**
	 * @Description trim string values
	 * @param Value
	 * @return
	 * * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 */
	//This Function is used to Trim the white space
		public static String trimValues(String Value){
			if(Value == null){
				return null;
			}
			else{
				String CheckValue=Value.trim();
				if(CheckValue.length()<GLOBALCONSTANT.IntOne){
					return null;
				}else{      
					return CheckValue;
				}
			}
		}

		
		/**
		 * @Description get team member name
		 * @author Rahul Kumar
		 * @date 26 dec 2014
		 * @param Tenant
		 * @param TeamMember
		 * @return
		 * @throws JASCIEXCEPTION
		 */
			
	public String getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {

		String TeamMemberName=GLOBALCONSTANT.BlankString;
		
		List<TEAMMEMBERS> objTeammembers= objMenuappiconserviceapi.getTeamMemberName(Tenant, TeamMember);
		
		try{
		TeamMemberName=objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.get(GLOBALCONSTANT.INT_ZERO).getLastName();
		}catch(Exception objException){
			TeamMemberName=GLOBALCONSTANT.Blank;
		}
		
		return TeamMemberName;
		
		
	}

	
	

	
	

}
