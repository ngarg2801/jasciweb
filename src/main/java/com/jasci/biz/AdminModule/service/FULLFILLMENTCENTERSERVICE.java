/**
 Date Developed  Dec 22 2014
 Created By Deepak Sharma
 Description  Provide service layer between  controller and Rest Full service  layer
 */
package com.jasci.biz.AdminModule.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.jasci.biz.AdminModule.ServicesApi.FULLFILLMENTCENTERSERVICEAPI;
import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.RESTTEAMMEMBERMESSAGES;
import com.jasci.biz.AdminModule.be.FULFILLMENTCENTERSCREENBE;
import com.jasci.biz.AdminModule.be.FULLFILLMENTCENTERBE;

import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;

import com.jasci.exception.JASCIEXCEPTION;



@Service
public class FULLFILLMENTCENTERSERVICE {
@Autowired
FULLFILLMENTCENTERSERVICEAPI objFullfillmentcenterserviceapi;
@Autowired
RESTTEAMMEMBERMESSAGES objRestteammembers;
@Autowired
LANGUANGELABELSAPI objLanguangelabelsapi;


/**
 * 
 * @param StrSelectedText
 * @param IsSearchByFullfillmentID
 * @param IsSearchByPartOfFullfillMentCenterName
 * @param IsSearchByPartOfFullfillment
 * @param StrTenant
 * @return
 * @throws JASCIEXCEPTION
 */
	public List<FULLFILLMENTCENTERBE> searchForfulfillmentCenter(String StrSelectedText,boolean IsSearchByFullfillmentID,boolean IsSearchByPartOfFullfillMentCenterName,boolean IsSearchByPartOfFullfillment,String StrTenant) throws JASCIEXCEPTION
	{
	
		List<FULFILLMENTCENTERSPOJO> lstOfFullfillmentCenterPojo=objFullfillmentcenterserviceapi.searchForfulfillmentCenter(StrSelectedText, IsSearchByFullfillmentID, IsSearchByPartOfFullfillMentCenterName, IsSearchByPartOfFullfillment,StrTenant);
		
		List<FULLFILLMENTCENTERBE> lstFullfillmentcenterbe=convertPojoToViewBean(lstOfFullfillmentCenterPojo);
		
		return lstFullfillmentcenterbe;
		
		
	}
	
	/**
	 * 
	 * @param lstOfFullfillmentCenterPojo
	 * @return
	 */
	private List<FULLFILLMENTCENTERBE> convertPojoToViewBean(List<FULFILLMENTCENTERSPOJO> lstOfFullfillmentCenterPojo)
	{
		List<FULLFILLMENTCENTERBE> lstFullfillmentcenterbe= new ArrayList<FULLFILLMENTCENTERBE>();
		
		for (FULFILLMENTCENTERSPOJO objFullfillmentCenterPojo : lstOfFullfillmentCenterPojo) {
			
			
			
			
			FULLFILLMENTCENTERBE objFullfillmentcenterbe=new FULLFILLMENTCENTERBE();
		/*	//B//
			
			objFullfillmentcenterbe.setTenant(objFullfillmentCenterPojo.getId().getTenant());
			objFullfillmentcenterbe.setFullfillment(objFullfillmentCenterPojo.getId().getFulfillmentCenter());
			
			objFullfillmentcenterbe.setName20(objFullfillmentCenterPojo.getName20());
			objFullfillmentcenterbe.setName50(objFullfillmentCenterPojo.getName50());
			objFullfillmentcenterbe.setAddressLine1(objFullfillmentCenterPojo.getAddressLine1());
			objFullfillmentcenterbe.setAddressLine2(objFullfillmentCenterPojo.getAddressLine2());
			objFullfillmentcenterbe.setAddressLine3(objFullfillmentCenterPojo.getAddressLine3());
			objFullfillmentcenterbe.setAddressLine4(objFullfillmentCenterPojo.getAddressLine4());
			objFullfillmentcenterbe.setCity(objFullfillmentCenterPojo.getCity());
			objFullfillmentcenterbe.setContactCell1(objFullfillmentCenterPojo.getContactCell1());
			objFullfillmentcenterbe.setContactCell2(objFullfillmentCenterPojo.getContactCell2());
			objFullfillmentcenterbe.setContactCell3(objFullfillmentCenterPojo.getContactCell3());
			objFullfillmentcenterbe.setContactCell4(objFullfillmentCenterPojo.getContactCell4());
			objFullfillmentcenterbe.setContactEmail1(objFullfillmentCenterPojo.getContactEmail1());
			objFullfillmentcenterbe.setContactEmail2(objFullfillmentCenterPojo.getContactEmail2());
			objFullfillmentcenterbe.setContactEmail3(objFullfillmentCenterPojo.getContactEmail3());
			objFullfillmentcenterbe.setContactEmail4(objFullfillmentCenterPojo.getContactEmail4());
		*/
			//BeanPropertyCopyUtil.
			try {
				BeanUtils.copyProperties(objFullfillmentcenterbe, objFullfillmentCenterPojo);
				objFullfillmentcenterbe.setTenant(objFullfillmentCenterPojo.getId().getTenant());
				objFullfillmentcenterbe.setFullfillment(objFullfillmentCenterPojo.getId().getFulfillmentCenter());
				
				
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			lstFullfillmentcenterbe.add(objFullfillmentcenterbe);
			
			
			
			
			
			
		}
		
		return lstFullfillmentcenterbe;
	}
	
	/**
	 * 
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrGeneralCodeID
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public  List<GENERALCODES>  getGeneralCode(String StrTenant,String StrCompany,String StrGeneralCodeID) throws JASCIEXCEPTION {
		
		List<GENERALCODES>  lstGeneralCode=	objRestteammembers.getGeneralCode(StrTenant, StrCompany, StrGeneralCodeID);
		
		return lstGeneralCode;	
		
		}
		
	
/**
 * 		
 * @param StrTenant
 * @return
 * @throws JASCIEXCEPTION
 */
public  List<FULLFILLMENTCENTERBE>  getAllFulfillment(String StrTenant) throws JASCIEXCEPTION {
	
	List<FULFILLMENTCENTERSPOJO> lstOfFullfillmentCenterPojo=objFullfillmentcenterserviceapi.getAllFulfillment(StrTenant);
	
	List<FULLFILLMENTCENTERBE> lstFullfillmentcenterbe=convertPojoToViewBean(lstOfFullfillmentCenterPojo);
	
	return lstFullfillmentcenterbe;	
	
	}

/**
 * @param StrModuleName
 * @param strLanguageCode
 * @return
 * @throws JASCIEXCEPTION
 */
public  FULFILLMENTCENTERSCREENBE  getScreenLabels(String StrModuleName,String strLanguageCode) throws JASCIEXCEPTION {
	
	List<LANGUAGES> lstLanguages=objLanguangelabelsapi.GetScreenLabels(StrModuleName, strLanguageCode);
	FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=ConvertLanguageListToFulfillmentCenter(lstLanguages);
	
	
	return objFulfillmentcenterscreenbe;
	
	}


/**
 * 
 * @param objFullfillmentcenterbe
 * @return
 * @throws JASCIEXCEPTION
 */


public WEBSERVICESTATUS AddUpdateFullfillmentCenter(FULLFILLMENTCENTERBE objFullfillmentcenterbe)throws JASCIEXCEPTION{
	
	
	FULFILLMENTCENTERSPOJO objFullfillmentCenterPojo = new FULFILLMENTCENTERSPOJO();
	WEBSERVICESTATUS objWEBSERVICESTATUS= new WEBSERVICESTATUS();
	
	try {
		BeanUtils.copyProperties( objFullfillmentCenterPojo,objFullfillmentcenterbe);
		FULFILLMENTCENTERS_PK objFullfillmentCenterPK= new FULFILLMENTCENTERS_PK();
		
		
		objFullfillmentCenterPK.setFulfillmentCenter(objFullfillmentcenterbe.getFullfillment());
		objFullfillmentCenterPK.setTenant(objFullfillmentcenterbe.getTenant());
		
		objFullfillmentCenterPojo.setId(objFullfillmentCenterPK);
		objFullfillmentcenterserviceapi.AddUpdateFullfillmentCenter(objFullfillmentCenterPojo);
		objWEBSERVICESTATUS.setBoolStatus(true);
		
		
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InvocationTargetException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}catch (JASCIEXCEPTION objJasciexception) {
		// TODO: handle exception
		
		objWEBSERVICESTATUS.setBoolStatus(false);
		
	}
	
	
	return objWEBSERVICESTATUS;
	
}
	
public FULFILLMENTCENTERSCREENBE ConvertLanguageListToFulfillmentCenter(List<LANGUAGES> lstLanguages)
{
	FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe = new FULFILLMENTCENTERSCREENBE();
	
	
	for (LANGUAGES languages : lstLanguages) {
		
		
		if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
		{
			objFulfillmentcenterscreenbe.setERR_Are_you_sure_you_want_to_delete_the_selected_row(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_center_already_used))
			{
				objFulfillmentcenterscreenbe.setERR_Fulfillment_center_already_used(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Fulfillment_Center))
			{
				objFulfillmentcenterscreenbe.setERR_Invalid_Fulfillment_Center(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_the_fulfillment))
			{
				objFulfillmentcenterscreenbe.setERR_Invalid_Part_of_the_fulfillment(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank))
			{
				objFulfillmentcenterscreenbe.setERR_Mandatory_field_cannot_be_left_blank(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table))
			{
				objFulfillmentcenterscreenbe.setERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table(languages.getTranslation());
				
				
			}
		

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Selected_fulfillment_center_deleted_successfully))
			{
				objFulfillmentcenterscreenbe.setERR_Selected_fulfillment_center_deleted_successfully(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully))
			{
				objFulfillmentcenterscreenbe.setERR_Your_record_has_been_saved_successfully(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Add_New))
			{
				objFulfillmentcenterscreenbe.setLbl_Add_New(languages.getTranslation());
				
				
			}
		

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_1))
			{
				objFulfillmentcenterscreenbe.setLbl_Address_Line_1(languages.getTranslation());
				
				
			}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_2))
		{
			objFulfillmentcenterscreenbe.setLbl_Address_Line_2(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_3))
		{
			objFulfillmentcenterscreenbe.setLbl_Address_Line_3(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Address_Line_4))
		{
			objFulfillmentcenterscreenbe.setLbl_Address_Line_4(languages.getTranslation());
			
			
		}

		/*else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_this_record))
			{
				objFulfillmentcenterscreenbe.setERR_Are_you_sure_you_want_to_delete_the_selected_row(languages.getTranslation());
				
				
			}*/

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel))
			{
				objFulfillmentcenterscreenbe.setLbl_Cancel(languages.getTranslation());
				
				
			}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_City))
		{
			objFulfillmentcenterscreenbe.setLbl_City(languages.getTranslation());
			
			
		}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell))
			{
				objFulfillmentcenterscreenbe.setLbl_Contact_Cell(languages.getTranslation());
				
				
			}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell_3))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Cell3(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell_2))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Cell2(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Cell_4))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Cell4(languages.getTranslation());
			
			
		}
		
		

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email))
			{
				objFulfillmentcenterscreenbe.setLbl_Contact_Email(languages.getTranslation());
				
				
			}


		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email_2))
			{
				objFulfillmentcenterscreenbe.setLbl_Contact_Email2(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email_3))
			{
				objFulfillmentcenterscreenbe.setLbl_Contact_Email3(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Email_4))
			{
				objFulfillmentcenterscreenbe.setLbl_Contact_Email4(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension))
			{
				objFulfillmentcenterscreenbe.setLbl_Contact_Extension(languages.getTranslation());
				
				
			}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension_2))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Extension2(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension_3))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Extension3(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Extension_4))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Extension4(languages.getTranslation());
			
			
		}
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax))
			{
				objFulfillmentcenterscreenbe.setLbl_Contact_Fax(languages.getTranslation());
				
				
			}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax_2))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Fax2(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax_3))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Fax3(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Fax_4))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Fax4(languages.getTranslation());
			
			
		}
		
		
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Name(languages.getTranslation());
			
			
		}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name_2))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Name2(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name_3))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Name3(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Name_4))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Name4(languages.getTranslation());
			
			
		}
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Country_Code))
			{
				objFulfillmentcenterscreenbe.setLbl_Country_Code(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Delete))
			{
				objFulfillmentcenterscreenbe.setLbl_Delete(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Display_All))
			{
				objFulfillmentcenterscreenbe.setLbl_Display_All(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Edit))
			{
				objFulfillmentcenterscreenbe.setLbl_Edit(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Enter_Fulfillment))
			{
				objFulfillmentcenterscreenbe.setLbl_Enter_Fulfillment(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_Center))
			{
				objFulfillmentcenterscreenbe.setLbl_Fulfillment_Center(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_center_already_used))
			{
				objFulfillmentcenterscreenbe.setERR_Fulfillment_center_already_used(languages.getTranslation());
				
				
			}

		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_Center_Maintenance))
			{
				objFulfillmentcenterscreenbe.setLbl_Fulfillment_Center_Maintenance(languages.getTranslation());
				
				
			}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fullfillment_Centers_Lookup))
		{
			objFulfillmentcenterscreenbe.setLbl_Fullfillment_Centers_Lookup(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fullfillment_Centers_Search_Lookup))
		{
			objFulfillmentcenterscreenbe.setLbl_Fullfillment_Centers_Search_Lookup(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Fulfillment_Center))
		{
			objFulfillmentcenterscreenbe.setERR_Invalid_Fulfillment_Center(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Part_of_the_fulfillment))
		{
			objFulfillmentcenterscreenbe.setERR_Invalid_Part_of_the_fulfillment(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_Date))
		{
			objFulfillmentcenterscreenbe.setLbl_Last_Activity_Date(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Main_Email))
		{
			objFulfillmentcenterscreenbe.setLbl_Main_Email(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Main_Fax))
		{
			objFulfillmentcenterscreenbe.setLbl_Main_Fax(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name))
		{
			objFulfillmentcenterscreenbe.setLbl_Name(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name_Long))
		{
			objFulfillmentcenterscreenbe.setLbl_Name_Long(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name_Short))
		{
			objFulfillmentcenterscreenbe.setLbl_Name_Short(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_New))
		{
			objFulfillmentcenterscreenbe.setLbl_New(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_valid_Email_Address))
		{
			objFulfillmentcenterscreenbe.setLbl_Not_a_valid_email_id(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_only_numeric_values))
		{
			objFulfillmentcenterscreenbe.setLbl_Not_a_valid_number(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_Center))
		{
			objFulfillmentcenterscreenbe.setLbl_Fullfillment_Center(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Part_of_the_Fulfillment_Center_Name))
		{
			objFulfillmentcenterscreenbe.setLbl_Part_of_the_Fulfillment_Center_Name(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Save_Update))
		{
			objFulfillmentcenterscreenbe.setLbl_Save_Update(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table))
		{
			objFulfillmentcenterscreenbe.setERR_Selected_fulfillment_center_cannot_be_deleted_as_it_is_used_in_another_table(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Selected_fulfillment_center_deleted_successfully))
		{
			objFulfillmentcenterscreenbe.setERR_Selected_fulfillment_center_deleted_successfully(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Set_up_Date))
		{
			objFulfillmentcenterscreenbe.setLbl_Set_up_Date(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Setup_Selected_Wizard))
		{
			objFulfillmentcenterscreenbe.setLbl_Set_up_Selected(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Setup_By))
		{
			objFulfillmentcenterscreenbe.setLbl_Setup_By(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_State))
		{
			objFulfillmentcenterscreenbe.setLbl_State(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Zip_Code))
		{
			objFulfillmentcenterscreenbe.setLbl_Zip_Code(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Phone(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone_2))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Phone2(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone_3))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Phone3(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact_Phone_4))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact_Phone4(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully))
		{
			objFulfillmentcenterscreenbe.setERR_Your_record_has_been_updated_successfully(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Last_Activity_By))
		{
			objFulfillmentcenterscreenbe.setLbl_Last_Activity_By(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Fulfillment_center_ID))
		{
			objFulfillmentcenterscreenbe.setERR_Please_enter_Fulfillment_center_ID(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_Part_of_Fulfillment_Center_Name))
		{
			objFulfillmentcenterscreenbe.setERR_Please_enter_Part_of_Fulfillment_Center_Name(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Actions))
		{
			objFulfillmentcenterscreenbe.setLbl_Actions(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Contact))
		{
			objFulfillmentcenterscreenbe.setLbl_Contact(languages.getTranslation());
			
			
		}
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Address))
		{
			objFulfillmentcenterscreenbe.setERR_Invalid_Address(languages.getTranslation());
			
			
		}
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Zip_Code))
		{
			objFulfillmentcenterscreenbe.setERR_Invalid_ZipCode(languages.getTranslation());
			
			
		}
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_Center_ID_already_used))
		{
			objFulfillmentcenterscreenbe.setERR_Fulfillment_center_already_used(languages.getTranslation());
			
			
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Fulfillment_Center_aready_exist_with_inactive_status))
		{
		   objFulfillmentcenterscreenbe.setERR_Fulfillment_Center_aready_exist_with_inactive_status(languages.getTranslation());
		   
		   
		}
				
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name_Short_already_used))
		{
		   objFulfillmentcenterscreenbe.setLbl_Name_Short_already_used(languages.getTranslation());
		   
		   
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Name_Long_already_used))
		{
		   objFulfillmentcenterscreenbe.setLbl_Name_Long_already_used(languages.getTranslation());
		   
		   
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank_and_must_be_numeric_only))
		{
		   objFulfillmentcenterscreenbe.setERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_NUMERIC_ONLY(languages.getTranslation());
		   
		   
		}
		
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank_and_must_be_valid_email_address))
		{
		   objFulfillmentcenterscreenbe.setERR_MANDATORY_FIELD_LEFT_BLANK_AND_MUST_BE_VALID_EMAIL_ADDRESS(languages.getTranslation());
		   
		   
		}
		
		else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Reset))
		{
		   objFulfillmentcenterscreenbe.setLbl_Button_ResetText(languages.getTranslation());
		   
		   
		}else if(languages.getId().getKeyPhrase().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Select))
		  {
		     objFulfillmentcenterscreenbe.setLbl_Select(languages.getTranslation());
		     
		     
		  }
		
		
		
		
		
	}
	return objFulfillmentcenterscreenbe;
	
	
}


}
