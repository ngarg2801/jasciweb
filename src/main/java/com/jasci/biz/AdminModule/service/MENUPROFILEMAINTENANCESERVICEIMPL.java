/**

Date Developed  Oct 15 2014
Description : Business Logic implemented Method for Menu Profile maintenance
Developed by: sarvendra tyagi
 */
package com.jasci.biz.AdminModule.service;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.MENUPROFILEBEANPOJOBE;
import com.jasci.biz.AdminModule.be.MENUPROFILEMAINTENANCELABELBE;
import com.jasci.biz.AdminModule.dao.IMENUPROFILEMAINTENANCEDAO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEHEADER;
import com.jasci.biz.AdminModule.model.MENUPROFILEHEADERPK;
import com.jasci.biz.AdminModule.model.MENUPROFILEOPTIONPK;
import com.jasci.biz.AdminModule.model.MENUPROFILEOPTIONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Service
public class MENUPROFILEMAINTENANCESERVICEIMPL implements IMENUPROFILEMAINTENANCESERVICE {

	
	@Autowired
	IMENUPROFILEMAINTENANCEDAO objIMenuProfileMaintenanceDao;
	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSION;
	
	@Autowired
	IMENUAPPICONSERVICE objMenuAppIconService;
	
	@Autowired
	LANGUANGELABELSAPI objLanguageServiceAPI;
	
	@Autowired
	IMENUOPTIONSERVICE ObjMENUOPTUONSERVICE;
	
	/** 
	 * @description  :This function is used for getting label of Menu Profile maintenance screen
	 * @param        :strMenuTypes
	 * @return       :objMENUPROFILEMAINTENANCELABELBE 
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Dec 26 2014
	 */
	
	@Transactional
	public MENUPROFILEMAINTENANCELABELBE getLanguageLabel(String language) throws JASCIEXCEPTION {

		MENUPROFILEMAINTENANCELABELBE objMenuprofilemaintenancelabelbe=null;
		List<LANGUAGES> objLanguagesList=null;
		
		
		try{
			
			objMenuprofilemaintenancelabelbe=new MENUPROFILEMAINTENANCELABELBE();
			objLanguagesList=objLanguageServiceAPI.GetScreenLabels(LANGUANGELABELSAPI.StrMenuProfileModule, language);
			for(LANGUAGES objLanguages:objLanguagesList){
				
				if(LANGUANGELABELSAPI.KP_Actions.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblAction(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Add_New.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblAddnew(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Available_Menu_Options.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					
					objMenuprofilemaintenancelabelbe.setLblAvailableMenuOptions(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Cancel.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblCancel(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Delete.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblDelete(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblDescription(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Description_Long.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLbldescriptionlong(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Description_Short.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLbldescriptionshrot(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Edit.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblEdit(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Menu_Profile.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_INVALID_MENU_PROFILE(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Part_Of_Menu_Profile_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_INVALID_PART_PART_OF_MENU_PROFILE_DESCRIPTION(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Mandatory_field_cannot_be_left_blank.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_MANDATORY_FIELD_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profile_already_used.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_MENU_PROFILE_ALREADY_USED(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_Menu_Profile.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_MENU_PROFILE_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_select_Menu_Type.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_MENU_TYPE_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Please_enter_Part_of_Menu_Profile_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_PART_OF_MENU_PROFILE_DESCRIPTION_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Help_Line.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblHelpLine(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Last_Activity_By.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblLastActivityBy(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Last_Activity_Date.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblLastActivityDate(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Option.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenuOption(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profile.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenuProfile(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profiles_Maintenance.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenuProfilesMaintenance(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profiles_Maintenance_Lookup.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenuProfilesMaintenanceLookup(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profiles_Maintenance_Search_Lookup.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenuProfilesMaintenanceSearchLookup(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profiles_Options.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenuProfilesOptions(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_New.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblNew(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_OR.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblOR(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Part_of_the_Menu_Profile_Description.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblPartoftheMenuProfileDescription(objLanguages.getTranslation());
					
					
				}else if(LANGUANGELABELSAPI.KP_SelectApplication.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
				
					objMenuprofilemaintenancelabelbe.setLblSelectApplication(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Save_Update.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblUpdate_Save(objLanguages.getTranslation());
				}else if(LANGUANGELABELSAPI.KP_Application.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblApplication(objLanguages.getTranslation());
				}else if(LANGUANGELABELSAPI.KP_Display_All.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLbldisplayall(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_MenuType.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenuType(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_App_Icon.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblMenu_App_Icon(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Select.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblSelect(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Invalid_Menu_Type.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblERR_INVALID_MENU_TYPE_LEFT_BLANK(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_APPLICATIONS.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblGeneralCodeApplication(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Your_record_has_been_saved_successfully.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblON_SAVE_SUCCESSFULLY(objLanguages.getTranslation());
				
				}else if(LANGUANGELABELSAPI.KP_Your_record_has_been_updated_successfully.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblON_UPDATE_SUCCESSFULLY(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Are_you_sure_you_want_to_delete_the_selected_row_With_an_Ok_and_Cancel_button.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLbl_ERR_WHILE_DELETING_MENU_OPTION(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Menu_Profile_is_currently_assigned_to_Menu_Assignments_if_Deleted_this_option_will_be_removed_from_all_Menu_Assignments.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLbl_ERR_WHILE_DELETING_MENU_PROFILE(objLanguages.getTranslation());
					
				}else if(LANGUANGELABELSAPI.KP_Sub_Application.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblSubApplication(objLanguages.getTranslation());
				}else if(LANGUANGELABELSAPI.KP_No_menu_option_has_been_assigned_to_the_profile.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblNo_menu_option_has_been_assigned_to_the_profile(objLanguages.getTranslation());
				}else if(LANGUANGELABELSAPI.KP_Reset.equalsIgnoreCase(objLanguages.getId().getKeyPhrase())){
					
					objMenuprofilemaintenancelabelbe.setLblReset(objLanguages.getTranslation());
				}
				
			
			}
			
			
			
		}catch(Exception objException){
			
			new JASCIEXCEPTION(objException.getMessage());
		}
		
		return objMenuprofilemaintenancelabelbe;
	}


	/** 
	 * @Description:This function is getting data from menu Profile table for edit or update
	 * @param      : strMenuProfile
	 * @return     :MENUPROFILEHEADER
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 27 2014
	 */
	@Transactional
	public MENUPROFILEBEANPOJOBE getMenuProfileList(String strMenuProfile) throws JASCIEXCEPTION {
	
		List<MENUPROFILEHEADER> objMenuprofileheadersList=null;
		MENUPROFILEHEADER objMenuprofileheader=null;
		MENUPROFILEBEANPOJOBE objMenuprofilebeanpojobe= new MENUPROFILEBEANPOJOBE();
		String strTeamMemberName=GLOBALCONSTANT.BlankString;
		
		try {
			
			objMenuprofileheadersList=objIMenuProfileMaintenanceDao.getMenuProfileList(strMenuProfile);
			
			objMenuprofileheader=objMenuprofileheadersList.get(GLOBALCONSTANT.INT_ZERO);
			
			try{
			BeanUtils.copyProperties(objMenuprofilebeanpojobe, objMenuprofileheader);
			
			}catch(IllegalAccessException objAccessException){
				new JASCIEXCEPTION(objAccessException.getMessage());
			
			}catch (InvocationTargetException objException ) {
			
				new JASCIEXCEPTION(objException.getMessage());
			}
			try{
			
			
			
				strTeamMemberName=objMenuAppIconService.getTeamMemberName(OBJCOMMONSESSION.getTenant(), objMenuprofileheader.getLastActivityTeamMember());
			}catch(Exception objException){
				
				new JASCIEXCEPTION(objException.getMessage());
			}
			
			
			objMenuprofilebeanpojobe.setDescription20(StringEscapeUtils.escapeHtml(objMenuprofilebeanpojobe.getDescription20()));
			objMenuprofilebeanpojobe.setMenuProfile(StringEscapeUtils.escapeHtml(objMenuprofileheader.getId().getMenuProfile()));
			objMenuprofilebeanpojobe.setDescription50(StringEscapeUtils.escapeHtml(objMenuprofilebeanpojobe.getDescription50()));
			objMenuprofilebeanpojobe.setLastActivityTeamMember(strTeamMemberName);
			
			//objMenuprofilebeanpojobe.setMenuProfile(objMenuprofileheader.getId().getMenuProfile());
			
		} catch (JASCIEXCEPTION objJasciexception) {
			 
			
		}
		
		return objMenuprofilebeanpojobe;
	}
	
	
	
	/** 
	 * @Description :This function is getting menu types from general code list
	 * @param 		:strMenuType
	 * @return      :List<GENERALCODES>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	
	@Transactional
	public List<GENERALCODES> getMenuTypes(String strMenuType) throws JASCIEXCEPTION {
		
try{
	
	   return ObjMENUOPTUONSERVICE.getMenuType(strMenuType);
			
		}catch(Exception objException){
			
			new JASCIEXCEPTION(objException.getMessage());
		}
		
		return null;
	}


	
	/** 
	 * @Description: This Method is used for get list of menu profile from menu profile header table
	 * @param: searchFieldValue
	 * @param :serchField
	 * @return:List<MENUPROFILEHEADER>
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date  : Dec 28 2014
	 */
	
	@Transactional
	public List<MENUPROFILEHEADER> getProfileHeaderList(String searchFieldValue, String serchField)
			throws JASCIEXCEPTION {
List<MENUPROFILEHEADER> ObjListMenuProfile=null;
		
		String strPartOfProfileDesc=GLOBALCONSTANT.BlankString;
		String strMenuType=GLOBALCONSTANT.BlankString;
		
		
		try{
			 if(serchField.equalsIgnoreCase(GLOBALCONSTANT.MENUProfile_lblPartMenuProfileDescription)){
				
				strPartOfProfileDesc=searchFieldValue;
				
				ObjListMenuProfile=objIMenuProfileMaintenanceDao.getMenuProfileListByDescription(strPartOfProfileDesc);	
				
			}else if(serchField.equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_lblMenuType)){
				
				strMenuType=searchFieldValue;
				
				ObjListMenuProfile=objIMenuProfileMaintenanceDao.getMenuProfileListByMenuType(strMenuType);
				
			
			
				
			}else{
				
				ObjListMenuProfile=objIMenuProfileMaintenanceDao.getMenuProfileDisplayAll();
				
			}
			
			
			
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
			
		}	
		return ObjListMenuProfile;
	}

	
	
	/** 
	 * @Description :This function is getting menu App Icon from MenuApp Icon table
	 * @param 		:
	 * @return      :List<MENUAPPICONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	
	
	@Transactional
	public List<MENUAPPICONS> getMenuAppIconList() throws JASCIEXCEPTION {
		
		return objIMenuProfileMaintenanceDao.getMenuAppIconList();
	}


	/** 
	 * @Description :This Method is used for save or update Menu Profile List
	 * @param 		:
	 * @return      :List<MENUPROFILEHEADER>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	@Transactional
	public Boolean SaveOrUpdate(MENUPROFILEBEANPOJOBE objMenuprofilebeanpojobe,String strJsonAssignOption) throws JASCIEXCEPTION {
		
		Boolean status=false;
		MENUPROFILEHEADERPK objMenuprofileheaderpk=new MENUPROFILEHEADERPK();
		MENUPROFILEHEADER objMenuprofileheader=new MENUPROFILEHEADER();
		 MENUPROFILEOPTIONS objMenuprofileoptions=null;
		 MENUPROFILEOPTIONPK objMenuprofileoptionpk=null;
		List<String> objListMenuOption=null;
		try{

             DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Insert_DateFormat_DD_MMM_YY);
             Date currentDate=ObjDateFormat1.parse(objMenuprofilebeanpojobe.getLastActivityDate());
             objMenuprofileheader.setLastActivityDate(currentDate);
             
             objMenuprofileheaderpk.setTenant(OBJCOMMONSESSION.getTenant());
             
             objMenuprofileheaderpk.setMenuProfile(trimValues(objMenuprofilebeanpojobe.getMenuProfile()));
             objMenuprofileheader.setId(objMenuprofileheaderpk);
             objMenuprofileheader.setMenuType(trimValues(objMenuprofilebeanpojobe.getMenuType()));
             objMenuprofileheader.setLastActivityTeamMember(OBJCOMMONSESSION.getTeam_Member());
             objMenuprofileheader.setDescription20(trimValues(objMenuprofilebeanpojobe.getDescription20()));
             objMenuprofileheader.setDescription50(trimValues(objMenuprofilebeanpojobe.getDescription50()));
             objMenuprofileheader.setAppIcon(trimValues(objMenuprofilebeanpojobe.getAppIcon()));
             
             objMenuprofileheader.setHelpLine(trimValues(objMenuprofilebeanpojobe.getHelpLine()));
             status=objIMenuProfileMaintenanceDao.SaveOrUpdate(objMenuprofileheader);
             //update menu profile Option table here
             objListMenuOption=getListMenuOption(strJsonAssignOption);
             
             for(String strMenuOption : objListMenuOption){
             objMenuprofileoptions=new MENUPROFILEOPTIONS();
             objMenuprofileoptionpk=new MENUPROFILEOPTIONPK();
             
             objMenuprofileoptionpk.setMenuOption(trimValues(strMenuOption));
             objMenuprofileoptionpk.setMenuProfile(trimValues(objMenuprofilebeanpojobe.getMenuProfile()));
             objMenuprofileoptionpk.setTenant(trimValues(OBJCOMMONSESSION.getTenant()));
             
             objMenuprofileoptions.setId(objMenuprofileoptionpk);
             objIMenuProfileMaintenanceDao.saveAssignedMenuProfile(objMenuprofileoptions);
             }
		}catch(Exception objException){
			
			new JASCIEXCEPTION(objException.getMessage());
		}
		
		return status;
	}



	/** 
	 * @Description :This Method is used for delete menu profile 
	 * @param 		:strMenuProfile
	 * @return      :Boolean
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 30 2014
	 */
	@Transactional
	public Boolean deleteMenuProfile(String strMenuProfile,String Tenant) throws JASCIEXCEPTION {
		
		
		try{
			
			return objIMenuProfileMaintenanceDao.deleteMenuProfile(strMenuProfile,Tenant);
			
		}catch(Exception objException){
			
		}
		return null;
	}

	
	// create List Of menu Option from json 
			public List<String> getListMenuOption(String strValue)
			{
					
	            List<String> lstMenuOption=new ArrayList<String>();

				JSONParser jsonParser=new JSONParser();
				try {

					Object objJson =jsonParser.parse(strValue);

					//create json array from object values
					JSONArray jsonArrayObject = (JSONArray) objJson;

					for(int subJson=GLOBALCONSTANT.IntZero;subJson<jsonArrayObject.size();subJson++){
					//fetch values from json array at first index
					JSONObject jsonObject=(JSONObject)jsonParser.parse(jsonArrayObject.get(subJson).toString());

					String menuOption=(String) jsonObject.get(GLOBALCONSTANT.MENUProfile_JsonKeyMenuOption);
					
					lstMenuOption.add(menuOption);
					
					}
					
					
				} catch (Exception objException) {

				new JASCIEXCEPTION(objException.getMessage());
					

				} 

				return lstMenuOption;

			}

			
			

			/** 
			 * @Description :This Method is used for get Team Member Name
			 * @param 		:tenant and TeammemberId
			 * @return      :strTeamMemberName
			 * @throws 		:JASCIEXCEPTION
			 * @Developedby:Sarvendra tyagi
			 * @Date	   :Dec 30 2014
			 */

			@Transactional
			public String getTeamMemberName(String strTenant,String strTeamMember)throws JASCIEXCEPTION{
				
				List<TEAMMEMBERS> objListTeammembers=null;
				String strTeamMemberName=GLOBALCONSTANT.BlankString;
				try{
				 objListTeammembers=objIMenuProfileMaintenanceDao.getTeamMemberName
						(strTenant,strTeamMember);
				
				TEAMMEMBERS objTeammembers=objListTeammembers.get(GLOBALCONSTANT.INT_ZERO);
				strTeamMemberName=objTeammembers.getFirstName()+GLOBALCONSTANT.Single_Space+objTeammembers.getLastName();
				
				}catch(Exception objException){
					
					new JASCIEXCEPTION(objException.getMessage());
					}
				
				return strTeamMemberName;
				
			}
	  
			  
			 

			/** 
			 * @Description :This Method is used for get Last Date and Last Team Member
			 * @param 		:tenant and TeammemberId
			 * @return      :strTeamMemberName
			 * @throws 		:JASCIEXCEPTION
			 * @Developedby :Sarvendra tyagi
			 * @Date	    :Dec 30 2014
			 */
			
			@Transactional
			public MENUPROFILEBEANPOJOBE getDateAndTeamMember(String strTenant,String strTeamMember)throws JASCIEXCEPTION{
				
				MENUPROFILEBEANPOJOBE objMenuprofilebeanpojobe=new MENUPROFILEBEANPOJOBE();
				
				try{
					Date ObjDate1 = new Date();
		             DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Insert_DateFormat_DD_MM_YY);
		             String StrCurrentDate=ObjDateFormat1.format(ObjDate1);
				objMenuprofilebeanpojobe.setLastActivityDate(StrCurrentDate);
				String strTeamMemberName=objMenuAppIconService.getTeamMemberName(strTenant, strTeamMember);
				
				objMenuprofilebeanpojobe.setLastActivityTeamMember(strTeamMemberName);
				}catch(Exception objException){	}
				
				return objMenuprofilebeanpojobe;
			}
			
			
			
			 /** 
			 * @Description :This Method is used for get assigned menu option in menuprofile Option table 
			 * @param 		:String Tenant, String strMenuProfile
			 * @return      :List<MENUOPTIONS>
			 * @throws 		:JASCIEXCEPTION
			 * @Developedby :Sarvendra tyagi
			 * @Date	    :Dec 31 2014
			 */
			@Transactional
		public List<MENUOPTIONS> getAssignedMenurofile(String Tenant, String strMenuProfile) throws JASCIEXCEPTION{
			
			
			List<MENUOPTIONS> objList=null;
			try{
				
				
				objList=objIMenuProfileMaintenanceDao.getAssignedMenurofile(Tenant, strMenuProfile);
				
				
			}catch(Exception objException){
				
				new JASCIEXCEPTION(objException.getMessage());
			}
			
			return objList;
		}


			/**
			 * @author Aakash Bishnoi
			 * @Date Dec 31, 2014
			 * Description This function is used to trim the space
			 * @param Value
			 * @return String
			 */
			public static String trimValues(String Value){
				if(Value == null){
					return null;
				}
				else{
					String CheckValue=Value.trim();
					if(CheckValue.length()<GLOBALCONSTANT.IntOne){
						return null;
					}else{						
						return CheckValue;
					}
				}
			}		
	
}
