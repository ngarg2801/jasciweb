/**

Developed By : Diksha Gupta
Date Developed : Oct 15 2014
Description:  Provide service layer between logincontroller and logindao
 */

package com.jasci.biz.AdminModule.service;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.LOGINBE;
import com.jasci.biz.AdminModule.be.PASSWORDEXPIRYBE;
import com.jasci.biz.AdminModule.dao.LOGINDAOIMPL;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.TENANTS;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.MAILCONFIGURATION;
import com.jasci.common.util.SENDEMAIL;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;



@Service
public class LOGINSERVICEIMPL
{

	/**
	 *Description this function design to get menumessages and tickertape messages
	 *Input parameter none
	 *Return Type List<MENUMESSAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "oct 21 2014" 
	 * */

	@Autowired
	private LOGINDAOIMPL ObjLoginDaoImpl;
	public static COMMONSESSIONBE ObjCommonSessionBe ;

	@Transactional
	public   List<MENUMESSAGES> GetMessage() throws JASCIEXCEPTION
	{
		List<MENUMESSAGES> ObjListMenuMessages=null;
		List<MENUMESSAGES> ObjListTempMenuMessages=new ArrayList<MENUMESSAGES>();

		try
		{
			/**Call this method for getting menu messages*/
			
			ObjListMenuMessages=ObjLoginDaoImpl.GetMenuMessages();

			for(MENUMESSAGES ObjMenuMSG:ObjListMenuMessages){



				Date DateStartDate=ObjMenuMSG.getStart_Date();
				Date ObjDate = new Date();
				DateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Only_Date_Format);
				Date StartDate =formatter.parse(formatter.format(DateStartDate));
				Date TodayDate =formatter.parse(formatter.format(ObjDate));

				Long StartDateMilisecs=	StartDate.getTime();
				Long TodayMilisecs=TodayDate.getTime();
				if(StartDateMilisecs<=TodayMilisecs)
				{
					/**Add menu message in list*/
					ObjListTempMenuMessages.add(ObjMenuMSG);
				}



			}

		}
		catch(JASCIEXCEPTION ObjFileNotFoundException)
		{
			ObjFileNotFoundException.getMessage();
		}
		catch(Exception e){

		}
		return ObjListTempMenuMessages;

	}


	/**
	 *Description: this function design to get  PasswordExpirationDate from Tenants
	 *Input parameter: StrTenant
	 *Return Type: List<TENANTS>
	 *Created By: "Diksha Gupta"
	 *Created Date: "oct 27 2014" 
	 * */


	@Transactional
	public   List<TENANTS> GetPasswordExpirationDate(String StrTenant) throws JASCIEXCEPTION
	{
		/**call GetPasswordExpirationDays(StrTenant) to get PasswordExpirationDays from Tenant Table*/
		List<TENANTS> ObjListTenant=ObjLoginDaoImpl.GetPasswordExpirationDays(StrTenant);
		return ObjListTenant;

	}

	/**
	 *Description: this function design to set login be from Languages table
	 *Input parameter: none
	 *Return Type: LOGINBE
	 *Created By: "Diksha Gupta"
	 *Created Date: "nov 02 2014" 
	 */


	@Transactional
	public  LOGINBE SetLoginBe() throws JASCIEXCEPTION
	{

		/**Call this method for getting login screen labels list*/
		
		LOGINBE ObjLoginBe= ObjLoginDaoImpl.SetLoginBe();			 
		return ObjLoginBe;

	}

	/**
	 *Description: this function design to set PasswordExpiryBe from Languages table
	 *Input parameter: none
	 *Return Type: PASSWORDEXPIRYBE
	 *Created By: "Diksha Gupta"
	 *Created Date: "nov 03 2014" 
	 */


	@Transactional
	public  PASSWORDEXPIRYBE SetPasswordExpiryBe() throws JASCIEXCEPTION
	{

		/**call SetPasswordExpiryBe() to set login be from Languages table*/ 

		PASSWORDEXPIRYBE ObjPasswordExpiryBe= ObjLoginDaoImpl.SetPasswordExpiryBe();			 
		return ObjPasswordExpiryBe;

	}


	/**
	 *Description: this function design to check if the emailId is valid or not in database
	 *Input parameter: StrEmailId
	 *Return Type:  List<TEAMMEMBERS>
	 *Created By: "Diksha Gupta"
	 *Created Date: "nov 08 2014" 
	 * @param lastActivityDateStringH 
	 */


	@Transactional
	public   boolean checkEmailId(String StrEmailId, String lastActivityDateStringH) throws JASCIEXCEPTION, JASCIEXCEPTION  
	{

		/** call SetPasswordExpiryBe() to set login be from Languages table*/ 
		List<TEAMMEMBERS> ObjListTeamMembers=null;
		boolean status=false;
		
			ObjListTeamMembers= ObjLoginDaoImpl.CheckEmailId(StrEmailId);			

			if(!ObjListTeamMembers.isEmpty())
			{

				TEAMMEMBERS objSecuruty=ObjListTeamMembers.get(GLOBALCONSTANT.INT_ZERO);

				String Tenant=objSecuruty.getId().getTenant();
				String TeamMember=objSecuruty.getId().getTeamMember();
				String TeamMemberFirstName = objSecuruty.getFirstName();
				String TeamMemberLastName = objSecuruty.getLastName();
				String TeamMemberName = TeamMemberFirstName+GLOBALCONSTANT.Single_Space+TeamMemberLastName;
				if(StrEmailId!=null)
				{

					//===================check invalid attempts=====================//


					List<SECURITYAUTHORIZATIONS> resultinvalidattempts = getInvalidAttempts(Tenant, TeamMember);

					Iterator<SECURITYAUTHORIZATIONS> obj_InvalidAttemptsitr = resultinvalidattempts.iterator();
					SECURITYAUTHORIZATIONS Obj_Invalidattempts = obj_InvalidAttemptsitr.next();
					int InvalidAttempts = Obj_Invalidattempts.getNumberAttempts();
					String strUSERID = Obj_Invalidattempts.getUserId();
					if(InvalidAttempts >= GLOBALCONSTANT.INT_FIVE)
					{
						throw new JASCIEXCEPTION(GLOBALCONSTANT.ErrorMessagetoInvalidAttemptForgot);
					}

					String uuid = UUID.randomUUID().toString();
					String RandomPassword=uuid.substring(GLOBALCONSTANT.INT_ZERO,uuid.indexOf(GLOBALCONSTANT.Hyphen));
					String SecurityOfficerContactNumber=GLOBALCONSTANT.BlankString;
					String SecurityOfficerEmailId=GLOBALCONSTANT.BlankString;  
					
					CONFIGURATIONEFILE ObjConfigurationFile=new CONFIGURATIONEFILE();
					SecurityOfficerContactNumber = ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.SecurityOfficerContactNumber);
					SecurityOfficerEmailId = ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.SecurityOfficerEmailId);

					String StrMessage=GLOBALCONSTANT.Security_MailPart1+TeamMemberName+GLOBALCONSTANT.Security_MailPart2+strUSERID+GLOBALCONSTANT.Security_MailPart3+RandomPassword+GLOBALCONSTANT.Security_MailPart4+SecurityOfficerContactNumber+GLOBALCONSTANT.Security_MailPart5+SecurityOfficerEmailId+GLOBALCONSTANT.Forgot_MailPart6;
				
					boolean result = SENDEMAIL.sendEmail(StrEmailId.trim(),null, null, GLOBALCONSTANT.Forget_MailSubject, StrMessage,MAILCONFIGURATION.FROM );
					

					if(result){

						try {
							status=SetTemporaryPassword(RandomPassword, Tenant, TeamMember,lastActivityDateStringH);
						} catch (JASCIEXCEPTION e) {
							

						} 


					}
				}

			}
			else
			{
				throw new JASCIEXCEPTION(GLOBALCONSTANT.EmailAddressDoesNotExit);
			}


		return status;

	}



	/**
	 *Description: this function design to check whether the user is authenticated or not and whether password is expired
	 *Input parameter: strUsername and strPassword
	 *Return Type: "boolean"
	 *Created By: "Diksha Gupta"
	 *Created Date: "oct 14 2014" 
	 * @param lastActivityDateStringH 
	 */

	@Transactional
	public COMMONSESSIONBE ValidateUser(String StrUserId, String StrPassword,COMMONSESSIONBE ObjCommonSessionBe, String lastActivityDateStringH) throws ParseException, JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION
	{

		/**call GetUserData() to get user data on the basis of entered userId*/
		List<Object> ObjListObject=ObjLoginDaoImpl.GetUserData(StrUserId,StrPassword);
		Iterator<Object> ObjIteratorObject=null;
		Object[] ObjObject = null;


		if(!(ObjListObject.isEmpty()))
		{

			/**iterate the list to get the data from list*/
			ObjIteratorObject=ObjListObject.iterator();
			ObjObject=(Object[]) ObjIteratorObject.next();
			String StrPassswordDb=GLOBALCONSTANT.BlankString;
			String StrInvalidDateDb=GLOBALCONSTANT.BlankString;
			String StrNumberAttemptsDb=GLOBALCONSTANT.BlankString;
			Long IntNumberAttempts=0L;

			try{
				ObjCommonSessionBe.setTenant(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
			}
			catch(Exception e){}
			try{
				ObjCommonSessionBe.setTeam_Member(ObjObject[GLOBALCONSTANT.IntOne].toString());
			}
			catch(Exception e){}
			try{
				ObjCommonSessionBe.setCompany(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setPasswordDate(ObjObject[GLOBALCONSTANT.INT_THREE].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setStatus(ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setCompany_Name_20(ObjObject[GLOBALCONSTANT.INT_FIVE].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setCompany_Name_50(ObjObject[GLOBALCONSTANT.INT_SIX].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setPurchaseOrdersRequireApproval(ObjObject[GLOBALCONSTANT.INT_SEVEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setFulfillmentCenter(ObjObject[GLOBALCONSTANT.INT_EIGHT].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setFulfillment_Center_Name_20(ObjObject[GLOBALCONSTANT.INT_NINE].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setFulfillment_Center_Name_50(ObjObject[GLOBALCONSTANT.INT_TEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setTenant_Language(ObjObject[GLOBALCONSTANT.INT_ELEVEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setTeam_Member_Language(ObjObject[GLOBALCONSTANT.INT_TWELVE].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setTheme_RF(ObjObject[GLOBALCONSTANT.INT_THIRTEEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setTheme_Full_Display(ObjObject[GLOBALCONSTANT.INT_FOURTEEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setPasswordExpirationDays(ObjObject[GLOBALCONSTANT.INT_FIFTEEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setTeam_Member_Name(ObjObject[GLOBALCONSTANT.INT_SIXTEEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setAuthority_Profile(ObjObject[GLOBALCONSTANT.INT_SEVENTEEN].toString());
			
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setMenu_Profile_Tablet(ObjObject[GLOBALCONSTANT.INT_EIGHTEEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setMenu_Profile_Station(ObjObject[GLOBALCONSTANT.INT_NINETEEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setMenu_Profile_RF(ObjObject[GLOBALCONSTANT.INT_TWENTY].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setMenu_Profile_Glass(ObjObject[GLOBALCONSTANT.INT_TWENTYONE].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setEquipment_Certification(ObjObject[GLOBALCONSTANT.INT_TWENTYTWO].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setSystemUse(ObjObject[GLOBALCONSTANT.INT_TWENTYTHREE].toString());
			}catch(Exception e){}
			try{
				StrPassswordDb=ObjObject[GLOBALCONSTANT.INT_TWENTYFOUR].toString();
			}catch(Exception e){}
			try{
				StrInvalidDateDb=ObjObject[GLOBALCONSTANT.INT_TWENTYFIVE].toString();
			}catch(Exception e){}
			try{
				StrNumberAttemptsDb=ObjObject[GLOBALCONSTANT.INT_TWENTYSIX].toString();
				IntNumberAttempts=Long.valueOf(StrNumberAttemptsDb);
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setTenant_NAME20(ObjObject[GLOBALCONSTANT.INT_TWENTYSEVEN].toString());
			}catch(Exception e){}
			try{
				ObjCommonSessionBe.setTenant_NAME50(ObjObject[GLOBALCONSTANT.INT_TWENTYEIGHT].toString());
			}catch(Exception e){}

			ObjCommonSessionBe.setCurrentLanguage(ObjCommonSessionBe.getTeam_Member_Language());
			try{
				if(StrPassswordDb.equals(StrPassword))
				{
					
					//feb 9 15
					try{
					      
					      List<Object> ObjListObjectResult=ObjLoginDaoImpl.GetUserDataActiveCompany(ObjCommonSessionBe.getTenant(),ObjCommonSessionBe.getTeam_Member());
					      if(ObjListObjectResult.isEmpty()){     
					      ObjCommonSessionBe.setUserLoginDetails(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_login_as_the_company_you_are_associated_with_is_inactive_Please_contact_administrator);
					      return ObjCommonSessionBe;
					       
					      }
					}
					     catch(Exception e){}
					
					String StrInvalidAttemptDate=GLOBALCONSTANT.BlankString;
					try{
						StrInvalidAttemptDate = StrInvalidDateDb.toString().replaceAll(GLOBALCONSTANT.Hyphen, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.colon, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.Single_Space, GLOBALCONSTANT.BlankString);
					}catch(Exception e){}

					/** to get current date*/
					String StrCurrentDate1=lastActivityDateStringH.substring(GLOBALCONSTANT.INT_ZERO, GLOBALCONSTANT.INT_ELEVEN).replaceAll(GLOBALCONSTANT.Hyphen, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.colon, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.Single_Space, GLOBALCONSTANT.BlankString);
					
					Long LongPasswordExpirationDays = 1L;
					try{
						LongPasswordExpirationDays= Long.valueOf(ObjCommonSessionBe.getPasswordExpirationDays().toString());
					}catch(Exception e){}

					
					String Strdate=ObjCommonSessionBe.getPasswordDate();
					
					Strdate = Strdate.replaceAll(GLOBALCONSTANT.Hyphen, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.colon, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.Single_Space, GLOBALCONSTANT.BlankString);
					DateFormat ObjDateFormat= new SimpleDateFormat(GLOBALCONSTANT.Only_Date_Format);  
					Date ObjPasswordDate = ObjDateFormat.parse(Strdate); 

					/** to get current date*/
					Date ObjDate=StringToDate(lastActivityDateStringH);
					Long diff = Math.abs(ObjDate.getTime() - ObjPasswordDate.getTime());
					Long diffDays = diff / (GLOBALCONSTANT.INT_TWENTYFOUR * GLOBALCONSTANT.INT_SIXTY * GLOBALCONSTANT.INT_SIXTY * GLOBALCONSTANT.INT_THOUSAND);

					/**check whether of invalid attempt and password expired*/
					if(StrCurrentDate1.equals(StrInvalidAttemptDate) && IntNumberAttempts>GLOBALCONSTANT.INT_FOUR)
					{
						ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.Not_Allowed);
						return ObjCommonSessionBe;
					}
					else if(LongPasswordExpirationDays<diffDays) 
					{
						ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.Password_Expired);
						return ObjCommonSessionBe;
					}
					else if(ObjCommonSessionBe.getStatus().equalsIgnoreCase(GLOBALCONSTANT.T))
					{
						ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.TemporaryPassword);
						return ObjCommonSessionBe;
					} 
					else
					{
						Date formatdatecheck=StringToDate(lastActivityDateStringH);
						
						ObjLoginDaoImpl.SetNumberAttemptstoZero(GLOBALCONSTANT.INT_ZERO, StrUserId.toUpperCase(), formatdatecheck);
						ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.Password_matched_Response);
						return ObjCommonSessionBe;
					}}

				else
				{
					String StrInvalidAttemptDate=GLOBALCONSTANT.BlankString;

					try{
						StrInvalidAttemptDate = StrInvalidDateDb.replaceAll(GLOBALCONSTANT.Hyphen, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.colon, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.Single_Space, GLOBALCONSTANT.BlankString);
						
					}catch(Exception e){}
					/** to get current date*/
					String StrCurrentDate1=lastActivityDateStringH.substring(GLOBALCONSTANT.INT_ZERO, GLOBALCONSTANT.INT_ELEVEN).replaceAll(GLOBALCONSTANT.Hyphen, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.colon, GLOBALCONSTANT.BlankString).replaceAll(GLOBALCONSTANT.Single_Space, GLOBALCONSTANT.BlankString);
					Date StrCurrentDate=StringToDate(lastActivityDateStringH);
					if(StrCurrentDate1.equals(StrInvalidAttemptDate) && IntNumberAttempts<GLOBALCONSTANT.INT_FIVE)
					{
						ObjLoginDaoImpl.SetInvalidAttempt(IntNumberAttempts+GLOBALCONSTANT.IntOne, StrUserId.toUpperCase());
					}
					else if(!StrCurrentDate1.equals(StrInvalidAttemptDate))
					{
						ObjLoginDaoImpl.SetNumberAttemptstoZero(GLOBALCONSTANT.IntOne, StrUserId.toUpperCase(), StrCurrentDate);
					}

					if(StrCurrentDate1.equals(StrInvalidAttemptDate) && IntNumberAttempts+GLOBALCONSTANT.IntOne > GLOBALCONSTANT.INT_FOUR)
					{
						ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.Not_Allowed);
						return ObjCommonSessionBe;
					}
					else
					{

						ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.Password_Not_Matched);
						return ObjCommonSessionBe;
					}
				}
			}catch(Exception e)
			{

				ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.Password_Not_Matched);
				return ObjCommonSessionBe;
			}

		}
		ObjCommonSessionBe.setUserLoginDetails(GLOBALCONSTANT.Password_Not_Matched);
		return ObjCommonSessionBe;
	}

	/**
	 *Description: this function design to get  Lastpaswords from security authorizations
	 *Input parameter: StrUserId
	 *Return Type: List<SECURITYAUTHORIZATIONS>
	 *Created By: "Diksha Gupta"
	 *Created Date: "oct 29 2014" 
	 */


	@Transactional

	public List<SECURITYAUTHORIZATIONS> GetLastPasswords(String StrUserId) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION
	{
		/** to get last 5 passwords from SecurityAuthorizations table*/
		
		List<SECURITYAUTHORIZATIONS> ObjListSecurity=null;
		ObjListSecurity=ObjLoginDaoImpl.GetLastPasswords(StrUserId);
		return ObjListSecurity;
	}



	/**
	 *Description: this function design to set new password into security authorizations
	 *Input parameter: strUserId, StrPassword
	 *Return Type: void
	 *Created By: "Diksha Gupta"
	 *Created Date: "oct 29 2014" 
	 * @param lastActivityDateStringH 
	 */


	@Transactional

	public void SetNewPassword(String StrUserId,String StrPassword, String lastActivityDateStringH) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION 
	{

		try{
			ObjLoginDaoImpl.SetNewPassword(StrUserId,StrPassword,StringToDate(lastActivityDateStringH));
		}
		catch(Exception ObjException)
		{}
	}


	/**
	 *Description: this function design to set new password into security authorizations
	 *Input parameter: strUserId, StrPassword,strTeammeber
	 *Return Type: void
	 *Created By: "Diksha Gupta"
	 *Created Date: "nov 09 2014" 
	 * @param lastActivityDateStringH 
	 */


	@Transactional

	public boolean SetTemporaryPassword(String StrPassword,String strTenant,String strTeammeber, String lastActivityDateStringH) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION 
	{
		boolean status=false;
		try{
			status=ObjLoginDaoImpl.SetTemporaryPassword(StrPassword,strTenant,strTeammeber,StringToDate(lastActivityDateStringH));

		}
		catch(Exception ObjException)
		{}
		return status;
	}

	/** 
	 * 
	 * @param strTenant
	 * @param strTeammeber
	 * @return
	 * @throws JASCIEXCEPTION
	 * @throws JASCIEXCEPTION
	 * @throws JASCIEXCEPTION
	 */
	@Transactional
	public List<SECURITYAUTHORIZATIONS> getInvalidAttempts(String strTenant,String strTeammeber) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION 
	{
		
		List<SECURITYAUTHORIZATIONS> Obj_InvalidAttemptslist=new ArrayList<SECURITYAUTHORIZATIONS>();
		try{
			Obj_InvalidAttemptslist=ObjLoginDaoImpl.getInvalidAttempts(strTenant,strTeammeber);
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(ObjException.getMessage());
		}
		return Obj_InvalidAttemptslist;
	}

	/** Check Cookies , Add Cookies and remove Cookies 
	 * @param response
	 * @param request
	 * @param struser
	 * @param rememberme
	 */

	public void saveRememberMeToken(HttpServletResponse response,HttpServletRequest request, String struser,boolean rememberme) {

		if(rememberme)
		{

			addCookie(response, struser);	
		}else
		{
			removeCookies(request,GLOBALCONSTANT.CookieLoginUserId);
		}


	}
	/**  
	 * Add cookies
	 * @param response
	 * @param struser
	 */
	
	private void addCookie(HttpServletResponse response, String struser) {
		Cookie cookie = new Cookie(GLOBALCONSTANT.CookieLoginUserId, struser);
		cookie.setPath(GLOBALCONSTANT.Forword_SLASH);
		cookie.setMaxAge(GLOBALCONSTANT.INT_SIXTY * GLOBALCONSTANT.INT_SIXTY * GLOBALCONSTANT.INT_TWENTYFOUR * GLOBALCONSTANT.INT_FIVE);
		response.addCookie(cookie);

	}

	/**  
	 * Remove cookies
	 * @param request
	 * @param COOKIE_NAME
	 */
	 

	private void removeCookies(HttpServletRequest request,String COOKIE_NAME) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (COOKIE_NAME.equals(cookie.getName())) {
					cookie.setMaxAge(GLOBALCONSTANT.INT_ZERO);
					cookie.setValue(GLOBALCONSTANT.BlankString);

				}
			}

		}
	}
	
	//It is used to change string in date
	public Date StringToDate(String ObjDate1)
	{
		
		
		SimpleDateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		Date StrCurrentDate=null;
		try {
			StrCurrentDate = ObjDateFormat1.parse(ObjDate1);
		} catch (ParseException e) {}
		
		return StrCurrentDate;
	}
}
