/*

Date Developed  dec 23 2014
Created By Deepak Sharma
Description This is used to get the screen access
 */
package com.jasci.biz.AdminModule.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jasci.biz.AdminModule.ServicesApi.SCREENACCESSSERVICEAPI;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;

/**
 * @author HP-USA-004
 *
 */
@Service
public class SCREENACCESSSERVICE {

	/**
	 * 
	 * @param StrTenant
	 * @param StrTeamMember
	 * @param StrMenuType
	 * @param StrMenuOption
	 * @return
	  
	 * Purpose:to Check for screen access
	 * CreatedBy:Deepak
	 * CretaedOn:2014-11-29
	 * 
	 */
	
	@Autowired
	private SCREENACCESSSERVICEAPI restScreenAccess;
	public WEBSERVICESTATUS  checkScreenAccess(String StrTenant,String StrTeamMember,String StrMenuType, String StrMenuOption) {
		
		// call getSecurityAuthorizationDataByTeamMeamberName to get SecurityAuthorization data based on a team member name
		
		WEBSERVICESTATUS objWebServiceStatus =restScreenAccess.checkScreenAccess(StrTenant, StrTeamMember, StrMenuType, StrMenuOption);
		
		//List<Object> listGeneralCodes = ObjTeamMemberDao.getGeneralCodeDataByCodeId(GLOBALCONSTANT.GeneralCode_SequrityQuestions);
		return objWebServiceStatus;
	}	

public WEBSERVICESTATUS  checkScreenAccess(String StrTenant,String StrTeamMember,String ExecutionPath) {
		
		// call getSecurityAuthorizationDataByTeamMeamberName to get SecurityAuthorization data based on a team member name
		
		WEBSERVICESTATUS objWebServiceStatus =restScreenAccess.checkScreenAccess(StrTenant, StrTeamMember, ExecutionPath);
		
		//List<Object> listGeneralCodes = ObjTeamMemberDao.getGeneralCodeDataByCodeId(GLOBALCONSTANT.GeneralCode_SequrityQuestions);
		return objWebServiceStatus;
	}
	
}
