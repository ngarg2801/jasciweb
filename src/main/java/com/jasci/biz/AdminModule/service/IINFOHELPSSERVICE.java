/*
Description its a interface where we declare the InfoHelpServiceImpl Functions
Created By Aakash Bishnoi  
Created Date Oct 20 2014
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.INFOHELPSBE;
import com.jasci.biz.AdminModule.model.INFOHELPS;
import com.jasci.biz.AdminModule.model.INFOHELPSBEAN;
import com.jasci.exception.JASCIEXCEPTION;


public interface IINFOHELPSSERVICE {
	
	public List<INFOHELPSBEAN> getInfoHelpList(String StrInfoHelp,String StrPartoftheDescription,String StrInfoHelpType) throws JASCIEXCEPTION;
	 
	public String insertInfoHelps(INFOHELPS ObjectInfohelps) throws JASCIEXCEPTION;
	
	//Fetch value for update
		public INFOHELPSBEAN fetchInfoHelp(String InfoHelp,String Language) throws JASCIEXCEPTION;
		
		
		//Update Records into infoHelps
		
		public String updateInfoHelps(INFOHELPS ObjectInfoHelps) throws JASCIEXCEPTION;
		
		//Delete records form Infohelps
		public Boolean deleteInfoHelps(String InfoHelp,String Language) throws JASCIEXCEPTION;
		
		//get the label and error message of InfoHelp Assignment search screen
		 public INFOHELPSBE getInfoHelpAssignmentSearchLabels(String StrLanguage) throws JASCIEXCEPTION;
		 
	
		 /**
		     * 
		     * @Description get team member name form team members table based on team member id
		     * @param Tenant
		     * @param Company
		     * @param TeamMember
		     * @return
		     * @throws JASCIEXCEPTION
		     */
		    public String getTeamMemberName(String Tenant,String TeamMember)throws JASCIEXCEPTION;
		    
		    
}
