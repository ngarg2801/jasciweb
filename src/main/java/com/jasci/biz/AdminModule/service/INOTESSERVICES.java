/*

Date Developed  dec 23 2014
Created By "Rakesh pal"
Description  Provide service layer between controller and Rest full service  layer.
 */


package com.jasci.biz.AdminModule.service;

import java.text.ParseException;
import java.util.List;

import com.jasci.biz.AdminModule.be.NOTESFIELDBE;
import com.jasci.biz.AdminModule.be.NOTESLABELBE;
import com.jasci.common.utilbe.NOTESBE;
import com.jasci.exception.JASCIEXCEPTION;


public interface INOTESSERVICES {

	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 23, 2014
	 * @param NotesID
	 * @param NotesLink
	 * @param Teammember
	 * @return NotesList
	 * @throws JASCIEXCEPTION
	 */
	
	public  List<NOTESFIELDBE>  getNotesList(String Notes_Link,String Notes_Id,String StrtenatID, String StrCompanyID) throws JASCIEXCEPTION;

	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 23, 2014
	 * @param NOTESBE
	 * @param Teammember
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	public Boolean addOrUpdateNotes(NOTESBE objNotesbe)throws JASCIEXCEPTION;
	
	
	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 23, 2014
	 * @param NotesID
	 * @param NotesLink
	 * @param Teammember
	 * @param Company_id
	 * @param Tenant_id
	 * @return Note_to_update
	 * @throws JASCIEXCEPTION
	 */
	public List<NOTESFIELDBE> getnote(String Notes_Link,String Notes_Id,String Tenant_id,String Company_id,int ID)throws JASCIEXCEPTION;


	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 23, 2014
	 * @param NotesID
	 * @param NotesLink
	 * @param Teammember
	 * @param Company_id
	 * @param Tenant_id
	 * @return Note_to_update
	 * @throws JASCIEXCEPTION
	 * @throws ParseException 
	 */

	public Boolean addOrUpdateNotesEntry(NOTESFIELDBE objNotesFiesdbe) throws JASCIEXCEPTION, ParseException;
	
	
	
	/**
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#deleteMenuAppIcon(java.lang.String, java.lang.String)
	 */
	public Boolean deleteNote(String Tenant_Id, String Company_id,String Note_id,String Note_link,String ID) throws JASCIEXCEPTION; 

	
	/**
	 * @author Rakesh Pal
	 * @Date Jan ,06 2015
	 * @param StrModuleName
	 * @param StrLanguageCode
	 * @return List_of_label
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE#deleteMenuAppIcon(java.lang.String, java.lang.String)
	
	 */
	public NOTESLABELBE getNotesLabels(String StrModuleName,String StrLanguageCode);
}


