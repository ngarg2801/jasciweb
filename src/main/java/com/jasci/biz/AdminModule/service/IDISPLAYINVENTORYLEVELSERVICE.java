/**
 *     
 * @author Shailendra Rajput

 * @Date 8 October,2015 
 * @Description Its a interface where we define the function that implement in class of DisplayInventoryLevelServiceImplement
 */
package com.jasci.biz.AdminModule.service;

import java.util.List;

import com.jasci.biz.AdminModule.be.PRODUCTINLOCATIONBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface IDISPLAYINVENTORYLEVELSERVICE {
	public  List<PRODUCTINLOCATIONBE> getDataFromInventoryLevels(String Tenant_ID,String FulfillmentCenter,String Company_ID,String Area,String Location) throws JASCIEXCEPTION ;
	public  List<PRODUCTINLOCATIONBE> getDataFromInventoryLevelsAll(String Tenant_ID,String FulfillmentCenter,String Company_ID,String Area,String Location) throws JASCIEXCEPTION ;
	
}
