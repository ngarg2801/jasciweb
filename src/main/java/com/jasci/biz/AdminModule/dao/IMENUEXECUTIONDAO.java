/*

Date Developed  Oct 15 2014
Description  Provide database connection
 */




package com.jasci.biz.AdminModule.dao;

import java.util.List;
import com.jasci.biz.AdminModule.be.MENUEXECUTIONLISTBE;
import com.jasci.biz.AdminModule.be.SUBMENULISTBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIESBEAN;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;






public interface IMENUEXECUTIONDAO {
	
	
	//function to get company data on basis of team member from two tables team member company and companies table
	public List<TEAMMEMBERCOMPANIESBEAN> GetDataFromTeamMemberCompanies(COMMONSESSIONBE ObjCommonSession) throws JASCIEXCEPTION;
	
	
	//function to get menu profile List and sub menu
	public List<MENUEXECUTIONLISTBE> GetAssignedMenuProfileList(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION;
	
	
	
	//function to get submenu list
	
	public SUBMENULISTBE GetSubMenuList(COMMONSESSIONBE ObjCommonSession) throws JASCIEXCEPTION;
	
	
	
	//function to get label from tabel
	public List<LANGUAGES> GetScreenLabel(String language,String screenName)throws JASCIEXCEPTION;
	
	/** 
	 * @Description: This function is used for getting label of menuExecution all screen
	 * @param  :language
	 * @return :List<LANGUAGES>
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Sarvendra Tyagi
	 * @Date :dec 26 2014
	 */
	
	
	public List<LANGUAGES> getLanguageLabel(String language)throws JASCIEXCEPTION;
	
	
	

}
