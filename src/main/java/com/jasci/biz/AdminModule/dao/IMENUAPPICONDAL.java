/**

Date Developed  Dec 14 2014
Created By "Rahul Kumar"
Description : define an interface method to communicate with DataBase 
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.exception.JASCIEXCEPTION;

public interface IMENUAPPICONDAL {

	
	/**
	 * @description set screen label language
	 * @author Rahul Kumar
	 * @Date Dec 12, 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<LANGUAGES>
	 *
	 */
	public List<LANGUAGES> setScreenLanguage( String StrScreenName,String StrLanguage)throws  JASCIEXCEPTION;
	
	/**
	 * @description add or update an app icon
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param objMenuappicons
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return Boolean
	 *
	 */
	public Boolean addOrUpdateMenuAppIcon(MENUAPPICONS objMenuappicons) throws JASCIEXCEPTION;
	
	
	/**
	 * @description get app icon list from 
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param Tenant
	 * @param Company
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<MENUAPPICONS>
	 *
	 */
	public List<MENUAPPICONS> getManuAppIconList(String Tenant,String Company)throws JASCIEXCEPTION;

    
	/**
	 * @description get app icon byicon part
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014 
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<MENUAPPICONS>
	 */
    public List<MENUAPPICONS> getMenuAppIconByAppIcon(String AppIcon)throws JASCIEXCEPTION;
	
    
    
    
	
    /**
     * @description get app icon by its name
     * @author Rahul Kumar
     * @Date Dec 15, 2014
     * @param AppIconName
     * @return
     * @throws JASCIEXCEPTION
     * @Return List<MENUAPPICONS>
     *
     */
    public List<MENUAPPICONS> getMenuAppIconByAppIconName(String AppIconName)throws JASCIEXCEPTION;
    
    /**
     * @description get app icon by icon part
     * @author Rahul Kumar
     * @Date Dec 15, 2014
     * @param AppIconPart
     * @return
     * @throws JASCIEXCEPTION
     * @Return List<MENUAPPICONS>
     *
     */
    public List<MENUAPPICONS> getMenuAppIconByAppIconPart(String AppIconPart)throws JASCIEXCEPTION;
    
	
	
   /**
    * @description get application list from general code table
    * @author Rahul Kumar
    * @Date Dec 16, 2014 
    * @param GeneralCodeId
    * @return
    * @throws JASCIEXCEPTION
    * List<GENERALCODES>
    */
    public List<GENERALCODES> getApplicationList(String Tenant,String Compnay,String GeneralCodeId) throws JASCIEXCEPTION;

    
    /**
     * @description delete an app icon
     * @author Rahul Kumar
     * @Date Dec 17, 2014 
     * @param Application
     * @param AppIcon
     * @return
     * @throws JASCIEXCEPTION
     * Boolean
     */
    public Boolean deleteMenuAppIcon(String Application,String AppIcon) throws JASCIEXCEPTION;
    
    
    /**
     * @description get menu app icon by appliacation
     * @author Rahul Kumar
     * @Date Dec 17, 2014 
     * @param Application
     * @return
     * @throws JASCIEXCEPTION
     * List<MENUAPPICONS>
     */
    public List<MENUAPPICONS> getMenuAppIconByApplication(String Application)throws JASCIEXCEPTION;
    
    
    /**
     * 
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
    public List<TEAMMEMBERS> getTeamMemberName(String Tenant,String TeamMember)throws JASCIEXCEPTION;
    
}
