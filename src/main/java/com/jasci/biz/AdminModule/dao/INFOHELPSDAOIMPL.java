/*

Created By Aakash Bishnoi
Created Date Oct 29 2014
Description   InfoHelps dao implementation for implement business logic
 */
package com.jasci.biz.AdminModule.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.INFOHELPS;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Repository
public class INFOHELPSDAOIMPL implements IINFOHELPSDAO 
{
  
	@Autowired
	SessionFactory sessionFactory;	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSISSIONBE;
	
	//this is used to get the list behalf of selection like infohelp, infohelptype and discription(both)
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getInfoHelpList(String StrInfoHelp,String StrPartoftheDescription,String StrInfoHelpType) throws JASCIEXCEPTION{

		List<Object> CombinedList=new ArrayList<Object>();
		List<INFOHELPS> InfoHelpsList=null;
		List<GENERALCODES> GeneralCocdeList=null;
		String DescriptionValue=GLOBALCONSTANT.StrPercentSign+StrPartoftheDescription.toUpperCase()+GLOBALCONSTANT.StrPercentSign;
		try{
		if( GLOBALCONSTANT.BlankString.equals(StrInfoHelp) && GLOBALCONSTANT.BlankString.equals(StrPartoftheDescription) && GLOBALCONSTANT.BlankString.equals(StrInfoHelpType) ){
		
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.InfoHelps_QueryNameGetAllList);
			
			InfoHelpsList = ObjQuery.list();	
		}
		else if(!GLOBALCONSTANT.BlankString.equals(StrInfoHelp)){			
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.InfoHelps_QueryNameSearchLookup);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrInfoHelp.toUpperCase());
			InfoHelpsList = ObjQuery.list();			
		}
		else if(!GLOBALCONSTANT.BlankString.equals(StrPartoftheDescription)){
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.InfoHelps_QuerySearchPartOfTheDescription);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, DescriptionValue);
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, DescriptionValue);
			InfoHelpsList = ObjQuery.list();	
		}
		else if(!GLOBALCONSTANT.BlankString.equals(StrInfoHelpType)){
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.InfoHelps_QuerySearchInfoHelpType);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrInfoHelpType.toUpperCase());
			InfoHelpsList = ObjQuery.list();	
		}
	
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.InfoHelps_QueryNameGeneralCodeList);
			GeneralCocdeList=ObjQuery.list();
			
			CombinedList.add(InfoHelpsList);
			CombinedList.add(GeneralCocdeList);
			
			return CombinedList;
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(GLOBALCONSTANT.InfoHelp_DataBaseException);
		}
				
	}
	
	//It is used for insert data in INFOHELPS table
		public String insertInfoHelps(INFOHELPS ObjectInfoHelps) throws JASCIEXCEPTION {		
			String Status=GLOBALCONSTANT.StatusFlase;
			
			//For save the data using hibernet function
			try{
			sessionFactory.getCurrentSession().save(ObjectInfoHelps);	
			Status=GLOBALCONSTANT.StatusTrue;
			
			}
			catch(Exception ObjectException){
				
				throw new JASCIEXCEPTION(GLOBALCONSTANT.InfoHelp_DataBaseException);
				
			}
			return Status;
		}			
		
		
		
		//This is used display All record form Infohepls table
		@SuppressWarnings(GLOBALCONSTANT.Strfinally)
		public List<INFOHELPS> getInfoHelpList(){
			//Query return all Data Form Infohelps table
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.InfoHelps_QueryNameGetAllList);
			
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			List<INFOHELPS> InfohelpsListAll = ObjQuery.list();	
			try{							
				return InfohelpsListAll;
			}
			catch(Exception ObjectException){
				throw new JASCIEXCEPTION(GLOBALCONSTANT.InfoHelp_DataBaseException);
			}finally{
				return InfohelpsListAll;
			}			
		}

		//This is used to uniqueness of infohelps
		@SuppressWarnings(GLOBALCONSTANT.Strfinally)
		public INFOHELPS checkInfoHelp(String InfoHelp,String Language){
			//Query return all Data Form Infohelps table
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.InfoHelps_QueryNameFetchInfoHelp);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, InfoHelp.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, Language.toUpperCase());
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			List<INFOHELPS> InfohelpsListAll = ObjQuery.list();	
			
			INFOHELPS ObjectInfoHelps=null; 
			
			try{			
				 ObjectInfoHelps=InfohelpsListAll.get(GLOBALCONSTANT.INT_ZERO);
				return ObjectInfoHelps;
			}
			catch(Exception ObjectException){
				throw new JASCIEXCEPTION(GLOBALCONSTANT.InfoHelp_DataBaseException);
			}finally{
				return ObjectInfoHelps;
			}			
		}

		
		//This is used to fetch data from infohelp on the basis of Infohelp and language
				@SuppressWarnings(GLOBALCONSTANT.Strfinally)
				public INFOHELPS fetchInfoHelp(String InfoHelp,String Language){
					//Query return all Data Form Infohelps table
					Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.InfoHelps_QueryNameFetchInfoHelp);
					ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, InfoHelp.toUpperCase());
					ObjQuery.setParameter(GLOBALCONSTANT.IntOne, Language.toUpperCase());
					@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
					List<INFOHELPS> InfohelpsListAll = ObjQuery.list();	
					INFOHELPS ObjectInfoHelps= InfohelpsListAll.get(GLOBALCONSTANT.INT_ZERO);
					
					try{			
						
						return ObjectInfoHelps;
					}
					catch(Exception ObjectException){
						throw new JASCIEXCEPTION(GLOBALCONSTANT.InfoHelp_DataBaseException);
					}finally{
						return ObjectInfoHelps;
					}			
				}
				
				
				//It is used for update data in INFOHELPS table
				
				public String updateInfoHelps(INFOHELPS ObjectInfoHelps) throws JASCIEXCEPTION {
					String Status=GLOBALCONSTANT.StatusFlase;
					
					//For save the data using hibernate function
					try{
					sessionFactory.getCurrentSession().saveOrUpdate(ObjectInfoHelps);	
					Status=GLOBALCONSTANT.StatusTrue;
					
					}
					catch(Exception ObjectException){
						
						throw new JASCIEXCEPTION(GLOBALCONSTANT.InfoHelp_DataBaseException);
						
					}
					return Status;
				}			
				
				@SuppressWarnings(GLOBALCONSTANT.Strfinally)
				public Boolean deleteInfoHelps(String InfoHelp,String Language) throws JASCIEXCEPTION {
					
					Boolean Status=false;
					try{
					Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.InfoHelps_QueryNameDeleteInfoHelp);
					ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, InfoHelp.toUpperCase());
					ObjQuery.setParameter(GLOBALCONSTANT.IntOne, Language.toUpperCase());
					
					ObjQuery.executeUpdate();
	
						Status=true;
						return Status;
					}
					catch(Exception ObjectException){
						throw new JASCIEXCEPTION(GLOBALCONSTANT.InfoHelp_DataBaseException);
					}
					finally{
						return Status;
					}
				}
				
				
				//It is used to get the list of screen label text from Language Table 
				@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
				public List<LANGUAGES> GetLanguage(String StrLanguage) throws JASCIEXCEPTION	  
				{	 
					try
					{
						return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.InfoHelpSearchlookup_Language_Query).setParameter(GLOBALCONSTANT.INT_ZERO, StrLanguage.toUpperCase()).list();	   
					}
					catch(Exception ObjException)
					{
						throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
					}	
				}
				
				//It is used to set the label in Info-Help Assignment Search screen
				public List<LANGUAGES> getInfoHelpAssignmentSearchLabels(String StrLanguage) throws  JASCIEXCEPTION
				{
					try{
					List<LANGUAGES> ObjListLanguages=GetLanguage(StrLanguage);
						return ObjListLanguages;
					}
					catch(Exception ObjException)
					{
						throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
					}	
				}
				
				 @SuppressWarnings(GLOBALCONSTANT.Strunchecked)
				public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
					 
					  List<TEAMMEMBERS> objTeammembers=null;
					  try
					  {
					   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
					  }
					  catch(Exception ObjException)
					  {
					   throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
					  }
					  
					  if(objTeammembers.size()==GLOBALCONSTANT.INT_ZERO){
					  try
					  {
					   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query_By_TEAMID).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.trim().toUpperCase()).list();
					  }
					  catch(Exception ObjException)
					  {
					   throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
					  }
					 }
					  return objTeammembers;
					 }
 }

