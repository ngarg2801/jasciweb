/*
Description for implement the GeneralCodeDao in which we implement the hibernet functions
Created By Aakash Bishnoi  
Created Date Oct 20 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.GENERALCODESPK;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

import org.springframework.stereotype.Repository;

@Repository
public class GENERALCODESDAOIMPL implements IGENERALCODEDAO {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	
	
	
	
	//It is used to get the list of screen GenerelCodeEditDelete text from Language Table 
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> GetLanguage(String StrLanguage) throws JASCIEXCEPTION	  
	{	 
		try
		{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.GeneralCode_Languages_Query).setParameter(GLOBALCONSTANT.INT_ZERO, StrLanguage).list();	   
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}	
	}
	
	//It is used to set Label on GeneralCodeEditDelete screen  
		public List<LANGUAGES> SetGeneralCodesBe(String StrLanguage) throws JASCIEXCEPTION
		{

			try{
			List<LANGUAGES> ObjListLanguages=GetLanguage(StrLanguage);
			return ObjListLanguages;
			}catch(SQLGrammarException objException){
				throw new JASCIEXCEPTION(objException.getMessage());
			}
			catch(Exception ObjException)
			{
				throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
			}	
		}
		
	//It is used for insert data in GENERAL_CODES table
	public String addEntry(GENERALCODES ObjectGeneralCodes,MENUOPTIONS Obj_MENUOPTIONS)  throws JASCIEXCEPTION {
		try{
				
			Serializable SerializableObject=GLOBALCONSTANT.BlankString;
				Session session = sessionFactory.getCurrentSession();
				session.save(Obj_MENUOPTIONS);	
				session.save(ObjectGeneralCodes);	
				return (String) SerializableObject;			
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}		
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_AddException);
		}
		
	}

	//It is used for insert data in GENERAL_CODES table for GeneralCodeID screen(New)
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			public String addGeneralCodeIdEntry(GENERALCODES ObjectGeneralCodes)  throws JASCIEXCEPTION {
				try{
						
				
					//used to insert data in one table only
					Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.GeneralCodes_QueryNameCheckUniqueManageGeneralCode);
					Serializable SerializableObject=GLOBALCONSTANT.BlankString;
					ObjQuery.setString(GLOBALCONSTANT.INT_ZERO,ObjectGeneralCodes.getId().getTenant().toUpperCase());
					ObjQuery.setString(GLOBALCONSTANT.IntOne,ObjectGeneralCodes.getId().getCompany().toUpperCase());		
					ObjQuery.setString(GLOBALCONSTANT.INT_TWO,ObjectGeneralCodes.getId().getApplication().toUpperCase());
					ObjQuery.setString(GLOBALCONSTANT.INT_THREE,ObjectGeneralCodes.getId().getGeneralCodeID().toUpperCase());
					ObjQuery.setString(GLOBALCONSTANT.INT_FOUR,ObjectGeneralCodes.getId().getGeneralCode().toUpperCase());
					
					
					//for Check dublicate value in table GENERAL_Codes for column generalcode using manage generalcodeid 
					List<Object> UniqueGeneralCode = ObjQuery.list();
					if(UniqueGeneralCode.size()<=GLOBALCONSTANT.INT_ZERO){
						Session session = sessionFactory.openSession();
						Transaction TransactionObject = session.beginTransaction();
						session.save(ObjectGeneralCodes);	
						
						TransactionObject.commit();
						session.close();
					return (String) SerializableObject;
					}
					else{
						throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_AddException);
					}
				}	catch(SQLGrammarException objException){
					throw new JASCIEXCEPTION(objException.getMessage());
				}		
				catch(Exception ObjException)
				{
					throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_AddException);
				}
				
			}
	
		
	//It is used for get List from GENERAL_CODES table	
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally, GLOBALCONSTANT.Strunchecked })
	public List<GENERALCODES> getList(String StrTenant,String StrCompany ) throws JASCIEXCEPTION {
	
		try
		{
			String Company = StrCompany;
			String Tenant=StrTenant;
			String TeamMember=OBJCOMMONSESSIONBE.getTeam_Member().toUpperCase();
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.GeneralCodes_QueryNameGeneralCodeList);
			ObjQuery.setString(GLOBALCONSTANT.INT_ZERO, Tenant);
			ObjQuery.setString(GLOBALCONSTANT.IntOne, Company);
			ObjQuery.setString(GLOBALCONSTANT.INT_TWO,TeamMember );
			List<GENERALCODES> GeneralCodesList = ObjQuery.list();
			try{					
				return GeneralCodesList;
			}catch(IndexOutOfBoundsException ObjectIndexoutofboundsException){		
				throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_IndexOutOfBoundsException);
			}
			finally{	
				return GeneralCodesList;
			}
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}	
	}
	//It is for provide ID(composite key) in GENERAL_CODES table and get data for showing in GeneralCodeEdit Screen.
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public GENERALCODES getEntry(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode) throws JASCIEXCEPTION{
		try{
			Query objqry = sessionFactory.getCurrentSession().getNamedQuery("GeneralCodeEditListUnique");
			objqry.setString(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase());
			objqry.setString(GLOBALCONSTANT.IntOne, Company.toUpperCase());
			objqry.setString(GLOBALCONSTANT.INT_TWO, Application.toUpperCase());
			objqry.setString(GLOBALCONSTANT.INT_THREE, GeneralCodeID.toUpperCase());
			objqry.setString(GLOBALCONSTANT.INT_FOUR, GeneralCode.toUpperCase());
			List<GENERALCODES> objls = objqry.list();
			GENERALCODES ObjectGeneralCodes = objls.get(GLOBALCONSTANT.INT_ZERO);
			return ObjectGeneralCodes;
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Update);
		}  

	}

	//It is used for Update data in GENERAL_CODES table for screen generalcode and menu options
	public String updateEntry(GENERALCODES ObjectGeneralCodes,MENUOPTIONS Obj_MENUOPTIONS) throws JASCIEXCEPTION {
		try
		{
			Serializable SerializableObject=GLOBALCONSTANT.BlankString;
			
				Session session = sessionFactory.openSession();
				Transaction TransactionObject = session.beginTransaction();
				session.saveOrUpdate(ObjectGeneralCodes);
				session.saveOrUpdate(Obj_MENUOPTIONS);
				TransactionObject.commit();
				session.close();
			return (String) SerializableObject;
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Update);
		}   
	}

	//It is used for Update data in GENERAL_CODES table for the screen GeneralCOdeID (it is used in updateEntry function on controller 
			public String updateGeneralCodeID(GENERALCODES ObjectGeneralCodes) throws JASCIEXCEPTION {
				try
				{
					Serializable SerializableObject=GLOBALCONSTANT.BlankString;
					try{
						Session session = sessionFactory.openSession();
						Transaction TransactionObject = session.beginTransaction();
						
						session.saveOrUpdate(ObjectGeneralCodes);
						
						TransactionObject.commit();
						session.close();
						return (String) SerializableObject;
					}catch(Exception ObjException)
					{				
						return (String) SerializableObject;
					}
				}
				
				catch(Exception ObjException)
				{
					throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Update);
				}   

			}
	
			@SuppressWarnings(GLOBALCONSTANT.Strfinally)
			 public Boolean deleteEntry(String tenant,String company,String application,String generalcodeid,String generalcode) throws JASCIEXCEPTION {
			  
			  Boolean Status=false;
			  try
			  {
			   GENERALCODESPK obj_GENERALCODESPK=new GENERALCODESPK();
			   obj_GENERALCODESPK.setTenant(tenant);
			   obj_GENERALCODESPK.setCompany(company);
			   obj_GENERALCODESPK.setApplication(application);
			   obj_GENERALCODESPK.setGeneralCodeID(generalcodeid);
			   obj_GENERALCODESPK.setGeneralCode(generalcode);         
			   Session session = sessionFactory.getCurrentSession();
			   
			   try{
			   //It is used to delete record from GENERAL_CODES TABLE.
			   GENERALCODES ObjectGeneralCodes = (GENERALCODES) session.load(GENERALCODES.class, obj_GENERALCODESPK);
			   
			   //It is used to delete All record related to GeneralCode In GENERALCODES TABLE
			   Query ObjQuery=session.getNamedQuery(GLOBALCONSTANT.GeneralCodes_QueryNameDeleteRelatedGeneralCode);
			   ObjQuery.setString(GLOBALCONSTANT.INT_ZERO, generalcode.toUpperCase());
			   ObjQuery.executeUpdate();
			   //It is used to delete record from MENU_PROFILE_OPTION Table..
			   Query ObjQueryMenuProfileOption=session.getNamedQuery(GLOBALCONSTANT.MenuProfileOption_QueryNameDeleteMenuProfileOption);
			   ObjQueryMenuProfileOption.setString(GLOBALCONSTANT.INT_ZERO, ObjectGeneralCodes.getMenuOptionName().toUpperCase());
			   ObjQueryMenuProfileOption.setString(GLOBALCONSTANT.IntOne, tenant.toUpperCase());
			   ObjQueryMenuProfileOption.executeUpdate();
			   
			   //It is used to delete record from MENU_OPTION TABLE.
			   MENUOPTIONS ObjectMenuOptions=(MENUOPTIONS) session.load(MENUOPTIONS.class,ObjectGeneralCodes.getMenuOptionName());
			   session.delete(ObjectMenuOptions);
			   session.delete(ObjectGeneralCodes);
			   
			   Status=true;
			   }catch(SQLGrammarException objException){
				   Status=false;
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			   catch(Exception Ex){
			    Status=false;
			   }
			  
			  }catch(SQLGrammarException objException){
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			  
			  catch(Exception ObjException)
			  {
			   throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
			  }
			  finally{
			   return Status;
			  }
			}
			
			
	@SuppressWarnings("finally")
	public Boolean deleteGeneralCodeID(String tenant,String company,String application,String generalcodeid,String generalcode) throws JASCIEXCEPTION {
		
		Boolean Status=false;
		try
		{
			GENERALCODESPK obj_GENERALCODESPK=new GENERALCODESPK();
			obj_GENERALCODESPK.setTenant(tenant);
			obj_GENERALCODESPK.setCompany(company);
			obj_GENERALCODESPK.setApplication(application);
			obj_GENERALCODESPK.setGeneralCodeID(generalcodeid);
			obj_GENERALCODESPK.setGeneralCode(generalcode);
			
			
			Session session = sessionFactory.openSession();
			Transaction TransactionObject = session.beginTransaction();
			try{
			GENERALCODES ObjectGeneralCodes = (GENERALCODES) session.load(GENERALCODES.class, obj_GENERALCODESPK);
			
			session.delete(ObjectGeneralCodes);
			TransactionObject.commit();
			
			
			session.getIdentifier(ObjectGeneralCodes);
			Status=true;
			}catch(Exception Ex){
				Status=false;
			}
			session.close();
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}
		finally{
			return Status;
		}
		
		

	}
	

	//List to get General Code for new Screen
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally, GLOBALCONSTANT.Strunchecked })
	public List<GENERALCODES> getGeneralCodeNewList(String Tenant,String Company,String Application,String GeneralCode,String GeneralCodeID)throws JASCIEXCEPTION{		

	
		Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.STR_GeneralCodeidentificationList);
		ObjQuery.setString(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase());
		ObjQuery.setString(GLOBALCONSTANT.IntOne, Company.toUpperCase());

		List<GENERALCODES> ApplicationList = ObjQuery.list();
		try{			
			return ApplicationList;
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}  
		finally{
			return ApplicationList;
		}

	}

	//List to get General Code for new Screen
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally, GLOBALCONSTANT.Strunchecked })
	public List<GENERALCODES> getGeneralCodeNewSubList(String Tenant,String Company,String GeneralCode)throws JASCIEXCEPTION{		

		Session session = sessionFactory.openSession();
		Query query = session.createQuery(GLOBALCONSTANT.GeneralCodes_QueryGeneralCodeNewSubList);
		query.setParameter(GLOBALCONSTANT.INT_ZERO, GeneralCode.toUpperCase());
		query.setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase());
		query.setParameter(GLOBALCONSTANT.INT_TWO, Company.toUpperCase());
		

		List<GENERALCODES> GeneralCodeNewSubList = query.list();

		try{			
			session.close();
			return GeneralCodeNewSubList;
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}  
		finally{

			return GeneralCodeNewSubList;
		}
	}

	//It is used for Add data in GENERAL_CODES table Using General Code Identification  Screen
	public String addGeneralCode(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode,GENERALCODES ObjectGeneralCodes)  throws JASCIEXCEPTION {
		try{
			Session session = sessionFactory.openSession();
			Transaction TransactionObject = session.beginTransaction();
			//For save the data using hibernet function
			session.save(ObjectGeneralCodes);	
			TransactionObject.commit();
			Serializable SerializableObject = session.getIdentifier(ObjectGeneralCodes);		
			session.close();
			return (String) SerializableObject;
		}		
		
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}	 
	}

	//It is for provide ID(Composite key) in GENERAL_CODES table and get data for showing in GeneralCode identification  Screen.
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public GENERALCODES getGeneralCode(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode) throws JASCIEXCEPTION{
		try{
			// Use Native query
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.STR_GeneralCodeIdentifiedEdit);		  
			ObjQuery.setString(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.IntOne, Company.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.INT_TWO, Application.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.INT_THREE, GeneralCodeID.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.INT_FOUR, GeneralCode.toUpperCase());
			List<GENERALCODES> ObjList = ObjQuery.list();
			GENERALCODES ObjectGeneralCodes = ObjList.get(GLOBALCONSTANT.INT_ZERO);
			return ObjectGeneralCodes;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}  

	}


	//It is used for Update data in GENERAL_CODES table for general code identification screen
	public String updateGeneralCode(GENERALCODES ObjectGeneralCodes) throws JASCIEXCEPTION {
		try
		{
			Session session = sessionFactory.openSession();
			Transaction TransactionObject = session.beginTransaction();
			//For update record in General_Codes table
			session.saveOrUpdate(ObjectGeneralCodes);
			
			TransactionObject.commit();
			Serializable SerializableObject = session.getIdentifier(ObjectGeneralCodes);
			session.close();
			return (String) SerializableObject;
		} 
		
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}   

	}

	

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getMainGeneralCodeList(String Tenant,String Company,String GeneralCode)throws JASCIEXCEPTION{
		try{
			// Use Native query
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.GeneralCodes_QueryNameGeneralCodeMainList);		  
			ObjQuery.setString(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.IntOne, Company.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.INT_TWO, GeneralCode.toUpperCase());

			List<GENERALCODES> MainList = ObjQuery.list();

			   
			return MainList;
		}
		
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		} 

	}
	//get the sub list of general code screen behalf of generalcodeid		 
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getSubGeneralCodeList(String Tenant,String Company,String GeneralCodeID)throws JASCIEXCEPTION{
		try{
			// Use Native query
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.GeneralCodes_QueryNameGeneralCodeSubList);		  
			ObjQuery.setString(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.IntOne, Company.toUpperCase());
			ObjQuery.setString(GLOBALCONSTANT.INT_TWO, GeneralCodeID.toUpperCase());
			List<GENERALCODES> SubList = ObjQuery.list();
				return SubList;
		}
		
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		} 

	}
	
	
	 /**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description20 in GeneralCodes TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param Application
	 * @param GeneralCodeID
	 * @param GeneralCode
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getDescription20Unique(String StrTenant,String StrCompany,String StrApplication,String StrGeneralCodeID,String StrGeneralCode,String StrDescription20,String StrScreenName) throws JASCIEXCEPTION{
		List<GENERALCODES> UniqueDescription20List=null;
		try{	

			if(StrScreenName.equalsIgnoreCase(GLOBALCONSTANT.GeneralCodes_Edit)){
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Description20Unique_Query_Edit);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrGeneralCodeID.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrGeneralCode.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrDescription20.toUpperCase().trim());
				UniqueDescription20List = ObjQuery.list();
			}
			else{
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Description20Unique_Query);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrGeneralCodeID.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrDescription20.toUpperCase().trim());
				UniqueDescription20List = ObjQuery.list();
			}
			return UniqueDescription20List;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}		
	}
	
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description20 in GeneralCodes TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param Application
	 * @param GeneralCodeID
	 * @param GeneralCode
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getDescription50Unique(String StrTenant,String StrCompany,String StrApplication,String StrGeneralCodeID,String StrGeneralCode,String StrDescription50,String StrScreenName) throws JASCIEXCEPTION{
		List<GENERALCODES> UniqueDescription50List=null;
		try{	
			if(StrScreenName.equalsIgnoreCase(GLOBALCONSTANT.GeneralCodes_Edit)){
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Description50Unique_Query_Edit);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrGeneralCodeID.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrGeneralCode.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrDescription50.toUpperCase().trim());
				UniqueDescription50List = ObjQuery.list();
			}
			else{
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Description50Unique_Query);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrGeneralCodeID.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrDescription50.toUpperCase().trim());
				UniqueDescription50List = ObjQuery.list();
			}
			return UniqueDescription50List;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}		
	}
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of GeneralCode in GeneralCodes TABLE before add record 
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrApplication
	 * @param StrGeneralCodeID
	 * @param StrGeneralCode
	 * @return List<GENERALCODES>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getGeneralCodeUnique(String StrTenant,String StrCompany,String StrApplication,String StrGeneralCodeID,String StrGeneralCode) throws JASCIEXCEPTION{
		List<GENERALCODES> UniqueGeneralCodeList=null;
		try{	
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.GeneralCodes_QueryNameCheckUniqueGeneralCode);	
			ObjQuery.setString(GLOBALCONSTANT.INT_ZERO,StrTenant.toUpperCase().trim());
			ObjQuery.setString(GLOBALCONSTANT.IntOne,StrCompany.toUpperCase().trim());		
			ObjQuery.setString(GLOBALCONSTANT.INT_TWO,StrApplication.toUpperCase().trim());
			ObjQuery.setString(GLOBALCONSTANT.INT_THREE,StrGeneralCodeID.toUpperCase().trim());
			ObjQuery.setString(GLOBALCONSTANT.INT_FOUR,StrGeneralCode.toUpperCase().trim());
			UniqueGeneralCodeList = ObjQuery.list();
			return UniqueGeneralCodeList;
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}		
	}
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description20 in GeneralCodes TABLE before add record  
	 * @param StrMenuName
	 * @return List<MENUOPTIONSBE>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuNameUnique(String StrMenuName) throws JASCIEXCEPTION{
		List<MENUOPTIONS> UniqueMenuNameList=null;
		try{	
			Query Obj_MenuName = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuOptions_QueryNameCheckUniqueMenuNameCode);
			Obj_MenuName.setString(GLOBALCONSTANT.INT_ZERO, StrMenuName.toUpperCase().trim());
			UniqueMenuNameList = Obj_MenuName.list();
			return UniqueMenuNameList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}		
	}
}
