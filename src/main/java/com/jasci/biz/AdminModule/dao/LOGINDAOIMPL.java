
/**

Date Developed  Oct 14 2014
Description  Provide validity of user 
*/

package com.jasci.biz.AdminModule.dao;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.LOGINBE;
import com.jasci.biz.AdminModule.be.PASSWORDEXPIRYBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.TENANTS;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.exception.GenericJDBCException;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.CannotCreateTransactionException;

import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class LOGINDAOIMPL implements ILOGINDAO
{
	/**
	 *Description This function design to get user data from database
	 *Input parameter strUsername
	 *Return Type "List<SECURITYAUTHORIZATIONS>"
	 *Created By "Diksha Gupta"
	 *Created Date "oct 14 2014" 
	 */


	@Autowired
	private SessionFactory sessionFactory ;
	@Autowired
	LANGUANGELABELSAPI objLanguangelabelsapi;




	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> GetUserData(String StrUsername,String StrPassword) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION
	{

		/** query to get user data from database from table securityAuthorizations*/
		try
		{
			
			Query objqry = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.GETCOMMONSESSIONBE);

			objqry.setString(GLOBALCONSTANT.STRUSERNAME, StrUsername.toUpperCase());
			List<Object> ObjListSecurityAuthorizations = objqry.list();

			return ObjListSecurityAuthorizations;
		}

		catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}


		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		} 
	}



	/**
	 *Description This function design to get password expiration days from tenant table
	 *Input parameter StrTenant
	 *Return Type "List<TENANT>"
	 *Created By "Diksha Gupta"
	 *Created Date "05 nov 2014" 
	 */

	public List<SECURITYAUTHORIZATIONS> GetInvalidAttemptsNumber(String StrUsername) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION
	{

		/** query to get InvalidNumberAttempts and AttemptDate from database from table securityAuthorizations*/
		try
		{
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			List<SECURITYAUTHORIZATIONS> ObjListSecurityAuthorizations=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_SelectInvalidAttemptQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrUsername.toUpperCase()).list();
			return ObjListSecurityAuthorizations;
		}
		catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}


		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		} 
	}


	/**
	 *Description This function design to get password expiration days from tenant table
	 *Input parameter StrTenant
	 *Return Type "List<TENANT>"
	 *Created By "Diksha Gupta"
	 *Created Date "oct 27 2014" 
	 */


	public List<TENANTS> GetPasswordExpirationDays(String StrTenant) throws JASCIEXCEPTION
	{

		/** query to get PasswordExpirationDays from table tenant on basis of "Tenant"*/
		try
		{
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)

			List<TENANTS> ObjListTenants=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Tenants_Query).setParameter(GLOBALCONSTANT.INT_ZERO,StrTenant).list();
			return ObjListTenants;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		} 
	}



	/**
	 *Description This function design to get menu messages from menuMessages table
	 *Input parameter none
	 *Return Type "List<MENUMESSAGES>"
	 *Created By "Diksha Gupta"
	 *Created Date "oct 21 2014" 
	 */

	public List<MENUMESSAGES> GetMenuMessages() throws JASCIEXCEPTION, JASCIEXCEPTION
	{

		/** get property from config.txt property file*/
		CONFIGURATIONEFILE ObjConfigurationFile=new CONFIGURATIONEFILE();
		GLOBALCONSTANT.Global_Config_Login_Tenant= ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.Login_Config_Login_Tenant);
		String Status = ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.Login_Config_Status);



		try
		{
		
			/** query to update menu messages table*/
			Query query=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuMessages_ProcedureCallQuery).setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant,GLOBALCONSTANT.Global_Config_Login_Tenant).setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Status,Status);
			/** CommonSessionBean object will be passed in setParameter();*/

			query.executeUpdate();

			/** query to get messages from menuMessages table*/
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)

			List<MENUMESSAGES> ObjListMenuMessages=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuMessages_Query).setParameter(GLOBALCONSTANT.INT_ZERO,GLOBALCONSTANT.Global_Config_Login_Tenant).setParameter(GLOBALCONSTANT.IntOne,Status).list();
			/**CommonSessionBean object will be passed in setParameter();*/
			return ObjListMenuMessages;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		} 



	}


	/**
	 *Description This function design to get Last 5 Passwords from table SECURITYAUTHORIZATIONS table
	 *Input parameter strUserId
	 *Return Type "List<SECURITYAUTHORIZATIONS>"
	 *Created By "Diksha Gupta"
	 *Created Date "oct 28 2014" 
	 */

	public List<SECURITYAUTHORIZATIONS> GetLastPasswords(String strUserId) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION 
	{

		/** query to get Last 5 Passwords from table SECURITYAUTHORIZATIONS*/
		try
		{
			Query query=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_SelectInvalidAttemptQuery).setParameter(GLOBALCONSTANT.INT_ZERO,strUserId.toUpperCase());
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			List<SECURITYAUTHORIZATIONS> ObjListSecurityAuthorizations=query.list();
			return ObjListSecurityAuthorizations;
		}
		catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}

		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}



	}


	/**
	 *Description This function design to set new password in table SECURITYAUTHORIZATIONS table
	 *Input parameter strUserId, strPassword
	 *Return Type void
	 *Created By "Diksha Gupta"
	 *Created Date "oct 30 2014" 
	 * @param date 
	 */

	public void SetNewPassword(String strUserId, String strPassword, Date date) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION 
	{
		/**query to set new password in table SECURITYAUTHORIZATIONS table*/
		try{

			DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
			Date StrCurrentDate=ObjDateFormat1.parse(ObjDateFormat1.format(date));
			
			Query query=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SecurityAuthorizations_ProcedureCallQuery).setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_UserId,strUserId)
					.setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Password,strPassword).setParameter(GLOBALCONSTANT.Currentdate, StrCurrentDate);
			query.executeUpdate();
		}

		catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		} 


	}


	/**
	 *Description This function design to get languages from LANGUAGES table
	 *Input parameter none
	 *Return Type List<LANGUAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "oct 30 2014" 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> GetLanguage( String StrScreenName,String StrLanguage) throws JASCIEXCEPTION
	{

		try
		{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Languages_Query).setParameter(GLOBALCONSTANT.INT_ZERO,StrScreenName.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrLanguage.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		
	}


	/**
	 *Description This function design to set loginbe from LANGUAGES table
	 *Input parameter none
	 *Return Type LOGINBE
	 *Created By "Diksha Gupta"
	 *Created Date "nov 01 2014" 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strstatic_access)
	public LOGINBE SetLoginBe() throws JASCIEXCEPTION
	{

		List<LANGUAGES> ObjListLanguages=objLanguangelabelsapi.GetScreenLabels(objLanguangelabelsapi.StrLoginModule,GLOBALCONSTANT.Param_Languages_Language);
		LOGINBE ObjLoginBe=new LOGINBE();  
		/** to set login be data from languages data from table LANGUAGES*/
		for (LANGUAGES ObjLanguages : ObjListLanguages) 
		{
			String StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
			if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Login_to_your_account.trim()))
			{
				ObjLoginBe.setStrLoginToAccount(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_MessageBoard.trim()))
			{
				ObjLoginBe.setStrMessageBoard(ObjLanguages.getTranslation());	
			} 
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Enter_correct_UserId_and_Password.trim()))
			{
				ObjLoginBe.setStrErrorMessageInvalidUser(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_UserID.trim()))
			{
				ObjLoginBe.setStrUserId(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Password.trim()))
			{
				ObjLoginBe.setStrPassword(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Remember_me_.trim()))
			{
				ObjLoginBe.setStrRememberMe(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Login_.trim()))
			{
				ObjLoginBe.setStrLoginButton(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Forgot_your_password.trim()))
			{
				ObjLoginBe.setStrForgotPassword(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_no_worries_click.trim()))
			{
				ObjLoginBe.setStrNoWorriesClick(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_here.trim()))
			{
				ObjLoginBe.setStrHereLink(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_to_reset_your_password.trim()))
			{
				ObjLoginBe.setStrResetPassword(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Forgot_Password.trim()))
			{
				ObjLoginBe.setStrForgetPasswordOnNewPassword(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Enter_your_e_mail_address_below_to_reset_your_password.trim()))
			{
				ObjLoginBe.setStrEnterEmailMessage(ObjLanguages.getTranslation());	
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel.trim()))
			{
				ObjLoginBe.setStrBackButton(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Submit.trim()))
			{
				ObjLoginBe.setStrSubmitButton(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Please_enter_a_valid_UserId_and_Password.trim()))
			{
				ObjLoginBe.setStrErrorMessageInvalidUser(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.equalsIgnoreCase(LANGUANGELABELSAPI.KP_Invalid_Sign_on_Please_contact_Tenant_s_security_officer))
			{
				ObjLoginBe.setStrErrorMessageNumberAttempts(ObjLanguages.getTranslation());
			}else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_login_as_the_company_you_are_associated_with_is_inactive_Please_contact_administrator.trim()))
			   {
			    ObjLoginBe.setStrErrorMessageLoginNotAllowedCompanyDeleted(ObjLanguages.getTranslation());
			   }
		}
		return ObjLoginBe;

	}

	
	/**
	  * @description get User Data based on Active Company
	  * @author Rahul Kumar
	  * @Date Feb 09, 2015
	  * @param Tenant
	  * @param TeamMember
	  * @return
	  * @throws JASCIEXCEPTION
	  * @Return List<LANGUAGES>
	  *
	  */
	 @SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> GetUserDataActiveCompany(String Tenant, String TeamMember) throws JASCIEXCEPTION,
	   JASCIEXCEPTION, JASCIEXCEPTION {
	  List<Object> objResult=null;
	  try
	  {
	   objResult=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.Query_Authenticate_User_Active_Company).setParameter(GLOBALCONSTANT.Parameter_MenuOption_Tenant,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.StrTeamMember, TeamMember.trim().toUpperCase()).list();
	   return objResult;
	  }catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
	  catch(Exception objException)
	    
	  {
	   throw new JASCIEXCEPTION(objException.getMessage());
	  }
	  
	 }


	/**
	 *Description This function design to set PasswordExpirybe from LANGUAGES table
	 *Input parameter none
	 *Return Type PasswordExpiryBE
	 *Created By "Diksha Gupta"
	 *Created Date "nov 03 2014" 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strstatic_access)
	public PASSWORDEXPIRYBE SetPasswordExpiryBe() throws JASCIEXCEPTION
	{

		List<LANGUAGES> ObjListLanguages=objLanguangelabelsapi.GetScreenLabels(objLanguangelabelsapi.StrPasswordExpiryModule,GLOBALCONSTANT.Param_Languages_Language);
		PASSWORDEXPIRYBE ObjPasswordBe=new PASSWORDEXPIRYBE();  
		/** to set login be data from languages data from table LANGUAGES*/
		for (LANGUAGES ObjLanguages : ObjListLanguages) 
		{
			String StrObjectFieldCode=ObjLanguages.getId().getKeyPhrase().trim();
			if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Create_New_Password.trim()))
			{
				ObjPasswordBe.setStrCreateNewPassword(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_New_Password.trim()))
			{
				ObjPasswordBe.setStrNewPassword(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Submit.trim()))
			{
				ObjPasswordBe.setStrSubmitButton(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Cancel.trim()))
			{
				ObjPasswordBe.setStrCancelButton(ObjLanguages.getTranslation());	
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Confirm_Password.trim()))
			{
				ObjPasswordBe.setStrConfirmPassword(ObjLanguages.getTranslation());	
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_Password_has_been_expired_Please_create_new_password.trim()))
			{
				ObjPasswordBe.setStrPasswordExpiredMessage(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_New_password_and_confirm_password_do_not_match.trim()))
			{
				ObjPasswordBe.setStrPasswordNotMatchedMessage(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Password_must_be_of_atleast_8_characters_including_one_capital_letter_and_three_numbers.trim()))
			{
				ObjPasswordBe.setStrPasswordPatternMessage(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Password_cant_be_reused_in_the_past_5_times.trim()))
			{
				ObjPasswordBe.setStrPasswordUsedMessage(ObjLanguages.getTranslation());
			}
			else if(StrObjectFieldCode.trim().equalsIgnoreCase(LANGUANGELABELSAPI.KP_Your_Password_is_temporary_Please_create_new_password.trim()))
			{
				ObjPasswordBe.setStrTemporaryPasswordMessage(ObjLanguages.getTranslation());
			}
		}

		return ObjPasswordBe;
	}


	/**
	 *Description This function design to set invalid attempt number in database Security authorization table.
	 *Input parameter IntInvalidAttemptNumber
	 *Return Type void
	 *Created By "Diksha Gupta"
	 *Created Date "nov 04 2014" 
	 */


	public void SetInvalidAttempt( long LongInvalidAttempts,String StrUserId) throws JASCIEXCEPTION
	{

		try{
			sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_InvalidAttempt_Query).setParameter(GLOBALCONSTANT.INT_ZERO,LongInvalidAttempts).setParameter(GLOBALCONSTANT.IntOne, StrUserId.toUpperCase()).executeUpdate();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		
	}



	/**
	 *Description This function design to set invalid attempt number in database Security authorization table.
	 *Input parameter IntInvalidAttemptNumber
	 *Return Type void
	 *Created By "Diksha Gupta"
	 *Created Date "nov 04 2014" 
	 */

	public void SetNumberAttemptstoZero(int IntNumberAttempts, String strUserId, Date strCurrentDate) throws JASCIEXCEPTION 
	{

		try
		{
			sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_SetNumberAttempt_ToZero_Query).setParameter(GLOBALCONSTANT.INT_ZERO,IntNumberAttempts).setParameter(GLOBALCONSTANT.IntOne,strCurrentDate).setParameter(GLOBALCONSTANT.INT_TWO,strUserId.toUpperCase()).executeUpdate();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}


	}


	/**
	 *Description This function design to get list of teammembers having the entered EmailId in TEAMMEMBERS table.
	 *Input parameter StrEmailId
	 *Return Type List<TeamMembers>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 07 2014" 
	 */



	@SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strfinally })
	public List<TEAMMEMBERS> CheckEmailId(String StrEmailId) throws JASCIEXCEPTION
	{

		/** to check if user have entered a valid emailId*/
		List<TEAMMEMBERS> ObjListTeamMember=null;
		try
		{

			ObjListTeamMember = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_EmailId_Query).setParameter(GLOBALCONSTANT.INT_ZERO,StrEmailId.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}
		finally{
			return ObjListTeamMember;
		}
		
	}


	/**
	 *Description This function design to setTemporary password in database Security authorization table.
	 *Input parameter  strUserId,strPassword
	 *Return Type void
	 *Created By "Diksha Gupta"
	 *Created Date "nov 04 2014" 
	 * @param lastActivityDateStringH 
	 */

	public boolean SetTemporaryPassword(String strPassword,String strTenant,String strTeammember, Date lastActivityDateStringH) throws JASCIEXCEPTION {

		try
		{

			DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
			Date StrCurrentDate=ObjDateFormat1.parse(ObjDateFormat1.format(lastActivityDateStringH));
			/** query to set temporary password in database in SecurityAuthorizations*/
			Query query =sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_SetTemporaryPassword_Query).setParameter(GLOBALCONSTANT.INT_ZERO,strPassword).setParameter(GLOBALCONSTANT.IntOne,GLOBALCONSTANT.Security_UserTemporaryStatus).setParameter(GLOBALCONSTANT.INT_TWO, StrCurrentDate).setParameter(GLOBALCONSTANT.INT_THREE, GLOBALCONSTANT.INT_ZERO).setParameter(GLOBALCONSTANT.INT_FOUR, strTenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_FIVE,strTeammember.toUpperCase() );
			int status = query.executeUpdate();
			if(status>0){
				return true;
			}
			else {
				return false;
			}
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}


	}

	/**
	 *Description This function design to setTemporary password in database Security authorization table.
	 *Input parameter  strUserId,strPassword
	 *Return Type void
	 *Created By "Diksha Gupta"
	 *Created Date "nov 04 2014" 
	 */

	
	@SuppressWarnings({ GLOBALCONSTANT.Strunused, GLOBALCONSTANT.Strunchecked })
	public List<SECURITYAUTHORIZATIONS> getInvalidAttempts(String strTenant,String strTeammember) throws JASCIEXCEPTION {

		try
		{

			Date ObjDate = new Date();
			DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
			Date StrCurrentDate=ObjDateFormat1.parse(ObjDateFormat1.format(ObjDate));
			/** query to set temporary password in database in SecurityAuthorizations*/
			List<SECURITYAUTHORIZATIONS> Obj_InvalidAttempts_list=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_getInvalidAttempts_Query).setParameter(GLOBALCONSTANT.INT_ZERO, strTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,strTeammember.toUpperCase() ).list();

			return Obj_InvalidAttempts_list;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.BlankString);
		}


	}





}



