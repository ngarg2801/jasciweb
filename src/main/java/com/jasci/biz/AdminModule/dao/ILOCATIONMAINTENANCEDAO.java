/*
Description it is an Interface where we can declare all of the methods that are implement in LOCATIONMAINTENANCEDAOIMPL
Created By Aakash Bishnoi  
Created Date Dec 26 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.INVENTORY_LEVELS;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILOCATIONMAINTENANCEDAO {

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL is used to Add Record in Location Table From LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	 */
	public String addEntry(LOCATION ObjectLocation) throws JASCIEXCEPTION;

	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 6, 2015
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL is used to Update Record in Location Table From LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	 */
	public String updateEntry(LOCATION ObjectLocation) throws JASCIEXCEPTION;

	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL to get the list of dropdown from GeneralCode where GENERAL_CODE_ID will be change 
	 *@param StrTenant
	 *@param StrCompany
	 *@param GeneralCodeID
	 *@return  List<GENERALCODES>	  
	 */
	public List<GENERALCODES> getGeneralCode(String StrTenant,String StrCompany,String GeneralCodeID) throws JASCIEXCEPTION;

	/**	 
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL to get the list of dropdown from LOCATION_PROFILES
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFullfilmentCenter
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATIONPROFILES> getLocationProfile(String StrTenant,String StrCompany,String StrFullfilmentCenter) throws JASCIEXCEPTION;

	/**
	 * 
	 * @Description get team member name form team members table based on team member id
	 * @param Tenant
	 * @param Company
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public List<TEAMMEMBERS> getTeamMemberName(String Tenant,String TeamMember)throws JASCIEXCEPTION;

	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 1, 2015
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL for Check uniqueness of Location in LOCATION TABLE.
	 *@param Tenant
	 *@param Company
	 *@param FulfillmentCenter
	 *@param Area
	 *@param Location
	 *@return List<LOCATION>
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATION> getLocationUnique(String Tenant,String Company,String FulfillmentCenter,String Area,String Location) throws JASCIEXCEPTION;


	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 1, 2015
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL for Check uniqueness of Alternate_Location in LOCATION TABLE.
	 *@param Tenant
	 *@param Company
	 *@param FulfillmentCenter
	 *@param AlternateLocation
	 *@return List<LOCATION>
	 *@throws JASCIEXCEPTION
	 */
	//public List<LOCATION> getAlternateLocationUnique(String Tenant,String Company,String FulfillmentCenter,String Area,String Location,String AlternateLocation) throws JASCIEXCEPTION;
	public List<LOCATION> getAlternateLocationUnique(String Tenant,String Company,String FulfillmentCenter,String AlternateLocation) throws JASCIEXCEPTION;

	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 3, 2015
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL for search the list from LOCATION on Location Search Screen.
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrDescription
	 *@param StrLocationType
	 *@return
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List getList(String StrLocation,String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrArea,String StrDescription,String StrLocationType);

	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL for fetch data from Location table on the behalf of parameters(Primary Keys)
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@return LOCATIONBEAN
	 */
	public LOCATION getFetchLocation(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrArea,String StrLocation);
	/**
	 * 
	  * @author Aakash Bishnoi
	  * @Date Jan 6, 2015
	 * Description:This is declaration of Function which is implement in LOCATIONMAINTENANCEDAOIMPL for fetch data from INVENTORY_LEVELS table on the behalf of parameters
	  *@param StrTenant
	  *@param StrCompany
	  *@param StrFulfillmentCenter
	  *@param StrArea
	  *@param StrLocation
	  *@return
	 */
	public INVENTORY_LEVELS getLocationExistForDelete(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrArea,String StrLocation);
	
	/**
	 * Created on:Jan 05 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute query to get FulfillmentCenter.
	 * Input parameter: String
	 * Return Type :List<Object>
	 * 
	 */
	public List<Object> getfulFillmentCenter(String tenant,String company,String PageName)throws JASCIEXCEPTION;
	
	 /**
	  * @author Aakash Bishnoi
	  * @Date Jan 14, 2015
	  * Description this is used to delete the selected From Fulfillmentcenter table
	  * @param model
	  * @return Boolean
	  * @throws JASCIEXCEPTION
	  */
	 public Boolean deleteEntry(String StrTenant_ID,String StrCompany_ID,String StrFetchFulfillment_Center,String StrFetchArea,String StrFetchLocation) throws JASCIEXCEPTION;

	 /**
	  * @author Aakash Bishnoi
	  * @Date Jan 16, 2015
	  * Description:This is used to get the record of notes
	  * @param tenant
	  * @param company
	  * @param profile
	  * @param locationprofileNotesid
	  * @return
	  * @throws JASCIEXCEPTION
	  */
	 public List<NOTES> GetNotes(String tenant, String company, String location, String locationNotesid) throws JASCIEXCEPTION;

}
