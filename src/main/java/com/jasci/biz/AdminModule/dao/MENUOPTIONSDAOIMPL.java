/**

Date Developed: Dec 15 2014
Description: for implement the MenuOptionDao in which we implement the hibernet functions
Created By: Sarvendra Tyagi  
Created Date Dec 15 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEOPTIONS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Repository
public class MENUOPTIONSDAOIMPL implements IMENUOPTIONSDAO {

	@Autowired
	private SessionFactory sessionFactory ;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;

	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Option List
	 * Input parameter: SearchFieldValue and SearchField
	 * Return Type :List<MENUOPTIONS>
	 * @throws DATABASEEXCEPTION And Jasci Exception
	 * 
	 */	

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionList(String strApplication, String strMenuOption,
			String strMenuType, String partOfdesc20, String partOfDesc50,String strTenant)
					throws JASCIEXCEPTION {
        List<MENUOPTIONS> ObjListMenuOption=null;

		try{

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
			//Criterion menuOption=Restrictions.like(GLOBALCONSTANT.MENUOPTION_MenuOption, GLOBALCONSTANT.StrPercentSign+strMenuOption+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion menuApplication=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Application, GLOBALCONSTANT.StrPercentSign+strApplication+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			//Criterion menuType=Restrictions.like(GLOBALCONSTANT.MENUOPTION_MenuType, GLOBALCONSTANT.StrPercentSign+strMenuType+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			//Criterion description20=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description20, GLOBALCONSTANT.StrPercentSign+partOfdesc20+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			//Criterion description50=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description50, GLOBALCONSTANT.StrPercentSign+partOfDesc50+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion tenant=null;
			//Criterion tenantjasci = null;
			if(OBJCOMMONSESSIONBE.getJasci_Tenant().equalsIgnoreCase(OBJCOMMONSESSIONBE.getTenant())){
			//if(GLOBALCONSTANT.MENUOPTION_JASCI.equalsIgnoreCase(OBJCOMMONSESSIONBE.getTenant())){
			tenant=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Tenant, GLOBALCONSTANT.StrPercentSign+strTenant+GLOBALCONSTANT.StrPercentSign).ignoreCase(); 
			}else{
				tenant=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Tenant, GLOBALCONSTANT.StrPercentSign+OBJCOMMONSESSIONBE.getTenant()+GLOBALCONSTANT.StrPercentSign).ignoreCase();
				//tenantjasci=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Tenant, GLOBALCONSTANT.StrPercentSign+strTenant+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			}
			//LogicalExpression andExp1 = Restrictions.and(menuOption,menuApplication);
			LogicalExpression andExp2 = Restrictions.and(menuApplication, tenant);
			//LogicalExpression andExp5 = Restrictions.and(andExp2, tenant);
			//LogicalExpression andExp4 = Restrictions.or(description20, description50);
			//LogicalExpression andExp3 = Restrictions.and(andExp5,andExp4);
			criteria.add(andExp2);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			ObjListMenuOption=criteria.list();




		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}

		return ObjListMenuOption;
	}


	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionListByApplication(String strApplication) throws JASCIEXCEPTION {
		List<MENUOPTIONS> ObjListMenuOption=null;

		try{
			if(OBJCOMMONSESSIONBE.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSIONBE.getJasci_Tenant()))
			{
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
			Criterion menuApplication=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Application, GLOBALCONSTANT.StrPercentSign+strApplication+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			criteria.add(menuApplication);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			ObjListMenuOption=criteria.list();
			}
			else
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
				Criterion menuApplication=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Application, GLOBALCONSTANT.StrPercentSign+strApplication+GLOBALCONSTANT.StrPercentSign).ignoreCase();
				Criterion tenant=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSIONBE.getTenant()).ignoreCase();
				//Criterion tenantjasci=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, GLOBALCONSTANT.MENUOPTION_JASCI).ignoreCase();
				Criterion tenantjasci=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSIONBE.getJasci_Tenant()).ignoreCase();
				LogicalExpression andExp1 = Restrictions.or(tenant,tenantjasci);
				LogicalExpression andExp2 = Restrictions.and(menuApplication,andExp1);
				criteria.add(andExp2);
				criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
				ObjListMenuOption=criteria.list();
			}
			
		}catch(HibernateException objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return ObjListMenuOption;
	}




	/**
	 * Created on:Dec 17 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function is checking menu Option assigned  in menu profile option or not
	 * Input parameter: String MenuOption
	 * Return Type :List<Object>
	 * 
	 */


	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUPROFILEOPTIONS> checkAssignedOption(String strMenuOption) throws  JASCIEXCEPTION {

		List<MENUPROFILEOPTIONS> objList=null;
		try{


			objList= sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_CheckingMenuProfileOptions)
					.setParameter(GLOBALCONSTANT.INT_ZERO, strMenuOption.toUpperCase()).list();

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		return objList;
	}



	/**
	 * @deprecated:This function is used for delete the menu option from menu option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 18 2014
	 */
	
	public Boolean deleteMenuOption(String strMenuOption) throws JASCIEXCEPTION {

		Boolean status=false;
		try{
			MENUOPTIONS objMenuoptions=(MENUOPTIONS) sessionFactory.getCurrentSession().get(MENUOPTIONS.class,strMenuOption);
			sessionFactory.getCurrentSession().delete(objMenuoptions);

			status=true;

		}
		catch(Exception objJasciexception){
		}

		return status;
	}


	
	
	
	
	/** 
	 * @Description:This function is getting data from menuoption table for edit or update
	 * @param      : strMenuOption
	 * @return     :List<MENUOPTIONS>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 18 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionData(String strMenuOption) throws JASCIEXCEPTION {

		List<MENUOPTIONS> objMenuoptions=null;
		
		try{
			if(OBJCOMMONSESSIONBE.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSIONBE.getJasci_Tenant()))
			{
			objMenuoptions=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuOptions_MenuOptionQueryName_CheckOne)
					.setParameter(GLOBALCONSTANT.DataBase_MenuOptions_MenuOption, strMenuOption.toUpperCase()).list();
			}
			else
			{
				objMenuoptions=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuOptions_MenuOptionQueryName_CheckTwo)
						.setParameter(GLOBALCONSTANT.INT_ZERO, strMenuOption.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
						.setParameter(GLOBALCONSTANT.INT_TWO, OBJCOMMONSESSIONBE.getJasci_Tenant()).list();
						//.setParameter(GLOBALCONSTANT.INT_TWO, GLOBALCONSTANT.MENUOPTION_JASCI.toUpperCase()).list();
				
			}

			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objJasciexception){

			throw new JASCIEXCEPTION(objJasciexception.getMessage());

		}

		return objMenuoptions;
	}


	/**  
	 * @Description :This function getting application and Sub Application from general code table
	 * @param       :strApplication
	 * @return	    :List<Object>
	 * @throws      :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 19 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getApplicationAndSubApplication(String strApplication) throws JASCIEXCEPTION {


		List<GENERALCODES> objApplicationList=null;

		try{
			objApplicationList=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuOptions_GeneralCode_ApplicationQueryName)
					.setParameter(GLOBALCONSTANT.InputParaMeter_MenuOption_valApplication, strApplication.toUpperCase()).list();


		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objApplicationList;
	}


	/** 
	 * @Description :This function is getting menu types from general code list
	 * @param 		:strMenuType
	 * @return      :List<Object>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby :Sarvendra tyagi
	 * @Date	    :Dec 19 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getMenuTypes(String strMenuType) throws JASCIEXCEPTION {

		List<GENERALCODES> objMenuTypesList=null;

		try{


			objMenuTypesList=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuOptions_GeneralCode_MenuTypeQueryName)
					.setParameter(GLOBALCONSTANT.InputMenuTypeName_MenuOptions_GeneralCode_MenuTypeQueryName, strMenuType.toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfile_Tenant, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuOption_Company, OBJCOMMONSESSIONBE.getCompany().toUpperCase()).list();

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuTypesList;
	}

	/** 
	 * @Description :This function is used for udate record 
	 * @param 		:strMenuType
	 * @return      :MENUOPTIONS
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 19 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public MENUOPTIONS update(MENUOPTIONS MenuoptionsObj) throws JASCIEXCEPTION {
		try{
			Session session = sessionFactory.getCurrentSession();
			
			
			List<MENUOPTIONS> OBJ_Menuoptions = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuOption_SelectQuery).setParameter(GLOBALCONSTANT.INT_ZERO, MenuoptionsObj.getMenuOption()).list();
			if(OBJ_Menuoptions.isEmpty())
			{
				MenuoptionsObj.setTenant(OBJCOMMONSESSIONBE.getTenant());	
				MenuoptionsObj.setApplication(MenuoptionsObj.getApplication().trim());
				MenuoptionsObj.setApplicationSub(MenuoptionsObj.getApplicationSub().trim());
				MenuoptionsObj.setDescription20(MenuoptionsObj.getDescription20().trim());
				MenuoptionsObj.setDescription50(MenuoptionsObj.getDescription50().trim());
				MenuoptionsObj.setExecution(MenuoptionsObj.getExecution().trim());
				MenuoptionsObj.setHelpline(MenuoptionsObj.getHelpline().trim());
				MenuoptionsObj.setLastActivityDate(MenuoptionsObj.getLastActivityDate());
				MenuoptionsObj.setLastActivityTeamMember(MenuoptionsObj.getLastActivityTeamMember().trim());
				MenuoptionsObj.setMenuOption(MenuoptionsObj.getMenuOption().trim());
				MenuoptionsObj.setMenuType(MenuoptionsObj.getMenuType().trim());
				session.save(MenuoptionsObj);
				
			}
			else
			{
			

				try{
			     
				Query objQuery= session.createSQLQuery(GLOBALCONSTANT.MenuOption_UpdateQuery);
				objQuery.setParameter(GLOBALCONSTANT.INT_ZERO, MenuoptionsObj.getApplication().trim());
				objQuery.setParameter(GLOBALCONSTANT.IntOne, MenuoptionsObj.getApplicationSub().trim());
				objQuery.setParameter(GLOBALCONSTANT.INT_TWO, MenuoptionsObj.getMenuType().trim());
				objQuery.setParameter(GLOBALCONSTANT.INT_THREE, MenuoptionsObj.getDescription20().trim());
				objQuery.setParameter(GLOBALCONSTANT.INT_FOUR, MenuoptionsObj.getDescription50().trim());
				objQuery.setParameter(GLOBALCONSTANT.INT_FIVE, MenuoptionsObj.getHelpline().trim());
				objQuery.setParameter(GLOBALCONSTANT.INT_SIX, MenuoptionsObj.getExecution().trim());
				objQuery.setParameter(GLOBALCONSTANT.INT_SEVEN, MenuoptionsObj.getLastActivityDate());
				objQuery.setParameter(GLOBALCONSTANT.INT_EIGHT, MenuoptionsObj.getLastActivityTeamMember().trim());
				objQuery.setParameter(GLOBALCONSTANT.INT_NINE, MenuoptionsObj.getMenuOption().toUpperCase().trim());
				
				objQuery.executeUpdate();
				
				}catch(SQLGrammarException objException){

					throw new JASCIEXCEPTION(objException.getMessage());

				}
				catch(HibernateException objHibernateException)
				{
					throw new JASCIEXCEPTION(objHibernateException.getMessage());
					
				}
				
			}
			

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return MenuoptionsObj;
	}



	/** 
	 * @Description:This function is getting all data from menuoption table 
	 * @param      : strMenuOption
	 * @return     :List<MENUOPTIONS>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 20 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionDisplayAll() throws JASCIEXCEPTION {
		List<MENUOPTIONS> objMenuoptions=null;


		try{

			//if(OBJCOMMONSESSIONBE.getTenant().toString().equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_JASCI))
			if(OBJCOMMONSESSIONBE.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSIONBE.getJasci_Tenant()))
			{
			objMenuoptions=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuOptions_DisplayAllQueryNameOne).list();
			}else
			{
			objMenuoptions=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuOptions_DisplayAllQueryNameTwo).setParameter(GLOBALCONSTANT.INT_ZERO, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, OBJCOMMONSESSIONBE.getJasci_Tenant().toUpperCase()).list();	
			}

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

		}
		return objMenuoptions;
	}



	/** 
	 * @Description: This function is used for getting label of menuoption all screen
	 * @param  :language
	 * @return :List<LANGUAGES>
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Sarvendra Tyagi
	 * @Date :dec 23 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> getLanguageLabel(String language) throws JASCIEXCEPTION {

		try{

			return sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuOptions_LabelQueryName).setParameter(GLOBALCONSTANT.INPUT_PARAMETER_LANGUAGE, language.toUpperCase()).list();

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}
	}

	/**
	 * @deprecated:This function is used for delete the menu option from menu Profile option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Jan 2  2014
	 */
	public Boolean deleteMenuOptionFromMenuProfileOption(String strMenuOption)throws JASCIEXCEPTION{
		Boolean Status=false;

		try{

			Query OBJ_Query = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileOption_deleteQuery);
			OBJ_Query.setParameter(GLOBALCONSTANT.INT_ZERO, strMenuOption.toUpperCase());
			OBJ_Query.executeUpdate();
			
			Status=true;
		
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objJasciexception){

				throw new JASCIEXCEPTION(objJasciexception.getMessage());
		}
		return Status;
	}
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionListByMenuType(String strMenuType) throws JASCIEXCEPTION {
		List<MENUOPTIONS> ObjListMenuOption=null;

		try{
			//if(OBJCOMMONSESSIONBE.getTenant().toString().equalsIgnoreCase(GLOBALCONSTANT.MENUOPTION_JASCI))
			if(OBJCOMMONSESSIONBE.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSIONBE.getJasci_Tenant()))
			{
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
			Criterion menutype=Restrictions.like(GLOBALCONSTANT.MENUOPTION_MenuType, GLOBALCONSTANT.StrPercentSign+strMenuType+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			criteria.add(menutype);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			ObjListMenuOption=criteria.list();
			}else
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
				Criterion menutype=Restrictions.like(GLOBALCONSTANT.MENUOPTION_MenuType, GLOBALCONSTANT.StrPercentSign+strMenuType+GLOBALCONSTANT.StrPercentSign).ignoreCase();
				Criterion tenant=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSIONBE.getTenant()).ignoreCase();
				Criterion tenantjasci=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSIONBE.getJasci_Tenant()).ignoreCase();
				LogicalExpression andExp1 = Restrictions.or(tenant,tenantjasci);
				LogicalExpression andExp2 = Restrictions.and(menutype,andExp1);
				criteria.add(andExp2);
				criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
				ObjListMenuOption=criteria.list();
			}
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return ObjListMenuOption;
	}

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionListByTenant(String strTenant) throws JASCIEXCEPTION {
		
		List<MENUOPTIONS> ObjListMenuOption=null;

		try{

			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
			Criterion menuApplication=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Tenant, GLOBALCONSTANT.StrPercentSign+strTenant+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			criteria.add(menuApplication);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			ObjListMenuOption=criteria.list();
		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return ObjListMenuOption;
	}







	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionListByDescription(String partOfdesc20) throws JASCIEXCEPTION {
		
		List<MENUOPTIONS> ObjListMenuOption=null;

		try{

			if(OBJCOMMONSESSIONBE.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSIONBE.getJasci_Tenant()))
			{
			Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
			Criterion description20=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description20, GLOBALCONSTANT.StrPercentSign+partOfdesc20+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion description50=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description50, GLOBALCONSTANT.StrPercentSign+partOfdesc20+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			
			LogicalExpression andExp1 = Restrictions.or(description20,description50);
			criteria.add(andExp1);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			ObjListMenuOption=criteria.list();
			}else
			{
				Criteria criteria = sessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
				Criterion description20=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description20, GLOBALCONSTANT.StrPercentSign+partOfdesc20+GLOBALCONSTANT.StrPercentSign).ignoreCase();
				Criterion description50=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description50, GLOBALCONSTANT.StrPercentSign+partOfdesc20+GLOBALCONSTANT.StrPercentSign).ignoreCase();

				Criterion tenant=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSIONBE.getTenant()).ignoreCase();
				Criterion tenantjasci=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSIONBE.getJasci_Tenant()).ignoreCase();		
				
				LogicalExpression andExp1 = Restrictions.or(tenant,tenantjasci);
				LogicalExpression andExp2 = Restrictions.or(description20,description50);
				LogicalExpression andExp3 = Restrictions.and(andExp1,andExp2);
				criteria.add(andExp3);
				criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
				ObjListMenuOption=criteria.list();	
			}
		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return ObjListMenuOption;
	}



	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> checkMenuOptionFromOptionTable(String strMenuOption) throws JASCIEXCEPTION {
		
		try{
		return sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuOption_CheckMenuOption_Query).setParameter(GLOBALCONSTANT.INT_ZERO,strMenuOption.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
	}


}
