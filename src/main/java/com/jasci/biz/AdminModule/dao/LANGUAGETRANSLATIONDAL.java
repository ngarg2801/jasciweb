/**

Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :LANGUAGETRANSLATIONDAL to execute query and get data from database.
 */
package com.jasci.biz.AdminModule.dao;


import java.text.ParseException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;


@Repository
public class LANGUAGETRANSLATIONDAL implements ILANGUAGETRANSLATIONDAL
{

	@Autowired
	private SessionFactory sessionFactory ;

	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function  get General code list based on tenant,company,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getLanguageList(String Tenant, String Company, String GeneralCodeId)
			throws  JASCIEXCEPTION {
		

		List<GENERALCODES> objGeneralcodes=null;
		try{
			objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LanguageTranslation_Generalcode_Select_Conditional_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}

		return objGeneralcodes;


	}


	

	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to check if the entered language exists in languages table or not.
	 * Input parameter: String
	 * Return Type :List<LANGUAGES>
	 * 
	 */

	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> CheckLanguage(String language) throws  JASCIEXCEPTION 
	{
	
	
		List<LANGUAGES> objLanguagesList=null;

		try{
			objLanguagesList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LanguageTranslation_CheckLanguage_Query).setParameter(GLOBALCONSTANT.INT_ZERO, language.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}

		return objLanguagesList;

	}
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to check if the entered language exists in languages table or not.
	 * Input parameter: String
	 * Return Type :List<LANGUAGES>
	 * 
	 */

	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> CheckKeyPhrase(String KeyPhrase) throws  JASCIEXCEPTION 
	{
	
		
		List<LANGUAGES> objLanguagesList=null;

		try{
			objLanguagesList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LanguageTranslation_CheckKeyPhrase_Query).setParameter(GLOBALCONSTANT.INT_ZERO, KeyPhrase.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}

		return objLanguagesList;

	}

	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to check if the entered language exists in languages table or not.
	 * Input parameter: String
	 * Return Type :List<LANGUAGES>
	 * 
	 */
	

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> CheckAlreadyExist(String language, String keyPhrase) throws JASCIEXCEPTION
	{
		
		List<LANGUAGES> objLanguagesList=null;

		try{
			objLanguagesList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LanguageTranslation_GetLanguageData_Query).setParameter(GLOBALCONSTANT.INT_ZERO, language.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, keyPhrase.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}

		return objLanguagesList;
		}

	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to add a record to languages table .
	 * Input parameter: LANGUAGES
	 * Return Type :Boolean
	 * @throws ParseException 
	 * 
	 */


	public Boolean addLanguageData(LANGUAGES ObjLanguage) throws JASCIEXCEPTION
	{
		boolean Status=false;
		  Session session = sessionFactory.openSession();
	  Transaction TransactionObject = session.beginTransaction();
		  
 		  try{  
			  
		   session.save(ObjLanguage);
		   }
 		  catch(Exception e){}
 		  try{
 			 TransactionObject.commit();
		     Status=true;
		  }catch(Exception e){
		   Status=false;
		   throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		   }
		   session.close();
		  return Status;
	}



	/**
	 * Created on:Dec 19 2014
	 * Created by:Diksha Gupta
	 * Input parameter: LANGUAGES
	 * Return Type :Boolean
	 * Description:To delete a record from languages table
	 */

	

	
	public Boolean DeleteLanguage(LANGUAGES objLanguages) throws JASCIEXCEPTION
	{
		Boolean Status=false;
		 Session session = sessionFactory.openSession();
		  Transaction TransactionObject = session.beginTransaction();
		  try{
			  session.delete(objLanguages);
		      TransactionObject.commit();
		      Status=true;
		  }catch(Exception e)
		  {
			  Status=false;
			  throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);

		  }
		  
		return Status;
	}

	
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to get language data from languages table.
	 * Input parameter: String
	 * Return Type :List<LANGUAGES>
	 * 
	 */

	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> getLanguage(String strLanguage, String strKeyPhrase)  throws JASCIEXCEPTION
	{
		List<LANGUAGES> objLanguagesList=null;
		try{
			objLanguagesList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LanguageTranslation_GetLanguageData_Query).setParameter(GLOBALCONSTANT.INT_ZERO, strLanguage.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, strKeyPhrase.toUpperCase()).list();
		}catch(Exception objException){
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
		return objLanguagesList;
	}
	
	
	/**
	 *Description This function design to get translation according to screen and language from LANGUAGES table
	 *Input parameter StrLanguage
	 *Return Type List<LANGUAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "Dec 18 2014" 
	 * */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> GetLanguage(String StrLanguage) throws JASCIEXCEPTION
	{
		try
		{
			List<LANGUAGES> objLanguages=null;
			objLanguages= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LanguageTranslation_ScreenLabelsQuery).setParameter(GLOBALCONSTANT.INT_ZERO, GLOBALCONSTANT.LanguageTranslation_ScreenName).setParameter(GLOBALCONSTANT.IntOne, StrLanguage.toUpperCase()).list();
			return objLanguages;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
	}
	/**
	 *Description This function design to edit language record in LANGUAGES table
	 *Input parameter LANGUAGES
	 *Return Type Boolean
	 *Created By "Diksha Gupta"
	 *Created Date "Dec 18 2014" 
	 * */
	public Boolean editLanguageData(LANGUAGES objLanguage) throws JASCIEXCEPTION
	{
		boolean Status=false;
		  Session session = sessionFactory.openSession();
	     Transaction TransactionObject = session.beginTransaction();
		  try{  
			  
		   session.update(objLanguage);
		   }
		  catch(Exception e){}
		  try{
			 TransactionObject.commit();
		     Status=true;
		  }catch(Exception e){
		   Status=false;
		   throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		   }
		   session.close();
		  return Status;
	}


	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
		return objTeammembers;
	}
}
