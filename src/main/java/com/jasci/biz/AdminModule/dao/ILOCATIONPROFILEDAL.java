/**

Date Developed :Dec 26 2014
Created by: Diksha Gupta
Description :ILOCATIONPROFILEDAL interface to get data from database.
*/



package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.be.LOCATIONPROFILEBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILOCATIONPROFILEDAL 
{
	/**
	 * Created on:Dec 28 2014
	 * Created by:Diksha Gupta
	 * Description: This function is get General code list based on tenant,company,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION;

	/**
	 * Created on:JAN 29 2015
	 * Created by:Diksha Gupta
	 * Description: This function is get FulFillmentCenter list based on tenant
	 * Input parameter: String
	 * Return Type :List<FULFILLMENTCENTERSPOJO>
	 * 
	 */
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentCenter(String Tenant)throws  JASCIEXCEPTION;
	/**
	 * Created on:Dec 30 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to check whether the locationProfile already exists at the time of adding a new record.
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	public List<LOCATIONPROFILES> CheckAlreadyExists(String locationProfile, String profileGroup,
			String Tenant, String Company, String FulfillmentCenter)throws  JASCIEXCEPTION;

	/**
	 * Created on:Dec 30 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to excute the query to save the data.
	 * Input parameter: LOCATIONPROFILES
	 * Return Type :Boolean
	 * 
	 */
	public Boolean addLocationProfile(LOCATIONPROFILES objLocationprofiles)throws  JASCIEXCEPTION;

	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to excute the query to check whether the given LocationProfile exists .
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILES
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List CheckLocationProfile(String locationProfile, String Tenant, String Company, String FulfillmentCenter)throws  JASCIEXCEPTION;

	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to excute the query to check whether the given ProfileGroup exists .
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILES
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List CheckProfileGroup(String profileGroup, String Tenant, String Company, String FulfillmentCenter)throws  JASCIEXCEPTION;

	
	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to excute the query to check whether the given description for LocationProfile exists .
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILES
	 * @param FulfillmentCenter 
	 * @param Company 
	 * @param Tenant 
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List CheckDescription(String description, String Tenant, String Company, String FulfillmentCenter)throws  JASCIEXCEPTION;

	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to excute the query to delete the record for given LocationProfile and ProfileGroup.
	 * Input parameter: LOCATIONPROFILES
	 * Return Type :Boolean
	 * 
	 */
	public Boolean DeleteLocationProfile(LOCATIONPROFILES objLocationprofiles)throws  JASCIEXCEPTION;
	
	/**
	 * Created on:Dec 30 2014
	 * Created by:Diksha Gupta
	 * Description: This function is edit a record for LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */
	
	public Boolean editLocationProfile(LOCATIONPROFILES objLocationProfiles)
			throws JASCIEXCEPTION ;

	/**
	 * Created on:Jan 2 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to excute the query to get the records for given LocationProfile or ProfileGroup or Description.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */
	public List<Object> GetLocationProfileList(
			 String tenant, String company,
			String fulfillmentCenter) throws JASCIEXCEPTION;
	
	/**
	 * Created on:Jan 2 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to excute the query to get the records for Reassign Profile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */
	public List<LOCATIONPROFILES> GetLocationProfileListReAssign(
			String tenant, String company,String ProfileGroup, String FulfillmentCenter,
			String LocationProfile) throws JASCIEXCEPTION;

	/**
	 * Created on:Jan 2 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to get Number Of Locations.
	 * Input parameter: String
	 * Return Type :List<Object>
	 * 
	 */
	public List<Object> GetNumberOfLocations(String tenant, String company,String fulfillmentCenter,String LocationProfile) throws JASCIEXCEPTION ;
	/**
	 * Created on:Jan 06 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to reassign location profile.
	 * Input parameter: String
	 * Return Type :Boolean
	 * 
	 */
	public Boolean ReassignLocation(String tolocationProfile,
			String fromlocationProfile, String tenant, String company,String VarFulFillmentCenterID)throws JASCIEXCEPTION;
	/**
	 * Created on:Jan 05 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute query to get FulfillmentCenter.
	 * Input parameter: String
	 * Return Type :List<Object>
	 * 
	 */
	public List<Object> getfulFillmentCenter(String tenant,
			String company,String PageName)throws JASCIEXCEPTION;
	
	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION ;

	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get list of distinct LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * @return 
	 * @throws JASCIEXCEPTION 
	 * 
*/

	public List<Object> getDistinctLocationProfile(String tenant, String company) throws JASCIEXCEPTION;

	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get notes for LocationProfile.
	 * Input parameter: String
	 * Return Type :List<NOTES>
	 * @return 
	 * @throws JASCIEXCEPTION 
	 * 
*/
	public List<NOTES> GetNotes(String tenant, String company, String profile,
			String locationprofileNotesid) throws JASCIEXCEPTION;	
	

	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description50 in LOCATIONPROFILEBE TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description50
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	public List<LOCATIONPROFILEBE> getDescription50Unique(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrDescription50,String StrScreenName,String LocationProfile) throws JASCIEXCEPTION;
		
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description20 in LOCATIONPROFILEBE TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description20
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	public List<LOCATIONPROFILEBE> getDescription20Unique(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrDescription20,String StrScreenName,String LocationProfile) throws JASCIEXCEPTION;
}
