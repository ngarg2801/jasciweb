/**

Date Developed  Dec 26 2014
Description for implement the IMENUPROFILEMAINTENANCEDAOIML in which we implement the hibernet Functionality
Created By : Sarvendra Tyagi  
Created Date Dec 26 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEHEADER;
import com.jasci.biz.AdminModule.model.MENUPROFILEOPTIONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class MENUPROFILEMAINTENANCEDAOIML implements IMENUPROFILEMAINTENANCEDAO {


	@Autowired
	SessionFactory objSessionFactory;

	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSION;

	/**
	 * Created on:Dec 26 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Profile list based on given parameter
	 * Input parameter:String strMenuProfile,String strMenuType,String strPartOfMenuProfile 
	 * Return Type :List<MENUPROFILEHEADER>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUPROFILEHEADER> getMenuProfileList(String strMenuType,
			String strPartOfMenuProfile) throws JASCIEXCEPTION {


		List<MENUPROFILEHEADER> objMenuprofileheaders=null;

		try{
			Criteria criteria=objSessionFactory.getCurrentSession().createCriteria(MENUPROFILEHEADER.class);
			Criterion menuType=Restrictions.like(GLOBALCONSTANT.MENUOPTION_MenuType, GLOBALCONSTANT.StrPercentSign+strMenuType+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion description20=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description20, GLOBALCONSTANT.StrPercentSign+strPartOfMenuProfile+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion description50=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description50, GLOBALCONSTANT.StrPercentSign+strPartOfMenuProfile+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion tenant=Restrictions.like(GLOBALCONSTANT.INUPUT_Parameter_MenuProfile_Id_Tenant, GLOBALCONSTANT.StrPercentSign+OBJCOMMONSESSION.getTenant()+GLOBALCONSTANT.StrPercentSign).ignoreCase();



			LogicalExpression andExp3 = Restrictions.or(description20, description50);
			
			LogicalExpression andExp4 = Restrictions.and(tenant, andExp3);
			
			LogicalExpression andExp2 = Restrictions.and(menuType, andExp4);
			criteria.add(andExp2);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			objMenuprofileheaders=criteria.list();

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuprofileheaders;
	}

	
	/**
	 * Created on:Jan 1 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Profile list based on given parameter
	 * Input parameter:String strMenuType 
	 * Return Type :List<MENUPROFILEHEADER>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUPROFILEHEADER> getMenuProfileListByMenuType(String strMenuType) throws JASCIEXCEPTION{
		
		List<MENUPROFILEHEADER> objMenuprofileheaders=null;

		try{

			Criteria criteria=objSessionFactory.getCurrentSession().createCriteria(MENUPROFILEHEADER.class);
			Criterion menuType=Restrictions.like(GLOBALCONSTANT.MENUOPTION_MenuType, GLOBALCONSTANT.StrPercentSign+strMenuType+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion tenant=Restrictions.eq(GLOBALCONSTANT.INUPUT_Parameter_MenuProfile_Id_Tenant, OBJCOMMONSESSION.getTenant()).ignoreCase();
			
			LogicalExpression andMenuType = Restrictions.and(tenant, menuType);
			
			
			criteria.add(andMenuType);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			objMenuprofileheaders=criteria.list();

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuprofileheaders;
		
	}
	
	
	/**
	 * Created on:Jan 1 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Profile list based on given parameter
	 * Input parameter:String strMenuType 
	 * Return Type :List<MENUPROFILEHEADER>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUPROFILEHEADER> getMenuProfileListByDescription(String strPartDescription) throws JASCIEXCEPTION{
		
		List<MENUPROFILEHEADER> objMenuprofileheaders=null;

		try{

			Criteria criteria=objSessionFactory.getCurrentSession().createCriteria(MENUPROFILEHEADER.class);
			
			Criterion description20=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description20, GLOBALCONSTANT.StrPercentSign+strPartDescription+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion description50=Restrictions.like(GLOBALCONSTANT.MENUOPTION_Description50, GLOBALCONSTANT.StrPercentSign+strPartDescription+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			
			Criterion tenant=Restrictions.eq(GLOBALCONSTANT.INUPUT_Parameter_MenuProfile_Id_Tenant, OBJCOMMONSESSION.getTenant()).ignoreCase();
			
			LogicalExpression OrDescription = Restrictions.or(description50, description20);
			LogicalExpression AndTenant = Restrictions.and(OrDescription, tenant);
			
			criteria.add(AndTenant);
			criteria.addOrder(Order.desc(GLOBALCONSTANT.MENUOPTION_LastActivityDate));
			objMenuprofileheaders=criteria.list();

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuprofileheaders;
				
		
	}
	
	
	
	

	/** 
	 * @Description: This function is used for getting label of menuProfile all screen
	 * @param  :language
	 * @return :List<LANGUAGES>
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Sarvendra Tyagi
	 * @Date   :Dec 26 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> getLanguageLabel(String language) throws JASCIEXCEPTION {

		try{

			return objSessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuProfile_Maintenance_Label_Query)
					.setParameter(GLOBALCONSTANT.INPUT_PARAMETER_LANGUAGE, language.toUpperCase()).list();


		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}

		return null;
	}


	/** 
	 * @Description:This function is getting data from menu Profile table for edit or update
	 * @param      : strMenuProfile
	 * @return     :List<MENUPROFILEHEADER>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 27 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUPROFILEHEADER> getMenuProfileList(String strMenuProfile) throws JASCIEXCEPTION {


		List<MENUPROFILEHEADER> objMenuProfile=null;
		try{


			objMenuProfile=objSessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfile_ProfileList_QueryName)
					.setParameter(GLOBALCONSTANT.MENUPROFILE_MenuProfile, strMenuProfile.toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfile_Tenant, OBJCOMMONSESSION.getTenant().toUpperCase()).list();


		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objJasciexception){

			throw new JASCIEXCEPTION(objJasciexception.getMessage());

		}
		return objMenuProfile;
	}






	/** 
	 * @Description :This function is getting menu types from general code list
	 * @param 		:strMenuType
	 * @return      :List<GENERALCODES>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getMenuTypes(String strMenuType) throws JASCIEXCEPTION {

		try{

			return objSessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfiles_GeneralCode_MenuTypeQueryName)
					.setParameter(GLOBALCONSTANT.InputMenuTypeName_MenuOptions_GeneralCode_MenuTypeQueryName, strMenuType.toUpperCase()).list();


		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}

		return null;
	}


	/** 
	 * @Description :This function is getting menu App Icon from MenuApp Icon table
	 * @param 		:
	 * @return      :List<GENERALCODES>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUAPPICONS> getMenuAppIconList() throws JASCIEXCEPTION {


		List<MENUAPPICONS> objMenuappicons=null;
		try{

			Criteria criteria = objSessionFactory.getCurrentSession().createCriteria(MENUAPPICONS.class);
			
			criteria.addOrder(Order.asc(GLOBALCONSTANT.MenuProfile_AppIcon).ignoreCase());
			objMenuappicons = (List<MENUAPPICONS>)criteria.list();
		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuappicons;
	}

	/** 
	 * @Description :This function is getting Menu Option from Menu Options table
	 * @param 		:MENUAPPICONS
	 * @return      :List<MENUOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuOptionList(String strApplication,String strMenuType) throws JASCIEXCEPTION{



		List<MENUOPTIONS> objMenuOptions=null;
		try{

			Criteria criteria = objSessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
			criteria.add(Restrictions.eq(GLOBALCONSTANT.StrApplication,strApplication).ignoreCase());
			if(OBJCOMMONSESSION.getJasci_Tenant().equalsIgnoreCase(OBJCOMMONSESSION.getTenant()))
			{
				criteria.add(Restrictions.eq(GLOBALCONSTANT.StrTenant,OBJCOMMONSESSION.getTenant()).ignoreCase());	
			}else{
				Criterion tenant=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSION.getTenant()).ignoreCase();
				Criterion tenantjasci=Restrictions.eq(GLOBALCONSTANT.MENUOPTION_Tenant, OBJCOMMONSESSION.getJasci_Tenant()).ignoreCase();
				LogicalExpression andExp1 = Restrictions.or(tenant,tenantjasci);
				criteria.add(andExp1);
			}
			
			criteria.add(Restrictions.eq(GLOBALCONSTANT.StrMenuType,strMenuType).ignoreCase());
			objMenuOptions = (List<MENUOPTIONS>)criteria.list();
		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuOptions;

	}

	/** 
	 * @Description :This function is getting Menu Option from Menu Options table
	 * @param 		:
	 * @return      :List<MENUOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUOPTIONS> getMenuAllOptionList(String strMenuType) throws JASCIEXCEPTION{


		List<MENUOPTIONS> objMenuOptions=null;
		try{

			Criteria criteria = objSessionFactory.getCurrentSession().createCriteria(MENUOPTIONS.class);
			criteria.add(Restrictions.eq(GLOBALCONSTANT.StrTenant,OBJCOMMONSESSION.getTenant()).ignoreCase());
			criteria.add(Restrictions.eq(GLOBALCONSTANT.StrMenuType,strMenuType).ignoreCase());
			criteria.addOrder(Order.asc(GLOBALCONSTANT.MENUOPTION_Application).ignoreCase());
			objMenuOptions = (List<MENUOPTIONS>)criteria.list();

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuOptions;


	}


	/** 
	 * @Description :This function is getting Menu Profile List from Menu Profile Header table
	 * @param 		:
	 * @return      :List<MENUPROFILEHEADER>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUPROFILEHEADER> getMenuProfileDisplayAll()throws JASCIEXCEPTION{

		List<MENUPROFILEHEADER> objMenuprofileheaders=null;

		try{
			
			objMenuprofileheaders=objSessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfile_ProfileDisplayAll_QueryName)
					.setParameter(GLOBALCONSTANT.Parameter_MenuOption_Tenant, OBJCOMMONSESSION.getTenant().toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}


		return objMenuprofileheaders;
	}



	/** 
	 * @Description :This Method is used for save or update Menu Profile List
	 * @param 		:objMenuprofileheader
	 * @return      :List<MENUPROFILEHEADER>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	public Boolean SaveOrUpdate(MENUPROFILEHEADER objMenuprofileheader) throws JASCIEXCEPTION {
		Boolean status=false;

		
		try{
			
			deleteMenuProfileOption(objMenuprofileheader.getId().getMenuProfile(), objMenuprofileheader.getId().getTenant());
			
		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}


		try{

			Session session = objSessionFactory.openSession();
			Transaction TransactionObject = session.beginTransaction();



			session.saveOrUpdate(objMenuprofileheader);

			TransactionObject.commit();
			session.close();
			status=true;

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}
		return status;
	}


	/** 
	 * @Description :This Method is used for check menu profile in menu profile option for delete menu profile 
	 * @param 		:strMenuProfile
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 30 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> CheckDeleteMenuProfile(String strMenuProfile,String strTenant) throws JASCIEXCEPTION {
		List<Object> objList=null;
		try{


			objList= objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.NativeQueryName_MenuOptionProfile_MEnuProfileAssignment_query)
					.setString(GLOBALCONSTANT.MENUPROFILE_MenuProfile, strMenuProfile.toUpperCase())
					.setString(GLOBALCONSTANT.Parameter_MenuOption_Tenant, strTenant.toUpperCase()).list();

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		return objList;
	}

	
	/** 
	 * @Description :This Method is used for delete menu profile 
	 * @param 		:strMenuProfile
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 30 2014
	 */

	public Boolean deleteMenuProfile(String strMenuProfile,String Tenant) throws JASCIEXCEPTION {
		Boolean Status=false;
		
		Transaction TransactionObject=null;
		try{


			Session session = objSessionFactory.openSession();

			TransactionObject= session.beginTransaction();

			String strProfile= strMenuProfile.toUpperCase();

				Query query = session.getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfileOPTION_Delete_QueryName);
				query.setString(GLOBALCONSTANT.MENUPROFILE_MenuProfile,strProfile);

				query.setString(GLOBALCONSTANT.Parameter_MenuOption_Tenant, Tenant.toUpperCase());
				query.executeUpdate();

				Status=true;
				TransactionObject.commit();
				session.close();

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objJasciexception){

			if (TransactionObject!=null) TransactionObject.rollback();
			throw new JASCIEXCEPTION(objJasciexception.getMessage());
		}
		
		
		//delete from Menu Profile Header table
		try{


			Session session = objSessionFactory.openSession();

			TransactionObject= session.beginTransaction();
			String strProfile= strMenuProfile.toUpperCase();

			//delete row from menu Profile Option
			
				Query query = session.getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfile_Assigment_Delete_QueryName);
				query.setString(GLOBALCONSTANT.MENUPROFILE_MenuProfile,strProfile);

				query.setString(GLOBALCONSTANT.Parameter_MenuOption_Tenant, Tenant.toUpperCase());
				query.executeUpdate();

				Status=true;
				TransactionObject.commit();
				session.close();
				}catch(Exception objJasciexception){

			if (TransactionObject!=null) TransactionObject.rollback();
			throw new JASCIEXCEPTION(objJasciexception.getMessage());
		}
		
		
		
		try{


			Session session = objSessionFactory.openSession();

			TransactionObject= session.beginTransaction();
			String strProfile= strMenuProfile.toUpperCase();

			//delete row from menu Profile Header
			
				Query query = session.getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfile_Delete_QueryName);
				query.setString(GLOBALCONSTANT.MENUPROFILE_MenuProfile,strProfile);

				query.setString(GLOBALCONSTANT.Parameter_MenuOption_Tenant, Tenant.toUpperCase());
				query.executeUpdate();

				Status=true;
				TransactionObject.commit();
				session.close();


			
			
			

		}catch(Exception objJasciexception){

			if (TransactionObject!=null) TransactionObject.rollback();
			throw new JASCIEXCEPTION(objJasciexception.getMessage());
		}

		
	

		
		
		return Status;
	}
	

	/** 
	 * @Description :This Method is used for delete menu profile 
	 * @param 		:strMenuProfile
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :JAN 11 2014
	 */
	public Boolean deleteMenuProfileOption(String strMenuProfile,String strTenant)throws JASCIEXCEPTION{
		
	Boolean Status=false;
		
		Transaction TransactionObject=null;
		try{


			Session session = objSessionFactory.openSession();

			TransactionObject= session.beginTransaction();
			String strProfile= strMenuProfile.toUpperCase();

			//delete row from menu Profile Option
			
				Query query = session.getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfileOPTION_Delete_QueryName);
				query.setString(GLOBALCONSTANT.MENUPROFILE_MenuProfile,strProfile);

				query.setString(GLOBALCONSTANT.Parameter_MenuOption_Tenant, strTenant.toUpperCase());
				query.executeUpdate();

				Status=true;
				TransactionObject.commit();
				session.close();


			
			
			

		}catch(Exception objJasciexception){

			if (TransactionObject!=null) TransactionObject.rollback();
			throw new JASCIEXCEPTION(objJasciexception.getMessage());
		}
		
		return Status;
		
	}
	


	/** 
	 * @Description :This Method is used for saved assigned menu option in menuprofile header 
	 * @param 		:strMenuProfile
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby :Sarvendra tyagi
	 * @Date	    :Dec 31 2014
	 */
	public Boolean saveAssignedMenuProfile(MENUPROFILEOPTIONS objMenuprofileoptions)throws JASCIEXCEPTION{
		Boolean status=false;
		try{

			Session session = objSessionFactory.openSession();
			Transaction TransactionObject = session.beginTransaction();
			session.saveOrUpdate(objMenuprofileoptions);

			TransactionObject.commit();
			session.close();
			status=true;

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			new JASCIEXCEPTION(objException.getMessage());
		}
		return status;

	}
	
	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
 public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
 
  List<TEAMMEMBERS> objTeammembers=null;
  try
  {
   objTeammembers= objSessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
  }
  catch(Exception ObjException)
  {
   throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
   
   
  }
return objTeammembers;
}
 
 
 /** 
	 * @Description :This Method is used for get assigned menu option in menuprofile Option table 
	 * @param 		:String Tenant, String strMenuProfile
	 * @return      :List<MENUOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby :Sarvendra tyagi
	 * @Date	    :Dec 31 2014
	 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<MENUOPTIONS> getAssignedMenurofile(String Tenant, String strMenuProfile) throws JASCIEXCEPTION{
	
	List<MENUOPTIONS> objMenuoptions=null;
	try{
		
		objMenuoptions=objSessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_QueryName)
				.setParameter(GLOBALCONSTANT.MENUPROFILE_MenuProfile, strMenuProfile.toUpperCase())
				.setParameter(GLOBALCONSTANT.Parameter_MenuOption_Tenant,Tenant.toUpperCase()).list();
		
	}catch(Exception objException){
		
		new JASCIEXCEPTION(objException.getMessage());
	}
	
	
	return objMenuoptions;
}


 /** 
 * @Description :This Method is used for get Display all after removing assigned menu option in menuprofile Option table 
 * @param 		:String Tenant, String strMenuProfile
 * @return      :List<MENUOPTIONS>
 * @throws 		:JASCIEXCEPTION
 * @Developedby :Sarvendra tyagi
 * @Date	    :JAN 10 2015
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<MENUOPTIONS> getAssignedMenuProfileDispalyAll(String Tenant, String strMenuProfile, String strMenuType) throws JASCIEXCEPTION{
	
	List<MENUOPTIONS> objMenuoptions=null;
	try{
		if(OBJCOMMONSESSION.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSION.getJasci_Tenant()))
		{
		objMenuoptions=objSessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_Display_All_Query_JASCI).setParameter(GLOBALCONSTANT.MENUPROFILE_MenuProfile, strMenuProfile.toUpperCase())
				.setParameter(GLOBALCONSTANT.MenuProfile_Maintenance_Input_MenuProfileTenant,Tenant.toUpperCase())
				.setParameter(GLOBALCONSTANT.MENUOPTION_MenuType,strMenuType.toUpperCase()).list();
		}else{
			objMenuoptions=objSessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_Display_All_Query).setParameter(GLOBALCONSTANT.MENUPROFILE_MenuProfile, strMenuProfile.toUpperCase())
					.setParameter(GLOBALCONSTANT.Parameter_MenuOption_Tenant,Tenant.toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfileAssigment_tenant1,OBJCOMMONSESSION.getJasci_Tenant())
					.setParameter(GLOBALCONSTANT.MenuProfile_Maintenance_Input_MenuProfileTenant,Tenant.toUpperCase())
					.setParameter(GLOBALCONSTANT.MENUOPTION_MenuType,strMenuType.toUpperCase()).list();

		}
				
	}catch(Exception objException){
		
		new JASCIEXCEPTION(objException.getMessage());
	}
	
	return objMenuoptions;
}



/** 
 * @Description :This Method is used for get Display all after removing assigned menu option in menuprofile Option table 
 * @param 		:String Tenant, String strMenuProfile
 * @return      :List<MENUOPTIONS>
 * @throws 		:JASCIEXCEPTION
 * @Developedby :Sarvendra tyagi
 * @Date	    :JAN 10 2015
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<MENUOPTIONS> getAssignedMenuProfileDispalyApplication(String Tenant, String strMenuProfile,String strMenuType,String strApplication) throws JASCIEXCEPTION{
	
	List<MENUOPTIONS> objMenuoptions=null;
	try{
		if(OBJCOMMONSESSION.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSION.getJasci_Tenant()))
		{
		objMenuoptions=objSessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_Display_Application_Query_JASCI).setParameter(GLOBALCONSTANT.MENUPROFILE_MenuProfile, strMenuProfile.toUpperCase())
				.setParameter(GLOBALCONSTANT.MenuProfile_Maintenance_Input_MenuProfileTenant,Tenant.toUpperCase())
				.setParameter(GLOBALCONSTANT.MENUOPTION_MenuType,strMenuType.toUpperCase())
				.setParameter(GLOBALCONSTANT.MENUOPTION_Application,strApplication.toUpperCase()).list();
		}else{
			objMenuoptions=objSessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuProfile_Maintenance_AssignMenuProfileOption_Display_Application_Query).setParameter(GLOBALCONSTANT.MENUPROFILE_MenuProfile, strMenuProfile.toUpperCase())
					.setParameter(GLOBALCONSTANT.Parameter_MenuOption_Tenant,Tenant.toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfileAssigment_tenant1,OBJCOMMONSESSION.getJasci_Tenant())
					.setParameter(GLOBALCONSTANT.MenuProfile_Maintenance_Input_MenuProfileTenant,Tenant.toUpperCase())
					.setParameter(GLOBALCONSTANT.MENUOPTION_MenuType,strMenuType.toUpperCase())
					.setParameter(GLOBALCONSTANT.MENUOPTION_Application,strApplication.toUpperCase()).list();

		}
		
	}catch(Exception objException){
		
		new JASCIEXCEPTION(objException.getMessage());
	}
	
	return objMenuoptions;
	
}




/** 
 * @Description :This Method is used for get image url from app icon table 
 * @param 		:String appIcon
 * @return      :List<MENUOPTIONS>
 * @throws 		:JASCIEXCEPTION
 * @Developedby :Sarvendra tyagi
 * @Date	    :JAN 10 2015
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<Object> getAppIConURl(String appIcon) throws JASCIEXCEPTION{
	
	try{
		
		return objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.GETDISTINCTAPPICONADDRESS)
		.setParameter(GLOBALCONSTANT.IntZero, appIcon.toUpperCase()).list();
		
	}catch(Exception objException){
		
		throw new JASCIEXCEPTION(objException.getMessage());
	}
}

}
