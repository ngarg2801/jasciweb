/**
Description it is an Interface where we can declare all of the methods that are implement in SMARTTASKCONFIGURATORDAOIMPL
Created By Shailendra Rajput  
Created Date May 15, 2015
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public interface ISMARTTASKEXECUTIONSDAO {
	
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List getDisplayAll(String StrTenant,String StrCompany) throws JASCIEXCEPTION;
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List getList(String StrTenant, String StrCompany, String StrApplication, String StrExecutionGroup, String StrExecutionType, String StrExecutionDevice, String StrExecutionName, String StrDescription20, String StrButton) throws JASCIEXCEPTION;
	public List<SMARTTASKEXECUTIONS> getExecutionData(String StrExecution_Name) throws JASCIEXCEPTION;
	public SMARTTASKSEQHEADERS getSTSheadersData(String StrTenant_Id,String Company_Id, String StrExecution_Sequence_Name) throws JASCIEXCEPTION;
	public List<SMARTTASKEXECUTIONS> getSmartTaskExecutionData(String StrTenant_Id,String Company_Id,String strExecutionGroup,String strExecutionType, String strExecutionDevice) throws JASCIEXCEPTION;
	public List<Object> getGeneralCodeDropDown(String StrTenant_Id, String Company_Id,String strGeneral_Code_Id) throws JASCIEXCEPTION;
	public List<TEAMMEMBERS> getTeamMemberName(String Tenant,String TeamMember) throws JASCIEXCEPTION;
	public Boolean addExecutions(SMARTTASKEXECUTIONS smartTaskExecutions) throws JASCIEXCEPTION;
	public List<SMARTTASKEXECUTIONS> isExecutionExist(String strCompanyId, String strTenantId, String strExecutionName) throws JASCIEXCEPTION;
	public Boolean deleteExecutions(SMARTTASKEXECUTIONS smartTaskExecutions) throws JASCIEXCEPTION;
	public boolean deleteEntry(String StrTenant_Id,String StrCompany_Id,String StrExecution_Name) throws JASCIEXCEPTION;
	public List<SMARTTASKSEQINSTRUCTIONS> getSmartTaskSeqInsttructions(String StrTenant_Id,String StrCompany_Id,String StrExecution_Name) throws JASCIEXCEPTION;
}

