/**
 * Date Developed  Dec 14 2014
 * Created By "Rahul Kumar"
 * Description : this class provide data base function of Menu app icon entity
 *
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class MENUAPPICONDAL  implements IMENUAPPICONDAL{

	@Autowired
	private SessionFactory sessionFactory ;
	/**
	 * @author Rahul Kumar
	 * @Date Dec 12, 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#setScreenLanguage(java.lang.String, java.lang.String)
	 */
	public List<LANGUAGES> setScreenLanguage(String StrScreenName, String StrLanguage) throws JASCIEXCEPTION {
		

		List<LANGUAGES> ObjListLanguages=GetLanguage(StrScreenName,StrLanguage);

		return ObjListLanguages;
	}

	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 12, 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<LANGUAGES>
	 *
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	private List<LANGUAGES> GetLanguage( String StrScreenName,String StrLanguage) throws JASCIEXCEPTION
	{

		try
		{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Languages_Query).setParameter(GLOBALCONSTANT.INT_ZERO,StrScreenName.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrLanguage.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
	}





	/**
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param objMenuappicons
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#addOrUpdateMenuAppIcon(com.jasci.biz.AdminModule.model.MENUAPPICONS)
	 */
	public Boolean addOrUpdateMenuAppIcon(MENUAPPICONS objMenuappicons) throws JASCIEXCEPTION {
		
		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();	
		Boolean status=false;

		try{
			session.saveOrUpdate(objMenuappicons);
			TransactionObject.commit();
			session.close();
			status=true;

		}catch(Exception objException){
			TransactionObject.rollback();
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return status;
	}





	/**
	 * @author Rahul Kumar
	 * @Date Dec 14, 2014
	 * @param Tenant
	 * @param Company
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#getManuAppIconList(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUAPPICONS> getManuAppIconList(String Tenant, String Company) throws JASCIEXCEPTION {
		
		List<MENUAPPICONS> objMenuappicons=null;
		try{
			objMenuappicons=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuAppIcon_GetList_NamedQuery).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objMenuappicons;
	}





	/**
	 * @author Rahul Kumar
	 * @Date Dec 15, 2014
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#getMenuAppIconByAppIcon(java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUAPPICONS> getMenuAppIconByAppIcon(String AppIcon) throws JASCIEXCEPTION {
		

		List<MENUAPPICONS> objMenuappicons=null;

		try{

			objMenuappicons=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuAppIcon_GetListByAppIcon_NamedQuery).setParameter(GLOBALCONSTANT.INT_ZERO,AppIcon.trim().toUpperCase()).list();

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception objException){
			throw new  JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuappicons;
	}





	/**
	 * @author Rahul Kumar
	 * @Date Dec 15, 2014
	 * @param AppIconName
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#getMenuAppIconByAppIconName(java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUAPPICONS> getMenuAppIconByAppIconName(String AppIconName) throws JASCIEXCEPTION {
		

		List<MENUAPPICONS> objMenuappicons=null;

		try{

			objMenuappicons=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuAppIcon_GetListByAppIconName_NamedQuery).setString(GLOBALCONSTANT.Companies_name2, GLOBALCONSTANT.StrPercentSign+AppIconName.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).setString(GLOBALCONSTANT.Companies_name5, GLOBALCONSTANT.StrPercentSign+AppIconName.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).list();

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch(Exception objException){
			throw new  JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuappicons;
	}





	/**
	 * @author Rahul Kumar
	 * @Date Dec 15, 2014
	 * @param AppIconPart
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#getMenuAppIconByAppIconPart(java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUAPPICONS> getMenuAppIconByAppIconPart(String AppIconPart) throws JASCIEXCEPTION {
		
		List<MENUAPPICONS> objMenuappicons=null;
		Session session = sessionFactory.openSession();

		try{

			
			Criteria cr = session.createCriteria(MENUAPPICONS.class);
			
			Criterion appIcon=Restrictions.like(GLOBALCONSTANT.MenuAppIcon_AppIcon, GLOBALCONSTANT.StrPercentSign+AppIconPart.trim()+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion appIconName=Restrictions.like(GLOBALCONSTANT.MenuAppIcon_DescriptionShort, GLOBALCONSTANT.StrPercentSign+AppIconPart.trim()+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			Criterion appIconPart=Restrictions.like(GLOBALCONSTANT.MenuAppIcon_DescriptionLong, GLOBALCONSTANT.StrPercentSign+AppIconPart.trim()+GLOBALCONSTANT.StrPercentSign).ignoreCase();
			
			LogicalExpression andExp1 = Restrictions.or(appIcon,appIconName);
			LogicalExpression andExp2 = Restrictions.or(andExp1, appIconPart);
			
			cr.add(andExp2);
			
			objMenuappicons = cr.list();
			session.close();
			
		}catch (HibernateException objHibernateException){
			throw new JASCIEXCEPTION(objHibernateException.getMessage());
		} 
		catch(Exception objException){
			session.close();
			throw new  JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuappicons;
	}





	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 16, 2014
	 * @param GeneralCodeId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#getApplicationList(java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getApplicationList(String Tenant,String Compnay,String GeneralCodeId) throws JASCIEXCEPTION {
		
		List<GENERALCODES> objGeneralcodes=null;
		Session session = sessionFactory.openSession();
		
		try{
			objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Select_Applications_Query).setParameter(GLOBALCONSTANT.INT_ZERO, GeneralCodeId.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, Compnay.trim().toUpperCase()).list();
			session.close();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception obException){
			session.close();
			throw new JASCIEXCEPTION(obException.getMessage());
			
		}
				
		
		return objGeneralcodes;
	}





	/**
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 * @param Application
	 * @param AppIcon
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#deleteMenuAppIcon(java.lang.String, java.lang.String)
	 */
	public Boolean deleteMenuAppIcon(String Application, String AppIcon) throws JASCIEXCEPTION {
		
		Session session = sessionFactory.openSession();
		Boolean Status=false;
		try{
			Query query = session.getNamedQuery(GLOBALCONSTANT.MenuAppIcon_Delete_NamedQuery);
			query.setString(GLOBALCONSTANT.DataBase_MenuAppIcons_APPLICATION, Application.toUpperCase());
			query.setString(GLOBALCONSTANT.DataBase_MenuAppIcons_APP_ICON, AppIcon.toUpperCase());
			query.executeUpdate();
			Status=true;
			session.close();
		}catch(Exception objException){
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return Status;
	}





	/**
	 * 
	 * @author Rahul Kumar
	 * @Date Dec 17, 2014
	 * @param Application
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.IMENUAPPICONDAL#getMenuAppIconByApplication(java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUAPPICONS> getMenuAppIconByApplication(String Application)
			throws JASCIEXCEPTION {
		List<MENUAPPICONS> objMenuappicons=null;

		try{
			objMenuappicons=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuAppIcon_GetListByApplication_NamedQuery).setParameter(GLOBALCONSTANT.INT_ZERO,Application.toUpperCase()).list();

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			throw new  JASCIEXCEPTION(objException.getMessage());
		}

		return objMenuappicons;
	}




	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
		
		if(objTeammembers.size()==GLOBALCONSTANT.INT_ZERO){
		try
		{
			objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query_By_TEAMID).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.trim().toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
			
			
		}
	}
		return objTeammembers;
	}
}
