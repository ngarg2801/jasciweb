
/**

Date Developed  Jan 15 2015
Created By "Rahul Kumar"
Description : define an interface method to communicate with DataBase 
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARD;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILOCATIONLABELPRINTDAL {
	
	
	/**
	 * 
	 *@Description get general code from general code table base on general code id
	 *@Auther Rahul Kumar
	 *@Date Jan 15, 2015			
	 *@Return type List<GENERALCODES>
	 *@param Tenant
	 *@param Company
	 *@param GeneralCodeId
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION;
	
	/**
	 * 
	 *@Description get location name from table Locations 
	 *@Auther Rahul Kumar
	 *@Date Jan 19, 2015			
	 *@Return type List<LOCATION>
	 *@param objCommonsessionbe
	 *@param Area
	 *@param Loaction
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATION> getLocation(COMMONSESSIONBE objCommonsessionbe,String Area,String Loaction)throws JASCIEXCEPTION;
	
	/**
	 * 
	 *@Description get WizardControlNumber from table Locations 
	 *@Auther Rahul Kumar
	 *@Date Jan 20, 2015			
	 *@Return type List<LOCATION>
	 *@param objCommonsessionbe
	 *@param WizardControlNumber
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATION> getWizardControlNumber(COMMONSESSIONBE objCommonsessionbe,String WizardControlNumber)throws JASCIEXCEPTION;
	
	/**
	 * 
	 * @Description get Location Wizard list from table wizard location
	 * @author Rahul Kumar
	 * @Date Jan 21, 2015
	 * @Return type List<LOCATIONWIZARD>
	 * @param objCommonsessionbe
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public List<LOCATIONWIZARD> getLocationWizardList(COMMONSESSIONBE objCommonsessionbe,String FulfillmentCenterid)throws JASCIEXCEPTION;
	

	/**
	 * 
	 *@Description get location name from table Locations 
	 *@Auther sarvendra tyagi
	 *@Date feb 23, 2015			
	 *@Return type List<LOCATION>
	 *@param objCommonsessionbe
	 *@param Area
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATION> getLocationReprint(COMMONSESSIONBE objCommonsessionbe,String Area)throws JASCIEXCEPTION;
	
	
	
	/**
	 * 
	 *@Description get Location Label print type from table Locations 
	 *@Auther Rahul Kumar
	 *@Date Jan 20, 2015			
	 *@Return type List<LOCATION>
	 *@param objCommonsessionbe
	 *@param Area
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public List<LOCATION> getLocationLabelTypePrint(COMMONSESSIONBE objCommonsessionbe,String Area)throws JASCIEXCEPTION;
	
}
