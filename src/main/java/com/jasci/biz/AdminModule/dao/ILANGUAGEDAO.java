/*
Date Developed  Sep 18 2014
Description   Get Language Translation
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.utilbe.COMMONSESSIONBE;

public interface ILANGUAGEDAO {
	
	public  List<LANGUAGES>  GetLanguage(COMMONSESSIONBE ObjCommonSessionBe);

}
