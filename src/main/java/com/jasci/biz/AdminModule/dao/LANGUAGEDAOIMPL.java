/*

Date Developed  Sep 18 2014
Description   Get Language Translation
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.utilbe.COMMONSESSIONBE;


public class LANGUAGEDAOIMPL implements ILANGUAGEDAO
{
	@Autowired
	private SessionFactory sessionFactory ;
	
	     /**
		 *Description this function design to get database connection
		 *Input parameter ObjCommonSessionBe
		 *Return Type "List<LANGUAGES>"
		 *Created By "Diksha Gupta"
		 *Created Date "17-10-14" 
		 * */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> GetLanguage(COMMONSESSIONBE ObjCommonSessionBe)
	{
		
		return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Languages_Query).setParameter(GLOBALCONSTANT.INT_ZERO, ObjCommonSessionBe.getKeyPhrase()).setParameter(GLOBALCONSTANT.IntOne, ObjCommonSessionBe.getLanguage()).list();
		
	}

}
