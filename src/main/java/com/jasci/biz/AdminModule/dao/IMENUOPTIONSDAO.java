/**

Date Developed: Dec 15 2014
Description: for Declare method IMenuOptionDao 
Created By: Sarvendra Tyagi  
Created Date Dec 15 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEOPTIONS;
import com.jasci.exception.JASCIEXCEPTION;



public interface IMENUOPTIONSDAO {
	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Option list based on given parameter
	 * Input parameter: String strApplication,String partOfApplication,String strMenuOption,String partOfMenuOption,String strMenuType,String partOfdesc20,String partOfDesc50
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	public List<MENUOPTIONS> getMenuOptionList(String strApplication,String strMenuOption,String strMenuType,String partOfdesc20,
			String partOfDesc50,String strTenant) throws JASCIEXCEPTION;
	

	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Option list based on given parameter
	 * Input parameter: String strApplication,String partOfApplication,String strMenuOption,String partOfMenuOption,String strMenuType,String partOfdesc20,String partOfDesc50
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	public List<MENUOPTIONS> getMenuOptionListByApplication(String strApplication) throws JASCIEXCEPTION;
	
	
	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Option list based on given parameter
	 * Input parameter: String strApplication,String partOfApplication,String strMenuOption,String partOfMenuOption,String strMenuType,String partOfdesc20,String partOfDesc50
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	public List<MENUOPTIONS> getMenuOptionListByMenuType(String strMenuType) throws JASCIEXCEPTION;

	
	
	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Option list based on given parameter
	 * Input parameter: String strApplication,String partOfApplication,String strMenuOption,String partOfMenuOption,String strMenuType,String partOfdesc20,String partOfDesc50
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	public List<Object> checkMenuOptionFromOptionTable(String strMenuOption) throws JASCIEXCEPTION;
	
	
	
	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Option list based on given parameter
	 * Input parameter: String strApplication,String partOfApplication,String strMenuOption,String partOfMenuOption,String strMenuType,String partOfdesc20,String partOfDesc50
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	public List<MENUOPTIONS> getMenuOptionListByTenant(String strTenant) throws JASCIEXCEPTION;
	
	/**
	 * Created on:Dec 15 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Option list based on given parameter
	 * Input parameter: String strApplication,String partOfApplication,String strMenuOption,String partOfMenuOption,String strMenuType,String partOfdesc20,String partOfDesc50
	 * Return Type :List<MENUOPTIONS>
	 * 
	 */
	
	public List<MENUOPTIONS> getMenuOptionListByDescription(String partOfdesc20) throws JASCIEXCEPTION;
	
	
	
	
	/** 
	 * @Description:This function is getting all data from menuoption table 
	 * @param      : strMenuOption
	 * @return     :List<MENUOPTIONS>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 18 2014
	 */
	public List<MENUOPTIONS> getMenuOptionDisplayAll() throws JASCIEXCEPTION;
	

	
	
	/** 
	 * @Description: This function is used for getting label of menuoption all screen
	 * @param  :language
	 * @return :List<LANGUAGES>
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Sarvendra Tyagi
	 * @Date :dec 23 2014
	 */
	
	
	public List<LANGUAGES> getLanguageLabel(String language)throws JASCIEXCEPTION;
	
	
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function is checking menu Option assigned  in menu profile option or not
	 * Input parameter: String MenuOption
	 * Return Type :List<Object>
	 * 
	 */
	
	
	public List<MENUPROFILEOPTIONS> checkAssignedOption(String strMenuOption)throws JASCIEXCEPTION;
	
	
	/**
	 * @deprecated:This function is used for delete the menu option from menu option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Dec 18 2014
	 */
	public Boolean deleteMenuOption(String strMenuOption)throws JASCIEXCEPTION;
	
	

	/**
	 * @deprecated:This function is used for delete the menu option from menu Profile option table if this menu option
	 * @param: strMenuOption
	 * @return: Boolean
	 * @throws: DATABASEEXCEPTION
	 * @throws: JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		: Jan 2  2014
	 */
	public Boolean deleteMenuOptionFromMenuProfileOption(String strMenuOption)throws JASCIEXCEPTION;
	
	
	
	
	/** 
	 * @Description:This function is getting data from menuoption table for edit or update
	 * @param      : strMenuOption
	 * @return     :List<MENUOPTIONS>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 18 2014
	 */
	public List<MENUOPTIONS> getMenuOptionData(String strMenuOption)throws JASCIEXCEPTION;
	
	
	/**  
	 * @Description :This function getting application and Sub Application from general code table
	 * @param       :strApplication
	 * @return	    :List<Object>
	 * @throws      :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 19 2014
	 */
	public List<GENERALCODES> getApplicationAndSubApplication(String strApplication) throws JASCIEXCEPTION;
	
	/** 
	 * @Description :This function is getting menu types from general code list
	 * @param 		:strMenuType
	 * @return      :List<Object>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 19 2014
	 */
	public List<GENERALCODES> getMenuTypes(String strMenuType)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This function is used for udate record 
	 * @param 		:strMenuType
	 * @return      :MENUOPTIONS
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 19 2014
	 */
	public MENUOPTIONS update(MENUOPTIONS objMenuoptions)throws JASCIEXCEPTION;
	
}
