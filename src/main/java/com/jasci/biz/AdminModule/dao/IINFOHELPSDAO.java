/*
Created By Aakash Bishnoi
Created Date Oct 29 2014
Description  This class is used to get/set transaction with database 
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;
import com.jasci.biz.AdminModule.model.INFOHELPS;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.exception.JASCIEXCEPTION;

public interface IINFOHELPSDAO {
	

	/** 
	 * 
	 * @param StrInfoHelp
	 * @param StrPartoftheDescription
	 * @param StrInfoHelpType
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	//Function for get data using specific query and Display All list also
	public List<?> getInfoHelpList(String StrInfoHelp,String StrPartoftheDescription,String StrInfoHelpType) throws JASCIEXCEPTION;

	/** 
	 * 
	 * @param ObjectInfohelps
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	//Function for insert INFOHELPS table
	public String insertInfoHelps(INFOHELPS ObjectInfohelps) throws JASCIEXCEPTION;
	

	/** 
	 * 
	 * @param InfoHelp
	 * @param Language
	 * @return
	 */
	//Check the uniqueness of infohelps
	public INFOHELPS checkInfoHelp(String InfoHelp,String Language);
	
	
 
	/** 
	 * 
	 * @param InfoHelp
	 * @param Language
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	//Fetch data on Serch lookup screen on the basis of
	public INFOHELPS fetchInfoHelp(String InfoHelp,String Language) throws JASCIEXCEPTION;
	
	
	/** 
	 * 
	 * @param ObjectInfoHelps
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	//It is used to update records
	public String updateInfoHelps(INFOHELPS ObjectInfoHelps) throws JASCIEXCEPTION;
	
	/** 
	 * 
	 * @param InfoHelp
	 * @param Language
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	//Delte Records form InfoHelps
	 public Boolean deleteInfoHelps(String InfoHelp,String Language) throws JASCIEXCEPTION;
	 
	 /** 
	  * 
	  * @param StrLanguage
	  * @return
	  * @throws JASCIEXCEPTION
	  */
	 //get the  InfoHelp screen label list from language table.
	 public  List<LANGUAGES> getInfoHelpAssignmentSearchLabels(String StrLanguage) throws JASCIEXCEPTION;

	 /**
		 * 
		 * @Description get team member name form team members table based on team member id
		 * @param Tenant
		 * @param Company
		 * @param TeamMember
		 * @return
		 * @throws JASCIEXCEPTION
		 */
		public List<TEAMMEMBERS> getTeamMemberName(String Tenant,String TeamMember)throws JASCIEXCEPTION;

	 
}
