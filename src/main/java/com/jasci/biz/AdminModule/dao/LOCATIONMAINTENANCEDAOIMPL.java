/*

Date Developed  Sep 18 2014
Description This class provide all of the functionality related to database for Location Maintenance screen 
Created By Aakash Bishnoi  
Created Date Dec 26 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.INVENTORY_LEVELS;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONPK;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class LOCATIONMAINTENANCEDAOIMPL implements ILOCATIONMAINTENANCEDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	
	COMMONSESSIONBE OBJCOMMONSESSIONBE;

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is used to Add Record in Location Table Form LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	 */
	public String addEntry(LOCATION ObjectLocation) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database
			sessionFactory.getCurrentSession().save(ObjectLocation);	
			Status=GLOBALCONSTANT.StatusTrue;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 6, 2015
	 * Description:This is used to Update Record in Location Table Form LocationMaintenance Sreen
	 *@param ObjectLocation
	 *@return String
	 *@throws JASCIEXCEPTION
	 */
	public String updateEntry(LOCATION ObjectLocation) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database
			sessionFactory.getCurrentSession().saveOrUpdate(ObjectLocation);	
			Status=GLOBALCONSTANT.StatusTrue;
		}catch (HibernateException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}
	
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This function is used to get the list in dropdown from GeneralCode where GENERAL_CODE_ID will be change
	 *@param StrTenant
	 *@param StrCompany
	 *@param GeneralCodeID
	 *@return  List<GENERALCODES>
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<GENERALCODES> getGeneralCode(String StrTenant,String StrCompany,String GeneralCodeID) {
		List<GENERALCODES> GeneralCodeList=null;
		try{	
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameSelectGeneralCode);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeID.toUpperCase().trim());
			GeneralCodeList = ObjQuery.list();
			return GeneralCodeList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return GeneralCodeList;
		}		
	}


	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Dec 30, 2014
	 * Description:This is used to get the value in dropdown from LOCATION_PROFILE table
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFullfilmentCenter
	 *@return
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATIONPROFILES> getLocationProfile(String StrTenant,String StrCompany,String StrFullfilmentCenter) {
		List<LOCATIONPROFILES> LocationProfileList=null;
		try{	
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Location_QuerySelectLocationProfile);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFullfilmentCenter.toUpperCase().trim());
			LocationProfileList = ObjQuery.list();
			return LocationProfileList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return LocationProfileList;
		}		
	}

	/**
	 * @Description get team member name form team members table based on team member id
	 * @param Tenant
	 * @param Company
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
	 public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
		 
		  List<TEAMMEMBERS> objTeammembers=null;
		  try
		  {
		   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		  }catch (SQLGrammarException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception ObjectException)
		  {
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		  }
		  
		  if(objTeammembers.size()== GLOBALCONSTANT.INT_ZERO){
		  try
		  {
		   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query_By_TEAMID).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.trim().toUpperCase()).list();
		  }catch (SQLGrammarException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception ObjectException)
		  {
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		  }
		 }
		  return objTeammembers;
		 }

	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 1, 2015
	 * Description:This function used to check uniqueness of Location in LOCATION TABLE.
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@return List<LOCATION>
	 *@throws JASCIEXCEPTION
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATION> getLocationUnique(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrArea,String StrLocation) throws JASCIEXCEPTION{
		List<LOCATION> UniqueLocationList=null;
		try{	
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameUniqueLocationCheck);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrArea.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrLocation.toUpperCase().trim());
			UniqueLocationList = ObjQuery.list();
			return UniqueLocationList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return UniqueLocationList;
		}		
	}
	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 1, 2015
	 * Description:This Function is used to check the uniqueness of alternate_location in LOCATION TABLE
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrLocation
	 *@param StrAlternateLocation
	 *@return List<LOCATION>
	 *@throws JASCIEXCEPTION
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATION> getAlternateLocationUnique(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrAlternateLocation){	
		List<LOCATION> UniqueAlternateLocationList=null;
		try{	

			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameUniqueAlternateLocationCheck);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrAlternateLocation.toUpperCase().trim());
			 UniqueAlternateLocationList = ObjQuery.list();
			 return UniqueAlternateLocationList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return UniqueAlternateLocationList;
		}		
	}



	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 3, 2015
	 * Description:This is used to search the list from LOCATION on Location Search Screen.
	 *@param StrTenant
	 *@param StrCompany
	 *@param StrFulfillmentCenter
	 *@param StrArea
	 *@param StrDescription20
	 *@param StrDescription50
	 *@param StrLocationType
	 *@return List<LOCATION> 
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strrawtypes})
	public List getList(String StrLocation,String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrArea,String StrDescription,String StrLocationType){
	
		List<Object> lOCObjects = null;
	    
		try{
		String  Location_QueryDisplayAll_New = GLOBALCONSTANT.Location_QueryDisplayAll_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_QueryDisplayAll_Second;
		if((StrLocation == null || GLOBALCONSTANT.BlankString.equals(StrLocation)) && (StrArea == null || GLOBALCONSTANT.BlankString.equals(StrArea)) && (StrDescription == null || GLOBALCONSTANT.BlankString.equals(StrDescription)) && (StrLocationType == null || GLOBALCONSTANT.BlankString.equals(StrLocationType))){
		
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_QueryDisplayAll_New);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			lOCObjects = OBJ_All_New.list();
		}
		else if(!GLOBALCONSTANT.BlankString.equals(StrLocation)){
			String  Location_QueryDisplayAll_Location = GLOBALCONSTANT.Location_QueryDisplayAll_Location_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_QueryDisplayAll_Location_Second;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_QueryDisplayAll_Location);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrLocation.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		else if(!GLOBALCONSTANT.BlankString.equals(StrArea)){
			String  Location_QueryDisplayAll_Area = GLOBALCONSTANT.Location_QueryDisplayAll_Area_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_QueryDisplayAll_Area_Second;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_QueryDisplayAll_Area);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrArea.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		else if(!GLOBALCONSTANT.BlankString.equals(StrDescription)){
			String  Location_QueryDisplayAll_Description = GLOBALCONSTANT.Location_QueryDisplayAll_Description_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_QueryDisplayAll_Description_Second;
			String DescriptionValue=GLOBALCONSTANT.StrPercentSign+StrDescription.toUpperCase().trim()+GLOBALCONSTANT.StrPercentSign;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_QueryDisplayAll_Description);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, DescriptionValue);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, DescriptionValue);
			lOCObjects = OBJ_All_New.list();	
		}
		else if(!GLOBALCONSTANT.BlankString.equals(StrLocationType)){
			String  Location_QueryDisplayAll_LocationType = GLOBALCONSTANT.Location_QueryDisplayAll_LocationType_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_QueryDisplayAll_LocationType_Second;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_QueryDisplayAll_LocationType);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrLocationType.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
			return lOCObjects;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return lOCObjects;
		}		
	}

	/**	
	  * @author Aakash Bishnoi
	  * @Date Jan 5, 2015
	  * Description:It is used to Fetch the record form Location table on the behalf of parameters
	  *@param StrTenant
	  *@param StrCompany
	  *@param StrFulfillmentCenter
	  *@param StrArea
	  *@param StrLocation
	  *@return LOCATIONBEAN
	  *@throws JASCIEXCEPTION
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public LOCATION getFetchLocation(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrArea,String StrLocation){
		LOCATION ObjectLocationBean=null;
		try{	

			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameUniqueLocationCheck);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrArea.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrLocation.toUpperCase().trim());

			List<LOCATION> FetchLocationList = ObjQuery.list();
				
			ObjectLocationBean= FetchLocationList.get(GLOBALCONSTANT.INT_ZERO);
			return ObjectLocationBean;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return ObjectLocationBean;
		}		
	}
	
	/**
	 * 
	  * @author Aakash Bishnoi
	  * @Date Jan 6, 2015
	 * Description:Thisis used to  fetch data from INVENTORY_LEVELS table on the behalf of parameters
	  *@param StrTenant
	  *@param StrCompany
	  *@param StrFulfillmentCenter
	  *@param StrArea
	  *@param StrLocation
	  *@return
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public INVENTORY_LEVELS getLocationExistForDelete(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrArea,String StrLocation){
		INVENTORY_LEVELS ObjectLocationBean=new INVENTORY_LEVELS();
		int quantityvalue = GLOBALCONSTANT.INT_MINUSONE;
		try{	
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameLocationExistForDelete);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrArea.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrLocation.toUpperCase().trim());

			List<INVENTORY_LEVELS> InventoryList = ObjQuery.list();
				
			 if(!InventoryList.isEmpty())
			 {
				ObjectLocationBean = InventoryList.get(GLOBALCONSTANT.INT_ZERO);
			 }
			 else{
				 ObjectLocationBean.setQuantity(quantityvalue);
			 }
				return ObjectLocationBean;						
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return ObjectLocationBean;
		}		
	}
	
	/**
	 * Created on:Jan 05 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute query to get FulfillmentCenter.
	 * Input parameter: String
	 * Return Type :List<Object>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getfulFillmentCenter(String tenant,String company,String PageName) throws JASCIEXCEPTION 
	{
		
		List<Object> objListFulfillmentcenterspojos=null;
		try{
			if(GLOBALCONSTANT.Copy.equalsIgnoreCase(PageName))
			{
			objListFulfillmentcenterspojos=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.LocationProfile_FulFillmentCenter_Query).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).list();			
			}
			else{
			objListFulfillmentcenterspojos=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.LocationProfile_FulFillmentCenter_Query_For_Edit).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).list();
			}
			
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException)
		{
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}
		return objListFulfillmentcenterspojos;
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 16, 2015
	 * Description:This is used to delete the record in location table
	 * @param tenant
	 * @param company
	 * @param profile
	 * @param locationprofileNotesid
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strfinally)
	public Boolean deleteEntry(String StrTenant_ID,String StrCompany_ID,String StrFetchFulfillment_Center,String StrFetchArea,String StrFetchLocation) throws JASCIEXCEPTION{
		Boolean Result=false;
		try
		{
			LOCATIONPK objLocationPK=new LOCATIONPK();
			objLocationPK.setTenant_ID(StrTenant_ID);
			objLocationPK.setCompany_ID(StrCompany_ID);
			objLocationPK.setArea(StrFetchArea);
			objLocationPK.setLocation(StrFetchLocation);
			objLocationPK.setFullfillment_Center_ID(StrFetchFulfillment_Center);

			try{
				LOCATION ObjectLocation = (LOCATION) sessionFactory.getCurrentSession().load(LOCATION.class, objLocationPK);
				sessionFactory.getCurrentSession().delete(ObjectLocation);			
				Result=true;
			}catch(Exception Ex){
				Result=false;
			}

		}

		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(ObjException.getMessage());
		}
		finally{
			return Result;
		}
	}
	/**
	 * @author Aakash Bishnoi
	 * @Date Jan 16, 2015
	 * Description:This is used to get the record of notes
	 * @param tenant
	 * @param company
	 * @param profile
	 * @param locationprofileNotesid
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
		public List<NOTES> GetNotes(String tenant, String company, String location, String locationNotesid) throws JASCIEXCEPTION {
			  List<NOTES> objListNotes=null;
			  try{
			   objListNotes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Location_Notes_Query).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO,location.toUpperCase().trim()).setParameter(GLOBALCONSTANT.INT_THREE,locationNotesid.toUpperCase().trim()).list();
			  }catch (SQLGrammarException objGrammarException){
					throw new JASCIEXCEPTION(objGrammarException.getMessage());
				}
			  catch(Exception ObjectException)
			  {
			   
			   throw new JASCIEXCEPTION(ObjectException.getMessage());
			  }
			  return objListNotes;
			 }
	
}
