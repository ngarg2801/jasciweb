/*
Description it is a interface where we define the function that implement in class of GeneralCodeDaoImplement
Created By Aakash Bishnoi 
Created Date Oct 20 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.util.*;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.exception.JASCIEXCEPTION;

public interface IGENERALCODEDAO {
//This function is used to insert data in General_Code table and menu option table
 public String addEntry(GENERALCODES ObjectGeneralCodes,MENUOPTIONS ObjMenuOprions) throws JASCIEXCEPTION;
 
//This function is used to insert data using generalcodeid only in general_codes table
 public String addGeneralCodeIdEntry(GENERALCODES ObjectGeneralCodes)  throws JASCIEXCEPTION;

//delete using kendo ui
 public Boolean deleteEntry(String tenant,String company,String application,String generalcodeid,String generalcode) throws JASCIEXCEPTION;
 
//delete using kendo ui GeneralCodeID
public Boolean deleteGeneralCodeID(String tenant,String company,String application,String generalcodeid,String generalcode) throws JASCIEXCEPTION;
 
 //This function is used to get the list of General_Code table
 public List<GENERALCODES> getList(String StrTenant,String StrCompany)throws JASCIEXCEPTION;
 
 
//This function is used to get the data for generalcodes and generalcodesid screen behalf of composite key
 public GENERALCODES getEntry(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode) throws JASCIEXCEPTION;

//This function is used to update record in General_Code table and menu option for generalcode and GENERAL_CODE for generalcodeid screen
 public String updateEntry(GENERALCODES ObjectGeneralCodes,MENUOPTIONS ObjMenuOprions)throws JASCIEXCEPTION;

	//It is used for Update data in GENERAL_CODES table for the screen GeneralCOdeID (it is used in updateEntry function on controller 
public String updateGeneralCodeID(GENERALCODES ObjectGeneralCodes) throws JASCIEXCEPTION;

 
 //This function is used to get data 
 public List<LANGUAGES> GetLanguage(String StrLanguage)throws JASCIEXCEPTION;
 
//This is used to set label of GeneralCodeEditUpdate Screen
 public  List<LANGUAGES> SetGeneralCodesBe(String StrLanguage)throws JASCIEXCEPTION;
 
/* //Bind data with Application drop down in generalcode
 public List SelectApplication()throws JASCIEXCEPTION;
 
 //Bind data with GeneralCode drop down in generalcode
 public List SelectGeneralCode()throws JASCIEXCEPTION;*/
 
//Add new record in GeneralCodeSublist
 public String addGeneralCode(String Tenant,String Company,String Application,String GeneralCode,String GeneralCodeID,GENERALCODES ObjectGeneralCodes) throws JASCIEXCEPTION;
 
 //Get the Record on edit screen behalf of composite key
 public GENERALCODES getGeneralCode(String Tenant,String Company,String Application,String GeneralCode,String GeneralCodeID) throws JASCIEXCEPTION;

 //update the record in new general code screen
 public String updateGeneralCode(GENERALCODES ObjectGeneralCodes) throws JASCIEXCEPTION;
 
// get the first list of general code screen
 public List<GENERALCODES> getMainGeneralCodeList(String Tenant,String Company,String GeneralCode)throws JASCIEXCEPTION;

 //get the sub list of general code screen behalf og generalcodeid
 public List<GENERALCODES> getSubGeneralCodeList(String Tenant,String Company,String GeneralCode)throws JASCIEXCEPTION;

 /**
 * @author Aakash Bishnoi
 * @Date Feb 16, 2015
 * Description:It is used to check uniqueness of Description20 in GeneralCodes TABLE before add record 
 * @param Tenant
 * @param Company
 * @param Application
 * @param GeneralCodeID
 * @param GeneralCode
 * @return Boolean
 * @throws JASCIEXCEPTION
 */
 public List<GENERALCODES> getDescription20Unique(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode,String StrDescription20,String StrScreenName) throws JASCIEXCEPTION;


 /**
  * @author Aakash Bishnoi
  * @Date Feb 16, 2015
  * Description:It is used to check uniqueness of Description50 in GeneralCodes TABLE before add record 
  * @param Tenant
  * @param Company
  * @param Application
  * @param GeneralCodeID
  * @param GeneralCode
  * @return Boolean
  * @throws JASCIEXCEPTION
  */
  public List<GENERALCODES> getDescription50Unique(String Tenant,String Company,String Application,String GeneralCodeID,String GeneralCode,String StrDescription50,String StrScreenName) throws JASCIEXCEPTION;

	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of GeneralCode in GeneralCodes TABLE before add record 
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrApplication
	 * @param StrGeneralCodeID
	 * @param StrGeneralCode
	 * @return List<GENERALCODES>
	 * @throws JASCIEXCEPTION
	 */
	public List<GENERALCODES> getGeneralCodeUnique(String StrTenant,String StrCompany,String StrApplication,String StrGeneralCodeID,String StrGeneralCode) throws JASCIEXCEPTION;
		
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description20 in GeneralCodes TABLE before add record  
	 * @param StrMenuName
	 * @return List<MENUOPTIONSBE>
	 * @throws JASCIEXCEPTION
	 */
	public List<MENUOPTIONS> getMenuNameUnique(String StrMenuName) throws JASCIEXCEPTION;
		
}


