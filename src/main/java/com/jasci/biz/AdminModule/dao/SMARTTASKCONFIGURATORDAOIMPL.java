/**

Description This class provide all of the functionality related to database for Smart Task Configurator screen 
Created By Shailendra Rajput  
Created Date May 15 2015
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONSPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.WORKFLOWSTEPS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class SMARTTASKCONFIGURATORDAOIMPL implements ISMARTTASKCONFIGURATORDAO {

	/** Create the instance of SessionFactory.*/
	@Autowired
	private SessionFactory sessionFactory;
	
	/** Make a common session object for get the value of tenant,company,team member etc.*/
	@Autowired
	private COMMONSESSIONBE OBJCOMMONSESSIONBE;
	

	/**
	 * @author Shailendra Rajput
	 * @Date May 15, 2015
	 * @Description:This is used to show All Data from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen for DisplayAll.
	 *@param StrTenant
	 *@param StrCompany
	 *@return List
	 */
	
	@SuppressWarnings({GLOBALCONSTANT.Strrawtypes,GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	public List getDisplayAll(String StrTenant,String StrCompany){
	
		/** Make the instance of List of Object*/
		List<Object> lOCObjects = null;
	    
		try{

			/**It is used get the all data form SMART_TASK_SEQ_HEADERS table*/
			if(OBJCOMMONSESSIONBE.getJasci_Tenant().equalsIgnoreCase(OBJCOMMONSESSIONBE.getTenant())){
				Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_DisplayAll_Tenant);
				lOCObjects = OBJ_All_New.list();
			}
			else{
			
				Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_DisplayAll);
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
				lOCObjects = OBJ_All_New.list();
			}
			
			
				return lOCObjects;
			}catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
			catch(Exception ObjectException){
				throw new JASCIEXCEPTION(ObjectException.getMessage());
			}finally{
				return lOCObjects;
			}		
		}
	/**
	 * @author Shailendra Rajput
	 * @Date May 15, 2015
	 * @Description:This is used to search the list from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
	 *@param StrTenant
	 *@param StrTask
	 *@param StrApplication
	 *@param StrExecutionGroup
	 *@param StrExecutionType
	 *@param StrExecutionDevice
	 *@param StrExecution
	 *@param StrDescription
	 *@param StrMenuName
	 *@return List
	 */
	@SuppressWarnings({GLOBALCONSTANT.Strrawtypes,GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	public List getList(String StrTenant,String StrCompany,String StrTask,String StrApplication,String StrExecutionGroup,String StrExecutionType,String StrExecutionDevice,String StrExecutionSequence,String StrDescription,String StrMenuName){
	
		List<Object> lOCObjects = null;
	    
		try{
		
			if(OBJCOMMONSESSIONBE.getJasci_Tenant().equalsIgnoreCase(OBJCOMMONSESSIONBE.getTenant())){
				
				if(!GLOBALCONSTANT.BlankString.equals(StrApplication)){
					
					/**It is used get the Application data form SMART_TASK_SEQ_HEADERS table*/
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Application_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrApplication.toUpperCase().trim());
					lOCObjects = OBJ_All_New.list();	
				}
				
				else if(!GLOBALCONSTANT.BlankString.equals(StrTask)){
					
					/**It is used get the Task data form SMART_TASK_SEQ_HEADERS table*/
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Task_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTask.toUpperCase().trim());
					lOCObjects = OBJ_All_New.list();	
				}
				
				else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionGroup)){
					
					/**It is used get the Execution_Group data form SMART_TASK_SEQ_HEADERS table*/
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Group_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrExecutionGroup.toUpperCase().trim());
					lOCObjects = OBJ_All_New.list();	
				}
				
				else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionType)){
					
					/**It is used get the Execution Type data form SMART_TASK_SEQ_HEADERS table*/
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Type_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrExecutionType.toUpperCase().trim());
					lOCObjects = OBJ_All_New.list();	
				}
				
				else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionDevice)){
					
					/**It is used get the Execution Device data form SMART_TASK_SEQ_HEADERS table*/
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Device_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrExecutionDevice.toUpperCase().trim());
					lOCObjects = OBJ_All_New.list();	
				}
				
				else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionSequence)){
					
					/**It is used get the Execution sequence Name data form SMART_TASK_SEQ_HEADERS table*/
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Sequence_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrExecutionSequence.toUpperCase().trim());
					lOCObjects = OBJ_All_New.list();	
				}
				
				else if(!GLOBALCONSTANT.BlankString.equals(StrDescription)){
					
					/**It is used get the Decription20 or Description50 data form SMART_TASK_SEQ_HEADERS table*/
					String DescriptionValue=GLOBALCONSTANT.StrPercentSign+StrDescription.toUpperCase().trim()+GLOBALCONSTANT.StrPercentSign;
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Description_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, DescriptionValue);
					OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, DescriptionValue);
					
					lOCObjects = OBJ_All_New.list();	
				}
				
				else if(!GLOBALCONSTANT.BlankString.equals(StrMenuName)){
				
					/**It is used get the Menu Name data form SMART_TASK_SEQ_HEADERS table*/
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Menu_Name_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrMenuName.toUpperCase().trim());
					lOCObjects = OBJ_All_New.list();	
				}
				

				else if(!GLOBALCONSTANT.BlankString.equals(StrTenant)){
					
					/**It is used get the Tenant_Id data form SMART_TASK_SEQ_HEADERS table*/
					//String TenantValue=GLOBALCONSTANT.StrPercentSign+StrTenant.toUpperCase().trim()+GLOBALCONSTANT.StrPercentSign;
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Tenant_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
					
					lOCObjects = OBJ_All_New.list();	
				}
			}
			else{
			
			
		if(!GLOBALCONSTANT.BlankString.equals(StrApplication)){
		
			/**It is used get the Application data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Application);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, StrApplication.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		
		else if(!GLOBALCONSTANT.BlankString.equals(StrTask)){
			
			/**It is used get the Task data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Task);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, StrTask.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		
		else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionGroup)){
			
			/**It is used get the Execution_Group data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Group);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, StrExecutionGroup.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		
		else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionType)){
			
			/**It is used get the Execution Type data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Type);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, StrExecutionType.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		
		else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionDevice)){
			
			/**It is used get the Execution Device data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Device);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, StrExecutionDevice.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		
		else if(!GLOBALCONSTANT.BlankString.equals(StrExecutionSequence)){
			
			/**It is used get the Execution sequence Name data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Execution_Sequence);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, StrExecutionSequence.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		
		else if(!GLOBALCONSTANT.BlankString.equals(StrDescription)){
			
			/**It is used get the Decription20 or Description50 data form SMART_TASK_SEQ_HEADERS table*/
			String DescriptionValue=GLOBALCONSTANT.StrPercentSign+StrDescription.toUpperCase().trim()+GLOBALCONSTANT.StrPercentSign;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Description);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, DescriptionValue);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIFTEEN, DescriptionValue);
			
			lOCObjects = OBJ_All_New.list();	
		}
		
		else if(!GLOBALCONSTANT.BlankString.equals(StrMenuName)){
		
			/**It is used get the Menu Name data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Menu_Name);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOURTEEN, StrMenuName.toUpperCase().trim());
			lOCObjects = OBJ_All_New.list();	
		}
		

		else if(!GLOBALCONSTANT.BlankString.equals(StrTenant)){
			
			/**It is used get the Tenant_Id data form SMART_TASK_SEQ_HEADERS table*/
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Tenant);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrTenant.toUpperCase().trim());
			
			lOCObjects = OBJ_All_New.list();	
		}
	}	
			return lOCObjects;
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return lOCObjects;
		}		
	}	
				
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:it is used to check Execution_Sequence_Name assigned in table WORK_FLOW_STEPS
	 * @param StrTenant_Id
	 * @param Company_Id
	 * @param StrExecution_Sequence_Name
	 * @return List<WORKFLOWSTEPS>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	public List<WORKFLOWSTEPS> getExistExecutionSequenceName(String StrTenant_Id,String StrCompany_Id,String StrExecution_Sequence_Name) throws JASCIEXCEPTION{
	
		/** Create the Instance of List<WORKFLOWSTEPS>*/
		List<WORKFLOWSTEPS> UniqueExecutionsSequenceNameList=null;
		try{	

			/** Query for get the Execution Sequence Name Exist in table WORKFLOWSTEPS or not*/
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_ExistExecutionSequenceName);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase().trim());
			
			UniqueExecutionsSequenceNameList = ObjQuery.list();

			return UniqueExecutionsSequenceNameList;
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return UniqueExecutionsSequenceNameList;
		}
	}
	
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:It  is used to delete the record from SMART_TASK_SEQUENCE_HEADERS,SMART_TASK_SEQUENCE_INSTRUCTIONS, MENU_OPTIONS and MENU_PROFILE table
	 * @param StrTenant_Id
	 * @param Company_Id
	 * @param StrApplication_Id
	 * @param StrExecution_Sequence_Group
	 * @param StrExecution_Sequence_Name
	 * @param StrMenu_Name
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strfinally)
	public boolean deleteEntry(String StrTenant_Id,String Company_Id,String StrExecution_Sequence_Name,String StrMenu_Name) throws JASCIEXCEPTION{
		
		 Boolean Status=false;
		  try
		  {
		   try{
		   //It is used to delete record from MENU_PROFILE_OPTION Table..
		   Query ObjQueryMenuProfileOption=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteMenuProfileOption);
		   ObjQueryMenuProfileOption.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQueryMenuProfileOption.setString(GLOBALCONSTANT.IntOne, StrMenu_Name.toUpperCase());
		   ObjQueryMenuProfileOption.executeUpdate();
		   
		   //It is used to delete record from MENU_OPTION Table..
		   Query ObjQueryMenuOption=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteMenuOption);
		   ObjQueryMenuOption.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQueryMenuOption.setString(GLOBALCONSTANT.IntOne, StrMenu_Name.toUpperCase());
		   ObjQueryMenuOption.executeUpdate();
		   
		   //It is used to delete record from SMART_TASK_SEQ_INSTRUCTIONS Table..
		   Query ObjQuerySmartTaskSeqInstructions=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteSmartTaskSeqInstruction);
		   ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.IntOne, Company_Id.toUpperCase());
		   ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase());
		   ObjQuerySmartTaskSeqInstructions.executeUpdate();
		   
		 //It is used to delete record from SMART_TASK_SEQ_HEADERS Table..
		   Query ObjQuerySmartTaskSeqHeaders=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteSmartTaskSeqHeaders);
		   ObjQuerySmartTaskSeqHeaders.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQuerySmartTaskSeqHeaders.setString(GLOBALCONSTANT.IntOne, Company_Id.toUpperCase());
		   ObjQuerySmartTaskSeqHeaders.setString(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase());
		   ObjQuerySmartTaskSeqHeaders.executeUpdate();
		   
		   Status=true;
		   }catch(Exception Ex){
		    Status=false;
		   }
		  
		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
		  
		  catch(Exception ObjException)
		  {
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  finally{
		   return Status;
		  }
		   
		   
	}


	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_SEQUENCE_HEADERS table on the behalf of Tenant_Id,Company_Id,Execution_Sequence_Name.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Sequence_Name
	 * @return List<SMARTTASKSEQHEADERS>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	public SMARTTASKSEQHEADERS getSTSheadersData(String StrTenant_Id,String StrCompany_Id,String StrExecution_Sequence_Name){
		SMARTTASKSEQHEADERS ObjSmartTaskHeaders=null;
		
	    
		try{

			
			 
				/**It is used get the data form SMART_TASK_SEQ_HEADERS table*/
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskSeqHeaders);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase().trim());
				List<SMARTTASKSEQHEADERS> ObjSmartTaskHeaderslist = ObjQuery.list();
				ObjSmartTaskHeaders= ObjSmartTaskHeaderslist.get(GLOBALCONSTANT.INT_ZERO);
				
				return ObjSmartTaskHeaders;
			
	}catch(SQLGrammarException objException){

		throw new JASCIEXCEPTION(objException.getMessage());

	}
		 catch(Exception ObjException)
		  {
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  finally{
		   return ObjSmartTaskHeaders;
		  }	
	}
	
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_EXECUTIONS table on the behalf of Parameters.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Group
	 * @param StrExecution_Type
	 * @param StrExecution_Device
	 * @return List<SMARTTASKEXECUTIONS>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	 public List<SMARTTASKEXECUTIONS> getSmartTaskExecutionData(String StrTenant_Id,String StrCompany_Id,String StrExecution_Group,String StrExecution_Type,String StrExecution_Device){
	  
	  List<SMARTTASKEXECUTIONS> ObjSmartTaskHeaders = null;
	     
	  try{

	   
	    
		  /**It is used get the data form SMART_TASK_EXECUTIONS table*/
		   if(GLOBALCONSTANT.BlankString.equals(StrTenant_Id) && GLOBALCONSTANT.BlankString.equals(StrCompany_Id) && GLOBALCONSTANT.BlankString.equals(StrExecution_Device ) && GLOBALCONSTANT.BlankString.equals(StrExecution_Type ) && GLOBALCONSTANT.BlankString.equals(StrExecution_Group )) 
		   { 
		    Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecutionData);
		    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, OBJCOMMONSESSIONBE.getTenant().toUpperCase().trim());
		    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, OBJCOMMONSESSIONBE.getCompany().toUpperCase().trim());
		    ObjSmartTaskHeaders = ObjQuery.list();
		   }
		   else if(!GLOBALCONSTANT.BlankString.equals(StrTenant_Id) && !GLOBALCONSTANT.BlankString.equals(StrCompany_Id) && !GLOBALCONSTANT.BlankString.equals(StrExecution_Device ) && !GLOBALCONSTANT.BlankString.equals(StrExecution_Type ) && !GLOBALCONSTANT.BlankString.equals(StrExecution_Group )) 
		   {
				   if(GLOBALCONSTANT.BACKEND.equalsIgnoreCase(StrExecution_Type.toUpperCase())){
					   Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution_BeckendOnly);
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase().trim());
					    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase().trim());
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());				   
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrExecution_Type.toUpperCase().trim());
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Device.toUpperCase().trim());		
					    ObjSmartTaskHeaders = ObjQuery.list();
				   }
				   else{
			    Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution);
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, GLOBALCONSTANT.BACKEND);
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Type.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_FIVE, StrExecution_Device.toUpperCase().trim());
			    ObjSmartTaskHeaders = ObjQuery.list();
				   }
		   }
		   else{
			   
			   if(GLOBALCONSTANT.BACKEND.equalsIgnoreCase(StrExecution_Type.toUpperCase())){
				   Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution_BeckendOnly);
				   ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO,  OBJCOMMONSESSIONBE.getTenant().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.IntOne,  OBJCOMMONSESSIONBE.getCompany().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());				   
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrExecution_Type.toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Device.toUpperCase().trim());		
				    ObjSmartTaskHeaders = ObjQuery.list();
			   }
			   else{
				   Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution);
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO,  OBJCOMMONSESSIONBE.getTenant().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, OBJCOMMONSESSIONBE.getCompany().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, GLOBALCONSTANT.BACKEND);
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Type.toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_FIVE, StrExecution_Device.toUpperCase().trim());
				    ObjSmartTaskHeaders = ObjQuery.list();
			   }
		   }
		    
		    
		    return ObjSmartTaskHeaders;
	   
	 }catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
	   catch(Exception ObjException)
	    {
	     throw new JASCIEXCEPTION(ObjException.getMessage());
	    }
	    finally{
	     return ObjSmartTaskHeaders;
	    } 
	 }
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from GENERAL_CODES table on the behalf of Parameters for DropDown.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrGeneral_Code_Id
	 * @return List<Object>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getGeneralCodeDropDown(String StrTenant_Id, String StrCompany_Id, String StrGeneral_Code_Id) throws JASCIEXCEPTION
	{

		
		try{
			
			return sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.GENERAL_CODES_DROPDOWN_HELPLINE)
					.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, StrGeneral_Code_Id.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		  {
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  	
	}


	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_SEQ_INSTRUCTIONS table on the behalf of Parameters for Sequence View.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Sequence_Name
	 * @return List<SMARTTASKSEQINSTRUCTIONS>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<SMARTTASKSEQINSTRUCTIONS> getSequenceViewData(String StrTenant_Id,String StrCompany_Id,String StrExecution_Sequence_Name) throws JASCIEXCEPTION
	{

		List<SMARTTASKSEQINSTRUCTIONS> ObjSmartTaskSeqInstructions = null;
		
		
		try{
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameGetSequenceView);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase().trim());
		
			ObjSmartTaskSeqInstructions = ObjQuery.list();
			return ObjSmartTaskSeqInstructions;
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		  {
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  	
	}


	/**
	  * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Add Record in SMART_TASK_SEQ_HEADERS Table From SmartTaskConfigurator Maintenance Sreen
	
	 * @param ObjectSTSHeadersBean
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String addEntrySTSH(SMARTTASKSEQHEADERS ObjectSTSHeaders) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database

			sessionFactory.getCurrentSession().saveOrUpdate(ObjectSTSHeaders);	
			Status=GLOBALCONSTANT.StatusTrue;

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){

			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}
	
	/**
	  * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Add Record in SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectSTSInstructionsBean
	
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String addEntrySTSI(SMARTTASKSEQINSTRUCTIONS ObjectSTSInstructions) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database

			sessionFactory.getCurrentSession().saveOrUpdate(ObjectSTSInstructions);	
			Status=GLOBALCONSTANT.StatusTrue;

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){

			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}
	
	/**
	  * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Add Record in MENU_OPTIONS Table From SmartTaskConfigurator Maintenance Sreen
	
	 * @param ObjectMenuOption
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String addEntrySTCMO(MENUOPTIONS ObjectMenuOption) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database
			sessionFactory.getCurrentSession().saveOrUpdate(ObjectMenuOption);	
			Status=GLOBALCONSTANT.StatusTrue;

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception ObjectException){

			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}
	
	/**
	  * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Update Record in MENU_OPTIONS Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectMenuOption
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String updateEntrySTCMO(MENUOPTIONS ObjectMenuOption) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database
			sessionFactory.getCurrentSession().saveOrUpdate(ObjectMenuOption);	
			Status=GLOBALCONSTANT.StatusTrue;

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception ObjectException){

			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}
	
	/**
	  * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Update Record in SMART_TASK_SEQ_HEADERS Table From SmartTaskConfigurator Maintenance Sreen
	 
	 * @param ObjectSTSHeadersBean
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String updateEntrySTSH(SMARTTASKSEQHEADERS ObjectSTSHeaders) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database

			sessionFactory.getCurrentSession().saveOrUpdate(ObjectSTSHeaders);	
		
			Status=GLOBALCONSTANT.StatusTrue;

		}catch(HibernateException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception ObjectException){

			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}
	
	/**
	  * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is used to Update Record in SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectSTSInstructionsBean
	
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String updateEntrySTSI(SMARTTASKSEQINSTRUCTIONS ObjectSTSInstructions) throws JASCIEXCEPTION{
		String Status=GLOBALCONSTANT.StatusFlase;
		try{
			//This is a Hibernate Function use to save record in database

				
			sessionFactory.getCurrentSession().saveOrUpdate(ObjectSTSInstructions);	
			Status=GLOBALCONSTANT.StatusTrue;

		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){

			throw new JASCIEXCEPTION(ObjectException.getMessage());			
		}		
		return Status;
	}
	
	/**
	 * @Description get team member name form team members table based on team member id
	 * @param Tenant
	 * @param Company
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	 @SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
		 
		  
		  List<TEAMMEMBERS> objTeammembers=null;
		  try
		  {
		   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
		  catch(Exception ObjectException)
		  {
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		   
		   
		  }
		  
		  if(objTeammembers.size()==GLOBALCONSTANT.INT_ZERO){
		  try
		  {
		   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query_By_TEAMID).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.trim().toUpperCase()).list();
		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
		  catch(Exception ObjectException)
		  {
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		   
		   
		  }
		 }
		  return objTeammembers;
		 }
	 
	 /**
		 * 
		 * @author Shailendra Rajput
		 * @Date May 19, 2015
		 * @Description:it is used to check Execution_Sequence_Name assigned in table WORK_FLOW_STEPS
		 * @param StrTenant_Id
		 * @param Company_Id
		 * @param StrExecution_Sequence_Name
		 * @return List<WORKFLOWSTEPS>
		 * @throws JASCIEXCEPTION
		 */
		@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
		public List<SMARTTASKSEQHEADERS> getExistExecutionSequenceNameinSTSH(String StrTenant_Id,String StrCompany_Id,String StrExecution_Sequence_Name) throws JASCIEXCEPTION{
		
			/** Create the Instance of List<WORKFLOWSTEPS>*/
			List<SMARTTASKSEQHEADERS> UniqueExecutionsSequenceNameList=null;
			try{	

				/** Query for get the Execution Sequence Name Exist in table WORKFLOWSTEPS or not*/
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_ExistExecutionSequenceNameSTSH);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase().trim());
				
				UniqueExecutionsSequenceNameList = ObjQuery.list();

				return UniqueExecutionsSequenceNameList;
			}catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
			catch(Exception ObjectException){
				throw new JASCIEXCEPTION(ObjectException.getMessage());
			}finally{
				return UniqueExecutionsSequenceNameList;
			}
		}
		
		/**
		 * 
		 * @author Shailendra Rajput
		 * @Date july 18, 2015
		 * @Description:it is used to check MenuOption is exist in table MENUOPTIONS
		 * @param StrMenu_Option
		 * @return List<MENUOPTIONS>
		 * @throws JASCIEXCEPTION
		 */
		@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
		public List<MENUOPTIONS> getExistMenuOptioninSTSH(String StrMenu_Option) throws JASCIEXCEPTION{
		
			/** Create the Instance of List<MENUOPTIONS>*/
			List<MENUOPTIONS> UniqueMenuOptionList=null;
			try{	
				

				/** Query for get the menu option Exist in table MENUOPTIONS or not*/
				Query Obj_MenuName = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_ExistMenuOptionSTSH);
				Obj_MenuName.setParameter(GLOBALCONSTANT.INT_ZERO, StrMenu_Option.toUpperCase().trim());
				UniqueMenuOptionList = Obj_MenuName.list();

				return UniqueMenuOptionList;
			}
			catch(Exception ObjectException){
				throw new JASCIEXCEPTION(ObjectException.getMessage());
			}finally{
				return UniqueMenuOptionList;
			}
		}
		public void deleteEntrySTSI(SMARTTASKSEQINSTRUCTIONSPK oBJSmarttaskseqinstructionspk) throws JASCIEXCEPTION
		{
			//sessionFactory.getCurrentSession().delete(oBJSmarttaskseqinstructionspk);
			
			try{
			 Query ObjQuerySmartTaskSeqInstructions=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteSmartTaskSeqInstruction);
		      ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.INT_ZERO, oBJSmarttaskseqinstructionspk.getTenant_Id().toUpperCase());
		      ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.IntOne, oBJSmarttaskseqinstructionspk.getCompany_Id().toUpperCase());
		      ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.INT_TWO, oBJSmarttaskseqinstructionspk.getExecution_Sequence_Name().toUpperCase());
		      ObjQuerySmartTaskSeqInstructions.executeUpdate();
			}catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
			catch(Exception objex)
			{
				throw new JASCIEXCEPTION(objex.getMessage());
			}		
			
		}
		public List<SMARTTASKEXECUTIONS> getExecutionData(String StrExecution_Name) throws JASCIEXCEPTION {
			
			Query objquery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecutionDescriotions);
			objquery.setParameter(GLOBALCONSTANT.INT_ZERO, StrExecution_Name.toUpperCase());
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			List<SMARTTASKEXECUTIONS> objSmarttaskexecutions = objquery.list();
			return objSmarttaskexecutions;
		}
		
		 /**
			 * 
			 * @author Shailendra Rajput
			 * @Date September 16, 2015
			 * @Description:it is used to check Custom Execution name is valid or not
			 * @param CustomExecutionPath
			 * @return boolean
			 * @throws JASCIEXCEPTION
			 */
			@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
			public boolean validateCustomExecution(String CustomExecutionPath) throws JASCIEXCEPTION{
			
				/** Create the Instance of List<WORKFLOWSTEPS>*/
				List<SMARTTASKEXECUTIONS> ValidCustomExecutionList=null;
				boolean Status = false;
				try{	

					/** Query for get the Execution Sequence Name Exist in table WORKFLOWSTEPS or not*/
					Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_ValidCustomExecution);
					ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, CustomExecutionPath.toUpperCase());
					
					ValidCustomExecutionList = ObjQuery.list();
					
					if(ValidCustomExecutionList.size()>0)
					{
						Status = true;
					}else{
						Status = false;
					}
				}catch(SQLGrammarException objException){

					throw new JASCIEXCEPTION(objException.getMessage());

				}
				catch(Exception ObjectException){
					throw new JASCIEXCEPTION(ObjectException.getMessage());
				}finally{
					return Status;
				}
			}
}
