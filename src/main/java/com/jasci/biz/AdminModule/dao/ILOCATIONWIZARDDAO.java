/**

Description This interface use for dao  for Location Wizard screen 
Created By Pradeep Kumar  
Created Date Jan 05 2015
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.INVENTORY_LEVELS;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONPK;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.LOCATIONREPLENISHMENT;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARD;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARDPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILOCATIONWIZARDDAO {
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<LOCATIONWIZARD>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 6, 2015
		 * @Description :Used for Search By wizard control number
	 */
	public List<Object> getSearchBYWizardControlNumber(String wizardID,String wizardControlNumber) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<Object>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 9, 2015
		 * @Description :Used for Search By Area
	 */
	public List<Object> getSearchByArea(String wizardID,String Area) throws JASCIEXCEPTION;
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<Object>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 9, 2015
		 * @Description :Used for Search By Location Type
	 */

	public List<Object> getSearchByLocationType(String wizardID,String locationType) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<Object>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 12, 2015
		 * @Description :Used for Search By AnyPartOfDiscription
	 */
	
	public List<Object> getSearchByAnyPartofWizardDiscription(String wizardID,String anyPartofWizardDiscription) throws JASCIEXCEPTION;
	
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<GENERALCODES>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 7, 2015
		 * @Description :Used for dropdown
	 */
	
	
	public List<GENERALCODES> getDropDownValuesList(String GeneralCodeId) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<FULFILLMENTCENTERSPOJO>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 8, 2015
		 * @Description :used for dropdown fulfillment
	 */
	
	public List<FULFILLMENTCENTERSPOJO> getDropDownValueFulfillmentList() throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<LOCATIONPROFILES>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 13, 2015
		 * @Description :getlist location profile
	 */
	public List<LOCATIONPROFILES> getDropDownValueLocationProfile(String fulfillmentCenter) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<LOCATIONWIZARD>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 12, 2015
		 * @Description :GetData on Grid
	 */
	public List<Object> getLocationWizardData(String wizardID) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return void
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 13, 2015
		 * @Description :used for save and update
	 */
	public LOCATIONWIZARD saveAndUpdate(LOCATIONWIZARD objLocationWizard,boolean saveorupdate) throws JASCIEXCEPTION;
	
	/**
	 * @return List
	 * @param locationList,objLocationWizard
	 * @throws JASCIEXCEPTION
	 */
	public List<Object> saveLocations(List<String> locationList,LOCATIONWIZARD objLocationwizard,List<Object> combineList,String DuplicateLocationFileName) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List<LOCATIONWIZARD>
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 14, 2015
		 * @Description :getdata for new record
	 */
	
	public List<LOCATIONWIZARD> getLocationWizardCopyEdit(String wizardid,String wizardControlNo,String fulfillment,String tenantId,String companyId,String copyOrEdit) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :List<String>
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 14, 2015
			 * @Description :get Max wizard control number
	 */
	public LOCATIONWIZARD getMaxWizardControlNumber(LOCATIONWIZARD objLocationWizard) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :boolean
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 14, 2015
			 * @Description :check for delete wizard
	 */
	public List<LOCATION> checkLocationforDeletion(String wizardid,String wizardControlNo,String fulfillment,String tenantId,String companyId) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :boolean
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 15, 2015
			 * @Description :deletion wizard only
	 */
	public boolean deleteWizardOnly(LOCATIONWIZARDPK objLocationWizard) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :List<INVENTORY_LEVELS>
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 15, 2015
			 * @Description :check inventory level list
	 */
	public List<INVENTORY_LEVELS> checkListInventryLevels(LOCATIONPK objLocation) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :boolean
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 16, 2015
			 * @Description :delete wizard with location
	 */
	public boolean deleteWizardLocation(LOCATIONWIZARD objLocationwizard) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :boolean
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 16, 2015
			 * @Description :deleteWizardAndLocation
	 */
	public boolean deleteWizardAndLocation(String wizardid,String wizardControlNo,String fulfillmentId) throws JASCIEXCEPTION;
	
	/**
	 * 
	 * @param objLocationwizard
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	
	public boolean deleteWizard(LOCATIONWIZARD objLocationwizard) throws JASCIEXCEPTION;
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :boolean
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 16, 2015
			 * @Description :deleteLocationRplenishment
	 */
	public boolean deleteLocationReplenishment(LOCATIONREPLENISHMENT objLocationReplenishment) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :List<INVENTORY_LEVELS>
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 16, 2015
			 * @Description :ListInventryLevels
	 */
	public List<INVENTORY_LEVELS> ListInventryLevels(LOCATIONPK objLocation) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :boolean
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 16, 2015
			 * @Description :deleteInventoryLevel
	 */
	public boolean deleteInventoryLevel(INVENTORY_LEVELS objINVENTORY_LEVELS) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :long
			 * @throws JASCIEXCEPTION
			 * @Date : Jan 17, 2015
			 * @Description :give no. of locations
	 */
	
	public long getNumberOfLocation(LOCATIONWIZARDPK objlocationwizardpk) throws JASCIEXCEPTION;
	
	/**
	 * 
		 * 
			 * Created By:Pradeep Kumar
			 * @return :List<FULFILLMENTCENTERSPOJO>
			 * @throws :JASCIEXCEPTION
			 * @Date : Jan 21, 2015
			 * @Description : get fulfillment description
	 */
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentList(FULFILLMENTCENTERS_PK objfulfillPK) throws JASCIEXCEPTION;
	


	/**
	 * 
	 * @Description get team member name form team members table based on team member id
	 * @param Tenant
	 * @param Company
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public List<TEAMMEMBERS> getTeamMemberName(String Tenant,String TeamMember)throws JASCIEXCEPTION;
	
	/**
	 * 
	 * @param objLocationWizard
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	public boolean deleteWizardForUpdate(LOCATIONWIZARDPK objLocationWizard) throws JASCIEXCEPTION;

}
