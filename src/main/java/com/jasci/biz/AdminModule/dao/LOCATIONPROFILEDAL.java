/**

Date Developed :Dec 26 2014
Created by: Diksha Gupta
Description :LOCATIONPROFILEDAL to execute query and get data from database.
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.be.LOCATIONPROFILEBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class LOCATIONPROFILEDAL implements  ILOCATIONPROFILEDAL
{
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	/**
	 * Created on:Dec 28 2014
	 * Created by:Diksha Gupta
	 * Description: This function is get General code list based on tenant,company,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	//@Override
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId)throws  JASCIEXCEPTION 
	{

		List<GENERALCODES> objGeneralcodes=null;

		try{
			objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_Generalcode_Select_Conditional_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objGeneralcodes;

	}


	/**
	 * Created on:JAN 29 2015
	 * Created by:Diksha Gupta
	 * Description: This function is get Fulfillment center list based on tenant
	 * Input parameter: String
	 * Return Type :List<FULFILLMENTCENTERSPOJO>
	 * 
	 */
	//@Override
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentCenter(String Tenant)throws  JASCIEXCEPTION 
	{
		List<FULFILLMENTCENTERSPOJO> FulfillmentCenterList=null;
		
		try{
			
			Query ObjQueryFulFillmentCenter = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameFulfillmentCenter);
			ObjQueryFulFillmentCenter.setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.trim().toUpperCase());
			FulfillmentCenterList=ObjQueryFulFillmentCenter.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return FulfillmentCenterList;


	}

	
	/**
	 * Created on:Dec 30 2014
	 * Created by:Diksha Gupta
	 * Description: This function is get General code list based on tenant,company,generalCodeId
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)

	public List<LOCATIONPROFILES> CheckAlreadyExists(String locationProfile,String profileGroup, String Tenant, String Company,String FulfillmentCenter) throws JASCIEXCEPTION 
	{
		
		List<LOCATIONPROFILES> objListLocationProfiles=null;
		try{
			objListLocationProfiles=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_LocationProfileList_Select_Query).setParameter(GLOBALCONSTANT.INT_ZERO, locationProfile.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, Company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, FulfillmentCenter.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListLocationProfiles;
	}

	/**
	 * Created on:Dec 30 2014
	 * Created by:Diksha Gupta
	 * Description: This function is add a record for LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */
	
	public Boolean addLocationProfile(LOCATIONPROFILES objLocationProfiles)
			throws JASCIEXCEPTION 
	{
		
		Boolean Status=false;
		  try{
			  sessionFactory.getCurrentSession().saveOrUpdate(objLocationProfiles);
		    
		      Status=true;
		  }catch (HibernateException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception objException)
		  {
			  Status=false;
			  throw new JASCIEXCEPTION(objException.getMessage());

		  }
		  
		return Status;
	
	}

	/**
	 * Created on:Dec 30 2014
	 * Created by:Diksha Gupta
	 * Description: This function is edit a record for LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */
	
	public Boolean editLocationProfile(LOCATIONPROFILES objLocationProfiles)
			throws JASCIEXCEPTION 
	{
		
		Boolean Status=false;
		  try{
			  sessionFactory.getCurrentSession().update(objLocationProfiles);
		    
		      Status=true;
		  }catch (HibernateException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception objException)
		  {
			  Status=false;
			  throw new JASCIEXCEPTION(objException.getMessage());

		  }
		  
		return Status;
	
	}


	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to check whether the given LocationProfile exists .
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILES
	 * 
	 */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> CheckLocationProfile(String locationProfile, String tenant, String company, String FulfillmentCenter)
			throws JASCIEXCEPTION {

		
		String  LocationProfile_LocationProfile_Select_Conditional_Query = GLOBALCONSTANT.LocationProfile_LocationProfile_Select_Conditional_Query_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_LocationProfile_Select_Conditional_Query_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_LocationProfile_Select_Conditional_Query_Third;
		List<Object> objListLocationProfiles=null;
		try{
			
				Query QueryObject=sessionFactory.getCurrentSession().createSQLQuery(LocationProfile_LocationProfile_Select_Conditional_Query);
				QueryObject.setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TWO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_THREE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FOUR, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FIVE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SIX, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SEVEN, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_EIGHT, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_NINE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TEN, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_ELEVEN, locationProfile.toUpperCase());
				objListLocationProfiles=QueryObject.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListLocationProfiles;
	}
	
	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to check whether the given ProfileGroup exists .
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILES
	 * 
	 */

	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strrawtypes})
	public List CheckProfileGroup(String profileGroup, String tenant, String company, String FulfillmentCenter)
			throws JASCIEXCEPTION {
		String  LocationProfile_ProfileGroup_Select_Conditional_Query = GLOBALCONSTANT.LocationProfile_ProfileGroup_Select_Conditional_Query_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_ProfileGroup_Select_Conditional_Query_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_ProfileGroup_Select_Conditional_Query_Third;
		List<Object> objListLocationProfiles=null;
		try{
			
				Query QueryObject=sessionFactory.getCurrentSession().createSQLQuery(LocationProfile_ProfileGroup_Select_Conditional_Query);
				QueryObject.setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TWO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_THREE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FOUR, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FIVE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SIX, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SEVEN, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_EIGHT, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_NINE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TEN, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_ELEVEN, profileGroup.toUpperCase());
				objListLocationProfiles=QueryObject.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListLocationProfiles;
	
	}


	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to check whether the given description for LocationProfile exists .
	 * Input parameter: String
	 * Return Type :LOCATIONPROFILES
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> CheckDescription(String description, String tenant, String company, String FulfillmentCenter)
			throws JASCIEXCEPTION {
		description=GLOBALCONSTANT.StrPercentSign+description+GLOBALCONSTANT.StrPercentSign;
		String  LocationProfile_Description_Select_Conditional_Query = GLOBALCONSTANT.LocationProfile_Description_Select_Conditional_Query_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_Description_Select_Conditional_Query_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_Description_Select_Conditional_Query_Third;
		List<Object> objListLocationProfiles=null;
		try{
				Query QueryObject=sessionFactory.getCurrentSession().createSQLQuery(LocationProfile_Description_Select_Conditional_Query);
				QueryObject.setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TWO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_THREE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FOUR, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FIVE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SIX, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SEVEN, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_EIGHT, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_NINE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TEN, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_ELEVEN, description.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TWELVE, description.toUpperCase());
				objListLocationProfiles=QueryObject.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListLocationProfiles;
	}


	/**
	 * Created on:Dec 31 2014
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to delete the record for given LocationProfile and ProfileGroup.
	 * Input parameter: LOCATIONPROFILES
	 * Return Type :Boolean
	 * 
	 */


	public Boolean DeleteLocationProfile(LOCATIONPROFILES objLocationprofiles)throws JASCIEXCEPTION
	{
		  Boolean Status=false;
		  try{
			  sessionFactory.getCurrentSession().delete(objLocationprofiles);
		      Status=true;
		  }catch (SQLGrammarException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception objException)
		  {
			  Status=false;
			  throw new JASCIEXCEPTION(objException.getMessage());
		  }
		return Status;
	}


	/**
	 * Created on:Jan 2 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to get all the record for given tenant,company and FulFillmentCenter.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */


	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strrawtypes})
	public List GetLocationProfileList(String tenant, String company,String fulfillmentCenter) throws JASCIEXCEPTION 
	{
		String  LocationProfile_ShowAll_Query = GLOBALCONSTANT.LocationProfile_ShowAll_Query_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_ShowAll_Query_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.LocationProfile_ShowAll_Query_Third;
		List<Object> objListLocationProfiles=null;
		try{
			
				Query QueryObject=sessionFactory.getCurrentSession().createSQLQuery(LocationProfile_ShowAll_Query);
				QueryObject.setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TWO, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_THREE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FOUR, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_FIVE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SIX, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_SEVEN, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_EIGHT, company.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_NINE, tenant.toUpperCase());
				QueryObject.setParameter(GLOBALCONSTANT.INT_TEN, company.toUpperCase());
				objListLocationProfiles=QueryObject.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListLocationProfiles;
	
	}

	/**
	 * Created on:Jan 2 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to get  record of Reassign for given tenant,company and FulFillmentCenter,Profile,LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILES>
	 * 
	 */
	
	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strrawtypes})
	public List GetLocationProfileListReAssign(String tenant, String company,String ProfileGroup, String FulfillmentCenter,
			String LocationProfile) throws JASCIEXCEPTION 
	{
	
		List<Object> objListLocationProfiles=null;
		try{
			objListLocationProfiles=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_Reassign_Query).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, FulfillmentCenter.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, LocationProfile.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListLocationProfiles;
	
	}
	
	/**
	 * Created on:Jan 2 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to get Number Of Locations.
	 * Input parameter: String
	 * Return Type :List<Object>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> GetNumberOfLocations(String tenant, String company,String fulfillmentCenter,String LocationProfile) throws JASCIEXCEPTION 
	{
	
		List<Object> objListObject=null;
		try{
			objListObject=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.LocationProfile_Count_NumberOfLocations_QueryName).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, fulfillmentCenter.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, LocationProfile.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListObject;
	
	}


	/**
	 * Created on:Jan 2 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute the query to get Number Of Locations.
	 * Input parameter: String
	 * Return Type :List<Object>
	 * 
	 */
	
	public Boolean ReassignLocation(String tolocationProfile,String fromlocationProfile, String tenant, String company,String FulFillmentCenterID)throws JASCIEXCEPTION 
	{
		Boolean Status=false;
		int Result=GLOBALCONSTANT.INT_ZERO;
		try{
			Result=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_RassignLocationProfile).setParameter(GLOBALCONSTANT.INT_ZERO, tolocationProfile).setParameter(GLOBALCONSTANT.IntOne, fromlocationProfile.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, tenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_FOUR, FulFillmentCenterID.toUpperCase()).executeUpdate();
			
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			Status=false;
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		if(Result>GLOBALCONSTANT.INT_ZERO)
		{
			Status=true;
		}
		return Status;
	}

	/**
	 * Created on:Jan 05 2015
	 * Created by:Diksha Gupta
	 * Description: This function is to execute query to get FulfillmentCenter.
	 * Input parameter: String
	 * Return Type :List<Object>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)

	public List<Object> getfulFillmentCenter(String tenant,String company,String PageName) throws JASCIEXCEPTION 
	{
		
		List<Object> objListFulfillmentcenterspojos=null;
		try{
			objListFulfillmentcenterspojos=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.LocationProfile_FulFillmentCenter_Query).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).list();
			if(GLOBALCONSTANT.LocationCopyScreen.equalsIgnoreCase(PageName))
			{
			objListFulfillmentcenterspojos=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.LocationProfile_FulFillmentCenter_Query).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).list();			
			}
			else{
			objListFulfillmentcenterspojos=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.LocationProfile_FulFillmentCenter_Query_For_Edit).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).list();
			}
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			objException.printStackTrace();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListFulfillmentcenterspojos;
	}

	

	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			throw new JASCIEXCEPTION(objException.getMessage());
	
		}
		
		return objTeammembers;
	}

	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get list of distinct LocationProfile.
	 * Input parameter: String
	 * Return Type :List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getDistinctLocationProfile(String tenant,String company) throws JASCIEXCEPTION {

		List<Object> objListLocationProfiles=null;
		try{
			objListLocationProfiles=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.LocationProfile_DistinctLocationProfile_Query).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		
		catch(Exception objException)
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListLocationProfiles;
	}

	/**
	 * Created on:Jan 2 2015
	 * Created by:"Diksha Gupta"
	 * Description: This function is to get notes for LocationProfile.
	 * Input parameter: String
	 * Return Type :List<NOTES>
	 * @throws JASCIEXCEPTION 
	 * 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<NOTES> GetNotes(String tenant, String company, String profile,
			String locationprofileNotesid) throws JASCIEXCEPTION {
		List<NOTES> objListNotes=null;
		try{
			objListNotes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_Notes_Query).setParameter(GLOBALCONSTANT.INT_ZERO, tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO,profile.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE,locationprofileNotesid.toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException)
		{
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return objListNotes;
	}
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description20 in LOCATIONPROFILEBE TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description20
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LOCATIONPROFILEBE> getDescription20Unique(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrDescription20,String StrScreenName,String LocationProfile) throws JASCIEXCEPTION{
		List<LOCATIONPROFILEBE> UniqueDescription20List=null;
		try{	

			if(StrScreenName.equalsIgnoreCase(GLOBALCONSTANT.GeneralCodes_Edit)){
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_Description20Unique_Query_Edit);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());	
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());				
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrDescription20.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, LocationProfile.toUpperCase().trim());
				
				UniqueDescription20List = ObjQuery.list();
			}
			else{
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_Description20Unique_Query);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());		
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());				
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrDescription20.toUpperCase().trim());
				UniqueDescription20List = ObjQuery.list();
			}
			

			return UniqueDescription20List;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}		
	}
	
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Feb 16, 2015
	 * Description:It is used to check uniqueness of Description50 in LOCATIONPROFILEBE TABLE before add record 
	 * @param Tenant
	 * @param Company
	 * @param FulfillmentCenter
	 * @param Description50
	 * @param ScreenName
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LOCATIONPROFILEBE> getDescription50Unique(String StrTenant,String StrCompany,String StrFulfillmentCenter,String StrDescription50,String StrScreenName,String LocationProfile) throws JASCIEXCEPTION{
		List<LOCATIONPROFILEBE> UniqueDescription50List=null;
		try{	

			
			if(StrScreenName.equalsIgnoreCase(GLOBALCONSTANT.GeneralCodes_Edit)){
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_Description50Unique_Query_Edit);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrDescription50.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, LocationProfile.toUpperCase().trim());
				UniqueDescription50List = ObjQuery.list();
			}
			else{
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.LocationProfile_Description50Unique_Query);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrFulfillmentCenter.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrDescription50.toUpperCase().trim());
				UniqueDescription50List = ObjQuery.list();
			}
			

			return UniqueDescription50List;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}		
	}
	
}
