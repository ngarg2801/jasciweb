/**

Description it is an Interface where we can declare all of the methods that are implement in SMARTTASKCONFIGURATORDAOIMPL
Created By Shailendra Rajput  
Created Date May 15, 2015
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONSPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.WORKFLOWSTEPS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

public interface ISMARTTASKCONFIGURATORDAO {
	
	/**
	 * @author Shailendra Rajput
	 * @Date May 15, 2015
	 * @Description:This is used to Display All data from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
	 * @param StrTenant
	 * @param StrCompany
	 * @return List<Object>
	 */
	
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List getDisplayAll(String StrTenant,String StrCompany) throws JASCIEXCEPTION;
	
	/**
	 * @author Shailendra Rajput
	 * @Date May 15, 2015
	 * @Description:This is used to search the Display All list from SMART_TASK_SEQ_HEADERS on Smart Task Configurator lookup Screen.
	 * @param StrTenant
	 * @param StrTask
	 * @param StrApplication
	 * @param StrExecutionGroup
	 * @param StrExecutionType
	 * @param StrExecutionDevice
	 * @param StrExecution
	 * @param StrDescription
	 * @param StrMenuName
	 * @return List<Object>
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public List getList(String StrTenant,String StrCompany,String StrTask,String StrApplication,String StrExecutionGroup,String StrExecutionType,String StrExecutionDevice,String StrExecution,String StrDescription,String StrMenuName) throws JASCIEXCEPTION;

	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:It is declaration of function,which is used to check Execution_Sequence_Name assigned in table WORK_FLOW_STEPS
	 * @param StrTenant_Id
	 * @param Company_Id
	 * @param StrApplication_Id
	 * @param StrExecution_Sequence_Group
	 * @param StrExecution_Sequence_Name
	 * @return List<WORKFLOWSTEPS>
	 * @throws JASCIEXCEPTION
	 */
	
	public List<WORKFLOWSTEPS> getExistExecutionSequenceName(String StrTenant_Id,String Company_Id,String StrExecution_Sequence_Name) throws JASCIEXCEPTION;
	
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:It is declaration of function,which is used to get execution description20 and description50 on the basis of execution name from ST_Execution table.
	 * @param StrExecution_Name
	 * @return List<WORKFLOWSTEPS>
	 * @throws JASCIEXCEPTION
	 */
	
	public List<SMARTTASKEXECUTIONS> getExecutionData(String StrExecution_Name) throws JASCIEXCEPTION;
	
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:It is declaration of function,which is used to delete the record from SMART_TASK_SEQUENCE_HEADERS,SMART_TASK_SEQUENCE_INSTRUCTIONS, MENU_OPTIONS and MENU_PROFILE table
	 * @param StrTenant_Id
	 * @param Company_Id
	 * @param StrApplication_Id
	 * @param StrExecution_Sequence_Group
	 * @param StrExecution_Sequence_Name
	 * @param StrMenu_Name
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	public boolean deleteEntry(String StrTenant_Id,String Company_Id,String StrExecution_Sequence_Name,String StrMenu_Name) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It is declaration of function, which is used to get the record into table SMART_TASK_SEQ_HEADERS.
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrExecutionSequenceName
	 * @return List<SMARTTASKSEQHEADERS>
	 * @throws JASCIEXCEPTION
	 */
	public SMARTTASKSEQHEADERS getSTSheadersData(String StrTenant_Id,String Company_Id, String StrExecution_Sequence_Name) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It is declaration of function, which is used to get the record into table SMART_TASK_EXECUTIONS.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Group
	 * @param StrExecution_Type
	 * @param StrExecution_Device
	 * @return List<SMARTTASKEXECUTIONS>
	 * @throws JASCIEXCEPTION
	 */
	public List<SMARTTASKEXECUTIONS> getSmartTaskExecutionData(String StrTenant_Id,String Company_Id,String strExecutionGroup,String strExecutionType, String strExecutionDevice) throws JASCIEXCEPTION;


	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It is declaration of function,Which is used to get the record from GENERAL_CODES table on the behalf of Parameters for DropDown.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrGeneral_Code_Id
	 * @return List<Object>
	 * @throws JASCIEXCEPTION
	 */
	public List<Object> getGeneralCodeDropDown(String StrTenant_Id, String Company_Id,String strGeneral_Code_Id) throws JASCIEXCEPTION;


	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It is declaration of function,Which is used to get the record from SMART_TASK_SEQ_INSTRUCTIONS table on the behalf of Parameters for Sequence View.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Sequence_Name
	 * @return List<SMARTTASKSEQINSTRUCTIONS>
	 * @throws JASCIEXCEPTION
	 */
	public List<SMARTTASKSEQINSTRUCTIONS> getSequenceViewData(String StrTenant_Id,String Company_Id, String StrExecution_Sequence_Name) throws JASCIEXCEPTION;

	
	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORDAOIMPL is used to Add Record in SMART_TASK_SEQ_HEADERS Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectSTSHeaders
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String addEntrySTSH(SMARTTASKSEQHEADERS ObjectSTSHeaders) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORDAOIMPL is used to Add Record in SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectSTSInstructions
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String addEntrySTSI(SMARTTASKSEQINSTRUCTIONS ObjectSTSInstructions) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORDAOIMPL is used to Add Record in Menu_Option Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectMenuOption
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String addEntrySTCMO(MENUOPTIONS ObjectMenuOption) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORDAOIMPL is used to Update Record in Menu_Option Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectMenuOption
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String updateEntrySTCMO(MENUOPTIONS ObjectMenuOption) throws JASCIEXCEPTION;

	
	
	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORDAOIMPL is used to Update Record in SMART_TASK_SEQ_HEADERS Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectSTSHeaders
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String updateEntrySTSH(SMARTTASKSEQHEADERS ObjectSTSHeaders) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORDAOIMPL is used to Update Record in SMART_TASK_SEQ_INST Table From SmartTaskConfigurator Maintenance Sreen
	 * @param ObjectSTSInstructions	
	 * @return String
	 * @throws JASCIEXCEPTION
	 */
	public String updateEntrySTSI(SMARTTASKSEQINSTRUCTIONS ObjectSTSInstructions) throws JASCIEXCEPTION;

	
	/**
	 * 
	 * @Description get team member name form team members table based on team member id
	 * @param Tenant
	 * @param Company
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public List<TEAMMEMBERS> getTeamMemberName(String Tenant,String TeamMember) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date July 18, 2015
	 * @Description get the list of SmarttaskSeqHeaders for check ExecutionSequenceName from the table SMART_TASK_SEQ_HEADERS
	 * @param Tenant
	 * @param Company
	 * @param strExecution_Sequence_Name
	 * @return List<SMARTTASKSEQHEADERS>
	 * @throws JASCIEXCEPTION
	 */
	public List<SMARTTASKSEQHEADERS> getExistExecutionSequenceNameinSTSH(String strTenant_Id, String company,String strExecution_Sequence_Name) throws JASCIEXCEPTION;

	/**
	 * @author Shailendra Rajput
	 * @Date July 18, 2015
	 * @Description get the list of menuoption for check menu_option from the table menu_option	
	 * @param StrMenu_Option
	 * @return List<MENUOPTIONS>
	 * @throws JASCIEXCEPTION
	 */
	
	public List<MENUOPTIONS> getExistMenuOptioninSTSH(String StrMenu_Option) throws JASCIEXCEPTION;
	
	/**
	 * @author Shailendra Rajput
	 * @Date Jun 30, 2015
	 * Description:This is declaration of Function which is implement in SMARTTASKCONFIGURATORDAOIMPL is used to Delete Record in SMART_TASK_SEQ_Instructions Table From SmartTaskConfigurator Maintenance Screen
	 * @param ObjectSTSInstructions
	 * @return void
	 * @throws JASCIEXCEPTION
	 */
	public void deleteEntrySTSI(SMARTTASKSEQINSTRUCTIONSPK oBJSmarttaskseqinstructionspk) throws JASCIEXCEPTION;
	
	 /**
		 * 
		 * @author Shailendra Rajput
		 * @Date September 16, 2015
		 * @Description:it is used to check Custom Execution name is valid or not
		 * @param CustomExecutionPath
		 * @return boolean
		 * @throws JASCIEXCEPTION
		 */
	public boolean validateCustomExecution(String CustomExecutionPath) throws JASCIEXCEPTION;

}
