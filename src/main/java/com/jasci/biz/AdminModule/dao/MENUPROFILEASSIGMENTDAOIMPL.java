/**

Date Developed :JAN 13 2014
Created by: sarvendra tyagi
Description :Menu Profile Assigment Dao class for intraction from data base
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.MENUPROFILEASSIGNMENT;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class MENUPROFILEASSIGMENTDAOIMPL implements IMENUPROFILEASSIGMENTDAO {

	
	@Autowired
	SessionFactory objSessionFactory;
	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	
	
	/** 
	 * @Description  :This function is used for getting label of Menu Profile maintenance screen
	 * @param        :strMenuTypes
	 * @return       :objMENUPROFILEMAINTENANCELABELBE 
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> serchByPartOfTeamMemberName(String partTeamMemberName)
			throws JASCIEXCEPTION {
		
		List<Object> objTeammembers=null;
		try{
			
			objTeammembers=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_SearchByNameLike_Query)
			.setParameter(GLOBALCONSTANT.INT_ZERO, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
			.setParameter(GLOBALCONSTANT.IntOne, (GLOBALCONSTANT.StrPercentSign+partTeamMemberName+GLOBALCONSTANT.StrPercentSign).toUpperCase())
			.setParameter(GLOBALCONSTANT.INT_TWO, (GLOBALCONSTANT.StrPercentSign+partTeamMemberName+GLOBALCONSTANT.StrPercentSign).toUpperCase())
			.setParameter(GLOBALCONSTANT.INT_THREE, (GLOBALCONSTANT.StrPercentSign+partTeamMemberName+GLOBALCONSTANT.StrPercentSign).toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return objTeammembers;
	}
	
	
	
	/** 
	 * @Description  : This function is used for getting Team Member list
	 * @param        : String TeamMemberID,String searchField
	 * @return       : TEAMMEMBERS
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getTeamMemberData(String TeamMemberID)throws JASCIEXCEPTION{
		
		List<Object> objTeammembers=null;
		
		try{
			
			objTeammembers=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_TeamMemberID_Query)
			.setParameter(GLOBALCONSTANT.INT_ZERO, TeamMemberID.toUpperCase())
			.setParameter(GLOBALCONSTANT.IntOne, OBJCOMMONSESSIONBE.getTenant().toUpperCase()).list();
			
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		
		return objTeammembers;
	}

	/** 
	 * @Description  : This function is used for getting all Team Member list
	 * @param        : 
	 * @return       : TEAMMEMBERS
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getTeamMemberDisplayAll()throws JASCIEXCEPTION{
		
		List<Object> objTeammembers=null;
		
		try{
			
			objTeammembers=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_DisplayAll_TeamMember_Query)
					.setParameter(GLOBALCONSTANT.MenuProfile_Tenant, OBJCOMMONSESSIONBE.getTenant().toUpperCase()).list();
			
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		
		return objTeammembers;
	}

		
	/** 
	 * @Description  : This Method is used for getting list of menu profile which assigned to team member
	 * @param        : strMenuTypes
	 * @return       : objMENUPROFILEMAINTENANCELABELBE 
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getTeamMemberAssignedProfileList(String teamMemberID)throws JASCIEXCEPTION{
		
		List<Object> objMenuprofileheaders=null;
		
		try{
			
			
			objMenuprofileheaders=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_TeamMemberProfile_Query)
					.setParameter(GLOBALCONSTANT.INT_ZERO, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, teamMemberID.toUpperCase()).list();
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return objMenuprofileheaders;
	}
	
	
	
	/** 
	 * @Description  : This Method is used for getting list of menu profile based on menu type
	 * @param        :String menuType
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getMenuProfileList(String menuType)throws JASCIEXCEPTION{
		
		List<Object> objMenuProfileList=null;
		
		try{
			
			
			objMenuProfileList=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_MenuProfile_MenuType_Query)
					.setParameter(GLOBALCONSTANT.INT_ZERO, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne,menuType.toUpperCase()).list();
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		
		return objMenuProfileList;
	}

	
	

	/** 
	 * @Description  : This Method is used for getting all list of menu profile based on menu type
	 * @param        :
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getAllMenuProfileList()throws JASCIEXCEPTION{
		List<Object> objMenuProfileList=null;
		try{

			objMenuProfileList=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_AllMenuProfile_MenuType_Query)
					.setParameter(GLOBALCONSTANT.INT_ZERO, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch (Exception objException) {
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
	
	return objMenuProfileList;
	
	}
	
	
	
	

	/** 
	 * @Description  : This Method is used for getting all list of menu profile based on assigned Menu Profile To team member
	 * @param        :String strTeamMemberID
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getAllMenuProfileListLeftMenu(String strTeamMemberID)throws JASCIEXCEPTION{
		
		List<Object> objMenuProfileList=null;
		try{

			objMenuProfileList=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_TeamMemberProfile_DispalyAll_Query)
					.setParameter(GLOBALCONSTANT.MenuProfile_Tenant, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfileAssigment_TeamMemberID, strTeamMemberID.toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfileAssigment_tenant1, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch (Exception objException) {
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
	
	return objMenuProfileList;
	}

	
	/** 
	 * @Description  : This Method is used for getting all list of menu profile based on assigned Menu Profile To team member
	 * @param        :String strTeamMemberID,String StrMenuType
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getAllMenuProfileListMenu(String strTeamMemberID,String StrMenuType)throws JASCIEXCEPTION{
		List<Object> objMenuProfileList=null;
		try{

			objMenuProfileList=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_TeamMemberProfile_MenuType_Query)
					.setParameter(GLOBALCONSTANT.MenuProfile_Tenant, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfileAssigment_TeamMemberID, strTeamMemberID.toUpperCase())
					.setParameter(GLOBALCONSTANT.MenuProfileAssigment_tenant1, OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.MENUOPTION_MenuType, StrMenuType.toUpperCase())
					.list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch (Exception objException) {
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
	
	return objMenuProfileList;
	}
	
	

	/** 
	 * @Description  : This Method is used for assigned menu profile from menu profile Assignmenttable
	 * @param        :String strTeamMemberID
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public Boolean getAllAssignedMenuProfileDelete(String strTeamMemberID)throws JASCIEXCEPTION{
		
		Boolean status=false;
		try{
			String tenant= OBJCOMMONSESSIONBE.getTenant().toUpperCase();
			Query query=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_Delete_MenuProfile_Query)
					.setParameter(GLOBALCONSTANT.INT_ZERO,tenant)
					.setParameter(GLOBALCONSTANT.IntOne, strTeamMemberID.toUpperCase());
			
			int intStatus=query.executeUpdate();
			
			if(intStatus>GLOBALCONSTANT.INT_ZERO){
			
				status=true;
			}
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch (Exception objException) {
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return status;
	}
	
	
	
	
	
	/** 
	 * @Description  : This Method is used for assigned menu profile saved records in Menu Profile assignment table
	 * @param        :String strTeamMemberID
	 * @return       : Boolean
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public Boolean getSaveAndUpdate(MENUPROFILEASSIGNMENT objMenuprofileassignment)throws JASCIEXCEPTION{
		
		
		Boolean status=false;
		try{
					objSessionFactory.getCurrentSession().saveOrUpdate(objMenuprofileassignment);
					status=true;
			
		
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch (Exception objException) {
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return status;
		
	}
	
	
	/** 
	 * @Description  : This Method is used to get the name of team member
	 * @param        :TeamMemberID
	 * @return       : List<TEAMMEMBERS>
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getTeamMemberName(String TeamMemberID)throws JASCIEXCEPTION{
		
		List<Object> objTeammembers=null;
		try{
			
			String strTenant=OBJCOMMONSESSIONBE.getTenant().toUpperCase();
			objTeammembers=objSessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuProfileAssignment_TeamMemberName_Query)
					.setParameter(GLOBALCONSTANT.INT_ZERO, strTenant)
					.setParameter(GLOBALCONSTANT.IntOne, TeamMemberID.toUpperCase()).list();
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		
		return objTeammembers;
	}
	
}
