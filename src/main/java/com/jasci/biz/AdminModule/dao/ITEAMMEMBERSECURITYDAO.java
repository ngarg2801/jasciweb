/*
Date Developed  Nov 17 2014
Created By "Rahul Kumar"
Description : define an interface method to communicate with DataBase (team members message) 
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONS;
import com.jasci.biz.AdminModule.model.UPDATESECURITYAUTHORIZATION;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.exception.JASCIEXCEPTION;

public interface ITEAMMEMBERSECURITYDAO {
	

	/** 
	 * 
	 * @param StrTenant
	 * @param StrFullfillmentCenter
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public List<Object> getAllTeamMember(String StrTenant,String StrFullfillmentCenter) throws JASCIEXCEPTION;
	/** 
	 * 
	 * @param TeamMemberName
	 * @param StrTenant
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public List<SECURITYAUTHORIZATIONS> getSecurityAuthorizationDataByTeamMemberName(String TeamMemberName,String StrTenant) throws JASCIEXCEPTION;
	/** 
	 * 
	 * @param StrGeneralCode
	 * @param Tenant
	 * @param Company
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public List<Object> getGeneralCodeDataByCodeId(String StrGeneralCode,String Tenant, String Company) throws JASCIEXCEPTION;
	/** 
	 * 
	 * @param objUpdateSecurityAuth
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	public String updateSecurityAuthorization(UPDATESECURITYAUTHORIZATION objUpdateSecurityAuth) throws JASCIEXCEPTION;
/** 
 * 
 * @param Tenant
 * @param TeamMember
 * @param Company
 * @return
 * @throws JASCIEXCEPTION
 */
	public WEBSERVICESTATUS deleteTeamMember(String Tenant,String TeamMember,String Company) throws JASCIEXCEPTION;

}