/*

Date Developed :Dec 24 2014
Created by: Rakesh Pal
Description : define an interface method to communicate with DataBase 
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.exception.JASCIEXCEPTION;


public interface INOTESDAL {
	
	
	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 26, 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return List<NOTES>
	 *
	 */

	public  List<NOTES>  getNotesList(String Notes_Link,String Notes_Id,String Tenant_id,String Company_id)throws  JASCIEXCEPTION;

	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 24, 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param TeamMember
	 * @param Company_id
	 * @param Tenant_id
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return BOOLEAN
	 *
	 */
	
	public List<NOTES> getnote(String Notes_Link,String Notes_Id,String Tenant_id,String Company_id,int ID)throws  JASCIEXCEPTION;

	
	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 24, 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param TeamMember
	 * @param Company_id
	 * @param Tenant_id
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return BOOLEAN
	 *
	 */
	
	public Boolean addOrUpdateNotesEntry(NOTES objNotes) throws JASCIEXCEPTION;

	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 31, 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param TeamMember
	 * @param Company_id
	 * @param Tenant_id
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return BOOLEAN
	 *
	 */

	public Boolean deleteNote(String tenant_Id, String company_id,
			String note_id, String note_link, String ID)throws JASCIEXCEPTION;

}
