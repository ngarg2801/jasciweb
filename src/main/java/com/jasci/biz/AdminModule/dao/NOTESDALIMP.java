/*

Date Developed :Dec 24 2014
Created by: Rakesh Pal
Description :NOTESDALIMP IS USED TO SEND AND FETHCH THE DATA FROM DATABASE
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.jasci.biz.AdminModule.model.NOTES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class NOTESDALIMP implements INOTESDAL  {

	@Autowired
	private SessionFactory sessionFactory ;
	
	/**
	 * @author Rakesh Pal
	 * @Date Dec 24, 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param Team_Member
	 * @return LIST<NOTES>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<NOTES> getNotesList(String Notes_Link,String Notes_Id,String Tenant_id,String Company_id)throws JASCIEXCEPTION {
		
		
		List <NOTES> objList=null;
		try{
			objList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.GetNotesListQuery).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant_id.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company_id.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO,Notes_Link.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, Notes_Id.toUpperCase()).list();
		
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			objException.printStackTrace();
		}
		
		return objList;
	}


	/**
	 * @author Rakesh Pal
	 * @Date Dec 24, 2014 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param Team_Member
	 * @param companyId
	 * @param TenantId
	 * @return update note
	 * @throws JASCIEXCEPTION
	 */
	
	
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<NOTES> getnote(String Notes_Link,String Notes_Id,String Tenant_id,String Company_id,int ID)throws  JASCIEXCEPTION
{

		
		List <NOTES> objNotes=null;
		try{
			objNotes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.UpdateNotesEntery).setParameter(GLOBALCONSTANT.INT_ZERO,Notes_Link.toUpperCase()).
					setParameter(GLOBALCONSTANT.IntOne,Notes_Id.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO,Tenant_id.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE,Company_id.toUpperCase()).setParameter(GLOBALCONSTANT.INT_FOUR,ID).list();		
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			objException.printStackTrace();
		}
		
		return objNotes;
	}


	
	/**
	 * @author Rakesh Pal
	 * @Date Dec 24, 2014 2014
	 * @param Notes_Link
	 * @param Notes_Id
	 * @param Team_Member
	 * @param companyId
	 * @param TenantId
	 * @return update note
	 * @throws JASCIEXCEPTION
	 */
	
	
	public Boolean addOrUpdateNotesEntry(NOTES objNotes) throws JASCIEXCEPTION {
		
		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();	
		Boolean status=false;

		try{

			if(objNotes.getId().getID() == GLOBALCONSTANT.INT_ZERO)
			{
				session.createSQLQuery(GLOBALCONSTANT.NOTES_INSERT_QUERY).setParameter(GLOBALCONSTANT.INT_ZERO, objNotes.getId().getTenant_Id())
				.setParameter(GLOBALCONSTANT.IntOne, objNotes.getId().getCompany_Id()).setParameter(GLOBALCONSTANT.INT_TWO, objNotes.getId().getNote_Id()).setParameter(GLOBALCONSTANT.INT_THREE, objNotes.getId().getNote_Link())
				.setParameter(GLOBALCONSTANT.INT_FOUR, objNotes.getNote()).setParameter(GLOBALCONSTANT.INT_FIVE, objNotes.getCONTINUATIONS_SEQUENCE()).setParameter(GLOBALCONSTANT.INT_SIX, objNotes.getLast_Acitivity_Date()).setParameter(GLOBALCONSTANT.INT_SEVEN, objNotes.getLast_Activity_Team_Member()).executeUpdate();
			}
			else
			{
			session.saveOrUpdate(objNotes);
			}
			TransactionObject.commit();
			session.close();
			status=true;

		}catch(Exception objException){
			TransactionObject.rollback();
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return status;
	}

	
	
	/**
	 * @author Rakesh Pal
	 * @Date Jan 01, 2015
	 * @param tenant_Id
	 * @param company_id
	 * @param note_id
	 * @param note_link
	 * @param last_Activity_date
	 * @return status
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.INOTESDAL#deleteNotes(java.lang.String, java.lang.String)
	 */
	
	
	public Boolean deleteNote(String Tenant_Id, String Company_id,
			String note_id, String note_link, String ID)
			throws JASCIEXCEPTION {
		Session session = sessionFactory.openSession();
		Boolean Status=false;
try
{
	
	
	Query query = session.getNamedQuery(GLOBALCONSTANT.Notes_Delete_NamedQuery);
	query.setString(GLOBALCONSTANT.DataBase_Notes_Tenant_Id, Tenant_Id.toUpperCase());
	query.setString(GLOBALCONSTANT.DataBase_Notes_Company_Id, Company_id.toUpperCase());
	query.setString(GLOBALCONSTANT.DataBase_Notes_Note_Id, note_id.toUpperCase());
	query.setString(GLOBALCONSTANT.DataBase_Notes_Note_Link, note_link.toUpperCase());
	query.setInteger(GLOBALCONSTANT.DataBase_Notes_ID, Integer.parseInt(ID));
	query.executeUpdate();
	Status=true;
	session.close();
}catch(SQLGrammarException objException){
session.close();
	throw new JASCIEXCEPTION(objException.getMessage());

}
catch(Exception objException){
	session.close();
	throw new JASCIEXCEPTION(objException.getMessage());
}
return Status;
	}

}
