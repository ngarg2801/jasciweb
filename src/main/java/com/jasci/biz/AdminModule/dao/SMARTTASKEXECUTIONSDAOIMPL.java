/**
Description This class provide all of the functionality related to database for Smart Task Configurator screen 
Created By Shailendra Rajput  
Created Date May 15 2015
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.controller.SMARTTASKEXECUTIONSCONTROLLER;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONSBEAN;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQINSTRUCTIONSPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.WORKFLOWSTEPS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class SMARTTASKEXECUTIONSDAOIMPL implements ISMARTTASKEXECUTIONSDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private COMMONSESSIONBE OBJCOMMONSESSIONBE;
	
	private static Logger log = LoggerFactory.getLogger(SMARTTASKEXECUTIONSDAOIMPL.class.getName());

	/**
	 * @Description:This is used to show All Data from SMART_TASK_SEQ_HEADERS on Smart Task Executions lookup Screen for DisplayAll.
	 *@param StrTenant
	 *@param StrCompany
	 *@return List
	 */
	
	@SuppressWarnings({GLOBALCONSTANT.Strrawtypes,GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	public List getDisplayAll(String StrTenant,String StrCompany){
	
		/** Make the instance of List of Object*/
		List<Object> lOCObjects = null;
		
		try{

	
			if(OBJCOMMONSESSIONBE.getJasci_Tenant().equalsIgnoreCase(OBJCOMMONSESSIONBE.getTenant())) {
				Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskExecution_Query_DisplayAll_Tenant);
				lOCObjects = OBJ_All_New.list();
			}
			else{
			
				Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskExecution_Query_DisplayAll);
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrCompany.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrTenant.toUpperCase());
				OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrCompany.toUpperCase());
				lOCObjects = OBJ_All_New.list();
			}
				return lOCObjects;
			}catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
			catch(Exception ObjectException){
				log.error("  GetDisplayAll Exception = " + ObjectException.getMessage());
				throw new JASCIEXCEPTION(ObjectException.getMessage());
			}finally{
				return lOCObjects;
			}		
		}
	/**
	 * @Description:This is used to search the list from SMART_TASK_SEQ_HEADERS on Smart Task Executions lookup Screen.
	 *@param StrTenant
	 *@param StrTask
	 *@param StrApplication
	 *@param StrExecutionGroup
	 *@param StrExecutionType
	 *@param StrExecutionDevice
	 *@param StrExecution
	 *@param StrDescription
	 *@param StrMenuName
	 *@return List
	 */
	
	@SuppressWarnings({GLOBALCONSTANT.Strrawtypes,GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	public List getList(String StrTenant,  String StrCompany, String StrApplication, String StrExecutionGroup, String StrExecutionType, String StrExecutionDevice, String StrExecutionName, String StrDescription20, String StrButton) {
		
		List<Object> lOCObjects = null;
	    
		try{
		
			if (OBJCOMMONSESSIONBE.getJasci_Tenant().equalsIgnoreCase(OBJCOMMONSESSIONBE.getTenant())){
				if (!GLOBALCONSTANT.BlankString.equals(StrDescription20)){
					/**It is used get the Decription20 or Description50 data form SMART_TASK_SEQ_HEADERS table*/
					String DescriptionValue=GLOBALCONSTANT.StrPercentSign+StrDescription20.toUpperCase().trim()+GLOBALCONSTANT.StrPercentSign;
					
					Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.SmartTaskConfigurator_Query_Description_Tenant);
					OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, DescriptionValue);
					OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, DescriptionValue);
					lOCObjects = OBJ_All_New.list();	
				}
			}
			else {
				
				StringBuffer SmartTaskExecution_Query = new StringBuffer( "Select TENANT_ID, COMPANY_ID, EXECUTION_TYPE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
						+ "SMART_TASK_EXECUTIONS.EXECUTION_TYPE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXETYPE') "
						+ "and ROWNUM <= 1) as EXECUTION_TYPE_DESCRIPTION, EXECUTION_DEVICE,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
						+ "SMART_TASK_EXECUTIONS.EXECUTION_DEVICE and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEDEVICE') "
						+ "and ROWNUM <= 1) as EXECUTION_DEVICE_DESCRIPTION, EXECUTION_GROUP,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
						+ "SMART_TASK_EXECUTIONS.EXECUTION_GROUP and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('EXEGRP') "
						+ "and ROWNUM <= 1) as EXECUTION_GROUP_DESCRIPTION,EXECUTION_NAME,DESCRIPTION50,BUTTON , APPLICATION_ID,(select GENERAL_CODES.DESCRIPTION20 from GENERAL_CODES where GENERAL_CODES.GENERAL_CODE="
						+ "SMART_TASK_EXECUTIONS.APPLICATION_ID and UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? and UPPER(GENERAL_CODES.GENERAL_CODE_ID)=UPPER('APPLICATIONS') and ROWNUM <= 1) as APPLICATION_DESCRIPTION "
						+ ",(select NOTE from (select * from NOTES NT where UPPER(NT.TENANT_ID)= ? and UPPER(NT.COMPANY_ID)= ? AND UPPER(NT.NOTE_ID)= UPPER('TaskExecution') order by NT.LAST_ACTIVITY_DATE DESC) NTT where UPPER(NTT.NOTE_LINK)=UPPER(SMART_TASK_EXECUTIONS.EXECUTION_NAME) AND ROWNUM = 1) "
						+ "as Note from SMART_TASK_EXECUTIONS where UPPER(TENANT_ID)=? and UPPER(COMPANY_ID)=? ");
				
	
				int parameterIdx = 0;
				String Description = "";
				StringBuffer desription  =new StringBuffer();
				
				if (!GLOBALCONSTANT.BlankString.equals(StrApplication)) {
					SmartTaskExecution_Query.append(" and APPLICATION_ID=?");
				}
				if (!GLOBALCONSTANT.BlankString.equals(StrExecutionGroup)) {
					SmartTaskExecution_Query.append(" and EXECUTION_GROUP=?");
				}
				if(!GLOBALCONSTANT.BlankString.equals(StrExecutionType)) { 
					SmartTaskExecution_Query.append("  and EXECUTION_TYPE=?");
				}
				if (!GLOBALCONSTANT.BlankString.equals(StrExecutionDevice)) {
					SmartTaskExecution_Query.append(" and EXECUTION_DEVICE=?");
				}
				if (!GLOBALCONSTANT.BlankString.equals(StrExecutionName)) {
					SmartTaskExecution_Query.append(" and EXECUTION_NAME=?");
				}
				if (!GLOBALCONSTANT.BlankString.equals(StrDescription20)) {
					Description= GLOBALCONSTANT.StrPercentSign+StrDescription20.toUpperCase().trim()+GLOBALCONSTANT.StrPercentSign ;
					desription.append("\"").append(Description).append("\"");
					SmartTaskExecution_Query.append(" and EXECUTION_NAME  like ? ");
				}
				
				if (!GLOBALCONSTANT.BlankString.equals(StrButton)) {
					SmartTaskExecution_Query.append(" and BUTTON=?");
				}
				SmartTaskExecution_Query.append("  ORDER BY LAST_ACTIVITY_DATE DESC");
				
				Query query = sessionFactory.getCurrentSession().createSQLQuery(SmartTaskExecution_Query.toString());
				query.setParameter(parameterIdx++, StrTenant.toUpperCase());
				query.setParameter(parameterIdx++, StrCompany.toUpperCase());
				query.setParameter(parameterIdx++, StrTenant.toUpperCase());
				query.setParameter(parameterIdx++, StrCompany.toUpperCase());
				query.setParameter(parameterIdx++, StrTenant.toUpperCase());
				query.setParameter(parameterIdx++, StrCompany.toUpperCase());
				query.setParameter(parameterIdx++, StrTenant.toUpperCase());
				query.setParameter(parameterIdx++, StrCompany.toUpperCase());
				query.setParameter(parameterIdx++, StrTenant.toUpperCase());
				query.setParameter(parameterIdx++, StrCompany.toUpperCase());
				query.setParameter(parameterIdx++, StrTenant.toUpperCase());
				query.setParameter(parameterIdx++, StrCompany.toUpperCase());
			    if (!GLOBALCONSTANT.BlankString.equals(StrApplication))  {
			    	query.setParameter(parameterIdx++, StrApplication.trim());
			    } 
			    if (!GLOBALCONSTANT.BlankString.equals(StrExecutionGroup)) {
			    	query.setParameter(parameterIdx++, StrExecutionGroup.trim());
			    }
			    if(!GLOBALCONSTANT.BlankString.equals(StrExecutionType)) {
			    	query.setParameter(parameterIdx++, StrExecutionType.trim());
			    }
			    if (!GLOBALCONSTANT.BlankString.equals(StrExecutionDevice)) {
					query.setParameter(parameterIdx++, StrExecutionDevice.trim());
			    }
			    if (!GLOBALCONSTANT.BlankString.equals(StrExecutionName)) {
			    	query.setParameter(parameterIdx++, StrExecutionName.trim());
			    }
			    if (!GLOBALCONSTANT.BlankString.equals(StrDescription20)) {
			    	query.setParameter(parameterIdx++, Description);
			    }
			    if (!GLOBALCONSTANT.BlankString.equals(StrButton)) {
			    	query.setParameter(parameterIdx++, StrButton.trim());
			    }
			    
			    lOCObjects = query.list();
			} 

			return lOCObjects;
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){
			log.error(" SMARTTASKEXECUTIONSDAOIMPL Exception : " + ObjectException.getMessage());
			throw new JASCIEXCEPTION(ObjectException.getMessage());
		}finally{
			return lOCObjects;
		}		
	}
	
	public StringBuffer buildSQL(String StrTenant,  String StrCompany, String StrApplication, String StrExecutionGroup, String StrExecutionType, String StrExecutionDevice, String StrExecutionName, String StrDescription20, String StrButton)  {
		StringBuffer sqlStrBuf = new StringBuffer();
		return sqlStrBuf;
	}
				
	
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 19, 2015
	 * @Description:It  is used to delete the record from SMART_TASK_SEQUENCE_HEADERS,SMART_TASK_SEQUENCE_INSTRUCTIONS, MENU_OPTIONS and MENU_PROFILE table
	 * @param StrTenant_Id
	 * @param Company_Id
	 * @param StrApplication_Id
	 * @param StrExecution_Sequence_Group
	 * @param StrExecution_Sequence_Name
	 * @param StrMenu_Name
	 * @return boolean
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strfinally)
	public boolean deleteEntry(String StrTenant_Id,String Company_Id,String StrExecution_Sequence_Name,String StrMenu_Name) throws JASCIEXCEPTION{
		
		 Boolean Status=false;
		  try
		  {
		   try{
		   //It is used to delete record from MENU_PROFILE_OPTION Table..
		   Query ObjQueryMenuProfileOption=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteMenuProfileOption);
		   ObjQueryMenuProfileOption.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQueryMenuProfileOption.setString(GLOBALCONSTANT.IntOne, StrMenu_Name.toUpperCase());
		   ObjQueryMenuProfileOption.executeUpdate();
		   
		   //It is used to delete record from MENU_OPTION Table..
		   Query ObjQueryMenuOption=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteMenuOption);
		   ObjQueryMenuOption.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQueryMenuOption.setString(GLOBALCONSTANT.IntOne, StrMenu_Name.toUpperCase());
		   ObjQueryMenuOption.executeUpdate();
		   
		   //It is used to delete record from SMART_TASK_SEQ_INSTRUCTIONS Table..
		   Query ObjQuerySmartTaskSeqInstructions=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteSmartTaskSeqInstruction);
		   ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.IntOne, Company_Id.toUpperCase());
		   ObjQuerySmartTaskSeqInstructions.setString(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase());
		   ObjQuerySmartTaskSeqInstructions.executeUpdate();
		   
		 //It is used to delete record from SMART_TASK_SEQ_HEADERS Table..
		   Query ObjQuerySmartTaskSeqHeaders=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameDeleteSmartTaskSeqHeaders);
		   ObjQuerySmartTaskSeqHeaders.setString(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase());
		   ObjQuerySmartTaskSeqHeaders.setString(GLOBALCONSTANT.IntOne, Company_Id.toUpperCase());
		   ObjQuerySmartTaskSeqHeaders.setString(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase());
		   ObjQuerySmartTaskSeqHeaders.executeUpdate();
		   
		   Status=true;
		   }catch(Exception Ex){
		    Status=false;
		   }
		  
		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
		  
		  catch(Exception ObjException)
		  {
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  finally{
		   return Status;
		  }
		   
		   
	}


	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_SEQUENCE_HEADERS table on the behalf of Tenant_Id,Company_Id,Execution_Sequence_Name.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Sequence_Name
	 * @return List<SMARTTASKSEQHEADERS>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	public SMARTTASKSEQHEADERS getSTSheadersData(String StrTenant_Id,String StrCompany_Id,String StrExecution_Sequence_Name){
		SMARTTASKSEQHEADERS ObjSmartTaskHeaders=null;
	    
		try{
				/**It is used get the data form SMART_TASK_SEQ_HEADERS table*/
				Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskSeqHeaders);
				ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase().trim());
				ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Sequence_Name.toUpperCase().trim());
				List<SMARTTASKSEQHEADERS> ObjSmartTaskHeaderslist = ObjQuery.list();
				ObjSmartTaskHeaders= ObjSmartTaskHeaderslist.get(GLOBALCONSTANT.INT_ZERO);
				
				return ObjSmartTaskHeaders;
			
	}catch(SQLGrammarException objException){

		throw new JASCIEXCEPTION(objException.getMessage());

	}
		 catch(Exception ObjException)
		  {
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  finally{
		   return ObjSmartTaskHeaders;
		  }	
	}
	
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from SMART_TASK_EXECUTIONS table on the behalf of Parameters.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrExecution_Group
	 * @param StrExecution_Type
	 * @param StrExecution_Device
	 * @return List<SMARTTASKEXECUTIONS>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
	 public List<SMARTTASKEXECUTIONS> getSmartTaskExecutionData(String StrTenant_Id,String StrCompany_Id,String StrExecution_Group,String StrExecution_Type,String StrExecution_Device){
	  
	  List<SMARTTASKEXECUTIONS> ObjSmartTaskHeaders = null;
	     
	  try{

	   
	    
		  /**It is used get the data form SMART_TASK_EXECUTIONS table*/
		   if(GLOBALCONSTANT.BlankString.equals(StrTenant_Id) && GLOBALCONSTANT.BlankString.equals(StrCompany_Id) && GLOBALCONSTANT.BlankString.equals(StrExecution_Device ) && GLOBALCONSTANT.BlankString.equals(StrExecution_Type ) && GLOBALCONSTANT.BlankString.equals(StrExecution_Group )) 
		   { 
		    Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecutionData);
		    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, OBJCOMMONSESSIONBE.getTenant().toUpperCase().trim());
		    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, OBJCOMMONSESSIONBE.getCompany().toUpperCase().trim());
		    ObjSmartTaskHeaders = ObjQuery.list();
		   }
		   else if(!GLOBALCONSTANT.BlankString.equals(StrTenant_Id) && !GLOBALCONSTANT.BlankString.equals(StrCompany_Id) && !GLOBALCONSTANT.BlankString.equals(StrExecution_Device ) && !GLOBALCONSTANT.BlankString.equals(StrExecution_Type ) && !GLOBALCONSTANT.BlankString.equals(StrExecution_Group )) 
		   {
				   if(GLOBALCONSTANT.BACKEND.equalsIgnoreCase(StrExecution_Type.toUpperCase())){
					   Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution_BeckendOnly);
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase().trim());
					    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase().trim());
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());				   
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrExecution_Type.toUpperCase().trim());
					    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Device.toUpperCase().trim());		
					    ObjSmartTaskHeaders = ObjQuery.list();
				   }
				   else{
			    Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution);
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, GLOBALCONSTANT.BACKEND);
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Type.toUpperCase().trim());
			    ObjQuery.setParameter(GLOBALCONSTANT.INT_FIVE, StrExecution_Device.toUpperCase().trim());
			    ObjSmartTaskHeaders = ObjQuery.list();
				   }
		   }
		   else{
			   
			   if(GLOBALCONSTANT.BACKEND.equalsIgnoreCase(StrExecution_Type.toUpperCase())){
				   Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution_BeckendOnly);
				   ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO,  OBJCOMMONSESSIONBE.getTenant().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.IntOne,  OBJCOMMONSESSIONBE.getCompany().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());				   
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrExecution_Type.toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Device.toUpperCase().trim());		
				    ObjSmartTaskHeaders = ObjQuery.list();
			   }
			   else{
				   Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecution);
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO,  OBJCOMMONSESSIONBE.getTenant().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.IntOne, OBJCOMMONSESSIONBE.getCompany().toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Group.toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, GLOBALCONSTANT.BACKEND);
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, StrExecution_Type.toUpperCase().trim());
				    ObjQuery.setParameter(GLOBALCONSTANT.INT_FIVE, StrExecution_Device.toUpperCase().trim());
				    ObjSmartTaskHeaders = ObjQuery.list();
			   }
		   }
		    
		    
		    return ObjSmartTaskHeaders;
	   
	 }catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
	   catch(Exception ObjException)
	    {
	     throw new JASCIEXCEPTION(ObjException.getMessage());
	    }
	    finally{
	     return ObjSmartTaskHeaders;
	    } 
	 }
	/**
	 * 
	 * @author Shailendra Rajput
	 * @Date May 27, 2015
	 * @Description:It  is used to get the record from GENERAL_CODES table on the behalf of Parameters for DropDown.
	 * @param StrTenant_Id
	 * @param StrCompany_Id
	 * @param StrGeneral_Code_Id
	 * @return List<Object>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> getGeneralCodeDropDown(String StrTenant_Id, String StrCompany_Id, String StrGeneral_Code_Id) throws JASCIEXCEPTION
	{

		
		try{
			
			return sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.GENERAL_CODES_DROPDOWN_HELPLINE)
					.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.toUpperCase())
					.setParameter(GLOBALCONSTANT.IntOne, StrCompany_Id.toUpperCase())
					.setParameter(GLOBALCONSTANT.INT_TWO, StrGeneral_Code_Id.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		  {
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  	
	}

	/**
	 * @Description get team member name form team members table based on team member id
	 * @param Tenant
	 * @param Company
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	 @SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
		 
		  
		  List<TEAMMEMBERS> objTeammembers=null;
		  try
		  {
		   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
		  catch(Exception ObjectException)
		  {
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		   
		   
		  }
		  
		  if(objTeammembers.size()==GLOBALCONSTANT.INT_ZERO){
		  try
		  {
		   objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query_By_TEAMID).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.trim().toUpperCase()).list();
		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}
		  catch(Exception ObjectException)
		  {
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		   
		   
		  }
		 }
		  return objTeammembers;
		 }
	 
				
		public List<SMARTTASKEXECUTIONS> getExecutionData(String StrExecution_Name) throws JASCIEXCEPTION {
			
			Query objquery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskConfigurator_QueryNameSelectSmartTaskExecutionDescriotions);
			objquery.setParameter(GLOBALCONSTANT.INT_ZERO, StrExecution_Name.toUpperCase());
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			List<SMARTTASKEXECUTIONS> objSmarttaskexecutions = objquery.list();
			return objSmarttaskexecutions;
		}
			
		public Boolean addExecutions(SMARTTASKEXECUTIONS smartTaskexecutions) throws JASCIEXCEPTION {
			  boolean Status=false;
			  Session session = sessionFactory.openSession();
		
			  	//	session.save(smartTaskexecutions);
			  try	{
				  			Transaction TransactionObject = session.beginTransaction();  		
				  			session.saveOrUpdate(smartTaskexecutions);
				  			TransactionObject.commit();
				  			Status=true;
			  }catch(SQLGrammarException objException){
				    log.error("addExecutions Exeception : " + objException.getMessage());
					throw new JASCIEXCEPTION(objException.getMessage());
				}
				catch(Exception ObjectException){
					log.error("addExecutions Exeception : " + ObjectException.getMessage());
					throw new JASCIEXCEPTION(ObjectException.getMessage());
				}finally{
					 session.close();
				}	  		
			  return Status;
		}
		
		public List<SMARTTASKEXECUTIONS> isExecutionExist(String strCompanyId, String strTenantId, String strExecutionName) throws JASCIEXCEPTION {
			List<SMARTTASKEXECUTIONS> smartTaskExecutionsList=null;

			try{
				
					Query query = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskExecutions_QuerySelectExecution);
					query.setParameter(0, strTenantId.trim());
					query.setParameter(1, strCompanyId.trim());
					query.setParameter(2, strExecutionName.trim());

					smartTaskExecutionsList = query.list();
					
			}catch (SQLGrammarException objGrammarException) {
				log.error(" isExecutionExist SQLGrammarException Exeception = " + objGrammarException.getMessage());
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
			catch(Exception objException){
				log.error("isExecutionExist Exeception = " + objException.getMessage());
				throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
			}

			return smartTaskExecutionsList;
		}
		
		public Boolean deleteExecutions(SMARTTASKEXECUTIONS smartTaskexecutions) throws JASCIEXCEPTION {
			  boolean Status=false;
			  Session session = sessionFactory.openSession();
			  
			  try	{
				  		Transaction TransactionObject = session.beginTransaction();  		
	 			  		session.delete(smartTaskexecutions);
	 			  		TransactionObject.commit();
	 			  		Status=true;
	 			  		
			  }catch(SQLGrammarException objException){
				    log.error("addExecutions Exeception : " + objException.getMessage());
					throw new JASCIEXCEPTION(objException.getMessage());
				}
				catch(Exception ObjectException){
					log.error("addExecutions Exeception : " + ObjectException.getMessage());
					throw new JASCIEXCEPTION(ObjectException.getMessage());
				}finally{
					 session.close();
				}	  		
			  return Status;
		}
		
		@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strfinally})
		public List<SMARTTASKSEQINSTRUCTIONS> getSmartTaskSeqInsttructions(String StrTenant_Id,String StrCompany_Id,String StrExecution_Name) throws JASCIEXCEPTION{
	
			List<SMARTTASKSEQINSTRUCTIONS> SmartTaskSeqInstructionsList=null;
	
			try {	

						Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SmartTaskExecutions_Seq_Instructions_Select);
						ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant_Id.trim());
						ObjQuery.setParameter(GLOBALCONSTANT.INT_ONE, StrCompany_Id.trim());
						ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrExecution_Name.trim());
						SmartTaskSeqInstructionsList = ObjQuery.list();
						
						return SmartTaskSeqInstructionsList;

			}catch(SQLGrammarException SQLGrammarException){
				log.error("getSmartTaskSeqInsttructions " +  SQLGrammarException.getMessage());
				throw new JASCIEXCEPTION(SQLGrammarException.getMessage());
			}
			catch(Exception ObjectException){
				log.error("getSmartTaskSeqInsttructions " +  ObjectException.getMessage());
				throw new JASCIEXCEPTION(ObjectException.getMessage());
			}finally{
				return SmartTaskSeqInstructionsList;
			}
		}
		
		
		public boolean deleteEntry(String StrTenant_Id, String StrCompany_Id, String StrExecution_Name)
				throws JASCIEXCEPTION {
			
			return false;
		}
		
		

}

