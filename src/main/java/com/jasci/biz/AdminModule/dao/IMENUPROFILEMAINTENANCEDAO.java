
/**

Date Developed:  Dec 25 2014
Description:  Provide data base method
Developed by: sarvendra tyagi
 */



package com.jasci.biz.AdminModule.dao;

import java.util.List;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEHEADER;
import com.jasci.biz.AdminModule.model.MENUPROFILEOPTIONS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.exception.JASCIEXCEPTION;

public interface IMENUPROFILEMAINTENANCEDAO {

	
	
	/**
	 * Created on:Dec 26 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Profile list based on given parameter
	 * Input parameter:String strMenuType,String strPartOfMenuProfile 
	 * Return Type :List<MENUPROFILEHEADER>
	 * 
	 */
	
	public List<MENUPROFILEHEADER> getMenuProfileList(String strMenuType,String strPartOfMenuProfile) throws JASCIEXCEPTION;
	
	
	/**
	 * Created on:Jan 1 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Profile list based on given parameter
	 * Input parameter:String strMenuType 
	 * Return Type :List<MENUPROFILEHEADER>
	 * 
	 */
	
	public List<MENUPROFILEHEADER> getMenuProfileListByMenuType(String strMenuType) throws JASCIEXCEPTION;
	
	
	/**
	 * Created on:Jan 1 2014
	 * Created by:Sarvendra Tyagi
	 * Description: This function  get Menu Profile list based on given parameter
	 * Input parameter:String strMenuType 
	 * Return Type :List<MENUPROFILEHEADER>
	 * 
	 */
	
	public List<MENUPROFILEHEADER> getMenuProfileListByDescription(String strPartDescription) throws JASCIEXCEPTION;
	
	
	
	
	/** 
	 * @Description: This function is used for getting label of menuProfile all screen
	 * @param  :language
	 * @return :List<LANGUAGES>
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Sarvendra Tyagi
	 * @Date   :Dec 26 2014
	 */
	
	
	public List<LANGUAGES> getLanguageLabel(String language)throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description:This function is getting data from menu Profile table for edit or update
	 * @param      : strMenuProfile
	 * @return     :List<MENUPROFILEHEADER>
	 * @throws     :JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 27 2014
	 */
	public List<MENUPROFILEHEADER> getMenuProfileList(String strMenuProfile)throws JASCIEXCEPTION;
	
	/** 
	 * @Description :This function is getting menu types from general code list
	 * @param 		:strMenuType
	 * @return      :List<GENERALCODES>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	public List<GENERALCODES> getMenuTypes(String strMenuType)throws JASCIEXCEPTION;
	
	
	
	
	/** 
	 * @Description :This function is getting menu App Icon from MenuApp Icon table
	 * @param 		:
	 * @return      :List<MENUAPPICONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 28 2014
	 */
	public List<MENUAPPICONS> getMenuAppIconList()throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This function is getting Menu Option from Menu Options table
	 * @param 		:MENUAPPICONS
	 * @return      :List<MENUOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	public List<MENUOPTIONS> getMenuOptionList(String strApplication,String strMenuType) throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description :This function is getting Menu Option from Menu Options table
	 * @param 		:
	 * @return      :List<MENUOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	public List<MENUOPTIONS> getMenuAllOptionList(String strMenuType) throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This function is getting Menu Profile List from Menu Profile Header table
	 * @param 		:
	 * @return      :List<MENUPROFILEHEADER>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	public List<MENUPROFILEHEADER> getMenuProfileDisplayAll()throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This Method is used for save or update Menu Profile List
	 * @param 		:
	 * @return      :List<MENUPROFILEHEADER>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 29 2014
	 */
	public Boolean SaveOrUpdate(MENUPROFILEHEADER objMenuprofileheader)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This Method is used for checking menu profile in MenuProfile option table
	 * @param 		:strMenuProfile
	 * @return      :List<MENUPROFILEOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 30 2014
	 */
	public List<Object> CheckDeleteMenuProfile(String strMenuProfile,String strTenant)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This Method is used for delete menu profile 
	 * @param 		:strMenuProfile
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :Dec 30 2014
	 */
	public Boolean deleteMenuProfile(String strMenuProfile,String strTenant)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description :This Method is used for delete menu profile 
	 * @param 		:strMenuProfile
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby:Sarvendra tyagi
	 * @Date	   :JAN 11 2014
	 */
	public Boolean deleteMenuProfileOption(String strMenuProfile,String strTenant)throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description :This Method is used for saved assigned menu option in menuprofile header 
	 * @param 		:MENUPROFILEOPTIONS objMenuprofileoptions
	 * @return      :MENUPROFILEHEADER
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby :Sarvendra tyagi
	 * @Date	    :Dec 30 2014
	 */
	public Boolean saveAssignedMenuProfile(MENUPROFILEOPTIONS objMenuprofileoptions)throws JASCIEXCEPTION;
	

	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
 public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION;
 
 
 /** 
	 * @Description :This Method is used for get assigned menu option in menuprofile Option table 
	 * @param 		:String Tenant, String strMenuProfile
	 * @return      :List<MENUOPTIONS>
	 * @throws 		:JASCIEXCEPTION
	 * @Developedby :Sarvendra tyagi
	 * @Date	    :Dec 31 2014
	 */
public List<MENUOPTIONS> getAssignedMenurofile(String Tenant, String strMenuProfile) throws JASCIEXCEPTION;

 
/** 
 * @Description :This Method is used for get Display all after removing assigned menu option in menuprofile Option table 
 * @param 		:String Tenant, String strMenuProfile
 * @return      :List<MENUOPTIONS>
 * @throws 		:JASCIEXCEPTION
 * @Developedby :Sarvendra tyagi
 * @Date	    :JAN 10 2015
 */
public List<MENUOPTIONS> getAssignedMenuProfileDispalyAll(String Tenant, String strMenuProfile,String strMenuType) throws JASCIEXCEPTION;


/** 
 * @Description :This Method is used for get Display all after removing assigned menu option in menuprofile Option table 
 * @param 		:String Tenant, String strMenuProfile
 * @return      :List<MENUOPTIONS>
 * @throws 		:JASCIEXCEPTION
 * @Developedby :Sarvendra tyagi
 * @Date	    :JAN 10 2015
 */
public List<MENUOPTIONS> getAssignedMenuProfileDispalyApplication(String Tenant, String strMenuProfile,String strMenuType,String strApplication) throws JASCIEXCEPTION;


/** 
 * @Description :This Method is used for get image url from app icon table 
 * @param 		:String appIcon
 * @return      :List<MENUOPTIONS>
 * @throws 		:JASCIEXCEPTION
 * @Developedby :Sarvendra tyagi
 * @Date	    :JAN 10 2015
 */
public List<Object> getAppIConURl(String appIcon) throws JASCIEXCEPTION;


 
	
}
