/*

Date Developed  Sep 18 2014
Description it is a interface where we define the function that implement in class of MENUMESSAGEDAOIMPL
Created By Aakash Bishnoi 
Created Date Dec 15 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.util.*;

import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;

import com.jasci.exception.JASCIEXCEPTION;




public interface IMENUMESSAGEDAO {

/**
 * 
 * @author Aakash Bishnoi
 * @Date Dec 15, 2014
 * Description This is used to get the list of MenuMessages  from MenuMessage Table
 * @param StrTenant_ID
 * @param StrCompany_ID
 * @param StrStatus
 * @throws JASCIEXCEPTION
 * @Return List<MENUMESSAGESBEAN>
 */
 public List<MENUMESSAGES> getList(String StrTenant_ID,String StrCompany_ID,String StrStatus)throws JASCIEXCEPTION;
 
 /**
  * @author Aakash Bishnoi
  * @Date Dec 16, 2014
  * Description This is used to fetch the list of MenuMessages on the behalf of Input Parameter from MenuMessage Table
  * @param StrTenant_ID
  * @param StrCompany_ID
  * @param StrStatus
  * @return MENUMESSAGESBEAN
  * @throws JASCIEXCEPTION
  */
 public MENUMESSAGES fetchMenuMessages(String StrTenant_ID,String StrCompany_ID,String StrStatus,String Strtype,String StrMessage_ID) throws JASCIEXCEPTION;
 /* public  List<LANGUAGES> SetMenuMessageLabels(String StrLanguage)throws JASCIEXCEPTION;*/
 
 /**
  *	@author Aakash Bishnoi
  * @Date Dec 16, 2014
  * Description this is used to Update the Menu messages
  * @param ObjectMenuMessages
  * @throws JASCIEXCEPTION
  */
 public String updateEntry(MENUMESSAGES ObjectMenuMessages) throws JASCIEXCEPTION ;
 

 /**
  *	@author Aakash Bishnoi
  * @Date Dec 17, 2014
  * Description this is used to Add the Menu messages
  * @param ObjectMenuMessages
  * @throws JASCIEXCEPTION
  */
 public String addEntry(MENUMESSAGES ObjectMenuMessages) throws JASCIEXCEPTION ;
 
 /**
  * @author Aakash Bishnoi
  * @Date Dec 17, 2014
  * Description this is used to get the List of Team Member Company
  * @param StrTenant
  * @param StrTeamMember
  * @return List<TEAMMEMBERCOMPANIESBEAN>
  * @throws JASCIEXCEPTION
  */
 public List<COMPANIES>  getCompanyList(String StrTenant,String StrTeamMember) throws JASCIEXCEPTION;
 
/**
 * @author Aakash Bishnoi
 * @Date Dec 18, 2014
 * Description this is used to check the uniqueness of Ticket Tape Message and Message
 * @param Tenant_ID
 * @param Company_ID
 * @param StrStatus
 * @param TicketTapeMessage
 * @param Message
 * @return MENUMESSAGES
 * @throws JASCIEXCEPTION
 */
 public MENUMESSAGES CheckTicketTypeMessage(String Tenant_ID,String Company_ID,String StrStatus,String TicketTapeMessage,String Message) throws JASCIEXCEPTION;
	
 /**
  * @author Aakash Bishnoi
  * @Date Dec 19, 2014
  * Description this is used to delete the selected menu messages
  * @param model
  * @return Boolean
  * @throws JASCIEXCEPTION
  */
 public Boolean deleteEntry(String Message_ID,String Tenant_ID,String Company_ID,String Status) throws JASCIEXCEPTION;



	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 19, 2014
	 * Description this is used to set language and get screen labels
	 * @param StrLanguage
	 * @return List<LANGUAGES>
	 * @throws JASCIEXCEPTION
	 */
 public  List<LANGUAGES> getMenuMessagesLabels(String StrLanguage) throws JASCIEXCEPTION;
}


