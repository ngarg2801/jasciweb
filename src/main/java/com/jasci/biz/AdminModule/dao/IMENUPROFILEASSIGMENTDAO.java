/**

Date Developed :JAN 13 2014
Created by: sarvendra tyagi
Description :Menu Profile Assigment Dao interface
 */

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.MENUPROFILEASSIGNMENT;
import com.jasci.exception.JASCIEXCEPTION;



public interface IMENUPROFILEASSIGMENTDAO {
	
	/** 
	 * @Description  :This function is used for getting label of Menu Profile maintenance screen
	 * @param        :strMenuTypes
	 * @return       :objMENUPROFILEMAINTENANCELABELBE 
	 * @throws       :JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	
	public List<Object> serchByPartOfTeamMemberName(String partTeamMemberName) throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description  : This function is used for getting Team Member list
	 * @param        : String TeamMemberID,String searchField
	 * @return       : TEAMMEMBERS
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	
	public List<Object> getTeamMemberData(String TeamMemberID)throws JASCIEXCEPTION;

	
	
	/** 
	 * @Description  : This function is used for getting all Team Member list
	 * @param        : 
	 * @return       : TEAMMEMBERS
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 13 2015
	 */
	
	public List<Object> getTeamMemberDisplayAll()throws JASCIEXCEPTION;

	
	

	/** 
	 * @Description  : This Method is used for getting list of menu profile which assigned to team member
	 * @param        : strMenuTypes
	 * @return       : objMENUPROFILEMAINTENANCELABELBE 
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	
	public List<Object> getTeamMemberAssignedProfileList(String teamMemberID)throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description  : This Method is used for getting list of menu profile based on menu type
	 * @param        :String menuType
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	
	public List<Object> getMenuProfileList(String menuType)throws JASCIEXCEPTION;
	
	
	/** 
	 * @Description  : This Method is used for getting all list of menu profile based on menu type
	 * @param        :
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 16 2015
	 */
	
	public List<Object> getAllMenuProfileList()throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description  : This Method is used for getting all list of menu profile based on assigned Menu Profile To team member
	 * @param        :String strTeamMemberID
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public List<Object> getAllMenuProfileListLeftMenu(String strTeamMemberID)throws JASCIEXCEPTION;
	
	
	

	/** 
	 * @Description  : This Method is used for getting all list of menu profile based on assigned Menu Profile To team member
	 * @param        :String strTeamMemberID,String StrMenuType
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public List<Object> getAllMenuProfileListMenu(String strTeamMemberID,String StrMenuType)throws JASCIEXCEPTION;
	
	/** 
	 * @Description  : This Method is used for assigned menu profile from menu profile Assignmenttable
	 * @param        :String strTeamMemberID
	 * @return       : Object
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public Boolean getAllAssignedMenuProfileDelete(String strTeamMemberID)throws JASCIEXCEPTION;
	
	
	
	/** 
	 * @Description  : This Method is used for assigned menu profile saved records in Menu Profile assignment table
	 * @param        :objMenuprofileassignment
	 * @return       : Boolean
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public Boolean getSaveAndUpdate(MENUPROFILEASSIGNMENT objMenuprofileassignment)throws JASCIEXCEPTION;
	

	/** 
	 * @Description  : This Method is used to get the name of team member
	 * @param        :TeamMemberID
	 * @return       : List<TEAMMEMBERS>
	 * @throws       : JASCIEXCEPTION
	 * @Developed by : Sarvendra Tyagi
	 * @Date		 : Jan 18 2015
	 */
	
	public List<Object> getTeamMemberName(String TeamMemberID)throws JASCIEXCEPTION;
	
	
}
