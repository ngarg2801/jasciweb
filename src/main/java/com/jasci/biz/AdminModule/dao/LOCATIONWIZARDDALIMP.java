/**

Description This class provide all of the functionality related to database for Location Wizard screen 
Created By Pradeep Kumar  
Created Date Jan 05 2015
 */
package com.jasci.biz.AdminModule.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.INVENTORY_LEVELS;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONPK;
import com.jasci.biz.AdminModule.model.LOCATIONPROFILES;
import com.jasci.biz.AdminModule.model.LOCATIONREPLENISHMENT;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARD;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARDPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class LOCATIONWIZARDDALIMP implements ILOCATIONWIZARDDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	public COMMONSESSIONBE OBJCOMMONSESSIONBE;

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getLocationWizard()
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 5, 2015
	 * @Description :Implements query for Search wizard Control number
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<Object> getSearchBYWizardControlNumber(String wizardID, String wizardControlNumber)
			throws JASCIEXCEPTION {
		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		String StrCompany = OBJCOMMONSESSIONBE.getCompany();

		
		List<Object> LocationWizardList = null;
		try {
			String  Location_Wizard_Query_WizardControlNo_Search = GLOBALCONSTANT.Location_Wizard_Query_WizardControlNo_Search_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_Query_WizardControlNo_Search_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_Query_WizardControlNo_Search_Third;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_Wizard_Query_WizardControlNo_Search);

			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.WIZARDCONTROL, wizardControlNumber.toUpperCase());

			LocationWizardList = OBJ_All_New.list();

			return LocationWizardList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return LocationWizardList;
		}

	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getSearchByArea(java.lang.String,
	 *      java.lang.String)
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description : Implements query for Search Area
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<Object> getSearchByArea(String wizardID, String Area) throws JASCIEXCEPTION {
		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		String StrCompany = OBJCOMMONSESSIONBE.getCompany();

		List<Object> LocationWizardList = null;
		try {
			String  Location_Wizard_QueryFetchDataForAreaSearch = GLOBALCONSTANT.Location_Wizard_QueryFetchDataForAreaSearch_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForAreaSearch_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForAreaSearch_Third;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_Wizard_QueryFetchDataForAreaSearch);

			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, Area.toUpperCase());

			LocationWizardList = OBJ_All_New.list();

			return LocationWizardList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return LocationWizardList;
		}

	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getSearchByLocationType(java.lang.String,
	 *      java.lang.String)
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description :Implements query for Search Location Type
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<Object> getSearchByLocationType(String wizardID, String LocationType) throws JASCIEXCEPTION {
		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		String StrCompany = OBJCOMMONSESSIONBE.getCompany();

		List<Object> LocationWizardList = null;
		try {
			String  Location_Wizard_QueryFetchDataForLocationTypeSearch = GLOBALCONSTANT.Location_Wizard_QueryFetchDataForLocationTypeSearch_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForLocationTypeSearch_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForLocationTypeSearch_Third;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_Wizard_QueryFetchDataForLocationTypeSearch);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THIRTEEN, LocationType.toUpperCase());

			LocationWizardList = OBJ_All_New.list();

			return LocationWizardList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return LocationWizardList;
		}

	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getSearchByAnyPartofWizardDiscription(java.lang.String,
	 *      java.lang.String)
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 12, 2015
	 * @Description :Used for Search By Any part of discription
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<Object> getSearchByAnyPartofWizardDiscription(String wizardID, String AnyPartofWizardDiscription)
			throws JASCIEXCEPTION {
		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		String StrCompany = OBJCOMMONSESSIONBE.getCompany();

		String AnyPartOfWizardDiscription = "AnyPartOfWizardDiscription";

		List<Object> LocationWizardList = null;
		try {
			String  Location_Wizard_QueryFetchDataForWizardDiscription = GLOBALCONSTANT.Location_Wizard_QueryFetchDataForWizardDiscription_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForWizardDiscription_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForWizardDiscription_Third;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_Wizard_QueryFetchDataForWizardDiscription);
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrCompany.toUpperCase());
			
			OBJ_All_New.setParameter(AnyPartOfWizardDiscription, GLOBALCONSTANT.StrPercentSign
					+ AnyPartofWizardDiscription.toUpperCase() + GLOBALCONSTANT.StrPercentSign);
			LocationWizardList = OBJ_All_New.list();
			// System.out.println("Stop");
			return LocationWizardList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return LocationWizardList;

		}

	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getDropDownValuesList(java.lang.String)
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 * @Description :Implements query for Drop Downs value list
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
	public List<GENERALCODES> getDropDownValuesList(String GeneralCodeId) throws JASCIEXCEPTION {
		
		List<GENERALCODES> objGeneralcodes = null;
		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		String StrCompany = OBJCOMMONSESSIONBE.getCompany();

		try {

			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(
					GLOBALCONSTANT.Location_Wizard_QueryNameFetchDataForSelect);

			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.toUpperCase());

			objGeneralcodes = ObjQuery.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception obException) {
			throw new JASCIEXCEPTION(obException.getMessage());
	}
		return objGeneralcodes;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getDropDownValueFulfillmentList()
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 9, 2015
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
	public List<FULFILLMENTCENTERSPOJO> getDropDownValueFulfillmentList() throws JASCIEXCEPTION {

		List<FULFILLMENTCENTERSPOJO> objFulfillmentpojo = null;

		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		try {

			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(
					GLOBALCONSTANT.Location_Wizard_QueryNameFetchFulfillment);

			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, GLOBALCONSTANT.Location_Wizard_Fulfilment_Status);

			objFulfillmentpojo = ObjQuery.list();

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception obException) {

			throw new JASCIEXCEPTION(obException.getMessage());

		}
		return objFulfillmentpojo;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getLocationWizardData()
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 12, 2015
	 * @Description :get data of location wizard on grid
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<Object> getLocationWizardData(String wizardID) throws JASCIEXCEPTION {
		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		String StrCompany = OBJCOMMONSESSIONBE.getCompany();

		List<Object> WizardlOCObjects = null;
		try {
					
			String  Location_Wizard_QueryFetchDataForGrid = GLOBALCONSTANT.Location_Wizard_QueryFetchDataForGrid_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForGrid_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.Location_Wizard_QueryFetchDataForGrid_Third;
			Query OBJ_All_New = sessionFactory.getCurrentSession().createSQLQuery(Location_Wizard_QueryFetchDataForGrid);

			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWO, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_THREE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FOUR, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_FIVE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SIX, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_SEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_EIGHT, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_NINE, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TEN, StrCompany.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_ELEVEN, StrTenant.toUpperCase());
			OBJ_All_New.setParameter(GLOBALCONSTANT.INT_TWELVE, StrCompany.toUpperCase());
			WizardlOCObjects = OBJ_All_New.list();
			return WizardlOCObjects;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return WizardlOCObjects;
		}
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#saveAndUpdate(com.jasci.biz.AdminModule.model.LOCATIONWIZARD)
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 13, 2015
	 * @Description :implements query for update and save data
	 */
	public LOCATIONWIZARD saveAndUpdate(LOCATIONWIZARD objLocationWizardbe,boolean saveorupdate) throws JASCIEXCEPTION {

		Session session = sessionFactory.getCurrentSession();
		objLocationWizardbe.setAISLE_CHARACTER_SET(trimValues(objLocationWizardbe.getAISLE_CHARACTER_SET()));
		objLocationWizardbe.setAISLE_NUMBER_CHARACTERS(trimValues(objLocationWizardbe.getAISLE_NUMBER_CHARACTERS()));
		objLocationWizardbe.setAISLE_NUMBER_OF(trimValues(objLocationWizardbe.getAISLE_NUMBER_OF()));
		objLocationWizardbe.setAISLE_STARTING(trimValues(objLocationWizardbe.getAISLE_STARTING()));
		objLocationWizardbe.setAREA(trimValues(objLocationWizardbe.getAREA()));
		objLocationWizardbe.setBAY_CHARACTER_SET(trimValues(objLocationWizardbe.getBAY_CHARACTER_SET()));
		objLocationWizardbe.setBAY_NUMBER_CHARACTERS(trimValues(objLocationWizardbe.getBAY_NUMBER_CHARACTERS()));
		objLocationWizardbe.setBAY_NUMBER_OF(trimValues(objLocationWizardbe.getBAY_NUMBER_OF()));
		objLocationWizardbe.setBAY_STARTING(trimValues(objLocationWizardbe.getBAY_STARTING()));
		objLocationWizardbe.setCREATED_BY_TEAM_MEMBER(trimValues(objLocationWizardbe.getCREATED_BY_TEAM_MEMBER()));
		objLocationWizardbe.setDESCRIPTION20(trimValues(objLocationWizardbe.getDESCRIPTION20()));
		objLocationWizardbe.setDESCRIPTION50(trimValues(objLocationWizardbe.getDESCRIPTION50()));
		objLocationWizardbe.setLEVEL_CHARACTER_SET(trimValues(objLocationWizardbe.getLEVEL_CHARACTER_SET()));
		objLocationWizardbe.setLEVEL_NUMBER_CHARACTERS(trimValues(objLocationWizardbe.getLEVEL_NUMBER_CHARACTERS()));
		objLocationWizardbe.setLEVEL_NUMBER_OF(trimValues(objLocationWizardbe.getLEVEL_NUMBER_OF()));
		objLocationWizardbe.setLEVEL_STARTING(trimValues(objLocationWizardbe.getLEVEL_STARTING()));
		objLocationWizardbe.setLOCATION_PROFILE(trimValues(objLocationWizardbe.getLOCATION_PROFILE()));
		objLocationWizardbe.setROW_CHARACTER_SET(trimValues(objLocationWizardbe.getROW_CHARACTER_SET()));
		objLocationWizardbe.setROW_NUMBER_CHARACTERS(trimValues(objLocationWizardbe.getROW_NUMBER_CHARACTERS()));
		objLocationWizardbe.setROW_NUMBER_OF(trimValues(objLocationWizardbe.getROW_NUMBER_OF()));
		objLocationWizardbe.setROW_STARTING(trimValues(objLocationWizardbe.getROW_STARTING()));
		objLocationWizardbe.setSEPARATOR_AISLE_BAY(trimValues(objLocationWizardbe.getSEPARATOR_AISLE_BAY()));
		objLocationWizardbe.setSEPARATOR_AISLE_BAY_PRINT(trimValues(objLocationWizardbe.getSEPARATOR_AISLE_BAY_PRINT()));
		objLocationWizardbe.setSEPARATOR_BAY_LEVEL(trimValues(objLocationWizardbe.getSEPARATOR_BAY_LEVEL()));
		objLocationWizardbe.setSEPARATOR_BAY_LEVEL_PRINT(trimValues(objLocationWizardbe.getSEPARATOR_BAY_LEVEL_PRINT()));
		objLocationWizardbe.setSEPARATOR_LEVEL_SLOT(trimValues(objLocationWizardbe.getSEPARATOR_LEVEL_SLOT()));
		objLocationWizardbe.setSEPARATOR_LEVEL_SLOT_PRINT(trimValues(objLocationWizardbe.getSEPARATOR_LEVEL_SLOT_PRINT()));
		objLocationWizardbe.setSEPARATOR_ROW_AISLE(trimValues(objLocationWizardbe.getSEPARATOR_ROW_AISLE()));
		objLocationWizardbe.setSEPARATOR_ROW_AISLE_PRINT(trimValues(objLocationWizardbe.getSEPARATOR_ROW_AISLE_PRINT()));
		objLocationWizardbe.setSLOT_CHARACTER_SET(trimValues(objLocationWizardbe.getSLOT_CHARACTER_SET()));
		objLocationWizardbe.setSLOT_NUMBER_CHARACTERS(trimValues(objLocationWizardbe.getSLOT_NUMBER_CHARACTERS()));
		objLocationWizardbe.setSLOT_NUMBER_OF(trimValues(objLocationWizardbe.getSLOT_NUMBER_OF()));
		objLocationWizardbe.setSLOT_STARTING(trimValues(objLocationWizardbe.getSLOT_STARTING()));
		objLocationWizardbe.setSTATUS(trimValues(objLocationWizardbe.getSTATUS()));
		objLocationWizardbe.setWORK_GROUP_ZONE(trimValues(objLocationWizardbe.getWORK_GROUP_ZONE()));
		objLocationWizardbe.setWORK_ZONE(trimValues(objLocationWizardbe.getWORK_ZONE()));
		try {
			LOCATIONWIZARD objLocationWizard=null;
			if(saveorupdate)
			{	
				objLocationWizard=getMaxWizardControlNumber(objLocationWizardbe);
			    session.save(objLocationWizard);
			}else{
				session.saveOrUpdate(objLocationWizardbe);
			}
            return objLocationWizard;
		}catch (HibernateException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception objException) {

			throw new JASCIEXCEPTION(objException.getMessage());
		}

	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#saveAndUpdate(com.jasci.biz.AdminModule.model.LOCATION)
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 26, 2015
	 * @Description :implements query for and save data
	 */

	public List<Object> saveLocations(List<String> locationList, LOCATIONWIZARD objLocationWizard,List<Object> combineList,String DuplicateLocationFileName)
			throws JASCIEXCEPTION {

		Session session = sessionFactory.getCurrentSession();
		long savedRecordList =  (Long) combineList.get(GLOBALCONSTANT.INT_ZERO);
		long duplicateRecordList =  (Long) combineList.get(GLOBALCONSTANT.IntOne);
		for (String location : locationList) {
			LOCATION objLocation = new LOCATION();
			LOCATIONPK objLocationpk = new LOCATIONPK();
			try {

				objLocationpk.setArea(objLocationWizard.getAREA().trim());
				objLocationpk.setLocation(location.trim());
				objLocationpk.setTenant_ID(OBJCOMMONSESSIONBE.getTenant().trim());
				objLocationpk.setFullfillment_Center_ID(objLocationWizard.getId().getFULFILLMENT_CENTER_ID().trim());
				objLocationpk.setCompany_ID(OBJCOMMONSESSIONBE.getCompany().trim());
				objLocation.setId(objLocationpk);
				objLocation.setLocation_Profile(objLocationWizard.getLOCATION_PROFILE().trim());
				objLocation.setLocation_Type(objLocationWizard.getLOCATION_TYPE().trim());
				objLocation.setWizard_Control_Number(objLocationWizard.getId().getWIZARD_CONTROL_NUMBER());
				objLocation.setWizard_ID(objLocationWizard.getId().getWIZARD_ID().trim());
				objLocation.setDescription20(objLocationWizard.getDESCRIPTION20().trim());
				objLocation.setDescription50(objLocationWizard.getDESCRIPTION50().trim());

				objLocation.setWork_Group_Zone(objLocationWizard.getWORK_GROUP_ZONE().trim());
				objLocation.setWork_Zone(objLocationWizard.getWORK_ZONE().trim());
				objLocation.setLocation_Height(0.0);
				objLocation.setLocation_Width(0.0);
				objLocation.setLocation_Depth(0.0);
				objLocation.setLocation_Weight_Capacity(0.0);
				objLocation.setLocation_Weight_Capacity(0.0);
				objLocation.setNumber_Pallets_Fit(0l);
				objLocation.setNumber_Floor_Locations(0l);
				objLocation.setLocation_Height_Capacity(0.0);
				objLocation.setFree_Prime_Days(0l);
				objLocation.setLast_Activity_Date(objLocationWizard.getLAST_ACTIVITY_DATE());
				objLocation.setLast_Activity_Team_Member(OBJCOMMONSESSIONBE.getTeam_Member());
				session.save(objLocation);
					session.flush();
					session.clear();
				savedRecordList++;

			} catch (Exception objException) {
				try {
					duplicateRecordList++;
					
					/**Write Duplicate location in a temp file*/
					FileWriterWithEncoding ObjfFileWriterWithEncoding = new FileWriterWithEncoding(DuplicateLocationFileName, GLOBALCONSTANT.UTF8,true);
					ObjfFileWriterWithEncoding.write(location+ GLOBALCONSTANT.NextLineRegex);
					ObjfFileWriterWithEncoding.flush();
					ObjfFileWriterWithEncoding.close();
					/**End file write*/
				} catch (Exception obj_e) {
					throw new JASCIEXCEPTION(obj_e.getMessage());
				}

			}
			
		}
		
		combineList.add(GLOBALCONSTANT.INT_ZERO, savedRecordList);
		combineList.add(GLOBALCONSTANT.IntOne, duplicateRecordList);
		
		return combineList;
	}
	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 
	 * @param
	 *@Description :Give Location profile drop down
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getDropDownValueLocationProfile()
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
	public List<LOCATIONPROFILES> getDropDownValueLocationProfile(String fulfillmentCenter) throws JASCIEXCEPTION {
		List<LOCATIONPROFILES> objLOCATIONPROFILES = null;

		String StrTenant = OBJCOMMONSESSIONBE.getTenant();
		String StrCompany = OBJCOMMONSESSIONBE.getCompany();

		try {

			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(
					GLOBALCONSTANT.Location_Wizard_Select_Location_Profile_Query);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, fulfillmentCenter.toUpperCase());

			objLOCATIONPROFILES = ObjQuery.list();

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch (Exception obException) {

			throw new JASCIEXCEPTION(obException.getMessage());

		}
		return objLOCATIONPROFILES;
	}

	/**
	 * 
	 * Created By:Pradeep Kumar
	 * 
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getLocationWizardCopyEdit(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String)
	 * @throws JASCIEXCEPTION
	 * @Developed Date:Jan 14, 2015
	 * @Description :get data for copy
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATIONWIZARD> getLocationWizardCopyEdit(String wizardid, String wizardControlNo, String fulfillment,
			String tenantId, String companyId, String copyOrEdit) throws JASCIEXCEPTION {

		List<LOCATIONWIZARD> LocationWizardList = null;
		List<LOCATIONWIZARD> LocationWizardList1 = new ArrayList<LOCATIONWIZARD>();
		try {
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(
					GLOBALCONSTANT.Location_Wizard_QueryNameFetchDataForCopy);

			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, wizardid.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, wizardControlNo.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, fulfillment.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, tenantId.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, companyId.toUpperCase());

			LocationWizardList = ObjQuery.list();
			if (copyOrEdit.equalsIgnoreCase(GLOBALCONSTANT.Copy)) {
				LocationWizardList1.add(getMaxWizardControlNumber(LocationWizardList.get(GLOBALCONSTANT.INT_ZERO)));
			}

			return LocationWizardList;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return LocationWizardList;
		}
	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 
	 * @Description : get max Wizard control number
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getMaxWizardControlNumber(com.jasci.biz.AdminModule.model.LOCATIONWIZARD)
	 */

	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public LOCATIONWIZARD getMaxWizardControlNumber(LOCATIONWIZARD objLocationwizard) throws JASCIEXCEPTION {

		Session session = sessionFactory.getCurrentSession();
		try {
			Query ObjQuery = session.createSQLQuery(GLOBALCONSTANT.Location_Wizard_QueryFetchMaxWizardControlNumber);

			List<Object> MaxWizardControlNumber = ObjQuery.list();

			long MaxWizardConNo = Long.parseLong(MaxWizardControlNumber.get(GLOBALCONSTANT.INT_ZERO).toString());

			MaxWizardConNo = MaxWizardConNo + GLOBALCONSTANT.IntOne;
			String wizardControlNumber = String.valueOf(MaxWizardConNo);
			LOCATIONWIZARDPK objPK = objLocationwizard.getId();
			objPK.setWIZARD_CONTROL_NUMBER(Long.parseLong(wizardControlNumber));

			objLocationwizard.setId(objPK);

			return objLocationwizard;
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return objLocationwizard;
		}
	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 
	 * @Description :check location for entry
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#checkLocationforDeletion(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String)
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATION> checkLocationforDeletion(String wizardid, String wizardControlNo, String fulfillment,
			String tenantId, String companyId) throws JASCIEXCEPTION {

		List<LOCATION> LocationList = null;

		try {
			
			List<LOCATIONWIZARD> locationWizardList=getLocationWizardCopyEdit(wizardid, wizardControlNo, fulfillment, OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.Blank);
			LOCATIONWIZARD locationWizardListValue=locationWizardList.get(GLOBALCONSTANT.INT_ZERO);
			
			Query ObjQuery = sessionFactory.getCurrentSession().createSQLQuery(
					GLOBALCONSTANT.Location_Wizard_QueryCheckDeletion);
			
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_FULFILLMENTCENTERID, fulfillment.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_TENANT, tenantId.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_COMPANY, companyId.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_AREA,locationWizardListValue.getAREA().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_FULFILLMENTCENTERID1, fulfillment.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_TENANT1, tenantId.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_COMPANY1, companyId.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_AREA1,locationWizardListValue.getAREA().toUpperCase());
			
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_WIZARDID,wizardid.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_WIZARDCONTROLNUMBER,wizardControlNo);

			LocationList=ObjQuery.list();
			
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return LocationList;
		}
	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 
	 * @Description :delete wizard only
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#deleteWizardOnly(com.jasci.biz.AdminModule.model.LOCATIONWIZARDPK)
	 */
	public boolean deleteWizardOnly(LOCATIONWIZARDPK objLocationWizard) throws JASCIEXCEPTION {

		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();
		Boolean status = false;

		try {
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(
					GLOBALCONSTANT.Location_Wizard_QueryNameUpdateLocations);

			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objLocationWizard.getTENANT_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objLocationWizard.getFULFILLMENT_CENTER_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, objLocationWizard.getCOMPANY_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, objLocationWizard.getWIZARD_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, objLocationWizard.getWIZARD_CONTROL_NUMBER());
			ObjQuery.executeUpdate();
			LOCATIONWIZARD objLocationwizardPOJO = new LOCATIONWIZARD();
			objLocationwizardPOJO.setId(objLocationWizard);
			session.delete(objLocationwizardPOJO);
			TransactionObject.commit();
			session.close();
			status = true;

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception objException) {
			TransactionObject.rollback();
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return status;
	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 
	 * @Description :check inventry level list
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#checkListInventryLevels(com.jasci.biz.AdminModule.model.LOCATIONPK)
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<INVENTORY_LEVELS> checkListInventryLevels(LOCATIONPK objLocation) throws JASCIEXCEPTION {

		List<INVENTORY_LEVELS> objINVENTORY_LEVELS = null;

		try {
			Query ObjQuery = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.Location_Wizard_QueryFetchListInventoryLevel);
			
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objLocation.getTenant_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objLocation.getCompany_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, objLocation.getFullfillment_Center_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, objLocation.getArea().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, objLocation.getLocation().toUpperCase());

			objINVENTORY_LEVELS = ObjQuery.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return objINVENTORY_LEVELS;
		}

	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 
	 * @Description :give list of inventry level
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#ListInventryLevels(com.jasci.biz.AdminModule.model.LOCATIONPK)
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<INVENTORY_LEVELS> ListInventryLevels(LOCATIONPK objLocation) throws JASCIEXCEPTION {

		List<INVENTORY_LEVELS> objINVENTORY_LEVELS = null;

		try {
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(
					GLOBALCONSTANT.Location_Wizard_QueryNameFetchListALLInventoryLevel);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objLocation.getTenant_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objLocation.getCompany_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, objLocation.getFullfillment_Center_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, objLocation.getArea().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, objLocation.getLocation().toUpperCase());

			objINVENTORY_LEVELS = ObjQuery.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		} finally {
			return objINVENTORY_LEVELS;
		}

	}
	
	/**
	 * 
		 * 
		 * @author: Pradeep Kumar
		 * @Date :Jan 20, 2015
		 * @Description :Delete wizard from only
		 * @see :@see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#deleteWizardLocation(com.jasci.biz.AdminModule.model.LOCATIONWIZARD)
	 */
	public boolean deleteWizardLocation(LOCATIONWIZARD objLocationwizard) throws JASCIEXCEPTION {
		boolean status = false;

		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();
		session.delete(objLocationwizard);
		TransactionObject.commit();
		session.close();
		status = true;
		return status;
	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 Description :delete wizard and location
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#deleteWizardAndLocation(com.jasci.biz.AdminModule.model.LOCATION)
	 */
	
	public boolean deleteWizardAndLocation(String wizardid,String wizardControlNo,String fulfillmentId) throws JASCIEXCEPTION {
		boolean status = false;
	
		try{
			
			List<LOCATIONWIZARD> locationWizardList=getLocationWizardCopyEdit(wizardid, wizardControlNo, fulfillmentId, OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.Blank);
			LOCATIONWIZARD locationWizardListValue=locationWizardList.get(GLOBALCONSTANT.INT_ZERO);
			
			
			Query query=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.LocationWizard_ProcedureCallQuery_DeleteAndLocation).
					setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_TENANT,OBJCOMMONSESSIONBE.getTenant().toUpperCase())
					.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_FULFILLMENTCENTERID,fulfillmentId.toUpperCase())
					.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_AREA,locationWizardListValue.getAREA().toUpperCase())
					.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_COMPANY,OBJCOMMONSESSIONBE.getCompany().toUpperCase())
					.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_WIZARDID,wizardid.toUpperCase())
					.setParameter(GLOBALCONSTANT.LocationWizard_ProcedureCall_WIZARDCONTROLNUMBER,wizardControlNo);
			query.executeUpdate();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception obj){
			
		}
		return status;
	}
	
	/**
	 * * @author: Pradeep Kumar
	 * 
	 * @Date :Jan 20, 2015 Description :delete deleteWizard
	 */

	public boolean deleteWizard(LOCATIONWIZARD objLocationwizard) throws JASCIEXCEPTION {
		boolean status = false;
		try {
			Session session = sessionFactory.openSession();
			Transaction TransactionObject = session.beginTransaction();
			session.delete(objLocationwizard);
			TransactionObject.commit();
			session.close();
			status = true;

			status = true;
		} catch (Exception obj) {
			obj.printStackTrace();
		}
		return status;
	}

	/**
	 * * @author: Pradeep Kumar
	 * 
	 * @Date :Jan 20, 2015 Description :delete LocationReplenishment
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#deleteLocationReplenishment
	 */
	public boolean deleteLocationReplenishment(LOCATIONREPLENISHMENT objLocationReplenishment) throws JASCIEXCEPTION {
		
		boolean status = false;

		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();
		session.delete(objLocationReplenishment);
		TransactionObject.commit();
		session.close();
		status = true;
		return status;
	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 Description :delete inventy level entry
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#deleteInventoryLevel(com.jasci.biz.AdminModule.model.INVENTORY_LEVELS)
	 */
	public boolean deleteInventoryLevel(INVENTORY_LEVELS objINVENTORY_LEVELS) throws JASCIEXCEPTION {
		
		boolean status = false;

		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();
		session.delete(objINVENTORY_LEVELS);
		TransactionObject.commit();
		session.close();
		status = true;
		return status;

	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :Jan 20, 2015 Description :give no of location for particular
	 *       wizard entry
	 * @param :LOCATIONWIZARDPK
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getNumberOfLocation(com.jasci.biz.AdminModule.model.LOCATIONWIZARDPK)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
	public long getNumberOfLocation(LOCATIONWIZARDPK objLocationWizard) throws JASCIEXCEPTION {

		Session session = sessionFactory.openSession();
		long locationsNo;

		try {
			Query ObjQuery = sessionFactory.getCurrentSession().createSQLQuery(
					GLOBALCONSTANT.Location_Wizard_QueryNumberOfLocations);

			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objLocationWizard.getWIZARD_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objLocationWizard.getWIZARD_CONTROL_NUMBER());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, objLocationWizard.getFULFILLMENT_CENTER_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, objLocationWizard.getTENANT_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, objLocationWizard.getCOMPANY_ID().toUpperCase());

			List<Object> numberOfLocations = ObjQuery.list();

			locationsNo = Long.parseLong(numberOfLocations.get(GLOBALCONSTANT.INT_ZERO).toString());

			session.close();

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		} 
		catch (Exception objException) {

			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return locationsNo;
	}
	/**
	 * 
		 * 
		 * @author: Pradeep Kumar
		 * @Date :Jan 20, 2015
		 * @Description : get fulfillment List
		 * @see :@see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#getFulfillmentList(com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
	public List<FULFILLMENTCENTERSPOJO> getFulfillmentList(FULFILLMENTCENTERS_PK objfulfillPK) throws JASCIEXCEPTION {

		Session session = sessionFactory.openSession();
		List<FULFILLMENTCENTERSPOJO> FulfillmentList = null;

		try {
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(
					GLOBALCONSTANT.Location_Wizard_Fulfillment_Query);

			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objfulfillPK.getTenant().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objfulfillPK.getFulfillmentCenter().toUpperCase());

			FulfillmentList = ObjQuery.list();

			session.close();

		}catch (SQLGrammarException objGrammarException){
			session.close();
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch (Exception objException) {

			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return FulfillmentList;
	}

	/**
	 * @Description get team member name form team members table based on team
	 *              member id
	 * @param Tenant
	 * @param Company
	 * @param TeamMember
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked )
	public List<TEAMMEMBERS> getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {

		List<TEAMMEMBERS> objTeammembers = null;
		try {
			objTeammembers = sessionFactory.getCurrentSession()
					.createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query)
					.setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase())
					.list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}  
		catch (Exception ObjectException) {
			throw new JASCIEXCEPTION(ObjectException.getMessage());

		}

		if (objTeammembers.size() == GLOBALCONSTANT.INT_ZERO) {
			try {
				objTeammembers = sessionFactory.getCurrentSession()
						.createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query_By_TEAMID)
						.setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.trim().toUpperCase()).list();
			} catch (Exception ObjectException) {
				throw new JASCIEXCEPTION(ObjectException.getMessage());

			}
		}
		return objTeammembers;
	}

	/**
	 * 
	 * @author: Pradeep Kumar
	 * @Date :feb 25, 2015 Description :delete Wizard For Update
	 * @param
	 * @see com.jasci.biz.AdminModule.dao.ILOCATIONWIZARDDAO#deleteWizardOnly(com.jasci.biz.AdminModule.model.LOCATIONWIZARDPK)
	 */
	public boolean deleteWizardForUpdate(LOCATIONWIZARDPK objLocationWizard) throws JASCIEXCEPTION {

		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();
		Boolean status = false;

		try {
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(
					GLOBALCONSTANT.Location_Wizard_QueryNameUpdateLocations);

			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objLocationWizard.getTENANT_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objLocationWizard.getFULFILLMENT_CENTER_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, objLocationWizard.getCOMPANY_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, objLocationWizard.getWIZARD_ID().toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, objLocationWizard.getWIZARD_CONTROL_NUMBER());
			ObjQuery.executeUpdate();
			
			TransactionObject.commit();
			session.close();
			status = true;

		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}  
		catch (Exception objException) {
			TransactionObject.rollback();
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return status;
	}

	public static String trimValues(String Value){
		if(Value == null){
			return null;
		}
		else{
			String CheckValue=Value.trim();
			if(CheckValue.length()<GLOBALCONSTANT.IntOne){
				return null;
			}else{						
				return CheckValue;
			}
		}
	}
	
}
