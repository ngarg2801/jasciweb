/**
Date Developed  Dec 22 2014
Created By "Rahul Kumar"
Description : define an interface method to communicate with DataBase 
 */
package com.jasci.biz.AdminModule.dao;


import java.util.List;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.exception.JASCIEXCEPTION;


public interface ICOMPANYMAINTENANCEDAL {

	
	/**
	 * @Description get all companies based on tenant from companies table
	 * @author Rahul Kumar
	 * @Date Dec 22, 2014 
	 * @param Tenant
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIES>
	 */
	
	public List<COMPANIES> getCompanies(String Tenant) throws JASCIEXCEPTION;
	
	
	/**
	 * @Description set screen language based on team member language
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<LANGUAGES>
	 */
	public List<LANGUAGES> setScreenLanguage( String StrScreenName,String StrLanguage)throws  JASCIEXCEPTION;
	
	
	
	/**
	 * @Description get company by company id from companies table
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIES>
	 */
	public List<COMPANIES> getCompanyById(String Tenant,String CompanyId)throws JASCIEXCEPTION;
	
	
	
	/**
	 * @Description get company by company part name from companies table
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param Tenant
	 * @param CompanyPartName
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<COMPANIES>
	 */
	public List<COMPANIES> getCompanyByPartName(String Tenant,String CompanyPartName)throws JASCIEXCEPTION;
	
	/**
	 * @Description get general code based on general code id form general code table
	 * @author Rahul Kumar
	 * @Date Dec 24, 2014 
	 * @param Tenant
	 * @param Company
	 * @param GeneralCodeId
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<GENERALCODES>
	 */
	
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION;
	
	
	/**
	 * 
	 * @Description add or update company into companies table 
	 * @author Rahul Kumar
	 * @Date Dec 26, 2014 
	 * @param objCompanies
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type Boolean
	 */
	public Boolean addOrUpdateCompany(COMPANIES objCompanies) throws JASCIEXCEPTION;
	
	
	
	/**
	 * 
	 * @Description Delete existing company from table companies based on tenant and company id
	 * @author Rahul Kumar
	 * @Date Dec 29, 2014 
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @Return type Boolean
	 */
	public Boolean deleteCompany(String Tenant,String CompanyId) throws JASCIEXCEPTION;
	
	
	/**
	 * 
	 *@Description check unique email from table companies
	 *@Auther Rahul Kumar
	 *@Date Jan 20, 2015			
	 *@Return type Boolean
	 *@param Email
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	public Boolean isEmailUnique(String Email)throws JASCIEXCEPTION;

	
}
