/**
 * Date Developed  Dec 22 2014
 * Created By "Rahul Kumar"
 * Description : this class provide data base function of company entity
 */
package com.jasci.biz.AdminModule.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class COMPANYMAINTENANCEDALIMPL  implements ICOMPANYMAINTENANCEDAL{
	
	@Autowired
	private SessionFactory sessionFactory ;

	/**
	 * @Deascription get all companies for specific tenant 
	 * @author Rahul Kumar
	 * @Date Dec 22, 2014
	 * @param Tenant
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#getCompanies(java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<COMPANIES> getCompanies(String Tenant) throws JASCIEXCEPTION {
		List<COMPANIES> ObjListTeamMembers=null;
		try{
			ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Companies_Select_Tenant_All_Companies_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.trim().toUpperCase()).list();
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return ObjListTeamMembers;
	}

	/**
	 * @Description set screen label language based on team member language
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#setScreenLanguage(java.lang.String, java.lang.String)
	 */
	public List<LANGUAGES> setScreenLanguage(String StrScreenName, String StrLanguage) throws JASCIEXCEPTION {
		
		List<LANGUAGES> ObjListLanguages=GetLanguage(StrScreenName,StrLanguage);
		return ObjListLanguages;
	}
	
	

	/**
	 * @Description get screen label language based on team member language
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014 
	 * @param StrScreenName
	 * @param StrLanguage
	 * @return
	 * @throws JASCIEXCEPTION
	 * List<LANGUAGES>
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	private List<LANGUAGES> GetLanguage( String StrScreenName,String StrLanguage) throws JASCIEXCEPTION
	{
		try
		{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Languages_Query).setParameter(GLOBALCONSTANT.INT_ZERO,StrScreenName.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrLanguage.toUpperCase()).list();
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
	}

	/**
	 * @Description get company by company id from companies table 
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#getCompanyById(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<COMPANIES> getCompanyById(String Tenant, String CompanyId) throws JASCIEXCEPTION {
		
		List<COMPANIES> ObjListTeamMembers=null;
		try{
			ObjListTeamMembers=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Companies_GetCompanyById_NamedQuery).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, CompanyId.trim().toUpperCase()).list();
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return ObjListTeamMembers;
	}

	/**
	 * @Description get company by company part name from companies table
	 * @author Rahul Kumar
	 * @Date Dec 23, 2014
	 * @param Tenant
	 * @param CompanyPartName
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#getCompanyByPartName(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<COMPANIES> getCompanyByPartName(String Tenant, String CompanyPartName) throws JASCIEXCEPTION {
		
		List<COMPANIES> ObjListTeamMembers=null;
		try{
			ObjListTeamMembers=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Companies_GetCompanyByPartName_NamedQuery).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).
					setString(GLOBALCONSTANT.Companies_name2, GLOBALCONSTANT.StrPercentSign+CompanyPartName.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).setString(GLOBALCONSTANT.Companies_name5, GLOBALCONSTANT.StrPercentSign+CompanyPartName.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).list();
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return ObjListTeamMembers;
	}

	/**
	 * @Description get general code based on general code id form general code table
	 * @author Rahul Kumar
	 * @Date Dec 24, 2014
	 * @param Tenant
	 * @param Company
	 * @param GeneralCodeId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#getGeneralCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION {
		
				List<GENERALCODES> objGeneralcodes=null;

				try{
					objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Select_Conditional_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company.trim().toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.trim().toUpperCase()).list();
				}catch(SQLGrammarException objException){
					throw new JASCIEXCEPTION(objException.getMessage());
				}
				catch(Exception objException){
					//e.printStackTrace();
					throw new JASCIEXCEPTION(objException.getMessage());
				}
			return objGeneralcodes;
	}

	
	/**
	 * 
	 * @Description add or update company into companies table 
	 * @author Rahul Kumar
	 * @Date Dec 26, 2014 
	 * @param objCompanies
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#addOrUpdateCompany(com.jasci.common.utilbe.COMPANIESBE)
	 */
	public Boolean addOrUpdateCompany(COMPANIES objCompanies) throws JASCIEXCEPTION {
		
		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();	
		Boolean status=false;

		try{
			
			session.saveOrUpdate(objCompanies);
			TransactionObject.commit();
			session.close();
			status=true;

		}
		catch(Exception objException){
			
			TransactionObject.rollback();
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return status;
	}

	
	/**
	 * 
	 * @Description Delete company record based on tenant and company id 
	 * @author Rahul Kumar
	 * @Date Dec 29, 2014 
	 * @param Tenant
	 * @param CompanyId
	 * @return
	 * @throws JASCIEXCEPTION
	 * @see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#deleteCompany(java.lang.String, java.lang.String)
	 */
	public Boolean deleteCompany(String Tenant, String CompanyId) throws JASCIEXCEPTION {
		
		Session session = sessionFactory.openSession();
		Boolean Status=false;
		try{
			Query query = session.getNamedQuery(GLOBALCONSTANT.Companies_Delete_NamedQuery);
			query.setString(GLOBALCONSTANT.DataBase_Companies_Tenant, Tenant.trim().toUpperCase());
			query.setString(GLOBALCONSTANT.DataBase_Companies_Company, CompanyId.trim().toUpperCase());
			query.executeUpdate();
			Status=true;
			session.close();
		}catch(SQLGrammarException objException){
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception objException){
			session.close();
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return Status;
		
		
	}

	/**
	 *@Description check unique email from table companies
	 *@Auther Rahul Kumar
	 *@Date Jan 20, 2015			
	 *@param Email
	 *@return
	 *@throws JASCIEXCEPTION
	 *@see com.jasci.biz.AdminModule.dao.ICOMPANYMAINTENANCEDAL#isEmailUnique(java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public Boolean isEmailUnique(String Email) throws JASCIEXCEPTION {
		
		List<COMPANIES> objCompaniesList=new ArrayList<COMPANIES>();

		Boolean Status=false;
		try{

			objCompaniesList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Companies_Select_Email_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Email.trim().toUpperCase()).list();
		}catch(SQLGrammarException objException){
			
		}
		catch(Exception objException){

		}
		
		if(objCompaniesList.size()>GLOBALCONSTANT.INT_ZERO){
			Status=true;	
			}
		else{
			Status=false;
			}
		return Status;
	}

	

}
