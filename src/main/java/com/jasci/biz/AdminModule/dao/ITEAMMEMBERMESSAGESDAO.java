/**

Date Developed  Nov 16 2014
Created by:Diksha Gupta
Description It is used to interact with database.
*/

package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;

public interface ITEAMMEMBERMESSAGESDAO 
{
    /**To add a record to TeamMemberMessages table. */
	Boolean InsertRow(TEAMMEMBERMESSAGES objTeamMemberMessagesbe) throws JASCIEXCEPTION;
    
	/**To get TeammemberList From TeamMembers Table */ 
	/*List<TEAMMEMBERS> getTeamMemberByName(String TeamMemberName,
			String PartOfTeamMember, String strDepartment, String strShift, String Tenant) throws JASCIEXCEPTION;*/
	
	/**To get Teammember messages From TeamMembers Messages Table */ 

	List<TEAMMEMBERMESSAGES> getTeamMemberMessages(String strTeamMember) throws JASCIEXCEPTION;

	/** To get messages data for updation */
	List<TEAMMEMBERMESSAGES> GetMessagesToEdit(String tenant, String teamMember, String company, String MessageNumber) throws JASCIEXCEPTION;
	
	/** To update Teammember messages data  */

	Boolean UpdateMessages(TEAMMEMBERMESSAGES objTeammembersMessages) throws JASCIEXCEPTION;
	List<TEAMMEMBERSBE> getTeamMemberList(String TeamMemberName,String PartOfTeamMember, String strDepartment, String strShift, String Tenant, String strCompany) throws JASCIEXCEPTION;
	
	/** To get TeamMember Messages Based on TeamMember. */ 
	 List<?> getTeamMemberMessagesList(String strTeamMember) throws JASCIEXCEPTION;

	/** To get TeamMember Messages Based on TeamMember. */ 

	List<LANGUAGES> GetLanguage(String StrLanguage) throws JASCIEXCEPTION;
	
	/** To Delete TeamMember Messages From TeamMemberMessages Table */ 

	Boolean DeleteMessages(TEAMMEMBERMESSAGES objTeammembermessages) throws JASCIEXCEPTION;
	
	/** To  get list of general code based on generalcodeId. */ 

	public List<GENERALCODES> getGeneralCode(String Tenant,String Company,String GeneralCodeId) throws  JASCIEXCEPTION;

	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION ;
	
	
	/**
     * @Description check if team member id exists.
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public List<TEAMMEMBERS> CheckTeamMemberId(String Tenant, String TeamMember) throws JASCIEXCEPTION ;
	
	/**
     * @Description check if part of teamMember exists.
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public List<TEAMMEMBERS> CheckTeamMemberName(String Tenant, String StrPartTeamMember) throws JASCIEXCEPTION ;
	
	/**
     * @Description check if valid department is selected
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public List<TEAMMEMBERS> CheckDepartment(String Tenant, String StrDepartment) throws JASCIEXCEPTION ;
	
	/**
     * @Description check if valid shift is selected
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	public List<TEAMMEMBERS>CheckShift(String StrTenant, String StrShift) throws JASCIEXCEPTION ;
	
	

}
