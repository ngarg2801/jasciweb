/**
 * Date Developed  Dec 22 2014
 * Created By "Deepak Sharma"
 * Description : this class provide data base function of company entity
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERS_PK;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.exception.JASCIEXCEPTION;


@Repository
public class FULLFILLMENTCENTERDAO {

	@Autowired
	private SessionFactory sessionFactory ;
	
	/**
	 * 
	 * @param StrFullfillment
	 * @param StrTenant
	 * @return
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<FULFILLMENTCENTERSPOJO> getfulfillmentcenterbyfulfillment(String StrFullfillment,String StrTenant) throws JASCIEXCEPTION
	
	{
		List<FULFILLMENTCENTERSPOJO> lstOfFullfillment=null;
		try{
		 lstOfFullfillment=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Q_SearchBy_Fulfilment_ID).setParameter(GLOBALCONSTANT.INT_ZERO, StrFullfillment.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase()).list();
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception objException)
		{
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return lstOfFullfillment;
	}
	
	/**
	 * 
	 * @param StrFullfillment
	 * @param StrTenant
	 * @return
	 * @throws JASCIEXCEPTION
	 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<FULFILLMENTCENTERSPOJO> getfulfillmentcenterbyPartOffulfillmentName(String StrFullfillment,String StrTenant) throws JASCIEXCEPTION
	
	{
	StrFullfillment=GLOBALCONSTANT.StrPercentSign+StrFullfillment.toUpperCase()+GLOBALCONSTANT.StrPercentSign;
	
		List<FULFILLMENTCENTERSPOJO> lstOfFullfillment=null;
		try{
		 lstOfFullfillment=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Q_SearchBy_FulfillmentName).setParameter(GLOBALCONSTANT.INT_ZERO, StrFullfillment).setParameter(GLOBALCONSTANT.IntOne, StrFullfillment).setParameter(GLOBALCONSTANT.INT_TWO, StrTenant.toUpperCase()).list();
		}catch(SQLGrammarException objException){
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		catch(Exception objException)
		{
			throw new JASCIEXCEPTION(objException.getMessage());
		}
		return lstOfFullfillment;
	}

/**
 * 
 * @param StrTenant
 * @return
 * @throws JASCIEXCEPTION
 */

@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<FULFILLMENTCENTERSPOJO> getAllFulfillment(String StrTenant) throws JASCIEXCEPTION

{
	List<FULFILLMENTCENTERSPOJO> lstOfFullfillment=null;
	try{
	 lstOfFullfillment=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Q_GetAllFulfillment).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).list();
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(Exception objException)
	{
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	return lstOfFullfillment;
}

/**
 * 
 * @param StrTenant
 * @param StrFulfillment
 * @return
 * @throws JASCIEXCEPTION
 */
public int deleteFulfillment(String StrTenant,String StrFulfillment) throws JASCIEXCEPTION

{
	int IntUpdateStatus;
	try{
	 IntUpdateStatus=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.Fulfillment_Q_Update_FulfillmentBy_ID).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrFulfillment.toUpperCase()).executeUpdate();
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(Exception objException)
	{
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	return IntUpdateStatus;
}

/**
 * 
 * @param StrTenant
 * @param StrFulfillment
 * @return
 * @throws JASCIEXCEPTION
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<FULFILLMENTCENTERSPOJO> isFulfillmentAvailable(String StrTenant,String StrFulfillment) throws JASCIEXCEPTION
{
	List<FULFILLMENTCENTERSPOJO> lstOfFullfillment=null;
	try{
	 lstOfFullfillment=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Q_Is_Fulfillment_AVAILABLE_By_ID).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrFulfillment.toUpperCase()).list();
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(Exception objException)
	{
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	return lstOfFullfillment;
}

/**
 * 
 * @param StrTenant
 * @param StrFulfillment
 * @return
 * @throws JASCIEXCEPTION
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<FULFILLMENTCENTERSPOJO> isFulfillmentAvailable1(String StrTenant,String StrFulfillment) throws JASCIEXCEPTION
{
	List<FULFILLMENTCENTERSPOJO> lstOfFullfillment=null;
	try{
	 lstOfFullfillment=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Q_Is_Fulfillment_AVAILABLE_By_ID).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrFulfillment.toUpperCase()).list();
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(Exception objException)
	{
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	return lstOfFullfillment;
}


/**
 * 
 * @param objFulfillmentcenterspojo
 * @return
 * @throws JASCIEXCEPTION
 */
public FULFILLMENTCENTERS_PK addFulfillment(FULFILLMENTCENTERSPOJO objFulfillmentcenterspojo) throws JASCIEXCEPTION
{
	Session session = sessionFactory.getCurrentSession();
	FULFILLMENTCENTERS_PK objFulfillmentcenters_PK=(FULFILLMENTCENTERS_PK) session.save(objFulfillmentcenterspojo);
	return objFulfillmentcenters_PK;
}

/**
 * 
 * @param objFulfillmentcenterspojo
 * @throws JASCIEXCEPTION
 */
public void updateFulfillment(FULFILLMENTCENTERSPOJO objFulfillmentcenterspojo) throws JASCIEXCEPTION
{
	FULFILLMENTCENTERSPOJO objFulfillmentcenterspojo1=null;
	Session session = sessionFactory.getCurrentSession();
	objFulfillmentcenterspojo1=(FULFILLMENTCENTERSPOJO) session.load(FULFILLMENTCENTERSPOJO.class, objFulfillmentcenterspojo.getId());
	objFulfillmentcenterspojo1=objFulfillmentcenterspojo;
	try{
 session.saveOrUpdate(objFulfillmentcenterspojo1);
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(HibernateException objHibernateException)
	{
		throw new JASCIEXCEPTION(objHibernateException.getMessage());
	}
}


/**
 * 
 * @param Tenant
 * @param Company
 * @param GeneralCodeId
 * @return
 * @throws JASCIEXCEPTION
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId)
		throws  JASCIEXCEPTION {
	List<GENERALCODES> objGeneralcodes=null;
	try{
		objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Select_Conditional_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.toUpperCase()).list();
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(Exception objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	return objGeneralcodes;
}

/**
 * @author Aakash Bishnoi
 * @Date Feb 16, 2015
 * Description:It is used to check uniqueness of Description20 in LOCATIONPROFILEBE TABLE before add record 
 * @param Tenant
 
 * @param FulfillmentCenter
 * @param Description20
 * @param ScreenName
 * @return Boolean
 * @throws JASCIEXCEPTION
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<FULFILLMENTCENTERSPOJO> getDescription20Unique(String StrTenant,String StrFulfillmentCenter,String StrDescription20,String StrScreenName) throws JASCIEXCEPTION{
	List<FULFILLMENTCENTERSPOJO> UniqueDescription20List=null;
	try{	
 
		if(StrScreenName.equalsIgnoreCase(GLOBALCONSTANT.GeneralCodes_Edit)){
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Description20Unique_Query_Edit);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrFulfillmentCenter.toUpperCase().trim());				
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrDescription20.toUpperCase().trim());
			UniqueDescription20List = ObjQuery.list();
		}
		else{
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Description20Unique_Query);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrDescription20.toUpperCase().trim());
			UniqueDescription20List = ObjQuery.list();
		}
		return UniqueDescription20List;
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(Exception ObjectException){
		throw new JASCIEXCEPTION(ObjectException.getMessage());
	}		
}


/**
 * @author Aakash Bishnoi
 * @Date Feb 16, 2015
 * Description:It is used to check uniqueness of Description50 in LOCATIONPROFILEBE TABLE before add record 
 * @param Tenant
 
 * @param FulfillmentCenter
 * @param Description50
 * @param ScreenName
 * @return Boolean
 * @throws JASCIEXCEPTION
 */
@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
public List<FULFILLMENTCENTERSPOJO> getDescription50Unique(String StrTenant,String StrFulfillmentCenter,String StrDescription50,String StrScreenName) throws JASCIEXCEPTION{
	List<FULFILLMENTCENTERSPOJO> UniqueDescription50List=null;
	try{
		if(StrScreenName.equalsIgnoreCase(GLOBALCONSTANT.GeneralCodes_Edit)){
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Description50Unique_Query_Edit);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrFulfillmentCenter.toUpperCase().trim());
			ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrDescription50.toUpperCase().trim());
			UniqueDescription50List = ObjQuery.list();
		}
		else{
			Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Fulfillment_Description50Unique_Query);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase().trim());			
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrDescription50.toUpperCase().trim());
			UniqueDescription50List = ObjQuery.list();
		}
		return UniqueDescription50List;
	}catch(SQLGrammarException objException){
		throw new JASCIEXCEPTION(objException.getMessage());
	}
	catch(Exception ObjectException){
		throw new JASCIEXCEPTION(ObjectException.getMessage());
	}		
}


}
