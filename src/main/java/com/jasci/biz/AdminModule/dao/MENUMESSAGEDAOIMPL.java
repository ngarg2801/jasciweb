/*
Description for implement the IMENUMESSAGEDAO in which we implement the hibernet Functionality
Created By Aakash Bishnoi  
Created Date Dec 14 2014
 */

package com.jasci.biz.AdminModule.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;

import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGESPK;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

import org.springframework.stereotype.Repository;



@Repository
public class MENUMESSAGEDAOIMPL implements IMENUMESSAGEDAO {

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Dec 15, 2014
	 * Discription This is used to get the list of MenuMessages on the behalf of Tenant,Company,Status from MenuMessage Table
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrStatus
	 * @throws JASCIEXCEPTION
	 * @Return List<MENUMESSAGESBEAN>
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strrawtypes, GLOBALCONSTANT.Strunchecked })
	public List getList(String StrTenant,String StrCompany,String StrStatus)throws JASCIEXCEPTION{
		String MenuMessage_QueryNameGetAllList=GLOBALCONSTANT.MenuMessage_QueryGetAllList_First+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.MenuMessage_QueryGetAllList_Second+OBJCOMMONSESSIONBE.getClietnDeviceTimeZone()+GLOBALCONSTANT.MenuMessage_QueryGetAllList_Third;
		Query ObjQuery = sessionFactory.getCurrentSession().createSQLQuery(MenuMessage_QueryNameGetAllList);
		ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant);
		ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrCompany);
		ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrStatus);
		List MenuMessageList=null;
		try{
			MenuMessageList = ObjQuery.list();
			return MenuMessageList;
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception e){ 
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
			}
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 17, 2014
	 * Description this is used to get the List of Team Member Company
	 * @param StrTenant
	 * @param StrTeamMember
	 * @return List<TEAMMEMBERCOMPANIESBEAN>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<COMPANIES>  getCompanyList(String StrTenant,String StrTeamMember) throws JASCIEXCEPTION{
		try{
			Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuMessage_QueryNameGetCompanyList);
			ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase());
			ObjQuery.setParameter(GLOBALCONSTANT.IntOne, StrTeamMember.toUpperCase());

			List<COMPANIES>  TeamMembercompanyList=null;

			TeamMembercompanyList = ObjQuery.list();
			return TeamMembercompanyList;
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception e){ throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);}
	}



	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Dec 16, 2014
	 * Discription This is used to get the list of MenuMessages on the behalf of  Tenant,Company,Status,Type from MenuMessage Table
	 * @param StrTenant
	 * @param StrCompany
	 * @param StrStatus
	 * @param Strtype
	 * @throws JASCIEXCEPTION
	 * @Return MENUMESSAGES
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally, GLOBALCONSTANT.Strunchecked })
	public MENUMESSAGES fetchMenuMessages(String Tenant_ID,String Company_ID,String Status,String Strtype,String StrMessage_ID){
		//Query return all Data Form Infohelps table
		Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuMessage_QueryNamefetchMenuMessage);
		ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, Tenant_ID.toUpperCase());
		ObjQuery.setParameter(GLOBALCONSTANT.IntOne, Company_ID.toUpperCase());
		ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, Status.toUpperCase());
		ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, StrMessage_ID.toUpperCase());
		List<MENUMESSAGES> MenuMessagesListAll = ObjQuery.list();	
		MENUMESSAGES ObjectMenuMessages= MenuMessagesListAll.get(GLOBALCONSTANT.INT_ZERO);

		try{			

			return ObjectMenuMessages;
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		}finally{
			return ObjectMenuMessages;
		}			
	}

	/**
	 *	@author Aakash Bishnoi
	 * @Date Dec 16, 2014
	 * Description this is used to Update the Menu messages
	 * @param ObjectMenuMessages
	 * @throws JASCIEXCEPTION
	 */	
	//It is used for Update data in Menu Messages table 
	public String updateEntry(MENUMESSAGES ObjectMenuMessages) throws JASCIEXCEPTION {
		try
		{

			Serializable SerializableObject=GLOBALCONSTANT.BlankString;

			Session session = sessionFactory.openSession();
			Transaction TransactionObject = session.beginTransaction();

			session.saveOrUpdate(ObjectMenuMessages);

			TransactionObject.commit();

			session.close();

			return (String) SerializableObject;

		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}

		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		}   

	}

	/**
	 *	@author Aakash Bishnoi
	 * @Date Dec 16, 2014
	 * Description this is used to Add the Menu messages
	 * @param ObjectMenuMessages
	 * @throws JASCIEXCEPTION
	 */	
	//It is used for Add data in Menu Messages table 
	public String addEntry(MENUMESSAGES ObjectMenuMessages) throws JASCIEXCEPTION {
		try
		{
			Serializable SerializableObject=GLOBALCONSTANT.BlankString;
			sessionFactory.getCurrentSession().save(ObjectMenuMessages);	
			return (String) SerializableObject;
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		}   

	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 18, 2014
	 * Description this is used to check the uniqueness of Ticket Tape Message and Message
	 * @param Tenant_ID
	 * @param Company_ID
	 * @param StrStatus
	 * @param TicketTapeMessage
	 * @param Message
	 * @return MENUMESSAGES
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally, GLOBALCONSTANT.Strunchecked })
	public MENUMESSAGES CheckTicketTypeMessage(String Tenant_ID,String Company_ID,String StrStatus,String TicketTapeMessage,String Message){
		//Query return all Data Form Infohelps table
		Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.MenuMessage_QueryName_UniqueTicketTapeMessage);
		ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, Tenant_ID.toUpperCase());
		ObjQuery.setParameter(GLOBALCONSTANT.IntOne, Company_ID.toUpperCase());
		ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, StrStatus.toUpperCase());
		ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, TicketTapeMessage.toUpperCase());
		ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, Message.toUpperCase());
		List<MENUMESSAGES> MenuMessageList = ObjQuery.list();	

		MENUMESSAGES ObjectMenuMessage=null; 

		try{			
			ObjectMenuMessage=MenuMessageList.get(GLOBALCONSTANT.INT_ZERO);
			return ObjectMenuMessage;
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjectException){
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		}finally{
			return ObjectMenuMessage;
		}			
	}


	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 19, 2014
	 * Description this is used to delete the selected menu messages
	 * @param model
	 * @return Boolean
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strfinally)
	public Boolean deleteEntry(String Message_ID,String Tenant_ID,String Company_ID,String Status) throws JASCIEXCEPTION{
		Boolean Result=false;
		try
		{
			MENUMESSAGESPK obj_MENUMESSAGESPK=new MENUMESSAGESPK();
			obj_MENUMESSAGESPK.setMessage_ID(Message_ID);
			obj_MENUMESSAGESPK.setTenant_ID(Tenant_ID);
			obj_MENUMESSAGESPK.setCompany_ID(Company_ID);
			obj_MENUMESSAGESPK.setStatus(Status);

			try{
				MENUMESSAGES ObjectMenuMessages = (MENUMESSAGES) sessionFactory.getCurrentSession().load(MENUMESSAGES.class, obj_MENUMESSAGESPK);
				sessionFactory.getCurrentSession().delete(ObjectMenuMessages);
				sessionFactory.getCurrentSession().getIdentifier(ObjectMenuMessages);
				Result=true;
			}catch (SQLGrammarException objException) {

				throw new JASCIEXCEPTION(objException.getMessage());

			}
			catch(Exception Ex){
				Result=false;
			}

		}

		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		}
		finally{
			return Result;
		}
	}
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 19, 2014
	 * Description this is used to set language and get screen labels
	 * @param StrLanguage
	 * @return List<LANGUAGES>
	 * @throws JASCIEXCEPTION
	 */
 
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> GetLanguage(String StrLanguage) throws JASCIEXCEPTION	  
	{	 
		try
		{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuMessages_Language_Query).setParameter(GLOBALCONSTANT.INT_ZERO, StrLanguage.toUpperCase()).list();	   
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		}	
	}
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 19, 2014
	 * Description this is used to set language and get screen labels
	 * @param StrLanguage
	 * @return List<LANGUAGES>
	 * @throws JASCIEXCEPTION
	 */

	public List<LANGUAGES> getMenuMessagesLabels(String StrLanguage) throws  JASCIEXCEPTION
	{

		try{
		List<LANGUAGES> ObjListLanguages=GetLanguage(StrLanguage);
  			return ObjListLanguages;
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.jasci_Exception);
		}	
	}
	
	
	
}