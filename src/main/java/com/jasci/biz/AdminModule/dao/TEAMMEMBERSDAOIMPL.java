/**

Date Developed  Nov 17 2014
Created By "Rahul Kumar"
Description : this class provide data base function of team members entiity
 */

package com.jasci.biz.AdminModule.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIESPK;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.TEAMMEMBERSPK;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class TEAMMEMBERSDAOIMPL implements ITEAMMEMBERSDAO
{

	@Autowired
	private SessionFactory sessionFactory ;

	/**
	 * Created on:Nov 17 2014
	 * Created by:Rahul Kumar
	 * Description: This function  get Team member list based on given team member name or PartOfTeamMember
	 * Input parameter: TeamMemberName,PartOfTeamMember, Tenant, FulfillmentCenter
	 * Return Type :List<TEAMMEMBERS>
	 * @throws JASCIEXCEPTION 
	 * 
	 */
	//@Override
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS> getList(String TeamMemberName,String PartOfTeamMember,String Tenant) throws JASCIEXCEPTION {
		

		List<TEAMMEMBERS> ObjListTeamMembers=null;

		try{

			if(TeamMemberName.isEmpty() && PartOfTeamMember.isEmpty()){

				ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).list();
			}

			else if(PartOfTeamMember.isEmpty()){

				ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_TeamMemberName_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMemberName.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase()).list();
			}
			else if(!TeamMemberName.isEmpty() && !PartOfTeamMember.isEmpty()){

				ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_TeamMemberName_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMemberName.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase()).list();
			}
			else{
				ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_PartOfTeamMember_ConditionalQuery).
						setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).
						setString(GLOBALCONSTANT.LNAME, GLOBALCONSTANT.StrPercentSign+PartOfTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
						setString(GLOBALCONSTANT.FNAME, GLOBALCONSTANT.StrPercentSign+PartOfTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
						setString(GLOBALCONSTANT.MNAME, GLOBALCONSTANT.StrPercentSign+PartOfTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
						list();
			}


			return ObjListTeamMembers;

		}catch(Exception e){



			if(ObjListTeamMembers.size()==GLOBALCONSTANT.INT_ZERO || ObjListTeamMembers.isEmpty()||ObjListTeamMembers==null){


				if(!PartOfTeamMember.isEmpty()){

					throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_INVALID_PART_OF_TEAMMEMEBR);
				}

				if(!TeamMemberName.isEmpty()){

					throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_INVALID_TEAMMEMEBR);
				}

			}

		}
		return ObjListTeamMembers;

	}



	/*
	 * Created on:Nov 17 2014
	 * Created by:Rahul Kumar
	 * Description: This function  register new team member
	 * Input parameter: TeamMemberName
	 * Return Type :Boolean
	 * 
	 */
	//@Override
	public  java.io.Serializable  save(TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList,String strLastActivityBy) throws JASCIEXCEPTION  {
		
		java.io.Serializable objSerializable=null;

		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();

		TEAMMEMBERS ObjTeammembers=getTeamMember(ObjectTeamMembersBe);
		List<TEAMMEMBERCOMPANIES> ObTeammembercompaniesList=loadTeamMemberCompanies(CompaniesList,ObjTeammembers);

		try{


			if(isEmailUnique(ObjTeammembers.getPersonalEmailAddress())){

				throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_TEAMMEMBER_Email_AllreadyExist);

			}
			else{

				Date Setupdate=null;
				try {
					SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

					 Setupdate = formatter.parse(ObjectTeamMembersBe.getSetupdate());
					Date LastActivitydate = formatter.parse(ObjectTeamMembersBe.getLastactivitydate());
					ObjTeammembers.setSetupDate(Setupdate);
					ObjTeammembers.setLastActivityDate(LastActivitydate);

				}catch(Exception objException){}
				
				
				ObjTeammembers.setSetupBy(strLastActivityBy);
				
				ObjTeammembers.setLastActivityTeamMember(strLastActivityBy);


				objSerializable=(java.io.Serializable) session.save(ObjTeammembers);

				for(TEAMMEMBERCOMPANIES objTeammembercompanies:ObTeammembercompaniesList){

					objTeammembercompanies.setLastActivityDate(Setupdate);
					
					objTeammembercompanies.setLastActivityTeamMember(strLastActivityBy);
					session.save(objTeammembercompanies);
				}


				TransactionObject.commit();
				session.close();

			}
			
		}catch(Exception obException){

			TransactionObject.rollback();


			if(obException.getMessage().equalsIgnoreCase(GLOBALCONSTANT.INFO_TEAMMEMBER_Email_AllreadyExist)){
				throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_TEAMMEMBER_Email_AllreadyExist);	
			}
			else{
				throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_TEAMMEMBER_USED);
			}
		}

		return objSerializable;

	}

	/*
	 * Created on:Nov 19 2014
	 * Created by:Rahul Kumar
	 * Description: This function  set  team member info
	 * Input parameter: TEAMMEMBERSBE
	 * Return Type :TEAMMEMBERSBE
	 * 	 * 
	 */
	private TEAMMEMBERS getTeamMember(TEAMMEMBERSBE ObjectTeamMembersBe){

		TEAMMEMBERSPK objTeammemberspk=new TEAMMEMBERSPK();
		TEAMMEMBERS objTeammembers=new TEAMMEMBERS();

		objTeammembers.setAddressLine1(trimValues(ObjectTeamMembersBe.getAddressline1()));
		objTeammembers.setAddressLine2(trimValues(ObjectTeamMembersBe.getAddressline2()));
		objTeammembers.setAddressLine3(trimValues(ObjectTeamMembersBe.getAddressline3()));
		objTeammembers.setAddressLine4(trimValues(ObjectTeamMembersBe.getAddressline4()));
		objTeammembers.setCell(trimValues(ObjectTeamMembersBe.getCell()));
		objTeammembers.setCity(trimValues(ObjectTeamMembersBe.getCity()));
		objTeammembers.setCountryCode(trimValues(ObjectTeamMembersBe.getCountrycode()));
		objTeammembers.setZipCode(trimValues(ObjectTeamMembersBe.getZipcode()));
		objTeammembers.setCurrentStatus(trimValues(ObjectTeamMembersBe.getCurrentstatus()));
		objTeammembers.setDepartment(trimValues(ObjectTeamMembersBe.getDepartment()));
		objTeammembers.setEmergencyContactCell(trimValues(ObjectTeamMembersBe.getEmergencycontactcell()));
		objTeammembers.setEmergencyContactHomePhone(trimValues(ObjectTeamMembersBe.getEmergencycontacthomephone()));
		objTeammembers.setEmergencyContactName(trimValues(ObjectTeamMembersBe.getEmergencycontactname()));
		objTeammembers.setEquipmentCertification(trimValues(ObjectTeamMembersBe.getEquipmentcertification()));
		objTeammembers.setFirstName(trimValues(ObjectTeamMembersBe.getFirstname()));
		objTeammembers.setHomePhone(trimValues(ObjectTeamMembersBe.getHomephone()));
		objTeammembers.setLanguage(trimValues(ObjectTeamMembersBe.getLanguage()));

		objTeammembers.setLastName(trimValues(ObjectTeamMembersBe.getLastname()));
		objTeammembers.setMenu_Profile_Glass(trimValues(ObjectTeamMembersBe.getMenuProfileGlass()));
		objTeammembers.setMenu_Profile_Mobile(trimValues(ObjectTeamMembersBe.getMenuProfileMobile()));
		objTeammembers.setMenu_Profile_RF(trimValues(ObjectTeamMembersBe.getMenuProfileRf()));
		objTeammembers.setMenu_Profile_Station(trimValues(ObjectTeamMembersBe.getMenuProfileStation()));
		objTeammembers.setMenu_Profile_Tablet(trimValues(ObjectTeamMembersBe.getMenuProfileTablet()));
		objTeammembers.setAuthorityProfile(trimValues(ObjectTeamMembersBe.getAuthorityprofile()));
		objTeammembers.setMiddleName(trimValues(ObjectTeamMembersBe.getMiddlename()));
		objTeammembers.setPersonalEmailAddress(trimValues(ObjectTeamMembersBe.getPersonalemailaddress()));
		objTeammembers.setShiftCode(trimValues(ObjectTeamMembersBe.getShiftcode()));

		objTeammembers.setStateCode(trimValues(ObjectTeamMembersBe.getStatecode()));
		objTeammembers.setSystemUse(trimValues(ObjectTeamMembersBe.getSystemuse()));
		objTeammembers.setTaskProfile(trimValues(ObjectTeamMembersBe.getTaskprofile()));
		objTeammembers.setWorkCell(trimValues(ObjectTeamMembersBe.getWorkcell()));
		objTeammembers.setWorkExtension(trimValues(ObjectTeamMembersBe.getWorkextension()));
		objTeammembers.setWorkFax(trimValues(ObjectTeamMembersBe.getWorkfax()));
		objTeammembers.setWorkPhone(trimValues(ObjectTeamMembersBe.getWorkphone()));
		objTeammembers.setWorkEmailAddress(ObjectTeamMembersBe.getWorkemailaddress());
		objTeammembers.setEmergencyEmailAddress(ObjectTeamMembersBe.getEmergencyemailaddress());		
		objTeammemberspk.setTeamMember(trimValues(ObjectTeamMembersBe.getTeammember()));

		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

			Date Startdate = formatter.parse(ObjectTeamMembersBe.getStartdate());
			objTeammembers.setStartDate(Startdate);
			
			try{
				
				Date Lastdate = formatter.parse(ObjectTeamMembersBe.getLastdate());
				objTeammembers.setLastDate(Lastdate);
			}catch(Exception e){
				objTeammembers.setLastDate(null);
			}

		}catch(Exception objException){}

		objTeammemberspk.setTenant(ObjectTeamMembersBe.getTenant());
		
		objTeammembers.setId(objTeammemberspk);
		objTeammembers.setFulfillmentCenterId(ObjectTeamMembersBe.getFulfillmentcenterId());
		return objTeammembers;

	}


	/*
	 * Created on:Nov 24 2014
	 * Created by:Rahul Kumar
	 * Description: This function  set  team member info for updatation
	 * Input parameter: TEAMMEMBERSBE,TEAMMEMBERS
	 * Return Type :TEAMMEMBERSBE
	 * 	 * 
	 */
	private TEAMMEMBERS getUpdateTeamMember(TEAMMEMBERSBE ObjectTeamMembersBe,TEAMMEMBERS objTeammembers){


		objTeammembers.setAddressLine1(trimValues(ObjectTeamMembersBe.getAddressline1()));
		objTeammembers.setAddressLine2(trimValues(ObjectTeamMembersBe.getAddressline2()));
		objTeammembers.setAddressLine3(trimValues(ObjectTeamMembersBe.getAddressline3()));
		objTeammembers.setAddressLine4(trimValues(ObjectTeamMembersBe.getAddressline4()));
		objTeammembers.setCell(trimValues(ObjectTeamMembersBe.getCell()));
		objTeammembers.setCity(trimValues(ObjectTeamMembersBe.getCity()));
		objTeammembers.setZipCode(trimValues(ObjectTeamMembersBe.getZipcode()));
		objTeammembers.setCountryCode(trimValues(ObjectTeamMembersBe.getCountrycode()));
		objTeammembers.setCurrentStatus(trimValues(ObjectTeamMembersBe.getCurrentstatus()));
		objTeammembers.setDepartment(trimValues(ObjectTeamMembersBe.getDepartment()));
		objTeammembers.setEmergencyContactCell(trimValues(ObjectTeamMembersBe.getEmergencycontactcell()));
		objTeammembers.setEmergencyContactHomePhone(trimValues(ObjectTeamMembersBe.getEmergencycontacthomephone()));
		objTeammembers.setEmergencyContactName(trimValues(ObjectTeamMembersBe.getEmergencycontactname()));
		objTeammembers.setEquipmentCertification(trimValues(ObjectTeamMembersBe.getEquipmentcertification()));
		objTeammembers.setFirstName(trimValues(ObjectTeamMembersBe.getFirstname()));
		objTeammembers.setHomePhone(trimValues(ObjectTeamMembersBe.getHomephone()));
		objTeammembers.setLanguage(trimValues(ObjectTeamMembersBe.getLanguage()));
		objTeammembers.setLastName(trimValues(ObjectTeamMembersBe.getLastname()));
		objTeammembers.setMenu_Profile_Glass(trimValues(ObjectTeamMembersBe.getMenuProfileGlass()));
		objTeammembers.setMenu_Profile_Mobile(trimValues(ObjectTeamMembersBe.getMenuProfileMobile()));
		objTeammembers.setMenu_Profile_RF(trimValues(ObjectTeamMembersBe.getMenuProfileRf()));
		objTeammembers.setMenu_Profile_Station(trimValues(ObjectTeamMembersBe.getMenuProfileStation()));
		objTeammembers.setMenu_Profile_Tablet(trimValues(ObjectTeamMembersBe.getMenuProfileTablet()));
		objTeammembers.setAuthorityProfile(trimValues(ObjectTeamMembersBe.getAuthorityprofile()));		
		objTeammembers.setMiddleName(trimValues(ObjectTeamMembersBe.getMiddlename()));
		objTeammembers.setPersonalEmailAddress(trimValues(ObjectTeamMembersBe.getPersonalemailaddress()));
		objTeammembers.setShiftCode(trimValues(ObjectTeamMembersBe.getShiftcode()));
		objTeammembers.setStateCode(trimValues(ObjectTeamMembersBe.getStatecode()));
		objTeammembers.setSystemUse(trimValues(ObjectTeamMembersBe.getSystemuse()));
		objTeammembers.setTaskProfile(trimValues(ObjectTeamMembersBe.getTaskprofile()));
		objTeammembers.setWorkCell(trimValues(ObjectTeamMembersBe.getWorkcell()));
		objTeammembers.setWorkExtension(trimValues(ObjectTeamMembersBe.getWorkextension()));
		objTeammembers.setWorkFax(trimValues(ObjectTeamMembersBe.getWorkfax()));
		objTeammembers.setWorkPhone(trimValues(ObjectTeamMembersBe.getWorkphone()));	
		objTeammembers.setWorkEmailAddress(ObjectTeamMembersBe.getWorkemailaddress());
		objTeammembers.setEmergencyEmailAddress(ObjectTeamMembersBe.getEmergencyemailaddress());
		objTeammembers.setFulfillmentCenterId(ObjectTeamMembersBe.getFulfillmentcenterId());
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

			Date Startdate = formatter.parse(ObjectTeamMembersBe.getStartdate());			
			objTeammembers.setStartDate(Startdate);
			try{
			Date Lastdate = formatter.parse(ObjectTeamMembersBe.getLastdate());
			objTeammembers.setLastDate(Lastdate);
			}catch(Exception e){
				objTeammembers.setLastDate(null);
			}

		}catch(Exception objException){}


		return objTeammembers;

	}

	/*
	 * Created on:Nov 21 2014
	 * Created by:Rahul Kumar
	 * Description: This function  set team member company List
	 * Input parameter: List CompaniesList,TEAMMEMBERS objTeammembers
	 * Return Type :List<TEAMMEMBERCOMPANIES>
	 * 	 * 
	 */
	private List<TEAMMEMBERCOMPANIES> loadTeamMemberCompanies(List<String> CompaniesList,TEAMMEMBERS ObjTeammembers){



		List<TEAMMEMBERCOMPANIES> ObjTeammembercompaniesList= new ArrayList<TEAMMEMBERCOMPANIES>();


		for(String CompanyList: CompaniesList){

			TEAMMEMBERCOMPANIES ObjTeammembercompanies=new TEAMMEMBERCOMPANIES();
			TEAMMEMBERCOMPANIESPK ObjTeammembercompaniespk=new TEAMMEMBERCOMPANIESPK();

			ObjTeammembercompaniespk.setTeamMember(ObjTeammembers.getId().getTeamMember());
			ObjTeammembercompaniespk.setTenant(ObjTeammembers.getId().getTenant());

			ObjTeammembercompanies.setCurrentStatus(ObjTeammembers.getCurrentStatus());
			ObjTeammembercompanies.setLastActivityDate(ObjTeammembers.getLastActivityDate());
			ObjTeammembercompanies.setLastActivityTeamMember(ObjTeammembers.getLastActivityTeamMember());

			ObjTeammembercompaniespk.setCompany(CompanyList);

			ObjTeammembercompanies.setId(ObjTeammembercompaniespk);
			ObjTeammembercompaniesList.add(ObjTeammembercompanies);

		}

		return ObjTeammembercompaniesList;

	}


	/*
	 * Created on:Nov 19 2014
	 * Created by:Rahul Kumar
	 * Description: This function  delete existing team member
	 * Input parameter: TEAMMEMBERS
	 * Return Type :boolean
	 * 	 * 
	 */
	//@Override
	public TEAMMEMBERS delete(String Tenant,String TeamMember,String Company) throws JASCIEXCEPTION {
		


		TEAMMEMBERSPK objTeammemberspk=new TEAMMEMBERSPK();

		objTeammemberspk.setTenant(Tenant);
		objTeammemberspk.setTeamMember(TeamMember);
		

		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();
		TEAMMEMBERS objTeammembers=null;
		try{  
			objTeammembers = (TEAMMEMBERS) session.load(TEAMMEMBERS.class, objTeammemberspk);	

			objTeammembers.setCurrentStatus(GLOBALCONSTANT.Status_Delete);

			session.update(objTeammembers);


			try{
				Query query = session.createQuery(GLOBALCONSTANT.Delete_TeamMember_Securty_Authorization_Query);
				query.setString(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant, Tenant.toUpperCase());
				query.setString(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Company, Company.toUpperCase());
				query.setString(GLOBALCONSTANT.DataBase_SecurityAuthorizations_TeamMember, TeamMember.toUpperCase());
				query.executeUpdate();
			}catch(Exception e){
				
				
			}
			sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Companies_SetInactive1_TeamMember_Companies_Query).setParameter(GLOBALCONSTANT.INT_ZERO,GLOBALCONSTANT.Status_Delete).setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, TeamMember.toUpperCase()).executeUpdate();


			TransactionObject.commit();
			session.getIdentifier(objTeammembers);
			session.close();
		}catch(Exception e){

			TransactionObject.rollback();
			session.close();
			throw new JASCIEXCEPTION(GLOBALCONSTANT.TeamMemberDeleteException);

		}


		return objTeammembers;

	}

	/*
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function  get Companies list based on tenant 
	 * Input parameter: String
	 * Return Type :List<String>
	 * 
	 */

	//@Override
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<COMPANIES> getCompanies(String Tenant) throws JASCIEXCEPTION {
		
    	List<COMPANIES> ObjListTeamMembers=null;
		try{
			ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Companies_Select_Tenant_Companies_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, GLOBALCONSTANT.Status_Active).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
		return ObjListTeamMembers;


	}


	/*
	 * Created on:Nov 20 2014
	 * Created by:Rahul Kumar
	 * Description: This function  get General code list based on tenant,company,application,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId)
			throws  JASCIEXCEPTION {

		List<GENERALCODES> objGeneralcodes=null;

		try{
			objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Select_Conditional_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception objException){
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}

		return objGeneralcodes;


	}


	/*
	 * Created on:Nov 22 2014
	 * Created by:Rahul Kumar
	 * Description: This function get Companies of TeamMember based on tenant an team member
	 * Input parameter: String Tenant,String TeamMember
	 * Return Type :List<TEAMMEMBERCOMPANIES>
	 * 
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERCOMPANIES> getTeamMemberCompanies(String Tenant, String TeamMember)
			throws JASCIEXCEPTION {

		List<TEAMMEMBERCOMPANIES> objTeammembercompanies=null;
		try{
			objTeammembercompanies=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Companies_Select_TeamMember_Companies_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception objException){
			throw new  JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
		return objTeammembercompanies;

	}


	/*
	 * Created on:Nov 23 2014
	 * Created by:Rahul Kumar
	 * Description: This function update existing   TeamMember 
	 * Input parameter:TEAMMEMBERSBE ObjectTeamMembersBe,List<String> CompaniesList
	 * Return Type :Boolean
	 * 
	 */
	public TEAMMEMBERS update(TEAMMEMBERSBE ObjectTeamMembersBe, List<String> CompaniesList,String TeamMemberoldId,String Company,String strLastActivityBy) throws JASCIEXCEPTION {
		



		TEAMMEMBERS ObjTeammembers=null;
		TEAMMEMBERS ObjTeammembersWithData=getTeamMember(ObjectTeamMembersBe);
		List<TEAMMEMBERCOMPANIES> ObTeammembercompaniesList=loadTeamMemberCompanies(CompaniesList,ObjTeammembersWithData);
		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();


		try{



			TEAMMEMBERSPK objTeammemberspk=new TEAMMEMBERSPK();
			
			objTeammemberspk.setTeamMember(TeamMemberoldId);
			objTeammemberspk.setTenant(ObjTeammembersWithData.getId().getTenant());			
			TEAMMEMBERS ObjTeammembers1 = (TEAMMEMBERS) session.load(TEAMMEMBERS.class, objTeammemberspk);	
			Boolean Status=false;
			String emailold=ObjTeammembers1.getPersonalEmailAddress();
			String emailNew=ObjectTeamMembersBe.getPersonalemailaddress();

			if(emailold==null ){
				emailold=GLOBALCONSTANT.OldEmail;

			}

			if(emailNew==null){
				emailNew=GLOBALCONSTANT.NewEmail;
			}

			if(!emailold.equalsIgnoreCase(emailNew)){


				if(isEmailUnique(emailNew)){

					Status=true;
				}
			}



			if(Status){


				throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_TEAMMEMBER_Email_AllreadyExist);
			}
			else{
				ObjTeammembers=getUpdateTeamMember(ObjectTeamMembersBe,ObjTeammembers1);
				
				Date Setupdate=null;
				Date LastActivitydate=null;
				try {
					SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);

					 Setupdate = formatter.parse(ObjectTeamMembersBe.getSetupdate());
					 LastActivitydate= formatter.parse(ObjectTeamMembersBe.getLastactivitydate());
					ObjTeammembers.setSetupDate(Setupdate);
					ObjTeammembers.setLastActivityDate(LastActivitydate);

				}catch(Exception objException){}
				
				
				ObjTeammembers.setSetupBy(ObjTeammembers1.getSetupBy());
				ObjTeammembers.setSetupDate(ObjTeammembers1.getSetupDate());
				ObjTeammembers.setLastActivityDate(LastActivitydate);
				ObjTeammembers.setLastActivityTeamMember(strLastActivityBy);
				

				if(ObjectTeamMembersBe.getCurrentstatus().toUpperCase().equalsIgnoreCase(GLOBALCONSTANT.Status_Active)){
					try{
						sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_Update_Status_Query).setParameter(GLOBALCONSTANT.INT_ZERO,GLOBALCONSTANT.Status_SQRTActive).setParameter(GLOBALCONSTANT.IntOne, ObjTeammembers.getId().getTenant().toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, ObjectTeamMembersBe.getTeammember().toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, Company.toUpperCase()).executeUpdate();
					}catch(Exception e){}
				}
				else if(ObjectTeamMembersBe.getCurrentstatus().toUpperCase().equalsIgnoreCase(GLOBALCONSTANT.Status_NotActive)){
					try{
						sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_Update_Status_Query).setParameter(GLOBALCONSTANT.INT_ZERO,GLOBALCONSTANT.Status_InActive).setParameter(GLOBALCONSTANT.IntOne, ObjTeammembers.getId().getTenant().toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, ObjectTeamMembersBe.getTeammember().toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, Company.toUpperCase()).executeUpdate();
					}catch(Exception e){}
				}
				else if(ObjectTeamMembersBe.getCurrentstatus().toUpperCase().equalsIgnoreCase(GLOBALCONSTANT.Status_Delete)){
					try{
						Query query = session.createQuery(GLOBALCONSTANT.Delete_TeamMember_Securty_Authorization_Query);
						query.setString(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant, ObjTeammembers.getId().getTenant().toUpperCase());
						query.setString(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Company, Company.toUpperCase());
						query.setString(GLOBALCONSTANT.DataBase_SecurityAuthorizations_TeamMember, ObjectTeamMembersBe.getTeammember().toUpperCase());
						query.executeUpdate();
					}catch(Exception e){}

				}
				else{
					try{
						sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.SecurityAuthorizations_Update_Status_Query).setParameter(GLOBALCONSTANT.INT_ZERO,GLOBALCONSTANT.Status_Leav).setParameter(GLOBALCONSTANT.IntOne, ObjTeammembers.getId().getTenant().toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, ObjectTeamMembersBe.getTeammember().toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, Company.toUpperCase()).executeUpdate();
					}catch(Exception e){}
				}
				session.update(ObjTeammembers);
				Query query = session.createQuery(GLOBALCONSTANT.Companies_Delete_TeamMember_Companies_Query);
				query.setString(GLOBALCONSTANT.DataBase_TeamMembersCompanies_Tenant, ObjTeammembers.getId().getTenant().toUpperCase());
				query.setString(GLOBALCONSTANT.DataBase_TeamMembersCompanies_TeamMember, TeamMemberoldId.toUpperCase());
				query.executeUpdate();

				for(TEAMMEMBERCOMPANIES objTeammembercompanies:ObTeammembercompaniesList){

					objTeammembercompanies.setLastActivityDate(LastActivitydate);
					objTeammembercompanies.setLastActivityTeamMember(strLastActivityBy);
					if(ObjectTeamMembersBe.getCurrentstatus().toUpperCase().equalsIgnoreCase(GLOBALCONSTANT.Status_Active)){
						objTeammembercompanies.setCurrentStatus(GLOBALCONSTANT.Status_Active);
					}
					else if(ObjectTeamMembersBe.getCurrentstatus().toUpperCase().equalsIgnoreCase(GLOBALCONSTANT.Status_NotActive)){
						objTeammembercompanies.setCurrentStatus(GLOBALCONSTANT.Status_NotActive);
					}
					else if(ObjectTeamMembersBe.getCurrentstatus().toUpperCase().equalsIgnoreCase(GLOBALCONSTANT.Status_Delete)){
						objTeammembercompanies.setCurrentStatus(GLOBALCONSTANT.Status_Delete);
					}
					else{
						objTeammembercompanies.setCurrentStatus(GLOBALCONSTANT.Status_LeavOfabsense);
					}
					session.saveOrUpdate(objTeammembercompanies);
				}
				TransactionObject.commit();
				session.close();
			}
		}catch(Exception e){

			TransactionObject.rollback();
			session.close();
			throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_TEAMMEMBER_Email_AllreadyExist);
		}
		return ObjTeammembers;
	}

	/*
	 *Description This function design to get languages from LANGUAGES table
	 *Input parameter none
	 *Return Type List<LANGUAGES>
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 24 2014" 
	 * */

	@SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strunused })
	private List<LANGUAGES> GetLanguage( String StrScreenName,String StrLanguage) throws JASCIEXCEPTION
	{
		try
		{
			return sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Languages_Query).setParameter(GLOBALCONSTANT.INT_ZERO,StrScreenName.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrLanguage.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
	}



	/*
	 * Created on:Nov 23 2014
	 * Created by:Rahul Kumar
	 * Description: This function check unque team member id 
	 * Input parameter:TEAMMEMBERS objTeammembers,String TeamMemberoldId
	 * Return Type :Boolean
	 * 
	 */
	@SuppressWarnings({GLOBALCONSTANT.Strunchecked,GLOBALCONSTANT.Strunused})
	private Boolean checkUniqueTeamMemberId(TEAMMEMBERS objTeammembers,String TeamMemberoldId){

		List<TEAMMEMBERS> ObjListTeamMembers=null;

		Boolean Status=true;

		if(TeamMemberoldId.equalsIgnoreCase(objTeammembers.getId().getTeamMember())){

			return true;
		}

		ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, objTeammembers.getId().getTenant()).list();

		for(TEAMMEMBERS objVal:ObjListTeamMembers){
				if(objVal.getId().getTenant().toString().equalsIgnoreCase(objTeammembers.getId().getTenant().toString())){

					if(objVal.getId().getTeamMember().toString().equalsIgnoreCase(objTeammembers.getId().getTeamMember().toString())){


						if(TeamMemberoldId.equalsIgnoreCase(objTeammembers.getId().getTeamMember())){
							Status=true;
						}
						else{
							Status=false;
						}
					}

				
			}

		}
		return Status;
	}
	/*
	 *Description This function get Team member list
	 *Input parameter none
	 *Return Type TEAMMEMBERSBE
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 24 2014" 
	 * */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERSBE> getTeamMemberList(String  SortOrder, String StrPartOfTeamMember,String Tenant) throws 	JASCIEXCEPTION {

		List<TEAMMEMBERS> ObjListTeamMembers=null;
		try{
			if(SortOrder.equalsIgnoreCase(GLOBALCONSTANT.AcityvityDate)){

				ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_OrderByActivityDateConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).list();
			}
			else if(SortOrder.equalsIgnoreCase(GLOBALCONSTANT.LastName)){

				if( !GLOBALCONSTANT.BlankString.equalsIgnoreCase(StrPartOfTeamMember)){
					ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_PartOfTeamMember_ConditionalQuery).
							setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).
							setString(GLOBALCONSTANT.LNAME, GLOBALCONSTANT.StrPercentSign+StrPartOfTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
							setString(GLOBALCONSTANT.FNAME, GLOBALCONSTANT.StrPercentSign+StrPartOfTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
							setString(GLOBALCONSTANT.MNAME, GLOBALCONSTANT.StrPercentSign+StrPartOfTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
							list();

				}
				else{

					ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_OrderByLastNameConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).list();
				}
			}
			else if(SortOrder.equalsIgnoreCase(GLOBALCONSTANT.FirstName)){


				if( !GLOBALCONSTANT.BlankString.equalsIgnoreCase(StrPartOfTeamMember)){


					ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_ALLPartOfTeamMember_FirstNameConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrPartOfTeamMember.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrPartOfTeamMember.trim().toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, StrPartOfTeamMember.trim().toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, Tenant.toUpperCase()).list();

				}
				else{
					ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_AllTeamMember_OrderByFirstNameConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).list();
				}
			}



		}catch(Exception objException){

			throw new JASCIEXCEPTION(GLOBALCONSTANT.INFO_INVALID_PART_OF_TEAMMEMEBR);
		}
		List<TEAMMEMBERSBE> objTeammembersbeList=new ArrayList<TEAMMEMBERSBE>();

		for(TEAMMEMBERS  valueTeamMembers:ObjListTeamMembers ){
			TEAMMEMBERSBE objTeammembersbe=new TEAMMEMBERSBE();


			objTeammembersbe.setTenant(valueTeamMembers.getId().getTenant());
			objTeammembersbe.setTeammember(valueTeamMembers.getId().getTeamMember());
			objTeammembersbe.setFirstname(valueTeamMembers.getFirstName());
			objTeammembersbe.setLastname(valueTeamMembers.getLastName());
			
			String status=valueTeamMembers.getCurrentStatus();			

			if(GLOBALCONSTANT.Security_TeamMember_Active_Status.equalsIgnoreCase(status)){
				objTeammembersbe.setCurrentstatus(GLOBALCONSTANT.Security_TeamMember_Active_Status_Text);
			}
			else if(GLOBALCONSTANT.Security_TeamMember_InActive_Status.equalsIgnoreCase(status)){
				objTeammembersbe.setCurrentstatus(GLOBALCONSTANT.Security_TeamMember_InActive_Status_Text);

			}
			else if(GLOBALCONSTANT.Security_TeamMember_LeaveOfAbsence_Status.equalsIgnoreCase(status)){
				objTeammembersbe.setCurrentstatus(GLOBALCONSTANT.Security_TeamMember_LeaveOfAbsence_Text);

			}
			else if(GLOBALCONSTANT.Security_TeamMember_Delete_Status.equalsIgnoreCase(status)){
				objTeammembersbe.setCurrentstatus(GLOBALCONSTANT.Security_TeamMember_Delete_Status_Text);

			}


			else{

			}


			String middleName=valueTeamMembers.getMiddleName();
			if(middleName==null){
				middleName=GLOBALCONSTANT.BlankString;
			}

			objTeammembersbe.setSortname(valueTeamMembers.getFirstName()+GLOBALCONSTANT.Single_Space+middleName+GLOBALCONSTANT.Single_Space+valueTeamMembers.getLastName());
			objTeammembersbeList.add(objTeammembersbe);

		}


		return objTeammembersbeList;
	}



	/*
	 *Description This function is check unique email
	 *Input parameter :String Email
	 *Return Type Boolean
	 *Created By "Rahul Kumar"
	 *Created Date "Dec 01 2014" 
	 * */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public Boolean isEmailUnique(String Email){

		List<TEAMMEMBERS> objTeammembersList=new ArrayList<TEAMMEMBERS>();

		Boolean Status=false;
		try{

			objTeammembersList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_Select_Email_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Email.trim().toUpperCase()).list();
		}catch(Exception objException){

		}

		if(objTeammembersList.size()>GLOBALCONSTANT.INT_ZERO){

			Status=true;
		}
		else{
			Status=false;
		}

		return Status;
	}


	//This Function is used to Trim the white space
	public static String trimValues(String Value){
		if(Value == null){
			return null;
		}
		else{
			String CheckValue=Value.trim();
			if(CheckValue.length()<GLOBALCONSTANT.IntOne){
				return null;
			}else{      
				return CheckValue;
			}
		}
	}



	//check teammember Availability 
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<Object> checkTeamember(String Teammember, String Tenant) {
		
		List<Object> objList;

		objList=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_Availability_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Teammember.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase()).list();	

		return objList;
	}



}
