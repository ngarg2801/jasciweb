/**
 * Date Developed  Jan 15 2015
 * Created By "Rahul Kumar"
 * Description : this class provide data base function of Location label print entity
 *
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LOCATION;
import com.jasci.biz.AdminModule.model.LOCATIONWIZARD;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class LOCATIONLABELPRINTDALIMPL implements ILOCATIONLABELPRINTDAL {
	@Autowired
	private SessionFactory sessionFactory ;

	/**
	 *@Description  get general code from general code table base on general code id
	 *@Auther Rahul Kumar
	 *@Date Jan 15, 2015	
	 *@param Tenant
	 *@param Company
	 *@param GeneralCodeId
	 *@return
	 *@throws JASCIEXCEPTION
	 *@see com.jasci.biz.AdminModule.dao.ILOCATIONLABELPRINTDAL#getGeneralCode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId) throws JASCIEXCEPTION {
		
		List<GENERALCODES> objGeneralcodes=null;

		try{
			objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.Generalcode_Select_Conditional_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company.trim().toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.trim().toUpperCase()).list();
		}catch (SQLGrammarException objGrammarException){
			throw new JASCIEXCEPTION(objGrammarException.getMessage());
		}
		catch(Exception objException){
			
			throw new JASCIEXCEPTION(objException.getMessage());
		}

		return objGeneralcodes;
	}

	/**
	 *@Description 
	 *@Auther Rahul Kumar
	 *@Date Jan 19, 2015	
	 *@param objCommonsessionbe
	 *@param Area
	 *@param Loaction
	 *@return
	 *@throws JASCIEXCEPTION
	 *@see com.jasci.biz.AdminModule.dao.ILOCATIONLABELPRINTDAL#getLocation(com.jasci.common.utilbe.COMMONSESSIONBE, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATION> getLocation(COMMONSESSIONBE objCommonsessionbe, String Area, String Loaction)
			throws JASCIEXCEPTION {
		List<LOCATION> LocationList=null;
		  try{ 
		   Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameUniqueLocationCheck);
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objCommonsessionbe.getTenant().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objCommonsessionbe.getCompany().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, String.valueOf(objCommonsessionbe.getFulfillmentCenter()).trim().toUpperCase());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, Area.trim().toUpperCase());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_FOUR, Loaction.trim().toUpperCase());
		   LocationList = ObjQuery.list();
		   return LocationList;
		  }catch (SQLGrammarException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception ObjectException){
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		  }
		  finally{
		   return LocationList;
		  }  
	}
	
	
	/**
	 * 
	 *@Description get location name from table Locations 
	 *@Auther sarvendra tyagi
	 *@Date feb 23, 2015			
	 *@Return type List<LOCATION>
	 *@param objCommonsessionbe
	 *@param Area
	 *@param Loaction
	 *@return
	 *@throws JASCIEXCEPTION
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LOCATION> getLocationReprint(COMMONSESSIONBE objCommonsessionbe,String Area)throws JASCIEXCEPTION{
		
		List<LOCATION> LocationList=null;
		  try{ 
		   Query ObjQuery = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.GETLOCATIONDATA);
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objCommonsessionbe.getTenant().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objCommonsessionbe.getCompany().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, String.valueOf(objCommonsessionbe.getFulfillmentCenter()).trim().toUpperCase());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, Area.trim().toUpperCase());
		   LocationList = ObjQuery.list();
		  }catch (SQLGrammarException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception objException){
			  new JASCIEXCEPTION(objException.getMessage());
		  }
		  return LocationList;
	}
	

	/**
	 *@Description 
	 *@Auther Rahul Kumar
	 *@Date Jan 20, 2015			
	 *@param objCommonsessionbe
	 *@param WizardControlNumber
	 *@return
	 *@throws JASCIEXCEPTION
	 *@see com.jasci.biz.AdminModule.dao.ILOCATIONLABELPRINTDAL#getWizardControlNumber(com.jasci.common.utilbe.COMMONSESSIONBE, java.lang.String)
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATION> getWizardControlNumber(COMMONSESSIONBE objCommonsessionbe,String WizardControlNumber)
			throws JASCIEXCEPTION {
		
		List<LOCATION> LocationList=null;
		  try{ 
		   Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameUniqueWizardControlNumberCheck);
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objCommonsessionbe.getTenant().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objCommonsessionbe.getCompany().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, String.valueOf(objCommonsessionbe.getFulfillmentCenter()).trim().toUpperCase());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, Integer.parseInt(WizardControlNumber.trim()));
		   LocationList = ObjQuery.list();
		   return LocationList;
		  }catch (SQLGrammarException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception ObjectException){
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		  }
		  finally{
		   return LocationList;
		  }  
	}

	/**
	 * @Description get Location Wizard list from table wizard location
	 * @author Rahul Kumar
	 * @Date Jan 21, 2015
	 * @param objCommonsessionbe
	 * @return
	 * @throws JASCIEXCEPTION
	  *@see com.jasci.biz.AdminModule.dao.ILOCATIONLABELPRINTDAL#getLocationWizardList(com.jasci.common.utilbe.COMMONSESSIONBE)
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATIONWIZARD> getLocationWizardList(COMMONSESSIONBE objCommonsessionbe,String FulfillmentCenterid) throws JASCIEXCEPTION {
		
		 List<LOCATIONWIZARD> oLocationwizards=null;
		 try{ 
			   Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_Wizard_QueryNameFetchWizardList);
			   ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objCommonsessionbe.getTenant().toUpperCase().trim());
			   ObjQuery.setParameter(GLOBALCONSTANT.IntOne, String.valueOf(FulfillmentCenterid).trim().toUpperCase());
			   ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, objCommonsessionbe.getCompany().toUpperCase().trim());
			   oLocationwizards = ObjQuery.list();
			   return oLocationwizards;
			  }catch (SQLGrammarException objGrammarException){
					throw new JASCIEXCEPTION(objGrammarException.getMessage());
				}
			  catch(Exception ObjectException){
			   throw new JASCIEXCEPTION(ObjectException.getMessage());
			  }
			  finally{
			   return oLocationwizards;
			  }  
		
	}

	 
	 /**
	  * 
	  *@Description get Location Label print type  from table Locations 
	  *@Auther Rahul Kumar
	  *@Date Jan 29, 2015			
	  *@Return type List<LOCATION>
	  *@param WizardControlNumber
	  *@param StrLocation
	  *@return
	  *@throws JASCIEXCEPTION
	  */
	@SuppressWarnings({ GLOBALCONSTANT.Strfinally,GLOBALCONSTANT.Strunchecked })
	public List<LOCATION> getLocationLabelTypePrint(COMMONSESSIONBE objCommonsessionbe, String Area)
			throws JASCIEXCEPTION {
		
		List<LOCATION> LocationList=null;
		  try{ 
		   Query ObjQuery = sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.Location_QueryNameLocationLabelTypePrint);
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_ZERO, objCommonsessionbe.getTenant().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.IntOne, objCommonsessionbe.getCompany().toUpperCase().trim());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_TWO, String.valueOf(objCommonsessionbe.getFulfillmentCenter()).trim().toUpperCase());
		   ObjQuery.setParameter(GLOBALCONSTANT.INT_THREE, Area.trim());
		   LocationList = ObjQuery.list();
		   return LocationList;
		  }catch (SQLGrammarException objGrammarException){
				throw new JASCIEXCEPTION(objGrammarException.getMessage());
			}
		  catch(Exception ObjectException){
		   throw new JASCIEXCEPTION(ObjectException.getMessage());
		  }
		  finally{
		   return LocationList;
		  }  
		
	}

}
