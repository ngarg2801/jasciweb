/**

Date Developed  Nov 14 2014
Description  TEAMMEMBERMESSAGESDAOIMPL implementation of ITEAMMEMBERMESSAGESDAO
 */

package com.jasci.biz.AdminModule.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class TEAMMEMBERMESSAGESDAOIMPL implements ITEAMMEMBERMESSAGESDAO
{

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	COMMONSESSIONBE ObjCommonSessionBe;
	
	 /**
	 *Description This function design to insert messages into database
	 *Input parameter objTeamMemberMessages
	 *Return Type Boolean
	 *Created By "Diksha Gupta"
	 *Created Date "Nov 16 2014" 
	 * */
	

	public Boolean InsertRow(TEAMMEMBERMESSAGES objTeammemberMessages) throws JASCIEXCEPTION 
	{
 	Boolean Status=false;
		Session session = sessionFactory.openSession();
		Transaction TransactionObject = session.beginTransaction();
			    		try{
			          		session.save(objTeammemberMessages);
			         		}catch(Exception ObjException)
		        		    {
			         			throw new JASCIEXCEPTION(ObjException.getMessage());
		        		    }

	        		try{
						TransactionObject.commit();
						Status=true;
						
					}catch(SQLGrammarException objException){

						throw new JASCIEXCEPTION(objException.getMessage());

					}catch(Exception ObjException){
						throw new JASCIEXCEPTION(ObjException.getMessage());
					}
			
		   session.close();
		   return Status;
		
	}
	
	

	

	

	/**
	 *Description This function design to get translation according to screen and language from LANGUAGES table
	 *Input parameter StrLanguage
	 *Return Type List<LANGUAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 18 2014" 
	 * */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> GetLanguage(String StrLanguage) throws JASCIEXCEPTION 
	{
		
		List<LANGUAGES> objLanguages=null;
		try
		{
			
			
			objLanguages= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMemberMessages_ScreenLabels_Query).setParameter(GLOBALCONSTANT.INT_ZERO, GLOBALCONSTANT.TeamMemberMessagesMaintenance_ScreenName).setParameter(GLOBALCONSTANT.IntOne, StrLanguage.toUpperCase()).list();
			
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(ObjException.getMessage());
		}
		return objLanguages;

	}

	

	

	/**
	 *Description This function design to get TeamMember data from TeamMember table
	 *Input parameter TeamMemberName,PartOfTeamMember,StrDepartment,StrShift,Tenant, Company
	 *Return Type List<TEAMMEMBERSBE>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 17 2014" 
	 * */
	
	
	
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERSBE> getTeamMemberList(String StrTeamMemberName,String StrPartTeamMember, String StrDepartment, String StrShift, String StrTenant, String StrCompany) throws JASCIEXCEPTION 
	 {
	  	 
	  List<TEAMMEMBERS> ObjListTeamMembers=null;
	  List<GENERALCODES> ObjListGeneralCodes=null;
	  
	  try
	  {
		  if(!StrTeamMemberName.equals(GLOBALCONSTANT.BlankString))
	  
	  {
		ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_TeamMember_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrTeamMemberName.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,StrTenant.toUpperCase()).list();

		if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
		{
			
	 try{
				 ObjListGeneralCodes= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_GeneralCodes_Department_ShiftQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,StrCompany.toUpperCase()).list();
			}catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}catch(Exception ObjException)
			 {
				throw new JASCIEXCEPTION(ObjException.getMessage());
			 }
			

		}
	  }
	  else if(!StrPartTeamMember.equals(GLOBALCONSTANT.BlankString))
	  {
		  ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_PartOfTeamMember_ConditionalQuery).
					setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).
					setString(GLOBALCONSTANT.LNAME, GLOBALCONSTANT.StrPercentSign+StrPartTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
					setString(GLOBALCONSTANT.FNAME, GLOBALCONSTANT.StrPercentSign+StrPartTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
					setString(GLOBALCONSTANT.MNAME, GLOBALCONSTANT.StrPercentSign+StrPartTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
					list();
		  
		  
		  if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
			{
		
				 try{
					 ObjListGeneralCodes= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_GeneralCodes_Department_ShiftQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,StrCompany.toUpperCase()).list();
				}catch(SQLGrammarException objException){

					throw new JASCIEXCEPTION(objException.getMessage());

				}catch(Exception ObjException)
				 {
					throw new JASCIEXCEPTION(ObjException.getMessage());
				 }
				

			}
	  }
	  else if(!StrDepartment.equals(GLOBALCONSTANT.BlankString))
	  {
		  ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_Department_Query).setParameter(GLOBALCONSTANT.INT_ZERO, StrDepartment.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase()).list();

		  if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
			{
	
				 try{
					 ObjListGeneralCodes= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_GeneralCodes_Department_ShiftQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,StrCompany.toUpperCase()).list();
				}catch(SQLGrammarException objException){

					throw new JASCIEXCEPTION(objException.getMessage());

				}catch(Exception ObjException)
				 {
					throw new JASCIEXCEPTION(ObjException.getMessage());
				 }
				

			}
	  }
	  else if(!StrShift.equals(GLOBALCONSTANT.BlankString))
	  {
		  try{
			  ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_Shift_Query).setParameter(GLOBALCONSTANT.INT_ZERO, StrShift.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase()).list();

		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}catch(Exception ObjException)
		  {
			  throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
		  if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
			{
				
	             try{
					 ObjListGeneralCodes= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_GeneralCodes_Department_ShiftQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,StrCompany.toUpperCase()).list();
				}catch(Exception ObjException)
				 {
					throw new JASCIEXCEPTION(ObjException.getMessage());
				 }
				

			}
		 
	  }
	  else
	  {
		  ObjListTeamMembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_AllTeamMember_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).list();
		 if(ObjListTeamMembers!=null && !ObjListTeamMembers.isEmpty())
			{
				
				 try{
					 ObjListGeneralCodes= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_GeneralCodes_Department_ShiftQuery).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,StrCompany.toUpperCase()).list();
				}catch(SQLGrammarException objException){

					throw new JASCIEXCEPTION(objException.getMessage());

				}catch(Exception ObjException)
				 {
					throw new JASCIEXCEPTION(ObjException.getMessage());
				 }
				

			}
	  }
	  }catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception ObjException)
	  {
			throw new JASCIEXCEPTION(ObjException.getMessage());
		  }
	  
	  
	List<TEAMMEMBERSBE> objTeammembersbeList=new ArrayList<TEAMMEMBERSBE>();

	for(TEAMMEMBERS  valueTeamMembers:ObjListTeamMembers )
	{
		
		TEAMMEMBERSBE objTeammembersbe=new TEAMMEMBERSBE();
		
     	objTeammembersbe.setTenant(valueTeamMembers.getId().getTenant());
		objTeammembersbe.setTeammember(valueTeamMembers.getId().getTeamMember());
		objTeammembersbe.setFirstname(valueTeamMembers.getFirstName());
		objTeammembersbe.setLastname(valueTeamMembers.getLastName());
		objTeammembersbe.setSortname(valueTeamMembers.getFirstName() + GLOBALCONSTANT.Single_Space+valueTeamMembers.getLastName());
		if(ObjListGeneralCodes!=null && !ObjListGeneralCodes.isEmpty())
		{
		try{
			for(GENERALCODES objGeneralCode:ObjListGeneralCodes)
		{
			if(objGeneralCode.getId().getGeneralCodeID().equalsIgnoreCase(GLOBALCONSTANT.GIC_DEPARTMENT) && objGeneralCode.getId().getGeneralCode().equalsIgnoreCase(valueTeamMembers.getDepartment()))
		       {
				objTeammembersbe.setDepartment(objGeneralCode.getDescription20());
		       }
			
					 	
			
			if(objGeneralCode.getId().getGeneralCodeID().equalsIgnoreCase(GLOBALCONSTANT.ShiftCode) && objGeneralCode.getId().getGeneralCode().equalsIgnoreCase(valueTeamMembers.getShiftCode()))
				{

		         objTeammembersbe.setShiftcode(objGeneralCode.getDescription20());
		         }
			
		}
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(ObjException.getMessage());
		}
		}
		else
		{
			objTeammembersbe.setDepartment(GLOBALCONSTANT.BlankString);
			objTeammembersbe.setShiftcode(GLOBALCONSTANT.BlankString);
			
		}
			
	
		objTeammembersbeList.add(objTeammembersbe);
		

	}


	return objTeammembersbeList;

	
	
	 }
	
	
	
	
	
	/**
	 *Description This function design to get TeamMemberMessages data from TeamMemberMessages table
	 *Input parameter TeamMember
	 *Return Type List<TEAMMEMBERMESSAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	
	@SuppressWarnings({GLOBALCONSTANT.Strrawtypes,GLOBALCONSTANT.Strunchecked})
	public List getTeamMemberMessages(String strTeamMember) throws JASCIEXCEPTION 
	{
		String TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery = GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_First + ObjCommonSessionBe.getClietnDeviceTimeZone() + GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_Second+ ObjCommonSessionBe.getClietnDeviceTimeZone() + GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_Third ;
		List objListTeamMemberMessages=null;
		
		String StrTenant=ObjCommonSessionBe.getTenant();
		String StrCompany=ObjCommonSessionBe.getCompany();
		try
		{
		 objListTeamMemberMessages=sessionFactory.getCurrentSession().createSQLQuery(TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, strTeamMember.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, StrCompany.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE,GLOBALCONSTANT.TeamMemberMessageStatus).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(ObjException.getMessage());
		}
		
		return objListTeamMemberMessages;
	}
	
	
	
	
	
	
	/**
	 *Description This function design to get TeamMemberMessages data from TeamMemberMessages table
	 *Input parameter TeamMember
	 *Return Type List<TEAMMEMBERMESSAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	

	public List<?> getTeamMemberMessagesList(String strTeamMember) throws JASCIEXCEPTION 
	{
		String TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery = GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_First + ObjCommonSessionBe.getClietnDeviceTimeZone() + GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_Second+ ObjCommonSessionBe.getClietnDeviceTimeZone() + GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery_Third ;
		List<?> objListTeamMemberMessages=null;
		
		  String StrTenant=ObjCommonSessionBe.getTenant();
		  String StrCompany=ObjCommonSessionBe.getCompany();
		  try{
			  objListTeamMemberMessages=sessionFactory.getCurrentSession().createSQLQuery(TeamMembersMessages_SelectTeamMemberMessages_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, strTeamMember.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO,StrCompany.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE,GLOBALCONSTANT.TeamMemberMessageStatus).list();
		  }catch(SQLGrammarException objException){

				throw new JASCIEXCEPTION(objException.getMessage());

			}catch(Exception ObjException)
			 {
				throw new JASCIEXCEPTION(ObjException.getMessage());

		 }
		 
		
		return objListTeamMemberMessages;
	}
	
	
	/**
	 *Description This function design to delete messages from TeamMemberMessages table
	 *Input parameter TEAMMEMBERMESSAGES
	 *Return Type Boolean
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	public Boolean DeleteMessages(TEAMMEMBERMESSAGES objTeammembermessages) throws JASCIEXCEPTION 
	{
		
		    	  Boolean Status=false;
		 		  Session session = sessionFactory.openSession();
		 		  Transaction TransactionObject = session.beginTransaction();
		 		  try{
		 			  session.delete(objTeammembermessages);
		 		      TransactionObject.commit();
		 		      Status=true;
		 		  }catch(HibernateException objException){

		 				throw new JASCIEXCEPTION(objException.getMessage());

		 			}catch(Exception ObjException)
		 		  {
		 			  Status=false;
		 			  throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);

		 		  }
		 		  
		 		return Status;
	}



	/**
	 *Description This function design to get messages from TeamMemberMessages table
	 *Input parameter Tenant,TeamMember,company,MessageNumber
	 *Return Type List<TEAMMEMBERMESSAGES>
	 *Created By "Diksha Gupta"
	 *Created Date "nov 19 2014" 
	 * */
	

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERMESSAGES> GetMessagesToEdit(String strTenant,String strTeamMember, String strCompany, String MessageNumber) throws JASCIEXCEPTION 
	{
		List<TEAMMEMBERMESSAGES>  objListTeamMemberMessages=null;
		try{
			objListTeamMemberMessages=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMemberMessages_Edit_Query).setParameter(GLOBALCONSTANT.INT_ZERO, strTeamMember.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, strTenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, strCompany.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, MessageNumber).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
		
				throw new JASCIEXCEPTION(ObjException.getMessage());

		 
		}

		return objListTeamMemberMessages;
	}



	/**
	 *Description This function design to update messages into TeamMemberMessages table
	 *Input parameter objTeamMemberMessagesBe
	 *Return Type Boolean 
	 *Created By "Diksha Gupta"
	 *Created Date "nov 24 2014" 
	 * */
	
	public Boolean UpdateMessages(TEAMMEMBERMESSAGES objTeammembersMessages) throws JASCIEXCEPTION 
	{
          Boolean Status=false;
		  Session session = sessionFactory.openSession();
	      Transaction TransactionObject = session.beginTransaction();
 		  try
 		  {  
			  
		   session.update(objTeammembersMessages);
		   }
 		  catch(Exception ObjException){}
 		  try{
 			  
		   TransactionObject.commit();
		   
		   session.getIdentifier(objTeammembersMessages);
		   Status=true;
		  }catch(Exception ObjException){
		   Status=false;
		   throw new JASCIEXCEPTION(ObjException.getMessage());
		   
		   
		  }
		  
		  session.close();
		  return Status;
	}
	
	
	

	/**
	 * Created on:Nov 28 2014
	 * Created by:Diksha Gupta
	 * Description: This function  get General code list based on tenant,company,generalCodeId
	 * Input parameter: String
	 * Return Type :List<GENERALCODES>
	 * 
	 */
	//@Override
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<GENERALCODES> getGeneralCode(String Tenant, String Company, String GeneralCodeId)
			throws  JASCIEXCEPTION {

		List<GENERALCODES> objGeneralcodes=null;

		try{
			objGeneralcodes=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMemberMessages_Generalcode_Select_Conditional_Query).setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Company.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, GeneralCodeId.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch(Exception objException){
			
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}

		return objGeneralcodes;


	}

	/**
     * @Description get team member name form team members table based on team member id
     * @param Tenant
     * @param Company
     * @param TeamMember
     * @return
     * @throws JASCIEXCEPTION
     */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS>getTeamMemberName(String Tenant, String TeamMember) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers= sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.MenuAppIcon_GetTeamMember_Name_Query).setParameter(GLOBALCONSTANT.INT_ZERO,Tenant.trim().toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, TeamMember.trim().toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
			
			
		}
		
		return objTeammembers;
	}
	
	/**
     * @Description check if team member id exists.
     * @param Tenant
     * @param TeamMemberId
     * @return List<TEAMMEMBERS>
     * @throws JASCIEXCEPTION
     */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS> CheckTeamMemberId(String Tenant, String TeamMember) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_TeamMember_ConditionalQuery).setParameter(GLOBALCONSTANT.INT_ZERO, TeamMember.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne,Tenant.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
			
			
		}
		
		return objTeammembers;
	}
	
	
	
	/**
     * @Description check if part of teamMember exists.
     * @param Tenant
     * @param PartTeamMember
     * @return List<TEAMMEMBERS>
     * @throws JASCIEXCEPTION
     */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS> CheckTeamMemberName(String Tenant, String StrPartTeamMember) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembers_SelectTeamMember_PartOfTeamMember_ConditionalQuery).
					setParameter(GLOBALCONSTANT.INT_ZERO, Tenant.toUpperCase()).
					setString(GLOBALCONSTANT.LNAME, GLOBALCONSTANT.StrPercentSign+StrPartTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
					setString(GLOBALCONSTANT.FNAME, GLOBALCONSTANT.StrPercentSign+StrPartTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
					setString(GLOBALCONSTANT.MNAME, GLOBALCONSTANT.StrPercentSign+StrPartTeamMember.trim().toUpperCase()+GLOBALCONSTANT.StrPercentSign).
					list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
		}
		
		return objTeammembers;
	}
	
	
	/**
     * @Description check if valid department is selected
     * @param Tenant
     * @param StrDepartment
     * @return List<TEAMMEMBERS>
     * @throws JASCIEXCEPTION
     */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS> CheckDepartment(String Tenant, String StrDepartment) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers=sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_Department_Query).setParameter(GLOBALCONSTANT.INT_ZERO, StrDepartment.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, Tenant.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
			
			
		}
		
		return objTeammembers;
	}
	
	
	/**
     * @Description check if valid shift is selected
     * @param Tenant
     * @param StrShift
     * @return List<TEAMMEMBERS>
     * @throws JASCIEXCEPTION
     */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERS>CheckShift(String StrTenant, String StrShift) throws JASCIEXCEPTION {
	
		List<TEAMMEMBERS> objTeammembers=null;
		try
		{
			objTeammembers = sessionFactory.getCurrentSession().createQuery(GLOBALCONSTANT.TeamMembersMessages_SelectTeamMember_Shift_Query).setParameter(GLOBALCONSTANT.INT_ZERO, StrShift.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase()).list();
		}catch(SQLGrammarException objException){

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.DataBaseException);
			
			
		}
		
		return objTeammembers;
	}
}
