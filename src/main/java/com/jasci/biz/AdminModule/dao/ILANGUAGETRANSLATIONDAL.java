/**

Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :ILANGUAGETRANSLATIONDAL interface.
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.exception.JASCIEXCEPTION;

public interface ILANGUAGETRANSLATIONDAL {
	
	/** To get LanguageList based on Tenant, Company, GeneralCodeId */

	public List<GENERALCODES> getLanguageList(String Tenant, String Company, String GeneralCodeId)
			throws  JASCIEXCEPTION;
	
	/** To Check if the entered Value of language id=s valid or not 
	 * @throws JASCIEXCEPTION */

	public List<LANGUAGES> CheckLanguage(String language) throws JASCIEXCEPTION;

	/** To Check if The entered value of keyphrase is valid or not 
	 * @throws JASCIEXCEPTION */

	public List<LANGUAGES> CheckKeyPhrase(String keyPhrase) throws JASCIEXCEPTION;
	
	/** To delete a record from languages
	 * @throws JASCIEXCEPTION */

	public Boolean DeleteLanguage(LANGUAGES objLanguages) throws JASCIEXCEPTION;
	
	/** to get language data based on entered Language
	 * @throws JASCIEXCEPTION */
	public List<LANGUAGES> getLanguage(String strLanguage, String strKeyPhrase) throws JASCIEXCEPTION;

    /** to add a record of language .
     * @throws JASCIEXCEPTION */

	public Boolean addLanguageData(LANGUAGES objLanguage) throws JASCIEXCEPTION;

	/** To get Language data
	 * @throws JASCIEXCEPTION */ 

	public List<LANGUAGES> GetLanguage(String strLanguage) throws JASCIEXCEPTION;

	/** To edit the record of  the language data
	 * @throws JASCIEXCEPTION */

	public Boolean editLanguageData(LANGUAGES objLanguage) throws JASCIEXCEPTION;

	/** To check if that record already exist in the language table
	 * @throws JASCIEXCEPTION */

	public List<LANGUAGES> CheckAlreadyExist(String language, String keyPhrase) throws JASCIEXCEPTION;

	public List<TEAMMEMBERS> getTeamMemberName(String tenant,
			String lastActivityTeamMember) throws JASCIEXCEPTION;

}
