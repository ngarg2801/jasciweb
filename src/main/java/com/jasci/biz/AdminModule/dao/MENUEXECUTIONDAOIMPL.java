/*
Date Developed  Nov 14 2014
Description   get the data from Menu_Options table and execute that menu option
Created By "Sarvendra Tyagi"
 */


package com.jasci.biz.AdminModule.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.exception.GenericJDBCException;
import org.hibernate.exception.SQLGrammarException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.CannotCreateTransactionException;

import com.jasci.biz.AdminModule.be.MENUEXECUTIONLISTBE;
import com.jasci.biz.AdminModule.be.SUBMENULISTBE;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIESBEAN;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Repository
public class MENUEXECUTIONDAOIMPL implements IMENUEXECUTIONDAO {


	/**
	 *Description That function design for get CompanyList and return TEAMMEMBERCOMPANIESBEAN list" 
	 *Input parameter "CommonSessionObj"
	 *Return Type: "TEAMMEMBERCOMPANIESBEAN"
	 *Created By: "Sarvendra Tyagi"
	 *Created Date: "11-18-2014" 
	 * */

	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<TEAMMEMBERCOMPANIESBEAN> GetDataFromTeamMemberCompanies(COMMONSESSIONBE ObjCommonSession)throws JASCIEXCEPTION
	{
		List<Object> listObject=null;
		List<TEAMMEMBERCOMPANIESBEAN> listTeamMemberCompnies= new ArrayList<TEAMMEMBERCOMPANIESBEAN>();
		TEAMMEMBERCOMPANIESBEAN objTeammembercompaniesbean=null;


		try{



			listObject=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_Team_Member_company).setString(GLOBALCONSTANT.InputQuery_TeamMember_TeamMember, ObjCommonSession.getTeam_Member().toUpperCase())
					.setString(GLOBALCONSTANT.InputQuery_MenuProfileAssigment_Tenant, ObjCommonSession.getTenant().toUpperCase()).list();


			Iterator<Object> ObjIteratorObject=null;
			Object[] ObjObject = null;

			if(!(listObject.isEmpty()))
			{




				ObjIteratorObject=listObject.iterator();
				for(int CompanyIndex=GLOBALCONSTANT.INT_ZERO;CompanyIndex<listObject.size();CompanyIndex++){
					ObjObject=(Object[]) ObjIteratorObject.next();
					objTeammembercompaniesbean=new TEAMMEMBERCOMPANIESBEAN();
					objTeammembercompaniesbean.setIntKendoID(CompanyIndex+GLOBALCONSTANT.IntOne);
					objTeammembercompaniesbean.setCompany(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
					objTeammembercompaniesbean.setName20(ObjObject[GLOBALCONSTANT.IntOne].toString());
					try{
						objTeammembercompaniesbean.setPurchaseOrderApproval(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
					}catch(ArrayIndexOutOfBoundsException e){
						objTeammembercompaniesbean.setPurchaseOrderApproval(GLOBALCONSTANT.Single_Space);
					}catch(Exception e){
						objTeammembercompaniesbean.setPurchaseOrderApproval(GLOBALCONSTANT.Single_Space);
					}
					
					try{
						objTeammembercompaniesbean.setComapnyLogo(ObjObject[GLOBALCONSTANT.INT_THREE].toString());
					}catch(ArrayIndexOutOfBoundsException e){

						objTeammembercompaniesbean.setComapnyLogo(GLOBALCONSTANT.Single_Space);
					}catch(Exception e){

						objTeammembercompaniesbean.setComapnyLogo(GLOBALCONSTANT.Single_Space);
					}

					try{
						objTeammembercompaniesbean.setTMobile(ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
					}catch(ArrayIndexOutOfBoundsException e){

						objTeammembercompaniesbean.setTMobile(GLOBALCONSTANT.Single_Space);
					}catch(Exception e){

						objTeammembercompaniesbean.setTMobile(GLOBALCONSTANT.Single_Space);
					}

					try{
						objTeammembercompaniesbean.setTRF(ObjObject[GLOBALCONSTANT.INT_FIVE].toString());
					}catch(ArrayIndexOutOfBoundsException e){

						objTeammembercompaniesbean.setTRF(GLOBALCONSTANT.Single_Space);
					}catch(Exception e){

						objTeammembercompaniesbean.setTRF(GLOBALCONSTANT.Single_Space);
					}

					try{
						objTeammembercompaniesbean.setTFullDisplay(ObjObject[GLOBALCONSTANT.INT_SIX].toString());
					}catch(ArrayIndexOutOfBoundsException e){

						objTeammembercompaniesbean.setTFullDisplay(GLOBALCONSTANT.Single_Space);
					}catch(Exception e){

						objTeammembercompaniesbean.setTFullDisplay(GLOBALCONSTANT.Single_Space);
					}
					listTeamMemberCompnies.add(objTeammembercompaniesbean);


				}
			}

		}catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_CannotCreateTransactionException);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_GenericJDBCException);
		}
		catch(NullPointerException ObjNullPointerException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_NullPointerException);
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}	
		return listTeamMemberCompnies;

	}


	/**
	 *Description That function design for get Menu list and return MENUEXECUTIONLISTBE" 
	 *Input parameter "CommonSessionObj"
	 *Return Type "MENUEXECUTIONLISTBE"
	 *Created By "Sarvendra Tyagi"
	 *Created Date "11-26-2014" 
	 * */



	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<MENUEXECUTIONLISTBE> GetAssignedMenuProfileList(
			COMMONSESSIONBE ObjCommonSession)
					throws JASCIEXCEPTION {
		

		List<Object> listObject=null;
		MENUEXECUTIONLISTBE objMenuexecutionlistbe=null;
		List<MENUEXECUTIONLISTBE> listTeamMemberCompnies=null;		
		try{

			listTeamMemberCompnies= new ArrayList<MENUEXECUTIONLISTBE>();

			listObject=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfileAssigment).setString(GLOBALCONSTANT.InputQuery_TeamMember_TeamMember,
					ObjCommonSession.getTeam_Member().toUpperCase())
					.setString(GLOBALCONSTANT.InputQuery_MenuProfileAssigment_Tenant, ObjCommonSession.getTenant().toUpperCase())
					.setString(GLOBALCONSTANT.InputQuery_MenuProfileAssigment_MenuType, ObjCommonSession.getMenuType().toUpperCase()).list();

			Iterator<Object> ObjIteratorObject=null;
			Object[] ObjObject = null;

			if(!(listObject.isEmpty()))
			{
				ObjIteratorObject=listObject.iterator();

				for(int indexProfile=GLOBALCONSTANT.INT_ZERO;indexProfile<listObject.size();indexProfile++){
					ObjObject=(Object[]) ObjIteratorObject.next();
					objMenuexecutionlistbe=new MENUEXECUTIONLISTBE();
					try{
						objMenuexecutionlistbe.setMenuProfile(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
					}catch(ArrayIndexOutOfBoundsException obj_e){}
					catch(Exception obj_e){}
					try{
						objMenuexecutionlistbe.setMenuAppIcon(ObjObject[GLOBALCONSTANT.IntOne].toString());
					}catch(ArrayIndexOutOfBoundsException obj_e){}catch(Exception obj_e){}
					try{				
						objMenuexecutionlistbe.setMenuAppIconAddress(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
					}catch(ArrayIndexOutOfBoundsException obj_e){}catch(Exception obj_e){}
					try{
						objMenuexecutionlistbe.setToolTipValue(ObjObject[GLOBALCONSTANT.INT_THREE].toString());
					}catch(ArrayIndexOutOfBoundsException obj_e){}catch(Exception obj_e){}

					listTeamMemberCompnies.add(objMenuexecutionlistbe);

				}




			}



		}catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_CannotCreateTransactionException);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_GenericJDBCException);
		}
		catch(NullPointerException ObjNullPointerException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_NullPointerException);
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}	


		return listTeamMemberCompnies;
	}



	/*
	 *Description That function design for get Menu list and return submenu List" 
	 *Input parameter "CommonSessionObj"
	 *Return Type subMenuListBE
	 *Created By "Sarvendra Tyagi"
	 *Created Date "12-3-2014" 
	 * */

	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public SUBMENULISTBE GetSubMenuList(COMMONSESSIONBE ObjCommonSession)
			throws JASCIEXCEPTION{

		List<Object> listObject=null;
		SUBMENULISTBE objSubmenulistbe=null;
		Map<String, String> hmSubMenuToolip=null;
		try{
if(ObjCommonSession.getSystemUse().equalsIgnoreCase(GLOBALCONSTANT.Yes))
{
	
	if(ObjCommonSession.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSIONBE.getJasci_Tenant()))
	{
			listObject=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.NativeQueryName_MenuOption_query_JASCI).
					setString(GLOBALCONSTANT.InputQuery_MenuOption_MenuProfile, ObjCommonSession.getAuthority_Profile().toUpperCase())
					.setString(GLOBALCONSTANT.InputQuery_MenuProfileAssigment_Tenant, ObjCommonSession.getTenant().toUpperCase()).list();
	}else{
		listObject=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuProfileOptions).
				setString(GLOBALCONSTANT.InputQuery_MenuOption_MenuProfile, ObjCommonSession.getAuthority_Profile().toUpperCase())
				.setString(GLOBALCONSTANT.InputQuery_MenuProfileAssigment_Tenant, ObjCommonSession.getTenant().toUpperCase())
				.setString(GLOBALCONSTANT.MenuProfileAssigment_tenant1, OBJCOMMONSESSIONBE.getJasci_Tenant()).list();
	}
		
}else{
	if(ObjCommonSession.getTenant().toString().equalsIgnoreCase(OBJCOMMONSESSIONBE.getJasci_Tenant()))
	{
	listObject=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.NativeQueryName_MenuOption_query_SystemUseY_JASCI).
	setString(GLOBALCONSTANT.InputQuery_MenuOption_MenuProfile, ObjCommonSession.getAuthority_Profile().toUpperCase())
	.setString(GLOBALCONSTANT.InputQuery_MenuProfileAssigment_Tenant, ObjCommonSession.getTenant().toUpperCase())
	.setParameter("EXECUTIONPATH", GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution.toUpperCase()).list();
	}else{
		listObject=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.NativeQueryName_MenuOption_query_SystemUseY).
				setString(GLOBALCONSTANT.InputQuery_MenuOption_MenuProfile, ObjCommonSession.getAuthority_Profile().toUpperCase())
				.setString(GLOBALCONSTANT.InputQuery_MenuProfileAssigment_Tenant, ObjCommonSession.getTenant().toUpperCase())
				.setParameter("EXECUTIONPATH", GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution.toUpperCase())
				.setString(GLOBALCONSTANT.MenuProfileAssigment_tenant1, OBJCOMMONSESSIONBE.getJasci_Tenant()).list();
	}
}


			Iterator<Object> ObjIteratorObject=null;
			Object[] ObjObject = null;

			if(!(listObject.isEmpty()))
			{
				ObjIteratorObject=listObject.iterator();
				objSubmenulistbe=new SUBMENULISTBE();
				for(int MenuIndex=GLOBALCONSTANT.IntZero;MenuIndex<listObject.size();MenuIndex++){
					ObjObject=(Object[]) ObjIteratorObject.next();

					

					if(ObjObject[GLOBALCONSTANT.INT_TWO]==null){

						ObjObject[GLOBALCONSTANT.INT_TWO]=GLOBALCONSTANT.Single_Space;
					}

					if(!objSubmenulistbe.getHmListSubMenu().containsKey(ObjObject[GLOBALCONSTANT.INT_TWO].toString())){

						hmSubMenuToolip=new HashMap<String, String>();

						if(ObjObject[GLOBALCONSTANT.INT_TWO].toString().equalsIgnoreCase(GLOBALCONSTANT.Single_Space)){
							objSubmenulistbe.getHmListSubMenu().put(ObjObject[GLOBALCONSTANT.INT_ZERO].toString()+GLOBALCONSTANT.DOUBLEHASH+ObjObject[3].toString()+GLOBALCONSTANT.DOUBLEHASH+ObjObject[4].toString(), hmSubMenuToolip);

						}else{
							//change by rahul kumar  10 aug 15 for get common session in glass 
							
							String jSonValue=GLOBALCONSTANT.BlankString;
							try{
							jSonValue=getValueToGlass(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
							}catch(JASCIEXCEPTION objJasciexception){
								
							}
							hmSubMenuToolip.put(ObjObject[GLOBALCONSTANT.INT_ZERO].toString(), ObjObject[GLOBALCONSTANT.INT_THREE].toString()+jSonValue+GLOBALCONSTANT.DOUBLEHASH+ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
							objSubmenulistbe.getHmListSubMenu().put(ObjObject[GLOBALCONSTANT.INT_TWO].toString(), hmSubMenuToolip);
						}


					}else{
						String jSonValue=GLOBALCONSTANT.BlankString;
						try{
						jSonValue=getValueToGlass(ObjObject[GLOBALCONSTANT.INT_ZERO].toString());
						}catch(JASCIEXCEPTION objJasciexception){
							
						}

						hmSubMenuToolip=objSubmenulistbe.getHmListSubMenu().get(ObjObject[GLOBALCONSTANT.INT_TWO].toString());
						hmSubMenuToolip.put(ObjObject[GLOBALCONSTANT.INT_ZERO].toString(), ObjObject[GLOBALCONSTANT.INT_THREE].toString()+jSonValue+GLOBALCONSTANT.DOUBLEHASH+ObjObject[GLOBALCONSTANT.INT_FOUR].toString());
						objSubmenulistbe.getHmListSubMenu().put(ObjObject[GLOBALCONSTANT.INT_TWO].toString(), hmSubMenuToolip);

					}
				}




			}


		}catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_CannotCreateTransactionException);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_GenericJDBCException);
		}
		catch(NullPointerException ObjNullPointerException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_NullPointerException);
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}	

		return objSubmenulistbe;
	}







	/**
	 *Description: This function design for get Menu list and return submenu List" 
	 *Input parameter language and Screen Name
	 *Return Type :object
	 *Created By "Sarvendra Tyagi"
	 *Created Date "12-7-2014" 
	 * */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)

	public List<LANGUAGES> GetScreenLabel(String language,String screenName) throws JASCIEXCEPTION{

		List<LANGUAGES> listObject=null;
		try {




			listObject=sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuExecutionLabel)
					.setString(GLOBALCONSTANT.InputQuery_MenuExecutionLabel_Language, language.toUpperCase())
					.setString(GLOBALCONSTANT.InputQuery_MenuExecutionLabel_ScreenName, screenName.toUpperCase()).list();




		} catch(CannotCreateTransactionException ObjCannotCreateTransactionException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_CannotCreateTransactionException);
		}
		catch(GenericJDBCException ObjGenericJDBCException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_GenericJDBCException);
		}
		catch(NullPointerException ObjNullPointerException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_NullPointerException);
		}
		catch(Exception ObjException)
		{
			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);
		}

		return listObject;
	}







	/*
	 *Description: This function design to get menu messages from menuMessages table
	 *Input: parameter ObjCommonSession
	 *Return: Type "List<MENUMESSAGES>"
	 *Created: By "SarvendraTyagi"
	 *Created: Date "12-10-14" 
	 * */


	public List<MENUMESSAGES> GetMenuMessagesCompany(COMMONSESSIONBE ObjCommonSession,String strDateTime) throws JASCIEXCEPTION{	 

		try
		{


			// query to update menu messages table
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
			DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.Date_Format_DD_MM_YYYY);
			Date date_obj = formatter.parse(strDateTime);
			strDateTime=dateFormat.format(date_obj);


			Query query=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.MenuMessages_ProcedureCallQuery_Company).
					setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant,ObjCommonSession.getTenant())
					.setParameter(GLOBALCONSTANT.DataBase_TeamMembersMessages_Company,ObjCommonSession.getCompany())
					.setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Status,GLOBALCONSTANT.STR_A)
					.setParameter("DateTime",strDateTime);
			// CommonSessionBean object will be passed in setParameter();

			query.executeUpdate();
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}catch (Exception objException) {

			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);

		}
		String strTenant = GLOBALCONSTANT.BlankString;
		String strCompany = GLOBALCONSTANT.BlankString;
		try{
			strTenant=ObjCommonSession.getTenant().toUpperCase();
			strCompany=ObjCommonSession.getCompany().toUpperCase();
		}
		catch (Exception e) {
			
		}
		// query to get messages from menuMessages table


		try{
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			List<MENUMESSAGES> ObjListMenuMessages=sessionFactory.getCurrentSession()
			.createQuery(GLOBALCONSTANT.MenuMessages_company_Query)
			.setParameter(GLOBALCONSTANT.INT_ZERO,strTenant)
			.setParameter(GLOBALCONSTANT.IntOne,GLOBALCONSTANT.STR_A.toUpperCase())
			.setParameter(GLOBALCONSTANT.INT_TWO,strCompany).list();
			return ObjListMenuMessages;

		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		} 
		catch (Exception objException) {

			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);

		}	

	}

	/*
	 *Description: This function design to get menu messages from menuMessages table
	 *Input: parameter ObjCommonSession
	 *Return: Type "List<TEAMMEMBERMESSAGES>"
	 *Created: By "SarvendraTyagi"
	 *Created: Date "12-10-14" 
	 * */
	public List<TEAMMEMBERMESSAGES> GetTeamMemberMenuMessages(COMMONSESSIONBE ObjCommonSession,String date) throws JASCIEXCEPTION{	 

		try
		{
			// query to update menu messages table
			SimpleDateFormat formatter = new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
			DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.Date_Format_DD_MM_YYYY);
			Date date_obj = formatter.parse(date);
			date=dateFormat.format(date_obj);
			Query query=sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.TeamMemberMenuMessages_ProcedureCallQuery_Company).
					setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant,ObjCommonSession.getTenant())
					.setParameter(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Status,GLOBALCONSTANT.STR_A)
					.setParameter(GLOBALCONSTANT.DataBase_TeamMembersMessages_TeamMember,ObjCommonSession.getTeam_Member())
					.setParameter(GLOBALCONSTANT.DataBase_TeamMembersMessages_Company,ObjCommonSession.getCompany())
					.setParameter("DateTime",date);
			// CommonSessionBean object will be passed in setParameter();

			query.executeUpdate();
		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch (Exception objException) {

			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);

		}	
		String strTenant = GLOBALCONSTANT.BlankString;
		String strCompany = GLOBALCONSTANT.BlankString;
		String strTeamMember = GLOBALCONSTANT.BlankString;

		try{
			strTenant=ObjCommonSession.getTenant().toUpperCase();
			strCompany=ObjCommonSession.getCompany().toUpperCase();
			strTeamMember=ObjCommonSession.getTeam_Member().toUpperCase();
		}catch(Exception e){}
		// query to get messages from menuMessages table

		try{
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)

			List<TEAMMEMBERMESSAGES> ObjListMenuMessages=sessionFactory.getCurrentSession().
			createQuery(GLOBALCONSTANT.TeamMemberMenuMessages_company_Query)
			.setParameter(GLOBALCONSTANT.INT_ZERO,strTenant)
			.setParameter(GLOBALCONSTANT.IntOne,GLOBALCONSTANT.STR_A.toUpperCase())
			.setParameter(GLOBALCONSTANT.INT_TWO,strCompany)
			.setParameter(GLOBALCONSTANT.INT_THREE,strTeamMember).list();
			// CommonSessionBean object will be passed in setParameter();

			return ObjListMenuMessages;

		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch (Exception objException) {

			throw new JASCIEXCEPTION(GLOBALCONSTANT.GeneralCodes_exception_Exception);

		}	
	}

	/** 
	 * @Description: This function is used for getting label of menuExecution all screen
	 * @param  :language
	 * @return :List<LANGUAGES>
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Sarvendra Tyagi
	 * @Date :dec 26 2014
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	public List<LANGUAGES> getLanguageLabel(String language) throws JASCIEXCEPTION {

		try{

			return sessionFactory.getCurrentSession().getNamedQuery(GLOBALCONSTANT.NativeQueryName_MenuExecution_LabelQueryName)
					.setParameter(GLOBALCONSTANT.INPUT_PARAMETER_LANGUAGE, language.toUpperCase()).list();

		}catch (SQLGrammarException objException) {

			throw new JASCIEXCEPTION(objException.getMessage());

		}
		catch(Exception objException){

			throw new JASCIEXCEPTION(objException.getMessage());
		}

	}

	/** 
	 * @Description: This function is used for get common session value for glass
	 * @param  :language
	 * @return :String
	 * @throws :DATABASEEXCEPTION
	 * @throws :JASCIEXCEPTION
	 * @Developed by :Rahul Kumar
	 * @Date :Aug 10 2015
	 */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	private String getValueToGlass(String menuExcutionName)throws JASCIEXCEPTION {
		

		String result=GLOBALCONSTANT.Blank;
		String menutype=OBJCOMMONSESSIONBE.getMenuType();
		if(GLOBALCONSTANT.MENU_TYPE_GLASS.equalsIgnoreCase(menutype) || GLOBALCONSTANT.MENU_TYPE_MOBILE.equalsIgnoreCase(menutype)){
		
			JSONObject objJson=new JSONObject();
		
		objJson.put(GLOBALCONSTANT.MENU_EXECUTION_NAME, menuExcutionName);
		objJson.put(GLOBALCONSTANT.TENANT, OBJCOMMONSESSIONBE.getTenant());
		objJson.put(GLOBALCONSTANT.COMPANY,OBJCOMMONSESSIONBE.getCompany());
		objJson.put(GLOBALCONSTANT.TEAMMEMBER,OBJCOMMONSESSIONBE.getTeam_Member());
		objJson.put(GLOBALCONSTANT.TEAMMEMBERNAME,OBJCOMMONSESSIONBE.getTeam_Member_Name());
		objJson.put(GLOBALCONSTANT.FULFILLMENTCENTER, OBJCOMMONSESSIONBE.getFulfillmentCenter());		
		objJson.put(GLOBALCONSTANT.CURRENTLANGUAGE, OBJCOMMONSESSIONBE.getCurrentLanguage());
		
		String responeResult=GLOBALCONSTANT.QUESTION_MARK+GLOBALCONSTANT.COMMON_SESSION_VALUES+GLOBALCONSTANT.Equal+objJson.toString();
		result= responeResult.replaceAll(GLOBALCONSTANT.DoubleQuatos,GLOBALCONSTANT.SingleQuote);
		}
		else{
		result= GLOBALCONSTANT.Blank;
		}
		return result;	}


}