/*
Date Developed  Oct 15 2014
Description  Provide database connection
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import com.jasci.exception.JASCIEXCEPTION;


public interface ILOGINDAO {

	public List<Object> GetUserData(String strUsername,String StrPassword) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION;
	/**
	  * @description get User Data based on Active Company
	  * @author Rahul Kumar
	  * @Date Feb 09, 2015
	  * @param Tenant
	  * @param TeamMember
	  * @return
	  * @throws JASCIEXCEPTION
	  * @Return List<LANGUAGES>
	  *
	  */
	 public List<Object> GetUserDataActiveCompany(String Tenant,String TeamMember) throws JASCIEXCEPTION, JASCIEXCEPTION, JASCIEXCEPTION;
	
}
