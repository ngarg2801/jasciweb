/*

Date Developed :Dec 24 2014
Created by: Deepak Sharma
Description :USED FOR SCREEN AUTHENTICATION
 */
package com.jasci.biz.AdminModule.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

/**
 * @author HP-USA-004
 *
 */
@Repository
public class SCREENACCESSDAO {

	
	/*
	 * Purpose: to check wheater the user have screen access or not 
	 * CreatedBy:Deepak Sharma
	 * CreatedOn:2014-12-01
	 */
	//@Override	
	@Autowired
	private SessionFactory sessionFactory ;
	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSION;
	
	
	/** 
	 * 
	 * @param StrTenant
	 * @param StrTeamMember
	 * @param StrMenuType
	 * @param StrMenuOption
	 * @return
	 */
			public List<Object> checkScreenAccess(String StrTenant,String StrTeamMember,String StrMenuType,String StrMenuOption) {
				
					List<Object> objListGeneralCodes = null;
				return objListGeneralCodes;
			
				
			}
			
			/** 
			 * 
			 * @param StrTenant
			 * @param StrTeamMember
			 * @param StrExecution
			 * @return
			 * @throws JASCIEXCEPTION
			 */
			@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
			public List<Object> checkScreenAccess(String StrTenant,String StrTeamMember,String StrExecution) throws JASCIEXCEPTION {
				
				try{
				List<Object> objListGeneralCodes = null;
				
				if(OBJCOMMONSESSION.getTenant().equalsIgnoreCase(OBJCOMMONSESSION.getJasci_Tenant()))
				{
					objListGeneralCodes = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.Security_SelectScrrenAccess_JASCI).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrTeamMember.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, StrExecution.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, OBJCOMMONSESSION.getMenuType().toUpperCase()).setParameter(GLOBALCONSTANT.INT_FOUR, OBJCOMMONSESSION.getMenuType().toUpperCase()).list();
				}
				else{
					
					objListGeneralCodes = sessionFactory.getCurrentSession().createSQLQuery(GLOBALCONSTANT.Security_SelectScrrenAccess).setParameter(GLOBALCONSTANT.INT_ZERO, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.IntOne, StrTenant.toUpperCase()).setParameter(GLOBALCONSTANT.INT_TWO, StrTeamMember.toUpperCase()).setParameter(GLOBALCONSTANT.INT_THREE, StrExecution.toUpperCase()).setParameter(GLOBALCONSTANT.INT_FOUR, OBJCOMMONSESSION.getMenuType().toUpperCase()).setParameter(5, OBJCOMMONSESSION.getMenuType().toUpperCase()).list();
				}
				return objListGeneralCodes;
				
				}catch(SQLGrammarException objException){

					throw new JASCIEXCEPTION(objException.getMessage());

				}
				catch(Exception obj_e)
				{
					throw new JASCIEXCEPTION(obj_e.getMessage());
				}
			
				
			}

}
