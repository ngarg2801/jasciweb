/*
 * * File purpose:Reset  url and send input json
File CreatedBy: Pradeep Kumar
Company Developed: Next Gen Invent Corporation
Created On:21 jul 2015

 *
 */
package com.jasci.biz.AdminModule.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;










import com.jasci.common.constant.*;;


public class RESTFULLSERVICECALLER implements RESTFULLSERVICE {

	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public  String URLCaller(String url, HashMap hmInputQueryParam) {
		
		String strResponse=null;
		String strRequest=getJsonInputFiled(hmInputQueryParam);
		if(hmInputQueryParam==null)
		{
			strResponse=getServerData(url,GLOBALCONSTANT.BLANKSPACESTRING);
		} 
		else
		{  

			strResponse=getServerData(url, strRequest);
		}

		return strResponse;
	}

	//-----------------------------
	//Reques json of input parameter
	@SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strrawtypes })
	public String getJsonInputFiled(HashMap hmInputQuery)
	{
		//objLoginBE=getScreenLables();
		if(hmInputQuery!=null){
			JSONObject objJson=new JSONObject();
			HashMap hmQueryInput=hmInputQuery;
			//hmQueryInput=new HashMap<String, String>();
			Iterator iter = hmQueryInput.entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry mEntry = (Map.Entry) iter.next();
				if(GLOBALCONSTANT.ISDEBUG)
					//System.out.println("//////"+mEntry.getKey() + " : ////" + mEntry.getValue());
				objJson.put(mEntry.getKey(),mEntry.getValue());

			}
			return objJson.toString();
		}else{
			return null;
		}
	}



	//-----------------------------------------------------------


	@SuppressWarnings(GLOBALCONSTANT.Strunused)
	public static String getServerData(String strURL,String strXmlToBeSent) {

		String DataGet =GLOBALCONSTANT.BLANKSPACESTRING;
		InputStream isResponseStream = null;
	
		try {

			URL obj = new URL(strURL+strXmlToBeSent);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			if(strURL.contains(GLOBALCONSTANT.STRENGINESTARTURL)){
				con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_GET);	
			}else{
				con.setRequestMethod(GLOBALCONSTANT.METHODTYPE_POST);
			}


			//add request header
			con.setRequestProperty(GLOBALCONSTANT.USER_AGENT,GLOBALCONSTANT.MOZILLA);

			int responseCode = con.getResponseCode();

			isResponseStream = con.getInputStream();
			DataGet = readIt(isResponseStream, GLOBALCONSTANT.INT_NUMBER_ZERO);



		} catch (Exception e) {

		
			if(GLOBALCONSTANT.ISDEBUG)
//				Log.e(GLOBALCONSTANT.HTTP, GLOBALCONSTANT.ERROR_IN_HTTP + e.toString());
			return GLOBALCONSTANT.Error;

		}
		
		return DataGet;
	}

	public static String readIt(InputStream stream, int len)
			throws IOException, UnsupportedEncodingException {
		Reader reader = null;
		reader = new InputStreamReader(stream, GLOBALCONSTANT.UTFFORMATE);

		int ch;
		StringBuffer strContent = new StringBuffer(GLOBALCONSTANT.BLANKSPACESTRING);

		while ((ch = reader.read()) != GLOBALCONSTANT.EMPTY)
			strContent.append((char) ch);

		String Lines = strContent.toString();

		return Lines;
	}
	//for post Request

	// HTTP POST request
	public String sendPost(String strURL,String strJSON) throws Exception {
	HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(strURL);

		// add header
		post.setHeader(GLOBALCONSTANT.USER_AGENT, GLOBALCONSTANT.MOZILLA);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair(GLOBALCONSTANT.STRJSON, strJSON));


		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);


		BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = GLOBALCONSTANT.Blank;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		//System.out.println(result.toString());
		return result.toString();

	}

	// HTTP POST request
		public String sendPostCustomExecution(String strURL,String strJSON) throws Exception {
		HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(strURL);

			// add header
			post.setHeader(GLOBALCONSTANT.USER_AGENT, GLOBALCONSTANT.MOZILLA);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair(GLOBALCONSTANT.STRJSON, strJSON));
			urlParameters.add(new BasicNameValuePair(GLOBALCONSTANT.STRJARPATH, GLOBALCONSTANT.JARPATHVALUE));
			


			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			HttpResponse response = client.execute(post);


			BufferedReader rd = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = GLOBALCONSTANT.Blank;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			//System.out.println(result.toString());
			return result.toString();

		}

	
}
