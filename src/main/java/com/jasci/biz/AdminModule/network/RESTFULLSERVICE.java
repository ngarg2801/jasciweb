
/*
 * * File purpose: define RestFull Service method 
File CreatedBy: Pradeep Kumar
Company Developed: Next Gen Invent Corporation
Created On:21 jul 2015

 *
 */
package com.jasci.biz.AdminModule.network;

import java.util.HashMap;

import com.jasci.common.constant.GLOBALCONSTANT;

public interface RESTFULLSERVICE {
	
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	public String URLCaller(String url , HashMap hmInputQueryParam);
	

}
