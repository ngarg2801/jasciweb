/**

Date Developed :Dec 15 2014
Created by: Sarvendra Tyagi
Description :MENUOPTIONS Controller TO HANDLE THE REQUESTS FROM SCREENS
 */


package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.jasci.biz.AdminModule.be.MENUOPTIONLABELBE;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE;
import com.jasci.biz.AdminModule.service.IMENUOPTIONSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class MENUOPTIONSCONTROLLER {


	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;

	@Autowired
	SCREENACCESSSERVICE screenAccessService;

	@Autowired
	IMENUOPTIONSERVICE objMenuOptionService;
	
	@Autowired
	IMENUAPPICONSERVICE objImenuappiconservice;

	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	private  String strApplication = GLOBALCONSTANT.BlankString;
	private  String strMenuOption  = GLOBALCONSTANT.BlankString;
	private  String strMenuType = GLOBALCONSTANT.BlankString;
	private  String partOfdesc20 = GLOBALCONSTANT.BlankString;
	private  String strTenant = GLOBALCONSTANT.BlankString;
	private  Boolean lookUp;
	static org.slf4j.Logger log = LoggerFactory.getLogger(MENUOPTIONSCONTROLLER.class);

/*	private List<GENERALCODES> objListMenutype=null; */

	/**
	 *Description: This function redirect to MenuOption LookUp screen page  
	 *Input parameter none
	 *Return Type: ModelAndView
	 *Created By: Sarvendra Tyagi
	 *Created Date:Dec 15 2014 
	 */
	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceLookUp, method = RequestMethod.GET)
	public ModelAndView getShowMenuOptionLookUpData( @ModelAttribute(GLOBALCONSTANT.TeamMemberInvalidMsg)  Object StatusMSG){
		ModelAndView objModelAndView =null;


		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}

		MENUOPTIONLABELBE objMenuoptionlabelbe=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
		
		try{

			objModelAndView=new ModelAndView(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceLookUpScreen,
					GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceLookUp_ObjectList,
					ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_MENUTYPES,GLOBALCONSTANT.BlankString));
			objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
			objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);
			
		}catch(Exception objException){
			log.error(objException.getMessage());
		}
		return objModelAndView;
	}

	/**
	 *Description: This function redirect to MenuOption Maintenance search LookUp screen page  
	 *Input parameter none
	 *Return Type: ModelAndView
	 *Created By: Sarvendra Tyagi
	 *Created Date:Dec 15 2014 
	 * @throws JASCIEXCEPTION 
	 */

	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaitenanceSearchLookUp, method = RequestMethod.GET)
	public ModelAndView getMenuOptionsList(HttpServletRequest objHttpServletRequest){

		ModelAndView objModelAndView=null;


		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		strApplication=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_lblApplication);
		strMenuOption=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_lblMenuOption);
		strMenuType=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_lblMenuType);
		partOfdesc20=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_lblPartDescription20);
		strTenant=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_txtTenant);

		MENUOPTIONLABELBE objMenuoptionlabelbe=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			objModelAndView=new ModelAndView(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceSearchLookUpScreen);
			objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
			objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);
	
		
		}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
		return objModelAndView;
	}






	/**
	 *Description: This function redirect to MenuOption Maintenance search LookUp screen page when we have been delete 
	 *Input parameter none
	 *Return Type: ModelAndView
	 *Created By: Sarvendra Tyagi
	 *Created Date:Dec 15 2014 
	 * @throws JASCIEXCEPTION 
	 */

	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaitenanceSearchLookUpViaDelete, method = RequestMethod.GET)
	public ModelAndView getMenuOptionsList() throws JASCIEXCEPTION{

		ModelAndView objModelAndView=null;


		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}

		MENUOPTIONLABELBE objMenuoptionlabelbe=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
		
		try{


			objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Forword_SLASH
					+GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceSearchLookUpScreen);
			objModelAndView.addObject(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
			objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);

		}catch(Exception objJasciexception){

			log.error(objJasciexception.getMessage());
		}

		return objModelAndView;
	}




	/**
	 *Description: This function is showing Menu option list on MenuOption Maintenance search LookUp screen page in kendo grid
	 *Input parameter none
	 *Return Type: objMenuoptions
	 *Created By: Sarvendra Tyagi
	 *Created Date:Dec 17 2014 
	 */
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_RestFull_Kendo, method = RequestMethod.GET)
	public @ResponseBody  List<MENUOPTIONS>  getkendoList() throws JASCIEXCEPTION{	
		List<MENUOPTIONS> objMenuoptions=null;
		try{

			if(strApplication!=null && (!strApplication.equalsIgnoreCase(GLOBALCONSTANT.BlankString)) ){

				objMenuoptions=objMenuOptionService.getMenuOptionsList(strApplication, GLOBALCONSTANT.MENUOPTION_lblApplication);
			}else
			if(strMenuOption!=null && (!strMenuOption.equalsIgnoreCase(GLOBALCONSTANT.BlankString)) ){

				objMenuoptions=objMenuOptionService.getMenuOptionsList(strApplication, GLOBALCONSTANT.MENUOPTION_lblApplication);
			}else

			if( strMenuType!=null && (!strMenuType.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){

				objMenuoptions=objMenuOptionService.getMenuOptionsList(strMenuType, GLOBALCONSTANT.MENUOPTION_lblMenuType);
			}else


			if(partOfdesc20 !=null && (!partOfdesc20.equalsIgnoreCase(GLOBALCONSTANT.BlankString)) ){

				objMenuoptions=objMenuOptionService.getMenuOptionsList(partOfdesc20, GLOBALCONSTANT.MENUOPTION_lblPartDescription20);
			}else
			
			if(strTenant !=null && (!strTenant.equalsIgnoreCase(GLOBALCONSTANT.BlankString))  ){
				objMenuoptions=objMenuOptionService.getMenuOptionsList(strTenant, GLOBALCONSTANT.MENUOPTION_txtTenant);
			}else{
				
				objMenuoptions=objMenuOptionService.getMenuOptionDisplayAll();
			}
			
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}

		return objMenuoptions;
	}



	/** 
	 * @Description  :This method is used for delete records
	 * @param		 : objHttpServletRequest
	 * @return		 :objModelAndView
	 * @throws JASCIEXCEPTION 
	 * @Developed by :sarendra tyagi
	 * @Date         : Dec 18 2014
	 */
	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_delete_controller_action, method = RequestMethod.POST)
	public ModelAndView getMenuOptionsDeletion(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION{

		ModelAndView objModelAndView=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		String strMenuOption=objHttpServletRequest.getParameter(GLOBALCONSTANT.DataBase_MenuOptions_MenuOption);
		Boolean status;
		
		MENUOPTIONLABELBE objMenuoptionlabelbe=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}

		try{
			status=objMenuOptionService.deleteMenuOption(strMenuOption);

			if(status){

				objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Forword_SLASH
						+GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaitenanceSearchLookUp);
			}


		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			objModelAndView=new ModelAndView(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceSearchLookUpScreen);
			objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.ErrorMessage, objJasciexception.getMessage());
			objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);
			return objModelAndView;
		}

		return objModelAndView;
	}






	/** 
	 * @Description  :This method is used for navigate to edit screen
	 * @param		 : objHttpServletRequest
	 * @return		 :objModelAndView
	 * @Developed by :sarvendra tyagi
	 * @Date         : Dec 19 2014
	 */
	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreen_Action, method = RequestMethod.GET)
	public ModelAndView getMenuOptionsData(HttpServletRequest objHttpServletRequest)throws JASCIEXCEPTION{

		ModelAndView objModelAndView=null;
		
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		
		
		List<MENUOPTIONS> objMenuoptionsList=null;
		String strMenuOption=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_lblMenuOption);
		String tenant=objHttpServletRequest.getParameter(GLOBALCONSTANT.Parameter_MenuOption_Tenant);
		MENUOPTIONLABELBE objMenuoptionlabelbe=null;
		MENUOPTIONS objMenuoptions=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
		

		try{
		if(strMenuOption!=null && !(strMenuOption.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){
			objMenuoptionsList=objMenuOptionService.getMenuOptionData(strMenuOption);
			
			try{
				objMenuoptions=objMenuoptionsList.get(GLOBALCONSTANT.INT_ZERO);
				String teamMember=objImenuappiconservice.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member());
				
				objMenuoptions.setLastActivityTeamMember(teamMember);
				objMenuoptions.setDescription20(StringEscapeUtils.escapeHtml(objMenuoptions.getDescription20()));
				objMenuoptions.setDescription50(StringEscapeUtils.escapeHtml(objMenuoptions.getDescription50()));
				objMenuoptions.setMenuOption(StringEscapeUtils.escapeHtml(objMenuoptions.getMenuOption()));
				objMenuoptions.setExecution(StringEscapeUtils.escapeHtml(objMenuoptions.getExecution()));
			}catch(Exception objException){
				log.error(objException.getMessage());
			}


			if(tenant==null){

				lookUp=true;

			}else{

				lookUp=false;

			}

			
			objModelAndView=new ModelAndView(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreen,
					GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_ListPojoObject,objMenuoptions);
			try{
			objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_MenuType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_MENUTYPES,GLOBALCONSTANT.BlankString)
);
			}catch(Exception objException){
				log.error(objException.getMessage());
			}
			objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_SearchLookUp,lookUp);
			objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_MapApplication,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
			objModelAndView.addObject(GLOBALCONSTANT.MENU_OPTION_MapSubApplication,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONSUBS,GLOBALCONSTANT.BlankString));
			objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);

			}else{
				
				String TeamMemberName=GLOBALCONSTANT.BlankString;
				String StrCurrentDate=GLOBALCONSTANT.BlankString;
				
				try{
					
					Date ObjDate1 = new Date();
		            DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Insert_DateFormat_DD_MM_YY);
		            StrCurrentDate=ObjDateFormat1.format(ObjDate1);
					
				}catch(Exception objException){
					log.error(objException.getMessage());	
				}
				try{
					
					TeamMemberName=objImenuappiconservice.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member());
					
				}catch(Exception objException){
					log.error(objException.getMessage());	
				}
				//String strScreenName=GLOBALCONSTANT.BlankString;
				objModelAndView=new ModelAndView(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreenNew);
				/*objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_MenuType,objListMenutype);*/
				try{
					objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_MenuType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_MENUTYPES,GLOBALCONSTANT.BlankString)
							);
					}catch(Exception objException){
						log.error(objException.getMessage());
					}
				objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);
				if(strMenuOption==null){
					objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_SearchLookUp,
							GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_SearchScreen);
				}else{
					
					objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_SearchLookUp,
							GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_LookUpScreen);
				}
				
				/*objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_MapApplication,objApplicationList);
				objModelAndView.addObject(GLOBALCONSTANT.MENU_OPTION_MapSubApplication,objSubApplication);
				*/
				objModelAndView.addObject(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_MapApplication,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
				objModelAndView.addObject(GLOBALCONSTANT.MENU_OPTION_MapSubApplication,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONSUBS,GLOBALCONSTANT.BlankString));
				 
				objModelAndView.addObject(GLOBALCONSTANT.MENU_OPTION_TEAMMEMBER, TeamMemberName);
					objModelAndView.addObject(GLOBALCONSTANT.MENU_OPTION_CURRENTDATE, StrCurrentDate);
			}



		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return objModelAndView;
	}




	/**
	 *Description: This function  redirect to Menu Option maintenance new screen page for update MenuOption  
	 *Input parameter: none
	 *Return Type :"ModelAndView"
	 *Created By :"Sarvendra Tyagi"
	 *Created Date: "Dec 20 2014" 
	 */
	///Team_member_maintenance_update/{teamMember}/{SortValue}"
	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MaintenanceUpdate_Action, method = RequestMethod.POST)
	public ModelAndView updateEntry(@ModelAttribute(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_Maintenance_ListPojoObject)  MENUOPTIONS objectMenuoptions,HttpServletRequest request,HttpServletResponse response){

		
		ModelAndView objModelAndView=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		String strDate=request.getParameter(GLOBALCONSTANT.LastActivityDateH);
		
		MENUOPTIONLABELBE objMenuoptionlabelbe=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
		
		
		try {
			
			DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Insert_DateFormat_DD_MMM_YY);
            
            Date currentDate=ObjDateFormat1.parse(strDate);
            objectMenuoptions.setLastActivityDate(currentDate);
			
		} catch (Exception objException) {
			log.error(objException.getMessage());
		}
		
		try{

			objMenuOptionService.update(objectMenuoptions);

			
				objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaitenanceSearchLookUp);


		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			
			
		}

		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);
		return objModelAndView;
	}



	/**
	 *Description: This function  redirect to Menu Option maintenance search lookup screen after insert or updat  
	 *Input parameter: none
	 *Return Type :"ModelAndView"
	 *Created By :"Sarvendra Tyagi"
	 *Created Date: "Dec 20 2014" 
	 * @throws JASCIEXCEPTION 
	 */
	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MaintenanceUpdate_AfterSearchAction, method = RequestMethod.GET)
	public ModelAndView showMenuMaintenanceSearch() throws JASCIEXCEPTION{
		
		ModelAndView objModelAndView=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		MENUOPTIONLABELBE objMenuoptionlabelbe=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
		
		objModelAndView=new ModelAndView(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceSearchLookUpScreen);
		objModelAndView.addObject(GLOBALCONSTANT.DataBase_SecurityAuthorizations_Tenant,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);
		return objModelAndView;
	}

	
	/** 
	 * @Description  :This method is used for go directly to menu maintenance screen
	 * @param		 : none
	 * @return		 :objModelAndView
	 * @throws JASCIEXCEPTION 
	 * @Developed by :sarvendra tyagi
	 * @Date         : Dec 20 2014
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreenNew_Action, method = RequestMethod.GET)
	public ModelAndView MenuMaintenanceNew() throws JASCIEXCEPTION{
		
		ModelAndView objModelAndView=null;

		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Option_Maintenmence_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			
			return objModelAndView;

		}
		MENUOPTIONLABELBE objMenuoptionlabelbe=null;

		try{
			objMenuoptionlabelbe=objMenuOptionService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
		
		
		
		String TeamMemberName=GLOBALCONSTANT.BlankString;
		String StrCurrentDate=GLOBALCONSTANT.BlankString;
		
		try{
			
			Date ObjDate1 = new Date();
            DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Insert_DateFormat_DD_MM_YY);
            StrCurrentDate=ObjDateFormat1.format(ObjDate1);
			
		}catch(Exception objException){
			log.error(objException.getMessage());	
		}
		try{
			
			TeamMemberName=objImenuappiconservice.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member());
			
		}catch(Exception objException){
			log.error(objException.getMessage());	
		}
		
		 objModelAndView=new ModelAndView(GLOBALCONSTANT.REQUESTMAPPING_MENUOPTION_MENUMaintenanceScreenNew);
		 objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		 objModelAndView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuoptionlabelbe);
		 objModelAndView.addObject(GLOBALCONSTANT.MENU_OPTION_TEAMMEMBER, TeamMemberName);
			objModelAndView.addObject(GLOBALCONSTANT.MENU_OPTION_CURRENTDATE, StrCurrentDate);
		return objModelAndView;
	}

	
	
	
	
	
}


