/**
 
Date Developed :Dec 23 2014
Created by: Rakesh Pal
Description :NOTES Controller TO HANDLE THE REQUESTS FROM SCREENS
 */

package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.NOTESFIELDBE;
import com.jasci.biz.AdminModule.be.NOTESLABELBE;
import com.jasci.biz.AdminModule.service.INOTESSERVICES;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class NOTESCONTROLLER {
	
	@Autowired
	INOTESSERVICES ObjNotesService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	
	
	ModelAndView ObjModelAndView=new ModelAndView();
	private static final Logger logger = LoggerFactory.getLogger(NOTESCONTROLLER.class);
	/**
	 *Description This function design to redirect on Notes Screen
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By " Rakesh Pal"
	 *Created Date "DEC 23 2014" 
	 * @throws JASCIEXCEPTION 
	 **/
	

	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_NotesScreen_Action, method = RequestMethod.GET)
	public ModelAndView getLoginPage(HttpServletRequest request) throws JASCIEXCEPTION
	{
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		HttpSession session = request.getSession();
		String StrNote_id=request.getParameter(GLOBALCONSTANT.Notes_id);//.replaceAll("&gt;", ">").replaceAll("&lt;", "<");
		String StrNote_link=request.getParameter(GLOBALCONSTANT.Notes_link);//.replaceAll("&lt;", "<");
	
		String StrCompanyID=OBJCOMMONSESSIONBE.getCompany();
		String StrTenantID=OBJCOMMONSESSIONBE.getTenant();
	
	
		if(StrNote_id == null || StrNote_link==null)
		{
		session = request.getSession();
		StrNote_id=session.getAttribute(GLOBALCONSTANT.Notes_id).toString();
		StrNote_link=session.getAttribute(GLOBALCONSTANT.Notes_link).toString();
		}
		else{
			session.setAttribute(GLOBALCONSTANT.Notes_id, StrNote_id);
		session.setAttribute(GLOBALCONSTANT.Notes_link, StrNote_link);
		}
		List<NOTESFIELDBE> objNotesFieldbe=new ArrayList<NOTESFIELDBE>();
		
		objNotesFieldbe=ObjNotesService.getNotesList(StrNote_id,StrNote_link,StrTenantID,StrCompanyID);
		
		NOTESLABELBE objNoteslabelbe= ObjNotesService.getNotesLabels(LANGUANGELABELSAPI.StrNotesModule,OBJCOMMONSESSIONBE.getCurrentLanguage());
		if(objNotesFieldbe.size()<GLOBALCONSTANT.RecordCounter)
		{	

			ModelAndView objModelandView =new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Notes_Add_Request_Action);	
			objModelandView.addObject(GLOBALCONSTANT.NOTE_viewLabels,objNoteslabelbe);
			return objModelandView;
		}
		else
		{
//		List<NOTES> objNotesServices=null;
		ModelAndView objModelandView =new ModelAndView(GLOBALCONSTANT.RequestMapping_NotesScreen_Page);
		objModelandView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		objModelandView.addObject(GLOBALCONSTANT.NOTE_viewLabels,objNoteslabelbe);
		return objModelandView;
		}
		
		
		}

	/**
	 *Description This function redirect to NotesLookUp screen after addOrUpdate a record  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rakesh Pal"
	 *Created Date "Dec 25 2014" 
	 * @throws JASCIEXCEPTION,ParseException 
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Notes_saveOrupdate_Request_Action, method = RequestMethod.GET)
	public ModelAndView addOrUpdateNotesEntry(@ModelAttribute(GLOBALCONSTANT.Notes_saveOrupdate_ModelAttribute) NOTESFIELDBE objNotesFiesdbe,HttpServletRequest request) throws JASCIEXCEPTION, ParseException{
		
		
		/**Here we Check user is already logged in or not*/
   		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	
		String NoteIDValue = request.getParameter(GLOBALCONSTANT.NOTE_ID_Value);
		String NoteLinkValue = request.getParameter(GLOBALCONSTANT.NOTE_LINK_Value);
		String LastActivityDateValue = request.getParameter(GLOBALCONSTANT.LastActivitydate_Value);
		String LastActivityByValue = request.getParameter(GLOBALCONSTANT.LastActivityBy_Value);
		String ID = request.getParameter(GLOBALCONSTANT.ID_Value);
		
		
		String StrTenant_id=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany_id=OBJCOMMONSESSIONBE.getCompany();

		try{
			objNotesFiesdbe.setTenant_Id(StrTenant_id);
			objNotesFiesdbe.setCompany_Id(StrCompany_id);
			objNotesFiesdbe.setNote_Id(NoteIDValue);
			objNotesFiesdbe.setNote_Link(NoteLinkValue);
			objNotesFiesdbe.setLast_Activity_Date(LastActivityDateValue);
			objNotesFiesdbe.setLast_Activity_Team_Member(LastActivityByValue);
			objNotesFiesdbe.setAUTO_ID(ID);
			
			
			
			
			//objNotesFiesdbe.setLast_Activity_Date(Formatted_date);
			
			ObjNotesService.addOrUpdateNotesEntry(objNotesFiesdbe);

		}catch(JASCIEXCEPTION objJasciexception){
			logger.error(objJasciexception.getMessage());
		}
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_NotesScreen_Action);
		ObjModelAndView.addObject(GLOBALCONSTANT.Notes_id,objNotesFiesdbe.getNote_Id());
		ObjModelAndView.addObject(GLOBALCONSTANT.Notes_link,objNotesFiesdbe.getNote_Link());
		return ObjModelAndView;
		
	}


	
	/**
	 *Description This function redirect to NotesMaintenance screen when we request to add a record  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rakesh Pal"
	 *Created Date "Dec 25 2014" 
	 * @throws ParseException 
	 */
	
	
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Notes_Add_Request_Action, method = RequestMethod.GET)
	public ModelAndView addNote(@ModelAttribute(GLOBALCONSTANT.Add_Notes_ModelAttribute) 
	NOTESFIELDBE objnotesFieldbe,HttpServletRequest request) throws JASCIEXCEPTION{
		
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	
		NOTESLABELBE objNoteslabelbe= ObjNotesService.getNotesLabels(LANGUANGELABELSAPI.StrNotesModule,OBJCOMMONSESSIONBE.getCurrentLanguage());
		HttpSession session = request.getSession();
		
		String StrNote_id=session.getAttribute(GLOBALCONSTANT.Notes_id).toString();
		String StrNote_link=session.getAttribute(GLOBALCONSTANT.Notes_link).toString();;
		Date date=new Date();
		String Formatted_date=ConvertDate(date, GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
		objnotesFieldbe.setTenant_Id(OBJCOMMONSESSIONBE.getTenant());
		objnotesFieldbe.setCompany_Id(OBJCOMMONSESSIONBE.getCompany());
		objnotesFieldbe.setNote_Id(StrNote_id);
		objnotesFieldbe.setNote_Link(StringEscapeUtils.escapeHtml(StrNote_link));
		objnotesFieldbe.setLast_Activity_Date(Formatted_date);
		objnotesFieldbe.setLast_Activity_Team_Member(OBJCOMMONSESSIONBE.getTeam_Member_Name());
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Notes_AddNewNote_Request_page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.NOTE_objnoteFields,objnotesFieldbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.NOTE_viewLabels,objNoteslabelbe);
		return  ObjModelAndView;
	}
	
	

	/** Description This function read data using Kendo ui in NotesLookup
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rakesh Pal"
	 *Created Date "Dec 23 2014" 
	 * @throws JASCIEXCEPTION 
	 */

	//read data using Kendo ui GeneralCode
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Notes_Read_Kendo_UI, method = RequestMethod.GET)
	public @ResponseBody  List<NOTESFIELDBE>  getNotesList(HttpServletRequest request) throws JASCIEXCEPTION {	
		
		HttpSession session = request.getSession();
		
		String StrNote_id=session.getAttribute(GLOBALCONSTANT.Notes_id).toString();
		String StrNote_link=session.getAttribute(GLOBALCONSTANT.Notes_link).toString().replace(GLOBALCONSTANT.StringDoubleQuote,GLOBALCONSTANT.DoubleQuote);
		String StrCompanyID=OBJCOMMONSESSIONBE.getCompany();
		String StrTenantID=OBJCOMMONSESSIONBE.getTenant();
		List<NOTESFIELDBE> ObjListNotesField=ObjNotesService.getNotesList(StrNote_id,StrNote_link,StrTenantID,StrCompanyID);
		return ObjListNotesField;
	}
	

	
	
	
	/**
	 *Description This function redirect to NotesLookup screen page after delete a Notes record
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rakesh Pal"
	 *Created Date "Dec 31 2014" 
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Notes_Delete_Screen_Action, method = RequestMethod.GET)
	public ModelAndView deleteNote(HttpServletRequest request)throws JASCIEXCEPTION{
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	HttpSession session = request.getSession();
		String StrNote_id=session.getAttribute(GLOBALCONSTANT.Notes_id).toString();
		String StrNote_link=session.getAttribute(GLOBALCONSTANT.Notes_link).toString().replace(GLOBALCONSTANT.StringDoubleQuote,GLOBALCONSTANT.DoubleQuote);
		/*String Last_Activity_date=request.getParameter(GLOBALCONSTANT.DataBase_Notes_Last_Acitivity_Date);
		String Last_Activity_Team_Member=request.getParameter(GLOBALCONSTANT.DataBase_Notes_Last_Activity_Team_Member);
		*/String ID=request.getParameter(GLOBALCONSTANT.DataBase_Notes_ID);
		
		String Tenant_Id=OBJCOMMONSESSIONBE.getTenant();
		String Company_id=OBJCOMMONSESSIONBE.getCompany();
		/*String KendoUrl=request.getParameter(GLOBALCONSTANT.Notes_Lookup_KendoUrl);
		NOTESFIELDBE objnotesfieldbe=null;*/

	
		try{
			
			ObjNotesService.deleteNote(Tenant_Id,Company_id,StrNote_id,StrNote_link,ID);
	
		}catch(JASCIEXCEPTION objJasciexception){
			logger.error(objJasciexception.getMessage());
		}
		
		return  new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_NotesScreen_Action);

	}

	/**
	 *Description This method is used to Notes_Maintenance screen to update/edit the note for a Team_Member.  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rakesh Pal"
	 *Created Date "Dec 24 2014" 
	 */
	

	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Notes_Update_Request_Action, method = RequestMethod.GET)
	public ModelAndView getnote(@ModelAttribute(GLOBALCONSTANT.Add_Notes_ModelAttribute) NOTESFIELDBE objnotesFieldbe,HttpServletRequest request){
		
		List<NOTESFIELDBE> ObjListNotesField=null;
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		
		NOTESLABELBE objNoteslabelbe= ObjNotesService.getNotesLabels(LANGUANGELABELSAPI.StrNotesModule,OBJCOMMONSESSIONBE.getCurrentLanguage());

		HttpSession session = request.getSession();
		try{
		String StrNote_id=session.getAttribute(GLOBALCONSTANT.Notes_id).toString();
		String StrNote_link=session.getAttribute(GLOBALCONSTANT.Notes_link).toString();//.replace(GLOBALCONSTANT.StringDoubleQuote,GLOBALCONSTANT.DoubleQuote);
		//String TeamMember=OBJCOMMONSESSIONBE.getTeam_Member_Name();
		String Tenant_id =OBJCOMMONSESSIONBE.getTenant();
		String Company_id =OBJCOMMONSESSIONBE.getCompany();
		
		int ID = Integer.parseInt(request.getParameter(GLOBALCONSTANT.DataBase_Notes_ID));
		//String LastActivityDate=request.getParameter(GLOBALCONSTANT.DataBase_Notes_Last_Acitivity_Date);
		try
		{
			ObjListNotesField=ObjNotesService.getnote(StrNote_link,StrNote_id,Tenant_id,Company_id,ID);
				
		}
		catch(JASCIEXCEPTION objJasciexception){
			logger.error(objJasciexception.getMessage());
		}

		NOTESFIELDBE ObjListNotesfieldbe=ObjListNotesField.get(0);

		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Notes_Update_Request_page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.NOTE_objnoteFields,ObjListNotesfieldbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.NOTE_viewLabels,objNoteslabelbe);
		return  ObjModelAndView;
		}catch(Exception objException){
			
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	}
	
	
	
	/**
	 * 
	 * @author Rakesh Pal
	 * @Date Dec 30, 2014 
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return
	 * String
	 */
	public String ConvertDate(Date dateInString,String StrInFormat,String StrOutFormat)
	 {
	  
	  DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
	  String StrFormattedString=GLOBALCONSTANT.BlankString ;
	   
	 
	  try {
	  
	   Date date = dateInString;
	   StrFormattedString=dateFormat.format(date);
	 	  
	  } catch (Exception e) {
	 
	  }
	  
	  return StrFormattedString;
	  
	 }
	}
	
	
	
