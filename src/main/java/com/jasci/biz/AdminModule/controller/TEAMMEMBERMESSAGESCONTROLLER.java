/**

Date Developed  NOV 15 2014
Created By "Diksha Gupta"
Description   TEAMMEMBERMESSAAGES controller to handle requests from screens.
 */

package com.jasci.biz.AdminModule.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESBE;
import com.jasci.biz.AdminModule.be.TEAMMEMBERMESSAGESSCREENBE;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ITEAMMEMBERMESSAGESSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class TEAMMEMBERMESSAGESCONTROLLER 
{
	
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	
	//private String StrTeamMemberFirstLastName = GLOBALCONSTANT.BlankString;
	private  String StrTeamMemberName = GLOBALCONSTANT.BlankString;
	private  String StrPartTeamMember = GLOBALCONSTANT.BlankString;
	private  String StrDepartment = GLOBALCONSTANT.BlankString;
	private  String StrShift = GLOBALCONSTANT.BlankString;
	private String StrTeammember=GLOBALCONSTANT.BlankString;
	private String StrTeammemberId=GLOBALCONSTANT.BlankString;
	private String StrCheckMsg=GLOBALCONSTANT.BlankString;
	private String StrSearchErrorMessage=GLOBALCONSTANT.BlankString;
	//private String StrUpdateMessage=GLOBALCONSTANT.BlankString;
	private String TypeEdit=GLOBALCONSTANT.BlankString;
	private String MessageNumberEdit=GLOBALCONSTANT.BlankString;
	private String TeamMemberEdit=GLOBALCONSTANT.BlankString;
	
	private String StrTeamMemberList[]=null; 
	private TEAMMEMBERMESSAGESBE ObjTeammeberMessagesEdit=null;
	
	/** It is used to logger the exception*/
	private static Logger log = LoggerFactory.getLogger(TEAMMEMBERMESSAGESCONTROLLER.class.getName());
	
	TEAMMEMBERMESSAGESSCREENBE ObjLabels=null;
	
	
	@Autowired
	 ITEAMMEMBERMESSAGESSERVICE ObjectTeamMemberMessagesService;
	ModelAndView ObjModelAndView=new ModelAndView();
	
	
	/**
	 *Description This function design to redirect on TeamMemberMessagesMaintenance page.
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Diksha Gupta"
	 *Created Date "Nov 15 2014" 
	 * */
	
	 @RequestMapping(value=GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceScreen_Action, method = RequestMethod.GET)
		public ModelAndView GetTeamMemberMessagesMaintenancePage()
		{
		 
			
			String StrUserid=OBJCOMMONSESSIONBE.getUserID();
			String StrPassword=OBJCOMMONSESSIONBE.getPassword();
			
			
			/** Here we Check user is logged in or not*/
			
			if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
			}
		 
			/** Here we Check user have this screen access or not*/

			WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
					GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

			if (!objWEBSERVICESTATUS.isBoolStatus()) {
				ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
				return ObjModelAndView;

			}
		 
		 
		 ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_MaintenancePage);
		 ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		 /** To get screen labels */
		 
		 try {
			ObjLabels=ObjectTeamMemberMessagesService.getScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
			} catch (JASCIEXCEPTION ObjectJasciException) {
				log.error(ObjectJasciException.getMessage());
		}
		 ObjModelAndView.addObject(GLOBALCONSTANT.ViewLabels,ObjLabels);
		 
		 
		 
		 if(!StrSearchErrorMessage.equals(GLOBALCONSTANT.BlankString) && StrSearchErrorMessage.equals(GLOBALCONSTANT.InvalidTeammemberMessage))
		 {
			 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSearchErrorMessage,ObjLabels.getTeamMemberMessages_InvalidTM()); 
		 }
		 else  if(!StrSearchErrorMessage.equals(GLOBALCONSTANT.BlankString) && StrSearchErrorMessage.equals(GLOBALCONSTANT.InvalidTeamMemberNameMessage))
		 {
			 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSearchErrorMessage,ObjLabels.getTeamMemberMessages_InvalidPartOfTm()); 
		 }
		 else  if(!StrSearchErrorMessage.equals(GLOBALCONSTANT.BlankString) && StrSearchErrorMessage.equals(GLOBALCONSTANT.InvalidDepartmentMessage))
		 {
			 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSearchErrorMessage,ObjLabels.getTeamMemberMessages_InvalidDepartment()); 
		 }
		 else  if(!StrSearchErrorMessage.equals(GLOBALCONSTANT.BlankString) && StrSearchErrorMessage.equals(GLOBALCONSTANT.InvalidShiftMessage))
		 {
			 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSearchErrorMessage,ObjLabels.getTeamMemberMessages_InvalidShift()); 
		 }
		 else 
		 {
			 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSearchErrorMessage,GLOBALCONSTANT.BlankString);
		 }
		 
		 StrSearchErrorMessage=GLOBALCONSTANT.BlankString;
		 try {
		 ObjModelAndView.addObject(GLOBALCONSTANT.DepartmentList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_DEPARTMENT,GLOBALCONSTANT.BlankString));
		 ObjModelAndView.addObject(GLOBALCONSTANT.ShiftCodeList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SHIFTCODE,GLOBALCONSTANT.BlankString));
		 } catch (JASCIEXCEPTION ObjectJasciException) {
			 log.error(ObjectJasciException.getMessage());
			}
		 return ObjModelAndView;
		}
	 
	 /**
		 *Description This function design to redirect on TeamMemberMessagesMaintenanceNew page.
		 *Input parameter none
		 *Return Type "ModelAndView"
		 *Created By "Diksha Gupta"
		 *Created Date "Nov 16 2014" 
		 * */
		
		 @RequestMapping(value=GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action, method = RequestMethod.GET)
			public ModelAndView GetTeamMemberMessagesMaintenanceNewPage()
			{
			 

				
				String StrUserid=OBJCOMMONSESSIONBE.getUserID();
				String StrPassword=OBJCOMMONSESSIONBE.getPassword();
				
				
				/** Here we Check user is logged in or not*/
				
				if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
			 
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
			 
			 ObjModelAndView =new ModelAndView(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_MaintenanceNewPage);
			 ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);	
			 /** To get screen labels */

			 try {
				ObjLabels=ObjectTeamMemberMessagesService.getScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
			}catch (JASCIEXCEPTION ObjectJasciException) {
				 log.error(ObjectJasciException.getMessage());
				}
			 ObjModelAndView.addObject(GLOBALCONSTANT.ViewLabels,ObjLabels);
			 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMember,StrTeammemberId);
			 if(!StrCheckMsg.equals(GLOBALCONSTANT.BlankString))
			 {
				 ObjModelAndView.addObject(GLOBALCONSTANT.SavedMessageToPage,StrCheckMsg); 
			 }
			 
			 StrCheckMsg=GLOBALCONSTANT.BlankString;
				 
			  return ObjModelAndView;
			}
	 
	 
		 
		 
		    /**
			 *Description This function design to redirect on TeamMemberMessagesMaintenanceLookUp page .
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 16 2014" 
			 * */
			
			 @RequestMapping(value=GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceLookup_Action, method = RequestMethod.GET)
				public ModelAndView GetTeamMemberMessagesMaintenanceLookupPage(HttpServletRequest request)
				{
				 

					String StrUserid=OBJCOMMONSESSIONBE.getUserID();
					String StrPassword=OBJCOMMONSESSIONBE.getPassword();
					
					
					/** Here we Check user is logged in or not*/
					
					if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

						return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
					}
				 
					/** Here we Check user have this screen access or not*/

					WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
							GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

					if (!objWEBSERVICESTATUS.isBoolStatus()) {
						ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
						ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
						ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
						return ObjModelAndView;

					}
				 
				 
				  StrTeamMemberName=request.getParameter(GLOBALCONSTANT.Param_TeamMemberMessage_TeamMember);
				  StrPartTeamMember=request.getParameter(GLOBALCONSTANT.Param_TeamMemberMessage_PartTeamMember);
				  StrDepartment=request.getParameter(GLOBALCONSTANT.Param_TeamMemberMessage_Department);
				  StrShift=request.getParameter(GLOBALCONSTANT.Param_TeamMemberMessage_Shift);
				  if(StrTeamMemberName==null)
					{
					  StrTeamMemberName=GLOBALCONSTANT.BlankString;
					}
					if(StrPartTeamMember==null)
					{
						StrPartTeamMember=GLOBALCONSTANT.BlankString;
					}
					if(StrDepartment==null)
					{
						StrDepartment=GLOBALCONSTANT.BlankString;
					}
					if(StrShift==null)
					{
						StrShift=GLOBALCONSTANT.BlankString;
					}
				  
				 String StrTenant=OBJCOMMONSESSIONBE.getTenant();
				 String StrCompany=OBJCOMMONSESSIONBE.getCompany();
				  	 
				 /** Call getTeamMemberList() to get list of teamMembers based on search.*/
				 
				  List<TEAMMEMBERSBE> ObjListTeamMembers=null;
				try {
					ObjListTeamMembers = ObjectTeamMemberMessagesService.getTeamMemberList(StrTeamMemberName,StrPartTeamMember,StrDepartment,StrShift,StrTenant,StrCompany);
				} catch (JASCIEXCEPTION ObjectJasciException) {
					 log.error(ObjectJasciException.getMessage());
				}
				 ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceScreen_Action);
				 if(ObjListTeamMembers.isEmpty() || ObjListTeamMembers==null)
				 {
					 if(!StrTeamMemberName.equals(GLOBALCONSTANT.BlankString))
					 {
						 StrSearchErrorMessage=GLOBALCONSTANT.InvalidTeammemberMessage;
						 }
					 else  if(!StrPartTeamMember.equals(GLOBALCONSTANT.BlankString))
					 {
						 StrSearchErrorMessage=GLOBALCONSTANT.InvalidTeamMemberNameMessage;
						
					 }
					 else  if(!StrDepartment.equals(GLOBALCONSTANT.BlankString))
					 {
						 StrSearchErrorMessage=GLOBALCONSTANT.InvalidDepartmentMessage;
						
					 }
					 else  if(!StrShift.equals(GLOBALCONSTANT.BlankString))
					 {
						 StrSearchErrorMessage=GLOBALCONSTANT.InvalidShiftMessage;
						
					 }
					 else {
						 StrSearchErrorMessage=GLOBALCONSTANT.BlankString;
					 }
					
				     return ObjModelAndView;
				 }
					
				 else
				 {
					if(!StrTeamMemberName.equals(GLOBALCONSTANT.BlankString))
					{
						StrTeammember=StrTeamMemberName;
						List<TEAMMEMBERMESSAGES> ObjListTeamMemberMessages=null;
						try {
							ObjListTeamMemberMessages = ObjectTeamMemberMessagesService.getMessages(StrTeamMemberName);
						} catch (JASCIEXCEPTION ObjectJasciException) {
							 log.error(ObjectJasciException.getMessage());
						}
						 if(!ObjListTeamMemberMessages.isEmpty() && ObjListTeamMemberMessages!=null)
						 {
						  ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_ActiveMessagesAction);
						 }
					 
					 else 
					 {
						
						 ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action);
						 StrTeammemberId=StrTeamMemberName;
					 }
					}
					else
					{
					
					 ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_MaintenanceLookupPage);
					 ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					}
					 try {
						ObjLabels=ObjectTeamMemberMessagesService.getScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
					} catch (JASCIEXCEPTION ObjectJasciException) {
						 log.error(ObjectJasciException.getMessage());
					}
					 ObjModelAndView.addObject(GLOBALCONSTANT.ViewLabels,ObjLabels);
					 ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMember,StrPartTeamMember);
				     return ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberList, ObjListTeamMembers);
				 }

				}
		 
			 
		 
		    /**
			 *Description This function design to save messages into TeamMemberMessages.
			 *Input parameter none
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 16 2014" 
			 * */
		 @RequestMapping(value=GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewEditScreen_Action, method = RequestMethod.POST)
			public ModelAndView InsertRow(@ModelAttribute(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenance_InsertRow) TEAMMEMBERMESSAGESBE ObjTeamMemberMessagesbe, BindingResult ObjResult, HttpSession session,HttpServletRequest request) 
			{
			 
			 String StrUserid=OBJCOMMONSESSIONBE.getUserID();
				String StrPassword=OBJCOMMONSESSIONBE.getPassword();
				
				
				/** Here we Check user is logged in or not*/
				
				if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
			 
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
			
			 /**Call InsertRow() to add a row of messages. */
			 String LastActivityDate=request.getParameter(GLOBALCONSTANT.GetLastActivitydate);
			 ObjTeamMemberMessagesbe.setLastActivityDate(LastActivityDate);
			 Boolean Status=false;
			try {
				Status = ObjectTeamMemberMessagesService.InsertRow(ObjTeamMemberMessagesbe,StrTeamMemberList);
			} catch (JASCIEXCEPTION ObjectJasciException) {
				 log.error(ObjectJasciException.getMessage());
				}
			 if(Status)
			 {
				 ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action);
				 StrCheckMsg=GLOBALCONSTANT.SavedMessage;
				 StrTeamMemberList=null;
				 return ObjModelAndView;
			}
			 ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action);
			 return ObjModelAndView;
			 }
		 
		 
		 /**
			 *Description This function design to get TeamMemberMessages.
			 *Input parameter none
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 18 2014" 
			 * */
		    @RequestMapping(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesGetMessages_Action)
			public ModelAndView GetMessages(@PathVariable String TeamMember,HttpServletRequest request) 
			{
            
		    	
		    	
		    	String StrUserid=OBJCOMMONSESSIONBE.getUserID();
				String StrPassword=OBJCOMMONSESSIONBE.getPassword();
				
				
				/** Here we Check user is logged in or not*/
				
				if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
			 
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
			StrTeammember=TeamMember;
			StrTeammemberId=TeamMember;
			
			/** To get TeamMember Message of Selected TeamMember */
		    List<TEAMMEMBERMESSAGES> ObjListTeamMemberMessages=null;
			try {
				ObjListTeamMemberMessages = ObjectTeamMemberMessagesService.getMessages(TeamMember);
			} catch (JASCIEXCEPTION ObjectJasciException) {
				 log.error(ObjectJasciException.getMessage());
				}
		    if(!ObjListTeamMemberMessages.isEmpty())
		    {
		     for(Object objTeamMemberMessages:ObjListTeamMemberMessages)
		     {
		    	 Object[] valueTeamMemberMessages = (Object[]) objTeamMemberMessages;
		    	 String TicketTape=GLOBALCONSTANT.BlankString;
		    	 String Message1=GLOBALCONSTANT.BlankString;
		    	 try{TicketTape=valueTeamMemberMessages[6].toString();}catch(Exception ObjException){TicketTape=GLOBALCONSTANT.BlankString;}
		    	 try{Message1=valueTeamMemberMessages[4].toString();}catch(Exception ObjException){Message1=GLOBALCONSTANT.BlankString;}
		    	
	            if(TicketTape==null)
	            {
		    		TicketTape=GLOBALCONSTANT.BlankString;
		    	}
		    	
	            if(Message1==null)
	            {
	            	Message1=GLOBALCONSTANT.BlankString;
		    	}
		    	 
		    if(TicketTape.equals(GLOBALCONSTANT.BlankString) && Message1.equals(GLOBALCONSTANT.BlankString))
		    {
		    	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action);
		    	
		    }
		     
		    
		    else
		    {
		    	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_ActiveMessagesAction);
		    	
		    }
		     }
		    }
		    else{
		       ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action);
		    }
		    return ObjModelAndView;
			}
		 

		    
		    
		    /**
			 *Description This function design to redirect on Active TeamMember Messages page.
			 *Input parameter none
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 15 2014" 
			 * */
			
			 @RequestMapping(value=GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_ActiveMessagesAction, method = RequestMethod.GET)
				public ModelAndView GetActiveTeamMemberMessagesPage()
				{

					String StrUserid=OBJCOMMONSESSIONBE.getUserID();
					String StrPassword=OBJCOMMONSESSIONBE.getPassword();
					String StrCompany=OBJCOMMONSESSIONBE.getCompany();
					
					
					/** Here we Check user is logged in or not*/
					
					if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

						return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
					}
				 
					/** Here we Check user have this screen access or not*/

					WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
							GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

					if (!objWEBSERVICESTATUS.isBoolStatus()) {
						ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
						ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
						ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
						return ObjModelAndView;

					}
					
					ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_ActiveMessagesPage);
					ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					/** To Get Screen Labels */
					try {
						ObjLabels=ObjectTeamMemberMessagesService.getScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
					} catch (JASCIEXCEPTION ObjectJasciException) {
						 log.error(ObjectJasciException.getMessage());
					}
					
					String TeamMemberName_show=ObjectTeamMemberMessagesService.getTeamMemberName(StrTeammember);
				 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMember,TeamMemberName_show);
				 ObjModelAndView.addObject(GLOBALCONSTANT.TMMCompany,StrCompany);
			
				 
				 return ObjModelAndView.addObject(GLOBALCONSTANT.ViewLabels,ObjLabels);
				}

			 
		    /**
			 *Description This function design to delete messages from TeamMemberMessages.
			 *Input parameter none
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 16 2014" 
			 * */
		 @RequestMapping(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesDeleteMessages_Action)
			public ModelAndView DeleteMessage(HttpServletRequest request) 
			{
			 
			 String StrUserid=OBJCOMMONSESSIONBE.getUserID();
				String StrPassword=OBJCOMMONSESSIONBE.getPassword();
				
				
				/** Here we Check user is logged in or not*/
				
				if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
			 
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
			 String TeamMember=request.getParameter(GLOBALCONSTANT.TeamMember);
			 String MessageNumber=request.getParameter(GLOBALCONSTANT.MessageNumber);
			 Boolean Status=false;
			 /**  Here we call deleteMessages() to delete selected message*/
			 
			try{
				Status=ObjectTeamMemberMessagesService.DeleteMessages(OBJCOMMONSESSIONBE.getTenant(),TeamMember,OBJCOMMONSESSIONBE.getCompany(),MessageNumber);
			}
			catch (JASCIEXCEPTION ObjectJasciException) {
				 log.error(ObjectJasciException.getMessage());
				}
			StrTeammember=TeamMember;
			
			 			 
			 if(Status)
			 {
				 List<TEAMMEMBERMESSAGES> ObjListTeamMemberMessages=null;
				try {
					ObjListTeamMemberMessages = ObjectTeamMemberMessagesService.getMessages(TeamMember);
				} catch (JASCIEXCEPTION ObjectJasciException) {
					 log.error(ObjectJasciException.getMessage());
				}
				 if(!ObjListTeamMemberMessages.isEmpty() && ObjListTeamMemberMessages!=null)
				 {
				  ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_ActiveMessagesAction);
				 }
			 
			 else 
			 {
				
				 ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceNewScreen_Action);
				 StrTeammemberId=TeamMember;
			 }
			 }
			 

			 
			 return ObjModelAndView;
			}
		 
		 

		     /**
			 *Description This function design to get messages to be edited from TeamMemberMessages.
			 *Input parameter Tenant,TeamMember,Company
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 20 2014" 
			 * */
		  @RequestMapping(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesGetToEditMessages_Action)
			public ModelAndView GetDataToEditMessage(HttpServletRequest request) 
			{
			  
			  String StrUserid=OBJCOMMONSESSIONBE.getUserID();
				String StrPassword=OBJCOMMONSESSIONBE.getPassword();
				
				
				/** Here we Check user is logged in or not*/
				
				if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
			 
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
			     String TeamMember=request.getParameter(GLOBALCONSTANT.TeamMember);
				 String MessageNumber=request.getParameter(GLOBALCONSTANT.MessageNumber);
				 String Type=request.getParameter(GLOBALCONSTANT.Type);
		   	     List<TEAMMEMBERMESSAGESBE> ObjListTeamMemberMessages=null;
				try {
					ObjListTeamMemberMessages = ObjectTeamMemberMessagesService.getMessagesToEdit(OBJCOMMONSESSIONBE.getTenant(),TeamMember,OBJCOMMONSESSIONBE.getCompany(),MessageNumber);
				} catch (JASCIEXCEPTION ObjectJasciException) {
					 log.error(ObjectJasciException.getMessage());
				}
				 TEAMMEMBERMESSAGESBE ObjTeamMemberMessages=null;
				 StrTeammember=TeamMember;
			 if(ObjListTeamMemberMessages!=null & !ObjListTeamMemberMessages.isEmpty())
			 {
				 ObjTeamMemberMessages=ObjListTeamMemberMessages.get(GLOBALCONSTANT.INT_ZERO);
			 }
			    ObjTeammeberMessagesEdit=ObjTeamMemberMessages;
			    TypeEdit=Type;
			    MessageNumberEdit=MessageNumber;
			    TeamMemberEdit=TeamMember;
			    ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceEditScreen_Action);
			  		
			    return ObjModelAndView;	
			    
			}
		  
		  
		     /**
			 *Description This function design to update messages into TeamMemberMessages.
			 *Input parameter objTeamMemberMessagesBe
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 22 2014" 
			 * */
		 @RequestMapping(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesEditMessages_Action)
			public ModelAndView UpdateMessage(@ModelAttribute(GLOBALCONSTANT.TeamMemberMessagesList) TEAMMEMBERMESSAGESBE objTeamMemberMessagesBe, HttpServletRequest request  ) 
			{
			 String StrUserid=OBJCOMMONSESSIONBE.getUserID();
				String StrPassword=OBJCOMMONSESSIONBE.getPassword();
				
				
				/** Here we Check user is logged in or not*/
				
				if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
			 
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
              Boolean Status=false;
 			 String LastActivityDate=request.getParameter(GLOBALCONSTANT.GetLastActivitydate);
 			objTeamMemberMessagesBe.setLastActivityDate(LastActivityDate);
			try {
				Status = ObjectTeamMemberMessagesService.UpdateMessages(objTeamMemberMessagesBe);
			} catch (JASCIEXCEPTION ObjectJasciException) {
				 log.error(ObjectJasciException.getMessage());
				}
			 if(Status)
			 {
				 ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceEditScreen_Action);
				 StrCheckMsg=GLOBALCONSTANT.EditedMessage;
				 return ObjModelAndView;
			 }
			 else
			 {
			     return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceScreen_Action);
			 }
			}
		 
		  
		  
		  
		     /**
			 *Description This function design to redirect on TeamMemberMessagesMaintenanceEdit page.
			 *Input parameter none
			 *Return Type "ModelAndView"
			 *Created By "Diksha Gupta"
			 *Created Date "Nov 22 2014" 
			 * */
			
			 @RequestMapping(value=GLOBALCONSTANT.RequestMapping_TeamMemberMessagesMaintenanceEditScreen_Action, method = RequestMethod.GET)
				public ModelAndView GetTeamMemberMessagesMaintenanceEditPage()
				{
			 
					String StrUserid=OBJCOMMONSESSIONBE.getUserID();
					String StrPassword=OBJCOMMONSESSIONBE.getPassword();
				
					/** Here we Check user is logged in or not*/
					
					if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

						return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
					}
				 
					/** Here we Check user have this screen access or not*/

					WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
							GLOBALCONSTANT.RequestMapping_Teammember_Message_Maintenance_Execution);

					if (!objWEBSERVICESTATUS.isBoolStatus()) {
						ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
						ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
						ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
						return ObjModelAndView;

					}
				 
				  ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_TeamMemberMessagesScreen_MaintenanceEditPage);
				  ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				  try {
					ObjLabels=ObjectTeamMemberMessagesService.getScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
				} catch (JASCIEXCEPTION ObjectJasciException) {
					 log.error(ObjectJasciException.getMessage());
				}
					ObjModelAndView.addObject(GLOBALCONSTANT.ViewLabels,ObjLabels);
				    ObjModelAndView.addObject(GLOBALCONSTANT.TeamMember,StrTeammemberId);
					 if(!StrCheckMsg.equals(GLOBALCONSTANT.BlankString))
					 {
						 ObjModelAndView.addObject(GLOBALCONSTANT.UpdatedMessageToPage,StrCheckMsg); 
					 }
					 
					 StrCheckMsg=GLOBALCONSTANT.BlankString;
					 ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberMessagesList,ObjTeammeberMessagesEdit);
					    ObjModelAndView.addObject(GLOBALCONSTANT.Type,TypeEdit);
					    ObjModelAndView.addObject(GLOBALCONSTANT.MessageNumber,MessageNumberEdit);
					   ObjModelAndView.addObject(GLOBALCONSTANT.TeamMember,TeamMemberEdit);	
					 return ObjModelAndView; 
				}
		 
			 
			     /**
				 *Description This function design to show teammembers on grid.
				 *Input parameter none
				 *Return Type List<TEAMMEMBERSBE>
				 *Created By "Diksha Gupta"
				 *Created Date "Nov 15 2014" 
				 * */
		 			 			 
			 @RequestMapping(value = GLOBALCONSTANT.TeamMember_Kendo_TeamMembersRead, method = RequestMethod.GET)
				public @ResponseBody  List<TEAMMEMBERSBE>  getTeamMemberList() 
				{	
				 
				  String StrTenant=OBJCOMMONSESSIONBE.getTenant();
				 String StrCompany=OBJCOMMONSESSIONBE.getCompany();
				    List<TEAMMEMBERSBE> objTeamMembersBe=null;
					try {
						objTeamMembersBe=ObjectTeamMemberMessagesService.getTeamMemberList(StrTeamMemberName,StrPartTeamMember,StrDepartment,StrShift,StrTenant,StrCompany);
					} catch (JASCIEXCEPTION ObjectJasciException) {
						 log.error(ObjectJasciException.getMessage());
					}
					return objTeamMembersBe;

				}

			 
			     /**
				 *Description This function design to show teammembersMessages on grid.
				 *Input parameter none
				 *Return Type List<TEAMMEMBERMESSAGESBE>
				 *Created By "Diksha Gupta"
				 *Created Date "Nov 15 2014" 
				 * */
		 			 			 
			
			 @RequestMapping(value = GLOBALCONSTANT.TeamMember_Kendo_TeamMembersToMessagesRead, method = RequestMethod.GET)
				public @ResponseBody  List<TEAMMEMBERMESSAGESBE>  getTeamMemberActiveMessages() 
				{	
				     List<TEAMMEMBERMESSAGESBE> ObjListTeamMemberMessagesBe=null;
					 try {
						ObjListTeamMemberMessagesBe=ObjectTeamMemberMessagesService.getMessagesList(StrTeammember);
					} catch (JASCIEXCEPTION ObjectJasciException) {
						 log.error(ObjectJasciException.getMessage());
					}
					 return ObjListTeamMemberMessagesBe;

				}
			 
			 /**
				 *Description This function design to show teammembersMessages on grid.
				 *Input parameter String
				 *Return Type List<TEAMMEMBERMESSAGESBE>
				 *Created By "Diksha Gupta"
				 *Created Date "Nov 15 2014" 
				 * */
			 
			 @RequestMapping(value =GLOBALCONSTANT.TeamMemmberMessages_Rest_ALLTeamMembers, method = RequestMethod.POST)
			@ResponseBody
			  public Boolean GetSelectedTeamMemberList(@RequestParam(value=GLOBALCONSTANT.TeamMemberListParam) String data) 
			 {
				 
			
				
					data= data.replace(GLOBALCONSTANT.BracketLeft,GLOBALCONSTANT.BlankString);
					data= data.replace(GLOBALCONSTANT.BracketRight,GLOBALCONSTANT.BlankString);
				
					 
				 data=data.replace(GLOBALCONSTANT.DoubleQuatos, GLOBALCONSTANT.BlankString);
				 
				 String TeamMemberList[]=data.split(GLOBALCONSTANT.Coma);
				 StrTeamMemberList=TeamMemberList;
				
				 
                     return true;
			  }
			 
			
}
