/*
Description make a controller of GENERALCODES class for mapping with jsp page
Created By Aakash Bishnoi
Created Date Oct 20 2014
Modified on Nov 14 2014
 */

package com.jasci.biz.AdminModule.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;
import com.jasci.biz.AdminModule.be.GENERALCODESBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.GENERALCODESBEAN;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.IGENERALCODESSERVICE;
import com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;

@Controller
//@Scope("request")

public class GENERALCODESCONTROLLER {
	//Use for logging
	static Logger log = LoggerFactory.getLogger(GENERALCODESCONTROLLER.class);
	
	

	//It is used to redirect GeneralCode Edit Page
	String GeneralCodeFlag=GLOBALCONSTANT.ZeroValue;
	String MenuValue= GLOBALCONSTANT.BlankString;
	String StrMenuType=GLOBALCONSTANT.StrMenuTypeFullScreen,StrMenuOption=GLOBALCONSTANT.Manage_GENERALCODES;
	String SaveMsg= GLOBALCONSTANT.BlankString;
	String GeneralCodeValues=GLOBALCONSTANT.BlankString;
	//It is Provide Reference of Service which is used to call Dao Implemente Functions
	@Autowired
	private IGENERALCODESSERVICE ObjectGeneralCodeService;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	

	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	
	//Create the OBject
	ModelAndView ObjModelAndView=new ModelAndView();
	//It is used to show form of Addgeneralcodes screen
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_GeneralCodes_EditDelete , method = RequestMethod.GET)
	// use ObjectGeneralCodes for provide the getter and setter of General_codes table
	public ModelAndView getGeneralCodeForm(@ModelAttribute(GLOBALCONSTANT.GeneralCode_ModelAttribute) GENERALCODESBEAN ObjectGeneralCodes) throws JASCIEXCEPTION  {	 
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		
	
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
	
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/*WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				StrMenuType, StrMenuOption);//GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList
		*/
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		GENERALCODESBE ObjectGeneralCodesBe=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());//It is used set the label on GeneralCodeAddNew
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_GeneralCodes_EditDelete);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		//ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_StrApplication,ObjectGeneralCodesBe.getGeneralCodes_SYSTEM());		
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_StrApplication,GLOBALCONSTANT.GeneralCodes_ApplicationValue);
		/*ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ApplicationValueList, ObjectGeneralCodeService.SelectApplication());//Used to get the application dropdown value
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList, ObjectGeneralCodeService.SelectGeneralCode());//Used to get the generalcode dropdown value
		*/
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ApplicationValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GENERALCODES,GLOBALCONSTANT.BlankString));
		if(SaveMsg!=GLOBALCONSTANT.BlankString){
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeEditDelete_Confirm_Save,SaveMsg);
		}
		else{
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeEditDelete_Confirm_Save,GLOBALCONSTANT.BlankString);
		}
		SaveMsg=GLOBALCONSTANT.BlankString;
		return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBe);
		
	}
	
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_ResponsePage , method = RequestMethod.GET)
	// use ObjectGeneralCodes for provide the getter and setter of General_codes table
	public ModelAndView getResponsepage(){	 
		

		//LOGINSERVICEIMPL.ObjCommonSessionBe=OBJCOMMONSESSIONBE;
		
		/**get userid and password from session object*/
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
	
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		//Redirect to Response page because System use of team member is 'N'
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_ResponsePage);	
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;		
	}

	//For insert data in General Codes table for screen Generalcodes
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_GeneralCodes_EditDelete , method = RequestMethod.POST)
	public ModelAndView addEntry(@ModelAttribute(GLOBALCONSTANT.GeneralCode_ModelAttribute)  GENERALCODESBEAN ObjectGeneralCodes,HttpServletRequest request) throws JASCIEXCEPTION, JASCIEXCEPTION  {	 		
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		GENERALCODESBE ObjectGeneralCodesBe=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());
		try{
			//Method for Add Data In Table General_Codes
			String SelectApplication=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspSelectApplication);
			String SelectGeneralCode=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspTextBoxGeneralCode);
			String StrGeneralCodeID=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspSelectGeneralCode);
			String StrLastActivityDateUTC=request.getParameter(GLOBALCONSTANT.UTCLastActivityDate);
			
			ObjectGeneralCodes.setLastActivityDate(StrLastActivityDateUTC);
			//Set the value in setter
			ObjectGeneralCodes.setApplication(SelectApplication);
			ObjectGeneralCodes.setGeneralCode(SelectGeneralCode);
			ObjectGeneralCodes.setGeneralCodeID(StrGeneralCodeID);
			
			//Function to call insert data into generalcodes and menu options
			ObjectGeneralCodeService.addEntry(ObjectGeneralCodes);		
			//Show Message for Save Successfully.
			SaveMsg = ObjectGeneralCodesBe.getGeneralCodes_Confirm_Save();
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList);			
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBe);

		}catch(JASCIEXCEPTION ex){			
			//ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_GeneralCodes_register);
			log.error(ex.getMessage(), ex);
			ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_GeneralCodes_EditDelete);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ConstraintViolation,ObjectGeneralCodesBe.getGeneralCodes_ErrorMsg_GeneralCode_MenuOption());
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBe);			
		}
		
	}


	//for showing List of GeneralCode class
	@RequestMapping(GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList)
	public ModelAndView getList(Map<String,Object> ObjectMap,HttpServletRequest request)  throws JASCIEXCEPTION {
		
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		LOGINSERVICEIMPL.ObjCommonSessionBe=OBJCOMMONSESSIONBE;
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution);
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		

		GENERALCODESBE ObjectGeneralCodesBe=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());
			
		try{
			ObjectMap.put(GLOBALCONSTANT.GeneralCode_ModelAttribute, new GENERALCODES());				
			//Get the list Which is display in grid
			List<GENERALCODESBEAN> ListGeneralCodes=ObjectGeneralCodeService.getList(OBJCOMMONSESSIONBE);
		
			//It is Redirect on Response page when TeamMember's SYSTEMUSE="N"
            if(OBJCOMMONSESSIONBE.getSystemUse().equals(GLOBALCONSTANT.GeneralCodes_SystemUse_N))
            {            
            	ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
    			objWebServiceStatus.setStrMessage(GLOBALCONSTANT.YOUDONTHAVEACCESS);
    			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
    			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
    			return ObjModelAndView;
            }
          //If list of size will larger than zero(0) than scren will navigate on GeneralCodeList Screen
            else if(ListGeneralCodes.size()>GLOBALCONSTANT.INT_ZERO){
				//This function is to return the list of data in General_Codes table
				ObjectMap.put(GLOBALCONSTANT.GeneralCodeList,ListGeneralCodes);
				ObjModelAndView = new ModelAndView(GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList);
				ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBe);
            	
			}
			else{		
				
				ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_EditDelete);
				return ObjModelAndView;
			}
		}catch(JASCIEXCEPTION ex){	
			log.error(ex.getMessage());
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.GeneralCodeList);
			return ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		}
	}

	//for deleting of GeneralCode and generalcodeid screen
	@RequestMapping(GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeDelete)
	public ModelAndView deleteEntry(@PathVariable String tenant,@PathVariable String company,@PathVariable String application,@PathVariable String generalcodeid,@PathVariable String generalcode) throws JASCIEXCEPTION  {	 
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		//This function is used to delete record behalf of tenant
			
		GENERALCODESBE ObjectGeneralCodesBeList=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());	
		try{
			//Delete Function Call
			ObjectGeneralCodeService.deleteEntry(tenant,company,application,generalcodeid,generalcode);
			//This is used to redirect to GeneralCodeList Screen
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList); 
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeList);
			//return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList);
		}catch(JASCIEXCEPTION ex){			
			//ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_GeneralCodes_register);
			log.error(ex.getMessage(), ex);
			ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_GeneralCodes_EditDelete);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ConstraintViolation, GLOBALCONSTANT.GeneralCodes_exception_ConstraintViolationException);
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeList);			
		}
	}

	//for fetch data according to Primary Composite key field	for screen generalcode and generalcodeid
	@RequestMapping(value=GLOBALCONSTANT.editdata, method = RequestMethod.GET)
	public ModelAndView getEntry(GENERALCODES GeneralCodes,HttpServletRequest request) throws JASCIEXCEPTION  {	 
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String tenant=request.getParameter(GLOBALCONSTANT.StrTenant);
		String company=request.getParameter(GLOBALCONSTANT.StrCompany);
		String application=request.getParameter(GLOBALCONSTANT.StrApplication);
		String generalcodeid=request.getParameter(GLOBALCONSTANT.StrGeneralCodeID);
		String generalcode=request.getParameter(GLOBALCONSTANT.StrGeneralCode);
		String pageflag=request.getParameter(GLOBALCONSTANT.PageStatus);
		
		//Get the data passing Tenant for showing in GeneralCodeEdit Screen
		GENERALCODESBE ObjectGeneralCodesBeAdd=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());	
		
		//Condition for Redirect Pages
		if(GLOBALCONSTANT.ZeroValue.equals(pageflag)){
			GeneralCodeFlag=GLOBALCONSTANT.ZeroValue;
		}
		else if(GLOBALCONSTANT.OneValue.equals(pageflag)){
			GeneralCodeFlag=GLOBALCONSTANT.OneValue;
		}
		try{
			GENERALCODES GeneralCodesObject = ObjectGeneralCodeService.getEntry(tenant,company,application,generalcodeid,generalcode);	
			//GLOBALCONSTANT.GeneralCodesObject is used to map on GeneralCodesEdit for show data in the table of General_Codes
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.GeneralCodeEdit,GLOBALCONSTANT.GeneralCodesObject, GeneralCodesObject); 
			/*ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ApplicationValueList, ObjectGeneralCodeService.SelectApplication());
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList, ObjectGeneralCodeService.SelectGeneralCode());
			*/
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ApplicationValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GENERALCODES,GLOBALCONSTANT.BlankString));
			
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeAdd);
			//return new ModelAndView(GLOBALCONSTANT.GeneralCodeEdit,GLOBALCONSTANT.GeneralCodesObject, GeneralCodesObject); 
		}catch(JASCIEXCEPTION ex){						
			log.error(ex.getMessage(), ex);
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList); 
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeAdd);
			//return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList);		
		}
		
	}



	//For update the record of General code for generalcode and generalcodeid screen
	@RequestMapping(value=GLOBALCONSTANT.GeneralCodes_RequestMapping_UpdateGeneralCode, method = RequestMethod.POST)
	public ModelAndView updateEntry(@ModelAttribute(GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodesObject) GENERALCODESBEAN ObjectGeneralCodes,HttpServletRequest request) throws JASCIEXCEPTION  {	 
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		//Function that used to update record that implement in DAOIML
		GENERALCODESBE ObjectGeneralCodesBeAdd=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());	
		
		try{
			//Set Primary keys Field for data update
			String StrTenant=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspStrTenant);
			String StrCompany=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspStrCompany);
			String SelectApplication=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspSelectApplication);
			String SelectGeneralCode=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspTextBoxGeneralCode);
			String StrGeneralCodeID=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspSelectGeneralCode);
			String StrLastActivityDateUTC=request.getParameter(GLOBALCONSTANT.UTCLastActivityDate);
			
			ObjectGeneralCodes.setLastActivityDate(StrLastActivityDateUTC);
			ObjectGeneralCodes.setTenant(StrTenant);
			ObjectGeneralCodes.setCompany(StrCompany);
			ObjectGeneralCodes.setApplication(SelectApplication);
			ObjectGeneralCodes.setGeneralCode(SelectGeneralCode);
			ObjectGeneralCodes.setGeneralCodeID(StrGeneralCodeID);


			//Use to redirect on GeneralCodeList
			if(GeneralCodeFlag.equals(GLOBALCONSTANT.ZeroValue)){
				ObjectGeneralCodeService.updateEntry(ObjectGeneralCodes);
				ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList); 
				return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeAdd);
			}
			//redirect General codes Identification screen
			else//return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList);
			{
				ObjectGeneralCodeService.updateGeneralCodeID(ObjectGeneralCodes);
				ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodesidentification+GLOBALCONSTANT.RequestMapping_MenuValue+MenuValue); 
				return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeAdd);
			}

		}
		catch(JASCIEXCEPTION ex){						
			log.error(ex.getMessage());
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList); 
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeAdd);
			//return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList);		
		}
	}
	//Screen will display according to GeneralcodeID screen both selected
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeIdentificationdata, method = RequestMethod.GET)
	public ModelAndView getGeneralCodeIDEntry(Map<String,Object> ObjectMap,Map<String,Object> ObjectMapNew,HttpServletRequest request)  throws JASCIEXCEPTION {
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		LOGINSERVICEIMPL.ObjCommonSessionBe=OBJCOMMONSESSIONBE;
		String FilteredValue=request.getParameter(GLOBALCONSTANT.GeneralCodes_MenuValue);
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeIdentification_Execution+FilteredValue);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		GENERALCODESBE ObjectGeneralCodesBeList=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());	
				

		try{
			FilteredValue.equalsIgnoreCase(null);
		}
		catch(NullPointerException ex){FilteredValue=GLOBALCONSTANT.BlankString;}
		MenuValue=FilteredValue;
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_GeneralCodes_General_code_identification);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBeList);
	}


	//It is used to show form of add general_codes id data
	@RequestMapping(GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeEditDelete)
	// use ObjectGeneralCodes for provide the details of General_codes table
	public ModelAndView getGeneralCodeId(@ModelAttribute(GLOBALCONSTANT.GeneralCode_ModelAttribute) GENERALCODESBEAN ObjectGeneralCodes,@PathVariable String tenant,@PathVariable String company,@PathVariable String application,@PathVariable String generalcodeid) throws JASCIEXCEPTION  {	 
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		GENERALCODESBE ObjectGeneralCodesBe=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());//It is used set the label on GeneralCodeAddNew	
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_GeneralCodes_EditDelete);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_StrApplication,application);
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_StrGeneralCode,generalcodeid);
		/*ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ApplicationValueList, ObjectGeneralCodeService.SelectApplication());//Used to get the application dropdown value
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList, ObjectGeneralCodeService.SelectGeneralCode());//Used to get the generalcode dropdown value
		*/
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ApplicationValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GENERALCODES,GLOBALCONSTANT.BlankString));
		
		return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBe);
	}



	//For insert data in General Codes table 
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeEditDelete , method = RequestMethod.POST)
	public ModelAndView addGeneralCodeID(@ModelAttribute(GLOBALCONSTANT.GeneralCode_ModelAttribute)  GENERALCODESBEAN ObjectGeneralCodes,HttpServletRequest request,@PathVariable String tenant,@PathVariable String company,@PathVariable String application,@PathVariable String generalcodeid) throws JASCIEXCEPTION  {	 
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		//This function is use to insert data in table using hibernet from DAOIMP
		GENERALCODESBE ObjectGeneralCodesBe=ObjectGeneralCodeService.SetGeneralCodesBe(OBJCOMMONSESSIONBE.getCurrentLanguage().toUpperCase());
		//ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodes_GeneralCodeList);  
		String StrApplication=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspSelectApplication);
		String StrGneralCodeID=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspSelectGeneralCode);

		String StrGeneralCode=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspTextBoxGeneralCode);
		String StrLastActivityDateUTC=request.getParameter(GLOBALCONSTANT.UTCLastActivityDate);
		
		ObjectGeneralCodes.setLastActivityDate(StrLastActivityDateUTC);
		ObjectGeneralCodes.setApplication(StrApplication);
		ObjectGeneralCodes.setGeneralCode(StrGeneralCode);
		ObjectGeneralCodes.setGeneralCodeID(StrGneralCodeID);
		
		//String SelectApplication=request.getParameter(GLOBALCONSTANT.GeneralCodes_JspSelectApplication);
		try{
			//Method for Add Data In Table General_Codes
			//ObjectGeneralCodeService.insertRow(ObjectGeneralCodes);	
			//method that use only inser in generalcodes table
			ObjectGeneralCodeService.addGeneralCodeIdEntry(ObjectGeneralCodes);	
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_GeneralCodesidentification+GLOBALCONSTANT.RequestMapping_MenuValue+MenuValue);			
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBe);
		}catch(JASCIEXCEPTION ex){			
			log.error(ex.getMessage(), ex);
			//ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeEditDelete+"/{"+tenant+"}/{"+company+"}/{"+application+"}/{"+generalcodeid+"}");
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ConstraintViolation,ObjectGeneralCodesBe.getGeneralCodes_ErrorMsg_GeneralCode());
			return ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_ScreenLabel,ObjectGeneralCodesBe);			
		}
	}

	//Delete Using Kendo UI First Screen GeneralCode
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Kendo_GeneralCodeDelete, method = RequestMethod.POST)
	public @ResponseBody Boolean destroy(@RequestBody Map<String, Object> model) throws JASCIEXCEPTION { 		
		Boolean StatusDelete=ObjectGeneralCodeService.deleteEntry((String)model.get(GLOBALCONSTANT.StrTenant),(String)model.get(GLOBALCONSTANT.StrCompany),(String)model.get(GLOBALCONSTANT.StrApplication),(String)model.get(GLOBALCONSTANT.StrGeneralCodeID),(String)model.get(GLOBALCONSTANT.StrGeneralCode));
		return StatusDelete;
	} 

	//Delete Using Kendo UI First Screen GeneralCode
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Kendo_GeneralCodeIDDelete, method = RequestMethod.POST)
	public @ResponseBody Boolean deleteGeneralCodeIdEntry(@RequestBody Map<String, Object> model) throws JASCIEXCEPTION { 
		Boolean StatusDelete=ObjectGeneralCodeService.deleteGeneralCodeID((String)model.get(GLOBALCONSTANT.StrTenant),(String)model.get(GLOBALCONSTANT.StrCompany),(String)model.get(GLOBALCONSTANT.StrApplication),(String)model.get(GLOBALCONSTANT.StrGeneralCodeID),(String)model.get(GLOBALCONSTANT.StrGeneralCode));
		return StatusDelete;
	} 

	//read data using Kendo ui GeneralCode
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Kendo_GeneralCodeRead, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODESBEAN>  getList() {	
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		

		List<GENERALCODESBEAN> ListGeneralCodes = new ArrayList<GENERALCODESBEAN>();

		try {
			ListGeneralCodes=ObjectGeneralCodeService.getList(OBJCOMMONSESSIONBE);

		} catch (JASCIEXCEPTION e) {
			log.error(e.getMessage());
			return ListGeneralCodes;
		}

		return ListGeneralCodes;
	}



	//read data using Kendo ui GeneralCodeID Main Grid(New Screen)
	@RequestMapping(value = GLOBALCONSTANT.GeneralCodes_Kendo_GeneralCodeIDReadMain, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODESBEAN>  getGeneralCodeListID() {
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();
		
		List<GENERALCODESBEAN> MainList = new ArrayList<GENERALCODESBEAN>();
		try {

			MainList=ObjectGeneralCodeService.getMainGeneralCodeList(StrTenant, StrCompany, MenuValue);
			GeneralCodeValues=MainList.get(GLOBALCONSTANT.INT_ZERO).getGeneralCode();

		} catch (JASCIEXCEPTION e) {
			log.error(e.getMessage(), e);
			return MainList;
		}			
		return MainList;
	}



	//read data using Kendo ui GeneralCodeID Sub Grid(New Screen)
	@RequestMapping(value =GLOBALCONSTANT.GeneralCodes_Kendo_GeneralCodeIDReadSub, method = RequestMethod.GET)
	public @ResponseBody  List<GENERALCODESBEAN>  getGeneralCodeSubListID() {
		//COMMONSESSIONBE OBJCOMMONSESSIONBE = LOGINSERVICEIMPL.OBJCOMMONSESSIONBE;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();
		List<GENERALCODESBEAN> SubList = new ArrayList<GENERALCODESBEAN>();
		try {
			SubList=ObjectGeneralCodeService.getSubGeneralCodeList(StrTenant, StrCompany, GeneralCodeValues);

		} catch (JASCIEXCEPTION e) {
			log.error(e.getMessage(), e);
			return SubList;
		}
		return SubList;
	}

}

