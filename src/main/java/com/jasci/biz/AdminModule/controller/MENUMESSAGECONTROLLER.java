/*
Description make a controller of MENUMESSAGES class for mapping with jsp page
Created By Aakash Bishnoi
Created Date Dec 14 2014

 */
package com.jasci.biz.AdminModule.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.MENUMESSAGESBEAN;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.IMENUMESSAGESERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;
@Controller
public class MENUMESSAGECONTROLLER {

	
	@Autowired
	private IMENUMESSAGESERVICE ObjectMenuMessageService;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	
	//Create the Object of Model And View for Present the Screen
	ModelAndView ObjModelAndView=new ModelAndView();
	static Logger log= LoggerFactory.getLogger(MENUMESSAGECONTROLLER.class);
	//List of the Company
	private String StrCompanyList[]=null; 
	
	//It is used to View the MenuMessageSearchLookUp
	
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_MenuMessageSearchlookUp , method = RequestMethod.GET)
	public ModelAndView ShowList(){	 
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();

		
		/** Here we Check user is logged in or not*/
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	 
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Message_Maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuMessageSearchlookUp);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		try {
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuMessages_ScreenLabels, ObjectMenuMessageService.getMenuMessagesLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;	
			
		}
		return ObjModelAndView;		
	}

	//It is used to view the Menu Message MainTainence Page
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Menumessagesmaintenance , method = RequestMethod.GET)
	public ModelAndView getMaintenance(@ModelAttribute(GLOBALCONSTANT.ObjectMenuMessages) MENUMESSAGESBEAN ObjectMenuMessages){	 
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();

		
		/** Here we Check user is logged in or not*/
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	 
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Message_Maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}		
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Menumessagesmaintenance);	
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		try {
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuMessages_ScreenLabels, ObjectMenuMessageService.getMenuMessagesLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;	
			
		}
		return ObjModelAndView;		
	}

	//It is used to insert the record 
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Menumessagesmaintenance , method = RequestMethod.POST)
	public ModelAndView addEntry(HttpServletRequest request,@ModelAttribute(GLOBALCONSTANT.ObjectMenuMessages) MENUMESSAGESBEAN ObjectMenuMessages) {	 

		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		
		/** Here we Check user is logged in or not*/
	
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	 
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Message_Maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}	
		String utcDate=request.getParameter(GLOBALCONSTANT.Last_Activity_DateH);	
		try{
			ObjectMenuMessages.setLast_Activity_Date(utcDate);
			ObjectMenuMessageService.addEntry(ObjectMenuMessages,StrCompanyList);

			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuMessageSearchlookUp);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuMessages_ScreenLabels, ObjectMenuMessageService.getMenuMessagesLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			return ObjModelAndView;
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;	
			
		}

	}

	//read data using Kendo ui Menu Messages
	@RequestMapping(value = GLOBALCONSTANT.MenuMessages_Kendo_ReadAllMessages, method = RequestMethod.GET)
	public @ResponseBody  List<MENUMESSAGESBEAN>  getList() {	
		List<MENUMESSAGESBEAN> ListMenuMessages = new ArrayList<MENUMESSAGESBEAN>();
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompant=OBJCOMMONSESSIONBE.getCompany();
		String StrStatus=GLOBALCONSTANT.MenuMessages_Status;

		try {
			ListMenuMessages=ObjectMenuMessageService.getList(StrTenant,StrCompant,StrStatus);
		
	}catch(JASCIEXCEPTION objJasciexception){
		log.error(objJasciexception.getMessage());
		return ListMenuMessages;
		
	}
	
		return ListMenuMessages;
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 17, 2014
	 * Description this is used to get the List of Team Member Company
	 * @param StrTenant
	 * @param StrTeamMember
	 * @return List<TEAMMEMBERCOMPANIESBEAN>
	 * @throws JASCIEXCEPTION
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strrawtypes })
	//read team member company data using Kendo ui Menu Messages
	@RequestMapping(value = GLOBALCONSTANT.MenuMessages_Kendo_ReadCompanyList, method = RequestMethod.GET)
	public @ResponseBody  List<COMPANIES>  getCompanyList() {	
		List<COMPANIES>  ListCompanyName = new ArrayList();
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrTeamMember=OBJCOMMONSESSIONBE.getTeam_Member();


		try {
			ListCompanyName=ObjectMenuMessageService.getCompanyList(StrTenant,StrTeamMember);
		
	}catch(JASCIEXCEPTION objJasciexception){
		log.error(objJasciexception.getMessage());
		return ListCompanyName;
		
	}
		return ListCompanyName;
	}


	//Fetch Data From MENUMESSAGES Table to pass Parameter Teneat_ID,Company_ID,Status
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Menumessagesmaintenance_Update , method = RequestMethod.GET)
	public ModelAndView EditInfoHelp(HttpServletRequest request) {	 
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();

		/** Here we Check user is logged in or not*/
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	 
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Message_Maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}		
		try{
			String StrFetchMessage_ID=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Message_ID);
			String StrFetchTenant_ID=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Tenant_ID);
			String StrFetchCompany_ID=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Company_ID);
			String StrFetchStatus=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Status);
			String StrStatus=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Type);

			MENUMESSAGESBEAN ObjectMenuMessages = ObjectMenuMessageService.fetchMenuMessages(StrFetchTenant_ID,StrFetchCompany_ID,StrFetchStatus,StrStatus,StrFetchMessage_ID);
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.Object_Menumessagesmaintenance_Update,GLOBALCONSTANT.ObjectMenuMessages, ObjectMenuMessages);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuMessages_ScreenLabels, ObjectMenuMessageService.getMenuMessagesLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			return ObjModelAndView;
		
	}catch(JASCIEXCEPTION objJasciexception){
		log.error(objJasciexception.getMessage());
		return ObjModelAndView;	
		
	}
	}

	//It is used to update the record 
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_MenuMessage_Update , method = RequestMethod.POST)
	public ModelAndView updateEntry(HttpServletRequest request,@ModelAttribute(GLOBALCONSTANT.ObjectMenuMessages) MENUMESSAGESBEAN ObjectMenuMessages) {	 

		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();		
		
	
		/** Here we Check user is logged in or not*/
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	 
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Message_Maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String utcDate=request.getParameter(GLOBALCONSTANT.Last_Activity_DateH);
		try{
			ObjectMenuMessages.setLast_Activity_Date(utcDate);
			ObjectMenuMessageService.updateEntry(ObjectMenuMessages);

			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuMessageSearchlookUp);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuMessages_ScreenLabels, ObjectMenuMessageService.getMenuMessagesLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			return ObjModelAndView;
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;	
			
		}

	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 19, 2014
	 * Description this is used to delete the selected menu messages
	 * @param request
	 * @return
	 */
	@RequestMapping(value = GLOBALCONSTANT.MenuMessage_Kendo_Delete , method = RequestMethod.POST)
	public ModelAndView deleteEntry(HttpServletRequest request) {	 
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		/** Here we Check user is logged in or not*/
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
	 
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_Message_Maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}		
		try{
			String StrFetchMessage_ID=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Message_ID);
			String StrFetchTenant_ID=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Tenant_ID);
			String StrFetchCompany_ID=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Company_ID);
			String StrFetchStatus=request.getParameter(GLOBALCONSTANT.MenuMessages_Restfull_Status);


			ObjectMenuMessageService.deleteEntry(StrFetchMessage_ID,StrFetchTenant_ID,StrFetchCompany_ID,StrFetchStatus);
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuMessageSearchlookUp);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuMessages_ScreenLabels, ObjectMenuMessageService.getMenuMessagesLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			return ObjModelAndView;
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;	
			
		}
	}
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 18, 2014
	 * Description this is used to Get the Checked Company Name
	 * @param data
	 * @return
	 */
	@RequestMapping(value = GLOBALCONSTANT.MenuMessage_CompanyListCheckController, method = RequestMethod.POST)
	@ResponseBody
	public Boolean validateSymbol(@RequestParam(value=GLOBALCONSTANT.MenuMessage_CompanyList) String CompanyList) 
	{
		CompanyList= CompanyList.replace(GLOBALCONSTANT.OpenBracket,GLOBALCONSTANT.BlankString);
		CompanyList= CompanyList.replace(GLOBALCONSTANT.CloseBracket,GLOBALCONSTANT.BlankString);


		CompanyList=CompanyList.replace(GLOBALCONSTANT.BackSpaceBracket, GLOBALCONSTANT.BlankString);

		String CompanyListArray[]=CompanyList.split(GLOBALCONSTANT.Comma);
		StrCompanyList=CompanyListArray;
		return true;
	}
}
