/**

Date Developed :Dec 22 2014
Created by: Rahul kumar
Description :COMPANYMAINTENANCECONTROLLER Controller TO HANDLE THE REQUESTS FROM SCREENS
 */
package com.jasci.biz.AdminModule.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.COMPANYSCREENBE;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ICOMPANYMAINTENANCESERVICE;
import com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.util.NEXTBILLINGCONTROLNUMBER;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.COMPANIESBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class COMPANYMAINTENANCECONTROLLER {


	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	@Autowired
	ICOMPANYMAINTENANCESERVICE objIcompanyservice;
	@Autowired
	IMENUAPPICONSERVICE ObjIMenuAppIconService;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	NEXTBILLINGCONTROLNUMBER objNextbillingcontrolnumber;
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	String StrCompanyPartName=GLOBALCONSTANT.BlankString;
	String StrBackKendoURL=GLOBALCONSTANT.BlankString;
	static Logger log= LoggerFactory.getLogger(COMPANYMAINTENANCECONTROLLER.class);


	ModelAndView ObjModelAndView=new ModelAndView();




	/**
	 * 
	 * @Description This function redirect to Company Lookup screen
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @return
	 * @Return type ModelAndView
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Company_LookupScreen_Action, method = RequestMethod.GET)
	public ModelAndView showCompanyLookup(){


		//String StrLanguage=OBJCOMMONSESSIONBE.getTeam_Member_Language();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		//String tnt=OBJCOMMONSESSIONBE.getTenant();
		COMPANYSCREENBE objCompanyscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Company_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}


		try{
			objCompanyscreenbe=	objIcompanyservice.setScreenLanguage(GLOBALCONSTANT.Param_Languages_CompanyMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}

		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Company_LookupScreen_Page);
		//	ObjModelAndView= new ModelAndView("uploadimage2");
		ObjModelAndView.addObject(GLOBALCONSTANT.CompanyMaintenanceSceernLabelObj,objCompanyscreenbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

		return ObjModelAndView;

	}



	/**
	 * 
	 * @Description This function redirect to Company Lookup new screen
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @return
	 * @Return type ModelAndView
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Company_Maintenance_New_Screen_Action, method = RequestMethod.GET)
	public ModelAndView showCompanyMaintenanceNew(HttpServletRequest request){


		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();
		COMPANYSCREENBE objCompanyscreenbe=null;
		String BillingControlNumber=GLOBALCONSTANT.STRING_ONE;
		String backSatus=request.getParameter(GLOBALCONSTANT.BACKSTATUS);
		String CountryCode=GLOBALCONSTANT.BlankString;
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Company_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		String TeamMemberName=GLOBALCONSTANT.BlankString;
		  Date ObjDate1 = new Date();
		  String StrCurrentDate=null;
		  
		  SimpleDateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format_yyyy_MM_dd);
		  try {
		   StrCurrentDate=ObjDateFormat1.format(ObjDate1);
		  } catch (Exception objException) {
		   
		   log.error(objException.getMessage());
		  }

		try{
			objCompanyscreenbe=	objIcompanyservice.setScreenLanguage(GLOBALCONSTANT.Param_Languages_CompanyMaintenance_ObjectCode,StrLanguage);
		
			CountryCode=objIcompanyservice.getCountryCode();
			TeamMemberName=ObjIMenuAppIconService.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member());
		    BillingControlNumber=GLOBALCONSTANT.STRING_ONE;

		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}

		
		Long NextBillingControlNumber=objNextbillingcontrolnumber.getNextBillingControlNumber();
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Company_Maintenance_New_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.CompanyMaintenanceSceernLabelObj,objCompanyscreenbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.CompanyMaintenanceBillingControlNumberObj,BillingControlNumber);
		
		
		try{
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageCountryCodeListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_COUNTRYCODE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStatesListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_STATECODE,GLOBALCONSTANT.BlankString));		
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeThemeFullDisplayListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_THEMEFULLDISPLAY,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeThemeMobileListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_THEMEMOBILE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeThemeRFListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_THEMERF,GLOBALCONSTANT.BlankString));
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		
		ObjModelAndView.addObject(GLOBALCONSTANT.CountryCodeObject,CountryCode);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_TeamMemberName,TeamMemberName);
		ObjModelAndView.addObject(GLOBALCONSTANT.SetupDate,StrCurrentDate);
		ObjModelAndView.addObject(GLOBALCONSTANT.NextBillingControlNumber,NextBillingControlNumber);
		ObjModelAndView.addObject(GLOBALCONSTANT.BACKSTATUS,backSatus);

		return ObjModelAndView;

	}







	/**
	 * 
	 * @Description This function redirect to Company Search Lookup new screen   
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @return
	 * @Return type ModelAndView
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Company_SearchLookup_Screen_Action, method = RequestMethod.GET)
	public ModelAndView showCompanySearchLookup(){


		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		COMPANYSCREENBE objCompanyscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Company_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}


		try{
			objCompanyscreenbe=	objIcompanyservice.setScreenLanguage(GLOBALCONSTANT.Param_Languages_CompanyMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}

		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Company_SearchLookup_Screen_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.Company_Kendo_Read_URL,GLOBALCONSTANT.RequestMapping_Company_Read_Kendo_UI);
		StrBackKendoURL=GLOBALCONSTANT.RequestMapping_Company_SearchLookup_Screen_Action.replace(GLOBALCONSTANT.Forward_Slash, GLOBALCONSTANT.BlankString);
		//	ObjModelAndView= new ModelAndView("uploadimage2");
		ObjModelAndView.addObject(GLOBALCONSTANT.CompanyMaintenanceSceernLabelObj,objCompanyscreenbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

		return ObjModelAndView;

	}





	/**
	 * 
	 * @Description  This function redirect to Company Search Lookup screen   
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param request
	 * @return
	 * @Return type ModelAndView
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Company_SearchLookup_By_Part_Screen_Action, method = RequestMethod.GET)
	public ModelAndView showCompanySearchLookupbyPart(HttpServletRequest request){

		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();

		String CompanyPartName=request.getParameter(GLOBALCONSTANT.CompanyPartName);


		COMPANYSCREENBE objCompanyscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Company_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			objCompanyscreenbe=	objIcompanyservice.setScreenLanguage(GLOBALCONSTANT.Param_Languages_CompanyMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}

		if(CompanyPartName!=null){
			StrCompanyPartName=CompanyPartName;
		}

		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Company_SearchLookup_Screen_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.CompanyMaintenanceSceernLabelObj,objCompanyscreenbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.Company_Kendo_Read_URL,GLOBALCONSTANT.RequestMapping_Company_Read_Kendo_UI_By_Part);
		StrBackKendoURL=GLOBALCONSTANT.RequestMapping_Company_SearchLookup_By_Part_Screen_Action.replace(GLOBALCONSTANT.Forward_Slash, GLOBALCONSTANT.BlankString);

		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

		return ObjModelAndView;

	}





	/**
	 * 
	 * @Description This function read data for Kendo ui Company Search Lookup
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param request
	 * @return
	 * @Return type List<COMPANIESBE>
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Company_Read_Kendo_UI, method = RequestMethod.GET)
	public @ResponseBody  List<COMPANIESBE>  getAllCompanyList(HttpServletRequest request) {	
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
				

		List<COMPANIESBE> objCompanies=null;
		//call service method to get Team memebr list

		try {
			objCompanies=objIcompanyservice.getAllCompanies(StrTenant);
		} catch (JASCIEXCEPTION objJasciexception) {
			
			log.error(objJasciexception.getMessage());
		}
		return objCompanies;


	}


	/**
	 * 
	 * @Description This function read data for Kendo ui Company Search Lookup
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param request
	 * @return
	 * @Return type List<COMPANIESBE>
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Company_Read_Kendo_UI_By_Part, method = RequestMethod.GET)
	public @ResponseBody  List<COMPANIESBE>  getList(HttpServletRequest request) {	
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		

		List<COMPANIESBE> objCompanies=null;
		//call service method to get Team memebr list
		try {
			objCompanies=objIcompanyservice.getCompanyByPartName(StrCompanyPartName);
		} catch (JASCIEXCEPTION objJasciexception) {
			
			log.error(objJasciexception.getMessage());
		}
		return objCompanies;


	}





	/**
	 * 
	 * @Description This function redirect to Company SearchLookUp screen after addOrUpdate a record 
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param objCompaniesbe
	 * @param request
	 * @return
	 * @Return type ModelAndView
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Company_AddOrUpdate_Action, method = RequestMethod.POST)
	public ModelAndView addOrUpdateMenuAppIcon(@ModelAttribute(GLOBALCONSTANT.Company_Register_ModelAttribute)  COMPANIESBE objCompaniesbe,HttpServletRequest request){

		//String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String strLastActivityBy=OBJCOMMONSESSIONBE.getTeam_Member();
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String address=request.getParameter(GLOBALCONSTANT.MenuAppIcon_IconAddress);
		//String ApplicationId=request.getParameter(GLOBALCONSTANT.MenuAppIcon_ApplicationId);
		//String BillingControlNumber=request.getParameter(GLOBALCONSTANT.BillingControlNumber);
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		//MENUAPPICONSCREENBE objMenuappiconscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Company_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			objCompaniesbe.setLastActivityTeamMember(strLastActivityBy);
			objCompaniesbe.setLogo(address);
			objCompaniesbe.setTenantId(StrTenant);
			objCompaniesbe.setSetUpBy(strLastActivityBy);
			
			objIcompanyservice.addOrUpdateCompany(objCompaniesbe);

		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}


		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Company_SearchLookup_Screen_Action);
		


		return ObjModelAndView;

	}





	/**
	 * 
	 * @Description This function use for request to edit a record. 
	 * @author Rahul Kumar
	 * @Date Dec 27, 2014 
	 * @param request
	 * @return
	 * @Return type ModelAndView
	 */


	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Company_AddOrUpdate_Request_Action, method = RequestMethod.GET)
	public ModelAndView addOrUpdateMenuAppIconRequest(HttpServletRequest request){

		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();
		String CompanyId=request.getParameter(GLOBALCONSTANT.Company_Restfull_CompanyByID);
		
		String FlagForReturn=request.getParameter(GLOBALCONSTANT.MenuAppIcon_FlagForReturn);
		String TeamMemberName=GLOBALCONSTANT.BlankString;
		String SetupTeamMemberName=GLOBALCONSTANT.BlankString;
		COMPANYSCREENBE objCompanyscreenbe=null;
	
		COMPANIESBE objCompaniesbe=null;
		String CountryCode=GLOBALCONSTANT.BlankString;
		

		//MENUAPPICONSCREENBE objMenuappiconscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Company_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			//objMenuappiconbe.setLastActivityTeamMember(strLastActivityBy);
			objCompaniesbe=objIcompanyservice.getCompanyById(StrTenant, CompanyId);

		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}

		try{
			objCompanyscreenbe=	objIcompanyservice.setScreenLanguage(GLOBALCONSTANT.Param_Languages_CompanyMaintenance_ObjectCode,StrLanguage);
		
			CountryCode=objIcompanyservice.getCountryCode();
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		
		
		try {
			TeamMemberName=ObjIMenuAppIconService.getTeamMemberName(StrTenant, objCompaniesbe.getLastActivityTeamMember());
			SetupTeamMemberName=ObjIMenuAppIconService.getTeamMemberName(StrTenant, objCompaniesbe.getSetUpBy());
		} catch (JASCIEXCEPTION objJasciexception) {
			
			log.error(objJasciexception.getMessage());
		}
		
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Company_Maintenance_Update_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.CompanyMaintenanceSceernLabelObj,objCompanyscreenbe);
		
		try{
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageCountryCodeListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_COUNTRYCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStatesListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_STATECODE,GLOBALCONSTANT.BlankString));		
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeThemeFullDisplayListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_THEMEFULLDISPLAY,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeThemeMobileListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_THEMEMOBILE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeThemeRFListObject,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant,StrCompany,GLOBALCONSTANT.GIC_THEMERF,GLOBALCONSTANT.BlankString));
			}catch(JASCIEXCEPTION objJasciexception){

				log.error(objJasciexception.getMessage());
			}
		
		ObjModelAndView.addObject(GLOBALCONSTANT.CountryCodeObject,CountryCode);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_TeamMemberName,TeamMemberName);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_SetupTeamMemberName,SetupTeamMemberName);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_FlagForReturnObj,FlagForReturn);
		
		
		ObjModelAndView.addObject(GLOBALCONSTANT.Company_Bean_Object,objCompaniesbe);
		//ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Kendo_Read_URL,FlagForReturn);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_SearchLookup_BackScreen,StrBackKendoURL);
		


		return ObjModelAndView;

	}


	
	
	
	/**
	 * 
	 * @Description This function redirect to CompanySearchlookup screen page after delete a Company record 
	 * @author Rahul Kumar
	 * @Date Dec 29, 2014 
	 * @param request
	 * @return
	 * @Return type ModelAndView
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Company_Maintenance_Delete_Screen_Action, method = RequestMethod.POST)
	public ModelAndView deleteMenuAppIcon(HttpServletRequest request){
		
		
		String CompanyId=request.getParameter(GLOBALCONSTANT.Company_Restfull_CompanyByID);
		String Tenant=OBJCOMMONSESSIONBE.getTenant();
		//String FlagForReturn=request.getParameter(GLOBALCONSTANT.MenuAppIcon_FlagForReturn);
		String KendoUrl=request.getParameter(GLOBALCONSTANT.MenuAppIcon_KendoUrl);
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		//COMPANYSCREENBE objCompanyscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Company_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			
			objIcompanyservice.deleteCompany(Tenant, CompanyId);
		
			if(GLOBALCONSTANT.RequestMapping_Company_Read_Kendo_UI.equalsIgnoreCase(KendoUrl)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Company_SearchLookup_Screen_Action);
			}
			else if(GLOBALCONSTANT.RequestMapping_Company_Read_Kendo_UI_By_Part.equalsIgnoreCase(KendoUrl)){
				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Company_SearchLookup_By_Part_Screen_Action);
			}
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}
		
		try{
			objIcompanyservice.setScreenLanguage(GLOBALCONSTANT.Param_Languages_CompanyMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;

	}




}