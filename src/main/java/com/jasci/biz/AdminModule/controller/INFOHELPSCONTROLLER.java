/*
Description make a controller of INFOHELPS class for mapping with View Of infoHelps
Created By Aakash Bishnoi
Created Date Oct 29 2014

 */
package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.model.INFOHELPS;
import com.jasci.biz.AdminModule.model.INFOHELPSBEAN;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.IINFOHELPSSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;




@Controller
public class INFOHELPSCONTROLLER {

	/**
	 * LAnguages Labels
	 */
	@Autowired
	private IINFOHELPSSERVICE ObjectInfohelpService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	

	@Autowired
	SCREENACCESSSERVICE screenAccessService;

	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	//for using search condition
	String StrInfoHelpValue=GLOBALCONSTANT.BlankString;
	String StrPartoftheDescriptionValue=GLOBALCONSTANT.BlankString;
	String StrInfoHelpTypeValue=GLOBALCONSTANT.BlankString;
	String StrFetchInfoHelp=GLOBALCONSTANT.BlankString;
	String StrFetchLanguage=GLOBALCONSTANT.BlankString;
	static Logger log = LoggerFactory.getLogger(INFOHELPSCONTROLLER.class.getName());
	
	ModelAndView ObjModelAndView=new ModelAndView();
	//It is used to show Info Help Assignment Search screen
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch , method = RequestMethod.GET)
	public ModelAndView getInfoHelp(@ModelAttribute(GLOBALCONSTANT.InfoHelps_ModelAttribute) INFOHELPS ObjectInfoHelps) {	 
		
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		try{
			//List<GENERALCODES> ObjGeneralCodes=ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_INFOHELP,GLOBALCONSTANT.BlankString);
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectInfoHelpType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_INFOHELP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		}catch (JASCIEXCEPTION ObjectDataBaseException) {	
			log.error(ObjectDataBaseException.getMessage());
			
			return ObjModelAndView;
		}	
		return ObjModelAndView;
	}
	
	//It is used to show all record on search_look_up screen (Kendo Grid)using Display All 
	@RequestMapping(value =GLOBALCONSTANT.RequestMapping_Infohelp_SearchLookUp , method = RequestMethod.GET)
	public ModelAndView getInfoHelpList(HttpServletRequest request) throws JASCIEXCEPTION {	 

	String StrUserID=OBJCOMMONSESSIONBE.getUserID();
	String StrPassword=OBJCOMMONSESSIONBE.getPassword();

	if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	
	/** Here we Check user have this screen access or not*/

	WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

	if (!objWEBSERVICESTATUS.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	List<INFOHELPSBEAN> ListAllInfoHelps = new ArrayList<INFOHELPSBEAN>();
	//List<GENERALCODES> ObjGeneralCodes=ObjectInfohelpService.SelectInfoHelpType(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.InfoHelp_GeneralCodeID_InfoHelp);
	StrInfoHelpValue = request.getParameter(GLOBALCONSTANT.InfoHelps_RequestAttribute_EnterInfoHelp);
	StrPartoftheDescriptionValue = request.getParameter(GLOBALCONSTANT.InfoHelps_RequestAttribute_PartoftheDescription);
	StrInfoHelpTypeValue = request.getParameter(GLOBALCONSTANT.InfoHelps_RequestAttribute_InfoHelpType);
	
	if(StrInfoHelpValue == null || StrPartoftheDescriptionValue == null  || StrInfoHelpTypeValue == null){
		StrInfoHelpValue = GLOBALCONSTANT.BlankString;
		StrPartoftheDescriptionValue = GLOBALCONSTANT.BlankString;
		StrInfoHelpTypeValue = GLOBALCONSTANT.BlankString;
	}
		try {
	ListAllInfoHelps=ObjectInfohelpService.getInfoHelpList(StrInfoHelpValue,StrPartoftheDescriptionValue,StrInfoHelpTypeValue);
	
	if((!StrInfoHelpValue.equals(GLOBALCONSTANT.BlankString)) && ListAllInfoHelps.size()==GLOBALCONSTANT.IntOne ){	
		
		
		StrFetchInfoHelp=ListAllInfoHelps.get(GLOBALCONSTANT.INT_ZERO).getInfoHelp();
		StrFetchLanguage=ListAllInfoHelps.get(GLOBALCONSTANT.INT_ZERO).getLanguageCode();
		INFOHELPSBEAN InfoHelpsObject = ObjectInfohelpService.fetchInfoHelp(StrFetchInfoHelp,StrFetchLanguage);
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_InfoHelps_assignment_maintenance,GLOBALCONSTANT.InfoHelp_InfoHelpsObject, InfoHelpsObject);
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectInfoHelpType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_INFOHELP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectLanguage,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;
		
	}
	else if((!StrPartoftheDescriptionValue.equals(GLOBALCONSTANT.BlankString)) && ListAllInfoHelps.size()>GLOBALCONSTANT.INT_ZERO){
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Infohelp_Search_LookUp);
		ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectInfoHelpType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_INFOHELP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;			
	}
	else if((!StrInfoHelpTypeValue.equals(GLOBALCONSTANT.BlankString)) && ListAllInfoHelps.size()>GLOBALCONSTANT.INT_ZERO){
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Infohelp_Search_LookUp);
		ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		
		return ObjModelAndView;		
	}
	else if( StrInfoHelpValue.equals(GLOBALCONSTANT.BlankString) && StrPartoftheDescriptionValue.equals(GLOBALCONSTANT.BlankString) && StrInfoHelpTypeValue.equals(GLOBALCONSTANT.BlankString) ){
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Infohelp_Search_LookUp);
		ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;
	}
	
	} catch (JASCIEXCEPTION ObjectDataBaseException) {		
		log.error(ObjectDataBaseException.getMessage());
		return ObjModelAndView;
	}	
	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Infohelp_Search_LookUp);
	ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
	return ObjModelAndView;
	
}
	
	
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 23, 2014
	 * Description this is used to Show the page of InfoHelp SearchLookup
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Infohelp_Search_LookUp , method = RequestMethod.GET)
	public ModelAndView getLocationSearchLookup() {	 

		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();

		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		try {
			
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Infohelp_SearchLookUp);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			
		} catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
		return ObjModelAndView;
	}
	
	
	
	//read data using Kendo ui InfoHelp Search Lookup All DATA
	 @RequestMapping(value = GLOBALCONSTANT.RequestMapping_InfoHelps_Kendo_DisplayAll, method = RequestMethod.GET)
	  public @ResponseBody  List<INFOHELPSBEAN> getInfoHelpListKindo() {										
		 List<INFOHELPSBEAN> ListAllInfoHelps = new ArrayList<INFOHELPSBEAN>();			 
			try {
				ListAllInfoHelps=ObjectInfohelpService.getInfoHelpList(StrInfoHelpValue,StrPartoftheDescriptionValue,StrInfoHelpTypeValue);					
			} catch (JASCIEXCEPTION ObjectDataBaseException) {
				log.error(ObjectDataBaseException.getMessage());
				return ListAllInfoHelps;
			}													
			return ListAllInfoHelps;
	  }


	//It is Use to Add record in InfoHelp Tables	
	@RequestMapping(value =GLOBALCONSTANT.RequestMapping_InfoHelps_assignment_maintenance_add , method = RequestMethod.GET)
	public ModelAndView addInfoHelp(HttpServletRequest request) {	 
	try{
		
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		String StrTenantLanguage=OBJCOMMONSESSIONBE.getTenant_Language();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String backSatus=request.getParameter(GLOBALCONSTANT.BACKSTATUS);
		String StrTeamMember=OBJCOMMONSESSIONBE.getTeam_Member();
		String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_InfoHelps_assignment_maintenance_add_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectInfoHelpType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_INFOHELP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectLanguage,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelpTenantLanguage,StrTenantLanguage);
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_TeamMember,ObjectInfohelpService.getTeamMemberName(StrTenant_ID, StrTeamMember));
		ObjModelAndView.addObject(GLOBALCONSTANT.BACKSTATUS,backSatus);
		//ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_TeamMember,StrTeamMember);
		Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelps_yyyyMMdd_Format);
		String testDateString = ObjDateFormat.format(CurrentDate);	
		ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_CurrentDate,testDateString);
		ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		return ObjModelAndView;
	}
	catch(JASCIEXCEPTION ObjectDataBaseException){
		log.error(ObjectDataBaseException.getMessage());
		return ObjModelAndView;}
}
	
	
	 //Fetch Data From InfoHelps Table to pass Parameter InfoHelps,Language
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_InfoHelps_assignment_maintenance , method = RequestMethod.GET)
public ModelAndView showInfoHelp() {	 

	String StrUserID=OBJCOMMONSESSIONBE.getUserID();
	String StrPassword=OBJCOMMONSESSIONBE.getPassword();
	

	if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	
	/** Here we Check user have this screen access or not*/

	WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

	if (!objWEBSERVICESTATUS.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	try{
	
		INFOHELPSBEAN InfoHelpsObject = ObjectInfohelpService.fetchInfoHelp(StrFetchInfoHelp,StrFetchLanguage);
	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_InfoHelps_assignment_maintenance,GLOBALCONSTANT.InfoHelp_InfoHelpsObject, InfoHelpsObject);
	ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
	ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectLanguage,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
	return ObjModelAndView;
}
catch(JASCIEXCEPTION ObjectDataBaseException){
	log.error(ObjectDataBaseException.getMessage());
	return ObjModelAndView;}


}


//Fetch Data From InfoHelps Table to pass Parameter InfoHelps,Language an search lookup screen using request.getparameter
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_InfoHelps_assignment_Edit , method = RequestMethod.GET)
public ModelAndView EditInfoHelp(HttpServletRequest request) {	 

String StrUserID=OBJCOMMONSESSIONBE.getUserID();
String StrPassword=OBJCOMMONSESSIONBE.getPassword();


if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
	return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
}

/** Here we Check user have this screen access or not*/

WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
		GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

if (!objWEBSERVICESTATUS.isBoolStatus()) {
	ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
	return ObjModelAndView;

}
try{
	String StrFetchInfoHelpValue=request.getParameter(GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp);
	String StrFetchLanguageValue=request.getParameter(GLOBALCONSTANT.InfoHelps_Restfull_StrLanguage);
	
	INFOHELPSBEAN InfoHelpsObject = ObjectInfohelpService.fetchInfoHelp(StrFetchInfoHelpValue,StrFetchLanguageValue);
	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_InfoHelps_assignment_maintenance,GLOBALCONSTANT.InfoHelp_InfoHelpsObject, InfoHelpsObject);
	ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectInfoHelpType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_INFOHELP,GLOBALCONSTANT.BlankString));
	ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_Combobox_SelectLanguage,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
	ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
	return ObjModelAndView;
}
catch(JASCIEXCEPTION ObjectDataBaseException){
	log.error(ObjectDataBaseException.getMessage());
	return ObjModelAndView;}


}

//It is used to update the record 
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_InfoHelps_Update , method = RequestMethod.GET)
public ModelAndView updateEntry(@ModelAttribute(GLOBALCONSTANT.RequestMapping_InfoHelps_ObjectInfohelps) INFOHELPS InfoHelpsObject) {	 

	String StrUserID=OBJCOMMONSESSIONBE.getUserID();
	String StrPassword=OBJCOMMONSESSIONBE.getPassword();
	

	if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	/** Here we Check user have this screen access or not*/

	WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

	if (!objWEBSERVICESTATUS.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	try{
	
	ObjectInfohelpService.updateInfoHelps(InfoHelpsObject);
	
	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Infohelp_SearchLookUp);
	ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
	return ObjModelAndView;
}
catch(JASCIEXCEPTION ObjectDataBaseException){
	log.error(ObjectDataBaseException.getMessage());
	return ObjModelAndView;}

}


//Delete Using Kendo UI First InfoHelpSearch lookup
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_InfoHelps_Kindo_Delete , method = RequestMethod.GET)
public ModelAndView deleteInfoHelps(HttpServletRequest request) {	 

String StrUserID=OBJCOMMONSESSIONBE.getUserID();
String StrPassword=OBJCOMMONSESSIONBE.getPassword();
if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){
	return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
}

/** Here we Check user have this screen access or not*/

WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
		GLOBALCONSTANT.RequestMapping_Infohelp_AssignmentSearch_Execution);

if (!objWEBSERVICESTATUS.isBoolStatus()) {
	ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
	return ObjModelAndView;

}
try{
	String StrFetchInfoHelpValue=request.getParameter(GLOBALCONSTANT.InfoHelps_Restfull_StrInfoHelp);
	String StrFetchLanguageValue=request.getParameter(GLOBALCONSTANT.InfoHelps_Restfull_StrLanguage);
	
ObjectInfohelpService.deleteInfoHelps(StrFetchInfoHelpValue,StrFetchLanguageValue);
ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Infohelp_SearchLookUp);
ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));

return ObjModelAndView;
}
catch(JASCIEXCEPTION ObjectDataBaseException){
	log.error(ObjectDataBaseException.getMessage());
	return ObjModelAndView;}


}

/**------------It is used to set the date format--------------*/ 
	public static String currectDate(){
		DateFormat DateFormat = new SimpleDateFormat(GLOBALCONSTANT.CurrentDateFormat);
		Date ObjectDate = new Date();
		String datedata=DateFormat.format(ObjectDate);														
		return datedata;
	}
	
}
