/**
Description : Make a controller for SCAN LPN execution form which we can mapped ScanLPN.jsp pages and pass the value.
Created By : Shailendra Rajput
Created Date : Oct 16 2015
 */
package com.jasci.biz.AdminModule.controller;


import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.EXECUTIONBE;
import com.jasci.biz.AdminModule.be.SHAREDDATA;
import com.jasci.biz.AdminModule.service.IEXECUTIONSLABELSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class SCANLPNCONTROLLER {

	/**create modelandview instance*/
	ModelAndView ObjModelAndView=new ModelAndView();
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	@Autowired
	IEXECUTIONSLABELSSERVICE ObjScanLPNService;
	final Logger log = LoggerFactory.getLogger(SCANLPNCONTROLLER.class.getName());

	/** 
	 * description: This method is use to call scan lpn execution 
	 * input Param: ScreenName and Language	 * 
	 * @param StrJson
	 * @param executionName
	 * @param IsCompleted
	 * @return ModelAndView
	 * developed by: Shailendar kumar
	 * Date : Oct 20 2015
	 */
	
	@SuppressWarnings(GLOBALCONSTANT.Strunused)
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPINGSCANLPN , method = RequestMethod.GET)
	public ModelAndView getScanLpnExecution(@RequestParam(value=GLOBALCONSTANT.STRJSON) String StrJson,@RequestParam(value=GLOBALCONSTANT.EXECUTIONName) String executionName,@RequestParam(value=GLOBALCONSTANT.IsCompleted) Boolean IsCompleted) {	 

		String last_execution_name=GLOBALCONSTANT.BLANKSPACESTRING;
		String execution_Name=GLOBALCONSTANT.BLANKSPACESTRING;
		SHAREDDATA objShareddata=null;
		List<String> execution_Name_list=null;
		//process Engin Working
		if(IsCompleted!=null){
			
			
			if(IsCompleted==true){
				
				try {
					StrJson=URLEncoder.encode(StrJson, GLOBALCONSTANT.UTF8);
					executionName=URLEncoder.encode(executionName, GLOBALCONSTANT.UTF8);
				     } catch (UnsupportedEncodingException e) {
				      // TODO Auto-generated catch block
				      e.printStackTrace();
				     };
				     //System.out.println(ExecutionPath);

				     /**
				      * Creating a URL and pass the shared Data with execution
				      * History and is completed flag.
				      */
				     String strExecutionURL = GLOBALCONSTANT.NEXT_EXECUTION + GLOBALCONSTANT.QUESTION_MARK + GLOBALCONSTANT.STRJSON+GLOBALCONSTANT.Equal+StrJson
				       +GLOBALCONSTANT.AMPERSANDEXECUTIONNAME+execution_Name+GLOBALCONSTANT.AMPERSANDCOMPLETEDTRUE;
				     /**
				      * redirect to that URL
				      */
				     return new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon + strExecutionURL);
			}
			else{

				try{

					Gson objGson=new Gson();
					if(StrJson!=null){
						objShareddata=objGson.fromJson(StrJson, SHAREDDATA.class);
					}
					Type type = new TypeToken<List<String>>() {
					}.getType();

					if(executionName!=null){
						try{
							 execution_Name_list = objGson.fromJson(executionName, type);
/*
							execution_Name=execution_Name_list.get(GLOBALCONSTANT.INT_ZERO);*/
						}catch(Exception objException){
							objException.getMessage();
						}
					}

					
					ObjModelAndView=new ModelAndView(GLOBALCONSTANT.MODELVIEWNAMESCANLPN);
					ObjModelAndView.addObject(GLOBALCONSTANT.MAPPEDTEAMMEMBERNAME,objShareddata.getStrTeamMember());
					
					try {
						ObjModelAndView.addObject(GLOBALCONSTANT.EXECUTIONLABELS,ObjScanLPNService.getScanLPNLabels(LANGUANGELABELSAPI.strWebExecutionModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
					} catch (JASCIEXCEPTION e) {

						e.getMessage();
					}
					//GLOBALCONSTANT.OBJECT_SHAREDDATE.getStrCompanyID()
					ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.EXECUTIONHISTORY,executionName);
					 
					String strExecutionName=objShareddata.getObjSequenceBE().getStrEXECUTIONNAME();

					EXECUTIONBE objExecutionBE=objShareddata.getMapExecution().get(strExecutionName);
					
					ObjModelAndView.addObject(GLOBALCONSTANT.EXECUTIONName,execution_Name_list);
					ObjModelAndView.addObject(GLOBALCONSTANT.SHARED_OBJECT_JSON,StrJson);
					ObjModelAndView.addObject(GLOBALCONSTANT.SHARED_OBJECT,objShareddata);
					ObjModelAndView.addObject(GLOBALCONSTANT.EXECUTIONBE,objExecutionBE);
					
					ArrayList<String> objTopBarList=new ArrayList<String>();
					
					/*objTopBarList.add(" JASCI"+GLOBALCONSTANT.Equal+GLOBALCONSTANT.TEXT_STYLE_CLASS_BLACK);
					objTopBarList.add(OBJCOMMONSESSIONBE.getTeam_Member_Name()+GLOBALCONSTANT.Equal+GLOBALCONSTANT.TEXT_STYLE_CLASS_BLACK);
					 */	
				
					ObjModelAndView.addObject(GLOBALCONSTANT.TOP_BAR_TEXT_LIST,objTopBarList);

				}catch(Exception objException){
					log.error(objException.getMessage());
				}
			}	
		}
		return ObjModelAndView;
	
	}



}
