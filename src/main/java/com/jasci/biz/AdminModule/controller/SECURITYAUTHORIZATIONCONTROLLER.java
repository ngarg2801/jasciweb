/*

Description: make a controller of GENERALCODES class for mapping with jsp page and model
Created By: Vikas Jain
Created Date: Nov 20 2014
Modified on: Nov 25 2014
 */

package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.SECURITYAUTHORIZATIONSCREENBE;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONS;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONSBE;
import com.jasci.biz.AdminModule.model.TEAMMEMBERBEAN;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.biz.AdminModule.service.SECURITYAUTHORIZATIONSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
//@Scope("request")
public class SECURITYAUTHORIZATIONCONTROLLER {

	/**
	 * Autowired objects
	 */
	@Autowired
	SECURITYAUTHORIZATIONSERVICE ObjSecurityAuthorizationService;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	
	ModelAndView ObjModelAndView=new ModelAndView();

	String TeamMembername=GLOBALCONSTANT.BlankString;
	String PartOfTeamMemberName=GLOBALCONSTANT.BlankString;
	String StrPasswordDate=GLOBALCONSTANT.BlankString;
	SECURITYAUTHORIZATIONSCREENBE objLabels;
	String StrMenuType=GLOBALCONSTANT.StrMenuTypeFullScreen,StrMenuOption=GLOBALCONSTANT.Security_Set_Up;
	private static final Logger logger = LoggerFactory.getLogger(SECURITYAUTHORIZATIONCONTROLLER.class);
	
	/**
	 * @Purpose this method is used to rediect to page of team member look up 
	 * @return
	 */

	@RequestMapping(value=GLOBALCONSTANT.Security_M_Team_member_lookup, method = RequestMethod.GET)
	public ModelAndView getTeamMemberLookup(){
		
		/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Teammember_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.Security_M_Team_member_lookup);

		/**
		 * Getting the Screen labels 
		 */
		try {
			objLabels=ObjSecurityAuthorizationService.getSecuritySetUpScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			
			logger.error(objJasciexception.getMessage());
		}

		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

		return ObjModelAndView;
	}

	/**
	 * @pupose this method is used for adding the user in security authorization
	 * @param objSecurityAuthorizations
	 * @param objRequest
	 * @return
	 */
	@RequestMapping(value=GLOBALCONSTANT.Security_M_AddUser, method = RequestMethod.POST)
	public ModelAndView addUser(@ModelAttribute(GLOBALCONSTANT.Security_ModelAttributeSecurity) SECURITYAUTHORIZATIONSBE objSecurityAuthorizations ,HttpServletRequest objRequest){

		
		String LastActivityDateStringH=objRequest.getParameter(GLOBALCONSTANT.LastActivityDateStringH);
		String InvalidAttemptDateH=objRequest.getParameter(GLOBALCONSTANT.InvalidAttemptDateH);
		StrPasswordDate=LastActivityDateStringH;
		String StrSequrityQuestion1=objRequest.getParameter(GLOBALCONSTANT.Security_R_P_SecurityQuestion1);
		objSecurityAuthorizations.setSecurityQuestion1(StrSequrityQuestion1);
		String StrSequrityQuestion2=objRequest.getParameter(GLOBALCONSTANT.Security_R_P_SecurityQuestion2);
		objSecurityAuthorizations.setSecurityQuestion2(StrSequrityQuestion2);
		String StrSequrityQuestion3=objRequest.getParameter(GLOBALCONSTANT.Security_R_P_SecurityQuestion3);
		objSecurityAuthorizations.setSecurityQuestion3(StrSequrityQuestion3);
		objSecurityAuthorizations.setStatus(GLOBALCONSTANT.Security_UserTemporaryStatus);
		objSecurityAuthorizations.setLastActivityDate(LastActivityDateStringH);
		objSecurityAuthorizations.setLastActivityTeamMember(OBJCOMMONSESSIONBE.getTeam_Member());
		objSecurityAuthorizations.setPasswordDate(LastActivityDateStringH);
		
		if(!objSecurityAuthorizations.getInvalidAttemptDate().toString().equalsIgnoreCase(GLOBALCONSTANT.BlankString))
		{
			objSecurityAuthorizations.setInvalidAttemptDate(InvalidAttemptDateH);
		}
		

		
			
		objSecurityAuthorizations.setTenant(OBJCOMMONSESSIONBE.getTenant());
		try {
			ObjSecurityAuthorizationService.addOrUpdateSequrityAuthorization(objSecurityAuthorizations);
		} catch (JASCIEXCEPTION e) {
			
			logger.error(e.getMessage());
		} 
		
	return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Teammember_security_setup_maintenance_Action);
	}


	
	
/**
 * @purpose: this controller is used when and kendo grid item is selected for edit 
 * @param request
 * @return
 */

	@RequestMapping(value=GLOBALCONSTANT.Security_M_AddUpdateTeamMember, method = RequestMethod.GET)
	 public ModelAndView getUpdateTheTeamMember(HttpServletRequest request){
		HttpSession session = request.getSession();
		
	  if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
	   return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	  }
	  
	  WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Teammember_lookup_Execution);

	  if (!objWebServiceStatus.isBoolStatus()) {
	   ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	   ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
	   ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
	   return ObjModelAndView;

	  }

	 String LastActivityDateStringH=request.getParameter(GLOBALCONSTANT.LastActivityDateStringH);
	  String StrTeamMember=request.getParameter(GLOBALCONSTANT.Security_R_P_TeamMember);
	  String fullName=request.getParameter(GLOBALCONSTANT.Security_R_P_FullName);
	  String StrTenant=request.getParameter(GLOBALCONSTANT.Security_R_P_Tenant);
	 
	 

	  String StrCompany=OBJCOMMONSESSIONBE.getCompany();//"Comapny1";
	/*  String StrCommTenant=OBJCOMMONSESSIONBE.getTenant();*/
	  String StrLoginUserName=OBJCOMMONSESSIONBE.getTeam_Member();
	  String StrUserAlreadyExist=GLOBALCONSTANT.Security_StringTrue;


	 // List<Object> listObjectGeneralCodes = null;
	  List<SECURITYAUTHORIZATIONS>  listSecurityAuthorizations = new ArrayList<SECURITYAUTHORIZATIONS>();
	  SECURITYAUTHORIZATIONS SecurityAuthorizationObject=null;
	  try {
	   //listObjectGeneralCodes = ObjSecurityAuthorizationService.getGeneralCodeForSecurityQuestion(StrCommTenant,StrCompany);
	   objLabels=ObjSecurityAuthorizationService.getSecuritySetUpScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
	   if(StrTeamMember!=null && StrTeamMember.length()>GLOBALCONSTANT.INT_ZERO){

	    listSecurityAuthorizations = ObjSecurityAuthorizationService.getSecurityAuthorizationDataByTeamMember(StrTeamMember, StrTenant);
	   }

	   if(listSecurityAuthorizations.size()>GLOBALCONSTANT.INT_ZERO)
	   {
	    StrUserAlreadyExist=GLOBALCONSTANT.Security_StringTrue;
	    SecurityAuthorizationObject  = listSecurityAuthorizations.get(GLOBALCONSTANT.INT_ZERO);
	    SecurityAuthorizationObject.setTeamMember(StringEscapeUtils.escapeHtml(SecurityAuthorizationObject.getTeamMember()));
	    SecurityAuthorizationObject.setTenant(StringEscapeUtils.escapeHtml(SecurityAuthorizationObject.getTenant()));
	    try{
	    SecurityAuthorizationObject.setLastActivityTeamMember(ObjSecurityAuthorizationService.getTeamMemberName(SecurityAuthorizationObject.getLastActivityTeamMember()));
	    }catch(JASCIEXCEPTION objJasciexception)
	    {
	    	logger.error(objJasciexception.getMessage());
	    }
	   
	   }
	   else
	   {
	    StrUserAlreadyExist=GLOBALCONSTANT.Security_StringFalse;
	    SecurityAuthorizationObject = new SECURITYAUTHORIZATIONS();
	    try{
	    SecurityAuthorizationObject.setPasswordDate(StringToDate(LastActivityDateStringH));
	    }catch(Exception objException){
	    	
	    	logger.error(objException.getMessage());
	    }
	    SecurityAuthorizationObject.setCompany(StringEscapeUtils.escapeHtml(StrCompany));
	    SecurityAuthorizationObject.setTeamMember(StringEscapeUtils.escapeHtml(StrTeamMember));
	    SecurityAuthorizationObject.setTenant(StringEscapeUtils.escapeHtml(StrTenant));
	    SecurityAuthorizationObject.setLastActivityTeamMember(OBJCOMMONSESSIONBE.getTeam_Member_Name());
	//    SecurityAuthorizationObject.setLastActivityDate(getCurrentDate(GLOBALCONSTANT.yyyyMMdd_Format_v1));
	   }
	  } catch (JASCIEXCEPTION objJasciexception) {

		  logger.error(objJasciexception.getMessage());
	  //System.out.println(objJasciexception.getMessage());
	  }

	  

	  SecurityAuthorizationObject.setTenant(OBJCOMMONSESSIONBE.getTenant_NAME50());
	    
	  ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_security_setup_maintenance_Action,GLOBALCONSTANT.Security,SecurityAuthorizationObject);
	  try{
	  ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SECURITYQUESTION,GLOBALCONSTANT.BlankString)
);//Used to get the application dropdown value
	  }catch(Exception ex){
		  session.setAttribute(GLOBALCONSTANT.BACKSTATUS,null);  
	  }
	  ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewCompanyID,OBJCOMMONSESSIONBE.getCompany());//Used to get the application dropdown value
	  ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_FullName,fullName);
	  ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLoginUserName,StrLoginUserName);//Used to get the application dropdown value
	  ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewIsUserExists, StrUserAlreadyExist);
	  ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels);
	  ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	  try{
	  String bkstate=session.getAttribute(GLOBALCONSTANT.BACKSTATUS).toString();
	  ObjModelAndView.addObject(GLOBALCONSTANT.BACKSTATUS,bkstate);
	  }catch(Exception objException){
			logger.error(objException.getMessage());
	  }
	  session.setAttribute(GLOBALCONSTANT.BACKSTATUS,null);
	  //ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodesListObject, listObjectGeneralCodes);
	  return ObjModelAndView;



	  //return null;
	 }
/**
 * @pupose this controller is used when user search on the basis of part of team member 
 * @param request
 * @return
 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_selection_Action, method = RequestMethod.GET)
	public ModelAndView getTeamMemberSearchLookup(HttpServletRequest request){
		
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Teammember_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}


		String StrTenant = OBJCOMMONSESSIONBE.getTenant();//"Jasci";
		
		//get Team Member name from screen
		TeamMembername=request.getParameter(GLOBALCONSTANT.Security_R_P_TeamMemberName);
		//get Part of Team Member name from screen
		PartOfTeamMemberName=request.getParameter(GLOBALCONSTANT.Security_R_P_PartOfTeamMemberName);
		 String backSatus=request.getParameter(GLOBALCONSTANT.BACKSTATUS);
		List<TEAMMEMBERBEAN> objTeamMembers=null;
			
		SECURITYAUTHORIZATIONSCREENBE objLabels=null;

		try{
			//call service method to get Team memebr list
			objTeamMembers=  ObjSecurityAuthorizationService.getTeamMemberByName(TeamMembername,PartOfTeamMemberName,OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getFulfillmentCenter()+GLOBALCONSTANT.BlankString);
			objLabels=ObjSecurityAuthorizationService.getSecuritySetUpScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

			
		}catch(JASCIEXCEPTION objException){
			logger.error(objException.getMessage());
		}

		if(objTeamMembers.isEmpty()||objTeamMembers==null){			
			if(TeamMembername.equalsIgnoreCase(GLOBALCONSTANT.BlankString)||TeamMembername.length()==GLOBALCONSTANT.INT_ZERO)
			   {
			   ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_lookup_Action);
			   ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objLabels.getErrLookUpNotAvalidPartOfTeamMember());
			   
			   ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels);
			   }
			   else if(PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString)||PartOfTeamMemberName.length()==GLOBALCONSTANT.INT_ZERO)
			   {
			    ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_lookup_Action);
			    ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objLabels.getErrLookUpNotAvalidTeamMember()); 
			    ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels); 
			   }	
		}
		else if(PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString)||PartOfTeamMemberName.length()==GLOBALCONSTANT.INT_ZERO)
		{
			HttpSession session = request.getSession();
			ModelAndView objModelAndView =new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Security_M_AddUpdateTeamMember+GLOBALCONSTANT.Question+GLOBALCONSTANT.Security_R_P_TeamMember+GLOBALCONSTANT.Equal+objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getTeamMember()+GLOBALCONSTANT.And+GLOBALCONSTANT.Security_R_P_Tenant+GLOBALCONSTANT.Equal+StrTenant);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			session.setAttribute(GLOBALCONSTANT.BACKSTATUS, backSatus);
			
			return objModelAndView;
			//request.getContextPath().toString()+;
		}
		else if(PartOfTeamMemberName!=null && PartOfTeamMemberName.length()>GLOBALCONSTANT.INT_ZERO){
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_selection_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMembersListObject,objTeamMembers);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMembersNamePart,PartOfTeamMemberName);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_KendoReadURL,GLOBALCONSTANT.Security_M_Menu);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_lblHidden,GLOBALCONSTANT.inline);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewTenant, StrTenant);
			/*ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList,listObjectGeneralCodes);//Used to get the application dropdown value*/			
			try{
				  ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodes_GeneralCodeValueList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SECURITYQUESTION,GLOBALCONSTANT.BlankString));//Used to get the application dropdown value
			 }catch(Exception objException){
					logger.error(objException.getMessage());
			  }
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;
	}

	/**
	 * @purpose: this method is used to show the team member list with kendo grid
	 * @param request
	 * @return
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_security_setup_maintenance_Action, method = RequestMethod.GET)
	public ModelAndView getAllTeamMember(HttpServletRequest request){		

		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Teammember_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		List<TEAMMEMBERBEAN> objAllTeamMembers=null;

		String StrTenant=OBJCOMMONSESSIONBE.getTenant();//"jasci";
		String StrFullfillment=OBJCOMMONSESSIONBE.getFulfillmentCenter()+GLOBALCONSTANT.BlankString;

		try{
			//call service method to get all team member data list
			objAllTeamMembers=  ObjSecurityAuthorizationService.getAllTeamMemberData(StrTenant,StrFullfillment);
			objLabels=ObjSecurityAuthorizationService.getSecuritySetUpScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());

		}catch(JASCIEXCEPTION objJasciexception){
			logger.error(objJasciexception.getMessage());
		
			
		}
		if(objAllTeamMembers.isEmpty()||objAllTeamMembers==null){			
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_lookup_Action);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objLabels.getErrLookUpNotAvalidTeamMember());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels);
		}
		else{
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_selection_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMembersListObject,objAllTeamMembers);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_KendoReadURL, GLOBALCONSTANT.Security_M_DisplayAll);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_lblHidden,GLOBALCONSTANT.none);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewLabels,objLabels);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_viewTenant, StrTenant);

		}
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;
	}	



/**
 * 
 * @return
 */
	/// FOR REST FULL SERVICES

	@RequestMapping(value =GLOBALCONSTANT.Security_M_Menu, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERBEAN>  getList() { 
		//COMMONSESSIONBE objCommonsessionbe=LOGINSERVICEIMPL.ObjCommonSessionBe;
		List<TEAMMEMBERBEAN> teamMemberSubList = new ArrayList<TEAMMEMBERBEAN>();		
		//List<TEAMMEMBERS> ListGeneralCodes = new ArrayList<TEAMMEMBERS>();

		try {
			teamMemberSubList =  ObjSecurityAuthorizationService.getTeamMemberByName(TeamMembername,PartOfTeamMemberName,GLOBALCONSTANT.BlankString,GLOBALCONSTANT.BlankString);

	

		}catch(JASCIEXCEPTION objJasciexception){
			logger.error(objJasciexception.getMessage());
		
			
		}
		return teamMemberSubList;
	}

	@RequestMapping(value =GLOBALCONSTANT.Security_M_DisplayAll, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERBEAN>  getAllTeammembersList() { 
		List<TEAMMEMBERBEAN> teamMemberSubList = new ArrayList<TEAMMEMBERBEAN>();		
		//List<TEAMMEMBERS> ListGeneralCodes = new ArrayList<TEAMMEMBERS>();


		String StrTenant=OBJCOMMONSESSIONBE.getTenant();//"jasci";
		String StrFullfillment=OBJCOMMONSESSIONBE.getFulfillmentCenter()+GLOBALCONSTANT.Blank;

		try{
			//call service method to get all team member data list
			teamMemberSubList=  ObjSecurityAuthorizationService.getAllTeamMemberData(StrTenant,StrFullfillment);

		}catch(JASCIEXCEPTION objJasciexception){
			logger.error(objJasciexception.getMessage());
		
			
		}
		return teamMemberSubList;
	}

	
	
/**
 * 
 * @param StrInFormat
 * @return
 */
	public String getCurrentDate(String StrInFormat)
	{

		DateFormat dateFormat = new SimpleDateFormat(StrInFormat);
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());

	}
	/**
	 * 
	 * @return
	 */
	public Date getCurrentDate()
	{

		return new Date();

	}
	
/**
 * 	
 * @param dateInString
 * @param StrInFormat
 * @param StrOutFormat
 * @return
 */
	public String ConvertDate(String dateInString,String StrInFormat,String StrOutFormat)
	{
		SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
		DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
		String StrFormattedString=dateInString ;
	 
		try {
	 
			Date date = formatter.parse(dateInString);
			StrFormattedString=dateFormat.format(date);
	 
		} catch (Exception e) {
			//e.printStackTrace();
		}
		
		return StrFormattedString;
		
	}

	//It is used to change string in date
		public Date StringToDate(String ObjDate1)
		{
			
			
			SimpleDateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
			Date StrCurrentDate=null;
			try {
				StrCurrentDate = ObjDateFormat1.parse(ObjDate1);
			} catch (ParseException e) {
			
			}
			
			return StrCurrentDate;
		}
}
