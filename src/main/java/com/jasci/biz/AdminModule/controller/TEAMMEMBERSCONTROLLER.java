/*
Date Developed :Nov 30 2014
Created by: Rahul kumar
Description :TEAMMEMBERS Controller TO HANDLE THE REQUESTS FROM SCREENS
 */

package com.jasci.biz.AdminModule.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.TEAMMEMBERSSCREENBE;
import com.jasci.biz.AdminModule.model.COMPANIES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERS;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE;
import com.jasci.biz.AdminModule.service.ITEAMMEMBERSSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.TEAMMEMBERSBE;
import com.jasci.exception.JASCIEXCEPTION;




@Controller
public class TEAMMEMBERSCONTROLLER 
{

	@Autowired
	ITEAMMEMBERSSERVICE ObjTeamMembersServiceImpl;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	IMENUAPPICONSERVICE ObjIMenuAppIconService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;

	ModelAndView ObjModelAndView=new ModelAndView();
	//variable declaration for common use 
	private String StrSortingOrder=GLOBALCONSTANT.BlankString;
	private String StrSortName=GLOBALCONSTANT.BlankString;
	//private String StrTeamMember=GLOBALCONSTANT.BlankString;
	private String StrPartOfTeamMember=GLOBALCONSTANT.BlankString;
	@SuppressWarnings(GLOBALCONSTANT.Strunused)
	private String StrPartOfTeamMemberCheck=GLOBALCONSTANT.BlankString;
	@SuppressWarnings(GLOBALCONSTANT.Strunused)
	private String StrCheckMsg=GLOBALCONSTANT.BlankString;
	private String StrCheckMsgUpdate=GLOBALCONSTANT.BlankString;
	private String StrCheckMsgLookup=GLOBALCONSTANT.BlankString;
	static Logger log= LoggerFactory.getLogger(TEAMMEMBERSCONTROLLER.class);
	String StrMenuType=GLOBALCONSTANT.FullScreen,StrMenuOption=GLOBALCONSTANT.MenuOption;



	/*
	 *Description This function redirect to Team member maintenance new screen page  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 17 2014" 
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Team_member_maintenancenew_Action, method = RequestMethod.GET)
	public ModelAndView getTeamMemberMaintenanceNew( @ModelAttribute(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg)  Object StatusMSG,@ModelAttribute(GLOBALCONSTANT.TeamMemberOBJ) TEAMMEMBERSBE objectTeammembersBe,HttpServletRequest request){

		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrTenantLanguage=OBJCOMMONSESSIONBE.getTenant_Language();
		String FulfillmentCenter=String.valueOf(OBJCOMMONSESSIONBE.getFulfillmentCenter());
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		String backSatus=request.getParameter(GLOBALCONSTANT.BACKSTATUS);
		
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		List<COMPANIES> objListCompanies=null;
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;
		
		String TeamMemberName=GLOBALCONSTANT.BlankString;
		  Date ObjDate1 = new Date();
		  String StrCurrentDate=null;
		  
		  SimpleDateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format_yyyy_MM_dd);
		  try {
		   StrCurrentDate=ObjDateFormat1.format(ObjDate1);
		  } catch (Exception objException) {
		 
		   log.error(objException.getMessage());
		  }

		try {

			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
			objListCompanies=ObjTeamMembersServiceImpl.getCompanies(StrTenant);
			 TeamMemberName=ObjIMenuAppIconService.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member());
			   
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenancenew_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesListObject,objListCompanies);
			try{
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeDepartmentListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_DEPARTMENT,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeShiftListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SHIFTCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageCountryCodeListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_COUNTRYCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStatesListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STATECODE,GLOBALCONSTANT.BlankString));
			 } catch (Exception objException) {
				 
				   log.error(objException.getMessage());
				  }

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberFulfilmentCenter,FulfillmentCenter);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberTenant,StrTenant);
			ObjModelAndView.addObject(GLOBALCONSTANT.TenantLanguage,StrTenantLanguage);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_TeamMemberName,TeamMemberName);
			ObjModelAndView.addObject(GLOBALCONSTANT.SetupDate,StrCurrentDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.BACKSTATUS,backSatus);

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg,StatusMSG);	

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			StrCheckMsg=GLOBALCONSTANT.BlankString;
			return ObjModelAndView;
		} catch (JASCIEXCEPTION objDatabaseexception) {
			 log.error(objDatabaseexception.getMessage());
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action);

			return ObjModelAndView;
		}
	}


	/*
	 *Description This function redirect to Team member maintenance new screen page  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 17 2014" 
	 */

	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Team_member_Insertnew_Action, method = RequestMethod.POST)
	public ModelAndView addEntry(@ModelAttribute(GLOBALCONSTANT.TeamMembers_Register_ModelAttribute)  TEAMMEMBERSBE objectTeammembersBe,HttpServletRequest request) throws JASCIEXCEPTION  {

		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String strLastActivityBy=OBJCOMMONSESSIONBE.getTeam_Member();
		String SetupDate=request.getParameter(GLOBALCONSTANT.GetSetupdate);
		String LastActivityDate=request.getParameter(GLOBALCONSTANT.GetLastActivitydate);
		String FulfillmentCenter=String.valueOf(OBJCOMMONSESSIONBE.getFulfillmentCenter());
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){

		}



		String[] Compnaynames = request.getParameterValues(GLOBALCONSTANT.CompanyCheckBox);
		List<String> Comapnieslist =  Arrays.asList(Compnaynames); 
		//call service method to get Team memebr list
		try {
			objectTeammembersBe.setTenant(StrTenant);
			objectTeammembersBe.setFulfillmentcenterId(FulfillmentCenter);			
			objectTeammembersBe.setLastactivitydate(LastActivityDate);
			objectTeammembersBe.setSetupdate(SetupDate);
			ObjTeamMembersServiceImpl.addEntry(objectTeammembersBe,Comapnieslist,strLastActivityBy);
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_lokup);
			ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);
			ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,GLOBALCONSTANT.BlankString);
			StrPartOfTeamMember=GLOBALCONSTANT.BlankString;
			return ObjModelAndView;


		} catch (JASCIEXCEPTION objTeammemberaddexception) {
			
			log.error(objTeammemberaddexception.getMessage());

			if(objTeammemberaddexception.getMessage().equalsIgnoreCase(GLOBALCONSTANT.INFO_TEAMMEMBER_Email_AllreadyExist)){
				ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS());
				StrCheckMsg=objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS();
			}
			else{
				ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST());
				StrCheckMsg=objTeammembersscreenbe.getTeamMemberMaintenance_ERR_TEAM_MEMBER_ALREADY_EXIST();		
			}


			ObjModelAndView=new  ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenancenew_Action);

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			/*ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberOBJ,objectTeammembersBe);*/

			return ObjModelAndView;
		}


	}






	/**
	 *Description This function redirect to TeamMemberMaintenance screen page  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 17 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action, method = RequestMethod.GET)
	public ModelAndView getTeamMemberMaintenance( @ModelAttribute(GLOBALCONSTANT.TeamMemberInvalidMsg)  Object StatusMSG){

		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String FulfillmentCenter=String.valueOf(OBJCOMMONSESSIONBE.getFulfillmentCenter());
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;
		}
		try {
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}  catch (JASCIEXCEPTION objDatabaseexception) {
			
			log.error(objDatabaseexception.getMessage());

			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberFulfilmentCenter,FulfillmentCenter);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberTenant,StrTenant);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,GLOBALCONSTANT.BlankString);
			return ObjModelAndView;
		}
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberFulfilmentCenter,FulfillmentCenter);
		ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberTenant,StrTenant);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		if(!StrCheckMsgLookup.equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,StrCheckMsgLookup);	
		}else{
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,GLOBALCONSTANT.BlankString);
		}


		ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
		StrCheckMsgLookup=GLOBALCONSTANT.BlankString;
		return ObjModelAndView;
	}

	/**
	 *Description This function redirect to TeamMemberSearchlookup screen page  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 17 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Action, method = RequestMethod.GET)
	public ModelAndView getTeamMemberSearchLookup(HttpServletRequest request){
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.toString());
		}
		String TeamMembername=request.getParameter(GLOBALCONSTANT.TeamMemberName);
		String PartOfTeamMemberName=request.getParameter(GLOBALCONSTANT.PartOfTeamMemberName);
		StrPartOfTeamMember=PartOfTeamMemberName;
		String TeamMemberName=GLOBALCONSTANT.BlankString;
		String SetupTeamMemberName=GLOBALCONSTANT.BlankString;
		
		//get Part of Team Member name from screen



		List<TEAMMEMBERCOMPANIES> objListTeamMemberCompanies=null;
		List<COMPANIES> objListCompanies=null;
		if(TeamMembername==null || PartOfTeamMemberName==null){
			TeamMembername=GLOBALCONSTANT.BlankString;
			PartOfTeamMemberName=GLOBALCONSTANT.BlankString;
		}

		List<TEAMMEMBERS> objTeamMembers=null;



		try{
			//call service method to get Team memebr list
			objTeamMembers=  ObjTeamMembersServiceImpl.getList(TeamMembername,PartOfTeamMemberName,StrTenant);
			TeamMemberName=ObjIMenuAppIconService.getTeamMemberName(StrTenant, objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getLastActivityTeamMember());
			SetupTeamMemberName=ObjIMenuAppIconService.getTeamMemberName(StrTenant, objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getSetupBy());
	


		}catch(JASCIEXCEPTION objDatabaseexception){

			log.error(objDatabaseexception.getMessage());
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action);

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER());
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);

		}

		try{

			if(objTeamMembers.size()>GLOBALCONSTANT.INT_ZERO){
				objListTeamMemberCompanies=ObjTeamMembersServiceImpl.getTeamMemberCompanies(StrTenant, objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getId().getTeamMember());
			}

			objListCompanies=ObjTeamMembersServiceImpl.getCompanies(StrTenant);

			/*objGeneralcodesforDepartment= ObjTeamMembersServiceImpl.getGeneralCode(StrTenant,StrCompany, GLOBALCONSTANT.DEPARTMENT);
			objGeneralcodesforShift= ObjTeamMembersServiceImpl.getGeneralCode(StrTenant,StrCompany,GLOBALCONSTANT.SHIFT);
			objGeneralcodesLaguage= ObjTeamMembersServiceImpl.getGeneralCode(StrTenant,StrCompany,GLOBALCONSTANT.LANGUAGES);
			objGeneralcodesCountryCode= ObjTeamMembersServiceImpl.getGeneralCode(StrTenant,StrCompany,GLOBALCONSTANT.GIC_COUNTRYCODE);
			objGeneralcodesStates= ObjTeamMembersServiceImpl.getGeneralCode(StrTenant,StrCompany,GLOBALCONSTANT.GIC_STATES);
*/


		}catch(JASCIEXCEPTION e){

			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER());
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
		}





		if(objTeamMembers.size()==GLOBALCONSTANT.INT_ZERO || objTeamMembers.isEmpty()||objTeamMembers==null){

			//	ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Page);

			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action);

			if(!PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && TeamMembername.equalsIgnoreCase(GLOBALCONSTANT.BlankString)){

				ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME());
				StrCheckMsgLookup=objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_PART_OF_TEAM_MEMBER_NAME();
			}
			else{
				ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER());
				StrCheckMsgLookup=objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER();
			}
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
		}
		else if(!PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && objTeamMembers.size()>GLOBALCONSTANT.INT_ZERO){


			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);	
			StrSortingOrder=GLOBALCONSTANT.LastName;
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMembersListObject,objTeamMembers);
			ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,StrPartOfTeamMember);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);

		}

		else if(PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && !TeamMembername.equalsIgnoreCase(GLOBALCONSTANT.BlankString)){

			//call when Team member not blank redirect to edit page with thier info

			
			TEAMMEMBERS ObjectTeammembers=objTeamMembers.get(GLOBALCONSTANT.INT_ZERO);
			ObjectTeammembers.setFirstName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getFirstName()));
			ObjectTeammembers.setLastName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getLastName())); //escap html text to plain text
			ObjectTeammembers.setMiddleName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getMiddleName()));
			ObjectTeammembers.setAddressLine1(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine1()));
			ObjectTeammembers.setAddressLine2(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine2()));
			ObjectTeammembers.setAddressLine3(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine3()));
			ObjectTeammembers.setAddressLine4(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine4()));
			ObjectTeammembers.setCity(StringEscapeUtils.escapeHtml(ObjectTeammembers.getCity()));
			ObjectTeammembers.setZipCode(StringEscapeUtils.escapeHtml(ObjectTeammembers.getZipCode()));

			String SetupDate=ConvertDate(ObjectTeammembers.getSetupDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String LastActivityDate=ConvertDate(ObjectTeammembers.getLastActivityDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String StartDate=ConvertDate(ObjectTeammembers.getStartDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String LastDate=ConvertDate(ObjectTeammembers.getLastDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);


			ObjectTeammembers.setEmergencyContactName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getEmergencyContactName()));


			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenanceUpdate_Page,GLOBALCONSTANT.TeamMember_Update_Object,ObjectTeammembers);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesListObject,objListCompanies);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesTeamMemberListObject,objListTeamMemberCompanies);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			try{
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeDepartmentListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_DEPARTMENT,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeShiftListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SHIFTCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageCountryCodeListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_COUNTRYCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStatesListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STATECODE,GLOBALCONSTANT.BlankString));
			 } catch (Exception objException) {
				 
				   log.error(objException.getMessage());
				  }

			
			
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberIdValue,StringEscapeUtils.escapeHtml(objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getId().getTeamMember()));
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.SetupDateObj,SetupDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.LastActivityDateObj,LastActivityDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.StartDateObj,StartDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.LastDateObj,LastDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_TeamMemberName,TeamMemberName);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_SetupTeamMemberName,SetupTeamMemberName);



		}
		else if(!PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && !TeamMembername.equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
			//call when Team member not blank and part of team members also not blank redirect to edit page with thier info
			TEAMMEMBERS ObjectTeammembers=objTeamMembers.get(GLOBALCONSTANT.INT_ZERO);
			ObjectTeammembers.setFirstName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getFirstName()));
			ObjectTeammembers.setLastName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getLastName())); //escap html text to plain text
			ObjectTeammembers.setMiddleName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getMiddleName()));
			ObjectTeammembers.setAddressLine1(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine1()));
			ObjectTeammembers.setAddressLine2(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine2()));
			ObjectTeammembers.setAddressLine3(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine3()));
			ObjectTeammembers.setAddressLine4(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine4()));
			ObjectTeammembers.setCity(StringEscapeUtils.escapeHtml(ObjectTeammembers.getCity()));
			ObjectTeammembers.setZipCode(StringEscapeUtils.escapeHtml(ObjectTeammembers.getZipCode()));

			String SetupDate=ConvertDate(ObjectTeammembers.getSetupDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String LastActivityDate=ConvertDate(ObjectTeammembers.getLastActivityDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String StartDate=ConvertDate(ObjectTeammembers.getStartDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String LastDate=ConvertDate(ObjectTeammembers.getLastDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);

			ObjectTeammembers.setEmergencyContactName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getEmergencyContactName()));

			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenanceUpdate_Page,GLOBALCONSTANT.TeamMember_Update_Object,ObjectTeammembers);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesListObject,objListCompanies);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesTeamMemberListObject,objListTeamMemberCompanies);
			
			try{
				ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeDepartmentListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_DEPARTMENT,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeShiftListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SHIFTCODE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageCountryCodeListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_COUNTRYCODE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStatesListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STATECODE,GLOBALCONSTANT.BlankString));
				 } catch (Exception objException) {
					 
					   log.error(objException.getMessage());
					  }
			
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberIdValue,StringEscapeUtils.escapeHtml(objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getId().getTeamMember()));
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.SetupDateObj,SetupDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.LastActivityDateObj,LastActivityDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.StartDateObj,StartDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.LastDateObj,LastDate);



			//ObjModelAndView.addObject(GLOBALCONSTANT.TeamMember_Update_Object,objTeamMembers.get(0));
		}



		else{
			//redirect to Team memeber search look up screen on calling display all
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);	
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			StrSortingOrder=GLOBALCONSTANT.LastName;
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMembersListObject,objTeamMembers);
			ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,StrPartOfTeamMember);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
		}

		return ObjModelAndView;


	}





	/**
	 *Description This function  redirect to TeamMemberSearchlookup screen page  after delete team member
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 19 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_Delete_searchlookup_Action, method = RequestMethod.POST)
	//@RequestMapping(GLOBALCONSTANT.RequestMapping_Teammember_Delete_searchlookup_Action)
	public ModelAndView deleteEntry(HttpServletRequest request){
		//call service method to get Team memebr list
		//@PathVariable String Tenant,@PathVariable String TeamMember,@PathVariable String FullFillmentCenter,@PathVariable String SortValue



		String Tenant=request.getParameter(GLOBALCONSTANT.StrTenantF);
		String TeamMember=request.getParameter(GLOBALCONSTANT.StrTeamMember);
		String SortValue=request.getParameter(GLOBALCONSTANT.StrsortValue);

		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.toString());
		}
		try {
			ObjTeamMembersServiceImpl.deleteEntry(Tenant,TeamMember,StrCompany);

			//redirect to Team memeber search look up screen
			if(SortValue.equalsIgnoreCase(GLOBALCONSTANT.LastName)){
				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_lokup);

				ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);
				ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,StrPartOfTeamMember);

			}

			if(SortValue.equalsIgnoreCase(GLOBALCONSTANT.FirstName)){
				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_lokup);
				ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);
				ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,StrPartOfTeamMember);
			}

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			return ObjModelAndView;

		} catch (JASCIEXCEPTION objTeammemberdeleteexception) {
			
			log.error(objTeammemberdeleteexception.getMessage());
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Action);
			return ObjModelAndView;

		}


	}


	/**
	 *Description This function  redirect to Team member maintenance new screen page for update Team member  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 19 2014" 
	 */
	///Team_member_maintenance_update/{teamMember}/{SortValue}"
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Team_member_maintenanceUpdate_Action, method = RequestMethod.POST)
	public ModelAndView updateEntry(@ModelAttribute(GLOBALCONSTANT.TeamMember_Update_Object)  TEAMMEMBERSBE objectTeammembersBe,HttpServletRequest request,HttpServletResponse response){

		StrSortName =request.getParameter(GLOBALCONSTANT.SortName);


		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();
		String strLastActivityBy=OBJCOMMONSESSIONBE.getTeam_Member();
		String FulfillmentCenter=String.valueOf(OBJCOMMONSESSIONBE.getFulfillmentCenter());
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String SetupDate=request.getParameter(GLOBALCONSTANT.GetSetupdate);
		String LastActivityDate=request.getParameter(GLOBALCONSTANT.GetLastActivitydate);
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;

		String[] names = request.getParameterValues(GLOBALCONSTANT.CompanyCheckBox);
		List<String> Comapnieslist =  Arrays.asList(names); 
		String TeamMemberoldId=request.getParameter(GLOBALCONSTANT.TeamMemberIdValue);
		

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){

		}


		try {
			
			objectTeammembersBe.setTenant(StrTenant);
			objectTeammembersBe.setFulfillmentcenterId(FulfillmentCenter);
			objectTeammembersBe.setLastactivitydate(LastActivityDate);
			objectTeammembersBe.setSetupdate(SetupDate);
			
			objectTeammembersBe.setTeammember(TeamMemberoldId);
			//objectTeammembersBe.setAuthorityprofile(OBJCOMMONSESSIONBE.getAuthority_Profile());
			ObjTeamMembersServiceImpl.updateEntry(objectTeammembersBe, Comapnieslist,TeamMemberoldId,StrCompany,strLastActivityBy);

			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_lokup);
			ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);
			ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,GLOBALCONSTANT.BlankString);
			StrPartOfTeamMember=GLOBALCONSTANT.BlankString;
			//ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Teammember_AfterUpdate_Action);
			return ObjModelAndView;


		} catch (JASCIEXCEPTION objTeammemberupdateexception) {
			
			log.error(objTeammemberupdateexception.getMessage());
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Teammember_AfterNotUpdate_Action);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberNameFromUpdate,TeamMemberoldId);
			StrCheckMsgUpdate=objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_EMAIL_ADDRESS();
			return ObjModelAndView;


		}

	}


	/**
	 *Description This function redirect to team member maintenance after  update Team member  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 23 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_AfterUpdate_Action, method = RequestMethod.GET)
	public ModelAndView redirectAfterUpdate(HttpServletRequest request){
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
	
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.toString());
		}
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenanceUpdate_Page);	
		if(GLOBALCONSTANT.FirstName.equalsIgnoreCase(StrSortName) || GLOBALCONSTANT.LastName.equalsIgnoreCase(StrSortName)){

			ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		StrPartOfTeamMember=GLOBALCONSTANT.BlankString;
		ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ON_UPDATE_SUCCESSFULLY());
		ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
		return ObjModelAndView;
	}




	/**
	 *Description This function design to redirect  to team member maintenance after not update Team member  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 23 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_AfterNotUpdate_Action, method = RequestMethod.GET)
	public ModelAndView redirectAfterNotUpdate( @ModelAttribute(GLOBALCONSTANT.TeamMemberNameFromUpdate) final Object TeamMemberfromUpdate,HttpServletRequest request){
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		//get Team Member name from screen
		String TeamMembername=TeamMemberfromUpdate.toString();
		String PartOfTeamMemberName=GLOBALCONSTANT.BlankString;
		//get Part of Team Member name from screen
		List<TEAMMEMBERCOMPANIES> objListTeamMemberCompanies=null;
		List<COMPANIES> objListCompanies=null;

		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;


		List<TEAMMEMBERS> objTeamMembers=null;

		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.toString());
		}
		try{
			//call service method to get Team memebr list
			objTeamMembers=  ObjTeamMembersServiceImpl.getList(TeamMembername,PartOfTeamMemberName,StrTenant);

			if(!objTeamMembers.isEmpty()||objTeamMembers!=null){
				objListTeamMemberCompanies=ObjTeamMembersServiceImpl.getTeamMemberCompanies(StrTenant, objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getId().getTeamMember());
			}

			//call when Team member not blank and part of team members also not blank redirect to edit page with thier info
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenanceUpdate_Page,GLOBALCONSTANT.TeamMember_Update_Object,objTeamMembers.get(GLOBALCONSTANT.INT_ZERO));
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesListObject,objListCompanies);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesTeamMemberListObject,objListTeamMemberCompanies);
		
			try{
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeDepartmentListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_DEPARTMENT,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeShiftListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SHIFTCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageCountryCodeListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_COUNTRYCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStatesListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STATECODE,GLOBALCONSTANT.BlankString));
			} catch (Exception objException) {
				   log.error(objException.getMessage());
				  }
		
			
			
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberIdValue,objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getId().getTeamMember());
			ObjModelAndView.addObject(GLOBALCONSTANT.SortName,StrSortingOrder);
			if(!StrCheckMsgUpdate.equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
				ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg,StrCheckMsgUpdate);
			}
			else{
				ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg,GLOBALCONSTANT.BlankString);
			}

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			StrCheckMsgUpdate=GLOBALCONSTANT.BlankString;

			return ObjModelAndView;

		}catch(JASCIEXCEPTION objDatabaseexception){

			log.error(objDatabaseexception.getMessage());
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidTeamMemberIdMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER());
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			return ObjModelAndView;

		}


	}





	/**
	 *Description This function  redirect to TeamMemberSearchlookup screen page  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 17 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_UpdateTeamMember_searchlookup_Action, method = RequestMethod.GET)
	public ModelAndView updateTeamMemberSearchLookup(HttpServletRequest request){

		//from grid edit button
		//get Team Member name from screen

		String TeamMember=request.getParameter(GLOBALCONSTANT.StrTeamMember);
		String SortValue=request.getParameter(GLOBALCONSTANT.StrLastName);
		String TeamMembername=TeamMember;
		String PartOfTeamMemberName=GLOBALCONSTANT.BlankString;
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String TeamMemberName=GLOBALCONSTANT.BlankString;
		String SetupTeamMemberName=GLOBALCONSTANT.BlankString;
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.toString());
		}
		List<TEAMMEMBERCOMPANIES> objListTeamMemberCompanies=null;
		List<COMPANIES> objListCompanies=null;

		if(TeamMembername==null || PartOfTeamMemberName==null){
			TeamMembername=GLOBALCONSTANT.BlankString;
			PartOfTeamMemberName=GLOBALCONSTANT.BlankString;
		}

		List<TEAMMEMBERS> objTeamMembers=null;


		try{
			//call service method to get Team memebr list
			objTeamMembers=  ObjTeamMembersServiceImpl.getList(TeamMembername,PartOfTeamMemberName,StrTenant);

			
			
				TeamMemberName=ObjIMenuAppIconService.getTeamMemberName(StrTenant, objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getLastActivityTeamMember());
				SetupTeamMemberName=ObjIMenuAppIconService.getTeamMemberName(StrTenant, objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getSetupBy());
			
			if(objTeamMembers.size()>GLOBALCONSTANT.INT_ZERO){
				objListTeamMemberCompanies=ObjTeamMembersServiceImpl.getTeamMemberCompanies(StrTenant, objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getId().getTeamMember());
			}
			objListCompanies=ObjTeamMembersServiceImpl.getCompanies(StrTenant);
			TEAMMEMBERS ObjectTeammembers=objTeamMembers.get(GLOBALCONSTANT.INT_ZERO);
			ObjectTeammembers.setFirstName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getFirstName()));
			ObjectTeammembers.setLastName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getLastName())); //escap html text to plain text
			ObjectTeammembers.setMiddleName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getMiddleName()));
			ObjectTeammembers.setAddressLine1(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine1()));
			ObjectTeammembers.setAddressLine2(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine2()));
			ObjectTeammembers.setAddressLine3(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine3()));
			ObjectTeammembers.setAddressLine4(StringEscapeUtils.escapeHtml(ObjectTeammembers.getAddressLine4()));
			ObjectTeammembers.setCity(StringEscapeUtils.escapeHtml(ObjectTeammembers.getCity()));
			ObjectTeammembers.setZipCode(StringEscapeUtils.escapeHtml(ObjectTeammembers.getZipCode()));

			String SetupDate=ConvertDate(ObjectTeammembers.getSetupDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String LastActivityDate=ConvertDate(ObjectTeammembers.getLastActivityDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String StartDate=ConvertDate(ObjectTeammembers.getStartDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			String LastDate=ConvertDate(ObjectTeammembers.getLastDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);


			ObjectTeammembers.setEmergencyContactName(StringEscapeUtils.escapeHtml(ObjectTeammembers.getEmergencyContactName()));

			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenanceUpdate_Page,GLOBALCONSTANT.TeamMember_Update_Object,ObjectTeammembers);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesListObject,objListCompanies);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompaniesTeamMemberListObject,objListTeamMemberCompanies);
			
			try{
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeDepartmentListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_DEPARTMENT,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeShiftListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_SHIFTCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLanguageCountryCodeListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_COUNTRYCODE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStatesListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STATECODE,GLOBALCONSTANT.BlankString));
			} catch (Exception objException) {
				  
				   log.error(objException.getMessage());
				  }
		
			
			
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberIdValue,StringEscapeUtils.escapeHtml(objTeamMembers.get(GLOBALCONSTANT.INT_ZERO).getId().getTeamMember()));
			ObjModelAndView.addObject(GLOBALCONSTANT.SortName,SortValue);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.SetupDateObj,SetupDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.LastActivityDateObj,LastActivityDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.StartDateObj,StartDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.LastDateObj,LastDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_TeamMemberName,TeamMemberName);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_SetupTeamMemberName,SetupTeamMemberName);



		}catch(JASCIEXCEPTION objDatabaseexception){

			log.error(objDatabaseexception.getMessage());
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberInvalidMsg,objTeammembersscreenbe.getTeamMemberMaintenance_ERR_INVALID_TEAM_MEMBER());
			return ObjModelAndView;

		}


		return ObjModelAndView;

	}





	/**
	 *Description This function use to sort searchlookup data based on first and last name  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 24 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Teammember_Sort_searchlookup_Action, method = RequestMethod.GET)
	public ModelAndView sortData(@PathVariable String SortValue,HttpServletRequest request){
		String TeamMembername=GLOBALCONSTANT.BlankString;
		String PartOfTeamMemberName=GLOBALCONSTANT.BlankString;
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.toString());
		}

		List<TEAMMEMBERS> objTeamMembers=null;


		try{
			//call service method to get Team memebr list
			objTeamMembers=  ObjTeamMembersServiceImpl.getList(TeamMembername,PartOfTeamMemberName,StrTenant);

			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMembersListObject,objTeamMembers);

			if(SortValue.equalsIgnoreCase(GLOBALCONSTANT.FirstName)){
				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Page);
				ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);
				ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,StrPartOfTeamMember);

			}

			if(SortValue.equalsIgnoreCase(GLOBALCONSTANT.LastName)){
				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Page);
				ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.FirstName);
				ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,StrPartOfTeamMember);
			}
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
			return ObjModelAndView;

		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.getMessage());
			if(SortValue.equalsIgnoreCase(GLOBALCONSTANT.FirstName)){
				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Page);
				ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);	
				ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,PartOfTeamMemberName);
			}

			if(SortValue.equalsIgnoreCase(GLOBALCONSTANT.LastName)){
				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Page);
				ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);	
				ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,PartOfTeamMemberName);
			}
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);

			return ObjModelAndView;

		}



	}



	//read data using Kendo ui GeneralCode
	@RequestMapping(value = GLOBALCONSTANT.TeamMember_Kendo_TeamMemberRead, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERSBE>  getList(HttpServletRequest request) {	
		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		List<TEAMMEMBERSBE> objTeamMembersBe=null;
		//call service method to get Team memebr list
		try {
			objTeamMembersBe=  ObjTeamMembersServiceImpl.getTeamMemberList(GLOBALCONSTANT.LastName,StrPartOfTeamMember,StrTenant);

		}  catch (JASCIEXCEPTION objDatabaseexception) {
			

			log.error(objDatabaseexception.getMessage());


		}

		return objTeamMembersBe;

	}




	//read data using Kendo ui GeneralCode
	@RequestMapping(value = GLOBALCONSTANT.TeamMember_Kendo_TeamMemberRead_Sort, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERSBE>  getSortList() {	



		List<TEAMMEMBERSBE> objTeamMembersBe=null;


		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		//call service method to get Team memebr list
		try {
			objTeamMembersBe=  ObjTeamMembersServiceImpl.getTeamMemberList(GLOBALCONSTANT.FirstName,StrPartOfTeamMember,StrTenant);
		} catch (JASCIEXCEPTION objDatabaseexception) {
			
			log.error(objDatabaseexception.getMessage());

		}


		return objTeamMembersBe;

	}


	/**
	 *Description This function use to show teammemeber list to lookup screen 
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Nov 27 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_lokup, method = RequestMethod.GET)
	public ModelAndView showList(){

		//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Team_member_maintenance_Action_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		TEAMMEMBERSSCREENBE objTeammembersscreenbe=null;
		try{
			objTeammembersscreenbe=	ObjTeamMembersServiceImpl.setScreenLanguage(GLOBALCONSTANT.Param_Languages_TeamMemberMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objDatabaseexception){
			log.error(objDatabaseexception.toString());
		}
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Teammember_searchlookup_Page);
		//ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.AcityvityDate);	
		StrPartOfTeamMemberCheck=GLOBALCONSTANT.BlankString;

		ObjModelAndView.addObject(GLOBALCONSTANT.SortName,GLOBALCONSTANT.LastName);	
		ObjModelAndView.addObject(GLOBALCONSTANT.PartOfTeamMemberObj,StrPartOfTeamMember);
		ObjModelAndView.addObject(GLOBALCONSTANT.TeamMemberSceernLabelObj,objTeammembersscreenbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;


	}

	public String ConvertDate(Date dateInString,String StrInFormat,String StrOutFormat)
	{
		
		DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		String StrFormattedString=GLOBALCONSTANT.BlankString ;
		
		try {

			Date date = dateInString;
			StrFormattedString=dateFormat.format(date);
			

		} catch (Exception e) {
			
		}

		return StrFormattedString;

	}



}


