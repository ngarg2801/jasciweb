/**

Description : Make a controller for Smart Task Configurator Screen form which we can mapped Configurator jsp pages and pass the value.
Created By : Shailendra Rajput
Created Date : May 14 2015
 */
package com.jasci.biz.AdminModule.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.ServicesApi.SMARTTASKCONFIGURATORSERVICEAPI;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERSBEAN;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ISMARTTASKCONFIGURATORSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class SMARTTASKCONFIGURATORCONTROLLER {

	
	
	/** Make a common session object for get the value of tenant,company,team member etc.*/
	@Autowired
	private COMMONSESSIONBE OBJCOMMONSESSIONBE;

	/** Make the instance of SCREENACCESSSERVICE for provide the authentication of particular screen for particular user*/
	@Autowired
	private SCREENACCESSSERVICE screenAccessService;
	
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	private LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	/** Make the instance of ISMARTTASKEXECUTIONSSERVICE for calling service method*/
	@Autowired
	private ISMARTTASKCONFIGURATORSERVICE ObjSmartTaskConfiguratorService;
	
	
	@Autowired
	private SMARTTASKCONFIGURATORSERVICEAPI ObjSmartTaskConfiguratorServiceApi;
	
	
	/** It is used to logger the exception*/
	private static Logger log = LoggerFactory.getLogger(SMARTTASKCONFIGURATORCONTROLLER.class.getName());
	
	
	/**create modelandview instance*/
	private ModelAndView ObjModelAndView=new ModelAndView();
	
	
	  /**This is used to get the value of search page*/

	  private  String StrApplication_Id;
	  private  String StrTask;
	  private  String StrExecution_Sequence_Group;
	  private  String StrExecution_Type;
	  private  String StrExecution_Device;
	  private  String StrExecution_Sequence_Name;
	  private  String StrDescription20;
	  private  String StrTenant_Id;
	  private  String StrMenu_Name;
	  
	 /**
	 * @author Shailendra Rajput
	 * @Date May 14, 2015
	 * @Description This Function is used to mapped the Smart Task Configurator lookup  page
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskConfigurator_Search , method = RequestMethod.GET)
public ModelAndView getSearch(@ModelAttribute(GLOBALCONSTANT.SmartTaskConfigurator_ModelAttribute) SMARTTASKSEQHEADERSBEAN ObjectSmartTaskSeqHeadersBean) {	 

	/**This is used to get the value from COMMONSESSION*/
	
	String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
	String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
	
	/** Here we Check user is logged in or not*/
	
	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	/** Here we Check user has Authentication for particular screen or not*/
	
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}

	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_SmartTaskConfigurator_Search);
	try{
		/**Call util.LOOKUPGENERALCODE.Drop_Down_Selection() function to call the general code descriptions.*/
		
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectApplication,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectExecutionType,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXETYPE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectExecutionDevice,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEDEVICE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectExecutionGroup,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEGRP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectTask,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.ScreenLabels, ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));

	}catch (JASCIEXCEPTION ObjectJasciException) {	
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
	}
	return ObjModelAndView;
}

/**
 * @author Shailendra Rajput
 * @Date May 18, 2015
 * @Description this is used to redirect the page of Smart Task Configurator Search Lookup
 * @return ModelAndView
 * @throws JASCIEXCEPTION
 */
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskConfigurator_SearchLookup , method = RequestMethod.GET)
public ModelAndView getLocationSearchLookup(HttpServletRequest request) {	 

	/** Here we Check user is logged in or not*/
	
	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	
	/** Here we Check user has Authentication for particular screen or not*/
	
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	
	try {
		 
		  /** It is used to get the value from jsp page using request parameter*/
		
		  StrApplication_Id=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Application_Id_JspPageValue);
		  StrTask=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Task_JspPageValue);
		  StrExecution_Sequence_Group=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Group_JspPageValue);
		  StrExecution_Type=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Execution_Type_JspPageValue);
		  StrExecution_Device=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Execution_Device_JspPageValue);
		  StrExecution_Sequence_Name=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue);
		  StrDescription20=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Description20_JspPageValue);
		  StrTenant_Id=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Tenant_Id_JspPageValue);
		  StrMenu_Name=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Menu_Name_JspPageValue);
		
	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Smart_Task_Configurator_Search_Lookup);
	
	/** Add the object of OBJCOMMONSESSION into jsp page*/
	ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

	/** Add the Screen Label*/
	ObjModelAndView.addObject(GLOBALCONSTANT.ScreenLabels, ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
	} catch (JASCIEXCEPTION ObjectJasciException) {
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
		
	}
	return ObjModelAndView;
}

/**
 * @author Shailendra Rajput
 * @Date May 18, 2015
 * Description this is used to Show the page of SmartTaskConfigurator Search Lookup
 * @return ModelAndView
 * @throws JASCIEXCEPTION
 */
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Smart_Task_Configurator_Search_Lookup , method = RequestMethod.GET)
public ModelAndView getLocationSearchLookup() {	 

	/** Here we Check user is logged in or not*/
	
	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	
	/** Here we Check user has Authentication for particular screen or not*/
	
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	
	try {
		
	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_SmartTaskConfigurator_SearchLookup);	
	
	/**Use to Add Tenant value on Jsp page using OBJCOMMONSESSION*/
	ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
	
	/** Add the object of OBJCOMMONSESSION into jsp page*/
	ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	
	/** Add the Screen Label*/
	ObjModelAndView.addObject(GLOBALCONSTANT.ScreenLabels, ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
	} catch (JASCIEXCEPTION ObjectJasciException) {
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
	}
	return ObjModelAndView;
}


/**
 * 
 * @author Shailendra Rajput
 * @Date May 18, 2015
 * @Description:It is used to show the data from SMART_TASK_SEQ_HEADERS table behalf of search conditions or display All
 * @return List<SMARTTASKEXECUTIONSBEAN>
 */

@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskConfigurator_Kendo_DisplayAll, method = RequestMethod.GET)
public @ResponseBody List<SMARTTASKSEQHEADERSBEAN> getDisplaykendoGrid()  throws JASCIEXCEPTION {											
	
	/** Create List of SMARTTASKSEQHEADERSBEAN*/
	List<SMARTTASKSEQHEADERSBEAN> ListSmartTaskConfigurator = new ArrayList<SMARTTASKSEQHEADERSBEAN>();			 
	try {
		/** Check the condition for display all functionality*/
		if(( StrTenant_Id == null ||  StrTenant_Id.isEmpty() ) && (StrTask == null|| StrTask.isEmpty()) && (StrApplication_Id == null || StrApplication_Id.isEmpty() )  && ( StrExecution_Sequence_Group== null || StrExecution_Sequence_Group.isEmpty()) && (StrExecution_Type == null || StrExecution_Type.isEmpty() ) && ( StrExecution_Device==null || StrExecution_Device.isEmpty())  && (StrExecution_Sequence_Name==null || StrExecution_Sequence_Name.isEmpty()) && (StrDescription20==null || StrDescription20.isEmpty()) && (StrMenu_Name==null || StrMenu_Name.isEmpty() )){
			   ListSmartTaskConfigurator=ObjSmartTaskConfiguratorService.getDisplayAll();
			  }
		else{
			/** Check the search condition*/
			ListSmartTaskConfigurator=ObjSmartTaskConfiguratorService.getList(StrTenant_Id,StrTask,StrApplication_Id,StrExecution_Sequence_Group,StrExecution_Type,StrExecution_Device,StrExecution_Sequence_Name,StrDescription20,StrMenu_Name);
		}
		} catch (JASCIEXCEPTION ObjectJasciException) {
		log.error(ObjectJasciException.getMessage());
		return ListSmartTaskConfigurator;
	}													
	return ListSmartTaskConfigurator;
}


/**
 * @author Shailendra Rajput
 * @Date May 19, 2015
 * @Description this is used to delete the selected Record from SMART_TASK_SEQUENCE_HEADERS,SMART_TASK_SEQUENCE_INSTRUCTIONS, MENU_OPTIONS and MENU_PROFILE table
 * @param request
 * @return ModelAndView
 */
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskConfigurator_Delete , method = RequestMethod.POST)
public ModelAndView deleteEntry(HttpServletRequest request) {	 
	
	/** Here we Check user is logged in or not*/
	
	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){

		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	
	/** Here we Check user has Authentication for particular screen or not*/
	
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	
	try{
		
		  /** It is used to get the value from jsp page using request parameter*/
		
		  String StrExecution_Sequence_Name_d=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue);
		  String StrTenant_Id_d=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Tenant_Id_JspPageValue);
		  String StrMenu_Name_d=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Menu_Name_JspPageValue);
		  String StrCompany_Id_d=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Company_Id_JspPageValue);
		    

		  //* This function is used to call service function in which we delete record after Validation check.**//
		   
		   ObjSmartTaskConfiguratorService.deleteEntry(StrTenant_Id_d,StrCompany_Id_d,StrExecution_Sequence_Name_d,StrMenu_Name_d);
		   ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Smart_Task_Configurator_Search_Lookup);			
		return ObjModelAndView;
	}
	catch (JASCIEXCEPTION ObjectJasciException) {
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
	}
}

/**
* @author Shailendra Rajput
* @Date June 05, 2015
* @Description This Function is used to add and configure Smart Task Configurator execution sequence
* @return ModelAndView
* @throws JASCIEXCEPTION
*/
@RequestMapping(value = GLOBALCONSTANT.StrRequestMapping_SmartTaskConfigurator_AddEntry , method = RequestMethod.GET)
public ModelAndView addEntry(@ModelAttribute(GLOBALCONSTANT.SmartTaskConfigurator_ModelAttribute) SMARTTASKSEQHEADERSBEAN ObjectSmartTaskSeqHeadersBean,BindingResult objBing,HttpServletRequest request) {	 

/**This is used to get the value from COMMONSESSION*/

String StrPageName=GLOBALCONSTANT.BlankString;
StrPageName=request.getParameter(GLOBALCONSTANT.PageName);
if(StrPageName==null){
	StrPageName=GLOBALCONSTANT.BlankString;
}
/** Here we Check user is logged in or not*/

if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
	return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
}
/** Here we Check user has Authentication for particular screen or not*/

WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
		GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);

if (!objWebServiceStatus.isBoolStatus()) {
	ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
	return ObjModelAndView;

}

ObjModelAndView=new ModelAndView(GLOBALCONSTANT.StrRequestMapping_SmartTaskConfigurator_Maintenance);
try{
	/**Call util.LOOKUPGENERALCODE.Drop_Down_Selection() function to call the general code descriptions.*/
	
	ObjSmartTaskConfiguratorService.setPageName(StrPageName);
	ObjSmartTaskConfiguratorService.fetchEntry(GLOBALCONSTANT.BlankString, GLOBALCONSTANT.BlankString, GLOBALCONSTANT.BlankString,GLOBALCONSTANT.BlankString);
	ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.ScreenLabels, ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));

}catch (JASCIEXCEPTION ObjectJasciException) {	
	log.error(ObjectJasciException.getMessage());
	return ObjModelAndView;
}
return ObjModelAndView;
}

/**
* @author Shailendra Rajput
* @Date June 09, 2015
* @Description This Function is used to open Configurator screen inside iframe
* @return ModelAndView
* @throws JASCIEXCEPTION
*/
@RequestMapping(value = GLOBALCONSTANT.Configurator , method = RequestMethod.GET)
public ModelAndView Sequence() {  
	
	/** Here we Check user is logged in or not*/

	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	/** Here we Check user has Authentication for particular screen or not*/

	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);

	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	/**This is used to get the value from COMMONSESSION*/

	String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
	String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
 
 ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RedirectConfiguratorHtmlPage);   
 
 try{
		/**Call util.LOOKUPGENERALCODE.Drop_Down_Selection() function to call the general code descriptions.*/
		
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectApplication,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectExecutionType,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXETYPE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectExecutionDevice,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEDEVICE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectExecutionGroup,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEGRP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectTask,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.ScreenLabels, ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));

	}catch (JASCIEXCEPTION ObjectJasciException) {	
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
	} return ObjModelAndView;
}


/**
* @author Shailendra Rajput
* @Date July 02, 2015
* @Description This Function is used to get the parameter when we click on edit
* @return ModelAndView
* @throws JASCIEXCEPTION
*/
@RequestMapping(value = GLOBALCONSTANT.StrRequestMapping_SmartTaskConfigurator_EditEntry , method = RequestMethod.GET)
public ModelAndView getEntry(HttpServletRequest request) {	 

/** Here we Check user is logged in or not*/

if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
	return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
}
/** Here we Check user has Authentication for particular screen or not*/

WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
		GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);

if (!objWebServiceStatus.isBoolStatus()) {
	ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
	return ObjModelAndView;

}

ObjModelAndView=new ModelAndView(GLOBALCONSTANT.StrRequestMapping_SmartTaskConfigurator_Maintenance);
try{
	String StrTenant_Id_E=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Tenant_Id_JspPageValue);
	 String StrCompany_Id_E=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Company_Id_JspPageValue);
	 String StrExecution_Sequence_Name_E=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue);
	 /**Call util.LOOKUPGENERALCODE.Drop_Down_Selection() function to call the general code descriptions.*/
	 
	 
	 ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	 String StrPageName=GLOBALCONSTANT.BlankString;
	 ObjSmartTaskConfiguratorService.setPageName(StrPageName);
	 ObjSmartTaskConfiguratorService.fetchEntry(StrTenant_Id_E, StrCompany_Id_E, StrExecution_Sequence_Name_E,GLOBALCONSTANT.Edit);
	 ObjModelAndView.addObject(GLOBALCONSTANT.ScreenLabels, ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));

}catch (JASCIEXCEPTION ObjectJasciException) {	
	log.error(ObjectJasciException.getMessage());
	return ObjModelAndView;
}
return ObjModelAndView;
}

/**
* @author Shailendra Rajput
* @Date July 17, 2015
* @Description This Function is used to get the parameter when we click on Copy
* @return ModelAndView
* @throws JASCIEXCEPTION
*/
@RequestMapping(value = GLOBALCONSTANT.StrRequestMapping_SmartTaskConfigurator_CopyEntry , method = RequestMethod.GET)
public ModelAndView copyEntry(HttpServletRequest request) {	 

/** Here we Check user is logged in or not*/

if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
	return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
}
/** Here we Check user has Authentication for particular screen or not*/

WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
		GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);

if (!objWebServiceStatus.isBoolStatus()) {
	ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
	ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
	return ObjModelAndView;

}

ObjModelAndView=new ModelAndView(GLOBALCONSTANT.StrRequestMapping_SmartTaskConfigurator_Maintenance);
try{
	String StrTenant_Id_C=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Tenant_Id_JspPageValue);
	 String StrCompany_Id_C=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Company_Id_JspPageValue);
	 String StrExecution_Sequence_Name_C=request.getParameter(GLOBALCONSTANT.SmartTaskConfigurator_Execution_Sequence_Name_JspPageValue);
	 /**Call util.LOOKUPGENERALCODE.Drop_Down_Selection() function to call the general code descriptions.*/
	 
	 
	 ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
	 String StrPageName=GLOBALCONSTANT.BlankString;
	 ObjSmartTaskConfiguratorService.setPageName(StrPageName);
	 ObjSmartTaskConfiguratorService.fetchEntry(StrTenant_Id_C, StrCompany_Id_C, StrExecution_Sequence_Name_C,GLOBALCONSTANT.Copy);
	 ObjModelAndView.addObject(GLOBALCONSTANT.ScreenLabels, ObjSmartTaskConfiguratorService.getSmartTaskConfiguratorLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));

}catch (JASCIEXCEPTION ObjectJasciException) {	
	log.error(ObjectJasciException.getMessage());
	return ObjModelAndView;
}
return ObjModelAndView;
}


}
