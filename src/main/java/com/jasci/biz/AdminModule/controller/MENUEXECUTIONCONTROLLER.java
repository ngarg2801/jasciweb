/**
Date Developed  Nov 20 2014
Description   Menu Execution Controller to handle the requests from login screen
Created By:Sarvendra Tyagi
 */


package com.jasci.biz.AdminModule.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.MENUEXECUTIONLABELBE;
import com.jasci.biz.AdminModule.be.MENUEXECUTIONLISTBE;
import com.jasci.biz.AdminModule.be.SUBMENULISTBE;
import com.jasci.biz.AdminModule.model.TEAMMEMBERCOMPANIESBEAN;
import com.jasci.biz.AdminModule.service.MENUEXECUTIONSERVICEIMP;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;




@Controller
public class MENUEXECUTIONCONTROLLER {



	@Autowired
	MENUEXECUTIONSERVICEIMP objMenuExecutionServiceImp;
	
	@Autowired
	
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	static Logger log= LoggerFactory.getLogger(MENUEXECUTIONCONTROLLER.class);
	/**
	 *Description: This function design to check company list of team member 
	 *Input parameter: ObjTeamMemberCompanies and ObjResult
	 *Return Type "ModelAndView"
	 *Created By "Sarvendra Tyagi"
	 *Created Date "Nov 14 2014" 
	 */


	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuExecutionScreen_Action, method = RequestMethod.GET)
	public ModelAndView CheckTeamMemberCompnies() throws JASCIEXCEPTION {

		ModelAndView objModelView=null;
		// Check TeamMember Companies 
		//COMMONSESSIONBE objCommonSessionBE= OBJCOMMONSESSIONBE;
		
		MENUEXECUTIONLABELBE objMenuexecutionlabelbe=null;
		
		/** Here we Check user logged in or not*/
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		List<TEAMMEMBERCOMPANIESBEAN> objliTeammembercompanies=null;
		try{
			
			//getting list of screen labels
			objMenuexecutionlabelbe=objMenuExecutionServiceImp.getLanguageLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
	
			//get company list
			objliTeammembercompanies=objMenuExecutionServiceImp.getListCompanies(OBJCOMMONSESSIONBE);
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}


		if(objliTeammembercompanies.isEmpty() || objliTeammembercompanies==null ){

			objModelView= new ModelAndView(GLOBALCONSTANT.RequestMapping_menu_profile_assignments_Action);

			return objModelView;

		}else if(objliTeammembercompanies.size()==GLOBALCONSTANT.IntOne){

			TEAMMEMBERCOMPANIESBEAN objTeammembercompaniesbean=null;
			objTeammembercompaniesbean=objliTeammembercompanies.get(GLOBALCONSTANT.INT_ZERO);

			//set company and compny logo in commonsessionBE 
			OBJCOMMONSESSIONBE.setCompany(objTeammembercompaniesbean.getCompany());
			OBJCOMMONSESSIONBE.setCompany_Name_20(objTeammembercompaniesbean.getName20());
			OBJCOMMONSESSIONBE.setCompanyLogo(objTeammembercompaniesbean.getComapnyLogo());
			OBJCOMMONSESSIONBE.setTheme_Full_Display(objTeammembercompaniesbean.getTFullDisplay());
			OBJCOMMONSESSIONBE.setTheme_Mobile(objTeammembercompaniesbean.getTMobile());
			OBJCOMMONSESSIONBE.setTheme_RF(objTeammembercompaniesbean.getTRF());
			
			

			//sending to main menu Screen
			objModelView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_menu_profile_assignments_Action);

			return objModelView; 

		}else{


			//sending list of company on compny list Screen
			objModelView= new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuExecutionView_Action,GLOBALCONSTANT.MenuAssignmentView_ObjMenuExecution,objliTeammembercompanies);	 
			objModelView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuexecutionlabelbe);
			objModelView.addObject(GLOBALCONSTANT.MenuAssignmentView_Key_TeamMemberName,OBJCOMMONSESSIONBE.getTeam_Member_Name());
			return	objModelView;

		}





	}




	/**
	 *Description This function design to send data into Kendo grid
	 *Input parameter ObjTeamMemberCompanies and ObjResult
	 *Return Type "function for sending data using resfull service send json in grid"
	 *Created By "Sarvendra Tyagi"
	 *Created Date "Nov 14 2014" 
	 */


	
	@RequestMapping(value =GLOBALCONSTANT.RequestMapping_MenuAssignmentView_RestFullService_Action, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERCOMPANIESBEAN>  getList() { 
		
		List<TEAMMEMBERCOMPANIESBEAN> ListTeamMemberCompany = new ArrayList<TEAMMEMBERCOMPANIESBEAN>();
		// Check TeamMember Companies 
		COMMONSESSIONBE objCommonSessionBE= OBJCOMMONSESSIONBE;

		try {
			ListTeamMemberCompany=objMenuExecutionServiceImp.getListCompanies(objCommonSessionBE);

		} catch (JASCIEXCEPTION obJJasciexception) {
			log.error(obJJasciexception.getMessage());
			return ListTeamMemberCompany;
		}
		return ListTeamMemberCompany;
	}





	/**
	 *Description This function design to to send list on menu assignment screen 
	 *Input parameter MenuExecution list and ObjResult
	 *Return Type "ModelAndView"
	 *Created By "Sarvendra Tyagi"
	 *Created Date "Dec 2 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAssignmentView_Action,method=RequestMethod.GET)
	public ModelAndView CheckAssignedMenu(HttpServletRequest obHttpRequest) throws JASCIEXCEPTION{


		ModelAndView objModelView=null;
		MENUEXECUTIONLABELBE objMenuexecutionlabelbe=null;
		List<MENUEXECUTIONLISTBE> objListMenuexecutionlistbe=null;
		Map<String,Map<String,Map<String,String>>> mainmenu=null;
		//COMMONSESSIONBE objCommonSessionBE= OBJCOMMONSESSIONBE;

		/** Here we Check user logged in or not*/
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		
		String name=obHttpRequest.getParameter(GLOBALCONSTANT.MenuAssignmentView_Key_CompanyName).toString();
		String company=obHttpRequest.getParameter(GLOBALCONSTANT.TeamMemebers_Restfull_Company).toString();
		String companyLogoUrl=obHttpRequest.getParameter(GLOBALCONSTANT.MenuAssignmentView_CompanyLogoURL).toString();
		String tmobile=obHttpRequest.getParameter(GLOBALCONSTANT.TMobile).toString();
		String tRF=obHttpRequest.getParameter(GLOBALCONSTANT.TRF).toString();
		String tFullDisplay=obHttpRequest.getParameter(GLOBALCONSTANT.TFullDisplay).toString();
		
		OBJCOMMONSESSIONBE.setCompany(company);
		OBJCOMMONSESSIONBE.setCompanyLogo(companyLogoUrl);

		OBJCOMMONSESSIONBE.setCompany_Name_20(name);
		OBJCOMMONSESSIONBE.setTheme_Full_Display(tFullDisplay);
		OBJCOMMONSESSIONBE.setTheme_Mobile(tmobile);
		OBJCOMMONSESSIONBE.setTheme_RF(tRF);
		
		try{

			//getting screen label
			objMenuexecutionlabelbe=objMenuExecutionServiceImp.getLanguageLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
		

			//get Assigned value of team member
			objListMenuexecutionlistbe=objMenuExecutionServiceImp.getAssignedMenuProfileList(OBJCOMMONSESSIONBE);
			mainmenu=objMenuExecutionServiceImp.getMenu(OBJCOMMONSESSIONBE);
			OBJCOMMONSESSIONBE.setMenu(mainmenu);
			if(objListMenuexecutionlistbe!=null){

				objModelView= new ModelAndView(GLOBALCONSTANT.RequestMapping_menu_profile_assignments_Action,
						GLOBALCONSTANT.MenuAssignmentView_MappingObject,objListMenuexecutionlistbe);
				objModelView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			}
			

		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			

		}
		objModelView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuexecutionlabelbe);

		return objModelView;

	}





	/**
	 *Description This function design to to send list on menu assignment screen 
	 *Input parameter MenuExecution list and ObjResult
	 *Return Type "ModelAndView"
	 *Created By "Sarvendra Tyagi"
	 *Created Date "Dec 2 2014" 
	 */


	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_menu_profile_assignments_Action,method=RequestMethod.GET)
	public ModelAndView CheckAssignedMenu() throws JASCIEXCEPTION{


		ModelAndView objModelView=null;
		List<MENUEXECUTIONLISTBE> objListMenuexecutionlistbe=null;
		Map<String,Map<String,Map<String,String>>> mainmenu=null;
		//COMMONSESSIONBE objCommonSessionBE= OBJCOMMONSESSIONBE;
		MENUEXECUTIONLABELBE objMenuexecutionlabelbe=null;

		/** Here we Check user logged in or not*/
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		//		String company=obHttpRequest.getParameter("Company").toString();


		try{
			//getting screen label
			objMenuexecutionlabelbe=objMenuExecutionServiceImp.getLanguageLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
		

			objListMenuexecutionlistbe=objMenuExecutionServiceImp.getAssignedMenuProfileList(OBJCOMMONSESSIONBE);
			mainmenu=objMenuExecutionServiceImp.getMenu(OBJCOMMONSESSIONBE);
			OBJCOMMONSESSIONBE.setMenu(mainmenu);
			
			if(objListMenuexecutionlistbe!=null){

				objModelView= new ModelAndView(GLOBALCONSTANT.RequestMapping_menu_profile_assignments_Action,GLOBALCONSTANT.MenuAssignmentView_MappingObject,objListMenuexecutionlistbe);
				objModelView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

			}

		}catch(JASCIEXCEPTION objJasciexception){


			log.error(objJasciexception.getMessage());
		}

		objModelView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuexecutionlabelbe);
		return objModelView;

	}







	/**
	 *Description This function design to to send list on menu assignment screen 
	 *Input parameter SubMenuBE list and ObjResult
	 *Return Type "ModelAndView"
	 *Created By "Sarvendra Tyagi"
	 *Created Date "Dec 3 2014" 
	 */


	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_SubMenu_Action,method=RequestMethod.GET)
	public ModelAndView SubMenuList(HttpServletRequest obHttpRequest)throws JASCIEXCEPTION{

		ModelAndView objModelView=null;
		//COMMONSESSIONBE ObjCommonSession= OBJCOMMONSESSIONBE;
		SUBMENULISTBE objSubmenulistbe=null;
		Map<String,Map<String,Map<String,String>>> mainmenu=null;
		/** Here we Check user logged in or not*/
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		String selectedProfile=obHttpRequest.getParameter(GLOBALCONSTANT.RequestParameter_MenuProfile).toString();
		OBJCOMMONSESSIONBE.setAuthority_Profile(selectedProfile);

		try{

			Map<String, Map<String, String>> hminput=null;

			objSubmenulistbe=objMenuExecutionServiceImp.getSubMenus(OBJCOMMONSESSIONBE);
			mainmenu=objMenuExecutionServiceImp.getMenu(OBJCOMMONSESSIONBE);
			OBJCOMMONSESSIONBE.setMenu(mainmenu);
			
			if(objSubmenulistbe!=null){

				hminput=objSubmenulistbe.getHmListSubMenu();
				OBJCOMMONSESSIONBE.setSubMenuListHM(hminput);
				

				objModelView= new ModelAndView(GLOBALCONSTANT.RequestMapping_SubMenuScreen_Action);//,GLOBALCONSTANT.SubMenuHashMap_MappingObject,hminput);
				objModelView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

			}




		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());

		}

		return objModelView;


	}









}
