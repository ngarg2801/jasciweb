/**
 * @author Shailendra Rajput
 * @purpose this java file is used for executing the engine from menu option
 * @createdon 16th Oct 2015
 */

package com.jasci.biz.AdminModule.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.jasci.biz.AdminModule.be.EXECUTIONBE;
import com.jasci.biz.AdminModule.be.SHAREDDATA;
import com.jasci.biz.execution.EXECUTIONFACTORY;
import com.jasci.biz.execution.IEXECUTIONENGINE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.RESTWEBSERVICECALLER;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class ENGINEMENUOPTION {

	/**
	 * Autowired the Common session for getting the session variables
	 */
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;

	/**
	 * Instanciate the Logger 
	 */
	final Logger log = LoggerFactory.getLogger(ENGINEMENUOPTION.class.getName());
	/**
	 * @purpose this controller is called by the execution when he finishes his task 
	 * @param StrJson
	 * @param executionName
	 * @param IsCompleted
	 * @return
	 * @throws JASCIEXCEPTION 
	 */

	@RequestMapping(value = GLOBALCONSTANT.NEXT_EXECUTION, method = RequestMethod.GET)
	public ModelAndView getNextExecution(@RequestParam(value = GLOBALCONSTANT.STR_JSON) String StrJson,
			@RequestParam(value = GLOBALCONSTANT.EXECUTION_NAME) String executionName,
			@RequestParam(value = GLOBALCONSTANT.ISCOMPLETED) Boolean IsCompleted) throws JASCIEXCEPTION {

		SHAREDDATA objShareddata = null;
		String strResponse = null;
		Gson objGson = new Gson();

		/**
		 * Checked if the IsCompleted is true and we have the shared data
		 */
		if (IsCompleted && StrJson != null) {
			try {
				
				
				/**
				 * Call for the next Execution
				 */
				strResponse = new RESTWEBSERVICECALLER().getRestResponse(GLOBALCONSTANT.NextGlassWebServiceURL, StrJson,
						HttpMethod.POST);
				if (strResponse.equalsIgnoreCase(GLOBALCONSTANT.SUCESS)) {
					return new ModelAndView(
							GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon + GLOBALCONSTANT.Forword_SLASH
									+ GLOBALCONSTANT.RequestMapping_menu_profile_assignments_Action);
				}

				
				/**
				 * Convert the Response to shared data
				 */
				objShareddata = objGson.fromJson(strResponse, SHAREDDATA.class);

			} catch (IOException objParseException) {
				// TODO Auto-generated catch block
				log.info(GLOBALCONSTANT.Failed_to_get_the_data_the_next_data_or_unable_to_parse_the_response_into_shared_data+objParseException.getMessage());
			//	e.printStackTrace();
			}
			/**
			 * Checked for shared data is not null
			 */
			if (objShareddata != null) {

				String strExecutionName = objShareddata.getObjSequenceBE().getStrEXECUTIONNAME();

				EXECUTIONBE objExecutionBE = objShareddata.getMapExecution().get(strExecutionName);
				EXECUTIONFACTORY objExecutionFactory = new EXECUTIONFACTORY();

				log.info(GLOBALCONSTANT.Next_Execution_is+strExecutionName);
				/**
				 * get the Instance of execution from factory
				 */
				IEXECUTIONENGINE objIexecutionengine = objExecutionFactory
						.getExecutionInstance(objExecutionBE.getStrEXECUTION());

				/**
				 * the loop runs till we are getting the IEXECUTIONENGINE object 
				 */
				while (objIexecutionengine != null) {

					/**
					 * execute the execution
					 */

					objShareddata = objIexecutionengine.Execute(objShareddata, objShareddata.getObjSequenceBE());

					try {
						/**
						 * getting the next execution and based of that the loop
						 * is continue
						 */
						strResponse = new RESTWEBSERVICECALLER().getRestResponse(GLOBALCONSTANT.NextGlassWebServiceURL,
								objGson.toJson(objShareddata), HttpMethod.POST);
					} catch (IOException objParseException) {
						// TODO Auto-generated catch block
						log.info(GLOBALCONSTANT.Failed_to_get_the_data_the_next_data_or_unable_to_parse_the_response_into_shared_data+objParseException.getMessage());
					}

					objShareddata = objGson.fromJson(strResponse, SHAREDDATA.class);
					strExecutionName = objShareddata.getObjSequenceBE().getStrEXECUTIONNAME();
					log.info(GLOBALCONSTANT.Next_Execution_is+strExecutionName);
					objExecutionBE = objShareddata.getMapExecution().get(strExecutionName);
					objIexecutionengine = objExecutionFactory.getExecutionInstance(objExecutionBE.getStrEXECUTION());
				} // END of LOOP

				/**
				 * Checking for execution type is visual and for full screen
				 */
				if (objExecutionBE.getStrEXECUTION_TYPE().equalsIgnoreCase(GLOBALCONSTANT.VISUAL)
				/*
				 * && objExecutionBE.getStrEXECUTION_DEVICE().equalsIgnoreCase(
				 * "FULLSCREEN")
				 */) {

					String ExecutionPath = objExecutionBE.getStrEXECUTION();
					String strData = objGson.toJson(objShareddata);
					try {
						strData = URLEncoder.encode(strData, GLOBALCONSTANT.UTF8);
						executionName =URLEncoder.encode(executionName, GLOBALCONSTANT.UTF8);
					} catch (UnsupportedEncodingException objEncodeException) {
						// TODO Auto-generated catch block
						log.error(GLOBALCONSTANT.Unable_to_encode_the_data+objEncodeException.getMessage());
					}
					//System.out.println(ExecutionPath);
					/**
					 * Creating a URL and pass the shared Data with execution
					 * History and is completed flag.
					 */
					String strExecutionURL = ExecutionPath + GLOBALCONSTANT.QUESTION_MARK + GLOBALCONSTANT.STRJSON+GLOBALCONSTANT.Equal
							+ strData + GLOBALCONSTANT.AMPERSANDEXECUTIONNAME + executionName
							+ GLOBALCONSTANT.AMPERSANDCOMPLETED;
					/**
					 * redirect to that URL
					 */
					return new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon + strExecutionURL);
				}
			}
		}

		return null;

	}

	/**
	 * @purpose this method is used when we start the engine from menu option
	 * @param objRequest
	 * @return
	 */
	@SuppressWarnings(GLOBALCONSTANT.unchecked)
	@RequestMapping(value = GLOBALCONSTANT.ENGINESTART, method = RequestMethod.GET)
	public ModelAndView startProcess(
			HttpServletRequest objRequest/*
											 * ,Device device
											 */) {

		/**
		 * Initialization of Local variables
		 */
		SHAREDDATA objShareddata = null;
		String strMenuName = null;
		Gson objGson = new Gson();

		/**
		 * getting the menu name from request parameters
		 */
		try {
			strMenuName = objRequest.getParameter(GLOBALCONSTANT.MENUNAME);

		} catch (Exception objException) {

		}

		/**
		 * checking for menu name is not null
		 * 
		 */
		if (strMenuName != null) {

			String strResponse = null;
			try {

				/**
				 * Creating a request parameters of start engine
				 */
				JSONObject objJsonObject = new JSONObject();
				objJsonObject.put(GLOBALCONSTANT.MENUNAMEKEY, strMenuName);
				objJsonObject.put(GLOBALCONSTANT.TENANTKEY, OBJCOMMONSESSIONBE.getTenant());
				objJsonObject.put(GLOBALCONSTANT.COMPANYKEY, OBJCOMMONSESSIONBE.getCompany());
				objJsonObject.put(GLOBALCONSTANT.TENENANTMEMBERKEY, OBJCOMMONSESSIONBE.getTeam_Member());

				String strJSON = "";// "{\"menuName\":\"Picktest006\",\"tenant\":\"53310asp-d6d4-4715-8762-6dcba01010b\",\"company\":\"Rock
									// Audio\",\"teamMember\":\"D john\"}";

				/**
				 * convert the json object to json string
				 */
				strJSON = objJsonObject.toJSONString();

				// String
				// strURL="http://192.168.1.79:8181/JASCI_GlassWebservice/GlassEngine";

				/**
				 * get the response from engine web service
				 */
				strResponse = new RESTWEBSERVICECALLER().getRestResponse(GLOBALCONSTANT.StartGlassWebServiceURL,
						strJSON, HttpMethod.GET);

				/**
				 * convert the json response to shared data object
				 */
				objShareddata = objGson.fromJson(strResponse, SHAREDDATA.class);
			} catch (Exception objParseException) {
				// TODO Auto-generated catch block
				log.info(GLOBALCONSTANT.Failed_to_get_the_data_the_next_data_or_unable_to_parse_the_response_into_shared_data+objParseException.getMessage());
			}

			/**
			 * checking for shared data that not null
			 */
			if (objShareddata != null) {

				/**
				 * get the deatils from shared data
				 */
				String strExecutionName = objShareddata.getObjSequenceBE().getStrEXECUTIONNAME();

				EXECUTIONBE objExecutionBE = objShareddata.getMapExecution().get(strExecutionName);
				EXECUTIONFACTORY objExecutionFactory = new EXECUTIONFACTORY();
				log.info(GLOBALCONSTANT.Next_Execution_is+strExecutionName);

				/**
				 * get the Instance of execution from factory
				 */
				IEXECUTIONENGINE objIexecutionengine = objExecutionFactory
						.getExecutionInstance(objExecutionBE.getStrEXECUTION());

				/**
				 * the loop runs till we are getting the backend execution
				 */
				while (objIexecutionengine != null) {

					/**
					 * execute the execution
					 */

					objShareddata = objIexecutionengine.Execute(objShareddata, objShareddata.getObjSequenceBE());

					try {
						/**
						 * getting the next execution and based of that the loop
						 * is continue
						 */
						strResponse = new RESTWEBSERVICECALLER().getRestResponse(GLOBALCONSTANT.NextGlassWebServiceURL,
								objGson.toJson(objShareddata), HttpMethod.POST);
					} catch (Exception objParseException) {
						// TODO Auto-generated catch block
						log.info(GLOBALCONSTANT.Failed_to_get_the_data_the_next_data_or_unable_to_parse_the_response_into_shared_data+objParseException.getMessage());
					}

					objShareddata = objGson.fromJson(strResponse, SHAREDDATA.class);
					strExecutionName = objShareddata.getObjSequenceBE().getStrEXECUTIONNAME();
					log.info(GLOBALCONSTANT.Next_Execution_is+strExecutionName);
					objExecutionBE = objShareddata.getMapExecution().get(strExecutionName);
					objIexecutionengine = objExecutionFactory.getExecutionInstance(objExecutionBE.getStrEXECUTION());
				} // END of LOOP

				// String
				// customExecution=objShareddata.getObjSequenceBE().getStrACTIONNAME();
				/**
				 * Checking for execution type is visual and for full screen
				 */
				if (objExecutionBE.getStrEXECUTION_TYPE().equalsIgnoreCase(GLOBALCONSTANT.VISUAL)) {
					/**
					 * getting the execution path here we get the URL
					 */

					String ExecutionPath = objExecutionBE.getStrEXECUTION();
				String	StrexecutionHistory ="{\"EXCECUTIONHISTORY\":[]}";
					String strData = objGson.toJson(objShareddata);
					try {
						strData = URLEncoder.encode(strData, GLOBALCONSTANT.UTF8);
						StrexecutionHistory=URLEncoder.encode(StrexecutionHistory, GLOBALCONSTANT.UTF8);
					} catch (UnsupportedEncodingException objEncodeException) {
						// TODO Auto-generated catch block
						log.error(GLOBALCONSTANT.Unable_to_encode_the_data+objEncodeException.getMessage());
					}
					;
					// System.out.println(ExecutionPath);

					/**
					 * Creating a URL and pass the shared Data with execution
					 * History and is completed flag.
					 */
					String strExecutionURL = ExecutionPath + GLOBALCONSTANT.QUESTION_MARK + GLOBALCONSTANT.STRJSON + GLOBALCONSTANT.Equal+strData
							+ GLOBALCONSTANT.AMPERSANDFIRSTEXECUTIONNAME+StrexecutionHistory+GLOBALCONSTANT.AMPERSANDCOMPLETED;
					/**
					 * redirect to that URL
					 */
					return new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon + strExecutionURL);
				}
			} //

		}

		return null;

	}
	
/*	@RequestMapping(value = "DStest", method = RequestMethod.GET)
	public @ResponseBody String addOrUpdateMenuAppIcon(HttpServletRequest objRequest) throws JASCIEXCEPTION {	

		String string="{\"EXCECUTIONHISTORY\":[{\"ExcecutionName\":\"\",\"EXCECUTIONHISTORYVALUES\":[{\"Name\":\"\",\"Value\":\"\"}],\"Position\":1}]}";
		Gson objGson = new Gson();
		EXECUTIONHISTORYLIST objImenuappicondal=objGson.fromJson(string, EXECUTIONHISTORYLIST.class);
		
		return objImenuappicondal.toString();

	
	}*/

}
