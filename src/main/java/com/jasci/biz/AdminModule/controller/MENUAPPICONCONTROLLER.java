/*
Date Developed :Dec 12 2014
Created by: Rahul kumar
Description :TEAMMEMBERS Controller TO HANDLE THE REQUESTS FROM SCREENS
 */

package com.jasci.biz.AdminModule.controller;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.MENUAPPICONSCREENBE;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.IMENUAPPICONSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.MENUAPPICONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class MENUAPPICONCONTROLLER {
	
	@Autowired
	IMENUAPPICONSERVICE ObjIMenuAppIconService;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	static Logger log= LoggerFactory.getLogger(MENUAPPICONCONTROLLER.class);
	String StrMenuType=GLOBALCONSTANT.StrMenuTypeFullScreen;
	String StrMenuOption=GLOBALCONSTANT.StrMenuOptionMenuAppIcon;
	String StrAppIconPart=GLOBALCONSTANT.BlankString;
	String StrAppIconName=GLOBALCONSTANT.BlankString;
	String StrDescription=GLOBALCONSTANT.BlankString;
	String StrBackKendoURL=GLOBALCONSTANT.BlankString;
	
	
	ModelAndView ObjModelAndView=new ModelAndView();
	
	
	/**
	 *Description This function redirect to MenuAppIcons Lookup screen   
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Dec12 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAppIcon_LookupScreen_Action, method = RequestMethod.GET)
	public ModelAndView showMenuAppIconLookup(){
		
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
        /** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_APPICON_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		MENUAPPICONSCREENBE objMenuappiconscreenbe=null;
		
		try{
			objMenuappiconscreenbe=	ObjIMenuAppIconService.setScreenLanguage(GLOBALCONSTANT.Param_Languages_MenuAppIconMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuAppIcon_LookupScreen_Page);
	//	ObjModelAndView= new ModelAndView("uploadimage2");
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIconMaintenanceSceernLabelObj,objMenuappiconscreenbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		
		return ObjModelAndView;
		
	}
	
	
	
	/**
	 *Description This function redirect to MenuAppIcons Maintenance screen   
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Dec12 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAppIcon_Maintenance_New_Screen_Action, method = RequestMethod.GET)
	public ModelAndView showMenuAppIconMaintenance(HttpServletRequest request){
		
		
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
 /** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_APPICON_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		MENUAPPICONSCREENBE objMenuappiconscreenbe=null;
		/*List<GENERALCODES> objGeneralcodesApplication=null;*/
		String backSatus=request.getParameter(GLOBALCONSTANT.BACKSTATUS);

		String TeamMemberName=GLOBALCONSTANT.BlankString;
		  Date ObjDate1 = new Date();
		  String StrCurrentDate=null;
		  
		  SimpleDateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format_yyyy_MM_dd);
		  try {
		   StrCurrentDate=ObjDateFormat1.format(ObjDate1);
		  } catch (Exception objException) {
		   
		   log.error(objException.getMessage());
		  }
		
		try{
			objMenuappiconscreenbe=	ObjIMenuAppIconService.setScreenLanguage(GLOBALCONSTANT.Param_Languages_MenuAppIconMaintenance_ObjectCode,StrLanguage);
			/*objGeneralcodesApplication=ObjIMenuAppIconService.getApplicationList(GLOBALCONSTANT.APPLICATIONS);*/
			TeamMemberName=ObjIMenuAppIconService.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member());
			  
			
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuAppIcon_Maintenance_Screen_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIconMaintenanceSceernLabelObj,objMenuappiconscreenbe);
		try{
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeApplicationListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_TeamMemberName,TeamMemberName);
		ObjModelAndView.addObject(GLOBALCONSTANT.SetupDate,StrCurrentDate);
		ObjModelAndView.addObject(GLOBALCONSTANT.BACKSTATUS,backSatus);
		
		
		return ObjModelAndView;
		
	}
	
	
	/**
	 *Description This function redirect to MenuAppIcons Search Lookup screen   
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Dec12 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_Screen_Action, method = RequestMethod.GET)
	public ModelAndView showMenuAppIconSearchLookup(){
		
		
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
 /** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_APPICON_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		MENUAPPICONSCREENBE objMenuappiconscreenbe=null;
		
		try{
			objMenuappiconscreenbe=	ObjIMenuAppIconService.setScreenLanguage(GLOBALCONSTANT.Param_Languages_MenuAppIconMaintenance_ObjectCode,StrLanguage);
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_Screen_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIconMaintenanceSceernLabelObj,objMenuappiconscreenbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Kendo_Read_URL,GLOBALCONSTANT.RequestMapping_MenuAppIcon_Read_Kendo_UI);
		StrBackKendoURL=GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_Screen_Action.replace(GLOBALCONSTANT.Forward_Slash, GLOBALCONSTANT.BlankString);	
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return ObjModelAndView;
		
	}
	
	
	
	/**
	 *Description This function redirect to ManuAppIcon SearchLookUp screen after addOrUpdate a record  
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Rahul Kumar"
	 *Created Date "Dec 14 2014" 
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAppIcon_AddOrUpdate_Action, method = RequestMethod.POST)
	public ModelAndView addOrUpdateMenuAppIcon(@ModelAttribute(GLOBALCONSTANT.MenuAppIcon_Register_ModelAttribute)  MENUAPPICONBE objMenuappiconbe,HttpServletRequest request){
		
		
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
 /** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Menu_APPICON_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		//String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String strLastActivityBy=OBJCOMMONSESSIONBE.getTeam_Member();
		String address=request.getParameter(GLOBALCONSTANT.MenuAppIcon_IconAddress);
		String ApplicationId=request.getParameter(GLOBALCONSTANT.MenuAppIcon_ApplicationId);
		String LastActivityDate=request.getParameter(GLOBALCONSTANT.GetLastActivitydate);
		//MENUAPPICONSCREENBE objMenuappiconscreenbe=null;
		
		try{
			objMenuappiconbe.setLastActivityTeamMember(strLastActivityBy);
			objMenuappiconbe.setAppIconAddress(address);
			if(ApplicationId!=null){
				objMenuappiconbe.setApplication(ApplicationId);
			}
			objMenuappiconbe.setLastActivityDate(LastActivityDate);
			ObjIMenuAppIconService.addOrUpdateMenuAppIcon(objMenuappiconbe);

		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}
		
	
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_Screen_Action);
	
		
		return ObjModelAndView;
		
	}


	
	
	
	

		
		
		/**
		 *Description This function use for request to edit a record.  
		 *Input parameter none
		 *Return Type "ModelAndView"
		 *Created By "Rahul Kumar"
		 *Created Date "Dec 14 2014" 
		 */
		@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAppIcon_AddOrUpdate_Request_Action, method = RequestMethod.GET)
		public ModelAndView addOrUpdateMenuAppIconRequest(HttpServletRequest request){
		
			String StrUserid=OBJCOMMONSESSIONBE.getUserID();
			String StrPassword=OBJCOMMONSESSIONBE.getPassword();
			
			if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
			}
			
			 /** Here we Check user have this screen access or not*/
			
			WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
					GLOBALCONSTANT.RequestMapping_Menu_APPICON_Maintenmence_Execution);

			if (!objWebServiceStatus.isBoolStatus()) {
				ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
				return ObjModelAndView;

			}
			
			String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
			//String strLastActivityBy=OBJCOMMONSESSIONBE.getTeam_Member();			
			String Tenant=OBJCOMMONSESSIONBE.getTenant();
			String Application=request.getParameter(GLOBALCONSTANT.MenuAppIcon_Application);
			String AppIcon=request.getParameter(GLOBALCONSTANT.MenuAppIcon_AppIcon);
			String FlagForReturn=request.getParameter(GLOBALCONSTANT.MenuAppIcon_FlagForReturn);
			String TeamMemberName=GLOBALCONSTANT.BlankString;
			List<MENUAPPICONS> objMenuappiconsList=null;
			/*List<GENERALCODES> objGeneralcodesApplication=null;*/
			
			MENUAPPICONSCREENBE objMenuappiconscreenbe=null;

		
			
			try{
				//objMenuappiconbe.setLastActivityTeamMember(strLastActivityBy);
				objMenuappiconsList=ObjIMenuAppIconService.getMenuAppIconListByApplicationAppIcon(Application, AppIcon);
				
			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
			
			try{
				objMenuappiconscreenbe=	ObjIMenuAppIconService.setScreenLanguage(GLOBALCONSTANT.Param_Languages_MenuAppIconMaintenance_ObjectCode,StrLanguage);
				/*objGeneralcodesApplication=ObjIMenuAppIconService.getApplicationList(GLOBALCONSTANT.APPLICATIONS);*/
				
			}catch(JASCIEXCEPTION objJasciexception){
				
				log.error(objJasciexception.getMessage());
			}
			String LastActivityDate=ConvertDate(objMenuappiconsList.get(GLOBALCONSTANT.INT_ZERO).getLastActivityDate(), GLOBALCONSTANT.Date_Format,GLOBALCONSTANT.yyyy_MM_dd_Format);
			//StringEscapeUtils.escapeHtml
			MENUAPPICONS objMenuappicons=objMenuappiconsList.get(GLOBALCONSTANT.INT_ZERO);
			try {
				TeamMemberName=ObjIMenuAppIconService.getTeamMemberName(Tenant, objMenuappicons.getLastActivityTeamMember());
			} catch (JASCIEXCEPTION objJasciexception) {
				
				log.error(objJasciexception.getMessage());
			}
			String AppIconName=objMenuappicons.getAppIcon();
			objMenuappicons.setAppIcon(StringEscapeUtils.escapeHtml(objMenuappicons.getAppIcon()));
			objMenuappicons.setAppIconAddress(StringEscapeUtils.escapeHtml(objMenuappicons.getAppIconAddress()));
			objMenuappicons.setApplication(StringEscapeUtils.escapeHtml(objMenuappicons.getApplication()));
			objMenuappicons.setDescriptionLong(StringEscapeUtils.escapeHtml(objMenuappicons.getDescriptionLong()));
			objMenuappicons.setDescriptionShort(StringEscapeUtils.escapeHtml(objMenuappicons.getDescriptionShort()));
			objMenuappicons.setLastActivityTeamMember(StringEscapeUtils.escapeHtml(objMenuappicons.getLastActivityTeamMember()));
			
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuAppIcon_Maintenance_Update_Screen_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIconMaintenanceSceernLabelObj,objMenuappiconscreenbe);
			//ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeApplicationListObject,objGeneralcodesApplication);
			try{
				ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeApplicationListObject,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
				}catch(JASCIEXCEPTION objJasciexception){
					
					log.error(objJasciexception.getMessage());
				}
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Format_LastActivityDate,LastActivityDate);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_List_Object,objMenuappicons);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_FlagForReturnObj,FlagForReturn);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_AppIconNameForCheck,AppIconName);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_TeamMemberName,TeamMemberName);
			
			
			//ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Kendo_Read_URL,FlagForReturn);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_SearchLookup_BackScreen,StrBackKendoURL);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			
			
			
			
			return ObjModelAndView;
			
		}


		
		
		
		
		
		
		/**
		 *Description This function redirect to MenuAppIconSearchlookup screen page after delete a MenuAppIcon record
		 *Input parameter none
		 *Return Type "ModelAndView"
		 *Created By "Rahul Kumar"
		 *Created Date "Dec 17 2014" 
		 */
		@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAppIcon_Maintenance_Delete_Screen_Action, method = RequestMethod.POST)
		public ModelAndView deleteMenuAppIcon(HttpServletRequest request){
			
			String StrUserid=OBJCOMMONSESSIONBE.getUserID();
			String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		
			if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
			}
			
			 /** Here we Check user have this screen access or not*/
			
			WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
					GLOBALCONSTANT.RequestMapping_Menu_APPICON_Maintenmence_Execution);

			if (!objWebServiceStatus.isBoolStatus()) {
				ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
				return ObjModelAndView;

			}
			String Application=request.getParameter(GLOBALCONSTANT.MenuAppIcon_Application);
			String AppIcon=request.getParameter(GLOBALCONSTANT.MenuAppIcon_AppIcon);
			String KendoUrl=request.getParameter(GLOBALCONSTANT.MenuAppIcon_KendoUrl);
			String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
			
			try{
				
				ObjIMenuAppIconService.deleteMenuAppIcon(Application, AppIcon);
			
				if(GLOBALCONSTANT.RequestMapping_MenuAppIcon_Read_Kendo_UI.equalsIgnoreCase(KendoUrl)){
				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_Screen_Action);
				}
				else if(GLOBALCONSTANT.RequestMapping_MenuAppIcon_Read_Kendo_UI_By_Part.equalsIgnoreCase(KendoUrl)){
					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_By_Part_Screen_Action);
				}
			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
			
			try{
				ObjIMenuAppIconService.setScreenLanguage(GLOBALCONSTANT.Param_Languages_MenuAppIconMaintenance_ObjectCode,StrLanguage);
			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			return ObjModelAndView;

		}

		
		
		
		
		

		
		
		
		/**
		 *Description This function redirect to MenuAppIcons Search Lookup screen   
		 *Input parameter none
		 *Return Type "ModelAndView"
		 *Created By "Rahul Kumar"
		 *Created Date "Dec12 2014" 
		 */
		@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_By_Part_Screen_Action, method = RequestMethod.GET)
		public ModelAndView showMenuAppIconSearchLookupbyPart(HttpServletRequest request){
			
			
			String StrUserid=OBJCOMMONSESSIONBE.getUserID();
			String StrPassword=OBJCOMMONSESSIONBE.getPassword();
			
			
			if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
			}
			
			 /** Here we Check user have this screen access or not*/
			
			WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
					GLOBALCONSTANT.RequestMapping_Menu_APPICON_Maintenmence_Execution);

			if (!objWebServiceStatus.isBoolStatus()) {
				ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
				ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
				return ObjModelAndView;

			}
			String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
			/*String Application=request.getParameter(GLOBALCONSTANT.MenuAppIcon_Application);
			String AppIcon=request.getParameter(GLOBALCONSTANT.MenuAppIcon_AppIcon);*/
			String AppIconName=request.getParameter(GLOBALCONSTANT.MenuAppIcon_AppIconName);
			String AppIconPart=request.getParameter(GLOBALCONSTANT.MenuAppIcon_AppIconPart);			
			MENUAPPICONSCREENBE objMenuappiconscreenbe=null;

			
			try{
				objMenuappiconscreenbe=	ObjIMenuAppIconService.setScreenLanguage(GLOBALCONSTANT.Param_Languages_MenuAppIconMaintenance_ObjectCode,StrLanguage);
			}catch(JASCIEXCEPTION objJasciexception){
				
				log.error(objJasciexception.getMessage());
			}
			
			
			if(AppIconName!=null){
				StrAppIconName=AppIconName;
			}
			if(AppIconPart!=null){
			 StrAppIconPart=AppIconPart;
			}
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_Screen_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIconMaintenanceSceernLabelObj,objMenuappiconscreenbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Kendo_Read_URL,GLOBALCONSTANT.RequestMapping_MenuAppIcon_Read_Kendo_UI_By_Part);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Kendo_Read_AppIconName,StrAppIconName);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Kendo_Read_AppIconPart,StrAppIconPart);
			ObjModelAndView.addObject(GLOBALCONSTANT.MenuAppIcon_Kendo_Read_Description,StrDescription);
			StrBackKendoURL=GLOBALCONSTANT.RequestMapping_MenuAppIcon_SearchLookup_By_Part_Screen_Action.replace(GLOBALCONSTANT.Forward_Slash, GLOBALCONSTANT.BlankString);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			
			
			return ObjModelAndView;
			
		}
		
		
		
		
		
		
		
		
		/**
		 *Description This function read data using Kendo ui MenuAppIcon
		 *Input parameter none
		 *Return Type "ModelAndView"
		 *Created By "Rahul Kumar"
		 *Created Date "Dec 14 2014" 
		 */
		
		//read data using Kendo ui menu app icon
			@RequestMapping(value = GLOBALCONSTANT.RequestMapping_MenuAppIcon_Read_Kendo_UI, method = RequestMethod.GET)
			public @ResponseBody  List<MENUAPPICONS>  getMenuAppIconList(HttpServletRequest request) {	
				//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
				String StrTenant=OBJCOMMONSESSIONBE.getTenant();
				String StrCompany=OBJCOMMONSESSIONBE.getCompany();
				
				
				List<MENUAPPICONS> objMenuappicons=null;
				//call service method to get Team memebr list
				
				try {
					objMenuappicons=ObjIMenuAppIconService.getManuAppIconList(StrTenant, StrCompany);
				} catch (JASCIEXCEPTION objJasciexception) {
					
					log.error(objJasciexception.getMessage());
				}
				return objMenuappicons;
				

			}
			
			
			
			/**
			 *Description This function read data using Kendo ui MenuAppIcon
			 *Input parameter none
			 *Return Type "ModelAndView"
			 *Created By "Rahul Kumar"
			 *Created Date "Dec 14 2014" 
			 */
			
			//read data using Kendo ui GeneralCode
				@RequestMapping(value = GLOBALCONSTANT.RequestMapping_MenuAppIcon_Read_Kendo_UI_By_Part, method = RequestMethod.GET)
				public @ResponseBody  List<MENUAPPICONS>  getMenuAppIconListByPart(HttpServletRequest request) {	
					//COMMONSESSIONBE objCommonsessionbe = LOGINSERVICEIMPL.ObjCommonSessionBe;
							
					List<MENUAPPICONS> objMenuappicons=null;
					//call service method to get Team memebr list
					
					try {
						objMenuappicons=ObjIMenuAppIconService.getAppIconListByPart(StrAppIconName, StrAppIconPart);
					} catch (JASCIEXCEPTION objJasciexception) {
						
						log.error(objJasciexception.getMessage());
					}
					return objMenuappicons;
					

				}


		
		
		
		/**
		 * 
		 * @author Rahul Kumar
		 * @Date Dec 17, 2014 
		 * @param dateInString
		 * @param StrInFormat
		 * @param StrOutFormat
		 * @return
		 * String
		 */
				public String ConvertDate(Date dateInString,String StrInFormat,String StrOutFormat)
				{
					
					DateFormat dateFormat = new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
					String StrFormattedString=GLOBALCONSTANT.BlankString ;
				
					try {

						Date date = dateInString;
						StrFormattedString=dateFormat.format(date);
						

					} catch (Exception e) {
						
					}

					return StrFormattedString;

				}

		
	
		
}
