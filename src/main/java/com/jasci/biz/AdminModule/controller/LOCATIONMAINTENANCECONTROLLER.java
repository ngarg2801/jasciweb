/**
Description : Make a controller of LOCATIONS Maintenance class for mapping with model and View Of Locations Maintenance Screens
Created By : Aakash Bishnoi
Created Date : Dec 23 2014

 */
package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.model.LOCATIONBEAN;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ILOCATIONMAINTENANCESERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;



@Controller
public class LOCATIONMAINTENANCECONTROLLER {

	
	@Autowired
	private ILOCATIONMAINTENANCESERVICE ObjectLocationMaintenanceService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;

	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	/** It is used to logger the exception*/
	static Logger log = LoggerFactory.getLogger(LOCATIONMAINTENANCECONTROLLER.class.getName());
	/**create modelandview instance*/
	ModelAndView ObjModelAndView=new ModelAndView();

	/**Parameter declaration and initialization*/
	String StrLocation=GLOBALCONSTANT.BlankString;
	String StrFulfillmentCenter=GLOBALCONSTANT.BlankString;
	String StrArea=GLOBALCONSTANT.BlankString;
	String StrDescription=GLOBALCONSTANT.BlankString;
	String StrLocationType=GLOBALCONSTANT.BlankString;
	
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 23, 2014
	 * @Description this is used to Show the page of Location Maintenance Search and show Dropdown of Area and Language Type
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Search , method = RequestMethod.GET)
	public ModelAndView getAreaSearch(@ModelAttribute(GLOBALCONSTANT.Location_ModelAttribute) LOCATIONBEAN ObjectLocationBean) {	 

		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Search);
		try{
			
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectArea,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLocationType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
			
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.LocationMaintenance_ScreenLabels, ObjectLocationMaintenanceService.getLocationMaintenanceLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		}catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}	
		return ObjModelAndView;
	}






	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 23, 2014
	 * @Description this is used to redirect the page of Location SearchLookup
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Search_Lookup , method = RequestMethod.GET)
	public ModelAndView getLocationSearchLookup(HttpServletRequest request) {	 


		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		try {
		StrLocation=request.getParameter(GLOBALCONSTANT.Location_Location_JspPageValue);
		StrArea=request.getParameter(GLOBALCONSTANT.Location_Area_JspPageValue);
		StrFulfillmentCenter=request.getParameter(GLOBALCONSTANT.Location_FulfillmentCenter_JspPageValue);
		StrDescription=request.getParameter(GLOBALCONSTANT.Location_Description_JspPageValue);
		StrLocationType=request.getParameter(GLOBALCONSTANT.Location_Locationtype_JspPageValue);
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_SearchLookup);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.LocationMaintenance_ScreenLabels, ObjectLocationMaintenanceService.getLocationMaintenanceLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		} catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
			
		}
		return ObjModelAndView;
	}

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 23, 2014
	 * Description this is used to Show the page of Location SearchLookup
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_SearchLookup , method = RequestMethod.GET)
	public ModelAndView getLocationSearchLookup() {	 


		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		try {
			
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Search_Lookup);	
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationMaintenance_ScreenLabels, ObjectLocationMaintenanceService.getLocationMaintenanceLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		} catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
		return ObjModelAndView;
	}
	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 24, 2014
	 * Description this is used to Show the page of Location Maintenance
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Maintenance_New , method = RequestMethod.GET)
	public ModelAndView getMaintenanceAdd(@ModelAttribute(GLOBALCONSTANT.Location_ModelAttribute) LOCATIONBEAN ObjectLocationBean,HttpServletRequest objHttpServletRequest) {	 

		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String backSatus=objHttpServletRequest.getParameter(GLOBALCONSTANT.BACKSTATUS);
		
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Maintenance_New);
		try{
			String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
			String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
			//String StrFullfillment_Center=OBJCOMMONSESSIONBE.getFulfillmentCenter();
			String StrTeamMember=OBJCOMMONSESSIONBE.getTeam_Member();
			Date CurrentDate=new Date();
			DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelps_yyyyMMdd_Format);
			String testDateString = ObjDateFormat.format(CurrentDate);	
			//Provide ComboBox value form GENERAL_CODE table on the behalf of Tenant_ID,Company_ID,General_Code_Id
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectFullfillment,ObjectLocationMaintenanceService.getFulfillmentCenter(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.LocationCopyScreen));
																														
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLastActivityTask,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectArea,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLocationType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWizardID,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WIZARDS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWorkGroupZone,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GRPWRKZONE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWorkZone,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WORKZONES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectStorageType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STRGTYPES,GLOBALCONSTANT.BlankString));
			//ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLocationProfile,ObjectLocationMaintenanceService.getLocationProfile(StrTenant_ID,StrCompany_ID,StrFullfillment_Center));
			
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_TeamMember_Name,ObjectLocationMaintenanceService.getTeamMemberName(StrTenant_ID, StrTeamMember));
			ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_CurrentDate,testDateString);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.LocationMaintenance_ScreenLabels, ObjectLocationMaintenanceService.getLocationMaintenanceLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			ObjModelAndView.addObject(GLOBALCONSTANT.BACKSTATUS,backSatus);
			//ObjModelAndView.addObject(GLOBALCONSTANT.Infohelp_ScreenLabels, ObjectInfohelpService.getInfoHelpAssignmentSearchLabels(GLOBALCONSTANT.InfoHelps_Language));
			//ObjectLocationMaintenanceService.addEntry(ObjectLocationBean);
			return ObjModelAndView;
		}catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}	

	}


	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 24, 2014
	 * Description this is used to Show the page of Location Maintenance
	 * @return ModelAndView
	 * @throws ParseException 
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Maintenance_Add , method = RequestMethod.GET)
	public ModelAndView addEntry(HttpServletRequest objHttpServletRequest,
			@ModelAttribute(GLOBALCONSTANT.Location_ModelAttribute) LOCATIONBEAN ObjectLocationBean) {	 

		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		String utcDate=objHttpServletRequest.getParameter(GLOBALCONSTANT.Last_Activity_DateH);

		
		ObjectLocationBean.setLast_Activity_Date(utcDate);
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_Search_Lookup);
		try{
			ObjectLocationMaintenanceService.addEntry(ObjectLocationBean);
		}catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}	
		return ObjModelAndView;
	}


	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description:It is used to show the data from LOCATIONS table behalf of search conditions
	 *@return
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Kendo_DisplayAll, method = RequestMethod.GET)
	public @ResponseBody  List<LOCATIONBEAN> getListKindo() {										
		List<LOCATIONBEAN> ListLocation = new ArrayList<LOCATIONBEAN>();			 
		try {
			ListLocation=ObjectLocationMaintenanceService.getList(StrLocation, StrFulfillmentCenter, StrArea, StrDescription, StrLocationType);					
		} catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ListLocation;
		}													
		return ListLocation;
	}

	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description:It is used to show data from LOCATION table,when we click in grid for edit record 
	 *@param request
	 *@return ModelAndView
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationMaintenance_Update, method = RequestMethod.GET)
	public ModelAndView EditEntry(HttpServletRequest request) {	 

		
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
		
		try{
			
			String StrFetchFulfillment_Center=request.getParameter(GLOBALCONSTANT.Location_FulfillmentCenter_JspPageValue);
			String StrFetchArea=request.getParameter(GLOBALCONSTANT.Location_Area_JspPageValue);
			String StrFetchLocation=request.getParameter(GLOBALCONSTANT.Location_Location_JspPageValue);
			LOCATIONBEAN ObjectLocation = ObjectLocationMaintenanceService.getFetchLocation(StrFetchFulfillment_Center,StrFetchArea,StrFetchLocation);
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Maintenance_Update,GLOBALCONSTANT.ObjectLocation, ObjectLocation);		
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectFullfillment,ObjectLocationMaintenanceService.getFulfillmentCenter(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.LocationEditScreen));
			
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLastActivityTask,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectArea,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLocationType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWizardID,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WIZARDS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWorkGroupZone,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GRPWRKZONE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWorkZone,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WORKZONES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectStorageType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STRGTYPES,GLOBALCONSTANT.BlankString));
			
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Pages,GLOBALCONSTANT.LocationEditScreen);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			
			ObjModelAndView.addObject(GLOBALCONSTANT.LocationMaintenance_ScreenLabels, ObjectLocationMaintenanceService.getLocationMaintenanceLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			return ObjModelAndView;
		}
		catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;}
	}

	/**
	 * 
	  * @author Aakash Bishnoi
	  * @Date Jan 6, 2015
	  * Description:It is used to save the updated record
	  *@param ObjectLocationBean
	  *@return
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationMaintenance_Update_Record , method = RequestMethod.GET)
	public ModelAndView UpdateEntry(@ModelAttribute(GLOBALCONSTANT.Location_ModelAttribute) LOCATIONBEAN ObjectLocationBean,HttpServletRequest request) {	 

		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String utcDate=request.getParameter(GLOBALCONSTANT.Last_Activity_DateH);
		String StrHiddenFulfillment_Center=request.getParameter(GLOBALCONSTANT.JspHiddenFullfillment_Center_ID);
		String StrHiddenArea=request.getParameter(GLOBALCONSTANT.JspHiddenArea);
		String StrHiddenLocation=request.getParameter(GLOBALCONSTANT.JspHiddenLocation);
		String StrHiddenLocation_Profile=request.getParameter(GLOBALCONSTANT.JspHiddenLocation_Profile);
		String StrHiddenWizard_ID=request.getParameter(GLOBALCONSTANT.JspHiddenWizard_ID);
		String StrHiddenWork_Group_Zone=request.getParameter(GLOBALCONSTANT.JspHiddenWork_Group_Zone);
		String StrHiddenWork_Zone=request.getParameter(GLOBALCONSTANT.JspHiddenWork_Zone);
		ObjectLocationBean.setTenant_ID(OBJCOMMONSESSIONBE.getTenant());
		ObjectLocationBean.setCompany_ID(OBJCOMMONSESSIONBE.getCompany());
		ObjectLocationBean.setFullfillment_Center_ID(StrHiddenFulfillment_Center);
		ObjectLocationBean.setArea(StrHiddenArea);
		ObjectLocationBean.setLocation(StrHiddenLocation);
		ObjectLocationBean.setLocation_Profile(StrHiddenLocation_Profile);
		ObjectLocationBean.setWizard_ID(StrHiddenWizard_ID);
		ObjectLocationBean.setWork_Group_Zone(StrHiddenWork_Group_Zone);
		ObjectLocationBean.setWork_Zone(StrHiddenWork_Zone);
		ObjectLocationBean.setLast_Activity_Date(utcDate);
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_Search_Lookup);
		try{
			ObjectLocationMaintenanceService.updateEntry(ObjectLocationBean);
		}catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}	
		return ObjModelAndView;
	}
	
	/**
	 * 
	 * @author Aakash Bishnoi
	 * @Date Jan 5, 2015
	 * Description:It is used to show data from LOCATION table,when we click in grid for edit record 
	 *@param request
	 *@return ModelAndView
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationMaintenance_CopyPage, method = RequestMethod.GET)
	public ModelAndView copyEntry(HttpServletRequest request) {	 

		
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
		String StrTeamMember=OBJCOMMONSESSIONBE.getTeam_Member();
		Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelps_yyyyMMdd_Format);
		String testDateString = ObjDateFormat.format(CurrentDate);	
		//String StrTeamMember=OBJCOMMONSESSIONBE.getTeam_Member();
		
		
		try{
			
			String StrFetchFulfillment_Center=request.getParameter(GLOBALCONSTANT.Location_FulfillmentCenter_JspPageValue);
			String StrFetchArea=request.getParameter(GLOBALCONSTANT.Location_Area_JspPageValue);
			String StrFetchLocation=request.getParameter(GLOBALCONSTANT.Location_Location_JspPageValue);
			LOCATIONBEAN ObjectLocation = ObjectLocationMaintenanceService.getFetchLocation(StrFetchFulfillment_Center,StrFetchArea,StrFetchLocation);
			ObjectLocation.setArea(GLOBALCONSTANT.BlankString);
			ObjectLocation.setFullfillment_Center_ID(GLOBALCONSTANT.BlankString);
			ObjectLocation.setLocation(GLOBALCONSTANT.BlankString);
			ObjectLocation.setAlternate_Location(GLOBALCONSTANT.BlankString);
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Maintenance_Copy,GLOBALCONSTANT.ObjectLocation, ObjectLocation);
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectFullfillment,ObjectLocationMaintenanceService.getFulfillmentCenter(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.LocationCopyScreen));
			
			
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLastActivityTask,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectArea,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectLocationType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWizardID,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WIZARDS,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWorkGroupZone,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GRPWRKZONE,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectWorkZone,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WORKZONES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Combobox_SelectStorageType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_STRGTYPES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_TeamMember_Name,ObjectLocationMaintenanceService.getTeamMemberName(StrTenant_ID, StrTeamMember));
			ObjModelAndView.addObject(GLOBALCONSTANT.InfoHelps_CurrentDate,testDateString);
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Pages,GLOBALCONSTANT.LocationCopyScreen);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.LocationMaintenance_ScreenLabels, ObjectLocationMaintenanceService.getLocationMaintenanceLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
			return ObjModelAndView;
		}
		catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
	}
	

	/**
	 * @author Aakash Bishnoi
	 * @Date Dec 19, 2014
	 * Description this is used to delete the selected menu messages
	 * @param request
	 * @return
	 */
	@RequestMapping(value = GLOBALCONSTANT.LocationMaintenance_Delete , method = RequestMethod.POST)
	public ModelAndView deleteEntry(HttpServletRequest request) {	 
		String StrUserID=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		/** Here we Check user is logged in or not*/
		
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Search_Execution);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		try{
			String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
			String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
			String StrFetchFulfillment_Center=request.getParameter(GLOBALCONSTANT.Location_FulfillmentCenter_JspPageValue);
			String StrFetchArea=request.getParameter(GLOBALCONSTANT.Location_Area_JspPageValue);
			String StrFetchLocation=request.getParameter(GLOBALCONSTANT.Location_Location_JspPageValue);


			ObjectLocationMaintenanceService.deleteEntry(StrTenant_ID,StrCompany_ID,StrFetchFulfillment_Center,StrFetchArea,StrFetchLocation);
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_SearchLookup);			
			return ObjModelAndView;
		}
		catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
	}
	
}
