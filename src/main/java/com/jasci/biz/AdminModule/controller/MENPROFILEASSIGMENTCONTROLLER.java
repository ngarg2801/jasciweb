/**
Date Developed  :Jan 5 2015
Developed by    : Sarvendra Tyagi
Description     :Menu Profile Assigment Controller to handle the requests and Response
 */

package com.jasci.biz.AdminModule.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.MENUPROFILEASSIGMENTLABELBE;
import com.jasci.biz.AdminModule.be.MENUPROFILEASSIGNMENTPOJOBE;

import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;

import com.jasci.biz.AdminModule.service.IMENUPROFILEASSIGMENTSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class MENPROFILEASSIGMENTCONTROLLER {
	
	//getting session object here using autowired
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	
	@Autowired
	IMENUPROFILEASSIGMENTSERVICE objMenuProfileAssigmentService;
	
	
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	MENUPROFILEASSIGMENTLABELBE objMenuprofileassigmentlabelbe=null;
	
	private String SearchValue=GLOBALCONSTANT.BlankString;
	private String SearchField=GLOBALCONSTANT.BlankString;
	private String TeamMemberID=GLOBALCONSTANT.BlankString;
	private String PartOfTeamMemberName=GLOBALCONSTANT.BlankString;
	
	static Logger log = LoggerFactory.getLogger(MENUPROFILEMAINTENANCECONTROLLER.class.getName());
	
	/** 
	 * @Description  :This method is used to map Menu Assigment Maintenance lookup screen
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Jan 5 2015
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Action, method = RequestMethod.GET)
	public ModelAndView MenuProfileAssigmentLookUp() throws JASCIEXCEPTION {
		
		ModelAndView objModelAndView=null;
		
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		
		try{
			
			objMenuprofileassigmentlabelbe=objMenuProfileAssigmentService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
			
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());	
		}
		
		
		objModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Page,GLOBALCONSTANT.ObjCommonSession,
				OBJCOMMONSESSIONBE);
		
		objModelAndView.addObject(GLOBALCONSTANT.RequestObjectMapping_MenuProfileAssigment_objMenuAssignmentLBL,objMenuprofileassigmentlabelbe);

		
		return objModelAndView;
	}
	
	/** 
	 * @Description  :This method is used to map Menu Assigment Maintenance screen
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Jan 5 2015
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentMaintenance_Action, method = RequestMethod.GET)
	public ModelAndView MenuProfileAssigmentMaintenance(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {
		
		ModelAndView objModelAndView=null;
		MENUPROFILEASSIGNMENTPOJOBE objMenuprofileassignmentpojobes=null;
		//List<GENERALCODES> objGeneralcodes=null;
		Boolean status=false,changedDropDown=false;
		

		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		
		try{
			
			objMenuprofileassigmentlabelbe=objMenuProfileAssigmentService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
			
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}
		
		
		
		String strDeviceType=OBJCOMMONSESSIONBE.getMenuType();
		String strTeamMember=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberID);
		String strPartTeamMemberName=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfileAssigment_lblPartOfTeamMemberNameID);
		String strstatus=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_Menu_LookUp);
		String strSelectedMenuType=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_MenuType);
		
		if(strstatus!=null){
			changedDropDown=true;
		if(strstatus.equalsIgnoreCase(GLOBALCONSTANT.strTrue)){
			
			status=true;
			
		}
		}
		
		if(strSelectedMenuType==null){
			strSelectedMenuType=GLOBALCONSTANT.BlankString;
		}
		
		if(strPartTeamMemberName==null){
			
			status=true;
		}
		
		
		try{
			
			objMenuprofileassignmentpojobes=objMenuProfileAssigmentService.getTeamMemberName(strTeamMember);
			
		}catch(Exception objException){
			log.error(objException.getMessage());
		}
		
		
		objModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentMaintenance_Page,GLOBALCONSTANT.ObjCommonSession,
				OBJCOMMONSESSIONBE);
		objModelAndView.addObject(GLOBALCONSTANT.RequestObjectMapping_MenuProfileAssigment_objMenuAssignmentLBL,objMenuprofileassigmentlabelbe);
		objModelAndView.addObject(GLOBALCONSTANT.RequestObjectMapping_MenuProfileAssigment_objMenuTypes,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_MENUTYPES,GLOBALCONSTANT.BlankString));
		objModelAndView.addObject(GLOBALCONSTANT.RequestObjectMapping_MenuProfileAssigment_objTeamMemberID,strTeamMember);
		objModelAndView.addObject(GLOBALCONSTANT.RequestObjectMapping_MenuProfileAssigment_objMenuProfileAssignmentPojo,objMenuprofileassignmentpojobes);
		objModelAndView.addObject(GLOBALCONSTANT.MenuProfile_Menu_LookUp,status);
		objModelAndView.addObject(GLOBALCONSTANT.objSelectedMenuType,strSelectedMenuType);
		objModelAndView.addObject(GLOBALCONSTANT.objLblTeamId,strTeamMember);
		objModelAndView.addObject(GLOBALCONSTANT.objchangedDropDown,changedDropDown);
		objModelAndView.addObject(GLOBALCONSTANT.MenuProfile_objDeviceType,strDeviceType.toUpperCase());
		
		
		return objModelAndView;
	}
	
	
	/** 
	 * @Description  :This method is used to map Menu Assigment Search lookup screen
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Jan 5 2015
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentSearchLookup_Action, method = RequestMethod.GET)
	public ModelAndView MenuProfileAssigmentSearchLookUp(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {
		
		ModelAndView objModelAndView=null;
			/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}

		TeamMemberID=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberID);
		PartOfTeamMemberName=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfileAssigment_lblPartOfTeamMemberNameID);

	
		
		if(PartOfTeamMemberName!=null && TeamMemberID !=null){
		 if((PartOfTeamMemberName!=null || (!PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString))) 
				 && (TeamMemberID ==null || TeamMemberID.equalsIgnoreCase(GLOBALCONSTANT.BlankString) )){
			
			SearchField=GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberName;
			SearchValue=PartOfTeamMemberName;
		}else if( (TeamMemberID !=null || (!TeamMemberID.equalsIgnoreCase(GLOBALCONSTANT.BlankString)) )
				&& (PartOfTeamMemberName==null || PartOfTeamMemberName.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){
			
			SearchField=GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberID;
			SearchValue=TeamMemberID;
			
		}else{
			
			SearchField=GLOBALCONSTANT.BlankString;
			SearchValue=GLOBALCONSTANT.BlankString;
		}
		}else{
			SearchField=GLOBALCONSTANT.BlankString;
			SearchValue=GLOBALCONSTANT.BlankString;
		}
		
		 try{
				
				objMenuprofileassigmentlabelbe=objMenuProfileAssigmentService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
				
			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
				
			}
			
		 
		 
		objModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentSearchLookup_Page,GLOBALCONSTANT.ObjCommonSession,
				OBJCOMMONSESSIONBE);
		objModelAndView.addObject(GLOBALCONSTANT.RequestObjectMapping_MenuProfileAssigment_objMenuAssignmentLBL,objMenuprofileassigmentlabelbe);
		
		return objModelAndView;
	}
	
	
	
	
	
	/** 
	 * @Description  :This method is used to map Menu Assigment Search looup comes from cancel
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Jan 5 2015
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileAssigment_SearchLookup_Action, method = RequestMethod.GET)
	public ModelAndView MenuProfileAssigmentSearchLookUpEdit(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {
		
		ModelAndView objModelAndView=null;
		
			/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		
		 try{
				
				objMenuprofileassigmentlabelbe=objMenuProfileAssigmentService.getScreenLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
				
			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
				
			}
			
		 
		 
		objModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentSearchLookup_Page,GLOBALCONSTANT.ObjCommonSession,
				OBJCOMMONSESSIONBE);
		objModelAndView.addObject(GLOBALCONSTANT.RequestObjectMapping_MenuProfileAssigment_objMenuAssignmentLBL,objMenuprofileassigmentlabelbe);
		
		return objModelAndView;
	}
	
	
	/** 
	 * @Description  :This method is used to delete assigned menu profile
	 * 
	 * @return 		 : objMENUPROFILEASSIGNMENTPOJOBE
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Jan 15 2015
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileAssigment_Delete_Action, method = RequestMethod.POST)
	public ModelAndView MenuProfileAssigmentDelete(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {
		
		ModelAndView objModelAndView=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		
		String strTeamMemberID=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberID);
		String tenant=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_Tenant);
		Boolean status=false;
		try{
		status=objMenuProfileAssigmentService.getAllAssignedMenuProfileDelete(strTeamMemberID, tenant);
	}catch(JASCIEXCEPTION objJasciexception){
		log.error(objJasciexception.getMessage());
		
	}
		if(status){
			
			objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentSearchLookup_Action);
		}else{
			objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentSearchLookup_Action);
		}
		
		return objModelAndView;
	}
	
	
	
	/** @Description  :This method is used to Update assigned menu profile
	 * 
	 * @return 		 : objMENUPROFILEASSIGNMENTPOJOBE
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Jan 15 2015
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileAssigment_SaveAndUpdate_Action, method = RequestMethod.POST)
	public ModelAndView SaveAndUpdate(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {
	
		ModelAndView objModelAndView=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		String strTeamMemberID=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfileAssigment_lblTeamMemberID);
		String strMenuAssignmentJson=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuAssignmentJson);
		String strDate=objHttpServletRequest.getParameter(GLOBALCONSTANT.LastActivityDateH);
		Boolean status=false;
		try{
			status=objMenuProfileAssigmentService.SaveAndUpdate(strTeamMemberID, strMenuAssignmentJson,strDate);
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			
		}
		
		
		if(status){
			
			objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentSearchLookup_Action);
		}else{
			objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuProfileAssigmentSearchLookup_Action);
		}
		
		return objModelAndView;
	}
	
	
	
	
	/** 
	 * @Description  :This method is used to bind data in kendo grid
	 * 
	 * @return 		 : objMENUPROFILEASSIGNMENTPOJOBE
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Jan 15 2015
	 * @Developed by : Sarvendra Tyagi
	 */
	
	@RequestMapping(value =GLOBALCONSTANT.RestFullMapping_Kendo_MenuProfileAssignment_List, method = RequestMethod.GET)
	public @ResponseBody List<MENUPROFILEASSIGNMENTPOJOBE> getList(){
		
		List<MENUPROFILEASSIGNMENTPOJOBE> objMenuprofileassignmentpojobesList=null;
		
		try{
			
			
			objMenuprofileassignmentpojobesList=objMenuProfileAssigmentService.getTeamMemberList(SearchValue, SearchField);
			
		}catch(JASCIEXCEPTION objJasciexception){
			
		}
		
		return objMenuprofileassignmentpojobesList;
	}
	
	
	

}
