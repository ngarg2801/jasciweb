/**

Date Developed :Dec 22 2014
Created by: Shailendra Kumar
Description :COMMONCLASSES Controller is used to check the Authentication of Users
 */
package com.jasci.biz.AdminModule.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.GETFULFILLMENTCENTERS;
import com.jasci.common.utilbe.GENERAL_CODES;
import com.jasci.common.utilbe.GETLANGUAGES;
import com.jasci.common.utilbe.TEAM_MEMBERS;
import com.jasci.common.util.ADDRESSVALIDATION;
import com.jasci.common.util.FULFILLMENTCENTERLOOKUP;
import com.jasci.common.util.GETINFOHELP;
import com.jasci.common.util.GETLANGUAGE;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.util.TEAMMEMBERLOOKUP;
import com.jasci.common.util.UPDATEINVENTORY;
import com.jasci.common.util.ZIPVALIDATION;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class COMMONCLASSES {

	@Autowired
	FULFILLMENTCENTERLOOKUP objfullfillmentcenter;
	@Autowired
	GETINFOHELP objgetinfohelp;
	@Autowired
	GETLANGUAGE objlanguge;
	@Autowired
	LOOKUPGENERALCODE objlookupgernalcode;
	@Autowired
	TEAMMEMBERLOOKUP objteammember;
	@Autowired
	UPDATEINVENTORY objupdateinventory;


	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_ResponseScreen_Actiontestcommonclasess, method = {RequestMethod.GET,RequestMethod.POST})
	@ExceptionHandler({JASCIEXCEPTION.class})
	public ModelAndView AuthenticateUser(@ModelAttribute(GLOBALCONSTANT.RequestMapping__LoginScreen_Validatetestcommonclasess) COMMONSESSIONBE ObjCommonSession, BindingResult ObjResult,HttpServletRequest request,HttpServletResponse response) throws JASCIEXCEPTION
	{
		ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.RequestMapping_ResponseScreen_Actiontestcommonclasess);
	
		try{


			if(!ObjCommonSession.getAddressLine1().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&!ObjCommonSession.getZipCode().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
				ADDRESSVALIDATION Objaddressvalidation = new ADDRESSVALIDATION();
				char result =  Objaddressvalidation.IsValidAddress(ObjCommonSession.getAddressLine1(),ObjCommonSession.getAddressLine2(),ObjCommonSession.getAddressLine3(),ObjCommonSession.getAddressLine4(),ObjCommonSession.getCity(),ObjCommonSession.getStateCode(),ObjCommonSession.getCountryCode(),ObjCommonSession.getZipCode());
				
				return ObjModelAndView.addObject(GLOBALCONSTANT.Addressvalidation,result);
			}else if(!ObjCommonSession.getTenant().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&!ObjCommonSession.getFulfillmentCenter().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&ObjCommonSession.getAttribute_Request().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&ObjCommonSession.getArea().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
				GETFULFILLMENTCENTERS Obj_FulfillmentCenterLookup = new GETFULFILLMENTCENTERS();
				Obj_FulfillmentCenterLookup = objfullfillmentcenter.GetFulfillmentCenterLookupRecord(ObjCommonSession);

				return ObjModelAndView.addObject(GLOBALCONSTANT.Location_Restfull_FulfillmentCenter,Obj_FulfillmentCenterLookup);
			}else if(!ObjCommonSession.getInfoHelp().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&!ObjCommonSession.getLanguage().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){

				objgetinfohelp.GetInfoHelp(ObjCommonSession.getInfoHelp(),ObjCommonSession.getLanguage(),ObjCommonSession.getInfoHelpType(),request,response);
							
				//return ObjModelAndView.addObject("InfoHelp",Obj_GETINFOHELPBE);
				return new ModelAndView(GLOBALCONSTANT.RequestMapping_ResponseScreen_Actiontestcommonclasess);
			}else if(!ObjCommonSession.getKeyPhrase().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&!ObjCommonSession.getLanguage().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
				GETLANGUAGES ObjLanguge=new GETLANGUAGES();
				ObjLanguge=objlanguge.getLanguage(ObjCommonSession.getKeyPhrase(),ObjCommonSession.getLanguage());
				return ObjModelAndView.addObject(GLOBALCONSTANT.LanguageVal,ObjLanguge);
			}else if(!ObjCommonSession.getAttribute_Request().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&!ObjCommonSession.getDrop_Down_Selection().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){

				List<GENERAL_CODES> Obj_LookupGeneralCodeList = new ArrayList<GENERAL_CODES>();
				GENERAL_CODES Obj_LookupGeneralCode = new GENERAL_CODES();
				Obj_LookupGeneralCodeList = objlookupgernalcode.GetLookupGeneralCode(ObjCommonSession);

				ObjModelAndView.addObject(GLOBALCONSTANT.LookupGeneralCode,Obj_LookupGeneralCode);
				ObjModelAndView.addObject(GLOBALCONSTANT.Attrebutes_Request,ObjCommonSession.getAttribute_Request().toUpperCase());
				return ObjModelAndView.addObject(GLOBALCONSTANT.LookupGeneralCodeList,Obj_LookupGeneralCodeList);
			}else if(!ObjCommonSession.getTeam_Member().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&ObjCommonSession.getAddressLine2().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&ObjCommonSession.getQuality().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
				TEAM_MEMBERS Obj_TeammemberLookup = new TEAM_MEMBERS();
				//Obj_TeammemberLookup = objteammember.GetTeammemberDetail(ObjCommonSession.getTeam_Member());	
				return ObjModelAndView.addObject(GLOBALCONSTANT.TeammemberLookup,Obj_TeammemberLookup);
			}else if(!ObjCommonSession.getQuality().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&!ObjCommonSession.getProduct().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){	
				String Result = objupdateinventory.addorUpdateINVENTORY(ObjCommonSession);
				return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ResponseObject,Result);
			}else if(!ObjCommonSession.getZipCode().equalsIgnoreCase(GLOBALCONSTANT.BlankString)&&ObjCommonSession.getAddressLine1().equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
				ZIPVALIDATION objzipvalidation = new ZIPVALIDATION();
				char result = objzipvalidation.IsValidZipcode(ObjCommonSession.getZipCode());
				
				return ObjModelAndView.addObject(GLOBALCONSTANT.zipvalidation,result);
			}
		}catch(JASCIEXCEPTION objexception){

			return  ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ResponseObject,objexception.getMessage());

		}catch (Exception e) {
			return  ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ResponseObject,e.getMessage());
		}

		return  ObjModelAndView;
	}

}
