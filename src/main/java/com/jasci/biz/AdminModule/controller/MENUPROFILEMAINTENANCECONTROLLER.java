/**

Date Developed  :Dec 25 2014
Developed by    : Sarvendra Tyagi
Description     :Menu Profile maintenance Controller to handle the requests
 */



package com.jasci.biz.AdminModule.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.MENUPROFILEBEANPOJOBE;
import com.jasci.biz.AdminModule.be.MENUPROFILEMAINTENANCELABELBE;
import com.jasci.biz.AdminModule.model.MENUAPPICONS;
import com.jasci.biz.AdminModule.model.MENUOPTIONS;
import com.jasci.biz.AdminModule.model.MENUPROFILEHEADER;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.IMENUPROFILEMAINTENANCESERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class MENUPROFILEMAINTENANCECONTROLLER {



	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;

	@Autowired
	IMENUPROFILEMAINTENANCESERVICE objMenuProfileMaintenanceService;
	
	@Autowired
	SCREENACCESSSERVICE screenAccessService;

	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	
	String strPartOfMenuProfileDescription=GLOBALCONSTANT.BlankString;
	String strMenuType=GLOBALCONSTANT.BlankString;
	static Logger log = LoggerFactory.getLogger(MENUPROFILEMAINTENANCECONTROLLER.class.getName());

	/** 
	 * @Description  :This method is used to map Menu Profile Maintenance lookup screen
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Dec 25 2014
	 * @Developed by : Sarvendra Tyagi
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Action, method = RequestMethod.GET)
	public ModelAndView MenuProfileMaintenanceLookUp() throws JASCIEXCEPTION {

		ModelAndView objModelView=null;
		MENUPROFILEMAINTENANCELABELBE objMenuprofilemaintenancelabelbe=null;
		//List<GENERALCODES> objGeneralcodesList=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelView;

		}
		
		
	
		try{

			//get labels of screen from language table
			objMenuprofilemaintenancelabelbe=objMenuProfileMaintenanceService.getLanguageLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
			objModelView = new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Page,GLOBALCONSTANT.ObjCommonSession,
					OBJCOMMONSESSIONBE);
			objModelView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuprofilemaintenancelabelbe);
			objModelView.addObject(GLOBALCONSTANT.MappingObject_MENUPROFILE_objGeneralCodeMenuType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_MENUTYPES,GLOBALCONSTANT.BlankString)
);
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}

		return objModelView;
	}

	
	/** 
	 * @Description  :This method is used to map Menu Profile Maintenance search lookup screen
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Dec 25 2014
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfile_Menu_Profile_searchLookup, method = RequestMethod.GET)
	public ModelAndView MenuProfileMaintenanceSearchLook(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {

		ModelAndView objModelView=null;
		MENUPROFILEMAINTENANCELABELBE objMenuprofilemaintenancelabelbe=null;

		try{
			
			objMenuprofilemaintenancelabelbe=objMenuProfileMaintenanceService.getLanguageLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			
			
		}
		
	/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelView;

		}
		
		objModelView = new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceSearchLookUp_Page,GLOBALCONSTANT.ObjCommonSession,
				OBJCOMMONSESSIONBE);
		objModelView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuprofilemaintenancelabelbe);
		
		return objModelView;
		
	}


	/** 
	 * @Description  :This method is used to map Menu Profile Maintenance search lookup screen
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Dec 25 2014
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceSearchLookUp_Action, method = RequestMethod.GET)
	public ModelAndView MenuProfileMaintenanceSearchLookUp(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {

		ModelAndView objModelView=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelView;

		}

		strPartOfMenuProfileDescription=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUProfile_lblPartMenuProfileDescription);
		strMenuType=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_lblMenuType);
		MENUPROFILEMAINTENANCELABELBE objMenuprofilemaintenancelabelbe=null;
		
		try{

			objMenuprofilemaintenancelabelbe=objMenuProfileMaintenanceService.getLanguageLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
		
			
			
		objModelView = new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceSearchLookUp_Page,GLOBALCONSTANT.ObjCommonSession,
				OBJCOMMONSESSIONBE);
		objModelView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuprofilemaintenancelabelbe);
		
		
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}

		return objModelView;
	}

	
	/**
	 *Description: This function is showing Menu option list on MenuOption Maintenance search LookUp screen page in kendo grid
	 *Input parameter none
	 *Return Type: objMenuoptions
	 *Created By: Sarvendra Tyagi
	 *Created Date:Dec 17 2014 
	 */
	@RequestMapping(value = GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_RestFull_Kendo, method = RequestMethod.GET)
	public @ResponseBody  List<MENUPROFILEHEADER>  getkendoList() throws JASCIEXCEPTION{	
		List<MENUPROFILEHEADER> objMenuHeaderList=null;
		try{

			if((strPartOfMenuProfileDescription!=null) && (!strPartOfMenuProfileDescription.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){

				objMenuHeaderList=objMenuProfileMaintenanceService.getProfileHeaderList(strPartOfMenuProfileDescription,
						GLOBALCONSTANT.MENUProfile_lblPartMenuProfileDescription);
			}else
			
			if((strMenuType!=null) && (!strMenuType.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){

				objMenuHeaderList=objMenuProfileMaintenanceService.getProfileHeaderList(strMenuType, GLOBALCONSTANT.MENUOPTION_lblMenuType);
			}else{
				
				
				objMenuHeaderList=objMenuProfileMaintenanceService.getProfileHeaderList(GLOBALCONSTANT.BlankString, GLOBALCONSTANT.BlankString);
			}
			
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}

		return objMenuHeaderList;
	}

	
	/** 
	 * @Description  :This method is used to map Menu Profile Maintenance lookup screen
	 * 
	 * @return 		 : objModelView
	 * @throws  	 : JASCIEXCEPTION
	 * @Date		 : Dec 25 2014
	 * @Developed by : Sarvendra Tyagi
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileMaintenance_Action, method = RequestMethod.GET)
	public ModelAndView MenuProfileMaintenance(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION {

		
		
		ModelAndView objModelView=null;
		MENUPROFILEBEANPOJOBE objMenuprofileheader=null;
		//List<GENERALCODES> objGeneralcodesMenuTypeList,objGeneralcodesApplicationList=null;
		List<MENUAPPICONS> objMenuappicons;
		List<MENUOPTIONS> obMenuoptions=null;
		Boolean screenStatus=false;
		
		
		MENUPROFILEMAINTENANCELABELBE objMenuprofilemaintenancelabelbe=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelView;

		}	
		
		String strSelectedMenutype=GLOBALCONSTANT.BlankString,strSelectedApplication=GLOBALCONSTANT.BlankString;
		String strLastActivityTeamMember=GLOBALCONSTANT.BlankString,lstDateActivity=GLOBALCONSTANT.BlankString;
		String strshortDescription=GLOBALCONSTANT.BlankString,strLongDescription=GLOBALCONSTANT.BlankString;
		String strHelpLine=GLOBALCONSTANT.BlankString,strSelectedStatus=GLOBALCONSTANT.BlankString,strAppIcon=GLOBALCONSTANT.BlankString;
		String strMenuProfile=GLOBALCONSTANT.BlankString;
		Boolean selectedChanged=false;
		String strEditScreen=GLOBALCONSTANT.BlankString,strDeviceType=GLOBALCONSTANT.BlankString;
		try{
			
			strDeviceType=OBJCOMMONSESSIONBE.getMenuType();
			
			
			strSelectedMenutype=objHttpServletRequest.getParameter(GLOBALCONSTANT.StrMenuType);
			strSelectedApplication=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUOPTION_lblApplication);
			strLastActivityTeamMember=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_lbllastActivityTeamMember);
			lstDateActivity=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_lbllastDate);
			strshortDescription=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_lbldescShort);
			strLongDescription=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_lbldescLong);
			strHelpLine=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_lblhelpLine);
			strSelectedStatus=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_Menu_LookUp);
			strAppIcon=objHttpServletRequest.getParameter(GLOBALCONSTANT.MenuProfile_lblAppIcon);
			strMenuProfile=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUProfile_lblMenuProfile);
			
			strEditScreen=objHttpServletRequest.getParameter(GLOBALCONSTANT.lblEdit);
			if(strSelectedMenutype==null && strSelectedApplication==null && strLastActivityTeamMember==null && lstDateActivity==null 
					&& strshortDescription==null && strLongDescription==null && strHelpLine==null && strSelectedStatus==null
					&& strAppIcon==null){
				
				selectedChanged=true;
				
			}else if(strSelectedMenutype.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && strSelectedApplication.equalsIgnoreCase(GLOBALCONSTANT.BlankString)
					&& strLastActivityTeamMember.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && lstDateActivity.equalsIgnoreCase(GLOBALCONSTANT.BlankString) 
					&& strshortDescription.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && strLongDescription.equalsIgnoreCase(GLOBALCONSTANT.BlankString) &&
					strHelpLine.equalsIgnoreCase(GLOBALCONSTANT.BlankString) && strSelectedStatus.equalsIgnoreCase(GLOBALCONSTANT.BlankString)
					&& strAppIcon.equalsIgnoreCase(GLOBALCONSTANT.BlankString)){
				selectedChanged=true;
			}else {
				objMenuprofileheader=new MENUPROFILEBEANPOJOBE();
				objMenuprofileheader.setMenuType(strSelectedMenutype);
				objMenuprofileheader.setAppIcon(strAppIcon);
				objMenuprofileheader.setDescription20(StringEscapeUtils.escapeHtml(strshortDescription));
				objMenuprofileheader.setDescription50(StringEscapeUtils.escapeHtml(strLongDescription));
				objMenuprofileheader.setHelpLine(StringEscapeUtils.escapeHtml(strHelpLine));
				objMenuprofileheader.setLastActivityDate(lstDateActivity);
				objMenuprofileheader.setLastActivityTeamMember(strLastActivityTeamMember);
				objMenuprofileheader.setMenuProfile(StringEscapeUtils.escapeHtml(strMenuProfile));
				
				
				
			}
			
			
		}catch(Exception objException){
			
			log.error(objException.getMessage());
		}
		
		try{

			
			String strMenuDescription=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUProfile_lblPartMenuProfileDescription);
			
			objMenuprofilemaintenancelabelbe=objMenuProfileMaintenanceService.getLanguageLabel(OBJCOMMONSESSIONBE.getCurrentLanguage());
			
			try{
				if(strMenuProfile!=null && (!strMenuProfile.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){
					
					//getting value from table whic populate on jsp page
					if(selectedChanged){
					objMenuprofileheader=objMenuProfileMaintenanceService.getMenuProfileList(strMenuProfile);
					
					obMenuoptions=objMenuProfileMaintenanceService.getAssignedMenurofile(OBJCOMMONSESSIONBE.getTenant(), strMenuProfile);

					}
							}else{
					if(selectedChanged){
					objMenuprofileheader=objMenuProfileMaintenanceService.getDateAndTeamMember(OBJCOMMONSESSIONBE.getTenant(),
							OBJCOMMONSESSIONBE.getTeam_Member());
					}
					
				}
			}catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			
				
			}
			//
			if(strMenuDescription!=null && strMenuProfile!=null){
				if(selectedChanged){
			if(strMenuProfile!=null && (!strMenuProfile.equalsIgnoreCase(GLOBALCONSTANT.BlankString))){
				strEditScreen=GLOBALCONSTANT.MenuProfile_CheckVariable_EditScreen;
			}
				}
				screenStatus=true;
				
				
			}else if(strMenuDescription==null && strMenuProfile==null){
				
					screenStatus=false;
			}else{
				strEditScreen=GLOBALCONSTANT.MenuProfile_CheckVariable_EditScreen;
				screenStatus=false;
			}
			objMenuappicons=objMenuProfileMaintenanceService.getMenuAppIconList();
			
		objModelView = new ModelAndView(GLOBALCONSTANT.RequestMapping_MenuProfileMaintenance_Page,GLOBALCONSTANT.ObjCommonSession,
				OBJCOMMONSESSIONBE);
		
		objModelView.addObject(GLOBALCONSTANT.objMenuOptionLabel,objMenuprofilemaintenancelabelbe);
		objModelView.addObject(GLOBALCONSTANT.MappingObject_MENUPROFILE_objHeader,objMenuprofileheader);
		objModelView.addObject(GLOBALCONSTANT.MappingObject_MENUPROFILE_objGeneralCodeMenuType,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_MENUTYPES,GLOBALCONSTANT.BlankString)
);
		objModelView.addObject(GLOBALCONSTANT.MappingObject_MENUPROFILE_objGeneralCodeApplication,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString)
);
		objModelView.addObject(GLOBALCONSTANT.MappingObject_MENUPROFILE_objMenuAppIcon,objMenuappicons);
		objModelView.addObject(GLOBALCONSTANT.MenuProfile_CheckVariable_objIsEdit,strEditScreen);
		
		objModelView.addObject(GLOBALCONSTANT.MenuProfile_CheckVariable_MenuOptionList,obMenuoptions);
		objModelView.addObject(GLOBALCONSTANT.MenuProfile_Menu_LookUp,screenStatus);
		objModelView.addObject(GLOBALCONSTANT.MenuProfile_objChangeDropDown,selectedChanged);
		objModelView.addObject(GLOBALCONSTANT.MenuProfile_objApplication,strSelectedApplication);
		objModelView.addObject(GLOBALCONSTANT.MenuProfile_objDeviceType,strDeviceType.toUpperCase());
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		
		return objModelView;
	}

	
	/**
	 *Description: This function  redirect to Menu Profile maintenance new screen page for update MenuProfile  
	 *Input parameter: none
	 *Return Type :"ModelAndView"
	 *Created By :"Sarvendra Tyagi"
	 *Created Date: "Dec 29 2014" 
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.REQUESTMAPPING_MENUProfiles_MaintenanceUpdate_Action, method = RequestMethod.POST)
	public ModelAndView updateEntry(@ModelAttribute(GLOBALCONSTANT.REQUESTMAPPING_MENUProfile_Maintenance_ListPojoObject)  MENUPROFILEBEANPOJOBE objectMenuprofileheader,HttpServletRequest request,HttpServletResponse response){

		Boolean UpdateStatus=false;
		ModelAndView objModelAndView=null;
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		String strAssignOption=request.getParameter(GLOBALCONSTANT.MENUProfile_lblAssignList);
		String strDate=request.getParameter(GLOBALCONSTANT.LastActivityDateH);
		try{
			
			objectMenuprofileheader.setLastActivityDate(strDate);

			
			
			UpdateStatus=objMenuProfileMaintenanceService.SaveOrUpdate(objectMenuprofileheader,strAssignOption);
		
			if(UpdateStatus){
				
			}
		strMenuType=GLOBALCONSTANT.BlankString;
		strPartOfMenuProfileDescription=GLOBALCONSTANT.BlankString;
		objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceSearchLookUp_Action);
		
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}
		
		return objModelAndView;
	}
	
	
	
	/** 
	 * @Description  :This method is used for delete records
	 * @param		 : objHttpServletRequest
	 * @return		 :objModelAndView
	 * @throws 		 :JASCIEXCEPTION 
	 * @Developed by :sarendra tyagi
	 * @Date         : Dec 30 2014
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_MenuProfileMaintenance_Delete_Action, method = RequestMethod.POST)
	public ModelAndView DeleteMenuProfile(HttpServletRequest objHttpServletRequest) throws JASCIEXCEPTION{

		ModelAndView objModelAndView=null;
		
		/** Here we Check user already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceLookUp_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			objModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return objModelAndView;

		}
		
		try{
			String strMenuProfile=objHttpServletRequest.getParameter(GLOBALCONSTANT.MENUPROFILE_MenuProfile);
			String strTenant=objHttpServletRequest.getParameter(GLOBALCONSTANT.Parameter_MenuOption_Tenant);
			objMenuProfileMaintenanceService.deleteMenuProfile(strMenuProfile,strTenant);
			objModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuProfileMaintenanceSearchLookUp_Action);
			
		}catch(JASCIEXCEPTION objJasciexception){
			
			log.error(objJasciexception.getMessage());
		}
		
		return objModelAndView;
	}

}
