/**

Description make a controller of LOCATIONS-Profile class for mapping with View Of Locations Profile Maintenance Screen
Created By Diksha Gupta
Created Date Dec 24 2014
 */
package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.LOCATIONPROFILEBE;
import com.jasci.biz.AdminModule.be.LOCATIONPROFILESCREENLABELSBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ILOCATIONPROFILESERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class LOCATIONPROFILEMAINTENANCECONTROLLER {

	@Autowired
	COMMONSESSIONBE objCommonsessionbe;
	@Autowired
	ILOCATIONPROFILESERVICE objIlocationprofileservice;
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	/** It is used to logger the exception*/
	static Logger log = LoggerFactory.getLogger(LOCATIONPROFILEMAINTENANCECONTROLLER.class.getName());
	ModelAndView ObjModelAndView=new ModelAndView();
	String StrLocationProfile=GLOBALCONSTANT.BlankString;
	String StrProfileGroup=GLOBALCONSTANT.BlankString;
	String StrPartOfDescription=GLOBALCONSTANT.BlankString;
	String StrSavedMessage=GLOBALCONSTANT.BlankString;
	String StrUpdatedMessage=GLOBALCONSTANT.BlankString;
	LOCATIONPROFILEBE objLocationProfileBeEdit=null;
	LOCATIONPROFILEBE objLocationProfileBeCopy=null;
	String CopyOrEdit=GLOBALCONSTANT.BlankString;
	String StrReassignprofileGroup=GLOBALCONSTANT.BlankString;
	String StrReassignfulFillmentCenter=GLOBALCONSTANT.BlankString;
	String StrReassignlocationProfile=GLOBALCONSTANT.BlankString;

	String NumberOfLocations=GLOBALCONSTANT.BlankString;

	/**
	 * @author Diksha Gupta
	 * @Date Dec 24, 2014
	 * Description this is used to Show the page of Location Profile Maintenance Search.
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_SearchAction , method = RequestMethod.GET)
	public ModelAndView getLocationProfileSearch() {	 

		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();


		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		LOCATIONPROFILESCREENLABELSBE ObjLocationLabels=new LOCATIONPROFILESCREENLABELSBE();

		try {
			ObjLocationLabels= objIlocationprofileservice.getScreenLabels(objCommonsessionbe.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Profile_SearchPage);
		try {
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeProfileGroup,ObjLookUpGeneralCode.Drop_Down_Selection(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationsProfileLabels,ObjLocationLabels);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
		return ObjModelAndView;
	}

	/**
	 * @author Diksha Gupta
	 * @Date Dec 24, 2014
	 * Description this is used to Show the page of Location Profile Maintenance Search Lookup.
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_SearchLookupAction , method = RequestMethod.GET)
	public ModelAndView getLocationProfileSearchLookUp(HttpServletRequest request) 
	{	 

		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		StrLocationProfile=request.getParameter(GLOBALCONSTANT.StrLocationProfileParam);
		StrProfileGroup=request.getParameter(GLOBALCONSTANT.StrLocationProfileGroup);
		StrPartOfDescription=request.getParameter(GLOBALCONSTANT.StrLocationProfileDescription);
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_Profile_SearchLookup);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);

		return ObjModelAndView;
	}


	/**
	 * @author Diksha Gupta
	 * @Date Dec 24, 2014
	 * Description this is used to Show the page of Location Profile Maintenance Search Lookup.
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_SearchLookup , method = RequestMethod.GET)
	public ModelAndView getLocationProfileSearchLookUp() 
	{	 

		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();

		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		//String Value=GLOBALCONSTANT.BlankString;
		ObjectMapper mapper = new ObjectMapper();

		LOCATIONPROFILESCREENLABELSBE ObjLocationLabels=new LOCATIONPROFILESCREENLABELSBE();

		try {
			ObjLocationLabels= objIlocationprofileservice.getScreenLabels(objCommonsessionbe.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Profile_SearchLookupPage);
		List<String> arrList=new ArrayList<String>();
		try {
			arrList=objIlocationprofileservice.GetDistinctLocationProfile();
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		String json = GLOBALCONSTANT.BlankString;
		try {
			json = mapper.writeValueAsString(arrList);
		} catch (Exception objJasciexception) {
			log.error(objJasciexception.getMessage());
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationProfileList,json);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationsProfileLabels,ObjLocationLabels);

		return ObjModelAndView;
	}



	/**
	 * @author Diksha Gupta
	 * @Date Dec 24, 2014
	 * Description this is used to Show the page of Location Profile Maintenance to add a new record.
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_MaintenanceAction , method = RequestMethod.GET)
	public ModelAndView getLocationProfileMaintenance() 
	{	 

		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();


		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
		String testDateString = ObjDateFormat.format(CurrentDate);
		List<FULFILLMENTCENTERSPOJO> objFulfillmentCenter=null;

		try {
			objFulfillmentCenter=objIlocationprofileservice.getFulfillmentCenter(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.Copy);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		LOCATIONPROFILESCREENLABELSBE ObjLocationLabels=new LOCATIONPROFILESCREENLABELSBE();

		try {
			ObjLocationLabels= objIlocationprofileservice.getScreenLabels(objCommonsessionbe.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		StrSavedMessage=GLOBALCONSTANT.BlankString;
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Profile_MaintenancePage);
		try {
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeProfileGroup,ObjLookUpGeneralCode.Drop_Down_Selection(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.GIC_LOCPFGRP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStorageType,ObjLookUpGeneralCode.Drop_Down_Selection(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.GIC_STRGTYPES,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLocationType,ObjLookUpGeneralCode.Drop_Down_Selection(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.FulfilmentCenterList,objFulfillmentCenter);
		ObjModelAndView.addObject(GLOBALCONSTANT.LastActivityDate,testDateString);
		ObjModelAndView.addObject(GLOBALCONSTANT.LastActivityTeamMember,objCommonsessionbe.getTeam_Member_Name());
		ObjModelAndView.addObject(GLOBALCONSTANT.StrLocationProfileSaved,StrSavedMessage);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationsProfileLabels,ObjLocationLabels);


		return ObjModelAndView;
	}

	/**
	 * @author Diksha Gupta
	 * @Date Dec 30, 2014
	 * Description: This function is used to get data from LocationProfileMaintenance Page and to add that record to database .
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_InsertAction , method = RequestMethod.POST)
	public ModelAndView addLocationProfile(HttpServletRequest objHttpServletRequest,@ModelAttribute(GLOBALCONSTANT.ModelAttribute_Locations_Profile_Insert)LOCATIONPROFILEBE objLocationProfileBe) 
	{	 

		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		
		String utcDate=objHttpServletRequest.getParameter(GLOBALCONSTANT.Last_Activity_DateH);
		objLocationProfileBe.setLastActivityDate(utcDate);	
		try 
		{
			objIlocationprofileservice.addLocationProfile(objLocationProfileBe);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		LOCATIONPROFILESCREENLABELSBE ObjLocationLabels=new LOCATIONPROFILESCREENLABELSBE();

		try {
			ObjLocationLabels= objIlocationprofileservice.getScreenLabels(objCommonsessionbe.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		StrSavedMessage=GLOBALCONSTANT.StrLocationProfileSaved;
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Profile_MaintenancePage);
		ObjModelAndView.addObject(GLOBALCONSTANT.StrLocationProfileSaved,StrSavedMessage);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationsProfileLabels,ObjLocationLabels);

		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);

		return ObjModelAndView;

	}	



	/**
	 * @author Diksha Gupta
	 * @Date Dec 30, 2014
	 * Description: This function is used to get data from LocationProfileMaintenance Page and to delete that record.
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_DeleteAction , method = RequestMethod.GET)
	public ModelAndView DeleteLocationProfile(HttpServletRequest request) 
	{	 
		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		String LocationProfile=request.getParameter(GLOBALCONSTANT.RestParam_Profile);
		String ProfileGroup=request.getParameter(GLOBALCONSTANT.RestParam_ProfileGroup);
		String FulfillmentCenter=request.getParameter(GLOBALCONSTANT.RestParam_FulFillmentCenter);

		
		try {
			objIlocationprofileservice.DeleteLocationProfile(LocationProfile,ProfileGroup,FulfillmentCenter);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_Profile_SearchLookup);

	}	


	/**
	 * @author Diksha Gupta
	 * @Date Jan 05 2015
	 * Description: This function is used to get data for updation.
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationProfile_maintenance_edit , method = RequestMethod.GET)
	public ModelAndView editLocationProfile(HttpServletRequest request) 
	{	 
		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		String LocationProfile=request.getParameter(GLOBALCONSTANT.RestParam_Profile);
		String ProfileGroup=request.getParameter(GLOBALCONSTANT.RestParam_ProfileGroup);
		String FulfillmentCenter=request.getParameter(GLOBALCONSTANT.RestParam_FulFillmentCenter);
		NumberOfLocations=request.getParameter(GLOBALCONSTANT.NumberOfLocations);

		LOCATIONPROFILEBE objListLocationProfilebe=new LOCATIONPROFILEBE();

		try {
			objListLocationProfilebe=objIlocationprofileservice.editLocationProfile(LocationProfile,ProfileGroup,FulfillmentCenter);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		objLocationProfileBeEdit=objListLocationProfilebe;
		CopyOrEdit=GLOBALCONSTANT.Edit;
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_Profile_Maintenance_EditAction); 

		return ObjModelAndView;

	}	


	/**
	 * @author Diksha Gupta
	 * @Date Jan 05 2015
	 * Description: This function is used to get data for updation.
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_Maintenance_EditAction , method = RequestMethod.GET)
	public ModelAndView getEditPage(HttpServletRequest request) 
	{	 
		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
		String testDateString = ObjDateFormat.format(CurrentDate);

	
		List<FULFILLMENTCENTERSPOJO> objFulfillmentCenter=null;

		StrUpdatedMessage=GLOBALCONSTANT.BlankString;
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Profile_Maintenance_EditCopy_Page); 
		objLocationProfileBeEdit.setNumberOfLocations(NumberOfLocations);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationProfileBe, objLocationProfileBeEdit);
		try {
			objFulfillmentCenter=objIlocationprofileservice.getFulfillmentCenter(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),CopyOrEdit);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		LOCATIONPROFILESCREENLABELSBE ObjLocationLabels=new LOCATIONPROFILESCREENLABELSBE();

		try {
			ObjLocationLabels= objIlocationprofileservice.getScreenLabels(objCommonsessionbe.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		ObjModelAndView.addObject(GLOBALCONSTANT.StrLocationProfileUpdated,StrUpdatedMessage);

		try {
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeProfileGroup,ObjLookUpGeneralCode.Drop_Down_Selection(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.GIC_LOCPFGRP,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeStorageType,ObjLookUpGeneralCode.Drop_Down_Selection(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.GIC_STRGTYPES,GLOBALCONSTANT.BlankString));
			ObjModelAndView.addObject(GLOBALCONSTANT.GeneralCodeLocationType,ObjLookUpGeneralCode.Drop_Down_Selection(objCommonsessionbe.getTenant(),objCommonsessionbe.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
			} catch (JASCIEXCEPTION objJasciexception) {
				log.error(objJasciexception.getMessage());
				return ObjModelAndView;
			}
		ObjModelAndView.addObject(GLOBALCONSTANT.FulfilmentCenterList,objFulfillmentCenter);
		ObjModelAndView.addObject(GLOBALCONSTANT.CopyOrEdit,CopyOrEdit);
		ObjModelAndView.addObject(GLOBALCONSTANT.LastActivityDate,testDateString);
		ObjModelAndView.addObject(GLOBALCONSTANT.LastActivityTeamMember,objCommonsessionbe.getTeam_Member_Name());
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationsProfileLabels,ObjLocationLabels);

		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);

		return ObjModelAndView;

	}	

	/**
	 * @author Diksha Gupta
	 * @Date Jan 05 2015
	 * Description: This function is used to get data to copy location profile.
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationProfile_maintenance_Copy , method = RequestMethod.GET)
	public ModelAndView copyLocationProfile(HttpServletRequest request) 
	{	 
		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String LocationProfile=request.getParameter(GLOBALCONSTANT.RestParam_Profile);
		String ProfileGroup=request.getParameter(GLOBALCONSTANT.RestParam_ProfileGroup);
		String FulfillmentCenter=request.getParameter(GLOBALCONSTANT.RestParam_FulFillmentCenter);


		LOCATIONPROFILEBE objListLocationProfilebe=new LOCATIONPROFILEBE();

		try {
			objListLocationProfilebe=objIlocationprofileservice.editLocationProfile(LocationProfile,ProfileGroup,FulfillmentCenter);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		objListLocationProfilebe.setLocationProfile(GLOBALCONSTANT.BlankString);
		objLocationProfileBeEdit=objListLocationProfilebe;
		CopyOrEdit=GLOBALCONSTANT.Copy;

		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Locations_Profile_Maintenance_EditAction); 

		return ObjModelAndView;

	}	

	/**
	 * @author Diksha Gupta
	 * @Date Dec 30, 2014
	 * Description: This function is used to get data from LocationProfileMaintenance Page and to add that record to database .
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_CopyAction , method = RequestMethod.POST)
	public ModelAndView copyLocationProfile(HttpServletRequest request,@ModelAttribute(GLOBALCONSTANT.ModelAttribute_Locations_Profile_Insert)LOCATIONPROFILEBE objLocationProfileBe) 
	{	 
		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		String utcDate=request.getParameter(GLOBALCONSTANT.Last_Activity_DateH);
		objLocationProfileBe.setLastActivityDate(utcDate);		
		try 
		{
			objIlocationprofileservice.addLocationProfile(objLocationProfileBe);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}


		LOCATIONPROFILESCREENLABELSBE ObjLocationLabels=new LOCATIONPROFILESCREENLABELSBE();

		try {
			ObjLocationLabels= objIlocationprofileservice.getScreenLabels(objCommonsessionbe.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		StrUpdatedMessage=GLOBALCONSTANT.StrLocationProfileSaved;
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Profile_Maintenance_EditCopy_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationsProfileLabels,ObjLocationLabels);

		ObjModelAndView.addObject(GLOBALCONSTANT.StrLocationProfileUpdated,StrUpdatedMessage);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);

		return ObjModelAndView;

	}	

	/**
	 * @author Diksha Gupta
	 * @Date Dec 30, 2014
	 * Description: This function is used to get data from LocationProfileMaintenance Page and to add that record to database .
	 * @return ModelAndView
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Locations_Profile_EditCopyAction , method = RequestMethod.POST)
	public ModelAndView updateCopyLocationProfile(@ModelAttribute(GLOBALCONSTANT.ModelAttribute_Locations_Profile_Insert)LOCATIONPROFILEBE objLocationProfileBe,HttpServletRequest request) 
	{	 
		String StrUserID=objCommonsessionbe.getUserID();
		String StrPassword=objCommonsessionbe.getPassword();
		if(GLOBALCONSTANT.BlankString.equals(StrUserID) || GLOBALCONSTANT.BlankString.equals(StrPassword))
		{
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(objCommonsessionbe.getTenant(), objCommonsessionbe.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_Profile_maintenance_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		
		String FulFillmentCenter=request.getParameter(GLOBALCONSTANT.FulFillmentCenterTextBox);
		String ProfileGroupTextBox=request.getParameter(GLOBALCONSTANT.ProfileGroupTextBox);
		String LocationProfile=request.getParameter(GLOBALCONSTANT.LocationProfileTextBox);
		String utcDate=request.getParameter(GLOBALCONSTANT.Last_Activity_DateH);
		objLocationProfileBe.setLastActivityDate(utcDate);	
		objLocationProfileBe.setFulFillmentCenter(FulFillmentCenter);
		objLocationProfileBe.setProfileGroup(ProfileGroupTextBox);
		objLocationProfileBe.setLocationProfile(LocationProfile);
		try 
		{
			objIlocationprofileservice.editLocationProfile(objLocationProfileBe);
		}catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}


		StrUpdatedMessage=GLOBALCONSTANT.StrLocationProfileUpdated;
		LOCATIONPROFILESCREENLABELSBE ObjLocationLabels=new LOCATIONPROFILESCREENLABELSBE();

		try {
			ObjLocationLabels= objIlocationprofileservice.getScreenLabels(objCommonsessionbe.getCurrentLanguage());
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjModelAndView;
		}

		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_Locations_Profile_Maintenance_EditCopy_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationsProfileLabels,ObjLocationLabels);

		ObjModelAndView.addObject(GLOBALCONSTANT.StrLocationProfileUpdated,StrUpdatedMessage);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,objCommonsessionbe);

		return ObjModelAndView;

	}	
	/**
	 * @author Diksha Gupta
	 * @Date Jan 2 2015
	 * Description: This function is used to show data to LocationProfileSearchLookup Page.
	 * @return List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationsProfileData_Grid_Read , method = RequestMethod.GET)
	public @ResponseBody List<LOCATIONPROFILEBE> GetLocationProfileList() 
	{	 
		List<LOCATIONPROFILEBE> ObjListLocationProfileBe=null;
		try {
			ObjListLocationProfileBe=objIlocationprofileservice.GetLocationProfileList(StrLocationProfile,StrProfileGroup,StrPartOfDescription);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjListLocationProfileBe; 
		}

		return ObjListLocationProfileBe; 
	}	


	/**
	 * @author Diksha Gupta
	 * @Date Feb 02 2015
	 * Description: This function is used to get the selected value from Reassign grid like ProfileGroupValue,FulfillmentCenter,ValuelocationProfileValue to LocationProfileSearchLookup Page .
	 * @return List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationsProfileData_Grid_Read_Reassign , method = RequestMethod.GET)
	public @ResponseBody List<LOCATIONPROFILEBE> getReassignGridSelectedValue() 
	{	 
		List<LOCATIONPROFILEBE> ObjListLocationProfileBe=null;
		try {


			StrLocationProfile=GLOBALCONSTANT.BlankString;
			StrProfileGroup=GLOBALCONSTANT.BlankString;
			StrPartOfDescription=GLOBALCONSTANT.BlankString;

			// ObjListLocationProfileBe=objIlocationprofileservice.GetLocationProfileList(StrLocationProfile,StrProfileGroup,StrPartOfDescription);
			ObjListLocationProfileBe=objIlocationprofileservice.GetLocationProfileListReAssign(StrReassignprofileGroup,StrReassignfulFillmentCenter,StrReassignlocationProfile);
		} catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return ObjListLocationProfileBe; 
		}
		return ObjListLocationProfileBe; 
	}



	/**
	 * @author Diksha Gupta
	 * @Date Jan 30 2015
	 * Description: This function is used to show data for Reasign functionality to LocationProfileSearchLookup Page .
	 * @return List<LOCATIONPROFILEBE>
	 * @throws JASCIEXCEPTION
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_LocationProfile_Reassign_JspValue , method = RequestMethod.POST)
	public @ResponseBody Boolean getLocationProfileReAssignRowValue(@RequestParam(value=GLOBALCONSTANT.LocationProfile_Jsp_ReassignprofileGroup) String ReassignprofileGroup,@RequestParam(value=GLOBALCONSTANT.LocationProfile_Jsp_ReassignfulFillmentCenter) String ReassignfulFillmentCenter,@RequestParam(value=GLOBALCONSTANT.LocationProfile_Jsp_ReassignlocationProfile) String ReassignlocationProfile) 
	{	 

		try{
			StrReassignprofileGroup=ReassignprofileGroup;
			StrReassignfulFillmentCenter=ReassignfulFillmentCenter;
			StrReassignlocationProfile=ReassignlocationProfile;
			List<LOCATIONPROFILEBE> ObjListLocationProfileBe=objIlocationprofileservice.GetLocationProfileListReAssign(StrReassignprofileGroup,StrReassignfulFillmentCenter,StrReassignlocationProfile);
			if(ObjListLocationProfileBe.size()>GLOBALCONSTANT.INT_ZERO){
				return true;
			}
			else{
				return false;
			}
		}catch (JASCIEXCEPTION objJasciexception) {
			log.error(objJasciexception.getMessage());
			return false;
		}
	}
}	


