

/**
 * @Createdon 16 October 2015
 * @author Shailendra Rajput
 * @purpose the class is used to call the rest full service
 */

package com.jasci.biz.AdminModule.controller;
import java.io.IOException;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.NETWORK;
import com.jasci.exception.JASCIEXCEPTION;

public class RESTWEBSERVICECALLER {

	//HttpClient client = new DefaultHttpClient();
	NETWORK Obj_Network = new NETWORK();
	/**
	 * @purpose this method is used to call the web service  
	 * @param strURL
	 * @param strJSON
	 * @param objHttpMethod
	 * @return
	 * @throws IOException
	 * @throws JASCIEXCEPTION
	 */
	public String getRestResponse(String strURL,String strJSON,HttpMethod objHttpMethod) throws IOException, JASCIEXCEPTION
	{
		/**
		 * Initialization of variables
		 */
		RestTemplate objRestTemplate = new RestTemplate();
		String strResponse=GLOBALCONSTANT.Blank;
		HttpHeaders headers = new HttpHeaders();
		
		/**
		 * Setting the headers that supports the Json
		 */
		  headers.set(GLOBALCONSTANT.Accept, MediaType.APPLICATION_JSON_VALUE);

		  
		  /**
		   * Creating a request parameters 
		   */
		  
		  UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(strURL)
		          .queryParam(GLOBALCONSTANT.STRJSON, strJSON);
		          

		  HttpEntity<?> entity = new HttpEntity<>(headers);

		  /**
		   * Calling the web service
		   */
		  HttpEntity<String> response = objRestTemplate.exchange(builder.build().encode().toUri(), objHttpMethod, entity, String.class);
	
		  /**
		   * getting the response of web service
		   */
		  strResponse=response.getBody();
		  
		  
		
		
		/** 
		 * return the response of rest service
		 */
		return strResponse;
		
		
		
		
	}
}
