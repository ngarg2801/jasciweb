/**
Description make a controller of LOCATIONS class for mapping with View Of Wizard Locations Maintenance Screen
Created By Pradeep Kumar
Created Date Jan 09 2015

 */
package com.jasci.biz.AdminModule.controller;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ILOCATIONWIZARDSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.common.utilbe.LOCATIONWIZARDBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class LOCATIONWIZARDCONTROLLER {
	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	@Autowired
	ILOCATIONWIZARDSERVICE ObjLocationWizardService;
	@Autowired
	private LANGUANGELABELSAPI objLanguageLevelsAPI;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	static Logger log= LoggerFactory.getLogger(LOCATIONWIZARDCONTROLLER.class);
	String wizardId;
	String searchedValue;
	String SearchBased;
	@SuppressWarnings(GLOBALCONSTANT.Strrawtypes)
	List combineList;
	ModelAndView ObjModelAndView=new ModelAndView();
	
	/**
	     * Description : Redirect to location wizard Lookup Screen 
		 * @author Pradeep Kumar
		 * @Date Dec 30, 2014  3:51:50 PM
		 * Description this is used to 
		 * @return ModelAndView
		 * @throws
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Lookup_Action , method = RequestMethod.GET)
	public ModelAndView getWizardLocation() {	
		
		String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
	   
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		// String Timez= OBJCOMMONSESSIONBE.getClietnDeviceTimeZone();
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		else {
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Wizard_Lookup_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		try{
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Lookup_Level,ObjLocationWizardService.getLocationWizardLevel(LANGUANGELABELSAPI.LocationWizardLookupModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
		
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Location_Type,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Wizard_Id,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_WIZARDS,GLOBALCONSTANT.BlankString));
	
		}catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
			
		}	
		
		return ObjModelAndView;
		}
	}
	/**
	 * 
		 * @author Pradeep Kumar
		 * @Date Dec 30, 2014  3:53:31 PM
		 * Description this is used to Redirect to Location Wizard Maintenance Page
		 * @return ModelAndView
		 * @throws
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Maintenance_Action, method = RequestMethod.GET)
	public ModelAndView getLocationWizardMaintenanceScreen() {	 
		
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		else {
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Wizard_Maintenance_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		try{
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Level,ObjLocationWizardService.getLocationWizardLevel(LANGUANGELABELSAPI.LocationWizardMaintenanceModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
		
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_WRKZONE,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GRPWRKZONE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_WZONE,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WORKZONES,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Location_Type,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Wizard_Id,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WIZARDS,GLOBALCONSTANT.BlankString));
		
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Fulfillment_Drop_Down,ObjLocationWizardService.getDropDownValuesFulfillmentList());
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_IncreasedWizardControlNumber,ObjLocationWizardService.getMaxWizardControlNumber());
		Date CurrentDate=new Date();
		DateFormat ObjDateFormat = new SimpleDateFormat(GLOBALCONSTANT.InfoHelps_yyyyMMdd_Format);
		String testDateString = ObjDateFormat.format(CurrentDate);	
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Set_Date_On_Maintenance,testDateString);

		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Team_MemberName,ObjLocationWizardService.getTeamMemberName(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member()));
		
		}catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
			
		}	
		return ObjModelAndView;
		}
	}
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return ModelAndView
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 9, 2015
		 * @Description :Search data and show on Location Maintence Screen
	 */
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Maintenance_Edit_Action, method = RequestMethod.GET)
	public ModelAndView getLocationWizardMaintenanceEditScreen(HttpServletRequest request) {	 
		
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		else
		{
		searchedValue=request.getParameter(GLOBALCONSTANT.Location_Wizard_Searched_Value);
		wizardId=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_ID);
		SearchBased=request.getParameter(GLOBALCONSTANT.Location_Wizard_Search_Based);
		
		
		
		
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Wizard_Maintenance_Edit_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		try{
			ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Level,ObjLocationWizardService.getLocationWizardLevel(LANGUANGELABELSAPI.LocationWizardMaintenanceModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
	
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_WRKZONE,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GRPWRKZONE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_WZONE,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WORKZONES,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Location_Type,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Wizard_Id,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WIZARDS,GLOBALCONSTANT.BlankString));
		
		
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Fulfillment_Drop_Down,ObjLocationWizardService.getDropDownValuesFulfillmentList());
			
		
		}catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
			
		}	
		return ObjModelAndView;
		}
	}
	
	
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return List
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 13, 2015
		 * @Description :used for add and update Record
	 */
	@SuppressWarnings({ GLOBALCONSTANT.Strunchecked, GLOBALCONSTANT.Strrawtypes })
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Maintenance_Add_Update, method = RequestMethod.GET)
	public @ResponseBody List<Object> saveAndUpdate(@ModelAttribute(GLOBALCONSTANT.Location_Wizard_Register_ModelAttribute)  LOCATIONWIZARDBE objLocationWizardbe) {	 
		
		
		
		combineList=new ArrayList();
		
		try{		
			
		combineList=ObjLocationWizardService.saveAndUpdate(objLocationWizardbe);
	
		
	}catch (JASCIEXCEPTION ObjectJasciException) {	
		log.error(ObjectJasciException.getMessage());
		
		
	}	
	   if(combineList.get(GLOBALCONSTANT.INT_ZERO).toString().equalsIgnoreCase(GLOBALCONSTANT.OnlyWizardCreated))
		{
			combineList.clear();
			combineList.add(GLOBALCONSTANT.INT_ZERO,GLOBALCONSTANT.OnlyWizardCreated);
		}
		
		return combineList;
	}
	
	
	/**
	 * 
		 * Created By:Pradeep Kumar
		 * @return ModelAndView
		 * @throws JASCIEXCEPTION
		 * @Developed Date:Jan 7, 2015
		 * @Description :Redirect to Location Search Lookup and fetch data and show on screen
	 */
	
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Search_Lookup_Action , method = RequestMethod.GET)
	public ModelAndView getLocationWizardSearchLookupScreen(@ModelAttribute(GLOBALCONSTANT.Location_Wizard_Register_ModelAttribute)  LOCATIONWIZARDBE objLocationWizardbe,HttpServletRequest request) {	 
		
		/**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		else {
		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Wizard_Search_Lookup_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		
		
		searchedValue=request.getParameter(GLOBALCONSTANT.Location_Wizard_Searched_Value);
		wizardId=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_ID);
		SearchBased=request.getParameter(GLOBALCONSTANT.Location_Wizard_Search_Based);
		
		try{
			
		ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Search_Lookup_Level,ObjLocationWizardService.getLocationWizardLevel(LANGUANGELABELSAPI.LocationWizardSearchLookupModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
		}catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
			
		}	
		return ObjModelAndView;
		}
	}

	//read data using Kendo ui Menu Messages
		@RequestMapping(value = GLOBALCONSTANT.Location_Wizard_Kendo_Grid_Data, method = RequestMethod.GET)
		public @ResponseBody  List<LOCATIONWIZARDBE>  getList(HttpServletRequest request) {	
			List<LOCATIONWIZARDBE> ListGridData=null;
			
			
			
			
			wizardId=GLOBALCONSTANT.Wizard_ID;
			try {
				if(searchedValue!=GLOBALCONSTANT.BlankString && searchedValue!=null)
				{
				ListGridData=ObjLocationWizardService.getLocationWizardSearch(wizardId,searchedValue,SearchBased);
				}
				else 
				{
				ListGridData=ObjLocationWizardService.getLocationWizardData(wizardId);
				}
			
		}catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ListGridData;
			
		}	
			

			return ListGridData;
		}
		
		/**
		 * 
			 * 
				 * Created By:Pradeep Kumar
				 * @return ModelAndView
				 * @throws 
				 * @Developed Jan 14, 2015
				 * @Description :
		 */
			@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Maintenance_Edit_And_Copy_OnSearch_Lookup_Action, method = RequestMethod.GET)
			public ModelAndView getLocationWizardMaintenanceEditSearchLookup(HttpServletRequest request) {	 
				
				/**Here we Check user is already logged in or not*/
				if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
				
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
				
				else {
				String wizardControlNumber=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER);
				wizardId=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_ID);
				String fulfillmentCenterId=request.getParameter(GLOBALCONSTANT.Location_Wizard_FULFILLMENT_CENTER_ID);
				String copyOrEdit=request.getParameter(GLOBALCONSTANT.Copy);
				
				
				
				
				ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Wizard_Maintenance_Edit_Page);
				ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				try{
					ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Level,ObjLocationWizardService.getLocationWizardLevel(LANGUANGELABELSAPI.LocationWizardMaintenanceModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
					ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Searched_Data,ObjLocationWizardService.getLocationWizardCopyEdit(wizardId,wizardControlNumber,fulfillmentCenterId,copyOrEdit));
				
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_WRKZONE,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_GRPWRKZONE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_WZONE,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WORKZONES,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Location_Type,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCTYPE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Maintenance_Drop_Down_Wizard_Id,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_WIZARDS,GLOBALCONSTANT.BlankString));
				
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Fulfillment_Drop_Down,ObjLocationWizardService.getDropDownValuesFulfillmentList());
				
					
				}catch(JASCIEXCEPTION objJasciexception){
					log.error(objJasciexception.getMessage());
					return ObjModelAndView;
					
				}
				return ObjModelAndView;
				}
			}
			
			/**
			 * 
				 * 
					 * Created By:Pradeep Kumar
					 * @return :ModelAndView
					 * @throws LOCATIONWIZARDCONTROLLER
					 * @Date : Jan 14, 2015
					 * @Description :delete Wizard Only
			 */
			
			@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Delete_WizardOnly_Action , method = RequestMethod.GET)
			public ModelAndView deleteWizardOnly(@ModelAttribute(GLOBALCONSTANT.Location_Wizard_Register_ModelAttribute)  LOCATIONWIZARDBE objLocationWizardbe,HttpServletRequest request) {	 
				
				/**Here we Check user is already logged in or not*/
				if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
				
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
				else
				{
				String wizardControlNumber=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER);
				wizardId=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_ID);
				String fulfillmentCenterId=request.getParameter(GLOBALCONSTANT.Location_Wizard_FULFILLMENT_CENTER_ID);
				
				ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Wizard_Search_Lookup_Page);
				ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				try{
					ObjLocationWizardService.deleteWizardOnly(wizardId, wizardControlNumber, fulfillmentCenterId);
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Search_Lookup_Level,ObjLocationWizardService.getLocationWizardLevel(LANGUANGELABELSAPI.LocationWizardSearchLookupModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
				}catch(JASCIEXCEPTION objJasciexception){
					log.error(objJasciexception.getMessage());
					return ObjModelAndView;
					
				}
				return ObjModelAndView;
				}
			}
			
			@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Location_Wizard_Delete_Wizard_And_Location_Action , method = RequestMethod.GET)
			public ModelAndView deleteWizardAndLocation(@ModelAttribute(GLOBALCONSTANT.Location_Wizard_Register_ModelAttribute)  LOCATIONWIZARDBE objLocationWizardbe,HttpServletRequest request) {	 
				
				/**Here we Check user is already logged in or not*/
				if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){
					return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
				}
				
				/** Here we Check user have this screen access or not*/

				WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
						GLOBALCONSTANT.RequestMapping_Wizard_Location_maintenance_Execution);

				if (!objWEBSERVICESTATUS.isBoolStatus()) {
					ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
					ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
					return ObjModelAndView;

				}
				else {
				String wizardControlNumber=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_CONTROL_NUMBER);
				wizardId=request.getParameter(GLOBALCONSTANT.Location_Wizard_WIZARD_ID);
				String fulfillmentCenterId=request.getParameter(GLOBALCONSTANT.Location_Wizard_FULFILLMENT_CENTER_ID);
				String Data=request.getParameter(GLOBALCONSTANT.Data);
				
				ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_Location_Wizard_Search_Lookup_Page);
				ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				try{
				ObjLocationWizardService.deleteWizardAndLocation(wizardId, wizardControlNumber, fulfillmentCenterId,Data);
				ObjModelAndView.addObject(GLOBALCONSTANT.Location_Wizard_Search_Lookup_Level,ObjLocationWizardService.getLocationWizardLevel(LANGUANGELABELSAPI.LocationWizardSearchLookupModule, OBJCOMMONSESSIONBE.getCurrentLanguage()));
				}catch(JASCIEXCEPTION objJasciexception){
					log.error(objJasciexception.getMessage());
					return ObjModelAndView;
					
				}
				return ObjModelAndView;
				}
			}
}
