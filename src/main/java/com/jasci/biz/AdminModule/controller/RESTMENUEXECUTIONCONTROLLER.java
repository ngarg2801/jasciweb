/*

Date Developed  Dec 6 2014
Description   Menu Execution Restfull Service Controller to handle data
Created By Sarvendra Tyagi
 */


package com.jasci.biz.AdminModule.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jasci.biz.AdminModule.dao.MENUEXECUTIONDAOIMPL;
import com.jasci.biz.AdminModule.model.LANGUAGES;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.TEAMMEMBERMESSAGES;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class RESTMENUEXECUTIONCONTROLLER {
	
	@Autowired
	MENUEXECUTIONDAOIMPL objMenuexecutiondaoimpl;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	static Logger log = LoggerFactory.getLogger(RESTMENUEXECUTIONCONTROLLER.class.getName());
	/** 
	 * description: This method is return label list from data base 
	 * input Param: ScreenName and Language
	 * 
	 * @param obHttpRequest
	 * @return objList
	 * developed by: sarvendra Tyagi
	 * Date : Dec-07-2014
	 */
	
	@Transactional
	@RequestMapping(value =GLOBALCONSTANT.RequestMapping_MenuExecutionLabel_Action, method = RequestMethod.GET)
	public @ResponseBody  List<LANGUAGES>  getScreenLabelList(HttpServletRequest obHttpRequest) { 
		
		List<LANGUAGES> objList=null;
		String language=obHttpRequest.getParameter(GLOBALCONSTANT.InputQuery_MenuExecutionLabel_Language).toString();
		String screenName=obHttpRequest.getParameter(GLOBALCONSTANT.InputQuery_MenuExecutionLabel_ScreenName).toString();
		try{
			
			//get list of labels
			objList=objMenuexecutiondaoimpl.GetScreenLabel(language, screenName);
						
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
			
			
		}
	return objList;	
	}
	
	
	
	
	/** 
	 * Desciption: This function is used for getting menu messages based on compnay,tenant and Status 
	 * @param: objCommonsessionbe
	 * @return: List<MENUMESSAGES>
	 * developed by: Sarvendra Tyagi
	 * Date: 12-10-2014
	 */
	
	@Transactional
	@RequestMapping(value =GLOBALCONSTANT.RESTSERVICE_MenuMessage_Action, method = RequestMethod.GET)
	public @ResponseBody  List<MENUMESSAGES>  getGetMenuMessagesList(@RequestParam(value=GLOBALCONSTANT.DateTime) String strTime) { 
		
		COMMONSESSIONBE objCommonsessionbe=OBJCOMMONSESSIONBE;
		List<MENUMESSAGES> listMenuMessages=null;
		try{
		
			listMenuMessages=	objMenuexecutiondaoimpl.GetMenuMessagesCompany(objCommonsessionbe,strTime);
		
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}
		
		
		return listMenuMessages;
	}
	
	
	
	
	/** 
	 * Desciption: This function is used for getting team member messages based on compnay,tenant,TeamMember and Status 
	 * @param: objCommonsessionbe
	 * @return: List<MENUMESSAGES>
	 * developed by: Sarvendra Tyagi
	 * Date: 12-10-2014
	 */
	
	@Transactional
	@RequestMapping(value =GLOBALCONSTANT.RESTSERVICE_TeamMemberMessage_Action, method = RequestMethod.GET)
	public @ResponseBody  List<TEAMMEMBERMESSAGES>  getGetTeamMemberMessagesList(@RequestParam(value=GLOBALCONSTANT.DateTime) String strTime) { 
		
		COMMONSESSIONBE objCommonsessionbe=OBJCOMMONSESSIONBE;
		List<TEAMMEMBERMESSAGES> listTeamMemberMessages=null;
		
		try{
		
			listTeamMemberMessages=objMenuexecutiondaoimpl.GetTeamMemberMenuMessages(objCommonsessionbe,strTime);
		
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}
		
		return listTeamMemberMessages;
	}
	
	

	

}
