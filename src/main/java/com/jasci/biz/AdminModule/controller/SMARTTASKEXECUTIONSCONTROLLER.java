/**

Description : Make a controller for Smart Task Configurator Screen form which we can mapped Configurator jsp pages and pass the value.
Created By : Shailendra Rajput
Created Date : May 14 2015
 */
package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.ServicesApi.SMARTTASKCONFIGURATORSERVICEAPI;
import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONBE;
import com.jasci.biz.AdminModule.be.SMARTTASKCONFIGURATORBE;
import com.jasci.biz.AdminModule.be.SMARTTASKEXECUTIONSBE;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONS;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONSBEAN;
import com.jasci.biz.AdminModule.model.SMARTTASKEXECUTIONSPK;
import com.jasci.biz.AdminModule.model.SMARTTASKSEQHEADERSBEAN;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
//import com.jasci.biz.AdminModule.service.ISMARTTASKCONFIGURATORSERVICE;
import com.jasci.biz.AdminModule.service.ISMARTTASKEXECUTIONSSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;


@Controller
public class SMARTTASKEXECUTIONSCONTROLLER  {
	
	/** Make a common session object for get the value of tenant,company,team member etc.*/
	@Autowired
	private COMMONSESSIONBE OBJCOMMONSESSIONBE;

	/** Make the instance of SCREENACCESSSERVICE for provide the authentication of particular screen for particular user*/
	@Autowired
	private SCREENACCESSSERVICE screenAccessService;
	
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	private LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	@Autowired
	private ISMARTTASKEXECUTIONSSERVICE smartTaskExeService;
	
	@Autowired
	private SMARTTASKCONFIGURATORSERVICEAPI ObjSmartTaskConfiguratorServiceApi;
	
	/** It is used to logger the exception*/
	private static Logger log = LoggerFactory.getLogger(SMARTTASKEXECUTIONSCONTROLLER.class.getName());
	
	/**create modelandview instance*/
	private ModelAndView ObjModelAndView=new ModelAndView();
	
	  /**This is used to get the value of search page*/

	  private  String StrApplication_Id;
	  private  String StrExecution_Sequence_Group;
	  private  String StrExecution_Group;
	  private  String StrExecution_Type;
	  private  String StrExecution_Device;
	  private  String StrExecution_Name;
	  private  String StrExecution_Path;
	  private  String StrDescription20;
	  private  String StrDescription50;
	  private  String StrTenant_Id;	  
	  private  String StrButton;
	  private  String StrButton_Name;
	  private  String StrLast_Activity_Date;
	  private  String StrLast_Activity_TeamMember;
	  private  String StrCompany_Id;
	  private  String StrHelp_Line;
	  
	  
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecution_Search , method = RequestMethod.GET)
public ModelAndView getSearch(@ModelAttribute(GLOBALCONSTANT.SmartTaskConfigurator_ModelAttribute) SMARTTASKSEQHEADERSBEAN ObjectSmartTaskSeqHeadersBean) {	 

	String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
	String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
	
	/** Here we Check user is logged in or not*/
	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	/** Here we Check user has Authentication for particular screen or not*/
	
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}

	ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_SmartTaskExecution_Search);
	
	try{
		/**Call util.LOOKUPGENERALCODE.Drop_Down_Selection() function to call the general code descriptions.*/
		
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectApplication,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionType,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXETYPE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionDevice,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEDEVICE,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionGroup,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEGRP,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectTask,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.SMARTTASKEXECUTIONSBE, smartTaskExeService.getSmartTaskExecutionsLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));

	}catch (JASCIEXCEPTION ObjectJasciException) {	
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
	}
	return ObjModelAndView;
}

//RequestMapping_SmartTaskExecutions_Search_Lookup="/SmartTaskExecutions_SearchLookup";  //method = RequestMethod.GET
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Search_Lookup , method = RequestMethod.GET)  
public ModelAndView getExecutionsSearchLookup(HttpServletRequest request) {	 

	/** Here we Check user is logged in or not*/
	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	
	/** Here we Check user has Authentication for particular screen or not*/
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	
	try  {
		 
		  		StrApplication_Id = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Application_Id_JspPageValue);
		  		StrExecution_Sequence_Group = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Execution_Group_JspPageValue); 	
		  		StrExecution_Type = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Execution_Type_JspPageValue);
		  		StrExecution_Device = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Execution_Device_JspPageValue);	
		  		StrExecution_Name = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Execution_Name_JspPageValue);
		  	    StrDescription20 = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Description20_JspPageValue);	
		  	    StrTenant_Id = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Tenant_Id_JspPageValue);					
		  	    StrButton = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Button_JspPageValue);
		                                                                                                                                                                               
		  	    ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Smart_Task_Executions_Search_Lookup);
		  	    ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		  	    ObjModelAndView.addObject(GLOBALCONSTANT.SMARTTASKEXECUTIONSBE, smartTaskExeService.getSmartTaskExecutionsLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		  
	} catch (JASCIEXCEPTION ObjectJasciException) {
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
		
	}
	return ObjModelAndView;
}

@RequestMapping(value = GLOBALCONSTANT.RequestMapping_Smart_Task_Executions_Search_Lookup , method = RequestMethod.GET)
public ModelAndView getExecutions_Search_Lookup() {	 
	                            
	/** Here we Check user is logged in or not*/
	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	
	/** Here we Check user has Authentication for particular screen or not*/
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	
	try {
			  ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Search_Lookup);	//SmartTaskExecutionsr_SearchLookup.jsp
			  ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
			  ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			  ObjModelAndView.addObject(GLOBALCONSTANT.SMARTTASKEXECUTIONSBE, smartTaskExeService.getSmartTaskExecutionsLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
	} catch (JASCIEXCEPTION ObjectJasciException) {
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
	}
	return ObjModelAndView;
}

@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Kendo_DisplayAll, method = RequestMethod.GET)
public @ResponseBody List<SMARTTASKEXECUTIONSBEAN> getDisplaykendoGrid()  throws JASCIEXCEPTION {		
	
		List<SMARTTASKEXECUTIONSBEAN> SmartTaskExecutionBeanList = new ArrayList<SMARTTASKEXECUTIONSBEAN>();
		try {
		
					if(( StrTenant_Id == null ||  StrTenant_Id.isEmpty() )  && 
						( StrApplication_Id == null || StrApplication_Id.isEmpty() )  && 	
						( StrExecution_Sequence_Group== null || StrExecution_Sequence_Group.isEmpty() ) &&
						( StrExecution_Type == null || StrExecution_Type.isEmpty() )    &&
						( StrExecution_Device==null || StrExecution_Device.isEmpty() )  && 
						( StrExecution_Name==null || StrExecution_Name.isEmpty() )   &&
						( StrDescription20==null || StrDescription20.isEmpty() )  &&  
						( StrTenant_Id == null || StrTenant_Id.isEmpty() ) &&
						(StrButton==null || StrButton.isEmpty() )){
							SmartTaskExecutionBeanList=smartTaskExeService.getDisplayAll();
					}
					else {
								SmartTaskExecutionBeanList=smartTaskExeService.getList(StrTenant_Id, StrApplication_Id, StrExecution_Sequence_Group, StrExecution_Type, StrExecution_Device, StrExecution_Name, StrDescription20, StrButton);
					}
		} catch (Exception ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return SmartTaskExecutionBeanList;
		}													
		return SmartTaskExecutionBeanList;
}

@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_ParametersSearch_Lookup , method = RequestMethod.POST)  
public ModelAndView getExecutionsParametersSearchLookup(@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_Application) String StrApplication,
		@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_ExecutionGroup) String StrExecutionGroup,
		@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_ExecutionType) String StrExecutionType,
		@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_ExecutionDevice) String StrExecutionDevice,
		@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_Execution) String StrExecutionName,
		@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_Description) String StrDescription20,
	    @RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_Tenant) String StrTenant,
		@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Restfull_Buttons) String StrButton)  throws JASCIEXCEPTION {	 

	if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
		return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}
	WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
			GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
	if (!objWebServiceStatus.isBoolStatus()) {
		ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
		ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
		return ObjModelAndView;

	}
	
	try  {
		  		StrApplication_Id = StrApplication;
		  		StrExecution_Sequence_Group = StrExecutionGroup; 	
		  		StrExecution_Type = StrExecutionType;
		  		StrExecution_Device = StrExecutionDevice;	
		  		StrExecution_Name = StrExecutionName;
		  	    this.StrDescription20 = StrDescription20;	
		  	    StrTenant_Id = StrTenant;					
		  	    this.StrButton = StrButton;
		                                                                                                                                                                               
		  	    ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_Smart_Task_Executions_Search_Lookup);
		  	    ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		        ObjModelAndView.addObject(GLOBALCONSTANT.SMARTTASKEXECUTIONSBE, smartTaskExeService.getSmartTaskExecutionsLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		  
	} catch (JASCIEXCEPTION ObjectJasciException) {
		log.error(ObjectJasciException.getMessage());
		return ObjModelAndView;
		
	}
	return ObjModelAndView;
}

	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Maintenance, method = RequestMethod.GET)
	public ModelAndView executionMaintenance(@ModelAttribute(GLOBALCONSTANT.SmartTaskExecution_ModelAttribute) SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean) {

		String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
	
		/** Here we Check user is logged in or not*/
		if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user has Authentication for particular screen or not*/
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(
				OBJCOMMONSESSIONBE.getTenant(),
				OBJCOMMONSESSIONBE.getTeam_Member(),
			   GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;
		}

		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Maintenance);
	
		try {
			
				smartTaskExecutionsBean.setTenant_Id(OBJCOMMONSESSIONBE.getTenant());
				smartTaskExecutionsBean.setCompany_Id(OBJCOMMONSESSIONBE.getCompany());
				smartTaskExecutionsBean.setLast_Activity_Team_Member(OBJCOMMONSESSIONBE.getTeam_Member());
				smartTaskExecutionsBean.setLast_Activity_Date(getCurrentDate(GLOBALCONSTANT.yyyyMMdd_Format_v1));
	
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecution_ModelAttribute,smartTaskExecutionsBean);
			
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectApplication,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionType,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXETYPE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionDevice,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEDEVICE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionGroup,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEGRP,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectTask,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
				ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				
				ObjModelAndView.addObject(GLOBALCONSTANT.SMARTTASKEXECUTIONSBE, smartTaskExeService.getSmartTaskExecutionsLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		} catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
		return ObjModelAndView;
	}
	
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Add, method = RequestMethod.POST)
	public @ResponseBody Boolean addExecutions(
			@ModelAttribute(GLOBALCONSTANT.SmartTaskExecution_ModelAttribute) SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean,  
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Application_Id) String StrApplication,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Execution_Group) String StrExecutionGroup,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Execution_Type) String StrExecutionType,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Execution_Device) String StrExecutionDevice,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Execution_Name) String StrExecutionName,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Execution_Path) String StrExecutionPath,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Description20) String StrDescription20,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Description50) String StrDescription50,
		    @RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Tenant_Id) String StrTenant,
		    @RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Button) String StrButton,
		    @RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Button_Name) String StrButtonName,
		    @RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Help_Line) String StrHelpLine,
		    @RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Last_Activity_Date) String StrLastActivityDate,
			@RequestParam(value=GLOBALCONSTANT.SmartTaskExecution_Last_Activity_Team_Member) String StrLastActivityTeamMember, HttpServletRequest request)  throws JASCIEXCEPTION {	 

			StrApplication_Id = StrApplication;
			StrExecution_Group = StrExecutionGroup;
			StrExecution_Type = StrExecutionType;
			StrExecution_Device = StrExecutionDevice;
			StrExecution_Name = StrExecutionName;
			StrExecution_Path = StrExecutionPath;
			this.StrDescription20 = StrDescription20;
			this.StrDescription50 = StrDescription50;
			StrTenant_Id =  StrTenant; 
			this.StrButton = StrButton;
			StrButton_Name = StrButtonName;
             StrHelp_Line = StrHelpLine;
			StrLast_Activity_Date = StrLastActivityDate;
			StrLast_Activity_TeamMember = StrLastActivityTeamMember;

			smartTaskExecutionsBean.setCompany_Id(OBJCOMMONSESSIONBE.getCompany());
			
			Boolean Status=false;
			try  {
						Status = smartTaskExeService.addExecutions(smartTaskExecutionsBean);
			} catch(JASCIEXCEPTION objJasciexception){
				log.error(objJasciexception.getMessage());
			}
			String SavedMessageValue=GLOBALCONSTANT.SavedValue;
			try {
								
			} catch (Exception ObjectJasciException) {	
				log.error(ObjectJasciException.getMessage());
					return Status;
			}		
			return Status;
    }
	
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Delete , method = RequestMethod.POST)
	public ModelAndView deleteExecutions(HttpServletRequest request) {	 
		
		if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user has Authentication for particular screen or not*/
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		try {
	  	        StrTenant_Id = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Tenant_Id_JspPageValue);					
	  	        StrCompany_Id=request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Company_id_JspPageValue);
	  	        StrExecution_Name = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Execution_Name_JspPageValue);
			   
			   smartTaskExeService.deleteExecutions(StrTenant_Id, StrCompany_Id, StrExecution_Name);
			   ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_SmartTaskExecution_Search);
			   return ObjModelAndView;
		}
		catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
	}
	
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecution_GetExecutions , method = RequestMethod.POST)
	public ModelAndView getExecutions(HttpServletRequest request) {	 
		
		if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user has Authentication for particular screen or not*/
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
		
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		try {
	  	        StrTenant_Id = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Tenant_Id_JspPageValue);					
	  	        StrCompany_Id=request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Company_id_JspPageValue);
	  	        StrExecution_Name = request.getParameter(GLOBALCONSTANT.SmartTaskExecution_Execution_Name_JspPageValue);
	  	        
	  	        List<SMARTTASKEXECUTIONS> SmartTaskExecutionsList = new ArrayList<SMARTTASKEXECUTIONS>();
	  				   
	  	        SmartTaskExecutionsList =  smartTaskExeService.getExecutions(StrTenant_Id, StrCompany_Id, StrExecution_Name);
	  	    
	  	        SMARTTASKEXECUTIONS SmartTaskExecutions = SmartTaskExecutionsList.get(0);
	  	        SMARTTASKEXECUTIONSPK  SmartTaskExecutionsPK = SmartTaskExecutions.getId();
	  	   
	  	        String StrTenant_ID  =  SmartTaskExecutionsPK.getTenant_Id();
	  	        String StrCompany_ID = SmartTaskExecutionsPK.getCompany_Id();
	  	        String StrApplication_ID =SmartTaskExecutions.getApplication_Id();
	  	        String StrExecution_Group = SmartTaskExecutions.getExecution_Group();
	  	        String StrExecution_Type = SmartTaskExecutions.getExecution_Type();
	  	        String StrExecution_Device = SmartTaskExecutions.getExecution_Device();
	  	        String StrExecution_Name  =  SmartTaskExecutionsPK.getExecution_Name();
	  	        String StrExecution_Path  =  SmartTaskExecutions.getExecution();
	  	        String StrDescription_Short  =  SmartTaskExecutions.getDescription20();
	  	        String StrDescription_Long =  SmartTaskExecutions.getDescription50();
	  	        String StrButton = SmartTaskExecutions.getButton();
	  	        String StrButton_Name  = SmartTaskExecutions.getButton_Name();  	     
	  	        String StrLastActivity_Date = SmartTaskExecutions.getLast_Activity_Date().toLocaleString();
	  	        String StrLastActivity_By =   SmartTaskExecutions.getLast_Activity_Team_Member();
	  	        String StrHelp_Line =   SmartTaskExecutions.getHelp_Line();
	  	        
	  	        
	  	 	  
	  	        SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean = new SMARTTASKEXECUTIONSBEAN();
				smartTaskExecutionsBean.setTenant_Id(StrTenant_ID.trim());
				smartTaskExecutionsBean.setCompany_Id(StrCompany_ID);
				smartTaskExecutionsBean.setApplication_Id(StrApplication_ID);
				smartTaskExecutionsBean.setExecution_Group(StrExecution_Group);
				smartTaskExecutionsBean.setExecution_Type(StrExecution_Type);
				smartTaskExecutionsBean.setExecution_Device(StrExecution_Device);
				smartTaskExecutionsBean.setExecution_Name(StrExecution_Name);
				smartTaskExecutionsBean.setExecution_Path(StrExecution_Path);
				smartTaskExecutionsBean.setDescription20(StrDescription_Short);
				smartTaskExecutionsBean.setDescription50(StrDescription_Long);
				smartTaskExecutionsBean.setButton(StrButton);
				smartTaskExecutionsBean.setButton_Name(StrButton_Name);
				smartTaskExecutionsBean.setHelp_Line(StrHelp_Line);
				smartTaskExecutionsBean.setLast_Activity_Team_Member(StrLastActivity_By);
				smartTaskExecutionsBean.setLast_Activity_Date(getCurrentDate(GLOBALCONSTANT.yyyyMMdd_Format_v1));
	
				ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Maintenance_Update);
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecution_ModelAttribute,smartTaskExecutionsBean);
			
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectApplication,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionType,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXETYPE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionDevice,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEDEVICE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionGroup,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEGRP,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectTask,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
				ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				
				ObjModelAndView.addObject(GLOBALCONSTANT.SMARTTASKEXECUTIONSBE, smartTaskExeService.getSmartTaskExecutionsLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
				return ObjModelAndView;
		}
		catch (JASCIEXCEPTION ObjectJasciException) {
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
	}
	
	@RequestMapping(value = GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Maintenance_Update, method = RequestMethod.GET)
	public ModelAndView executionMaintenanceUpdate(@ModelAttribute(GLOBALCONSTANT.SmartTaskExecution_ModelAttribute) SMARTTASKEXECUTIONSBEAN smartTaskExecutionsBean) {
	
		String StrTenant_ID=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany_ID=OBJCOMMONSESSIONBE.getCompany();
		
		/** Here we Check user is logged in or not*/
		if(OBJCOMMONSESSIONBE.getUserID().isEmpty() || OBJCOMMONSESSIONBE.getPassword().isEmpty()){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}
		
		/** Here we Check user has Authentication for particular screen or not*/
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(
				OBJCOMMONSESSIONBE.getTenant(),
				OBJCOMMONSESSIONBE.getTeam_Member(),
			   GLOBALCONSTANT.WebServiceStatus_SmartTaskConfigurator_lookup);
	
		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;
		}

		ObjModelAndView=new ModelAndView(GLOBALCONSTANT.RequestMapping_SmartTaskExecutions_Maintenance);
	
		try {
			
				smartTaskExecutionsBean.setTenant_Id(OBJCOMMONSESSIONBE.getTenant());
				smartTaskExecutionsBean.setCompany_Id(OBJCOMMONSESSIONBE.getCompany());
				smartTaskExecutionsBean.setLast_Activity_Team_Member(OBJCOMMONSESSIONBE.getTeam_Member());
				smartTaskExecutionsBean.setLast_Activity_Date(getCurrentDate(GLOBALCONSTANT.yyyyMMdd_Format_v1));
	
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecution_ModelAttribute,smartTaskExecutionsBean);
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectApplication,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_APPLICATIONS,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionType,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXETYPE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionDevice,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEDEVICE,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskExecutions_Combobox_SelectExecutionGroup,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_EXEGRP,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.SmartTaskConfigurator_Combobox_SelectTask,ObjLookUpGeneralCode.Drop_Down_Selection(StrTenant_ID,StrCompany_ID,GLOBALCONSTANT.GIC_TASK,GLOBALCONSTANT.BlankString));
				ObjModelAndView.addObject(GLOBALCONSTANT.JASCI,OBJCOMMONSESSIONBE.getTenant().toUpperCase());
				ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
				
				ObjModelAndView.addObject(GLOBALCONSTANT.SMARTTASKEXECUTIONSBE, smartTaskExeService.getSmartTaskExecutionsLabels(OBJCOMMONSESSIONBE.getCurrentLanguage()));
		} catch (JASCIEXCEPTION ObjectJasciException) {	
			log.error(ObjectJasciException.getMessage());
			return ObjModelAndView;
		}
		return ObjModelAndView;
	}
	
	public String getCurrentDate(String StrInFormat) {
		DateFormat dateFormat = new SimpleDateFormat(StrInFormat);
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());

	}
}
