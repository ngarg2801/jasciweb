/**
 
Date Developed :Dec 17 2014
Created by: Diksha Gupta
Description :LANGUAGE TRANSLATION Controller TO HANDLE THE REQUESTS FROM SCREENS and to redirect to other pages with data.
 */
package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONBE;
import com.jasci.biz.AdminModule.be.LANGUAGETRANSLATIONSCREENLABELSBE;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ILANGUAGETRANSLATIONSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class LANGUAGETRANSLATIONCONTROLLER 
{
 
	
	@Autowired
	ILANGUAGETRANSLATIONSERVICE ObjLanguageTranslationService;
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	ModelAndView ObjModelAndView=new ModelAndView();
	LANGUAGETRANSLATIONBE ObjLanguageEdit=new LANGUAGETRANSLATIONBE();
	String StrSelectedLanguage=GLOBALCONSTANT.BlankString;
	String StrKeyPhrase=GLOBALCONSTANT.BlankString;
	String SavedMessageValue=GLOBALCONSTANT.BlankString;
	String StrLanguageEdit=GLOBALCONSTANT.BlankString;
	String StrKeyPhraseEdit=GLOBALCONSTANT.BlankString;	
	String StrGeneralCodeId=GLOBALCONSTANT.LanguageTranslation_GeneralCodeId_Languages;
    String UpdatedMessageValue=GLOBALCONSTANT.BlankString;
    LANGUAGETRANSLATIONSCREENLABELSBE objLabels=null;
    static Logger log= LoggerFactory.getLogger(LANGUAGETRANSLATIONCONTROLLER.class);
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to redirect to Language Translation Maintenance page.
	 * Input parameter: HttpServletRequest
	 * Return Type :ModelAndView
	 * 
	 */
	
	
	@RequestMapping(value=GLOBALCONSTANT.Language_Translations_Maintenance_Action, method = RequestMethod.GET)
	public ModelAndView getMaintenance_Page(HttpServletRequest request)
	{
		//List<LANGUAGETRANSLATIONBE> objGeneralcodesLanguage=null;
		LANGUAGETRANSLATIONBE ObjLanguageTranslationBe=new LANGUAGETRANSLATIONBE();
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.Language_Translations_Maintenance_Page);
		String backSatus=request.getParameter(GLOBALCONSTANT.BACKSTATUS);
		/*String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();*/
		String StrTeamMember=OBJCOMMONSESSIONBE.getTeam_Member_Name();
		Date ObjDate = new Date();
		DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_YYYY_MM_DD);
		String StrCurrentDate=ObjDateFormat1.format(ObjDate);
		//String StrGeneralCodeId=GLOBALCONSTANT.LanguageTranslation_GeneralCodeId_Languages;
		ObjLanguageTranslationBe.setLastActivityBy(StrTeamMember);
		ObjLanguageTranslationBe.setLastActivityDate(StrCurrentDate);
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		/*String StrMenuType=GLOBALCONSTANT.FullScreen;
	    String StrMenuOption=GLOBALCONSTANT.MenuOption;*/
		
	    /**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Language_Translations_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		/** Call GetLanguageList() to get the list of Languages from GeneralCodes. */

		/*try{
			objGeneralcodesLanguage=ObjLanguageTranslationService.getLanguageList(StrTenant, StrCompany, StrGeneralCodeId);
		}catch(JASCIEXCEPTION ObjJasciException){}
		*//** Call GetScreenLabels() to get screen labels from Languages. */
		
		try {
			objLabels=ObjLanguageTranslationService.GetScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageLabels,objLabels);
		try {
		ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.Language_Translations_LanguageBe,ObjLanguageTranslationBe);
		SavedMessageValue=GLOBALCONSTANT.BlankString;
		ObjModelAndView.addObject(GLOBALCONSTANT.SavedValue,SavedMessageValue);
		ObjModelAndView.addObject(GLOBALCONSTANT.BACKSTATUS,backSatus);
		return ObjModelAndView;
	
		
    }
	
	
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to redirect to Language Translation Maintenance Edit page.
	 * Input parameter: HttpServletRequest
	 * Return Type :ModelAndView
	 * 
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.Language_Translations_MaintenanceEdit_Action, method = RequestMethod.GET)
	public ModelAndView getMaintenanceEdit_Page(HttpServletRequest request)
	{
		
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		/*String StrMenuType=GLOBALCONSTANT.FullScreen;
	    String StrMenuOption=GLOBALCONSTANT.MenuOption;*/
		
	    /**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Language_Translations_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.Language_Translations_Maintenance_EditPage);
		try {
			objLabels=ObjLanguageTranslationService.GetScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageLabels,objLabels);
		ObjModelAndView.addObject(GLOBALCONSTANT.Language_Translations_LanguageBe,ObjLanguageEdit);
		UpdatedMessageValue=GLOBALCONSTANT.BlankString;
		ObjModelAndView.addObject(GLOBALCONSTANT.SavedValue,UpdatedMessageValue);
		return ObjModelAndView;
	
		
    }
	
	
	
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to redirect to Language Translation lookup page. 
	 * Input parameter: HttpServletRequest
	 * Return Type :ModelAndView
	 * 
	 */
	
	@RequestMapping(value=GLOBALCONSTANT.Language_Translations_Lookup_Action, method = RequestMethod.GET)
	public ModelAndView getLookup_Page(HttpServletRequest request)
	{
		
		
		//List<LANGUAGETRANSLATIONBE> objLanguageBe=null;
		/*String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();*/
		String TeamMemberLanguage =OBJCOMMONSESSIONBE.getTeam_Member_Language();
		String DefaultLanguage =GLOBALCONSTANT.BlankString;
		//String DefaultLanguageValue=GLOBALCONSTANT.BlankString;
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		/*String StrMenuType=GLOBALCONSTANT.FullScreen;
	    String StrMenuOption=GLOBALCONSTANT.MenuOption;
	    */
	    /**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Language_Translations_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		//int index=0;
		/** call GetLanguageList() to get Languages from GeneralCodes.   */
		/*try{
			objLanguageBe=ObjLanguageTranslationService.getLanguageList(StrTenant, StrCompany, StrGeneralCodeId);
		}catch(JASCIEXCEPTION ObjJasciException){}*/
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.Language_Translations_Lookup_Page);
		
		try {
			ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageList,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LANGUAGES,GLOBALCONSTANT.BlankString));
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		//ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageList,objLanguageBe);
		try{
			objLabels=ObjLanguageTranslationService.GetScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageLabels,objLabels);
		ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_DefaultLanguage,DefaultLanguage);
		ObjModelAndView.addObject(GLOBALCONSTANT.DefaultLanguageValue,TeamMemberLanguage);
		return ObjModelAndView;
		
    }
	
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to redirect to Language Translation Search lookup page 
	 * Input parameter:none
	 * Return Type :ModelAndView
	 * 
	 */
	
	
	@RequestMapping(value=GLOBALCONSTANT.Language_Translations_Search_Lookup_Action, method = RequestMethod.GET)
	public ModelAndView getSearch_Lookup_Page()
	{
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		/*String StrMenuType=GLOBALCONSTANT.FullScreen;
	    String StrMenuOption=GLOBALCONSTANT.MenuOption;	*/
	    /**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Language_Translations_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.Language_Translations_Search_Lookup_Page);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		try{
			objLabels=ObjLanguageTranslationService.GetScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		 
		return ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageLabels,objLabels);
		
    }
	
	
	/**
	 * Created on:Dec 17 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to redirect to Language Translation Search lookup page 
	 * Input parameter: HttpServletRequest
	 * Return Type :ModelAndView
	 * 
	 */
	
	
	@RequestMapping(value=GLOBALCONSTANT.Language_Translations_Search_Lookup_ActionLanguage, method = RequestMethod.GET)
	public ModelAndView getSearch_Lookup_Page(HttpServletRequest request)
	{
		StrSelectedLanguage=request.getParameter(GLOBALCONSTANT.LanguageVal);
		StrKeyPhrase=request.getParameter(GLOBALCONSTANT.KeyPhraseVal);
		if(StrSelectedLanguage==null)
		{
			StrSelectedLanguage=GLOBALCONSTANT.BlankString;
		}
		if(StrKeyPhrase==null)
		{
			StrKeyPhrase=GLOBALCONSTANT.BlankString;
		}
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Language_Translations_Search_Lookup_Action);
		return ObjModelAndView;
    }
	
	
	
	/**
	 * Created on:Dec 18 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to redirect to add the data to Languages Table
	 * Input parameter: HttpServletRequest,String
	 * Return Type :ModelAndView
	 * @param request 
	 * 
	 */
	
	
	@RequestMapping(value=GLOBALCONSTANT.Language_Translations_Search_Lookup_ActionInsert, method = RequestMethod.POST)
	public ModelAndView AddLanguageData(@ModelAttribute(GLOBALCONSTANT.Language_Translations_CommandNameAdd) LANGUAGETRANSLATIONBE ObjLanguageBe, HttpServletRequest request)
	{
		
		 /**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Language_Translations_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String LastActivityDate=request.getParameter(GLOBALCONSTANT.GetLastActivitydate);
		ObjLanguageBe.setLastActivityDate(LastActivityDate);
		Boolean Status=false;
		try {
			Status = ObjLanguageTranslationService.addLanguageData(ObjLanguageBe);
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		SavedMessageValue=GLOBALCONSTANT.SavedValue;
		if(Status)
		{   
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.Language_Translations_Maintenance_Page);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			try{
				objLabels=ObjLanguageTranslationService.GetScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
			}catch(JASCIEXCEPTION objJasciexception){

				log.error(objJasciexception.getMessage());
			}
			ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageLabels,objLabels);
			ObjModelAndView.addObject(GLOBALCONSTANT.SavedValue,SavedMessageValue);
		}
		
		return ObjModelAndView;
    }
	
	
	
	/**
	 * Created on:Dec 19 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to edit the data to Languages Table
	 * Input parameter: LANGUAGETRANSLATIONBE
	 * Return Type :ModelAndView
	 * @param request 
	 * 
	 */
	
	
	@RequestMapping(value=GLOBALCONSTANT.Language_Translations_Search_Lookup_ActionEdit, method = RequestMethod.POST)
	public ModelAndView EditLanguageData(@ModelAttribute(GLOBALCONSTANT.Language_Translations_CommandNameEdit) LANGUAGETRANSLATIONBE ObjLanguageBe, HttpServletRequest request)
	{
		
		 /**Here we Check user is already logged in or not*/
		if(GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getUserID()) || GLOBALCONSTANT.BlankString.equals(OBJCOMMONSESSIONBE.getPassword())){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		
		WEBSERVICESTATUS objWEBSERVICESTATUS = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_Language_Translations_Execution);

		if (!objWEBSERVICESTATUS.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWEBSERVICESTATUS.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		String LastActivityDate=request.getParameter(GLOBALCONSTANT.GetLastActivitydate);
		String StrTeamMember=OBJCOMMONSESSIONBE.getTeam_Member();
		ObjLanguageBe.setLastActivityBy(StrTeamMember);
		ObjLanguageBe.setLastActivityDate(LastActivityDate);
		
		/** Call EditLanguageData() to update the language table. */
		Boolean Status=false;
		try{
			Status =ObjLanguageTranslationService.editLanguageData(ObjLanguageBe);
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		UpdatedMessageValue=GLOBALCONSTANT.SavedValue;
		if(Status)
		{
			
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.Language_Translations_Maintenance_EditPage);
			ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			try {
				objLabels=ObjLanguageTranslationService.GetScreenLabels(OBJCOMMONSESSIONBE.getCurrentLanguage());
			}catch(JASCIEXCEPTION objJasciexception){

				log.error(objJasciexception.getMessage());
			}
			 ObjModelAndView.addObject(GLOBALCONSTANT.LanguageTranslation_LanguageLabels,objLabels);
			
			ObjModelAndView.addObject(GLOBALCONSTANT.SavedValue,UpdatedMessageValue);
		}
		
		return ObjModelAndView;
    }
	
	/**
	 * Created on:Dec 19 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to show the language data on grid from Languages Table.
	 * Input parameter: none
	 * Return Type : List<LANGUAGETRANSLATIONBE>
	 * 
	 */
	
	
	@RequestMapping(value = GLOBALCONSTANT.Language_Translation_LanguageData_Read, method = RequestMethod.GET)
	public @ResponseBody   List<LANGUAGETRANSLATIONBE>  getLanguageList()
	{	
		String StrTenant=OBJCOMMONSESSIONBE.getTenant();
		String StrCompany=OBJCOMMONSESSIONBE.getCompany();
		List<LANGUAGETRANSLATIONBE> ObjLanguageList=null;
        
        if(!StrSelectedLanguage.equals(GLOBALCONSTANT.BlankString))
        {
        	  try{
        		  ObjLanguageList=ObjLanguageTranslationService.GetLanguageDataOnLanguage(StrSelectedLanguage,StrTenant,StrCompany,StrGeneralCodeId);
        	  }catch(JASCIEXCEPTION objJasciexception){

      			log.error(objJasciexception.getMessage());
      		}
        }
        else if(!StrKeyPhrase.equals(GLOBALCONSTANT.BlankString))
        {
        	  try{
        		  ObjLanguageList=ObjLanguageTranslationService.GetLanguageDataOnKeyPhrase(StrKeyPhrase,StrTenant,StrCompany,StrGeneralCodeId);
        	  }catch(JASCIEXCEPTION objJasciexception){

      			log.error(objJasciexception.getMessage());
      		}
        }
        return ObjLanguageList;
	}
		
	
	/**
	 * Created on:Dec 19 2014
	 * Created by:Diksha Gupta
	 * Description: This function has designed to get language data to be updated from Languages Table.
	 * Input parameter: HttpServletRequest
	 * Return Type : ModelAndView
	 * 
	 */
	
	 @RequestMapping(GLOBALCONSTANT.Language_Translations_GetToEdit)
		public ModelAndView GetDataLanguage(HttpServletRequest Request) 
		{
		 
		 StrLanguageEdit=Request.getParameter(GLOBALCONSTANT.LanguageVal);
		 StrKeyPhraseEdit=Request.getParameter(GLOBALCONSTANT.KeyPhraseVal);
		 String StrDescription=Request.getParameter(GLOBALCONSTANT.Description);
		 LANGUAGETRANSLATIONBE ObjListLanguageTranslationBe=new LANGUAGETRANSLATIONBE();
		 /** call getLanguageData() to be updated */
		try{
			 ObjListLanguageTranslationBe =ObjLanguageTranslationService.getLanguageData(StrLanguageEdit,StrKeyPhraseEdit,StrDescription);
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		 ObjLanguageEdit=ObjListLanguageTranslationBe;
		 ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Language_Translations_MaintenanceEdit_Action);
		 return ObjModelAndView;
		    
		}
	  
	 
	 /**
		 * Created on:Dec 19 2014
		 * Created by:Diksha Gupta
		 * Description: This function has designed to delete language data from Languages Table.
		 * Input parameter: HttpServletRequest
		 * Return Type : ModelAndView
		 * 
		 */
		

	
	 @RequestMapping(GLOBALCONSTANT.Language_Translations_Delete)
		public ModelAndView DeleteMessage(HttpServletRequest Request) 
		
		{
		String Language= Request.getParameter(GLOBALCONSTANT.LanguageVal);
		String KeyPhrase=Request.getParameter(GLOBALCONSTANT.KeyPhraseVal);
		
		/** call deleteLanguage() to delete the record. */
	   	try {
			 ObjLanguageTranslationService.DeleteLanguage(Language,KeyPhrase);
	   	}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
	   	Boolean Result=false;
		
	   	if(!StrSelectedLanguage.equals(GLOBALCONSTANT.BlankString))
        {
	   		try {
				Result=ObjLanguageTranslationService.CheckLanguage(StrSelectedLanguage);
	   		}catch(JASCIEXCEPTION objJasciexception){

				log.error(objJasciexception.getMessage());
			}
        }
        else if(!StrKeyPhrase.equals(GLOBALCONSTANT.BlankString))
        {
        	 try {
				Result=ObjLanguageTranslationService.CheckKeyPhrase(StrKeyPhrase);
        	 }catch(JASCIEXCEPTION objJasciexception){

     			log.error(objJasciexception.getMessage());
     		}
        }
	   	   
	   	   if(!Result)
	   	   {
	   		   ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Language_Translations_Lookup_Action);
	   	   }
	   	   else
	   	   {
	   		   ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Language_Translations_Search_Lookup_Action);
	   	   }

		   return ObjModelAndView;
		}
	 
     
	
}
