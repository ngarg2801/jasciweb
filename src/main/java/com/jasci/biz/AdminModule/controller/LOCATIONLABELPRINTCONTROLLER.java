/**

Date Developed :Dec 31 2014
Created by: Rahul kumar
Description :LOCATIONLABELPRINT Controller TO HANDLE THE REQUESTS FROM SCREENS
 */

package com.jasci.biz.AdminModule.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.ServicesApi.LOCATIONMAINTENANCEAPI;
import com.jasci.biz.AdminModule.be.LOCATIONLABELPRINTSCREENBE;
import com.jasci.biz.AdminModule.model.FULFILLMENTCENTERSPOJO;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.ILOCATIONLABELPRINTSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class LOCATIONLABELPRINTCONTROLLER {
	
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;	
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	@Autowired
	ILOCATIONLABELPRINTSERVICE objlocationlabelprintservice;
	
	@Autowired
	LOCATIONMAINTENANCEAPI objLocationAPI;
	
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;
	
	
	static Logger log= LoggerFactory.getLogger(LOCATIONLABELPRINTCONTROLLER.class);
	ModelAndView ObjModelAndView=new ModelAndView();
	
	
	/**
	 * 
	 * @Description This function redirect to Location label print Lookup screen
	 * @author Rahul Kumar
	 * @Date Jan 2, 2015 
	 * @return
	 * @Return type ModelAndView
	 */
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_LocationLabelPrint_Action, method = RequestMethod.GET)
	public ModelAndView showCompanyLookup(){

		List<FULFILLMENTCENTERSPOJO> objFulfillmentcenterspojos=null;
		String StrLanguage=OBJCOMMONSESSIONBE.getCurrentLanguage();
		String StrUserid=OBJCOMMONSESSIONBE.getUserID();
		String StrPassword=OBJCOMMONSESSIONBE.getPassword();
		LOCATIONLABELPRINTSCREENBE objLocationlabelprintscreenbe=null;
		/*List<GENERALCODES> objGeneralcodesLocLabel=null;
		List<GENERALCODES> objGeneralcodesAreas=null;*/

		if(GLOBALCONSTANT.BlankString.equals(StrUserid) || GLOBALCONSTANT.BlankString.equals(StrPassword)){

			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/

		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_LocationLabelPrint_Maintenmence_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		try{
			//get fullfillment center list  edit by sarvendra tyagi
			objFulfillmentcenterspojos=objLocationAPI.getFulFillmentCenter(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getCompany(),"Copy");
			
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}

		try{
			objLocationlabelprintscreenbe=objlocationlabelprintservice.setScreenLanguage(LANGUANGELABELSAPI.StrLocationLabelPrintModule,StrLanguage);
			
		}catch(JASCIEXCEPTION objJasciexception){

			log.error(objJasciexception.getMessage());
		}
		

		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_LocationLabelPrint_Action);
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationLabelPrintSceernLabelObj,objLocationlabelprintscreenbe);
		try{
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationLabelPrintLocLabelListObj,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_LOCLABEL,GLOBALCONSTANT.BlankString));
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationLabelPrintAreasListObj,ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),GLOBALCONSTANT.GIC_AREAS,GLOBALCONSTANT.BlankString));
		}catch(JASCIEXCEPTION objJasciexception){
			log.error(objJasciexception.getMessage());
		}
		ObjModelAndView.addObject(GLOBALCONSTANT.LocationLabelPrintFullfillmentCenterListObj,objFulfillmentcenterspojos);
		
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

		return ObjModelAndView;

	}


}
