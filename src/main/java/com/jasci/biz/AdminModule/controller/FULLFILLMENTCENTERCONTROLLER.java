/**
Date Developed :Dec 22 2014
Created by: Deepak Sharma
Description :FULLFILLMENTCENTERCONTROLLER Controller TO HANDLE THE REQUESTS FROM SCREENS
 */
package com.jasci.biz.AdminModule.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jasci.biz.AdminModule.be.FULFILLMENTCENTERSCREENBE;
import com.jasci.biz.AdminModule.be.FULLFILLMENTCENTERBE;
import com.jasci.biz.AdminModule.model.GENERALCODES;
import com.jasci.biz.AdminModule.model.WEBSERVICESTATUS;
import com.jasci.biz.AdminModule.service.FULLFILLMENTCENTERSERVICE;
import com.jasci.biz.AdminModule.service.SCREENACCESSSERVICE;
import com.jasci.biz.AdminModule.service.SECURITYAUTHORIZATIONSERVICE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.util.LOOKUPGENERALCODE;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;

@Controller
public class FULLFILLMENTCENTERCONTROLLER {

	@Autowired
	FULLFILLMENTCENTERSERVICE objFulfillmentCenterService;
	@Autowired
	SECURITYAUTHORIZATIONSERVICE objSecurityauthorizationService;
	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;
	
	@Autowired
	SCREENACCESSSERVICE screenAccessService;
	/** Make the instance of LOOKUPGENERALCODEDAOIMPL for using get the list of generalcode dropdown using method GetLookupGeneralCode*/
	@Autowired
	LOOKUPGENERALCODE ObjLookUpGeneralCode;

	private static final Logger logger = LoggerFactory.getLogger(FULLFILLMENTCENTERCONTROLLER.class);
	
	
	String StrTenant = GLOBALCONSTANT.BlankString;
	String LanguageCode = GLOBALCONSTANT.BlankString;
	String StrModuleName = GLOBALCONSTANT.FulfillmentCenterModule;
	String StrCompany = GLOBALCONSTANT.BlankString;
	
	String StrCountryGeneralCodeID = GLOBALCONSTANT.GIC_COUNTRYCODE, StrStatesGeneralCodeID = GLOBALCONSTANT.GIC_STATECODE, StrTeamMember = GLOBALCONSTANT.BlankString;


	
	
	/**
	 * screen get request of fullfillment center look up
	 * 
	 * @param objRequest
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_fullfillmentCenterLookup, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterLookup(HttpServletRequest objRequest/*,Device device*/) {
		
		SetDefaultVariables();
		
		if(GLOBALCONSTANT.IsFullfillmentLogging)
		{
			logger.info(GLOBALCONSTANT.Fulfillment_center_look_up_controller_is_called);
		}
		
	/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		ModelAndView objModelAndView = new ModelAndView(GLOBALCONSTANT.Fullfullment_V_N_fullfillment_center_lookup);
		String StrPageRedirectLoadMessage = objRequest.getParameter(GLOBALCONSTANT.Fullfullment_R_P_PageRedirectLoadMessage);

		FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe = null;
		try {
			objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
		} catch (JASCIEXCEPTION e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewLabels,
				objFulfillmentcenterscreenbe);
		if (!IsNullOrBlank(StrPageRedirectLoadMessage)) {
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_PageRedirectLoadMessage,
					StrPageRedirectLoadMessage);
		}
		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		
		objModelAndView.addObject(GLOBALCONSTANT.viewTenant, OBJCOMMONSESSIONBE.getTenant());
		return objModelAndView;

	}

	// fullfillment_search_lookup

	/**
	 * controller when user click on search icons of center look up
	 * 
	 * @param objRequest
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterLookupFormSubmit, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterLookupSubmit(HttpServletRequest objRequest) {
		
		
		SetDefaultVariables();
		ModelAndView objModelAndView = null;
		
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=null;
		try {
			objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
		} catch (JASCIEXCEPTION e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}
		
		String StrFufffillmentID = objRequest.getParameter(GLOBALCONSTANT.Fullfullment_R_P_FufffillmentID);
		String StrPartOfFullfillMentCenterName = objRequest
				.getParameter(GLOBALCONSTANT.Fullfullment_R_P_PartOfFullfillMentCenterName);
		String StrPartOfFullfillment = objRequest.getParameter(GLOBALCONSTANT.Fullfullment_R_P_PartOfFullfillment);
		String StrSearchedText = GLOBALCONSTANT.BlankString;
		boolean IsSearchByFullfillmentID = false, IsSearchByPartOfFullfillMentCenterName = false, IsSearchByPartOfFullfillment = false;

		if (!IsNullOrBlank(StrFufffillmentID)) {
			StrSearchedText = StrFufffillmentID;
			IsSearchByFullfillmentID = true;
		} else if (!IsNullOrBlank(StrPartOfFullfillMentCenterName)) {
			StrSearchedText = StrPartOfFullfillMentCenterName;
			IsSearchByPartOfFullfillMentCenterName = true;
		} else if (!IsNullOrBlank(StrPartOfFullfillment)) {
			StrSearchedText = StrPartOfFullfillment;
			IsSearchByPartOfFullfillment = true;
		}

		// System.out.println(StrSearchedText+"\t"+IsSearchByFullfillmentID+"\t"+IsSearchByPartOfFullfillment+"\t"+IsSearchByPartOfFullfillMentCenterName);
		List<FULLFILLMENTCENTERBE> lstFulfillmentCenterBE = new ArrayList<FULLFILLMENTCENTERBE>();
		try {
			lstFulfillmentCenterBE = objFulfillmentCenterService.searchForfulfillmentCenter(
					StrSearchedText, IsSearchByFullfillmentID, IsSearchByPartOfFullfillMentCenterName,
					IsSearchByPartOfFullfillment, StrTenant);
		} catch (JASCIEXCEPTION e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}

		if (lstFulfillmentCenterBE.size() == 0) {
			if (IsSearchByFullfillmentID) {
				objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
						+ GLOBALCONSTANT.Fullfullment_M_fullfillmentCenterLookup);
				objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_PageRedirectLoadMessage,
						objFulfillmentcenterscreenbe.getERR_Invalid_Fulfillment_Center());
				return objModelAndView;

			} else if (IsSearchByPartOfFullfillMentCenterName) {
				objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
						+ GLOBALCONSTANT.Fullfullment_M_fullfillmentCenterLookup);
				objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_PageRedirectLoadMessage,
						objFulfillmentcenterscreenbe.getERR_Invalid_Part_of_the_fulfillment());
				return objModelAndView;

			}
		} else {
			HttpSession objSession = objRequest.getSession();
			objSession.setAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_lstOfFullFillmentCenterView,
					lstFulfillmentCenterBE);

			if (IsSearchByFullfillmentID) {

				/**
				 * When record found for fulfillment id and going to update the
				 * fulfillment
				 */

				objSession
						.setAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_FullfillmentID, StrSearchedText);
				objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
						+ GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterMaintenance);
				// return objModelAndView;
				// objModelAndView =new
				// ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.Fullfullment_M_fullfillmentCenterLookup);
				// objModelAndView.addObject("PageRedirectLoadMessage","Not a valid fullfillment");
				return objModelAndView;

			} else if (IsSearchByPartOfFullfillMentCenterName) {
				// HttpSession objSession=objRequest.getSession();
				objSession.setAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_PartOfFullfillmentName,
						StrSearchedText);
				objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
						+ GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterSearchByName);
				// objModelAndView.addObject("PageRedirectLoadMessage","Not a valid fullfillment Center Name");
				return objModelAndView;

			}

		}

		return null;

	}

	/**
	 * User search by Fullfillment center name
	 * 
	 * @param objRequest
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterSearchByName, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterSearchByName(HttpServletRequest objRequest) {
		// System.out.println("Display All");

		SetDefaultVariables();
		ModelAndView objModelAndView = new ModelAndView();
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		HttpSession objSession = objRequest.getSession();
		FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=null;
		try {
			objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
		} catch (JASCIEXCEPTION e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}
		String StrPartOfFullfillmentName = null;

		if (objSession != null) {
			StrPartOfFullfillmentName = (String) objSession
					.getAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_PartOfFullfillmentName);

		}

		if (StrPartOfFullfillmentName != null) {

			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewLabels,
					objFulfillmentcenterscreenbe);
			objModelAndView.setViewName(GLOBALCONSTANT.Fullfullment_V_N_Fulfillment_search_lookup);
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_KendroReadURL,
					GLOBALCONSTANT.Fullfullment_M_SearchByFulfillmentName);

		} else {
			objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
					+ GLOBALCONSTANT.Fullfullment_M_fullfillmentCenterLookup);

		}

		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return objModelAndView;
	}

	/**
	 * Controller when user click on display all on center look up and redirect
	 * to search look up
	 * 
	 * @param objRequest
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterDisplayAllLookUp, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterSearchLookUp(HttpServletRequest objRequest) {
		

		SetDefaultVariables();
		ModelAndView objModelAndView = new ModelAndView();
		
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=null;
		try {
			objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
		} catch (JASCIEXCEPTION e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewLabels,
				objFulfillmentcenterscreenbe);
		objModelAndView.setViewName(GLOBALCONSTANT.Fullfullment_V_N_Fulfillment_search_lookup);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_KendroReadURL,
				GLOBALCONSTANT.Fullfullment_M_DisplayAllFulfillment);
		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return objModelAndView;
	}

	/**
	 * Controller when user click on display all on center loop up
	 * 
	 * @param objRequest
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterLookupDisplayAllFormSubmit, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterLookupDisplayAll(HttpServletRequest objRequest) {

		SetDefaultVariables();
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		ModelAndView objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
				+ GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterDisplayAllLookUp);
		return objModelAndView;

		// return null;
	}

	/**
	 * When user click on new on center look up
	 * 
	 * @param objRequest
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterLookupNewRecordFormSubmit, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterLookupNewRecord(HttpServletRequest objRequest) {
		// System.out.println("New Record");
		SetDefaultVariables();
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		ModelAndView objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
				+ GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterMaintenance_new);
		return objModelAndView;
		// return null;
	}

	/**
	 * 
	 * @param objRequest
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterMaintenance_new, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterMaintenanceNew(HttpServletRequest objRequest) {

		FULLFILLMENTCENTERBE objFullfillmentcenterbe = new FULLFILLMENTCENTERBE();
		ModelAndView objModelAndView = new ModelAndView();
		SetDefaultVariables();
		
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		
		FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=null;
		try {
			objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
		} catch (JASCIEXCEPTION e1) {
			
			e1.printStackTrace();
		}
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewLabels,
				objFulfillmentcenterscreenbe);
		objModelAndView.setViewName(GLOBALCONSTANT.Fullfullment_V_N_Fullfillment_Centre_Maintenance);

		
		objFullfillmentcenterbe.setSetUpDate(getCurrentDate(GLOBALCONSTANT.yyyyMMdd_Format_v1));
		objFullfillmentcenterbe.setLastActivityDateString(getCurrentDate(GLOBALCONSTANT.yyyyMMdd_Format_v1));
		objFullfillmentcenterbe.setLastActivityTeamMember(OBJCOMMONSESSIONBE.getTeam_Member_Name());
		objFullfillmentcenterbe.setSetUpBy(StrTeamMember);
		objFullfillmentcenterbe.setSetUpByName(OBJCOMMONSESSIONBE.getTeam_Member_Name());
		
		
		
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowSecondContactDiv, checkForContactDetails(objFullfillmentcenterbe, 2));
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowThirdContactDiv, checkForContactDetails(objFullfillmentcenterbe, 3));
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowFourthContactDiv, checkForContactDetails(objFullfillmentcenterbe, 4));
		try {
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewCountryCodelst, ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),StrCountryGeneralCodeID,GLOBALCONSTANT.BlankString));
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewStatesCodelst, ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),StrStatesGeneralCodeID,GLOBALCONSTANT.BlankString));
		} catch (JASCIEXCEPTION e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewFulfillment, objFullfillmentcenterbe);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_viewIsFulfillmentExists, GLOBALCONSTANT.Security_StringFalse);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewTenant, StrTenant);
		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return objModelAndView;

	}

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_AddUpdateFullfillmentCenter, method = RequestMethod.POST)
	public ModelAndView AddUpdateFullfillmentCenter(
			@ModelAttribute(GLOBALCONSTANT.Fullfullment_V_P_ViewFulfillment) FULLFILLMENTCENTERBE objFullfillmentcenterbe,
			HttpServletRequest objRequest) {
		SetDefaultVariables();
		String StrSetUpDate=objRequest.getParameter(GLOBALCONSTANT.SetUpDateH);
		String StrLastActivityDateString=objRequest.getParameter(GLOBALCONSTANT.LastActivityDateStringH);
	
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}
		ModelAndView objModelAndView = new ModelAndView();
		objFullfillmentcenterbe.setTenant(StrTenant);
		//objFullfillmentcenterbe.setLastActivityDate(getDBDate());
		//objFullfillmentcenterbe.setLastActivityDateString(getCurrentDate(GLOBALCONSTANT.dd_MMM_yyyy).toUpperCase());
		objFullfillmentcenterbe.setLastActivityDate(StringToDate(StrLastActivityDateString));
		objFullfillmentcenterbe.setLastActivityDateString(StrLastActivityDateString);
				
		String StrSetUpDateDisplay=null;
		if (!IsNullOrBlank(StrSetUpDate)) {
			//StrSetUpDateDisplay=objFullfillmentcenterbe.getSetUpDate();
			//StrSetUpDateDisplay=StrSetUpDate;
			
			objFullfillmentcenterbe.setSetUpDate(ConvertDate(StrSetUpDate,
					GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.dd_MMM_yyyy));
		} else {
			objFullfillmentcenterbe.setSetUpBy(StrTeamMember);
			StrSetUpDateDisplay=objFullfillmentcenterbe.getSetUpDate();
			objFullfillmentcenterbe.setSetUpDate(ConvertDate(StrSetUpDateDisplay,
					GLOBALCONSTANT.yyyyMMdd_Format_v1, GLOBALCONSTANT.dd_MMM_yyyy));
			//objFullfillmentcenterbe.setSetUpDate(StrSetUpDateDisplay);
		}
		objFullfillmentcenterbe.setLastActivityTeamMember(StrTeamMember);

		objFullfillmentcenterbe.setSTATUS(GLOBALCONSTANT.A);
		WEBSERVICESTATUS objWEBSERVICESTATUS = null;
		try {
			objWEBSERVICESTATUS = objFulfillmentCenterService
					.AddUpdateFullfillmentCenter(objFullfillmentcenterbe);
		} catch (JASCIEXCEPTION e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}

		
		if (objWEBSERVICESTATUS.isBoolStatus()) {
			//System.out.println("Status of add or delete" + objWEBSERVICESTATUS.isBoolStatus());

			FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=null;
			try {
				objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
			} catch (JASCIEXCEPTION e) {
				
				e.printStackTrace();
			}
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewLabels,
					objFulfillmentcenterscreenbe);
			
			
			objFullfillmentcenterbe.setLastActivityDateString(StrLastActivityDateString);
				objFullfillmentcenterbe.setSetUpDate(StrSetUpDateDisplay);
				objFullfillmentcenterbe.setLastActivityTeamMember(OBJCOMMONSESSIONBE.getTeam_Member_Name());
			
			
			objModelAndView.setViewName(GLOBALCONSTANT.Fullfullment_V_N_Fullfillment_Centre_Maintenance);
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewFulfillment, objFullfillmentcenterbe);
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_viewIsFulfillmentExists, objFullfillmentcenterbe.getIsNew());
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewTenant, StrTenant);

			if (objFullfillmentcenterbe.getIsNew().toUpperCase().equalsIgnoreCase(GLOBALCONSTANT.Security_StringTrue.toUpperCase())) {
				objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewIsSuccessMessage, objFulfillmentcenterscreenbe.getERR_Your_record_has_been_updated_successfully());
			} else {
				objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewIsSuccessMessage, objFulfillmentcenterscreenbe.getERR_Your_record_has_been_saved_successfully());
			}

		}

		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return objModelAndView;

	}

	/**
	 * Maintenance screen
	 * 
	 * @param objRequest
	 * @return
	 */

	@SuppressWarnings({ GLOBALCONSTANT.Strunused, GLOBALCONSTANT.Strunchecked })
	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterMaintenance, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterMaintenance(HttpServletRequest objRequest) {
		// System.out.println("New Record");
		SetDefaultVariables();
		String StrFulFillmentID = null;

		// String IsSearchByRequest="0";
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		ModelAndView objModelAndView = new ModelAndView();
		FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=null;
		try {
			objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
		} catch (JASCIEXCEPTION e1) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e1.getMessage());
			}
		}
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewLabels,
				objFulfillmentcenterscreenbe);
		List<FULLFILLMENTCENTERBE> lstFulfillmentCenterBE = null;
		List<GENERALCODES> objGeneralcodesCountryCode = null;

		FULLFILLMENTCENTERBE objFullfillmentcenterbe = null;
		HttpSession objHttpSession = objRequest.getSession();
		if (objHttpSession != null) {
			lstFulfillmentCenterBE = (List<FULLFILLMENTCENTERBE>) objHttpSession
					.getAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_lstOfFullFillmentCenterView);
			StrFulFillmentID = (String) objHttpSession
					.getAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_FullfillmentID);

		}

		/**
		 * if we get the null fulfillment id then redirect to center look up
		 */

		String IsSearchById = GLOBALCONSTANT.Security_StringTrue;
		if (StrFulFillmentID == null) {

			IsSearchById = GLOBALCONSTANT.Security_StringFalse;

			objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
					+ GLOBALCONSTANT.Fullfullment_M_fullfillmentCenterLookup);

			return objModelAndView;
		}
		/**
		 * if we get the null list from session
		 */
		if (lstFulfillmentCenterBE == null) {
			try {
				lstFulfillmentCenterBE = objFulfillmentCenterService.searchForfulfillmentCenter(StrFulFillmentID, true,
						false, false, StrTenant);
			} catch (JASCIEXCEPTION e) {
				if(GLOBALCONSTANT.IsFullfillmentLogging)
				{
					logger.error(e.getMessage());
				}
			}
		} else {
			/**
			 * setting the value to null so we get the updated data as per
			 * screen refresh
			 */
			objHttpSession.setAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_lstOfFullFillmentCenterView,
					null);

		}

		if (lstFulfillmentCenterBE.size() > GLOBALCONSTANT.INT_ZERO) {
			objFullfillmentcenterbe = lstFulfillmentCenterBE.get(GLOBALCONSTANT.INT_ZERO);
			try {
				objFullfillmentcenterbe.setLastActivityTeamMember(objSecurityauthorizationService.getTeamMemberName(objFullfillmentcenterbe.getLastActivityTeamMember()));
				objFullfillmentcenterbe.setSetUpByName(objSecurityauthorizationService.getTeamMemberName(objFullfillmentcenterbe.getSetUpBy()));

				
			} catch (JASCIEXCEPTION objJasci) {
				
				logger.error(objJasci.getMessage());
			}
			
			if (objFullfillmentcenterbe.getLastActivityDate()!=null) {
				
				
				objFullfillmentcenterbe.setLastActivityDateString(ConvertDate(objFullfillmentcenterbe.getLastActivityDate(),
						GLOBALCONSTANT.yyyyMMdd_Format_v1));
			}
			if (!IsNullOrBlank(objFullfillmentcenterbe.getSetUpDate())) {
				objFullfillmentcenterbe.setSetUpDate(ConvertDate(objFullfillmentcenterbe.getSetUpDate(),
						GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.yyyyMMdd_Format_v1));
			}
		}

		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowSecondContactDiv, checkForContactDetails(objFullfillmentcenterbe, 2));
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowThirdContactDiv, checkForContactDetails(objFullfillmentcenterbe, 3));
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowFourthContactDiv, checkForContactDetails(objFullfillmentcenterbe, 4));
		/*objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewCountryCodelst, lstGeneralCode);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewStatesCodelst, lstStateGeneralCode);*/
		try {
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewCountryCodelst, ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),StrCountryGeneralCodeID,GLOBALCONSTANT.BlankString));
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewStatesCodelst, ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),StrStatesGeneralCodeID,GLOBALCONSTANT.BlankString));
			} catch (JASCIEXCEPTION e) {
				if(GLOBALCONSTANT.IsFullfillmentLogging)
				{
					logger.error(e.getMessage());
				}
			}
		objModelAndView.setViewName(GLOBALCONSTANT.Fullfullment_V_N_Fullfillment_Centre_Maintenance);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewFulfillment, objFullfillmentcenterbe);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_viewIsFulfillmentExists, GLOBALCONSTANT.Security_StringTrue);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewTenant, StrTenant);
		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return objModelAndView;
	}

	
	/**
	 * 
	 * @param objRequest
	 * @return
	 */
	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_FullfillmentCenterMaintenanceUpdate, method = RequestMethod.GET)
	public ModelAndView getFullfillmentCenterMaintenanceUpdate(HttpServletRequest objRequest) {
		// System.out.println("New Record");
		SetDefaultVariables();
		String StrFulFillmentID = null;
		
/** Here we Check user is logged in or not */
		
		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		/** Here we Check user have this screen access or not*/
		
		WEBSERVICESTATUS objWebServiceStatus = screenAccessService.checkScreenAccess(OBJCOMMONSESSIONBE.getTenant(), OBJCOMMONSESSIONBE.getTeam_Member(),
				GLOBALCONSTANT.RequestMapping_FullfillmentCenter_lookup_Execution);

		if (!objWebServiceStatus.isBoolStatus()) {
			ModelAndView ObjModelAndView = new ModelAndView(GLOBALCONSTANT.Security_M_notAccess,GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Status, objWebServiceStatus.getStrMessage());
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message, GLOBALCONSTANT.Security_NotaccessMessage);
			return ObjModelAndView;

		}

		// String IsSearchByRequest="0";
		StrFulFillmentID = (String) objRequest.getParameter(GLOBALCONSTANT.Fullfullment_R_P_FulfillmentID);

		ModelAndView objModelAndView = new ModelAndView();
		List<FULLFILLMENTCENTERBE> lstFulfillmentCenterBE = null;
		FULLFILLMENTCENTERBE objFullfillmentcenterbe = null;

		/**
		 * if we get the null fulfillment id then redirect to center look up
		 */

		//String IsSearchById = GLOBALCONSTANT.Security_StringTrue;
		if (StrFulFillmentID == null) {

			//String IsSearchById = GLOBALCONSTANT.Security_StringFalse;

			objModelAndView = new ModelAndView(GLOBALCONSTANT.redirect + GLOBALCONSTANT.colon
					+ GLOBALCONSTANT.Fullfullment_M_fullfillmentCenterLookup);

			return objModelAndView;
		}
		/**
		 * if we get the null list from session
		 */
		if (lstFulfillmentCenterBE == null) {
			try {
				lstFulfillmentCenterBE = objFulfillmentCenterService.searchForfulfillmentCenter(StrFulFillmentID, true,
						false, false, StrTenant);
			} catch (JASCIEXCEPTION e) {
				if(GLOBALCONSTANT.IsFullfillmentLogging)
				{
					logger.error(e.getMessage());
				}
			}
		}

		if (lstFulfillmentCenterBE.size() > GLOBALCONSTANT.INT_ZERO) {
			objFullfillmentcenterbe = lstFulfillmentCenterBE.get(GLOBALCONSTANT.INT_ZERO);
			
			try {
				objFullfillmentcenterbe.setLastActivityTeamMember(objSecurityauthorizationService.getTeamMemberName(objFullfillmentcenterbe.getLastActivityTeamMember()));
				objFullfillmentcenterbe.setSetUpByName(objSecurityauthorizationService.getTeamMemberName(objFullfillmentcenterbe.getSetUpBy()));

				
			} catch (JASCIEXCEPTION objJasci) {
				
				logger.error(objJasci.getMessage());
			}
			
				if (objFullfillmentcenterbe.getLastActivityDate()!=null) {
				
				
				objFullfillmentcenterbe.setLastActivityDateString(ConvertDate(objFullfillmentcenterbe.getLastActivityDate(),
						GLOBALCONSTANT.yyyyMMdd_Format_v1));
			}
			if (!IsNullOrBlank(objFullfillmentcenterbe.getSetUpDate())) {
				objFullfillmentcenterbe.setSetUpDate(ConvertDate(objFullfillmentcenterbe.getSetUpDate(),
						GLOBALCONSTANT.yyyy_mm_dd_hh_mm_ss_Format, GLOBALCONSTANT.yyyyMMdd_Format_v1));
			}
		}

		FULFILLMENTCENTERSCREENBE objFulfillmentcenterscreenbe=null;
		try {
			objFulfillmentcenterscreenbe = objFulfillmentCenterService.getScreenLabels(StrModuleName, LanguageCode);
		} catch (JASCIEXCEPTION e) {
			
			e.printStackTrace();
		}
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewLabels,
				objFulfillmentcenterscreenbe);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowSecondContactDiv, checkForContactDetails(objFullfillmentcenterbe, 2));
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowThirdContactDiv, checkForContactDetails(objFullfillmentcenterbe, 3));
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewShowFourthContactDiv, checkForContactDetails(objFullfillmentcenterbe, 4));
		
		try {
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewCountryCodelst, ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),StrCountryGeneralCodeID,GLOBALCONSTANT.BlankString));
			objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewStatesCodelst, ObjLookUpGeneralCode.Drop_Down_Selection(OBJCOMMONSESSIONBE.getTenant(),OBJCOMMONSESSIONBE.getCompany(),StrStatesGeneralCodeID,GLOBALCONSTANT.BlankString));
			} catch (JASCIEXCEPTION e) {
				if(GLOBALCONSTANT.IsFullfillmentLogging)
				{
					logger.error(e.getMessage());
				}
			}
		objModelAndView.setViewName(GLOBALCONSTANT.Fullfullment_V_N_Fullfillment_Centre_Maintenance);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewFulfillment, objFullfillmentcenterbe);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_viewIsFulfillmentExists, GLOBALCONSTANT.Security_StringTrue);
		objModelAndView.addObject(GLOBALCONSTANT.Fullfullment_V_P_ViewTenant, StrTenant);
		objModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);
		return objModelAndView;
	}

	/**
	 * Kendo Display all fulfillment
	 * 
	 * @return
	 */

	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_DisplayAllFulfillment, method = RequestMethod.GET)
	public @ResponseBody List<FULLFILLMENTCENTERBE> getAllFulfillment() {
		SetDefaultVariables();
		List<FULLFILLMENTCENTERBE> lstFulfillmentCenterBE = null;

		try {
			lstFulfillmentCenterBE = objFulfillmentCenterService.getAllFulfillment(StrTenant);
		} catch (JASCIEXCEPTION objJasciexception) {

			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
			logger.error(objJasciexception.getMessage());
			}
		}

		return lstFulfillmentCenterBE;

	}

	/**
	 * Kendo call back method when user search by part of name
	 * 
	 * @param objRequest
	 * @return
	 */
	@SuppressWarnings(GLOBALCONSTANT.Strunchecked)
	@RequestMapping(value = GLOBALCONSTANT.Fullfullment_M_SearchByFulfillmentName, method = RequestMethod.GET)
	public @ResponseBody List<FULLFILLMENTCENTERBE> getFulfillmentByName(HttpServletRequest objRequest) {
		SetDefaultVariables();
		HttpSession objSession = objRequest.getSession();
		String StrPartOfFullfillmentName = null;
		List<FULLFILLMENTCENTERBE> lstFulfillmentCenterBE = null;
		if (objSession != null) {
			StrPartOfFullfillmentName = (String) objSession
					.getAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_PartOfFullfillmentName);
			lstFulfillmentCenterBE = (List<FULLFILLMENTCENTERBE>) objSession
					.getAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_lstOfFullFillmentCenterView);
			objSession.setAttribute(GLOBALCONSTANT.Session_Fullfullment_Key_Session_lstOfFullFillmentCenterView, null);
		}

		if (lstFulfillmentCenterBE == null) {
			try {
				lstFulfillmentCenterBE = objFulfillmentCenterService.searchForfulfillmentCenter(StrPartOfFullfillmentName,
						false, true, false, StrTenant);
			} catch (JASCIEXCEPTION e) {
				if(GLOBALCONSTANT.IsFullfillmentLogging)
				{
				logger.error(e.getMessage());
				}
			}
		}

		return lstFulfillmentCenterBE;

	}

	// AddUpdateFullfillmentCenter

	/**
	 * 
	 * @param objFullfillmentcenterbe
	 * @param intContactType
	 * @return
	 */
	public boolean checkForContactDetails(FULLFILLMENTCENTERBE objFullfillmentcenterbe, int intContactType) {
		boolean IsShowContactDetails = false;

		if (intContactType == GLOBALCONSTANT.INT_TWO) {
			if (!IsNullOrBlank(objFullfillmentcenterbe.getContactName2())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactPhone2())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactExtension2())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactFax2())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactEmail2())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactCell2())) {
				IsShowContactDetails = true;
			}

		} else if (intContactType == GLOBALCONSTANT.INT_THREE) {
			if (!IsNullOrBlank(objFullfillmentcenterbe.getContactName3())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactPhone3())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactExtension3())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactFax3())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactEmail3())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactCell3())) {
				IsShowContactDetails = true;
			}
		} else if (intContactType == GLOBALCONSTANT.INT_FOUR) {
			if (!IsNullOrBlank(objFullfillmentcenterbe.getContactName4())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactPhone4())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactExtension4())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactFax4())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactEmail4())) {
				IsShowContactDetails = true;
			} else if (!IsNullOrBlank(objFullfillmentcenterbe.getContactCell4())) {
				IsShowContactDetails = true;
			}
		}

		// if(objFullfillmentcenterbe.getContactName2())

		return IsShowContactDetails;
	}

	/**
	 * 
	 * @param StrInFormat
	 * @return
	 */
	public String getCurrentDate(String StrInFormat) {

		DateFormat dateFormat = new SimpleDateFormat(StrInFormat);
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());

	}

	public Date getCurrentDate() {

		return new Date();

	}

	/**
	 * 
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return
	 */
	public String ConvertDate(String dateInString, String StrInFormat, String StrOutFormat) {
		SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
		DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
		String StrFormattedString = dateInString;

		try {

			Date date = formatter.parse(dateInString);
			StrFormattedString = dateFormat.format(date);

		} catch (Exception e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				logger.error(e.getMessage());
			}
		}

		return StrFormattedString;

	}
	
	/**
	 * 
	 * @param dateInString
	 * @param StrInFormat
	 * @param StrOutFormat
	 * @return
	 */
	public String ConvertDate(Date date, String StrOutFormat) {
		//SimpleDateFormat formatter = new SimpleDateFormat(StrInFormat);
		DateFormat dateFormat = new SimpleDateFormat(StrOutFormat);
		String StrFormattedString = GLOBALCONSTANT.BlankString;

		try {

			//Date date = formatter.parse(dateInString);
			StrFormattedString = dateFormat.format(date);

		} catch (Exception e) {
			if(GLOBALCONSTANT.IsFullfillmentLogging)
			{
				//logger.error("Fulfillment- Parse Date: "+e.getMessage()+"\n inDateFormat:"+StrInFormat+"\n outDateFormat:"+StrOutFormat+"\n Date:"+dateInString);
			}
		}

		return StrFormattedString;

	}

	/**
	 * 
	 * @param StrText
	 * @return
	 */
	public boolean IsNullOrBlank(String StrText) {
		boolean boolStatus = true;

		if (StrText != null) {
			if (StrText.trim().length() > GLOBALCONSTANT.INT_ZERO) {
				boolStatus = false;
			}
		}

		return boolStatus;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean SetDefaultVariables()
	{
		
		
		StrTenant = OBJCOMMONSESSIONBE.getTenant();
		LanguageCode = OBJCOMMONSESSIONBE.getCurrentLanguage();
		StrCompany = OBJCOMMONSESSIONBE.getCompany();
		StrTeamMember = OBJCOMMONSESSIONBE.getTeam_Member();
		
		if(GLOBALCONSTANT.IsFullfillmentLogging)
		{
		logger.info(StrTenant);
		logger.info(LanguageCode);
		logger.info(StrCompany);
		logger.info(StrTeamMember);
		}
		
		
		return true;
	}
	public Date getDBDate()
	{
		Date ObjDate1 = new Date();
		DateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		Date StrCurrentDate=null;
		try {
			StrCurrentDate = ObjDateFormat1.parse(ObjDateFormat1.format(ObjDate1));
		} catch (ParseException e) {
			
			
		}
		
		return StrCurrentDate;
	}
	
	
	public Date StringToDate(String ObjDate1)
	{
		
		
		SimpleDateFormat ObjDateFormat1=new SimpleDateFormat(GLOBALCONSTANT.Date_Format);
		Date StrCurrentDate=null;
		try {
			StrCurrentDate = ObjDateFormat1.parse(ObjDate1);
		} catch (ParseException e) {
		
		}
		
		return StrCurrentDate;
	}
}
