/**

Date Developed:  Oct 14 2014
Developed by: Diksha Gupta
Description :  Login Controller to handle the requests from login screen
 */

package com.jasci.biz.AdminModule.controller;

import java.text.ParseException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jasci.biz.AdminModule.ServicesApi.LANGUANGELABELSAPI;
import com.jasci.biz.AdminModule.be.LOGINBE;
import com.jasci.biz.AdminModule.be.PASSWORDEXPIRYBE;
import com.jasci.biz.AdminModule.model.MENUMESSAGES;
import com.jasci.biz.AdminModule.model.SECURITYAUTHORIZATIONS;
import com.jasci.biz.AdminModule.model.SETNEWPASSWORD;
import com.jasci.biz.AdminModule.service.LOGINSERVICEIMPL;
import com.jasci.common.constant.CONFIGURATIONEFILE;
import com.jasci.common.constant.GLOBALCONSTANT;
import com.jasci.common.utilbe.COMMONSESSIONBE;
import com.jasci.exception.JASCIEXCEPTION;



@Controller
public class LOGINCONTROLLER 
{

	/** We use @Autowired annotation for inject bean properties*/
	@Autowired
	LOGINSERVICEIMPL ObjLoginServiceImpl;

	@Autowired
	COMMONSESSIONBE OBJCOMMONSESSIONBE;


	/** parameters declaration  and initialization*/
	List<MENUMESSAGES> ObjListMenuMessages=null;
	LOGINBE ObjLoginBe=null;
	PASSWORDEXPIRYBE ObjPasswordExpiryBe=null;
	ModelAndView ObjModelAndView=new ModelAndView();
	String ErrorMessage=GLOBALCONSTANT.BlankString;
	String EmailErrorMessage=GLOBALCONSTANT.BlankString;
	String StrResult=GLOBALCONSTANT.BlankString;
	int screentype = GLOBALCONSTANT.IntZero;
	String GlobalLastActivityDateStringH=GLOBALCONSTANT.BlankString;

	/**
	 *Description This function design to redirect on login page and display Menu messages
	 *Input parameter none
	 *Return Type "ModelAndView"
	 *Created By "Diksha Gupta"
	 *Created Date "oct 14 2014" 
	 **/
	String StrPath = GLOBALCONSTANT.BlankString;
	String footer = GLOBALCONSTANT.BlankString;
	String forvalidemailid = GLOBALCONSTANT.BlankString;
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_LoginScreen_Action, method = RequestMethod.GET)
	public ModelAndView getLoginPage(HttpServletRequest request)
	{

		
		try
		{
			/** call method getMessage() to get MenuMessages from database from table MenuMessages*/
			ObjListMenuMessages=ObjLoginServiceImpl.GetMessage();
			ObjLoginBe=ObjLoginServiceImpl.SetLoginBe();
			
			/** to get company logo and footer text from config file*/
			CONFIGURATIONEFILE ObjConfigurationFile=new CONFIGURATIONEFILE();
			StrPath = ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.CompanyLogo);
			if(StrPath.isEmpty())
			{
				StrPath = GLOBALCONSTANT.COMPANYLOGOVALUE;
			}
			footer =  ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.FOOTERTEXT);
			if(footer.isEmpty())
			{
				footer = GLOBALCONSTANT.FOOTERTEXTVALUE;
			}
			forvalidemailid =  ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.FORVALIDEMAILID);
			if(forvalidemailid.isEmpty())
			{
				forvalidemailid = GLOBALCONSTANT.FORVALIDEMAILIDVALUE;
			}

			try
			{
			  OBJCOMMONSESSIONBE.setJasci_Tenant(ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.Login_Config_Login_Tenant).toUpperCase());
			}
			catch(JASCIEXCEPTION objJasciException)
			{
			  OBJCOMMONSESSIONBE.setJasci_Tenant(GLOBALCONSTANT.JASCI.toUpperCase());
			}
			if(OBJCOMMONSESSIONBE.getJasci_Tenant().isEmpty())
			{
				OBJCOMMONSESSIONBE.setJasci_Tenant(GLOBALCONSTANT.JASCI.toUpperCase());
			}
			
			    /** Set the parameter value and url of Xtlytics page */
			   String XlaticsDashBoardPath=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.XlaticsDashBoardPath);
			   String CompIDValue=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.CompIDValue);
			   String APIKEYValue=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.APIKEYValue);
			   String SECRETKEYValue=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.SECRETKEYValue);
			   String UserIDValue=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.UserIDValue);
			   String ProjectNameValue=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.ProjectNameValue);
			   String DepartmentIDValue=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.DepartmentIDValue);
			   String DashboardIdValue=ObjConfigurationFile.getConfigProperty(GLOBALCONSTANT.DashboardIdValue);
			   if(XlaticsDashBoardPath.isEmpty()){
			    XlaticsDashBoardPath=GLOBALCONSTANT.XlaticsDashBoardPathCatch;
			    
			   }
			   if(CompIDValue.isEmpty()){
			    CompIDValue=GLOBALCONSTANT.CompIDCatch;
			   }
			   if(APIKEYValue.isEmpty()){
			    APIKEYValue=GLOBALCONSTANT.APIKEYCatch;
			   }
			   if(SECRETKEYValue.isEmpty()){
			    SECRETKEYValue=GLOBALCONSTANT.SECRETKEYCatch;
			   }
			   if(UserIDValue.isEmpty()){
			    UserIDValue=GLOBALCONSTANT.UserIDCatch;
			   }
			   if(ProjectNameValue.isEmpty()){
			    ProjectNameValue=GLOBALCONSTANT.ProjectNameCatch;
			   }
			   if(DepartmentIDValue.isEmpty()){
			    DepartmentIDValue=GLOBALCONSTANT.DepartmentIDCatch;
			   }
			   if(DashboardIdValue.isEmpty()){
			    DashboardIdValue=GLOBALCONSTANT.DashboardIdCatch;
			   }
			   
		        OBJCOMMONSESSIONBE.setXlaticsDashBoardPath(XlaticsDashBoardPath);       
		        OBJCOMMONSESSIONBE.setCompIDValue(CompIDValue);
		        OBJCOMMONSESSIONBE.setAPIKEYValue(APIKEYValue);  
		        OBJCOMMONSESSIONBE.setSECRETKEYValue(SECRETKEYValue);    
		        OBJCOMMONSESSIONBE.setUserIDValue(UserIDValue);     
		        OBJCOMMONSESSIONBE.setProjectNameValue(ProjectNameValue);
		        OBJCOMMONSESSIONBE.setDepartmentIDValue(DepartmentIDValue);
		        OBJCOMMONSESSIONBE.setDashboardIdValue(DashboardIdValue);
			/**Check user is already logged in or not if logged in then send home page*/
			
			if(!OBJCOMMONSESSIONBE.getUserID().equalsIgnoreCase(GLOBALCONSTANT.BlankString) && !OBJCOMMONSESSIONBE.getPassword().equalsIgnoreCase(GLOBALCONSTANT.BlankString) )
			{
				
				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuExecutionScreen_Action);	
			}
			/**Set footer value in the CommonSession obj*/
			
			OBJCOMMONSESSIONBE.setFooterText(footer);
			OBJCOMMONSESSIONBE.setForValidEmaildID(forvalidemailid);
			
			
			/** 
			 * Modelandview Object to send data to login page
			 * Model is a map through which we pass the objects to the page
			 * view is the page on the data is to be shown
			 *
			 */
			
 			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_LoginScreen_loginPage);
			ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping__LoginScreen_ScreenLabelsObject,ObjLoginBe);
			ObjModelAndView.addObject(GLOBALCONSTANT.CompanyLogo, StrPath);
			
			

			if(!ErrorMessage.equals(GLOBALCONSTANT.BlankString))
			{
				ObjModelAndView.addObject(GLOBALCONSTANT.ErrorMessage,ErrorMessage);
				ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ResponseObject,true);
			}
			else
			{
				ObjModelAndView.addObject(GLOBALCONSTANT.ErrorMessage,GLOBALCONSTANT.BlankString);
				ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ResponseObject,false);
			}

			ErrorMessage=GLOBALCONSTANT.BlankString;
			return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping__LoginScreen_ResponseObject,ObjListMenuMessages);

		}

		catch(JASCIEXCEPTION ObjDatabaseException)
		{
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_LoginScreen_loginPage);	
			return ObjModelAndView.addObject(GLOBALCONSTANT.CompanyLogo, StrPath);
		} 
		


	}


	/**
	 *Description This function design to check whether the user is authenticated or not 
	 *Input parameter ObjSecurityAuthorizations and ObjResult
	 *Return Type "ModelAndView"
	 *Created By "Diksha Gupta"
	 *Created Date "oct 14 2014" 
	 * */

	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_ResponseScreen_Action, method = RequestMethod.POST)
	public ModelAndView AuthenticateUser(HttpServletResponse response, HttpServletRequest request, @ModelAttribute(GLOBALCONSTANT.RequestMapping__LoginScreen_Validate) SECURITYAUTHORIZATIONS ObjSecurityAuthorizations, BindingResult ObjResult) throws ParseException,JASCIEXCEPTION 
	{

		

		ModelAndView ObjModelAndView=new ModelAndView();
		

		/**get userId and password using @ModelAttribute() for class SECURITYAUTHORIZATIONS*/

		String StrUserId=ObjSecurityAuthorizations.getUserId();
		String StrPassword=ObjSecurityAuthorizations.getPassword();
		String StrResult=GLOBALCONSTANT.BlankString;
		String ClientBrowserTimeZone=request.getParameter(GLOBALCONSTANT.ClientDeviceTimeZone);
		String LastActivityDateStringH=request.getParameter(GLOBALCONSTANT.LastActivityDateStringH);
		GlobalLastActivityDateStringH=LastActivityDateStringH;
		String value = request.getParameter(GLOBALCONSTANT.Remember);
		boolean rememberMe = false;
		if (value != null && value.equalsIgnoreCase(GLOBALCONSTANT.ON)) {
			rememberMe = true;
		}

		/** Call this method for Check, add and remove cookies  */
		ObjLoginServiceImpl.saveRememberMeToken(response,request, StrUserId,rememberMe);

		/**call this function ValidateUser(StrUsername,StrPassword) to authenticate user which returns a boolean value*/
		try
		{
			OBJCOMMONSESSIONBE.setClietnDeviceTimeZone(ClientBrowserTimeZone);
			
			OBJCOMMONSESSIONBE = ObjLoginServiceImpl.ValidateUser(StrUserId, StrPassword,OBJCOMMONSESSIONBE,LastActivityDateStringH);
			StrResult=OBJCOMMONSESSIONBE.getUserLoginDetails();

		}

		catch(Exception ObjNotAValidUserException)
		{

			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);

			if(ObjListMenuMessages!=null)
			{

				ErrorMessage=ObjLoginBe.getStrErrorMessageInvalidUser();
				return ObjModelAndView;
			}
		}


		if(StrResult.equals(GLOBALCONSTANT.Password_Expired))
		{

			ObjPasswordExpiryBe=ObjLoginServiceImpl.SetPasswordExpiryBe();
			OBJCOMMONSESSIONBE.setUserID(StrUserId);
			OBJCOMMONSESSIONBE.setPassword(StrPassword);


			screentype = GLOBALCONSTANT.IntZero;     
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_SetPasswordScreen_Action);
			
			
		}	

		else if(StrResult.equals(GLOBALCONSTANT.Password_matched_Response))	
		{

			OBJCOMMONSESSIONBE.setUserID(StrUserId);
			OBJCOMMONSESSIONBE.setPassword(StrPassword);
			


			//return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_ResponsePage);
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_MenuExecutionScreen_Action);

		}
		else if(StrResult.equals(GLOBALCONSTANT.Not_Allowed))
		{
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);

			ErrorMessage=ObjLoginBe.getStrErrorMessageNumberAttempts();
			return ObjModelAndView;


		}
		else if(StrResult.equals(GLOBALCONSTANT.TemporaryPassword))
		{
			ObjPasswordExpiryBe=ObjLoginServiceImpl.SetPasswordExpiryBe();
			OBJCOMMONSESSIONBE.setUserID(StrUserId);
			OBJCOMMONSESSIONBE.setPassword(StrPassword);

			screentype = GLOBALCONSTANT.IntOne;
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_SetPasswordScreen_Action); 


		}else if(StrResult.equals(LANGUANGELABELSAPI.KP_You_are_not_allowed_to_login_as_the_company_you_are_associated_with_is_inactive_Please_contact_administrator)){
			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
			ErrorMessage=ObjLoginBe.getStrErrorMessageLoginNotAllowedCompanyDeleted();
			return ObjModelAndView;
		}
		else
		{


			ObjModelAndView=new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
			ErrorMessage=ObjLoginBe.getStrErrorMessageInvalidUser();
			return ObjModelAndView;

		}

	}


	/**
	 *Description This function design to set new password
	 *Input parameter ObjSetNewPassword and ObjResult
	 *Return Type ModelAndView
	 *Created By "Diksha Gupta"
	 *Created Date "oct 29 2014" 
	 */

	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_SetPasswordScreen_Action, method = RequestMethod.POST)
	public ModelAndView SetPassword(@ModelAttribute(GLOBALCONSTANT.RequestMapping_SetPasswordScreen_SetPassword) SETNEWPASSWORD ObjSetNewPassword, BindingResult ObjResult,HttpServletRequest request) throws JASCIEXCEPTION
	{

		String StrPassword=ObjSetNewPassword.getPassword();
		String StrUserId=ObjSetNewPassword.getStruserId();
		String StrConfirmPassword=ObjSetNewPassword.getUserId();
		String LastActivityDateStringH=request.getParameter(GLOBALCONSTANT.LastActivityDateStringH);
		
		String StrRegex=GLOBALCONSTANT.Password_Regex;
		boolean BoolResult=RegularExpressionValidator(StrRegex,StrPassword);
		if(BoolResult)
		{

			if(StrPassword.equals(StrConfirmPassword))
			{

				List<SECURITYAUTHORIZATIONS> ObjListSecurity;
				SECURITYAUTHORIZATIONS ObjSecurityAuthorizations=null;
				ModelAndView ObjModelAndView=new ModelAndView();
				try 
				{
					ObjListSecurity = ObjLoginServiceImpl.GetLastPasswords(StrUserId.toUpperCase());
					Iterator<SECURITYAUTHORIZATIONS> ObjIteratorSecurity=ObjListSecurity.iterator();
					ObjSecurityAuthorizations=ObjIteratorSecurity.next();
				}
				catch( JASCIEXCEPTION ObjDatabaseNotFoundException)
				{}
				
				if(StrPassword.equals(ObjSecurityAuthorizations.getLastPassword1()) || StrPassword.equals(ObjSecurityAuthorizations.getLastPassword2()) || StrPassword.equals(ObjSecurityAuthorizations.getLastPassword3()) || StrPassword.equals(ObjSecurityAuthorizations.getLastPassword4()) || StrPassword.equals(ObjSecurityAuthorizations.getLastPassword5()))
				{
					/**if password is expired redirect to setnewpassword screen*/

					ObjPasswordExpiryBe=ObjLoginServiceImpl.SetPasswordExpiryBe();
					ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_NewPassword_Page,GLOBALCONSTANT.StrUserId,StrUserId.toUpperCase());
					ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_PasswordExpiryScreen_ScreenLabelsObject,ObjPasswordExpiryBe);
					return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ValidationResult,GLOBALCONSTANT.PasswordUsedMessage);
				}
				else
				{
					/**to update the password field*/ 

					try
					{
						ObjLoginServiceImpl.SetNewPassword(StrUserId.toUpperCase(),StrPassword,LastActivityDateStringH);
						OBJCOMMONSESSIONBE.setUserID(GLOBALCONSTANT.BlankString);
						OBJCOMMONSESSIONBE.setPassword(GLOBALCONSTANT.BlankString);
						return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
					}
					catch( JASCIEXCEPTION ObjDatabaseNotFoundException)
					{

					}
					
					return ObjModelAndView;
				}
			}
			else
			{
				/** password and confirm password is not same.*/

				ObjPasswordExpiryBe=ObjLoginServiceImpl.SetPasswordExpiryBe();
				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_NewPassword_Page,GLOBALCONSTANT.StrUserId,StrUserId.toUpperCase());
				ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_PasswordExpiryScreen_ScreenLabelsObject,ObjPasswordExpiryBe);
				return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ValidationResult	,GLOBALCONSTANT.PasswordNotMatchedMessage);

			}
		}

		else
		{
			/** password is not in valid pattern*/
			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_NewPassword_Page,GLOBALCONSTANT.StrUserId,StrUserId.toUpperCase());
			ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_PasswordExpiryScreen_ScreenLabelsObject,ObjPasswordExpiryBe);
			return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ValidationResult	,GLOBALCONSTANT.PasswordPatternMessage);

		}

	}



	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_SetPasswordScreen_Action, method = RequestMethod.GET)
	public ModelAndView GetSetPasswordPage()
	{


		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_NewPassword_Page);

		if(OBJCOMMONSESSIONBE.getUserID().equals(GLOBALCONSTANT.BlankString) || OBJCOMMONSESSIONBE.getPassword().equals(GLOBALCONSTANT.BlankString) ){
			return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
		}

		if(screentype==GLOBALCONSTANT.IntZero)
		{
			ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ValidationResult,GLOBALCONSTANT.PasswordExpiredMessage); 
		}else if(screentype==GLOBALCONSTANT.IntOne){
			ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_ResponseScreen_ValidationResult,GLOBALCONSTANT.TemporaryPasswordMessage); 
		}

		ObjModelAndView.addObject(GLOBALCONSTANT.StrUserId, OBJCOMMONSESSIONBE.getUserID().toUpperCase());
		OBJCOMMONSESSIONBE.setUserID(GLOBALCONSTANT.BlankString);
		OBJCOMMONSESSIONBE.setPassword(GLOBALCONSTANT.BlankString);
		return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping_PasswordExpiryScreen_ScreenLabelsObject,ObjPasswordExpiryBe);

	}


	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_ForgotPasswordScreen_Action, method = RequestMethod.POST)
	public ModelAndView EmailIdForPassword(@ModelAttribute(GLOBALCONSTANT.ModelAttribute_LoginScreen_ForgotPassword) SETNEWPASSWORD ObjSetNewPassword, BindingResult ObjResult,HttpServletRequest request) throws JASCIEXCEPTION, JASCIEXCEPTION
	{
		/**if emailid doesnt exist in database redirect to page
		 * else send a mail on that and go back to  page with message
		 */
		String LastActivityDateStringH=request.getParameter(GLOBALCONSTANT.LastActivityDateStringH);
		String StrEmailId=ObjSetNewPassword.getEmail();

		
		try{

			boolean BoolResult=ObjLoginServiceImpl.checkEmailId(StrEmailId,LastActivityDateStringH);
			if(BoolResult)
			{
				return new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);


			}
			else
			{

				ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_ForgotPasswordScreen_Action);
				StrResult=GLOBALCONSTANT.Security_StringFalse;
				EmailErrorMessage=GLOBALCONSTANT.EnterYourEmailAgain;
				return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping__LoginScreen_ScreenLabelsObject,ObjLoginBe);
			}
		}
		catch(JASCIEXCEPTION ObjSpringException)
		{

			ObjModelAndView= new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_ForgotPasswordScreen_Action);
			StrResult=GLOBALCONSTANT.Security_StringTrue;
			EmailErrorMessage=ObjSpringException.getMessage();
			return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping__LoginScreen_ScreenLabelsObject,ObjLoginBe);
		}

	}

	/**
	 *Description This function design to redirect on login page and display Menu messages
	 *Input parameter none
	 *Return Type: ModelAndView
	 *Created By: "Diksha Gupta
	 *Created Date: "oct 14 2014 
	 * */


	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_ForgotPasswordScreen_Action, method = RequestMethod.GET)
	public ModelAndView GetForgotPasswordPage() throws JASCIEXCEPTION
	{

		/** call getMessage() of LOGINSERVICEIMPL class to get MenuMessages from database from table MenuMessage.
		 *Modelandview Object to send data to login page.
		 * Model is a map through which we pass the objects to the page.
		 * view is the page on the data is to be shown.
		 */
		
		ObjModelAndView= new ModelAndView(GLOBALCONSTANT.RequestMapping_LoginScreen_ForgetPasswordPage);
		ObjModelAndView.addObject(GLOBALCONSTANT.ObjCommonSession,OBJCOMMONSESSIONBE);

		ObjLoginBe=ObjLoginServiceImpl.SetLoginBe();

		if(StrResult.equals(GLOBALCONSTANT.Security_StringFalse))
		{
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message,EmailErrorMessage);
			
			ObjModelAndView.addObject(GLOBALCONSTANT.Result,false);
		}
		else if(StrResult.equals(GLOBALCONSTANT.Security_StringTrue))
		{
			ObjModelAndView.addObject(GLOBALCONSTANT.Security_V_obj_Message,EmailErrorMessage);	
			ObjModelAndView.addObject(GLOBALCONSTANT.Result,true);
		}
		else
		{
			ObjModelAndView.addObject(GLOBALCONSTANT.Result,GLOBALCONSTANT.BlankString);
		}
		EmailErrorMessage=GLOBALCONSTANT.BlankString;
		StrResult=GLOBALCONSTANT.BlankString;


		return ObjModelAndView.addObject(GLOBALCONSTANT.RequestMapping__LoginScreen_ScreenLabelsObject,ObjLoginBe);


	}

	
	/** Call this request mapping for logout a user */
	
	@RequestMapping(value=GLOBALCONSTANT.RequestMapping_Logout_Action, method = RequestMethod.GET)
	public ModelAndView logoutUser(HttpSession session)
	{

		OBJCOMMONSESSIONBE.setUserID(GLOBALCONSTANT.BlankString);
		OBJCOMMONSESSIONBE.setPassword(GLOBALCONSTANT.BlankString);
		session.invalidate();
		return  new ModelAndView(GLOBALCONSTANT.redirect+GLOBALCONSTANT.colon+GLOBALCONSTANT.RequestMapping_LoginScreen_Action);
	}

	/** Check the string pattern*/
	public static boolean RegularExpressionValidator(String strRezix,String strRezixPattern) 
	{

		Pattern pattern = Pattern.compile(strRezix);

		Matcher matcher = pattern.matcher(strRezixPattern);
		if (matcher.find() && !(strRezixPattern.contains(GLOBALCONSTANT.Single_Space))) 
		{
			return true;

		} else 
		{
			return false;
		}

	}


}
