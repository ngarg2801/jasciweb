/**

Date Developed: May 13 2015
Description: This interface will be implement in execution class
Created By: Sarvendra Tyagi
 */
package com.jasci.biz.execution;

import com.jasci.biz.AdminModule.be.SEQUENCEBE;
import com.jasci.biz.AdminModule.be.SHAREDDATA;

public interface IEXECUTIONENGINE {

	/**
	 * @description: This method will use in execution class
	 * @param: objSharedData
	 * @param: objInputValueBE
	
	 * @return: objSharedData
	 * @developedby: Sarvendra Tyagi
	 * @Date: may 14 2015
	 */
	SHAREDDATA Execute(SHAREDDATA objSharedData, SEQUENCEBE objSequence);
	
}
